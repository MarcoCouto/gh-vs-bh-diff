package android.support.multidex;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.util.Log;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

final class MultiDexExtractor {
    private static final int BUFFER_SIZE = 16384;
    private static final String DEX_PREFIX = "classes";
    private static final String DEX_SUFFIX = ".dex";
    private static final String EXTRACTED_NAME_EXT = ".classes";
    private static final String EXTRACTED_SUFFIX = ".zip";
    private static final String KEY_CRC = "crc";
    private static final String KEY_DEX_CRC = "dex.crc.";
    private static final String KEY_DEX_NUMBER = "dex.number";
    private static final String KEY_DEX_TIME = "dex.time.";
    private static final String KEY_TIME_STAMP = "timestamp";
    private static final String LOCK_FILENAME = "MultiDex.lock";
    private static final int MAX_EXTRACT_ATTEMPTS = 3;
    private static final long NO_VALUE = -1;
    private static final String PREFS_FILE = "multidex.version";
    private static final String TAG = "MultiDex";

    private static class ExtractedDex extends File {
        public long crc = -1;

        public ExtractedDex(File file, String str) {
            super(file, str);
        }
    }

    MultiDexExtractor() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x011e A[SYNTHETIC, Splitter:B:40:0x011e] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x013e  */
    static List<? extends File> load(Context context, File file, File file2, String str, boolean z) throws IOException {
        FileLock fileLock;
        FileChannel fileChannel;
        String str2;
        List performExtractions;
        Throwable e;
        String str3 = str;
        boolean z2 = z;
        String str4 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("MultiDexExtractor.load(");
        sb.append(file.getPath());
        sb.append(", ");
        sb.append(z2);
        sb.append(", ");
        sb.append(str3);
        sb.append(")");
        Log.i(str4, sb.toString());
        long zipCrc = getZipCrc(file);
        File file3 = new File(file2, LOCK_FILENAME);
        RandomAccessFile randomAccessFile = new RandomAccessFile(file3, "rw");
        try {
            fileChannel = randomAccessFile.getChannel();
            String str5 = TAG;
            try {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Blocking on lock ");
                sb2.append(file3.getPath());
                Log.i(str5, sb2.toString());
                fileLock = fileChannel.lock();
                str2 = TAG;
                List list = performExtractions;
                if (fileLock != null) {
                    try {
                        fileLock.release();
                    } catch (IOException e2) {
                        e = e2;
                        String str6 = TAG;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Failed to release lock on ");
                        sb3.append(file3.getPath());
                        Log.e(str6, sb3.toString());
                    }
                }
                e = null;
                if (fileChannel != null) {
                    closeQuietly(fileChannel);
                }
                closeQuietly(randomAccessFile);
                if (e == null) {
                    String str7 = TAG;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("load found ");
                    sb4.append(list.size());
                    sb4.append(" secondary dex files");
                    Log.i(str7, sb4.toString());
                    return list;
                }
                throw e;
            } catch (Throwable th) {
                th = th;
                fileLock = null;
                if (fileLock != null) {
                    try {
                        fileLock.release();
                    } catch (IOException unused) {
                        String str8 = TAG;
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("Failed to release lock on ");
                        sb5.append(file3.getPath());
                        Log.e(str8, sb5.toString());
                    }
                }
                if (fileChannel != null) {
                    closeQuietly(fileChannel);
                }
                closeQuietly(randomAccessFile);
                throw th;
            }
            try {
                StringBuilder sb6 = new StringBuilder();
                sb6.append(file3.getPath());
                sb6.append(" locked");
                Log.i(str2, sb6.toString());
                if (!z2) {
                    Context context2 = context;
                    File file4 = file;
                    if (!isModified(context, file, zipCrc, str3)) {
                        performExtractions = loadExistingExtractions(context, file, file2, str);
                    }
                    Log.i(TAG, "Detected that extraction must be performed.");
                    performExtractions = performExtractions(file, file2);
                    putStoredApkInfo(context, str, getTimeStamp(file), zipCrc, performExtractions);
                } else {
                    Context context3 = context;
                    File file5 = file;
                    Log.i(TAG, "Detected that extraction must be performed.");
                    performExtractions = performExtractions(file, file2);
                    putStoredApkInfo(context, str, getTimeStamp(file), zipCrc, performExtractions);
                }
            } catch (IOException e3) {
                Log.w(TAG, "Failed to reload existing extracted secondary dex files, falling back to fresh extraction", e3);
                performExtractions = performExtractions(file, file2);
                putStoredApkInfo(context, str, getTimeStamp(file), zipCrc, performExtractions);
            } catch (Throwable th2) {
                th = th2;
                if (fileLock != null) {
                }
                if (fileChannel != null) {
                }
                closeQuietly(randomAccessFile);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileChannel = null;
            fileLock = null;
            if (fileLock != null) {
            }
            if (fileChannel != null) {
            }
            closeQuietly(randomAccessFile);
            throw th;
        }
    }

    private static List<ExtractedDex> loadExistingExtractions(Context context, File file, File file2, String str) throws IOException {
        String str2 = str;
        Log.i(TAG, "loading existing secondary dex files");
        StringBuilder sb = new StringBuilder();
        sb.append(file.getName());
        sb.append(EXTRACTED_NAME_EXT);
        String sb2 = sb.toString();
        SharedPreferences multiDexPreferences = getMultiDexPreferences(context);
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str2);
        sb3.append(KEY_DEX_NUMBER);
        int i = multiDexPreferences.getInt(sb3.toString(), 1);
        ArrayList arrayList = new ArrayList(i - 1);
        int i2 = 2;
        while (i2 <= i) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(sb2);
            sb4.append(i2);
            sb4.append(EXTRACTED_SUFFIX);
            ExtractedDex extractedDex = new ExtractedDex(file2, sb4.toString());
            if (extractedDex.isFile()) {
                extractedDex.crc = getZipCrc(extractedDex);
                StringBuilder sb5 = new StringBuilder();
                sb5.append(str2);
                sb5.append(KEY_DEX_CRC);
                sb5.append(i2);
                long j = multiDexPreferences.getLong(sb5.toString(), -1);
                StringBuilder sb6 = new StringBuilder();
                sb6.append(str2);
                sb6.append(KEY_DEX_TIME);
                sb6.append(i2);
                long j2 = multiDexPreferences.getLong(sb6.toString(), -1);
                long lastModified = extractedDex.lastModified();
                if (j2 == lastModified) {
                    String str3 = sb2;
                    SharedPreferences sharedPreferences = multiDexPreferences;
                    if (j == extractedDex.crc) {
                        arrayList.add(extractedDex);
                        i2++;
                        sb2 = str3;
                        multiDexPreferences = sharedPreferences;
                    }
                }
                StringBuilder sb7 = new StringBuilder();
                sb7.append("Invalid extracted dex: ");
                sb7.append(extractedDex);
                sb7.append(" (key \"");
                sb7.append(str2);
                sb7.append("\"), expected modification time: ");
                sb7.append(j2);
                sb7.append(", modification time: ");
                sb7.append(lastModified);
                sb7.append(", expected crc: ");
                sb7.append(j);
                sb7.append(", file crc: ");
                sb7.append(extractedDex.crc);
                throw new IOException(sb7.toString());
            }
            StringBuilder sb8 = new StringBuilder();
            sb8.append("Missing extracted secondary dex file '");
            sb8.append(extractedDex.getPath());
            sb8.append("'");
            throw new IOException(sb8.toString());
        }
        return arrayList;
    }

    private static boolean isModified(Context context, File file, long j, String str) {
        SharedPreferences multiDexPreferences = getMultiDexPreferences(context);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("timestamp");
        if (multiDexPreferences.getLong(sb.toString(), -1) == getTimeStamp(file)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(KEY_CRC);
            if (multiDexPreferences.getLong(sb2.toString(), -1) == j) {
                return false;
            }
        }
        return true;
    }

    private static long getTimeStamp(File file) {
        long lastModified = file.lastModified();
        return lastModified == -1 ? lastModified - 1 : lastModified;
    }

    private static long getZipCrc(File file) throws IOException {
        long zipCrc = ZipUtil.getZipCrc(file);
        return zipCrc == -1 ? zipCrc - 1 : zipCrc;
    }

    private static List<ExtractedDex> performExtractions(File file, File file2) throws IOException {
        ExtractedDex extractedDex;
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append(file.getName());
        sb.append(EXTRACTED_NAME_EXT);
        String sb2 = sb.toString();
        prepareDexDir(file2, sb2);
        ArrayList arrayList = new ArrayList();
        ZipFile zipFile = new ZipFile(file);
        int i = 2;
        try {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(DEX_PREFIX);
            sb3.append(2);
            sb3.append(DEX_SUFFIX);
            ZipEntry entry = zipFile.getEntry(sb3.toString());
            while (entry != null) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append(sb2);
                sb4.append(i);
                sb4.append(EXTRACTED_SUFFIX);
                extractedDex = new ExtractedDex(file2, sb4.toString());
                arrayList.add(extractedDex);
                String str = TAG;
                StringBuilder sb5 = new StringBuilder();
                sb5.append("Extraction is needed for file ");
                sb5.append(extractedDex);
                Log.i(str, sb5.toString());
                int i2 = 0;
                z = false;
                while (i2 < 3 && !z) {
                    i2++;
                    extract(zipFile, entry, extractedDex, sb2);
                    extractedDex.crc = getZipCrc(extractedDex);
                    z = true;
                    String str2 = TAG;
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("Extraction ");
                    sb6.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : ParametersKeys.FAILED);
                    sb6.append(" - length ");
                    sb6.append(extractedDex.getAbsolutePath());
                    sb6.append(": ");
                    sb6.append(extractedDex.length());
                    sb6.append(" - crc: ");
                    sb6.append(extractedDex.crc);
                    Log.i(str2, sb6.toString());
                    if (!z) {
                        extractedDex.delete();
                        if (extractedDex.exists()) {
                            String str3 = TAG;
                            StringBuilder sb7 = new StringBuilder();
                            sb7.append("Failed to delete corrupted secondary dex '");
                            sb7.append(extractedDex.getPath());
                            sb7.append("'");
                            Log.w(str3, sb7.toString());
                        }
                    }
                }
                if (z) {
                    i++;
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(DEX_PREFIX);
                    sb8.append(i);
                    sb8.append(DEX_SUFFIX);
                    entry = zipFile.getEntry(sb8.toString());
                } else {
                    StringBuilder sb9 = new StringBuilder();
                    sb9.append("Could not create zip file ");
                    sb9.append(extractedDex.getAbsolutePath());
                    sb9.append(" for secondary dex (");
                    sb9.append(i);
                    sb9.append(")");
                    throw new IOException(sb9.toString());
                }
            }
            try {
                zipFile.close();
            } catch (IOException e) {
                Log.w(TAG, "Failed to close resource", e);
            }
            return arrayList;
        } catch (IOException e2) {
            String str4 = TAG;
            StringBuilder sb10 = new StringBuilder();
            sb10.append("Failed to read crc from ");
            sb10.append(extractedDex.getAbsolutePath());
            Log.w(str4, sb10.toString(), e2);
            z = false;
        } catch (Throwable th) {
            try {
                zipFile.close();
            } catch (IOException e3) {
                Log.w(TAG, "Failed to close resource", e3);
            }
            throw th;
        }
    }

    private static void putStoredApkInfo(Context context, String str, long j, long j2, List<ExtractedDex> list) {
        Editor edit = getMultiDexPreferences(context).edit();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("timestamp");
        edit.putLong(sb.toString(), j);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(KEY_CRC);
        edit.putLong(sb2.toString(), j2);
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append(KEY_DEX_NUMBER);
        edit.putInt(sb3.toString(), list.size() + 1);
        int i = 2;
        for (ExtractedDex extractedDex : list) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append(KEY_DEX_CRC);
            sb4.append(i);
            edit.putLong(sb4.toString(), extractedDex.crc);
            StringBuilder sb5 = new StringBuilder();
            sb5.append(str);
            sb5.append(KEY_DEX_TIME);
            sb5.append(i);
            edit.putLong(sb5.toString(), extractedDex.lastModified());
            i++;
        }
        edit.commit();
    }

    private static SharedPreferences getMultiDexPreferences(Context context) {
        return context.getSharedPreferences(PREFS_FILE, VERSION.SDK_INT < 11 ? 0 : 4);
    }

    private static void prepareDexDir(File file, final String str) {
        File[] listFiles = file.listFiles(new FileFilter() {
            public boolean accept(File file) {
                String name = file.getName();
                return !name.startsWith(str) && !name.equals(MultiDexExtractor.LOCK_FILENAME);
            }
        });
        if (listFiles == null) {
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to list secondary dex dir content (");
            sb.append(file.getPath());
            sb.append(").");
            Log.w(str2, sb.toString());
            return;
        }
        for (File file2 : listFiles) {
            String str3 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Trying to delete old file ");
            sb2.append(file2.getPath());
            sb2.append(" of size ");
            sb2.append(file2.length());
            Log.i(str3, sb2.toString());
            if (!file2.delete()) {
                String str4 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Failed to delete old file ");
                sb3.append(file2.getPath());
                Log.w(str4, sb3.toString());
            } else {
                String str5 = TAG;
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Deleted old file ");
                sb4.append(file2.getPath());
                Log.i(str5, sb4.toString());
            }
        }
    }

    private static void extract(ZipFile zipFile, ZipEntry zipEntry, File file, String str) throws IOException, FileNotFoundException {
        ZipOutputStream zipOutputStream;
        InputStream inputStream = zipFile.getInputStream(zipEntry);
        StringBuilder sb = new StringBuilder();
        sb.append("tmp-");
        sb.append(str);
        File createTempFile = File.createTempFile(sb.toString(), EXTRACTED_SUFFIX, file.getParentFile());
        String str2 = TAG;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Extracting ");
        sb2.append(createTempFile.getPath());
        Log.i(str2, sb2.toString());
        try {
            zipOutputStream = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(createTempFile)));
            ZipEntry zipEntry2 = new ZipEntry("classes.dex");
            zipEntry2.setTime(zipEntry.getTime());
            zipOutputStream.putNextEntry(zipEntry2);
            byte[] bArr = new byte[16384];
            for (int read = inputStream.read(bArr); read != -1; read = inputStream.read(bArr)) {
                zipOutputStream.write(bArr, 0, read);
            }
            zipOutputStream.closeEntry();
            zipOutputStream.close();
            if (createTempFile.setReadOnly()) {
                String str3 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Renaming to ");
                sb3.append(file.getPath());
                Log.i(str3, sb3.toString());
                if (createTempFile.renameTo(file)) {
                    closeQuietly(inputStream);
                    createTempFile.delete();
                    return;
                }
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Failed to rename \"");
                sb4.append(createTempFile.getAbsolutePath());
                sb4.append("\" to \"");
                sb4.append(file.getAbsolutePath());
                sb4.append("\"");
                throw new IOException(sb4.toString());
            }
            StringBuilder sb5 = new StringBuilder();
            sb5.append("Failed to mark readonly \"");
            sb5.append(createTempFile.getAbsolutePath());
            sb5.append("\" (tmp of \"");
            sb5.append(file.getAbsolutePath());
            sb5.append("\")");
            throw new IOException(sb5.toString());
        } catch (Throwable th) {
            closeQuietly(inputStream);
            createTempFile.delete();
            throw th;
        }
    }

    private static void closeQuietly(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            Log.w(TAG, "Failed to close resource", e);
        }
    }
}
