package io.presage;

import android.content.Context;

public final class cx {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final cy b;
    private final cy c;
    private final cy d;
    private final cy e;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static cx a(Context context, PontlEveque pontlEveque) {
            Taleggio taleggio = new Taleggio(context);
            da daVar = new da(taleggio);
            cy czVar = new cz(taleggio, RaclettedeSavoiefumee.a(pontlEveque));
            cx cxVar = new cx(daVar, new cw(taleggio, czVar, pontlEveque), new cv(), czVar, 0);
            return cxVar;
        }
    }

    private cx(cy cyVar, cy cyVar2, cy cyVar3, cy cyVar4) {
        this.b = cyVar;
        this.c = cyVar2;
        this.d = cyVar3;
        this.e = cyVar4;
    }

    public /* synthetic */ cx(cy cyVar, cy cyVar2, cy cyVar3, cy cyVar4, byte b2) {
        this(cyVar, cyVar2, cyVar3, cyVar4);
    }

    public final void a(db dbVar) {
        this.b.a(dbVar.getMraidCommandExecutor());
    }

    public final void b(db dbVar) {
        this.c.a(dbVar.getMraidCommandExecutor());
    }

    public final void c(db dbVar) {
        this.d.a(dbVar.getMraidCommandExecutor());
    }

    public final void d(db dbVar) {
        this.e.a(dbVar.getMraidCommandExecutor());
    }
}
