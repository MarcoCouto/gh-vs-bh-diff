package io.presage;

public final class Langres {
    public static final Langres a = new Langres();
    private static int b = -1;

    private Langres() {
    }

    public static int a() {
        if (b == -1) {
            b = b();
        }
        return b;
    }

    private static int b() {
        if (c()) {
            return 1;
        }
        if (d()) {
            return 2;
        }
        if (f()) {
            return 3;
        }
        return e() ? 4 : 0;
    }

    private static boolean a(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    private static boolean c() {
        return a("com.unity3d.player.UnityPlayer");
    }

    private static boolean d() {
        return a("org.apache.cordova.CordovaWebView");
    }

    private static boolean e() {
        return a("com.adobe.fre.FREFunction");
    }

    private static boolean f() {
        return a("mono.android.Runtime");
    }
}
