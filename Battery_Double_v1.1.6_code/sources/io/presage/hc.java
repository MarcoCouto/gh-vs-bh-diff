package io.presage;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class hc<T> implements hr, Iterator<T> {
    private int a;
    private final T[] b;

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public hc(T[] tArr) {
        this.b = tArr;
    }

    public final boolean hasNext() {
        return this.a < this.b.length;
    }

    public final T next() {
        try {
            T[] tArr = this.b;
            int i = this.a;
            this.a = i + 1;
            return tArr[i];
        } catch (ArrayIndexOutOfBoundsException e) {
            this.a--;
            throw new NoSuchElementException(e.getMessage());
        }
    }
}
