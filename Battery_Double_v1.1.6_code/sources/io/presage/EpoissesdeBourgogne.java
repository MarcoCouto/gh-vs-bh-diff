package io.presage;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.FragmentLifecycleCallbacks;

public final class EpoissesdeBourgogne implements CoeurdArras {
    private FragmentActivity a;
    private final Application b;
    private final CamembertauCalvados c;
    private final CamembertdeNormandie d;
    private final Chaource e;
    private final Entrammes f;
    private final aj g;

    public static final class CamembertauCalvados extends FragmentLifecycleCallbacks {
        final /* synthetic */ EpoissesdeBourgogne a;

        CamembertauCalvados(EpoissesdeBourgogne epoissesdeBourgogne) {
            this.a = epoissesdeBourgogne;
        }

        public final void onFragmentResumed(FragmentManager fragmentManager, Fragment fragment) {
            FragmentActivity activity = fragment.getActivity();
            FragmentManager supportFragmentManager = activity != null ? activity.getSupportFragmentManager() : null;
            if (supportFragmentManager != null) {
                this.a.a(supportFragmentManager);
            }
        }

        public final void onFragmentPaused(FragmentManager fragmentManager, Fragment fragment) {
            FragmentActivity activity = fragment.getActivity();
            FragmentManager supportFragmentManager = activity != null ? activity.getSupportFragmentManager() : null;
            if (supportFragmentManager != null) {
                this.a.b(supportFragmentManager);
            }
        }
    }

    public static final class CamembertdeNormandie extends GrandMunster {
        final /* synthetic */ EpoissesdeBourgogne a;

        CamembertdeNormandie(EpoissesdeBourgogne epoissesdeBourgogne) {
            this.a = epoissesdeBourgogne;
        }

        public final void onActivityResumed(Activity activity) {
            if (activity instanceof FragmentActivity) {
                this.a.a((FragmentActivity) activity);
            }
        }

        public final void onActivityPaused(Activity activity) {
            if (activity instanceof FragmentActivity) {
                this.a.b((FragmentActivity) activity);
            }
        }
    }

    public EpoissesdeBourgogne(Activity activity, Chaource chaource, Entrammes entrammes, aj ajVar) {
        this.e = chaource;
        this.f = entrammes;
        this.g = ajVar;
        this.a = (FragmentActivity) activity;
        this.b = activity.getApplication();
        this.c = new CamembertauCalvados(this);
        this.d = new CamembertdeNormandie(this);
    }

    public /* synthetic */ EpoissesdeBourgogne(Activity activity, Chaource chaource, Entrammes entrammes, aj ajVar, int i, hi hiVar) {
        if ((i & 8) != 0) {
            ajVar = aj.a;
        }
        this(activity, chaource, entrammes, ajVar);
    }

    /* access modifiers changed from: private */
    public final void a(FragmentActivity fragmentActivity) {
        this.a = fragmentActivity;
        fragmentActivity.getSupportFragmentManager().registerFragmentLifecycleCallbacks(this.c, true);
    }

    /* access modifiers changed from: private */
    public final void b(FragmentActivity fragmentActivity) {
        fragmentActivity.getSupportFragmentManager().unregisterFragmentLifecycleCallbacks(this.c);
        this.e.a();
    }

    /* access modifiers changed from: private */
    public final void a(FragmentManager fragmentManager) {
        c(fragmentManager);
    }

    /* access modifiers changed from: private */
    public final void b(FragmentManager fragmentManager) {
        c(fragmentManager);
    }

    private final void c(FragmentManager fragmentManager) {
        if (this.f.a(Echourgnacalaliqueurdenoix.a(fragmentManager))) {
            this.e.a(this.a);
        } else {
            this.e.a();
        }
    }

    public final void registerLifecycleListener() {
        this.b.registerActivityLifecycleCallbacks(this.d);
    }

    public final void injectInitialOverlay() {
        Activity a2 = aj.a();
        if (!(a2 instanceof FragmentActivity)) {
            a2 = null;
        }
        FragmentActivity fragmentActivity = (FragmentActivity) a2;
        if (fragmentActivity == null) {
            fragmentActivity = this.a;
        }
        a(fragmentActivity);
        FragmentManager supportFragmentManager = this.a.getSupportFragmentManager();
        hl.a((Object) supportFragmentManager, "fragmentActivity.supportFragmentManager");
        c(supportFragmentManager);
    }

    public final void unregisterLifecycleListener() {
        this.b.unregisterActivityLifecycleCallbacks(this.d);
        b(this.a);
    }
}
