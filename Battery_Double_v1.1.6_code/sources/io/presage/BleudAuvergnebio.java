package io.presage;

public final class BleudAuvergnebio implements Bethmale {
    private final BleudeGex a;
    private final Bouyssou b;

    private BleudAuvergnebio(BleudeGex bleudeGex, Bouyssou bouyssou) {
        this.a = bleudeGex;
        this.b = bouyssou;
    }

    public /* synthetic */ BleudAuvergnebio(BleudeGex bleudeGex) {
        this(bleudeGex, Bouyssou.a);
    }

    public final void a(CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
        camembertdeNormandie.a();
        abbayedeCiteauxentiere.f();
        abbayedeCiteauxentiere.l();
        camembertdeNormandie.setEnableDrag(false);
        camembertdeNormandie.c();
        String a2 = Bouyssou.a(new BoulettedAvesnes(camembertdeNormandie, abbayedeCiteauxentiere));
        PontlEveque d = abbayedeCiteauxentiere.d();
        if (d != null) {
            this.a.a(abbayedeCiteauxentiere.a(), a2, d, abbayedeCiteauxentiere.c());
        }
    }
}
