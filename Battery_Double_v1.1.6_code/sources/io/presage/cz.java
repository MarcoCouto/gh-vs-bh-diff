package io.presage;

import android.graphics.Rect;
import android.view.View;

public final class cz implements cy {
    private final Taleggio a;
    private final boolean b;

    public cz(Taleggio taleggio, boolean z) {
        this.a = taleggio;
        this.b = z;
    }

    public final void a(ax axVar) {
        int a2 = af.a(this.a.j());
        int a3 = af.a(this.a.k());
        Rect a4 = Taleggio.a((View) axVar.c());
        axVar.a(a2, a3);
        axVar.b(af.a(a4.width()), af.a(a4.height()));
        b(axVar);
    }

    private final void b(ax axVar) {
        String m = this.a.m();
        axVar.a(m, this.b);
        if (!this.b) {
            m = "none";
        }
        axVar.a(!this.b, m);
    }
}
