package io.presage;

import com.iab.omid.library.oguryco.adsession.AdSessionConfiguration;
import com.iab.omid.library.oguryco.adsession.AdSessionContext;

public final class du {
    private AdSessionContext a;
    private AdSessionConfiguration b;

    public final AdSessionContext a() {
        return this.a;
    }

    public final void a(AdSessionContext adSessionContext) {
        this.a = adSessionContext;
    }

    public final void a(AdSessionConfiguration adSessionConfiguration) {
        this.b = adSessionConfiguration;
    }

    public final AdSessionConfiguration b() {
        return this.b;
    }
}
