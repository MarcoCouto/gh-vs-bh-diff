package io.presage;

import android.annotation.SuppressLint;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;

public final class BurratadesPouilles {
    private ImageButton a = new ImageButton(this.d.getContext());
    private Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final AbbayedeCiteauxentiere c;
    private final ViewGroup d;
    private final PavedAremberg e;
    private final String f;

    static final class CamembertauCalvados implements OnClickListener {
        final /* synthetic */ BurratadesPouilles a;

        CamembertauCalvados(BurratadesPouilles burratadesPouilles) {
            this.a = burratadesPouilles;
        }

        public final void onClick(View view) {
            this.a.c.p();
            this.a.e();
        }
    }

    static final class CamembertdeNormandie implements Runnable {
        final /* synthetic */ BurratadesPouilles a;

        CamembertdeNormandie(BurratadesPouilles burratadesPouilles) {
            this.a = burratadesPouilles;
        }

        public final void run() {
            this.a.a();
        }
    }

    public BurratadesPouilles(AbbayedeCiteauxentiere abbayedeCiteauxentiere, ViewGroup viewGroup, PavedAremberg pavedAremberg, String str) {
        this.c = abbayedeCiteauxentiere;
        this.d = viewGroup;
        this.e = pavedAremberg;
        this.f = str;
        d();
    }

    @SuppressLint({"RtlHardcoded"})
    private final void d() {
        f();
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 5;
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        this.a.setLayoutParams(layoutParams2);
        this.a.setOnClickListener(new CamembertauCalvados(this));
        this.a.setVisibility(8);
        this.d.addView(this.a, layoutParams2);
    }

    /* access modifiers changed from: private */
    public final void e() {
        if (this.f.length() > 0) {
            this.e.b(this.f);
        }
    }

    private final void f() {
        if (VERSION.SDK_INT >= 16) {
            this.a.setBackground(null);
        } else {
            this.a.setBackgroundResource(0);
        }
        this.a.setImageResource(R.drawable.btn_presage_mraid_close);
    }

    public final void a(long j) {
        this.b.postDelayed(new CamembertdeNormandie(this), j);
    }

    public final void a() {
        this.a.setVisibility(0);
    }

    public final void b() {
        this.b.removeCallbacksAndMessages(null);
        this.a.setVisibility(8);
    }

    public final void c() {
        this.b.removeCallbacksAndMessages(null);
    }
}
