package io.presage;

import android.content.Context;

public final class cq {
    private final Context a;
    private final ct b;
    private final gf<ep> c;

    public cq(Context context, ct ctVar, gf<ep> gfVar) {
        this.a = context;
        this.b = ctVar;
        this.c = gfVar;
    }

    public final cr a(db dbVar) {
        cr crVar = new cr(this.a, dbVar.getMraidCommandExecutor(), this.b);
        crVar.a(this.c);
        return crVar;
    }
}
