package io.presage;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Set;

public final class bp {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final SharedPreferences b = this.c.getSharedPreferences("PERSISTED_SETS", 0);
    private final Context c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public bp(Context context) {
        this.c = context;
    }

    public final boolean a(String str) {
        Set stringSet = this.b.getStringSet("shortcutIdentifierList", null);
        if (stringSet != null) {
            return stringSet.contains(str);
        }
        return false;
    }

    public final String a(String str, String str2) {
        this.b.edit().putString("argsJson:".concat(String.valueOf(str)), str2).apply();
        return str;
    }

    public final String b(String str) {
        String string = this.b.getString("argsJson:".concat(String.valueOf(str)), "");
        return string == null ? "" : string;
    }

    public final boolean c(String str) {
        String string = this.b.getString("argsJson:".concat(String.valueOf(str)), "");
        return string != null && string.length() > 0;
    }
}
