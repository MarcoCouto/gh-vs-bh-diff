package io.presage;

import android.app.Activity;

public final class CapGrisNez {
    public static CarreMirabelle a(Activity activity, CremeuxduJura cremeuxduJura, e eVar) {
        CarreMirabelle carreMirabelle = new CarreMirabelle(eVar, activity.getClass());
        carreMirabelle.a(activity);
        carreMirabelle.a(cremeuxduJura.a());
        carreMirabelle.b(cremeuxduJura.b());
        return carreMirabelle;
    }
}
