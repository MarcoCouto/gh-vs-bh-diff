package io.presage;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map.Entry;

public final class dm implements df {
    private final dq a;
    private final dh b;

    public dm(dq dqVar, dh dhVar) {
        this.a = dqVar;
        this.b = dhVar;
    }

    public final dr a() {
        try {
            HttpURLConnection a2 = a(new URL(this.a.a()));
            b(a2);
            c(a2);
            int responseCode = a2.getResponseCode();
            if (dn.a(responseCode)) {
                return a(a2);
            }
            return new di(new dp(responseCode));
        } catch (Exception e) {
            return new di(e);
        }
    }

    private static ds a(HttpURLConnection httpURLConnection) {
        if (httpURLConnection.getContentLength() == 0) {
            return new ds("");
        }
        InputStream inputStream = httpURLConnection.getInputStream();
        hl.a((Object) inputStream, "inputStream");
        byte[] a2 = fw.a(inputStream);
        inputStream.close();
        String headerField = httpURLConnection.getHeaderField("content-encoding");
        if (headerField != null) {
            Locale locale = Locale.US;
            hl.a((Object) locale, "Locale.US");
            String lowerCase = headerField.toLowerCase(locale);
            hl.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            if (hl.a((Object) lowerCase, (Object) HttpRequest.ENCODING_GZIP)) {
                dj djVar = dj.a;
                return new ds(dj.a(a2));
            }
        }
        return new ds(new String(a2, is.a));
    }

    private final void b(HttpURLConnection httpURLConnection) {
        for (Entry entry : this.a.d().a().entrySet()) {
            httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0060  */
    private final void c(HttpURLConnection httpURLConnection) {
        OutputStream outputStream;
        Throwable th;
        byte[] bArr;
        if (this.a.c().length() > 0) {
            try {
                outputStream = httpURLConnection.getOutputStream();
                try {
                    if (dl.a(this.a.d())) {
                        dj djVar = dj.a;
                        bArr = dj.a(this.a.c());
                    } else {
                        String c = this.a.c();
                        Charset charset = is.a;
                        if (c != null) {
                            bArr = c.getBytes(charset);
                            hl.a((Object) bArr, "(this as java.lang.String).getBytes(charset)");
                        } else {
                            throw new em("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    outputStream.write(bArr);
                    if (outputStream != null) {
                        dg.a(outputStream);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (outputStream != null) {
                        dg.a(outputStream);
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                outputStream = null;
                if (outputStream != null) {
                }
                throw th;
            }
        }
    }

    private final HttpURLConnection a(URL url) throws IOException {
        URLConnection openConnection = url.openConnection();
        if (openConnection != null) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            httpURLConnection.setRequestProperty("Connection", "close");
            httpURLConnection.setReadTimeout(this.b.a());
            httpURLConnection.setConnectTimeout(this.b.b());
            httpURLConnection.setRequestMethod(this.a.b());
            httpURLConnection.setDoOutput(this.a.c().length() > 0);
            return httpURLConnection;
        }
        throw new em("null cannot be cast to non-null type java.net.HttpURLConnection");
    }
}
