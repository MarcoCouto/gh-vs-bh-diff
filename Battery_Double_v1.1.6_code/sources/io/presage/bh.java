package io.presage;

import android.content.Context;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public final class bh {
    private final Context a;
    private final FrameLayout b;
    private final PontlEveque c;

    public bh(Context context, FrameLayout frameLayout, PontlEveque pontlEveque) {
        this.a = context;
        this.b = frameLayout;
        this.c = pontlEveque;
    }

    public final db a(bq bqVar) {
        LayoutParams b2 = a(bqVar, (LayoutParams) null);
        db a2 = de.a(this.a, this.c);
        if (a2 == null) {
            return null;
        }
        a2.setTag(bqVar.c());
        bi.a(a2);
        this.b.addView(a2, b2);
        return a2;
    }

    public static void a(WebView webView, bq bqVar) {
        ViewGroup.LayoutParams layoutParams = webView.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            layoutParams = null;
        }
        webView.setLayoutParams(a(bqVar, (LayoutParams) layoutParams));
    }

    public final void a(WebView webView) {
        this.b.removeView(webView);
    }

    /* access modifiers changed from: private */
    public static LayoutParams a(bq bqVar, LayoutParams layoutParams) {
        if (layoutParams == null) {
            layoutParams = new LayoutParams(-1, -1);
        }
        b(bqVar, layoutParams);
        a(layoutParams, bqVar);
        return layoutParams;
    }

    private static void b(bq bqVar, LayoutParams layoutParams) {
        if (bqVar.g() != -1) {
            layoutParams.leftMargin = af.b(bqVar.g());
        }
        if (bqVar.f() != -1) {
            layoutParams.topMargin = af.b(bqVar.f());
        }
    }

    private static void a(LayoutParams layoutParams, bq bqVar) {
        int i = -1;
        layoutParams.width = bqVar.e() <= 0 ? -1 : af.b(bqVar.e());
        if (bqVar.d() > 0) {
            i = af.b(bqVar.d());
        }
        layoutParams.height = i;
    }
}
