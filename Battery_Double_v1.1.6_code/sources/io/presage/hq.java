package io.presage;

import java.util.List;

public class hq {
    private static <T extends Throwable> T a(T t) {
        return hl.a(t, hq.class.getName());
    }

    private static void a(Object obj, String str) {
        String name = obj == null ? "null" : obj.getClass().getName();
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" cannot be cast to ");
        sb.append(str);
        a(sb.toString());
    }

    private static void a(String str) {
        throw a(new ClassCastException(str));
    }

    private static ClassCastException a(ClassCastException classCastException) {
        throw ((ClassCastException) a((T) classCastException));
    }

    public static Iterable a(Object obj) {
        if ((obj instanceof hr) && !(obj instanceof ht)) {
            a(obj, "kotlin.collections.MutableIterable");
        }
        return c(obj);
    }

    private static Iterable c(Object obj) {
        try {
            return (Iterable) obj;
        } catch (ClassCastException e) {
            throw a(e);
        }
    }

    public static List b(Object obj) {
        if ((obj instanceof hr) && !(obj instanceof hu)) {
            a(obj, "kotlin.collections.MutableList");
        }
        return d(obj);
    }

    private static List d(Object obj) {
        try {
            return (List) obj;
        } catch (ClassCastException e) {
            throw a(e);
        }
    }
}
