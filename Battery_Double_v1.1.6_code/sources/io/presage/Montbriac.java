package io.presage;

import android.content.Context;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import org.json.JSONObject;

public final class Montbriac {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final PavedAremberg b;
    private final OlivetauPoivre c;
    private final Context d;

    static final /* synthetic */ class AbbayedeTamie extends hk implements gg<Throwable, ep> {
        AbbayedeTamie(ac acVar) {
            super(1, acVar);
        }

        public final ig b() {
            return hn.a(ac.class);
        }

        public final String c() {
            return "e";
        }

        public final String d() {
            return "e(Ljava/lang/Throwable;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            return ep.a;
        }
    }

    static final class AbbayedeTimadeuc extends hm implements gf<ep> {
        public static final AbbayedeTimadeuc a = new AbbayedeTimadeuc();

        AbbayedeTimadeuc() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a() {
            return ep.a;
        }
    }

    static final /* synthetic */ class AbbayeduMontdesCats extends hk implements gg<Throwable, ep> {
        AbbayeduMontdesCats(ac acVar) {
            super(1, acVar);
        }

        public final ig b() {
            return hn.a(ac.class);
        }

        public final String c() {
            return "e";
        }

        public final String d() {
            return "e(Ljava/lang/Throwable;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            return ep.a;
        }
    }

    static final class AffideliceauChablis extends hm implements gf<ep> {
        public static final AffideliceauChablis a = new AffideliceauChablis();

        AffideliceauChablis() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a() {
            return ep.a;
        }
    }

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static Montbriac a(Context context) {
            PersilledumontBlanc persilledumontBlanc = PersilledumontBlanc.a;
            return new Montbriac(PersilledumontBlanc.a(context), new OlivetauPoivre(context), context, 0);
        }
    }

    static final /* synthetic */ class CamembertdeNormandie extends hk implements gg<Throwable, ep> {
        CamembertdeNormandie(ac acVar) {
            super(1, acVar);
        }

        public final ig b() {
            return hn.a(ac.class);
        }

        public final String c() {
            return "e";
        }

        public final String d() {
            return "e(Ljava/lang/Throwable;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            return ep.a;
        }
    }

    static final class EcirdelAubrac extends hm implements gf<ep> {
        public static final EcirdelAubrac a = new EcirdelAubrac();

        EcirdelAubrac() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a() {
            return ep.a;
        }
    }

    private Montbriac(PavedAremberg pavedAremberg, OlivetauPoivre olivetauPoivre, Context context) {
        this.b = pavedAremberg;
        this.c = olivetauPoivre;
        this.d = context;
    }

    public /* synthetic */ Montbriac(PavedAremberg pavedAremberg, OlivetauPoivre olivetauPoivre, Context context, byte b2) {
        this(pavedAremberg, olivetauPoivre, context);
    }

    public final void a(Mimolette24mois mimolette24mois) {
        if (mimolette24mois instanceof Morbier) {
            a((Morbier) mimolette24mois);
        } else if (mimolette24mois instanceof Munster) {
            a((Munster) mimolette24mois);
        } else {
            if (mimolette24mois instanceof Mascare) {
                a((Mascare) mimolette24mois);
            }
        }
    }

    private final void a(Morbier morbier) {
        this.b.a(b(morbier)).a((gg<? super Throwable, ep>) new AbbayedeTamie<Object,ep>(ac.a)).a((gf<ep>) AbbayedeTimadeuc.a);
    }

    private static String b(Morbier morbier) {
        StringBuilder sb = new StringBuilder("{\"content\":[{\"type\":\"");
        sb.append(morbier.f());
        sb.append("\",\"timestamp_diff\":0}]}");
        return sb.toString();
    }

    private final void a(Munster munster) {
        this.b.b(b(munster)).a((gg<? super Throwable, ep>) new AbbayeduMontdesCats<Object,ep>(ac.a)).a((gf<ep>) AffideliceauChablis.a);
    }

    private final String a() {
        String str = this.d.getPackageManager().getPackageInfo(this.d.getPackageName(), 0).versionName;
        hl.a((Object) str, "context.packageManager.g…ckageName, 0).versionName");
        return str;
    }

    private final JSONObject b(Munster munster) {
        PontlEveque a2 = munster.a();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("event", munster.f());
        jSONObject.put(Param.CAMPAIGN, a2.g());
        jSONObject.put("advertiser", a2.f());
        jSONObject.put("advert", a2.b());
        jSONObject.put("ad_unit_id", a2.m().a());
        jSONObject.put("version_publisher_app", a());
        JSONObject a3 = this.c.a();
        a3.put("content", jSONObject);
        return a3;
    }

    private final void a(Mascare mascare) {
        this.b.c(b(mascare)).a((gg<? super Throwable, ep>) new CamembertdeNormandie<Object,ep>(ac.a)).a((gf<ep>) EcirdelAubrac.a);
    }

    private final JSONObject b(Mascare mascare) {
        PontlEveque a2 = mascare.a();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("campaign_id", a2.g());
        jSONObject.put("advert_id", a2.b());
        jSONObject.put("advertiser_id", a2.f());
        jSONObject.put("ad_unit_id", a2.m().a());
        jSONObject.put("url", mascare.b());
        jSONObject.put("source", mascare.c());
        if (mascare.d() != null) {
            jSONObject.put("tracker_pattern", mascare.d());
        }
        if (mascare.e() != null) {
            jSONObject.put("tracker_url", mascare.e());
        }
        JSONObject a3 = this.c.a();
        a3.put("content", jSONObject);
        return a3;
    }
}
