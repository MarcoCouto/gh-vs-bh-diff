package io.presage;

public final class AffideliceauChablis {
    public static final AffideliceauChablis a = new AffideliceauChablis();
    private static boolean b;
    private static boolean c;

    private AffideliceauChablis() {
    }

    public static void a(boolean z) {
        b = z;
    }

    public static boolean a() {
        return b;
    }

    public static void b(boolean z) {
        c = z;
    }

    public static boolean b() {
        return c;
    }

    public static boolean c() {
        return !b;
    }
}
