package io.presage;

import android.webkit.WebSettings;
import android.webkit.WebView;
import java.util.regex.Pattern;

public final class ch implements cl {
    private cg a;
    private final Pattern b = Pattern.compile(this.e.u());
    /* access modifiers changed from: private */
    public boolean c;
    private final db d;
    private final PontlEveque e;

    public static final class CamembertauCalvados extends cc {
        final /* synthetic */ ch a;

        CamembertauCalvados(ch chVar, Pattern pattern) {
            this.a = chVar;
            super(pattern);
        }

        public final void b() {
            this.a.d();
        }

        public final void b(WebView webView, String str) {
            this.a.c = true;
            this.a.d();
        }

        public final void c() {
            this.a.d();
        }
    }

    public ch(db dbVar, PontlEveque pontlEveque) {
        this.d = dbVar;
        this.e = pontlEveque;
        c();
    }

    private final void c() {
        db dbVar = this.d;
        Pattern pattern = this.b;
        hl.a((Object) pattern, "whitelistPattern");
        dbVar.setClientAdapter(new CamembertauCalvados(this, pattern));
    }

    /* access modifiers changed from: private */
    public final void d() {
        cg cgVar = this.a;
        if (cgVar != null) {
            cgVar.a();
        }
        e();
        an.e(this.d);
    }

    private final void e() {
        db dbVar = this.d;
        Pattern pattern = this.b;
        hl.a((Object) pattern, "whitelistPattern");
        dbVar.setClientAdapter(new cc(pattern));
    }

    public final void a(cg cgVar) {
        this.a = cgVar;
        if (this.e.t()) {
            WebSettings settings = this.d.getSettings();
            hl.a((Object) settings, "webView.settings");
            settings.setJavaScriptEnabled(false);
        }
        this.d.loadUrl(this.e.s());
    }

    public final void b() {
        this.a = null;
        e();
        an.e(this.d);
    }

    public final boolean a() {
        return this.c;
    }
}
