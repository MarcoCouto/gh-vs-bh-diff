package io.presage;

import android.graphics.Bitmap;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import java.io.ByteArrayInputStream;

public final class dd extends bf {
    private dc b;
    private final db c;

    public dd(db dbVar) {
        super(0);
        this.c = dbVar;
    }

    public final void a(dc dcVar) {
        this.b = dcVar;
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        dc dcVar = this.b;
        if (dcVar != null) {
            dcVar.a(webView, str);
        }
    }

    public final void onPageFinished(WebView webView, String str) {
        dc dcVar = this.b;
        if (dcVar != null) {
            dcVar.b(webView, str);
        }
    }

    public final void a(int i, String str, String str2) {
        StringBuilder sb = new StringBuilder("onReceivedError ");
        sb.append(i);
        sb.append(" description ");
        sb.append(str);
        sb.append(" ulr ");
        sb.append(str2);
        dc dcVar = this.b;
        if (dcVar != null) {
            dcVar.c();
        }
    }

    public final boolean b(WebView webView, String str) {
        dc dcVar = this.b;
        if (dcVar != null) {
            return dcVar.a();
        }
        return super.b(webView, str);
    }

    public final void a(String str) {
        this.c.a(str);
    }

    public final WebResourceResponse a(WebView webView, String str) {
        dc dcVar = this.b;
        if (dcVar != null) {
            dcVar.c(webView, str);
        }
        dc dcVar2 = this.b;
        if (dcVar2 == null || !dcVar2.a(str)) {
            return super.a(webView, str);
        }
        return b();
    }

    public final void a() {
        dc dcVar = this.b;
        if (dcVar != null) {
            dcVar.c();
        }
    }

    private static WebResourceResponse b() {
        byte[] bytes = "".getBytes(is.a);
        hl.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        return new WebResourceResponse("text/image", "UTF-8", new ByteArrayInputStream(bytes));
    }
}
