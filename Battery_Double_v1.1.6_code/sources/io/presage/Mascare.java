package io.presage;

public final class Mascare extends Mimolette24mois {
    private final PontlEveque a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    public Mascare(PontlEveque pontlEveque, String str, String str2, String str3, String str4) {
        super("ad_history", 0);
        this.a = pontlEveque;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    public final PontlEveque a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }
}
