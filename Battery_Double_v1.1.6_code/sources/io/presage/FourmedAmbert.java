package io.presage;

public final class FourmedAmbert {
    public static final FourmedAmbert a = new FourmedAmbert();

    private FourmedAmbert() {
    }

    public static boolean a() {
        return a("androidx.fragment.app.Fragment");
    }

    public static boolean b() {
        return a("android.support.v4.app.Fragment");
    }

    private static boolean a(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
