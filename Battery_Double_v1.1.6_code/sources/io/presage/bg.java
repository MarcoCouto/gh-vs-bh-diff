package io.presage;

import java.util.Locale;

public final class bg {
    public static final boolean a(String str) {
        Locale locale = Locale.US;
        hl.a((Object) locale, "Locale.US");
        String lowerCase = str.toLowerCase(locale);
        hl.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        return iv.b(lowerCase, "http://ogymraid") || iv.b(lowerCase, "https://ogymraid");
    }
}
