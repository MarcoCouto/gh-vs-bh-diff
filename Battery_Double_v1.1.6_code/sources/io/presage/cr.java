package io.presage;

import android.content.Context;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import io.presage.mraid.browser.ShortcutActivity;
import io.presage.mraid.browser.ShortcutActivity.CamembertauCalvados;
import java.util.Map;

public final class cr extends co {
    private gf<ep> b;
    private CamembertauCalvados c;
    private final Context d;
    private final ax e;
    private final ct f;
    private final aw g;
    private final bb h;

    public /* synthetic */ cr(Context context, ax axVar, ct ctVar) {
        this(context, axVar, ctVar, aw.a, bb.a);
    }

    private cr(Context context, ax axVar, ct ctVar, aw awVar, bb bbVar) {
        super(axVar);
        this.d = context;
        this.e = axVar;
        this.f = ctVar;
        this.g = awVar;
        this.h = bbVar;
        this.c = ShortcutActivity.a;
    }

    public final void a(gf<ep> gfVar) {
        this.b = gfVar;
    }

    public final void a(boolean z) {
        if (z) {
            this.f.n();
        } else {
            this.f.o();
        }
    }

    public final void a() {
        this.f.t();
    }

    public final void a(String str) {
        aw.a(this.d, str);
    }

    public final void a(String str, String str2) {
        if (aw.c(this.d, str)) {
            this.e.b(str2, "{isResolved: true}");
        } else {
            this.e.b(str2, "{isResolved: false}");
        }
    }

    public final void b(String str, String str2) {
        if (aw.b(this.d, str)) {
            this.e.b(str2, "{isStarted: true}");
            gf<ep> gfVar = this.b;
            if (gfVar != null) {
                gfVar.a();
            }
            return;
        }
        this.e.b(str2, "{isStarted: false}");
    }

    public final void a(Map<String, String> map, String str) {
        boolean z = false;
        if (str.length() > 0) {
            CharSequence charSequence = (CharSequence) map.get("name");
            if (!(charSequence == null || charSequence.length() == 0)) {
                CharSequence charSequence2 = (CharSequence) map.get(SettingsJsonConstants.APP_ICON_KEY);
                if (charSequence2 == null || charSequence2.length() == 0) {
                    z = true;
                }
                if (!z) {
                    CamembertauCalvados.a(this.d, new cs(String.valueOf(map.get("id")), String.valueOf(map.get("name")), String.valueOf(map.get(SettingsJsonConstants.APP_ICON_KEY)), str));
                }
            }
        }
    }

    public final void a(SaintFelicien saintFelicien) {
        bb.a((ba) new bd(saintFelicien.b(), saintFelicien.e()));
    }

    public final void b(String str) {
        this.f.a(str);
    }

    public final void a(cu cuVar) {
        this.f.a(cuVar);
    }

    public final void c() {
        this.f.s();
    }

    public final void d() {
        this.f.q();
    }

    public final void e() {
        this.f.u();
    }

    public final void b() {
        this.f.u();
    }
}
