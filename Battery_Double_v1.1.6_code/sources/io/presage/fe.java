package io.presage;

import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

class fe extends fd {
    private static final <T> boolean a(Iterable<? extends T> iterable, gg<? super T, Boolean> ggVar) {
        Iterator it = iterable.iterator();
        boolean z = false;
        while (it.hasNext()) {
            if (((Boolean) ggVar.a(it.next())).booleanValue()) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    public static final <T> boolean a(List<T> list, gg<? super T, Boolean> ggVar) {
        return b(list, ggVar);
    }

    private static final <T> boolean b(List<T> list, gg<? super T, Boolean> ggVar) {
        int i;
        if (list instanceof RandomAccess) {
            int a = ex.a(list);
            if (a >= 0) {
                int i2 = 0;
                i = 0;
                while (true) {
                    Object obj = list.get(i2);
                    if (!((Boolean) ggVar.a(obj)).booleanValue()) {
                        if (i != i2) {
                            list.set(i, obj);
                        }
                        i++;
                    }
                    if (i2 == a) {
                        break;
                    }
                    i2++;
                }
            } else {
                i = 0;
            }
            if (i >= list.size()) {
                return false;
            }
            int a2 = ex.a(list);
            if (a2 >= i) {
                while (true) {
                    list.remove(a2);
                    if (a2 == i) {
                        break;
                    }
                    a2--;
                }
            }
            return true;
        } else if (list != null) {
            return a(hq.a((Object) list), ggVar);
        } else {
            throw new em("null cannot be cast to non-null type kotlin.collections.MutableIterable<T>");
        }
    }
}
