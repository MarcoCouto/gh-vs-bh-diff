package io.presage;

import android.widget.FrameLayout;
import io.presage.bj.CamembertauCalvados;
import io.presage.mraid.browser.ShortcutActivity;

public final class bm {
    private bj a;
    private final bp b;
    private final ShortcutActivity c;
    private final cq d;
    private final br e;
    private final CamembertauCalvados f;

    private bm(bp bpVar, ShortcutActivity shortcutActivity, cq cqVar, br brVar, CamembertauCalvados camembertauCalvados) {
        this.b = bpVar;
        this.c = shortcutActivity;
        this.d = cqVar;
        this.e = brVar;
        this.f = camembertauCalvados;
    }

    public /* synthetic */ bm(bp bpVar, ShortcutActivity shortcutActivity, cq cqVar) {
        this(bpVar, shortcutActivity, cqVar, br.a, bj.a);
    }

    public final boolean a(String str, String str2, FrameLayout frameLayout) {
        String b2 = this.b.b(str2);
        if (b2.length() > 0) {
            str = b2;
        }
        if (str.length() == 0) {
            return false;
        }
        bq a2 = br.a(str);
        if (a2 == null) {
            return false;
        }
        if (!this.b.a(a2.c()) && !this.b.c(a2.c())) {
            return false;
        }
        a(frameLayout, a2);
        return true;
    }

    private final void a(FrameLayout frameLayout, bq bqVar) {
        PontlEveque pontlEveque = new PontlEveque();
        pontlEveque.h("http://ogury.io");
        this.a = CamembertauCalvados.a(this.c, pontlEveque, frameLayout, this.d);
        bj bjVar = this.a;
        if (bjVar != null) {
            bjVar.a(bqVar);
        }
    }

    public final void a() {
        bj bjVar = this.a;
        if (bjVar != null) {
            bjVar.d();
        }
    }
}
