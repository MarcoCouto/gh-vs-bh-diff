package io.presage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import io.presage.cx.CamembertauCalvados;

@SuppressLint({"ViewConstructor"})
public final class db extends WebView {
    private boolean a;
    private String b;
    private ax c;
    private boolean d;
    private be e;
    private dd f;
    private boolean g;
    private boolean h;
    private dc i;
    private by j;
    private aj k;
    private cx l;
    private final iu m;
    private PontlEveque n;
    private MutableContextWrapper o;

    public /* synthetic */ db(Context context, PontlEveque pontlEveque) {
        this(context, pontlEveque, new MutableContextWrapper(context));
    }

    private db(Context context, PontlEveque pontlEveque, MutableContextWrapper mutableContextWrapper) {
        super(mutableContextWrapper);
        this.n = pontlEveque;
        this.o = mutableContextWrapper;
        this.a = true;
        this.b = "loading";
        this.c = new ax(this);
        this.e = new cn(this);
        this.f = new dd(this);
        this.h = true;
        this.j = by.a;
        this.k = aj.a;
        CamembertauCalvados camembertauCalvados = cx.a;
        this.l = CamembertauCalvados.a(context, this.n);
        this.m = new iu("bunaZiua");
        setAdUnit(this.n.m());
        setWebViewClient(this.f);
    }

    public final boolean getShowSdkCloseButton() {
        return this.a;
    }

    public final void setShowSdkCloseButton(boolean z) {
        this.a = z;
    }

    public final String getAdState() {
        return this.b;
    }

    public final void setAdState(String str) {
        this.b = str;
    }

    public final ax getMraidCommandExecutor() {
        ax axVar = this.c;
        return axVar == null ? new ax(this) : axVar;
    }

    public final boolean getContainsMraid() {
        return this.d;
    }

    public final void setContainsMraid(boolean z) {
        this.d = z;
    }

    public final be getMraidUrlHandler() {
        return this.e;
    }

    public final void setMraidUrlHandler(be beVar) {
        this.e = beVar;
    }

    public final boolean a() {
        return this.g;
    }

    public final void setResumed(boolean z) {
        this.g = z;
    }

    public final boolean b() {
        return this.h;
    }

    public final void setMultiBrowserNotOpened(boolean z) {
        this.h = z;
    }

    public final dc getClientAdapter() {
        return this.i;
    }

    public final void setClientAdapter(dc dcVar) {
        this.i = dcVar;
        dd ddVar = this.f;
        if (ddVar != null) {
            ddVar.a(dcVar);
        }
    }

    public final void setTestTopActivityMonitor(aj ajVar) {
        this.k = ajVar;
    }

    public final void setTestMraidLifecycle(cx cxVar) {
        this.l = cxVar;
    }

    public final void setTestCacheStore(by byVar) {
        this.j = byVar;
    }

    public final void setTestMraidViewClientWrapper(dd ddVar) {
        this.f = ddVar;
    }

    public final dd getMraidWebViewClient() {
        return this.f;
    }

    private final void setAdUnit(SaintFelicien saintFelicien) {
        dd ddVar = this.f;
        if (ddVar != null) {
            ddVar.a(saintFelicien);
        }
    }

    public final void c() {
        this.l.a(this);
    }

    private final void i() {
        this.l.b(this);
    }

    public final void d() {
        this.l.c(this);
    }

    public final void e() {
        this.l.d(this);
    }

    public final void a(String str) {
        if (this.m.a(str)) {
            this.d = true;
            i();
            dc dcVar = this.i;
            if (dcVar != null) {
                dcVar.a((WebView) this);
            }
        }
        this.e.a(str, this, this.n.m());
    }

    public final void f() {
        getMraidCommandExecutor().b();
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        if (webViewClient != null && (!hl.a((Object) this.f, (Object) webViewClient))) {
            new IllegalAccessError("Cannot change the webview client for MraidWebView");
        }
        super.setWebViewClient(webViewClient);
    }

    public final void g() {
        by.a(this.n.b());
        dc dcVar = this.i;
        if (dcVar != null) {
            dcVar.b();
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("MraidWebView>> ");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        return sb.toString();
    }

    public final void h() {
        setClientAdapter(null);
        cp.CamembertauCalvados camembertauCalvados = cp.a;
        this.e = cp.CamembertauCalvados.a();
        this.c = null;
        setWebViewClient(null);
        this.f = null;
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        Activity a2 = aj.a();
        if (a2 != null) {
            this.o.setBaseContext(a2);
        }
    }
}
