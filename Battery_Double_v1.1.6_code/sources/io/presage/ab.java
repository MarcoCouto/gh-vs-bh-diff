package io.presage;

import org.json.JSONObject;

public final class ab {
    public static final int a(JSONObject jSONObject, String str, int i) {
        return jSONObject != null ? jSONObject.optInt(str, i) : i;
    }

    public static final String a(JSONObject jSONObject, String str, String str2) {
        if (jSONObject != null) {
            String optString = jSONObject.optString(str, str2);
            if (optString != null) {
                return optString;
            }
        }
        return str2;
    }

    public static final boolean a(JSONObject jSONObject, String str, boolean z) {
        return jSONObject != null ? jSONObject.optBoolean(str, z) : z;
    }

    public static final long a(JSONObject jSONObject, String str, long j) {
        return jSONObject != null ? jSONObject.optLong(str, j) : j;
    }

    public static final boolean a(JSONObject jSONObject) {
        return jSONObject.length() == 0;
    }
}
