package io.presage;

public final class TrouduCru {
    private final String a;
    private final boolean b;

    public TrouduCru(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public final String a() {
        return this.a;
    }

    public final boolean b() {
        return this.b;
    }
}
