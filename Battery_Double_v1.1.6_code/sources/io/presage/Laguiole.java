package io.presage;

import com.ogury.crashreport.CrashReport;

public final class Laguiole {
    public static final Laguiole a = new Laguiole();

    private Laguiole() {
    }

    public static void a(Throwable th) {
        try {
            CrashReport.logException(th);
        } catch (Throwable unused) {
        }
    }
}
