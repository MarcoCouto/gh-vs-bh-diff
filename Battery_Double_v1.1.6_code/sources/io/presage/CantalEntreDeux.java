package io.presage;

import android.content.Context;
import io.presage.common.AdConfig;
import io.presage.common.network.models.RewardItem;

public final class CantalEntreDeux {
    private AbbayedeTimadeuc a;
    private EcirdelAubrac b;
    private String c;
    private gg<? super RewardItem, ep> d;
    private final Context e;
    private final AdConfig f;
    private final SableduBoulonnais g;
    private final AbbayeduMontdesCats h;

    private CantalEntreDeux(Context context, AdConfig adConfig, SableduBoulonnais sableduBoulonnais, AbbayeduMontdesCats abbayeduMontdesCats) {
        this.e = context;
        this.f = adConfig;
        this.g = sableduBoulonnais;
        this.h = abbayeduMontdesCats;
        this.c = "";
    }

    public /* synthetic */ CantalEntreDeux(Context context, AdConfig adConfig, SableduBoulonnais sableduBoulonnais) {
        this(context, adConfig, sableduBoulonnais, new AbbayeduMontdesCats(context, adConfig, sableduBoulonnais));
    }

    public final void a(gg<? super RewardItem, ep> ggVar) {
        this.d = ggVar;
    }

    public final void a() {
        AbbayedeTimadeuc abbayedeTimadeuc = this.a;
        if (abbayedeTimadeuc != null && abbayedeTimadeuc.c()) {
            AbbayedeTimadeuc abbayedeTimadeuc2 = this.a;
            if (abbayedeTimadeuc2 != null) {
                abbayedeTimadeuc2.e();
            }
        }
        this.a = c();
        AbbayedeTimadeuc abbayedeTimadeuc3 = this.a;
        if (abbayedeTimadeuc3 != null) {
            abbayedeTimadeuc3.d();
        }
    }

    private final AbbayedeTimadeuc c() {
        AbbayedeTimadeuc a2 = this.h.a();
        a2.a(this.b);
        a2.a(this.d);
        a2.a(this.c);
        return a2;
    }

    public final void a(EcirdelAubrac ecirdelAubrac) {
        this.b = ecirdelAubrac;
        AbbayedeTimadeuc abbayedeTimadeuc = this.a;
        if (abbayedeTimadeuc != null) {
            abbayedeTimadeuc.a(ecirdelAubrac);
        }
    }

    public final void a(Appenzeller appenzeller) {
        if (this.a == null) {
            EcirdelAubrac ecirdelAubrac = this.b;
            if (ecirdelAubrac != null) {
                ecirdelAubrac.d();
            }
        }
        AbbayedeTimadeuc abbayedeTimadeuc = this.a;
        if (abbayedeTimadeuc != null) {
            abbayedeTimadeuc.a(appenzeller);
        }
    }

    public final boolean b() {
        AbbayedeTimadeuc abbayedeTimadeuc = this.a;
        if (abbayedeTimadeuc != null) {
            return abbayedeTimadeuc.a();
        }
        return false;
    }

    public final void a(String str) {
        this.c = str;
    }
}
