package io.presage;

import android.content.Context;
import android.webkit.WebView;
import java.util.List;

public final class bz {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final Context b;
    private final by c;
    private cm d;
    private final cd e;
    private final k f;
    private long g;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static bz a(Context context) {
            return new bz(context, by.a);
        }
    }

    private bz(Context context, by byVar, cm cmVar, cd cdVar, k kVar) {
        this.c = byVar;
        this.d = cmVar;
        this.e = cdVar;
        this.f = kVar;
        this.g = 80000;
        this.b = context.getApplicationContext();
    }

    public /* synthetic */ bz(Context context, by byVar) {
        this(context, byVar, new cm(), new cd(), k.a);
    }

    public final void a(cb cbVar, List<PontlEveque> list, ca caVar) {
        a(caVar, cbVar);
        for (PontlEveque pontlEveque : list) {
            Context context = this.b;
            hl.a((Object) context, "context");
            db a2 = de.a(context, pontlEveque);
            if (a2 != null) {
                WebView webView = a2;
                this.e.a(webView);
                an.a(webView);
                this.d.a((cl) new cf(a2, pontlEveque, this.c, cbVar));
                if (pontlEveque.s().length() > 0) {
                    a(pontlEveque);
                }
            } else {
                caVar.b();
                return;
            }
        }
        this.d.a(this.e, this.g, list.size());
    }

    private final void a(ca caVar, cb cbVar) {
        this.d.a(caVar);
        this.d.b();
        by.a(cbVar);
        a();
    }

    private final void a() {
        Context context = this.b;
        hl.a((Object) context, "context");
        j a2 = k.a(context);
        if (a2 != null) {
            this.g = a2.k();
        }
    }

    private final void a(PontlEveque pontlEveque) {
        db b2 = b(pontlEveque);
        if (b2 != null) {
            bi.a(b2);
            this.d.a((cl) new ch(b2, pontlEveque));
        }
    }

    private final db b(PontlEveque pontlEveque) {
        Context context = this.b;
        hl.a((Object) context, "context");
        db a2 = de.a(context, pontlEveque);
        if (a2 != null) {
            an.a(a2);
        }
        return a2;
    }

    public final void a(cb cbVar) {
        this.d.a((ca) null);
        this.d.b();
        by.a(cbVar);
    }
}
