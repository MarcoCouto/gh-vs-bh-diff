package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public final class TommedAuvergne {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static TommedAuvergne m;
    private boolean b;
    private long c;
    private c d;
    private final Context e;
    private final h f;
    private final TommedeSavoie g;
    private final TommeMarcdeRaisin h;
    private final PavedAremberg i;
    private final q j;
    private final y k;
    private final OlivetalaSauge l;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public final TommedAuvergne a(Context context) {
            TommedAuvergne a;
            ai.a("Profig.getInstance");
            synchronized (this) {
                if (TommedAuvergne.m == null) {
                    CamembertauCalvados camembertauCalvados = TommedAuvergne.a;
                    Context applicationContext = context.getApplicationContext();
                    hl.a((Object) applicationContext, "context.applicationContext");
                    TommedAuvergne.m = b(applicationContext);
                }
                a = TommedAuvergne.m;
                if (a == null) {
                    hl.a();
                }
            }
            return a;
        }

        private static TommedAuvergne b(Context context) {
            io.presage.h.CamembertauCalvados camembertauCalvados = h.a;
            h a = io.presage.h.CamembertauCalvados.a(context);
            TommedeSavoie tommedeSavoie = new TommedeSavoie(new Taleggio(context), new TetedeMoine(context));
            PersilledumontBlanc persilledumontBlanc = PersilledumontBlanc.a;
            Context context2 = context;
            TommedAuvergne tommedAuvergne = new TommedAuvergne(context2, a, tommedeSavoie, TommeMarcdeRaisin.a, PersilledumontBlanc.a(context), s.a, new z(), new OlivetalaSauge(), 0);
            return tommedAuvergne;
        }
    }

    private TommedAuvergne(Context context, h hVar, TommedeSavoie tommedeSavoie, TommeMarcdeRaisin tommeMarcdeRaisin, PavedAremberg pavedAremberg, q qVar, y yVar, OlivetalaSauge olivetalaSauge) {
        this.e = context;
        this.f = hVar;
        this.g = tommedeSavoie;
        this.h = tommeMarcdeRaisin;
        this.i = pavedAremberg;
        this.j = qVar;
        this.k = yVar;
        this.l = olivetalaSauge;
        this.c = TimeUnit.SECONDS.toMillis(7200);
        this.d = TommeMarcdeRaisin.a(this.e);
    }

    public /* synthetic */ TommedAuvergne(Context context, h hVar, TommedeSavoie tommedeSavoie, TommeMarcdeRaisin tommeMarcdeRaisin, PavedAremberg pavedAremberg, q qVar, y yVar, OlivetalaSauge olivetalaSauge, byte b2) {
        this(context, hVar, tommedeSavoie, tommeMarcdeRaisin, pavedAremberg, qVar, yVar, olivetalaSauge);
    }

    private final void b() {
        if (c()) {
            this.f.a(0);
            this.f.e();
        }
    }

    private final boolean c() {
        return this.f.f() != System.currentTimeMillis() / TimeUnit.DAYS.toMillis(1);
    }

    private final void d() {
        this.f.a(this.f.a() + 1);
    }

    private final TommeduJura a(boolean z) {
        this.d = TommeMarcdeRaisin.a(this.e);
        return new TommeduJura(this.g, this.d, this.f, z);
    }

    /* access modifiers changed from: private */
    public void b(boolean z) throws Salers {
        if (!this.b || z) {
            this.b = true;
            b();
            TommeduJura a2 = a(z);
            TommedeYenne b2 = a2.b();
            StringBuilder sb = new StringBuilder("Profig - profigRequest ");
            sb.append(z);
            sb.append(' ');
            sb.append(b2);
            j a3 = a2.a();
            if (a3 != null) {
                this.c = a3.e();
            }
            if (!this.k.a(this.e)) {
                this.j.a(this.e, this.c);
                this.b = false;
                return;
            }
            if (b2.d()) {
                this.j.a(this.e, b2.b());
            } else {
                this.j.a(this.e);
            }
            if (b2.a()) {
                a(b2);
            }
            this.b = false;
        }
    }

    private final void a(TommedeYenne tommedeYenne) {
        new StringBuilder("making profig api call ").append(tommedeYenne);
        try {
            dr a2 = this.i.a(tommedeYenne.c());
            if (a2 instanceof ds) {
                JSONObject jSONObject = new JSONObject(((ds) a2).a());
                OlivetalaSauge.a(jSONObject);
                a(tommedeYenne, jSONObject);
                return;
            }
            if (a2 instanceof di) {
                a(((di) a2).a());
            }
        } catch (Exception e2) {
            a((Throwable) e2);
        }
    }

    private final void a(Throwable th) {
        if (ai.a(th)) {
            d();
            e();
        }
        if (th instanceof Salers) {
            throw th;
        }
    }

    private final void a(TommedeYenne tommedeYenne, JSONObject jSONObject) {
        d();
        e();
        a(tommedeYenne.e());
        this.f.b(this.d.a());
        this.f.a(System.currentTimeMillis());
        l lVar = l.a;
        a(tommedeYenne, l.a(jSONObject), jSONObject);
    }

    private final void a(String str) {
        if (str != null) {
            this.f.a(str);
        }
    }

    private final void e() {
        this.f.d(al.a());
    }

    private final void a(TommedeYenne tommedeYenne, n nVar, JSONObject jSONObject) {
        if (nVar instanceof j) {
            k kVar = k.a;
            j jVar = (j) nVar;
            k.a(jVar);
            h hVar = this.f;
            String jSONObject2 = jSONObject.toString();
            hl.a((Object) jSONObject2, "profigJsonResponse.toString()");
            hVar.c(jSONObject2);
            a(jVar.a(), jVar.g());
        } else if (nVar instanceof d) {
            b(true);
        } else {
            if (nVar instanceof i) {
                a(tommedeYenne.d(), tommedeYenne.b());
            }
        }
    }

    private final void a(boolean z, long j2) {
        if (z) {
            this.j.a(this.e, j2);
        } else {
            this.j.a(this.e);
        }
    }
}
