package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.ironsource.sdk.precache.DownloadManager;
import java.io.File;

public final class an {

    static final class CamembertauCalvados implements Runnable {
        final /* synthetic */ gf a;

        CamembertauCalvados(gf gfVar) {
            this.a = gfVar;
        }

        public final void run() {
            this.a.a();
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public static final void a(WebView webView) {
        webView.getSettings().setAppCacheEnabled(true);
        Context context = webView.getContext();
        hl.a((Object) context, "context");
        File cacheDir = context.getCacheDir();
        String absolutePath = cacheDir != null ? cacheDir.getAbsolutePath() : null;
        if (absolutePath != null) {
            webView.getSettings().setAppCachePath(absolutePath);
        }
        WebSettings settings = webView.getSettings();
        hl.a((Object) settings, DownloadManager.SETTINGS);
        settings.setJavaScriptEnabled(true);
        WebSettings settings2 = webView.getSettings();
        hl.a((Object) settings2, DownloadManager.SETTINGS);
        settings2.setDomStorageEnabled(true);
    }

    public static final void b(WebView webView) {
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setScrollContainer(false);
    }

    public static final void c(WebView webView) {
        if (VERSION.SDK_INT >= 17) {
            WebSettings settings = webView.getSettings();
            hl.a((Object) settings, DownloadManager.SETTINGS);
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
    }

    public static final void a(WebView webView, gf<ep> gfVar) {
        if (webView.getHeight() > 0 || webView.getWidth() > 0) {
            gfVar.a();
        } else {
            webView.post(new CamembertauCalvados(gfVar));
        }
    }

    public static final boolean d(WebView webView) {
        if (VERSION.SDK_INT >= 19) {
            return webView.isAttachedToWindow();
        }
        return webView.getParent() != null;
    }

    public static final void e(WebView webView) {
        if (!d(webView)) {
            webView.destroy();
        }
    }
}
