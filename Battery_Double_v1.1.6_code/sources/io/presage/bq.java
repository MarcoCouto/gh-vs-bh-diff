package io.presage;

public final class bq {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private String b = "";
    private String c = "";
    private String d = "";
    private int e;
    private int f;
    private int g;
    private int h;
    private boolean i;
    private boolean j;
    private boolean k;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public final String a() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String b() {
        return this.c;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.d;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final void a(int i2) {
        this.e = i2;
    }

    public final int d() {
        return this.e;
    }

    public final void b(int i2) {
        this.f = i2;
    }

    public final int e() {
        return this.f;
    }

    public final void c(int i2) {
        this.g = i2;
    }

    public final int f() {
        return this.g;
    }

    public final void d(int i2) {
        this.h = i2;
    }

    public final int g() {
        return this.h;
    }

    public final void a(boolean z) {
        this.i = z;
    }

    public final boolean h() {
        return this.i;
    }

    public final void b(boolean z) {
        this.j = z;
    }

    public final boolean i() {
        return this.j;
    }

    public final void c(boolean z) {
        this.k = z;
    }

    public final boolean j() {
        return this.k;
    }
}
