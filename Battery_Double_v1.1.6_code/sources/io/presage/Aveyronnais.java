package io.presage;

import android.content.Context;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;

public final class Aveyronnais {
    private boolean a;
    private final PavedAremberg b;
    private final bb c;
    private final MoelleuxduRevard d;

    private Aveyronnais(PavedAremberg pavedAremberg, bb bbVar, MoelleuxduRevard moelleuxduRevard) {
        this.b = pavedAremberg;
        this.c = bbVar;
        this.d = moelleuxduRevard;
        this.a = true;
    }

    public Aveyronnais(Context context) {
        PersilledumontBlanc persilledumontBlanc = PersilledumontBlanc.a;
        this(PersilledumontBlanc.a(context), bb.a, MoelleuxduRevard.a);
    }

    public final void a(PontlEveque pontlEveque) {
        if (this.a && pontlEveque != null) {
            this.a = false;
            b(pontlEveque);
        }
    }

    private final void b(PontlEveque pontlEveque) {
        if (pontlEveque.e().length() > 0) {
            this.b.b(pontlEveque.e());
        } else {
            MoelleuxduRevard.a((Mimolette24mois) new Munster("shown", pontlEveque));
        }
        bb.a(new ba(pontlEveque.b(), Events.AD_IMPRESSION));
    }
}
