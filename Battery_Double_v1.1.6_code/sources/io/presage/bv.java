package io.presage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import io.presage.mraid.browser.listeners.OrientationListener$1;

public final class bv {
    /* access modifiers changed from: private */
    public int a;
    private final BroadcastReceiver b = new OrientationListener$1(this);
    private final Context c;
    private final bk d;

    public bv(Context context, bk bkVar) {
        this.c = context;
        this.d = bkVar;
        Resources resources = this.c.getResources();
        hl.a((Object) resources, "context.resources");
        this.a = resources.getConfiguration().orientation;
        b();
    }

    private final void b() {
        this.c.registerReceiver(this.b, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
    }

    /* access modifiers changed from: private */
    public final void c() {
        this.d.c();
    }

    public final void a() {
        try {
            this.c.unregisterReceiver(this.b);
        } catch (Exception unused) {
        }
    }
}
