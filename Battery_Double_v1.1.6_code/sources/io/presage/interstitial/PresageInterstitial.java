package io.presage.interstitial;

import android.content.Context;
import io.presage.Appenzeller;
import io.presage.CantalEntreDeux;
import io.presage.EcirdelAubrac;
import io.presage.SableduBoulonnais;
import io.presage.ao;
import io.presage.ar;
import io.presage.common.AdConfig;

public final class PresageInterstitial {
    private final CantalEntreDeux a;

    private PresageInterstitial(CantalEntreDeux cantalEntreDeux) {
        this.a = cantalEntreDeux;
    }

    public PresageInterstitial(Context context) {
        this(context, null);
    }

    public PresageInterstitial(Context context, AdConfig adConfig) {
        this(new CantalEntreDeux(context, adConfig, SableduBoulonnais.INTERSTITIAL));
    }

    public final boolean isLoaded() {
        return this.a.b();
    }

    public final void setInterstitialCallback(PresageInterstitialCallback presageInterstitialCallback) {
        this.a.a((EcirdelAubrac) new ao(presageInterstitialCallback));
    }

    public final void load() {
        this.a.a();
    }

    public final void show() {
        this.a.a((Appenzeller) ar.a);
    }
}
