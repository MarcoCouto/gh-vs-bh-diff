package io.presage.interstitial.optinvideo;

import android.content.Context;
import io.presage.Appenzeller;
import io.presage.CantalEntreDeux;
import io.presage.EcirdelAubrac;
import io.presage.SableduBoulonnais;
import io.presage.ao;
import io.presage.ar;
import io.presage.common.AdConfig;
import io.presage.common.network.models.RewardItem;
import io.presage.ep;
import io.presage.gg;
import io.presage.hm;

public final class PresageOptinVideo {
    private final CantalEntreDeux a;

    static final class CamembertauCalvados extends hm implements gg<RewardItem, ep> {
        final /* synthetic */ PresageOptinVideoCallback a;

        CamembertauCalvados(PresageOptinVideoCallback presageOptinVideoCallback) {
            this.a = presageOptinVideoCallback;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((RewardItem) obj);
            return ep.a;
        }

        private void a(RewardItem rewardItem) {
            this.a.onAdRewarded(rewardItem);
        }
    }

    private PresageOptinVideo(CantalEntreDeux cantalEntreDeux) {
        this.a = cantalEntreDeux;
    }

    public PresageOptinVideo(Context context, AdConfig adConfig) {
        this(new CantalEntreDeux(context, adConfig, SableduBoulonnais.OPTIN_VIDEO));
    }

    public final boolean isLoaded() {
        return this.a.b();
    }

    public final void setOptinVideoCallback(PresageOptinVideoCallback presageOptinVideoCallback) {
        this.a.a((EcirdelAubrac) new ao(presageOptinVideoCallback));
        this.a.a((gg<? super RewardItem, ep>) new CamembertauCalvados<Object,ep>(presageOptinVideoCallback));
    }

    public final void load() {
        this.a.a();
    }

    public final void show() {
        this.a.a((Appenzeller) ar.a);
    }

    public final void setUserId(String str) {
        this.a.a(str);
    }
}
