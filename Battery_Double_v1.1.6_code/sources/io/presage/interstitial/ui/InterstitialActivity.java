package io.presage.interstitial.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import com.integralads.avid.library.inmobi.session.internal.InternalAvidAdSessionContext;
import io.presage.AbbayedeCiteauxentiere;
import io.presage.AffideliceauChablis;
import io.presage.BleudeGex;
import io.presage.CamembertdeNormandie;
import io.presage.Laguiole;
import io.presage.Livarot;
import io.presage.PontlEveque;
import io.presage.SaintNectaire;
import io.presage.aq;
import io.presage.ba;
import io.presage.bb;
import io.presage.em;
import io.presage.hl;
import io.presage.hq;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InterstitialActivity extends Activity implements Livarot {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private AbbayedeCiteauxentiere b;
    private Laguiole c = Laguiole.a;
    private boolean d;

    public static final class CamembertauCalvados implements BleudeGex {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public final void a(Context context, String str, PontlEveque pontlEveque, List<PontlEveque> list) {
            Intent a = a(pontlEveque, list, context);
            a.putExtra(InternalAvidAdSessionContext.CONTEXT_MODE, 1);
            a.putExtra("expand_cache_item_id", str);
            context.startActivity(a);
        }

        public final void a(Context context, PontlEveque pontlEveque, List<PontlEveque> list) {
            context.startActivity(a(pontlEveque, list, context));
        }

        private static Intent a(PontlEveque pontlEveque, List<PontlEveque> list, Context context) {
            Intent intent = new Intent(context, a(pontlEveque));
            intent.putExtra("ad", pontlEveque);
            intent.putExtra("not_displayed_ads", new ArrayList(list));
            intent.addFlags(268435456);
            return intent;
        }

        private static Class<?> a(PontlEveque pontlEveque) {
            if (a()) {
                return InterstitialActivity.class;
            }
            if (pontlEveque.n()) {
                return InterstitialAndroid8TransparentActivity.class;
            }
            return InterstitialAndroid8RotableActivity.class;
        }

        private static boolean a() {
            return VERSION.SDK_INT != 26;
        }
    }

    private static boolean a(Bundle bundle) {
        return bundle != null;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (a(bundle)) {
            finish();
            return;
        }
        c();
        try {
            List b2 = b();
            PontlEveque a2 = a();
            if (a2 != null) {
                this.d = SaintNectaire.a(a2);
                Intent intent = getIntent();
                hl.a((Object) intent, "intent");
                aq aqVar = new aq(this, intent, a2, b2);
                CamembertdeNormandie a3 = aqVar.a();
                a3.setDisplayedInFullScreen(true);
                this.b = aqVar.b();
                setContentView(a3);
                return;
            }
            throw new IllegalStateException("Ad not sent to interstitial activity");
        } catch (Throwable th) {
            Laguiole.a(th);
            finish();
        }
    }

    private final PontlEveque a() {
        Serializable serializableExtra = getIntent().getSerializableExtra("ad");
        if (!(serializableExtra instanceof PontlEveque)) {
            serializableExtra = null;
        }
        return (PontlEveque) serializableExtra;
    }

    private final List<PontlEveque> b() {
        Serializable serializableExtra = getIntent().getSerializableExtra("not_displayed_ads");
        if (serializableExtra != null) {
            return hq.b(serializableExtra);
        }
        throw new em("null cannot be cast to non-null type kotlin.collections.MutableList<io.presage.common.network.models.Ad>");
    }

    private final void c() {
        getWindow().setFlags(16777216, 16777216);
    }

    public void a(PontlEveque pontlEveque) {
        String str = null;
        if (hl.a(pontlEveque != null ? pontlEveque.d() : null, (Object) "landscape")) {
            setRequestedOrientation(0);
            return;
        }
        if (pontlEveque != null) {
            str = pontlEveque.d();
        }
        if (hl.a((Object) str, (Object) "portrait")) {
            setRequestedOrientation(1);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.d) {
            AffideliceauChablis affideliceauChablis = AffideliceauChablis.a;
            AffideliceauChablis.b(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.d) {
            AffideliceauChablis affideliceauChablis = AffideliceauChablis.a;
            AffideliceauChablis.b(false);
        }
        overridePendingTransition(0, 0);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = this.b;
        if (abbayedeCiteauxentiere != null) {
            abbayedeCiteauxentiere.i();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.b != null) {
            AbbayedeCiteauxentiere abbayedeCiteauxentiere = this.b;
            if (abbayedeCiteauxentiere != null) {
                abbayedeCiteauxentiere.j();
            }
            return;
        }
        d();
    }

    public void onBackPressed() {
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = this.b;
        if (abbayedeCiteauxentiere != null ? abbayedeCiteauxentiere.k() : true) {
            super.onBackPressed();
        }
    }

    private final void d() {
        PontlEveque a2 = a();
        if (a2 != null) {
            bb bbVar = bb.a;
            bb.a(new ba(a2.b(), "adClosed"));
            bb bbVar2 = bb.a;
            bb.a(a2.b());
        }
    }
}
