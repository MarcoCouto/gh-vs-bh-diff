package io.presage;

import android.app.Application;

public final class Chambertin {
    private final BleudeGex a;

    public Chambertin(BleudeGex bleudeGex) {
        this.a = bleudeGex;
    }

    public final CendreduBeauzac a(Application application, CremeuxduJura cremeuxduJura, EtivazGruyereSuisse etivazGruyereSuisse) {
        return new CendreduBeauzac(application, new CoeurdeNeufchatel(cremeuxduJura, etivazGruyereSuisse), this.a);
    }
}
