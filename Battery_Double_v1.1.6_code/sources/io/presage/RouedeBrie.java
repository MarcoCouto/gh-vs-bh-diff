package io.presage;

import java.util.List;

public final class RouedeBrie {
    private final List<PontlEveque> a;

    public RouedeBrie(List<PontlEveque> list) {
        this.a = list;
    }

    public final List<PontlEveque> a() {
        return this.a;
    }
}
