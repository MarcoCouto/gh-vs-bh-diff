package io.presage;

import android.content.SharedPreferences;

public final class ah {
    public static final String a(SharedPreferences sharedPreferences, String str, String str2) {
        String string = sharedPreferences.getString(str, str2);
        return string == null ? str2 : string;
    }
}
