package io.presage;

public final class RomansPartDieu implements RegaldeBourgogne {
    private final j a;
    private final TetedeMoine b;
    private final Taleggio c;
    private final SaintPaulin d;

    public final String i() {
        return "3.3.9-moat";
    }

    public RomansPartDieu(TetedeMoine tetedeMoine, Taleggio taleggio, SaintPaulin saintPaulin) {
        this.b = tetedeMoine;
        this.c = taleggio;
        this.d = saintPaulin;
        j a2 = k.a(this.b.g());
        if (a2 != null) {
            this.a = a2;
            return;
        }
        throw new IllegalStateException("Profig must not be null");
    }

    public final int g() {
        return this.c.j();
    }

    public final int h() {
        return this.c.k();
    }

    public final String a() {
        return this.c.i();
    }

    public final String b() {
        return this.c.f();
    }

    public final String c() {
        return this.b.c();
    }

    public final String d() {
        return this.b.a();
    }

    public final boolean e() {
        return this.a.c() && SaintPaulin.a();
    }

    public final boolean f() {
        return this.a.d() && SaintPaulin.b();
    }

    public final float j() {
        return this.c.n();
    }
}
