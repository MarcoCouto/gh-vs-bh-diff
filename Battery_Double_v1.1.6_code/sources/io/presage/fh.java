package io.presage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

class fh extends fg {
    public static final <T> T b(List<? extends T> list) {
        if (!list.isEmpty()) {
            return list.get(ex.a(list));
        }
        throw new NoSuchElementException("List is empty.");
    }

    public static final <T> List<T> a(Collection<? extends T> collection) {
        return new ArrayList<>(collection);
    }
}
