package io.presage;

import android.content.Context;
import io.presage.ParmigianoReggiano.CamembertauCalvados;

public final class PersilledumontBlanc {
    public static final PersilledumontBlanc a = new PersilledumontBlanc();
    private static PavedAremberg b;

    private PersilledumontBlanc() {
    }

    public static PavedAremberg a(Context context) {
        if (b == null) {
            Context applicationContext = context.getApplicationContext();
            CamembertauCalvados camembertauCalvados = ParmigianoReggiano.a;
            hl.a((Object) applicationContext, "appContext");
            b = new PetitMorin(CamembertauCalvados.a(applicationContext), new TetedeMoine(applicationContext), new Taleggio(applicationContext), new Cdo(new OlivetCendre(k.a, applicationContext, m.a(3))));
        }
        PavedAremberg pavedAremberg = b;
        if (pavedAremberg == null) {
            hl.a();
        }
        return pavedAremberg;
    }
}
