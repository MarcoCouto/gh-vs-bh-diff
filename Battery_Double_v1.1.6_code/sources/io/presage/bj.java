package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

public final class bj {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public bl b;
    private final Pattern c;
    private final PontlEveque d;
    private final Map<String, db> e;
    /* access modifiers changed from: private */
    public final Map<String, bs> f;
    private final bh g;
    private final bk h;
    private final bv i;
    private final MoelleuxduRevard j;
    private final cq k;
    private final bt l;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static bj a(Context context, PontlEveque pontlEveque, FrameLayout frameLayout, cq cqVar) {
            Map synchronizedMap = Collections.synchronizedMap(new LinkedHashMap());
            hl.a((Object) synchronizedMap, "Collections.synchronizedMap(mutableMapOf())");
            Map synchronizedMap2 = Collections.synchronizedMap(new LinkedHashMap());
            hl.a((Object) synchronizedMap2, "Collections.synchronizedMap(mutableMapOf())");
            bh bhVar = new bh(context, frameLayout, pontlEveque);
            bk bkVar = new bk(synchronizedMap, synchronizedMap2);
            PontlEveque pontlEveque2 = pontlEveque;
            bk bkVar2 = bkVar;
            bj bjVar = new bj(pontlEveque2, synchronizedMap, synchronizedMap2, bhVar, bkVar2, new bv(context, bkVar), MoelleuxduRevard.a, cqVar, new bt(context, bkVar), 0);
            bjVar.b = new bl(bjVar, bkVar);
            return bjVar;
        }
    }

    public static final class CamembertdeNormandie extends dc {
        final /* synthetic */ bj a;
        final /* synthetic */ db b;
        private String c = "";
        private boolean d;

        public final boolean a() {
            return false;
        }

        CamembertdeNormandie(bj bjVar, db dbVar) {
            this.a = bjVar;
            this.b = dbVar;
        }

        public final void a(WebView webView, String str) {
            this.c = str;
            this.d = true;
            this.a.a(webView, str);
        }

        public final void b(WebView webView, String str) {
            this.a.a(webView, str, this.d);
            this.d = false;
        }

        public final void c(WebView webView, String str) {
            this.a.a(webView, this.c, str);
        }

        public final void a(WebView webView) {
            bs bsVar = (bs) this.a.f.get(bi.b(webView));
            if (bsVar != null) {
                bsVar.e();
            }
            this.b.c();
        }
    }

    private bj(PontlEveque pontlEveque, Map<String, db> map, Map<String, bs> map2, bh bhVar, bk bkVar, bv bvVar, MoelleuxduRevard moelleuxduRevard, cq cqVar, bt btVar) {
        this.d = pontlEveque;
        this.e = map;
        this.f = map2;
        this.g = bhVar;
        this.h = bkVar;
        this.i = bvVar;
        this.j = moelleuxduRevard;
        this.k = cqVar;
        this.l = btVar;
        this.c = Pattern.compile(this.d.i());
    }

    public /* synthetic */ bj(PontlEveque pontlEveque, Map map, Map map2, bh bhVar, bk bkVar, bv bvVar, MoelleuxduRevard moelleuxduRevard, cq cqVar, bt btVar, byte b2) {
        this(pontlEveque, map, map2, bhVar, bkVar, bvVar, moelleuxduRevard, cqVar, btVar);
    }

    public final void a(gf<ep> gfVar) {
        bl blVar = this.b;
        if (blVar == null) {
            hl.a("multiWebViewUrlHandler");
        }
        blVar.a(gfVar);
    }

    public final void b(gf<ep> gfVar) {
        bl blVar = this.b;
        if (blVar == null) {
            hl.a("multiWebViewUrlHandler");
        }
        blVar.b(gfVar);
    }

    public final be a() {
        bl blVar = this.b;
        if (blVar == null) {
            hl.a("multiWebViewUrlHandler");
        }
        return blVar;
    }

    public final boolean b() {
        return this.h.d();
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final void a(bq bqVar) {
        db a2 = this.g.a(bqVar);
        if (a2 != null) {
            this.e.put(bqVar.c(), a2);
            bs bsVar = new bs(bqVar.h(), bqVar.i(), bqVar.a(), false, 56);
            this.f.put(bqVar.c(), bsVar);
            a(a2);
            if (bqVar.j()) {
                an.a(a2);
                WebSettings settings = a2.getSettings();
                hl.a((Object) settings, "webView.settings");
                settings.setCacheMode(1);
            }
            a(bqVar, (WebView) a2);
        }
    }

    private final void a(bq bqVar, WebView webView) {
        if (bqVar.a().length() > 0) {
            webView.loadUrl(bqVar.a());
        } else {
            webView.loadDataWithBaseURL(this.d.h(), bqVar.b(), WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        }
    }

    public final void a(String str, db dbVar, boolean z) {
        dbVar.setTag(str);
        this.e.put(str, dbVar);
        Map<String, bs> map = this.f;
        bs bsVar = new bs(false, z, "", true, 48);
        map.put(str, bsVar);
    }

    public final void b(bq bqVar) {
        db dbVar = (db) this.e.get(bqVar.c());
        if (dbVar != null) {
            WebView webView = dbVar;
            bh.a(webView, bqVar);
            boolean z = false;
            if (!(bqVar.a().length() > 0)) {
                if (bqVar.b().length() > 0) {
                    z = true;
                }
                if (!z) {
                    return;
                }
            }
            a(bqVar, webView);
        }
    }

    public final void a(String str) {
        db dbVar = (db) this.e.get(str);
        if (dbVar != null) {
            this.g.a((WebView) dbVar);
        }
        this.e.remove(str);
        this.f.remove(str);
    }

    private final void a(db dbVar) {
        be[] beVarArr = new be[2];
        bl blVar = this.b;
        if (blVar == null) {
            hl.a("multiWebViewUrlHandler");
        }
        beVarArr[0] = blVar;
        beVarArr[1] = this.k.a(dbVar);
        dbVar.setMraidUrlHandler(new cp(beVarArr));
        dbVar.setClientAdapter(new CamembertdeNormandie(this, dbVar));
    }

    /* access modifiers changed from: private */
    public final void a(WebView webView, String str) {
        this.h.a(ParametersKeys.VIDEO_STATUS_STARTED, f(), e(), bi.b(webView), str);
    }

    /* access modifiers changed from: private */
    public final void a(WebView webView, String str, boolean z) {
        this.h.a("finished", f(), e(), bi.b(webView), str);
        bs bsVar = (bs) this.f.get(bi.b(webView));
        if (bsVar != null) {
            boolean z2 = false;
            boolean z3 = (!bsVar.f() || (hl.a((Object) bsVar.c(), (Object) str) ^ true)) && bsVar.a();
            if (z && z3) {
                if (this.d.i().length() == 0) {
                    z2 = true;
                }
                if (z2) {
                    Mascare mascare = new Mascare(this.d, str, "format", null, null);
                    MoelleuxduRevard.a((Mimolette24mois) mascare);
                }
            }
            bsVar.g();
        }
    }

    /* access modifiers changed from: private */
    public final void a(WebView webView, String str, String str2) {
        bs bsVar = (bs) this.f.get(bi.b(webView));
        if (bsVar != null && !bsVar.h()) {
            if ((this.d.i().length() > 0) && this.c.matcher(str2).matches()) {
                Mascare mascare = new Mascare(this.d, str, "format", this.d.i(), str2);
                MoelleuxduRevard.a((Mimolette24mois) mascare);
                bsVar.i();
            }
        }
    }

    public final void c() {
        for (db dbVar : this.e.values()) {
            if (dbVar.canGoBack()) {
                dbVar.goBack();
            }
        }
    }

    public final void d() {
        this.h.a();
        this.i.a();
        this.l.a();
    }

    private final boolean e() {
        for (db canGoBack : this.e.values()) {
            if (canGoBack.canGoBack()) {
                return true;
            }
        }
        return false;
    }

    private final boolean f() {
        for (db canGoForward : this.e.values()) {
            if (canGoForward.canGoForward()) {
                return true;
            }
        }
        return false;
    }

    public final void b(String str) {
        db dbVar = (db) this.e.get(str);
        if (dbVar != null && dbVar.canGoBack()) {
            dbVar.goBack();
        }
    }

    public final void c(String str) {
        db dbVar = (db) this.e.get(str);
        if (dbVar != null && dbVar.canGoForward()) {
            dbVar.goForward();
        }
    }
}
