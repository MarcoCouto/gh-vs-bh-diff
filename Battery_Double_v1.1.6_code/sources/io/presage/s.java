package io.presage;

import android.content.Context;
import android.os.Build.VERSION;

public final class s implements q {
    public static final s a = new s();
    private static C0133r b;

    private s() {
    }

    private static C0133r b(Context context) {
        if (b == null) {
            Context applicationContext = context.getApplicationContext();
            hl.a((Object) applicationContext, "context.applicationContext");
            b = c(applicationContext);
        }
        C0133r rVar = b;
        if (rVar == null) {
            hl.a();
        }
        return rVar;
    }

    private static C0133r c(Context context) {
        if (VERSION.SDK_INT >= 21) {
            return new o(context);
        }
        return new p(context);
    }

    public final void a(Context context) {
        b(context).a();
    }

    public final void a(Context context, long j) {
        C0133r b2 = b(context);
        b2.a();
        b2.a(j);
    }
}
