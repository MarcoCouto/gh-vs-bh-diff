package io.presage;

import android.os.Handler;
import android.os.Looper;

public final class Gaperon implements GorgonzolaPiccante {
    private final Handler a = new Handler(Looper.getMainLooper());

    public final void a(Runnable runnable) {
        this.a.post(runnable);
    }
}
