package io.presage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class FourmedeHauteLoire implements GorgonzolaPiccante {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final ExecutorService b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public FourmedeHauteLoire() {
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Math.max(4, Runtime.getRuntime().availableProcessors()));
        hl.a((Object) newFixedThreadPool, "Executors.newFixedThreadPool(nrOfCachedThreads)");
        this.b = newFixedThreadPool;
    }

    public final void a(Runnable runnable) {
        this.b.execute(runnable);
    }
}
