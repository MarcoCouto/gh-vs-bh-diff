package io.presage;

import android.app.Activity;

public final class BleudeLaqueuille implements Bethmale {
    private final VacherinSuisse a;
    private final Activity b;
    private final Bethmale c;

    public BleudeLaqueuille(VacherinSuisse vacherinSuisse, Activity activity, Bethmale bethmale) {
        this.a = vacherinSuisse;
        this.b = activity;
        this.c = bethmale;
    }

    public final void a(CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
        camembertdeNormandie.a();
        camembertdeNormandie.setEnableDrag(true);
        VacherinSuisse vacherinSuisse = this.a;
        if (vacherinSuisse != null) {
            vacherinSuisse.a(camembertdeNormandie);
        }
        abbayedeCiteauxentiere.e();
        this.b.finish();
        abbayedeCiteauxentiere.b(this.c);
        camembertdeNormandie.b();
        abbayedeCiteauxentiere.a((Bethmale) new BleudesCausses());
    }
}
