package io.presage;

import android.content.Context;

public final class ParmigianoReggiano {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final PaletdeBourgogne b;
    private final Piastrellou c;
    private final Murol d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static ParmigianoReggiano a(Context context) {
            Context applicationContext = context.getApplicationContext();
            hl.a((Object) applicationContext, "context.applicationContext");
            return new ParmigianoReggiano(applicationContext);
        }
    }

    public ParmigianoReggiano(Context context) {
        this.b = new PaletdeBourgogne(context);
        this.c = new Piastrellou(context);
        this.d = new Murol(context);
    }

    public final dk a() {
        return this.b;
    }

    public final dk b() {
        return this.c;
    }

    public final dk c() {
        return this.d;
    }
}
