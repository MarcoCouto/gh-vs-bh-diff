package io.presage;

import java.util.Collections;
import java.util.Map;

class fp extends fo {
    public static final int a(int i) {
        if (i < 0) {
            return i;
        }
        if (i < 3) {
            return i + 1;
        }
        if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        }
        return Integer.MAX_VALUE;
    }

    public static final <K, V> Map<K, V> a(ej<? extends K, ? extends V> ejVar) {
        Map<K, V> singletonMap = Collections.singletonMap(ejVar.a(), ejVar.b());
        hl.a((Object) singletonMap, "java.util.Collections.si…(pair.first, pair.second)");
        return singletonMap;
    }
}
