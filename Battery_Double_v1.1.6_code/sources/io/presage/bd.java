package io.presage;

import io.presage.common.network.models.RewardItem;

public final class bd extends ba {
    private final RewardItem a;

    public bd(String str, RewardItem rewardItem) {
        super(str, rewardItem.getName());
        this.a = rewardItem;
    }

    public final RewardItem c() {
        return this.a;
    }
}
