package io.presage;

public final class dp extends Exception {
    private final int a;

    public dp(int i) {
        StringBuilder sb = new StringBuilder("Received ");
        sb.append(i);
        sb.append(" from the server");
        super(sb.toString());
        this.a = i;
    }
}
