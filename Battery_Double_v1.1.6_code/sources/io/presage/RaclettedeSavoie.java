package io.presage;

public final class RaclettedeSavoie {
    private final String a;
    private final String b;
    private final String c;

    public RaclettedeSavoie(String str, String str2) {
        this.a = str;
        this.b = str2;
        this.c = null;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    private /* synthetic */ RaclettedeSavoie() {
        this("", null);
    }
}
