package io.presage;

import android.app.Activity;
import java.util.List;

public final class Coulommiers {
    public static final String a(Activity activity) {
        String a = a((Object) activity);
        List b = iv.b((CharSequence) a, new char[]{'.'});
        if (b.size() <= 2) {
            return a;
        }
        StringBuilder sb = new StringBuilder();
        sb.append((String) b.get(0));
        sb.append(".");
        sb.append((String) b.get(1));
        sb.append(".");
        return sb.toString();
    }

    public static final String a(Object obj) {
        String canonicalName = obj.getClass().getCanonicalName();
        if (canonicalName != null) {
            return canonicalName;
        }
        String name = obj.getClass().getName();
        hl.a((Object) name, "this.javaClass.name");
        return name;
    }
}
