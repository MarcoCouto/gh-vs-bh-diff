package io.presage;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;

public final class Maroilles {
    public static final Maroilles a = new Maroilles();

    private Maroilles() {
    }

    public static void a(Context context) {
        if (!b(context)) {
            if (!ae.a(context, "android.permission.INTERNET")) {
                x xVar = x.a;
                x.a("No internet permission");
            }
            c(context);
        }
    }

    private static boolean b(Context context) {
        return (context.getApplicationInfo().flags & 2) == 0;
    }

    private static void c(Context context) {
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 1);
        PackageInfo packageInfo2 = context.getPackageManager().getPackageInfo(context.getPackageName(), 4);
        PackageInfo packageInfo3 = context.getPackageManager().getPackageInfo(context.getPackageName(), 2);
        hl.a((Object) packageInfo, "activitiesInfo");
        c(packageInfo);
        hl.a((Object) packageInfo2, "servicesInfo");
        b(packageInfo2);
        hl.a((Object) packageInfo3, "receiversInfo");
        a(packageInfo3);
    }

    private static void a(PackageInfo packageInfo) {
        ActivityInfo[] activityInfoArr = packageInfo.receivers;
        if (activityInfoArr != null) {
            a(activityInfoArr, "io.presage.common.profig.schedule.ProfigAlarmReceiver");
        }
    }

    private static void b(PackageInfo packageInfo) {
        ServiceInfo[] serviceInfoArr = packageInfo.services;
        if (serviceInfoArr != null) {
            a(serviceInfoArr, "io.presage.common.profig.schedule.ProfigSyncIntentService");
            a(serviceInfoArr, "io.presage.common.profig.schedule.ProfigJobService");
        }
    }

    private static void c(PackageInfo packageInfo) {
        ActivityInfo[] activityInfoArr = packageInfo.activities;
        if (activityInfoArr != null) {
            b(activityInfoArr, "io.presage.interstitial.ui.InterstitialActivity");
            b(activityInfoArr, "io.presage.interstitial.ui.InterstitialAndroid8TransparentActivity");
            b(activityInfoArr, "io.presage.interstitial.ui.InterstitialAndroid8RotableActivity");
        }
    }

    private static void a(ActivityInfo[] activityInfoArr, String str) {
        int length = activityInfoArr.length;
        boolean z = false;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            } else if (hl.a((Object) activityInfoArr[i].name, (Object) str)) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            x xVar = x.a;
            x.a("Missing receiver: ".concat(String.valueOf(str)));
        }
    }

    private static void a(ServiceInfo[] serviceInfoArr, String str) {
        int length = serviceInfoArr.length;
        boolean z = false;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            } else if (hl.a((Object) serviceInfoArr[i].name, (Object) str)) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            x xVar = x.a;
            x.a("Missing service: ".concat(String.valueOf(str)));
        }
    }

    private static void b(ActivityInfo[] activityInfoArr, String str) {
        int length = activityInfoArr.length;
        boolean z = false;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            } else if (hl.a((Object) activityInfoArr[i].name, (Object) str)) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            x xVar = x.a;
            x.a("Missing activity: ".concat(String.valueOf(str)));
        }
    }
}
