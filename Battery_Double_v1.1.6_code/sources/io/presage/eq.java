package io.presage;

import java.util.Collection;
import java.util.Iterator;

final class eq<T> implements hr, Collection<T> {
    private final T[] a;
    private final boolean b = false;

    public final boolean add(T t) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final <T> T[] toArray(T[] tArr) {
        return hh.a(this, tArr);
    }

    public eq(T[] tArr) {
        this.a = tArr;
    }

    public final int size() {
        return a();
    }

    private int a() {
        return this.a.length;
    }

    public final boolean isEmpty() {
        return this.a.length == 0;
    }

    public final boolean contains(Object obj) {
        return er.a(this.a, obj);
    }

    public final boolean containsAll(Collection<? extends Object> collection) {
        Iterable<Object> iterable = collection;
        if (!((Collection) iterable).isEmpty()) {
            for (Object contains : iterable) {
                if (!contains(contains)) {
                    return false;
                }
            }
        }
        return true;
    }

    public final Iterator<T> iterator() {
        return hd.a(this.a);
    }

    public final Object[] toArray() {
        return ex.a(this.a, this.b);
    }
}
