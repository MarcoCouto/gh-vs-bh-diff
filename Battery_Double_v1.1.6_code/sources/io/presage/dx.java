package io.presage;

import android.webkit.WebView;
import com.iab.omid.library.oguryco.adsession.AdSession;

public final class dx {
    private AdSession a;
    private dy b = new dy();

    public final void a(WebView webView, boolean z) {
        this.a = dy.a(webView, z);
        AdSession adSession = this.a;
        if (adSession != null) {
            adSession.start();
        }
    }

    public final void a() {
        AdSession adSession = this.a;
        if (adSession != null) {
            adSession.finish();
        }
    }
}
