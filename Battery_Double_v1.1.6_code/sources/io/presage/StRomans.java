package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.appodeal.ads.AppodealNetworks;
import java.io.File;

public final class StRomans {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static StRomans d;
    private final SharedPreferences b;
    private final Context c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static StRomans a(Context context) {
            if (StRomans.d == null) {
                Context applicationContext = context.getApplicationContext();
                hl.a((Object) applicationContext, "context.applicationContext");
                StRomans.d = new StRomans(applicationContext, 0);
            }
            StRomans c = StRomans.d;
            if (c == null) {
                hl.a();
            }
            return c;
        }
    }

    private StRomans(Context context) {
        this.c = context;
        this.b = this.c.getSharedPreferences(AppodealNetworks.MRAID, 0);
    }

    public /* synthetic */ StRomans(Context context, byte b2) {
        this(context);
    }

    public final void a(String str) {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.c.getFilesDir();
        hl.a((Object) filesDir, "context.filesDir");
        sb.append(filesDir.getPath().toString());
        sb.append("/mraidJs.txt");
        fy.a(new File(sb.toString()), str);
    }

    public final void b(String str) {
        this.b.edit().putString("mraid_download_url", str).apply();
    }

    public final String a() {
        SharedPreferences sharedPreferences = this.b;
        hl.a((Object) sharedPreferences, "sharedPref");
        return ah.a(sharedPreferences, "mraid_download_url", "");
    }

    public final String b() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.c.getFilesDir();
        hl.a((Object) filesDir, "context.filesDir");
        sb.append(filesDir.getPath().toString());
        sb.append("/mraidJs.txt");
        File file = new File(sb.toString());
        return file.exists() ? fy.a(file) : "";
    }
}
