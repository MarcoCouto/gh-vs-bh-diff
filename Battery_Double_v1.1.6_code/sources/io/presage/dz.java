package io.presage;

import android.webkit.WebView;

public final class dz {
    private dx a;
    private final dw b;

    public /* synthetic */ dz() {
        this(dw.a);
    }

    private dz(dw dwVar) {
        this.b = dwVar;
    }

    public final void a(boolean z, WebView webView) {
        if (dw.a()) {
            this.a = dw.b();
            dx dxVar = this.a;
            if (dxVar != null) {
                dxVar.a(webView, z);
            }
        }
    }

    public final void a() {
        if (dw.a()) {
            dx dxVar = this.a;
            if (dxVar != null) {
                dxVar.a();
            }
        }
    }
}
