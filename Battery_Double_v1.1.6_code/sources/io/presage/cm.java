package io.presage;

import android.os.Handler;
import android.os.Looper;
import java.util.LinkedList;
import java.util.List;

public final class cm {
    private final List<cl> a;
    private final cg b;
    /* access modifiers changed from: private */
    public int c;
    private int d;
    /* access modifiers changed from: private */
    public boolean e;
    private Handler f;
    /* access modifiers changed from: private */
    public ci g;
    private ca h;
    private final cj i;

    public static final class CamembertauCalvados implements cg {
        final /* synthetic */ cm a;

        CamembertauCalvados(cm cmVar) {
            this.a = cmVar;
        }

        public final void a(PontlEveque pontlEveque) {
            ca a2 = this.a.a();
            if (a2 != null) {
                a2.a(pontlEveque);
            }
            cm cmVar = this.a;
            cmVar.c = cmVar.c + 1;
            ci b = this.a.g;
            if (b != null) {
                b.a();
            }
            this.a.d();
        }

        public final void a() {
            cm cmVar = this.a;
            cmVar.c = cmVar.c + 1;
            ci b = this.a.g;
            if (b != null) {
                b.a();
            }
            if (this.a.e() && !this.a.e) {
                this.a.g();
            }
        }

        public final void b() {
            this.a.h();
        }
    }

    static final class CamembertdeNormandie implements Runnable {
        final /* synthetic */ cm a;

        CamembertdeNormandie(cm cmVar) {
            this.a = cmVar;
        }

        public final void run() {
            this.a.j();
        }
    }

    public /* synthetic */ cm() {
        this(new cj());
    }

    private cm(cj cjVar) {
        this.i = cjVar;
        this.a = new LinkedList();
        this.b = c();
        this.f = new Handler(Looper.getMainLooper());
    }

    public final ca a() {
        return this.h;
    }

    public final void a(ca caVar) {
        this.h = caVar;
    }

    private final cg c() {
        return new CamembertauCalvados(this);
    }

    /* access modifiers changed from: private */
    public final void d() {
        if (f()) {
            h();
            return;
        }
        if (e()) {
            g();
        }
    }

    /* access modifiers changed from: private */
    public final boolean e() {
        return this.c == this.a.size();
    }

    private final boolean f() {
        return this.d == 1;
    }

    /* access modifiers changed from: private */
    public final void g() {
        this.a.clear();
        i();
        ca caVar = this.h;
        if (caVar != null) {
            caVar.a();
        }
    }

    /* access modifiers changed from: private */
    public final void h() {
        i();
        this.e = true;
        ca caVar = this.h;
        if (caVar != null) {
            caVar.b();
        }
    }

    public final void a(cl clVar) {
        this.a.add(clVar);
    }

    public final void a(cd cdVar, long j, int i2) {
        this.d = i2;
        this.g = cj.a(this.b, cdVar);
        ci ciVar = this.g;
        if (ciVar != null) {
            ciVar.a(this.a);
        }
        a(j);
    }

    private final void i() {
        this.f.removeCallbacksAndMessages(null);
    }

    private final void a(long j) {
        this.f.postDelayed(new CamembertdeNormandie(this), j);
    }

    /* access modifiers changed from: private */
    public final void j() {
        if (l()) {
            k();
            g();
            return;
        }
        m();
        ca caVar = this.h;
        if (caVar != null) {
            caVar.b();
        }
    }

    private final void k() {
        for (cl clVar : this.a) {
            if (clVar instanceof ch) {
                clVar.b();
            }
        }
    }

    private final boolean l() {
        for (cl clVar : this.a) {
            if (!clVar.a() && !(clVar instanceof ch)) {
                return false;
            }
        }
        return true;
    }

    private final void m() {
        for (cl b2 : this.a) {
            b2.b();
        }
    }

    public final void b() {
        i();
        m();
        this.a.clear();
        this.c = 0;
        this.e = false;
    }
}
