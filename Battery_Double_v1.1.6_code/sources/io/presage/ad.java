package io.presage;

import java.util.Map;

public final class ad {
    public static final int a(Map<String, String> map, String str) {
        String str2 = (String) map.get(str);
        if (str2 != null) {
            return Integer.parseInt(str2);
        }
        StringBuilder sb = new StringBuilder("Key ");
        sb.append(str);
        sb.append(" not found in map");
        throw new IllegalStateException(sb.toString().toString());
    }
}
