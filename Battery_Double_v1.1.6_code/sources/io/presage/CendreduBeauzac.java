package io.presage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public final class CendreduBeauzac {
    private AbbayedeCiteauxentiere a;
    private cu b;
    private Activity c;
    private final Application d;
    private final CoeurdeNeufchatel e;
    private final BleudeGex f;
    private CoeurdArras g;
    private CamembertdeNormandie h;
    private final Comte18mois i;

    public static final class CamembertauCalvados implements Bethmale {
        final /* synthetic */ CendreduBeauzac a;

        CamembertauCalvados(CendreduBeauzac cendreduBeauzac) {
            this.a = cendreduBeauzac;
        }

        public final void a(CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            this.a.a();
        }
    }

    static final /* synthetic */ class CamembertdeNormandie extends hk implements gr<PontlEveque, List<PontlEveque>, ep> {
        CamembertdeNormandie(CendreduBeauzac cendreduBeauzac) {
            super(2, cendreduBeauzac);
        }

        public final ig b() {
            return hn.a(CendreduBeauzac.class);
        }

        public final String c() {
            return "showNextAd";
        }

        public final String d() {
            return "showNextAd(Lio/presage/common/network/models/Ad;Ljava/util/List;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj, Object obj2) {
            a((PontlEveque) obj, (List) obj2);
            return ep.a;
        }

        private void a(PontlEveque pontlEveque, List<PontlEveque> list) {
            ((CendreduBeauzac) this.a).a(pontlEveque, list);
        }
    }

    private CendreduBeauzac(Application application, CoeurdeNeufchatel coeurdeNeufchatel, BleudeGex bleudeGex, CamembertdeNormandie camembertdeNormandie, Comte18mois comte18mois) {
        this.d = application;
        this.e = coeurdeNeufchatel;
        this.f = bleudeGex;
        this.g = null;
        this.h = camembertdeNormandie;
        this.i = comte18mois;
        this.b = new cu(0);
        this.h.setContainsOverlayAd(true);
        this.a = b();
    }

    public /* synthetic */ CendreduBeauzac(Application application, CoeurdeNeufchatel coeurdeNeufchatel, BleudeGex bleudeGex) {
        Context applicationContext = application.getApplicationContext();
        hl.a((Object) applicationContext, "application.applicationContext");
        this(application, coeurdeNeufchatel, bleudeGex, new CamembertdeNormandie(applicationContext), new Comte18mois(bleudeGex));
    }

    private final AbbayedeCiteauxentiere b() {
        AbbayedeCiteauxentiere a2 = this.i.a(this.d, this.h);
        a2.a((Bethmale) new BleudesCausses());
        a2.b((Bethmale) new CamembertauCalvados(this));
        a2.a((Bofavre) new CureNantais(new CamembertdeNormandie(this)));
        return a2;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final void a(Activity activity, int i2, int i3, List<PontlEveque> list) {
        this.c = activity;
        try {
            PontlEveque pontlEveque = (PontlEveque) list.remove(0);
            FourmedAmbertBio.a(list);
            this.b.c(af.b(i2));
            this.b.d(af.b(i3));
            this.a.a(af.b(i2), af.b(i3));
            if (pontlEveque.v()) {
                this.f.a(this.d, pontlEveque, new ArrayList());
                if (!list.isEmpty()) {
                    a((PontlEveque) list.remove(0), list, activity, false);
                    return;
                }
                return;
            }
            a(pontlEveque, list, activity, true);
        } catch (Throwable unused) {
            a();
        }
    }

    /* access modifiers changed from: private */
    public final void a(PontlEveque pontlEveque, List<PontlEveque> list) {
        Activity activity = this.c;
        if (activity != null) {
            this.h = new CamembertdeNormandie(this.d);
            this.h.setContainsOverlayAd(true);
            this.a = b();
            this.a.a(this.b.e(), this.b.f());
            a(pontlEveque, list, activity, true);
        }
    }

    private final void a(PontlEveque pontlEveque, List<PontlEveque> list, Activity activity, boolean z) {
        CoeurdArras coeurdArras = this.g;
        if (coeurdArras != null) {
            coeurdArras.unregisterLifecycleListener();
        }
        this.g = this.e.a(activity, this.h, this.a);
        this.a.a(pontlEveque, list);
        if (z) {
            CoeurdArras coeurdArras2 = this.g;
            if (coeurdArras2 != null) {
                coeurdArras2.injectInitialOverlay();
            }
        }
        CoeurdArras coeurdArras3 = this.g;
        if (coeurdArras3 != null) {
            coeurdArras3.registerLifecycleListener();
        }
    }

    public final void a() {
        CoeurdArras coeurdArras = this.g;
        if (coeurdArras != null) {
            coeurdArras.unregisterLifecycleListener();
        }
        this.h.a();
        this.a.h();
    }
}
