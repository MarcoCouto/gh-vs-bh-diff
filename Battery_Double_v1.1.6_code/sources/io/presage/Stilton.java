package io.presage;

import java.util.Iterator;
import java.util.List;

public final class Stilton {
    public static final Stilton a = new Stilton();

    private Stilton() {
    }

    public final void a(List<PontlEveque> list, StRomans stRomans, PavedAremberg pavedAremberg) throws StVincentauChablis {
        PontlEveque a2 = a(list);
        if (a2 != null && (!hl.a((Object) a2.k(), (Object) stRomans.a()))) {
            try {
                a(a2.k(), true, stRomans, pavedAremberg);
            } catch (StVincentauChablis e) {
                MoelleuxduRevard moelleuxduRevard = MoelleuxduRevard.a;
                MoelleuxduRevard.a((Mimolette24mois) new Munster("loaded_error", a2));
                throw e;
            }
        }
    }

    private static PontlEveque a(List<PontlEveque> list) {
        Object obj;
        boolean z;
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((PontlEveque) obj).k().length() > 0) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (PontlEveque) obj;
    }

    private final void a(String str, boolean z, StRomans stRomans, PavedAremberg pavedAremberg) throws StVincentauChablis {
        dr c = pavedAremberg.c(str);
        if (c instanceof ds) {
            stRomans.a(((ds) c).a());
            stRomans.b(str);
        } else if (z) {
            Thread.sleep(400);
            a(str, false, stRomans, pavedAremberg);
        } else {
            throw new StVincentauChablis();
        }
    }
}
