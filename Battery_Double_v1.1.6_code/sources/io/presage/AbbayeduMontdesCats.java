package io.presage;

import android.content.Context;
import io.presage.common.AdConfig;

public final class AbbayeduMontdesCats {
    private final Context a;
    private final AdConfig b;
    private final SableduBoulonnais c;

    public AbbayeduMontdesCats(Context context, AdConfig adConfig, SableduBoulonnais sableduBoulonnais) {
        this.a = context;
        this.b = adConfig;
        this.c = sableduBoulonnais;
    }

    public final AbbayedeTimadeuc a() {
        return new AbbayedeTimadeuc(this.a, this.b, this.c);
    }
}
