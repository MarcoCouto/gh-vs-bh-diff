package io.presage;

import android.content.Context;
import android.webkit.WebResourceResponse;
import io.presage.StRomans.CamembertauCalvados;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;

public final class bc {
    public static final bc a = new bc();

    private bc() {
    }

    private static WebResourceResponse a(String str, String str2) {
        StringBuilder sb = new StringBuilder("javascript:");
        sb.append(str2);
        sb.append(str);
        String sb2 = sb.toString();
        String str3 = "text/javascript";
        String str4 = "UTF-8";
        Charset charset = is.a;
        if (sb2 != null) {
            byte[] bytes = sb2.getBytes(charset);
            hl.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return new WebResourceResponse(str3, str4, new ByteArrayInputStream(bytes));
        }
        throw new em("null cannot be cast to non-null type java.lang.String");
    }

    public static WebResourceResponse a(Context context, SaintFelicien saintFelicien) {
        new az();
        String a2 = az.a(saintFelicien);
        CamembertauCalvados camembertauCalvados = StRomans.a;
        String b = CamembertauCalvados.a(context).b();
        if (b.length() > 0) {
            return a(b, a2);
        }
        return null;
    }
}
