package io.presage;

public final class ds extends dr {
    private final String a;

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (io.presage.hl.a((java.lang.Object) r1.a, (java.lang.Object) ((io.presage.ds) r2).a) != false) goto L_0x0015;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ds) {
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("SuccessResponse(responseBody=");
        sb.append(this.a);
        sb.append(")");
        return sb.toString();
    }

    public ds(String str) {
        super(0);
        this.a = str;
    }

    public final String a() {
        return this.a;
    }
}
