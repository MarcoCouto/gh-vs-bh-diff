package io.presage;

import android.app.Activity;
import java.util.List;

public final class CremeuxduJura {
    private List<String> a = ex.a();
    private List<? extends Class<? extends Activity>> b = ex.a();

    public final List<String> a() {
        return this.a;
    }

    public final void a(List<String> list) {
        this.a = list;
    }

    public final List<Class<? extends Activity>> b() {
        return this.b;
    }

    public final void b(List<? extends Class<? extends Activity>> list) {
        this.b = list;
    }
}
