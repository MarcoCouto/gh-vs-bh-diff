package io.presage;

import android.content.Context;
import io.presage.common.PresageSdk;
import java.util.List;

public final class AbbayedeTamie {
    private j a;
    private final Context b;
    private final t c;
    private final y d;
    private final SableduBoulonnais e;
    private final k f;
    private final PresageSdk g;
    private final AffideliceauChablis h;

    private AbbayedeTamie(Context context, t tVar, y yVar, SableduBoulonnais sableduBoulonnais, k kVar, PresageSdk presageSdk, AffideliceauChablis affideliceauChablis) {
        this.b = context;
        this.c = tVar;
        this.d = yVar;
        this.e = sableduBoulonnais;
        this.f = kVar;
        this.g = presageSdk;
        this.h = affideliceauChablis;
    }

    public /* synthetic */ AbbayedeTamie(Context context, t tVar, y yVar, SableduBoulonnais sableduBoulonnais) {
        this(context, tVar, yVar, sableduBoulonnais, k.a, PresageSdk.a, AffideliceauChablis.a);
    }

    public final boolean a(EcirdelAubrac ecirdelAubrac, boolean z, List<PontlEveque> list) {
        if (a()) {
            if (ecirdelAubrac != null) {
                ecirdelAubrac.a(5);
            }
            return false;
        }
        this.a = k.a(this.b);
        if (!z || list.isEmpty()) {
            if (ecirdelAubrac != null) {
                ecirdelAubrac.d();
            }
            return false;
        }
        for (Number intValue : ex.b(Integer.valueOf(3), Integer.valueOf(2), Integer.valueOf(7), Integer.valueOf(8), Integer.valueOf(1))) {
            int intValue2 = intValue.intValue();
            if (a(intValue2)) {
                if (ecirdelAubrac != null) {
                    ecirdelAubrac.a(intValue2);
                }
                return false;
            }
        }
        return true;
    }

    private final boolean a(int i) {
        switch (i) {
            case 1:
                return h();
            case 2:
                return c();
            case 3:
                return b();
            case 7:
                return d();
            case 8:
                return g();
            default:
                throw new IllegalArgumentException("Illegal argument ".concat(String.valueOf(i)));
        }
    }

    public static boolean a() {
        return !PresageSdk.b();
    }

    private final boolean b() {
        return this.a == null;
    }

    private final boolean c() {
        if (!b()) {
            j jVar = this.a;
            if (jVar == null || jVar.b()) {
                return false;
            }
        }
        return true;
    }

    private final boolean d() {
        return t.a(this.b);
    }

    private final boolean e() {
        return this.e.a() && AffideliceauChablis.b();
    }

    private final boolean f() {
        return this.e.b() && AffideliceauChablis.a();
    }

    private final boolean g() {
        return e() || f();
    }

    private final boolean h() {
        return !this.d.a(this.b);
    }

    public final boolean a(EcirdelAubrac ecirdelAubrac) {
        if (this.d.a(this.b)) {
            return false;
        }
        if (ecirdelAubrac != null) {
            ecirdelAubrac.a(1);
        }
        return true;
    }
}
