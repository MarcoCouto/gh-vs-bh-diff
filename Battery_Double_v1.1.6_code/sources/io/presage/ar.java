package io.presage;

import android.content.Context;
import io.presage.interstitial.ui.InterstitialActivity;
import io.presage.interstitial.ui.InterstitialActivity.CamembertauCalvados;
import java.util.List;

public final class ar implements Appenzeller {
    public static final ar a = new ar();
    private static CamembertauCalvados b = InterstitialActivity.a;

    private ar() {
    }

    public final void a(Context context, List<PontlEveque> list) {
        if (!list.isEmpty()) {
            b.a(context, (PontlEveque) list.remove(0), list);
        }
    }
}
