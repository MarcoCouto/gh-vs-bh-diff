package io.presage;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import java.io.ByteArrayInputStream;

public abstract class bf extends am {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private static final WebResourceResponse e;
    private final Handler b;
    private bc c;
    private SaintFelicien d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    static final class CamembertdeNormandie implements Runnable {
        final /* synthetic */ bf a;

        CamembertdeNormandie(bf bfVar) {
            this.a = bfVar;
        }

        public final void run() {
            this.a.a();
        }
    }

    static final class EcirdelAubrac implements Runnable {
        final /* synthetic */ bf a;
        final /* synthetic */ String b;

        EcirdelAubrac(bf bfVar, String str) {
            this.a = bfVar;
            this.b = str;
        }

        public final void run() {
            ak.a(this.b);
            this.a.a(this.b);
        }
    }

    public abstract void a();

    public abstract void a(String str);

    public boolean b(WebView webView, String str) {
        return true;
    }

    private bf() {
        this.d = null;
        this.b = new Handler(Looper.getMainLooper());
        this.c = bc.a;
    }

    public /* synthetic */ bf(byte b2) {
        this();
    }

    public final void a(SaintFelicien saintFelicien) {
        this.d = saintFelicien;
    }

    static {
        byte[] bytes = "".getBytes(is.a);
        hl.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        e = new WebResourceResponse("text/image", "UTF-8", new ByteArrayInputStream(bytes));
    }

    public WebResourceResponse a(WebView webView, String str) {
        if (bg.a(str)) {
            this.b.post(new EcirdelAubrac(this, str));
            return e;
        } else if (b(str)) {
            return a(webView);
        } else {
            return null;
        }
    }

    private final WebResourceResponse a(WebView webView) {
        Context context = webView.getContext();
        hl.a((Object) context, "view.context");
        WebResourceResponse a2 = bc.a(context, this.d);
        if (a2 != null) {
            return a2;
        }
        this.b.post(new CamembertdeNormandie(this));
        return e;
    }

    private static boolean b(String str) {
        Uri parse = Uri.parse(str);
        hl.a((Object) parse, "uri");
        return hl.a((Object) "mraid.js", (Object) parse.getLastPathSegment());
    }
}
