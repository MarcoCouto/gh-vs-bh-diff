package io.presage.mraid.browser.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import io.presage.bv;
import io.presage.hl;

public final class OrientationListener$1 extends BroadcastReceiver {
    final /* synthetic */ bv a;

    public OrientationListener$1(bv bvVar) {
        this.a = bvVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if (hl.a((Object) "android.intent.action.CONFIGURATION_CHANGED", (Object) intent.getAction())) {
            Resources resources = context.getResources();
            hl.a((Object) resources, "context.resources");
            int i = resources.getConfiguration().orientation;
            if (this.a.a != i) {
                this.a.a = i;
                this.a.c();
            }
        }
    }
}
