package io.presage.mraid.browser.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import io.presage.bt;

public final class CloseSystemDialogsListener$1 extends BroadcastReceiver {
    final /* synthetic */ bt a;

    public CloseSystemDialogsListener$1(bt btVar) {
        this.a = btVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.a.c.d()) {
            this.a.c();
        }
    }
}
