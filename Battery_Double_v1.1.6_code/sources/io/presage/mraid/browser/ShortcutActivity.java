package io.presage.mraid.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import io.presage.FourmedeMontbrison;
import io.presage.Livarot;
import io.presage.bm;
import io.presage.bn;
import io.presage.bo;
import io.presage.bp;
import io.presage.cq;
import io.presage.cs;
import io.presage.ep;
import io.presage.gf;
import io.presage.hl;
import io.presage.hm;

public class ShortcutActivity extends Activity implements Livarot {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private bm b;

    public static final class CamembertauCalvados {

        /* renamed from: io.presage.mraid.browser.ShortcutActivity$CamembertauCalvados$CamembertauCalvados reason: collision with other inner class name */
        static final class C0131CamembertauCalvados extends hm implements gf<ep> {
            final /* synthetic */ Context a;
            final /* synthetic */ cs b;

            C0131CamembertauCalvados(Context context, cs csVar) {
                this.a = context;
                this.b = csVar;
                super(0);
            }

            public final /* synthetic */ Object a() {
                b();
                return ep.a;
            }

            private void b() {
                new bn(this.a, this.b).a();
            }
        }

        static final class CamembertdeNormandie extends hm implements gf<ep> {
            public static final CamembertdeNormandie a = new CamembertdeNormandie();

            CamembertdeNormandie() {
                super(0);
            }

            public final /* bridge */ /* synthetic */ Object a() {
                return ep.a;
            }
        }

        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static void a(Context context, cs csVar) {
            io.presage.FourmedeMontbrison.CamembertauCalvados camembertauCalvados = FourmedeMontbrison.a;
            io.presage.FourmedeMontbrison.CamembertauCalvados.a(new C0131CamembertauCalvados(context, csVar)).a((gf<ep>) CamembertdeNormandie.a);
        }
    }

    static final class CamembertdeNormandie extends hm implements gf<ep> {
        final /* synthetic */ ShortcutActivity a;

        CamembertdeNormandie(ShortcutActivity shortcutActivity) {
            this.a = shortcutActivity;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            this.a.finish();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        if (r2 == null) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001f, code lost:
        if (r1 == null) goto L_0x0021;
     */
    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        super.onCreate(bundle);
        Context context = this;
        FrameLayout frameLayout = new FrameLayout(context);
        Intent intent = getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                str = extras.getString("args", "");
            }
        }
        str = "";
        Intent intent2 = getIntent();
        if (intent2 != null) {
            Bundle extras2 = intent2.getExtras();
            if (extras2 != null) {
                str2 = extras2.getString(SettingsJsonConstants.APP_IDENTIFIER_KEY, "");
            }
        }
        str2 = "";
        Context applicationContext = getApplicationContext();
        hl.a((Object) applicationContext, "this.applicationContext");
        bm bmVar = new bm(new bp(context), this, new cq(applicationContext, new bo(this), new CamembertdeNormandie(this)));
        this.b = bmVar;
        if (!bmVar.a(str, str2, frameLayout)) {
            Toast.makeText(context, "Invalid shortcut", 0).show();
            finish();
            return;
        }
        setContentView(frameLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        bm bmVar = this.b;
        if (bmVar != null) {
            bmVar.a();
        }
    }
}
