package io.presage;

import io.presage.common.network.models.RewardItem;
import java.io.Serializable;

public final class SaintFelicien implements Serializable {
    private String a = "";
    private String b = "";
    private String c = "";
    private String d = "";
    private String e = "";
    private RewardItem f = new RewardItem("", "");

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final String d() {
        return this.d;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final void e(String str) {
        this.e = str;
    }

    public final RewardItem e() {
        return this.f;
    }
}
