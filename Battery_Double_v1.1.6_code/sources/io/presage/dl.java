package io.presage;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Map;

public final class dl {
    private static final dk a = new CamembertauCalvados();

    public static final class CamembertauCalvados implements dk {
        CamembertauCalvados() {
        }

        public final Map<String, String> a() {
            return fn.a();
        }
    }

    public static final boolean a(dk dkVar) {
        return hl.a((Object) (String) dkVar.a().get(HttpRequest.HEADER_CONTENT_ENCODING), (Object) HttpRequest.ENCODING_GZIP);
    }
}
