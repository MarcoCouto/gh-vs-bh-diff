package io.presage;

import android.os.Looper;

public final class ai {
    public static final boolean a(Throwable th) {
        return (th instanceof dp) || (th instanceof Salers);
    }

    public static final void a(String str) {
        if (hl.a((Object) Looper.myLooper(), (Object) Looper.getMainLooper())) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" cannot be called from the main thread");
            new IllegalStateException(sb.toString());
        }
    }
}
