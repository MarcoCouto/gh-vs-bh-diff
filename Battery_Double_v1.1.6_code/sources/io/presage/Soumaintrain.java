package io.presage;

public final class Soumaintrain {
    private final int a;
    private final int b;

    private /* synthetic */ Soumaintrain() {
        this(0, 0);
    }

    public Soumaintrain(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public final int a() {
        return this.a;
    }

    public final int b() {
        return this.b;
    }
}
