package io.presage;

import android.webkit.WebView;
import com.iab.omid.library.oguryco.adsession.AdSession;

public final class dy {
    public static AdSession a(WebView webView, boolean z) {
        try {
            dt dtVar = dt.a;
            du a = dt.a(webView, z);
            AdSession createAdSession = AdSession.createAdSession(a != null ? a.b() : null, a != null ? a.a() : null);
            createAdSession.registerAdView(webView);
            return createAdSession;
        } catch (Exception e) {
            dv dvVar = dv.a;
            dv.a(e);
            return null;
        }
    }
}
