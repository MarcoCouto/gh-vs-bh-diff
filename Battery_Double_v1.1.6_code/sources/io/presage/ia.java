package io.presage;

public final class ia extends hy {
    public static final CamembertauCalvados b = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public static final ia c = new ia(1, 0);

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static ia a() {
            return ia.c;
        }
    }

    public ia(int i, int i2) {
        super(i, i2);
    }

    public final Integer e() {
        return Integer.valueOf(a());
    }

    public final Integer f() {
        return Integer.valueOf(b());
    }

    public final boolean d() {
        return a() > b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (b() == r3.b()) goto L_0x0029;
     */
    public final boolean equals(Object obj) {
        if (obj instanceof ia) {
            if (!d() || !((ia) obj).d()) {
                ia iaVar = (ia) obj;
                if (a() == iaVar.a()) {
                }
            }
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (d()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a());
        sb.append("..");
        sb.append(b());
        return sb.toString();
    }
}
