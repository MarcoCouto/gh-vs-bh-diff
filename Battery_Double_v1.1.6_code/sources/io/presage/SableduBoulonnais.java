package io.presage;

import com.smaato.sdk.core.api.VideoType;

public enum SableduBoulonnais {
    INTERSTITIAL(VideoType.INTERSTITIAL),
    OPTIN_VIDEO("optin_video"),
    OVERLAY_THUMBNAIL("overlay_thumbnail");
    
    private final String e;

    private SableduBoulonnais(String str) {
        this.e = str;
    }

    public final String c() {
        return this.e;
    }

    public final boolean a() {
        SableduBoulonnais sableduBoulonnais = this;
        return sableduBoulonnais == INTERSTITIAL || sableduBoulonnais == OPTIN_VIDEO;
    }

    public final boolean b() {
        return this == OVERLAY_THUMBNAIL;
    }
}
