package io.presage;

import com.applovin.sdk.AppLovinMediationProvider;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class l {
    public static final l a = new l();

    private l() {
    }

    public static n a(JSONObject jSONObject) {
        n nVar;
        if (jSONObject.optBoolean("force")) {
            return d.a;
        }
        if (ab.a(jSONObject)) {
            return i.a;
        }
        try {
            nVar = b(jSONObject);
        } catch (JSONException e) {
            Laguiole laguiole = Laguiole.a;
            Laguiole.a(e);
            nVar = i.a;
        }
        return nVar;
    }

    private static j b(JSONObject jSONObject) {
        j jVar = new j();
        JSONObject optJSONObject = jSONObject.optJSONObject("profig");
        if (optJSONObject == null) {
            optJSONObject = new JSONObject();
        }
        a(optJSONObject, jVar);
        jVar.a(ab.a(optJSONObject.optJSONObject("max_per_day"), "profig", 10));
        jVar.c(m.a(ab.a(optJSONObject.optJSONObject("timeout"), "ads", 3)));
        jVar.a(ab.a(optJSONObject.optJSONObject("logs"), "crash_report", ""));
        b(optJSONObject, jVar);
        c(optJSONObject, jVar);
        a(optJSONObject, jVar.p(), jVar.q());
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("cache");
        jVar.b(ab.a(optJSONObject2 != null ? optJSONObject2.optJSONObject("ads_to_precache") : null, AppLovinMediationProvider.MAX, -1));
        jVar.d(m.a(ab.a(optJSONObject2, "ad_expiration", 14400)));
        return jVar;
    }

    private static void a(JSONObject jSONObject, j jVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject("timing_finder");
        jVar.b(m.a(ab.a(optJSONObject, "profig", 43200)));
        jVar.a(m.a(ab.a(optJSONObject, "no_internet_retry", 7200)));
        jVar.e(m.a(ab.a(optJSONObject, "show_close_button", 2)));
    }

    private static void b(JSONObject jSONObject, j jVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(ParametersKeys.WEB_VIEW);
        jVar.f(ab.a(optJSONObject, "back_button_enabled", false));
        jVar.g(ab.a(optJSONObject, "close_ad_when_leaving_app", true));
        jVar.c(m.a(ab.a(optJSONObject, "webview_load_timeout", 80)));
    }

    private static void c(JSONObject jSONObject, j jVar) {
        JSONArray optJSONArray = jSONObject.optJSONArray(String.ENABLED);
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        String jSONArray = optJSONArray.toString();
        hl.a((Object) jSONArray, "enabledArray.toString()");
        CharSequence charSequence = jSONArray;
        jVar.a(iv.b(charSequence, (CharSequence) "profig"));
        jVar.b(iv.b(charSequence, (CharSequence) "ads"));
        jVar.c(iv.b(charSequence, (CharSequence) "launch"));
        jVar.d(iv.b(charSequence, (CharSequence) "moat"));
        jVar.e(iv.b(charSequence, (CharSequence) "omid"));
    }

    private static void a(JSONObject jSONObject, e eVar, g gVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject("overlay_config");
        if (optJSONObject != null) {
            eVar.a(optJSONObject.optBoolean("multiactivity_enabled", eVar.a()));
            a((f) eVar, optJSONObject);
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("overlay_fragment_config");
        if (optJSONObject2 != null) {
            gVar.a(optJSONObject2.optBoolean("fragment_filter_enabled", gVar.a()));
            a((f) gVar, optJSONObject2);
        }
    }

    private static void a(f fVar, JSONObject jSONObject) {
        fVar.b(jSONObject.optBoolean("default_whitelist_enabled", fVar.b()));
        fVar.d(jSONObject.optBoolean("publisher_blacklist_enabled", fVar.d()));
        fVar.c(jSONObject.optBoolean("publisher_whitelist_enabled", fVar.c()));
        fVar.a(aa.a(jSONObject.optJSONArray("whitelist")));
        fVar.b(aa.a(jSONObject.optJSONArray("blacklist")));
    }

    public static j a(String str) {
        try {
            if ((str.length() > 0) && (!hl.a((Object) str, (Object) "{}"))) {
                return b(new JSONObject(str));
            }
        } catch (Exception unused) {
        }
        return null;
    }
}
