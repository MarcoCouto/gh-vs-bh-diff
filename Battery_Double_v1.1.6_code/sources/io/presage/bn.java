package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutInfo.Builder;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Build.VERSION;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import io.presage.mraid.browser.Android8AndLaterShortcutActivity;
import io.presage.mraid.browser.ShortcutActivity;
import java.util.Collection;
import java.util.List;

public final class bn {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private ShortcutInfo b;
    private Context c;
    private final cs d;
    private final u e;
    private bw f;
    private bp g;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private bn(Context context, cs csVar, u uVar, bw bwVar, bp bpVar) {
        this.c = context;
        this.d = csVar;
        this.e = uVar;
        this.f = bwVar;
        this.g = bpVar;
    }

    public /* synthetic */ bn(Context context, cs csVar) {
        this(context, csVar, new u(), new bw(), new bp(context));
    }

    public final void a() {
        if (!this.d.a()) {
            Bitmap b2 = b();
            if (b2 != null) {
                if (VERSION.SDK_INT >= 26) {
                    this.b = a(b2);
                    ShortcutManager a2 = bw.a(this.c);
                    ShortcutInfo shortcutInfo = this.b;
                    if (shortcutInfo == null) {
                        hl.a("shortcutInfo");
                    }
                    String id = shortcutInfo.getId();
                    hl.a((Object) id, "shortcutInfo.id");
                    if (a(a2, id)) {
                        ShortcutInfo shortcutInfo2 = this.b;
                        if (shortcutInfo2 == null) {
                            hl.a("shortcutInfo");
                        }
                        b(shortcutInfo2, a2);
                    } else {
                        ShortcutInfo shortcutInfo3 = this.b;
                        if (shortcutInfo3 == null) {
                            hl.a("shortcutInfo");
                        }
                        a(shortcutInfo3, a2);
                    }
                } else {
                    if (this.g.a(this.d.b()) || this.g.c(this.d.b())) {
                        a(this.d.c());
                    }
                    b(b2);
                }
                this.g.a(this.d.b(), this.d.e());
            }
        }
    }

    private final Bitmap b() {
        return u.a(this.d.d());
    }

    @SuppressLint({"NewApi"})
    private static boolean a(ShortcutManager shortcutManager, String str) {
        List pinnedShortcuts = shortcutManager.getPinnedShortcuts();
        hl.a((Object) pinnedShortcuts, "shortcutManager.pinnedShortcuts");
        Iterable<ShortcutInfo> iterable = pinnedShortcuts;
        if (!(iterable instanceof Collection) || !((Collection) iterable).isEmpty()) {
            for (ShortcutInfo shortcutInfo : iterable) {
                hl.a((Object) shortcutInfo, "it");
                if (hl.a((Object) str, (Object) shortcutInfo.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressLint({"NewApi"})
    private final ShortcutInfo a(Bitmap bitmap) {
        ShortcutInfo build = new Builder(this.c, this.d.b()).setShortLabel(this.d.c()).setIcon(Icon.createWithBitmap(bitmap)).setIntent(a(Android8AndLaterShortcutActivity.class)).build();
        hl.a((Object) build, "ShortcutInfo.Builder(con…va))\n            .build()");
        return build;
    }

    @SuppressLint({"NewApi"})
    private static void a(ShortcutInfo shortcutInfo, ShortcutManager shortcutManager) {
        if (shortcutManager.isRequestPinShortcutSupported()) {
            shortcutManager.requestPinShortcut(shortcutInfo, null);
        }
    }

    @SuppressLint({"NewApi"})
    private static boolean b(ShortcutInfo shortcutInfo, ShortcutManager shortcutManager) {
        return shortcutManager.updateShortcuts(ex.a(shortcutInfo));
    }

    private final void b(Bitmap bitmap) {
        Intent intent = new Intent();
        intent.putExtra("android.intent.extra.shortcut.INTENT", a(ShortcutActivity.class));
        intent.putExtra("android.intent.extra.shortcut.NAME", this.d.c());
        intent.putExtra("android.intent.extra.shortcut.ICON", bitmap);
        intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        try {
            this.c.sendBroadcast(intent);
        } catch (Exception unused) {
        }
    }

    private final Intent a(Class<?> cls) {
        Intent intent = new Intent(this.c.getApplicationContext(), cls);
        intent.addFlags(268435456);
        intent.addFlags(32768);
        intent.putExtra(SettingsJsonConstants.APP_IDENTIFIER_KEY, this.d.b());
        intent.addFlags(8388608);
        intent.addFlags(67108864);
        intent.setAction("android.intent.action.MAIN");
        return intent;
    }

    private final void a(String str) {
        Intent intent = new Intent();
        intent.putExtra("android.intent.extra.shortcut.NAME", str);
        intent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        try {
            this.c.sendBroadcast(intent);
        } catch (Exception unused) {
        }
    }
}
