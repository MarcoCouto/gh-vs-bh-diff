package io.presage;

import android.app.Activity;
import android.content.Context;
import io.presage.DelicedesFiouves.CamembertauCalvados;

public final class CoeurdeNeufchatel {
    private final CremeuxduJura a;
    private final EtivazGruyereSuisse b;
    private final FourmedAmbert c;
    private final k d;

    private CoeurdeNeufchatel(CremeuxduJura cremeuxduJura, EtivazGruyereSuisse etivazGruyereSuisse, FourmedAmbert fourmedAmbert, k kVar) {
        this.a = cremeuxduJura;
        this.b = etivazGruyereSuisse;
        this.c = fourmedAmbert;
        this.d = kVar;
    }

    public /* synthetic */ CoeurdeNeufchatel(CremeuxduJura cremeuxduJura, EtivazGruyereSuisse etivazGruyereSuisse) {
        this(cremeuxduJura, etivazGruyereSuisse, FourmedAmbert.a, k.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0012, code lost:
        if (r10 == null) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001f, code lost:
        if (r9 == null) goto L_0x0021;
     */
    public final CoeurdArras a(Activity activity, CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
        e eVar;
        g gVar;
        Chaource chaource = new Chaource(camembertdeNormandie, abbayedeCiteauxentiere);
        j a2 = k.a((Context) activity);
        if (a2 != null) {
            eVar = a2.p();
        }
        eVar = new e();
        if (a2 != null) {
            gVar = a2.q();
        }
        gVar = new g();
        new CapGrisNez();
        CarreMirabelle a3 = CapGrisNez.a(activity, this.a, eVar);
        CamembertauCalvados camembertauCalvados = DelicedesFiouves.a;
        DelicedesFiouves a4 = CamembertauCalvados.a(activity, gVar, this.b);
        if (this.b.c() || !gVar.a()) {
            return new CarreNormand(activity, chaource, a3);
        }
        if (FourmedAmbert.a()) {
            EmmentalGrandCru emmentalGrandCru = new EmmentalGrandCru(activity, chaource, new Entrammes(a4), null, 8, null);
            return emmentalGrandCru;
        } else if (FourmedAmbert.b()) {
            EpoissesdeBourgogne epoissesdeBourgogne = new EpoissesdeBourgogne(activity, chaource, new Entrammes(a4), null, 8, null);
            return epoissesdeBourgogne;
        } else {
            x xVar = x.a;
            x.a("Fragment filter defined for thumbnail but no fragment dependency found. Only androidx and support v4 fragments are supported");
            return new CarreNormand(activity, chaource, a3);
        }
    }
}
