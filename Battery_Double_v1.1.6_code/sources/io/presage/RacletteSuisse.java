package io.presage;

import io.presage.common.network.models.RewardItem;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public final class RacletteSuisse {
    public static final RacletteSuisse a = new RacletteSuisse();
    private static OlivetalaSauge b = new OlivetalaSauge();

    private RacletteSuisse() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0088, code lost:
        if (r7 == null) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0097, code lost:
        if (r7 == null) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x006b, code lost:
        if (r7 == null) goto L_0x006d;
     */
    public static RouedeBrie a(String str, SableduBoulonnais sableduBoulonnais, Soumaintrain soumaintrain) throws Salers {
        String str2;
        String str3;
        String str4;
        JSONObject jSONObject = new JSONObject(str);
        OlivetalaSauge.a(jSONObject);
        List arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("ad");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        int length = optJSONArray.length();
        for (int i = 0; i < length; i++) {
            PontlEveque pontlEveque = new PontlEveque();
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            JSONObject optJSONObject = jSONObject2.optJSONObject("format");
            String optString = jSONObject2.optString("ad_content");
            hl.a((Object) optString, "adJson.optString(\"ad_content\")");
            pontlEveque.c(optString);
            String optString2 = jSONObject2.optString("impression_url");
            hl.a((Object) optString2, "adJson.optString(\"impression_url\")");
            pontlEveque.e(optString2);
            String optString3 = jSONObject2.optString("id");
            hl.a((Object) optString3, "adJson.optString(\"id\")");
            pontlEveque.b(optString3);
            JSONObject optJSONObject2 = jSONObject2.optJSONObject("advertiser");
            if (optJSONObject2 != null) {
                str2 = optJSONObject2.optString("id");
            }
            str2 = "";
            pontlEveque.f(str2);
            String optString4 = jSONObject2.optString("campaign_id");
            hl.a((Object) optString4, "adJson.optString(\"campaign_id\")");
            pontlEveque.g(optString4);
            if (optJSONObject != null) {
                str3 = optJSONObject.optString("webview_base_url");
            }
            str3 = "";
            pontlEveque.h(str3);
            if (optJSONObject != null) {
                str4 = optJSONObject.optString("mraid_download_url");
            }
            str4 = "";
            pontlEveque.k(str4);
            pontlEveque.b(jSONObject2.optBoolean("moat", false));
            pontlEveque.c(jSONObject2.optBoolean("omid", false));
            pontlEveque.d(jSONObject2.optBoolean("is_video", false));
            pontlEveque.a(a(jSONObject2.optJSONObject("overlay"), soumaintrain));
            pontlEveque.a(a(jSONObject2.optJSONObject("ad_unit"), jSONObject2.optString("id")));
            hl.a((Object) jSONObject2, "adJson");
            pontlEveque.d(a("orientation", jSONObject2));
            pontlEveque.j(a(optJSONObject != null ? optJSONObject.optJSONArray("params") : null));
            pontlEveque.i(a(jSONObject2));
            pontlEveque.a(jSONObject2.optBoolean("has_transparency", false));
            String optString5 = jSONObject2.optString("sdk_close_button_url", "");
            hl.a((Object) optString5, "adJson.optString(\"sdk_close_button_url\", \"\")");
            pontlEveque.l(optString5);
            String optString6 = jSONObject2.optString("landing_page_prefetch_url", "");
            hl.a((Object) optString6, "adJson.optString(\"landing_page_prefetch_url\", \"\")");
            pontlEveque.m(optString6);
            pontlEveque.e(jSONObject2.optBoolean("landing_page_disable_javascript", false));
            String optString7 = jSONObject2.optString("landing_page_prefetch_whitelist", "");
            hl.a((Object) optString7, "adJson.optString(\"landin…_prefetch_whitelist\", \"\")");
            pontlEveque.n(optString7);
            pontlEveque.g(jSONObject2.optBoolean("ad_keep_alive", false));
            StringBuilder sb = new StringBuilder();
            sb.append(UUID.randomUUID().toString());
            sb.append(pontlEveque.g());
            pontlEveque.a(sb.toString());
            pontlEveque.f(!jSONObject2.has("overlay"));
            pontlEveque.a(sableduBoulonnais);
            if (hl.a((Object) pontlEveque.m().c(), (Object) sableduBoulonnais.c())) {
                arrayList.add(pontlEveque);
            }
        }
        return new RouedeBrie(arrayList);
    }

    private static SaintFelicien a(JSONObject jSONObject, String str) {
        SaintFelicien saintFelicien = new SaintFelicien();
        if (jSONObject == null) {
            return saintFelicien;
        }
        String optString = jSONObject.optString("id");
        if (optString == null) {
            optString = "";
        }
        saintFelicien.a(optString);
        if (str == null) {
            str = "";
        }
        saintFelicien.b(str);
        String optString2 = jSONObject.optString("type");
        if (optString2 == null) {
            optString2 = "";
        }
        saintFelicien.c(optString2);
        if (hl.a((Object) saintFelicien.c(), (Object) "optin_video")) {
            String optString3 = jSONObject.optString("app_user_id");
            if (optString3 == null) {
                optString3 = "";
            }
            saintFelicien.e(optString3);
            String optString4 = jSONObject.optString("reward_launch");
            if (optString4 == null) {
                optString4 = "";
            }
            saintFelicien.d(optString4);
            RewardItem e = saintFelicien.e();
            String optString5 = jSONObject.optString("reward_name");
            if (optString5 == null) {
                optString5 = "";
            }
            e.setName(optString5);
            RewardItem e2 = saintFelicien.e();
            String optString6 = jSONObject.optString("reward_value");
            if (optString6 == null) {
                optString6 = "";
            }
            e2.setValue(optString6);
        }
        return saintFelicien;
    }

    private static String a(JSONObject jSONObject) {
        String optString = jSONObject.optString("client_tracker_pattern", "");
        if (hl.a((Object) optString, (Object) "null")) {
            optString = "";
        }
        hl.a((Object) optString, "clientTrackerPattern");
        return optString;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        if (r4 == null) goto L_0x0036;
     */
    private static String a(JSONArray jSONArray) {
        String str;
        if (jSONArray == null) {
            return "";
        }
        for (int length = jSONArray.length() - 1; length >= 0; length--) {
            JSONObject jSONObject = jSONArray.getJSONObject(length);
            if (hl.a((Object) jSONObject.getString("name"), (Object) "zones")) {
                JSONArray optJSONArray = jSONObject.optJSONArray("value");
                if (optJSONArray != null) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(0);
                    if (optJSONObject != null) {
                        str = optJSONObject.optString("name");
                    }
                }
                str = "";
                return str;
            }
        }
        return "";
    }

    private static String a(String str, JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("params");
        if (optJSONArray == null) {
            return "";
        }
        int length = optJSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            if (hl.a((Object) jSONObject2.getString("name"), (Object) str)) {
                String string = jSONObject2.getString("value");
                hl.a((Object) string, "paramElement.getString(\"value\")");
                return string;
            }
        }
        return "";
    }

    private static StMarcellin a(JSONObject jSONObject, Soumaintrain soumaintrain) {
        StMarcellin stMarcellin = new StMarcellin();
        if (soumaintrain == null) {
            return stMarcellin;
        }
        boolean z = true;
        if (jSONObject != null) {
            z = jSONObject.optBoolean("draggable", true);
        }
        stMarcellin.a(z);
        JSONObject optJSONObject = jSONObject != null ? jSONObject.optJSONObject("initial_size") : null;
        stMarcellin.a(af.b(optJSONObject != null ? optJSONObject.optInt("width") : soumaintrain.a()));
        stMarcellin.b(af.b(optJSONObject != null ? optJSONObject.getInt("height") : soumaintrain.b()));
        return stMarcellin;
    }
}
