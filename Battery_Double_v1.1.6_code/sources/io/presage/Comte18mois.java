package io.presage;

import android.app.Application;
import io.presage.AbbayedeCiteauxentiere.CamembertauCalvados;

public final class Comte18mois {
    private final BleudeGex a;

    public Comte18mois(BleudeGex bleudeGex) {
        this.a = bleudeGex;
    }

    public final AbbayedeCiteauxentiere a(Application application, CamembertdeNormandie camembertdeNormandie) {
        return new CamembertauCalvados(application, camembertdeNormandie, new BleudAuvergnebio(this.a), false).l();
    }
}
