package io.presage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class jf extends je {

    static final class CamembertauCalvados extends hm implements gr<CharSequence, Integer, ej<? extends Integer, ? extends Integer>> {
        final /* synthetic */ char[] a;
        final /* synthetic */ boolean b = false;

        CamembertauCalvados(char[] cArr, boolean z) {
            this.a = cArr;
            super(2);
        }

        public final /* synthetic */ Object a(Object obj, Object obj2) {
            return a((CharSequence) obj, ((Number) obj2).intValue());
        }

        private ej<Integer, Integer> a(CharSequence charSequence, int i) {
            int a2 = iv.a(charSequence, this.a, i, this.b);
            if (a2 < 0) {
                return null;
            }
            return el.a(Integer.valueOf(a2), Integer.valueOf(1));
        }
    }

    public static final int a(CharSequence charSequence) {
        return charSequence.length() - 1;
    }

    public static final String a(CharSequence charSequence, ia iaVar) {
        return charSequence.subSequence(iaVar.e().intValue(), iaVar.f().intValue() + 1).toString();
    }

    public static final String c(String str, String str2) {
        int b = iv.b((CharSequence) str, iv.a((CharSequence) str));
        if (b == -1) {
            return str2;
        }
        String substring = str.substring(b + 1, str.length());
        hl.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final CharSequence a(CharSequence charSequence, int i, int i2, CharSequence charSequence2) {
        if (i2 >= i) {
            StringBuilder sb = new StringBuilder();
            sb.append(charSequence, 0, i);
            hl.a((Object) sb, "this.append(value, startIndex, endIndex)");
            sb.append(charSequence2);
            sb.append(charSequence, i2, charSequence.length());
            hl.a((Object) sb, "this.append(value, startIndex, endIndex)");
            return sb;
        }
        StringBuilder sb2 = new StringBuilder("End index (");
        sb2.append(i2);
        sb2.append(") is less than start index (");
        sb2.append(i);
        sb2.append(").");
        throw new IndexOutOfBoundsException(sb2.toString());
    }

    public static final boolean a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z) {
        if (i < 0 || charSequence.length() - i2 < 0 || i > charSequence2.length() - i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (!ip.a(charSequence.charAt(i3 + 0), charSequence2.charAt(i + i3), z)) {
                return false;
            }
        }
        return true;
    }

    public static final int a(CharSequence charSequence, char[] cArr, int i, boolean z) {
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            int a = ib.a(i);
            int a2 = iv.a(charSequence);
            if (a <= a2) {
                while (true) {
                    char charAt = charSequence.charAt(a);
                    int length = cArr.length;
                    boolean z2 = false;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            break;
                        } else if (ip.a(cArr[i2], charAt, z)) {
                            z2 = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (!z2) {
                        if (a == a2) {
                            break;
                        }
                        a++;
                    } else {
                        return a;
                    }
                }
            }
            return -1;
        }
        return ((String) charSequence).indexOf(er.a(cArr), i);
    }

    public static final int a(CharSequence charSequence, char[] cArr, int i) {
        if (charSequence instanceof String) {
            return ((String) charSequence).lastIndexOf(er.a(cArr), i);
        }
        for (int b = ib.b(i, iv.a(charSequence)); b >= 0; b--) {
            char charAt = charSequence.charAt(b);
            boolean z = false;
            int i2 = 0;
            while (true) {
                if (i2 > 0) {
                    break;
                } else if (ip.a(cArr[i2], charAt, false)) {
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                return b;
            }
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public static final int b(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z) {
        hy iaVar = new ia(ib.a(i), ib.b(i2, charSequence.length()));
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int a = iaVar.a();
            int b = iaVar.b();
            int c = iaVar.c();
            if (c < 0 ? a >= b : a <= b) {
                while (!iv.a(charSequence2, charSequence, a, charSequence2.length(), z)) {
                    if (a != b) {
                        a += c;
                    }
                }
                return a;
            }
        } else {
            int a2 = iaVar.a();
            int b2 = iaVar.b();
            int c2 = iaVar.c();
            if (c2 < 0 ? a2 >= b2 : a2 <= b2) {
                while (!iv.a((String) charSequence2, (String) charSequence, a2, charSequence2.length(), z)) {
                    if (a2 != b2) {
                        a2 += c2;
                    }
                }
                return a2;
            }
        }
        return -1;
    }

    public static /* synthetic */ int a(CharSequence charSequence, int i, int i2) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return iv.a(charSequence, i);
    }

    public static final int a(CharSequence charSequence, int i) {
        if (charSequence instanceof String) {
            return ((String) charSequence).indexOf(46, i);
        }
        return iv.a(charSequence, new char[]{'.'}, i, false);
    }

    public static final int a(CharSequence charSequence, String str, int i, boolean z) {
        if (z || !(charSequence instanceof String)) {
            return b(charSequence, str, i, charSequence.length(), z);
        }
        return ((String) charSequence).indexOf(str, i);
    }

    public static final int b(CharSequence charSequence, int i) {
        if (charSequence instanceof String) {
            return ((String) charSequence).lastIndexOf(46, i);
        }
        return iv.a(charSequence, new char[]{'.'}, i);
    }

    public static final boolean a(CharSequence charSequence, CharSequence charSequence2) {
        return charSequence2 instanceof String ? iv.a(charSequence, (String) charSequence2, 0, false) >= 0 : b(charSequence, charSequence2, 0, charSequence.length(), false) >= 0;
    }

    /* access modifiers changed from: private */
    public static final ii<ia> c(CharSequence charSequence, char[] cArr) {
        return new it<>(charSequence, 0, new CamembertauCalvados(cArr, false));
    }

    public static final List<String> a(CharSequence charSequence, char[] cArr) {
        if (cArr.length == 1) {
            return b(charSequence, String.valueOf(cArr[0]));
        }
        Iterable<ia> a = ij.a(c(charSequence, cArr));
        Collection arrayList = new ArrayList(ex.a(a));
        for (ia a2 : a) {
            arrayList.add(iv.a(charSequence, a2));
        }
        return (List) arrayList;
    }

    private static final List<String> b(CharSequence charSequence, String str) {
        int a = iv.a(charSequence, str, 0, false);
        if (a == -1) {
            return ex.a(charSequence.toString());
        }
        ArrayList arrayList = new ArrayList(10);
        int i = a;
        int i2 = 0;
        do {
            arrayList.add(charSequence.subSequence(i2, i).toString());
            i2 = str.length() + i;
            i = iv.a(charSequence, str, i2, false);
        } while (i != -1);
        arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
        return arrayList;
    }
}
