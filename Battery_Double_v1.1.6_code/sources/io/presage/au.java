package io.presage;

import android.app.Application;
import android.webkit.WebView;
import com.moat.analytics.mobile.ogury.MoatAnalytics;
import com.moat.analytics.mobile.ogury.WebAdTracker;

public final class au {
    private WebAdTracker a;
    private final at b;

    private au(at atVar) {
        this.b = atVar;
    }

    public au() {
        MoatAnalytics instance = MoatAnalytics.getInstance();
        hl.a((Object) instance, "MoatAnalytics.getInstance()");
        this(new at(instance));
    }

    private final void a(Application application) {
        this.b.a(application);
    }

    private final void a(WebView webView) {
        this.a = at.a().createWebAdTracker(webView);
    }

    public final void a(Application application, WebView webView) {
        a(application);
        a(webView);
        WebAdTracker webAdTracker = this.a;
        if (webAdTracker != null) {
            webAdTracker.startTracking();
        }
    }

    public final void a() {
        WebAdTracker webAdTracker = this.a;
        if (webAdTracker != null) {
            webAdTracker.stopTracking();
        }
    }
}
