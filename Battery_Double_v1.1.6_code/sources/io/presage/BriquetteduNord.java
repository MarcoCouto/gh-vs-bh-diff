package io.presage;

import android.graphics.Rect;

public final class BriquetteduNord {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final Rect b;
    private final Rect c;
    private final float d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public BriquetteduNord(Rect rect, Rect rect2, float f) {
        this.b = rect;
        this.c = rect2;
        this.d = f;
    }

    public final boolean a() {
        return d() || e() || f() || g();
    }

    private final boolean d() {
        if (this.b.left < this.c.left) {
            this.b.right += this.c.left - this.b.left;
            this.b.left = this.c.left;
        }
        return h();
    }

    private final boolean e() {
        if (this.b.top < this.c.top) {
            this.b.bottom += this.c.top - this.b.top;
            this.b.top = this.c.top;
        }
        return h();
    }

    private final boolean f() {
        if (this.b.right > this.c.right) {
            int i = this.b.right - this.c.right;
            this.b.left -= i;
            this.b.right -= i;
        }
        return h();
    }

    private final boolean g() {
        if (this.b.bottom > this.c.bottom) {
            int i = this.b.bottom - this.c.bottom;
            this.b.top -= i;
            this.b.bottom -= i;
        }
        return h();
    }

    private final boolean h() {
        return c() >= this.d;
    }

    public final void b() {
        i();
        j();
    }

    private final void i() {
        if (this.b.width() > this.c.width()) {
            int a2 = a((float) this.b.width(), (float) this.c.width());
            this.b.right = (this.b.right - a(this.b.right, a2)) - this.b.left;
            this.b.bottom -= a(this.b.bottom, a2);
            this.b.left = this.c.left;
        }
    }

    private final void j() {
        if (this.b.height() > this.c.height()) {
            int a2 = a((float) this.b.height(), (float) this.c.height());
            this.b.bottom = (this.b.bottom - a(this.b.bottom, a2)) - this.b.top;
            this.b.right -= a(this.b.right, a2);
            this.b.top = this.c.top;
        }
    }

    private static int a(float f, float f2) {
        return hv.a(((f - f2) / f) * 100.0f);
    }

    private static int a(int i, int i2) {
        return (i * i2) / 100;
    }

    public final float c() {
        Rect a2 = a(this.b, this.c);
        if (a2 == null) {
            return 0.0f;
        }
        return ((float) (a2.width() * a2.height())) / ((float) (this.b.width() * this.b.height()));
    }

    private static Rect a(Rect rect, Rect rect2) {
        Rect rect3 = new Rect(rect);
        if (!rect3.intersect(rect2)) {
            return null;
        }
        return rect3;
    }
}
