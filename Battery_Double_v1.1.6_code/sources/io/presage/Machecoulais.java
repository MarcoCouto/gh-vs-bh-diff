package io.presage;

import android.content.Context;
import android.util.Log;
import com.ogury.crashreport.CrashConfig;
import com.ogury.crashreport.CrashReport;
import com.ogury.crashreport.SdkInfo;
import io.presage.common.PresageSdkInitCallback;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public final class Machecoulais {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public String c;
    private List<PresageSdkInitCallback> d;
    private final io.presage.FourmedeMontbrison.CamembertauCalvados e;
    /* access modifiers changed from: private */
    public final io.presage.h.CamembertauCalvados f;
    private final io.presage.TommedAuvergne.CamembertauCalvados g;
    private final MoelleuxduRevard h;
    /* access modifiers changed from: private */
    public final k i;
    private final dw j;
    private final Maroilles k;
    private final aj l;

    static final class AbbayedeTamie extends hm implements gf<ep> {
        final /* synthetic */ Machecoulais a;
        final /* synthetic */ Context b;

        AbbayedeTamie(Machecoulais machecoulais, Context context) {
            this.a = machecoulais;
            this.b = context;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            this.a.b = 1;
            this.a.f();
            this.a.b(this.b);
        }
    }

    static final class AbbayedeTimadeuc extends hm implements gf<j> {
        final /* synthetic */ Machecoulais a;
        final /* synthetic */ Context b;

        AbbayedeTimadeuc(Machecoulais machecoulais, Context context) {
            this.a = machecoulais;
            this.b = context;
            super(0);
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public j a() {
            this.a.i;
            return k.a(this.b);
        }
    }

    static final class AbbayeduMontdesCats extends hm implements gg<j, ep> {
        final /* synthetic */ Machecoulais a;
        final /* synthetic */ Context b;

        AbbayeduMontdesCats(Machecoulais machecoulais, Context context) {
            this.a = machecoulais;
            this.b = context;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((j) obj);
            return ep.a;
        }

        private void a(j jVar) {
            Machecoulais.b(jVar, this.b);
        }
    }

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    static final class CamembertdeNormandie extends hm implements gf<ep> {
        final /* synthetic */ Machecoulais a;
        final /* synthetic */ Context b;
        final /* synthetic */ String c;

        CamembertdeNormandie(Machecoulais machecoulais, Context context, String str) {
            this.a = machecoulais;
            this.b = context;
            this.c = str;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            this.a.f;
            h a2 = io.presage.h.CamembertauCalvados.a(this.b);
            a2.e(this.c);
            Machecoulais.b(this.a.c, a2);
            this.a.a(this.b, a2);
        }
    }

    static final class EcirdelAubrac extends hm implements gg<Throwable, ep> {
        final /* synthetic */ Machecoulais a;

        EcirdelAubrac(Machecoulais machecoulais) {
            this.a = machecoulais;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a();
            return ep.a;
        }

        private void a() {
            this.a.b = 3;
            this.a.g();
        }
    }

    public static String d() {
        return "3.3.9-moat";
    }

    private Machecoulais(io.presage.FourmedeMontbrison.CamembertauCalvados camembertauCalvados, io.presage.h.CamembertauCalvados camembertauCalvados2, io.presage.TommedAuvergne.CamembertauCalvados camembertauCalvados3, MoelleuxduRevard moelleuxduRevard, k kVar, dw dwVar, Maroilles maroilles, aj ajVar) {
        this.e = camembertauCalvados;
        this.f = camembertauCalvados2;
        this.g = camembertauCalvados3;
        this.h = moelleuxduRevard;
        this.i = kVar;
        this.j = dwVar;
        this.k = maroilles;
        this.l = ajVar;
        List<PresageSdkInitCallback> synchronizedList = Collections.synchronizedList(new LinkedList());
        hl.a((Object) synchronizedList, "Collections.synchronizedList(LinkedList())");
        this.d = synchronizedList;
    }

    public /* synthetic */ Machecoulais() {
        this(FourmedeMontbrison.a, h.a, TommedAuvergne.a, MoelleuxduRevard.a, k.a, dw.a, Maroilles.a, aj.a);
    }

    public final void a(Context context, String str) {
        a(context);
        Maroilles.a(context);
        if (this.b == 0 || this.b == 3) {
            this.b = 2;
            CharSequence charSequence = str;
            if (!(charSequence == null || charSequence.length() == 0)) {
                io.presage.FourmedeMontbrison.CamembertauCalvados.a(new CamembertdeNormandie(this, context, str)).a((gg<? super Throwable, ep>) new EcirdelAubrac<Object,ep>(this)).a((gf<ep>) new AbbayedeTamie<ep>(this, context));
                return;
            }
            Log.e("Presage", "PresageSdk.init() error", new IllegalArgumentException("The api key is null empty. Please provide a valid api key"));
            this.b = 0;
        }
    }

    private final void a(Context context) {
        try {
            this.l.a(context);
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: private */
    public static void b(String str, h hVar) {
        if (str != null) {
            hVar.f(str);
        }
    }

    public final void a(String str) {
        this.c = str;
    }

    public final boolean a() {
        return this.b == 2;
    }

    public final boolean b() {
        return this.b == 1;
    }

    private boolean e() {
        return this.b == 0;
    }

    public final boolean c() {
        return this.b == 3;
    }

    public final void a(PresageSdkInitCallback presageSdkInitCallback) {
        if (b()) {
            presageSdkInitCallback.onSdkInitialized();
        } else if (a()) {
            this.d.add(presageSdkInitCallback);
        } else if (e()) {
            presageSdkInitCallback.onSdkNotInitialized();
        } else {
            if (c()) {
                presageSdkInitCallback.onSdkInitFailed();
            }
        }
    }

    /* access modifiers changed from: private */
    public final void a(Context context, h hVar) {
        if (!c(context)) {
            Context applicationContext = context.getApplicationContext();
            String i2 = hVar.i();
            if (!hl.a((Object) i2, (Object) "")) {
                io.presage.TommedAuvergne.CamembertauCalvados camembertauCalvados = this.g;
                hl.a((Object) applicationContext, "appContext");
                camembertauCalvados.a(applicationContext).b(false);
                MoelleuxduRevard.a(applicationContext);
                a(applicationContext, i2, hVar);
                return;
            }
            Throwable illegalStateException = new IllegalStateException("There is no api key. Please call PresageSdk.init(context, apiKey) before trying to load or display an ad");
            Log.e("Presage", "Init Error", illegalStateException);
            throw illegalStateException;
        }
        throw new IllegalStateException("The app is not in main application process");
    }

    private static void a(Context context, String str, h hVar) {
        j a2 = k.a(context);
        if (a2 != null) {
            try {
                CrashReport.register(context, new SdkInfo(d(), str, hVar.c()), new CrashConfig(a2.o(), context.getPackageName()));
            } catch (Throwable unused) {
            }
        }
    }

    /* access modifiers changed from: private */
    public final void b(Context context) {
        io.presage.Goudaauxepices.CamembertauCalvados camembertauCalvados = Goudaauxepices.a;
        io.presage.Goudaauxepices.CamembertauCalvados.a(new AbbayedeTimadeuc(this, context)).b((gg<? super T, ep>) new AbbayeduMontdesCats<Object,ep>(this, context));
    }

    /* access modifiers changed from: private */
    public static void b(j jVar, Context context) {
        if (jVar != null && jVar.d()) {
            dw.a(context);
        }
    }

    private final boolean c(Context context) {
        return !b() && !ag.a(context);
    }

    /* access modifiers changed from: private */
    public final void f() {
        for (PresageSdkInitCallback onSdkInitialized : this.d) {
            onSdkInitialized.onSdkInitialized();
        }
        this.d.clear();
    }

    /* access modifiers changed from: private */
    public final void g() {
        for (PresageSdkInitCallback onSdkInitFailed : this.d) {
            onSdkInitFailed.onSdkInitFailed();
        }
        this.d.clear();
    }
}
