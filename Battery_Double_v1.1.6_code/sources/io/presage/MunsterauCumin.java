package io.presage;

import android.content.Context;
import io.presage.common.AdConfig;

public final class MunsterauCumin {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public final PavedAremberg b;
    /* access modifiers changed from: private */
    public final StRomans c;
    /* access modifiers changed from: private */
    public final Stilton d;
    /* access modifiers changed from: private */
    public final RacletteSuisse e;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    static final class CamembertdeNormandie extends hm implements gf<RouedeBrie> {
        final /* synthetic */ MunsterauCumin a;
        final /* synthetic */ SableduBoulonnais b;
        final /* synthetic */ AdConfig c;
        final /* synthetic */ String d;
        final /* synthetic */ Soumaintrain e;

        CamembertdeNormandie(MunsterauCumin munsterauCumin, SableduBoulonnais sableduBoulonnais, AdConfig adConfig, String str, Soumaintrain soumaintrain) {
            this.a = munsterauCumin;
            this.b = sableduBoulonnais;
            this.c = adConfig;
            this.d = str;
            this.e = soumaintrain;
            super(0);
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public RouedeBrie a() {
            dr a2 = this.a.b.a(this.b, this.c, this.d, this.e);
            if (a2 instanceof ds) {
                this.a.e;
                RouedeBrie a3 = RacletteSuisse.a(((ds) a2).a(), this.b, this.e);
                this.a.d.a(a3.a(), this.a.c, this.a.b);
                return a3;
            } else if (a2 instanceof di) {
                throw ((di) a2).a();
            } else {
                throw new ei();
            }
        }
    }

    private MunsterauCumin(PavedAremberg pavedAremberg, StRomans stRomans, Stilton stilton, RacletteSuisse racletteSuisse) {
        this.b = pavedAremberg;
        this.c = stRomans;
        this.d = stilton;
        this.e = racletteSuisse;
    }

    private /* synthetic */ MunsterauCumin(PavedAremberg pavedAremberg, StRomans stRomans) {
        this(pavedAremberg, stRomans, Stilton.a, RacletteSuisse.a);
    }

    public MunsterauCumin(Context context) {
        PersilledumontBlanc persilledumontBlanc = PersilledumontBlanc.a;
        PavedAremberg a2 = PersilledumontBlanc.a(context);
        io.presage.StRomans.CamembertauCalvados camembertauCalvados = StRomans.a;
        this(a2, io.presage.StRomans.CamembertauCalvados.a(context));
    }

    public final Goudaauxepices<RouedeBrie> a(SableduBoulonnais sableduBoulonnais, AdConfig adConfig, String str, Soumaintrain soumaintrain) {
        io.presage.Goudaauxepices.CamembertauCalvados camembertauCalvados = Goudaauxepices.a;
        CamembertdeNormandie camembertdeNormandie = new CamembertdeNormandie(this, sableduBoulonnais, adConfig, str, soumaintrain);
        return io.presage.Goudaauxepices.CamembertauCalvados.a(camembertdeNormandie);
    }
}
