package io.presage;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;

public final class VacherinSuisse {
    private ActivityLifecycleCallbacks a;
    private final Application b;

    public static final class CamembertauCalvados extends GrandMunster {
        final /* synthetic */ VacherinSuisse a;
        final /* synthetic */ CamembertdeNormandie b;

        CamembertauCalvados(VacherinSuisse vacherinSuisse, CamembertdeNormandie camembertdeNormandie) {
            this.a = vacherinSuisse;
            this.b = camembertdeNormandie;
        }

        public final void onActivityPaused(Activity activity) {
            this.b.a();
        }

        public final void onActivityResumed(Activity activity) {
            this.a.a();
            activity.addContentView(this.b, this.b.getLayoutParams());
        }
    }

    public VacherinSuisse(Application application) {
        this.b = application;
    }

    public final void a(CamembertdeNormandie camembertdeNormandie) {
        this.a = new CamembertauCalvados(this, camembertdeNormandie);
        this.b.registerActivityLifecycleCallbacks(this.a);
    }

    public final void a() {
        this.b.unregisterActivityLifecycleCallbacks(this.a);
    }
}
