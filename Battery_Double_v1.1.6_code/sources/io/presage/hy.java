package io.presage;

public class hy implements hr, Iterable<Integer> {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final int b;
    private final int c;
    private final int d = 1;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public hy(int i, int i2) {
        this.b = i;
        this.c = fu.a(i, i2);
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public fm iterator() {
        return new hz(this.b, this.c, this.d);
    }

    public boolean d() {
        return this.d > 0 ? this.b > this.c : this.b < this.c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r2.d == r3.d) goto L_0x0027;
     */
    public boolean equals(Object obj) {
        if (obj instanceof hy) {
            if (!d() || !((hy) obj).d()) {
                hy hyVar = (hy) obj;
                if (this.b == hyVar.b) {
                    if (this.c == hyVar.c) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (d()) {
            return -1;
        }
        return (((this.b * 31) + this.c) * 31) + this.d;
    }

    public String toString() {
        StringBuilder sb;
        int i;
        if (this.d > 0) {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append("..");
            sb.append(this.c);
            sb.append(" step ");
            i = this.d;
        } else {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append(" downTo ");
            sb.append(this.c);
            sb.append(" step ");
            i = -this.d;
        }
        sb.append(i);
        return sb.toString();
    }
}
