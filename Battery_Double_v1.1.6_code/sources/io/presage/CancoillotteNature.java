package io.presage;

import android.graphics.Rect;
import android.view.ViewGroup;

public final class CancoillotteNature {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final int b;
    private final Camembertbio c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public /* synthetic */ CancoillotteNature() {
        this(Camembertbio.a);
    }

    private CancoillotteNature(Camembertbio camembertbio) {
        this.c = camembertbio;
        this.b = af.b(50);
    }

    public final boolean a(ViewGroup viewGroup, cu cuVar) {
        if (cuVar.c() < this.b || cuVar.d() < this.b) {
            return false;
        }
        Rect rect = new Rect();
        viewGroup.getWindowVisibleDisplayFrame(rect);
        Rect a2 = a(rect, cuVar);
        BriquetteduNord a3 = Camembertbio.a(a2, rect, 0.75f);
        float c2 = a3.c();
        if (c2 < 0.5f) {
            return false;
        }
        if (!cuVar.b() && c2 < 0.75f) {
            return false;
        }
        if (!cuVar.b() || c2 >= 0.75f) {
            return true;
        }
        if (!a3.a()) {
            return false;
        }
        a(cuVar, a2, rect);
        return true;
    }

    private static void a(cu cuVar, Rect rect, Rect rect2) {
        cuVar.c(rect.left - rect2.left);
        cuVar.d(rect.top - rect2.top);
        cuVar.a(rect.width());
        cuVar.b(rect.height());
    }

    private static Rect a(Rect rect, cu cuVar) {
        Rect rect2 = new Rect();
        rect2.left = rect.left + cuVar.e();
        rect2.top = rect.top + cuVar.f();
        rect2.right = rect2.left + cuVar.c();
        rect2.bottom = rect2.top + cuVar.d();
        return rect2;
    }
}
