package io.presage;

import android.webkit.WebView;
import com.iab.omid.library.oguryco.adsession.AdSessionConfiguration;
import com.iab.omid.library.oguryco.adsession.AdSessionContext;
import com.iab.omid.library.oguryco.adsession.CreativeType;
import com.iab.omid.library.oguryco.adsession.ImpressionType;
import com.iab.omid.library.oguryco.adsession.Owner;
import com.iab.omid.library.oguryco.adsession.Partner;

public final class dt {
    public static final dt a = new dt();

    private dt() {
    }

    public static du a(WebView webView, boolean z) {
        du duVar = new du();
        Partner a2 = a();
        if (a2 == null) {
            return null;
        }
        duVar.a(a(a2, webView));
        duVar.a(a(z));
        return duVar;
    }

    private static Partner a() {
        try {
            return Partner.createPartner("Ogury", "3.3.9-moat");
        } catch (IllegalArgumentException e) {
            dv dvVar = dv.a;
            dv.a(e);
            return null;
        }
    }

    private static AdSessionContext a(Partner partner, WebView webView) {
        try {
            return AdSessionContext.createHtmlAdSessionContext(partner, webView, "", null);
        } catch (IllegalArgumentException e) {
            dv dvVar = dv.a;
            dv.a(e);
            return null;
        }
    }

    private static AdSessionConfiguration a(boolean z) {
        ImpressionType impressionType = ImpressionType.DEFINED_BY_JAVASCRIPT;
        CreativeType creativeType = CreativeType.DEFINED_BY_JAVASCRIPT;
        Owner owner = Owner.JAVASCRIPT;
        Owner owner2 = Owner.NONE;
        if (z) {
            owner2 = Owner.JAVASCRIPT;
        }
        try {
            return AdSessionConfiguration.createAdSessionConfiguration(creativeType, impressionType, owner, owner2, false);
        } catch (IllegalArgumentException e) {
            dv dvVar = dv.a;
            dv.a(e);
            return null;
        }
    }
}
