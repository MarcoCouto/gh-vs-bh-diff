package io.presage;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import java.util.ArrayList;
import java.util.List;

public final class Echourgnacalaliqueurdenoix {
    public static final List<Fragment> a(FragmentManager fragmentManager) {
        List<Fragment> arrayList = new ArrayList<>();
        a(arrayList, fragmentManager);
        return arrayList;
    }

    private static final void a(List<Fragment> list, FragmentManager fragmentManager) {
        List<Fragment> fragments = fragmentManager.getFragments();
        hl.a((Object) fragments, "fm.fragments");
        for (Fragment fragment : fragments) {
            hl.a((Object) fragment, "it");
            if (fragment.getUserVisibleHint() && fragment.isResumed()) {
                list.add(fragment);
                FragmentManager childFragmentManager = fragment.getChildFragmentManager();
                hl.a((Object) childFragmentManager, "it.childFragmentManager");
                a(list, childFragmentManager);
            }
        }
    }
}
