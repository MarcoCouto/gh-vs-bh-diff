package io.presage;

import android.content.Context;
import io.presage.h.CamembertauCalvados;

public final class k {
    public static final k a = new k();
    private static j b;

    private k() {
    }

    public static void a(j jVar) {
        b = jVar;
    }

    public static j a(Context context) {
        if (b == null) {
            CamembertauCalvados camembertauCalvados = h.a;
            String d = CamembertauCalvados.a(context).d();
            l lVar = l.a;
            b = l.a(d);
        }
        return b;
    }

    public static void b(Context context) throws Salers {
        TommedAuvergne.a.a(context).b(false);
    }
}
