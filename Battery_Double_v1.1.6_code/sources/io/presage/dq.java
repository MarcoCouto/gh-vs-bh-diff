package io.presage;

public final class dq {
    private final String a;
    private final String b;
    private final String c;
    private final dk d;

    public dq(String str, String str2, String str3, dk dkVar) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = dkVar;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final dk d() {
        return this.d;
    }
}
