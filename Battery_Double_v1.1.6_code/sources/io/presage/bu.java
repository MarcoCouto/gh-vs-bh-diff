package io.presage;

import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.webkit.DownloadListener;
import android.widget.Toast;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public final class bu implements DownloadListener {
    private final Context a;

    public bu(Context context) {
        this.a = context;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        List list;
        String str5;
        if (ae.a(this.a, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            Uri parse = Uri.parse(str);
            hl.a((Object) parse, "uri");
            String path = parse.getPath();
            if (path != null) {
                list = new iu("/").b(path);
            } else {
                list = null;
            }
            if (list == null || !(!list.isEmpty())) {
                str5 = UUID.randomUUID().toString();
                hl.a((Object) str5, "UUID.randomUUID().toString()");
            } else {
                str5 = (String) ex.b(list);
            }
            Request request = new Request(parse);
            request.setTitle(str5);
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(1);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "download");
            Object systemService = this.a.getSystemService("download");
            if (systemService != null) {
                ((DownloadManager) systemService).enqueue(request);
                Context context = this.a;
                hp hpVar = hp.a;
                String format = String.format("Start downloading %s", Arrays.copyOf(new Object[]{str5}, 1));
                hl.a((Object) format, "java.lang.String.format(format, *args)");
                Toast.makeText(context, format, 0).show();
                return;
            }
            throw new em("null cannot be cast to non-null type android.app.DownloadManager");
        }
    }
}
