package io.presage;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import com.integralads.avid.library.inmobi.session.internal.InternalAvidAdSessionContext;
import io.presage.interstitial.ui.InterstitialActivity;
import java.util.List;

public final class aq {
    private CamembertdeNormandie a;
    private AbbayedeCiteauxentiere b;
    /* access modifiers changed from: private */
    public final InterstitialActivity c;
    private final Intent d;
    private final PontlEveque e;
    private final List<PontlEveque> f;
    private final ap g;
    private final Bouyssou h;

    public static final class CamembertauCalvados implements Bethmale {
        final /* synthetic */ aq a;
        final /* synthetic */ Bethmale b;

        CamembertauCalvados(aq aqVar, Bethmale bethmale) {
            this.a = aqVar;
            this.b = bethmale;
        }

        public final void a(CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            Bethmale bethmale = this.b;
            if (bethmale != null) {
                bethmale.a(camembertdeNormandie, abbayedeCiteauxentiere);
            }
            this.a.c.finish();
        }
    }

    public static final class CamembertdeNormandie implements Bethmale {
        final /* synthetic */ aq a;

        CamembertdeNormandie(aq aqVar) {
            this.a = aqVar;
        }

        public final void a(CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            this.a.c.a(abbayedeCiteauxentiere.d());
        }
    }

    private aq(InterstitialActivity interstitialActivity, Intent intent, PontlEveque pontlEveque, List<PontlEveque> list, ap apVar, Bouyssou bouyssou) {
        this.c = interstitialActivity;
        this.d = intent;
        this.e = pontlEveque;
        this.f = list;
        this.g = apVar;
        this.h = bouyssou;
        int intExtra = this.d.getIntExtra(InternalAvidAdSessionContext.CONTEXT_MODE, 0);
        switch (intExtra) {
            case 0:
                c();
                break;
            case 1:
                d();
                break;
            default:
                throw new IllegalStateException("Wrong mode ".concat(String.valueOf(intExtra)));
        }
        e();
        f();
    }

    public /* synthetic */ aq(InterstitialActivity interstitialActivity, Intent intent, PontlEveque pontlEveque, List list) {
        this(interstitialActivity, intent, pontlEveque, list, ap.a, Bouyssou.a);
    }

    public final CamembertdeNormandie a() {
        CamembertdeNormandie camembertdeNormandie = this.a;
        if (camembertdeNormandie == null) {
            hl.a("adLayout");
        }
        return camembertdeNormandie;
    }

    public final AbbayedeCiteauxentiere b() {
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = this.b;
        if (abbayedeCiteauxentiere == null) {
            hl.a("adController");
        }
        return abbayedeCiteauxentiere;
    }

    private final void c() {
        Context applicationContext = this.c.getApplicationContext();
        hl.a((Object) applicationContext, "activity.applicationContext");
        this.a = new CamembertdeNormandie(applicationContext);
        InterstitialActivity interstitialActivity = this.c;
        CamembertdeNormandie camembertdeNormandie = this.a;
        if (camembertdeNormandie == null) {
            hl.a("adLayout");
        }
        this.b = ap.a(interstitialActivity, camembertdeNormandie, new BleudAuvergnebio(InterstitialActivity.a));
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = this.b;
        if (abbayedeCiteauxentiere == null) {
            hl.a("adController");
        }
        abbayedeCiteauxentiere.a((Bofavre) new as());
        AbbayedeCiteauxentiere abbayedeCiteauxentiere2 = this.b;
        if (abbayedeCiteauxentiere2 == null) {
            hl.a("adController");
        }
        Application application = this.c.getApplication();
        hl.a((Object) application, "activity.application");
        abbayedeCiteauxentiere2.a((Bethmale) new BleudeLaqueuille(new VacherinSuisse(application), this.c, new BleudAuvergne()));
        AbbayedeCiteauxentiere abbayedeCiteauxentiere3 = this.b;
        if (abbayedeCiteauxentiere3 == null) {
            hl.a("adController");
        }
        abbayedeCiteauxentiere3.a(this.e, this.f);
    }

    private final void d() {
        VacherinSuisse vacherinSuisse;
        String stringExtra = this.d.getStringExtra("expand_cache_item_id");
        hl.a((Object) stringExtra, "expandCacheItemId");
        BoulettedAvesnes a2 = Bouyssou.a(stringExtra);
        if (a2 != null) {
            this.a = a2.a();
            this.b = a2.b();
            if (SaintNectaire.a(this.e)) {
                Application application = this.c.getApplication();
                hl.a((Object) application, "activity.application");
                vacherinSuisse = new VacherinSuisse(application);
            } else {
                vacherinSuisse = null;
            }
            AbbayedeCiteauxentiere abbayedeCiteauxentiere = this.b;
            if (abbayedeCiteauxentiere == null) {
                hl.a("adController");
            }
            Activity activity = this.c;
            AbbayedeCiteauxentiere abbayedeCiteauxentiere2 = this.b;
            if (abbayedeCiteauxentiere2 == null) {
                hl.a("adController");
            }
            abbayedeCiteauxentiere.a((Bethmale) new BleudeLaqueuille(vacherinSuisse, activity, abbayedeCiteauxentiere2.b()));
            return;
        }
        throw new IllegalStateException("Cache Item not found");
    }

    private final void e() {
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = this.b;
        if (abbayedeCiteauxentiere == null) {
            hl.a("adController");
        }
        abbayedeCiteauxentiere.c((Bethmale) new CamembertdeNormandie(this));
    }

    private final void f() {
        Bethmale bethmale;
        if (!SaintNectaire.a(this.e)) {
            AbbayedeCiteauxentiere abbayedeCiteauxentiere = this.b;
            if (abbayedeCiteauxentiere == null) {
                hl.a("adController");
            }
            bethmale = abbayedeCiteauxentiere.b();
        } else {
            bethmale = null;
        }
        AbbayedeCiteauxentiere abbayedeCiteauxentiere2 = this.b;
        if (abbayedeCiteauxentiere2 == null) {
            hl.a("adController");
        }
        abbayedeCiteauxentiere2.b((Bethmale) new CamembertauCalvados(this, bethmale));
    }
}
