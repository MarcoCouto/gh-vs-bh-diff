package io.presage;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import java.util.List;

public final class t {
    public static boolean a(Context context) {
        Object systemService = context.getSystemService("activity");
        if (systemService != null) {
            List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) systemService).getRunningAppProcesses();
            if (runningAppProcesses == null) {
                return false;
            }
            String packageName = context.getPackageName();
            for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.importance == 100 && hl.a((Object) runningAppProcessInfo.processName, (Object) packageName)) {
                    return false;
                }
            }
            return true;
        }
        throw new em("null cannot be cast to non-null type android.app.ActivityManager");
    }
}
