package io.presage;

public final class di extends dr {
    private final Throwable a;

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (io.presage.hl.a((java.lang.Object) r1.a, (java.lang.Object) ((io.presage.di) r2).a) != false) goto L_0x0015;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof di) {
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        Throwable th = this.a;
        if (th != null) {
            return th.hashCode();
        }
        return 0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ErrorResponse(exception=");
        sb.append(this.a);
        sb.append(")");
        return sb.toString();
    }

    public di(Throwable th) {
        super(0);
        this.a = th;
    }

    public final Throwable a() {
        return this.a;
    }
}
