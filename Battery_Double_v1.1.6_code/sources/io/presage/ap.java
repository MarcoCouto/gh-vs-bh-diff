package io.presage;

import android.app.Application;
import io.presage.AbbayedeCiteauxentiere.CamembertauCalvados;
import io.presage.interstitial.ui.InterstitialActivity;

public final class ap {
    public static final ap a = new ap();

    private ap() {
    }

    public static AbbayedeCiteauxentiere a(InterstitialActivity interstitialActivity, CamembertdeNormandie camembertdeNormandie, BleudAuvergnebio bleudAuvergnebio) {
        Application application = interstitialActivity.getApplication();
        hl.a((Object) application, "activity.application");
        return new CamembertauCalvados(application, camembertdeNormandie, bleudAuvergnebio, true).l();
    }
}
