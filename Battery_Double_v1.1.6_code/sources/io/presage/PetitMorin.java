package io.presage;

import io.fabric.sdk.android.services.network.HttpRequest;
import io.presage.common.AdConfig;
import org.json.JSONObject;

public final class PetitMorin implements PavedAremberg {
    private final ParmigianoReggiano a;
    private final TetedeMoine b;
    private final Taleggio c;
    /* access modifiers changed from: private */
    public final Cdo d;
    private final PasdelEscalette e;
    private final k f;
    private final SaintPaulin g;

    static final class AbbayedeTamie extends hm implements gf<ep> {
        final /* synthetic */ PetitMorin a;
        final /* synthetic */ dq b;

        AbbayedeTamie(PetitMorin petitMorin, dq dqVar) {
            this.a = petitMorin;
            this.b = dqVar;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            dr a2 = this.a.d.a(this.b).a();
            if (a2 instanceof di) {
                throw ((di) a2).a();
            }
        }
    }

    static final class AbbayedeTimadeuc extends hm implements gf<ep> {
        final /* synthetic */ PetitMorin a;
        final /* synthetic */ dq b;

        AbbayedeTimadeuc(PetitMorin petitMorin, dq dqVar) {
            this.a = petitMorin;
            this.b = dqVar;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            dr a2 = this.a.d.a(this.b).a();
            if (a2 instanceof di) {
                throw ((di) a2).a();
            }
        }
    }

    static final class AbbayeduMontdesCats extends hm implements gf<ep> {
        final /* synthetic */ PetitMorin a;
        final /* synthetic */ dq b;

        AbbayeduMontdesCats(PetitMorin petitMorin, dq dqVar) {
            this.a = petitMorin;
            this.b = dqVar;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            dr a2 = this.a.d.a(this.b).a();
            if (a2 instanceof di) {
                throw ((di) a2).a();
            }
        }
    }

    static final /* synthetic */ class CamembertauCalvados extends hk implements gg<Throwable, ep> {
        CamembertauCalvados(ac acVar) {
            super(1, acVar);
        }

        public final ig b() {
            return hn.a(ac.class);
        }

        public final String c() {
            return "e";
        }

        public final String d() {
            return "e(Ljava/lang/Throwable;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            return ep.a;
        }
    }

    static final class CamembertdeNormandie extends hm implements gf<ep> {
        public static final CamembertdeNormandie a = new CamembertdeNormandie();

        CamembertdeNormandie() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a() {
            return ep.a;
        }
    }

    static final class EcirdelAubrac extends hm implements gf<ep> {
        final /* synthetic */ PetitMorin a;
        final /* synthetic */ dq b;

        EcirdelAubrac(PetitMorin petitMorin, dq dqVar) {
            this.a = petitMorin;
            this.b = dqVar;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            dr a2 = this.a.d.a(this.b).a();
            if (a2 instanceof di) {
                throw ((di) a2).a();
            }
        }
    }

    private PetitMorin(ParmigianoReggiano parmigianoReggiano, TetedeMoine tetedeMoine, Taleggio taleggio, Cdo doVar, PasdelEscalette pasdelEscalette, k kVar, SaintPaulin saintPaulin) {
        this.a = parmigianoReggiano;
        this.b = tetedeMoine;
        this.c = taleggio;
        this.d = doVar;
        this.e = pasdelEscalette;
        this.f = kVar;
        this.g = saintPaulin;
    }

    public /* synthetic */ PetitMorin(ParmigianoReggiano parmigianoReggiano, TetedeMoine tetedeMoine, Taleggio taleggio, Cdo doVar) {
        this(parmigianoReggiano, tetedeMoine, taleggio, doVar, new PasdelEscalette(), k.a, SaintPaulin.a);
    }

    public final dr a(SableduBoulonnais sableduBoulonnais, AdConfig adConfig, String str, Soumaintrain soumaintrain) {
        PlaisirauChablis plaisirauChablis = PlaisirauChablis.a;
        String a2 = PlaisirauChablis.a();
        RaclettedeSavoie raclettedeSavoie = new RaclettedeSavoie(sableduBoulonnais.c(), adConfig != null ? adConfig.getAdUnitId() : null);
        RomansPartDieu romansPartDieu = new RomansPartDieu(this.b, this.c, this.g);
        Reblochon reblochon = Reblochon.a;
        return this.d.a(new dq(a2, HttpRequest.METHOD_POST, Reblochon.a(romansPartDieu, raclettedeSavoie, str, soumaintrain), this.a.c())).a();
    }

    public final dr c(String str) {
        return this.d.a(new dq(str, HttpRequest.METHOD_GET, "", this.a.b())).a();
    }

    public final dr a(JSONObject jSONObject) {
        boolean a2 = PasdelEscalette.a(jSONObject);
        PlaisirauChablis plaisirauChablis = PlaisirauChablis.a;
        String a3 = PlaisirauChablis.a(a2);
        String str = HttpRequest.METHOD_POST;
        String jSONObject2 = jSONObject.toString();
        hl.a((Object) jSONObject2, "requestBody.toString()");
        return this.d.a(new dq(a3, str, jSONObject2, this.a.b())).a();
    }

    public final FourmedeMontbrison a(String str) {
        PlaisirauChablis plaisirauChablis = PlaisirauChablis.a;
        dq dqVar = new dq(PlaisirauChablis.b(), HttpRequest.METHOD_POST, str, this.a.b());
        io.presage.FourmedeMontbrison.CamembertauCalvados camembertauCalvados = FourmedeMontbrison.a;
        return io.presage.FourmedeMontbrison.CamembertauCalvados.a(new AbbayedeTamie(this, dqVar));
    }

    public final FourmedeMontbrison b(JSONObject jSONObject) {
        PlaisirauChablis plaisirauChablis = PlaisirauChablis.a;
        String c2 = PlaisirauChablis.c();
        String str = HttpRequest.METHOD_POST;
        String jSONObject2 = jSONObject.toString();
        hl.a((Object) jSONObject2, "requestBody.toString()");
        dq dqVar = new dq(c2, str, jSONObject2, this.a.a());
        io.presage.FourmedeMontbrison.CamembertauCalvados camembertauCalvados = FourmedeMontbrison.a;
        return io.presage.FourmedeMontbrison.CamembertauCalvados.a(new AbbayeduMontdesCats(this, dqVar));
    }

    public final void b(String str) {
        dq dqVar = new dq(str, HttpRequest.METHOD_GET, "", this.a.a());
        io.presage.FourmedeMontbrison.CamembertauCalvados camembertauCalvados = FourmedeMontbrison.a;
        io.presage.FourmedeMontbrison.CamembertauCalvados.a(new EcirdelAubrac(this, dqVar)).a((gg<? super Throwable, ep>) new CamembertauCalvados<Object,ep>(ac.a)).a((gf<ep>) CamembertdeNormandie.a);
    }

    public final FourmedeMontbrison c(JSONObject jSONObject) {
        PlaisirauChablis plaisirauChablis = PlaisirauChablis.a;
        String d2 = PlaisirauChablis.d();
        String str = HttpRequest.METHOD_POST;
        String jSONObject2 = jSONObject.toString();
        hl.a((Object) jSONObject2, "requestBody.toString()");
        dq dqVar = new dq(d2, str, jSONObject2, this.a.a());
        io.presage.FourmedeMontbrison.CamembertauCalvados camembertauCalvados = FourmedeMontbrison.a;
        return io.presage.FourmedeMontbrison.CamembertauCalvados.a(new AbbayedeTimadeuc(this, dqVar));
    }
}
