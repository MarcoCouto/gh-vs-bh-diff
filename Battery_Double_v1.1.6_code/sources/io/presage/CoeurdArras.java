package io.presage;

public interface CoeurdArras {
    void injectInitialOverlay();

    void registerLifecycleListener();

    void unregisterLifecycleListener();
}
