package io.presage;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import java.util.ArrayList;
import java.util.List;

public final class AbbayedeCiteauxentiere implements ct {
    public static final CamembertdeNormandie a = new CamembertdeNormandie(0);
    private OnLayoutChangeListener A;
    private int B;
    private Bethmale C;
    private Bofavre D;
    private Bethmale E;
    private Bethmale F;
    private final Application b;
    private final io.presage.bj.CamembertauCalvados c;
    private final io.presage.Abondance.CamembertauCalvados d;
    private final k e;
    private final Beaufort f;
    private final PersilledumontBlanc g;
    private final VacherinduhautDoubsMontdOr h;
    private final bb i;
    private final CamembertdeNormandie j;
    private final Bethmale k;
    private final CancoillotteNature l;
    private boolean m;
    /* access modifiers changed from: private */
    public final BrieauPoivre n;
    private final Taleggio o;
    private final Aveyronnais p;
    /* access modifiers changed from: private */
    public db q;
    private Abondance r;
    private ax s;
    private boolean t;
    private boolean u;
    private bj v;
    private PontlEveque w;
    private List<PontlEveque> x;
    private BurratadesPouilles y;
    private cu z;

    static final /* synthetic */ class AbbayedeTamie extends hk implements gf<ep> {
        AbbayedeTamie(AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            super(0, abbayedeCiteauxentiere);
        }

        public final ig b() {
            return hn.a(AbbayedeCiteauxentiere.class);
        }

        public final String c() {
            return "handleNewOguryBrowserWebViewCreated";
        }

        public final String d() {
            return "handleNewOguryBrowserWebViewCreated()V";
        }

        public final /* synthetic */ Object a() {
            h();
            return ep.a;
        }

        private void h() {
            ((AbbayedeCiteauxentiere) this.a).A();
        }
    }

    static final /* synthetic */ class AbbayedeTimadeuc extends hk implements gf<ep> {
        AbbayedeTimadeuc(AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            super(0, abbayedeCiteauxentiere);
        }

        public final ig b() {
            return hn.a(AbbayedeCiteauxentiere.class);
        }

        public final String c() {
            return "closeAd";
        }

        public final String d() {
            return "closeAd()V";
        }

        public final /* synthetic */ Object a() {
            h();
            return ep.a;
        }

        private void h() {
            ((AbbayedeCiteauxentiere) this.a).u();
        }
    }

    static final /* synthetic */ class AbbayeduMontdesCats extends hk implements gf<ep> {
        AbbayeduMontdesCats(AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            super(0, abbayedeCiteauxentiere);
        }

        public final ig b() {
            return hn.a(AbbayedeCiteauxentiere.class);
        }

        public final String c() {
            return "resumeAd";
        }

        public final String d() {
            return "resumeAd()V";
        }

        public final /* synthetic */ Object a() {
            h();
            return ep.a;
        }

        private void h() {
            ((AbbayedeCiteauxentiere) this.a).m();
        }
    }

    static final /* synthetic */ class AffideliceauChablis extends hk implements gf<ep> {
        AffideliceauChablis(AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            super(0, abbayedeCiteauxentiere);
        }

        public final ig b() {
            return hn.a(AbbayedeCiteauxentiere.class);
        }

        public final String c() {
            return "pauseAd";
        }

        public final String d() {
            return "pauseAd()V";
        }

        public final /* synthetic */ Object a() {
            h();
            return ep.a;
        }

        private void h() {
            ((AbbayedeCiteauxentiere) this.a).l();
        }
    }

    static final class Appenzeller extends hm implements gg<CamembertdeNormandie, ep> {
        final /* synthetic */ AbbayedeCiteauxentiere a;

        Appenzeller(AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            this.a = abbayedeCiteauxentiere;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((CamembertdeNormandie) obj);
            return ep.a;
        }

        private void a(CamembertdeNormandie camembertdeNormandie) {
            AbbayedeCiteauxentiere.b(this.a).b(af.a(camembertdeNormandie.getWidth()), af.a(camembertdeNormandie.getHeight()), af.a(camembertdeNormandie.getX()), af.a(camembertdeNormandie.getY()));
            this.a.n.a();
            this.a.w();
        }
    }

    public static final class CamembertauCalvados {
        private io.presage.bj.CamembertauCalvados a = bj.a;
        private io.presage.Abondance.CamembertauCalvados b = Abondance.a;
        private k c = k.a;
        private Beaufort d;
        private PersilledumontBlanc e;
        private VacherinduhautDoubsMontdOr f;
        private bb g;
        private VacherinSuisse h;
        private CancoillotteNature i;
        private BrieauPoivre j;
        private Taleggio k;
        private Aveyronnais l;
        private final Application m;
        private final CamembertdeNormandie n;
        private final Bethmale o;
        private final boolean p;

        public CamembertauCalvados(Application application, CamembertdeNormandie camembertdeNormandie, Bethmale bethmale, boolean z) {
            this.m = application;
            this.n = camembertdeNormandie;
            this.o = bethmale;
            this.p = z;
            io.presage.Beaufort.CamembertauCalvados camembertauCalvados = Beaufort.a;
            this.d = io.presage.Beaufort.CamembertauCalvados.a();
            this.e = PersilledumontBlanc.a;
            this.f = VacherinduhautDoubsMontdOr.a;
            this.g = bb.a;
            this.h = new VacherinSuisse(this.m);
            this.i = new CancoillotteNature();
            this.j = new BrieauPoivre(this.n);
            this.k = new Taleggio(this.m);
            this.l = new Aveyronnais(this.m);
        }

        public final Application m() {
            return this.m;
        }

        public final CamembertdeNormandie n() {
            return this.n;
        }

        public final Bethmale o() {
            return this.o;
        }

        public final boolean p() {
            return this.p;
        }

        public final io.presage.bj.CamembertauCalvados a() {
            return this.a;
        }

        public final io.presage.Abondance.CamembertauCalvados b() {
            return this.b;
        }

        public final k c() {
            return this.c;
        }

        public final Beaufort d() {
            return this.d;
        }

        public final PersilledumontBlanc e() {
            return this.e;
        }

        public final VacherinduhautDoubsMontdOr f() {
            return this.f;
        }

        public final bb g() {
            return this.g;
        }

        public final CancoillotteNature h() {
            return this.i;
        }

        public final BrieauPoivre i() {
            return this.j;
        }

        public final Taleggio j() {
            return this.k;
        }

        public final Aveyronnais k() {
            return this.l;
        }

        public final AbbayedeCiteauxentiere l() {
            return new AbbayedeCiteauxentiere(this, 0);
        }
    }

    public static final class CamembertdeNormandie {
        private CamembertdeNormandie() {
        }

        public /* synthetic */ CamembertdeNormandie(byte b) {
            this();
        }
    }

    static final class EcirdelAubrac implements OnLayoutChangeListener {
        final /* synthetic */ AbbayedeCiteauxentiere a;

        EcirdelAubrac(AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
            this.a = abbayedeCiteauxentiere;
        }

        public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            if (this.a.q != null && (!hl.a((Object) AbbayedeCiteauxentiere.e(this.a).getAdState(), (Object) "hidden")) && an.d(AbbayedeCiteauxentiere.e(this.a))) {
                this.a.n.a();
            }
        }
    }

    private AbbayedeCiteauxentiere(CamembertauCalvados camembertauCalvados) {
        this.b = camembertauCalvados.m();
        this.c = camembertauCalvados.a();
        this.d = camembertauCalvados.b();
        this.e = camembertauCalvados.c();
        this.f = camembertauCalvados.d();
        this.g = camembertauCalvados.e();
        this.h = camembertauCalvados.f();
        this.i = camembertauCalvados.g();
        this.j = camembertauCalvados.n();
        this.k = camembertauCalvados.o();
        this.l = camembertauCalvados.h();
        this.m = camembertauCalvados.p();
        this.n = camembertauCalvados.i();
        this.o = camembertauCalvados.j();
        this.p = camembertauCalvados.k();
        this.u = true;
        this.x = new ArrayList();
        this.z = new cu(0);
        this.A = x();
        this.B = 1;
        this.C = BleudeSassenage.a;
        this.E = BleudeSassenage.a;
        this.F = BleudeSassenage.a;
    }

    public /* synthetic */ AbbayedeCiteauxentiere(CamembertauCalvados camembertauCalvados, byte b2) {
        this(camembertauCalvados);
    }

    public static final /* synthetic */ ax b(AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
        ax axVar = abbayedeCiteauxentiere.s;
        if (axVar == null) {
            hl.a("mraidCommandExecutor");
        }
        return axVar;
    }

    public static final /* synthetic */ db e(AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
        db dbVar = abbayedeCiteauxentiere.q;
        if (dbVar == null) {
            hl.a("webView");
        }
        return dbVar;
    }

    public final Application a() {
        return this.b;
    }

    private final void a(int i2) {
        if (this.B != 4) {
            this.B = i2;
        }
    }

    public final void a(Bethmale bethmale) {
        this.C = bethmale;
    }

    public final void a(Bofavre bofavre) {
        this.D = bofavre;
    }

    public final Bethmale b() {
        return this.E;
    }

    public final void b(Bethmale bethmale) {
        this.E = bethmale;
    }

    public final void c(Bethmale bethmale) {
        this.F = bethmale;
    }

    public final List<PontlEveque> c() {
        return this.x;
    }

    public final PontlEveque d() {
        return this.w;
    }

    public final void e() {
        a(2);
    }

    public final void f() {
        a(3);
    }

    public final boolean g() {
        if (this.B != 3) {
            db dbVar = this.q;
            if (dbVar == null) {
                hl.a("webView");
            }
            if (!hl.a((Object) dbVar.getAdState(), (Object) "expanded")) {
                return true;
            }
        }
        return false;
    }

    public final void a(PontlEveque pontlEveque, List<PontlEveque> list) {
        this.x = list;
        this.w = pontlEveque;
        E();
        a(this.j);
        cq cqVar = new cq(this.b, this, null);
        bj a2 = io.presage.bj.CamembertauCalvados.a(this.b, pontlEveque, this.j, cqVar);
        this.v = a2;
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = this;
        a2.a((gf<ep>) new AbbayedeTamie<ep>(abbayedeCiteauxentiere));
        a2.b((gf<ep>) new AbbayedeTimadeuc<ep>(abbayedeCiteauxentiere));
        this.r = io.presage.Abondance.CamembertauCalvados.a(a2, cqVar);
        Abondance abondance = this.r;
        if (abondance == null) {
            hl.a("webViewGateway");
        }
        db a3 = abondance.a(pontlEveque);
        if (a3 != null) {
            this.q = a3;
            this.s = a3.getMraidCommandExecutor();
            a2.a(pontlEveque.j().length() > 0 ? pontlEveque.j() : "controller", a3, pontlEveque.w());
            j a4 = k.a((Context) this.b);
            if (a4 != null) {
                a(a4);
                a(a3);
                this.j.addView(a3, new LayoutParams(-1, -1));
                if (!this.m) {
                    a(pontlEveque);
                }
                this.f.a(a4, pontlEveque, a3, this.b);
                v();
                this.j.setOnWindowGainFocusListener(new AbbayeduMontdesCats(abbayedeCiteauxentiere));
                this.j.setOnWindowLoseFocusListener(new AffideliceauChablis(abbayedeCiteauxentiere));
                return;
            }
            throw new IllegalStateException("Profig must not be null");
        }
        throw new IllegalStateException("WebView must not be null");
    }

    private final void v() {
        this.j.setAdLayoutChangeListener(new Appenzeller(this));
    }

    /* access modifiers changed from: private */
    public final void w() {
        db dbVar = this.q;
        if (dbVar == null) {
            hl.a("webView");
        }
        Rect a2 = Taleggio.a((View) dbVar);
        ax axVar = this.s;
        if (axVar == null) {
            hl.a("mraidCommandExecutor");
        }
        axVar.b(af.a(a2.width()), af.a(a2.height()));
    }

    private final OnLayoutChangeListener x() {
        return new EcirdelAubrac(this);
    }

    private final void y() {
        ViewGroup parentAsViewGroup = this.j.getParentAsViewGroup();
        if (parentAsViewGroup != null) {
            parentAsViewGroup.addOnLayoutChangeListener(this.A);
        }
    }

    private final void z() {
        ViewGroup parentAsViewGroup = this.j.getParentAsViewGroup();
        if (parentAsViewGroup != null) {
            parentAsViewGroup.removeOnLayoutChangeListener(this.A);
        }
    }

    private final void a(PontlEveque pontlEveque) {
        this.z.a(pontlEveque.l().b());
        this.z.b(pontlEveque.l().c());
        this.j.setInitialSize(this.z);
        this.j.setEnableDrag(pontlEveque.l().a());
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0044  */
    public final void A() {
        boolean z2;
        boolean z3;
        db dbVar;
        if (this.m) {
            db dbVar2 = this.q;
            if (dbVar2 == null) {
                hl.a("webView");
            }
            if (!hl.a((Object) dbVar2.getAdState(), (Object) "default")) {
                z2 = true;
                if (!this.m) {
                    db dbVar3 = this.q;
                    if (dbVar3 == null) {
                        hl.a("webView");
                    }
                    if (!hl.a((Object) dbVar3.getAdState(), (Object) "expanded")) {
                        z3 = true;
                        if (z2 || z3) {
                            dbVar = this.q;
                            if (dbVar == null) {
                                hl.a("webView");
                            }
                            dbVar.setMultiBrowserNotOpened(false);
                            l();
                            B();
                        }
                        this.m = true;
                    }
                }
                z3 = false;
                dbVar = this.q;
                if (dbVar == null) {
                }
                dbVar.setMultiBrowserNotOpened(false);
                l();
                B();
                this.m = true;
            }
        }
        z2 = false;
        if (!this.m) {
        }
        z3 = false;
        dbVar = this.q;
        if (dbVar == null) {
        }
        dbVar.setMultiBrowserNotOpened(false);
        l();
        B();
        this.m = true;
    }

    private final void B() {
        q();
    }

    private final void a(j jVar) {
        this.t = jVar.i();
        this.u = jVar.j();
        BurratadesPouilles burratadesPouilles = this.y;
        if (burratadesPouilles != null) {
            burratadesPouilles.a(jVar.m());
        }
    }

    private final void a(db dbVar) {
        if (!dbVar.getShowSdkCloseButton()) {
            BurratadesPouilles burratadesPouilles = this.y;
            if (burratadesPouilles != null) {
                burratadesPouilles.b();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0008, code lost:
        if (r0 == null) goto L_0x000a;
     */
    @SuppressLint({"RtlHardcoded"})
    private final void a(CamembertdeNormandie camembertdeNormandie) {
        String str;
        PontlEveque pontlEveque = this.w;
        if (pontlEveque != null) {
            str = pontlEveque.r();
        }
        str = "";
        this.y = VacherinduhautDoubsMontdOr.a(this, camembertdeNormandie, PersilledumontBlanc.a(this.b), str);
    }

    private final boolean C() {
        bj bjVar = this.v;
        if (bjVar != null) {
            return bjVar.b();
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r0 == null) goto L_0x002a;
     */
    public final void h() {
        String str;
        if (this.B != 4) {
            ac.a("destroying ad");
            a(4);
            this.n.b();
            bj bjVar = this.v;
            if (bjVar != null) {
                bjVar.d();
            }
            BurratadesPouilles burratadesPouilles = this.y;
            if (burratadesPouilles != null) {
                burratadesPouilles.c();
            }
            PontlEveque pontlEveque = this.w;
            if (pontlEveque != null) {
                str = pontlEveque.b();
            }
            str = "";
            bb.a(new ba(str, "adClosed"));
            bb.a(str);
            this.f.a();
            this.j.e();
            this.C = BleudeSassenage.a;
            if (this.q != null) {
                db dbVar = this.q;
                if (dbVar == null) {
                    hl.a("webView");
                }
                dbVar.h();
            }
        }
    }

    public final void i() {
        if (D() && C() && this.u) {
            h();
            this.E.a(this.j, this);
        }
    }

    public final void j() {
        if (D()) {
            h();
        }
    }

    private final boolean D() {
        return this.m && this.B != 2;
    }

    public final boolean k() {
        bj bjVar = this.v;
        if (bjVar != null) {
            bjVar.c();
        }
        return this.t;
    }

    public final void l() {
        db dbVar = this.q;
        if (dbVar == null) {
            hl.a("webView");
        }
        if (!dbVar.a()) {
            ac.a("ad already paused");
            return;
        }
        ac.a("pauseAd");
        db dbVar2 = this.q;
        if (dbVar2 == null) {
            hl.a("webView");
        }
        dbVar2.setResumed(false);
        z();
        av avVar = new av();
        avVar.a(0.0f);
        ax axVar = this.s;
        if (axVar == null) {
            hl.a("mraidCommandExecutor");
        }
        axVar.a(false);
        ax axVar2 = this.s;
        if (axVar2 == null) {
            hl.a("mraidCommandExecutor");
        }
        axVar2.a(avVar);
    }

    public final void m() {
        db dbVar = this.q;
        if (dbVar == null) {
            hl.a("webView");
        }
        if (dbVar.a()) {
            ac.a("ad already resumed");
            return;
        }
        ac.a("resumeAd");
        db dbVar2 = this.q;
        if (dbVar2 == null) {
            hl.a("webView");
        }
        dbVar2.setResumed(true);
        this.p.a(this.w);
        if (this.m) {
            y();
        }
        a(1);
        db dbVar3 = this.q;
        if (dbVar3 == null) {
            hl.a("webView");
        }
        if (dbVar3.b()) {
            ax axVar = this.s;
            if (axVar == null) {
                hl.a("mraidCommandExecutor");
            }
            axVar.a(true);
        }
        this.n.a();
    }

    public final void a(int i2, int i3) {
        this.z.c(i2);
        this.z.d(i3);
    }

    private final void E() {
        this.F.a(this.j, this);
    }

    public final void n() {
        BurratadesPouilles burratadesPouilles = this.y;
        if (burratadesPouilles != null) {
            burratadesPouilles.a();
        }
    }

    public final void o() {
        BurratadesPouilles burratadesPouilles = this.y;
        if (burratadesPouilles != null) {
            burratadesPouilles.b();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (io.presage.hl.a((java.lang.Object) r0.getAdState(), (java.lang.Object) "hidden") != false) goto L_0x0019;
     */
    public final void a(String str) {
        if (!this.m) {
            db dbVar = this.q;
            if (dbVar == null) {
                hl.a("webView");
            }
        }
        Bofavre bofavre = this.D;
        if (bofavre != null) {
            bofavre.a(this.b, this.x, str);
        }
    }

    public final void p() {
        t();
    }

    public final void a(cu cuVar) {
        this.j.setResizeProps(cuVar);
    }

    public final void q() {
        if (!r()) {
            this.k.a(this.j, this);
            b(this.m ? "default" : "expanded");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
        if (io.presage.hl.a((java.lang.Object) r0.getAdState(), (java.lang.Object) "default") != false) goto L_0x0031;
     */
    public final boolean r() {
        db dbVar = this.q;
        if (dbVar == null) {
            hl.a("webView");
        }
        if (!hl.a((Object) dbVar.getAdState(), (Object) "expanded")) {
            if (this.m) {
                db dbVar2 = this.q;
                if (dbVar2 == null) {
                    hl.a("webView");
                }
            }
            return false;
        }
        return true;
    }

    public final void s() {
        cu resizeProps = this.j.getResizeProps();
        if (resizeProps == null) {
            throw new IllegalStateException("setResizeProperties must be called first");
        } else if (this.l.a((ViewGroup) this.j, resizeProps)) {
            this.C.a(this.j, this);
            b("resized");
        } else {
            throw new IllegalArgumentException("Invalid resize command".toString());
        }
    }

    public final void t() {
        db dbVar = this.q;
        if (dbVar == null) {
            hl.a("webView");
        }
        if (hl.a((Object) dbVar.getAdState(), (Object) "default") || this.m) {
            this.E.a(this.j, this);
            return;
        }
        F();
        b("default");
    }

    public final void u() {
        this.E.a(this.j, this);
    }

    private final void b(String str) {
        ax axVar = this.s;
        if (axVar == null) {
            hl.a("mraidCommandExecutor");
        }
        axVar.b(str);
    }

    private final void F() {
        this.j.d();
        this.C.a(this.j, this);
    }
}
