package io.presage;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import java.lang.ref.WeakReference;

public final class aj {
    public static final aj a = new aj();
    /* access modifiers changed from: private */
    public static WeakReference<Activity> b = new WeakReference<>(null);
    private static boolean c;

    public static final class CamembertauCalvados extends GrandMunster {
        CamembertauCalvados() {
        }

        public final void onActivityResumed(Activity activity) {
            aj ajVar = aj.a;
            aj.b = new WeakReference(activity);
        }
    }

    private aj() {
    }

    public static Activity a() {
        return (Activity) b.get();
    }

    public final synchronized void a(Context context) {
        if (!c) {
            Context applicationContext = context.getApplicationContext();
            if (!(applicationContext instanceof Application)) {
                applicationContext = null;
            }
            Application application = (Application) applicationContext;
            if (application != null) {
                c = true;
                application.registerActivityLifecycleCallbacks(new CamembertauCalvados());
            }
        }
    }
}
