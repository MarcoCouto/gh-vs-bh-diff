package io.presage;

import android.content.res.Resources;

public final class w {
    public static final w a = new w();

    private w() {
    }

    public static float a() {
        Resources system = Resources.getSystem();
        hl.a((Object) system, "Resources.getSystem()");
        return system.getDisplayMetrics().density;
    }
}
