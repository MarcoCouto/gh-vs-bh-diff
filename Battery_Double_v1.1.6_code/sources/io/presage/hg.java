package io.presage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class hg implements hf, Cif<Object> {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private static final Map<Class<? extends Object<?>>, Integer> c;
    private static final HashMap<String, String> d;
    private static final HashMap<String, String> e;
    private static final HashMap<String, String> f;
    private static final Map<String, String> g;
    private final Class<?> b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public hg(Class<?> cls) {
        this.b = cls;
    }

    public final Class<?> a() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof hg) && hl.a((Object) ge.a(this), (Object) ge.a((Cif) obj));
    }

    public final int hashCode() {
        return ge.a(this).hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a().toString());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }

    static {
        int i = 0;
        Iterable b2 = ex.b(gf.class, gg.class, gr.class, gv.class, gw.class, gx.class, gy.class, gz.class, ha.class, hb.class, gh.class, gi.class, gj.class, gk.class, gl.class, gm.class, gn.class, go.class, gp.class, gq.class, gs.class, gt.class, gu.class);
        Collection arrayList = new ArrayList(ex.a(b2));
        for (Object next : b2) {
            int i2 = i + 1;
            if (i < 0) {
                ex.b();
            }
            arrayList.add(el.a((Class) next, Integer.valueOf(i)));
            i = i2;
        }
        c = fn.a((List) arrayList);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("boolean", "kotlin.Boolean");
        hashMap.put("char", "kotlin.Char");
        hashMap.put("byte", "kotlin.Byte");
        hashMap.put("short", "kotlin.Short");
        hashMap.put("int", "kotlin.Int");
        hashMap.put("float", "kotlin.Float");
        hashMap.put("long", "kotlin.Long");
        hashMap.put("double", "kotlin.Double");
        d = hashMap;
        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put("java.lang.Boolean", "kotlin.Boolean");
        hashMap2.put("java.lang.Character", "kotlin.Char");
        hashMap2.put("java.lang.Byte", "kotlin.Byte");
        hashMap2.put("java.lang.Short", "kotlin.Short");
        hashMap2.put("java.lang.Integer", "kotlin.Int");
        hashMap2.put("java.lang.Float", "kotlin.Float");
        hashMap2.put("java.lang.Long", "kotlin.Long");
        hashMap2.put("java.lang.Double", "kotlin.Double");
        e = hashMap2;
        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put("java.lang.Object", "kotlin.Any");
        hashMap3.put("java.lang.String", "kotlin.String");
        hashMap3.put("java.lang.CharSequence", "kotlin.CharSequence");
        hashMap3.put("java.lang.Throwable", "kotlin.Throwable");
        hashMap3.put("java.lang.Cloneable", "kotlin.Cloneable");
        hashMap3.put("java.lang.Number", "kotlin.Number");
        hashMap3.put("java.lang.Comparable", "kotlin.Comparable");
        hashMap3.put("java.lang.Enum", "kotlin.Enum");
        hashMap3.put("java.lang.annotation.Annotation", "kotlin.Annotation");
        hashMap3.put("java.lang.Iterable", "kotlin.collections.Iterable");
        hashMap3.put("java.util.Iterator", "kotlin.collections.Iterator");
        hashMap3.put("java.util.Collection", "kotlin.collections.Collection");
        hashMap3.put("java.util.List", "kotlin.collections.List");
        hashMap3.put("java.util.Set", "kotlin.collections.Set");
        hashMap3.put("java.util.ListIterator", "kotlin.collections.ListIterator");
        hashMap3.put("java.util.Map", "kotlin.collections.Map");
        hashMap3.put("java.util.Map$Entry", "kotlin.collections.Map.Entry");
        hashMap3.put("kotlin.jvm.internal.StringCompanionObject", "kotlin.String.Companion");
        hashMap3.put("kotlin.jvm.internal.EnumCompanionObject", "kotlin.Enum.Companion");
        hashMap3.putAll(d);
        hashMap3.putAll(e);
        Collection<String> values = d.values();
        hl.a((Object) values, "primitiveFqNames.values");
        for (String str : values) {
            Map map = hashMap3;
            StringBuilder sb = new StringBuilder("kotlin.jvm.internal.");
            hl.a((Object) str, "kotlinName");
            sb.append(iv.a(str));
            sb.append("CompanionObject");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(".Companion");
            ej a2 = el.a(sb2, sb3.toString());
            map.put(a2.a(), a2.b());
        }
        for (Entry entry : c.entrySet()) {
            hashMap3.put(((Class) entry.getKey()).getName(), "kotlin.Function".concat(String.valueOf(((Number) entry.getValue()).intValue())));
        }
        f = hashMap3;
        Map map2 = hashMap3;
        Map<String, String> linkedHashMap = new LinkedHashMap<>(fn.a(map2.size()));
        for (Entry entry2 : map2.entrySet()) {
            linkedHashMap.put(entry2.getKey(), iv.a((String) entry2.getValue()));
        }
        g = linkedHashMap;
    }
}
