package io.presage;

public final class SaintPaulin {
    public static final SaintPaulin a = new SaintPaulin();

    private SaintPaulin() {
    }

    public static boolean a() {
        try {
            Class.forName("com.moat.analytics.mobile.ogury.MoatAnalytics");
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean b() {
        try {
            Class.forName("com.iab.omid.library.oguryco.Omid");
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
