package io.presage;

import java.util.List;

public final class EtivazGruyereSuisse {
    private List<String> a = ex.a();
    private List<? extends Class<?>> b = ex.a();

    public final List<String> a() {
        return this.a;
    }

    public final void a(List<String> list) {
        this.a = list;
    }

    public final List<Class<?>> b() {
        return this.b;
    }

    public final void b(List<? extends Class<?>> list) {
        this.b = list;
    }

    public final boolean c() {
        return this.a.isEmpty() && this.b.isEmpty();
    }
}
