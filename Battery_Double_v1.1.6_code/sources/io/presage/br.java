package io.presage;

import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import org.json.JSONObject;

public final class br {
    public static final br a = new br();

    private br() {
    }

    public static bq a(String str) {
        try {
            return a(new JSONObject(str));
        } catch (Throwable unused) {
            return null;
        }
    }

    public static bq a(JSONObject jSONObject) {
        bq bqVar = new bq();
        String optString = jSONObject.optString("url", "");
        hl.a((Object) optString, "zoneJson.optString(\"url\", \"\")");
        bqVar.a(optString);
        String optString2 = jSONObject.optString("content", "");
        hl.a((Object) optString2, "zoneJson.optString(\"content\", \"\")");
        bqVar.b(optString2);
        String optString3 = jSONObject.optString("webViewId", jSONObject.optString("id", ""));
        hl.a((Object) optString3, "zoneJson.optString(\"webViewId\", id)");
        bqVar.c(optString3);
        JSONObject optJSONObject = jSONObject.optJSONObject("size");
        int i = -1;
        bqVar.b(optJSONObject != null ? optJSONObject.optInt("width", -1) : -1);
        JSONObject optJSONObject2 = jSONObject.optJSONObject("size");
        bqVar.a(optJSONObject2 != null ? optJSONObject2.optInt("height", -1) : -1);
        JSONObject optJSONObject3 = jSONObject.optJSONObject(ParametersKeys.POSITION);
        bqVar.d(optJSONObject3 != null ? optJSONObject3.optInt(AvidJSONUtil.KEY_X, -1) : -1);
        JSONObject optJSONObject4 = jSONObject.optJSONObject(ParametersKeys.POSITION);
        if (optJSONObject4 != null) {
            i = optJSONObject4.optInt(AvidJSONUtil.KEY_Y, -1);
        }
        bqVar.c(i);
        bqVar.a(jSONObject.optBoolean("enableTracking", false));
        bqVar.b(jSONObject.optBoolean("keepAlive", false));
        bqVar.c(jSONObject.optBoolean("isLandingPage", false));
        return bqVar;
    }
}
