package io.presage;

import android.graphics.Rect;
import java.util.Map;

public final class cu {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private boolean b;
    private int c;
    private int d;
    private int e;
    private int f;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static cu a(Map<String, String> map) {
            cu cuVar = new cu(0);
            String str = (String) map.get("allowOffscreen");
            cuVar.a(str != null ? Boolean.parseBoolean(str) : true);
            try {
                cuVar.a(af.b(ad.a(map, "width")));
                cuVar.b(af.b(ad.a(map, "height")));
                cuVar.c(af.b(ad.a(map, "offsetX")));
                cuVar.d(af.b(ad.a(map, "offsetY")));
                return cuVar;
            } catch (Throwable unused) {
                return null;
            }
        }

        public static cu a(Rect rect) {
            return a(rect.left, rect.top, rect.width(), rect.height());
        }

        private static cu a(int i, int i2, int i3, int i4) {
            cu cuVar = new cu(0);
            cuVar.a(false);
            cuVar.c(i);
            cuVar.d(i2);
            cuVar.a(i3);
            cuVar.b(i4);
            return cuVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        if (r2.f == r3.f) goto L_0x0029;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof cu) {
                cu cuVar = (cu) obj;
                if (this.b == cuVar.b) {
                    if (this.c == cuVar.c) {
                        if (this.d == cuVar.d) {
                            if (this.e == cuVar.e) {
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        boolean z = this.b;
        if (z) {
            z = true;
        }
        return ((((((((z ? 1 : 0) * true) + this.c) * 31) + this.d) * 31) + this.e) * 31) + this.f;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ResizeProps(allowOffscreen=");
        sb.append(this.b);
        sb.append(", width=");
        sb.append(this.c);
        sb.append(", height=");
        sb.append(this.d);
        sb.append(", offsetX=");
        sb.append(this.e);
        sb.append(", offsetY=");
        sb.append(this.f);
        sb.append(")");
        return sb.toString();
    }

    private cu() {
        this.b = false;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final boolean b() {
        return this.b;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final int c() {
        return this.c;
    }

    public final void b(int i) {
        this.d = i;
    }

    public final int d() {
        return this.d;
    }

    public final void c(int i) {
        this.e = i;
    }

    public final int e() {
        return this.e;
    }

    public final void d(int i) {
        this.f = i;
    }

    public final int f() {
        return this.f;
    }

    public cu(byte b2) {
        this();
    }

    public final Rect a() {
        return new Rect(this.e, this.f, this.e + this.c, this.f + this.d);
    }
}
