package io.presage;

import android.content.Context;
import io.presage.Montbriac.CamembertauCalvados;

public final class MoelleuxduRevard {
    public static final MoelleuxduRevard a = new MoelleuxduRevard();
    private static Montbriac b;

    private MoelleuxduRevard() {
    }

    public static void a(Context context) {
        if (b == null) {
            CamembertauCalvados camembertauCalvados = Montbriac.a;
            b = CamembertauCalvados.a(context);
        }
    }

    public static void a(Mimolette24mois mimolette24mois) {
        Montbriac montbriac = b;
        if (montbriac != null) {
            montbriac.a(mimolette24mois);
        }
    }
}
