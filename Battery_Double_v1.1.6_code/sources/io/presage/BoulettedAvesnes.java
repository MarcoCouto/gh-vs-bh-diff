package io.presage;

public final class BoulettedAvesnes {
    private final CamembertdeNormandie a;
    private final AbbayedeCiteauxentiere b;

    public BoulettedAvesnes(CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
        this.a = camembertdeNormandie;
        this.b = abbayedeCiteauxentiere;
    }

    public final CamembertdeNormandie a() {
        return this.a;
    }

    public final AbbayedeCiteauxentiere b() {
        return this.b;
    }
}
