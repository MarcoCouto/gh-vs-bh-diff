package io.presage.core;

import android.support.annotation.NonNull;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class IIllllII extends IIlllIll implements ExecutorService {
    public IIllllII(ExecutorService executorService) {
        super(executorService);
    }

    public boolean awaitTermination(long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.awaitTermination(j, timeUnit);
    }

    @NonNull
    public <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection) {
        return this.IIIIIIII.invokeAll(collection);
    }

    @NonNull
    public <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.invokeAll(collection, j, timeUnit);
    }

    @NonNull
    public <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection) {
        return this.IIIIIIII.invokeAny(collection);
    }

    public <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.invokeAny(collection, j, timeUnit);
    }

    public boolean isShutdown() {
        return this.IIIIIIII.isShutdown();
    }

    public boolean isTerminated() {
        return this.IIIIIIII.isTerminated();
    }

    @NonNull
    public List<Runnable> shutdownNow() {
        return this.IIIIIIII.shutdownNow();
    }

    @NonNull
    public Future<?> submit(@NonNull Runnable runnable) {
        return this.IIIIIIII.submit(runnable);
    }

    @NonNull
    public <T> Future<T> submit(@NonNull Runnable runnable, T t) {
        return this.IIIIIIII.submit(runnable, t);
    }

    @NonNull
    public <T> Future<T> submit(@NonNull Callable<T> callable) {
        return this.IIIIIIII.submit(callable);
    }
}
