package io.presage.core.service;

import android.app.job.JobParameters;
import android.support.annotation.RequiresApi;
import io.presage.core.e2;

@RequiresApi(api = 26)
public class SMJobService extends e2 {
    public void onCreate() {
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onStartJob(JobParameters jobParameters) {
        return super.onStartJob(jobParameters);
    }

    public boolean onStopJob(JobParameters jobParameters) {
        return super.onStopJob(jobParameters);
    }

    public void onTrimMemory(int i) {
        new Object[1][0] = Integer.valueOf(i);
    }
}
