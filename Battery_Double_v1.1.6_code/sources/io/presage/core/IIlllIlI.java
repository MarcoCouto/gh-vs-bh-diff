package io.presage.core;

import android.os.AsyncTask;
import android.support.annotation.MainThread;
import android.support.annotation.VisibleForTesting;

public abstract class IIlllIlI<Params, Result> {
    public IIIIIIII IIIIIIII;

    @VisibleForTesting
    public static class IIIIIIII<Params, Result> extends AsyncTask<Params, Void, Result> {
        public IIlllIlI<Params, Result> IIIIIIII;
        public IIllllII IIIIIIIl;
        public boolean IIIIIIlI;

        @VisibleForTesting
        public IIIIIIII(IIlllIlI<Params, Result> iIlllIlI, IIllllII iIllllII, boolean z) {
            this.IIIIIIII = iIlllIlI;
            this.IIIIIIIl = iIllllII;
            this.IIIIIIlI = z;
        }

        public Result doInBackground(Params... paramsArr) {
            return this.IIIIIIII.IIIIIIII(paramsArr);
        }

        public void onPostExecute(Result result) {
            this.IIIIIIIl.IIIIIIIl();
            if (this.IIIIIIlI) {
                IIIIlIll.IIIIIIII((IIlllIll) this.IIIIIIIl);
            }
            this.IIIIIIII.IIIIIIII(result);
        }
    }

    public abstract Result IIIIIIII(Params... paramsArr);

    @MainThread
    public void IIIIIIII(IIllllII iIllllII, boolean z, Params... paramsArr) {
        try {
            if (!iIllllII.IIIIIIII.isShutdown()) {
                if (this.IIIIIIII == null) {
                    this.IIIIIIII = new IIIIIIII(this, iIllllII, z);
                }
                this.IIIIIIII.executeOnExecutor(iIllllII, paramsArr);
                return;
            }
            throw new IIlllIII();
        } catch (Exception e) {
            throw new IIlllIII(e);
        }
    }

    public abstract void IIIIIIII(Result result);
}
