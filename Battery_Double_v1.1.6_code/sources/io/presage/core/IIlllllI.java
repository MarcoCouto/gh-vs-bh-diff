package io.presage.core;

import android.support.annotation.NonNull;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class IIlllllI extends IIlllIll implements ScheduledExecutorService {
    public IIlllllI(ScheduledExecutorService scheduledExecutorService) {
        super(scheduledExecutorService);
    }

    public boolean awaitTermination(long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.awaitTermination(j, timeUnit);
    }

    @NonNull
    public <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection) {
        return this.IIIIIIII.invokeAll(collection);
    }

    @NonNull
    public <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.invokeAll(collection, j, timeUnit);
    }

    @NonNull
    public <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection) {
        return this.IIIIIIII.invokeAny(collection);
    }

    public <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.invokeAny(collection, j, timeUnit);
    }

    public boolean isShutdown() {
        return this.IIIIIIII.isShutdown();
    }

    public boolean isTerminated() {
        return this.IIIIIIII.isTerminated();
    }

    @NonNull
    public ScheduledFuture<?> schedule(@NonNull Runnable runnable, long j, @NonNull TimeUnit timeUnit) {
        this.IIIIIIlI.incrementAndGet();
        new Object[1][0] = Integer.valueOf(this.IIIIIIlI.intValue());
        try {
            return ((ScheduledExecutorService) this.IIIIIIII).schedule(runnable, j, timeUnit);
        } catch (Exception unused) {
            this.IIIIIIlI.decrementAndGet();
            return null;
        }
    }

    @NonNull
    public <V> ScheduledFuture<V> schedule(@NonNull Callable<V> callable, long j, @NonNull TimeUnit timeUnit) {
        this.IIIIIIlI.incrementAndGet();
        try {
            return ((ScheduledExecutorService) this.IIIIIIII).schedule(callable, j, timeUnit);
        } catch (Exception unused) {
            this.IIIIIIlI.decrementAndGet();
            return null;
        }
    }

    @NonNull
    public ScheduledFuture<?> scheduleAtFixedRate(@NonNull Runnable runnable, long j, long j2, @NonNull TimeUnit timeUnit) {
        this.IIIIIIlI.incrementAndGet();
        try {
            return ((ScheduledExecutorService) this.IIIIIIII).scheduleAtFixedRate(runnable, j, j2, timeUnit);
        } catch (Exception unused) {
            this.IIIIIIlI.decrementAndGet();
            return null;
        }
    }

    @NonNull
    public ScheduledFuture<?> scheduleWithFixedDelay(@NonNull Runnable runnable, long j, long j2, @NonNull TimeUnit timeUnit) {
        this.IIIIIIlI.incrementAndGet();
        try {
            return ((ScheduledExecutorService) this.IIIIIIII).scheduleWithFixedDelay(runnable, j, j2, timeUnit);
        } catch (Exception unused) {
            this.IIIIIIlI.decrementAndGet();
            return null;
        }
    }

    @NonNull
    public List<Runnable> shutdownNow() {
        return this.IIIIIIII.shutdownNow();
    }

    @NonNull
    public Future<?> submit(@NonNull Runnable runnable) {
        return this.IIIIIIII.submit(runnable);
    }

    @NonNull
    public <T> Future<T> submit(@NonNull Runnable runnable, T t) {
        return this.IIIIIIII.submit(runnable, t);
    }

    @NonNull
    public <T> Future<T> submit(@NonNull Callable<T> callable) {
        return this.IIIIIIII.submit(callable);
    }
}
