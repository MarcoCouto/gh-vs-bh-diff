package io.presage;

import android.annotation.TargetApi;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import io.presage.common.profig.schedule.ProfigJobService;

@TargetApi(21)
public final class o implements C0133r {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private JobScheduler b;
    private final Context c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public o(Context context) {
        this.c = context;
        Object systemService = this.c.getSystemService("jobscheduler");
        if (systemService != null) {
            this.b = (JobScheduler) systemService;
            return;
        }
        throw new em("null cannot be cast to non-null type android.app.job.JobScheduler");
    }

    public final void a(long j) {
        Builder builder = new Builder(475439765, new ComponentName(this.c, ProfigJobService.class));
        builder.setMinimumLatency(j);
        double d = (double) j;
        Double.isNaN(d);
        builder.setOverrideDeadline((long) (d * 1.2d));
        builder.setRequiredNetworkType(1);
        builder.setRequiresCharging(false);
        builder.setRequiresDeviceIdle(false);
        this.b.schedule(builder.build());
    }

    public final void a() {
        this.b.cancel(475439765);
    }
}
