package io.presage;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class it implements ii<ia> {
    /* access modifiers changed from: private */
    public final CharSequence a;
    /* access modifiers changed from: private */
    public final int b = 0;
    /* access modifiers changed from: private */
    public final int c = 0;
    /* access modifiers changed from: private */
    public final gr<CharSequence, Integer, ej<Integer, Integer>> d;

    public static final class CamembertauCalvados implements hr, Iterator<ia> {
        final /* synthetic */ it a;
        private int b = -1;
        private int c;
        private int d;
        private ia e;
        private int f;

        public final void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        CamembertauCalvados(it itVar) {
            this.a = itVar;
            this.c = ib.c(itVar.b, itVar.a.length());
            this.d = this.c;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0022, code lost:
            if (r6.f < io.presage.it.a(r6.a)) goto L_0x0024;
         */
        private final void a() {
            int i = 0;
            if (this.d < 0) {
                this.b = 0;
                this.e = null;
                return;
            }
            if (this.a.c > 0) {
                this.f++;
            }
            if (this.d <= this.a.a.length()) {
                ej ejVar = (ej) this.a.d.a(this.a.a, Integer.valueOf(this.d));
                if (ejVar == null) {
                    this.e = new ia(this.c, iv.a(this.a.a));
                    this.d = -1;
                } else {
                    int intValue = ((Number) ejVar.c()).intValue();
                    int intValue2 = ((Number) ejVar.d()).intValue();
                    this.e = ib.a(this.c, intValue);
                    this.c = intValue + intValue2;
                    int i2 = this.c;
                    if (intValue2 == 0) {
                        i = 1;
                    }
                    this.d = i2 + i;
                }
                this.b = 1;
            }
            this.e = new ia(this.c, iv.a(this.a.a));
            this.d = -1;
            this.b = 1;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public ia next() {
            if (this.b == -1) {
                a();
            }
            if (this.b != 0) {
                ia iaVar = this.e;
                if (iaVar != null) {
                    this.e = null;
                    this.b = -1;
                    return iaVar;
                }
                throw new em("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }

        public final boolean hasNext() {
            if (this.b == -1) {
                a();
            }
            return this.b == 1;
        }
    }

    public it(CharSequence charSequence, int i, gr<? super CharSequence, ? super Integer, ej<Integer, Integer>> grVar) {
        this.a = charSequence;
        this.d = grVar;
    }

    public final Iterator<ia> a() {
        return new CamembertauCalvados<>(this);
    }
}
