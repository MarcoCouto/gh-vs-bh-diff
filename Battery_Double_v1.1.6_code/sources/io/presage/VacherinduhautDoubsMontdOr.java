package io.presage;

import android.widget.FrameLayout;

public final class VacherinduhautDoubsMontdOr {
    public static final VacherinduhautDoubsMontdOr a = new VacherinduhautDoubsMontdOr();

    private VacherinduhautDoubsMontdOr() {
    }

    public static BurratadesPouilles a(AbbayedeCiteauxentiere abbayedeCiteauxentiere, FrameLayout frameLayout, PavedAremberg pavedAremberg, String str) {
        return new BurratadesPouilles(abbayedeCiteauxentiere, frameLayout, pavedAremberg, str);
    }
}
