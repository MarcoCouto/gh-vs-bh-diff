package io.presage;

public class hk extends he implements hj, ih {
    private final int c;

    public hk(int i, Object obj) {
        super(obj);
        this.c = i;
    }

    /* access modifiers changed from: protected */
    public final ie e() {
        return hn.a(this);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof hk) {
            hk hkVar = (hk) obj;
            if (b() != null ? b().equals(hkVar.b()) : hkVar.b() == null) {
                return c().equals(hkVar.c()) && d().equals(hkVar.d()) && hl.a(f(), hkVar.f());
            }
        } else if (obj instanceof ih) {
            return obj.equals(g());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (((b() == null ? 0 : b().hashCode() * 31) + c().hashCode()) * 31) + d().hashCode();
    }

    public String toString() {
        ie g = g();
        if (g != this) {
            return g.toString();
        }
        if ("<init>".equals(c())) {
            return "constructor (Kotlin reflection is not available)";
        }
        StringBuilder sb = new StringBuilder("function ");
        sb.append(c());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }
}
