package io.presage;

import com.smaato.sdk.core.api.VideoType;
import com.tapjoy.TJAdUnitConstants.String;

public final class cw implements cy {
    private final Taleggio a;
    private final cy b;
    private final PontlEveque c;

    public cw(Taleggio taleggio, cy cyVar, PontlEveque pontlEveque) {
        this.a = taleggio;
        this.b = cyVar;
        this.c = pontlEveque;
    }

    public final void a(ax axVar) {
        db c2 = axVar.c();
        int a2 = af.a(c2.getWidth());
        int a3 = af.a(c2.getHeight());
        int a4 = af.a(c2.getX());
        int a5 = af.a(c2.getY());
        this.b.a(axVar);
        axVar.a(SaintNectaire.a(this.c) ? VideoType.INTERSTITIAL : String.INLINE);
        axVar.a(false);
        axVar.a(this.a.l());
        axVar.a();
        axVar.a(a2, a3, a4, a5);
        axVar.b(a2, a3, a4, a5);
        axVar.c(a2, a3, a4, a5);
        axVar.c(a2, a3);
        axVar.b("default");
    }
}
