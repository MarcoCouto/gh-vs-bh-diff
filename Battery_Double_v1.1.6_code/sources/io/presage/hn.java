package io.presage;

public final class hn {
    private static final ho a;
    private static final Cif[] b = new Cif[0];

    static {
        ho hoVar = null;
        try {
            hoVar = (ho) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (hoVar == null) {
            hoVar = new ho();
        }
        a = hoVar;
    }

    public static Cif a(Class cls) {
        return ho.a(cls);
    }

    public static String a(hm hmVar) {
        return ho.a(hmVar);
    }

    public static ih a(hk hkVar) {
        return ho.a(hkVar);
    }
}
