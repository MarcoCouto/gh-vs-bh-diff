package io.presage;

import org.json.JSONObject;

public final class TommedeYenne {
    private final boolean a;
    private final long b;
    private final JSONObject c;
    private final boolean d;
    private final String e;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002e, code lost:
        if (io.presage.hl.a((java.lang.Object) r5.e, (java.lang.Object) r6.e) != false) goto L_0x0033;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof TommedeYenne) {
                TommedeYenne tommedeYenne = (TommedeYenne) obj;
                if (this.a == tommedeYenne.a) {
                    if (this.b == tommedeYenne.b) {
                        if (hl.a((Object) this.c, (Object) tommedeYenne.c)) {
                            if (this.d == tommedeYenne.d) {
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        boolean z = this.a;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = (z ? 1 : 0) * true;
        long j = this.b;
        int i3 = (i2 + ((int) (j ^ (j >>> 32)))) * 31;
        JSONObject jSONObject = this.c;
        int i4 = 0;
        int hashCode = (i3 + (jSONObject != null ? jSONObject.hashCode() : 0)) * 31;
        boolean z2 = this.d;
        if (!z2) {
            i = z2;
        }
        int i5 = (hashCode + i) * 31;
        String str = this.e;
        if (str != null) {
            i4 = str.hashCode();
        }
        return i5 + i4;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ProfigRequest(allowRequest=");
        sb.append(this.a);
        sb.append(", jobScheduleWindow=");
        sb.append(this.b);
        sb.append(", request=");
        sb.append(this.c);
        sb.append(", profigEnabled=");
        sb.append(this.d);
        sb.append(", profigHash=");
        sb.append(this.e);
        sb.append(")");
        return sb.toString();
    }

    public TommedeYenne(boolean z, long j, JSONObject jSONObject, boolean z2, String str) {
        this.a = z;
        this.b = j;
        this.c = jSONObject;
        this.d = z2;
        this.e = str;
    }

    public final boolean a() {
        return this.a;
    }

    public final long b() {
        return this.b;
    }

    public final JSONObject c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public /* synthetic */ TommedeYenne(long j, JSONObject jSONObject, boolean z) {
        this(false, j, jSONObject, z, null);
    }

    public final String e() {
        return this.e;
    }
}
