package io.presage;

import java.io.Serializable;

public final class ec<T> implements ee<T>, Serializable {
    private final T a;

    public ec(T t) {
        this.a = t;
    }

    public final T a() {
        return this.a;
    }

    public final String toString() {
        return String.valueOf(a());
    }
}
