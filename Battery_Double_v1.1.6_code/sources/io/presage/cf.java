package io.presage;

import android.webkit.WebView;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.lang.ref.WeakReference;

public final class cf implements cl {
    private cg a;
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public final db c;
    /* access modifiers changed from: private */
    public final PontlEveque d;
    private final by e;
    private final cb f;

    public static final class CamembertauCalvados extends dc {
        final /* synthetic */ cf a;

        CamembertauCalvados(cf cfVar) {
            this.a = cfVar;
        }

        public final void b() {
            cf.b(this.a.c, this.a.d);
            this.a.d();
        }

        public final void b(WebView webView, String str) {
            this.a.b = true;
            this.a.g();
        }

        public final void c() {
            if (!this.a.b) {
                this.a.e();
            }
        }
    }

    public cf(db dbVar, PontlEveque pontlEveque, by byVar, cb cbVar) {
        this.c = dbVar;
        this.d = pontlEveque;
        this.e = byVar;
        this.f = cbVar;
        c();
    }

    private final void c() {
        this.c.setClientAdapter(new CamembertauCalvados(this));
    }

    /* access modifiers changed from: private */
    public final void d() {
        cg cgVar = this.a;
        if (cgVar != null) {
            cgVar.a(this.d);
        }
        f();
    }

    /* access modifiers changed from: private */
    public final void e() {
        MoelleuxduRevard moelleuxduRevard = MoelleuxduRevard.a;
        MoelleuxduRevard.a((Mimolette24mois) new Munster("loaded_error", this.d));
        cg cgVar = this.a;
        if (cgVar != null) {
            cgVar.b();
        }
        f();
    }

    private final void f() {
        this.c.setWebViewClient(null);
        this.c.setClientAdapter(null);
    }

    /* access modifiers changed from: private */
    public final void g() {
        MoelleuxduRevard moelleuxduRevard = MoelleuxduRevard.a;
        MoelleuxduRevard.a((Mimolette24mois) new Munster(ParametersKeys.LOADED, this.d));
        by.a(new bx(new WeakReference(this.f), this.c, this.d));
        cg cgVar = this.a;
        if (cgVar != null) {
            cgVar.a();
        }
    }

    /* access modifiers changed from: private */
    public static void b(WebView webView, PontlEveque pontlEveque) {
        an.e(webView);
        MoelleuxduRevard moelleuxduRevard = MoelleuxduRevard.a;
        MoelleuxduRevard.a((Mimolette24mois) new Munster("loaded_error", pontlEveque));
    }

    public final void a(cg cgVar) {
        this.a = cgVar;
        de.a(this.c, this.d);
    }

    public final boolean a() {
        return this.b;
    }

    public final void b() {
        this.a = null;
        f();
        an.e(this.c);
    }
}
