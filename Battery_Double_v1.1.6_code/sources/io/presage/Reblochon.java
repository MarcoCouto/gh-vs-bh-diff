package io.presage;

import org.json.JSONObject;

public final class Reblochon {
    public static final Reblochon a = new Reblochon();

    private Reblochon() {
    }

    public static String a(RegaldeBourgogne regaldeBourgogne, RaclettedeSavoie raclettedeSavoie, String str, Soumaintrain soumaintrain) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("name", raclettedeSavoie.a());
        jSONObject.put("ad_sync_type", "load");
        if (soumaintrain != null) {
            jSONObject.put("overlay", a(soumaintrain, regaldeBourgogne.j()));
        }
        if (raclettedeSavoie.b() != null) {
            jSONObject.put("ad_unit_id", raclettedeSavoie.b());
        }
        if (str.length() > 0) {
            jSONObject.put("app_user_id", str);
        }
        jSONObject.put("is_moat_compliant", regaldeBourgogne.e());
        jSONObject.put("is_omid_compliant", regaldeBourgogne.f());
        jSONObject.put("omid_integration_version", 3);
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("width", regaldeBourgogne.g());
        jSONObject2.put("height", regaldeBourgogne.h());
        StringBuilder sb = new StringBuilder("{\"connectivity\":\"");
        sb.append(regaldeBourgogne.a());
        sb.append("\",\"at\":\"");
        sb.append(regaldeBourgogne.b());
        sb.append("\",\"country\":\"");
        sb.append(regaldeBourgogne.c());
        sb.append("\",\"build\":30070,\"apps_publishers\":[\"");
        sb.append(regaldeBourgogne.d());
        sb.append("\"],\"version\":\"");
        sb.append(regaldeBourgogne.i());
        sb.append("\",\"device\":");
        sb.append(jSONObject2);
        sb.append(',');
        sb.append("\"content\":");
        sb.append(jSONObject);
        sb.append('}');
        return sb.toString();
    }

    private static JSONObject a(Soumaintrain soumaintrain, float f) {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("width", soumaintrain.a());
        jSONObject2.put("height", soumaintrain.b());
        jSONObject2.put("scaler", Float.valueOf(f));
        jSONObject.put("overlay_max_size", jSONObject2);
        return jSONObject;
    }
}
