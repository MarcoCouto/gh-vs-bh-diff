package io.presage;

import android.graphics.Rect;
import java.util.Collections;
import java.util.List;

public final class av {
    private Rect a;
    private List<Rect> b;
    private float c;

    public av() {
        List<Rect> emptyList = Collections.emptyList();
        hl.a((Object) emptyList, "Collections.emptyList()");
        this.b = emptyList;
    }

    public final Rect a() {
        return this.a;
    }

    public final void a(Rect rect) {
        this.a = rect;
    }

    public final void a(List<Rect> list) {
        this.b = list;
    }

    public final List<Rect> b() {
        return this.b;
    }

    public final void a(float f) {
        this.c = f;
    }

    public final float c() {
        return this.c;
    }
}
