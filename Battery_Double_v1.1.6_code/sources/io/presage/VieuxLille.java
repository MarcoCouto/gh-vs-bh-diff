package io.presage;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.LinkedBlockingQueue;

public final class VieuxLille implements ServiceConnection {
    private final LinkedBlockingQueue<IBinder> a = new LinkedBlockingQueue<>(1);
    private boolean b;

    public final void onServiceDisconnected(ComponentName componentName) {
    }

    public final IBinder a() throws InterruptedException {
        if (!this.b) {
            this.b = true;
            Object take = this.a.take();
            if (take != null) {
                return (IBinder) take;
            }
            throw new em("null cannot be cast to non-null type android.os.IBinder");
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.a.put(iBinder);
        } catch (InterruptedException unused) {
        }
    }
}
