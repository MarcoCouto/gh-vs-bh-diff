package io.presage;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public final class da implements cy {
    private final Taleggio a;

    static final class CamembertauCalvados extends hm implements gf<ep> {
        final /* synthetic */ da a;
        final /* synthetic */ ax b;

        CamembertauCalvados(da daVar, ax axVar) {
            this.a = daVar;
            this.b = axVar;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            da.c(this.b);
        }
    }

    public da(Taleggio taleggio) {
        this.a = taleggio;
    }

    public final void a(ax axVar) {
        an.a(axVar.c(), new CamembertauCalvados(this, axVar));
    }

    /* access modifiers changed from: private */
    public static void c(ax axVar) {
        db c = axVar.c();
        int a2 = af.a(c.getWidth());
        int a3 = af.a(c.getHeight());
        ViewParent parent = c.getParent();
        if (!(parent instanceof ViewGroup)) {
            parent = null;
        }
        ViewGroup viewGroup = (ViewGroup) parent;
        if (viewGroup == null) {
            viewGroup = c;
        }
        int a4 = af.a(viewGroup.getX());
        int a5 = af.a(viewGroup.getY());
        Rect a6 = Taleggio.a((View) axVar.c());
        axVar.b(af.a(a6.width()), af.a(a6.height()));
        axVar.a(a2, a3, a4, a5);
    }
}
