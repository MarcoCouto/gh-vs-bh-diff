package io.presage;

import java.util.Collection;
import java.util.List;

class ez extends ey {
    public static final <T> Collection<T> a(T[] tArr) {
        return new eq<>(tArr);
    }

    public static final <T> List<T> a() {
        return fj.a;
    }

    public static final <T> List<T> b(T... tArr) {
        return tArr.length > 0 ? er.a(tArr) : ex.a();
    }

    public static final <T> int a(List<? extends T> list) {
        return list.size() - 1;
    }

    public static final void b() {
        throw new ArithmeticException("Index overflow has happened.");
    }
}
