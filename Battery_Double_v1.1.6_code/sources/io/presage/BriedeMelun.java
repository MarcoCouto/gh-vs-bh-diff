package io.presage;

import android.graphics.Rect;
import java.util.List;

public final class BriedeMelun {
    public static final int a(Rect rect) {
        return rect.width() * rect.height();
    }

    public static final void a(Rect rect, Rect rect2) {
        rect.offset(-rect2.left, -rect2.top);
    }

    public static final void a(List<Rect> list, Rect rect) {
        for (Rect a : list) {
            a(a, rect);
        }
    }
}
