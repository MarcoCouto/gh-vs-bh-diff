package io.presage;

public final class Munster extends Mimolette24mois {
    private final PontlEveque a;

    public Munster(String str, PontlEveque pontlEveque) {
        super(str, 0);
        this.a = pontlEveque;
    }

    public final PontlEveque a() {
        return this.a;
    }
}
