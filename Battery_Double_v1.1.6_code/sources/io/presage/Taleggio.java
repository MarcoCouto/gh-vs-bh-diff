package io.presage;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.media.AudioManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.util.DisplayMetrics;
import android.view.View;
import com.google.android.exoplayer2.util.MimeTypes;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class Taleggio {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final String b;
    private final String c;
    private final Context d;
    private final TimeZone e;
    private final DisplayMetrics f;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private Taleggio(Context context, TimeZone timeZone, DisplayMetrics displayMetrics) {
        this.d = context;
        this.e = timeZone;
        this.f = displayMetrics;
        String str = Build.MANUFACTURER;
        hl.a((Object) str, "Build.MANUFACTURER");
        this.b = str;
        this.c = Build.MODEL;
    }

    private /* synthetic */ Taleggio(Context context, TimeZone timeZone) {
        Resources system = Resources.getSystem();
        hl.a((Object) system, "Resources.getSystem()");
        DisplayMetrics displayMetrics = system.getDisplayMetrics();
        hl.a((Object) displayMetrics, "Resources.getSystem().displayMetrics");
        this(context, timeZone, displayMetrics);
    }

    public Taleggio(Context context) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        hl.a((Object) timeZone, "TimeZone.getTimeZone(\"UTC\")");
        this(context, timeZone);
    }

    public static String a() {
        String str = VERSION.RELEASE;
        hl.a((Object) str, "Build.VERSION.RELEASE");
        return str;
    }

    public static String b() {
        String property = System.getProperty("java.vm.version");
        return property == null ? "" : property;
    }

    public static String c() {
        String property = System.getProperty("java.vm.name");
        return property == null ? "" : property;
    }

    public static String d() {
        String property = System.getProperty("os.arch");
        return property == null ? "" : property;
    }

    public static String e() {
        try {
            String format = new SimpleDateFormat("Z", Locale.US).format(new Date());
            StringBuilder sb = new StringBuilder();
            hl.a((Object) format, "formattedTime");
            if (format != null) {
                String substring = format.substring(0, 3);
                hl.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                sb.append(substring);
                sb.append(":");
                String substring2 = format.substring(3, format.length());
                hl.a((Object) substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                sb.append(substring2);
                return sb.toString();
            }
            throw new em("null cannot be cast to non-null type java.lang.String");
        } catch (Exception unused) {
            return "";
        }
    }

    public final String f() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        simpleDateFormat.setTimeZone(this.e);
        String format = simpleDateFormat.format(date);
        hl.a((Object) format, "format.format(currentLocalDate)");
        return format;
    }

    public final boolean g() {
        try {
            if (VERSION.SDK_INT >= 17) {
                if (VERSION.SDK_INT < 21) {
                    if (VERSION.SDK_INT < 17 || Global.getInt(this.d.getContentResolver(), "install_non_market_apps", 0) != 1) {
                        return false;
                    }
                    return true;
                }
            }
            if (Secure.getInt(this.d.getContentResolver(), "install_non_market_apps", 0) != 1) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public final String h() {
        String str = this.c;
        if (str != null) {
            String a2 = a(this.b, str);
            if (a2 != null) {
                return a2;
            }
        }
        return "Unknown";
    }

    private static String a(String str, String str2) {
        if (iv.b(str2, str)) {
            return str2;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" ");
        sb.append(str2);
        return sb.toString();
    }

    public final String i() {
        String str = "UNKNOWN";
        try {
            NetworkInfo a2 = v.a(this.d);
            return a2 != null ? a(a2) : str;
        } catch (Exception unused) {
            return "NONE";
        }
    }

    private static String a(NetworkInfo networkInfo) {
        if (v.a(networkInfo)) {
            String typeName = networkInfo.getTypeName();
            hl.a((Object) typeName, "info.typeName");
            return typeName;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(networkInfo.getTypeName());
        sb.append(" - ");
        sb.append(networkInfo.getSubtypeName());
        return sb.toString();
    }

    public final int j() {
        return this.f.widthPixels;
    }

    public final int k() {
        return this.f.heightPixels;
    }

    public static Rect a(View view) {
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        return rect;
    }

    public final int l() {
        Object systemService = this.d.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (systemService != null) {
            return ((AudioManager) systemService).getStreamVolume(3);
        }
        throw new em("null cannot be cast to non-null type android.media.AudioManager");
    }

    public final String m() {
        Resources resources = this.d.getResources();
        hl.a((Object) resources, "context.resources");
        return resources.getConfiguration().orientation == 2 ? "landscape" : "portrait";
    }

    public final float n() {
        return this.f.density;
    }
}
