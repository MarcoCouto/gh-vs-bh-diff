package io.presage;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public final class bb {
    public static final bb a = new bb();
    private static final Map<String, gg<ba, ep>> b = Collections.synchronizedMap(new LinkedHashMap());

    private bb() {
    }

    public static void a(String str, gg<? super ba, ep> ggVar) {
        Map<String, gg<ba, ep>> map = b;
        hl.a((Object) map, "listeners");
        map.put(str, ggVar);
    }

    public static void a(ba baVar) {
        gg ggVar = (gg) b.get(baVar.a());
        if (ggVar != null) {
            ggVar.a(baVar);
        }
    }

    public static void a(String str) {
        b.remove(str);
    }
}
