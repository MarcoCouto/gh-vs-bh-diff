package io.presage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class ey {
    public static final <T> List<T> a(T t) {
        List<T> singletonList = Collections.singletonList(t);
        hl.a((Object) singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }

    public static final <T> Object[] a(T[] tArr, boolean z) {
        if (z && hl.a((Object) tArr.getClass(), (Object) Object[].class)) {
            return tArr;
        }
        Object[] copyOf = Arrays.copyOf(tArr, tArr.length, Object[].class);
        hl.a((Object) copyOf, "java.util.Arrays.copyOf(… Array<Any?>::class.java)");
        return copyOf;
    }
}
