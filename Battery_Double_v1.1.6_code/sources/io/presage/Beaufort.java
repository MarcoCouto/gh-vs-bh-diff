package io.presage;

import android.app.Application;
import android.webkit.WebView;

public final class Beaufort {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final au b;
    private final dz c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static Beaufort a() {
            return new Beaufort(new au(), new dz());
        }
    }

    public Beaufort(au auVar, dz dzVar) {
        this.b = auVar;
        this.c = dzVar;
    }

    public final void a(j jVar, PontlEveque pontlEveque, WebView webView, Application application) {
        if (jVar.d() && pontlEveque.p()) {
            this.c.a(pontlEveque.q(), webView);
        }
        if (jVar.c() && pontlEveque.o()) {
            this.b.a(application, webView);
        }
    }

    public final void a() {
        this.c.a();
        this.b.a();
    }
}
