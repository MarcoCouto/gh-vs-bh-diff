package io.presage;

import android.content.Context;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.presage.common.SdkType;
import io.presage.common.SdkType.CamembertauCalvados;
import java.util.Map;

public final class Piastrellou extends PaletdeBourgogne {
    private final Context a;
    private final TetedeMoine b;
    private final Taleggio c;
    private final SdkType d;
    private final h e;

    public /* synthetic */ Piastrellou(Context context) {
        TetedeMoine tetedeMoine = new TetedeMoine(context);
        Taleggio taleggio = new Taleggio(context);
        TommeMarcdeRaisin tommeMarcdeRaisin = TommeMarcdeRaisin.a;
        CamembertauCalvados camembertauCalvados = SdkType.a;
        SdkType a2 = CamembertauCalvados.a(context);
        h.CamembertauCalvados camembertauCalvados2 = h.a;
        this(context, tetedeMoine, taleggio, tommeMarcdeRaisin, a2, h.CamembertauCalvados.a(context));
    }

    private Piastrellou(Context context, TetedeMoine tetedeMoine, Taleggio taleggio, TommeMarcdeRaisin tommeMarcdeRaisin, SdkType sdkType, h hVar) {
        super(context, tetedeMoine, tommeMarcdeRaisin);
        this.a = context;
        this.b = tetedeMoine;
        this.c = taleggio;
        this.d = sdkType;
        this.e = hVar;
    }

    public final Map<String, String> a() {
        Map<String, String> a2 = super.a();
        StringBuilder sb = new StringBuilder(RequestParameters.LEFT_BRACKETS);
        sb.append(this.b.a());
        sb.append(']');
        a2.put("Api-Key", sb.toString());
        a2.put("Sdk-Version", "[3.3.9-moat]");
        a2.put("Timezone", Taleggio.e());
        a2.put("Connectivity", this.c.i());
        a2.put("Sdk-Version-Type", "ads");
        a2.put("Sdk-Type", String.valueOf(this.d.a()));
        Langres langres = Langres.a;
        a2.put("Framework", String.valueOf(Langres.a()));
        a2.put("Mediation", this.e.j());
        return a2;
    }
}
