package io.presage;

import android.app.Activity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class CarreMirabelle {
    private List<String> a = new ArrayList();
    private List<String> b = new ArrayList();
    private final e c;
    private final Class<? extends Activity> d;

    public CarreMirabelle(e eVar, Class<? extends Activity> cls) {
        this.c = eVar;
        this.d = cls;
        b();
        a();
    }

    public final void a(List<String> list) {
        if (this.c.c()) {
            this.a.addAll(list);
        }
    }

    public final void b(List<? extends Class<? extends Activity>> list) {
        if (this.c.d()) {
            for (Class cls : list) {
                List<String> list2 = this.b;
                String canonicalName = cls.getCanonicalName();
                hl.a((Object) canonicalName, "it.canonicalName");
                list2.add(canonicalName);
            }
        }
    }

    public final void a(Activity activity) {
        if (this.c.b()) {
            this.a.add(Coulommiers.a(activity));
        }
    }

    private final void a() {
        if (!this.c.e().isEmpty()) {
            this.a.addAll(this.c.e());
        }
    }

    private final void b() {
        if (!this.c.f().isEmpty()) {
            this.b.addAll(this.c.f());
        }
    }

    public final boolean b(Activity activity) {
        if (activity instanceof Livarot) {
            return false;
        }
        if ((this.c.a() || !(!hl.a((Object) activity.getClass(), (Object) this.d))) && !a(Coulommiers.a((Object) activity)) && b(Coulommiers.a((Object) activity))) {
            return true;
        }
        return false;
    }

    private final boolean a(String str) {
        Iterable<String> iterable = this.b;
        if (!(iterable instanceof Collection) || !((Collection) iterable).isEmpty()) {
            for (String b2 : iterable) {
                if (iv.b(str, b2)) {
                    return true;
                }
            }
        }
        return false;
    }

    private final boolean b(String str) {
        Iterable<String> iterable = this.a;
        if (!(iterable instanceof Collection) || !((Collection) iterable).isEmpty()) {
            for (String b2 : iterable) {
                if (iv.b(str, b2)) {
                    return true;
                }
            }
        }
        return false;
    }
}
