package io.presage;

import org.json.JSONObject;

public final class TommeduJura {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private JSONObject b;
    private String c;
    private final String d = this.g.d();
    private final j e;
    private final c f;
    private final h g;
    private final boolean h;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public TommeduJura(TommedeSavoie tommedeSavoie, c cVar, h hVar, boolean z) {
        this.f = cVar;
        this.g = hVar;
        this.h = z;
        this.b = tommedeSavoie.a(this.f);
        TommeCrayeuse tommeCrayeuse = TommeCrayeuse.a;
        String jSONObject = this.b.toString();
        hl.a((Object) jSONObject, "generatedProfig.toString()");
        this.c = TommeCrayeuse.a(jSONObject);
        l lVar = l.a;
        this.e = l.a(this.d);
    }

    public final j a() {
        return this.e;
    }

    private final boolean c() {
        return this.e != null ? this.g.a() >= this.e.f() : this.g.a() >= 10;
    }

    private final boolean d() {
        j jVar = this.e;
        if (jVar != null) {
            return jVar.a();
        }
        return true;
    }

    private final boolean e() {
        return hl.a((Object) this.g.g(), (Object) al.a());
    }

    private final boolean f() {
        j jVar = this.e;
        return this.g.h() + (jVar != null ? jVar.g() : 0) > System.currentTimeMillis();
    }

    public final TommedeYenne b() {
        j jVar = this.e;
        long g2 = jVar != null ? jVar.g() : 43200000;
        boolean d2 = d();
        boolean z = !d2;
        boolean c2 = c();
        boolean z2 = true;
        boolean z3 = !f();
        boolean z4 = !e();
        boolean z5 = z && z4;
        if (!d2 || c2 || !z3 || (!this.h && !g() && !z4 && !z3)) {
            z2 = false;
        }
        if ((this.h || j()) && !c2) {
            TommedeYenne tommedeYenne = new TommedeYenne(true, g2, this.b, d2, this.c);
            return tommedeYenne;
        } else if (!z2 && !z5 && !h()) {
            return new TommedeYenne(g2, new JSONObject(), d2);
        } else {
            String str = null;
            if (g()) {
                str = this.c;
            }
            TommedeYenne tommedeYenne2 = new TommedeYenne(true, g2, i(), d2, str);
            return tommedeYenne2;
        }
    }

    private final boolean g() {
        return !hl.a((Object) this.g.b(), (Object) this.c);
    }

    private final boolean h() {
        return !hl.a((Object) this.g.c(), (Object) this.f.a());
    }

    private final JSONObject i() {
        if (this.h || g()) {
            return this.b;
        }
        return new JSONObject();
    }

    private final boolean j() {
        return (this.d.length() == 0) || hl.a((Object) this.d, (Object) "{}");
    }
}
