package io.presage;

import java.lang.reflect.Method;

public class fs {

    static final class CamembertauCalvados {
        public static final Method a;
        public static final CamembertauCalvados b = new CamembertauCalvados();

        /* JADX WARNING: Removed duplicated region for block: B:13:0x0049 A[EDGE_INSN: B:13:0x0049->B:11:0x0049 ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0045 A[LOOP:0: B:1:0x0015->B:9:0x0045, LOOP_END] */
        static {
            Method method;
            boolean z;
            Class<Throwable> cls = Throwable.class;
            Method[] methods = cls.getMethods();
            hl.a((Object) methods, "throwableClass.methods");
            int length = methods.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    method = null;
                    break;
                }
                method = methods[i];
                hl.a((Object) method, "it");
                if (hl.a((Object) method.getName(), (Object) "addSuppressed")) {
                    Class[] parameterTypes = method.getParameterTypes();
                    hl.a((Object) parameterTypes, "it.parameterTypes");
                    if (hl.a((Object) (Class) er.b(parameterTypes), (Object) cls)) {
                        z = true;
                        if (!z) {
                            break;
                        }
                        i++;
                    }
                }
                z = false;
                if (!z) {
                }
            }
            a = method;
        }

        private CamembertauCalvados() {
        }
    }

    public void a(Throwable th, Throwable th2) {
        Method method = CamembertauCalvados.a;
        if (method != null) {
            method.invoke(th, new Object[]{th2});
        }
    }
}
