package io.presage;

import java.util.Collection;
import java.util.List;

public final class Entrammes {
    private final DelicedesFiouves a;

    public Entrammes(DelicedesFiouves delicedesFiouves) {
        this.a = delicedesFiouves;
    }

    public final boolean a(List<? extends Object> list) {
        Iterable<Object> iterable = list;
        for (Object a2 : iterable) {
            if (a(a2)) {
                return false;
            }
        }
        for (Object b : iterable) {
            if (b(b)) {
                return true;
            }
        }
        return false;
    }

    private final boolean a(Object obj) {
        String a2 = Coulommiers.a(obj);
        Iterable<String> b = this.a.b();
        if (!(b instanceof Collection) || !((Collection) b).isEmpty()) {
            for (String a3 : b) {
                if (hl.a((Object) a3, (Object) a2)) {
                    return true;
                }
            }
        }
        return false;
    }

    private final boolean b(Object obj) {
        String a2 = Coulommiers.a(obj);
        Iterable<String> a3 = this.a.a();
        if (!(a3 instanceof Collection) || !((Collection) a3).isEmpty()) {
            for (String b : a3) {
                if (iv.b(a2, b)) {
                    return true;
                }
            }
        }
        return false;
    }
}
