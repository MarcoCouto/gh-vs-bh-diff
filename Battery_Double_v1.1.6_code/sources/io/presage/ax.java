package io.presage;

public final class ax {
    private final db a;

    public ax(db dbVar) {
        this.a = dbVar;
    }

    public final db c() {
        return this.a;
    }

    public final void a(String str, String str2) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(str, str2));
    }

    public final void a(int i) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(i));
    }

    public final void a() {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a());
    }

    public final void a(String str, boolean z) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(str, z));
    }

    public final void a(boolean z, String str) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(z, str));
    }

    public final void a(int i, int i2) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(i, i2));
    }

    public final void a(String str) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(str));
    }

    public final void a(boolean z) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(z));
    }

    public final void b(int i, int i2) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.b(i, i2));
    }

    public final void a(int i, int i2, int i3, int i4) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(i, i2, i3, i4));
    }

    public final void b(int i, int i2, int i3, int i4) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.b(i, i2, i3, i4));
    }

    public final void c(int i, int i2, int i3, int i4) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.c(i, i2, i3, i4));
    }

    public final void c(int i, int i2) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.c(i, i2));
    }

    public final void b(String str) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.b(str));
        this.a.setAdState(str);
    }

    public final void b(String str, String str2) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.b(str, str2));
    }

    public final void a(av avVar) {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.a(avVar));
    }

    public final void b() {
        db dbVar = this.a;
        ay ayVar = ay.a;
        de.a(dbVar, ay.c());
    }
}
