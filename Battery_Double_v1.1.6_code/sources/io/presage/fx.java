package io.presage;

import java.io.Closeable;

public final class fx {
    public static final void a(Closeable closeable, Throwable th) {
        if (th == null) {
            closeable.close();
            return;
        }
        try {
            closeable.close();
        } catch (Throwable th2) {
            ea.a(th, th2);
        }
    }
}
