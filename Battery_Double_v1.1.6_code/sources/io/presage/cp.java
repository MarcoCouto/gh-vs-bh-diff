package io.presage;

public final class cp implements be {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public static final cp c = new cp(new be[0]);
    private final be[] b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static cp a() {
            return cp.c;
        }
    }

    public cp(be[] beVarArr) {
        this.b = beVarArr;
    }

    public final boolean a(String str, db dbVar, SaintFelicien saintFelicien) {
        for (be a2 : this.b) {
            if (a2.a(str, dbVar, saintFelicien)) {
                return true;
            }
        }
        return false;
    }
}
