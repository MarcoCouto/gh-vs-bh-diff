package io.presage;

import java.util.Iterator;
import java.util.List;

public final class FourmedAmbertBio {

    static final class CamembertauCalvados extends hm implements gg<PontlEveque, Boolean> {
        public static final CamembertauCalvados a = new CamembertauCalvados();

        CamembertauCalvados() {
            super(1);
        }

        public final /* synthetic */ Object a(Object obj) {
            return Boolean.valueOf(a((PontlEveque) obj));
        }

        private static boolean a(PontlEveque pontlEveque) {
            return pontlEveque.v();
        }
    }

    public static final PontlEveque a(List<PontlEveque> list, String str) {
        if (list.isEmpty()) {
            return null;
        }
        if ((str.length() == 0) || hl.a((Object) str, (Object) "null")) {
            return (PontlEveque) list.remove(0);
        }
        return b(list, str);
    }

    public static final void a(List<PontlEveque> list) {
        ex.a((List) list, (gg) CamembertauCalvados.a);
    }

    private static final PontlEveque b(List<PontlEveque> list, String str) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            PontlEveque pontlEveque = (PontlEveque) it.next();
            if (hl.a((Object) pontlEveque.b(), (Object) str)) {
                it.remove();
                return pontlEveque;
            }
        }
        return null;
    }
}
