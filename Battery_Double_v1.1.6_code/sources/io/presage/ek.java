package io.presage;

import java.io.Serializable;

final class ek<T> implements ee<T>, Serializable {
    private gf<? extends T> a;
    private volatile Object b;
    private final Object c;

    private ek(gf<? extends T> gfVar) {
        this.a = gfVar;
        this.b = en.a;
        this.c = this;
    }

    public /* synthetic */ ek(gf gfVar, byte b2) {
        this(gfVar);
    }

    public final T a() {
        T t;
        T t2 = this.b;
        if (t2 != en.a) {
            return t2;
        }
        synchronized (this.c) {
            t = this.b;
            if (t == en.a) {
                gf<? extends T> gfVar = this.a;
                if (gfVar == null) {
                    hl.a();
                }
                t = gfVar.a();
                this.b = t;
                this.a = null;
            }
        }
        return t;
    }

    private boolean b() {
        return this.b != en.a;
    }

    public final String toString() {
        return b() ? String.valueOf(a()) : "Lazy value not initialized yet.";
    }

    private final Object writeReplace() {
        return new ec(a());
    }
}
