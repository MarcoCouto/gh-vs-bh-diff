package io.presage;

import java.util.LinkedList;
import java.util.List;

public final class ck implements ci {
    private LinkedList<cl> a = new LinkedList<>();
    private final cg b;

    public ck(cg cgVar) {
        this.b = cgVar;
    }

    public final void a(List<? extends cl> list) {
        this.a.addAll(list);
        b();
    }

    public final void a() {
        b();
    }

    private final void b() {
        cl clVar = (cl) this.a.pollFirst();
        if (clVar != null) {
            clVar.a(this.b);
        }
    }
}
