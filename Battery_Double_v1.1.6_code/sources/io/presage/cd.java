package io.presage;

import android.webkit.WebSettings;
import android.webkit.WebView;

public final class cd {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private String b = "";
    private int c = -1;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public final void a(WebView webView) {
        if (this.b.length() == 0) {
            WebSettings settings = webView.getSettings();
            hl.a((Object) settings, "webView.settings");
            String userAgentString = settings.getUserAgentString();
            hl.a((Object) userAgentString, "webView.settings.userAgentString");
            this.b = userAgentString;
            b();
        }
    }

    private final void b() {
        int a2 = iv.a((CharSequence) this.b, "chrome/", 0, true);
        if (a2 != -1) {
            int i = a2 + 7;
            try {
                String str = this.b;
                int i2 = i + 2;
                if (str != null) {
                    String substring = str.substring(i, i2);
                    hl.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    this.c = Integer.parseInt(substring);
                    return;
                }
                throw new em("null cannot be cast to non-null type java.lang.String");
            } catch (Throwable unused) {
            }
        }
    }

    public final boolean a() {
        return this.c <= 57;
    }
}
