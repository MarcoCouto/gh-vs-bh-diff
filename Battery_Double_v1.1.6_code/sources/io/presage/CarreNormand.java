package io.presage;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;

public final class CarreNormand implements CoeurdArras {
    private final Application a;
    private ActivityLifecycleCallbacks b;
    private final Activity c;
    /* access modifiers changed from: private */
    public final Chaource d;
    private final CarreMirabelle e;
    private final aj f;

    public static final class CamembertauCalvados extends GrandMunster {
        final /* synthetic */ CarreNormand a;

        CamembertauCalvados(CarreNormand carreNormand) {
            this.a = carreNormand;
        }

        public final void onActivityPaused(Activity activity) {
            if (!(activity instanceof Livarot)) {
                this.a.d.a();
            }
        }

        public final void onActivityResumed(Activity activity) {
            if (!(activity instanceof Livarot)) {
                this.a.a(activity);
            }
        }
    }

    private CarreNormand(Activity activity, Chaource chaource, CarreMirabelle carreMirabelle, aj ajVar) {
        this.c = activity;
        this.d = chaource;
        this.e = carreMirabelle;
        this.f = ajVar;
        this.a = this.c.getApplication();
    }

    public /* synthetic */ CarreNormand(Activity activity, Chaource chaource, CarreMirabelle carreMirabelle) {
        this(activity, chaource, carreMirabelle, aj.a);
    }

    public final void registerLifecycleListener() {
        this.b = new CamembertauCalvados(this);
        this.a.registerActivityLifecycleCallbacks(this.b);
    }

    public final void injectInitialOverlay() {
        Activity a2 = aj.a();
        if (a2 == null) {
            a2 = this.c;
        }
        a(a2);
    }

    /* access modifiers changed from: private */
    public final void a(Activity activity) {
        if (this.e.b(activity)) {
            this.d.a(activity);
        }
    }

    public final void unregisterLifecycleListener() {
        this.a.unregisterActivityLifecycleCallbacks(this.b);
    }
}
