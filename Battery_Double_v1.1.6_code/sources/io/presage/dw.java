package io.presage;

import android.content.Context;
import com.iab.omid.library.oguryco.Omid;

public final class dw {
    public static final dw a = new dw();

    private dw() {
    }

    public static void a(Context context) {
        try {
            Omid.activate(context.getApplicationContext());
        } catch (IllegalArgumentException e) {
            dv dvVar = dv.a;
            dv.a(e);
        }
    }

    public static boolean a() {
        return Omid.isActive();
    }

    public static dx b() {
        return new dx();
    }
}
