package io.presage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class aw {
    public static final aw a = new aw();

    private aw() {
    }

    public static void a(Context context, String str) {
        CharSequence charSequence = str;
        if (!(charSequence == null || charSequence.length() == 0)) {
            try {
                Uri parse = Uri.parse(str);
                hl.a((Object) parse, "uri");
                a(context, parse);
            } catch (Exception unused) {
            }
        }
    }

    private static void a(Context context, Uri uri) {
        Intent intent = new Intent("android.intent.action.VIEW", uri);
        if (b(context, intent)) {
            a(context, intent);
        }
    }

    public static boolean b(Context context, String str) {
        CharSequence charSequence = str;
        if (charSequence == null || charSequence.length() == 0) {
            return false;
        }
        try {
            Intent parseUri = Intent.parseUri(str, 0);
            hl.a((Object) parseUri, "intent");
            if (b(context, parseUri)) {
                a(context, parseUri);
                return true;
            }
        } catch (Exception unused) {
        }
        return false;
    }

    public static boolean c(Context context, String str) {
        CharSequence charSequence = str;
        if (charSequence == null || charSequence.length() == 0) {
            return false;
        }
        try {
            Intent parseUri = Intent.parseUri(str, 0);
            hl.a((Object) parseUri, "intent");
            return b(context, parseUri);
        } catch (Exception unused) {
            return false;
        }
    }

    private static void a(Context context, Intent intent) {
        if (b(context, intent)) {
            intent.addFlags(268435456);
            context.startActivity(intent);
        }
    }

    private static boolean b(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0;
    }
}
