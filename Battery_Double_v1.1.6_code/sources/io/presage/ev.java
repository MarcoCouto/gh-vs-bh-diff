package io.presage;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

class ev extends eu {
    public static final <T> boolean a(T[] tArr, T t) {
        return er.b(tArr, t) >= 0;
    }

    public static final <T> int b(T[] tArr, T t) {
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
        } else {
            int length2 = tArr.length;
            while (i < length2) {
                if (hl.a((Object) t, (Object) tArr[i])) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    public static final char a(char[] cArr) {
        switch (cArr.length) {
            case 0:
                throw new NoSuchElementException("Array is empty.");
            case 1:
                return cArr[0];
            default:
                throw new IllegalArgumentException("Array has more than one element.");
        }
    }

    public static final <T> T b(T[] tArr) {
        if (tArr.length == 1) {
            return tArr[0];
        }
        return null;
    }

    public static final <T> List<T> c(T[] tArr) {
        switch (tArr.length) {
            case 0:
                return ex.a();
            case 1:
                return ex.a(tArr[0]);
            default:
                return er.d(tArr);
        }
    }

    public static final <T> List<T> d(T[] tArr) {
        return new ArrayList<>(ex.a((Object[]) tArr));
    }
}
