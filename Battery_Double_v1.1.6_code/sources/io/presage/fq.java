package io.presage;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class fq extends fp {
    public static final <K, V> Map<K, V> a() {
        fk fkVar = fk.a;
        if (fkVar != null) {
            return fkVar;
        }
        throw new em("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    }

    public static final <K, V> void a(Map<? super K, ? super V> map, Iterable<? extends ej<? extends K, ? extends V>> iterable) {
        for (ej ejVar : iterable) {
            map.put(ejVar.c(), ejVar.d());
        }
    }

    public static final <K, V> Map<K, V> a(Iterable<? extends ej<? extends K, ? extends V>> iterable) {
        Collection collection = (Collection) iterable;
        switch (collection.size()) {
            case 0:
                return fn.a();
            case 1:
                return fn.a((ej) ((List) iterable).get(0));
            default:
                return fn.a(iterable, (M) new LinkedHashMap(fn.a(collection.size())));
        }
    }

    public static final <K, V, M extends Map<? super K, ? super V>> M a(Iterable<? extends ej<? extends K, ? extends V>> iterable, M m) {
        fn.a((Map<? super K, ? super V>) m, iterable);
        return m;
    }
}
