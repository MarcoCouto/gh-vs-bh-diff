package io.presage;

public final class c {
    private final String a;
    private final boolean b;
    private final boolean c;

    public c(String str, boolean z, boolean z2) {
        this.a = str;
        this.b = z;
        this.c = z2;
    }

    public final String a() {
        return this.a;
    }

    public final boolean b() {
        return this.b;
    }

    public final boolean c() {
        return this.c;
    }
}
