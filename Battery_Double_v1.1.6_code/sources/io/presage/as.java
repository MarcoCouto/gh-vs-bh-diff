package io.presage;

import android.app.Application;
import android.content.Context;
import io.presage.interstitial.ui.InterstitialActivity;
import io.presage.interstitial.ui.InterstitialActivity.CamembertauCalvados;
import java.util.List;

public final class as implements Bofavre {
    private final CamembertauCalvados a;

    public /* synthetic */ as() {
        this(InterstitialActivity.a);
    }

    private as(CamembertauCalvados camembertauCalvados) {
        this.a = camembertauCalvados;
    }

    public final void a(Application application, List<PontlEveque> list, String str) {
        PontlEveque a2 = FourmedAmbertBio.a(list, str);
        if (a2 != null) {
            this.a.a((Context) application, a2, list);
        }
    }
}
