package io.presage;

import android.app.Activity;
import java.util.ArrayList;
import java.util.List;

public final class DelicedesFiouves {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final List<String> b;
    private final List<String> c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static DelicedesFiouves a(Activity activity, g gVar, EtivazGruyereSuisse etivazGruyereSuisse) {
            return new DelicedesFiouves(a(gVar, etivazGruyereSuisse, activity), a(gVar, etivazGruyereSuisse));
        }

        private static List<String> a(g gVar, EtivazGruyereSuisse etivazGruyereSuisse, Activity activity) {
            List<String> arrayList = new ArrayList<>();
            arrayList.addAll(gVar.e());
            if (gVar.c()) {
                arrayList.addAll(etivazGruyereSuisse.a());
            }
            if (gVar.b()) {
                arrayList.add(Coulommiers.a(activity));
            }
            return arrayList;
        }

        private static List<String> a(g gVar, EtivazGruyereSuisse etivazGruyereSuisse) {
            List<String> arrayList = new ArrayList<>();
            arrayList.addAll(gVar.f());
            if (gVar.d()) {
                for (Class cls : etivazGruyereSuisse.b()) {
                    String canonicalName = cls.getCanonicalName();
                    if (canonicalName == null) {
                        canonicalName = cls.getName();
                    }
                    hl.a((Object) canonicalName, "fullName");
                    arrayList.add(canonicalName);
                }
            }
            return arrayList;
        }
    }

    public DelicedesFiouves(List<String> list, List<String> list2) {
        this.b = list;
        this.c = list2;
    }

    public final List<String> a() {
        return this.b;
    }

    public final List<String> b() {
        return this.c;
    }
}
