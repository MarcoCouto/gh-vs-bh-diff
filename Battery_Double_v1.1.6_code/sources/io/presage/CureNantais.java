package io.presage;

import android.app.Application;
import java.util.List;

public final class CureNantais implements Bofavre {
    private final gr<PontlEveque, List<PontlEveque>, ep> a;

    public CureNantais(gr<? super PontlEveque, ? super List<PontlEveque>, ep> grVar) {
        this.a = grVar;
    }

    public final void a(Application application, List<PontlEveque> list, String str) {
        PontlEveque a2 = FourmedAmbertBio.a(list, str);
        if (a2 != null) {
            this.a.a(a2, list);
        }
    }
}
