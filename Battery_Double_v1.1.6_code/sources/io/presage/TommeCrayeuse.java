package io.presage;

import com.google.android.exoplayer2.C;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class TommeCrayeuse {
    public static final TommeCrayeuse a = new TommeCrayeuse();

    private TommeCrayeuse() {
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            hl.a((Object) instance, "MessageDigest.getInstance(\"MD5\")");
            Charset forName = Charset.forName(C.ASCII_NAME);
            hl.a((Object) forName, "Charset.forName(\"US-ASCII\")");
            byte[] bytes = str.getBytes(forName);
            hl.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            instance.update(bytes, 0, str.length());
            byte[] digest = instance.digest();
            BigInteger bigInteger = new BigInteger(1, digest);
            hp hpVar = hp.a;
            StringBuilder sb = new StringBuilder("%0");
            sb.append(digest.length << 1);
            sb.append(AvidJSONUtil.KEY_X);
            String format = String.format(sb.toString(), Arrays.copyOf(new Object[]{bigInteger}, 1));
            hl.a((Object) format, "java.lang.String.format(format, *args)");
            return format;
        } catch (NoSuchAlgorithmException e) {
            Laguiole laguiole = Laguiole.a;
            Laguiole.a(e);
            return "";
        }
    }
}
