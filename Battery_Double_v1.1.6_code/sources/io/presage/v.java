package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class v {
    @SuppressLint({"MissingPermission"})
    public static final NetworkInfo a(Context context) {
        if (!ae.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return null;
        }
        Object systemService = context.getSystemService("connectivity");
        if (systemService != null) {
            return ((ConnectivityManager) systemService).getActiveNetworkInfo();
        }
        throw new em("null cannot be cast to non-null type android.net.ConnectivityManager");
    }

    public static final boolean a(NetworkInfo networkInfo) {
        return networkInfo.isConnected() && networkInfo.getType() == 1;
    }

    public static final boolean b(Context context) {
        NetworkInfo a = a(context);
        return a != null && a.isConnected();
    }
}
