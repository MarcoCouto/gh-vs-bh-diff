package io.presage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public final class bk {
    private final Map<String, db> a;
    private final Map<String, bs> b;

    public bk(Map<String, db> map, Map<String, bs> map2) {
        this.a = map;
        this.b = map2;
    }

    private final List<db> e() {
        List<db> arrayList = new ArrayList<>();
        for (db dbVar : this.a.values()) {
            bs bsVar = (bs) this.b.get(bi.b(dbVar));
            if (bsVar != null && bsVar.d()) {
                arrayList.add(dbVar);
            }
        }
        return arrayList;
    }

    public final void a() {
        for (db d : e()) {
            d.d();
        }
    }

    public final void a(String str, boolean z, boolean z2, String str2, String str3) {
        for (db dbVar : e()) {
            ay ayVar = ay.a;
            de.a(dbVar, ay.a(str, z, z2, str2, str3));
        }
    }

    public final void b() {
        for (db dbVar : e()) {
            ay ayVar = ay.a;
            de.a(dbVar, ay.b());
        }
    }

    public final void c() {
        for (db e : e()) {
            e.e();
        }
    }

    public static void a(db dbVar, String str, String str2) {
        ay ayVar = ay.a;
        StringBuilder sb = new StringBuilder("{webviewId:\"");
        sb.append(str2);
        sb.append("\"}");
        de.a(dbVar, ay.b(str, sb.toString()));
    }

    public final boolean d() {
        Iterable<bs> values = this.b.values();
        if (!(values instanceof Collection) || !((Collection) values).isEmpty()) {
            for (bs b2 : values) {
                if (b2.b()) {
                    return false;
                }
            }
        }
        return true;
    }
}
