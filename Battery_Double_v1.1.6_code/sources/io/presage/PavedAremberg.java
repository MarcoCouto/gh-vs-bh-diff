package io.presage;

import io.presage.common.AdConfig;
import org.json.JSONObject;

public interface PavedAremberg {
    FourmedeMontbrison a(String str);

    dr a(SableduBoulonnais sableduBoulonnais, AdConfig adConfig, String str, Soumaintrain soumaintrain);

    dr a(JSONObject jSONObject);

    FourmedeMontbrison b(JSONObject jSONObject);

    void b(String str);

    FourmedeMontbrison c(JSONObject jSONObject);

    dr c(String str);
}
