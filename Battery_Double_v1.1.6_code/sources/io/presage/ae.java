package io.presage;

import android.content.Context;

public final class ae {
    public static final boolean a(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }
}
