package io.presage;

import android.graphics.Rect;

public final class ay {
    public static final ay a = new ay();

    public static String a() {
        return "ogySdkMraidGateway.updateSupportFlags({sms: false, tel: false, calendar: false, storePicture: false, inlineVideo: false, vpaid: false, location: false})";
    }

    public static String b() {
        return "ogySdkMraidGateway.callEventListeners(\"ogyOnCloseSystem\", {})";
    }

    public static String c() {
        return "ogySdkMraidGateway.callEventListeners(\"ogyOnTouchEnd\", {})";
    }

    private ay() {
    }

    public static String a(String str, String str2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.callErrorListeners(\"");
        sb.append(str2);
        sb.append("\", \"");
        sb.append(str);
        sb.append("\")");
        return sb.toString();
    }

    public static String a(int i) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateAudioVolume(");
        sb.append(i);
        sb.append(')');
        return sb.toString();
    }

    public static String a(String str, boolean z) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateCurrentAppOrientation({orientation: \"");
        sb.append(str);
        sb.append("\", locked: ");
        sb.append(z);
        sb.append("})");
        return sb.toString();
    }

    public static String a(boolean z, String str) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateOrientationProperties({allowOrientationChange: ");
        sb.append(z);
        sb.append(", forceOrientation: \"");
        sb.append(str);
        sb.append("\"})");
        return sb.toString();
    }

    public static String a(int i, int i2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateScreenSize({width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("})");
        return sb.toString();
    }

    public static String a(String str) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updatePlacementType(\"");
        sb.append(str);
        sb.append("\")");
        return sb.toString();
    }

    public static String a(boolean z) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateViewability(");
        sb.append(z);
        sb.append(')');
        return sb.toString();
    }

    public static String b(int i, int i2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateMaxSize({width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("})");
        return sb.toString();
    }

    public static String a(int i, int i2, int i3, int i4) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateDefaultPosition({x: ");
        sb.append(i3);
        sb.append(", y: ");
        sb.append(i4);
        sb.append(", width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("})");
        return sb.toString();
    }

    public static String b(int i, int i2, int i3, int i4) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateCurrentPosition({x: ");
        sb.append(i3);
        sb.append(", y: ");
        sb.append(i4);
        sb.append(", width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("})");
        return sb.toString();
    }

    public static String c(int i, int i2, int i3, int i4) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateResizeProperties({width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append(", offsetX: ");
        sb.append(i3);
        sb.append(", offsetY: ");
        sb.append(i4);
        sb.append(", customClosePosition: \"right\", allowOffscreen: false})");
        return sb.toString();
    }

    public static String c(int i, int i2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateExpandProperties({width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append(", useCustomClose: false, isModal: true})");
        return sb.toString();
    }

    public static String b(String str) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateState(\"");
        sb.append(str);
        sb.append("\")");
        return sb.toString();
    }

    public static String b(String str, String str2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.callPendingMethodCallback(\"");
        sb.append(str);
        sb.append("\", null, ");
        sb.append(str2);
        sb.append(')');
        return sb.toString();
    }

    public static String a(av avVar) {
        String str;
        StringBuilder sb = new StringBuilder();
        for (Rect rect : avVar.b()) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            StringBuilder sb2 = new StringBuilder("{x: ");
            sb2.append(af.a(rect.left));
            sb2.append(", y: ");
            sb2.append(af.a(rect.top));
            sb2.append(", width: ");
            sb2.append(af.a(rect.width()));
            sb2.append(", height: ");
            sb2.append(af.a(rect.height()));
            sb2.append('}');
            sb.append(sb2.toString());
        }
        Rect a2 = avVar.a();
        if (a2 != null) {
            StringBuilder sb3 = new StringBuilder("visibleRectangle: {x: ");
            sb3.append(af.a(a2.left));
            sb3.append(", y: ");
            sb3.append(af.a(a2.top));
            sb3.append(", width: ");
            sb3.append(af.a(a2.width()));
            sb3.append(", height: ");
            sb3.append(af.a(a2.height()));
            sb3.append('}');
            str = sb3.toString();
        } else {
            str = "visibleRectangle: null";
        }
        StringBuilder sb4 = new StringBuilder("ogySdkMraidGateway.updateExposure({exposedPercentage: ");
        sb4.append(avVar.c());
        sb4.append(", ");
        sb4.append(str);
        sb4.append(", occlusionRectangles: [");
        sb4.append(sb);
        sb4.append("]})");
        return sb4.toString();
    }

    public static String a(String str, boolean z, boolean z2, String str2, String str3) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.callEventListeners(\"ogyOnNavigation\", {event: \"");
        sb.append(str);
        sb.append("\", canGoBack: ");
        sb.append(z2);
        sb.append(", canGoForward: ");
        sb.append(z);
        sb.append(", webviewId: \"");
        sb.append(str2);
        sb.append("\", url: \"");
        sb.append(str3);
        sb.append("\"})");
        return sb.toString();
    }
}
