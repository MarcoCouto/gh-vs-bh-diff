package io.presage;

import android.content.Context;
import org.json.JSONArray;
import org.json.JSONObject;

public final class OlivetauPoivre {
    private final Taleggio a;
    private final TetedeMoine b;

    private OlivetauPoivre(Taleggio taleggio, TetedeMoine tetedeMoine) {
        this.a = taleggio;
        this.b = tetedeMoine;
    }

    public /* synthetic */ OlivetauPoivre(Context context) {
        this(new Taleggio(context), new TetedeMoine(context));
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("connectivity", this.a.i());
        jSONObject.put("at", this.a.f());
        jSONObject.put("build", 30070);
        jSONObject.put("version", "3.3.9-moat");
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.b.a());
        jSONObject.put("apps_publishers", jSONArray);
        return jSONObject;
    }
}
