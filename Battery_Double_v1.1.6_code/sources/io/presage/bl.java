package io.presage;

import android.content.Context;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Locale;
import org.json.JSONObject;

public final class bl implements be {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private gf<ep> b;
    private gf<ep> c;
    private y d = new z();
    private final String[] e = {"ogyCreateWebView", "ogyUpdateWebView", "ogyCloseWebView", "ogyNavigateBack", "ogyNavigateForward"};
    private final bj f;
    private final bk g;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public bl(bj bjVar, bk bkVar) {
        this.f = bjVar;
        this.g = bkVar;
    }

    public final void a(gf<ep> gfVar) {
        this.b = gfVar;
    }

    public final void b(gf<ep> gfVar) {
        this.c = gfVar;
    }

    public final boolean a(String str, db dbVar, SaintFelicien saintFelicien) {
        Locale locale = Locale.US;
        hl.a((Object) locale, "Locale.US");
        String lowerCase = str.toLowerCase(locale);
        hl.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        if (!iv.b(lowerCase, "http://ogymraid")) {
            return false;
        }
        String substring = str.substring(19);
        hl.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
        JSONObject jSONObject = new JSONObject(ak.a(substring));
        String optString = jSONObject.optString("method", "");
        String optString2 = jSONObject.optString(String.CALLBACK_ID, "");
        JSONObject optJSONObject = jSONObject.optJSONObject("args");
        if (optJSONObject == null) {
            optJSONObject = new JSONObject();
        }
        hl.a((Object) optString, String.COMMAND);
        hl.a((Object) optString2, String.CALLBACK_ID);
        a(optString, optJSONObject, optString2, dbVar);
        return er.a(this.e, optString);
    }

    private final void a(String str, JSONObject jSONObject, String str2, db dbVar) {
        switch (str.hashCode()) {
            case -1797727422:
                if (str.equals("ogyCloseWebView")) {
                    c(jSONObject, str2, dbVar);
                    return;
                }
                break;
            case -1244773540:
                if (str.equals("ogyCreateWebView")) {
                    a(jSONObject, str2, dbVar);
                    return;
                }
                break;
            case -692274449:
                if (str.equals("ogyUpdateWebView")) {
                    b(jSONObject, str2, dbVar);
                    return;
                }
                break;
            case 960350259:
                if (str.equals("ogyNavigateForward")) {
                    b(jSONObject);
                    break;
                }
                break;
            case 1635219001:
                if (str.equals("ogyNavigateBack")) {
                    a(jSONObject);
                    return;
                }
                break;
        }
    }

    private final void a(JSONObject jSONObject, String str, db dbVar) {
        y yVar = this.d;
        Context context = dbVar.getContext();
        hl.a((Object) context, "webView.context");
        if (yVar.a(context)) {
            br brVar = br.a;
            bq a2 = br.a(jSONObject);
            this.f.a(a2);
            bk.a(dbVar, str, a2.c());
            gf<ep> gfVar = this.b;
            if (gfVar != null) {
                gfVar.a();
            }
            return;
        }
        gf<ep> gfVar2 = this.c;
        if (gfVar2 != null) {
            gfVar2.a();
        }
    }

    private final void b(JSONObject jSONObject, String str, db dbVar) {
        br brVar = br.a;
        bq a2 = br.a(jSONObject);
        this.f.b(a2);
        bk.a(dbVar, str, a2.c());
    }

    private final void c(JSONObject jSONObject, String str, db dbVar) {
        String optString = jSONObject.optString("webViewId", "");
        bj bjVar = this.f;
        hl.a((Object) optString, "webViewId");
        bjVar.a(optString);
        bk.a(dbVar, str, optString);
    }

    private final void a(JSONObject jSONObject) {
        String optString = jSONObject.optString("webViewId", "");
        bj bjVar = this.f;
        hl.a((Object) optString, "webViewId");
        bjVar.b(optString);
    }

    private final void b(JSONObject jSONObject) {
        String optString = jSONObject.optString("webViewId", "");
        bj bjVar = this.f;
        hl.a((Object) optString, "webViewId");
        bjVar.c(optString);
    }
}
