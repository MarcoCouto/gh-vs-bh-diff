package io.presage;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

class ga extends fz {
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        io.presage.fx.a(r0, r2);
     */
    public static final void a(File file, byte[] bArr) {
        Closeable fileOutputStream = new FileOutputStream(file);
        ((FileOutputStream) fileOutputStream).write(bArr);
        ep epVar = ep.a;
        fx.a(fileOutputStream, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        io.presage.fx.a(r1, r2);
     */
    public static final String a(File file, Charset charset) {
        Closeable inputStreamReader = new InputStreamReader(new FileInputStream(file), charset);
        String a = gd.a((InputStreamReader) inputStreamReader);
        fx.a(inputStreamReader, null);
        return a;
    }

    public static final void a(File file, String str, Charset charset) {
        byte[] bytes = str.getBytes(charset);
        hl.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        fy.a(file, bytes);
    }
}
