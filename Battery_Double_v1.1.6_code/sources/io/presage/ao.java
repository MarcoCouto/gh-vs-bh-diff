package io.presage;

import io.presage.interstitial.PresageInterstitialCallback;

public final class ao implements EcirdelAubrac {
    private final PresageInterstitialCallback a;

    public ao(PresageInterstitialCallback presageInterstitialCallback) {
        this.a = presageInterstitialCallback;
    }

    public final void a() {
        this.a.onAdAvailable();
    }

    public final void b() {
        this.a.onAdNotAvailable();
    }

    public final void c() {
        this.a.onAdLoaded();
    }

    public final void d() {
        this.a.onAdNotLoaded();
    }

    public final void e() {
        this.a.onAdDisplayed();
    }

    public final void f() {
        this.a.onAdClosed();
    }

    public final void a(int i) {
        this.a.onAdError(i);
    }
}
