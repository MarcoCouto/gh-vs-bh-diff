package io.presage;

import android.app.Activity;

public final class Chaource {
    private final CamembertdeNormandie a;
    private final AbbayedeCiteauxentiere b;
    private final AffideliceauChablis c;

    private Chaource(CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere, AffideliceauChablis affideliceauChablis) {
        this.a = camembertdeNormandie;
        this.b = abbayedeCiteauxentiere;
        this.c = affideliceauChablis;
    }

    public /* synthetic */ Chaource(CamembertdeNormandie camembertdeNormandie, AbbayedeCiteauxentiere abbayedeCiteauxentiere) {
        this(camembertdeNormandie, abbayedeCiteauxentiere, AffideliceauChablis.a);
    }

    public final void a(Activity activity) {
        if (AffideliceauChablis.c() && this.a.getParent() == null && this.b.g()) {
            AffideliceauChablis.a(true);
            activity.addContentView(this.a, this.a.getLayoutParams());
            b(activity);
        }
    }

    private final void b(Activity activity) {
        if (activity.hasWindowFocus()) {
            this.b.m();
        } else {
            this.b.l();
        }
    }

    public final void a() {
        if (this.a.getParent() != null && !this.b.r()) {
            this.b.l();
            this.a.a();
        }
    }
}
