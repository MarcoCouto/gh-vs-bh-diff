package io.presage;

import java.util.ArrayList;
import java.util.List;

public final class FourmedeRochefort implements GaletteLyonnaise {
    private final List<GaletteLyonnaise> a = new ArrayList();

    public final void a() {
        for (GaletteLyonnaise a2 : this.a) {
            a2.a();
        }
        this.a.clear();
    }

    public final void a(GaletteLyonnaise galetteLyonnaise) {
        this.a.add(galetteLyonnaise);
    }
}
