package io.presage;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import io.presage.common.AdConfig;
import io.presage.interstitial.ui.InterstitialActivity;
import java.util.List;

public final class Cheddar {
    private EcirdelAubrac a;
    /* access modifiers changed from: private */
    public CendreduBeauzac b;
    private AbbayedeTimadeuc c;
    private final Context d;
    private final Chambertin e;
    private final AbbayeduMontdesCats f;
    private final CremeuxduJura g;
    private final EtivazGruyereSuisse h;

    public static final class CamembertauCalvados implements Appenzeller {
        final /* synthetic */ Cheddar a;
        final /* synthetic */ Activity b;
        final /* synthetic */ int c;
        final /* synthetic */ int d;

        CamembertauCalvados(Cheddar cheddar, Activity activity, int i, int i2) {
            this.a = cheddar;
            this.b = activity;
            this.c = i;
            this.d = i2;
        }

        public final void a(Context context, List<PontlEveque> list) {
            CendreduBeauzac a2 = this.a.b;
            if (a2 != null) {
                a2.a(this.b, this.c, this.d, list);
            }
        }
    }

    private Cheddar(Context context, Chambertin chambertin, AbbayeduMontdesCats abbayeduMontdesCats, CremeuxduJura cremeuxduJura, EtivazGruyereSuisse etivazGruyereSuisse) {
        this.d = context;
        this.e = chambertin;
        this.f = abbayeduMontdesCats;
        this.g = cremeuxduJura;
        this.h = etivazGruyereSuisse;
    }

    private /* synthetic */ Cheddar(Context context, Chambertin chambertin, AbbayeduMontdesCats abbayeduMontdesCats) {
        this(context, chambertin, abbayeduMontdesCats, new CremeuxduJura(), new EtivazGruyereSuisse());
    }

    public Cheddar(Context context, AdConfig adConfig, SableduBoulonnais sableduBoulonnais) {
        this(context, new Chambertin(InterstitialActivity.a), new AbbayeduMontdesCats(context, adConfig, sableduBoulonnais));
    }

    public final boolean a() {
        AbbayedeTimadeuc abbayedeTimadeuc = this.c;
        if (abbayedeTimadeuc != null) {
            return abbayedeTimadeuc.a();
        }
        return false;
    }

    public final void a(EcirdelAubrac ecirdelAubrac) {
        this.a = ecirdelAubrac;
        AbbayedeTimadeuc abbayedeTimadeuc = this.c;
        if (abbayedeTimadeuc != null) {
            abbayedeTimadeuc.a(this.a);
        }
    }

    public final void a(int i, int i2) {
        AbbayedeTimadeuc abbayedeTimadeuc = this.c;
        if (abbayedeTimadeuc != null && abbayedeTimadeuc.c()) {
            CendreduBeauzac cendreduBeauzac = this.b;
            if (cendreduBeauzac != null) {
                cendreduBeauzac.a();
            }
            AbbayedeTimadeuc abbayedeTimadeuc2 = this.c;
            if (abbayedeTimadeuc2 != null) {
                abbayedeTimadeuc2.e();
            }
        }
        b();
        b(i, i2);
        AbbayedeTimadeuc abbayedeTimadeuc3 = this.c;
        if (abbayedeTimadeuc3 != null) {
            abbayedeTimadeuc3.d();
        }
    }

    private final void b(int i, int i2) {
        this.c = this.f.a();
        AbbayedeTimadeuc abbayedeTimadeuc = this.c;
        if (abbayedeTimadeuc != null) {
            abbayedeTimadeuc.a(this.a);
        }
        AbbayedeTimadeuc abbayedeTimadeuc2 = this.c;
        if (abbayedeTimadeuc2 != null) {
            abbayedeTimadeuc2.a(new Soumaintrain(i, i2));
        }
    }

    private final void b() {
        Chambertin chambertin = this.e;
        Context applicationContext = this.d.getApplicationContext();
        if (applicationContext != null) {
            this.b = chambertin.a((Application) applicationContext, this.g, this.h);
            return;
        }
        throw new em("null cannot be cast to non-null type android.app.Application");
    }

    public final void a(Activity activity, int i, int i2) {
        if (this.c == null) {
            EcirdelAubrac ecirdelAubrac = this.a;
            if (ecirdelAubrac != null) {
                ecirdelAubrac.d();
            }
        }
        AbbayedeTimadeuc abbayedeTimadeuc = this.c;
        if (abbayedeTimadeuc != null) {
            abbayedeTimadeuc.a((Appenzeller) new CamembertauCalvados(this, activity, i, i2));
        }
    }

    public final void a(String[] strArr) {
        this.g.a(er.c(strArr));
    }

    public final void a(Class<? extends Activity>[] clsArr) {
        this.g.b(er.c(clsArr));
    }

    public final void b(String[] strArr) {
        this.h.a(er.a(strArr));
    }

    public final void b(Class<? extends Object>[] clsArr) {
        this.h.b(er.a(clsArr));
    }
}
