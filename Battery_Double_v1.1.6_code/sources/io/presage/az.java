package io.presage;

public final class az {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private static String a() {
        return "window.MRAID_ENV =  { version: '3.0', sdk: 'Presage', sdkVersion: '3.3.9-moat'};";
    }

    private static String b(SaintFelicien saintFelicien) {
        StringBuilder sb = new StringBuilder("window.MRAID_ENV =  { version: '3.0', sdk: 'Presage', sdkVersion: '3.3.9-moat', adUnit: { type: '");
        sb.append(saintFelicien.c());
        sb.append("', reward : { name: '");
        sb.append(saintFelicien.e().getName());
        sb.append("', value: '");
        sb.append(saintFelicien.e().getValue());
        sb.append("', launch: '");
        sb.append(saintFelicien.d());
        sb.append("'}}};");
        return sb.toString();
    }

    public static String a(SaintFelicien saintFelicien) {
        if (saintFelicien == null || !hl.a((Object) saintFelicien.c(), (Object) "optin_video")) {
            return a();
        }
        return b(saintFelicien);
    }
}
