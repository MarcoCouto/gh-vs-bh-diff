package io.presage;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import java.util.List;

public final class BrieauPoivre {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private FourmedeRochefort b;
    private final ViewGroup c;
    private final BriedeMeaux d;
    private final BrillatSavarin e;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    static final class CamembertdeNormandie extends hm implements gf<av> {
        final /* synthetic */ BrieauPoivre a;
        final /* synthetic */ View b;

        CamembertdeNormandie(BrieauPoivre brieauPoivre, View view) {
            this.a = brieauPoivre;
            this.b = view;
            super(0);
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public av a() {
            return this.a.a((db) this.b);
        }
    }

    static final class EcirdelAubrac extends hm implements gg<av, ep> {
        final /* synthetic */ BrieauPoivre a;
        final /* synthetic */ View b;

        EcirdelAubrac(BrieauPoivre brieauPoivre, View view) {
            this.a = brieauPoivre;
            this.b = view;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((av) obj);
            return ep.a;
        }

        private void a(av avVar) {
            BrieauPoivre.b((db) this.b, avVar);
        }
    }

    private BrieauPoivre(ViewGroup viewGroup, BriedeMeaux briedeMeaux, BrillatSavarin brillatSavarin) {
        this.c = viewGroup;
        this.d = briedeMeaux;
        this.e = brillatSavarin;
        this.b = new FourmedeRochefort();
    }

    public /* synthetic */ BrieauPoivre(ViewGroup viewGroup) {
        this(viewGroup, new BriedeMeaux(), new BrillatSavarin());
    }

    public final void a() {
        this.b.a();
        int childCount = this.c.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.c.getChildAt(i);
            if ((childAt instanceof db) && ((db) childAt).getContainsMraid()) {
                io.presage.Goudaauxepices.CamembertauCalvados camembertauCalvados = Goudaauxepices.a;
                this.b.a(io.presage.Goudaauxepices.CamembertauCalvados.a(new CamembertdeNormandie(this, childAt)).b((gg<? super T, ep>) new EcirdelAubrac<Object,ep>(this, childAt)));
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(db dbVar, av avVar) {
        if (dbVar.a() && dbVar.b()) {
            dbVar.getMraidCommandExecutor().a(avVar);
        }
    }

    /* access modifiers changed from: private */
    public final av a(db dbVar) {
        av avVar = new av();
        View rootView = this.c.getRootView();
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView;
            Rect a2 = BriedeMeaux.a(dbVar);
            Rect b2 = BriedeMeaux.b(this.c);
            if (!a2.intersect(b2)) {
                return avVar;
            }
            List a3 = BriedeMeaux.a(this.e.a(viewGroup, (WebView) dbVar), a2);
            int a4 = BriedeMeaux.a(a2, a3);
            int measuredWidth = dbVar.getMeasuredWidth() * dbVar.getMeasuredHeight();
            int a5 = measuredWidth - BriedeMelun.a(a2);
            if (a5 < 0) {
                a5 = 0;
            }
            float f = (float) measuredWidth;
            if (measuredWidth != 0) {
                avVar.a(100.0f - ((((float) (a4 + a5)) * 100.0f) / f));
            } else {
                avVar.a(0.0f);
            }
            if (avVar.c() != 0.0f) {
                BriedeMelun.a(a3, b2);
                BriedeMelun.a(a2, b2);
                avVar.a(a3);
                avVar.a(a2);
            }
            return avVar;
        }
        throw new em("null cannot be cast to non-null type android.view.ViewGroup");
    }

    public final void b() {
        this.b.a();
    }
}
