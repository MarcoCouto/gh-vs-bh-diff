package io.presage;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public final class dj {
    public static final dj a = new dj();

    private dj() {
    }

    /* JADX INFO: finally extract failed */
    public static byte[] a(String str) throws Exception {
        if (str.length() == 0) {
            return new byte[0];
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        try {
            Charset forName = Charset.forName("UTF-8");
            hl.a((Object) forName, "Charset.forName(charsetName)");
            byte[] bytes = str.getBytes(forName);
            hl.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            gZIPOutputStream.write(bytes);
            dg.a(gZIPOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            hl.a((Object) byteArray, "obj.toByteArray()");
            return byteArray;
        } catch (Throwable th) {
            dg.a(gZIPOutputStream);
            throw th;
        }
    }

    public static String a(byte[] bArr) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new ByteArrayInputStream(bArr)), "UTF-8"));
        try {
            String a2 = gd.a(bufferedReader);
            return a2;
        } finally {
            dg.a(bufferedReader);
        }
    }
}
