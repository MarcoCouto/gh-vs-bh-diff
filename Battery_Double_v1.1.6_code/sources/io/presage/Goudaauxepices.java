package io.presage;

public final class Goudaauxepices<T> implements GaletteLyonnaise {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public gg<? super Throwable, ep> c;
    /* access modifiers changed from: private */
    public final gf<T> d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static <T> Goudaauxepices<T> a(gf<? extends T> gfVar) {
            return new Goudaauxepices<>(gfVar, 0);
        }
    }

    static final class CamembertdeNormandie implements Runnable {
        final /* synthetic */ Goudaauxepices a;
        final /* synthetic */ gg b;
        final /* synthetic */ Object c;

        CamembertdeNormandie(Goudaauxepices goudaauxepices, gg ggVar, Object obj) {
            this.a = goudaauxepices;
            this.b = ggVar;
            this.c = obj;
        }

        public final void run() {
            if (!this.a.b) {
                this.b.a(this.c);
            }
        }
    }

    static final class EcirdelAubrac implements Runnable {
        final /* synthetic */ Goudaauxepices a;
        final /* synthetic */ gg b;

        EcirdelAubrac(Goudaauxepices goudaauxepices, gg ggVar) {
            this.a = goudaauxepices;
            this.b = ggVar;
        }

        public final void run() {
            try {
                this.a.a(this.b, this.a.d.a());
            } catch (Throwable th) {
                gg b2 = this.a.c;
                if (b2 != null) {
                    this.a.a(b2, th);
                }
            }
        }
    }

    private Goudaauxepices(gf<? extends T> gfVar) {
        this.d = gfVar;
    }

    public /* synthetic */ Goudaauxepices(gf gfVar, byte b2) {
        this(gfVar);
    }

    public final void a() {
        this.c = null;
        this.b = true;
    }

    public final Goudaauxepices<T> a(gg<? super Throwable, ep> ggVar) {
        this.c = ggVar;
        return this;
    }

    public final GaletteLyonnaise b(gg<? super T, ep> ggVar) {
        Gouda36mois gouda36mois = Gouda36mois.a;
        Gouda36mois.b().a(new EcirdelAubrac(this, ggVar));
        return this;
    }

    /* access modifiers changed from: private */
    public final <R> void a(gg<? super R, ep> ggVar, R r) {
        Gouda36mois gouda36mois = Gouda36mois.a;
        Gouda36mois.c().a(new CamembertdeNormandie(this, ggVar, r));
    }
}
