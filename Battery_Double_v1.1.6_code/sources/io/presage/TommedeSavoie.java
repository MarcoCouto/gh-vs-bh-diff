package io.presage;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.facebook.internal.NativeProtocol;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

public final class TommedeSavoie {
    private final Taleggio a;
    private final TetedeMoine b;

    public TommedeSavoie(Taleggio taleggio, TetedeMoine tetedeMoine) {
        this.a = taleggio;
        this.b = tetedeMoine;
    }

    public final JSONObject a(c cVar) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(TapjoyConstants.TJC_DEVICE_TIMEZONE, Taleggio.e());
        jSONObject.put("aaid", cVar.a());
        jSONObject.put(TapjoyConstants.TJC_DEVICE_LANGUAGE, this.b.b());
        jSONObject.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, this.b.c());
        jSONObject.put("install_unknown_sources", this.a.g());
        jSONObject.put("aaid_optin", cVar.b());
        jSONObject.put("fake_aaid", cVar.c());
        jSONObject.put("device", a());
        jSONObject.put(NativeProtocol.RESULT_ARGS_PERMISSIONS, c());
        return jSONObject;
    }

    private final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("name", this.a.h());
        jSONObject.put("screen", b());
        jSONObject.put(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, Taleggio.a());
        jSONObject.put("vm_name", Taleggio.c());
        jSONObject.put("phone_arch", Taleggio.d());
        jSONObject.put("vm_version", Taleggio.b());
        return jSONObject;
    }

    private static JSONObject b() {
        Resources system = Resources.getSystem();
        hl.a((Object) system, "Resources.getSystem()");
        DisplayMetrics displayMetrics = system.getDisplayMetrics();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("density", Float.valueOf(displayMetrics.density));
        jSONObject.put("height", displayMetrics.heightPixels);
        jSONObject.put("width", displayMetrics.widthPixels);
        return jSONObject;
    }

    private final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("ACCESS_NETWORK_STATE", this.b.a("android.permission.ACCESS_NETWORK_STATE"));
        jSONObject.put("RECEIVE_BOOT_COMPLETED", this.b.a("android.permission.RECEIVE_BOOT_COMPLETED"));
        jSONObject.put("SYSTEM_ALERT_WINDOW", this.b.a("android.permission.SYSTEM_ALERT_WINDOW"));
        jSONObject.put("GET_ACCOUNTS", this.b.a("android.permission.GET_ACCOUNTS"));
        return jSONObject;
    }
}
