package io.presage;

import android.graphics.Rect;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class BriedeMeaux {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private Camembertbio b = Camembertbio.a;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public static Rect a(View view) {
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        return rect;
    }

    public static Rect b(View view) {
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        return rect;
    }

    public static List<Rect> a(List<? extends View> list, Rect rect) {
        List<Rect> arrayList = new ArrayList<>();
        for (View a2 : list) {
            Rect a3 = a(a2);
            Rect rect2 = new Rect(rect);
            if (rect2.intersect(a3)) {
                arrayList.add(rect2);
            }
        }
        return arrayList;
    }

    public static int a(Rect rect, List<Rect> list) {
        int i = rect.right;
        int i2 = 0;
        for (int i3 = rect.left; i3 < i; i3++) {
            int i4 = rect.bottom;
            for (int i5 = rect.top; i5 < i4; i5++) {
                Iterator it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((Rect) it.next()).contains(i3, i5)) {
                            i2++;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        return i2;
    }

    public static cu a(View view, cu cuVar) {
        if (cuVar == null || view == null) {
            return null;
        }
        Rect a2 = cuVar.a();
        Rect c = c(view);
        a2.offset(c.left, c.top);
        return a(view, a2);
    }

    public static cu a(View view, View view2) {
        return a(view2, c(view));
    }

    private static cu a(View view, Rect rect) {
        Rect c = c(view);
        if (b(view, c)) {
            return null;
        }
        BriquetteduNord a2 = Camembertbio.a(rect, c, 1.0f);
        a2.b();
        a2.a();
        rect.offset(-c.left, -c.top);
        io.presage.cu.CamembertauCalvados camembertauCalvados = cu.a;
        return io.presage.cu.CamembertauCalvados.a(rect);
    }

    private static boolean b(View view, Rect rect) {
        View rootView = view.getRootView();
        hl.a((Object) rootView, "container.rootView");
        return ((float) rect.height()) <= ((float) rootView.getHeight()) * 0.4f;
    }

    private static Rect c(View view) {
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        if (rect.width() != view.getWidth()) {
            rect.right = rect.left + view.getWidth();
        }
        if (rect.height() != view.getHeight()) {
            rect.bottom = rect.top + view.getHeight();
        }
        return rect;
    }
}
