package io.presage;

import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONObject;

public abstract class co implements be {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private Laguiole b = Laguiole.a;
    private final ax c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public void a() {
    }

    public void a(SaintFelicien saintFelicien) {
    }

    public void a(cu cuVar) {
    }

    public void a(String str) {
    }

    public void a(String str, String str2) {
    }

    public void a(Map<String, String> map, String str) {
    }

    public void a(boolean z) {
    }

    public void b() {
    }

    public void b(String str) {
    }

    public void b(String str, String str2) {
    }

    public void c() {
    }

    public void d() {
    }

    public void e() {
    }

    public co(ax axVar) {
        this.c = axVar;
    }

    public final boolean a(String str, db dbVar, SaintFelicien saintFelicien) {
        ak.a(str);
        if (a(dbVar)) {
            a(str, saintFelicien);
        }
        return true;
    }

    private static boolean a(db dbVar) {
        return (hl.a((Object) dbVar.getAdState(), (Object) "hidden") ^ true) && (hl.a((Object) dbVar.getAdState(), (Object) "loading") ^ true);
    }

    private final void a(String str, SaintFelicien saintFelicien) {
        String str2;
        Map linkedHashMap = new LinkedHashMap();
        String str3 = "";
        try {
            JSONObject c2 = c(str);
            str2 = c2.optString("method", "");
            hl.a((Object) str2, "json.optString(\"method\", \"\")");
            String str4 = String.CALLBACK_ID;
            try {
                String optString = c2.optString(String.CALLBACK_ID);
                hl.a((Object) optString, "json.optString(\"callbackId\")");
                linkedHashMap.put(str4, optString);
                Object opt = c2.opt("args");
                if (opt == null) {
                    opt = "";
                }
                a(opt, linkedHashMap);
                a(str2, linkedHashMap, opt.toString(), saintFelicien);
            } catch (Exception e) {
                e = e;
                Laguiole.a(e);
                this.c.a(str2, "");
            }
        } catch (Exception e2) {
            e = e2;
            str2 = str3;
            Laguiole.a(e);
            this.c.a(str2, "");
        }
    }

    private static void a(Object obj, Map<String, String> map) {
        if (obj != null && (obj instanceof JSONObject)) {
            JSONObject jSONObject = (JSONObject) obj;
            Iterator keys = jSONObject.keys();
            hl.a((Object) keys, "keys");
            while (keys.hasNext()) {
                String str = (String) keys.next();
                hl.a((Object) str, ParametersKeys.KEY);
                String optString = jSONObject.optString(str, "");
                hl.a((Object) optString, "args.optString(key, \"\")");
                map.put(str, optString);
            }
        }
    }

    private static JSONObject c(String str) {
        int a2 = iv.a((CharSequence) str, "/?q=") + 4;
        if (str != null) {
            String substring = str.substring(a2);
            hl.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
            return new JSONObject(ak.a(substring));
        }
        throw new em("null cannot be cast to non-null type java.lang.String");
    }

    private final void a(String str, Map<String, String> map, String str2, SaintFelicien saintFelicien) {
        switch (str.hashCode()) {
            case -1289167206:
                if (str.equals("expand")) {
                    d();
                    break;
                }
                break;
            case -984419449:
                if (str.equals("ogyResolveIntent")) {
                    String str3 = (String) map.get("intentUri");
                    String str4 = (String) map.get(String.CALLBACK_ID);
                    if (str4 == null) {
                        str4 = "";
                    }
                    a(str3, str4);
                    return;
                }
                break;
            case -934437708:
                if (str.equals(MraidJsMethods.RESIZE)) {
                    f();
                    return;
                }
                break;
            case -840442113:
                if (str.equals(MraidJsMethods.UNLOAD)) {
                    b();
                    return;
                }
                break;
            case 3417674:
                if (str.equals(MraidJsMethods.OPEN)) {
                    a((String) map.get("url"));
                    return;
                }
                break;
            case 94756344:
                if (str.equals("close")) {
                    a(map);
                    return;
                }
                break;
            case 451326307:
                if (str.equals("ogyCreateShortcut")) {
                    a(map, str2);
                    return;
                }
                break;
            case 624734601:
                if (str.equals("setResizeProperties")) {
                    e(map);
                    return;
                }
                break;
            case 901631159:
                if (str.equals("ogyOnAdEvent")) {
                    a(saintFelicien);
                    return;
                }
                break;
            case 1614272768:
                if (str.equals("useCustomClose")) {
                    d(map);
                    return;
                }
                break;
            case 1622028878:
                if (str.equals("ogyForceClose")) {
                    b(map);
                    return;
                }
                break;
            case 1805873469:
                if (str.equals("ogyStartIntent")) {
                    String str5 = (String) map.get("intentUri");
                    String str6 = (String) map.get(String.CALLBACK_ID);
                    if (str6 == null) {
                        str6 = "";
                    }
                    b(str5, str6);
                    return;
                }
                break;
        }
    }

    private final void a(Map<String, String> map) {
        a();
        c(map);
    }

    private final void b(Map<String, String> map) {
        e();
        c(map);
    }

    private final void c(Map<String, String> map) {
        String str = (String) map.get("showNextAd");
        if (str == null) {
            str = "true";
        }
        String str2 = (String) map.get("nextAdId");
        if (str2 == null) {
            str2 = "";
        }
        if (hl.a((Object) str, (Object) "true")) {
            b(str2);
        }
    }

    private final void d(Map<String, String> map) {
        String str = (String) map.get("useCustomClose");
        a(str != null ? str.equals("false") : false);
    }

    private final void e(Map<String, String> map) {
        io.presage.cu.CamembertauCalvados camembertauCalvados = cu.a;
        cu a2 = io.presage.cu.CamembertauCalvados.a(map);
        a(a2);
        if (a2 == null) {
            this.c.a("setResizeProperties", "Wrong parameters");
        }
    }

    private final void f() {
        try {
            c();
        } catch (Throwable th) {
            ax axVar = this.c;
            String str = MraidJsMethods.RESIZE;
            String message = th.getMessage();
            if (message == null) {
                message = "";
            }
            axVar.a(str, message);
        }
    }
}
