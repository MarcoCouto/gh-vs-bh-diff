package io.presage;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class fd extends fc {
    public static final <T> void a(List<T> list, Comparator<? super T> comparator) {
        if (list.size() > 1) {
            Collections.sort(list, comparator);
        }
    }
}
