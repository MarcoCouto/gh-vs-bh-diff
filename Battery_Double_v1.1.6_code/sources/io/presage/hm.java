package io.presage;

import java.io.Serializable;

public abstract class hm<R> implements hj<R>, Serializable {
    private final int a;

    public hm(int i) {
        this.a = i;
    }

    public String toString() {
        String a2 = hn.a(this);
        hl.a((Object) a2, "Reflection.renderLambdaToString(this)");
        return a2;
    }
}
