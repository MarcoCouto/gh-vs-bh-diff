package io.presage;

public final class FourmedeMontbrison implements GaletteLyonnaise {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public gg<? super Throwable, ep> c;
    /* access modifiers changed from: private */
    public final gf<ep> d;

    static final class AbbayedeTamie implements Runnable {
        final /* synthetic */ FourmedeMontbrison a;
        final /* synthetic */ gf b;

        AbbayedeTamie(FourmedeMontbrison fourmedeMontbrison, gf gfVar) {
            this.a = fourmedeMontbrison;
            this.b = gfVar;
        }

        public final void run() {
            try {
                this.a.d.a();
                this.a.b(this.b);
            } catch (Throwable th) {
                gg b2 = this.a.c;
                if (b2 != null) {
                    this.a.a(b2, th);
                }
            }
        }
    }

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static FourmedeMontbrison a(gf<ep> gfVar) {
            return new FourmedeMontbrison(gfVar, 0);
        }
    }

    static final class CamembertdeNormandie implements Runnable {
        final /* synthetic */ FourmedeMontbrison a;
        final /* synthetic */ gf b;

        CamembertdeNormandie(FourmedeMontbrison fourmedeMontbrison, gf gfVar) {
            this.a = fourmedeMontbrison;
            this.b = gfVar;
        }

        public final void run() {
            if (!this.a.b) {
                this.b.a();
            }
        }
    }

    static final class EcirdelAubrac implements Runnable {
        final /* synthetic */ FourmedeMontbrison a;
        final /* synthetic */ gg b;
        final /* synthetic */ Object c;

        EcirdelAubrac(FourmedeMontbrison fourmedeMontbrison, gg ggVar, Object obj) {
            this.a = fourmedeMontbrison;
            this.b = ggVar;
            this.c = obj;
        }

        public final void run() {
            if (!this.a.b) {
                this.b.a(this.c);
            }
        }
    }

    private FourmedeMontbrison(gf<ep> gfVar) {
        this.d = gfVar;
    }

    public /* synthetic */ FourmedeMontbrison(gf gfVar, byte b2) {
        this(gfVar);
    }

    public final void a() {
        this.c = null;
        this.b = true;
    }

    public final FourmedeMontbrison a(gg<? super Throwable, ep> ggVar) {
        this.c = ggVar;
        return this;
    }

    public final GaletteLyonnaise a(gf<ep> gfVar) {
        Gouda36mois gouda36mois = Gouda36mois.a;
        Gouda36mois.b().a(new AbbayedeTamie(this, gfVar));
        return this;
    }

    /* access modifiers changed from: private */
    public final void b(gf<ep> gfVar) {
        Gouda36mois gouda36mois = Gouda36mois.a;
        Gouda36mois.c().a(new CamembertdeNormandie(this, gfVar));
    }

    /* access modifiers changed from: private */
    public final <R> void a(gg<? super R, ep> ggVar, R r) {
        Gouda36mois gouda36mois = Gouda36mois.a;
        Gouda36mois.c().a(new EcirdelAubrac(this, ggVar, r));
    }
}
