package io.presage;

public final class Gouda36mois implements Goudaaucumin {
    public static final Gouda36mois a;
    private static Goudaaucumin b;
    private static final ee c = ef.a(CamembertauCalvados.a);
    private static final ee d = ef.a(CamembertdeNormandie.a);

    static final class CamembertauCalvados extends hm implements gf<GorgonzolaPiccante> {
        public static final CamembertauCalvados a = new CamembertauCalvados();

        CamembertauCalvados() {
            super(0);
        }

        public final /* synthetic */ Object a() {
            return b();
        }

        private static GorgonzolaPiccante b() {
            Gouda36mois gouda36mois = Gouda36mois.a;
            return Gouda36mois.a().e();
        }
    }

    static final class CamembertdeNormandie extends hm implements gf<GorgonzolaPiccante> {
        public static final CamembertdeNormandie a = new CamembertdeNormandie();

        CamembertdeNormandie() {
            super(0);
        }

        public final /* synthetic */ Object a() {
            return b();
        }

        private static GorgonzolaPiccante b() {
            Gouda36mois gouda36mois = Gouda36mois.a;
            return Gouda36mois.a().d();
        }
    }

    private static GorgonzolaPiccante f() {
        return (GorgonzolaPiccante) c.a();
    }

    private static GorgonzolaPiccante g() {
        return (GorgonzolaPiccante) d.a();
    }

    static {
        Gouda36mois gouda36mois = new Gouda36mois();
        a = gouda36mois;
        b = gouda36mois;
    }

    private Gouda36mois() {
    }

    public static Goudaaucumin a() {
        return b;
    }

    public static GorgonzolaPiccante b() {
        return f();
    }

    public static GorgonzolaPiccante c() {
        return g();
    }

    public final GorgonzolaPiccante d() {
        return new Gaperon();
    }

    public final GorgonzolaPiccante e() {
        return new FourmedeHauteLoire();
    }
}
