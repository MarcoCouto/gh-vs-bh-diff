package io.presage;

import android.app.Application;
import com.moat.analytics.mobile.ogury.MoatAnalytics;
import com.moat.analytics.mobile.ogury.MoatFactory;

public final class at {
    private boolean a;
    private final MoatAnalytics b;

    public at(MoatAnalytics moatAnalytics) {
        this.b = moatAnalytics;
    }

    public final void a(Application application) {
        this.a = true;
        this.b.start(application);
    }

    public static MoatFactory a() {
        MoatFactory create = MoatFactory.create();
        hl.a((Object) create, "MoatFactory.create()");
        return create;
    }
}
