package io.presage;

import java.io.ObjectStreamException;
import java.io.Serializable;

public abstract class he implements ie, Serializable {
    public static final Object b = CamembertauCalvados.a;
    protected final Object a;
    private transient ie c;

    static class CamembertauCalvados implements Serializable {
        /* access modifiers changed from: private */
        public static final CamembertauCalvados a = new CamembertauCalvados();

        private CamembertauCalvados() {
        }

        private Object readResolve() throws ObjectStreamException {
            return a;
        }
    }

    /* access modifiers changed from: protected */
    public abstract ie e();

    protected he(Object obj) {
        this.a = obj;
    }

    public final Object f() {
        return this.a;
    }

    public final ie g() {
        ie ieVar = this.c;
        if (ieVar != null) {
            return ieVar;
        }
        ie e = e();
        this.c = e;
        return e;
    }

    public ig b() {
        throw new AbstractMethodError();
    }

    public String c() {
        throw new AbstractMethodError();
    }

    public String d() {
        throw new AbstractMethodError();
    }
}
