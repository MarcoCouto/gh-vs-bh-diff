package io.presage;

import java.lang.ref.WeakReference;

public final class bx {
    private final WeakReference<cb> a;
    private final db b;
    private final PontlEveque c;
    private final long d;

    private bx(WeakReference<cb> weakReference, db dbVar, PontlEveque pontlEveque, long j) {
        this.a = weakReference;
        this.b = dbVar;
        this.c = pontlEveque;
        this.d = j;
    }

    public final WeakReference<cb> a() {
        return this.a;
    }

    public final db b() {
        return this.b;
    }

    public final PontlEveque c() {
        return this.c;
    }

    public /* synthetic */ bx(WeakReference weakReference, db dbVar, PontlEveque pontlEveque) {
        this(weakReference, dbVar, pontlEveque, System.currentTimeMillis());
    }

    public final long d() {
        return this.d;
    }
}
