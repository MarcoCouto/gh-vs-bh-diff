package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

@SuppressLint({"StaticFieldLeak"})
public final class by {
    public static final by a = new by();
    private static final Map<String, bx> b = new ConcurrentHashMap();
    private static Handler c = new Handler(Looper.getMainLooper());
    private static final Runnable d = CamembertauCalvados.a;
    private static Integer e;
    private static k f = k.a;

    static final class CamembertauCalvados implements Runnable {
        public static final CamembertauCalvados a = new CamembertauCalvados();

        CamembertauCalvados() {
        }

        public final void run() {
            by byVar = by.a;
            by.d();
        }
    }

    private by() {
    }

    public static void a(cb cbVar) {
        Iterator it = b.entrySet().iterator();
        while (it.hasNext()) {
            if (hl.a((Object) (cb) ((bx) ((Entry) it.next()).getValue()).a().get(), (Object) cbVar)) {
                it.remove();
            }
        }
    }

    public static void a(String str) {
        Iterator it = b.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (hl.a((Object) (String) entry.getKey(), (Object) str)) {
                it.remove();
                cb cbVar = (cb) ((bx) entry.getValue()).a().get();
                if (cbVar != null) {
                    cbVar.b(((bx) entry.getValue()).c());
                }
            }
        }
    }

    public static void a(bx bxVar) {
        b(bxVar);
        b.put(bxVar.c().a(), bxVar);
        c();
    }

    private static void b(bx bxVar) {
        if (e == null) {
            Context context = bxVar.b().getContext();
            hl.a((Object) context, "mraidCacheItem.webView.context");
            j a2 = k.a(context);
            if (a2 != null) {
                e = a2.h() > 0 ? Integer.valueOf(a2.h()) : null;
            }
        }
    }

    public static void a() {
        Iterator it = b.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (c((bx) entry.getValue())) {
                it.remove();
                MoelleuxduRevard moelleuxduRevard = MoelleuxduRevard.a;
                MoelleuxduRevard.a((Mimolette24mois) new Munster("expired", ((bx) entry.getValue()).c()));
                cb cbVar = (cb) ((bx) entry.getValue()).a().get();
                if (cbVar != null) {
                    cbVar.a(((bx) entry.getValue()).c());
                }
            }
        }
    }

    private static boolean c(bx bxVar) {
        Context context = bxVar.b().getContext();
        hl.a((Object) context, "mraidCacheItem.webView.context");
        j a2 = k.a(context);
        return a2 != null && System.currentTimeMillis() - bxVar.d() > a2.l();
    }

    public static db b(String str) {
        db dbVar = null;
        if (!b.containsKey(str)) {
            return null;
        }
        bx bxVar = (bx) b.get(str);
        if (bxVar != null) {
            dbVar = bxVar.b();
        }
        b.remove(str);
        return dbVar;
    }

    private static void c() {
        Iterator it = b.entrySet().iterator();
        while (it.hasNext()) {
            if (((bx) ((Entry) it.next()).getValue()).a().get() == null) {
                it.remove();
            }
        }
        c.removeCallbacksAndMessages(null);
        if (!b.isEmpty()) {
            c.postDelayed(d, 1200000);
        }
    }

    /* access modifiers changed from: private */
    public static void d() {
        new StringBuilder("clean cache ").append(b.size());
        a();
        c();
        new StringBuilder("after cache ").append(b.size());
    }
}
