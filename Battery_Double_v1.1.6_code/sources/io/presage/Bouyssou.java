package io.presage;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public final class Bouyssou {
    public static final Bouyssou a = new Bouyssou();
    private static final Map<String, BoulettedAvesnes> b;

    static {
        Map<String, BoulettedAvesnes> synchronizedMap = Collections.synchronizedMap(new LinkedHashMap());
        hl.a((Object) synchronizedMap, "Collections.synchronizedMap(mutableMapOf())");
        b = synchronizedMap;
    }

    private Bouyssou() {
    }

    public static String a(BoulettedAvesnes boulettedAvesnes) {
        String uuid = UUID.randomUUID().toString();
        hl.a((Object) uuid, "UUID.randomUUID().toString()");
        b.put(uuid, boulettedAvesnes);
        return uuid;
    }

    public static BoulettedAvesnes a(String str) {
        return (BoulettedAvesnes) b.remove(str);
    }
}
