package io.presage;

public final class ho {
    public static ih a(hk hkVar) {
        return hkVar;
    }

    public static Cif a(Class cls) {
        return new hg(cls);
    }

    public static String a(hm hmVar) {
        return a((hj) hmVar);
    }

    private static String a(hj hjVar) {
        String obj = hjVar.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
