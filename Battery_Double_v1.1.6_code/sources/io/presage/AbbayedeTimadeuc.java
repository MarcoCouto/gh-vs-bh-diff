package io.presage;

import android.content.Context;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import io.presage.common.AdConfig;
import io.presage.common.PresageSdk;
import io.presage.common.PresageSdkInitCallback;
import io.presage.common.network.models.RewardItem;
import java.util.ArrayList;
import java.util.List;

public final class AbbayedeTimadeuc implements cb {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public boolean b;
    private String c;
    /* access modifiers changed from: private */
    public List<PontlEveque> d;
    private EcirdelAubrac e;
    private PresageSdk f;
    private gg<? super RewardItem, ep> g;
    private Soumaintrain h;
    private boolean i;
    private boolean j;
    private final Context k;
    private final AdConfig l;
    private final by m;
    private final k n;
    private final MoelleuxduRevard o;
    private final bb p;
    private final bz q;
    private final MunsterauCumin r;
    private final AbbayedeTamie s;
    private final h t;
    private final SableduBoulonnais u;

    static final class AbbayedeTamie extends hm implements gg<Throwable, ep> {
        final /* synthetic */ AbbayedeTimadeuc a;

        AbbayedeTamie(AbbayedeTimadeuc abbayedeTimadeuc) {
            this.a = abbayedeTimadeuc;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a();
            return ep.a;
        }

        private void a() {
            EcirdelAubrac b = this.a.b();
            if (b != null) {
                b.a(3);
            }
        }
    }

    /* renamed from: io.presage.AbbayedeTimadeuc$AbbayedeTimadeuc reason: collision with other inner class name */
    static final class C0118AbbayedeTimadeuc extends hm implements gg<j, ep> {
        final /* synthetic */ AbbayedeTimadeuc a;

        C0118AbbayedeTimadeuc(AbbayedeTimadeuc abbayedeTimadeuc) {
            this.a = abbayedeTimadeuc;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((j) obj);
            return ep.a;
        }

        private void a(j jVar) {
            this.a.a(jVar);
        }
    }

    static final class AbbayeduMontdesCats extends hm implements gg<Throwable, ep> {
        final /* synthetic */ AbbayedeTimadeuc a;

        AbbayeduMontdesCats(AbbayedeTimadeuc abbayedeTimadeuc) {
            this.a = abbayedeTimadeuc;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a();
            return ep.a;
        }

        private void a() {
            EcirdelAubrac b = this.a.b();
            if (b != null) {
                b.a(0);
            }
        }
    }

    static final class AffideliceauChablis extends hm implements gg<RouedeBrie, ep> {
        final /* synthetic */ AbbayedeTimadeuc a;

        AffideliceauChablis(AbbayedeTimadeuc abbayedeTimadeuc) {
            this.a = abbayedeTimadeuc;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((RouedeBrie) obj);
            return ep.a;
        }

        private void a(RouedeBrie rouedeBrie) {
            this.a.a(rouedeBrie);
        }
    }

    public static final class Appenzeller implements ca {
        final /* synthetic */ AbbayedeTimadeuc a;

        Appenzeller(AbbayedeTimadeuc abbayedeTimadeuc) {
            this.a = abbayedeTimadeuc;
        }

        public final void a() {
            this.a.b = true;
            EcirdelAubrac b = this.a.b();
            if (b != null) {
                b.c();
            }
        }

        public final void b() {
            EcirdelAubrac b = this.a.b();
            if (b != null) {
                b.a(0);
            }
        }

        public final void a(PontlEveque pontlEveque) {
            this.a.d.remove(pontlEveque);
        }
    }

    static final /* synthetic */ class Aveyronnais extends hk implements gg<ba, ep> {
        Aveyronnais(AbbayedeTimadeuc abbayedeTimadeuc) {
            super(1, abbayedeTimadeuc);
        }

        public final ig b() {
            return hn.a(AbbayedeTimadeuc.class);
        }

        public final String c() {
            return "sendShowEvent";
        }

        public final String d() {
            return "sendShowEvent(Lio/presage/mraid/MraidEvent;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((ba) obj);
            return ep.a;
        }

        private void a(ba baVar) {
            ((AbbayedeTimadeuc) this.a).a(baVar);
        }
    }

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public static final class CamembertdeNormandie implements PresageSdkInitCallback {
        final /* synthetic */ AbbayedeTimadeuc a;

        CamembertdeNormandie(AbbayedeTimadeuc abbayedeTimadeuc) {
            this.a = abbayedeTimadeuc;
        }

        public final void onSdkNotInitialized() {
            EcirdelAubrac b = this.a.b();
            if (b != null) {
                b.a(5);
            }
        }

        public final void onSdkInitialized() {
            this.a.h();
        }

        public final void onSdkInitFailed() {
            EcirdelAubrac b = this.a.b();
            if (b != null) {
                b.a(6);
            }
        }
    }

    static final /* synthetic */ class EcirdelAubrac extends hk implements gf<j> {
        EcirdelAubrac(AbbayedeTimadeuc abbayedeTimadeuc) {
            super(0, abbayedeTimadeuc);
        }

        public final ig b() {
            return hn.a(AbbayedeTimadeuc.class);
        }

        public final String c() {
            return "getProfigAndSyncIfNeeded";
        }

        public final String d() {
            return "getProfigAndSyncIfNeeded()Lio/presage/common/profig/data/ProfigFullResponse;";
        }

        /* access modifiers changed from: private */
        /* renamed from: h */
        public j a() {
            return ((AbbayedeTimadeuc) this.a).i();
        }
    }

    private AbbayedeTimadeuc(Context context, AdConfig adConfig, by byVar, k kVar, MoelleuxduRevard moelleuxduRevard, bb bbVar, bz bzVar, MunsterauCumin munsterauCumin, AbbayedeTamie abbayedeTamie, h hVar, SableduBoulonnais sableduBoulonnais) {
        this.k = context;
        this.l = adConfig;
        this.m = byVar;
        this.n = kVar;
        this.o = moelleuxduRevard;
        this.p = bbVar;
        this.q = bzVar;
        this.r = munsterauCumin;
        this.s = abbayedeTamie;
        this.t = hVar;
        this.u = sableduBoulonnais;
        this.c = "";
        this.d = new ArrayList();
        this.f = PresageSdk.a;
        this.j = true;
    }

    public final boolean a() {
        return this.b;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(EcirdelAubrac ecirdelAubrac) {
        this.e = ecirdelAubrac;
    }

    public final EcirdelAubrac b() {
        return this.e;
    }

    public final void a(gg<? super RewardItem, ep> ggVar) {
        this.g = ggVar;
    }

    public final void a(Soumaintrain soumaintrain) {
        this.h = soumaintrain;
    }

    public final boolean c() {
        return this.j;
    }

    public AbbayedeTimadeuc(Context context, AdConfig adConfig, SableduBoulonnais sableduBoulonnais) {
        Context applicationContext = context.getApplicationContext();
        hl.a((Object) applicationContext, "context.applicationContext");
        by byVar = by.a;
        k kVar = k.a;
        MoelleuxduRevard moelleuxduRevard = MoelleuxduRevard.a;
        bb bbVar = bb.a;
        io.presage.bz.CamembertauCalvados camembertauCalvados = bz.a;
        bz a2 = io.presage.bz.CamembertauCalvados.a(context);
        MunsterauCumin munsterauCumin = new MunsterauCumin(context);
        Context applicationContext2 = context.getApplicationContext();
        hl.a((Object) applicationContext2, "context.applicationContext");
        AbbayedeTamie abbayedeTamie = new AbbayedeTamie(applicationContext2, new t(), new z(), sableduBoulonnais);
        io.presage.h.CamembertauCalvados camembertauCalvados2 = h.a;
        this(applicationContext, adConfig, byVar, kVar, moelleuxduRevard, bbVar, a2, munsterauCumin, abbayedeTamie, io.presage.h.CamembertauCalvados.a(context), sableduBoulonnais);
    }

    public final void a(PontlEveque pontlEveque) {
        EcirdelAubrac ecirdelAubrac = this.e;
        if (ecirdelAubrac != null) {
            ecirdelAubrac.a(4);
        }
        this.d.remove(pontlEveque);
    }

    public final void d() {
        if (!this.s.a(this.e)) {
            if (PresageSdk.c()) {
                EcirdelAubrac ecirdelAubrac = this.e;
                if (ecirdelAubrac != null) {
                    ecirdelAubrac.a(6);
                }
            } else if (PresageSdk.a()) {
                g();
            } else if (AbbayedeTamie.a()) {
                f();
            } else {
                h();
            }
        }
    }

    private final void f() {
        String i2 = this.t.i();
        if (i2.length() == 0) {
            EcirdelAubrac ecirdelAubrac = this.e;
            if (ecirdelAubrac != null) {
                ecirdelAubrac.a(5);
            }
            return;
        }
        PresageSdk.init(this.k, i2);
        g();
    }

    private final void g() {
        this.f.addSdkInitCallback(new CamembertdeNormandie(this));
    }

    /* access modifiers changed from: private */
    public final void h() {
        io.presage.Goudaauxepices.CamembertauCalvados camembertauCalvados = Goudaauxepices.a;
        io.presage.Goudaauxepices.CamembertauCalvados.a(new EcirdelAubrac(this)).a((gg<? super Throwable, ep>) new AbbayedeTamie<Object,ep>(this)).b((gg<? super T, ep>) new C0118AbbayedeTimadeuc<Object,ep>(this));
    }

    /* access modifiers changed from: private */
    public final j i() {
        j a2 = k.a(this.k);
        if (a2 != null) {
            return a2;
        }
        k.b(this.k);
        return k.a(this.k);
    }

    /* access modifiers changed from: private */
    public final void a(j jVar) {
        if (jVar == null) {
            EcirdelAubrac ecirdelAubrac = this.e;
            if (ecirdelAubrac != null) {
                ecirdelAubrac.a(3);
            }
        } else if (!jVar.b()) {
            EcirdelAubrac ecirdelAubrac2 = this.e;
            if (ecirdelAubrac2 != null) {
                ecirdelAubrac2.a(2);
            }
        } else {
            MoelleuxduRevard.a((Mimolette24mois) new Morbier("LOAD"));
            this.r.a(this.u, this.l, this.c, this.h).a((gg<? super Throwable, ep>) new AbbayeduMontdesCats<Object,ep>(this)).b((gg<? super T, ep>) new AffideliceauChablis<Object,ep>(this));
        }
    }

    /* access modifiers changed from: private */
    public final void a(RouedeBrie rouedeBrie) {
        if (!this.i) {
            if (!rouedeBrie.a().isEmpty()) {
                EcirdelAubrac ecirdelAubrac = this.e;
                if (ecirdelAubrac != null) {
                    ecirdelAubrac.a();
                }
                a(ex.a(rouedeBrie.a()));
                return;
            }
            EcirdelAubrac ecirdelAubrac2 = this.e;
            if (ecirdelAubrac2 != null) {
                ecirdelAubrac2.b();
            }
        }
    }

    private final void a(List<PontlEveque> list) {
        boolean z;
        for (PontlEveque c2 : list) {
            if (c2.c().length() == 0) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                EcirdelAubrac ecirdelAubrac = this.e;
                if (ecirdelAubrac != null) {
                    ecirdelAubrac.d();
                    return;
                }
                return;
            }
        }
        this.d = ex.a(list);
        this.q.a(this, list, new Appenzeller(this));
    }

    public final void b(PontlEveque pontlEveque) {
        this.b = false;
        this.d.remove(pontlEveque);
    }

    public final void a(Appenzeller appenzeller) {
        by.a();
        if (this.s.a(this.e, this.b, this.d)) {
            this.j = false;
            MoelleuxduRevard.a((Mimolette24mois) new Morbier("SHOW"));
            this.b = false;
            List<PontlEveque> list = this.d;
            for (PontlEveque b2 : list) {
                bb.a(b2.b(), new Aveyronnais(this));
            }
            appenzeller.a(this.k, ex.a(list));
        }
    }

    /* access modifiers changed from: private */
    public final void a(ba baVar) {
        if (hl.a((Object) baVar.b(), (Object) Events.AD_IMPRESSION)) {
            EcirdelAubrac ecirdelAubrac = this.e;
            if (ecirdelAubrac != null) {
                ecirdelAubrac.e();
            }
        } else if (hl.a((Object) baVar.b(), (Object) "adClosed")) {
            EcirdelAubrac ecirdelAubrac2 = this.e;
            if (ecirdelAubrac2 != null) {
                ecirdelAubrac2.f();
            }
        } else {
            if (baVar instanceof bd) {
                gg<? super RewardItem, ep> ggVar = this.g;
                if (ggVar != null) {
                    ggVar.a(((bd) baVar).c());
                }
            }
        }
    }

    public final void e() {
        this.i = true;
        j();
        this.q.a((cb) this);
        this.e = null;
        this.g = null;
    }

    private final void j() {
        for (PontlEveque b2 : this.d) {
            bb.a(b2.b());
        }
    }
}
