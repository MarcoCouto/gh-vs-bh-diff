package io.presage;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.provider.Settings.Secure;
import com.tapjoy.TapjoyConstants;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public final class TommeMarcdeRaisin {
    public static final TommeMarcdeRaisin a = new TommeMarcdeRaisin();

    static final class CamembertauCalvados<T> implements Comparator<ApplicationInfo> {
        public static final CamembertauCalvados a = new CamembertauCalvados();

        CamembertauCalvados() {
        }

        public final /* synthetic */ int compare(Object obj, Object obj2) {
            return a((ApplicationInfo) obj, (ApplicationInfo) obj2);
        }

        private static int a(ApplicationInfo applicationInfo, ApplicationInfo applicationInfo2) {
            String str = applicationInfo.packageName;
            String str2 = applicationInfo2.packageName;
            hl.a((Object) str2, "rhs.packageName");
            return str.compareTo(str2);
        }
    }

    private TommeMarcdeRaisin() {
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:8|9|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
        return b(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0031, code lost:
        return new io.presage.c(c(r5), true, true);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0023 */
    public static c a(Context context) {
        a aVar = a.a;
        TrouduCru a2 = a.a(context);
        String a3 = a2.a();
        if (a3 != null) {
            return new c(a3, !a2.b(), false);
        }
        throw new IllegalStateException("androidAdvertisingId is null");
    }

    private static c b(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String string = Secure.getString(contentResolver, TapjoyConstants.TJC_ADVERTISING_ID);
        boolean z = Secure.getInt(contentResolver, "limit_ad_tracking") == 0;
        if (string != null) {
            return new c(string, z, false);
        }
        throw new IllegalStateException("Amazon Fire TV aaid is null".toString());
    }

    private static String c(Context context) {
        ApplicationInfo d = d(context);
        if (d == null) {
            return "00000000-0000-0000-0000-000000000000";
        }
        return a(context, d);
    }

    private static String a(Context context, ApplicationInfo applicationInfo) {
        try {
            long j = context.getPackageManager().getPackageInfo(applicationInfo.packageName, 128).firstInstallTime;
            StringBuilder sb = new StringBuilder();
            sb.append(String.valueOf(j));
            String sb2 = sb.toString();
            Charset charset = is.a;
            if (sb2 != null) {
                byte[] bytes = sb2.getBytes(charset);
                hl.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                String uuid = UUID.nameUUIDFromBytes(bytes).toString();
                hl.a((Object) uuid, "UUID.nameUUIDFromBytes((…toByteArray()).toString()");
                return uuid;
            }
            throw new em("null cannot be cast to non-null type java.lang.String");
        } catch (Exception unused) {
            return "00000000-0000-0000-0000-000000000000";
        }
    }

    private static ApplicationInfo d(Context context) {
        try {
            return e(context);
        } catch (Exception unused) {
            return null;
        }
    }

    private static ApplicationInfo e(Context context) {
        if (context != null) {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager != null) {
                List installedApplications = packageManager.getInstalledApplications(128);
                ArrayList arrayList = new ArrayList();
                if (installedApplications.size() == 0) {
                    return null;
                }
                hl.a((Object) installedApplications, "apps");
                for (int i = 0; i < installedApplications.size(); i++) {
                    ApplicationInfo applicationInfo = (ApplicationInfo) installedApplications.get(i);
                    if (!((applicationInfo.flags & 1) == 0 || applicationInfo.packageName == null)) {
                        arrayList.add(applicationInfo);
                    }
                }
                ex.a(arrayList, CamembertauCalvados.a);
                return (ApplicationInfo) arrayList.get(0);
            }
        }
        return null;
    }
}
