package io.presage;

import android.content.Context;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.LinkedHashMap;
import java.util.Map;

public class PaletdeBourgogne implements dk {
    private final Context a;
    private final TetedeMoine b;
    private final TommeMarcdeRaisin c;

    public PaletdeBourgogne(Context context, TetedeMoine tetedeMoine, TommeMarcdeRaisin tommeMarcdeRaisin) {
        this.a = context;
        this.b = tetedeMoine;
        this.c = tommeMarcdeRaisin;
    }

    public /* synthetic */ PaletdeBourgogne(Context context) {
        this(context, new TetedeMoine(context), TommeMarcdeRaisin.a);
    }

    public Map<String, String> a() {
        c a2 = TommeMarcdeRaisin.a(this.a);
        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("Content-Type", "application/json");
        linkedHashMap.put(HttpRequest.HEADER_ACCEPT_ENCODING, HttpRequest.ENCODING_GZIP);
        linkedHashMap.put(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
        linkedHashMap.put("Device-OS", "android");
        linkedHashMap.put("User", a2.a());
        linkedHashMap.put("User-Agent", this.b.d());
        linkedHashMap.put("Package-Name", this.b.e());
        return linkedHashMap;
    }
}
