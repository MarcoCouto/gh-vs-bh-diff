package io.presage;

import android.content.Context;
import android.os.Build.VERSION;

public final class de {
    public static final void a(db dbVar, String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(bi.b(dbVar));
            sb.append(" >> ");
            sb.append(str);
            dbVar.loadUrl("javascript:".concat(String.valueOf(str)));
        } catch (Throwable th) {
            Laguiole laguiole = Laguiole.a;
            Laguiole.a(th);
        }
    }

    public static final void a(db dbVar, PontlEveque pontlEveque) {
        boolean z = false;
        String h = pontlEveque.h().length() > 0 ? pontlEveque.h() : "http://ads-test.st.ogury.com/";
        if (pontlEveque.c().length() > 0) {
            z = true;
        }
        try {
            dbVar.loadDataWithBaseURL(h, z ? pontlEveque.c() : "The ad contains no ad_content", WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        } catch (Throwable th) {
            Laguiole laguiole = Laguiole.a;
            Laguiole.a(th);
        }
    }

    public static final db a(Context context, PontlEveque pontlEveque) {
        try {
            db dbVar = new db(context, pontlEveque);
            dbVar.setBackgroundColor(0);
            if (VERSION.SDK_INT >= 19) {
                dbVar.setLayerType(2, null);
            }
            return dbVar;
        } catch (Throwable th) {
            Laguiole laguiole = Laguiole.a;
            Laguiole.a(th);
            return null;
        }
    }
}
