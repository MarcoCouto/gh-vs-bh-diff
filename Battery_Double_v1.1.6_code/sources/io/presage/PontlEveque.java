package io.presage;

import java.io.Serializable;

public final class PontlEveque implements Serializable {
    private String a = "";
    private String b = "";
    private String c = "";
    private String d = "";
    private String e = "";
    private String f = "";
    private String g = "";
    private String h = "";
    private String i = "";
    private String j = "";
    private String k = "";
    private String l = "";
    private StMarcellin m = new StMarcellin();
    private SaintFelicien n = new SaintFelicien();
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private String s = "";
    private String t = "";
    private boolean u;
    private String v = "";
    private boolean w;
    private boolean x;
    private SableduBoulonnais y = SableduBoulonnais.INTERSTITIAL;

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final String d() {
        return this.e;
    }

    public final void d(String str) {
        this.e = str;
    }

    public final String e() {
        return this.f;
    }

    public final void e(String str) {
        this.f = str;
    }

    public final String f() {
        return this.g;
    }

    public final void f(String str) {
        this.g = str;
    }

    public final String g() {
        return this.h;
    }

    public final void g(String str) {
        this.h = str;
    }

    public final String h() {
        return this.i;
    }

    public final void h(String str) {
        this.i = str;
    }

    public final String i() {
        return this.j;
    }

    public final void i(String str) {
        this.j = str;
    }

    public final String j() {
        return this.k;
    }

    public final void j(String str) {
        this.k = str;
    }

    public final String k() {
        return this.l;
    }

    public final void k(String str) {
        this.l = str;
    }

    public final void a(StMarcellin stMarcellin) {
        this.m = stMarcellin;
    }

    public final StMarcellin l() {
        return this.m;
    }

    public final void a(SaintFelicien saintFelicien) {
        this.n = saintFelicien;
    }

    public final SaintFelicien m() {
        return this.n;
    }

    public final void a(boolean z) {
        this.o = z;
    }

    public final boolean n() {
        return this.o;
    }

    public final void b(boolean z) {
        this.p = z;
    }

    public final boolean o() {
        return this.p;
    }

    public final void c(boolean z) {
        this.q = z;
    }

    public final boolean p() {
        return this.q;
    }

    public final void d(boolean z) {
        this.r = z;
    }

    public final boolean q() {
        return this.r;
    }

    public final void l(String str) {
        this.s = str;
    }

    public final String r() {
        return this.s;
    }

    public final void m(String str) {
        this.t = str;
    }

    public final String s() {
        return this.t;
    }

    public final void e(boolean z) {
        this.u = z;
    }

    public final boolean t() {
        return this.u;
    }

    public final void n(String str) {
        this.v = str;
    }

    public final String u() {
        return this.v;
    }

    public final void f(boolean z) {
        this.w = z;
    }

    public final boolean v() {
        return this.w;
    }

    public final void g(boolean z) {
        this.x = z;
    }

    public final boolean w() {
        return this.x;
    }

    public final void a(SableduBoulonnais sableduBoulonnais) {
        this.y = sableduBoulonnais;
    }

    public final SableduBoulonnais x() {
        return this.y;
    }
}
