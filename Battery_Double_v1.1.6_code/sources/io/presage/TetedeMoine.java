package io.presage;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.facebook.places.model.PlaceFields;
import java.lang.reflect.Constructor;
import java.util.Locale;
import java.util.MissingResourceException;

public final class TetedeMoine {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final Context b;
    private final h c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private TetedeMoine(Context context, h hVar) {
        this.b = context;
        this.c = hVar;
    }

    public final Context g() {
        return this.b;
    }

    public /* synthetic */ TetedeMoine(Context context) {
        io.presage.h.CamembertauCalvados camembertauCalvados = h.a;
        this(context, io.presage.h.CamembertauCalvados.a(context));
    }

    public final boolean a(String str) {
        return ae.a(this.b, str);
    }

    public final String a() {
        return this.c.i();
    }

    public final String b() {
        if (VERSION.SDK_INT >= 24) {
            Resources resources = this.b.getResources();
            hl.a((Object) resources, "context.resources");
            Configuration configuration = resources.getConfiguration();
            hl.a((Object) configuration, "context.resources.configuration");
            Locale locale = configuration.getLocales().get(0);
            hl.a((Object) locale, "context.resources.configuration.locales[0]");
            String language = locale.getLanguage();
            hl.a((Object) language, "context.resources.config…ation.locales[0].language");
            return language;
        }
        Resources resources2 = this.b.getResources();
        hl.a((Object) resources2, "context.resources");
        Locale locale2 = resources2.getConfiguration().locale;
        hl.a((Object) locale2, "context.resources.configuration.locale");
        String language2 = locale2.getLanguage();
        hl.a((Object) language2, "context.resources.configuration.locale.language");
        return language2;
    }

    public final String c() {
        String h = h();
        if (h == null || h.length() != 3) {
            return i();
        }
        return h;
    }

    private final String h() {
        try {
            Object systemService = this.b.getSystemService(PlaceFields.PHONE);
            if (systemService != null) {
                return new Locale("", ((TelephonyManager) systemService).getNetworkCountryIso()).getISO3Country();
            }
            throw new em("null cannot be cast to non-null type android.telephony.TelephonyManager");
        } catch (Throwable unused) {
            return null;
        }
    }

    private final String i() {
        try {
            if (VERSION.SDK_INT >= 24) {
                Resources resources = this.b.getResources();
                hl.a((Object) resources, "context.resources");
                Configuration configuration = resources.getConfiguration();
                hl.a((Object) configuration, "context.resources.configuration");
                Locale locale = configuration.getLocales().get(0);
                hl.a((Object) locale, "context.resources.configuration.locales[0]");
                String iSO3Country = locale.getISO3Country();
                hl.a((Object) iSO3Country, "context.resources.config…on.locales[0].isO3Country");
                return iSO3Country;
            }
            Resources resources2 = this.b.getResources();
            hl.a((Object) resources2, "context.resources");
            Locale locale2 = resources2.getConfiguration().locale;
            hl.a((Object) locale2, "context.resources.configuration.locale");
            String iSO3Country2 = locale2.getISO3Country();
            hl.a((Object) iSO3Country2, "context.resources.configuration.locale.isO3Country");
            return iSO3Country2;
        } catch (MissingResourceException unused) {
            return "ZZZ";
        }
    }

    public final String d() {
        StringBuilder sb = new StringBuilder("3.3.9-moat/");
        sb.append(a());
        sb.append("/");
        sb.append(VERSION.RELEASE);
        return sb.toString();
    }

    public final String e() {
        String packageName = this.b.getPackageName();
        hl.a((Object) packageName, "context.packageName");
        return packageName;
    }

    public final String f() {
        String str;
        Constructor declaredConstructor;
        if (VERSION.SDK_INT >= 17) {
            String defaultUserAgent = WebSettings.getDefaultUserAgent(this.b);
            hl.a((Object) defaultUserAgent, "WebSettings.getDefaultUserAgent(context)");
            return defaultUserAgent;
        }
        try {
            declaredConstructor = WebSettings.class.getDeclaredConstructor(new Class[]{Context.class, WebView.class});
            hl.a((Object) declaredConstructor, "WebSettings::class.java.…ava, WebView::class.java)");
            declaredConstructor.setAccessible(true);
            Object newInstance = declaredConstructor.newInstance(new Object[]{this.b, null});
            hl.a(newInstance, "constructor.newInstance(context, null)");
            str = ((WebSettings) newInstance).getUserAgentString();
            declaredConstructor.setAccessible(false);
        } catch (Exception unused) {
            WebSettings settings = new WebView(this.b).getSettings();
            hl.a((Object) settings, "WebView(context).settings");
            str = settings.getUserAgentString();
        } catch (Throwable th) {
            declaredConstructor.setAccessible(false);
            throw th;
        }
        hl.a((Object) str, "try {\n            val co…userAgentString\n        }");
        return str;
    }
}
