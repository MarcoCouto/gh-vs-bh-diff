package io.presage.common;

import android.content.Context;
import android.content.SharedPreferences;
import com.tapjoy.TapjoyConstants;

public final class SdkType {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final SharedPreferences b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static SdkType a(Context context) {
            return new SdkType(context, 0);
        }
    }

    public static final SdkType create(Context context) {
        return CamembertauCalvados.a(context);
    }

    private SdkType(Context context) {
        this.b = context.getSharedPreferences("sdktypefile", 0);
    }

    public /* synthetic */ SdkType(Context context, byte b2) {
        this(context);
    }

    public final void setType(int i) {
        this.b.edit().putInt(TapjoyConstants.TJC_SDK_TYPE, i).apply();
    }

    public final int a() {
        return this.b.getInt(TapjoyConstants.TJC_SDK_TYPE, 0);
    }
}
