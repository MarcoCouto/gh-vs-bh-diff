package io.presage.common.profig.schedule;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import io.presage.common.profig.schedule.ProfigSyncIntentService.CamembertauCalvados;

public final class ProfigAlarmReceiver extends BroadcastReceiver {
    public final void onReceive(Context context, Intent intent) {
        CamembertauCalvados camembertauCalvados = ProfigSyncIntentService.a;
        CamembertauCalvados.a(context);
    }
}
