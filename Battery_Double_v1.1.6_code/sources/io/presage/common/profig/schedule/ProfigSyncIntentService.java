package io.presage.common.profig.schedule;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import io.presage.TommedAuvergne;
import io.presage.hl;

public final class ProfigSyncIntentService extends IntentService {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static void a(Context context) {
            context.startService(new Intent(context, ProfigSyncIntentService.class));
        }

        public static void b(Context context) {
            try {
                io.presage.TommedAuvergne.CamembertauCalvados camembertauCalvados = TommedAuvergne.a;
                Context applicationContext = context.getApplicationContext();
                hl.a((Object) applicationContext, "context.applicationContext");
                camembertauCalvados.a(applicationContext).b(false);
            } catch (Exception unused) {
            }
        }
    }

    public ProfigSyncIntentService() {
        super("ProfigService");
    }

    /* access modifiers changed from: protected */
    public final void onHandleIntent(Intent intent) {
        Context applicationContext = getApplicationContext();
        hl.a((Object) applicationContext, "applicationContext");
        CamembertauCalvados.b(applicationContext);
    }
}
