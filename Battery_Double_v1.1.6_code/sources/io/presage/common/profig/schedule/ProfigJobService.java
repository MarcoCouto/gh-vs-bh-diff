package io.presage.common.profig.schedule;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import io.presage.FourmedeMontbrison;
import io.presage.ep;
import io.presage.gf;
import io.presage.gg;
import io.presage.hl;
import io.presage.hm;

@TargetApi(21)
public final class ProfigJobService extends JobService {
    private JobParameters a;

    static final class CamembertauCalvados extends hm implements gf<ep> {
        final /* synthetic */ ProfigJobService a;

        CamembertauCalvados(ProfigJobService profigJobService) {
            this.a = profigJobService;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            io.presage.common.profig.schedule.ProfigSyncIntentService.CamembertauCalvados camembertauCalvados = ProfigSyncIntentService.a;
            Context applicationContext = this.a.getApplicationContext();
            hl.a((Object) applicationContext, "applicationContext");
            io.presage.common.profig.schedule.ProfigSyncIntentService.CamembertauCalvados.b(applicationContext);
        }
    }

    static final class CamembertdeNormandie extends hm implements gg<Throwable, ep> {
        final /* synthetic */ ProfigJobService a;

        CamembertdeNormandie(ProfigJobService profigJobService) {
            this.a = profigJobService;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a();
            return ep.a;
        }

        private void a() {
            this.a.a();
        }
    }

    static final class EcirdelAubrac extends hm implements gf<ep> {
        final /* synthetic */ ProfigJobService a;

        EcirdelAubrac(ProfigJobService profigJobService) {
            this.a = profigJobService;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return ep.a;
        }

        private void b() {
            this.a.a();
        }
    }

    public final boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    public final boolean onStartJob(JobParameters jobParameters) {
        this.a = jobParameters;
        io.presage.FourmedeMontbrison.CamembertauCalvados camembertauCalvados = FourmedeMontbrison.a;
        io.presage.FourmedeMontbrison.CamembertauCalvados.a(new CamembertauCalvados(this)).a((gg<? super Throwable, ep>) new CamembertdeNormandie<Object,ep>(this)).a((gf<ep>) new EcirdelAubrac<ep>(this));
        return true;
    }

    /* access modifiers changed from: private */
    public final void a() {
        StringBuilder sb = new StringBuilder("marking job as finished ");
        JobParameters jobParameters = this.a;
        sb.append(jobParameters != null ? jobParameters.toString() : null);
        jobFinished(this.a, false);
    }
}
