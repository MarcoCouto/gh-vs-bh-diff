package io.presage.common;

import android.content.Context;
import io.presage.Machecoulais;

public final class PresageSdk {
    public static final PresageSdk a = new PresageSdk();
    private static final Machecoulais b = new Machecoulais();

    private PresageSdk() {
    }

    public static final void init(Context context, String str) {
        b.a(context, str);
    }

    public static final String getAdsSdkVersion() {
        return Machecoulais.d();
    }

    public static boolean a() {
        return b.a();
    }

    public static boolean b() {
        return b.b();
    }

    public static boolean c() {
        return b.c();
    }

    public final void addSdkInitCallback(PresageSdkInitCallback presageSdkInitCallback) {
        b.a(presageSdkInitCallback);
    }

    public static final void setMediationName(String str) {
        b.a(str);
    }
}
