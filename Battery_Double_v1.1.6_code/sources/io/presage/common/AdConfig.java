package io.presage.common;

public final class AdConfig {
    private final String adUnitId;

    public AdConfig(String str) {
        this.adUnitId = str;
    }

    public final String getAdUnitId() {
        return this.adUnitId;
    }
}
