package io.presage;

public final class j extends n {
    private boolean a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private long f;
    private int g;
    private long h;
    private int i = -1;
    private boolean j;
    private boolean k = true;
    private long l;
    private long m;
    private long n;
    private int o;
    private String p = "";
    private final e q = new e();
    private final g r = new g();

    public j() {
        super(0);
    }

    public final void a(boolean z) {
        this.a = z;
    }

    public final boolean a() {
        return this.a;
    }

    public final void b(boolean z) {
        this.b = z;
    }

    public final boolean b() {
        return this.b;
    }

    public final void c(boolean z) {
        this.c = z;
    }

    public final boolean c() {
        return this.d;
    }

    public final void d(boolean z) {
        this.d = z;
    }

    public final boolean d() {
        return this.e;
    }

    public final void e(boolean z) {
        this.e = z;
    }

    public final void a(long j2) {
        this.f = j2;
    }

    public final long e() {
        return this.f;
    }

    public final void a(int i2) {
        this.g = i2;
    }

    public final int f() {
        return this.g;
    }

    public final void b(long j2) {
        this.h = j2;
    }

    public final long g() {
        return this.h;
    }

    public final void b(int i2) {
        this.i = i2;
    }

    public final int h() {
        return this.i;
    }

    public final void f(boolean z) {
        this.j = z;
    }

    public final boolean i() {
        return this.j;
    }

    public final void g(boolean z) {
        this.k = z;
    }

    public final boolean j() {
        return this.k;
    }

    public final void c(long j2) {
        this.l = j2;
    }

    public final long k() {
        return this.l;
    }

    public final void d(long j2) {
        this.m = j2;
    }

    public final long l() {
        return this.m;
    }

    public final void e(long j2) {
        this.n = j2;
    }

    public final long m() {
        return this.n;
    }

    public final void c(int i2) {
        this.o = i2;
    }

    public final int n() {
        return this.o;
    }

    public final void a(String str) {
        this.p = str;
    }

    public final String o() {
        return this.p;
    }

    public final e p() {
        return this.q;
    }

    public final g q() {
        return this.r;
    }
}
