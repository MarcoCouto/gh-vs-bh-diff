package io.presage;

import android.content.Context;

public final class OlivetCendre implements dh {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final k b;
    private final Context c;
    private final int d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public OlivetCendre(k kVar, Context context, int i) {
        this.b = kVar;
        this.c = context;
        this.d = i;
    }

    public final int a() {
        j a2 = k.a(this.c);
        return a2 != null ? a2.n() : this.d;
    }

    public final int b() {
        return a() * 5;
    }
}
