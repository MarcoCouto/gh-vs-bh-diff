package io.presage;

import java.io.Serializable;

public final class StMarcellin implements Serializable {
    private boolean a;
    private int b = 200;
    private int c = 300;

    public final void a(boolean z) {
        this.a = z;
    }

    public final boolean a() {
        return this.a;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final int b() {
        return this.b;
    }

    public final void b(int i) {
        this.c = i;
    }

    public final int c() {
        return this.c;
    }
}
