package io.presage;

import android.os.Build.VERSION;
import android.security.NetworkSecurityPolicy;
import org.json.JSONObject;

public final class PasdelEscalette {
    public static boolean a(JSONObject jSONObject) {
        return a() && ab.a(jSONObject);
    }

    private static boolean a() {
        if (VERSION.SDK_INT < 23) {
            return true;
        }
        NetworkSecurityPolicy instance = NetworkSecurityPolicy.getInstance();
        hl.a((Object) instance, "NetworkSecurityPolicy.getInstance()");
        return instance.isCleartextTrafficPermitted();
    }
}
