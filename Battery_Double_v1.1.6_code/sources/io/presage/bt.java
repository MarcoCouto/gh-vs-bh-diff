package io.presage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import io.presage.mraid.browser.listeners.CloseSystemDialogsListener$1;

public final class bt {
    private final BroadcastReceiver a = new CloseSystemDialogsListener$1(this);
    private final Context b;
    /* access modifiers changed from: private */
    public final bk c;

    public bt(Context context, bk bkVar) {
        this.b = context;
        this.c = bkVar;
        b();
    }

    private final void b() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        this.b.registerReceiver(this.a, intentFilter);
    }

    /* access modifiers changed from: private */
    public final void c() {
        this.c.b();
    }

    public final void a() {
        try {
            this.b.unregisterReceiver(this.a);
        } catch (Throwable unused) {
        }
    }
}
