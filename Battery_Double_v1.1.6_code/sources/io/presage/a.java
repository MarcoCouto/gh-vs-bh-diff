package io.presage;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Looper;
import java.io.IOException;

public final class a {
    public static final a a = new a();

    private a() {
    }

    public static TrouduCru a(Context context) throws Exception {
        a();
        b(context);
        VieuxLille vieuxLille = new VieuxLille();
        Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
        intent.setPackage("com.google.android.gms");
        ServiceConnection serviceConnection = vieuxLille;
        if (context.bindService(intent, serviceConnection, 1)) {
            try {
                b bVar = new b(vieuxLille.a());
                return new TrouduCru(bVar.a(), bVar.b());
            } finally {
                context.unbindService(serviceConnection);
            }
        } else {
            throw new IOException("Google Play connection failed");
        }
    }

    private static void a() {
        if (!(!hl.a((Object) Looper.myLooper(), (Object) Looper.getMainLooper()))) {
            throw new IllegalStateException("Cannot be called from the main thread".toString());
        }
    }

    private static void b(Context context) {
        if (!(!c(context))) {
            throw new IllegalStateException("Google play is not installed".toString());
        }
    }

    private static boolean c(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            return false;
        } catch (Exception unused) {
            return true;
        }
    }
}
