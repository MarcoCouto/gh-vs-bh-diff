package io.presage;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public final class aa {
    public static final List<String> a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return ex.a();
        }
        int length = jSONArray.length();
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            arrayList.add(jSONArray.getString(i));
        }
        return arrayList;
    }
}
