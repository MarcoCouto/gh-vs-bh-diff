package io.presage;

import android.content.Context;
import java.util.Map;

public final class Murol extends PaletdeBourgogne {
    private final Context a;
    private final TetedeMoine b;
    private final Taleggio c;

    public /* synthetic */ Murol(Context context) {
        this(context, new TetedeMoine(context), new Taleggio(context), TommeMarcdeRaisin.a);
    }

    private Murol(Context context, TetedeMoine tetedeMoine, Taleggio taleggio, TommeMarcdeRaisin tommeMarcdeRaisin) {
        super(context, tetedeMoine, tommeMarcdeRaisin);
        this.a = context;
        this.b = tetedeMoine;
        this.c = taleggio;
    }

    public final Map<String, String> a() {
        Map<String, String> a2 = super.a();
        a2.put("WebView-User-Agent", this.b.f());
        a2.put("Orientation", this.c.m());
        return a2;
    }
}
