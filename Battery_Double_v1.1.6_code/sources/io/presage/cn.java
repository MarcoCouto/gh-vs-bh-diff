package io.presage;

public final class cn extends co {
    private final db b;

    public cn(db dbVar) {
        super(dbVar.getMraidCommandExecutor());
        this.b = dbVar;
    }

    public final void a() {
        f();
    }

    public final void b() {
        f();
    }

    private final void f() {
        this.b.d();
        this.b.g();
    }

    public final void a(boolean z) {
        this.b.setShowSdkCloseButton(z);
    }
}
