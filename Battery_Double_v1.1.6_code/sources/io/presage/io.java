package io.presage;

import java.util.Iterator;

class io extends in {

    public static final class CamembertauCalvados implements hr, Iterable<T> {
        final /* synthetic */ ii a;

        public CamembertauCalvados(ii iiVar) {
            this.a = iiVar;
        }

        public final Iterator<T> iterator() {
            return this.a.a();
        }
    }

    public static final <T> Iterable<T> a(ii<? extends T> iiVar) {
        return new CamembertauCalvados<>(iiVar);
    }
}
