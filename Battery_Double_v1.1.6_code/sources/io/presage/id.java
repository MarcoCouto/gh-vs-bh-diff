package io.presage;

import io.presage.ia.CamembertauCalvados;

class id extends ic {
    public static final int a(int i) {
        if (i < 0) {
            return 0;
        }
        return i;
    }

    public static final int b(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    public static final ia a(int i, int i2) {
        if (i2 > Integer.MIN_VALUE) {
            return new ia(i, i2 - 1);
        }
        CamembertauCalvados camembertauCalvados = ia.b;
        return CamembertauCalvados.a();
    }

    public static final int c(int i, int i2) {
        if (i2 < 0) {
            StringBuilder sb = new StringBuilder("Cannot coerce value to an empty range: maximum ");
            sb.append(i2);
            sb.append(" is less than minimum 0.");
            throw new IllegalArgumentException(sb.toString());
        } else if (i < 0) {
            return 0;
        } else {
            return i > i2 ? i2 : i;
        }
    }
}
