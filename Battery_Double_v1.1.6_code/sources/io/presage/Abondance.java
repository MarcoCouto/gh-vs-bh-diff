package io.presage;

import android.annotation.SuppressLint;
import android.webkit.WebSettings;
import android.webkit.WebView;

public final class Abondance {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private db b;
    private final by c;
    private final be d;
    private final cq e;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static Abondance a(bj bjVar, cq cqVar) {
            return new Abondance(by.a, bjVar.a(), cqVar, 0);
        }
    }

    public static final class CamembertdeNormandie extends dc {
        final /* synthetic */ Abondance a;

        CamembertdeNormandie(Abondance abondance) {
            this.a = abondance;
        }

        public final void a(WebView webView) {
            this.a.b();
        }
    }

    private Abondance(by byVar, be beVar, cq cqVar) {
        this.c = byVar;
        this.d = beVar;
        this.e = cqVar;
    }

    public /* synthetic */ Abondance(by byVar, be beVar, cq cqVar, byte b2) {
        this(byVar, beVar, cqVar);
    }

    public final db a(PontlEveque pontlEveque) {
        db b2 = b(pontlEveque);
        if (b2 == null) {
            return null;
        }
        this.b = b2;
        a();
        a(this.b);
        b();
        return this.b;
    }

    private static db b(PontlEveque pontlEveque) {
        return by.b(pontlEveque.a());
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private final void a() {
        db dbVar = this.b;
        if (dbVar != null) {
            dbVar.setMraidUrlHandler(new cp(new be[]{this.d, this.e.a(dbVar)}));
            WebSettings settings = dbVar.getSettings();
            if (settings != null) {
                settings.setJavaScriptEnabled(true);
            }
            WebView webView = dbVar;
            an.c(webView);
            an.b(webView);
        }
    }

    private final void a(db dbVar) {
        if (dbVar != null) {
            dbVar.setClientAdapter(new CamembertdeNormandie(this));
        }
    }

    /* access modifiers changed from: private */
    public final void b() {
        db dbVar = this.b;
        if (dbVar != null) {
            dbVar.c();
        }
    }
}
