package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import java.util.concurrent.TimeUnit;

public final class h {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static h c;
    private final SharedPreferences b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static h a(Context context) {
            if (h.c == null) {
                Context applicationContext = context.getApplicationContext();
                hl.a((Object) applicationContext, "context.applicationContext");
                h.c = new h(applicationContext, 0);
            }
            h k = h.c;
            if (k == null) {
                hl.a();
            }
            return k;
        }
    }

    private h(Context context) {
        this.b = context.getSharedPreferences("profig", 0);
    }

    public /* synthetic */ h(Context context, byte b2) {
        this(context);
    }

    public final void a(int i) {
        this.b.edit().putInt("numberOfProfigApiCalls", i).apply();
    }

    public final int a() {
        return this.b.getInt("numberOfProfigApiCalls", 0);
    }

    public final String b() {
        SharedPreferences sharedPreferences = this.b;
        hl.a((Object) sharedPreferences, "sharedPref");
        return ah.a(sharedPreferences, "md5Profig", "");
    }

    public final void a(String str) {
        this.b.edit().putString("md5Profig", str).apply();
    }

    public final void b(String str) {
        this.b.edit().putString("aaid", str).apply();
    }

    public final String c() {
        SharedPreferences sharedPreferences = this.b;
        hl.a((Object) sharedPreferences, "sharedPref");
        return ah.a(sharedPreferences, "aaid", "");
    }

    public final void c(String str) {
        this.b.edit().putString("fullProfigResponseJson", str).apply();
    }

    public final String d() {
        SharedPreferences sharedPreferences = this.b;
        hl.a((Object) sharedPreferences, "sharedPref");
        return ah.a(sharedPreferences, "fullProfigResponseJson", "");
    }

    public final void e() {
        this.b.edit().putLong("numberOfDays", System.currentTimeMillis() / TimeUnit.DAYS.toMillis(1)).apply();
    }

    public final long f() {
        return this.b.getLong("numberOfDays", 0);
    }

    public final void d(String str) {
        this.b.edit().putString(RequestParameters.APPLICATION_VERSION_NAME, str).apply();
    }

    public final String g() {
        String string = this.b.getString(RequestParameters.APPLICATION_VERSION_NAME, al.a());
        return string == null ? "" : string;
    }

    public final void a(long j) {
        this.b.edit().putLong("last_profig_sync", j).apply();
    }

    public final long h() {
        return this.b.getLong("last_profig_sync", 0);
    }

    public final void e(String str) {
        this.b.edit().putString(TapjoyConstants.TJC_API_KEY, str).apply();
    }

    public final String i() {
        SharedPreferences sharedPreferences = this.b;
        hl.a((Object) sharedPreferences, "sharedPref");
        return ah.a(sharedPreferences, TapjoyConstants.TJC_API_KEY, "");
    }

    public final void f(String str) {
        this.b.edit().putString("mediationName", str).apply();
    }

    public final String j() {
        SharedPreferences sharedPreferences = this.b;
        hl.a((Object) sharedPreferences, "sharedPref");
        return ah.a(sharedPreferences, "mediationName", "none");
    }
}
