package io.presage;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import java.util.Calendar;

public final class CamembertdeNormandie extends FrameLayout {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private float b;
    private float c;
    private int d;
    private int e;
    private long f;
    private BriedeMeaux g = new BriedeMeaux();
    private boolean h;
    private cu i;
    private gg<? super CamembertdeNormandie, ep> j;
    private gf<ep> k;
    private gf<ep> l;
    private boolean m;
    private boolean n;
    private cu o;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public CamembertdeNormandie(Context context) {
        super(context);
        setLayoutParams(new LayoutParams(600, 600));
        addOnLayoutChangeListener(new OnLayoutChangeListener(this) {
            final /* synthetic */ CamembertdeNormandie a;

            {
                this.a = r1;
            }

            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                gg adLayoutChangeListener = this.a.getAdLayoutChangeListener();
                if (adLayoutChangeListener != null) {
                    adLayoutChangeListener.a(this.a);
                }
            }
        });
    }

    public final boolean getEnableDrag() {
        return this.h;
    }

    public final void setEnableDrag(boolean z) {
        this.h = z;
    }

    public final cu getResizeProps() {
        return this.i;
    }

    public final void setResizeProps(cu cuVar) {
        this.i = cuVar;
    }

    public final gg<CamembertdeNormandie, ep> getAdLayoutChangeListener() {
        return this.j;
    }

    public final void setAdLayoutChangeListener(gg<? super CamembertdeNormandie, ep> ggVar) {
        this.j = ggVar;
    }

    public final gf<ep> getOnWindowGainFocusListener() {
        return this.k;
    }

    public final void setOnWindowGainFocusListener(gf<ep> gfVar) {
        this.k = gfVar;
    }

    public final gf<ep> getOnWindowLoseFocusListener() {
        return this.l;
    }

    public final void setOnWindowLoseFocusListener(gf<ep> gfVar) {
        this.l = gfVar;
    }

    public final void setDisplayedInFullScreen(boolean z) {
        this.m = z;
    }

    public final boolean getContainsOverlayAd() {
        return this.n;
    }

    public final void setContainsOverlayAd(boolean z) {
        this.n = z;
    }

    public final int getContainerHeight() {
        return this.d;
    }

    public final int getContainerWidth() {
        return this.e;
    }

    public final void setRectHelper(BriedeMeaux briedeMeaux) {
        this.g = briedeMeaux;
    }

    public final void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (!(view instanceof WebView) || getChildCount() <= 0) {
            super.addView(view, layoutParams);
        } else {
            super.addView(view, getChildCount() - 1, layoutParams);
        }
    }

    public final void a() {
        ViewGroup parentAsViewGroup = getParentAsViewGroup();
        if (parentAsViewGroup != null) {
            parentAsViewGroup.removeView(this);
        }
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (!this.h || a(motionEvent)) {
            return super.dispatchTouchEvent(motionEvent);
        }
        g();
        return true;
    }

    private final boolean a(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                Calendar instance = Calendar.getInstance();
                hl.a((Object) instance, "Calendar.getInstance()");
                this.f = instance.getTimeInMillis();
                break;
            case 1:
                gg<? super CamembertdeNormandie, ep> ggVar = this.j;
                if (ggVar != null) {
                    ggVar.a(this);
                }
                Calendar instance2 = Calendar.getInstance();
                hl.a((Object) instance2, "Calendar.getInstance()");
                if (instance2.getTimeInMillis() - this.f < 200) {
                    return true;
                }
                return false;
        }
        b(motionEvent);
        return true;
    }

    private final void b(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action == 2) {
                d(motionEvent);
            }
            return;
        }
        c(motionEvent);
    }

    private final void c(MotionEvent motionEvent) {
        this.b = getX() - motionEvent.getRawX();
        this.c = getY() - motionEvent.getRawY();
    }

    private final void d(MotionEvent motionEvent) {
        bringToFront();
        if (a(motionEvent.getRawX() + this.b + ((float) (getWidth() / 4)))) {
            setX(motionEvent.getRawX() + this.b);
        }
        if (b(motionEvent.getRawY() + this.c + ((float) (getHeight() / 4)))) {
            setY(motionEvent.getRawY() + this.c);
        }
    }

    private final boolean a(float f2) {
        return f2 > 0.0f && f2 + ((float) (getWidth() / 2)) < ((float) this.e);
    }

    private final boolean b(float f2) {
        return f2 > 0.0f && f2 + ((float) (getHeight() / 2)) < ((float) this.d);
    }

    public final void b() {
        a(this.i);
    }

    private final void a(cu cuVar) {
        if (cuVar != null) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams != null) {
                LayoutParams layoutParams2 = (LayoutParams) layoutParams;
                setX((float) cuVar.e());
                setY((float) cuVar.f());
                layoutParams2.width = cuVar.c();
                layoutParams2.height = cuVar.d();
                setLayoutParams(layoutParams2);
                return;
            }
            throw new em("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        }
    }

    private final void f() {
        setX(0.0f);
        setY(0.0f);
    }

    public final void c() {
        f();
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null) {
            LayoutParams layoutParams2 = (LayoutParams) layoutParams;
            layoutParams2.width = -1;
            layoutParams2.height = -1;
            setLayoutParams(layoutParams2);
            return;
        }
        throw new em("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
    }

    public final void setInitialSize(cu cuVar) {
        this.o = cuVar;
        f();
        a(cuVar);
    }

    public final void d() {
        f();
        ViewParent parent = getParent();
        if (!(parent instanceof ViewGroup)) {
            parent = null;
        }
        ViewGroup viewGroup = (ViewGroup) parent;
        if (viewGroup != null) {
            this.i = BriedeMeaux.a((View) viewGroup, this.o);
            a(viewGroup);
        }
    }

    private final void a(ViewGroup viewGroup) {
        this.e = viewGroup.getMeasuredWidth();
        this.d = viewGroup.getMeasuredHeight();
    }

    private final void g() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt instanceof db) {
                ((db) childAt).f();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (!this.m) {
            h();
        }
    }

    private final void h() {
        ViewParent parent = getParent();
        if (!(parent instanceof ViewGroup)) {
            parent = null;
        }
        ViewGroup viewGroup = (ViewGroup) parent;
        if (viewGroup != null) {
            if (!(this.d == viewGroup.getMeasuredHeight() && this.e == viewGroup.getMeasuredWidth())) {
                a(viewGroup);
                b(viewGroup);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        if (this.n) {
            AffideliceauChablis affideliceauChablis = AffideliceauChablis.a;
            AffideliceauChablis.a(true);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.m = false;
        if (this.n) {
            AffideliceauChablis affideliceauChablis = AffideliceauChablis.a;
            AffideliceauChablis.a(false);
        }
        super.onDetachedFromWindow();
    }

    private final void b(ViewGroup viewGroup) {
        a(BriedeMeaux.a((View) this, (View) viewGroup));
    }

    public final ViewGroup getParentAsViewGroup() {
        ViewParent parent = getParent();
        if (!(parent instanceof ViewGroup)) {
            parent = null;
        }
        return (ViewGroup) parent;
    }

    public final void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            gf<ep> gfVar = this.k;
            if (gfVar != null) {
                gfVar.a();
            }
            return;
        }
        gf<ep> gfVar2 = this.l;
        if (gfVar2 != null) {
            gfVar2.a();
        }
    }

    public final void e() {
        this.j = null;
        this.k = null;
        this.l = null;
        removeAllViews();
    }
}
