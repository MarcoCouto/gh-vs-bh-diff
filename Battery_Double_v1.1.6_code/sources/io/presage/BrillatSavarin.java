package io.presage;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.List;

public final class BrillatSavarin {
    public final List<View> a(ViewGroup viewGroup, WebView webView) {
        List arrayList = new ArrayList();
        a(viewGroup, arrayList);
        return arrayList.subList(arrayList.indexOf(webView) + 1, arrayList.size());
    }

    private final void a(ViewGroup viewGroup, List<View> list) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if (!(childAt instanceof CamembertdeNormandie)) {
                hl.a((Object) childAt, "child");
                list.add(childAt);
            }
            if (childAt instanceof ViewGroup) {
                a((ViewGroup) childAt, list);
            }
        }
    }
}
