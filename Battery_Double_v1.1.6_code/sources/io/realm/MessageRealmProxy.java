package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mansoon.BatteryDouble.models.data.Message;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.exceptions.RealmException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageRealmProxy extends Message implements RealmObjectProxy, MessageRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private MessageColumnInfo columnInfo;
    private ProxyState<Message> proxyState;

    static final class MessageColumnInfo extends ColumnInfo {
        long bodyIndex;
        long dateIndex;
        long idIndex;
        long readIndex;
        long titleIndex;
        long typeIndex;

        MessageColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(6);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("Message");
            this.idIndex = addColumnDetails("id", objectSchemaInfo);
            this.typeIndex = addColumnDetails("type", objectSchemaInfo);
            this.titleIndex = addColumnDetails("title", objectSchemaInfo);
            this.bodyIndex = addColumnDetails(TtmlNode.TAG_BODY, objectSchemaInfo);
            this.dateIndex = addColumnDetails("date", objectSchemaInfo);
            this.readIndex = addColumnDetails("read", objectSchemaInfo);
        }

        MessageColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new MessageColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            MessageColumnInfo messageColumnInfo = (MessageColumnInfo) columnInfo;
            MessageColumnInfo messageColumnInfo2 = (MessageColumnInfo) columnInfo2;
            messageColumnInfo2.idIndex = messageColumnInfo.idIndex;
            messageColumnInfo2.typeIndex = messageColumnInfo.typeIndex;
            messageColumnInfo2.titleIndex = messageColumnInfo.titleIndex;
            messageColumnInfo2.bodyIndex = messageColumnInfo.bodyIndex;
            messageColumnInfo2.dateIndex = messageColumnInfo.dateIndex;
            messageColumnInfo2.readIndex = messageColumnInfo.readIndex;
        }
    }

    public static String getTableName() {
        return "class_Message";
    }

    static {
        ArrayList arrayList = new ArrayList(6);
        arrayList.add("id");
        arrayList.add("type");
        arrayList.add("title");
        arrayList.add(TtmlNode.TAG_BODY);
        arrayList.add("date");
        arrayList.add("read");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    MessageRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (MessageColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public int realmGet$id() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.idIndex);
    }

    public void realmSet$id(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
        }
    }

    public String realmGet$type() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.typeIndex);
    }

    public void realmSet$type(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.typeIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.typeIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.typeIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.typeIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$title() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.titleIndex);
    }

    public void realmSet$title(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.titleIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.titleIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.titleIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.titleIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$body() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.bodyIndex);
    }

    public void realmSet$body(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.bodyIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.bodyIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.bodyIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.bodyIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$date() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.dateIndex);
    }

    public void realmSet$date(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.dateIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.dateIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.dateIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.dateIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public boolean realmGet$read() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getBoolean(this.columnInfo.readIndex);
    }

    public void realmSet$read(boolean z) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setBoolean(this.columnInfo.readIndex, z);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setBoolean(this.columnInfo.readIndex, row$realm.getIndex(), z, true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("Message", 6, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("id", RealmFieldType.INTEGER, true, true, true);
        builder2.addPersistedProperty("type", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("title", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty(TtmlNode.TAG_BODY, RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("date", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("read", RealmFieldType.BOOLEAN, false, false, true);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static MessageColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new MessageColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x011c  */
    public static Message createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        MessageRealmProxy messageRealmProxy;
        List emptyList = Collections.emptyList();
        if (z) {
            Table table = realm.getTable(Message.class);
            long findFirstLong = !jSONObject.isNull("id") ? table.findFirstLong(((MessageColumnInfo) realm.getSchema().getColumnInfo(Message.class)).idIndex, jSONObject.getLong("id")) : -1;
            if (findFirstLong != -1) {
                RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
                try {
                    realmObjectContext.set(realm, table.getUncheckedRow(findFirstLong), realm.getSchema().getColumnInfo(Message.class), false, Collections.emptyList());
                    messageRealmProxy = new MessageRealmProxy();
                    if (messageRealmProxy == null) {
                        if (!jSONObject.has("id")) {
                            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
                        } else if (jSONObject.isNull("id")) {
                            messageRealmProxy = (MessageRealmProxy) realm.createObjectInternal(Message.class, null, true, emptyList);
                        } else {
                            messageRealmProxy = (MessageRealmProxy) realm.createObjectInternal(Message.class, Integer.valueOf(jSONObject.getInt("id")), true, emptyList);
                        }
                    }
                    MessageRealmProxyInterface messageRealmProxyInterface = messageRealmProxy;
                    if (jSONObject.has("type")) {
                        if (jSONObject.isNull("type")) {
                            messageRealmProxyInterface.realmSet$type(null);
                        } else {
                            messageRealmProxyInterface.realmSet$type(jSONObject.getString("type"));
                        }
                    }
                    if (jSONObject.has("title")) {
                        if (jSONObject.isNull("title")) {
                            messageRealmProxyInterface.realmSet$title(null);
                        } else {
                            messageRealmProxyInterface.realmSet$title(jSONObject.getString("title"));
                        }
                    }
                    if (jSONObject.has(TtmlNode.TAG_BODY)) {
                        if (jSONObject.isNull(TtmlNode.TAG_BODY)) {
                            messageRealmProxyInterface.realmSet$body(null);
                        } else {
                            messageRealmProxyInterface.realmSet$body(jSONObject.getString(TtmlNode.TAG_BODY));
                        }
                    }
                    if (jSONObject.has("date")) {
                        if (jSONObject.isNull("date")) {
                            messageRealmProxyInterface.realmSet$date(null);
                        } else {
                            messageRealmProxyInterface.realmSet$date(jSONObject.getString("date"));
                        }
                    }
                    if (jSONObject.has("read")) {
                        if (!jSONObject.isNull("read")) {
                            messageRealmProxyInterface.realmSet$read(jSONObject.getBoolean("read"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'read' to null.");
                        }
                    }
                    return messageRealmProxy;
                } finally {
                    realmObjectContext.clear();
                }
            }
        }
        messageRealmProxy = null;
        if (messageRealmProxy == null) {
        }
        MessageRealmProxyInterface messageRealmProxyInterface2 = messageRealmProxy;
        if (jSONObject.has("type")) {
        }
        if (jSONObject.has("title")) {
        }
        if (jSONObject.has(TtmlNode.TAG_BODY)) {
        }
        if (jSONObject.has("date")) {
        }
        if (jSONObject.has("read")) {
        }
        return messageRealmProxy;
    }

    @TargetApi(11)
    public static Message createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        Message message = new Message();
        MessageRealmProxyInterface messageRealmProxyInterface = message;
        jsonReader.beginObject();
        boolean z = false;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("id")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    messageRealmProxyInterface.realmSet$id(jsonReader.nextInt());
                    z = true;
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
            } else if (nextName.equals("type")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    messageRealmProxyInterface.realmSet$type(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    messageRealmProxyInterface.realmSet$type(null);
                }
            } else if (nextName.equals("title")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    messageRealmProxyInterface.realmSet$title(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    messageRealmProxyInterface.realmSet$title(null);
                }
            } else if (nextName.equals(TtmlNode.TAG_BODY)) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    messageRealmProxyInterface.realmSet$body(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    messageRealmProxyInterface.realmSet$body(null);
                }
            } else if (nextName.equals("date")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    messageRealmProxyInterface.realmSet$date(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    messageRealmProxyInterface.realmSet$date(null);
                }
            } else if (!nextName.equals("read")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                messageRealmProxyInterface.realmSet$read(jsonReader.nextBoolean());
            } else {
                jsonReader.skipValue();
                throw new IllegalArgumentException("Trying to set non-nullable field 'read' to null.");
            }
        }
        jsonReader.endObject();
        if (z) {
            return (Message) realm.copyToRealm(message);
        }
        throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00aa  */
    public static Message copyOrUpdate(Realm realm, Message message, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        boolean z2;
        if (message instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) message;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return message;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(message);
        if (realmObjectProxy2 != null) {
            return (Message) realmObjectProxy2;
        }
        MessageRealmProxy messageRealmProxy = null;
        if (z) {
            Table table = realm.getTable(Message.class);
            long findFirstLong = table.findFirstLong(((MessageColumnInfo) realm.getSchema().getColumnInfo(Message.class)).idIndex, (long) message.realmGet$id());
            if (findFirstLong == -1) {
                z2 = false;
                return !z2 ? update(realm, messageRealmProxy, message, map) : copy(realm, message, z, map);
            }
            try {
                realmObjectContext.set(realm, table.getUncheckedRow(findFirstLong), realm.getSchema().getColumnInfo(Message.class), false, Collections.emptyList());
                messageRealmProxy = new MessageRealmProxy();
                map.put(message, messageRealmProxy);
            } finally {
                realmObjectContext.clear();
            }
        }
        z2 = z;
        return !z2 ? update(realm, messageRealmProxy, message, map) : copy(realm, message, z, map);
    }

    public static Message copy(Realm realm, Message message, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(message);
        if (realmObjectProxy != null) {
            return (Message) realmObjectProxy;
        }
        MessageRealmProxyInterface messageRealmProxyInterface = message;
        Message message2 = (Message) realm.createObjectInternal(Message.class, Integer.valueOf(messageRealmProxyInterface.realmGet$id()), false, Collections.emptyList());
        map.put(message, (RealmObjectProxy) message2);
        MessageRealmProxyInterface messageRealmProxyInterface2 = message2;
        messageRealmProxyInterface2.realmSet$type(messageRealmProxyInterface.realmGet$type());
        messageRealmProxyInterface2.realmSet$title(messageRealmProxyInterface.realmGet$title());
        messageRealmProxyInterface2.realmSet$body(messageRealmProxyInterface.realmGet$body());
        messageRealmProxyInterface2.realmSet$date(messageRealmProxyInterface.realmGet$date());
        messageRealmProxyInterface2.realmSet$read(messageRealmProxyInterface.realmGet$read());
        return message2;
    }

    public static long insert(Realm realm, Message message, Map<RealmModel, Long> map) {
        long j;
        long j2;
        Message message2 = message;
        if (message2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) message2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(Message.class);
        long nativePtr = table.getNativePtr();
        MessageColumnInfo messageColumnInfo = (MessageColumnInfo) realm.getSchema().getColumnInfo(Message.class);
        long j3 = messageColumnInfo.idIndex;
        MessageRealmProxyInterface messageRealmProxyInterface = message2;
        Integer valueOf = Integer.valueOf(messageRealmProxyInterface.realmGet$id());
        if (valueOf != null) {
            j = Table.nativeFindFirstInt(nativePtr, j3, (long) messageRealmProxyInterface.realmGet$id());
        } else {
            j = -1;
        }
        if (j == -1) {
            j2 = OsObject.createRowWithPrimaryKey(table, j3, Integer.valueOf(messageRealmProxyInterface.realmGet$id()));
        } else {
            Table.throwDuplicatePrimaryKeyException(valueOf);
            j2 = j;
        }
        map.put(message2, Long.valueOf(j2));
        String realmGet$type = messageRealmProxyInterface.realmGet$type();
        if (realmGet$type != null) {
            Table.nativeSetString(nativePtr, messageColumnInfo.typeIndex, j2, realmGet$type, false);
        }
        String realmGet$title = messageRealmProxyInterface.realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(nativePtr, messageColumnInfo.titleIndex, j2, realmGet$title, false);
        }
        String realmGet$body = messageRealmProxyInterface.realmGet$body();
        if (realmGet$body != null) {
            Table.nativeSetString(nativePtr, messageColumnInfo.bodyIndex, j2, realmGet$body, false);
        }
        String realmGet$date = messageRealmProxyInterface.realmGet$date();
        if (realmGet$date != null) {
            Table.nativeSetString(nativePtr, messageColumnInfo.dateIndex, j2, realmGet$date, false);
        }
        Table.nativeSetBoolean(nativePtr, messageColumnInfo.readIndex, j2, messageRealmProxyInterface.realmGet$read(), false);
        return j2;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(Message.class);
        long nativePtr = table.getNativePtr();
        MessageColumnInfo messageColumnInfo = (MessageColumnInfo) realm.getSchema().getColumnInfo(Message.class);
        long j2 = messageColumnInfo.idIndex;
        while (it.hasNext()) {
            Message message = (Message) it.next();
            if (!map2.containsKey(message)) {
                if (message instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) message;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(message, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                MessageRealmProxyInterface messageRealmProxyInterface = message;
                Integer valueOf = Integer.valueOf(messageRealmProxyInterface.realmGet$id());
                if (valueOf != null) {
                    j = Table.nativeFindFirstInt(nativePtr, j2, (long) messageRealmProxyInterface.realmGet$id());
                } else {
                    j = -1;
                }
                if (j == -1) {
                    j = OsObject.createRowWithPrimaryKey(table, j2, Integer.valueOf(messageRealmProxyInterface.realmGet$id()));
                } else {
                    Table.throwDuplicatePrimaryKeyException(valueOf);
                }
                long j3 = j;
                map2.put(message, Long.valueOf(j3));
                String realmGet$type = messageRealmProxyInterface.realmGet$type();
                if (realmGet$type != null) {
                    Table.nativeSetString(nativePtr, messageColumnInfo.typeIndex, j3, realmGet$type, false);
                }
                String realmGet$title = messageRealmProxyInterface.realmGet$title();
                if (realmGet$title != null) {
                    Table.nativeSetString(nativePtr, messageColumnInfo.titleIndex, j3, realmGet$title, false);
                }
                String realmGet$body = messageRealmProxyInterface.realmGet$body();
                if (realmGet$body != null) {
                    Table.nativeSetString(nativePtr, messageColumnInfo.bodyIndex, j3, realmGet$body, false);
                }
                String realmGet$date = messageRealmProxyInterface.realmGet$date();
                if (realmGet$date != null) {
                    Table.nativeSetString(nativePtr, messageColumnInfo.dateIndex, j3, realmGet$date, false);
                }
                Table.nativeSetBoolean(nativePtr, messageColumnInfo.readIndex, j3, messageRealmProxyInterface.realmGet$read(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, Message message, Map<RealmModel, Long> map) {
        Message message2 = message;
        if (message2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) message2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(Message.class);
        long nativePtr = table.getNativePtr();
        MessageColumnInfo messageColumnInfo = (MessageColumnInfo) realm.getSchema().getColumnInfo(Message.class);
        long j = messageColumnInfo.idIndex;
        MessageRealmProxyInterface messageRealmProxyInterface = message2;
        long nativeFindFirstInt = Integer.valueOf(messageRealmProxyInterface.realmGet$id()) != null ? Table.nativeFindFirstInt(nativePtr, j, (long) messageRealmProxyInterface.realmGet$id()) : -1;
        long createRowWithPrimaryKey = nativeFindFirstInt == -1 ? OsObject.createRowWithPrimaryKey(table, j, Integer.valueOf(messageRealmProxyInterface.realmGet$id())) : nativeFindFirstInt;
        map.put(message2, Long.valueOf(createRowWithPrimaryKey));
        String realmGet$type = messageRealmProxyInterface.realmGet$type();
        if (realmGet$type != null) {
            Table.nativeSetString(nativePtr, messageColumnInfo.typeIndex, createRowWithPrimaryKey, realmGet$type, false);
        } else {
            Table.nativeSetNull(nativePtr, messageColumnInfo.typeIndex, createRowWithPrimaryKey, false);
        }
        String realmGet$title = messageRealmProxyInterface.realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(nativePtr, messageColumnInfo.titleIndex, createRowWithPrimaryKey, realmGet$title, false);
        } else {
            Table.nativeSetNull(nativePtr, messageColumnInfo.titleIndex, createRowWithPrimaryKey, false);
        }
        String realmGet$body = messageRealmProxyInterface.realmGet$body();
        if (realmGet$body != null) {
            Table.nativeSetString(nativePtr, messageColumnInfo.bodyIndex, createRowWithPrimaryKey, realmGet$body, false);
        } else {
            Table.nativeSetNull(nativePtr, messageColumnInfo.bodyIndex, createRowWithPrimaryKey, false);
        }
        String realmGet$date = messageRealmProxyInterface.realmGet$date();
        if (realmGet$date != null) {
            Table.nativeSetString(nativePtr, messageColumnInfo.dateIndex, createRowWithPrimaryKey, realmGet$date, false);
        } else {
            Table.nativeSetNull(nativePtr, messageColumnInfo.dateIndex, createRowWithPrimaryKey, false);
        }
        Table.nativeSetBoolean(nativePtr, messageColumnInfo.readIndex, createRowWithPrimaryKey, messageRealmProxyInterface.realmGet$read(), false);
        return createRowWithPrimaryKey;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(Message.class);
        long nativePtr = table.getNativePtr();
        MessageColumnInfo messageColumnInfo = (MessageColumnInfo) realm.getSchema().getColumnInfo(Message.class);
        long j2 = messageColumnInfo.idIndex;
        while (it.hasNext()) {
            Message message = (Message) it.next();
            if (!map2.containsKey(message)) {
                if (message instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) message;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(message, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                MessageRealmProxyInterface messageRealmProxyInterface = message;
                if (Integer.valueOf(messageRealmProxyInterface.realmGet$id()) != null) {
                    j = Table.nativeFindFirstInt(nativePtr, j2, (long) messageRealmProxyInterface.realmGet$id());
                } else {
                    j = -1;
                }
                if (j == -1) {
                    j = OsObject.createRowWithPrimaryKey(table, j2, Integer.valueOf(messageRealmProxyInterface.realmGet$id()));
                }
                long j3 = j;
                map2.put(message, Long.valueOf(j3));
                String realmGet$type = messageRealmProxyInterface.realmGet$type();
                if (realmGet$type != null) {
                    Table.nativeSetString(nativePtr, messageColumnInfo.typeIndex, j3, realmGet$type, false);
                } else {
                    Table.nativeSetNull(nativePtr, messageColumnInfo.typeIndex, j3, false);
                }
                String realmGet$title = messageRealmProxyInterface.realmGet$title();
                if (realmGet$title != null) {
                    Table.nativeSetString(nativePtr, messageColumnInfo.titleIndex, j3, realmGet$title, false);
                } else {
                    Table.nativeSetNull(nativePtr, messageColumnInfo.titleIndex, j3, false);
                }
                String realmGet$body = messageRealmProxyInterface.realmGet$body();
                if (realmGet$body != null) {
                    Table.nativeSetString(nativePtr, messageColumnInfo.bodyIndex, j3, realmGet$body, false);
                } else {
                    Table.nativeSetNull(nativePtr, messageColumnInfo.bodyIndex, j3, false);
                }
                String realmGet$date = messageRealmProxyInterface.realmGet$date();
                if (realmGet$date != null) {
                    Table.nativeSetString(nativePtr, messageColumnInfo.dateIndex, j3, realmGet$date, false);
                } else {
                    Table.nativeSetNull(nativePtr, messageColumnInfo.dateIndex, j3, false);
                }
                Table.nativeSetBoolean(nativePtr, messageColumnInfo.readIndex, j3, messageRealmProxyInterface.realmGet$read(), false);
            }
        }
    }

    public static Message createDetachedCopy(Message message, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        Message message2;
        if (i > i2 || message == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(message);
        if (cacheData == null) {
            message2 = new Message();
            map.put(message, new CacheData(i, message2));
        } else if (i >= cacheData.minDepth) {
            return (Message) cacheData.object;
        } else {
            Message message3 = (Message) cacheData.object;
            cacheData.minDepth = i;
            message2 = message3;
        }
        MessageRealmProxyInterface messageRealmProxyInterface = message2;
        MessageRealmProxyInterface messageRealmProxyInterface2 = message;
        messageRealmProxyInterface.realmSet$id(messageRealmProxyInterface2.realmGet$id());
        messageRealmProxyInterface.realmSet$type(messageRealmProxyInterface2.realmGet$type());
        messageRealmProxyInterface.realmSet$title(messageRealmProxyInterface2.realmGet$title());
        messageRealmProxyInterface.realmSet$body(messageRealmProxyInterface2.realmGet$body());
        messageRealmProxyInterface.realmSet$date(messageRealmProxyInterface2.realmGet$date());
        messageRealmProxyInterface.realmSet$read(messageRealmProxyInterface2.realmGet$read());
        return message2;
    }

    static Message update(Realm realm, Message message, Message message2, Map<RealmModel, RealmObjectProxy> map) {
        MessageRealmProxyInterface messageRealmProxyInterface = message;
        MessageRealmProxyInterface messageRealmProxyInterface2 = message2;
        messageRealmProxyInterface.realmSet$type(messageRealmProxyInterface2.realmGet$type());
        messageRealmProxyInterface.realmSet$title(messageRealmProxyInterface2.realmGet$title());
        messageRealmProxyInterface.realmSet$body(messageRealmProxyInterface2.realmGet$body());
        messageRealmProxyInterface.realmSet$date(messageRealmProxyInterface2.realmGet$date());
        messageRealmProxyInterface.realmSet$read(messageRealmProxyInterface2.realmGet$read());
        return message;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Message = proxy[");
        sb.append("{id:");
        sb.append(realmGet$id());
        sb.append("}");
        sb.append(",");
        sb.append("{type:");
        sb.append(realmGet$type() != null ? realmGet$type() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{title:");
        sb.append(realmGet$title() != null ? realmGet$title() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{body:");
        sb.append(realmGet$body() != null ? realmGet$body() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{date:");
        sb.append(realmGet$date() != null ? realmGet$date() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{read:");
        sb.append(realmGet$read());
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MessageRealmProxy messageRealmProxy = (MessageRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = messageRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = messageRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == messageRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
