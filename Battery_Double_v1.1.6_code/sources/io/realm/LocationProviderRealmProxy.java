package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mansoon.BatteryDouble.models.data.LocationProvider;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationProviderRealmProxy extends LocationProvider implements RealmObjectProxy, LocationProviderRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private LocationProviderColumnInfo columnInfo;
    private ProxyState<LocationProvider> proxyState;

    static final class LocationProviderColumnInfo extends ColumnInfo {
        long providerIndex;

        LocationProviderColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(1);
            this.providerIndex = addColumnDetails("provider", osSchemaInfo.getObjectSchemaInfo("LocationProvider"));
        }

        LocationProviderColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new LocationProviderColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            ((LocationProviderColumnInfo) columnInfo2).providerIndex = ((LocationProviderColumnInfo) columnInfo).providerIndex;
        }
    }

    public static String getTableName() {
        return "class_LocationProvider";
    }

    static {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add("provider");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    LocationProviderRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (LocationProviderColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public String realmGet$provider() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.providerIndex);
    }

    public void realmSet$provider(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.providerIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.providerIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.providerIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.providerIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("LocationProvider", 1, 0);
        builder.addPersistedProperty("provider", RealmFieldType.STRING, false, false, false);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static LocationProviderColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new LocationProviderColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static LocationProvider createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        LocationProvider locationProvider = (LocationProvider) realm.createObjectInternal(LocationProvider.class, true, Collections.emptyList());
        LocationProviderRealmProxyInterface locationProviderRealmProxyInterface = locationProvider;
        if (jSONObject.has("provider")) {
            if (jSONObject.isNull("provider")) {
                locationProviderRealmProxyInterface.realmSet$provider(null);
            } else {
                locationProviderRealmProxyInterface.realmSet$provider(jSONObject.getString("provider"));
            }
        }
        return locationProvider;
    }

    @TargetApi(11)
    public static LocationProvider createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        LocationProvider locationProvider = new LocationProvider();
        LocationProviderRealmProxyInterface locationProviderRealmProxyInterface = locationProvider;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            if (!jsonReader.nextName().equals("provider")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                locationProviderRealmProxyInterface.realmSet$provider(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
                locationProviderRealmProxyInterface.realmSet$provider(null);
            }
        }
        jsonReader.endObject();
        return (LocationProvider) realm.copyToRealm(locationProvider);
    }

    public static LocationProvider copyOrUpdate(Realm realm, LocationProvider locationProvider, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (locationProvider instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) locationProvider;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return locationProvider;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(locationProvider);
        if (realmObjectProxy2 != null) {
            return (LocationProvider) realmObjectProxy2;
        }
        return copy(realm, locationProvider, z, map);
    }

    public static LocationProvider copy(Realm realm, LocationProvider locationProvider, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(locationProvider);
        if (realmObjectProxy != null) {
            return (LocationProvider) realmObjectProxy;
        }
        LocationProvider locationProvider2 = (LocationProvider) realm.createObjectInternal(LocationProvider.class, false, Collections.emptyList());
        map.put(locationProvider, (RealmObjectProxy) locationProvider2);
        locationProvider2.realmSet$provider(locationProvider.realmGet$provider());
        return locationProvider2;
    }

    public static long insert(Realm realm, LocationProvider locationProvider, Map<RealmModel, Long> map) {
        if (locationProvider instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) locationProvider;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(LocationProvider.class);
        long nativePtr = table.getNativePtr();
        LocationProviderColumnInfo locationProviderColumnInfo = (LocationProviderColumnInfo) realm.getSchema().getColumnInfo(LocationProvider.class);
        long createRow = OsObject.createRow(table);
        map.put(locationProvider, Long.valueOf(createRow));
        String realmGet$provider = locationProvider.realmGet$provider();
        if (realmGet$provider != null) {
            Table.nativeSetString(nativePtr, locationProviderColumnInfo.providerIndex, createRow, realmGet$provider, false);
        }
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Table table = realm.getTable(LocationProvider.class);
        long nativePtr = table.getNativePtr();
        LocationProviderColumnInfo locationProviderColumnInfo = (LocationProviderColumnInfo) realm.getSchema().getColumnInfo(LocationProvider.class);
        while (it.hasNext()) {
            LocationProvider locationProvider = (LocationProvider) it.next();
            if (!map.containsKey(locationProvider)) {
                if (locationProvider instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) locationProvider;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map.put(locationProvider, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map.put(locationProvider, Long.valueOf(createRow));
                String realmGet$provider = locationProvider.realmGet$provider();
                if (realmGet$provider != null) {
                    Table.nativeSetString(nativePtr, locationProviderColumnInfo.providerIndex, createRow, realmGet$provider, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, LocationProvider locationProvider, Map<RealmModel, Long> map) {
        if (locationProvider instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) locationProvider;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(LocationProvider.class);
        long nativePtr = table.getNativePtr();
        LocationProviderColumnInfo locationProviderColumnInfo = (LocationProviderColumnInfo) realm.getSchema().getColumnInfo(LocationProvider.class);
        long createRow = OsObject.createRow(table);
        map.put(locationProvider, Long.valueOf(createRow));
        String realmGet$provider = locationProvider.realmGet$provider();
        if (realmGet$provider != null) {
            Table.nativeSetString(nativePtr, locationProviderColumnInfo.providerIndex, createRow, realmGet$provider, false);
        } else {
            Table.nativeSetNull(nativePtr, locationProviderColumnInfo.providerIndex, createRow, false);
        }
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Table table = realm.getTable(LocationProvider.class);
        long nativePtr = table.getNativePtr();
        LocationProviderColumnInfo locationProviderColumnInfo = (LocationProviderColumnInfo) realm.getSchema().getColumnInfo(LocationProvider.class);
        while (it.hasNext()) {
            LocationProvider locationProvider = (LocationProvider) it.next();
            if (!map.containsKey(locationProvider)) {
                if (locationProvider instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) locationProvider;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map.put(locationProvider, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map.put(locationProvider, Long.valueOf(createRow));
                String realmGet$provider = locationProvider.realmGet$provider();
                if (realmGet$provider != null) {
                    Table.nativeSetString(nativePtr, locationProviderColumnInfo.providerIndex, createRow, realmGet$provider, false);
                } else {
                    Table.nativeSetNull(nativePtr, locationProviderColumnInfo.providerIndex, createRow, false);
                }
            }
        }
    }

    public static LocationProvider createDetachedCopy(LocationProvider locationProvider, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        LocationProvider locationProvider2;
        if (i > i2 || locationProvider == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(locationProvider);
        if (cacheData == null) {
            locationProvider2 = new LocationProvider();
            map.put(locationProvider, new CacheData(i, locationProvider2));
        } else if (i >= cacheData.minDepth) {
            return (LocationProvider) cacheData.object;
        } else {
            LocationProvider locationProvider3 = (LocationProvider) cacheData.object;
            cacheData.minDepth = i;
            locationProvider2 = locationProvider3;
        }
        locationProvider2.realmSet$provider(locationProvider.realmGet$provider());
        return locationProvider2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("LocationProvider = proxy[");
        sb.append("{provider:");
        sb.append(realmGet$provider() != null ? realmGet$provider() : "null");
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LocationProviderRealmProxy locationProviderRealmProxy = (LocationProviderRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = locationProviderRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = locationProviderRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == locationProviderRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
