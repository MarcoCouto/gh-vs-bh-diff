package io.realm;

public interface AppPermissionRealmProxyInterface {
    String realmGet$permission();

    void realmSet$permission(String str);
}
