package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.chartboost.sdk.CBLocation;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.precache.DownloadManager;
import com.mansoon.BatteryDouble.models.data.BatteryDetails;
import com.mansoon.BatteryDouble.models.data.CallInfo;
import com.mansoon.BatteryDouble.models.data.CpuStatus;
import com.mansoon.BatteryDouble.models.data.Feature;
import com.mansoon.BatteryDouble.models.data.LocationProvider;
import com.mansoon.BatteryDouble.models.data.NetworkDetails;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import com.mansoon.BatteryDouble.models.data.Sample;
import com.mansoon.BatteryDouble.models.data.Settings;
import com.mansoon.BatteryDouble.models.data.StorageDetails;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.exceptions.RealmException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SampleRealmProxy extends Sample implements RealmObjectProxy, SampleRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private SampleColumnInfo columnInfo;
    private RealmList<Feature> featuresRealmList;
    private RealmList<LocationProvider> locationProvidersRealmList;
    private RealmList<ProcessInfo> processInfosRealmList;
    private ProxyState<Sample> proxyState;

    static final class SampleColumnInfo extends ColumnInfo {
        long batteryDetailsIndex;
        long batteryLevelIndex;
        long batteryStateIndex;
        long callInfoIndex;
        long countryCodeIndex;
        long cpuStatusIndex;
        long databaseIndex;
        long distanceTraveledIndex;
        long featuresIndex;
        long idIndex;
        long locationProvidersIndex;
        long memoryActiveIndex;
        long memoryFreeIndex;
        long memoryInactiveIndex;
        long memoryUserIndex;
        long memoryWiredIndex;
        long networkDetailsIndex;
        long networkStatusIndex;
        long processInfosIndex;
        long screenBrightnessIndex;
        long screenOnIndex;
        long settingsIndex;
        long storageDetailsIndex;
        long timeZoneIndex;
        long timestampIndex;
        long triggeredByIndex;
        long uuIdIndex;
        long versionIndex;

        SampleColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(28);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("Sample");
            this.idIndex = addColumnDetails("id", objectSchemaInfo);
            this.uuIdIndex = addColumnDetails("uuId", objectSchemaInfo);
            this.timestampIndex = addColumnDetails("timestamp", objectSchemaInfo);
            this.versionIndex = addColumnDetails("version", objectSchemaInfo);
            this.databaseIndex = addColumnDetails("database", objectSchemaInfo);
            this.batteryStateIndex = addColumnDetails("batteryState", objectSchemaInfo);
            this.batteryLevelIndex = addColumnDetails(RequestParameters.BATTERY_LEVEL, objectSchemaInfo);
            this.memoryWiredIndex = addColumnDetails("memoryWired", objectSchemaInfo);
            this.memoryActiveIndex = addColumnDetails("memoryActive", objectSchemaInfo);
            this.memoryInactiveIndex = addColumnDetails("memoryInactive", objectSchemaInfo);
            this.memoryFreeIndex = addColumnDetails("memoryFree", objectSchemaInfo);
            this.memoryUserIndex = addColumnDetails("memoryUser", objectSchemaInfo);
            this.triggeredByIndex = addColumnDetails("triggeredBy", objectSchemaInfo);
            this.networkStatusIndex = addColumnDetails("networkStatus", objectSchemaInfo);
            this.distanceTraveledIndex = addColumnDetails("distanceTraveled", objectSchemaInfo);
            this.screenBrightnessIndex = addColumnDetails("screenBrightness", objectSchemaInfo);
            this.networkDetailsIndex = addColumnDetails("networkDetails", objectSchemaInfo);
            this.batteryDetailsIndex = addColumnDetails("batteryDetails", objectSchemaInfo);
            this.cpuStatusIndex = addColumnDetails("cpuStatus", objectSchemaInfo);
            this.callInfoIndex = addColumnDetails("callInfo", objectSchemaInfo);
            this.screenOnIndex = addColumnDetails("screenOn", objectSchemaInfo);
            this.timeZoneIndex = addColumnDetails("timeZone", objectSchemaInfo);
            this.settingsIndex = addColumnDetails(DownloadManager.SETTINGS, objectSchemaInfo);
            this.storageDetailsIndex = addColumnDetails("storageDetails", objectSchemaInfo);
            this.countryCodeIndex = addColumnDetails("countryCode", objectSchemaInfo);
            this.locationProvidersIndex = addColumnDetails("locationProviders", objectSchemaInfo);
            this.processInfosIndex = addColumnDetails("processInfos", objectSchemaInfo);
            this.featuresIndex = addColumnDetails(SettingsJsonConstants.FEATURES_KEY, objectSchemaInfo);
        }

        SampleColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new SampleColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            SampleColumnInfo sampleColumnInfo = (SampleColumnInfo) columnInfo;
            SampleColumnInfo sampleColumnInfo2 = (SampleColumnInfo) columnInfo2;
            sampleColumnInfo2.idIndex = sampleColumnInfo.idIndex;
            sampleColumnInfo2.uuIdIndex = sampleColumnInfo.uuIdIndex;
            sampleColumnInfo2.timestampIndex = sampleColumnInfo.timestampIndex;
            sampleColumnInfo2.versionIndex = sampleColumnInfo.versionIndex;
            sampleColumnInfo2.databaseIndex = sampleColumnInfo.databaseIndex;
            sampleColumnInfo2.batteryStateIndex = sampleColumnInfo.batteryStateIndex;
            sampleColumnInfo2.batteryLevelIndex = sampleColumnInfo.batteryLevelIndex;
            sampleColumnInfo2.memoryWiredIndex = sampleColumnInfo.memoryWiredIndex;
            sampleColumnInfo2.memoryActiveIndex = sampleColumnInfo.memoryActiveIndex;
            sampleColumnInfo2.memoryInactiveIndex = sampleColumnInfo.memoryInactiveIndex;
            sampleColumnInfo2.memoryFreeIndex = sampleColumnInfo.memoryFreeIndex;
            sampleColumnInfo2.memoryUserIndex = sampleColumnInfo.memoryUserIndex;
            sampleColumnInfo2.triggeredByIndex = sampleColumnInfo.triggeredByIndex;
            sampleColumnInfo2.networkStatusIndex = sampleColumnInfo.networkStatusIndex;
            sampleColumnInfo2.distanceTraveledIndex = sampleColumnInfo.distanceTraveledIndex;
            sampleColumnInfo2.screenBrightnessIndex = sampleColumnInfo.screenBrightnessIndex;
            sampleColumnInfo2.networkDetailsIndex = sampleColumnInfo.networkDetailsIndex;
            sampleColumnInfo2.batteryDetailsIndex = sampleColumnInfo.batteryDetailsIndex;
            sampleColumnInfo2.cpuStatusIndex = sampleColumnInfo.cpuStatusIndex;
            sampleColumnInfo2.callInfoIndex = sampleColumnInfo.callInfoIndex;
            sampleColumnInfo2.screenOnIndex = sampleColumnInfo.screenOnIndex;
            sampleColumnInfo2.timeZoneIndex = sampleColumnInfo.timeZoneIndex;
            sampleColumnInfo2.settingsIndex = sampleColumnInfo.settingsIndex;
            sampleColumnInfo2.storageDetailsIndex = sampleColumnInfo.storageDetailsIndex;
            sampleColumnInfo2.countryCodeIndex = sampleColumnInfo.countryCodeIndex;
            sampleColumnInfo2.locationProvidersIndex = sampleColumnInfo.locationProvidersIndex;
            sampleColumnInfo2.processInfosIndex = sampleColumnInfo.processInfosIndex;
            sampleColumnInfo2.featuresIndex = sampleColumnInfo.featuresIndex;
        }
    }

    public static String getTableName() {
        return "class_Sample";
    }

    static {
        ArrayList arrayList = new ArrayList(28);
        arrayList.add("id");
        arrayList.add("uuId");
        arrayList.add("timestamp");
        arrayList.add("version");
        arrayList.add("database");
        arrayList.add("batteryState");
        arrayList.add(RequestParameters.BATTERY_LEVEL);
        arrayList.add("memoryWired");
        arrayList.add("memoryActive");
        arrayList.add("memoryInactive");
        arrayList.add("memoryFree");
        arrayList.add("memoryUser");
        arrayList.add("triggeredBy");
        arrayList.add("networkStatus");
        arrayList.add("distanceTraveled");
        arrayList.add("screenBrightness");
        arrayList.add("networkDetails");
        arrayList.add("batteryDetails");
        arrayList.add("cpuStatus");
        arrayList.add("callInfo");
        arrayList.add("screenOn");
        arrayList.add("timeZone");
        arrayList.add(DownloadManager.SETTINGS);
        arrayList.add("storageDetails");
        arrayList.add("countryCode");
        arrayList.add("locationProviders");
        arrayList.add("processInfos");
        arrayList.add(SettingsJsonConstants.FEATURES_KEY);
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    SampleRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (SampleColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public int realmGet$id() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.idIndex);
    }

    public void realmSet$id(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
        }
    }

    public String realmGet$uuId() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.uuIdIndex);
    }

    public void realmSet$uuId(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.uuIdIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.uuIdIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.uuIdIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.uuIdIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public long realmGet$timestamp() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getLong(this.columnInfo.timestampIndex);
    }

    public void realmSet$timestamp(long j) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.timestampIndex, j);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.timestampIndex, row$realm.getIndex(), j, true);
        }
    }

    public int realmGet$version() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.versionIndex);
    }

    public void realmSet$version(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.versionIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.versionIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$database() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.databaseIndex);
    }

    public void realmSet$database(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.databaseIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.databaseIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$batteryState() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.batteryStateIndex);
    }

    public void realmSet$batteryState(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.batteryStateIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.batteryStateIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.batteryStateIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.batteryStateIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public double realmGet$batteryLevel() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.batteryLevelIndex);
    }

    public void realmSet$batteryLevel(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.batteryLevelIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.batteryLevelIndex, row$realm.getIndex(), d, true);
        }
    }

    public int realmGet$memoryWired() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.memoryWiredIndex);
    }

    public void realmSet$memoryWired(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.memoryWiredIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.memoryWiredIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$memoryActive() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.memoryActiveIndex);
    }

    public void realmSet$memoryActive(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.memoryActiveIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.memoryActiveIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$memoryInactive() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.memoryInactiveIndex);
    }

    public void realmSet$memoryInactive(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.memoryInactiveIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.memoryInactiveIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$memoryFree() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.memoryFreeIndex);
    }

    public void realmSet$memoryFree(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.memoryFreeIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.memoryFreeIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$memoryUser() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.memoryUserIndex);
    }

    public void realmSet$memoryUser(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.memoryUserIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.memoryUserIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$triggeredBy() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.triggeredByIndex);
    }

    public void realmSet$triggeredBy(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.triggeredByIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.triggeredByIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.triggeredByIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.triggeredByIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$networkStatus() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.networkStatusIndex);
    }

    public void realmSet$networkStatus(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.networkStatusIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.networkStatusIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.networkStatusIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.networkStatusIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public double realmGet$distanceTraveled() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.distanceTraveledIndex);
    }

    public void realmSet$distanceTraveled(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.distanceTraveledIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.distanceTraveledIndex, row$realm.getIndex(), d, true);
        }
    }

    public int realmGet$screenBrightness() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.screenBrightnessIndex);
    }

    public void realmSet$screenBrightness(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.screenBrightnessIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.screenBrightnessIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public NetworkDetails realmGet$networkDetails() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.proxyState.getRow$realm().isNullLink(this.columnInfo.networkDetailsIndex)) {
            return null;
        }
        return (NetworkDetails) this.proxyState.getRealm$realm().get(NetworkDetails.class, this.proxyState.getRow$realm().getLink(this.columnInfo.networkDetailsIndex), false, Collections.emptyList());
    }

    public void realmSet$networkDetails(NetworkDetails networkDetails) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (networkDetails == null) {
                this.proxyState.getRow$realm().nullifyLink(this.columnInfo.networkDetailsIndex);
                return;
            }
            this.proxyState.checkValidObject(networkDetails);
            this.proxyState.getRow$realm().setLink(this.columnInfo.networkDetailsIndex, ((RealmObjectProxy) networkDetails).realmGet$proxyState().getRow$realm().getIndex());
        } else if (this.proxyState.getAcceptDefaultValue$realm() && !this.proxyState.getExcludeFields$realm().contains("networkDetails")) {
            if (networkDetails != null && !RealmObject.isManaged(networkDetails)) {
                networkDetails = (NetworkDetails) ((Realm) this.proxyState.getRealm$realm()).copyToRealm(networkDetails);
            }
            Row row$realm = this.proxyState.getRow$realm();
            if (networkDetails == null) {
                row$realm.nullifyLink(this.columnInfo.networkDetailsIndex);
                return;
            }
            this.proxyState.checkValidObject(networkDetails);
            row$realm.getTable().setLink(this.columnInfo.networkDetailsIndex, row$realm.getIndex(), ((RealmObjectProxy) networkDetails).realmGet$proxyState().getRow$realm().getIndex(), true);
        }
    }

    public BatteryDetails realmGet$batteryDetails() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.proxyState.getRow$realm().isNullLink(this.columnInfo.batteryDetailsIndex)) {
            return null;
        }
        return (BatteryDetails) this.proxyState.getRealm$realm().get(BatteryDetails.class, this.proxyState.getRow$realm().getLink(this.columnInfo.batteryDetailsIndex), false, Collections.emptyList());
    }

    public void realmSet$batteryDetails(BatteryDetails batteryDetails) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (batteryDetails == null) {
                this.proxyState.getRow$realm().nullifyLink(this.columnInfo.batteryDetailsIndex);
                return;
            }
            this.proxyState.checkValidObject(batteryDetails);
            this.proxyState.getRow$realm().setLink(this.columnInfo.batteryDetailsIndex, ((RealmObjectProxy) batteryDetails).realmGet$proxyState().getRow$realm().getIndex());
        } else if (this.proxyState.getAcceptDefaultValue$realm() && !this.proxyState.getExcludeFields$realm().contains("batteryDetails")) {
            if (batteryDetails != null && !RealmObject.isManaged(batteryDetails)) {
                batteryDetails = (BatteryDetails) ((Realm) this.proxyState.getRealm$realm()).copyToRealm(batteryDetails);
            }
            Row row$realm = this.proxyState.getRow$realm();
            if (batteryDetails == null) {
                row$realm.nullifyLink(this.columnInfo.batteryDetailsIndex);
                return;
            }
            this.proxyState.checkValidObject(batteryDetails);
            row$realm.getTable().setLink(this.columnInfo.batteryDetailsIndex, row$realm.getIndex(), ((RealmObjectProxy) batteryDetails).realmGet$proxyState().getRow$realm().getIndex(), true);
        }
    }

    public CpuStatus realmGet$cpuStatus() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.proxyState.getRow$realm().isNullLink(this.columnInfo.cpuStatusIndex)) {
            return null;
        }
        return (CpuStatus) this.proxyState.getRealm$realm().get(CpuStatus.class, this.proxyState.getRow$realm().getLink(this.columnInfo.cpuStatusIndex), false, Collections.emptyList());
    }

    public void realmSet$cpuStatus(CpuStatus cpuStatus) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (cpuStatus == null) {
                this.proxyState.getRow$realm().nullifyLink(this.columnInfo.cpuStatusIndex);
                return;
            }
            this.proxyState.checkValidObject(cpuStatus);
            this.proxyState.getRow$realm().setLink(this.columnInfo.cpuStatusIndex, ((RealmObjectProxy) cpuStatus).realmGet$proxyState().getRow$realm().getIndex());
        } else if (this.proxyState.getAcceptDefaultValue$realm() && !this.proxyState.getExcludeFields$realm().contains("cpuStatus")) {
            if (cpuStatus != null && !RealmObject.isManaged(cpuStatus)) {
                cpuStatus = (CpuStatus) ((Realm) this.proxyState.getRealm$realm()).copyToRealm(cpuStatus);
            }
            Row row$realm = this.proxyState.getRow$realm();
            if (cpuStatus == null) {
                row$realm.nullifyLink(this.columnInfo.cpuStatusIndex);
                return;
            }
            this.proxyState.checkValidObject(cpuStatus);
            row$realm.getTable().setLink(this.columnInfo.cpuStatusIndex, row$realm.getIndex(), ((RealmObjectProxy) cpuStatus).realmGet$proxyState().getRow$realm().getIndex(), true);
        }
    }

    public CallInfo realmGet$callInfo() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.proxyState.getRow$realm().isNullLink(this.columnInfo.callInfoIndex)) {
            return null;
        }
        return (CallInfo) this.proxyState.getRealm$realm().get(CallInfo.class, this.proxyState.getRow$realm().getLink(this.columnInfo.callInfoIndex), false, Collections.emptyList());
    }

    public void realmSet$callInfo(CallInfo callInfo) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (callInfo == null) {
                this.proxyState.getRow$realm().nullifyLink(this.columnInfo.callInfoIndex);
                return;
            }
            this.proxyState.checkValidObject(callInfo);
            this.proxyState.getRow$realm().setLink(this.columnInfo.callInfoIndex, ((RealmObjectProxy) callInfo).realmGet$proxyState().getRow$realm().getIndex());
        } else if (this.proxyState.getAcceptDefaultValue$realm() && !this.proxyState.getExcludeFields$realm().contains("callInfo")) {
            if (callInfo != null && !RealmObject.isManaged(callInfo)) {
                callInfo = (CallInfo) ((Realm) this.proxyState.getRealm$realm()).copyToRealm(callInfo);
            }
            Row row$realm = this.proxyState.getRow$realm();
            if (callInfo == null) {
                row$realm.nullifyLink(this.columnInfo.callInfoIndex);
                return;
            }
            this.proxyState.checkValidObject(callInfo);
            row$realm.getTable().setLink(this.columnInfo.callInfoIndex, row$realm.getIndex(), ((RealmObjectProxy) callInfo).realmGet$proxyState().getRow$realm().getIndex(), true);
        }
    }

    public int realmGet$screenOn() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.screenOnIndex);
    }

    public void realmSet$screenOn(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.screenOnIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.screenOnIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$timeZone() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.timeZoneIndex);
    }

    public void realmSet$timeZone(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.timeZoneIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.timeZoneIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.timeZoneIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.timeZoneIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public Settings realmGet$settings() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.proxyState.getRow$realm().isNullLink(this.columnInfo.settingsIndex)) {
            return null;
        }
        return (Settings) this.proxyState.getRealm$realm().get(Settings.class, this.proxyState.getRow$realm().getLink(this.columnInfo.settingsIndex), false, Collections.emptyList());
    }

    public void realmSet$settings(Settings settings) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (settings == null) {
                this.proxyState.getRow$realm().nullifyLink(this.columnInfo.settingsIndex);
                return;
            }
            this.proxyState.checkValidObject(settings);
            this.proxyState.getRow$realm().setLink(this.columnInfo.settingsIndex, ((RealmObjectProxy) settings).realmGet$proxyState().getRow$realm().getIndex());
        } else if (this.proxyState.getAcceptDefaultValue$realm() && !this.proxyState.getExcludeFields$realm().contains(DownloadManager.SETTINGS)) {
            if (settings != null && !RealmObject.isManaged(settings)) {
                settings = (Settings) ((Realm) this.proxyState.getRealm$realm()).copyToRealm(settings);
            }
            Row row$realm = this.proxyState.getRow$realm();
            if (settings == null) {
                row$realm.nullifyLink(this.columnInfo.settingsIndex);
                return;
            }
            this.proxyState.checkValidObject(settings);
            row$realm.getTable().setLink(this.columnInfo.settingsIndex, row$realm.getIndex(), ((RealmObjectProxy) settings).realmGet$proxyState().getRow$realm().getIndex(), true);
        }
    }

    public StorageDetails realmGet$storageDetails() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.proxyState.getRow$realm().isNullLink(this.columnInfo.storageDetailsIndex)) {
            return null;
        }
        return (StorageDetails) this.proxyState.getRealm$realm().get(StorageDetails.class, this.proxyState.getRow$realm().getLink(this.columnInfo.storageDetailsIndex), false, Collections.emptyList());
    }

    public void realmSet$storageDetails(StorageDetails storageDetails) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (storageDetails == null) {
                this.proxyState.getRow$realm().nullifyLink(this.columnInfo.storageDetailsIndex);
                return;
            }
            this.proxyState.checkValidObject(storageDetails);
            this.proxyState.getRow$realm().setLink(this.columnInfo.storageDetailsIndex, ((RealmObjectProxy) storageDetails).realmGet$proxyState().getRow$realm().getIndex());
        } else if (this.proxyState.getAcceptDefaultValue$realm() && !this.proxyState.getExcludeFields$realm().contains("storageDetails")) {
            if (storageDetails != null && !RealmObject.isManaged(storageDetails)) {
                storageDetails = (StorageDetails) ((Realm) this.proxyState.getRealm$realm()).copyToRealm(storageDetails);
            }
            Row row$realm = this.proxyState.getRow$realm();
            if (storageDetails == null) {
                row$realm.nullifyLink(this.columnInfo.storageDetailsIndex);
                return;
            }
            this.proxyState.checkValidObject(storageDetails);
            row$realm.getTable().setLink(this.columnInfo.storageDetailsIndex, row$realm.getIndex(), ((RealmObjectProxy) storageDetails).realmGet$proxyState().getRow$realm().getIndex(), true);
        }
    }

    public String realmGet$countryCode() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.countryCodeIndex);
    }

    public void realmSet$countryCode(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.countryCodeIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.countryCodeIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.countryCodeIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.countryCodeIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public RealmList<LocationProvider> realmGet$locationProviders() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.locationProvidersRealmList != null) {
            return this.locationProvidersRealmList;
        }
        this.locationProvidersRealmList = new RealmList<>(LocationProvider.class, this.proxyState.getRow$realm().getModelList(this.columnInfo.locationProvidersIndex), this.proxyState.getRealm$realm());
        return this.locationProvidersRealmList;
    }

    public void realmSet$locationProviders(RealmList<LocationProvider> realmList) {
        if (this.proxyState.isUnderConstruction()) {
            if (!this.proxyState.getAcceptDefaultValue$realm() || this.proxyState.getExcludeFields$realm().contains("locationProviders")) {
                return;
            }
            if (realmList != null && !realmList.isManaged()) {
                Realm realm = (Realm) this.proxyState.getRealm$realm();
                RealmList<LocationProvider> realmList2 = new RealmList<>();
                Iterator it = realmList.iterator();
                while (it.hasNext()) {
                    LocationProvider locationProvider = (LocationProvider) it.next();
                    if (locationProvider == null || RealmObject.isManaged(locationProvider)) {
                        realmList2.add(locationProvider);
                    } else {
                        realmList2.add(realm.copyToRealm(locationProvider));
                    }
                }
                realmList = realmList2;
            }
        }
        this.proxyState.getRealm$realm().checkIfValid();
        OsList modelList = this.proxyState.getRow$realm().getModelList(this.columnInfo.locationProvidersIndex);
        int i = 0;
        if (realmList == null || ((long) realmList.size()) != modelList.size()) {
            modelList.removeAll();
            if (realmList != null) {
                int size = realmList.size();
                while (i < size) {
                    LocationProvider locationProvider2 = (LocationProvider) realmList.get(i);
                    this.proxyState.checkValidObject(locationProvider2);
                    modelList.addRow(((RealmObjectProxy) locationProvider2).realmGet$proxyState().getRow$realm().getIndex());
                    i++;
                }
            }
        } else {
            int size2 = realmList.size();
            while (i < size2) {
                LocationProvider locationProvider3 = (LocationProvider) realmList.get(i);
                this.proxyState.checkValidObject(locationProvider3);
                modelList.setRow((long) i, ((RealmObjectProxy) locationProvider3).realmGet$proxyState().getRow$realm().getIndex());
                i++;
            }
        }
    }

    public RealmList<ProcessInfo> realmGet$processInfos() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.processInfosRealmList != null) {
            return this.processInfosRealmList;
        }
        this.processInfosRealmList = new RealmList<>(ProcessInfo.class, this.proxyState.getRow$realm().getModelList(this.columnInfo.processInfosIndex), this.proxyState.getRealm$realm());
        return this.processInfosRealmList;
    }

    public void realmSet$processInfos(RealmList<ProcessInfo> realmList) {
        if (this.proxyState.isUnderConstruction()) {
            if (!this.proxyState.getAcceptDefaultValue$realm() || this.proxyState.getExcludeFields$realm().contains("processInfos")) {
                return;
            }
            if (realmList != null && !realmList.isManaged()) {
                Realm realm = (Realm) this.proxyState.getRealm$realm();
                RealmList<ProcessInfo> realmList2 = new RealmList<>();
                Iterator it = realmList.iterator();
                while (it.hasNext()) {
                    ProcessInfo processInfo = (ProcessInfo) it.next();
                    if (processInfo == null || RealmObject.isManaged(processInfo)) {
                        realmList2.add(processInfo);
                    } else {
                        realmList2.add(realm.copyToRealm(processInfo));
                    }
                }
                realmList = realmList2;
            }
        }
        this.proxyState.getRealm$realm().checkIfValid();
        OsList modelList = this.proxyState.getRow$realm().getModelList(this.columnInfo.processInfosIndex);
        int i = 0;
        if (realmList == null || ((long) realmList.size()) != modelList.size()) {
            modelList.removeAll();
            if (realmList != null) {
                int size = realmList.size();
                while (i < size) {
                    ProcessInfo processInfo2 = (ProcessInfo) realmList.get(i);
                    this.proxyState.checkValidObject(processInfo2);
                    modelList.addRow(((RealmObjectProxy) processInfo2).realmGet$proxyState().getRow$realm().getIndex());
                    i++;
                }
            }
        } else {
            int size2 = realmList.size();
            while (i < size2) {
                ProcessInfo processInfo3 = (ProcessInfo) realmList.get(i);
                this.proxyState.checkValidObject(processInfo3);
                modelList.setRow((long) i, ((RealmObjectProxy) processInfo3).realmGet$proxyState().getRow$realm().getIndex());
                i++;
            }
        }
    }

    public RealmList<Feature> realmGet$features() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.featuresRealmList != null) {
            return this.featuresRealmList;
        }
        this.featuresRealmList = new RealmList<>(Feature.class, this.proxyState.getRow$realm().getModelList(this.columnInfo.featuresIndex), this.proxyState.getRealm$realm());
        return this.featuresRealmList;
    }

    public void realmSet$features(RealmList<Feature> realmList) {
        if (this.proxyState.isUnderConstruction()) {
            if (!this.proxyState.getAcceptDefaultValue$realm() || this.proxyState.getExcludeFields$realm().contains(SettingsJsonConstants.FEATURES_KEY)) {
                return;
            }
            if (realmList != null && !realmList.isManaged()) {
                Realm realm = (Realm) this.proxyState.getRealm$realm();
                RealmList<Feature> realmList2 = new RealmList<>();
                Iterator it = realmList.iterator();
                while (it.hasNext()) {
                    Feature feature = (Feature) it.next();
                    if (feature == null || RealmObject.isManaged(feature)) {
                        realmList2.add(feature);
                    } else {
                        realmList2.add(realm.copyToRealm(feature));
                    }
                }
                realmList = realmList2;
            }
        }
        this.proxyState.getRealm$realm().checkIfValid();
        OsList modelList = this.proxyState.getRow$realm().getModelList(this.columnInfo.featuresIndex);
        int i = 0;
        if (realmList == null || ((long) realmList.size()) != modelList.size()) {
            modelList.removeAll();
            if (realmList != null) {
                int size = realmList.size();
                while (i < size) {
                    Feature feature2 = (Feature) realmList.get(i);
                    this.proxyState.checkValidObject(feature2);
                    modelList.addRow(((RealmObjectProxy) feature2).realmGet$proxyState().getRow$realm().getIndex());
                    i++;
                }
            }
        } else {
            int size2 = realmList.size();
            while (i < size2) {
                Feature feature3 = (Feature) realmList.get(i);
                this.proxyState.checkValidObject(feature3);
                modelList.setRow((long) i, ((RealmObjectProxy) feature3).realmGet$proxyState().getRow$realm().getIndex());
                i++;
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("Sample", 28, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("id", RealmFieldType.INTEGER, true, true, true);
        builder2.addPersistedProperty("uuId", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("timestamp", RealmFieldType.INTEGER, false, true, true);
        builder2.addPersistedProperty("version", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("database", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("batteryState", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty(RequestParameters.BATTERY_LEVEL, RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("memoryWired", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("memoryActive", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("memoryInactive", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("memoryFree", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("memoryUser", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("triggeredBy", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("networkStatus", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("distanceTraveled", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("screenBrightness", RealmFieldType.INTEGER, false, false, true);
        builder.addPersistedLinkProperty("networkDetails", RealmFieldType.OBJECT, "NetworkDetails");
        builder.addPersistedLinkProperty("batteryDetails", RealmFieldType.OBJECT, "BatteryDetails");
        builder.addPersistedLinkProperty("cpuStatus", RealmFieldType.OBJECT, "CpuStatus");
        builder.addPersistedLinkProperty("callInfo", RealmFieldType.OBJECT, "CallInfo");
        Builder builder3 = builder;
        builder3.addPersistedProperty("screenOn", RealmFieldType.INTEGER, false, false, true);
        builder3.addPersistedProperty("timeZone", RealmFieldType.STRING, false, false, false);
        builder.addPersistedLinkProperty(DownloadManager.SETTINGS, RealmFieldType.OBJECT, CBLocation.LOCATION_SETTINGS);
        builder.addPersistedLinkProperty("storageDetails", RealmFieldType.OBJECT, "StorageDetails");
        builder.addPersistedProperty("countryCode", RealmFieldType.STRING, false, false, false);
        builder.addPersistedLinkProperty("locationProviders", RealmFieldType.LIST, "LocationProvider");
        builder.addPersistedLinkProperty("processInfos", RealmFieldType.LIST, "ProcessInfo");
        builder.addPersistedLinkProperty(SettingsJsonConstants.FEATURES_KEY, RealmFieldType.LIST, "Feature");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static SampleColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new SampleColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x0204  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0226  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0248  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x026a  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x028c  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x02a9  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x02c6  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x02e8  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x030a  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x032b  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x034c  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x036d  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x038e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x03b0  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x03cd  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x03ee  */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x040f  */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x042d  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x0467  */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x04a1  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x013d  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01c0  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01e2  */
    public static Sample createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        SampleRealmProxy sampleRealmProxy;
        ArrayList arrayList = new ArrayList(9);
        if (z) {
            Table table = realm.getTable(Sample.class);
            long findFirstLong = !jSONObject.isNull("id") ? table.findFirstLong(((SampleColumnInfo) realm.getSchema().getColumnInfo(Sample.class)).idIndex, jSONObject.getLong("id")) : -1;
            if (findFirstLong != -1) {
                RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
                try {
                    realmObjectContext.set(realm, table.getUncheckedRow(findFirstLong), realm.getSchema().getColumnInfo(Sample.class), false, Collections.emptyList());
                    sampleRealmProxy = new SampleRealmProxy();
                    if (sampleRealmProxy == null) {
                        if (jSONObject.has("networkDetails")) {
                            arrayList.add("networkDetails");
                        }
                        if (jSONObject.has("batteryDetails")) {
                            arrayList.add("batteryDetails");
                        }
                        if (jSONObject.has("cpuStatus")) {
                            arrayList.add("cpuStatus");
                        }
                        if (jSONObject.has("callInfo")) {
                            arrayList.add("callInfo");
                        }
                        if (jSONObject.has(DownloadManager.SETTINGS)) {
                            arrayList.add(DownloadManager.SETTINGS);
                        }
                        if (jSONObject.has("storageDetails")) {
                            arrayList.add("storageDetails");
                        }
                        if (jSONObject.has("locationProviders")) {
                            arrayList.add("locationProviders");
                        }
                        if (jSONObject.has("processInfos")) {
                            arrayList.add("processInfos");
                        }
                        if (jSONObject.has(SettingsJsonConstants.FEATURES_KEY)) {
                            arrayList.add(SettingsJsonConstants.FEATURES_KEY);
                        }
                        if (!jSONObject.has("id")) {
                            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
                        } else if (jSONObject.isNull("id")) {
                            sampleRealmProxy = (SampleRealmProxy) realm.createObjectInternal(Sample.class, null, true, arrayList);
                        } else {
                            sampleRealmProxy = (SampleRealmProxy) realm.createObjectInternal(Sample.class, Integer.valueOf(jSONObject.getInt("id")), true, arrayList);
                        }
                    }
                    SampleRealmProxyInterface sampleRealmProxyInterface = sampleRealmProxy;
                    if (jSONObject.has("uuId")) {
                        if (jSONObject.isNull("uuId")) {
                            sampleRealmProxyInterface.realmSet$uuId(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$uuId(jSONObject.getString("uuId"));
                        }
                    }
                    if (jSONObject.has("timestamp")) {
                        if (!jSONObject.isNull("timestamp")) {
                            sampleRealmProxyInterface.realmSet$timestamp(jSONObject.getLong("timestamp"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'timestamp' to null.");
                        }
                    }
                    if (jSONObject.has("version")) {
                        if (!jSONObject.isNull("version")) {
                            sampleRealmProxyInterface.realmSet$version(jSONObject.getInt("version"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'version' to null.");
                        }
                    }
                    if (jSONObject.has("database")) {
                        if (!jSONObject.isNull("database")) {
                            sampleRealmProxyInterface.realmSet$database(jSONObject.getInt("database"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'database' to null.");
                        }
                    }
                    if (jSONObject.has("batteryState")) {
                        if (jSONObject.isNull("batteryState")) {
                            sampleRealmProxyInterface.realmSet$batteryState(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$batteryState(jSONObject.getString("batteryState"));
                        }
                    }
                    if (jSONObject.has(RequestParameters.BATTERY_LEVEL)) {
                        if (!jSONObject.isNull(RequestParameters.BATTERY_LEVEL)) {
                            sampleRealmProxyInterface.realmSet$batteryLevel(jSONObject.getDouble(RequestParameters.BATTERY_LEVEL));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'batteryLevel' to null.");
                        }
                    }
                    if (jSONObject.has("memoryWired")) {
                        if (!jSONObject.isNull("memoryWired")) {
                            sampleRealmProxyInterface.realmSet$memoryWired(jSONObject.getInt("memoryWired"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'memoryWired' to null.");
                        }
                    }
                    if (jSONObject.has("memoryActive")) {
                        if (!jSONObject.isNull("memoryActive")) {
                            sampleRealmProxyInterface.realmSet$memoryActive(jSONObject.getInt("memoryActive"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'memoryActive' to null.");
                        }
                    }
                    if (jSONObject.has("memoryInactive")) {
                        if (!jSONObject.isNull("memoryInactive")) {
                            sampleRealmProxyInterface.realmSet$memoryInactive(jSONObject.getInt("memoryInactive"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'memoryInactive' to null.");
                        }
                    }
                    if (jSONObject.has("memoryFree")) {
                        if (!jSONObject.isNull("memoryFree")) {
                            sampleRealmProxyInterface.realmSet$memoryFree(jSONObject.getInt("memoryFree"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'memoryFree' to null.");
                        }
                    }
                    if (jSONObject.has("memoryUser")) {
                        if (!jSONObject.isNull("memoryUser")) {
                            sampleRealmProxyInterface.realmSet$memoryUser(jSONObject.getInt("memoryUser"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'memoryUser' to null.");
                        }
                    }
                    if (jSONObject.has("triggeredBy")) {
                        if (jSONObject.isNull("triggeredBy")) {
                            sampleRealmProxyInterface.realmSet$triggeredBy(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$triggeredBy(jSONObject.getString("triggeredBy"));
                        }
                    }
                    if (jSONObject.has("networkStatus")) {
                        if (jSONObject.isNull("networkStatus")) {
                            sampleRealmProxyInterface.realmSet$networkStatus(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$networkStatus(jSONObject.getString("networkStatus"));
                        }
                    }
                    if (jSONObject.has("distanceTraveled")) {
                        if (!jSONObject.isNull("distanceTraveled")) {
                            sampleRealmProxyInterface.realmSet$distanceTraveled(jSONObject.getDouble("distanceTraveled"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'distanceTraveled' to null.");
                        }
                    }
                    if (jSONObject.has("screenBrightness")) {
                        if (!jSONObject.isNull("screenBrightness")) {
                            sampleRealmProxyInterface.realmSet$screenBrightness(jSONObject.getInt("screenBrightness"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'screenBrightness' to null.");
                        }
                    }
                    if (jSONObject.has("networkDetails")) {
                        if (jSONObject.isNull("networkDetails")) {
                            sampleRealmProxyInterface.realmSet$networkDetails(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$networkDetails(NetworkDetailsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject.getJSONObject("networkDetails"), z));
                        }
                    }
                    if (jSONObject.has("batteryDetails")) {
                        if (jSONObject.isNull("batteryDetails")) {
                            sampleRealmProxyInterface.realmSet$batteryDetails(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$batteryDetails(BatteryDetailsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject.getJSONObject("batteryDetails"), z));
                        }
                    }
                    if (jSONObject.has("cpuStatus")) {
                        if (jSONObject.isNull("cpuStatus")) {
                            sampleRealmProxyInterface.realmSet$cpuStatus(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$cpuStatus(CpuStatusRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject.getJSONObject("cpuStatus"), z));
                        }
                    }
                    if (jSONObject.has("callInfo")) {
                        if (jSONObject.isNull("callInfo")) {
                            sampleRealmProxyInterface.realmSet$callInfo(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$callInfo(CallInfoRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject.getJSONObject("callInfo"), z));
                        }
                    }
                    if (jSONObject.has("screenOn")) {
                        if (!jSONObject.isNull("screenOn")) {
                            sampleRealmProxyInterface.realmSet$screenOn(jSONObject.getInt("screenOn"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'screenOn' to null.");
                        }
                    }
                    if (jSONObject.has("timeZone")) {
                        if (jSONObject.isNull("timeZone")) {
                            sampleRealmProxyInterface.realmSet$timeZone(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$timeZone(jSONObject.getString("timeZone"));
                        }
                    }
                    if (jSONObject.has(DownloadManager.SETTINGS)) {
                        if (jSONObject.isNull(DownloadManager.SETTINGS)) {
                            sampleRealmProxyInterface.realmSet$settings(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$settings(SettingsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject.getJSONObject(DownloadManager.SETTINGS), z));
                        }
                    }
                    if (jSONObject.has("storageDetails")) {
                        if (jSONObject.isNull("storageDetails")) {
                            sampleRealmProxyInterface.realmSet$storageDetails(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$storageDetails(StorageDetailsRealmProxy.createOrUpdateUsingJsonObject(realm, jSONObject.getJSONObject("storageDetails"), z));
                        }
                    }
                    if (jSONObject.has("countryCode")) {
                        if (jSONObject.isNull("countryCode")) {
                            sampleRealmProxyInterface.realmSet$countryCode(null);
                        } else {
                            sampleRealmProxyInterface.realmSet$countryCode(jSONObject.getString("countryCode"));
                        }
                    }
                    if (jSONObject.has("locationProviders")) {
                        if (jSONObject.isNull("locationProviders")) {
                            sampleRealmProxyInterface.realmSet$locationProviders(null);
                        } else {
                            sampleRealmProxyInterface.realmGet$locationProviders().clear();
                            JSONArray jSONArray = jSONObject.getJSONArray("locationProviders");
                            for (int i = 0; i < jSONArray.length(); i++) {
                                sampleRealmProxyInterface.realmGet$locationProviders().add(LocationProviderRealmProxy.createOrUpdateUsingJsonObject(realm, jSONArray.getJSONObject(i), z));
                            }
                        }
                    }
                    if (jSONObject.has("processInfos")) {
                        if (jSONObject.isNull("processInfos")) {
                            sampleRealmProxyInterface.realmSet$processInfos(null);
                        } else {
                            sampleRealmProxyInterface.realmGet$processInfos().clear();
                            JSONArray jSONArray2 = jSONObject.getJSONArray("processInfos");
                            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                                sampleRealmProxyInterface.realmGet$processInfos().add(ProcessInfoRealmProxy.createOrUpdateUsingJsonObject(realm, jSONArray2.getJSONObject(i2), z));
                            }
                        }
                    }
                    if (jSONObject.has(SettingsJsonConstants.FEATURES_KEY)) {
                        if (jSONObject.isNull(SettingsJsonConstants.FEATURES_KEY)) {
                            sampleRealmProxyInterface.realmSet$features(null);
                        } else {
                            sampleRealmProxyInterface.realmGet$features().clear();
                            JSONArray jSONArray3 = jSONObject.getJSONArray(SettingsJsonConstants.FEATURES_KEY);
                            for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                                sampleRealmProxyInterface.realmGet$features().add(FeatureRealmProxy.createOrUpdateUsingJsonObject(realm, jSONArray3.getJSONObject(i3), z));
                            }
                        }
                    }
                    return sampleRealmProxy;
                } finally {
                    realmObjectContext.clear();
                }
            }
        }
        sampleRealmProxy = null;
        if (sampleRealmProxy == null) {
        }
        SampleRealmProxyInterface sampleRealmProxyInterface2 = sampleRealmProxy;
        if (jSONObject.has("uuId")) {
        }
        if (jSONObject.has("timestamp")) {
        }
        if (jSONObject.has("version")) {
        }
        if (jSONObject.has("database")) {
        }
        if (jSONObject.has("batteryState")) {
        }
        if (jSONObject.has(RequestParameters.BATTERY_LEVEL)) {
        }
        if (jSONObject.has("memoryWired")) {
        }
        if (jSONObject.has("memoryActive")) {
        }
        if (jSONObject.has("memoryInactive")) {
        }
        if (jSONObject.has("memoryFree")) {
        }
        if (jSONObject.has("memoryUser")) {
        }
        if (jSONObject.has("triggeredBy")) {
        }
        if (jSONObject.has("networkStatus")) {
        }
        if (jSONObject.has("distanceTraveled")) {
        }
        if (jSONObject.has("screenBrightness")) {
        }
        if (jSONObject.has("networkDetails")) {
        }
        if (jSONObject.has("batteryDetails")) {
        }
        if (jSONObject.has("cpuStatus")) {
        }
        if (jSONObject.has("callInfo")) {
        }
        if (jSONObject.has("screenOn")) {
        }
        if (jSONObject.has("timeZone")) {
        }
        if (jSONObject.has(DownloadManager.SETTINGS)) {
        }
        if (jSONObject.has("storageDetails")) {
        }
        if (jSONObject.has("countryCode")) {
        }
        if (jSONObject.has("locationProviders")) {
        }
        if (jSONObject.has("processInfos")) {
        }
        if (jSONObject.has(SettingsJsonConstants.FEATURES_KEY)) {
        }
        return sampleRealmProxy;
    }

    @TargetApi(11)
    public static Sample createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        Sample sample = new Sample();
        SampleRealmProxyInterface sampleRealmProxyInterface = sample;
        jsonReader.beginObject();
        boolean z = false;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("id")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$id(jsonReader.nextInt());
                    z = true;
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
            } else if (nextName.equals("uuId")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$uuId(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$uuId(null);
                }
            } else if (nextName.equals("timestamp")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$timestamp(jsonReader.nextLong());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'timestamp' to null.");
                }
            } else if (nextName.equals("version")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$version(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'version' to null.");
                }
            } else if (nextName.equals("database")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$database(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'database' to null.");
                }
            } else if (nextName.equals("batteryState")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$batteryState(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$batteryState(null);
                }
            } else if (nextName.equals(RequestParameters.BATTERY_LEVEL)) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$batteryLevel(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'batteryLevel' to null.");
                }
            } else if (nextName.equals("memoryWired")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$memoryWired(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'memoryWired' to null.");
                }
            } else if (nextName.equals("memoryActive")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$memoryActive(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'memoryActive' to null.");
                }
            } else if (nextName.equals("memoryInactive")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$memoryInactive(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'memoryInactive' to null.");
                }
            } else if (nextName.equals("memoryFree")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$memoryFree(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'memoryFree' to null.");
                }
            } else if (nextName.equals("memoryUser")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$memoryUser(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'memoryUser' to null.");
                }
            } else if (nextName.equals("triggeredBy")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$triggeredBy(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$triggeredBy(null);
                }
            } else if (nextName.equals("networkStatus")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$networkStatus(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$networkStatus(null);
                }
            } else if (nextName.equals("distanceTraveled")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$distanceTraveled(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'distanceTraveled' to null.");
                }
            } else if (nextName.equals("screenBrightness")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$screenBrightness(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'screenBrightness' to null.");
                }
            } else if (nextName.equals("networkDetails")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$networkDetails(null);
                } else {
                    sampleRealmProxyInterface.realmSet$networkDetails(NetworkDetailsRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
            } else if (nextName.equals("batteryDetails")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$batteryDetails(null);
                } else {
                    sampleRealmProxyInterface.realmSet$batteryDetails(BatteryDetailsRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
            } else if (nextName.equals("cpuStatus")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$cpuStatus(null);
                } else {
                    sampleRealmProxyInterface.realmSet$cpuStatus(CpuStatusRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
            } else if (nextName.equals("callInfo")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$callInfo(null);
                } else {
                    sampleRealmProxyInterface.realmSet$callInfo(CallInfoRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
            } else if (nextName.equals("screenOn")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$screenOn(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'screenOn' to null.");
                }
            } else if (nextName.equals("timeZone")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$timeZone(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$timeZone(null);
                }
            } else if (nextName.equals(DownloadManager.SETTINGS)) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$settings(null);
                } else {
                    sampleRealmProxyInterface.realmSet$settings(SettingsRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
            } else if (nextName.equals("storageDetails")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$storageDetails(null);
                } else {
                    sampleRealmProxyInterface.realmSet$storageDetails(StorageDetailsRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
            } else if (nextName.equals("countryCode")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    sampleRealmProxyInterface.realmSet$countryCode(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$countryCode(null);
                }
            } else if (nextName.equals("locationProviders")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$locationProviders(null);
                } else {
                    sampleRealmProxyInterface.realmSet$locationProviders(new RealmList());
                    jsonReader.beginArray();
                    while (jsonReader.hasNext()) {
                        sampleRealmProxyInterface.realmGet$locationProviders().add(LocationProviderRealmProxy.createUsingJsonStream(realm, jsonReader));
                    }
                    jsonReader.endArray();
                }
            } else if (nextName.equals("processInfos")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    sampleRealmProxyInterface.realmSet$processInfos(null);
                } else {
                    sampleRealmProxyInterface.realmSet$processInfos(new RealmList());
                    jsonReader.beginArray();
                    while (jsonReader.hasNext()) {
                        sampleRealmProxyInterface.realmGet$processInfos().add(ProcessInfoRealmProxy.createUsingJsonStream(realm, jsonReader));
                    }
                    jsonReader.endArray();
                }
            } else if (!nextName.equals(SettingsJsonConstants.FEATURES_KEY)) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.skipValue();
                sampleRealmProxyInterface.realmSet$features(null);
            } else {
                sampleRealmProxyInterface.realmSet$features(new RealmList());
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    sampleRealmProxyInterface.realmGet$features().add(FeatureRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
                jsonReader.endArray();
            }
        }
        jsonReader.endObject();
        if (z) {
            return (Sample) realm.copyToRealm(sample);
        }
        throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00aa  */
    public static Sample copyOrUpdate(Realm realm, Sample sample, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        boolean z2;
        if (sample instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) sample;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return sample;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(sample);
        if (realmObjectProxy2 != null) {
            return (Sample) realmObjectProxy2;
        }
        SampleRealmProxy sampleRealmProxy = null;
        if (z) {
            Table table = realm.getTable(Sample.class);
            long findFirstLong = table.findFirstLong(((SampleColumnInfo) realm.getSchema().getColumnInfo(Sample.class)).idIndex, (long) sample.realmGet$id());
            if (findFirstLong == -1) {
                z2 = false;
                return !z2 ? update(realm, sampleRealmProxy, sample, map) : copy(realm, sample, z, map);
            }
            try {
                realmObjectContext.set(realm, table.getUncheckedRow(findFirstLong), realm.getSchema().getColumnInfo(Sample.class), false, Collections.emptyList());
                sampleRealmProxy = new SampleRealmProxy();
                map.put(sample, sampleRealmProxy);
            } finally {
                realmObjectContext.clear();
            }
        }
        z2 = z;
        return !z2 ? update(realm, sampleRealmProxy, sample, map) : copy(realm, sample, z, map);
    }

    public static Sample copy(Realm realm, Sample sample, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(sample);
        if (realmObjectProxy != null) {
            return (Sample) realmObjectProxy;
        }
        SampleRealmProxyInterface sampleRealmProxyInterface = sample;
        Sample sample2 = (Sample) realm.createObjectInternal(Sample.class, Integer.valueOf(sampleRealmProxyInterface.realmGet$id()), false, Collections.emptyList());
        map.put(sample, (RealmObjectProxy) sample2);
        SampleRealmProxyInterface sampleRealmProxyInterface2 = sample2;
        sampleRealmProxyInterface2.realmSet$uuId(sampleRealmProxyInterface.realmGet$uuId());
        sampleRealmProxyInterface2.realmSet$timestamp(sampleRealmProxyInterface.realmGet$timestamp());
        sampleRealmProxyInterface2.realmSet$version(sampleRealmProxyInterface.realmGet$version());
        sampleRealmProxyInterface2.realmSet$database(sampleRealmProxyInterface.realmGet$database());
        sampleRealmProxyInterface2.realmSet$batteryState(sampleRealmProxyInterface.realmGet$batteryState());
        sampleRealmProxyInterface2.realmSet$batteryLevel(sampleRealmProxyInterface.realmGet$batteryLevel());
        sampleRealmProxyInterface2.realmSet$memoryWired(sampleRealmProxyInterface.realmGet$memoryWired());
        sampleRealmProxyInterface2.realmSet$memoryActive(sampleRealmProxyInterface.realmGet$memoryActive());
        sampleRealmProxyInterface2.realmSet$memoryInactive(sampleRealmProxyInterface.realmGet$memoryInactive());
        sampleRealmProxyInterface2.realmSet$memoryFree(sampleRealmProxyInterface.realmGet$memoryFree());
        sampleRealmProxyInterface2.realmSet$memoryUser(sampleRealmProxyInterface.realmGet$memoryUser());
        sampleRealmProxyInterface2.realmSet$triggeredBy(sampleRealmProxyInterface.realmGet$triggeredBy());
        sampleRealmProxyInterface2.realmSet$networkStatus(sampleRealmProxyInterface.realmGet$networkStatus());
        sampleRealmProxyInterface2.realmSet$distanceTraveled(sampleRealmProxyInterface.realmGet$distanceTraveled());
        sampleRealmProxyInterface2.realmSet$screenBrightness(sampleRealmProxyInterface.realmGet$screenBrightness());
        NetworkDetails realmGet$networkDetails = sampleRealmProxyInterface.realmGet$networkDetails();
        if (realmGet$networkDetails == null) {
            sampleRealmProxyInterface2.realmSet$networkDetails(null);
        } else {
            NetworkDetails networkDetails = (NetworkDetails) map.get(realmGet$networkDetails);
            if (networkDetails != null) {
                sampleRealmProxyInterface2.realmSet$networkDetails(networkDetails);
            } else {
                sampleRealmProxyInterface2.realmSet$networkDetails(NetworkDetailsRealmProxy.copyOrUpdate(realm, realmGet$networkDetails, z, map));
            }
        }
        BatteryDetails realmGet$batteryDetails = sampleRealmProxyInterface.realmGet$batteryDetails();
        if (realmGet$batteryDetails == null) {
            sampleRealmProxyInterface2.realmSet$batteryDetails(null);
        } else {
            BatteryDetails batteryDetails = (BatteryDetails) map.get(realmGet$batteryDetails);
            if (batteryDetails != null) {
                sampleRealmProxyInterface2.realmSet$batteryDetails(batteryDetails);
            } else {
                sampleRealmProxyInterface2.realmSet$batteryDetails(BatteryDetailsRealmProxy.copyOrUpdate(realm, realmGet$batteryDetails, z, map));
            }
        }
        CpuStatus realmGet$cpuStatus = sampleRealmProxyInterface.realmGet$cpuStatus();
        if (realmGet$cpuStatus == null) {
            sampleRealmProxyInterface2.realmSet$cpuStatus(null);
        } else {
            CpuStatus cpuStatus = (CpuStatus) map.get(realmGet$cpuStatus);
            if (cpuStatus != null) {
                sampleRealmProxyInterface2.realmSet$cpuStatus(cpuStatus);
            } else {
                sampleRealmProxyInterface2.realmSet$cpuStatus(CpuStatusRealmProxy.copyOrUpdate(realm, realmGet$cpuStatus, z, map));
            }
        }
        CallInfo realmGet$callInfo = sampleRealmProxyInterface.realmGet$callInfo();
        if (realmGet$callInfo == null) {
            sampleRealmProxyInterface2.realmSet$callInfo(null);
        } else {
            CallInfo callInfo = (CallInfo) map.get(realmGet$callInfo);
            if (callInfo != null) {
                sampleRealmProxyInterface2.realmSet$callInfo(callInfo);
            } else {
                sampleRealmProxyInterface2.realmSet$callInfo(CallInfoRealmProxy.copyOrUpdate(realm, realmGet$callInfo, z, map));
            }
        }
        sampleRealmProxyInterface2.realmSet$screenOn(sampleRealmProxyInterface.realmGet$screenOn());
        sampleRealmProxyInterface2.realmSet$timeZone(sampleRealmProxyInterface.realmGet$timeZone());
        Settings realmGet$settings = sampleRealmProxyInterface.realmGet$settings();
        if (realmGet$settings == null) {
            sampleRealmProxyInterface2.realmSet$settings(null);
        } else {
            Settings settings = (Settings) map.get(realmGet$settings);
            if (settings != null) {
                sampleRealmProxyInterface2.realmSet$settings(settings);
            } else {
                sampleRealmProxyInterface2.realmSet$settings(SettingsRealmProxy.copyOrUpdate(realm, realmGet$settings, z, map));
            }
        }
        StorageDetails realmGet$storageDetails = sampleRealmProxyInterface.realmGet$storageDetails();
        if (realmGet$storageDetails == null) {
            sampleRealmProxyInterface2.realmSet$storageDetails(null);
        } else {
            StorageDetails storageDetails = (StorageDetails) map.get(realmGet$storageDetails);
            if (storageDetails != null) {
                sampleRealmProxyInterface2.realmSet$storageDetails(storageDetails);
            } else {
                sampleRealmProxyInterface2.realmSet$storageDetails(StorageDetailsRealmProxy.copyOrUpdate(realm, realmGet$storageDetails, z, map));
            }
        }
        sampleRealmProxyInterface2.realmSet$countryCode(sampleRealmProxyInterface.realmGet$countryCode());
        RealmList realmGet$locationProviders = sampleRealmProxyInterface.realmGet$locationProviders();
        if (realmGet$locationProviders != null) {
            RealmList realmGet$locationProviders2 = sampleRealmProxyInterface2.realmGet$locationProviders();
            realmGet$locationProviders2.clear();
            for (int i = 0; i < realmGet$locationProviders.size(); i++) {
                LocationProvider locationProvider = (LocationProvider) realmGet$locationProviders.get(i);
                LocationProvider locationProvider2 = (LocationProvider) map.get(locationProvider);
                if (locationProvider2 != null) {
                    realmGet$locationProviders2.add(locationProvider2);
                } else {
                    realmGet$locationProviders2.add(LocationProviderRealmProxy.copyOrUpdate(realm, locationProvider, z, map));
                }
            }
        }
        RealmList realmGet$processInfos = sampleRealmProxyInterface.realmGet$processInfos();
        if (realmGet$processInfos != null) {
            RealmList realmGet$processInfos2 = sampleRealmProxyInterface2.realmGet$processInfos();
            realmGet$processInfos2.clear();
            for (int i2 = 0; i2 < realmGet$processInfos.size(); i2++) {
                ProcessInfo processInfo = (ProcessInfo) realmGet$processInfos.get(i2);
                ProcessInfo processInfo2 = (ProcessInfo) map.get(processInfo);
                if (processInfo2 != null) {
                    realmGet$processInfos2.add(processInfo2);
                } else {
                    realmGet$processInfos2.add(ProcessInfoRealmProxy.copyOrUpdate(realm, processInfo, z, map));
                }
            }
        }
        RealmList realmGet$features = sampleRealmProxyInterface.realmGet$features();
        if (realmGet$features != null) {
            RealmList realmGet$features2 = sampleRealmProxyInterface2.realmGet$features();
            realmGet$features2.clear();
            for (int i3 = 0; i3 < realmGet$features.size(); i3++) {
                Feature feature = (Feature) realmGet$features.get(i3);
                Feature feature2 = (Feature) map.get(feature);
                if (feature2 != null) {
                    realmGet$features2.add(feature2);
                } else {
                    realmGet$features2.add(FeatureRealmProxy.copyOrUpdate(realm, feature, z, map));
                }
            }
        }
        return sample2;
    }

    public static long insert(Realm realm, Sample sample, Map<RealmModel, Long> map) {
        long j;
        long j2;
        long j3;
        Realm realm2 = realm;
        Sample sample2 = sample;
        Map<RealmModel, Long> map2 = map;
        if (sample2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) sample2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm2.getTable(Sample.class);
        long nativePtr = table.getNativePtr();
        SampleColumnInfo sampleColumnInfo = (SampleColumnInfo) realm.getSchema().getColumnInfo(Sample.class);
        long j4 = sampleColumnInfo.idIndex;
        SampleRealmProxyInterface sampleRealmProxyInterface = sample2;
        Integer valueOf = Integer.valueOf(sampleRealmProxyInterface.realmGet$id());
        if (valueOf != null) {
            j = Table.nativeFindFirstInt(nativePtr, j4, (long) sampleRealmProxyInterface.realmGet$id());
        } else {
            j = -1;
        }
        if (j == -1) {
            j = OsObject.createRowWithPrimaryKey(table, j4, Integer.valueOf(sampleRealmProxyInterface.realmGet$id()));
        } else {
            Table.throwDuplicatePrimaryKeyException(valueOf);
        }
        long j5 = j;
        map2.put(sample2, Long.valueOf(j5));
        String realmGet$uuId = sampleRealmProxyInterface.realmGet$uuId();
        if (realmGet$uuId != null) {
            j2 = j5;
            Table.nativeSetString(nativePtr, sampleColumnInfo.uuIdIndex, j5, realmGet$uuId, false);
        } else {
            j2 = j5;
        }
        long j6 = nativePtr;
        long j7 = j2;
        Table.nativeSetLong(j6, sampleColumnInfo.timestampIndex, j7, sampleRealmProxyInterface.realmGet$timestamp(), false);
        Table.nativeSetLong(j6, sampleColumnInfo.versionIndex, j7, (long) sampleRealmProxyInterface.realmGet$version(), false);
        Table.nativeSetLong(j6, sampleColumnInfo.databaseIndex, j7, (long) sampleRealmProxyInterface.realmGet$database(), false);
        String realmGet$batteryState = sampleRealmProxyInterface.realmGet$batteryState();
        if (realmGet$batteryState != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.batteryStateIndex, j2, realmGet$batteryState, false);
        }
        long j8 = nativePtr;
        long j9 = j2;
        Table.nativeSetDouble(j8, sampleColumnInfo.batteryLevelIndex, j9, sampleRealmProxyInterface.realmGet$batteryLevel(), false);
        Table.nativeSetLong(j8, sampleColumnInfo.memoryWiredIndex, j9, (long) sampleRealmProxyInterface.realmGet$memoryWired(), false);
        Table.nativeSetLong(j8, sampleColumnInfo.memoryActiveIndex, j9, (long) sampleRealmProxyInterface.realmGet$memoryActive(), false);
        Table.nativeSetLong(j8, sampleColumnInfo.memoryInactiveIndex, j9, (long) sampleRealmProxyInterface.realmGet$memoryInactive(), false);
        Table.nativeSetLong(j8, sampleColumnInfo.memoryFreeIndex, j9, (long) sampleRealmProxyInterface.realmGet$memoryFree(), false);
        Table.nativeSetLong(j8, sampleColumnInfo.memoryUserIndex, j9, (long) sampleRealmProxyInterface.realmGet$memoryUser(), false);
        String realmGet$triggeredBy = sampleRealmProxyInterface.realmGet$triggeredBy();
        if (realmGet$triggeredBy != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.triggeredByIndex, j2, realmGet$triggeredBy, false);
        }
        String realmGet$networkStatus = sampleRealmProxyInterface.realmGet$networkStatus();
        if (realmGet$networkStatus != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.networkStatusIndex, j2, realmGet$networkStatus, false);
        }
        long j10 = nativePtr;
        long j11 = j2;
        Table.nativeSetDouble(j10, sampleColumnInfo.distanceTraveledIndex, j11, sampleRealmProxyInterface.realmGet$distanceTraveled(), false);
        Table.nativeSetLong(j10, sampleColumnInfo.screenBrightnessIndex, j11, (long) sampleRealmProxyInterface.realmGet$screenBrightness(), false);
        NetworkDetails realmGet$networkDetails = sampleRealmProxyInterface.realmGet$networkDetails();
        if (realmGet$networkDetails != null) {
            Long l = (Long) map2.get(realmGet$networkDetails);
            if (l == null) {
                l = Long.valueOf(NetworkDetailsRealmProxy.insert(realm2, realmGet$networkDetails, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.networkDetailsIndex, j2, l.longValue(), false);
        }
        BatteryDetails realmGet$batteryDetails = sampleRealmProxyInterface.realmGet$batteryDetails();
        if (realmGet$batteryDetails != null) {
            Long l2 = (Long) map2.get(realmGet$batteryDetails);
            if (l2 == null) {
                l2 = Long.valueOf(BatteryDetailsRealmProxy.insert(realm2, realmGet$batteryDetails, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.batteryDetailsIndex, j2, l2.longValue(), false);
        }
        CpuStatus realmGet$cpuStatus = sampleRealmProxyInterface.realmGet$cpuStatus();
        if (realmGet$cpuStatus != null) {
            Long l3 = (Long) map2.get(realmGet$cpuStatus);
            if (l3 == null) {
                l3 = Long.valueOf(CpuStatusRealmProxy.insert(realm2, realmGet$cpuStatus, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.cpuStatusIndex, j2, l3.longValue(), false);
        }
        CallInfo realmGet$callInfo = sampleRealmProxyInterface.realmGet$callInfo();
        if (realmGet$callInfo != null) {
            Long l4 = (Long) map2.get(realmGet$callInfo);
            if (l4 == null) {
                l4 = Long.valueOf(CallInfoRealmProxy.insert(realm2, realmGet$callInfo, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.callInfoIndex, j2, l4.longValue(), false);
        }
        Table.nativeSetLong(nativePtr, sampleColumnInfo.screenOnIndex, j2, (long) sampleRealmProxyInterface.realmGet$screenOn(), false);
        String realmGet$timeZone = sampleRealmProxyInterface.realmGet$timeZone();
        if (realmGet$timeZone != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.timeZoneIndex, j2, realmGet$timeZone, false);
        }
        Settings realmGet$settings = sampleRealmProxyInterface.realmGet$settings();
        if (realmGet$settings != null) {
            Long l5 = (Long) map2.get(realmGet$settings);
            if (l5 == null) {
                l5 = Long.valueOf(SettingsRealmProxy.insert(realm2, realmGet$settings, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.settingsIndex, j2, l5.longValue(), false);
        }
        StorageDetails realmGet$storageDetails = sampleRealmProxyInterface.realmGet$storageDetails();
        if (realmGet$storageDetails != null) {
            Long l6 = (Long) map2.get(realmGet$storageDetails);
            if (l6 == null) {
                l6 = Long.valueOf(StorageDetailsRealmProxy.insert(realm2, realmGet$storageDetails, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.storageDetailsIndex, j2, l6.longValue(), false);
        }
        String realmGet$countryCode = sampleRealmProxyInterface.realmGet$countryCode();
        if (realmGet$countryCode != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.countryCodeIndex, j2, realmGet$countryCode, false);
        }
        RealmList realmGet$locationProviders = sampleRealmProxyInterface.realmGet$locationProviders();
        if (realmGet$locationProviders != null) {
            j3 = j2;
            OsList osList = new OsList(table.getUncheckedRow(j3), sampleColumnInfo.locationProvidersIndex);
            Iterator it = realmGet$locationProviders.iterator();
            while (it.hasNext()) {
                LocationProvider locationProvider = (LocationProvider) it.next();
                Long l7 = (Long) map2.get(locationProvider);
                if (l7 == null) {
                    l7 = Long.valueOf(LocationProviderRealmProxy.insert(realm2, locationProvider, map2));
                }
                osList.addRow(l7.longValue());
            }
        } else {
            j3 = j2;
        }
        RealmList realmGet$processInfos = sampleRealmProxyInterface.realmGet$processInfos();
        if (realmGet$processInfos != null) {
            OsList osList2 = new OsList(table.getUncheckedRow(j3), sampleColumnInfo.processInfosIndex);
            Iterator it2 = realmGet$processInfos.iterator();
            while (it2.hasNext()) {
                ProcessInfo processInfo = (ProcessInfo) it2.next();
                Long l8 = (Long) map2.get(processInfo);
                if (l8 == null) {
                    l8 = Long.valueOf(ProcessInfoRealmProxy.insert(realm2, processInfo, map2));
                }
                osList2.addRow(l8.longValue());
            }
        }
        RealmList realmGet$features = sampleRealmProxyInterface.realmGet$features();
        if (realmGet$features != null) {
            OsList osList3 = new OsList(table.getUncheckedRow(j3), sampleColumnInfo.featuresIndex);
            Iterator it3 = realmGet$features.iterator();
            while (it3.hasNext()) {
                Feature feature = (Feature) it3.next();
                Long l9 = (Long) map2.get(feature);
                if (l9 == null) {
                    l9 = Long.valueOf(FeatureRealmProxy.insert(realm2, feature, map2));
                }
                osList3.addRow(l9.longValue());
            }
        }
        return j3;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        long j2;
        long j3;
        long j4;
        Realm realm2 = realm;
        Map<RealmModel, Long> map2 = map;
        Table table = realm2.getTable(Sample.class);
        long nativePtr = table.getNativePtr();
        SampleColumnInfo sampleColumnInfo = (SampleColumnInfo) realm.getSchema().getColumnInfo(Sample.class);
        long j5 = sampleColumnInfo.idIndex;
        while (it.hasNext()) {
            Sample sample = (Sample) it.next();
            if (!map2.containsKey(sample)) {
                if (sample instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) sample;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(sample, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                SampleRealmProxyInterface sampleRealmProxyInterface = sample;
                Integer valueOf = Integer.valueOf(sampleRealmProxyInterface.realmGet$id());
                if (valueOf != null) {
                    j = Table.nativeFindFirstInt(nativePtr, j5, (long) sampleRealmProxyInterface.realmGet$id());
                } else {
                    j = -1;
                }
                if (j == -1) {
                    j = OsObject.createRowWithPrimaryKey(table, j5, Integer.valueOf(sampleRealmProxyInterface.realmGet$id()));
                } else {
                    Table.throwDuplicatePrimaryKeyException(valueOf);
                }
                long j6 = j;
                map2.put(sample, Long.valueOf(j6));
                String realmGet$uuId = sampleRealmProxyInterface.realmGet$uuId();
                if (realmGet$uuId != null) {
                    j2 = j6;
                    j3 = j5;
                    Table.nativeSetString(nativePtr, sampleColumnInfo.uuIdIndex, j6, realmGet$uuId, false);
                } else {
                    j2 = j6;
                    j3 = j5;
                }
                long j7 = j2;
                Table.nativeSetLong(nativePtr, sampleColumnInfo.timestampIndex, j7, sampleRealmProxyInterface.realmGet$timestamp(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.versionIndex, j7, (long) sampleRealmProxyInterface.realmGet$version(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.databaseIndex, j7, (long) sampleRealmProxyInterface.realmGet$database(), false);
                String realmGet$batteryState = sampleRealmProxyInterface.realmGet$batteryState();
                if (realmGet$batteryState != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.batteryStateIndex, j2, realmGet$batteryState, false);
                }
                long j8 = j2;
                Table.nativeSetDouble(nativePtr, sampleColumnInfo.batteryLevelIndex, j8, sampleRealmProxyInterface.realmGet$batteryLevel(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryWiredIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryWired(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryActiveIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryActive(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryInactiveIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryInactive(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryFreeIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryFree(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryUserIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryUser(), false);
                String realmGet$triggeredBy = sampleRealmProxyInterface.realmGet$triggeredBy();
                if (realmGet$triggeredBy != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.triggeredByIndex, j2, realmGet$triggeredBy, false);
                }
                String realmGet$networkStatus = sampleRealmProxyInterface.realmGet$networkStatus();
                if (realmGet$networkStatus != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.networkStatusIndex, j2, realmGet$networkStatus, false);
                }
                long j9 = j2;
                Table.nativeSetDouble(nativePtr, sampleColumnInfo.distanceTraveledIndex, j9, sampleRealmProxyInterface.realmGet$distanceTraveled(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.screenBrightnessIndex, j9, (long) sampleRealmProxyInterface.realmGet$screenBrightness(), false);
                NetworkDetails realmGet$networkDetails = sampleRealmProxyInterface.realmGet$networkDetails();
                if (realmGet$networkDetails != null) {
                    Long l = (Long) map2.get(realmGet$networkDetails);
                    if (l == null) {
                        l = Long.valueOf(NetworkDetailsRealmProxy.insert(realm2, realmGet$networkDetails, map2));
                    }
                    table.setLink(sampleColumnInfo.networkDetailsIndex, j2, l.longValue(), false);
                }
                BatteryDetails realmGet$batteryDetails = sampleRealmProxyInterface.realmGet$batteryDetails();
                if (realmGet$batteryDetails != null) {
                    Long l2 = (Long) map2.get(realmGet$batteryDetails);
                    if (l2 == null) {
                        l2 = Long.valueOf(BatteryDetailsRealmProxy.insert(realm2, realmGet$batteryDetails, map2));
                    }
                    table.setLink(sampleColumnInfo.batteryDetailsIndex, j2, l2.longValue(), false);
                }
                CpuStatus realmGet$cpuStatus = sampleRealmProxyInterface.realmGet$cpuStatus();
                if (realmGet$cpuStatus != null) {
                    Long l3 = (Long) map2.get(realmGet$cpuStatus);
                    if (l3 == null) {
                        l3 = Long.valueOf(CpuStatusRealmProxy.insert(realm2, realmGet$cpuStatus, map2));
                    }
                    table.setLink(sampleColumnInfo.cpuStatusIndex, j2, l3.longValue(), false);
                }
                CallInfo realmGet$callInfo = sampleRealmProxyInterface.realmGet$callInfo();
                if (realmGet$callInfo != null) {
                    Long l4 = (Long) map2.get(realmGet$callInfo);
                    if (l4 == null) {
                        l4 = Long.valueOf(CallInfoRealmProxy.insert(realm2, realmGet$callInfo, map2));
                    }
                    table.setLink(sampleColumnInfo.callInfoIndex, j2, l4.longValue(), false);
                }
                Table.nativeSetLong(nativePtr, sampleColumnInfo.screenOnIndex, j2, (long) sampleRealmProxyInterface.realmGet$screenOn(), false);
                String realmGet$timeZone = sampleRealmProxyInterface.realmGet$timeZone();
                if (realmGet$timeZone != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.timeZoneIndex, j2, realmGet$timeZone, false);
                }
                Settings realmGet$settings = sampleRealmProxyInterface.realmGet$settings();
                if (realmGet$settings != null) {
                    Long l5 = (Long) map2.get(realmGet$settings);
                    if (l5 == null) {
                        l5 = Long.valueOf(SettingsRealmProxy.insert(realm2, realmGet$settings, map2));
                    }
                    table.setLink(sampleColumnInfo.settingsIndex, j2, l5.longValue(), false);
                }
                StorageDetails realmGet$storageDetails = sampleRealmProxyInterface.realmGet$storageDetails();
                if (realmGet$storageDetails != null) {
                    Long l6 = (Long) map2.get(realmGet$storageDetails);
                    if (l6 == null) {
                        l6 = Long.valueOf(StorageDetailsRealmProxy.insert(realm2, realmGet$storageDetails, map2));
                    }
                    table.setLink(sampleColumnInfo.storageDetailsIndex, j2, l6.longValue(), false);
                }
                String realmGet$countryCode = sampleRealmProxyInterface.realmGet$countryCode();
                if (realmGet$countryCode != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.countryCodeIndex, j2, realmGet$countryCode, false);
                }
                RealmList realmGet$locationProviders = sampleRealmProxyInterface.realmGet$locationProviders();
                if (realmGet$locationProviders != null) {
                    j4 = j2;
                    OsList osList = new OsList(table.getUncheckedRow(j4), sampleColumnInfo.locationProvidersIndex);
                    Iterator it2 = realmGet$locationProviders.iterator();
                    while (it2.hasNext()) {
                        LocationProvider locationProvider = (LocationProvider) it2.next();
                        Long l7 = (Long) map2.get(locationProvider);
                        if (l7 == null) {
                            l7 = Long.valueOf(LocationProviderRealmProxy.insert(realm2, locationProvider, map2));
                        }
                        osList.addRow(l7.longValue());
                    }
                } else {
                    j4 = j2;
                }
                RealmList realmGet$processInfos = sampleRealmProxyInterface.realmGet$processInfos();
                if (realmGet$processInfos != null) {
                    OsList osList2 = new OsList(table.getUncheckedRow(j4), sampleColumnInfo.processInfosIndex);
                    Iterator it3 = realmGet$processInfos.iterator();
                    while (it3.hasNext()) {
                        ProcessInfo processInfo = (ProcessInfo) it3.next();
                        Long l8 = (Long) map2.get(processInfo);
                        if (l8 == null) {
                            l8 = Long.valueOf(ProcessInfoRealmProxy.insert(realm2, processInfo, map2));
                        }
                        osList2.addRow(l8.longValue());
                    }
                }
                RealmList realmGet$features = sampleRealmProxyInterface.realmGet$features();
                if (realmGet$features != null) {
                    OsList osList3 = new OsList(table.getUncheckedRow(j4), sampleColumnInfo.featuresIndex);
                    Iterator it4 = realmGet$features.iterator();
                    while (it4.hasNext()) {
                        Feature feature = (Feature) it4.next();
                        Long l9 = (Long) map2.get(feature);
                        if (l9 == null) {
                            l9 = Long.valueOf(FeatureRealmProxy.insert(realm2, feature, map2));
                        }
                        osList3.addRow(l9.longValue());
                    }
                }
                j5 = j3;
            }
        }
    }

    public static long insertOrUpdate(Realm realm, Sample sample, Map<RealmModel, Long> map) {
        long j;
        long j2;
        Realm realm2 = realm;
        Sample sample2 = sample;
        Map<RealmModel, Long> map2 = map;
        if (sample2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) sample2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm2.getTable(Sample.class);
        long nativePtr = table.getNativePtr();
        SampleColumnInfo sampleColumnInfo = (SampleColumnInfo) realm.getSchema().getColumnInfo(Sample.class);
        long j3 = sampleColumnInfo.idIndex;
        SampleRealmProxyInterface sampleRealmProxyInterface = sample2;
        if (Integer.valueOf(sampleRealmProxyInterface.realmGet$id()) != null) {
            j = Table.nativeFindFirstInt(nativePtr, j3, (long) sampleRealmProxyInterface.realmGet$id());
        } else {
            j = -1;
        }
        if (j == -1) {
            j = OsObject.createRowWithPrimaryKey(table, j3, Integer.valueOf(sampleRealmProxyInterface.realmGet$id()));
        }
        long j4 = j;
        map2.put(sample2, Long.valueOf(j4));
        String realmGet$uuId = sampleRealmProxyInterface.realmGet$uuId();
        if (realmGet$uuId != null) {
            j2 = j4;
            Table.nativeSetString(nativePtr, sampleColumnInfo.uuIdIndex, j4, realmGet$uuId, false);
        } else {
            j2 = j4;
            Table.nativeSetNull(nativePtr, sampleColumnInfo.uuIdIndex, j2, false);
        }
        long j5 = nativePtr;
        long j6 = j2;
        Table.nativeSetLong(j5, sampleColumnInfo.timestampIndex, j6, sampleRealmProxyInterface.realmGet$timestamp(), false);
        Table.nativeSetLong(j5, sampleColumnInfo.versionIndex, j6, (long) sampleRealmProxyInterface.realmGet$version(), false);
        Table.nativeSetLong(j5, sampleColumnInfo.databaseIndex, j6, (long) sampleRealmProxyInterface.realmGet$database(), false);
        String realmGet$batteryState = sampleRealmProxyInterface.realmGet$batteryState();
        if (realmGet$batteryState != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.batteryStateIndex, j2, realmGet$batteryState, false);
        } else {
            Table.nativeSetNull(nativePtr, sampleColumnInfo.batteryStateIndex, j2, false);
        }
        long j7 = nativePtr;
        long j8 = j2;
        Table.nativeSetDouble(j7, sampleColumnInfo.batteryLevelIndex, j8, sampleRealmProxyInterface.realmGet$batteryLevel(), false);
        Table.nativeSetLong(j7, sampleColumnInfo.memoryWiredIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryWired(), false);
        Table.nativeSetLong(j7, sampleColumnInfo.memoryActiveIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryActive(), false);
        Table.nativeSetLong(j7, sampleColumnInfo.memoryInactiveIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryInactive(), false);
        Table.nativeSetLong(j7, sampleColumnInfo.memoryFreeIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryFree(), false);
        Table.nativeSetLong(j7, sampleColumnInfo.memoryUserIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryUser(), false);
        String realmGet$triggeredBy = sampleRealmProxyInterface.realmGet$triggeredBy();
        if (realmGet$triggeredBy != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.triggeredByIndex, j2, realmGet$triggeredBy, false);
        } else {
            Table.nativeSetNull(nativePtr, sampleColumnInfo.triggeredByIndex, j2, false);
        }
        String realmGet$networkStatus = sampleRealmProxyInterface.realmGet$networkStatus();
        if (realmGet$networkStatus != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.networkStatusIndex, j2, realmGet$networkStatus, false);
        } else {
            Table.nativeSetNull(nativePtr, sampleColumnInfo.networkStatusIndex, j2, false);
        }
        long j9 = nativePtr;
        long j10 = j2;
        Table.nativeSetDouble(j9, sampleColumnInfo.distanceTraveledIndex, j10, sampleRealmProxyInterface.realmGet$distanceTraveled(), false);
        Table.nativeSetLong(j9, sampleColumnInfo.screenBrightnessIndex, j10, (long) sampleRealmProxyInterface.realmGet$screenBrightness(), false);
        NetworkDetails realmGet$networkDetails = sampleRealmProxyInterface.realmGet$networkDetails();
        if (realmGet$networkDetails != null) {
            Long l = (Long) map2.get(realmGet$networkDetails);
            if (l == null) {
                l = Long.valueOf(NetworkDetailsRealmProxy.insertOrUpdate(realm2, realmGet$networkDetails, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.networkDetailsIndex, j2, l.longValue(), false);
        } else {
            Table.nativeNullifyLink(nativePtr, sampleColumnInfo.networkDetailsIndex, j2);
        }
        BatteryDetails realmGet$batteryDetails = sampleRealmProxyInterface.realmGet$batteryDetails();
        if (realmGet$batteryDetails != null) {
            Long l2 = (Long) map2.get(realmGet$batteryDetails);
            if (l2 == null) {
                l2 = Long.valueOf(BatteryDetailsRealmProxy.insertOrUpdate(realm2, realmGet$batteryDetails, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.batteryDetailsIndex, j2, l2.longValue(), false);
        } else {
            Table.nativeNullifyLink(nativePtr, sampleColumnInfo.batteryDetailsIndex, j2);
        }
        CpuStatus realmGet$cpuStatus = sampleRealmProxyInterface.realmGet$cpuStatus();
        if (realmGet$cpuStatus != null) {
            Long l3 = (Long) map2.get(realmGet$cpuStatus);
            if (l3 == null) {
                l3 = Long.valueOf(CpuStatusRealmProxy.insertOrUpdate(realm2, realmGet$cpuStatus, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.cpuStatusIndex, j2, l3.longValue(), false);
        } else {
            Table.nativeNullifyLink(nativePtr, sampleColumnInfo.cpuStatusIndex, j2);
        }
        CallInfo realmGet$callInfo = sampleRealmProxyInterface.realmGet$callInfo();
        if (realmGet$callInfo != null) {
            Long l4 = (Long) map2.get(realmGet$callInfo);
            if (l4 == null) {
                l4 = Long.valueOf(CallInfoRealmProxy.insertOrUpdate(realm2, realmGet$callInfo, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.callInfoIndex, j2, l4.longValue(), false);
        } else {
            Table.nativeNullifyLink(nativePtr, sampleColumnInfo.callInfoIndex, j2);
        }
        Table.nativeSetLong(nativePtr, sampleColumnInfo.screenOnIndex, j2, (long) sampleRealmProxyInterface.realmGet$screenOn(), false);
        String realmGet$timeZone = sampleRealmProxyInterface.realmGet$timeZone();
        if (realmGet$timeZone != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.timeZoneIndex, j2, realmGet$timeZone, false);
        } else {
            Table.nativeSetNull(nativePtr, sampleColumnInfo.timeZoneIndex, j2, false);
        }
        Settings realmGet$settings = sampleRealmProxyInterface.realmGet$settings();
        if (realmGet$settings != null) {
            Long l5 = (Long) map2.get(realmGet$settings);
            if (l5 == null) {
                l5 = Long.valueOf(SettingsRealmProxy.insertOrUpdate(realm2, realmGet$settings, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.settingsIndex, j2, l5.longValue(), false);
        } else {
            Table.nativeNullifyLink(nativePtr, sampleColumnInfo.settingsIndex, j2);
        }
        StorageDetails realmGet$storageDetails = sampleRealmProxyInterface.realmGet$storageDetails();
        if (realmGet$storageDetails != null) {
            Long l6 = (Long) map2.get(realmGet$storageDetails);
            if (l6 == null) {
                l6 = Long.valueOf(StorageDetailsRealmProxy.insertOrUpdate(realm2, realmGet$storageDetails, map2));
            }
            Table.nativeSetLink(nativePtr, sampleColumnInfo.storageDetailsIndex, j2, l6.longValue(), false);
        } else {
            Table.nativeNullifyLink(nativePtr, sampleColumnInfo.storageDetailsIndex, j2);
        }
        String realmGet$countryCode = sampleRealmProxyInterface.realmGet$countryCode();
        if (realmGet$countryCode != null) {
            Table.nativeSetString(nativePtr, sampleColumnInfo.countryCodeIndex, j2, realmGet$countryCode, false);
        } else {
            Table.nativeSetNull(nativePtr, sampleColumnInfo.countryCodeIndex, j2, false);
        }
        long j11 = j2;
        OsList osList = new OsList(table.getUncheckedRow(j11), sampleColumnInfo.locationProvidersIndex);
        RealmList realmGet$locationProviders = sampleRealmProxyInterface.realmGet$locationProviders();
        if (realmGet$locationProviders == null || ((long) realmGet$locationProviders.size()) != osList.size()) {
            osList.removeAll();
            if (realmGet$locationProviders != null) {
                Iterator it = realmGet$locationProviders.iterator();
                while (it.hasNext()) {
                    LocationProvider locationProvider = (LocationProvider) it.next();
                    Long l7 = (Long) map2.get(locationProvider);
                    if (l7 == null) {
                        l7 = Long.valueOf(LocationProviderRealmProxy.insertOrUpdate(realm2, locationProvider, map2));
                    }
                    osList.addRow(l7.longValue());
                }
            }
        } else {
            int size = realmGet$locationProviders.size();
            for (int i = 0; i < size; i++) {
                LocationProvider locationProvider2 = (LocationProvider) realmGet$locationProviders.get(i);
                Long l8 = (Long) map2.get(locationProvider2);
                if (l8 == null) {
                    l8 = Long.valueOf(LocationProviderRealmProxy.insertOrUpdate(realm2, locationProvider2, map2));
                }
                osList.setRow((long) i, l8.longValue());
            }
        }
        OsList osList2 = new OsList(table.getUncheckedRow(j11), sampleColumnInfo.processInfosIndex);
        RealmList realmGet$processInfos = sampleRealmProxyInterface.realmGet$processInfos();
        if (realmGet$processInfos == null || ((long) realmGet$processInfos.size()) != osList2.size()) {
            osList2.removeAll();
            if (realmGet$processInfos != null) {
                Iterator it2 = realmGet$processInfos.iterator();
                while (it2.hasNext()) {
                    ProcessInfo processInfo = (ProcessInfo) it2.next();
                    Long l9 = (Long) map2.get(processInfo);
                    if (l9 == null) {
                        l9 = Long.valueOf(ProcessInfoRealmProxy.insertOrUpdate(realm2, processInfo, map2));
                    }
                    osList2.addRow(l9.longValue());
                }
            }
        } else {
            int size2 = realmGet$processInfos.size();
            for (int i2 = 0; i2 < size2; i2++) {
                ProcessInfo processInfo2 = (ProcessInfo) realmGet$processInfos.get(i2);
                Long l10 = (Long) map2.get(processInfo2);
                if (l10 == null) {
                    l10 = Long.valueOf(ProcessInfoRealmProxy.insertOrUpdate(realm2, processInfo2, map2));
                }
                osList2.setRow((long) i2, l10.longValue());
            }
        }
        OsList osList3 = new OsList(table.getUncheckedRow(j11), sampleColumnInfo.featuresIndex);
        RealmList realmGet$features = sampleRealmProxyInterface.realmGet$features();
        if (realmGet$features == null || ((long) realmGet$features.size()) != osList3.size()) {
            osList3.removeAll();
            if (realmGet$features != null) {
                Iterator it3 = realmGet$features.iterator();
                while (it3.hasNext()) {
                    Feature feature = (Feature) it3.next();
                    Long l11 = (Long) map2.get(feature);
                    if (l11 == null) {
                        l11 = Long.valueOf(FeatureRealmProxy.insertOrUpdate(realm2, feature, map2));
                    }
                    osList3.addRow(l11.longValue());
                }
            }
        } else {
            int size3 = realmGet$features.size();
            for (int i3 = 0; i3 < size3; i3++) {
                Feature feature2 = (Feature) realmGet$features.get(i3);
                Long l12 = (Long) map2.get(feature2);
                if (l12 == null) {
                    l12 = Long.valueOf(FeatureRealmProxy.insertOrUpdate(realm2, feature2, map2));
                }
                osList3.setRow((long) i3, l12.longValue());
            }
        }
        return j11;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        long j2;
        long j3;
        long j4;
        Realm realm2 = realm;
        Map<RealmModel, Long> map2 = map;
        Table table = realm2.getTable(Sample.class);
        long nativePtr = table.getNativePtr();
        SampleColumnInfo sampleColumnInfo = (SampleColumnInfo) realm.getSchema().getColumnInfo(Sample.class);
        long j5 = sampleColumnInfo.idIndex;
        while (it.hasNext()) {
            Sample sample = (Sample) it.next();
            if (!map2.containsKey(sample)) {
                if (sample instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) sample;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(sample, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                SampleRealmProxyInterface sampleRealmProxyInterface = sample;
                if (Integer.valueOf(sampleRealmProxyInterface.realmGet$id()) != null) {
                    j = Table.nativeFindFirstInt(nativePtr, j5, (long) sampleRealmProxyInterface.realmGet$id());
                } else {
                    j = -1;
                }
                if (j == -1) {
                    j = OsObject.createRowWithPrimaryKey(table, j5, Integer.valueOf(sampleRealmProxyInterface.realmGet$id()));
                }
                long j6 = j;
                map2.put(sample, Long.valueOf(j6));
                String realmGet$uuId = sampleRealmProxyInterface.realmGet$uuId();
                if (realmGet$uuId != null) {
                    j3 = j6;
                    j2 = j5;
                    Table.nativeSetString(nativePtr, sampleColumnInfo.uuIdIndex, j6, realmGet$uuId, false);
                } else {
                    j3 = j6;
                    j2 = j5;
                    Table.nativeSetNull(nativePtr, sampleColumnInfo.uuIdIndex, j6, false);
                }
                long j7 = j3;
                Table.nativeSetLong(nativePtr, sampleColumnInfo.timestampIndex, j7, sampleRealmProxyInterface.realmGet$timestamp(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.versionIndex, j7, (long) sampleRealmProxyInterface.realmGet$version(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.databaseIndex, j7, (long) sampleRealmProxyInterface.realmGet$database(), false);
                String realmGet$batteryState = sampleRealmProxyInterface.realmGet$batteryState();
                if (realmGet$batteryState != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.batteryStateIndex, j3, realmGet$batteryState, false);
                } else {
                    Table.nativeSetNull(nativePtr, sampleColumnInfo.batteryStateIndex, j3, false);
                }
                long j8 = j3;
                Table.nativeSetDouble(nativePtr, sampleColumnInfo.batteryLevelIndex, j8, sampleRealmProxyInterface.realmGet$batteryLevel(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryWiredIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryWired(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryActiveIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryActive(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryInactiveIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryInactive(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryFreeIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryFree(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.memoryUserIndex, j8, (long) sampleRealmProxyInterface.realmGet$memoryUser(), false);
                String realmGet$triggeredBy = sampleRealmProxyInterface.realmGet$triggeredBy();
                if (realmGet$triggeredBy != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.triggeredByIndex, j3, realmGet$triggeredBy, false);
                } else {
                    Table.nativeSetNull(nativePtr, sampleColumnInfo.triggeredByIndex, j3, false);
                }
                String realmGet$networkStatus = sampleRealmProxyInterface.realmGet$networkStatus();
                if (realmGet$networkStatus != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.networkStatusIndex, j3, realmGet$networkStatus, false);
                } else {
                    Table.nativeSetNull(nativePtr, sampleColumnInfo.networkStatusIndex, j3, false);
                }
                long j9 = j3;
                Table.nativeSetDouble(nativePtr, sampleColumnInfo.distanceTraveledIndex, j9, sampleRealmProxyInterface.realmGet$distanceTraveled(), false);
                Table.nativeSetLong(nativePtr, sampleColumnInfo.screenBrightnessIndex, j9, (long) sampleRealmProxyInterface.realmGet$screenBrightness(), false);
                NetworkDetails realmGet$networkDetails = sampleRealmProxyInterface.realmGet$networkDetails();
                if (realmGet$networkDetails != null) {
                    Long l = (Long) map2.get(realmGet$networkDetails);
                    if (l == null) {
                        l = Long.valueOf(NetworkDetailsRealmProxy.insertOrUpdate(realm2, realmGet$networkDetails, map2));
                    }
                    Table.nativeSetLink(nativePtr, sampleColumnInfo.networkDetailsIndex, j3, l.longValue(), false);
                } else {
                    Table.nativeNullifyLink(nativePtr, sampleColumnInfo.networkDetailsIndex, j3);
                }
                BatteryDetails realmGet$batteryDetails = sampleRealmProxyInterface.realmGet$batteryDetails();
                if (realmGet$batteryDetails != null) {
                    Long l2 = (Long) map2.get(realmGet$batteryDetails);
                    if (l2 == null) {
                        l2 = Long.valueOf(BatteryDetailsRealmProxy.insertOrUpdate(realm2, realmGet$batteryDetails, map2));
                    }
                    Table.nativeSetLink(nativePtr, sampleColumnInfo.batteryDetailsIndex, j3, l2.longValue(), false);
                } else {
                    Table.nativeNullifyLink(nativePtr, sampleColumnInfo.batteryDetailsIndex, j3);
                }
                CpuStatus realmGet$cpuStatus = sampleRealmProxyInterface.realmGet$cpuStatus();
                if (realmGet$cpuStatus != null) {
                    Long l3 = (Long) map2.get(realmGet$cpuStatus);
                    if (l3 == null) {
                        l3 = Long.valueOf(CpuStatusRealmProxy.insertOrUpdate(realm2, realmGet$cpuStatus, map2));
                    }
                    Table.nativeSetLink(nativePtr, sampleColumnInfo.cpuStatusIndex, j3, l3.longValue(), false);
                } else {
                    Table.nativeNullifyLink(nativePtr, sampleColumnInfo.cpuStatusIndex, j3);
                }
                CallInfo realmGet$callInfo = sampleRealmProxyInterface.realmGet$callInfo();
                if (realmGet$callInfo != null) {
                    Long l4 = (Long) map2.get(realmGet$callInfo);
                    if (l4 == null) {
                        l4 = Long.valueOf(CallInfoRealmProxy.insertOrUpdate(realm2, realmGet$callInfo, map2));
                    }
                    Table.nativeSetLink(nativePtr, sampleColumnInfo.callInfoIndex, j3, l4.longValue(), false);
                } else {
                    Table.nativeNullifyLink(nativePtr, sampleColumnInfo.callInfoIndex, j3);
                }
                Table.nativeSetLong(nativePtr, sampleColumnInfo.screenOnIndex, j3, (long) sampleRealmProxyInterface.realmGet$screenOn(), false);
                String realmGet$timeZone = sampleRealmProxyInterface.realmGet$timeZone();
                if (realmGet$timeZone != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.timeZoneIndex, j3, realmGet$timeZone, false);
                } else {
                    Table.nativeSetNull(nativePtr, sampleColumnInfo.timeZoneIndex, j3, false);
                }
                Settings realmGet$settings = sampleRealmProxyInterface.realmGet$settings();
                if (realmGet$settings != null) {
                    Long l5 = (Long) map2.get(realmGet$settings);
                    if (l5 == null) {
                        l5 = Long.valueOf(SettingsRealmProxy.insertOrUpdate(realm2, realmGet$settings, map2));
                    }
                    Table.nativeSetLink(nativePtr, sampleColumnInfo.settingsIndex, j3, l5.longValue(), false);
                } else {
                    Table.nativeNullifyLink(nativePtr, sampleColumnInfo.settingsIndex, j3);
                }
                StorageDetails realmGet$storageDetails = sampleRealmProxyInterface.realmGet$storageDetails();
                if (realmGet$storageDetails != null) {
                    Long l6 = (Long) map2.get(realmGet$storageDetails);
                    if (l6 == null) {
                        l6 = Long.valueOf(StorageDetailsRealmProxy.insertOrUpdate(realm2, realmGet$storageDetails, map2));
                    }
                    Table.nativeSetLink(nativePtr, sampleColumnInfo.storageDetailsIndex, j3, l6.longValue(), false);
                } else {
                    Table.nativeNullifyLink(nativePtr, sampleColumnInfo.storageDetailsIndex, j3);
                }
                String realmGet$countryCode = sampleRealmProxyInterface.realmGet$countryCode();
                if (realmGet$countryCode != null) {
                    Table.nativeSetString(nativePtr, sampleColumnInfo.countryCodeIndex, j3, realmGet$countryCode, false);
                } else {
                    Table.nativeSetNull(nativePtr, sampleColumnInfo.countryCodeIndex, j3, false);
                }
                long j10 = j3;
                OsList osList = new OsList(table.getUncheckedRow(j10), sampleColumnInfo.locationProvidersIndex);
                RealmList realmGet$locationProviders = sampleRealmProxyInterface.realmGet$locationProviders();
                if (realmGet$locationProviders == null || ((long) realmGet$locationProviders.size()) != osList.size()) {
                    osList.removeAll();
                    if (realmGet$locationProviders != null) {
                        Iterator it2 = realmGet$locationProviders.iterator();
                        while (it2.hasNext()) {
                            LocationProvider locationProvider = (LocationProvider) it2.next();
                            Long l7 = (Long) map2.get(locationProvider);
                            if (l7 == null) {
                                l7 = Long.valueOf(LocationProviderRealmProxy.insertOrUpdate(realm2, locationProvider, map2));
                            }
                            osList.addRow(l7.longValue());
                        }
                    }
                } else {
                    int size = realmGet$locationProviders.size();
                    int i = 0;
                    while (i < size) {
                        LocationProvider locationProvider2 = (LocationProvider) realmGet$locationProviders.get(i);
                        Long l8 = (Long) map2.get(locationProvider2);
                        if (l8 == null) {
                            l8 = Long.valueOf(LocationProviderRealmProxy.insertOrUpdate(realm2, locationProvider2, map2));
                        }
                        int i2 = size;
                        osList.setRow((long) i, l8.longValue());
                        i++;
                        size = i2;
                    }
                }
                OsList osList2 = new OsList(table.getUncheckedRow(j10), sampleColumnInfo.processInfosIndex);
                RealmList realmGet$processInfos = sampleRealmProxyInterface.realmGet$processInfos();
                if (realmGet$processInfos == null || ((long) realmGet$processInfos.size()) != osList2.size()) {
                    j4 = nativePtr;
                    osList2.removeAll();
                    if (realmGet$processInfos != null) {
                        Iterator it3 = realmGet$processInfos.iterator();
                        while (it3.hasNext()) {
                            ProcessInfo processInfo = (ProcessInfo) it3.next();
                            Long l9 = (Long) map2.get(processInfo);
                            if (l9 == null) {
                                l9 = Long.valueOf(ProcessInfoRealmProxy.insertOrUpdate(realm2, processInfo, map2));
                            }
                            osList2.addRow(l9.longValue());
                        }
                    }
                } else {
                    int size2 = realmGet$processInfos.size();
                    int i3 = 0;
                    while (i3 < size2) {
                        ProcessInfo processInfo2 = (ProcessInfo) realmGet$processInfos.get(i3);
                        Long l10 = (Long) map2.get(processInfo2);
                        if (l10 == null) {
                            l10 = Long.valueOf(ProcessInfoRealmProxy.insertOrUpdate(realm2, processInfo2, map2));
                        }
                        long j11 = nativePtr;
                        osList2.setRow((long) i3, l10.longValue());
                        i3++;
                        nativePtr = j11;
                    }
                    j4 = nativePtr;
                }
                OsList osList3 = new OsList(table.getUncheckedRow(j10), sampleColumnInfo.featuresIndex);
                RealmList realmGet$features = sampleRealmProxyInterface.realmGet$features();
                if (realmGet$features == null || ((long) realmGet$features.size()) != osList3.size()) {
                    osList3.removeAll();
                    if (realmGet$features != null) {
                        Iterator it4 = realmGet$features.iterator();
                        while (it4.hasNext()) {
                            Feature feature = (Feature) it4.next();
                            Long l11 = (Long) map2.get(feature);
                            if (l11 == null) {
                                l11 = Long.valueOf(FeatureRealmProxy.insertOrUpdate(realm2, feature, map2));
                            }
                            osList3.addRow(l11.longValue());
                        }
                    }
                } else {
                    int size3 = realmGet$features.size();
                    for (int i4 = 0; i4 < size3; i4++) {
                        Feature feature2 = (Feature) realmGet$features.get(i4);
                        Long l12 = (Long) map2.get(feature2);
                        if (l12 == null) {
                            l12 = Long.valueOf(FeatureRealmProxy.insertOrUpdate(realm2, feature2, map2));
                        }
                        osList3.setRow((long) i4, l12.longValue());
                    }
                }
                j5 = j2;
                nativePtr = j4;
            }
        }
    }

    public static Sample createDetachedCopy(Sample sample, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        Sample sample2;
        if (i > i2 || sample == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(sample);
        if (cacheData == null) {
            sample2 = new Sample();
            map.put(sample, new CacheData(i, sample2));
        } else if (i >= cacheData.minDepth) {
            return (Sample) cacheData.object;
        } else {
            Sample sample3 = (Sample) cacheData.object;
            cacheData.minDepth = i;
            sample2 = sample3;
        }
        SampleRealmProxyInterface sampleRealmProxyInterface = sample2;
        SampleRealmProxyInterface sampleRealmProxyInterface2 = sample;
        sampleRealmProxyInterface.realmSet$id(sampleRealmProxyInterface2.realmGet$id());
        sampleRealmProxyInterface.realmSet$uuId(sampleRealmProxyInterface2.realmGet$uuId());
        sampleRealmProxyInterface.realmSet$timestamp(sampleRealmProxyInterface2.realmGet$timestamp());
        sampleRealmProxyInterface.realmSet$version(sampleRealmProxyInterface2.realmGet$version());
        sampleRealmProxyInterface.realmSet$database(sampleRealmProxyInterface2.realmGet$database());
        sampleRealmProxyInterface.realmSet$batteryState(sampleRealmProxyInterface2.realmGet$batteryState());
        sampleRealmProxyInterface.realmSet$batteryLevel(sampleRealmProxyInterface2.realmGet$batteryLevel());
        sampleRealmProxyInterface.realmSet$memoryWired(sampleRealmProxyInterface2.realmGet$memoryWired());
        sampleRealmProxyInterface.realmSet$memoryActive(sampleRealmProxyInterface2.realmGet$memoryActive());
        sampleRealmProxyInterface.realmSet$memoryInactive(sampleRealmProxyInterface2.realmGet$memoryInactive());
        sampleRealmProxyInterface.realmSet$memoryFree(sampleRealmProxyInterface2.realmGet$memoryFree());
        sampleRealmProxyInterface.realmSet$memoryUser(sampleRealmProxyInterface2.realmGet$memoryUser());
        sampleRealmProxyInterface.realmSet$triggeredBy(sampleRealmProxyInterface2.realmGet$triggeredBy());
        sampleRealmProxyInterface.realmSet$networkStatus(sampleRealmProxyInterface2.realmGet$networkStatus());
        sampleRealmProxyInterface.realmSet$distanceTraveled(sampleRealmProxyInterface2.realmGet$distanceTraveled());
        sampleRealmProxyInterface.realmSet$screenBrightness(sampleRealmProxyInterface2.realmGet$screenBrightness());
        int i3 = i + 1;
        sampleRealmProxyInterface.realmSet$networkDetails(NetworkDetailsRealmProxy.createDetachedCopy(sampleRealmProxyInterface2.realmGet$networkDetails(), i3, i2, map));
        sampleRealmProxyInterface.realmSet$batteryDetails(BatteryDetailsRealmProxy.createDetachedCopy(sampleRealmProxyInterface2.realmGet$batteryDetails(), i3, i2, map));
        sampleRealmProxyInterface.realmSet$cpuStatus(CpuStatusRealmProxy.createDetachedCopy(sampleRealmProxyInterface2.realmGet$cpuStatus(), i3, i2, map));
        sampleRealmProxyInterface.realmSet$callInfo(CallInfoRealmProxy.createDetachedCopy(sampleRealmProxyInterface2.realmGet$callInfo(), i3, i2, map));
        sampleRealmProxyInterface.realmSet$screenOn(sampleRealmProxyInterface2.realmGet$screenOn());
        sampleRealmProxyInterface.realmSet$timeZone(sampleRealmProxyInterface2.realmGet$timeZone());
        sampleRealmProxyInterface.realmSet$settings(SettingsRealmProxy.createDetachedCopy(sampleRealmProxyInterface2.realmGet$settings(), i3, i2, map));
        sampleRealmProxyInterface.realmSet$storageDetails(StorageDetailsRealmProxy.createDetachedCopy(sampleRealmProxyInterface2.realmGet$storageDetails(), i3, i2, map));
        sampleRealmProxyInterface.realmSet$countryCode(sampleRealmProxyInterface2.realmGet$countryCode());
        if (i == i2) {
            sampleRealmProxyInterface.realmSet$locationProviders(null);
        } else {
            RealmList realmGet$locationProviders = sampleRealmProxyInterface2.realmGet$locationProviders();
            RealmList realmList = new RealmList();
            sampleRealmProxyInterface.realmSet$locationProviders(realmList);
            int size = realmGet$locationProviders.size();
            for (int i4 = 0; i4 < size; i4++) {
                realmList.add(LocationProviderRealmProxy.createDetachedCopy((LocationProvider) realmGet$locationProviders.get(i4), i3, i2, map));
            }
        }
        if (i == i2) {
            sampleRealmProxyInterface.realmSet$processInfos(null);
        } else {
            RealmList realmGet$processInfos = sampleRealmProxyInterface2.realmGet$processInfos();
            RealmList realmList2 = new RealmList();
            sampleRealmProxyInterface.realmSet$processInfos(realmList2);
            int size2 = realmGet$processInfos.size();
            for (int i5 = 0; i5 < size2; i5++) {
                realmList2.add(ProcessInfoRealmProxy.createDetachedCopy((ProcessInfo) realmGet$processInfos.get(i5), i3, i2, map));
            }
        }
        if (i == i2) {
            sampleRealmProxyInterface.realmSet$features(null);
        } else {
            RealmList realmGet$features = sampleRealmProxyInterface2.realmGet$features();
            RealmList realmList3 = new RealmList();
            sampleRealmProxyInterface.realmSet$features(realmList3);
            int size3 = realmGet$features.size();
            for (int i6 = 0; i6 < size3; i6++) {
                realmList3.add(FeatureRealmProxy.createDetachedCopy((Feature) realmGet$features.get(i6), i3, i2, map));
            }
        }
        return sample2;
    }

    static Sample update(Realm realm, Sample sample, Sample sample2, Map<RealmModel, RealmObjectProxy> map) {
        SampleRealmProxyInterface sampleRealmProxyInterface = sample;
        SampleRealmProxyInterface sampleRealmProxyInterface2 = sample2;
        sampleRealmProxyInterface.realmSet$uuId(sampleRealmProxyInterface2.realmGet$uuId());
        sampleRealmProxyInterface.realmSet$timestamp(sampleRealmProxyInterface2.realmGet$timestamp());
        sampleRealmProxyInterface.realmSet$version(sampleRealmProxyInterface2.realmGet$version());
        sampleRealmProxyInterface.realmSet$database(sampleRealmProxyInterface2.realmGet$database());
        sampleRealmProxyInterface.realmSet$batteryState(sampleRealmProxyInterface2.realmGet$batteryState());
        sampleRealmProxyInterface.realmSet$batteryLevel(sampleRealmProxyInterface2.realmGet$batteryLevel());
        sampleRealmProxyInterface.realmSet$memoryWired(sampleRealmProxyInterface2.realmGet$memoryWired());
        sampleRealmProxyInterface.realmSet$memoryActive(sampleRealmProxyInterface2.realmGet$memoryActive());
        sampleRealmProxyInterface.realmSet$memoryInactive(sampleRealmProxyInterface2.realmGet$memoryInactive());
        sampleRealmProxyInterface.realmSet$memoryFree(sampleRealmProxyInterface2.realmGet$memoryFree());
        sampleRealmProxyInterface.realmSet$memoryUser(sampleRealmProxyInterface2.realmGet$memoryUser());
        sampleRealmProxyInterface.realmSet$triggeredBy(sampleRealmProxyInterface2.realmGet$triggeredBy());
        sampleRealmProxyInterface.realmSet$networkStatus(sampleRealmProxyInterface2.realmGet$networkStatus());
        sampleRealmProxyInterface.realmSet$distanceTraveled(sampleRealmProxyInterface2.realmGet$distanceTraveled());
        sampleRealmProxyInterface.realmSet$screenBrightness(sampleRealmProxyInterface2.realmGet$screenBrightness());
        NetworkDetails realmGet$networkDetails = sampleRealmProxyInterface2.realmGet$networkDetails();
        if (realmGet$networkDetails == null) {
            sampleRealmProxyInterface.realmSet$networkDetails(null);
        } else {
            NetworkDetails networkDetails = (NetworkDetails) map.get(realmGet$networkDetails);
            if (networkDetails != null) {
                sampleRealmProxyInterface.realmSet$networkDetails(networkDetails);
            } else {
                sampleRealmProxyInterface.realmSet$networkDetails(NetworkDetailsRealmProxy.copyOrUpdate(realm, realmGet$networkDetails, true, map));
            }
        }
        BatteryDetails realmGet$batteryDetails = sampleRealmProxyInterface2.realmGet$batteryDetails();
        if (realmGet$batteryDetails == null) {
            sampleRealmProxyInterface.realmSet$batteryDetails(null);
        } else {
            BatteryDetails batteryDetails = (BatteryDetails) map.get(realmGet$batteryDetails);
            if (batteryDetails != null) {
                sampleRealmProxyInterface.realmSet$batteryDetails(batteryDetails);
            } else {
                sampleRealmProxyInterface.realmSet$batteryDetails(BatteryDetailsRealmProxy.copyOrUpdate(realm, realmGet$batteryDetails, true, map));
            }
        }
        CpuStatus realmGet$cpuStatus = sampleRealmProxyInterface2.realmGet$cpuStatus();
        if (realmGet$cpuStatus == null) {
            sampleRealmProxyInterface.realmSet$cpuStatus(null);
        } else {
            CpuStatus cpuStatus = (CpuStatus) map.get(realmGet$cpuStatus);
            if (cpuStatus != null) {
                sampleRealmProxyInterface.realmSet$cpuStatus(cpuStatus);
            } else {
                sampleRealmProxyInterface.realmSet$cpuStatus(CpuStatusRealmProxy.copyOrUpdate(realm, realmGet$cpuStatus, true, map));
            }
        }
        CallInfo realmGet$callInfo = sampleRealmProxyInterface2.realmGet$callInfo();
        if (realmGet$callInfo == null) {
            sampleRealmProxyInterface.realmSet$callInfo(null);
        } else {
            CallInfo callInfo = (CallInfo) map.get(realmGet$callInfo);
            if (callInfo != null) {
                sampleRealmProxyInterface.realmSet$callInfo(callInfo);
            } else {
                sampleRealmProxyInterface.realmSet$callInfo(CallInfoRealmProxy.copyOrUpdate(realm, realmGet$callInfo, true, map));
            }
        }
        sampleRealmProxyInterface.realmSet$screenOn(sampleRealmProxyInterface2.realmGet$screenOn());
        sampleRealmProxyInterface.realmSet$timeZone(sampleRealmProxyInterface2.realmGet$timeZone());
        Settings realmGet$settings = sampleRealmProxyInterface2.realmGet$settings();
        if (realmGet$settings == null) {
            sampleRealmProxyInterface.realmSet$settings(null);
        } else {
            Settings settings = (Settings) map.get(realmGet$settings);
            if (settings != null) {
                sampleRealmProxyInterface.realmSet$settings(settings);
            } else {
                sampleRealmProxyInterface.realmSet$settings(SettingsRealmProxy.copyOrUpdate(realm, realmGet$settings, true, map));
            }
        }
        StorageDetails realmGet$storageDetails = sampleRealmProxyInterface2.realmGet$storageDetails();
        if (realmGet$storageDetails == null) {
            sampleRealmProxyInterface.realmSet$storageDetails(null);
        } else {
            StorageDetails storageDetails = (StorageDetails) map.get(realmGet$storageDetails);
            if (storageDetails != null) {
                sampleRealmProxyInterface.realmSet$storageDetails(storageDetails);
            } else {
                sampleRealmProxyInterface.realmSet$storageDetails(StorageDetailsRealmProxy.copyOrUpdate(realm, realmGet$storageDetails, true, map));
            }
        }
        sampleRealmProxyInterface.realmSet$countryCode(sampleRealmProxyInterface2.realmGet$countryCode());
        RealmList realmGet$locationProviders = sampleRealmProxyInterface2.realmGet$locationProviders();
        RealmList realmGet$locationProviders2 = sampleRealmProxyInterface.realmGet$locationProviders();
        int i = 0;
        if (realmGet$locationProviders == null || realmGet$locationProviders.size() != realmGet$locationProviders2.size()) {
            realmGet$locationProviders2.clear();
            if (realmGet$locationProviders != null) {
                for (int i2 = 0; i2 < realmGet$locationProviders.size(); i2++) {
                    LocationProvider locationProvider = (LocationProvider) realmGet$locationProviders.get(i2);
                    LocationProvider locationProvider2 = (LocationProvider) map.get(locationProvider);
                    if (locationProvider2 != null) {
                        realmGet$locationProviders2.add(locationProvider2);
                    } else {
                        realmGet$locationProviders2.add(LocationProviderRealmProxy.copyOrUpdate(realm, locationProvider, true, map));
                    }
                }
            }
        } else {
            int size = realmGet$locationProviders.size();
            for (int i3 = 0; i3 < size; i3++) {
                LocationProvider locationProvider3 = (LocationProvider) realmGet$locationProviders.get(i3);
                LocationProvider locationProvider4 = (LocationProvider) map.get(locationProvider3);
                if (locationProvider4 != null) {
                    realmGet$locationProviders2.set(i3, locationProvider4);
                } else {
                    realmGet$locationProviders2.set(i3, LocationProviderRealmProxy.copyOrUpdate(realm, locationProvider3, true, map));
                }
            }
        }
        RealmList realmGet$processInfos = sampleRealmProxyInterface2.realmGet$processInfos();
        RealmList realmGet$processInfos2 = sampleRealmProxyInterface.realmGet$processInfos();
        if (realmGet$processInfos == null || realmGet$processInfos.size() != realmGet$processInfos2.size()) {
            realmGet$processInfos2.clear();
            if (realmGet$processInfos != null) {
                for (int i4 = 0; i4 < realmGet$processInfos.size(); i4++) {
                    ProcessInfo processInfo = (ProcessInfo) realmGet$processInfos.get(i4);
                    ProcessInfo processInfo2 = (ProcessInfo) map.get(processInfo);
                    if (processInfo2 != null) {
                        realmGet$processInfos2.add(processInfo2);
                    } else {
                        realmGet$processInfos2.add(ProcessInfoRealmProxy.copyOrUpdate(realm, processInfo, true, map));
                    }
                }
            }
        } else {
            int size2 = realmGet$processInfos.size();
            for (int i5 = 0; i5 < size2; i5++) {
                ProcessInfo processInfo3 = (ProcessInfo) realmGet$processInfos.get(i5);
                ProcessInfo processInfo4 = (ProcessInfo) map.get(processInfo3);
                if (processInfo4 != null) {
                    realmGet$processInfos2.set(i5, processInfo4);
                } else {
                    realmGet$processInfos2.set(i5, ProcessInfoRealmProxy.copyOrUpdate(realm, processInfo3, true, map));
                }
            }
        }
        RealmList realmGet$features = sampleRealmProxyInterface2.realmGet$features();
        RealmList realmGet$features2 = sampleRealmProxyInterface.realmGet$features();
        if (realmGet$features == null || realmGet$features.size() != realmGet$features2.size()) {
            realmGet$features2.clear();
            if (realmGet$features != null) {
                while (i < realmGet$features.size()) {
                    Feature feature = (Feature) realmGet$features.get(i);
                    Feature feature2 = (Feature) map.get(feature);
                    if (feature2 != null) {
                        realmGet$features2.add(feature2);
                    } else {
                        realmGet$features2.add(FeatureRealmProxy.copyOrUpdate(realm, feature, true, map));
                    }
                    i++;
                }
            }
        } else {
            int size3 = realmGet$features.size();
            while (i < size3) {
                Feature feature3 = (Feature) realmGet$features.get(i);
                Feature feature4 = (Feature) map.get(feature3);
                if (feature4 != null) {
                    realmGet$features2.set(i, feature4);
                } else {
                    realmGet$features2.set(i, FeatureRealmProxy.copyOrUpdate(realm, feature3, true, map));
                }
                i++;
            }
        }
        return sample;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("Sample = proxy[");
        sb.append("{id:");
        sb.append(realmGet$id());
        sb.append("}");
        sb.append(",");
        sb.append("{uuId:");
        sb.append(realmGet$uuId() != null ? realmGet$uuId() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{timestamp:");
        sb.append(realmGet$timestamp());
        sb.append("}");
        sb.append(",");
        sb.append("{version:");
        sb.append(realmGet$version());
        sb.append("}");
        sb.append(",");
        sb.append("{database:");
        sb.append(realmGet$database());
        sb.append("}");
        sb.append(",");
        sb.append("{batteryState:");
        sb.append(realmGet$batteryState() != null ? realmGet$batteryState() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{batteryLevel:");
        sb.append(realmGet$batteryLevel());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryWired:");
        sb.append(realmGet$memoryWired());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryActive:");
        sb.append(realmGet$memoryActive());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryInactive:");
        sb.append(realmGet$memoryInactive());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryFree:");
        sb.append(realmGet$memoryFree());
        sb.append("}");
        sb.append(",");
        sb.append("{memoryUser:");
        sb.append(realmGet$memoryUser());
        sb.append("}");
        sb.append(",");
        sb.append("{triggeredBy:");
        sb.append(realmGet$triggeredBy() != null ? realmGet$triggeredBy() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{networkStatus:");
        sb.append(realmGet$networkStatus() != null ? realmGet$networkStatus() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{distanceTraveled:");
        sb.append(realmGet$distanceTraveled());
        sb.append("}");
        sb.append(",");
        sb.append("{screenBrightness:");
        sb.append(realmGet$screenBrightness());
        sb.append("}");
        sb.append(",");
        sb.append("{networkDetails:");
        sb.append(realmGet$networkDetails() != null ? "NetworkDetails" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{batteryDetails:");
        sb.append(realmGet$batteryDetails() != null ? "BatteryDetails" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{cpuStatus:");
        sb.append(realmGet$cpuStatus() != null ? "CpuStatus" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{callInfo:");
        sb.append(realmGet$callInfo() != null ? "CallInfo" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{screenOn:");
        sb.append(realmGet$screenOn());
        sb.append("}");
        sb.append(",");
        sb.append("{timeZone:");
        sb.append(realmGet$timeZone() != null ? realmGet$timeZone() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{settings:");
        sb.append(realmGet$settings() != null ? CBLocation.LOCATION_SETTINGS : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{storageDetails:");
        sb.append(realmGet$storageDetails() != null ? "StorageDetails" : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{countryCode:");
        sb.append(realmGet$countryCode() != null ? realmGet$countryCode() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{locationProviders:");
        sb.append("RealmList<LocationProvider>[");
        sb.append(realmGet$locationProviders().size());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        sb.append("}");
        sb.append(",");
        sb.append("{processInfos:");
        sb.append("RealmList<ProcessInfo>[");
        sb.append(realmGet$processInfos().size());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        sb.append("}");
        sb.append(",");
        sb.append("{features:");
        sb.append("RealmList<Feature>[");
        sb.append(realmGet$features().size());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SampleRealmProxy sampleRealmProxy = (SampleRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = sampleRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = sampleRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == sampleRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
