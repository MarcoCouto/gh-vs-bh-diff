package io.realm.internal;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.realm.RealmFieldType;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nullable;

public abstract class ColumnInfo {
    private final Map<String, ColumnDetails> indicesMap;
    private final boolean mutable;

    public static final class ColumnDetails {
        public final long columnIndex;
        public final RealmFieldType columnType;
        public final String linkedClassName;

        private ColumnDetails(long j, RealmFieldType realmFieldType, @Nullable String str) {
            this.columnIndex = j;
            this.columnType = realmFieldType;
            this.linkedClassName = str;
        }

        ColumnDetails(Property property) {
            this(property.getColumnIndex(), property.getType(), property.getLinkedObjectName());
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("ColumnDetails[");
            sb.append(this.columnIndex);
            sb.append(", ");
            sb.append(this.columnType);
            sb.append(", ");
            sb.append(this.linkedClassName);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            return sb.toString();
        }
    }

    /* access modifiers changed from: protected */
    public abstract ColumnInfo copy(boolean z);

    /* access modifiers changed from: protected */
    public abstract void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2);

    protected ColumnInfo(int i) {
        this(i, true);
    }

    protected ColumnInfo(@Nullable ColumnInfo columnInfo, boolean z) {
        this(columnInfo == null ? 0 : columnInfo.indicesMap.size(), z);
        if (columnInfo != null) {
            this.indicesMap.putAll(columnInfo.indicesMap);
        }
    }

    private ColumnInfo(int i, boolean z) {
        this.indicesMap = new HashMap(i);
        this.mutable = z;
    }

    public final boolean isMutable() {
        return this.mutable;
    }

    public long getColumnIndex(String str) {
        ColumnDetails columnDetails = (ColumnDetails) this.indicesMap.get(str);
        if (columnDetails == null) {
            return -1;
        }
        return columnDetails.columnIndex;
    }

    @Nullable
    public ColumnDetails getColumnDetails(String str) {
        return (ColumnDetails) this.indicesMap.get(str);
    }

    public void copyFrom(ColumnInfo columnInfo) {
        if (!this.mutable) {
            throw new UnsupportedOperationException("Attempt to modify an immutable ColumnInfo");
        } else if (columnInfo != null) {
            this.indicesMap.clear();
            this.indicesMap.putAll(columnInfo.indicesMap);
            copy(columnInfo, this);
        } else {
            throw new NullPointerException("Attempt to copy null ColumnInfo");
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ColumnInfo[");
        sb.append(this.mutable);
        sb.append(",");
        if (this.indicesMap != null) {
            boolean z = false;
            for (Entry entry : this.indicesMap.entrySet()) {
                if (z) {
                    sb.append(",");
                }
                sb.append((String) entry.getKey());
                sb.append("->");
                sb.append(entry.getValue());
                z = true;
            }
        }
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final long addColumnDetails(String str, OsObjectSchemaInfo osObjectSchemaInfo) {
        Property property = osObjectSchemaInfo.getProperty(str);
        this.indicesMap.put(str, new ColumnDetails(property));
        return property.getColumnIndex();
    }

    /* access modifiers changed from: protected */
    public final void addBacklinkDetails(OsSchemaInfo osSchemaInfo, String str, String str2, String str3) {
        long columnIndex = osSchemaInfo.getObjectSchemaInfo(str2).getProperty(str3).getColumnIndex();
        Map<String, ColumnDetails> map = this.indicesMap;
        ColumnDetails columnDetails = new ColumnDetails(columnIndex, RealmFieldType.LINKING_OBJECTS, str2);
        map.put(str, columnDetails);
    }

    public Map<String, ColumnDetails> getIndicesMap() {
        return this.indicesMap;
    }
}
