package io.realm;

public interface CallInfoRealmProxyInterface {
    String realmGet$callStatus();

    double realmGet$incomingCallTime();

    double realmGet$nonCallTime();

    double realmGet$outgoingCallTime();

    void realmSet$callStatus(String str);

    void realmSet$incomingCallTime(double d);

    void realmSet$nonCallTime(double d);

    void realmSet$outgoingCallTime(double d);
}
