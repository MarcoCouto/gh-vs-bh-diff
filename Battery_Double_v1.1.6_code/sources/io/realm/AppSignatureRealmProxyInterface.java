package io.realm;

public interface AppSignatureRealmProxyInterface {
    String realmGet$signature();

    void realmSet$signature(String str);
}
