package io.realm;

public interface BatteryDetailsRealmProxyInterface {
    int realmGet$capacity();

    int realmGet$chargeCounter();

    String realmGet$charger();

    int realmGet$currentAverage();

    int realmGet$currentNow();

    long realmGet$energyCounter();

    String realmGet$health();

    String realmGet$technology();

    double realmGet$temperature();

    double realmGet$voltage();

    void realmSet$capacity(int i);

    void realmSet$chargeCounter(int i);

    void realmSet$charger(String str);

    void realmSet$currentAverage(int i);

    void realmSet$currentNow(int i);

    void realmSet$energyCounter(long j);

    void realmSet$health(String str);

    void realmSet$technology(String str);

    void realmSet$temperature(double d);

    void realmSet$voltage(double d);
}
