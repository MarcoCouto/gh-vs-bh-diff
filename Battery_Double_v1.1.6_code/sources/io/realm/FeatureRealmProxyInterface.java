package io.realm;

public interface FeatureRealmProxyInterface {
    String realmGet$key();

    String realmGet$value();

    void realmSet$key(String str);

    void realmSet$value(String str);
}
