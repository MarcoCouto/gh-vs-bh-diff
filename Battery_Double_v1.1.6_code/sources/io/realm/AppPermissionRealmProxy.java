package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mansoon.BatteryDouble.models.data.AppPermission;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AppPermissionRealmProxy extends AppPermission implements RealmObjectProxy, AppPermissionRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private AppPermissionColumnInfo columnInfo;
    private ProxyState<AppPermission> proxyState;

    static final class AppPermissionColumnInfo extends ColumnInfo {
        long permissionIndex;

        AppPermissionColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(1);
            this.permissionIndex = addColumnDetails(ParametersKeys.PERMISSION, osSchemaInfo.getObjectSchemaInfo("AppPermission"));
        }

        AppPermissionColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new AppPermissionColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            ((AppPermissionColumnInfo) columnInfo2).permissionIndex = ((AppPermissionColumnInfo) columnInfo).permissionIndex;
        }
    }

    public static String getTableName() {
        return "class_AppPermission";
    }

    static {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(ParametersKeys.PERMISSION);
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    AppPermissionRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (AppPermissionColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public String realmGet$permission() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.permissionIndex);
    }

    public void realmSet$permission(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.permissionIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.permissionIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.permissionIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.permissionIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("AppPermission", 1, 0);
        builder.addPersistedProperty(ParametersKeys.PERMISSION, RealmFieldType.STRING, false, false, false);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static AppPermissionColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new AppPermissionColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static AppPermission createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        AppPermission appPermission = (AppPermission) realm.createObjectInternal(AppPermission.class, true, Collections.emptyList());
        AppPermissionRealmProxyInterface appPermissionRealmProxyInterface = appPermission;
        if (jSONObject.has(ParametersKeys.PERMISSION)) {
            if (jSONObject.isNull(ParametersKeys.PERMISSION)) {
                appPermissionRealmProxyInterface.realmSet$permission(null);
            } else {
                appPermissionRealmProxyInterface.realmSet$permission(jSONObject.getString(ParametersKeys.PERMISSION));
            }
        }
        return appPermission;
    }

    @TargetApi(11)
    public static AppPermission createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        AppPermission appPermission = new AppPermission();
        AppPermissionRealmProxyInterface appPermissionRealmProxyInterface = appPermission;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            if (!jsonReader.nextName().equals(ParametersKeys.PERMISSION)) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                appPermissionRealmProxyInterface.realmSet$permission(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
                appPermissionRealmProxyInterface.realmSet$permission(null);
            }
        }
        jsonReader.endObject();
        return (AppPermission) realm.copyToRealm(appPermission);
    }

    public static AppPermission copyOrUpdate(Realm realm, AppPermission appPermission, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (appPermission instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appPermission;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return appPermission;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(appPermission);
        if (realmObjectProxy2 != null) {
            return (AppPermission) realmObjectProxy2;
        }
        return copy(realm, appPermission, z, map);
    }

    public static AppPermission copy(Realm realm, AppPermission appPermission, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(appPermission);
        if (realmObjectProxy != null) {
            return (AppPermission) realmObjectProxy;
        }
        AppPermission appPermission2 = (AppPermission) realm.createObjectInternal(AppPermission.class, false, Collections.emptyList());
        map.put(appPermission, (RealmObjectProxy) appPermission2);
        appPermission2.realmSet$permission(appPermission.realmGet$permission());
        return appPermission2;
    }

    public static long insert(Realm realm, AppPermission appPermission, Map<RealmModel, Long> map) {
        if (appPermission instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appPermission;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(AppPermission.class);
        long nativePtr = table.getNativePtr();
        AppPermissionColumnInfo appPermissionColumnInfo = (AppPermissionColumnInfo) realm.getSchema().getColumnInfo(AppPermission.class);
        long createRow = OsObject.createRow(table);
        map.put(appPermission, Long.valueOf(createRow));
        String realmGet$permission = appPermission.realmGet$permission();
        if (realmGet$permission != null) {
            Table.nativeSetString(nativePtr, appPermissionColumnInfo.permissionIndex, createRow, realmGet$permission, false);
        }
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Table table = realm.getTable(AppPermission.class);
        long nativePtr = table.getNativePtr();
        AppPermissionColumnInfo appPermissionColumnInfo = (AppPermissionColumnInfo) realm.getSchema().getColumnInfo(AppPermission.class);
        while (it.hasNext()) {
            AppPermission appPermission = (AppPermission) it.next();
            if (!map.containsKey(appPermission)) {
                if (appPermission instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appPermission;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map.put(appPermission, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map.put(appPermission, Long.valueOf(createRow));
                String realmGet$permission = appPermission.realmGet$permission();
                if (realmGet$permission != null) {
                    Table.nativeSetString(nativePtr, appPermissionColumnInfo.permissionIndex, createRow, realmGet$permission, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, AppPermission appPermission, Map<RealmModel, Long> map) {
        if (appPermission instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appPermission;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(AppPermission.class);
        long nativePtr = table.getNativePtr();
        AppPermissionColumnInfo appPermissionColumnInfo = (AppPermissionColumnInfo) realm.getSchema().getColumnInfo(AppPermission.class);
        long createRow = OsObject.createRow(table);
        map.put(appPermission, Long.valueOf(createRow));
        String realmGet$permission = appPermission.realmGet$permission();
        if (realmGet$permission != null) {
            Table.nativeSetString(nativePtr, appPermissionColumnInfo.permissionIndex, createRow, realmGet$permission, false);
        } else {
            Table.nativeSetNull(nativePtr, appPermissionColumnInfo.permissionIndex, createRow, false);
        }
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Table table = realm.getTable(AppPermission.class);
        long nativePtr = table.getNativePtr();
        AppPermissionColumnInfo appPermissionColumnInfo = (AppPermissionColumnInfo) realm.getSchema().getColumnInfo(AppPermission.class);
        while (it.hasNext()) {
            AppPermission appPermission = (AppPermission) it.next();
            if (!map.containsKey(appPermission)) {
                if (appPermission instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appPermission;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map.put(appPermission, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map.put(appPermission, Long.valueOf(createRow));
                String realmGet$permission = appPermission.realmGet$permission();
                if (realmGet$permission != null) {
                    Table.nativeSetString(nativePtr, appPermissionColumnInfo.permissionIndex, createRow, realmGet$permission, false);
                } else {
                    Table.nativeSetNull(nativePtr, appPermissionColumnInfo.permissionIndex, createRow, false);
                }
            }
        }
    }

    public static AppPermission createDetachedCopy(AppPermission appPermission, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        AppPermission appPermission2;
        if (i > i2 || appPermission == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(appPermission);
        if (cacheData == null) {
            appPermission2 = new AppPermission();
            map.put(appPermission, new CacheData(i, appPermission2));
        } else if (i >= cacheData.minDepth) {
            return (AppPermission) cacheData.object;
        } else {
            AppPermission appPermission3 = (AppPermission) cacheData.object;
            cacheData.minDepth = i;
            appPermission2 = appPermission3;
        }
        appPermission2.realmSet$permission(appPermission.realmGet$permission());
        return appPermission2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("AppPermission = proxy[");
        sb.append("{permission:");
        sb.append(realmGet$permission() != null ? realmGet$permission() : "null");
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AppPermissionRealmProxy appPermissionRealmProxy = (AppPermissionRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = appPermissionRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = appPermissionRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == appPermissionRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
