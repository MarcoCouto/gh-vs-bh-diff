package io.realm;

import com.mansoon.BatteryDouble.models.data.BatteryDetails;
import com.mansoon.BatteryDouble.models.data.CallInfo;
import com.mansoon.BatteryDouble.models.data.CpuStatus;
import com.mansoon.BatteryDouble.models.data.Feature;
import com.mansoon.BatteryDouble.models.data.LocationProvider;
import com.mansoon.BatteryDouble.models.data.NetworkDetails;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import com.mansoon.BatteryDouble.models.data.Settings;
import com.mansoon.BatteryDouble.models.data.StorageDetails;

public interface SampleRealmProxyInterface {
    BatteryDetails realmGet$batteryDetails();

    double realmGet$batteryLevel();

    String realmGet$batteryState();

    CallInfo realmGet$callInfo();

    String realmGet$countryCode();

    CpuStatus realmGet$cpuStatus();

    int realmGet$database();

    double realmGet$distanceTraveled();

    RealmList<Feature> realmGet$features();

    int realmGet$id();

    RealmList<LocationProvider> realmGet$locationProviders();

    int realmGet$memoryActive();

    int realmGet$memoryFree();

    int realmGet$memoryInactive();

    int realmGet$memoryUser();

    int realmGet$memoryWired();

    NetworkDetails realmGet$networkDetails();

    String realmGet$networkStatus();

    RealmList<ProcessInfo> realmGet$processInfos();

    int realmGet$screenBrightness();

    int realmGet$screenOn();

    Settings realmGet$settings();

    StorageDetails realmGet$storageDetails();

    String realmGet$timeZone();

    long realmGet$timestamp();

    String realmGet$triggeredBy();

    String realmGet$uuId();

    int realmGet$version();

    void realmSet$batteryDetails(BatteryDetails batteryDetails);

    void realmSet$batteryLevel(double d);

    void realmSet$batteryState(String str);

    void realmSet$callInfo(CallInfo callInfo);

    void realmSet$countryCode(String str);

    void realmSet$cpuStatus(CpuStatus cpuStatus);

    void realmSet$database(int i);

    void realmSet$distanceTraveled(double d);

    void realmSet$features(RealmList<Feature> realmList);

    void realmSet$id(int i);

    void realmSet$locationProviders(RealmList<LocationProvider> realmList);

    void realmSet$memoryActive(int i);

    void realmSet$memoryFree(int i);

    void realmSet$memoryInactive(int i);

    void realmSet$memoryUser(int i);

    void realmSet$memoryWired(int i);

    void realmSet$networkDetails(NetworkDetails networkDetails);

    void realmSet$networkStatus(String str);

    void realmSet$processInfos(RealmList<ProcessInfo> realmList);

    void realmSet$screenBrightness(int i);

    void realmSet$screenOn(int i);

    void realmSet$settings(Settings settings);

    void realmSet$storageDetails(StorageDetails storageDetails);

    void realmSet$timeZone(String str);

    void realmSet$timestamp(long j);

    void realmSet$triggeredBy(String str);

    void realmSet$uuId(String str);

    void realmSet$version(int i);
}
