package io.realm;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.SystemClock;
import android.util.JsonReader;
import io.reactivex.Flowable;
import io.realm.BaseRealm.InstanceCallback;
import io.realm.RealmConfiguration.Builder;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnIndices;
import io.realm.internal.ObjectServerFacade;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectStore;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmCore;
import io.realm.internal.RealmNotifier;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.SharedRealm;
import io.realm.internal.SharedRealm.VersionID;
import io.realm.internal.Table;
import io.realm.internal.async.RealmAsyncTaskImpl;
import io.realm.internal.async.RealmThreadPoolExecutor;
import io.realm.log.RealmLog;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Realm extends BaseRealm {
    public static final String DEFAULT_REALM_NAME = "default.realm";
    private static final String NULL_CONFIG_MSG = "A non-null RealmConfiguration must be provided";
    private static RealmConfiguration defaultConfiguration;
    private static final Object defaultConfigurationLock = new Object();
    private final RealmSchema schema;

    public static abstract class Callback extends InstanceCallback<Realm> {
        public abstract void onSuccess(Realm realm);

        public void onError(Throwable th) {
            super.onError(th);
        }
    }

    public static abstract class PartialSyncCallback<T extends RealmModel> {
        public abstract void onError(RealmException realmException);

        public abstract void onSuccess(RealmResults<T> realmResults);
    }

    public interface Transaction {

        public static class Callback {
            public void onError(Exception exc) {
            }

            public void onSuccess() {
            }
        }

        public interface OnError {
            void onError(Throwable th);
        }

        public interface OnSuccess {
            void onSuccess();
        }

        void execute(Realm realm);
    }

    public /* bridge */ /* synthetic */ void beginTransaction() {
        super.beginTransaction();
    }

    public /* bridge */ /* synthetic */ void cancelTransaction() {
        super.cancelTransaction();
    }

    public /* bridge */ /* synthetic */ void close() {
        super.close();
    }

    public /* bridge */ /* synthetic */ void commitTransaction() {
        super.commitTransaction();
    }

    public /* bridge */ /* synthetic */ void deleteAll() {
        super.deleteAll();
    }

    public /* bridge */ /* synthetic */ RealmConfiguration getConfiguration() {
        return super.getConfiguration();
    }

    public /* bridge */ /* synthetic */ String getPath() {
        return super.getPath();
    }

    public /* bridge */ /* synthetic */ long getVersion() {
        return super.getVersion();
    }

    public /* bridge */ /* synthetic */ boolean isAutoRefresh() {
        return super.isAutoRefresh();
    }

    public /* bridge */ /* synthetic */ boolean isClosed() {
        return super.isClosed();
    }

    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    public /* bridge */ /* synthetic */ boolean isInTransaction() {
        return super.isInTransaction();
    }

    public /* bridge */ /* synthetic */ void refresh() {
        super.refresh();
    }

    public /* bridge */ /* synthetic */ void setAutoRefresh(boolean z) {
        super.setAutoRefresh(z);
    }

    public /* bridge */ /* synthetic */ void stopWaitForChange() {
        super.stopWaitForChange();
    }

    public /* bridge */ /* synthetic */ boolean waitForChange() {
        return super.waitForChange();
    }

    public /* bridge */ /* synthetic */ void writeCopyTo(File file) {
        super.writeCopyTo(file);
    }

    public /* bridge */ /* synthetic */ void writeEncryptedCopyTo(File file, byte[] bArr) {
        super.writeEncryptedCopyTo(file, bArr);
    }

    private Realm(RealmCache realmCache) {
        super(realmCache, createExpectedSchemaInfo(realmCache.getConfiguration().getSchemaMediator()));
        this.schema = new ImmutableRealmSchema(this, new ColumnIndices(this.configuration.getSchemaMediator(), this.sharedRealm.getSchemaInfo()));
        if (this.configuration.isReadOnly()) {
            RealmProxyMediator schemaMediator = this.configuration.getSchemaMediator();
            for (Class tableName : schemaMediator.getModelClasses()) {
                String tableName2 = schemaMediator.getTableName(tableName);
                if (!this.sharedRealm.hasTable(tableName2)) {
                    this.sharedRealm.close();
                    throw new RealmMigrationNeededException(this.configuration.getPath(), String.format(Locale.US, "Cannot open the read only Realm. '%s' is missing.", new Object[]{Table.getClassNameForTable(tableName2)}));
                }
            }
        }
    }

    private Realm(SharedRealm sharedRealm) {
        super(sharedRealm);
        this.schema = new ImmutableRealmSchema(this, new ColumnIndices(this.configuration.getSchemaMediator(), sharedRealm.getSchemaInfo()));
    }

    private static OsSchemaInfo createExpectedSchemaInfo(RealmProxyMediator realmProxyMediator) {
        return new OsSchemaInfo(realmProxyMediator.getExpectedObjectSchemaInfoMap().values());
    }

    public Flowable<Realm> asFlowable() {
        return this.configuration.getRxFactory().from(this);
    }

    public RealmSchema getSchema() {
        return this.schema;
    }

    public static synchronized void init(Context context) {
        synchronized (Realm.class) {
            if (BaseRealm.applicationContext == null) {
                if (context != null) {
                    checkFilesDirAvailable(context);
                    RealmCore.loadLibrary(context);
                    setDefaultConfiguration(new Builder(context).build());
                    ObjectServerFacade.getSyncFacadeIfPossible().init(context);
                    if (context.getApplicationContext() != null) {
                        BaseRealm.applicationContext = context.getApplicationContext();
                    } else {
                        BaseRealm.applicationContext = context;
                    }
                    SharedRealm.initialize(new File(context.getFilesDir(), ".realm.temp"));
                } else {
                    throw new IllegalArgumentException("Non-null context required.");
                }
            }
        }
    }

    private static void checkFilesDirAvailable(Context context) {
        File filesDir = context.getFilesDir();
        if (filesDir != null) {
            if (!filesDir.exists()) {
                try {
                    filesDir.mkdirs();
                } catch (SecurityException unused) {
                }
            } else {
                return;
            }
        }
        if (filesDir == null || !filesDir.exists()) {
            long[] jArr = {1, 2, 5, 10, 16};
            long j = 0;
            int i = -1;
            do {
                if (context.getFilesDir() != null && context.getFilesDir().exists()) {
                    break;
                }
                i++;
                long j2 = jArr[Math.min(i, jArr.length - 1)];
                SystemClock.sleep(j2);
                j += j2;
            } while (j <= 200);
        }
        if (context.getFilesDir() == null || !context.getFilesDir().exists()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Context.getFilesDir() returns ");
            sb.append(context.getFilesDir());
            sb.append(" which is not an existing directory. See https://issuetracker.google.com/issues/36918154");
            throw new IllegalStateException(sb.toString());
        }
    }

    public static Realm getDefaultInstance() {
        RealmConfiguration defaultConfiguration2 = getDefaultConfiguration();
        if (defaultConfiguration2 != null) {
            return (Realm) RealmCache.createRealmOrGetFromCache(defaultConfiguration2, Realm.class);
        }
        if (BaseRealm.applicationContext == null) {
            throw new IllegalStateException("Call `Realm.init(Context)` before calling this method.");
        }
        throw new IllegalStateException("Set default configuration by using `Realm.setDefaultConfiguration(RealmConfiguration)`.");
    }

    public static Realm getInstance(RealmConfiguration realmConfiguration) {
        if (realmConfiguration != null) {
            return (Realm) RealmCache.createRealmOrGetFromCache(realmConfiguration, Realm.class);
        }
        throw new IllegalArgumentException(NULL_CONFIG_MSG);
    }

    public static RealmAsyncTask getInstanceAsync(RealmConfiguration realmConfiguration, Callback callback) {
        if (realmConfiguration != null) {
            return RealmCache.createRealmOrGetFromCacheAsync(realmConfiguration, callback, Realm.class);
        }
        throw new IllegalArgumentException(NULL_CONFIG_MSG);
    }

    public static void setDefaultConfiguration(RealmConfiguration realmConfiguration) {
        if (realmConfiguration != null) {
            synchronized (defaultConfigurationLock) {
                defaultConfiguration = realmConfiguration;
            }
            return;
        }
        throw new IllegalArgumentException(NULL_CONFIG_MSG);
    }

    @Nullable
    public static RealmConfiguration getDefaultConfiguration() {
        RealmConfiguration realmConfiguration;
        synchronized (defaultConfigurationLock) {
            realmConfiguration = defaultConfiguration;
        }
        return realmConfiguration;
    }

    public static void removeDefaultConfiguration() {
        synchronized (defaultConfigurationLock) {
            defaultConfiguration = null;
        }
    }

    static Realm createInstance(RealmCache realmCache) {
        return new Realm(realmCache);
    }

    static Realm createInstance(SharedRealm sharedRealm) {
        return new Realm(sharedRealm);
    }

    public <E extends RealmModel> void createAllFromJson(Class<E> cls, JSONArray jSONArray) {
        if (cls != null && jSONArray != null) {
            checkIfValid();
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(cls, this, jSONArray.getJSONObject(i), false);
                    i++;
                } catch (JSONException e) {
                    throw new RealmException("Could not map JSON", e);
                }
            }
        }
    }

    public <E extends RealmModel> void createOrUpdateAllFromJson(Class<E> cls, JSONArray jSONArray) {
        if (cls != null && jSONArray != null) {
            checkIfValid();
            checkHasPrimaryKey(cls);
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(cls, this, jSONArray.getJSONObject(i), true);
                    i++;
                } catch (JSONException e) {
                    throw new RealmException("Could not map JSON", e);
                }
            }
        }
    }

    public <E extends RealmModel> void createAllFromJson(Class<E> cls, String str) {
        if (cls != null && str != null && str.length() != 0) {
            try {
                createAllFromJson(cls, new JSONArray(str));
            } catch (JSONException e) {
                throw new RealmException("Could not create JSON array from string", e);
            }
        }
    }

    public <E extends RealmModel> void createOrUpdateAllFromJson(Class<E> cls, String str) {
        if (cls != null && str != null && str.length() != 0) {
            checkIfValid();
            checkHasPrimaryKey(cls);
            try {
                createOrUpdateAllFromJson(cls, new JSONArray(str));
            } catch (JSONException e) {
                throw new RealmException("Could not create JSON array from string", e);
            }
        }
    }

    @TargetApi(11)
    public <E extends RealmModel> void createAllFromJson(Class<E> cls, InputStream inputStream) throws IOException {
        if (cls != null && inputStream != null) {
            checkIfValid();
            JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
            try {
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    this.configuration.getSchemaMediator().createUsingJsonStream(cls, this, jsonReader);
                }
                jsonReader.endArray();
            } finally {
                jsonReader.close();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x004a  */
    @TargetApi(11)
    public <E extends RealmModel> void createOrUpdateAllFromJson(Class<E> cls, InputStream inputStream) {
        if (cls != null && inputStream != null) {
            checkIfValid();
            checkHasPrimaryKey(cls);
            Scanner scanner = null;
            try {
                Scanner scanner2 = getFullStringScanner(inputStream);
                try {
                    JSONArray jSONArray = new JSONArray(scanner2.next());
                    for (int i = 0; i < jSONArray.length(); i++) {
                        this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(cls, this, jSONArray.getJSONObject(i), true);
                    }
                    if (scanner2 != null) {
                        scanner2.close();
                    }
                } catch (JSONException e) {
                    e = e;
                    scanner = scanner2;
                    try {
                        throw new RealmException("Failed to read JSON", e);
                    } catch (Throwable th) {
                        th = th;
                        scanner2 = scanner;
                        if (scanner2 != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (scanner2 != null) {
                        scanner2.close();
                    }
                    throw th;
                }
            } catch (JSONException e2) {
                e = e2;
                throw new RealmException("Failed to read JSON", e);
            }
        }
    }

    @Nullable
    public <E extends RealmModel> E createObjectFromJson(Class<E> cls, JSONObject jSONObject) {
        if (cls == null || jSONObject == null) {
            return null;
        }
        checkIfValid();
        try {
            return this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(cls, this, jSONObject, false);
        } catch (JSONException e) {
            throw new RealmException("Could not map JSON", e);
        }
    }

    public <E extends RealmModel> E createOrUpdateObjectFromJson(Class<E> cls, JSONObject jSONObject) {
        if (cls == null || jSONObject == null) {
            return null;
        }
        checkIfValid();
        checkHasPrimaryKey(cls);
        try {
            return this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(cls, this, jSONObject, true);
        } catch (JSONException e) {
            throw new RealmException("Could not map JSON", e);
        }
    }

    @Nullable
    public <E extends RealmModel> E createObjectFromJson(Class<E> cls, String str) {
        if (cls == null || str == null || str.length() == 0) {
            return null;
        }
        try {
            return createObjectFromJson(cls, new JSONObject(str));
        } catch (JSONException e) {
            throw new RealmException("Could not create Json object from string", e);
        }
    }

    public <E extends RealmModel> E createOrUpdateObjectFromJson(Class<E> cls, String str) {
        if (cls == null || str == null || str.length() == 0) {
            return null;
        }
        checkIfValid();
        checkHasPrimaryKey(cls);
        try {
            return createOrUpdateObjectFromJson(cls, new JSONObject(str));
        } catch (JSONException e) {
            throw new RealmException("Could not create Json object from string", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x004c  */
    @TargetApi(11)
    @Nullable
    public <E extends RealmModel> E createObjectFromJson(Class<E> cls, InputStream inputStream) throws IOException {
        E e;
        Scanner scanner = null;
        if (cls == null || inputStream == null) {
            return null;
        }
        checkIfValid();
        if (OsObjectStore.getPrimaryKeyForObject(this.sharedRealm, this.configuration.getSchemaMediator().getSimpleClassName(cls)) != null) {
            try {
                Scanner scanner2 = getFullStringScanner(inputStream);
                try {
                    e = this.configuration.getSchemaMediator().createOrUpdateUsingJsonObject(cls, this, new JSONObject(scanner2.next()), false);
                    if (scanner2 != null) {
                        scanner2.close();
                    }
                } catch (JSONException e2) {
                    e = e2;
                    scanner = scanner2;
                    try {
                        throw new RealmException("Failed to read JSON", e);
                    } catch (Throwable th) {
                        th = th;
                        scanner2 = scanner;
                        if (scanner2 != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (scanner2 != null) {
                        scanner2.close();
                    }
                    throw th;
                }
            } catch (JSONException e3) {
                e = e3;
                throw new RealmException("Failed to read JSON", e);
            }
        } else {
            JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
            try {
                e = this.configuration.getSchemaMediator().createUsingJsonStream(cls, this, jsonReader);
            } finally {
                jsonReader.close();
            }
        }
        return e;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0036  */
    @TargetApi(11)
    public <E extends RealmModel> E createOrUpdateObjectFromJson(Class<E> cls, InputStream inputStream) {
        Scanner scanner = null;
        if (cls == null || inputStream == null) {
            return null;
        }
        checkIfValid();
        checkHasPrimaryKey(cls);
        try {
            Scanner fullStringScanner = getFullStringScanner(inputStream);
            try {
                E createOrUpdateObjectFromJson = createOrUpdateObjectFromJson(cls, new JSONObject(fullStringScanner.next()));
                if (fullStringScanner != null) {
                    fullStringScanner.close();
                }
                return createOrUpdateObjectFromJson;
            } catch (JSONException e) {
                e = e;
                scanner = fullStringScanner;
                try {
                    throw new RealmException("Failed to read JSON", e);
                } catch (Throwable th) {
                    th = th;
                    if (scanner != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                scanner = fullStringScanner;
                if (scanner != null) {
                    scanner.close();
                }
                throw th;
            }
        } catch (JSONException e2) {
            e = e2;
            throw new RealmException("Failed to read JSON", e);
        }
    }

    private Scanner getFullStringScanner(InputStream inputStream) {
        return new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
    }

    public <E extends RealmModel> E createObject(Class<E> cls) {
        checkIfValid();
        return createObjectInternal(cls, true, Collections.emptyList());
    }

    /* access modifiers changed from: 0000 */
    public <E extends RealmModel> E createObjectInternal(Class<E> cls, boolean z, List<String> list) {
        Table table = this.schema.getTable(cls);
        if (OsObjectStore.getPrimaryKeyForObject(this.sharedRealm, this.configuration.getSchemaMediator().getSimpleClassName(cls)) == null) {
            return this.configuration.getSchemaMediator().newInstance(cls, this, OsObject.create(table), this.schema.getColumnInfo(cls), z, list);
        }
        throw new RealmException(String.format(Locale.US, "'%s' has a primary key, use 'createObject(Class<E>, Object)' instead.", new Object[]{table.getClassName()}));
    }

    public <E extends RealmModel> E createObject(Class<E> cls, @Nullable Object obj) {
        checkIfValid();
        return createObjectInternal(cls, obj, true, Collections.emptyList());
    }

    /* access modifiers changed from: 0000 */
    public <E extends RealmModel> E createObjectInternal(Class<E> cls, @Nullable Object obj, boolean z, List<String> list) {
        return this.configuration.getSchemaMediator().newInstance(cls, this, OsObject.createWithPrimaryKey(this.schema.getTable(cls), obj), this.schema.getColumnInfo(cls), z, list);
    }

    public <E extends RealmModel> E copyToRealm(E e) {
        checkNotNullObject(e);
        return copyOrUpdate(e, false, new HashMap());
    }

    public <E extends RealmModel> E copyToRealmOrUpdate(E e) {
        checkNotNullObject(e);
        checkHasPrimaryKey(e.getClass());
        return copyOrUpdate(e, true, new HashMap());
    }

    public <E extends RealmModel> List<E> copyToRealm(Iterable<E> iterable) {
        if (iterable == null) {
            return new ArrayList();
        }
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        for (E e : iterable) {
            checkNotNullObject(e);
            arrayList.add(copyOrUpdate(e, false, hashMap));
        }
        return arrayList;
    }

    public void insert(Collection<? extends RealmModel> collection) {
        checkIfValidAndInTransaction();
        if (collection == null) {
            throw new IllegalArgumentException("Null objects cannot be inserted into Realm.");
        } else if (!collection.isEmpty()) {
            this.configuration.getSchemaMediator().insert(this, collection);
        }
    }

    public void insert(RealmModel realmModel) {
        checkIfValidAndInTransaction();
        if (realmModel != null) {
            this.configuration.getSchemaMediator().insert(this, realmModel, new HashMap());
            return;
        }
        throw new IllegalArgumentException("Null object cannot be inserted into Realm.");
    }

    public void insertOrUpdate(Collection<? extends RealmModel> collection) {
        checkIfValidAndInTransaction();
        if (collection == null) {
            throw new IllegalArgumentException("Null objects cannot be inserted into Realm.");
        } else if (!collection.isEmpty()) {
            this.configuration.getSchemaMediator().insertOrUpdate(this, collection);
        }
    }

    public void insertOrUpdate(RealmModel realmModel) {
        checkIfValidAndInTransaction();
        if (realmModel != null) {
            this.configuration.getSchemaMediator().insertOrUpdate(this, realmModel, new HashMap());
            return;
        }
        throw new IllegalArgumentException("Null object cannot be inserted into Realm.");
    }

    public <E extends RealmModel> List<E> copyToRealmOrUpdate(Iterable<E> iterable) {
        if (iterable == null) {
            return new ArrayList(0);
        }
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        for (E e : iterable) {
            checkNotNullObject(e);
            arrayList.add(copyOrUpdate(e, true, hashMap));
        }
        return arrayList;
    }

    public <E extends RealmModel> List<E> copyFromRealm(Iterable<E> iterable) {
        return copyFromRealm(iterable, Integer.MAX_VALUE);
    }

    public <E extends RealmModel> List<E> copyFromRealm(Iterable<E> iterable, int i) {
        checkMaxDepth(i);
        if (iterable == null) {
            return new ArrayList(0);
        }
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        for (E e : iterable) {
            checkValidObjectForDetach(e);
            arrayList.add(createDetachedCopy(e, i, hashMap));
        }
        return arrayList;
    }

    public <E extends RealmModel> E copyFromRealm(E e) {
        return copyFromRealm(e, Integer.MAX_VALUE);
    }

    public <E extends RealmModel> E copyFromRealm(E e, int i) {
        checkMaxDepth(i);
        checkValidObjectForDetach(e);
        return createDetachedCopy(e, i, new HashMap());
    }

    public <E extends RealmModel> RealmQuery<E> where(Class<E> cls) {
        checkIfValid();
        return RealmQuery.createQuery(this, cls);
    }

    public void addChangeListener(RealmChangeListener<Realm> realmChangeListener) {
        addListener(realmChangeListener);
    }

    public void removeChangeListener(RealmChangeListener<Realm> realmChangeListener) {
        removeListener(realmChangeListener);
    }

    public void removeAllChangeListeners() {
        removeAllListeners();
    }

    public void executeTransaction(Transaction transaction) {
        if (transaction != null) {
            beginTransaction();
            try {
                transaction.execute(this);
                commitTransaction();
            } catch (Throwable th) {
                if (isInTransaction()) {
                    cancelTransaction();
                } else {
                    RealmLog.warn("Could not cancel transaction, not currently in a transaction.", new Object[0]);
                }
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Transaction should not be null");
        }
    }

    public RealmAsyncTask executeTransactionAsync(Transaction transaction) {
        return executeTransactionAsync(transaction, null, null);
    }

    public RealmAsyncTask executeTransactionAsync(Transaction transaction, OnSuccess onSuccess) {
        if (onSuccess != null) {
            return executeTransactionAsync(transaction, onSuccess, null);
        }
        throw new IllegalArgumentException("onSuccess callback can't be null");
    }

    public RealmAsyncTask executeTransactionAsync(Transaction transaction, OnError onError) {
        if (onError != null) {
            return executeTransactionAsync(transaction, null, onError);
        }
        throw new IllegalArgumentException("onError callback can't be null");
    }

    public RealmAsyncTask executeTransactionAsync(Transaction transaction, @Nullable OnSuccess onSuccess, @Nullable OnError onError) {
        checkIfValid();
        if (transaction != null) {
            final boolean canDeliverNotification = this.sharedRealm.capabilities.canDeliverNotification();
            if (!(onSuccess == null && onError == null)) {
                this.sharedRealm.capabilities.checkCanDeliverNotification("Callback cannot be delivered on current thread.");
            }
            final RealmConfiguration configuration = getConfiguration();
            final RealmNotifier realmNotifier = this.sharedRealm.realmNotifier;
            RealmThreadPoolExecutor realmThreadPoolExecutor = asyncTaskExecutor;
            final Transaction transaction2 = transaction;
            final OnSuccess onSuccess2 = onSuccess;
            final OnError onError2 = onError;
            AnonymousClass1 r1 = new Runnable() {
                /* JADX INFO: finally extract failed */
                public void run() {
                    final VersionID versionID;
                    if (!Thread.currentThread().isInterrupted()) {
                        Realm instance = Realm.getInstance(configuration);
                        instance.beginTransaction();
                        final Throwable th = null;
                        try {
                            transaction2.execute(instance);
                            if (Thread.currentThread().isInterrupted()) {
                                try {
                                    if (instance.isInTransaction()) {
                                        instance.cancelTransaction();
                                    }
                                } finally {
                                    instance.close();
                                }
                            } else {
                                instance.commitTransaction();
                                versionID = instance.sharedRealm.getVersionID();
                                try {
                                    if (instance.isInTransaction()) {
                                        instance.cancelTransaction();
                                    }
                                    if (canDeliverNotification) {
                                        if (versionID != null && onSuccess2 != null) {
                                            realmNotifier.post(new Runnable() {
                                                public void run() {
                                                    if (Realm.this.isClosed()) {
                                                        onSuccess2.onSuccess();
                                                        return;
                                                    }
                                                    if (Realm.this.sharedRealm.getVersionID().compareTo(versionID) < 0) {
                                                        Realm.this.sharedRealm.realmNotifier.addTransactionCallback(new Runnable() {
                                                            public void run() {
                                                                onSuccess2.onSuccess();
                                                            }
                                                        });
                                                    } else {
                                                        onSuccess2.onSuccess();
                                                    }
                                                }
                                            });
                                        } else if (th != null) {
                                            realmNotifier.post(new Runnable() {
                                                public void run() {
                                                    if (onError2 != null) {
                                                        onError2.onError(th);
                                                    } else {
                                                        throw new RealmException("Async transaction failed", th);
                                                    }
                                                }
                                            });
                                        }
                                    } else if (th != null) {
                                        throw new RealmException("Async transaction failed", th);
                                    }
                                } finally {
                                    instance.close();
                                }
                            }
                        } catch (Throwable th2) {
                            instance.close();
                            throw th2;
                        }
                    }
                }
            };
            return new RealmAsyncTaskImpl(realmThreadPoolExecutor.submitTransaction(r1), asyncTaskExecutor);
        }
        throw new IllegalArgumentException("Transaction should not be null");
    }

    public void delete(Class<? extends RealmModel> cls) {
        checkIfValid();
        this.schema.getTable(cls).clear();
    }

    private <E extends RealmModel> E copyOrUpdate(E e, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        checkIfValid();
        return this.configuration.getSchemaMediator().copyOrUpdate(this, e, z, map);
    }

    private <E extends RealmModel> E createDetachedCopy(E e, int i, Map<RealmModel, CacheData<RealmModel>> map) {
        checkIfValid();
        return this.configuration.getSchemaMediator().createDetachedCopy(e, i, map);
    }

    private <E extends RealmModel> void checkNotNullObject(E e) {
        if (e == null) {
            throw new IllegalArgumentException("Null objects cannot be copied into Realm.");
        }
    }

    private void checkHasPrimaryKey(Class<? extends RealmModel> cls) {
        if (this.sharedRealm.getSchemaInfo().getObjectSchemaInfo(this.configuration.getSchemaMediator().getSimpleClassName(cls)).getPrimaryKeyProperty() == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("A RealmObject with no @PrimaryKey cannot be updated: ");
            sb.append(cls.toString());
            throw new IllegalArgumentException(sb.toString());
        }
    }

    private void checkMaxDepth(int i) {
        if (i < 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("maxDepth must be > 0. It was: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    private <E extends RealmModel> void checkValidObjectForDetach(E e) {
        if (e == null) {
            throw new IllegalArgumentException("Null objects cannot be copied from Realm.");
        } else if (!RealmObject.isManaged(e) || !RealmObject.isValid(e)) {
            throw new IllegalArgumentException("Only valid managed objects can be copied from Realm.");
        } else if (e instanceof DynamicRealmObject) {
            throw new IllegalArgumentException("DynamicRealmObject cannot be copied from Realm.");
        }
    }

    public static void migrateRealm(RealmConfiguration realmConfiguration) throws FileNotFoundException {
        migrateRealm(realmConfiguration, null);
    }

    public static void migrateRealm(RealmConfiguration realmConfiguration, @Nullable RealmMigration realmMigration) throws FileNotFoundException {
        BaseRealm.migrateRealm(realmConfiguration, realmMigration);
    }

    public static boolean deleteRealm(RealmConfiguration realmConfiguration) {
        return BaseRealm.deleteRealm(realmConfiguration);
    }

    public static boolean compactRealm(RealmConfiguration realmConfiguration) {
        if (!realmConfiguration.isSyncConfiguration()) {
            return BaseRealm.compactRealm(realmConfiguration);
        }
        throw new UnsupportedOperationException("Compacting is not supported yet on synced Realms. See https://github.com/realm/realm-core/issues/2345");
    }

    public <E extends RealmModel> void subscribeToObjects(final Class<E> cls, String str, final PartialSyncCallback<E> partialSyncCallback) {
        checkIfValid();
        if (this.configuration.isSyncConfiguration()) {
            this.sharedRealm.capabilities.checkCanDeliverNotification("Listeners cannot be used on current thread.");
            this.sharedRealm.registerPartialSyncQuery(str, new io.realm.internal.SharedRealm.PartialSyncCallback(this.configuration.getSchemaMediator().getSimpleClassName(cls)) {
                public void onSuccess(io.realm.internal.Collection collection) {
                    partialSyncCallback.onSuccess(new RealmResults((BaseRealm) Realm.this, collection, cls));
                }

                public void onError(RealmException realmException) {
                    partialSyncCallback.onError(realmException);
                }
            });
            return;
        }
        throw new IllegalStateException("Partial sync is only available for synchronized Realm (Realm Object Server)");
    }

    /* access modifiers changed from: 0000 */
    public Table getTable(Class<? extends RealmModel> cls) {
        return this.schema.getTable(cls);
    }

    @Nullable
    public static Object getDefaultModule() {
        String str = "io.realm.DefaultRealmModule";
        try {
            Constructor constructor = Class.forName(str).getDeclaredConstructors()[0];
            constructor.setAccessible(true);
            return constructor.newInstance(new Object[0]);
        } catch (ClassNotFoundException unused) {
            return null;
        } catch (InvocationTargetException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Could not create an instance of ");
            sb.append(str);
            throw new RealmException(sb.toString(), e);
        } catch (InstantiationException e2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Could not create an instance of ");
            sb2.append(str);
            throw new RealmException(sb2.toString(), e2);
        } catch (IllegalAccessException e3) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Could not create an instance of ");
            sb3.append(str);
            throw new RealmException(sb3.toString(), e3);
        }
    }

    public static int getGlobalInstanceCount(RealmConfiguration realmConfiguration) {
        final AtomicInteger atomicInteger = new AtomicInteger(0);
        RealmCache.invokeWithGlobalRefCount(realmConfiguration, new Callback() {
            public void onResult(int i) {
                atomicInteger.set(i);
            }
        });
        return atomicInteger.get();
    }

    public static int getLocalInstanceCount(RealmConfiguration realmConfiguration) {
        return RealmCache.getLocalThreadCount(realmConfiguration);
    }
}
