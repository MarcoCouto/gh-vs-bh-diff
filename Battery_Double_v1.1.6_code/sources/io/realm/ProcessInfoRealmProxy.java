package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mansoon.BatteryDouble.models.data.AppPermission;
import com.mansoon.BatteryDouble.models.data.AppSignature;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProcessInfoRealmProxy extends ProcessInfo implements RealmObjectProxy, ProcessInfoRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private RealmList<AppPermission> appPermissionsRealmList;
    private RealmList<AppSignature> appSignaturesRealmList;
    private ProcessInfoColumnInfo columnInfo;
    private ProxyState<ProcessInfo> proxyState;

    static final class ProcessInfoColumnInfo extends ColumnInfo {
        long appPermissionsIndex;
        long appSignaturesIndex;
        long applicationLabelIndex;
        long importanceIndex;
        long installationPkgIndex;
        long isSystemAppIndex;
        long nameIndex;
        long processIdIndex;
        long versionCodeIndex;
        long versionNameIndex;

        ProcessInfoColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(10);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("ProcessInfo");
            this.processIdIndex = addColumnDetails("processId", objectSchemaInfo);
            this.nameIndex = addColumnDetails("name", objectSchemaInfo);
            this.applicationLabelIndex = addColumnDetails("applicationLabel", objectSchemaInfo);
            this.isSystemAppIndex = addColumnDetails("isSystemApp", objectSchemaInfo);
            this.importanceIndex = addColumnDetails("importance", objectSchemaInfo);
            this.versionNameIndex = addColumnDetails("versionName", objectSchemaInfo);
            this.versionCodeIndex = addColumnDetails("versionCode", objectSchemaInfo);
            this.installationPkgIndex = addColumnDetails("installationPkg", objectSchemaInfo);
            this.appPermissionsIndex = addColumnDetails("appPermissions", objectSchemaInfo);
            this.appSignaturesIndex = addColumnDetails("appSignatures", objectSchemaInfo);
        }

        ProcessInfoColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new ProcessInfoColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            ProcessInfoColumnInfo processInfoColumnInfo = (ProcessInfoColumnInfo) columnInfo;
            ProcessInfoColumnInfo processInfoColumnInfo2 = (ProcessInfoColumnInfo) columnInfo2;
            processInfoColumnInfo2.processIdIndex = processInfoColumnInfo.processIdIndex;
            processInfoColumnInfo2.nameIndex = processInfoColumnInfo.nameIndex;
            processInfoColumnInfo2.applicationLabelIndex = processInfoColumnInfo.applicationLabelIndex;
            processInfoColumnInfo2.isSystemAppIndex = processInfoColumnInfo.isSystemAppIndex;
            processInfoColumnInfo2.importanceIndex = processInfoColumnInfo.importanceIndex;
            processInfoColumnInfo2.versionNameIndex = processInfoColumnInfo.versionNameIndex;
            processInfoColumnInfo2.versionCodeIndex = processInfoColumnInfo.versionCodeIndex;
            processInfoColumnInfo2.installationPkgIndex = processInfoColumnInfo.installationPkgIndex;
            processInfoColumnInfo2.appPermissionsIndex = processInfoColumnInfo.appPermissionsIndex;
            processInfoColumnInfo2.appSignaturesIndex = processInfoColumnInfo.appSignaturesIndex;
        }
    }

    public static String getTableName() {
        return "class_ProcessInfo";
    }

    static {
        ArrayList arrayList = new ArrayList(10);
        arrayList.add("processId");
        arrayList.add("name");
        arrayList.add("applicationLabel");
        arrayList.add("isSystemApp");
        arrayList.add("importance");
        arrayList.add("versionName");
        arrayList.add("versionCode");
        arrayList.add("installationPkg");
        arrayList.add("appPermissions");
        arrayList.add("appSignatures");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    ProcessInfoRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (ProcessInfoColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public int realmGet$processId() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.processIdIndex);
    }

    public void realmSet$processId(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.processIdIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.processIdIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$name() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.nameIndex);
    }

    public void realmSet$name(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.nameIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.nameIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.nameIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.nameIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$applicationLabel() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.applicationLabelIndex);
    }

    public void realmSet$applicationLabel(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.applicationLabelIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.applicationLabelIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.applicationLabelIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.applicationLabelIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public boolean realmGet$isSystemApp() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getBoolean(this.columnInfo.isSystemAppIndex);
    }

    public void realmSet$isSystemApp(boolean z) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setBoolean(this.columnInfo.isSystemAppIndex, z);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setBoolean(this.columnInfo.isSystemAppIndex, row$realm.getIndex(), z, true);
        }
    }

    public String realmGet$importance() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.importanceIndex);
    }

    public void realmSet$importance(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.importanceIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.importanceIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.importanceIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.importanceIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public String realmGet$versionName() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.versionNameIndex);
    }

    public void realmSet$versionName(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.versionNameIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.versionNameIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.versionNameIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.versionNameIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public int realmGet$versionCode() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.versionCodeIndex);
    }

    public void realmSet$versionCode(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.versionCodeIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.versionCodeIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$installationPkg() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.installationPkgIndex);
    }

    public void realmSet$installationPkg(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.installationPkgIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.installationPkgIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.installationPkgIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.installationPkgIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    public RealmList<AppPermission> realmGet$appPermissions() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.appPermissionsRealmList != null) {
            return this.appPermissionsRealmList;
        }
        this.appPermissionsRealmList = new RealmList<>(AppPermission.class, this.proxyState.getRow$realm().getModelList(this.columnInfo.appPermissionsIndex), this.proxyState.getRealm$realm());
        return this.appPermissionsRealmList;
    }

    public void realmSet$appPermissions(RealmList<AppPermission> realmList) {
        if (this.proxyState.isUnderConstruction()) {
            if (!this.proxyState.getAcceptDefaultValue$realm() || this.proxyState.getExcludeFields$realm().contains("appPermissions")) {
                return;
            }
            if (realmList != null && !realmList.isManaged()) {
                Realm realm = (Realm) this.proxyState.getRealm$realm();
                RealmList<AppPermission> realmList2 = new RealmList<>();
                Iterator it = realmList.iterator();
                while (it.hasNext()) {
                    AppPermission appPermission = (AppPermission) it.next();
                    if (appPermission == null || RealmObject.isManaged(appPermission)) {
                        realmList2.add(appPermission);
                    } else {
                        realmList2.add(realm.copyToRealm(appPermission));
                    }
                }
                realmList = realmList2;
            }
        }
        this.proxyState.getRealm$realm().checkIfValid();
        OsList modelList = this.proxyState.getRow$realm().getModelList(this.columnInfo.appPermissionsIndex);
        int i = 0;
        if (realmList == null || ((long) realmList.size()) != modelList.size()) {
            modelList.removeAll();
            if (realmList != null) {
                int size = realmList.size();
                while (i < size) {
                    AppPermission appPermission2 = (AppPermission) realmList.get(i);
                    this.proxyState.checkValidObject(appPermission2);
                    modelList.addRow(((RealmObjectProxy) appPermission2).realmGet$proxyState().getRow$realm().getIndex());
                    i++;
                }
            }
        } else {
            int size2 = realmList.size();
            while (i < size2) {
                AppPermission appPermission3 = (AppPermission) realmList.get(i);
                this.proxyState.checkValidObject(appPermission3);
                modelList.setRow((long) i, ((RealmObjectProxy) appPermission3).realmGet$proxyState().getRow$realm().getIndex());
                i++;
            }
        }
    }

    public RealmList<AppSignature> realmGet$appSignatures() {
        this.proxyState.getRealm$realm().checkIfValid();
        if (this.appSignaturesRealmList != null) {
            return this.appSignaturesRealmList;
        }
        this.appSignaturesRealmList = new RealmList<>(AppSignature.class, this.proxyState.getRow$realm().getModelList(this.columnInfo.appSignaturesIndex), this.proxyState.getRealm$realm());
        return this.appSignaturesRealmList;
    }

    public void realmSet$appSignatures(RealmList<AppSignature> realmList) {
        if (this.proxyState.isUnderConstruction()) {
            if (!this.proxyState.getAcceptDefaultValue$realm() || this.proxyState.getExcludeFields$realm().contains("appSignatures")) {
                return;
            }
            if (realmList != null && !realmList.isManaged()) {
                Realm realm = (Realm) this.proxyState.getRealm$realm();
                RealmList<AppSignature> realmList2 = new RealmList<>();
                Iterator it = realmList.iterator();
                while (it.hasNext()) {
                    AppSignature appSignature = (AppSignature) it.next();
                    if (appSignature == null || RealmObject.isManaged(appSignature)) {
                        realmList2.add(appSignature);
                    } else {
                        realmList2.add(realm.copyToRealm(appSignature));
                    }
                }
                realmList = realmList2;
            }
        }
        this.proxyState.getRealm$realm().checkIfValid();
        OsList modelList = this.proxyState.getRow$realm().getModelList(this.columnInfo.appSignaturesIndex);
        int i = 0;
        if (realmList == null || ((long) realmList.size()) != modelList.size()) {
            modelList.removeAll();
            if (realmList != null) {
                int size = realmList.size();
                while (i < size) {
                    AppSignature appSignature2 = (AppSignature) realmList.get(i);
                    this.proxyState.checkValidObject(appSignature2);
                    modelList.addRow(((RealmObjectProxy) appSignature2).realmGet$proxyState().getRow$realm().getIndex());
                    i++;
                }
            }
        } else {
            int size2 = realmList.size();
            while (i < size2) {
                AppSignature appSignature3 = (AppSignature) realmList.get(i);
                this.proxyState.checkValidObject(appSignature3);
                modelList.setRow((long) i, ((RealmObjectProxy) appSignature3).realmGet$proxyState().getRow$realm().getIndex());
                i++;
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("ProcessInfo", 10, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("processId", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("name", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("applicationLabel", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("isSystemApp", RealmFieldType.BOOLEAN, false, false, true);
        builder2.addPersistedProperty("importance", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("versionName", RealmFieldType.STRING, false, false, false);
        builder2.addPersistedProperty("versionCode", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("installationPkg", RealmFieldType.STRING, false, false, false);
        builder.addPersistedLinkProperty("appPermissions", RealmFieldType.LIST, "AppPermission");
        builder.addPersistedLinkProperty("appSignatures", RealmFieldType.LIST, "AppSignature");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ProcessInfoColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new ProcessInfoColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static ProcessInfo createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        ArrayList arrayList = new ArrayList(2);
        if (jSONObject.has("appPermissions")) {
            arrayList.add("appPermissions");
        }
        if (jSONObject.has("appSignatures")) {
            arrayList.add("appSignatures");
        }
        ProcessInfo processInfo = (ProcessInfo) realm.createObjectInternal(ProcessInfo.class, true, arrayList);
        ProcessInfoRealmProxyInterface processInfoRealmProxyInterface = processInfo;
        if (jSONObject.has("processId")) {
            if (!jSONObject.isNull("processId")) {
                processInfoRealmProxyInterface.realmSet$processId(jSONObject.getInt("processId"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'processId' to null.");
            }
        }
        if (jSONObject.has("name")) {
            if (jSONObject.isNull("name")) {
                processInfoRealmProxyInterface.realmSet$name(null);
            } else {
                processInfoRealmProxyInterface.realmSet$name(jSONObject.getString("name"));
            }
        }
        if (jSONObject.has("applicationLabel")) {
            if (jSONObject.isNull("applicationLabel")) {
                processInfoRealmProxyInterface.realmSet$applicationLabel(null);
            } else {
                processInfoRealmProxyInterface.realmSet$applicationLabel(jSONObject.getString("applicationLabel"));
            }
        }
        if (jSONObject.has("isSystemApp")) {
            if (!jSONObject.isNull("isSystemApp")) {
                processInfoRealmProxyInterface.realmSet$isSystemApp(jSONObject.getBoolean("isSystemApp"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isSystemApp' to null.");
            }
        }
        if (jSONObject.has("importance")) {
            if (jSONObject.isNull("importance")) {
                processInfoRealmProxyInterface.realmSet$importance(null);
            } else {
                processInfoRealmProxyInterface.realmSet$importance(jSONObject.getString("importance"));
            }
        }
        if (jSONObject.has("versionName")) {
            if (jSONObject.isNull("versionName")) {
                processInfoRealmProxyInterface.realmSet$versionName(null);
            } else {
                processInfoRealmProxyInterface.realmSet$versionName(jSONObject.getString("versionName"));
            }
        }
        if (jSONObject.has("versionCode")) {
            if (!jSONObject.isNull("versionCode")) {
                processInfoRealmProxyInterface.realmSet$versionCode(jSONObject.getInt("versionCode"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'versionCode' to null.");
            }
        }
        if (jSONObject.has("installationPkg")) {
            if (jSONObject.isNull("installationPkg")) {
                processInfoRealmProxyInterface.realmSet$installationPkg(null);
            } else {
                processInfoRealmProxyInterface.realmSet$installationPkg(jSONObject.getString("installationPkg"));
            }
        }
        if (jSONObject.has("appPermissions")) {
            if (jSONObject.isNull("appPermissions")) {
                processInfoRealmProxyInterface.realmSet$appPermissions(null);
            } else {
                processInfoRealmProxyInterface.realmGet$appPermissions().clear();
                JSONArray jSONArray = jSONObject.getJSONArray("appPermissions");
                for (int i = 0; i < jSONArray.length(); i++) {
                    processInfoRealmProxyInterface.realmGet$appPermissions().add(AppPermissionRealmProxy.createOrUpdateUsingJsonObject(realm, jSONArray.getJSONObject(i), z));
                }
            }
        }
        if (jSONObject.has("appSignatures")) {
            if (jSONObject.isNull("appSignatures")) {
                processInfoRealmProxyInterface.realmSet$appSignatures(null);
            } else {
                processInfoRealmProxyInterface.realmGet$appSignatures().clear();
                JSONArray jSONArray2 = jSONObject.getJSONArray("appSignatures");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    processInfoRealmProxyInterface.realmGet$appSignatures().add(AppSignatureRealmProxy.createOrUpdateUsingJsonObject(realm, jSONArray2.getJSONObject(i2), z));
                }
            }
        }
        return processInfo;
    }

    @TargetApi(11)
    public static ProcessInfo createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        ProcessInfo processInfo = new ProcessInfo();
        ProcessInfoRealmProxyInterface processInfoRealmProxyInterface = processInfo;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("processId")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    processInfoRealmProxyInterface.realmSet$processId(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'processId' to null.");
                }
            } else if (nextName.equals("name")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    processInfoRealmProxyInterface.realmSet$name(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    processInfoRealmProxyInterface.realmSet$name(null);
                }
            } else if (nextName.equals("applicationLabel")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    processInfoRealmProxyInterface.realmSet$applicationLabel(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    processInfoRealmProxyInterface.realmSet$applicationLabel(null);
                }
            } else if (nextName.equals("isSystemApp")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    processInfoRealmProxyInterface.realmSet$isSystemApp(jsonReader.nextBoolean());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isSystemApp' to null.");
                }
            } else if (nextName.equals("importance")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    processInfoRealmProxyInterface.realmSet$importance(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    processInfoRealmProxyInterface.realmSet$importance(null);
                }
            } else if (nextName.equals("versionName")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    processInfoRealmProxyInterface.realmSet$versionName(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    processInfoRealmProxyInterface.realmSet$versionName(null);
                }
            } else if (nextName.equals("versionCode")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    processInfoRealmProxyInterface.realmSet$versionCode(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'versionCode' to null.");
                }
            } else if (nextName.equals("installationPkg")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    processInfoRealmProxyInterface.realmSet$installationPkg(jsonReader.nextString());
                } else {
                    jsonReader.skipValue();
                    processInfoRealmProxyInterface.realmSet$installationPkg(null);
                }
            } else if (nextName.equals("appPermissions")) {
                if (jsonReader.peek() == JsonToken.NULL) {
                    jsonReader.skipValue();
                    processInfoRealmProxyInterface.realmSet$appPermissions(null);
                } else {
                    processInfoRealmProxyInterface.realmSet$appPermissions(new RealmList());
                    jsonReader.beginArray();
                    while (jsonReader.hasNext()) {
                        processInfoRealmProxyInterface.realmGet$appPermissions().add(AppPermissionRealmProxy.createUsingJsonStream(realm, jsonReader));
                    }
                    jsonReader.endArray();
                }
            } else if (!nextName.equals("appSignatures")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.skipValue();
                processInfoRealmProxyInterface.realmSet$appSignatures(null);
            } else {
                processInfoRealmProxyInterface.realmSet$appSignatures(new RealmList());
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    processInfoRealmProxyInterface.realmGet$appSignatures().add(AppSignatureRealmProxy.createUsingJsonStream(realm, jsonReader));
                }
                jsonReader.endArray();
            }
        }
        jsonReader.endObject();
        return (ProcessInfo) realm.copyToRealm(processInfo);
    }

    public static ProcessInfo copyOrUpdate(Realm realm, ProcessInfo processInfo, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (processInfo instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) processInfo;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return processInfo;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(processInfo);
        if (realmObjectProxy2 != null) {
            return (ProcessInfo) realmObjectProxy2;
        }
        return copy(realm, processInfo, z, map);
    }

    public static ProcessInfo copy(Realm realm, ProcessInfo processInfo, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(processInfo);
        if (realmObjectProxy != null) {
            return (ProcessInfo) realmObjectProxy;
        }
        ProcessInfo processInfo2 = (ProcessInfo) realm.createObjectInternal(ProcessInfo.class, false, Collections.emptyList());
        map.put(processInfo, (RealmObjectProxy) processInfo2);
        ProcessInfoRealmProxyInterface processInfoRealmProxyInterface = processInfo;
        ProcessInfoRealmProxyInterface processInfoRealmProxyInterface2 = processInfo2;
        processInfoRealmProxyInterface2.realmSet$processId(processInfoRealmProxyInterface.realmGet$processId());
        processInfoRealmProxyInterface2.realmSet$name(processInfoRealmProxyInterface.realmGet$name());
        processInfoRealmProxyInterface2.realmSet$applicationLabel(processInfoRealmProxyInterface.realmGet$applicationLabel());
        processInfoRealmProxyInterface2.realmSet$isSystemApp(processInfoRealmProxyInterface.realmGet$isSystemApp());
        processInfoRealmProxyInterface2.realmSet$importance(processInfoRealmProxyInterface.realmGet$importance());
        processInfoRealmProxyInterface2.realmSet$versionName(processInfoRealmProxyInterface.realmGet$versionName());
        processInfoRealmProxyInterface2.realmSet$versionCode(processInfoRealmProxyInterface.realmGet$versionCode());
        processInfoRealmProxyInterface2.realmSet$installationPkg(processInfoRealmProxyInterface.realmGet$installationPkg());
        RealmList realmGet$appPermissions = processInfoRealmProxyInterface.realmGet$appPermissions();
        if (realmGet$appPermissions != null) {
            RealmList realmGet$appPermissions2 = processInfoRealmProxyInterface2.realmGet$appPermissions();
            realmGet$appPermissions2.clear();
            for (int i = 0; i < realmGet$appPermissions.size(); i++) {
                AppPermission appPermission = (AppPermission) realmGet$appPermissions.get(i);
                AppPermission appPermission2 = (AppPermission) map.get(appPermission);
                if (appPermission2 != null) {
                    realmGet$appPermissions2.add(appPermission2);
                } else {
                    realmGet$appPermissions2.add(AppPermissionRealmProxy.copyOrUpdate(realm, appPermission, z, map));
                }
            }
        }
        RealmList realmGet$appSignatures = processInfoRealmProxyInterface.realmGet$appSignatures();
        if (realmGet$appSignatures != null) {
            RealmList realmGet$appSignatures2 = processInfoRealmProxyInterface2.realmGet$appSignatures();
            realmGet$appSignatures2.clear();
            for (int i2 = 0; i2 < realmGet$appSignatures.size(); i2++) {
                AppSignature appSignature = (AppSignature) realmGet$appSignatures.get(i2);
                AppSignature appSignature2 = (AppSignature) map.get(appSignature);
                if (appSignature2 != null) {
                    realmGet$appSignatures2.add(appSignature2);
                } else {
                    realmGet$appSignatures2.add(AppSignatureRealmProxy.copyOrUpdate(realm, appSignature, z, map));
                }
            }
        }
        return processInfo2;
    }

    public static long insert(Realm realm, ProcessInfo processInfo, Map<RealmModel, Long> map) {
        long j;
        Realm realm2 = realm;
        ProcessInfo processInfo2 = processInfo;
        Map<RealmModel, Long> map2 = map;
        if (processInfo2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) processInfo2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm2.getTable(ProcessInfo.class);
        long nativePtr = table.getNativePtr();
        ProcessInfoColumnInfo processInfoColumnInfo = (ProcessInfoColumnInfo) realm.getSchema().getColumnInfo(ProcessInfo.class);
        long createRow = OsObject.createRow(table);
        map2.put(processInfo2, Long.valueOf(createRow));
        ProcessInfoRealmProxyInterface processInfoRealmProxyInterface = processInfo2;
        long j2 = createRow;
        Table.nativeSetLong(nativePtr, processInfoColumnInfo.processIdIndex, createRow, (long) processInfoRealmProxyInterface.realmGet$processId(), false);
        String realmGet$name = processInfoRealmProxyInterface.realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.nameIndex, j2, realmGet$name, false);
        }
        String realmGet$applicationLabel = processInfoRealmProxyInterface.realmGet$applicationLabel();
        if (realmGet$applicationLabel != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.applicationLabelIndex, j2, realmGet$applicationLabel, false);
        }
        Table.nativeSetBoolean(nativePtr, processInfoColumnInfo.isSystemAppIndex, j2, processInfoRealmProxyInterface.realmGet$isSystemApp(), false);
        String realmGet$importance = processInfoRealmProxyInterface.realmGet$importance();
        if (realmGet$importance != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.importanceIndex, j2, realmGet$importance, false);
        }
        String realmGet$versionName = processInfoRealmProxyInterface.realmGet$versionName();
        if (realmGet$versionName != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.versionNameIndex, j2, realmGet$versionName, false);
        }
        Table.nativeSetLong(nativePtr, processInfoColumnInfo.versionCodeIndex, j2, (long) processInfoRealmProxyInterface.realmGet$versionCode(), false);
        String realmGet$installationPkg = processInfoRealmProxyInterface.realmGet$installationPkg();
        if (realmGet$installationPkg != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.installationPkgIndex, j2, realmGet$installationPkg, false);
        }
        RealmList realmGet$appPermissions = processInfoRealmProxyInterface.realmGet$appPermissions();
        if (realmGet$appPermissions != null) {
            j = j2;
            OsList osList = new OsList(table.getUncheckedRow(j), processInfoColumnInfo.appPermissionsIndex);
            Iterator it = realmGet$appPermissions.iterator();
            while (it.hasNext()) {
                AppPermission appPermission = (AppPermission) it.next();
                Long l = (Long) map2.get(appPermission);
                if (l == null) {
                    l = Long.valueOf(AppPermissionRealmProxy.insert(realm2, appPermission, map2));
                }
                osList.addRow(l.longValue());
            }
        } else {
            j = j2;
        }
        RealmList realmGet$appSignatures = processInfoRealmProxyInterface.realmGet$appSignatures();
        if (realmGet$appSignatures != null) {
            OsList osList2 = new OsList(table.getUncheckedRow(j), processInfoColumnInfo.appSignaturesIndex);
            Iterator it2 = realmGet$appSignatures.iterator();
            while (it2.hasNext()) {
                AppSignature appSignature = (AppSignature) it2.next();
                Long l2 = (Long) map2.get(appSignature);
                if (l2 == null) {
                    l2 = Long.valueOf(AppSignatureRealmProxy.insert(realm2, appSignature, map2));
                }
                osList2.addRow(l2.longValue());
            }
        }
        return j;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Realm realm2 = realm;
        Map<RealmModel, Long> map2 = map;
        Table table = realm2.getTable(ProcessInfo.class);
        long nativePtr = table.getNativePtr();
        ProcessInfoColumnInfo processInfoColumnInfo = (ProcessInfoColumnInfo) realm.getSchema().getColumnInfo(ProcessInfo.class);
        while (it.hasNext()) {
            ProcessInfo processInfo = (ProcessInfo) it.next();
            if (!map2.containsKey(processInfo)) {
                if (processInfo instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) processInfo;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(processInfo, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(processInfo, Long.valueOf(createRow));
                ProcessInfoRealmProxyInterface processInfoRealmProxyInterface = processInfo;
                long j2 = createRow;
                Table.nativeSetLong(nativePtr, processInfoColumnInfo.processIdIndex, createRow, (long) processInfoRealmProxyInterface.realmGet$processId(), false);
                String realmGet$name = processInfoRealmProxyInterface.realmGet$name();
                if (realmGet$name != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.nameIndex, j2, realmGet$name, false);
                }
                String realmGet$applicationLabel = processInfoRealmProxyInterface.realmGet$applicationLabel();
                if (realmGet$applicationLabel != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.applicationLabelIndex, j2, realmGet$applicationLabel, false);
                }
                Table.nativeSetBoolean(nativePtr, processInfoColumnInfo.isSystemAppIndex, j2, processInfoRealmProxyInterface.realmGet$isSystemApp(), false);
                String realmGet$importance = processInfoRealmProxyInterface.realmGet$importance();
                if (realmGet$importance != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.importanceIndex, j2, realmGet$importance, false);
                }
                String realmGet$versionName = processInfoRealmProxyInterface.realmGet$versionName();
                if (realmGet$versionName != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.versionNameIndex, j2, realmGet$versionName, false);
                }
                Table.nativeSetLong(nativePtr, processInfoColumnInfo.versionCodeIndex, j2, (long) processInfoRealmProxyInterface.realmGet$versionCode(), false);
                String realmGet$installationPkg = processInfoRealmProxyInterface.realmGet$installationPkg();
                if (realmGet$installationPkg != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.installationPkgIndex, j2, realmGet$installationPkg, false);
                }
                RealmList realmGet$appPermissions = processInfoRealmProxyInterface.realmGet$appPermissions();
                if (realmGet$appPermissions != null) {
                    j = j2;
                    OsList osList = new OsList(table.getUncheckedRow(j), processInfoColumnInfo.appPermissionsIndex);
                    Iterator it2 = realmGet$appPermissions.iterator();
                    while (it2.hasNext()) {
                        AppPermission appPermission = (AppPermission) it2.next();
                        Long l = (Long) map2.get(appPermission);
                        if (l == null) {
                            l = Long.valueOf(AppPermissionRealmProxy.insert(realm2, appPermission, map2));
                        }
                        osList.addRow(l.longValue());
                    }
                } else {
                    j = j2;
                }
                RealmList realmGet$appSignatures = processInfoRealmProxyInterface.realmGet$appSignatures();
                if (realmGet$appSignatures != null) {
                    OsList osList2 = new OsList(table.getUncheckedRow(j), processInfoColumnInfo.appSignaturesIndex);
                    Iterator it3 = realmGet$appSignatures.iterator();
                    while (it3.hasNext()) {
                        AppSignature appSignature = (AppSignature) it3.next();
                        Long l2 = (Long) map2.get(appSignature);
                        if (l2 == null) {
                            l2 = Long.valueOf(AppSignatureRealmProxy.insert(realm2, appSignature, map2));
                        }
                        osList2.addRow(l2.longValue());
                    }
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, ProcessInfo processInfo, Map<RealmModel, Long> map) {
        Realm realm2 = realm;
        ProcessInfo processInfo2 = processInfo;
        Map<RealmModel, Long> map2 = map;
        if (processInfo2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) processInfo2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm2.getTable(ProcessInfo.class);
        long nativePtr = table.getNativePtr();
        ProcessInfoColumnInfo processInfoColumnInfo = (ProcessInfoColumnInfo) realm.getSchema().getColumnInfo(ProcessInfo.class);
        long createRow = OsObject.createRow(table);
        map2.put(processInfo2, Long.valueOf(createRow));
        ProcessInfoRealmProxyInterface processInfoRealmProxyInterface = processInfo2;
        long j = createRow;
        Table.nativeSetLong(nativePtr, processInfoColumnInfo.processIdIndex, createRow, (long) processInfoRealmProxyInterface.realmGet$processId(), false);
        String realmGet$name = processInfoRealmProxyInterface.realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.nameIndex, j, realmGet$name, false);
        } else {
            Table.nativeSetNull(nativePtr, processInfoColumnInfo.nameIndex, j, false);
        }
        String realmGet$applicationLabel = processInfoRealmProxyInterface.realmGet$applicationLabel();
        if (realmGet$applicationLabel != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.applicationLabelIndex, j, realmGet$applicationLabel, false);
        } else {
            Table.nativeSetNull(nativePtr, processInfoColumnInfo.applicationLabelIndex, j, false);
        }
        Table.nativeSetBoolean(nativePtr, processInfoColumnInfo.isSystemAppIndex, j, processInfoRealmProxyInterface.realmGet$isSystemApp(), false);
        String realmGet$importance = processInfoRealmProxyInterface.realmGet$importance();
        if (realmGet$importance != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.importanceIndex, j, realmGet$importance, false);
        } else {
            Table.nativeSetNull(nativePtr, processInfoColumnInfo.importanceIndex, j, false);
        }
        String realmGet$versionName = processInfoRealmProxyInterface.realmGet$versionName();
        if (realmGet$versionName != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.versionNameIndex, j, realmGet$versionName, false);
        } else {
            Table.nativeSetNull(nativePtr, processInfoColumnInfo.versionNameIndex, j, false);
        }
        Table.nativeSetLong(nativePtr, processInfoColumnInfo.versionCodeIndex, j, (long) processInfoRealmProxyInterface.realmGet$versionCode(), false);
        String realmGet$installationPkg = processInfoRealmProxyInterface.realmGet$installationPkg();
        if (realmGet$installationPkg != null) {
            Table.nativeSetString(nativePtr, processInfoColumnInfo.installationPkgIndex, j, realmGet$installationPkg, false);
        } else {
            Table.nativeSetNull(nativePtr, processInfoColumnInfo.installationPkgIndex, j, false);
        }
        long j2 = j;
        OsList osList = new OsList(table.getUncheckedRow(j2), processInfoColumnInfo.appPermissionsIndex);
        RealmList realmGet$appPermissions = processInfoRealmProxyInterface.realmGet$appPermissions();
        if (realmGet$appPermissions == null || ((long) realmGet$appPermissions.size()) != osList.size()) {
            osList.removeAll();
            if (realmGet$appPermissions != null) {
                Iterator it = realmGet$appPermissions.iterator();
                while (it.hasNext()) {
                    AppPermission appPermission = (AppPermission) it.next();
                    Long l = (Long) map2.get(appPermission);
                    if (l == null) {
                        l = Long.valueOf(AppPermissionRealmProxy.insertOrUpdate(realm2, appPermission, map2));
                    }
                    osList.addRow(l.longValue());
                }
            }
        } else {
            int size = realmGet$appPermissions.size();
            for (int i = 0; i < size; i++) {
                AppPermission appPermission2 = (AppPermission) realmGet$appPermissions.get(i);
                Long l2 = (Long) map2.get(appPermission2);
                if (l2 == null) {
                    l2 = Long.valueOf(AppPermissionRealmProxy.insertOrUpdate(realm2, appPermission2, map2));
                }
                osList.setRow((long) i, l2.longValue());
            }
        }
        OsList osList2 = new OsList(table.getUncheckedRow(j2), processInfoColumnInfo.appSignaturesIndex);
        RealmList realmGet$appSignatures = processInfoRealmProxyInterface.realmGet$appSignatures();
        if (realmGet$appSignatures == null || ((long) realmGet$appSignatures.size()) != osList2.size()) {
            osList2.removeAll();
            if (realmGet$appSignatures != null) {
                Iterator it2 = realmGet$appSignatures.iterator();
                while (it2.hasNext()) {
                    AppSignature appSignature = (AppSignature) it2.next();
                    Long l3 = (Long) map2.get(appSignature);
                    if (l3 == null) {
                        l3 = Long.valueOf(AppSignatureRealmProxy.insertOrUpdate(realm2, appSignature, map2));
                    }
                    osList2.addRow(l3.longValue());
                }
            }
        } else {
            int size2 = realmGet$appSignatures.size();
            for (int i2 = 0; i2 < size2; i2++) {
                AppSignature appSignature2 = (AppSignature) realmGet$appSignatures.get(i2);
                Long l4 = (Long) map2.get(appSignature2);
                if (l4 == null) {
                    l4 = Long.valueOf(AppSignatureRealmProxy.insertOrUpdate(realm2, appSignature2, map2));
                }
                osList2.setRow((long) i2, l4.longValue());
            }
        }
        return j2;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Realm realm2 = realm;
        Map<RealmModel, Long> map2 = map;
        Table table = realm2.getTable(ProcessInfo.class);
        long nativePtr = table.getNativePtr();
        ProcessInfoColumnInfo processInfoColumnInfo = (ProcessInfoColumnInfo) realm.getSchema().getColumnInfo(ProcessInfo.class);
        while (it.hasNext()) {
            ProcessInfo processInfo = (ProcessInfo) it.next();
            if (!map2.containsKey(processInfo)) {
                if (processInfo instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) processInfo;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(processInfo, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(processInfo, Long.valueOf(createRow));
                ProcessInfoRealmProxyInterface processInfoRealmProxyInterface = processInfo;
                long j = createRow;
                Table.nativeSetLong(nativePtr, processInfoColumnInfo.processIdIndex, createRow, (long) processInfoRealmProxyInterface.realmGet$processId(), false);
                String realmGet$name = processInfoRealmProxyInterface.realmGet$name();
                if (realmGet$name != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.nameIndex, j, realmGet$name, false);
                } else {
                    Table.nativeSetNull(nativePtr, processInfoColumnInfo.nameIndex, j, false);
                }
                String realmGet$applicationLabel = processInfoRealmProxyInterface.realmGet$applicationLabel();
                if (realmGet$applicationLabel != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.applicationLabelIndex, j, realmGet$applicationLabel, false);
                } else {
                    Table.nativeSetNull(nativePtr, processInfoColumnInfo.applicationLabelIndex, j, false);
                }
                Table.nativeSetBoolean(nativePtr, processInfoColumnInfo.isSystemAppIndex, j, processInfoRealmProxyInterface.realmGet$isSystemApp(), false);
                String realmGet$importance = processInfoRealmProxyInterface.realmGet$importance();
                if (realmGet$importance != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.importanceIndex, j, realmGet$importance, false);
                } else {
                    Table.nativeSetNull(nativePtr, processInfoColumnInfo.importanceIndex, j, false);
                }
                String realmGet$versionName = processInfoRealmProxyInterface.realmGet$versionName();
                if (realmGet$versionName != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.versionNameIndex, j, realmGet$versionName, false);
                } else {
                    Table.nativeSetNull(nativePtr, processInfoColumnInfo.versionNameIndex, j, false);
                }
                Table.nativeSetLong(nativePtr, processInfoColumnInfo.versionCodeIndex, j, (long) processInfoRealmProxyInterface.realmGet$versionCode(), false);
                String realmGet$installationPkg = processInfoRealmProxyInterface.realmGet$installationPkg();
                if (realmGet$installationPkg != null) {
                    Table.nativeSetString(nativePtr, processInfoColumnInfo.installationPkgIndex, j, realmGet$installationPkg, false);
                } else {
                    Table.nativeSetNull(nativePtr, processInfoColumnInfo.installationPkgIndex, j, false);
                }
                long j2 = j;
                OsList osList = new OsList(table.getUncheckedRow(j2), processInfoColumnInfo.appPermissionsIndex);
                RealmList realmGet$appPermissions = processInfoRealmProxyInterface.realmGet$appPermissions();
                if (realmGet$appPermissions == null || ((long) realmGet$appPermissions.size()) != osList.size()) {
                    osList.removeAll();
                    if (realmGet$appPermissions != null) {
                        Iterator it2 = realmGet$appPermissions.iterator();
                        while (it2.hasNext()) {
                            AppPermission appPermission = (AppPermission) it2.next();
                            Long l = (Long) map2.get(appPermission);
                            if (l == null) {
                                l = Long.valueOf(AppPermissionRealmProxy.insertOrUpdate(realm2, appPermission, map2));
                            }
                            osList.addRow(l.longValue());
                        }
                    }
                } else {
                    int size = realmGet$appPermissions.size();
                    int i = 0;
                    while (i < size) {
                        AppPermission appPermission2 = (AppPermission) realmGet$appPermissions.get(i);
                        Long l2 = (Long) map2.get(appPermission2);
                        if (l2 == null) {
                            l2 = Long.valueOf(AppPermissionRealmProxy.insertOrUpdate(realm2, appPermission2, map2));
                        }
                        int i2 = size;
                        osList.setRow((long) i, l2.longValue());
                        i++;
                        size = i2;
                    }
                }
                OsList osList2 = new OsList(table.getUncheckedRow(j2), processInfoColumnInfo.appSignaturesIndex);
                RealmList realmGet$appSignatures = processInfoRealmProxyInterface.realmGet$appSignatures();
                if (realmGet$appSignatures == null || ((long) realmGet$appSignatures.size()) != osList2.size()) {
                    osList2.removeAll();
                    if (realmGet$appSignatures != null) {
                        Iterator it3 = realmGet$appSignatures.iterator();
                        while (it3.hasNext()) {
                            AppSignature appSignature = (AppSignature) it3.next();
                            Long l3 = (Long) map2.get(appSignature);
                            if (l3 == null) {
                                l3 = Long.valueOf(AppSignatureRealmProxy.insertOrUpdate(realm2, appSignature, map2));
                            }
                            osList2.addRow(l3.longValue());
                        }
                    }
                } else {
                    int size2 = realmGet$appSignatures.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        AppSignature appSignature2 = (AppSignature) realmGet$appSignatures.get(i3);
                        Long l4 = (Long) map2.get(appSignature2);
                        if (l4 == null) {
                            l4 = Long.valueOf(AppSignatureRealmProxy.insertOrUpdate(realm2, appSignature2, map2));
                        }
                        osList2.setRow((long) i3, l4.longValue());
                    }
                }
            }
        }
    }

    public static ProcessInfo createDetachedCopy(ProcessInfo processInfo, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        ProcessInfo processInfo2;
        if (i > i2 || processInfo == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(processInfo);
        if (cacheData == null) {
            processInfo2 = new ProcessInfo();
            map.put(processInfo, new CacheData(i, processInfo2));
        } else if (i >= cacheData.minDepth) {
            return (ProcessInfo) cacheData.object;
        } else {
            ProcessInfo processInfo3 = (ProcessInfo) cacheData.object;
            cacheData.minDepth = i;
            processInfo2 = processInfo3;
        }
        ProcessInfoRealmProxyInterface processInfoRealmProxyInterface = processInfo2;
        ProcessInfoRealmProxyInterface processInfoRealmProxyInterface2 = processInfo;
        processInfoRealmProxyInterface.realmSet$processId(processInfoRealmProxyInterface2.realmGet$processId());
        processInfoRealmProxyInterface.realmSet$name(processInfoRealmProxyInterface2.realmGet$name());
        processInfoRealmProxyInterface.realmSet$applicationLabel(processInfoRealmProxyInterface2.realmGet$applicationLabel());
        processInfoRealmProxyInterface.realmSet$isSystemApp(processInfoRealmProxyInterface2.realmGet$isSystemApp());
        processInfoRealmProxyInterface.realmSet$importance(processInfoRealmProxyInterface2.realmGet$importance());
        processInfoRealmProxyInterface.realmSet$versionName(processInfoRealmProxyInterface2.realmGet$versionName());
        processInfoRealmProxyInterface.realmSet$versionCode(processInfoRealmProxyInterface2.realmGet$versionCode());
        processInfoRealmProxyInterface.realmSet$installationPkg(processInfoRealmProxyInterface2.realmGet$installationPkg());
        if (i == i2) {
            processInfoRealmProxyInterface.realmSet$appPermissions(null);
        } else {
            RealmList realmGet$appPermissions = processInfoRealmProxyInterface2.realmGet$appPermissions();
            RealmList realmList = new RealmList();
            processInfoRealmProxyInterface.realmSet$appPermissions(realmList);
            int i3 = i + 1;
            int size = realmGet$appPermissions.size();
            for (int i4 = 0; i4 < size; i4++) {
                realmList.add(AppPermissionRealmProxy.createDetachedCopy((AppPermission) realmGet$appPermissions.get(i4), i3, i2, map));
            }
        }
        if (i == i2) {
            processInfoRealmProxyInterface.realmSet$appSignatures(null);
        } else {
            RealmList realmGet$appSignatures = processInfoRealmProxyInterface2.realmGet$appSignatures();
            RealmList realmList2 = new RealmList();
            processInfoRealmProxyInterface.realmSet$appSignatures(realmList2);
            int i5 = i + 1;
            int size2 = realmGet$appSignatures.size();
            for (int i6 = 0; i6 < size2; i6++) {
                realmList2.add(AppSignatureRealmProxy.createDetachedCopy((AppSignature) realmGet$appSignatures.get(i6), i5, i2, map));
            }
        }
        return processInfo2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("ProcessInfo = proxy[");
        sb.append("{processId:");
        sb.append(realmGet$processId());
        sb.append("}");
        sb.append(",");
        sb.append("{name:");
        sb.append(realmGet$name() != null ? realmGet$name() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{applicationLabel:");
        sb.append(realmGet$applicationLabel() != null ? realmGet$applicationLabel() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{isSystemApp:");
        sb.append(realmGet$isSystemApp());
        sb.append("}");
        sb.append(",");
        sb.append("{importance:");
        sb.append(realmGet$importance() != null ? realmGet$importance() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{versionName:");
        sb.append(realmGet$versionName() != null ? realmGet$versionName() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{versionCode:");
        sb.append(realmGet$versionCode());
        sb.append("}");
        sb.append(",");
        sb.append("{installationPkg:");
        sb.append(realmGet$installationPkg() != null ? realmGet$installationPkg() : "null");
        sb.append("}");
        sb.append(",");
        sb.append("{appPermissions:");
        sb.append("RealmList<AppPermission>[");
        sb.append(realmGet$appPermissions().size());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        sb.append("}");
        sb.append(",");
        sb.append("{appSignatures:");
        sb.append("RealmList<AppSignature>[");
        sb.append(realmGet$appSignatures().size());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ProcessInfoRealmProxy processInfoRealmProxy = (ProcessInfoRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = processInfoRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = processInfoRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == processInfoRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
