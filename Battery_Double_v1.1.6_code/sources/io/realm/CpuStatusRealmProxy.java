package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mansoon.BatteryDouble.models.data.CpuStatus;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class CpuStatusRealmProxy extends CpuStatus implements RealmObjectProxy, CpuStatusRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private CpuStatusColumnInfo columnInfo;
    private ProxyState<CpuStatus> proxyState;

    static final class CpuStatusColumnInfo extends ColumnInfo {
        long cpuUsageIndex;
        long sleepTimeIndex;
        long upTimeIndex;

        CpuStatusColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(3);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("CpuStatus");
            this.cpuUsageIndex = addColumnDetails("cpuUsage", objectSchemaInfo);
            this.upTimeIndex = addColumnDetails("upTime", objectSchemaInfo);
            this.sleepTimeIndex = addColumnDetails("sleepTime", objectSchemaInfo);
        }

        CpuStatusColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new CpuStatusColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            CpuStatusColumnInfo cpuStatusColumnInfo = (CpuStatusColumnInfo) columnInfo;
            CpuStatusColumnInfo cpuStatusColumnInfo2 = (CpuStatusColumnInfo) columnInfo2;
            cpuStatusColumnInfo2.cpuUsageIndex = cpuStatusColumnInfo.cpuUsageIndex;
            cpuStatusColumnInfo2.upTimeIndex = cpuStatusColumnInfo.upTimeIndex;
            cpuStatusColumnInfo2.sleepTimeIndex = cpuStatusColumnInfo.sleepTimeIndex;
        }
    }

    public static String getTableName() {
        return "class_CpuStatus";
    }

    static {
        ArrayList arrayList = new ArrayList(3);
        arrayList.add("cpuUsage");
        arrayList.add("upTime");
        arrayList.add("sleepTime");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    CpuStatusRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (CpuStatusColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public double realmGet$cpuUsage() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.cpuUsageIndex);
    }

    public void realmSet$cpuUsage(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.cpuUsageIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.cpuUsageIndex, row$realm.getIndex(), d, true);
        }
    }

    public long realmGet$upTime() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getLong(this.columnInfo.upTimeIndex);
    }

    public void realmSet$upTime(long j) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.upTimeIndex, j);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.upTimeIndex, row$realm.getIndex(), j, true);
        }
    }

    public long realmGet$sleepTime() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getLong(this.columnInfo.sleepTimeIndex);
    }

    public void realmSet$sleepTime(long j) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.sleepTimeIndex, j);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.sleepTimeIndex, row$realm.getIndex(), j, true);
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("CpuStatus", 3, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("cpuUsage", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("upTime", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("sleepTime", RealmFieldType.INTEGER, false, false, true);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static CpuStatusColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new CpuStatusColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static CpuStatus createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        CpuStatus cpuStatus = (CpuStatus) realm.createObjectInternal(CpuStatus.class, true, Collections.emptyList());
        CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface = cpuStatus;
        if (jSONObject.has("cpuUsage")) {
            if (!jSONObject.isNull("cpuUsage")) {
                cpuStatusRealmProxyInterface.realmSet$cpuUsage(jSONObject.getDouble("cpuUsage"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'cpuUsage' to null.");
            }
        }
        if (jSONObject.has("upTime")) {
            if (!jSONObject.isNull("upTime")) {
                cpuStatusRealmProxyInterface.realmSet$upTime(jSONObject.getLong("upTime"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'upTime' to null.");
            }
        }
        if (jSONObject.has("sleepTime")) {
            if (!jSONObject.isNull("sleepTime")) {
                cpuStatusRealmProxyInterface.realmSet$sleepTime(jSONObject.getLong("sleepTime"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'sleepTime' to null.");
            }
        }
        return cpuStatus;
    }

    @TargetApi(11)
    public static CpuStatus createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        CpuStatus cpuStatus = new CpuStatus();
        CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface = cpuStatus;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("cpuUsage")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    cpuStatusRealmProxyInterface.realmSet$cpuUsage(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'cpuUsage' to null.");
                }
            } else if (nextName.equals("upTime")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    cpuStatusRealmProxyInterface.realmSet$upTime(jsonReader.nextLong());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'upTime' to null.");
                }
            } else if (!nextName.equals("sleepTime")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                cpuStatusRealmProxyInterface.realmSet$sleepTime(jsonReader.nextLong());
            } else {
                jsonReader.skipValue();
                throw new IllegalArgumentException("Trying to set non-nullable field 'sleepTime' to null.");
            }
        }
        jsonReader.endObject();
        return (CpuStatus) realm.copyToRealm(cpuStatus);
    }

    public static CpuStatus copyOrUpdate(Realm realm, CpuStatus cpuStatus, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (cpuStatus instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cpuStatus;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return cpuStatus;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(cpuStatus);
        if (realmObjectProxy2 != null) {
            return (CpuStatus) realmObjectProxy2;
        }
        return copy(realm, cpuStatus, z, map);
    }

    public static CpuStatus copy(Realm realm, CpuStatus cpuStatus, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(cpuStatus);
        if (realmObjectProxy != null) {
            return (CpuStatus) realmObjectProxy;
        }
        CpuStatus cpuStatus2 = (CpuStatus) realm.createObjectInternal(CpuStatus.class, false, Collections.emptyList());
        map.put(cpuStatus, (RealmObjectProxy) cpuStatus2);
        CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface = cpuStatus;
        CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface2 = cpuStatus2;
        cpuStatusRealmProxyInterface2.realmSet$cpuUsage(cpuStatusRealmProxyInterface.realmGet$cpuUsage());
        cpuStatusRealmProxyInterface2.realmSet$upTime(cpuStatusRealmProxyInterface.realmGet$upTime());
        cpuStatusRealmProxyInterface2.realmSet$sleepTime(cpuStatusRealmProxyInterface.realmGet$sleepTime());
        return cpuStatus2;
    }

    public static long insert(Realm realm, CpuStatus cpuStatus, Map<RealmModel, Long> map) {
        CpuStatus cpuStatus2 = cpuStatus;
        if (cpuStatus2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cpuStatus2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(CpuStatus.class);
        long nativePtr = table.getNativePtr();
        CpuStatusColumnInfo cpuStatusColumnInfo = (CpuStatusColumnInfo) realm.getSchema().getColumnInfo(CpuStatus.class);
        long createRow = OsObject.createRow(table);
        map.put(cpuStatus2, Long.valueOf(createRow));
        CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface = cpuStatus2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetDouble(j, cpuStatusColumnInfo.cpuUsageIndex, j2, cpuStatusRealmProxyInterface.realmGet$cpuUsage(), false);
        Table.nativeSetLong(j, cpuStatusColumnInfo.upTimeIndex, j2, cpuStatusRealmProxyInterface.realmGet$upTime(), false);
        Table.nativeSetLong(j, cpuStatusColumnInfo.sleepTimeIndex, j2, cpuStatusRealmProxyInterface.realmGet$sleepTime(), false);
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(CpuStatus.class);
        long nativePtr = table.getNativePtr();
        CpuStatusColumnInfo cpuStatusColumnInfo = (CpuStatusColumnInfo) realm.getSchema().getColumnInfo(CpuStatus.class);
        while (it.hasNext()) {
            CpuStatus cpuStatus = (CpuStatus) it.next();
            if (!map2.containsKey(cpuStatus)) {
                if (cpuStatus instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cpuStatus;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(cpuStatus, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(cpuStatus, Long.valueOf(createRow));
                CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface = cpuStatus;
                long j = nativePtr;
                long j2 = createRow;
                Table.nativeSetDouble(j, cpuStatusColumnInfo.cpuUsageIndex, j2, cpuStatusRealmProxyInterface.realmGet$cpuUsage(), false);
                Table.nativeSetLong(j, cpuStatusColumnInfo.upTimeIndex, j2, cpuStatusRealmProxyInterface.realmGet$upTime(), false);
                Table.nativeSetLong(j, cpuStatusColumnInfo.sleepTimeIndex, j2, cpuStatusRealmProxyInterface.realmGet$sleepTime(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, CpuStatus cpuStatus, Map<RealmModel, Long> map) {
        CpuStatus cpuStatus2 = cpuStatus;
        if (cpuStatus2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cpuStatus2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(CpuStatus.class);
        long nativePtr = table.getNativePtr();
        CpuStatusColumnInfo cpuStatusColumnInfo = (CpuStatusColumnInfo) realm.getSchema().getColumnInfo(CpuStatus.class);
        long createRow = OsObject.createRow(table);
        map.put(cpuStatus2, Long.valueOf(createRow));
        CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface = cpuStatus2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetDouble(j, cpuStatusColumnInfo.cpuUsageIndex, j2, cpuStatusRealmProxyInterface.realmGet$cpuUsage(), false);
        Table.nativeSetLong(j, cpuStatusColumnInfo.upTimeIndex, j2, cpuStatusRealmProxyInterface.realmGet$upTime(), false);
        Table.nativeSetLong(j, cpuStatusColumnInfo.sleepTimeIndex, j2, cpuStatusRealmProxyInterface.realmGet$sleepTime(), false);
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(CpuStatus.class);
        long nativePtr = table.getNativePtr();
        CpuStatusColumnInfo cpuStatusColumnInfo = (CpuStatusColumnInfo) realm.getSchema().getColumnInfo(CpuStatus.class);
        while (it.hasNext()) {
            CpuStatus cpuStatus = (CpuStatus) it.next();
            if (!map2.containsKey(cpuStatus)) {
                if (cpuStatus instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cpuStatus;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(cpuStatus, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(cpuStatus, Long.valueOf(createRow));
                CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface = cpuStatus;
                long j = nativePtr;
                long j2 = createRow;
                Table.nativeSetDouble(j, cpuStatusColumnInfo.cpuUsageIndex, j2, cpuStatusRealmProxyInterface.realmGet$cpuUsage(), false);
                Table.nativeSetLong(j, cpuStatusColumnInfo.upTimeIndex, j2, cpuStatusRealmProxyInterface.realmGet$upTime(), false);
                Table.nativeSetLong(j, cpuStatusColumnInfo.sleepTimeIndex, j2, cpuStatusRealmProxyInterface.realmGet$sleepTime(), false);
            }
        }
    }

    public static CpuStatus createDetachedCopy(CpuStatus cpuStatus, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        CpuStatus cpuStatus2;
        if (i > i2 || cpuStatus == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(cpuStatus);
        if (cacheData == null) {
            cpuStatus2 = new CpuStatus();
            map.put(cpuStatus, new CacheData(i, cpuStatus2));
        } else if (i >= cacheData.minDepth) {
            return (CpuStatus) cacheData.object;
        } else {
            CpuStatus cpuStatus3 = (CpuStatus) cacheData.object;
            cacheData.minDepth = i;
            cpuStatus2 = cpuStatus3;
        }
        CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface = cpuStatus2;
        CpuStatusRealmProxyInterface cpuStatusRealmProxyInterface2 = cpuStatus;
        cpuStatusRealmProxyInterface.realmSet$cpuUsage(cpuStatusRealmProxyInterface2.realmGet$cpuUsage());
        cpuStatusRealmProxyInterface.realmSet$upTime(cpuStatusRealmProxyInterface2.realmGet$upTime());
        cpuStatusRealmProxyInterface.realmSet$sleepTime(cpuStatusRealmProxyInterface2.realmGet$sleepTime());
        return cpuStatus2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("CpuStatus = proxy[");
        sb.append("{cpuUsage:");
        sb.append(realmGet$cpuUsage());
        sb.append("}");
        sb.append(",");
        sb.append("{upTime:");
        sb.append(realmGet$upTime());
        sb.append("}");
        sb.append(",");
        sb.append("{sleepTime:");
        sb.append(realmGet$sleepTime());
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CpuStatusRealmProxy cpuStatusRealmProxy = (CpuStatusRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = cpuStatusRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = cpuStatusRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == cpuStatusRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
