package io.realm;

public interface BatterySessionRealmProxyInterface {
    int realmGet$id();

    float realmGet$level();

    int realmGet$screenOn();

    long realmGet$timestamp();

    String realmGet$triggeredBy();

    void realmSet$id(int i);

    void realmSet$level(float f);

    void realmSet$screenOn(int i);

    void realmSet$timestamp(long j);

    void realmSet$triggeredBy(String str);
}
