package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.components.AxisBase;
import java.text.DecimalFormat;

public class DefaultAxisValueFormatter implements IAxisValueFormatter {
    protected int digits = 0;
    protected DecimalFormat mFormat;

    public DefaultAxisValueFormatter(int i) {
        this.digits = i;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 == 0) {
                stringBuffer.append(".");
            }
            stringBuffer.append("0");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("###,###,###,##0");
        sb.append(stringBuffer.toString());
        this.mFormat = new DecimalFormat(sb.toString());
    }

    public String getFormattedValue(float f, AxisBase axisBase) {
        return this.mFormat.format((double) f);
    }

    public int getDecimalDigits() {
        return this.digits;
    }
}
