package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.text.DecimalFormat;

public class LargeValueFormatter implements IValueFormatter, IAxisValueFormatter {
    private static final int MAX_LENGTH = 5;
    private static String[] SUFFIX = {"", "k", "m", "b", "t"};
    private DecimalFormat mFormat;
    private String mText;

    public int getDecimalDigits() {
        return 0;
    }

    public LargeValueFormatter() {
        this.mText = "";
        this.mFormat = new DecimalFormat("###E00");
    }

    public LargeValueFormatter(String str) {
        this();
        this.mText = str;
    }

    public String getFormattedValue(float f, Entry entry, int i, ViewPortHandler viewPortHandler) {
        StringBuilder sb = new StringBuilder();
        sb.append(makePretty((double) f));
        sb.append(this.mText);
        return sb.toString();
    }

    public String getFormattedValue(float f, AxisBase axisBase) {
        StringBuilder sb = new StringBuilder();
        sb.append(makePretty((double) f));
        sb.append(this.mText);
        return sb.toString();
    }

    public void setAppendix(String str) {
        this.mText = str;
    }

    public void setSuffix(String[] strArr) {
        SUFFIX = strArr;
    }

    private String makePretty(double d) {
        String format = this.mFormat.format(d);
        int numericValue = Character.getNumericValue(format.charAt(format.length() - 1));
        int numericValue2 = Character.getNumericValue(format.charAt(format.length() - 2));
        StringBuilder sb = new StringBuilder();
        sb.append(numericValue2);
        sb.append("");
        sb.append(numericValue);
        String replaceAll = format.replaceAll("E[0-9][0-9]", SUFFIX[Integer.valueOf(sb.toString()).intValue() / 3]);
        while (true) {
            if (replaceAll.length() <= 5 && !replaceAll.matches("[0-9]+\\.[a-z]")) {
                return replaceAll;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(replaceAll.substring(0, replaceAll.length() - 2));
            sb2.append(replaceAll.substring(replaceAll.length() - 1));
            replaceAll = sb2.toString();
        }
    }
}
