package com.github.mikephil.charting.buffer;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

public class HorizontalBarBuffer extends BarBuffer {
    public HorizontalBarBuffer(int i, int i2, boolean z) {
        super(i, i2, z);
    }

    public void feed(IBarDataSet iBarDataSet) {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float entryCount = ((float) iBarDataSet.getEntryCount()) * this.phaseX;
        float f6 = this.mBarWidth / 2.0f;
        for (int i = 0; ((float) i) < entryCount; i++) {
            BarEntry barEntry = (BarEntry) iBarDataSet.getEntryForIndex(i);
            if (barEntry != null) {
                float x = barEntry.getX();
                float y = barEntry.getY();
                float[] yVals = barEntry.getYVals();
                if (!this.mContainsStacks || yVals == null) {
                    float f7 = x - f6;
                    float f8 = x + f6;
                    if (this.mInverted) {
                        float f9 = y >= 0.0f ? y : 0.0f;
                        if (y > 0.0f) {
                            y = 0.0f;
                        }
                        float f10 = y;
                        y = f9;
                        f = f10;
                    } else {
                        f = y >= 0.0f ? y : 0.0f;
                        if (y > 0.0f) {
                            y = 0.0f;
                        }
                    }
                    if (f > 0.0f) {
                        f *= this.phaseY;
                    } else {
                        y *= this.phaseY;
                    }
                    addBar(y, f8, f, f7);
                } else {
                    float f11 = -barEntry.getNegativeSum();
                    int i2 = 0;
                    float f12 = 0.0f;
                    while (i2 < yVals.length) {
                        float f13 = yVals[i2];
                        if (f13 >= 0.0f) {
                            f3 = f13 + f12;
                            f2 = f11;
                            f4 = f3;
                        } else {
                            float abs = Math.abs(f13) + f11;
                            float abs2 = Math.abs(f13) + f11;
                            float f14 = f11;
                            f4 = f12;
                            f12 = f14;
                            float f15 = abs;
                            f2 = abs2;
                            f3 = f15;
                        }
                        float f16 = x - f6;
                        float f17 = x + f6;
                        if (this.mInverted) {
                            float f18 = f12 >= f3 ? f12 : f3;
                            if (f12 > f3) {
                                f12 = f3;
                            }
                            float f19 = f12;
                            f12 = f18;
                            f5 = f19;
                        } else {
                            f5 = f12 >= f3 ? f12 : f3;
                            if (f12 > f3) {
                                f12 = f3;
                            }
                        }
                        addBar(f12 * this.phaseY, f17, f5 * this.phaseY, f16);
                        i2++;
                        f12 = f4;
                        f11 = f2;
                    }
                }
            }
        }
        reset();
    }
}
