package com.github.mikephil.charting.utils;

import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.ArrayList;
import java.util.List;

public class ColorTemplate {
    public static final int[] COLORFUL_COLORS = {Color.rgb(193, 37, 82), Color.rgb(255, 102, 0), Color.rgb(245, 199, 0), Color.rgb(106, IronSourceConstants.REWARDED_VIDEO_DAILY_CAPPED, 31), Color.rgb(179, 100, 53)};
    public static final int COLOR_NONE = 1122867;
    public static final int COLOR_SKIP = 1122868;
    public static final int[] JOYFUL_COLORS = {Color.rgb(217, 80, TsExtractor.TS_STREAM_TYPE_DTS), Color.rgb(254, 149, 7), Color.rgb(254, 247, 120), Color.rgb(106, 167, TsExtractor.TS_STREAM_TYPE_SPLICE_INFO), Color.rgb(53, 194, 209)};
    public static final int[] LIBERTY_COLORS = {Color.rgb(207, 248, 246), Color.rgb(148, 212, 212), Color.rgb(136, 180, 187), Color.rgb(118, 174, 175), Color.rgb(42, 109, TsExtractor.TS_STREAM_TYPE_HDMV_DTS)};
    public static final int[] MATERIAL_COLORS = {rgb("#2ecc71"), rgb("#f1c40f"), rgb("#e74c3c"), rgb("#3498db")};
    public static final int[] PASTEL_COLORS = {Color.rgb(64, 89, 128), Color.rgb(149, 165, 124), Color.rgb(217, 184, 162), Color.rgb(191, TsExtractor.TS_STREAM_TYPE_SPLICE_INFO, TsExtractor.TS_STREAM_TYPE_SPLICE_INFO), Color.rgb(179, 48, 80)};
    public static final int[] VORDIPLOM_COLORS = {Color.rgb(PsExtractor.AUDIO_STREAM, 255, IronSourceConstants.USING_CACHE_FOR_INIT_EVENT), Color.rgb(255, 247, IronSourceConstants.USING_CACHE_FOR_INIT_EVENT), Color.rgb(255, 208, IronSourceConstants.USING_CACHE_FOR_INIT_EVENT), Color.rgb(IronSourceConstants.USING_CACHE_FOR_INIT_EVENT, 234, 255), Color.rgb(255, IronSourceConstants.USING_CACHE_FOR_INIT_EVENT, 157)};

    public static int colorWithAlpha(int i, int i2) {
        return (i & ViewCompat.MEASURED_SIZE_MASK) | ((i2 & 255) << 24);
    }

    public static int rgb(String str) {
        int parseLong = (int) Long.parseLong(str.replace("#", ""), 16);
        return Color.rgb((parseLong >> 16) & 255, (parseLong >> 8) & 255, (parseLong >> 0) & 255);
    }

    public static int getHoloBlue() {
        return Color.rgb(51, 181, 229);
    }

    public static List<Integer> createColors(Resources resources, int[] iArr) {
        ArrayList arrayList = new ArrayList();
        for (int color : iArr) {
            arrayList.add(Integer.valueOf(resources.getColor(color)));
        }
        return arrayList;
    }

    public static List<Integer> createColors(int[] iArr) {
        ArrayList arrayList = new ArrayList();
        for (int valueOf : iArr) {
            arrayList.add(Integer.valueOf(valueOf));
        }
        return arrayList;
    }
}
