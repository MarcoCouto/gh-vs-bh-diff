package com.github.mikephil.charting.utils;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    private static final String LOG = "MPChart-FileUtils";

    public static List<Entry> loadEntriesFromFile(String str) {
        File file = new File(Environment.getExternalStorageDirectory(), str);
        ArrayList arrayList = new ArrayList();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                String[] split = readLine.split("#");
                if (split.length <= 2) {
                    arrayList.add(new Entry(Float.parseFloat(split[0]), (float) Integer.parseInt(split[1])));
                } else {
                    float[] fArr = new float[(split.length - 1)];
                    for (int i = 0; i < fArr.length; i++) {
                        fArr[i] = Float.parseFloat(split[i]);
                    }
                    arrayList.add(new BarEntry((float) Integer.parseInt(split[split.length - 1]), fArr));
                }
            }
        } catch (IOException e) {
            Log.e(LOG, e.toString());
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x007d A[SYNTHETIC, Splitter:B:27:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008e A[SYNTHETIC, Splitter:B:33:0x008e] */
    public static List<Entry> loadEntriesFromAssets(AssetManager assetManager, String str) {
        ArrayList arrayList = new ArrayList();
        BufferedReader bufferedReader = null;
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(assetManager.open(str), "UTF-8"));
            try {
                for (String readLine = bufferedReader2.readLine(); readLine != null; readLine = bufferedReader2.readLine()) {
                    String[] split = readLine.split("#");
                    if (split.length <= 2) {
                        arrayList.add(new Entry(Float.parseFloat(split[1]), Float.parseFloat(split[0])));
                    } else {
                        float[] fArr = new float[(split.length - 1)];
                        for (int i = 0; i < fArr.length; i++) {
                            fArr[i] = Float.parseFloat(split[i]);
                        }
                        arrayList.add(new BarEntry((float) Integer.parseInt(split[split.length - 1]), fArr));
                    }
                }
            } catch (IOException e) {
                e = e;
                bufferedReader = bufferedReader2;
                try {
                    Log.e(LOG, e.toString());
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    bufferedReader2 = bufferedReader;
                    if (bufferedReader2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (bufferedReader2 != null) {
                    try {
                        bufferedReader2.close();
                    } catch (IOException e2) {
                        Log.e(LOG, e2.toString());
                    }
                }
                throw th;
            }
            try {
                bufferedReader2.close();
            } catch (IOException e3) {
                Log.e(LOG, e3.toString());
            }
        } catch (IOException e4) {
            e = e4;
            Log.e(LOG, e.toString());
            if (bufferedReader != null) {
            }
            return arrayList;
        }
        return arrayList;
    }

    public static void saveToSdCard(List<Entry> list, String str) {
        File file = new File(Environment.getExternalStorageDirectory(), str);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                Log.e(LOG, e.toString());
            }
        }
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            for (Entry entry : list) {
                StringBuilder sb = new StringBuilder();
                sb.append(entry.getY());
                sb.append("#");
                sb.append(entry.getX());
                bufferedWriter.append(sb.toString());
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e2) {
            Log.e(LOG, e2.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055 A[SYNTHETIC, Splitter:B:19:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0066 A[SYNTHETIC, Splitter:B:25:0x0066] */
    public static List<BarEntry> loadBarEntriesFromAssets(AssetManager assetManager, String str) {
        ArrayList arrayList = new ArrayList();
        BufferedReader bufferedReader = null;
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(assetManager.open(str), "UTF-8"));
            try {
                for (String readLine = bufferedReader2.readLine(); readLine != null; readLine = bufferedReader2.readLine()) {
                    String[] split = readLine.split("#");
                    arrayList.add(new BarEntry(Float.parseFloat(split[1]), Float.parseFloat(split[0])));
                }
            } catch (IOException e) {
                e = e;
                bufferedReader = bufferedReader2;
                try {
                    Log.e(LOG, e.toString());
                    if (bufferedReader != null) {
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    bufferedReader2 = bufferedReader;
                    if (bufferedReader2 != null) {
                        try {
                            bufferedReader2.close();
                        } catch (IOException e2) {
                            Log.e(LOG, e2.toString());
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (bufferedReader2 != null) {
                }
                throw th;
            }
            try {
                bufferedReader2.close();
            } catch (IOException e3) {
                Log.e(LOG, e3.toString());
            }
        } catch (IOException e4) {
            e = e4;
            Log.e(LOG, e.toString());
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            return arrayList;
        }
        return arrayList;
    }
}
