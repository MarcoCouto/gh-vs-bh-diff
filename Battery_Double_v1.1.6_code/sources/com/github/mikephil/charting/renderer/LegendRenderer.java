package com.github.mikephil.charting.renderer;

import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Typeface;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendDirection;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment;
import com.github.mikephil.charting.components.Legend.LegendOrientation;
import com.github.mikephil.charting.components.Legend.LegendVerticalAlignment;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ICandleDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LegendRenderer extends Renderer {
    protected List<LegendEntry> computedEntries = new ArrayList(16);
    protected FontMetrics legendFontMetrics = new FontMetrics();
    protected Legend mLegend;
    protected Paint mLegendFormPaint;
    protected Paint mLegendLabelPaint;
    private Path mLineFormPath = new Path();

    public LegendRenderer(ViewPortHandler viewPortHandler, Legend legend) {
        super(viewPortHandler);
        this.mLegend = legend;
        this.mLegendLabelPaint = new Paint(1);
        this.mLegendLabelPaint.setTextSize(Utils.convertDpToPixel(9.0f));
        this.mLegendLabelPaint.setTextAlign(Align.LEFT);
        this.mLegendFormPaint = new Paint(1);
        this.mLegendFormPaint.setStyle(Style.FILL);
    }

    public Paint getLabelPaint() {
        return this.mLegendLabelPaint;
    }

    public Paint getFormPaint() {
        return this.mLegendFormPaint;
    }

    public void computeLegend(ChartData<?> chartData) {
        ChartData<?> chartData2;
        String str;
        ChartData<?> chartData3 = chartData;
        if (!this.mLegend.isLegendCustom()) {
            this.computedEntries.clear();
            int i = 0;
            while (i < chartData.getDataSetCount()) {
                IDataSet dataSetByIndex = chartData3.getDataSetByIndex(i);
                List colors = dataSetByIndex.getColors();
                int entryCount = dataSetByIndex.getEntryCount();
                if (dataSetByIndex instanceof IBarDataSet) {
                    IBarDataSet iBarDataSet = (IBarDataSet) dataSetByIndex;
                    if (iBarDataSet.isStacked()) {
                        String[] stackLabels = iBarDataSet.getStackLabels();
                        int i2 = 0;
                        while (i2 < colors.size() && i2 < iBarDataSet.getStackSize()) {
                            List<LegendEntry> list = this.computedEntries;
                            LegendEntry legendEntry = r10;
                            LegendEntry legendEntry2 = new LegendEntry(stackLabels[i2 % stackLabels.length], dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), ((Integer) colors.get(i2)).intValue());
                            list.add(legendEntry);
                            i2++;
                        }
                        if (iBarDataSet.getLabel() != null) {
                            List<LegendEntry> list2 = this.computedEntries;
                            LegendEntry legendEntry3 = new LegendEntry(dataSetByIndex.getLabel(), LegendForm.NONE, Float.NaN, Float.NaN, null, ColorTemplate.COLOR_NONE);
                            list2.add(legendEntry3);
                        }
                        chartData2 = chartData3;
                        i++;
                        chartData3 = chartData2;
                    }
                }
                if (dataSetByIndex instanceof IPieDataSet) {
                    IPieDataSet iPieDataSet = (IPieDataSet) dataSetByIndex;
                    int i3 = 0;
                    while (i3 < colors.size() && i3 < entryCount) {
                        List<LegendEntry> list3 = this.computedEntries;
                        LegendEntry legendEntry4 = r9;
                        LegendEntry legendEntry5 = new LegendEntry(((PieEntry) iPieDataSet.getEntryForIndex(i3)).getLabel(), dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), ((Integer) colors.get(i3)).intValue());
                        list3.add(legendEntry4);
                        i3++;
                        ChartData<?> chartData4 = chartData;
                    }
                    if (iPieDataSet.getLabel() != null) {
                        List<LegendEntry> list4 = this.computedEntries;
                        LegendEntry legendEntry6 = new LegendEntry(dataSetByIndex.getLabel(), LegendForm.NONE, Float.NaN, Float.NaN, null, ColorTemplate.COLOR_NONE);
                        list4.add(legendEntry6);
                    }
                } else {
                    if (dataSetByIndex instanceof ICandleDataSet) {
                        ICandleDataSet iCandleDataSet = (ICandleDataSet) dataSetByIndex;
                        if (iCandleDataSet.getDecreasingColor() != 1122867) {
                            int decreasingColor = iCandleDataSet.getDecreasingColor();
                            int increasingColor = iCandleDataSet.getIncreasingColor();
                            List<LegendEntry> list5 = this.computedEntries;
                            LegendEntry legendEntry7 = new LegendEntry(null, dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), decreasingColor);
                            list5.add(legendEntry7);
                            List<LegendEntry> list6 = this.computedEntries;
                            LegendEntry legendEntry8 = new LegendEntry(dataSetByIndex.getLabel(), dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), increasingColor);
                            list6.add(legendEntry8);
                        }
                    }
                    int i4 = 0;
                    while (i4 < colors.size() && i4 < entryCount) {
                        if (i4 >= colors.size() - 1 || i4 >= entryCount - 1) {
                            str = chartData.getDataSetByIndex(i).getLabel();
                        } else {
                            str = null;
                            ChartData<?> chartData5 = chartData;
                        }
                        List<LegendEntry> list7 = this.computedEntries;
                        LegendEntry legendEntry9 = new LegendEntry(str, dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), ((Integer) colors.get(i4)).intValue());
                        list7.add(legendEntry9);
                        i4++;
                    }
                }
                chartData2 = chartData;
                i++;
                chartData3 = chartData2;
            }
            if (this.mLegend.getExtraEntries() != null) {
                Collections.addAll(this.computedEntries, this.mLegend.getExtraEntries());
            }
            this.mLegend.setEntries(this.computedEntries);
        }
        Typeface typeface = this.mLegend.getTypeface();
        if (typeface != null) {
            this.mLegendLabelPaint.setTypeface(typeface);
        }
        this.mLegendLabelPaint.setTextSize(this.mLegend.getTextSize());
        this.mLegendLabelPaint.setColor(this.mLegend.getTextColor());
        this.mLegend.calculateDimensions(this.mLegendLabelPaint, this.mViewPortHandler);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0292, code lost:
        switch(r1) {
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.TOP :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x02b4;
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.BOTTOM :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x02a7;
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.CENTER :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x0297;
            default: goto L_0x0295;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0295, code lost:
        r2 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0297, code lost:
        r2 = r2 + ((r6.mViewPortHandler.getChartHeight() - r6.mLegend.mNeededHeight) / 2.0f);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x02a7, code lost:
        r2 = (r6.mViewPortHandler.getChartHeight() - r2) - r6.mLegend.mNeededHeight;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02b4, code lost:
        r1 = r12.length;
        r0 = r2;
        r27 = r4;
        r17 = r7;
        r2 = 0;
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x02bc, code lost:
        if (r2 >= r1) goto L_0x03d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x02be, code lost:
        r28 = r8;
        r8 = r12[r2];
        r29 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x02c8, code lost:
        if (r8.form == com.github.mikephil.charting.components.Legend.LegendForm.NONE) goto L_0x02cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x02ca, code lost:
        r18 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x02cd, code lost:
        r18 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x02d5, code lost:
        if (java.lang.Float.isNaN(r8.formSize) == false) goto L_0x02da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x02d7, code lost:
        r20 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x02da, code lost:
        r20 = com.github.mikephil.charting.utils.Utils.convertDpToPixel(r8.formSize);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x02e6, code lost:
        if (r2 >= r3.size()) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x02f2, code lost:
        if (((java.lang.Boolean) r3.get(r2)).booleanValue() == false) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02f4, code lost:
        r22 = r0 + (r24 + r21);
        r17 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x02fc, code lost:
        r22 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0300, code lost:
        if (r17 != r7) goto L_0x032c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0304, code lost:
        if (r15 != com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.CENTER) goto L_0x032c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x030a, code lost:
        if (r4 >= r10.size()) goto L_0x032c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x030e, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x031b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0310, code lost:
        r0 = ((com.github.mikephil.charting.utils.FSize) r10.get(r4)).width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x031b, code lost:
        r0 = -((com.github.mikephil.charting.utils.FSize) r10.get(r4)).width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0325, code lost:
        r17 = r17 + (r0 / 2.0f);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x032e, code lost:
        r23 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0332, code lost:
        if (r8.label != null) goto L_0x0337;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0334, code lost:
        r25 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0337, code lost:
        r25 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x0339, code lost:
        if (r18 == false) goto L_0x0366;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x033d, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x0341;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x033f, code lost:
        r17 = r17 - r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0341, code lost:
        r26 = r29;
        r30 = r7;
        r7 = r2;
        r29 = r3;
        r31 = r10;
        r10 = r27;
        r32 = r11;
        r11 = r34;
        drawForm(r34, r17, r22 + r11, r8, r6.mLegend);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0361, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x0375;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0363, code lost:
        r17 = r17 + r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0366, code lost:
        r30 = r7;
        r31 = r10;
        r32 = r11;
        r10 = r27;
        r26 = r29;
        r11 = r34;
        r7 = r2;
        r29 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0375, code lost:
        if (r25 != false) goto L_0x03b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0377, code lost:
        if (r18 == false) goto L_0x0382;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x037b, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x037f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x037d, code lost:
        r0 = -r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x037f, code lost:
        r0 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0380, code lost:
        r17 = r17 + r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0384, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x0390;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0386, code lost:
        r17 = r17 - ((com.github.mikephil.charting.utils.FSize) r10.get(r7)).width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0390, code lost:
        r0 = r17;
        drawLabel(r11, r0, r22 + r24, r8.label);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x039b, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x03a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x039d, code lost:
        r0 = r0 + ((com.github.mikephil.charting.utils.FSize) r10.get(r7)).width;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x03a8, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x03ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x03aa, code lost:
        r1 = -r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x03ac, code lost:
        r1 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x03ad, code lost:
        r17 = r0 + r1;
        r0 = r28;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x03b5, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x03bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x03b7, code lost:
        r0 = r28;
        r4 = -r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x03bb, code lost:
        r0 = r28;
        r4 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x03be, code lost:
        r17 = r17 + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x03c0, code lost:
        r2 = r7 + 1;
        r8 = r0;
        r27 = r10;
        r5 = r11;
        r0 = r22;
        r4 = r23;
        r1 = r26;
        r3 = r29;
        r7 = r30;
        r10 = r31;
        r11 = r32;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03d6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x014e, code lost:
        r7 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x016b, code lost:
        r7 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0174, code lost:
        switch(r0) {
            case com.github.mikephil.charting.components.Legend.LegendOrientation.HORIZONTAL :com.github.mikephil.charting.components.Legend$LegendOrientation: goto L_0x0273;
            case com.github.mikephil.charting.components.Legend.LegendOrientation.VERTICAL :com.github.mikephil.charting.components.Legend$LegendOrientation: goto L_0x0179;
            default: goto L_0x0177;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0181, code lost:
        switch(r1) {
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.TOP :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x01b5;
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.BOTTOM :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x019d;
            case com.github.mikephil.charting.components.Legend.LegendVerticalAlignment.CENTER :com.github.mikephil.charting.components.Legend$LegendVerticalAlignment: goto L_0x0186;
            default: goto L_0x0184;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0184, code lost:
        r0 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0186, code lost:
        r0 = ((r6.mViewPortHandler.getChartHeight() / 2.0f) - (r6.mLegend.mNeededHeight / 2.0f)) + r6.mLegend.getYOffset();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x019f, code lost:
        if (r15 != com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.CENTER) goto L_0x01a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01a1, code lost:
        r0 = r6.mViewPortHandler.getChartHeight();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01a8, code lost:
        r0 = r6.mViewPortHandler.contentBottom();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01ae, code lost:
        r0 = r0 - (r6.mLegend.mNeededHeight + r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01b7, code lost:
        if (r15 != com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment.CENTER) goto L_0x01bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01b9, code lost:
        r0 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01bb, code lost:
        r0 = r6.mViewPortHandler.contentTop();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01c1, code lost:
        r0 = r0 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01c2, code lost:
        r15 = r0;
        r10 = 0;
        r14 = 0.0f;
        r17 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01c8, code lost:
        if (r10 >= r12.length) goto L_0x03d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01ca, code lost:
        r4 = r12[r10];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01d0, code lost:
        if (r4.form == com.github.mikephil.charting.components.Legend.LegendForm.NONE) goto L_0x01d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01d2, code lost:
        r19 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01d5, code lost:
        r19 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01dd, code lost:
        if (java.lang.Float.isNaN(r4.formSize) == false) goto L_0x01e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01df, code lost:
        r22 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01e2, code lost:
        r22 = com.github.mikephil.charting.utils.Utils.convertDpToPixel(r4.formSize);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01ea, code lost:
        if (r19 == false) goto L_0x021b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01ee, code lost:
        if (r5 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01f0, code lost:
        r0 = r7 + r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01f2, code lost:
        r23 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01f5, code lost:
        r0 = r7 - (r22 - r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01fa, code lost:
        r26 = r4;
        r8 = r20;
        r9 = r5;
        drawForm(r34, r23, r15 + r11, r4, r6.mLegend);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0212, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x0218;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0214, code lost:
        r23 = r23 + r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0218, code lost:
        r0 = r26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x021b, code lost:
        r9 = r5;
        r8 = r20;
        r0 = r4;
        r23 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0223, code lost:
        if (r0.label == null) goto L_0x0264;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0225, code lost:
        if (r19 == false) goto L_0x0233;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0227, code lost:
        if (r17 != false) goto L_0x0233;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x022b, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.LEFT_TO_RIGHT) goto L_0x022f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x022d, code lost:
        r1 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x022f, code lost:
        r1 = -r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0230, code lost:
        r1 = r23 + r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0233, code lost:
        if (r17 == false) goto L_0x0237;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0235, code lost:
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0237, code lost:
        r1 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x023b, code lost:
        if (r9 != com.github.mikephil.charting.components.Legend.LegendDirection.RIGHT_TO_LEFT) goto L_0x0247;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x023d, code lost:
        r1 = r1 - ((float) com.github.mikephil.charting.utils.Utils.calcTextWidth(r6.mLegendLabelPaint, r0.label));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0247, code lost:
        if (r17 != false) goto L_0x0253;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0249, code lost:
        drawLabel(r34, r1, r15 + r24, r0.label);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0253, code lost:
        r15 = r15 + (r24 + r21);
        drawLabel(r34, r1, r15 + r24, r0.label);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x025f, code lost:
        r15 = r15 + (r24 + r21);
        r14 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0264, code lost:
        r5 = r34;
        r14 = r14 + (r22 + r8);
        r17 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x026c, code lost:
        r10 = r10 + 1;
        r20 = r8;
        r5 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0273, code lost:
        r9 = r5;
        r8 = r20;
        r5 = r34;
        r10 = r6.mLegend.getCalculatedLineSizes();
        r4 = r6.mLegend.getCalculatedLabelSizes();
        r3 = r6.mLegend.getCalculatedLabelBreakPoints();
     */
    public void renderLegend(Canvas canvas) {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        double d;
        Canvas canvas2 = canvas;
        if (this.mLegend.isEnabled()) {
            Typeface typeface = this.mLegend.getTypeface();
            if (typeface != null) {
                this.mLegendLabelPaint.setTypeface(typeface);
            }
            this.mLegendLabelPaint.setTextSize(this.mLegend.getTextSize());
            this.mLegendLabelPaint.setColor(this.mLegend.getTextColor());
            float lineHeight = Utils.getLineHeight(this.mLegendLabelPaint, this.legendFontMetrics);
            float lineSpacing = Utils.getLineSpacing(this.mLegendLabelPaint, this.legendFontMetrics) + Utils.convertDpToPixel(this.mLegend.getYEntrySpace());
            float calcTextHeight = lineHeight - (((float) Utils.calcTextHeight(this.mLegendLabelPaint, "ABC")) / 2.0f);
            LegendEntry[] entries = this.mLegend.getEntries();
            float convertDpToPixel = Utils.convertDpToPixel(this.mLegend.getFormToTextSpace());
            float convertDpToPixel2 = Utils.convertDpToPixel(this.mLegend.getXEntrySpace());
            LegendOrientation orientation = this.mLegend.getOrientation();
            LegendHorizontalAlignment horizontalAlignment = this.mLegend.getHorizontalAlignment();
            LegendVerticalAlignment verticalAlignment = this.mLegend.getVerticalAlignment();
            LegendDirection direction = this.mLegend.getDirection();
            float convertDpToPixel3 = Utils.convertDpToPixel(this.mLegend.getFormSize());
            float convertDpToPixel4 = Utils.convertDpToPixel(this.mLegend.getStackSpace());
            float yOffset = this.mLegend.getYOffset();
            float xOffset = this.mLegend.getXOffset();
            switch (horizontalAlignment) {
                case LEFT:
                    f3 = convertDpToPixel4;
                    f = lineHeight;
                    f2 = lineSpacing;
                    if (orientation != LegendOrientation.VERTICAL) {
                        xOffset += this.mViewPortHandler.contentLeft();
                    }
                    if (direction == LegendDirection.RIGHT_TO_LEFT) {
                        xOffset += this.mLegend.mNeededWidth;
                        break;
                    }
                    break;
                case RIGHT:
                    f3 = convertDpToPixel4;
                    f = lineHeight;
                    f2 = lineSpacing;
                    if (orientation == LegendOrientation.VERTICAL) {
                        f4 = this.mViewPortHandler.getChartWidth() - xOffset;
                    } else {
                        f4 = this.mViewPortHandler.contentRight() - xOffset;
                    }
                    if (direction == LegendDirection.LEFT_TO_RIGHT) {
                        xOffset = f4 - this.mLegend.mNeededWidth;
                        break;
                    }
                    break;
                case CENTER:
                    if (orientation == LegendOrientation.VERTICAL) {
                        f5 = this.mViewPortHandler.getChartWidth() / 2.0f;
                        f3 = convertDpToPixel4;
                    } else {
                        f3 = convertDpToPixel4;
                        f5 = this.mViewPortHandler.contentLeft() + (this.mViewPortHandler.contentWidth() / 2.0f);
                    }
                    f4 = (direction == LegendDirection.LEFT_TO_RIGHT ? xOffset : -xOffset) + f5;
                    if (orientation != LegendOrientation.VERTICAL) {
                        f = lineHeight;
                        f2 = lineSpacing;
                        break;
                    } else {
                        f2 = lineSpacing;
                        double d2 = (double) f4;
                        if (direction == LegendDirection.LEFT_TO_RIGHT) {
                            f = lineHeight;
                            double d3 = (double) (-this.mLegend.mNeededWidth);
                            Double.isNaN(d3);
                            double d4 = d3 / 2.0d;
                            double d5 = (double) xOffset;
                            Double.isNaN(d5);
                            d = d4 + d5;
                        } else {
                            f = lineHeight;
                            double d6 = (double) this.mLegend.mNeededWidth;
                            Double.isNaN(d6);
                            double d7 = d6 / 2.0d;
                            double d8 = (double) xOffset;
                            Double.isNaN(d8);
                            d = d7 - d8;
                        }
                        Double.isNaN(d2);
                        xOffset = (float) (d2 + d);
                        break;
                    }
                default:
                    f3 = convertDpToPixel4;
                    f = lineHeight;
                    f2 = lineSpacing;
                    float f6 = 0.0f;
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawForm(Canvas canvas, float f, float f2, LegendEntry legendEntry, Legend legend) {
        if (legendEntry.formColor != 1122868 && legendEntry.formColor != 1122867 && legendEntry.formColor != 0) {
            int save = canvas.save();
            LegendForm legendForm = legendEntry.form;
            if (legendForm == LegendForm.DEFAULT) {
                legendForm = legend.getForm();
            }
            this.mLegendFormPaint.setColor(legendEntry.formColor);
            float convertDpToPixel = Utils.convertDpToPixel(Float.isNaN(legendEntry.formSize) ? legend.getFormSize() : legendEntry.formSize);
            float f3 = convertDpToPixel / 2.0f;
            switch (legendForm) {
                case DEFAULT:
                case CIRCLE:
                    this.mLegendFormPaint.setStyle(Style.FILL);
                    canvas.drawCircle(f + f3, f2, f3, this.mLegendFormPaint);
                    break;
                case SQUARE:
                    this.mLegendFormPaint.setStyle(Style.FILL);
                    canvas.drawRect(f, f2 - f3, f + convertDpToPixel, f2 + f3, this.mLegendFormPaint);
                    break;
                case LINE:
                    float convertDpToPixel2 = Utils.convertDpToPixel(Float.isNaN(legendEntry.formLineWidth) ? legend.getFormLineWidth() : legendEntry.formLineWidth);
                    DashPathEffect formLineDashEffect = legendEntry.formLineDashEffect == null ? legend.getFormLineDashEffect() : legendEntry.formLineDashEffect;
                    this.mLegendFormPaint.setStyle(Style.STROKE);
                    this.mLegendFormPaint.setStrokeWidth(convertDpToPixel2);
                    this.mLegendFormPaint.setPathEffect(formLineDashEffect);
                    this.mLineFormPath.reset();
                    this.mLineFormPath.moveTo(f, f2);
                    this.mLineFormPath.lineTo(f + convertDpToPixel, f2);
                    canvas.drawPath(this.mLineFormPath, this.mLegendFormPaint);
                    break;
            }
            canvas.restoreToCount(save);
        }
    }

    /* access modifiers changed from: protected */
    public void drawLabel(Canvas canvas, float f, float f2, String str) {
        canvas.drawText(str, f, f2, this.mLegendLabelPaint);
    }
}
