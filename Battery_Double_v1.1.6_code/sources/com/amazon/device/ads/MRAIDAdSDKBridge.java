package com.amazon.device.ads;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;
import com.amazon.device.ads.AdEvent.AdEventType;
import com.amazon.device.ads.InAppBrowser.InAppBrowserBuilder;
import com.amazon.device.ads.JavascriptInteractor.Executor;
import com.amazon.device.ads.JavascriptInteractor.JavascriptMethodExecutor;
import com.amazon.device.ads.ThreadUtils.ExecutionStyle;
import com.amazon.device.ads.ThreadUtils.ExecutionThread;
import com.amazon.device.ads.ThreadUtils.ThreadRunner;
import com.amazon.device.ads.WebRequest.WebRequestException;
import com.amazon.device.ads.WebRequest.WebRequestFactory;
import com.amazon.device.ads.WebRequest.WebResponse;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

class MRAIDAdSDKBridge implements AdSDKBridge {
    private static final int CLOSE_BUTTON_SIZE = 50;
    private static final String CONTENT_DESCRIPTION_RESIZED_VIEW = "resizedView";
    private static final String ERROR_EVENT_FORMAT = "mraidBridge.error('%s', '%s');";
    private static final String JAVASCRIPT;
    private static final String LOGTAG = "MRAIDAdSDKBridge";
    private static final String MRAID_BRIDGE_NAME = "mraidObject";
    private static final String PLACEMENT_TYPE_INLINE = "inline";
    private static final String PLACEMENT_TYPE_INTERSTITIAL = "interstitial";
    /* access modifiers changed from: private */
    public final AdControlAccessor adControlAccessor;
    private final AdUtils2 adUtils;
    private final AlertDialogFactory alertDialogFactory;
    private final AndroidBuildInfo buildInfo;
    private final Position defaultPosition;
    private final ExpandProperties expandProperties;
    private boolean expandedWithUrl;
    /* access modifiers changed from: private */
    public final GraphicsUtils graphicsUtils;
    private final IntentBuilderFactory intentBuilderFactory;
    private final JavascriptInteractor javascriptInteractor;
    private final LayoutFactory layoutFactory;
    private final MobileAdsLogger logger;
    private final OrientationProperties orientationProperties;
    private final PermissionChecker permissionChecker;
    private final ResizeProperties resizeProperties;
    /* access modifiers changed from: private */
    public ViewGroup resizedView;
    private FrameLayout rootView;
    private SDKEventListener sdkEventListener;
    private final ThreadRunner threadRunner;
    /* access modifiers changed from: private */
    public final ViewUtils viewUtils;
    private final WebRequestFactory webRequestFactory;
    private final WebUtils2 webUtils;

    static class AlertDialogFactory {
        AlertDialogFactory() {
        }

        public Builder createBuilder(Context context) {
            return new Builder(context);
        }
    }

    private static class CloseJSIF extends JavascriptMethodExecutor {
        private static final String name = "Close";
        private final MRAIDAdSDKBridge bridge;

        public CloseJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.close();
            return null;
        }
    }

    private static class CreateCalendarEventJSIF extends JavascriptMethodExecutor {
        private static final String name = "CreateCalendarEvent";
        private final MRAIDAdSDKBridge bridge;

        public CreateCalendarEventJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.createCalendarEvent(JSONUtils.getStringFromJSON(jSONObject, "description", null), JSONUtils.getStringFromJSON(jSONObject, "location", null), JSONUtils.getStringFromJSON(jSONObject, "summary", null), JSONUtils.getStringFromJSON(jSONObject, "start", null), JSONUtils.getStringFromJSON(jSONObject, TtmlNode.END, null));
            return null;
        }
    }

    private static class DeregisterViewabilityInterestJSIF extends JavascriptMethodExecutor {
        private static final String name = "DeregisterViewabilityInterest";
        private final MRAIDAdSDKBridge bridge;

        public DeregisterViewabilityInterestJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        /* access modifiers changed from: protected */
        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.deregisterViewabilityInterest();
            return null;
        }
    }

    private static class ExpandJSIF extends JavascriptMethodExecutor {
        private static final String name = "Expand";
        private final MRAIDAdSDKBridge bridge;

        public ExpandJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.expand(JSONUtils.getStringFromJSON(jSONObject, "url", null));
            return null;
        }
    }

    private static class GetCurrentPositionJSIF extends JavascriptMethodExecutor {
        private static final String name = "GetCurrentPosition";
        private final MRAIDAdSDKBridge bridge;

        public GetCurrentPositionJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            return this.bridge.getCurrentPosition();
        }
    }

    private static class GetDefaultPositionJSIF extends JavascriptMethodExecutor {
        private static final String name = "GetDefaultPosition";
        private final MRAIDAdSDKBridge bridge;

        public GetDefaultPositionJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            return this.bridge.getDefaultPosition();
        }
    }

    private static class GetExpandPropertiesJSIF extends JavascriptMethodExecutor {
        private static final String name = "GetExpandProperties";
        private final MRAIDAdSDKBridge bridge;

        public GetExpandPropertiesJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            return this.bridge.getExpandPropertiesForCreative();
        }
    }

    private static class GetMaxSizeJSIF extends JavascriptMethodExecutor {
        private static final String name = "GetMaxSize";
        private final MRAIDAdSDKBridge bridge;

        public GetMaxSizeJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            return this.bridge.getMaxSize();
        }
    }

    private static class GetPlacementTypeJSIF extends JavascriptMethodExecutor {
        private static final String name = "GetPlacementType";
        private final MRAIDAdSDKBridge bridge;

        public GetPlacementTypeJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            JSONObject jSONObject2 = new JSONObject();
            JSONUtils.put(jSONObject2, "placementType", this.bridge.getPlacementType());
            return jSONObject2;
        }
    }

    private static class GetResizePropertiesJSIF extends JavascriptMethodExecutor {
        private static final String name = "GetResizeProperties";
        private final MRAIDAdSDKBridge bridge;

        public GetResizePropertiesJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            return this.bridge.getResizeProperties();
        }
    }

    private static class GetScreenSizeJSIF extends JavascriptMethodExecutor {
        private static final String name = "GetScreenSize";
        private final MRAIDAdSDKBridge bridge;

        public GetScreenSizeJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            return this.bridge.getScreenSize();
        }
    }

    private static class IsViewableJSIF extends JavascriptMethodExecutor {
        private static final String name = "IsViewable";
        private final MRAIDAdSDKBridge bridge;

        public IsViewableJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            JSONObject jSONObject2 = new JSONObject();
            JSONUtils.put(jSONObject2, ParametersKeys.IS_VIEWABLE, this.bridge.isViewable());
            return jSONObject2;
        }
    }

    private static class OpenJSIF extends JavascriptMethodExecutor {
        private static final String name = "Open";
        private final MRAIDAdSDKBridge bridge;

        public OpenJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.open(JSONUtils.getStringFromJSON(jSONObject, "url", null));
            return null;
        }
    }

    private static class PlayVideoJSIF extends JavascriptMethodExecutor {
        private static final String name = "PlayVideo";
        private final MRAIDAdSDKBridge bridge;

        public PlayVideoJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.playVideo(JSONUtils.getStringFromJSON(jSONObject, "url", null));
            return null;
        }
    }

    private static class RegisterViewabilityInterestJSIF extends JavascriptMethodExecutor {
        private static final String name = "RegisterViewabilityInterest";
        private final MRAIDAdSDKBridge bridge;

        public RegisterViewabilityInterestJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        /* access modifiers changed from: protected */
        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.registerViewabilityInterest();
            return null;
        }
    }

    private static class ResizeJSIF extends JavascriptMethodExecutor {
        private static final String name = "Resize";
        private final MRAIDAdSDKBridge bridge;

        public ResizeJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.resize();
            return null;
        }
    }

    private static class SetExpandPropertiesJSIF extends JavascriptMethodExecutor {
        private static final String name = "SetExpandProperties";
        private final MRAIDAdSDKBridge bridge;

        public SetExpandPropertiesJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.setExpandProperties(jSONObject);
            return null;
        }
    }

    private static class SetOrientationPropertiesJSIF extends JavascriptMethodExecutor {
        private static final String name = "SetOrientationProperties";
        private final MRAIDAdSDKBridge bridge;

        public SetOrientationPropertiesJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.setOrientationProperties(jSONObject);
            return null;
        }
    }

    private static class SetResizePropertiesJSIF extends JavascriptMethodExecutor {
        private static final String name = "SetResizeProperties";
        private final MRAIDAdSDKBridge bridge;

        public SetResizePropertiesJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.setResizeProperties(jSONObject);
            return null;
        }
    }

    private static class StorePictureJSIF extends JavascriptMethodExecutor {
        private static final String name = "StorePicture";
        private final MRAIDAdSDKBridge bridge;

        public StorePictureJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.storePicture(JSONUtils.getStringFromJSON(jSONObject, "url", null));
            return null;
        }
    }

    private static class SupportsJSIF extends JavascriptMethodExecutor {
        private static final String name = "Supports";
        private final MRAIDAdSDKBridge bridge;

        public SupportsJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            return this.bridge.getSupportedFeatures();
        }
    }

    private static class UseCustomCloseJSIF extends JavascriptMethodExecutor {
        private static final String name = "UseCustomClose";
        private final MRAIDAdSDKBridge bridge;

        public UseCustomCloseJSIF(MRAIDAdSDKBridge mRAIDAdSDKBridge) {
            super(name);
            this.bridge = mRAIDAdSDKBridge;
        }

        public JSONObject execute(JSONObject jSONObject) {
            this.bridge.setUseCustomClose(JSONUtils.getBooleanFromJSON(jSONObject, "useCustomClose", false));
            return null;
        }
    }

    public String getName() {
        return MRAID_BRIDGE_NAME;
    }

    public boolean hasNativeExecution() {
        return true;
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("(function (window, console) {\n    var is_array = function (obj) {\n        return Object.prototype.toString.call(obj) === '[object Array]';\n    },\n    registerViewabilityInterest = function(){\n       mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"RegisterViewabilityInterest\", null);\n    },\n    deregisterViewabilityInterest = function(){\n       mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"DeregisterViewabilityInterest\", null);\n    },\n    forEach = function (array, fn) {\n        var i;\n        for (i = 0; i < array.length; i++) {\n            if (i in array) {\n                fn.call(null, array[i], i);\n            }\n        }\n    },\n    events = {\n            error: 'error',\n            ready: 'ready',\n            sizeChange: 'sizeChange',\n            stateChange: 'stateChange',\n            viewableChange: 'viewableChange'\n    },\n    states = [\"loading\",\"default\",\"expanded\",\"resized\",\"hidden\"],\n    placementTypes = [\"inline\", \"interstitial\"],\n    listeners = [],\n    version = '2.0',\n    currentState = \"loading\",\n    currentlyViewable = false,\n    supportedFeatures = null,\n    orientationProperties = {\"allowOrientationChange\":true,\"forceOrientation\":\"none\"},\n    // Error Event fires listeners\n    invokeListeners = function(event, args) {\n        var eventListeners = listeners[event] || [];\n        // fire all the listeners\n        forEach(eventListeners, function(listener){\n            try {\n                listener.apply(null, args);\n            }catch(e){\n                debug(\"Error executing \" + event + \" listener\");\n                debug(e);\n            }\n        });\n    },\n    debug = function(msg) {\n        console.log(\"MRAID log: \" + msg);\n    },\n    readyEvent = function() {\n        debug(\"MRAID ready\");\n        invokeListeners(\"ready\");\n    },\n    errorEvent = function(message, action) {\n        debug(\"error: \" + message + \" action: \" + action);\n        var args = [message, action];\n        invokeListeners(\"error\", args);\n    },\n    stateChangeEvent = function(state) {\n        debug(\"stateChange: \" + state);\n        var args = [state];\n        currentState = state;\n        invokeListeners(\"stateChange\", args);\n    },\n    viewableChangeEvent = function(viewable) {\n        if (viewable != currentlyViewable) {            debug(\"viewableChange: \" + viewable);\n            var args = [viewable];\n            invokeListeners(\"viewableChange\", args);\n            currentlyViewable = viewable;\n        }\n    }, \n    sizeChangeEvent = function(width, height) {\n        debug(\"sizeChange: \" + width + \"x\" + height);\n        var args = [width, height];\n        invokeListeners(\"sizeChange\", args);\n    };\n    window.mraidBridge = {\n            error : errorEvent,\n            ready : readyEvent,\n            stateChange : stateChangeEvent,\n            sizeChange : sizeChangeEvent,\n            viewableChange : viewableChangeEvent\n    };\n    // Define the mraid object\n    window.mraid = {\n            // Command Flow\n            addEventListener : function(event, listener){\n                var eventListeners = listeners[event] || [],\n                alreadyRegistered = false;\n                \n                //verify the event is one that will actually occur\n                if (!events.hasOwnProperty(event)){\n                    return;\n                }\n                \n                //register first set of listeners for this event\n                if (!is_array(listeners[event])) {\n                    listeners[event] = eventListeners;\n                }\n                \n                forEach(eventListeners, function(l){ \n                    // Listener already registered, so no need to add it.\n                        if (listener === l){\n                            alreadyRegistered = true;\n                        }\n                    }\n                );\n                if (!alreadyRegistered){\n                    debug('Registering Listener for ' + event + ': ' + listener)\n                    listeners[event].push(listener);\n                    if (event = 'viewableChange'){ \n                       registerViewabilityInterest();  \n                    } \n                }\n            },\n            removeEventListener : function(event, listener){\n                if (listeners.hasOwnProperty(event)) {\n                    var eventListeners = listeners[event];\n                    if (eventListeners) {\n                        var idx = eventListeners.indexOf(listener);\n                        if (idx !== -1) {\n                           eventListeners.splice(idx, 1);\n                           if (event = 'viewableChange'){ \n                               deregisterViewabilityInterest();  \n                           } \n                        }\n                    }\n                }\n            },\n            useCustomClose: function(bool){\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"UseCustomClose\", JSON.stringify({useCustomClose: bool}));\n            },\n            // Support\n            supports: function(feature){\n                if (!supportedFeatures)\n                {\n                    supportedFeatures = JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"Supports\", null));\n                }\n                return supportedFeatures[feature];\n            },\n            // Properties\n            getVersion: function(){\n                return version;\n            },\n            getState: function(){\n                return currentState;\n            },\n            getPlacementType: function(){\n                var json = JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"GetPlacementType\", null));\n                return json.placementType;\n            },\n            isViewable: function(){\n                var json = JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"IsViewable\", null));\n                return json.isViewable;\n            },\n            getExpandProperties: function(){\n                return JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"GetExpandProperties\", null));\n            },\n            setExpandProperties: function(properties){\n                //Backwards compatibility with MRAID 1.0 creatives\n                if (!!properties.lockOrientation){\n                    mraid.setOrientationProperties({\"allowOrientationChange\":false});\n                }\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"SetExpandProperties\", JSON.stringify(properties));\n            },\n            getOrientationProperties: function(){\n                return orientationProperties;\n            },\n            setOrientationProperties: function(properties){\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"SetOrientationProperties\", JSON.stringify(properties));\n            },\n            getResizeProperties: function(){\n                return JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"GetResizeProperties\", null));\n            },\n            setResizeProperties: function(properties){\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"SetResizeProperties\", JSON.stringify(properties));\n            },\n            getCurrentPosition: function(){\n                return JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"GetCurrentPosition\", null));\n            },\n            getMaxSize: function(){\n                return JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"GetMaxSize\", null));\n            },\n            getDefaultPosition: function(){\n                return JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"GetDefaultPosition\", null));\n            },\n            getScreenSize: function(){\n                return JSON.parse(mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"GetScreenSize\", null));\n            },\n            // Operations\n            open: function(url) {\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"Open\", JSON.stringify({url: url}));\n            },\n            close: function() {\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"Close\", null);\n            },\n            expand: function(url) {\n                if (url !== undefined) {\n                    mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"Expand\", JSON.stringify({url: url}));\n                } else {\n                    mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"Expand\", JSON.stringify({url: \"\"}));\n                }\n            },\n            resize: function() {\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"Resize\", null);\n            },\n            createCalendarEvent: function(eventObject) {\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"CreateCalendarEvent\", JSON.stringify(eventObject));\n            },\n            playVideo: function(url){\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"PlayVideo\", JSON.stringify({url: url}));\n            },\n            storePicture: function(url){\n                mraidObject.");
        sb.append(JavascriptInteractor.getExecutorMethodName());
        sb.append("(\"StorePicture\", JSON.stringify({url: url}));\n            }\n    };\n})(window, console);\n");
        JAVASCRIPT = sb.toString();
    }

    MRAIDAdSDKBridge(AdControlAccessor adControlAccessor2, JavascriptInteractor javascriptInteractor2) {
        AdControlAccessor adControlAccessor3 = adControlAccessor2;
        JavascriptInteractor javascriptInteractor3 = javascriptInteractor2;
        PermissionChecker permissionChecker2 = r4;
        PermissionChecker permissionChecker3 = new PermissionChecker();
        MobileAdsLoggerFactory mobileAdsLoggerFactory = r5;
        MobileAdsLoggerFactory mobileAdsLoggerFactory2 = new MobileAdsLoggerFactory();
        WebRequestFactory webRequestFactory2 = r6;
        WebRequestFactory webRequestFactory3 = new WebRequestFactory();
        ThreadRunner threadRunner2 = ThreadUtils.getThreadRunner();
        GraphicsUtils graphicsUtils2 = r8;
        GraphicsUtils graphicsUtils3 = new GraphicsUtils();
        AlertDialogFactory alertDialogFactory2 = r9;
        AlertDialogFactory alertDialogFactory3 = new AlertDialogFactory();
        WebUtils2 webUtils2 = r10;
        WebUtils2 webUtils22 = new WebUtils2();
        AdUtils2 adUtils2 = r11;
        AdUtils2 adUtils22 = new AdUtils2();
        IntentBuilderFactory intentBuilderFactory2 = r12;
        IntentBuilderFactory intentBuilderFactory3 = new IntentBuilderFactory();
        ExpandProperties expandProperties2 = r13;
        ExpandProperties expandProperties3 = new ExpandProperties();
        OrientationProperties orientationProperties2 = r14;
        OrientationProperties orientationProperties3 = new OrientationProperties();
        Position position = r15;
        Position position2 = new Position();
        ResizeProperties resizeProperties2 = r16;
        ResizeProperties resizeProperties3 = new ResizeProperties();
        AndroidBuildInfo androidBuildInfo = r17;
        AndroidBuildInfo androidBuildInfo2 = new AndroidBuildInfo();
        LayoutFactory layoutFactory2 = r18;
        LayoutFactory layoutFactory3 = new LayoutFactory();
        ViewUtils viewUtils2 = r19;
        ViewUtils viewUtils3 = new ViewUtils();
        this(adControlAccessor3, javascriptInteractor3, permissionChecker2, mobileAdsLoggerFactory, webRequestFactory2, threadRunner2, graphicsUtils2, alertDialogFactory2, webUtils2, adUtils2, intentBuilderFactory2, expandProperties2, orientationProperties2, position, resizeProperties2, androidBuildInfo, layoutFactory2, viewUtils2);
    }

    MRAIDAdSDKBridge(AdControlAccessor adControlAccessor2, JavascriptInteractor javascriptInteractor2, PermissionChecker permissionChecker2, MobileAdsLoggerFactory mobileAdsLoggerFactory, WebRequestFactory webRequestFactory2, ThreadRunner threadRunner2, GraphicsUtils graphicsUtils2, AlertDialogFactory alertDialogFactory2, WebUtils2 webUtils2, AdUtils2 adUtils2, IntentBuilderFactory intentBuilderFactory2, ExpandProperties expandProperties2, OrientationProperties orientationProperties2, Position position, ResizeProperties resizeProperties2, AndroidBuildInfo androidBuildInfo, LayoutFactory layoutFactory2, ViewUtils viewUtils2) {
        this.expandedWithUrl = true;
        this.adControlAccessor = adControlAccessor2;
        this.javascriptInteractor = javascriptInteractor2;
        MobileAdsLoggerFactory mobileAdsLoggerFactory2 = mobileAdsLoggerFactory;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        this.permissionChecker = permissionChecker2;
        this.webRequestFactory = webRequestFactory2;
        this.threadRunner = threadRunner2;
        this.graphicsUtils = graphicsUtils2;
        this.alertDialogFactory = alertDialogFactory2;
        this.webUtils = webUtils2;
        this.adUtils = adUtils2;
        this.intentBuilderFactory = intentBuilderFactory2;
        this.expandProperties = expandProperties2;
        this.orientationProperties = orientationProperties2;
        this.defaultPosition = position;
        this.resizeProperties = resizeProperties2;
        this.buildInfo = androidBuildInfo;
        this.layoutFactory = layoutFactory2;
        this.viewUtils = viewUtils2;
        populateJavascriptExecutorsInInteractor();
    }

    private void populateJavascriptExecutorsInInteractor() {
        this.javascriptInteractor.addMethodExecutor(new CloseJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new CreateCalendarEventJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new ExpandJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new GetCurrentPositionJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new GetDefaultPositionJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new GetExpandPropertiesJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new GetMaxSizeJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new GetPlacementTypeJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new GetResizePropertiesJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new GetScreenSizeJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new OpenJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new PlayVideoJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new ResizeJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new SetExpandPropertiesJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new SetOrientationPropertiesJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new SetResizePropertiesJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new StorePictureJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new SupportsJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new UseCustomCloseJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new IsViewableJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new RegisterViewabilityInterestJSIF(this));
        this.javascriptInteractor.addMethodExecutor(new DeregisterViewabilityInterestJSIF(this));
    }

    public Executor getJavascriptInteractorExecutor() {
        return this.javascriptInteractor.getExecutor();
    }

    public String getJavascript() {
        return JAVASCRIPT;
    }

    public SDKEventListener getSDKEventListener() {
        if (this.sdkEventListener == null) {
            this.sdkEventListener = new MRAIDAdSDKEventListener(this);
        }
        return this.sdkEventListener;
    }

    /* access modifiers changed from: private */
    public Context getContext() {
        return this.adControlAccessor.getContext();
    }

    /* access modifiers changed from: 0000 */
    public void updateDefaultPosition(int i, int i2, int i3, int i4) {
        this.defaultPosition.setSize(new Size(i, i2));
        this.defaultPosition.setX(i3);
        this.defaultPosition.setY(i4);
    }

    public JSONObject getCurrentPosition() {
        if (this.adControlAccessor.getCurrentPosition() != null) {
            return this.adControlAccessor.getCurrentPosition().toJSONObject();
        }
        fireErrorEvent("Current position is unavailable because the ad has not yet been displayed.", "getCurrentPosition");
        return new Position(new Size(0, 0), 0, 0).toJSONObject();
    }

    public JSONObject getDefaultPosition() {
        return this.defaultPosition.toJSONObject();
    }

    public JSONObject getMaxSize() {
        Size maxSize = this.adControlAccessor.getMaxSize();
        if (maxSize == null) {
            return new Size(0, 0).toJSONObject();
        }
        return maxSize.toJSONObject();
    }

    public JSONObject getScreenSize() {
        Size screenSize = this.adControlAccessor.getScreenSize();
        if (screenSize == null) {
            return new Size(0, 0).toJSONObject();
        }
        return screenSize.toJSONObject();
    }

    public String getPlacementType() {
        return this.adControlAccessor.isInterstitial() ? "interstitial" : "inline";
    }

    public String getOrientationProperties() {
        return this.orientationProperties.toString();
    }

    public void setOrientationProperties(JSONObject jSONObject) {
        if (this.adControlAccessor.isInterstitial() && !this.adControlAccessor.isModal()) {
            this.adControlAccessor.orientationChangeAttemptedWhenNotAllowed();
        }
        this.orientationProperties.fromJSONObject(jSONObject);
        orientationPropertyChange();
    }

    public JSONObject getExpandPropertiesForCreative() {
        Size size;
        ExpandProperties clone = this.expandProperties.toClone();
        if (clone.getWidth() == -1) {
            size = this.adControlAccessor.getScreenSize();
            clone.setWidth(size.getWidth());
        } else {
            size = null;
        }
        if (clone.getHeight() == -1) {
            if (size == null) {
                size = this.adControlAccessor.getScreenSize();
            }
            clone.setHeight(size.getHeight());
        }
        return clone.toJSONObject();
    }

    public void setExpandProperties(JSONObject jSONObject) {
        this.expandProperties.fromJSONObject(jSONObject);
        showNativeCloseButtonIfNeeded();
    }

    public JSONObject getResizeProperties() {
        return this.resizeProperties.toJSONObject();
    }

    public void setResizeProperties(JSONObject jSONObject) {
        if (!this.resizeProperties.fromJSONObject(jSONObject)) {
            fireErrorEvent("Invalid resize properties", "setResizeProperties");
        } else if (this.resizeProperties.getWidth() < 50 || this.resizeProperties.getHeight() < 50) {
            fireErrorEvent("Resize properties width and height must be greater than 50dp in order to fit the close button.", "setResizeProperties");
            this.resizeProperties.reset();
        } else {
            Size maxSize = this.adControlAccessor.getMaxSize();
            if (this.resizeProperties.getWidth() > maxSize.getWidth() || this.resizeProperties.getHeight() > maxSize.getHeight()) {
                fireErrorEvent("Resize properties width and height cannot be larger than the maximum size.", "setResizeProperties");
                this.resizeProperties.reset();
            } else if (this.resizeProperties.getAllowOffscreen()) {
                Size computeResizeSizeInPixels = computeResizeSizeInPixels(this.resizeProperties);
                int deviceIndependentPixelToPixel = this.adUtils.deviceIndependentPixelToPixel(this.defaultPosition.getX() + this.resizeProperties.getOffsetX());
                if (!isValidClosePosition(RelativePosition.fromString(this.resizeProperties.getCustomClosePosition()), this.adUtils.deviceIndependentPixelToPixel(this.defaultPosition.getY() + this.resizeProperties.getOffsetY()), deviceIndependentPixelToPixel, computeResizeSizeInPixels, this.adUtils.deviceIndependentPixelToPixel(maxSize.getWidth()), this.adUtils.deviceIndependentPixelToPixel(maxSize.getHeight()))) {
                    fireErrorEvent("Invalid resize properties. Close event area must be entirely on screen.", "setResizeProperties");
                    this.resizeProperties.reset();
                }
            }
        }
    }

    public void setUseCustomClose(boolean z) {
        this.expandProperties.setUseCustomClose(Boolean.valueOf(z));
        showNativeCloseButtonIfNeeded();
    }

    private void showNativeCloseButtonIfNeeded() {
        if (this.adControlAccessor.isModal()) {
            this.adControlAccessor.showNativeCloseButtonImage(!this.expandProperties.getUseCustomClose().booleanValue());
        }
    }

    public void close() {
        if (!this.adControlAccessor.closeAd()) {
            fireErrorEvent("Unable to close ad in its current state.", "close");
        }
    }

    /* access modifiers changed from: private */
    public AdControlAccessor getAdControlAccessor() {
        return this.adControlAccessor;
    }

    public void expand(String str) {
        if (this.adControlAccessor.isInterstitial()) {
            fireErrorEvent("Unable to expand an interstitial ad placement", "expand");
        } else if (this.adControlAccessor.isModal()) {
            fireErrorEvent("Unable to expand while expanded.", "expand");
        } else if (!this.adControlAccessor.isVisible()) {
            fireErrorEvent("Unable to expand ad while it is not visible.", "expand");
        } else if ((this.expandProperties.getWidth() >= 50 || this.expandProperties.getWidth() == -1) && (this.expandProperties.getHeight() >= 50 || this.expandProperties.getHeight() == -1)) {
            if (StringUtils.isNullOrWhiteSpace(str)) {
                AdControllerFactory.cacheAdControlAccessor(this.adControlAccessor);
                launchExpandActivity(null, this.expandProperties);
            } else if (this.webUtils.isUrlValid(str)) {
                final ExpandProperties clone = this.expandProperties.toClone();
                this.adControlAccessor.preloadUrl(str, new PreloadCallback() {
                    public void onPreloadComplete(String str) {
                        MRAIDAdSDKBridge.this.getAdControlAccessor().injectJavascriptPreload("mraidBridge.stateChange('expanded');");
                        MRAIDAdSDKBridge.this.getAdControlAccessor().injectJavascriptPreload("mraidBridge.ready();");
                        AdControllerFactory.cacheAdControlAccessor(MRAIDAdSDKBridge.this.getAdControlAccessor());
                        MRAIDAdSDKBridge.this.launchExpandActivity(str, clone);
                    }
                });
            } else {
                fireErrorEvent("Unable to expand with invalid URL.", "expand");
            }
        } else {
            fireErrorEvent("Expand size is too small, must leave room for close.", "expand");
        }
    }

    /* access modifiers changed from: private */
    public void launchExpandActivity(String str, ExpandProperties expandProperties2) {
        if (this.intentBuilderFactory.createIntentBuilder().withClass(AdActivity.class).withContext(this.adControlAccessor.getContext().getApplicationContext()).withExtra("adapter", ModalAdActivityAdapter.class.getName()).withExtra("url", str).withExtra("expandProperties", expandProperties2.toString()).withExtra("orientationProperties", this.orientationProperties.toString()).fireIntent()) {
            this.logger.d("Successfully expanded ad");
        }
    }

    public void resize() {
        if (this.adControlAccessor.isInterstitial()) {
            fireErrorEvent("Unable to resize an interstitial ad placement.", MraidJsMethods.RESIZE);
        } else if (this.adControlAccessor.isModal()) {
            fireErrorEvent("Unable to resize while expanded.", MraidJsMethods.RESIZE);
        } else if (!this.adControlAccessor.isVisible()) {
            fireErrorEvent("Unable to resize ad while it is not visible.", MraidJsMethods.RESIZE);
        } else if (this.resizeProperties == null || !this.resizeProperties.areResizePropertiesSet()) {
            fireErrorEvent("Resize properties must be set before calling resize.", MraidJsMethods.RESIZE);
        } else {
            resizeAd(this.resizeProperties);
        }
    }

    public void playVideo(String str) {
        if (!this.adControlAccessor.isVisible()) {
            fireErrorEvent("Unable to play a video while the ad is not visible", MraidJsMethods.PLAY_VIDEO);
        } else if (StringUtils.isNullOrEmpty(str)) {
            fireErrorEvent("Unable to play a video without a URL", MraidJsMethods.PLAY_VIDEO);
        } else {
            try {
                Bundle bundle = new Bundle();
                bundle.putString("url", str);
                Intent intent = new Intent(getContext(), AdActivity.class);
                intent.putExtra("adapter", VideoActionHandler.class.getName());
                intent.putExtras(bundle);
                getContext().startActivity(intent);
            } catch (ActivityNotFoundException unused) {
                this.logger.d("Failed to open VideoAction activity");
                fireErrorEvent("Internal SDK Failure. Unable to launch VideoActionHandler", MraidJsMethods.PLAY_VIDEO);
            }
        }
    }

    public void open(String str) {
        if (!this.adControlAccessor.isVisible()) {
            fireErrorEvent("Unable to open a URL while the ad is not visible", MraidJsMethods.OPEN);
            return;
        }
        MobileAdsLogger mobileAdsLogger = this.logger;
        StringBuilder sb = new StringBuilder();
        sb.append("Opening URL ");
        sb.append(str);
        mobileAdsLogger.d(sb.toString());
        if (this.webUtils.isUrlValid(str)) {
            String scheme = WebUtils.getScheme(str);
            if ("http".equals(scheme) || "https".equals(scheme)) {
                new InAppBrowserBuilder().withContext(getContext()).withExternalBrowserButton().withUrl(str).show();
            } else {
                this.adControlAccessor.loadUrl(str);
            }
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("URL ");
            sb2.append(str);
            sb2.append(" is not a valid URL");
            String sb3 = sb2.toString();
            this.logger.d(sb3);
            fireErrorEvent(sb3, MraidJsMethods.OPEN);
        }
    }

    public JSONObject getSupportedFeatures() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("sms", getContext().getPackageManager().hasSystemFeature("android.hardware.telephony"));
            jSONObject.put("tel", getContext().getPackageManager().hasSystemFeature("android.hardware.telephony"));
            jSONObject.put(MRAIDNativeFeature.CALENDAR, AndroidTargetUtils.isAtLeastAndroidAPI(14));
            jSONObject.put(MRAIDNativeFeature.STORE_PICTURE, this.permissionChecker.hasWriteExternalStoragePermission(getContext()));
            jSONObject.put(MRAIDNativeFeature.INLINE_VIDEO, AndroidTargetUtils.isAtLeastAndroidAPI(11));
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public void createCalendarEvent(String str, String str2, String str3, String str4, String str5) {
        if (!AndroidTargetUtils.isAtLeastAndroidAPI(14)) {
            this.logger.d("API version does not support calendar operations.");
            fireErrorEvent("API version does not support calendar operations.", "createCalendarEvent");
            return;
        }
        try {
            CalendarEventParameters calendarEventParameters = new CalendarEventParameters(str, str2, str3, str4, str5);
            createCalendarIntent(calendarEventParameters);
        } catch (IllegalArgumentException e) {
            this.logger.d(e.getMessage());
            fireErrorEvent(e.getMessage(), "createCalendarEvent");
        }
    }

    @TargetApi(14)
    private void createCalendarIntent(CalendarEventParameters calendarEventParameters) {
        Intent type = new Intent("android.intent.action.INSERT").setType("vnd.android.cursor.item/event");
        type.putExtra("title", calendarEventParameters.getDescription());
        if (!StringUtils.isNullOrEmpty(calendarEventParameters.getLocation())) {
            type.putExtra("eventLocation", calendarEventParameters.getLocation());
        }
        if (!StringUtils.isNullOrEmpty(calendarEventParameters.getSummary())) {
            type.putExtra("description", calendarEventParameters.getSummary());
        }
        type.putExtra("beginTime", calendarEventParameters.getStart().getTime());
        if (calendarEventParameters.getEnd() != null) {
            type.putExtra("endTime", calendarEventParameters.getEnd().getTime());
        }
        getContext().startActivity(type);
    }

    public void storePicture(final String str) {
        if (!this.permissionChecker.hasWriteExternalStoragePermission(getContext())) {
            fireErrorEvent("Picture could not be stored because permission was denied.", MRAIDNativeFeature.STORE_PICTURE);
        } else {
            this.threadRunner.execute(new Runnable() {
                public void run() {
                    MRAIDAdSDKBridge.this.fetchPicture(str);
                }
            }, ExecutionStyle.RUN_ASAP, ExecutionThread.BACKGROUND_THREAD);
        }
    }

    /* access modifiers changed from: private */
    public void fetchPicture(String str) {
        WebRequest createWebRequest = this.webRequestFactory.createWebRequest();
        createWebRequest.enableLog(true);
        createWebRequest.setUrlString(str);
        try {
            WebResponse makeCall = createWebRequest.makeCall();
            if (makeCall == null) {
                fireErrorEvent("Server could not be contacted to download picture.", MRAIDNativeFeature.STORE_PICTURE);
                return;
            }
            final Bitmap readAsBitmap = new ImageResponseReader(makeCall.getResponseReader(), this.graphicsUtils).readAsBitmap();
            if (readAsBitmap == null) {
                fireErrorEvent("Picture could not be retrieved from server.", MRAIDNativeFeature.STORE_PICTURE);
            } else {
                this.threadRunner.execute(new Runnable() {
                    public void run() {
                        MRAIDAdSDKBridge.this.savePicture(readAsBitmap);
                    }
                }, ExecutionStyle.SCHEDULE, ExecutionThread.MAIN_THREAD);
            }
        } catch (WebRequestException unused) {
            fireErrorEvent("Server could not be contacted to download picture.", MRAIDNativeFeature.STORE_PICTURE);
        }
    }

    /* access modifiers changed from: private */
    public void savePicture(final Bitmap bitmap) {
        Builder createBuilder = this.alertDialogFactory.createBuilder(getContext());
        createBuilder.setTitle("Would you like to save the image to your gallery?");
        createBuilder.setPositiveButton("Yes", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String insertImageInMediaStore = MRAIDAdSDKBridge.this.graphicsUtils.insertImageInMediaStore(MRAIDAdSDKBridge.this.getContext(), bitmap, "AdImage", "Image created by rich media ad.");
                if (StringUtils.isNullOrEmpty(insertImageInMediaStore)) {
                    MRAIDAdSDKBridge.this.fireErrorEvent("Picture could not be stored to device.", MRAIDNativeFeature.STORE_PICTURE);
                    return;
                }
                MediaScannerConnection.scanFile(MRAIDAdSDKBridge.this.getContext(), new String[]{insertImageInMediaStore}, null, null);
            }
        });
        createBuilder.setNegativeButton("No", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MRAIDAdSDKBridge.this.fireErrorEvent("User chose not to store image.", MRAIDNativeFeature.STORE_PICTURE);
            }
        });
        createBuilder.show();
    }

    /* access modifiers changed from: 0000 */
    public void collapseExpandedAd(final AdControlAccessor adControlAccessor2) {
        MobileAdsLogger mobileAdsLogger = this.logger;
        StringBuilder sb = new StringBuilder();
        sb.append("Collapsing expanded ad ");
        sb.append(this);
        mobileAdsLogger.d(sb.toString());
        this.threadRunner.execute(new Runnable() {
            public void run() {
                MRAIDAdSDKBridge.this.collapseExpandedAdOnThread(adControlAccessor2);
            }
        }, ExecutionStyle.RUN_ASAP, ExecutionThread.MAIN_THREAD);
    }

    /* access modifiers changed from: private */
    @SuppressLint({"InlinedApi"})
    public void collapseExpandedAdOnThread(final AdControlAccessor adControlAccessor2) {
        adControlAccessor2.setAdActivity(null);
        if (this.expandedWithUrl) {
            this.logger.d("Expanded With URL");
            adControlAccessor2.popView();
        } else {
            this.logger.d("Not Expanded with URL");
        }
        adControlAccessor2.moveViewBackToParent(new LayoutParams(-1, -1, 17));
        adControlAccessor2.removeCloseButton();
        adControlAccessor2.fireAdEvent(new AdEvent(AdEventType.CLOSED));
        adControlAccessor2.injectJavascript("mraidBridge.stateChange('default');");
        adControlAccessor2.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                adControlAccessor2.removeOnGlobalLayoutListener(this);
                MRAIDAdSDKBridge.this.reportSizeChangeEvent();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void resizeAd(final ResizeProperties resizeProperties2) {
        final Size computeResizeSizeInPixels = computeResizeSizeInPixels(resizeProperties2);
        this.threadRunner.execute(new Runnable() {
            public void run() {
                MRAIDAdSDKBridge.this.resizeAdOnThread(resizeProperties2, computeResizeSizeInPixels);
            }
        }, ExecutionStyle.RUN_ASAP, ExecutionThread.MAIN_THREAD);
    }

    /* access modifiers changed from: private */
    public void resizeAdOnThread(final ResizeProperties resizeProperties2, final Size size) {
        Size maxSize = this.adControlAccessor.getMaxSize();
        if (maxSize == null) {
            this.adControlAccessor.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    MRAIDAdSDKBridge.this.adControlAccessor.removeOnGlobalLayoutListener(this);
                    MRAIDAdSDKBridge.this.resizeAdHelper(resizeProperties2, size, MRAIDAdSDKBridge.this.adControlAccessor.getMaxSize());
                }
            });
        } else {
            resizeAdHelper(resizeProperties2, size, maxSize);
        }
    }

    /* access modifiers changed from: private */
    public void resizeAdHelper(ResizeProperties resizeProperties2, Size size, Size size2) {
        if (size2 == null) {
            this.logger.d("Size is null");
            return;
        }
        createResizedView();
        int deviceIndependentPixelToPixel = this.adUtils.deviceIndependentPixelToPixel(this.defaultPosition.getX() + resizeProperties2.getOffsetX());
        int deviceIndependentPixelToPixel2 = this.adUtils.deviceIndependentPixelToPixel(this.defaultPosition.getY() + resizeProperties2.getOffsetY());
        RelativePosition fromString = RelativePosition.fromString(resizeProperties2.getCustomClosePosition());
        int deviceIndependentPixelToPixel3 = this.adUtils.deviceIndependentPixelToPixel(size2.getWidth());
        int deviceIndependentPixelToPixel4 = this.adUtils.deviceIndependentPixelToPixel(size2.getHeight());
        if (!resizeProperties2.getAllowOffscreen()) {
            if (size.getWidth() > deviceIndependentPixelToPixel3) {
                size.setWidth(deviceIndependentPixelToPixel3);
            }
            if (size.getHeight() > deviceIndependentPixelToPixel4) {
                size.setHeight(deviceIndependentPixelToPixel4);
            }
            if (deviceIndependentPixelToPixel < 0) {
                deviceIndependentPixelToPixel = 0;
            } else if (size.getWidth() + deviceIndependentPixelToPixel > deviceIndependentPixelToPixel3) {
                deviceIndependentPixelToPixel = deviceIndependentPixelToPixel3 - size.getWidth();
            }
            if (deviceIndependentPixelToPixel2 < 0) {
                deviceIndependentPixelToPixel2 = 0;
            } else if (size.getHeight() + deviceIndependentPixelToPixel2 > deviceIndependentPixelToPixel4) {
                deviceIndependentPixelToPixel2 = deviceIndependentPixelToPixel4 - size.getHeight();
            }
        } else if (!isValidClosePosition(fromString, deviceIndependentPixelToPixel2, deviceIndependentPixelToPixel, size, deviceIndependentPixelToPixel3, deviceIndependentPixelToPixel4)) {
            fireErrorEvent("Resize failed because close event area must be entirely on screen.", MraidJsMethods.RESIZE);
            return;
        }
        this.adControlAccessor.moveViewToViewGroup(this.resizedView, new RelativeLayout.LayoutParams(size.getWidth(), size.getHeight()), false);
        LayoutParams layoutParams = new LayoutParams(size.getWidth(), size.getHeight());
        layoutParams.gravity = 48;
        layoutParams.leftMargin = deviceIndependentPixelToPixel;
        layoutParams.topMargin = deviceIndependentPixelToPixel2;
        if (this.rootView.equals(this.resizedView.getParent())) {
            this.resizedView.setLayoutParams(layoutParams);
        } else {
            this.rootView.addView(this.resizedView, layoutParams);
        }
        this.adControlAccessor.enableCloseButton(false, fromString);
        final ViewTreeObserver viewTreeObserver = this.resizedView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MRAIDAdSDKBridge.this.viewUtils.removeOnGlobalLayoutListener(viewTreeObserver, this);
                int[] iArr = new int[2];
                MRAIDAdSDKBridge.this.resizedView.getLocationOnScreen(iArr);
                Rect rect = new Rect(iArr[0], iArr[1], iArr[0] + MRAIDAdSDKBridge.this.resizedView.getWidth(), iArr[1] + MRAIDAdSDKBridge.this.resizedView.getHeight());
                AdEvent adEvent = new AdEvent(AdEventType.RESIZED);
                adEvent.setParameter(AdEvent.POSITION_ON_SCREEN, rect);
                MRAIDAdSDKBridge.this.adControlAccessor.fireAdEvent(adEvent);
                MRAIDAdSDKBridge.this.adControlAccessor.injectJavascript("mraidBridge.stateChange('resized');");
                MRAIDAdSDKBridge.this.reportSizeChangeEvent();
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isViewable() {
        return this.adControlAccessor.isViewable();
    }

    private boolean isValidClosePosition(RelativePosition relativePosition, int i, int i2, Size size, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int deviceIndependentPixelToPixel = this.adUtils.deviceIndependentPixelToPixel(50);
        switch (relativePosition) {
            case TOP_LEFT:
                i6 = i + deviceIndependentPixelToPixel;
                i5 = i2 + deviceIndependentPixelToPixel;
                break;
            case TOP_RIGHT:
                int width = size.getWidth() + i2;
                i7 = width - deviceIndependentPixelToPixel;
                i5 = width;
                i6 = i + deviceIndependentPixelToPixel;
                break;
            case TOP_CENTER:
                i2 = ((size.getWidth() / 2) + i2) - (deviceIndependentPixelToPixel / 2);
                i6 = i + deviceIndependentPixelToPixel;
                i5 = i2 + deviceIndependentPixelToPixel;
                break;
            case BOTTOM_LEFT:
                i6 = size.getHeight() + i;
                i = i6 - deviceIndependentPixelToPixel;
                i5 = i2 + deviceIndependentPixelToPixel;
                break;
            case BOTTOM_RIGHT:
                i6 = size.getHeight() + i;
                int width2 = size.getWidth() + i2;
                i7 = width2 - deviceIndependentPixelToPixel;
                i5 = width2;
                i = i6 - deviceIndependentPixelToPixel;
                break;
            case BOTTOM_CENTER:
                i6 = size.getHeight() + i;
                i2 = ((size.getWidth() / 2) + i2) - (deviceIndependentPixelToPixel / 2);
                i = i6 - deviceIndependentPixelToPixel;
                i5 = i2 + deviceIndependentPixelToPixel;
                break;
            case CENTER:
                int height = (size.getHeight() / 2) + i;
                int i8 = deviceIndependentPixelToPixel / 2;
                int i9 = height - i8;
                i2 = ((size.getWidth() / 2) + i2) - i8;
                i5 = i2 + deviceIndependentPixelToPixel;
                i = i9;
                i6 = i9 + deviceIndependentPixelToPixel;
                break;
            default:
                i6 = 0;
                i = 0;
                i2 = 0;
                i5 = 0;
                break;
        }
        i2 = i7;
        if (i < 0 || i2 < 0 || i6 > i4 || i5 > i3) {
            return false;
        }
        return true;
    }

    private Size computeResizeSizeInPixels(ResizeProperties resizeProperties2) {
        return new Size(this.adUtils.deviceIndependentPixelToPixel(resizeProperties2.getWidth()), this.adUtils.deviceIndependentPixelToPixel(resizeProperties2.getHeight()));
    }

    private void createResizedView() {
        if (this.resizedView == null) {
            if (this.rootView == null) {
                this.rootView = (FrameLayout) this.adControlAccessor.getRootView();
            }
            this.resizedView = this.layoutFactory.createLayout(getContext(), LayoutType.RELATIVE_LAYOUT, CONTENT_DESCRIPTION_RESIZED_VIEW);
        }
    }

    /* access modifiers changed from: 0000 */
    public void reportSizeChangeEvent() {
        Position currentPosition = this.adControlAccessor.getCurrentPosition();
        if (currentPosition != null) {
            AdControlAccessor adControlAccessor2 = this.adControlAccessor;
            StringBuilder sb = new StringBuilder();
            sb.append("mraidBridge.sizeChange(");
            sb.append(currentPosition.getSize().getWidth());
            sb.append(",");
            sb.append(currentPosition.getSize().getHeight());
            sb.append(");");
            adControlAccessor2.injectJavascript(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public void orientationPropertyChange() {
        if (this.adControlAccessor.isVisible() && this.adControlAccessor.isModal()) {
            Activity adActivity = this.adControlAccessor.getAdActivity();
            if (adActivity == null) {
                this.logger.e("unable to handle orientation property change on a non-expanded ad");
                return;
            }
            int requestedOrientation = adActivity.getRequestedOrientation();
            Position currentPosition = this.adControlAccessor.getCurrentPosition();
            MobileAdsLogger mobileAdsLogger = this.logger;
            StringBuilder sb = new StringBuilder();
            sb.append("Current Orientation: ");
            sb.append(requestedOrientation);
            mobileAdsLogger.d(sb.toString());
            switch (this.orientationProperties.getForceOrientation()) {
                case PORTRAIT:
                    adActivity.setRequestedOrientation(7);
                    break;
                case LANDSCAPE:
                    adActivity.setRequestedOrientation(6);
                    break;
            }
            if (ForceOrientation.NONE.equals(this.orientationProperties.getForceOrientation())) {
                if (this.orientationProperties.isAllowOrientationChange().booleanValue()) {
                    if (adActivity.getRequestedOrientation() != -1) {
                        adActivity.setRequestedOrientation(-1);
                    }
                } else if (this.adControlAccessor.isModal()) {
                    adActivity.setRequestedOrientation(DisplayUtils.determineCanonicalScreenOrientation(adActivity, this.buildInfo));
                }
            }
            int requestedOrientation2 = adActivity.getRequestedOrientation();
            MobileAdsLogger mobileAdsLogger2 = this.logger;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("New Orientation: ");
            sb2.append(requestedOrientation2);
            mobileAdsLogger2.d(sb2.toString());
            if (!(requestedOrientation2 == requestedOrientation || currentPosition == null)) {
                if (currentPosition.getSize().getWidth() != this.adControlAccessor.getCurrentPosition().getSize().getWidth()) {
                    this.adControlAccessor.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                        public void onGlobalLayout() {
                            MRAIDAdSDKBridge.this.adControlAccessor.removeOnGlobalLayoutListener(this);
                            MRAIDAdSDKBridge.this.reportSizeChangeEvent();
                        }
                    });
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void registerViewabilityInterest() {
        this.adControlAccessor.registerViewabilityInterest();
    }

    /* access modifiers changed from: private */
    public void deregisterViewabilityInterest() {
        this.adControlAccessor.deregisterViewabilityInterest();
    }

    /* access modifiers changed from: private */
    public void fireErrorEvent(String str, String str2) {
        this.adControlAccessor.injectJavascript(String.format(Locale.US, ERROR_EVENT_FORMAT, new Object[]{str, str2}));
    }
}
