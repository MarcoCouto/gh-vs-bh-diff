package com.amazon.device.ads;

import android.annotation.SuppressLint;
import com.amazon.device.ads.Configuration.ConfigOption;
import com.amazon.device.ads.JSONUtils.JSONUtilities;
import com.amazon.device.ads.WebRequest.HttpMethod;
import com.amazon.device.ads.WebRequest.WebRequestFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class AdRequest {
    private static final String LOGTAG = "AdRequest";
    private static final AAXParameter<?>[] PARAMETERS = {AAXParameter.APP_KEY, AAXParameter.CHANNEL, AAXParameter.PUBLISHER_KEYWORDS, AAXParameter.PUBLISHER_ASINS, AAXParameter.USER_AGENT, AAXParameter.SDK_VERSION, AAXParameter.GEOLOCATION, AAXParameter.DEVICE_INFO, AAXParameter.GDPR, AAXParameter.PACKAGE_INFO, AAXParameter.TEST, AAXParameter.OPT_OUT};
    private static final AAXParameterGroup[] PARAMETER_GROUPS = {AAXParameterGroup.USER_ID, AAXParameterGroup.PUBLISHER_EXTRA_PARAMETERS};
    private Info advertisingIdentifierInfo;
    private final Configuration configuration;
    private final ConnectionInfo connectionInfo;
    private final DebugProperties debugProperties;
    private String instrPixelUrl;
    private final JSONObjectBuilder jsonObjectBuilder;
    private final JSONUtilities jsonUtilities;
    private final MobileAdsLogger logger;
    private final AdTargetingOptions opt;
    private final String orientation;
    protected final Map<Integer, LOISlot> slots;
    private final WebRequestFactory webRequestFactory;

    static class AdRequestBuilder {
        private AdTargetingOptions adTargetingOptions;
        private Info advertisingIdentifierInfo;

        AdRequestBuilder() {
        }

        public AdRequestBuilder withAdTargetingOptions(AdTargetingOptions adTargetingOptions2) {
            this.adTargetingOptions = adTargetingOptions2;
            return this;
        }

        public AdRequestBuilder withAdvertisingIdentifierInfo(Info info) {
            this.advertisingIdentifierInfo = info;
            return this;
        }

        public AdRequest build() {
            return new AdRequest(this.adTargetingOptions).setAdvertisingIdentifierInfo(this.advertisingIdentifierInfo);
        }
    }

    static class JSONObjectBuilder {
        private AAXParameterGroup[] aaxParameterGroups;
        private AAXParameter<?>[] aaxParameters;
        private Map<String, String> advancedOptions;
        private final JSONObject json;
        private final MobileAdsLogger logger;
        private ParameterData parameterData;

        JSONObjectBuilder(MobileAdsLogger mobileAdsLogger) {
            this(mobileAdsLogger, new JSONObject());
        }

        JSONObjectBuilder(MobileAdsLogger mobileAdsLogger, JSONObject jSONObject) {
            this.logger = mobileAdsLogger;
            this.json = jSONObject;
        }

        /* access modifiers changed from: 0000 */
        public JSONObjectBuilder setAAXParameters(AAXParameter<?>[] aAXParameterArr) {
            this.aaxParameters = aAXParameterArr;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public JSONObjectBuilder setAAXParameterGroups(AAXParameterGroup[] aAXParameterGroupArr) {
            this.aaxParameterGroups = aAXParameterGroupArr;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public JSONObjectBuilder setAdvancedOptions(Map<String, String> map) {
            this.advancedOptions = map;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public JSONObjectBuilder setParameterData(ParameterData parameterData2) {
            this.parameterData = parameterData2;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public ParameterData getParameterData() {
            return this.parameterData;
        }

        /* access modifiers changed from: 0000 */
        public JSONObject getJSON() {
            return this.json;
        }

        /* access modifiers changed from: 0000 */
        public void build() {
            AAXParameter<?>[] aAXParameterArr;
            if (this.aaxParameterGroups != null) {
                for (AAXParameterGroup evaluate : this.aaxParameterGroups) {
                    evaluate.evaluate(this.parameterData, this.json);
                }
            }
            for (AAXParameter<?> aAXParameter : this.aaxParameters) {
                putIntoJSON(aAXParameter, aAXParameter.getValue(this.parameterData));
            }
            if (this.advancedOptions != null) {
                for (Entry entry : this.advancedOptions.entrySet()) {
                    if (!StringUtils.isNullOrWhiteSpace((String) entry.getValue())) {
                        putIntoJSON((String) entry.getKey(), entry.getValue());
                    }
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void putIntoJSON(AAXParameter<?> aAXParameter, Object obj) {
            putIntoJSON(aAXParameter.getName(), obj);
        }

        /* access modifiers changed from: 0000 */
        public void putIntoJSON(String str, Object obj) {
            if (obj != null) {
                try {
                    this.json.put(str, obj);
                } catch (JSONException unused) {
                    this.logger.d("Could not add parameter to JSON %s: %s", str, obj);
                }
            }
        }
    }

    static class LOISlot {
        static final AAXParameter<?>[] PARAMETERS = {AAXParameter.SIZE, AAXParameter.PAGE_TYPE, AAXParameter.SLOT, AAXParameter.SLOT_POSITION, AAXParameter.MAX_SIZE, AAXParameter.SLOT_ID, AAXParameter.FLOOR_PRICE, AAXParameter.SUPPORTED_MEDIA_TYPES, AAXParameter.VIDEO_OPTIONS};
        private final AdSlot adSlot;
        private final DebugProperties debugProperties;
        private final JSONObjectBuilder jsonObjectBuilder;
        private final JSONUtilities jsonUtilities;
        private final AdTargetingOptions opt;

        LOISlot(AdSlot adSlot2, AdRequest adRequest, MobileAdsLogger mobileAdsLogger) {
            this(adSlot2, adRequest, mobileAdsLogger, new JSONObjectBuilder(mobileAdsLogger), DebugProperties.getInstance(), new JSONUtilities());
        }

        LOISlot(AdSlot adSlot2, AdRequest adRequest, MobileAdsLogger mobileAdsLogger, JSONObjectBuilder jSONObjectBuilder, DebugProperties debugProperties2, JSONUtilities jSONUtilities) {
            this.opt = adSlot2.getAdTargetingOptions();
            this.adSlot = adSlot2;
            this.debugProperties = debugProperties2;
            this.jsonUtilities = jSONUtilities;
            HashMap copyOfAdvancedOptions = this.opt.getCopyOfAdvancedOptions();
            if (this.debugProperties.containsDebugProperty(DebugProperties.DEBUG_ADVTARGETING)) {
                JSONObject debugPropertyAsJSONObject = this.debugProperties.getDebugPropertyAsJSONObject(DebugProperties.DEBUG_ADVTARGETING, null);
                if (debugPropertyAsJSONObject != null) {
                    copyOfAdvancedOptions.putAll(this.jsonUtilities.createMapFromJSON(debugPropertyAsJSONObject));
                }
            }
            this.jsonObjectBuilder = jSONObjectBuilder.setAAXParameters(PARAMETERS).setAdvancedOptions(copyOfAdvancedOptions).setParameterData(new ParameterData().setAdTargetingOptions(this.opt).setAdvancedOptions(copyOfAdvancedOptions).setLOISlot(this).setAdRequest(adRequest));
        }

        /* access modifiers changed from: 0000 */
        public AdTargetingOptions getAdTargetingOptions() {
            return this.opt;
        }

        /* access modifiers changed from: 0000 */
        public JSONObject getJSON() {
            this.jsonObjectBuilder.build();
            return this.jsonObjectBuilder.getJSON();
        }

        /* access modifiers changed from: 0000 */
        public AdSlot getAdSlot() {
            return this.adSlot;
        }
    }

    public AdRequest(AdTargetingOptions adTargetingOptions) {
        this(adTargetingOptions, new WebRequestFactory(), MobileAdsInfoStore.getInstance(), Configuration.getInstance(), DebugProperties.getInstance(), new MobileAdsLoggerFactory(), new JSONUtilities(), new ConnectionInfo(MobileAdsInfoStore.getInstance()));
    }

    @SuppressLint({"UseSparseArrays"})
    AdRequest(AdTargetingOptions adTargetingOptions, WebRequestFactory webRequestFactory2, MobileAdsInfoStore mobileAdsInfoStore, Configuration configuration2, DebugProperties debugProperties2, MobileAdsLoggerFactory mobileAdsLoggerFactory, JSONUtilities jSONUtilities, ConnectionInfo connectionInfo2) {
        this.opt = adTargetingOptions;
        this.webRequestFactory = webRequestFactory2;
        this.jsonUtilities = jSONUtilities;
        this.slots = new HashMap();
        this.orientation = mobileAdsInfoStore.getDeviceInfo().getOrientation();
        this.connectionInfo = connectionInfo2;
        this.configuration = configuration2;
        this.debugProperties = debugProperties2;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        HashMap copyOfAdvancedOptions = this.opt.getCopyOfAdvancedOptions();
        if (this.debugProperties.containsDebugProperty(DebugProperties.DEBUG_ADVTARGETING)) {
            JSONObject debugPropertyAsJSONObject = this.debugProperties.getDebugPropertyAsJSONObject(DebugProperties.DEBUG_ADVTARGETING, null);
            if (debugPropertyAsJSONObject != null) {
                copyOfAdvancedOptions.putAll(this.jsonUtilities.createMapFromJSON(debugPropertyAsJSONObject));
            }
        }
        this.jsonObjectBuilder = new JSONObjectBuilder(this.logger).setAAXParameters(PARAMETERS).setAAXParameterGroups(PARAMETER_GROUPS).setAdvancedOptions(copyOfAdvancedOptions).setParameterData(new ParameterData().setAdTargetingOptions(this.opt).setAdvancedOptions(copyOfAdvancedOptions).setAdRequest(this));
    }

    public void setInstrumentationPixelURL(String str) {
        this.instrPixelUrl = str;
    }

    public String getInstrumentationPixelURL() {
        return this.instrPixelUrl;
    }

    /* access modifiers changed from: 0000 */
    public AdTargetingOptions getAdTargetingOptions() {
        return this.opt;
    }

    /* access modifiers changed from: 0000 */
    public String getOrientation() {
        return this.orientation;
    }

    /* access modifiers changed from: 0000 */
    public Info getAdvertisingIdentifierInfo() {
        return this.advertisingIdentifierInfo;
    }

    /* access modifiers changed from: 0000 */
    public AdRequest setAdvertisingIdentifierInfo(Info info) {
        this.advertisingIdentifierInfo = info;
        return this;
    }

    public void putSlot(AdSlot adSlot) {
        if (getAdvertisingIdentifierInfo().hasSISDeviceIdentifier()) {
            adSlot.getMetricsCollector().incrementMetric(MetricType.AD_COUNTER_IDENTIFIED_DEVICE);
        }
        adSlot.setConnectionInfo(this.connectionInfo);
        this.slots.put(Integer.valueOf(adSlot.getSlotNumber()), new LOISlot(adSlot, this, this.logger));
    }

    /* access modifiers changed from: protected */
    public JSONArray getSlots() {
        JSONArray jSONArray = new JSONArray();
        for (LOISlot json : this.slots.values()) {
            jSONArray.put(json.getJSON());
        }
        return jSONArray;
    }

    public WebRequest getWebRequest() {
        WebRequest createWebRequest = this.webRequestFactory.createWebRequest();
        createWebRequest.setUseSecure(isSSLRequired() || createWebRequest.getUseSecure());
        createWebRequest.setExternalLogTag(LOGTAG);
        createWebRequest.setHttpMethod(HttpMethod.POST);
        createWebRequest.setHost(this.configuration.getString(ConfigOption.AAX_HOSTNAME));
        createWebRequest.setPath(this.configuration.getString(ConfigOption.AD_RESOURCE_PATH));
        createWebRequest.enableLog(true);
        createWebRequest.setContentType("application/json");
        createWebRequest.setDisconnectEnabled(false);
        setParametersInWebRequest(createWebRequest);
        return createWebRequest;
    }

    private boolean isSSLRequired() {
        return !Configuration.getInstance().getBoolean(ConfigOption.TRUNCATE_LAT_LON) && Configuration.getInstance().getBoolean(ConfigOption.SEND_GEO) && getAdTargetingOptions().isGeoLocationEnabled();
    }

    /* access modifiers changed from: protected */
    public void setParametersInWebRequest(WebRequest webRequest) {
        this.jsonObjectBuilder.build();
        Object value = AAXParameter.SLOTS.getValue(this.jsonObjectBuilder.getParameterData());
        if (value == null) {
            value = getSlots();
        }
        this.jsonObjectBuilder.putIntoJSON(AAXParameter.SLOTS, value);
        JSONObject json = this.jsonObjectBuilder.getJSON();
        String debugPropertyAsString = this.debugProperties.getDebugPropertyAsString(DebugProperties.DEBUG_AAX_AD_PARAMS, null);
        if (!StringUtils.isNullOrEmpty(debugPropertyAsString)) {
            webRequest.setAdditionalQueryParamsString(debugPropertyAsString);
        }
        setRequestBodyString(webRequest, json);
    }

    /* access modifiers changed from: protected */
    public void setRequestBodyString(WebRequest webRequest, JSONObject jSONObject) {
        webRequest.setRequestBodyString(jSONObject.toString());
    }
}
