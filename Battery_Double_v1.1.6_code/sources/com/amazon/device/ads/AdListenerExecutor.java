package com.amazon.device.ads;

import android.graphics.Rect;
import com.amazon.device.ads.ThreadUtils.ExecutionStyle;
import com.amazon.device.ads.ThreadUtils.ExecutionThread;
import com.amazon.device.ads.ThreadUtils.ThreadRunner;

class AdListenerExecutor {
    private static final String LOGTAG = "AdListenerExecutor";
    private final AdListener adListener;
    private final MobileAdsLogger logger;
    private OnAdEventCommand onAdEventCommand;
    private OnAdExpiredCommand onAdExpiredCommand;
    private OnAdReceivedCommand onAdReceivedCommand;
    private OnAdResizedCommand onAdResizedCommand;
    private OnSpecialUrlClickedCommand onSpecialUrlClickedCommand;
    private final ThreadRunner threadRunner;

    public AdListenerExecutor(AdListener adListener2, MobileAdsLoggerFactory mobileAdsLoggerFactory) {
        this(adListener2, ThreadUtils.getThreadRunner(), mobileAdsLoggerFactory);
    }

    AdListenerExecutor(AdListener adListener2, ThreadRunner threadRunner2, MobileAdsLoggerFactory mobileAdsLoggerFactory) {
        this.adListener = adListener2;
        this.threadRunner = threadRunner2;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
    }

    /* access modifiers changed from: 0000 */
    public AdListener getAdListener() {
        return this.adListener;
    }

    public void onAdLoaded(final Ad ad, final AdProperties adProperties) {
        execute(new Runnable() {
            public void run() {
                AdListenerExecutor.this.getAdListener().onAdLoaded(ad, adProperties);
            }
        });
    }

    public void onAdFailedToLoad(final Ad ad, final AdError adError) {
        execute(new Runnable() {
            public void run() {
                AdListenerExecutor.this.getAdListener().onAdFailedToLoad(ad, adError);
            }
        });
    }

    public void onAdExpanded(final Ad ad) {
        execute(new Runnable() {
            public void run() {
                AdListenerExecutor.this.getAdListener().onAdExpanded(ad);
            }
        });
    }

    public void onAdCollapsed(final Ad ad) {
        execute(new Runnable() {
            public void run() {
                AdListenerExecutor.this.getAdListener().onAdCollapsed(ad);
            }
        });
    }

    public void onAdDismissed(final Ad ad) {
        execute(new Runnable() {
            public void run() {
                AdListenerExecutor.this.getAdListener().onAdDismissed(ad);
            }
        });
    }

    public void onAdResized(Ad ad, Rect rect) {
        if (this.onAdResizedCommand == null) {
            this.logger.d("Ad listener called - Ad Resized.");
        } else {
            this.onAdResizedCommand.onAdResized(ad, rect);
        }
    }

    public void onAdExpired(Ad ad) {
        if (this.onAdExpiredCommand == null) {
            this.logger.d("Ad listener called - Ad Expired.");
        } else {
            this.onAdExpiredCommand.onAdExpired(ad);
        }
    }

    public void onSpecialUrlClicked(Ad ad, String str) {
        if (this.onSpecialUrlClickedCommand == null) {
            this.logger.d("Ad listener called - Special Url Clicked.");
        } else {
            this.onSpecialUrlClickedCommand.onSpecialUrlClicked(ad, str);
        }
    }

    public ActionCode onAdReceived(Ad ad, AdData adData) {
        if (this.onAdReceivedCommand != null) {
            return this.onAdReceivedCommand.onAdReceived(ad, adData);
        }
        this.logger.d("Ad listener called - Ad Received.");
        return ActionCode.DISPLAY;
    }

    public void onAdEvent(AdEvent adEvent) {
        if (this.onAdEventCommand == null) {
            MobileAdsLogger mobileAdsLogger = this.logger;
            StringBuilder sb = new StringBuilder();
            sb.append("Ad listener called - Ad Event: ");
            sb.append(adEvent);
            mobileAdsLogger.d(sb.toString());
            return;
        }
        this.onAdEventCommand.onAdEvent(adEvent);
    }

    /* access modifiers changed from: protected */
    public void execute(Runnable runnable) {
        this.threadRunner.execute(runnable, ExecutionStyle.SCHEDULE, ExecutionThread.MAIN_THREAD);
    }

    public void setOnAdEventCommand(OnAdEventCommand onAdEventCommand2) {
        this.onAdEventCommand = onAdEventCommand2;
    }

    public void setOnAdResizedCommand(OnAdResizedCommand onAdResizedCommand2) {
        this.onAdResizedCommand = onAdResizedCommand2;
    }

    public void setOnAdExpiredCommand(OnAdExpiredCommand onAdExpiredCommand2) {
        this.onAdExpiredCommand = onAdExpiredCommand2;
    }

    public void setOnSpecialUrlClickedCommand(OnSpecialUrlClickedCommand onSpecialUrlClickedCommand2) {
        this.onSpecialUrlClickedCommand = onSpecialUrlClickedCommand2;
    }

    public void setOnAdReceivedCommand(OnAdReceivedCommand onAdReceivedCommand2) {
        this.onAdReceivedCommand = onAdReceivedCommand2;
    }
}
