package com.amazon.device.ads;

class GooglePlayServices {
    private static final String GPS_AVAILABLE_SETTING = "gps-available";
    private static final String LOGTAG = "GooglePlayServices";
    private final MobileAdsLogger logger;
    private final ReflectionUtils reflectionUtils;

    static class AdvertisingInfo {
        private String advertisingIdentifier;
        private boolean gpsAvailable = true;
        private boolean limitAdTrackingEnabled;

        protected AdvertisingInfo() {
        }

        static AdvertisingInfo createNotAvailable() {
            return new AdvertisingInfo().setGPSAvailable(false);
        }

        /* access modifiers changed from: 0000 */
        public boolean isGPSAvailable() {
            return this.gpsAvailable;
        }

        private AdvertisingInfo setGPSAvailable(boolean z) {
            this.gpsAvailable = z;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public String getAdvertisingIdentifier() {
            return this.advertisingIdentifier;
        }

        /* access modifiers changed from: 0000 */
        public AdvertisingInfo setAdvertisingIdentifier(String str) {
            this.advertisingIdentifier = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public boolean hasAdvertisingIdentifier() {
            return getAdvertisingIdentifier() != null;
        }

        /* access modifiers changed from: 0000 */
        public boolean isLimitAdTrackingEnabled() {
            return this.limitAdTrackingEnabled;
        }

        /* access modifiers changed from: 0000 */
        public AdvertisingInfo setLimitAdTrackingEnabled(boolean z) {
            this.limitAdTrackingEnabled = z;
            return this;
        }
    }

    public GooglePlayServices() {
        this(new MobileAdsLoggerFactory(), new ReflectionUtils());
    }

    GooglePlayServices(MobileAdsLoggerFactory mobileAdsLoggerFactory, ReflectionUtils reflectionUtils2) {
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        this.reflectionUtils = reflectionUtils2;
    }

    public AdvertisingInfo getAdvertisingIdentifierInfo() {
        if (!isGPSAvailable()) {
            this.logger.v("The Google Play Services Advertising Identifier feature is not available.");
            return AdvertisingInfo.createNotAvailable();
        } else if (isGPSAvailableSet() || isGoogleAdvertisingClassAvailable()) {
            if (isGoogleAdvertisingClassAvailable()) {
                GooglePlayServicesAdapter createGooglePlayServicesAdapter = createGooglePlayServicesAdapter();
                if (createGooglePlayServicesAdapter != null) {
                    AdvertisingInfo advertisingIdentifierInfo = createGooglePlayServicesAdapter.getAdvertisingIdentifierInfo();
                    if (!(advertisingIdentifierInfo == null || advertisingIdentifierInfo.getAdvertisingIdentifier() == null || advertisingIdentifierInfo.getAdvertisingIdentifier().isEmpty())) {
                        setGooglePlayServicesAvailable(advertisingIdentifierInfo.isGPSAvailable());
                        return advertisingIdentifierInfo;
                    }
                }
            }
            AmazonFireServicesAdapter createAmazonFireServiceAdapter = createAmazonFireServiceAdapter();
            if (createAmazonFireServiceAdapter != null) {
                AdvertisingInfo advertisingIdentifierInfo2 = createAmazonFireServiceAdapter.getAdvertisingIdentifierInfo();
                if (!(advertisingIdentifierInfo2 == null || advertisingIdentifierInfo2.getAdvertisingIdentifier() == null || advertisingIdentifierInfo2.getAdvertisingIdentifier().isEmpty())) {
                    setGooglePlayServicesAvailable(advertisingIdentifierInfo2.isGPSAvailable());
                    return advertisingIdentifierInfo2;
                }
            }
            this.logger.v("Advertising Identifier feature is not available.");
            setGooglePlayServicesAvailable(false);
            return AdvertisingInfo.createNotAvailable();
        } else {
            this.logger.v("The Google Play Services Advertising Identifier feature is not available.");
            setGooglePlayServicesAvailable(false);
            return AdvertisingInfo.createNotAvailable();
        }
    }

    /* access modifiers changed from: protected */
    public GooglePlayServicesAdapter createGooglePlayServicesAdapter() {
        return new GooglePlayServicesAdapter();
    }

    /* access modifiers changed from: protected */
    public AmazonFireServicesAdapter createAmazonFireServiceAdapter() {
        return AmazonFireServicesAdapter.newAdapter();
    }

    private boolean isGoogleAdvertisingClassAvailable() {
        return this.reflectionUtils.isClassAvailable("com.google.android.gms.ads.identifier.AdvertisingIdClient");
    }

    private boolean isGPSAvailable() {
        return Settings.getInstance().getBoolean(GPS_AVAILABLE_SETTING, true);
    }

    private boolean isGPSAvailableSet() {
        return Settings.getInstance().containsKey(GPS_AVAILABLE_SETTING);
    }

    private void setGooglePlayServicesAvailable(boolean z) {
        Settings.getInstance().putTransientBoolean(GPS_AVAILABLE_SETTING, z);
    }
}
