package com.amazon.device.ads;

import android.content.Context;
import com.amazon.device.ads.ThreadUtils.ExecutionStyle;
import com.amazon.device.ads.ThreadUtils.ExecutionThread;
import com.amazon.device.ads.ThreadUtils.ThreadRunner;

class UserAgentManager {
    private final ThreadRunner threadRunner;
    private String userAgentStringWithSDKVersion;
    private String userAgentStringWithoutSDKVersion;
    /* access modifiers changed from: private */
    public final WebViewFactory webViewFactory;

    public UserAgentManager() {
        this(new ThreadRunner(), WebViewFactory.getInstance());
    }

    UserAgentManager(ThreadRunner threadRunner2, WebViewFactory webViewFactory2) {
        this.threadRunner = threadRunner2;
        this.webViewFactory = webViewFactory2;
    }

    public String getUserAgentString() {
        return this.userAgentStringWithSDKVersion;
    }

    public void setUserAgentString(String str) {
        if (str != null && !str.equals(this.userAgentStringWithoutSDKVersion) && !str.equals(this.userAgentStringWithSDKVersion)) {
            this.userAgentStringWithoutSDKVersion = str;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" ");
            sb.append(Version.getUserAgentSDKVersion());
            this.userAgentStringWithSDKVersion = sb.toString();
        }
    }

    public void populateUserAgentString(final Context context) {
        this.threadRunner.execute(new Runnable() {
            public void run() {
                UserAgentManager.this.setUserAgentString(UserAgentManager.this.webViewFactory.createWebView(context).getSettings().getUserAgentString());
            }
        }, ExecutionStyle.RUN_ASAP, ExecutionThread.MAIN_THREAD);
    }
}
