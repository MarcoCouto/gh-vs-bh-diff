package com.amazon.device.ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import com.amazon.device.ads.ThreadUtils.ExecutionStyle;
import com.amazon.device.ads.ThreadUtils.ExecutionThread;
import com.amazon.device.ads.ThreadUtils.ThreadRunner;

class NativeCloseButton {
    private static final int CLOSE_BUTTON_SIZE_DP = 60;
    private static final int CLOSE_BUTTON_TAP_TARGET_SIZE_DP = 80;
    private static final String CONTENT_DESCRIPTION_NATIVE_CLOSE_BUTTON = "nativeCloseButton";
    private static final String CONTENT_DESCRIPTION_NATIVE_CLOSE_BUTTON_CONTAINER = "nativeCloseButtonContainer";
    private static final String CONTENT_DESCRIPTION_NATIVE_CLOSE_BUTTON_IMAGE = "nativeCloseButtonImage";
    private final AdCloser adCloser;
    private ViewGroup closeButton;
    private ViewGroup closeButtonContainer;
    private ImageView closeButtonImage;
    private boolean hasNativeCloseButton;
    private final ImageViewFactory imageViewFactory;
    private final LayoutFactory layoutFactory;
    private final ThreadRunner threadRunner;
    private final ViewGroup viewGroup;

    public NativeCloseButton(ViewGroup viewGroup2, AdCloser adCloser2) {
        this(viewGroup2, adCloser2, ThreadUtils.getThreadRunner(), new LayoutFactory(), new ImageButtonFactory());
    }

    NativeCloseButton(ViewGroup viewGroup2, AdCloser adCloser2, ThreadRunner threadRunner2, LayoutFactory layoutFactory2, ImageViewFactory imageViewFactory2) {
        this.hasNativeCloseButton = false;
        this.viewGroup = viewGroup2;
        this.adCloser = adCloser2;
        this.threadRunner = threadRunner2;
        this.layoutFactory = layoutFactory2;
        this.imageViewFactory = imageViewFactory2;
    }

    private Context getContext() {
        return this.viewGroup.getContext();
    }

    public void enable(boolean z, RelativePosition relativePosition) {
        this.hasNativeCloseButton = true;
        if (this.closeButton == null || this.closeButtonImage == null || !this.viewGroup.equals(this.closeButton.getParent()) || (!this.closeButton.equals(this.closeButtonImage.getParent()) && z)) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            final int i = (int) ((displayMetrics.density * 60.0f) + 0.5f);
            final int i2 = (int) ((displayMetrics.density * 80.0f) + 0.5f);
            final boolean z2 = z;
            final RelativePosition relativePosition2 = relativePosition;
            AnonymousClass1 r3 = new MobileAdsAsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... voidArr) {
                    NativeCloseButton.this.createButtonIfNeeded(i2);
                    return null;
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Void voidR) {
                    NativeCloseButton.this.addCloseButtonToTapTargetIfNeeded(z2, relativePosition2, i, i2);
                }
            };
            this.threadRunner.executeAsyncTask(r3, new Void[0]);
            return;
        }
        if (!z) {
            hideImage();
        }
    }

    /* access modifiers changed from: private */
    public void createButtonIfNeeded(int i) {
        boolean z;
        synchronized (this) {
            if (this.closeButton == null) {
                this.closeButton = this.layoutFactory.createLayout(getContext(), LayoutType.RELATIVE_LAYOUT, CONTENT_DESCRIPTION_NATIVE_CLOSE_BUTTON);
                this.closeButtonImage = this.imageViewFactory.createImageView(getContext(), CONTENT_DESCRIPTION_NATIVE_CLOSE_BUTTON_IMAGE);
                z = true;
            } else {
                z = false;
            }
        }
        if (z) {
            final BitmapDrawable createBitmapDrawable = this.imageViewFactory.createBitmapDrawable(getContext().getResources(), Assets.getInstance().getFilePath(Assets.CLOSE_NORMAL));
            final BitmapDrawable createBitmapDrawable2 = this.imageViewFactory.createBitmapDrawable(getContext().getResources(), Assets.getInstance().getFilePath(Assets.CLOSE_PRESSED));
            this.closeButtonImage.setImageDrawable(createBitmapDrawable);
            this.closeButtonImage.setScaleType(ScaleType.FIT_CENTER);
            this.closeButtonImage.setBackgroundDrawable(null);
            AnonymousClass2 r2 = new OnClickListener() {
                public void onClick(View view) {
                    NativeCloseButton.this.closeAd();
                }
            };
            this.closeButtonImage.setOnClickListener(r2);
            this.closeButton.setOnClickListener(r2);
            AnonymousClass3 r22 = new OnTouchListener() {
                @SuppressLint({"ClickableViewAccessibility"})
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    NativeCloseButton.this.animateCloseButton(motionEvent, createBitmapDrawable, createBitmapDrawable2);
                    return false;
                }
            };
            this.closeButton.setOnTouchListener(r22);
            this.closeButtonImage.setOnTouchListener(r22);
            LayoutParams layoutParams = new LayoutParams(i, i);
            layoutParams.addRule(11);
            layoutParams.addRule(10);
            this.closeButtonContainer = this.layoutFactory.createLayout(getContext(), LayoutType.RELATIVE_LAYOUT, CONTENT_DESCRIPTION_NATIVE_CLOSE_BUTTON_CONTAINER);
            this.closeButtonContainer.addView(this.closeButton, layoutParams);
        }
    }

    /* access modifiers changed from: private */
    public void closeAd() {
        this.adCloser.closeAd();
    }

    /* access modifiers changed from: private */
    public void animateCloseButton(MotionEvent motionEvent, BitmapDrawable bitmapDrawable, BitmapDrawable bitmapDrawable2) {
        switch (motionEvent.getAction()) {
            case 0:
                this.closeButtonImage.setImageDrawable(bitmapDrawable2);
                return;
            case 1:
                this.closeButtonImage.setImageDrawable(bitmapDrawable);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"InlinedApi"})
    public void addCloseButtonToTapTargetIfNeeded(boolean z, RelativePosition relativePosition, int i, int i2) {
        if (z && !this.closeButton.equals(this.closeButtonImage.getParent())) {
            LayoutParams layoutParams = new LayoutParams(i, i);
            layoutParams.addRule(13);
            this.closeButton.addView(this.closeButtonImage, layoutParams);
        } else if (!z && this.closeButton.equals(this.closeButtonImage.getParent())) {
            this.closeButton.removeView(this.closeButtonImage);
        }
        if (!this.viewGroup.equals(this.closeButtonContainer.getParent())) {
            this.viewGroup.addView(this.closeButtonContainer, new FrameLayout.LayoutParams(-1, -1));
        }
        LayoutParams layoutParams2 = new LayoutParams(i2, i2);
        if (relativePosition == null) {
            relativePosition = RelativePosition.TOP_RIGHT;
        }
        switch (relativePosition) {
            case BOTTOM_CENTER:
                layoutParams2.addRule(12);
                layoutParams2.addRule(14);
                break;
            case BOTTOM_LEFT:
                layoutParams2.addRule(12);
                layoutParams2.addRule(9);
                break;
            case BOTTOM_RIGHT:
                layoutParams2.addRule(12);
                layoutParams2.addRule(11);
                break;
            case CENTER:
                layoutParams2.addRule(13);
                break;
            case TOP_CENTER:
                layoutParams2.addRule(10);
                layoutParams2.addRule(14);
                break;
            case TOP_LEFT:
                layoutParams2.addRule(10);
                layoutParams2.addRule(9);
                break;
            case TOP_RIGHT:
                layoutParams2.addRule(10);
                layoutParams2.addRule(11);
                break;
            default:
                layoutParams2.addRule(10);
                layoutParams2.addRule(11);
                break;
        }
        this.closeButton.setLayoutParams(layoutParams2);
        this.closeButtonContainer.bringToFront();
    }

    public void remove() {
        this.hasNativeCloseButton = false;
        this.threadRunner.execute(new Runnable() {
            public void run() {
                NativeCloseButton.this.removeNativeCloseButtonOnMainThread();
            }
        }, ExecutionStyle.RUN_ASAP, ExecutionThread.MAIN_THREAD);
    }

    /* access modifiers changed from: private */
    public void removeNativeCloseButtonOnMainThread() {
        this.viewGroup.removeView(this.closeButtonContainer);
    }

    public void showImage(boolean z) {
        if (this.hasNativeCloseButton && this.closeButton != null) {
            if (z) {
                enable(true, null);
            } else {
                hideImage();
            }
        }
    }

    private void hideImage() {
        this.threadRunner.execute(new Runnable() {
            public void run() {
                NativeCloseButton.this.hideImageOnMainThread();
            }
        }, ExecutionStyle.RUN_ASAP, ExecutionThread.MAIN_THREAD);
    }

    /* access modifiers changed from: private */
    public void hideImageOnMainThread() {
        this.closeButton.removeAllViews();
    }
}
