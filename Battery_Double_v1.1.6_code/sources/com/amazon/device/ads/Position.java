package com.amazon.device.ads;

import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import org.json.JSONObject;

class Position {
    private Size size;
    private int x;
    private int y;

    public Position() {
        this.size = new Size(0, 0);
        this.x = 0;
        this.y = 0;
    }

    public Position(Size size2, int i, int i2) {
        this.size = size2;
        this.x = i;
        this.y = i2;
    }

    public Size getSize() {
        return this.size;
    }

    public void setSize(Size size2) {
        this.size = size2;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int i) {
        this.x = i;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int i) {
        this.y = i;
    }

    public JSONObject toJSONObject() {
        JSONObject jSONObject = this.size.toJSONObject();
        JSONUtils.put(jSONObject, AvidJSONUtil.KEY_X, this.x);
        JSONUtils.put(jSONObject, AvidJSONUtil.KEY_Y, this.y);
        return jSONObject;
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (!(obj instanceof Position)) {
            return false;
        }
        Position position = (Position) obj;
        if (this.size.equals(position.size) && this.x == position.x && this.y == position.y) {
            z = true;
        }
        return z;
    }
}
