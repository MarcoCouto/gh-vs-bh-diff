package com.amazon.device.ads;

import android.content.ContentResolver;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import com.tapjoy.TapjoyConstants;

public class AmazonFireServicesAdapter {
    public static final int FIREOS_ADTRACKING_NOT_LIMITED = 0;
    private static final String LOGTAG = "AmazonFireServicesAdapter";
    private final MobileAdsLogger logger = new MobileAdsLoggerFactory().createMobileAdsLogger(LOGTAG);

    public static AmazonFireServicesAdapter newAdapter() {
        return new AmazonFireServicesAdapter();
    }

    private AmazonFireServicesAdapter() {
    }

    public AdvertisingInfo getAdvertisingIdentifierInfo() {
        try {
            ContentResolver contentResolver = MobileAdsInfoStore.getInstance().getApplicationContext().getContentResolver();
            int i = Secure.getInt(contentResolver, "limit_ad_tracking");
            String string = Secure.getString(contentResolver, TapjoyConstants.TJC_ADVERTISING_ID);
            boolean z = true;
            this.logger.v("Fire Id retrieved : %s", string);
            if (i != 0) {
                this.logger.v("Fire Device does not allow ad tracking : %s", string);
            } else {
                z = false;
            }
            AdvertisingInfo advertisingInfo = new AdvertisingInfo();
            advertisingInfo.setAdvertisingIdentifier(string);
            advertisingInfo.setLimitAdTrackingEnabled(z);
            return advertisingInfo;
        } catch (SettingNotFoundException e) {
            MobileAdsLogger mobileAdsLogger = this.logger;
            StringBuilder sb = new StringBuilder();
            sb.append(" Advertising setting not found on this device : %s");
            sb.append(e.getLocalizedMessage());
            mobileAdsLogger.v(sb.toString());
            return new AdvertisingInfo();
        } catch (Exception e2) {
            MobileAdsLogger mobileAdsLogger2 = this.logger;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(" Attempt to retrieve fireID failed. Reason : %s ");
            sb2.append(e2.getLocalizedMessage());
            mobileAdsLogger2.v(sb2.toString());
            return new AdvertisingInfo();
        }
    }
}
