package com.amazon.device.ads;

import com.github.mikephil.charting.utils.Utils;
import java.util.Iterator;

class AdHtmlPreprocessor {
    private static final String LOGTAG = "AdHtmlPreprocessor";
    private final AdControlAccessor adControlAccessor;
    private final AdUtils2 adUtils;
    private final AdSDKBridgeList bridgeList;
    private final BridgeSelector bridgeSelector;
    private final MobileAdsLogger logger;

    public AdHtmlPreprocessor(BridgeSelector bridgeSelector2, AdSDKBridgeList adSDKBridgeList, AdControlAccessor adControlAccessor2, MobileAdsLoggerFactory mobileAdsLoggerFactory, AdUtils2 adUtils2) {
        this.bridgeSelector = bridgeSelector2;
        this.bridgeList = adSDKBridgeList;
        this.adControlAccessor = adControlAccessor2;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        this.adUtils = adUtils2;
    }

    private void addAdSDKBridge(AdSDKBridge adSDKBridge) {
        this.bridgeList.addBridge(adSDKBridge);
    }

    public String preprocessHtml(String str, boolean z) {
        for (AdSDKBridgeFactory createAdSDKBridge : this.bridgeSelector.getBridgeFactories(str)) {
            addAdSDKBridge(createAdSDKBridge.createAdSDKBridge(this.adControlAccessor));
        }
        this.logger.d("Scaling Params: scalingDensity: %f, windowWidth: %d, windowHeight: %d, adWidth: %d, adHeight: %d, scale: %f", Float.valueOf(this.adUtils.getScalingFactorAsFloat()), Integer.valueOf(this.adControlAccessor.getWindowWidth()), Integer.valueOf(this.adControlAccessor.getWindowHeight()), Integer.valueOf((int) (((float) this.adControlAccessor.getAdWidth()) * this.adUtils.getScalingFactorAsFloat())), Integer.valueOf((int) (((float) this.adControlAccessor.getAdHeight()) * this.adUtils.getScalingFactorAsFloat())), Double.valueOf(this.adControlAccessor.getScalingMultiplier()));
        String str2 = "";
        Iterator it = this.bridgeList.iterator();
        while (it.hasNext()) {
            AdSDKBridge adSDKBridge = (AdSDKBridge) it.next();
            if (adSDKBridge.getSDKEventListener() != null) {
                this.adControlAccessor.addSDKEventListener(adSDKBridge.getSDKEventListener());
            }
            if (adSDKBridge.getJavascript() != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                sb.append(adSDKBridge.getJavascript());
                str2 = sb.toString();
            }
            if (adSDKBridge.hasNativeExecution()) {
                this.adControlAccessor.addJavascriptInterface(adSDKBridge.getJavascriptInteractorExecutor(), z, adSDKBridge.getName());
            }
        }
        return addHeadData(ensureHtmlTags(str), str2);
    }

    private String ensureHtmlTags(String str) {
        String str2 = "";
        String str3 = "";
        if (!StringUtils.containsRegEx("\\A\\s*<![Dd][Oo][Cc][Tt][Yy][Pp][Ee]\\s+[Hh][Tt][Mm][Ll][\\s>]", str)) {
            str2 = "<!DOCTYPE html>";
        }
        if (!StringUtils.containsRegEx("<[Hh][Tt][Mm][Ll][\\s>]", str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append("<html>");
            str2 = sb.toString();
            str3 = "</html>";
        }
        if (!StringUtils.containsRegEx("<[Hh][Ee][Aa][Dd][\\s>]", str)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append("<head></head>");
            str2 = sb2.toString();
        }
        if (!StringUtils.containsRegEx("<[Bb][Oo][Dd][Yy][\\s>]", str)) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str2);
            sb3.append("<body>");
            str2 = sb3.toString();
            StringBuilder sb4 = new StringBuilder();
            sb4.append("</body>");
            sb4.append(str3);
            str3 = sb4.toString();
        }
        StringBuilder sb5 = new StringBuilder();
        sb5.append(str2);
        sb5.append(str);
        sb5.append(str3);
        return sb5.toString();
    }

    private String addHeadData(String str, String str2) {
        String firstMatch = StringUtils.getFirstMatch("<[Hh][Ee][Aa][Dd](\\s*>|\\s[^>]*>)", str);
        String str3 = "";
        if (!StringUtils.containsRegEx("<[Mm][Ee][Tt][Aa](\\s[^>]*\\s|\\s)[Nn][Aa][Mm][Ee]\\s*=\\s*[\"'][Vv][Ii][Ee][Ww][Pp][Oo][Rr][Tt][\"']", str)) {
            if (this.adControlAccessor.getScalingMultiplier() >= Utils.DOUBLE_EPSILON) {
                StringBuilder sb = new StringBuilder();
                sb.append(str3);
                sb.append("<meta name=\"viewport\" content=\"width=");
                sb.append(this.adControlAccessor.getWindowWidth());
                sb.append(", height=");
                sb.append(this.adControlAccessor.getWindowHeight());
                sb.append(", initial-scale=");
                sb.append(this.adUtils.getViewportInitialScale(this.adControlAccessor.getScalingMultiplier()));
                sb.append(", minimum-scale=");
                sb.append(this.adControlAccessor.getScalingMultiplier());
                sb.append(", maximum-scale=");
                sb.append(this.adControlAccessor.getScalingMultiplier());
                sb.append("\"/>");
                str3 = sb.toString();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str3);
                sb2.append("<meta name=\"viewport\" content=\"width=device-width, height=device-height, user-scalable=no, initial-scale=1.0\"/>");
                str3 = sb2.toString();
            }
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str3);
        sb3.append("<style>html,body{margin:0;padding:0;height:100%;border:none;}</style>");
        String sb4 = sb3.toString();
        if (str2.length() > 0) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append(sb4);
            sb5.append("<script type='text/javascript'>");
            sb5.append(str2);
            sb5.append("</script>");
            sb4 = sb5.toString();
        }
        StringBuilder sb6 = new StringBuilder();
        sb6.append(firstMatch);
        sb6.append(sb4);
        return str.replace(firstMatch, sb6.toString());
    }
}
