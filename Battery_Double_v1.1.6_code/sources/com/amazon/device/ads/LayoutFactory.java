package com.amazon.device.ads;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

class LayoutFactory {

    enum LayoutType {
        RELATIVE_LAYOUT,
        LINEAR_LAYOUT,
        FRAME_LAYOUT
    }

    LayoutFactory() {
    }

    public ViewGroup createLayout(Context context, LayoutType layoutType, String str) {
        ViewGroup viewGroup;
        switch (layoutType) {
            case RELATIVE_LAYOUT:
                viewGroup = new RelativeLayout(context);
                break;
            case FRAME_LAYOUT:
                viewGroup = new FrameLayout(context);
                break;
            default:
                viewGroup = new LinearLayout(context);
                break;
        }
        viewGroup.setContentDescription(str);
        return viewGroup;
    }
}
