package com.amazon.device.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout.LayoutParams;
import com.amazon.device.ads.AdActivity.AdActivityAdapter;
import com.amazon.device.ads.AdEvent.AdEventType;
import com.amazon.device.ads.JSONUtils.JSONUtilities;
import com.amazon.device.ads.SDKEvent.SDKEventType;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;

class ModalAdActivityAdapter implements AdActivityAdapter {
    private static final String CONTENT_DESCRIPTION_AD_CONTAINER_VIEW = "adContainerView";
    private static final String CONTENT_DESCRIPTION_EXPANSION_VIEW = "expansionView";
    private static final String LOGTAG = "ModalAdActivityAdapter";
    private Activity activity;
    private ViewGroup adContainerView;
    /* access modifiers changed from: private */
    public AdControlAccessor adControlAccessor;
    private final AdUtils2 adUtils;
    private final AndroidBuildInfo buildInfo;
    private final ExpandProperties expandProperties;
    /* access modifiers changed from: private */
    public ViewGroup expansionView;
    private final JSONUtilities jsonUtils;
    /* access modifiers changed from: private */
    public Size lastReportedSize;
    private final LayoutFactory layoutFactory;
    private final MobileAdsLogger logger;
    private final OrientationProperties orientationProperties;
    private String url;
    /* access modifiers changed from: private */
    public final ViewUtils viewUtils;

    private class ModalAdSDKEventListener implements SDKEventListener {
        private ModalAdSDKEventListener() {
        }

        public void onSDKEvent(SDKEvent sDKEvent, AdControlAccessor adControlAccessor) {
            if (sDKEvent.getEventType().equals(SDKEventType.CLOSED)) {
                ModalAdActivityAdapter.this.finishActivity();
            }
        }
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void onWindowFocusChanged() {
    }

    public ModalAdActivityAdapter() {
        this(new MobileAdsLoggerFactory(), new AdUtils2(), new JSONUtilities(), new ExpandProperties(), new OrientationProperties(), new AndroidBuildInfo(), new LayoutFactory(), new ViewUtils());
    }

    ModalAdActivityAdapter(MobileAdsLoggerFactory mobileAdsLoggerFactory, AdUtils2 adUtils2, JSONUtilities jSONUtilities, ExpandProperties expandProperties2, OrientationProperties orientationProperties2, AndroidBuildInfo androidBuildInfo, LayoutFactory layoutFactory2, ViewUtils viewUtils2) {
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        this.adUtils = adUtils2;
        this.jsonUtils = jSONUtilities;
        this.expandProperties = expandProperties2;
        this.orientationProperties = orientationProperties2;
        this.buildInfo = androidBuildInfo;
        this.layoutFactory = layoutFactory2;
        this.viewUtils = viewUtils2;
    }

    public void setActivity(Activity activity2) {
        this.activity = activity2;
    }

    public void preOnCreate() {
        this.activity.requestWindowFeature(1);
        this.activity.getWindow().setFlags(1024, 1024);
        AndroidTargetUtils.hideActionAndStatusBars(this.buildInfo, this.activity);
    }

    public void onCreate() {
        Intent intent = this.activity.getIntent();
        String stringExtra = intent.getStringExtra("url");
        if (!StringUtils.isNullOrWhiteSpace(stringExtra)) {
            this.url = stringExtra;
        }
        this.expandProperties.fromJSONObject(this.jsonUtils.getJSONObjectFromString(intent.getStringExtra("expandProperties")));
        if (this.url != null) {
            this.expandProperties.setWidth(-1);
            this.expandProperties.setHeight(-1);
        }
        this.orientationProperties.fromJSONObject(this.jsonUtils.getJSONObjectFromString(intent.getStringExtra("orientationProperties")));
        AndroidTargetUtils.enableHardwareAcceleration(this.buildInfo, this.activity.getWindow());
        this.adControlAccessor = AdControllerFactory.getCachedAdControlAccessor();
        if (this.adControlAccessor == null) {
            this.logger.e("Failed to show expanded ad due to an error in the Activity.");
            this.activity.finish();
            return;
        }
        this.adControlAccessor.setAdActivity(this.activity);
        this.adControlAccessor.addSDKEventListener(new ModalAdSDKEventListener());
        expandAd();
        orientationPropertyChange();
        this.adControlAccessor.fireAdEvent(new AdEvent(AdEventType.EXPANDED));
        this.adControlAccessor.injectJavascript("mraidBridge.stateChange('expanded');");
        reportSizeChangeEvent();
    }

    private void reportSizeChangeEvent() {
        this.expansionView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                ModalAdActivityAdapter.this.viewUtils.removeOnGlobalLayoutListener(ModalAdActivityAdapter.this.expansionView.getViewTreeObserver(), this);
                Position currentPosition = ModalAdActivityAdapter.this.adControlAccessor.getCurrentPosition();
                if (currentPosition != null) {
                    Size size = currentPosition.getSize();
                    if (size != null && !size.equals(ModalAdActivityAdapter.this.lastReportedSize)) {
                        ModalAdActivityAdapter.this.lastReportedSize = size;
                        AdControlAccessor access$300 = ModalAdActivityAdapter.this.adControlAccessor;
                        StringBuilder sb = new StringBuilder();
                        sb.append("mraidBridge.sizeChange(");
                        sb.append(size.getWidth());
                        sb.append(",");
                        sb.append(size.getHeight());
                        sb.append(");");
                        access$300.injectJavascript(sb.toString());
                    }
                }
            }
        });
    }

    private void orientationPropertyChange() {
        if (this.adControlAccessor.isVisible() && this.adControlAccessor.isModal()) {
            if (this.activity == null) {
                this.logger.e("unable to handle orientation property change because the context did not contain an activity");
                return;
            }
            int requestedOrientation = this.activity.getRequestedOrientation();
            MobileAdsLogger mobileAdsLogger = this.logger;
            StringBuilder sb = new StringBuilder();
            sb.append("Current Orientation: ");
            sb.append(requestedOrientation);
            mobileAdsLogger.d(sb.toString());
            switch (this.orientationProperties.getForceOrientation()) {
                case PORTRAIT:
                    this.activity.setRequestedOrientation(7);
                    break;
                case LANDSCAPE:
                    this.activity.setRequestedOrientation(6);
                    break;
            }
            if (ForceOrientation.NONE.equals(this.orientationProperties.getForceOrientation())) {
                if (this.orientationProperties.isAllowOrientationChange().booleanValue()) {
                    this.activity.setRequestedOrientation(-1);
                } else {
                    this.activity.setRequestedOrientation(DisplayUtils.determineCanonicalScreenOrientation(this.activity, this.buildInfo));
                }
            }
            int requestedOrientation2 = this.activity.getRequestedOrientation();
            MobileAdsLogger mobileAdsLogger2 = this.logger;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("New Orientation: ");
            sb2.append(requestedOrientation2);
            mobileAdsLogger2.d(sb2.toString());
            if (requestedOrientation2 != requestedOrientation) {
                reportSizeChangeEvent();
            }
        }
    }

    public void onStop() {
        if (this.activity.isFinishing() && this.adControlAccessor != null) {
            this.adControlAccessor.closeAd();
        }
    }

    public void onDestroy() {
        if (this.adControlAccessor != null) {
            this.adControlAccessor.closeAd();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        reportSizeChangeEvent();
    }

    public boolean onBackPressed() {
        if (this.adControlAccessor != null) {
            return this.adControlAccessor.onBackButtonPress();
        }
        return false;
    }

    private void expandAd() {
        if (this.url != null) {
            this.adControlAccessor.stashView();
        }
        Size computeExpandedSizeInPixels = computeExpandedSizeInPixels(this.expandProperties);
        createExpandedView();
        this.adControlAccessor.moveViewToViewGroup(this.adContainerView, new LayoutParams(-1, -1), true);
        LayoutParams layoutParams = new LayoutParams(computeExpandedSizeInPixels.getWidth(), computeExpandedSizeInPixels.getHeight());
        layoutParams.addRule(13);
        this.expansionView.addView(this.adContainerView, layoutParams);
        this.activity.setContentView(this.expansionView, new LayoutParams(-1, -1));
        this.adControlAccessor.enableCloseButton(!this.expandProperties.getUseCustomClose().booleanValue());
    }

    private void createExpandedView() {
        this.expansionView = this.layoutFactory.createLayout(this.activity, LayoutType.RELATIVE_LAYOUT, CONTENT_DESCRIPTION_EXPANSION_VIEW);
        this.adContainerView = this.layoutFactory.createLayout(this.activity, LayoutType.FRAME_LAYOUT, CONTENT_DESCRIPTION_AD_CONTAINER_VIEW);
    }

    private Size computeExpandedSizeInPixels(ExpandProperties expandProperties2) {
        MobileAdsLogger mobileAdsLogger = this.logger;
        StringBuilder sb = new StringBuilder();
        sb.append("Expanding Ad to ");
        sb.append(expandProperties2.getWidth());
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(expandProperties2.getHeight());
        mobileAdsLogger.d(sb.toString());
        return new Size(this.adUtils.deviceIndependentPixelToPixel(expandProperties2.getWidth()), this.adUtils.deviceIndependentPixelToPixel(expandProperties2.getHeight()));
    }

    /* access modifiers changed from: private */
    public void finishActivity() {
        if (!this.activity.isFinishing()) {
            this.adControlAccessor = null;
            this.activity.finish();
        }
    }
}
