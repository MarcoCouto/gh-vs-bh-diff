package com.amazon.device.ads;

class AdvertisingIdentifier {
    private static final String GPS_ADVERTISING_IDENTIFIER_SETTING = "gpsAdId";
    private static final String LOGTAG = "AdvertisingIdentifier";
    private static final String TRANSITION_MIGRATE = "migrate";
    private static final String TRANSITION_RESET = "reset";
    private static final String TRANSITION_REVERT = "revert";
    private static final String TRANSITION_SETTING = "adIdTransistion";
    private final DebugProperties debugProperties;
    private AdvertisingInfo gpsAdvertisingInfo;
    private final MobileAdsInfoStore infoStore;
    private final MobileAdsLogger logger;
    private final Settings settings;
    private boolean shouldSetCurrentAdvertisingIdentifier;

    static class Info {
        private String advertisingIdentifier;
        private boolean canDo;
        private final DebugProperties debugProperties;
        private boolean limitAdTrackingEnabled;
        private String sisDeviceIdentifier;

        private Info(DebugProperties debugProperties2) {
            this.debugProperties = debugProperties2;
            this.canDo = true;
        }

        /* access modifiers changed from: 0000 */
        public boolean canDo() {
            return this.canDo;
        }

        /* access modifiers changed from: private */
        public Info setCanDo(boolean z) {
            this.canDo = z;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public String getAdvertisingIdentifier() {
            return this.debugProperties.getDebugPropertyAsString(DebugProperties.DEBUG_IDFA, this.advertisingIdentifier);
        }

        /* access modifiers changed from: private */
        public Info setAdvertisingIdentifier(String str) {
            this.advertisingIdentifier = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public boolean hasAdvertisingIdentifier() {
            return !StringUtils.isNullOrEmpty(getAdvertisingIdentifier());
        }

        /* access modifiers changed from: 0000 */
        public boolean isLimitAdTrackingEnabled() {
            return this.debugProperties.getDebugPropertyAsBoolean(DebugProperties.DEBUG_OPT_OUT, Boolean.valueOf(this.limitAdTrackingEnabled)).booleanValue();
        }

        /* access modifiers changed from: private */
        public Info setLimitAdTrackingEnabled(boolean z) {
            this.limitAdTrackingEnabled = z;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public String getSISDeviceIdentifier() {
            return this.debugProperties.getDebugPropertyAsString(DebugProperties.DEBUG_ADID, this.sisDeviceIdentifier);
        }

        /* access modifiers changed from: 0000 */
        public boolean hasSISDeviceIdentifier() {
            return getSISDeviceIdentifier() != null;
        }

        /* access modifiers changed from: 0000 */
        public Info setSISDeviceIdentifier(String str) {
            this.sisDeviceIdentifier = str;
            return this;
        }
    }

    public AdvertisingIdentifier() {
        this(Settings.getInstance(), MobileAdsInfoStore.getInstance(), new MobileAdsLoggerFactory(), DebugProperties.getInstance());
    }

    AdvertisingIdentifier(Settings settings2, MobileAdsInfoStore mobileAdsInfoStore, MobileAdsLoggerFactory mobileAdsLoggerFactory, DebugProperties debugProperties2) {
        this.shouldSetCurrentAdvertisingIdentifier = true;
        this.settings = settings2;
        this.infoStore = mobileAdsInfoStore;
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
        this.debugProperties = debugProperties2;
    }

    /* access modifiers changed from: 0000 */
    public String getAndClearTransition() {
        String string = this.settings.getString(TRANSITION_SETTING, null);
        this.settings.remove(TRANSITION_SETTING);
        return string;
    }

    /* access modifiers changed from: 0000 */
    public AdvertisingIdentifier setShouldSetCurrentAdvertisingIdentifier(boolean z) {
        this.shouldSetCurrentAdvertisingIdentifier = z;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public Info getAdvertisingIdentifierInfo() {
        if (ThreadUtils.isOnMainThread()) {
            this.logger.e("You must obtain the advertising indentifier information on a background thread.");
            return new Info(this.debugProperties).setCanDo(false);
        }
        fetchGooglePlayServicesAdvertisingIdentifierInfo();
        if (this.shouldSetCurrentAdvertisingIdentifier) {
            determineTransition();
        }
        Info info = new Info(this.debugProperties);
        if (getGPSAdvertisingInfo().hasAdvertisingIdentifier()) {
            info.setAdvertisingIdentifier(getGPSAdvertisingInfo().getAdvertisingIdentifier());
            info.setLimitAdTrackingEnabled(getGPSAdvertisingInfo().isLimitAdTrackingEnabled());
            if (this.shouldSetCurrentAdvertisingIdentifier) {
                setCurrentGPSAID(getGPSAdvertisingInfo().getAdvertisingIdentifier());
            }
        }
        RegistrationInfo registrationInfo = this.infoStore.getRegistrationInfo();
        if (registrationInfo.isAdIdCurrent(info)) {
            info.setSISDeviceIdentifier(registrationInfo.getAdId());
        } else {
            registrationInfo.requestNewSISDeviceIdentifier();
        }
        return info;
    }

    private void determineTransition() {
        String str = isTransitionMigrated() ? TRANSITION_MIGRATE : isTransitionReset() ? TRANSITION_RESET : isTransitionReverted() ? TRANSITION_REVERT : null;
        if (str != null) {
            setTransition(str);
        } else {
            this.logger.d("No transition detected.");
        }
    }

    private void setTransition(String str) {
        this.logger.d("Transition: %s", str);
        this.settings.putString(TRANSITION_SETTING, str);
    }

    /* access modifiers changed from: protected */
    public void fetchGooglePlayServicesAdvertisingIdentifierInfo() {
        this.gpsAdvertisingInfo = new GooglePlayServices().getAdvertisingIdentifierInfo();
    }

    /* access modifiers changed from: protected */
    public AdvertisingInfo getGPSAdvertisingInfo() {
        return this.gpsAdvertisingInfo;
    }

    private boolean isTransitionMigrated() {
        return this.infoStore.getRegistrationInfo().hasAdId() && RegistrationInfo.isAdIdOriginatedFromNonAdvertisingIdentifier() && !hasCurrentGPSAID() && getGPSAdvertisingInfo().hasAdvertisingIdentifier();
    }

    private boolean isTransitionReset() {
        return hasCurrentGPSAID() && getGPSAdvertisingInfo().hasAdvertisingIdentifier() && !getCurrentGPSAID().equals(getGPSAdvertisingInfo().getAdvertisingIdentifier());
    }

    private boolean isTransitionReverted() {
        return hasCurrentGPSAID() && !getGPSAdvertisingInfo().hasAdvertisingIdentifier();
    }

    private void setCurrentGPSAID(String str) {
        this.settings.putString(GPS_ADVERTISING_IDENTIFIER_SETTING, str);
    }

    private String getCurrentGPSAID() {
        return this.settings.getString(GPS_ADVERTISING_IDENTIFIER_SETTING, "");
    }

    private boolean hasCurrentGPSAID() {
        return !StringUtils.isNullOrEmpty(getCurrentGPSAID());
    }
}
