package com.amazon.device.ads;

import com.amazon.device.ads.WebRequest.WebRequestException;
import com.amazon.device.ads.WebRequest.WebRequestStatus;
import com.amazon.device.ads.WebRequest.WebResponse;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Map.Entry;

class HttpURLConnectionWebRequest extends WebRequest {
    private static final String LOGTAG = "HttpURLConnectionWebRequest";
    private HttpURLConnection connection;

    HttpURLConnectionWebRequest() {
    }

    /* access modifiers changed from: protected */
    public WebResponse doHttpNetworkCall(URL url) throws WebRequestException {
        if (this.connection != null) {
            closeConnection();
        }
        try {
            this.connection = openConnection(url);
            setupRequestProperties(this.connection);
            try {
                this.connection.connect();
                return prepareResponse(this.connection);
            } catch (SocketTimeoutException e) {
                getLogger().e("Socket timed out while connecting to URL: %s", e.getMessage());
                throw new WebRequestException(WebRequestStatus.NETWORK_TIMEOUT, "Socket timed out while connecting to URL", e);
            } catch (Exception e2) {
                getLogger().e("Problem while connecting to URL: %s", e2.getMessage());
                throw new WebRequestException(WebRequestStatus.NETWORK_FAILURE, "Probem while connecting to URL", e2);
            }
        } catch (IOException e3) {
            getLogger().e("Problem while opening the URL connection: %s", e3.getMessage());
            throw new WebRequestException(WebRequestStatus.NETWORK_FAILURE, "Problem while opening the URL connection", e3);
        }
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection openConnection(URL url) throws IOException {
        return (HttpURLConnection) url.openConnection();
    }

    /* access modifiers changed from: protected */
    public void closeConnection() {
        if (this.connection != null) {
            this.connection.disconnect();
            this.connection = null;
        }
    }

    /* access modifiers changed from: protected */
    public void setupRequestProperties(HttpURLConnection httpURLConnection) throws WebRequestException {
        try {
            httpURLConnection.setRequestMethod(getHttpMethod().name());
            for (Entry entry : this.headers.entrySet()) {
                if (entry.getValue() != null && !((String) entry.getValue()).equals("")) {
                    httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }
            }
            httpURLConnection.setConnectTimeout(getTimeout());
            httpURLConnection.setReadTimeout(getTimeout());
            logUrl(httpURLConnection.getURL().toString());
            switch (getHttpMethod()) {
                case GET:
                    httpURLConnection.setDoOutput(false);
                    return;
                case POST:
                    httpURLConnection.setDoOutput(true);
                    writePostBody(httpURLConnection);
                    return;
                default:
                    return;
            }
        } catch (ProtocolException e) {
            getLogger().e("Invalid client protocol: %s", e.getMessage());
            throw new WebRequestException(WebRequestStatus.INVALID_CLIENT_PROTOCOL, "Invalid client protocol", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00e2 A[SYNTHETIC, Splitter:B:42:0x00e2] */
    private void writePostBody(HttpURLConnection httpURLConnection) throws WebRequestException {
        String str;
        StringBuilder sb = new StringBuilder();
        if (this.requestBody != null) {
            sb.append(this.requestBody);
        } else if (this.postParameters != null && !this.postParameters.isEmpty()) {
            for (Entry entry : this.postParameters.entrySet()) {
                sb.append((String) entry.getKey());
                sb.append(RequestParameters.EQUAL);
                sb.append(WebUtils.getURLEncodedString((String) entry.getValue()));
                sb.append(RequestParameters.AMPERSAND);
            }
            sb.deleteCharAt(sb.lastIndexOf(RequestParameters.AMPERSAND));
        }
        if (this.logRequestBodyEnabled && getRequestBody() != null) {
            if (!this.logSessionIdEnabled) {
                str = getRequestBody().replaceAll(",\\s*\"\\s*sessionId\\s*\"\\s*:\\s*\".*?\"|\\s*\"\\s*sessionId\\s*\"\\s*:\\s*\".*?\"\\s*,*", "");
            } else {
                str = getRequestBody();
            }
            getLogger().d("Request Body: %s", str);
        }
        OutputStreamWriter outputStreamWriter = null;
        try {
            OutputStreamWriter outputStreamWriter2 = new OutputStreamWriter(httpURLConnection.getOutputStream(), "UTF-8");
            try {
                outputStreamWriter2.write(sb.toString());
                try {
                    outputStreamWriter2.close();
                } catch (IOException e) {
                    getLogger().e("Problem while closing output stream writer for request body: %s", e.getMessage());
                    throw new WebRequestException(WebRequestStatus.NETWORK_FAILURE, "Problem while closing output stream writer for request body", e);
                }
            } catch (IOException e2) {
                e = e2;
                outputStreamWriter = outputStreamWriter2;
                try {
                    getLogger().e("Problem while creating output steam for request body: %s", e.getMessage());
                    throw new WebRequestException(WebRequestStatus.NETWORK_FAILURE, "Problem while creating output steam for request body", e);
                } catch (Throwable th) {
                    th = th;
                    if (outputStreamWriter != null) {
                        try {
                            outputStreamWriter.close();
                        } catch (IOException e3) {
                            getLogger().e("Problem while closing output stream writer for request body: %s", e3.getMessage());
                            throw new WebRequestException(WebRequestStatus.NETWORK_FAILURE, "Problem while closing output stream writer for request body", e3);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                outputStreamWriter = outputStreamWriter2;
                if (outputStreamWriter != null) {
                }
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
            getLogger().e("Problem while creating output steam for request body: %s", e.getMessage());
            throw new WebRequestException(WebRequestStatus.NETWORK_FAILURE, "Problem while creating output steam for request body", e);
        }
    }

    /* access modifiers changed from: protected */
    public WebResponse prepareResponse(HttpURLConnection httpURLConnection) throws WebRequestException {
        WebResponse webResponse = new WebResponse();
        try {
            webResponse.setHttpStatusCode(httpURLConnection.getResponseCode());
            webResponse.setHttpStatus(httpURLConnection.getResponseMessage());
            if (webResponse.getHttpStatusCode() == 200) {
                try {
                    webResponse.setInputStream(httpURLConnection.getInputStream());
                } catch (IOException e) {
                    getLogger().e("IOException while reading the input stream from response: %s", e.getMessage());
                    throw new WebRequestException(WebRequestStatus.NETWORK_FAILURE, "IOException while reading the input stream from response", e);
                }
            }
            return webResponse;
        } catch (SocketTimeoutException e2) {
            getLogger().e("Socket Timeout while getting the response status code: %s", e2.getMessage());
            throw new WebRequestException(WebRequestStatus.NETWORK_TIMEOUT, "Socket Timeout while getting the response status code", e2);
        } catch (IOException e3) {
            getLogger().e("IOException while getting the response status code: %s", e3.getMessage());
            throw new WebRequestException(WebRequestStatus.NETWORK_FAILURE, "IOException while getting the response status code", e3);
        } catch (IndexOutOfBoundsException e4) {
            getLogger().e("IndexOutOfBoundsException while getting the response status code: %s", e4.getMessage());
            throw new WebRequestException(WebRequestStatus.MALFORMED_URL, "IndexOutOfBoundsException while getting the response status code", e4);
        }
    }

    /* access modifiers changed from: protected */
    public String getSubLogTag() {
        return LOGTAG;
    }
}
