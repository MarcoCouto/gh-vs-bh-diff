package com.amazon.device.ads;

import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;

class MobileAdsLogger implements Logger {
    private static final String DEFAULT_LOGTAG_PREFIX = "AmazonMobileAds";
    private static final int DEFAULT_MAX_LENGTH = 1000;
    static final String LOGGING_ENABLED = "loggingEnabled";
    private final DebugProperties debugProperties;
    private final Logger logger;
    private int maxLength;
    private final Settings settings;

    public enum Level {
        DEBUG,
        ERROR,
        INFO,
        VERBOSE,
        WARN
    }

    public MobileAdsLogger(Logger logger2) {
        this(logger2, DebugProperties.getInstance(), Settings.getInstance());
    }

    MobileAdsLogger(Logger logger2, DebugProperties debugProperties2, Settings settings2) {
        this.maxLength = 1000;
        this.logger = logger2.withLogTag(DEFAULT_LOGTAG_PREFIX);
        this.debugProperties = debugProperties2;
        this.settings = settings2;
    }

    public MobileAdsLogger withMaxLength(int i) {
        this.maxLength = i;
        return this;
    }

    public MobileAdsLogger withLogTag(String str) {
        Logger logger2 = this.logger;
        StringBuilder sb = new StringBuilder();
        sb.append("AmazonMobileAds ");
        sb.append(str);
        logger2.withLogTag(sb.toString());
        return this;
    }

    public void enableLogging(boolean z) {
        this.settings.putTransientBoolean(LOGGING_ENABLED, z);
    }

    public boolean canLog() {
        if (this.logger == null || this.debugProperties == null) {
            return false;
        }
        return this.debugProperties.getDebugPropertyAsBoolean(DebugProperties.DEBUG_LOGGING, Boolean.valueOf(this.settings.getBoolean(LOGGING_ENABLED, false))).booleanValue();
    }

    public final void enableLoggingWithSetterNotification(boolean z) {
        if (!z) {
            logSetterNotification("Debug logging", Boolean.valueOf(z));
        }
        enableLogging(z);
        if (z) {
            logSetterNotification("Debug logging", Boolean.valueOf(z));
            d("Amazon Mobile Ads API Version: %s", Version.getRawSDKVersion());
        }
    }

    public void logSetterNotification(String str, Object obj) {
        if (canLog()) {
            if (obj instanceof Boolean) {
                String str2 = "%s has been %s.";
                Object[] objArr = new Object[2];
                objArr[0] = str;
                objArr[1] = ((Boolean) obj).booleanValue() ? String.ENABLED : "disabled";
                d(str2, objArr);
            } else {
                d("%s has been set: %s", str, String.valueOf(obj));
            }
        }
    }

    public void i(String str, Object... objArr) {
        log(Level.INFO, str, objArr);
    }

    public void v(String str, Object... objArr) {
        log(Level.VERBOSE, str, objArr);
    }

    public void d(String str, Object... objArr) {
        log(Level.DEBUG, str, objArr);
    }

    public void w(String str, Object... objArr) {
        log(Level.WARN, str, objArr);
    }

    public void e(String str, Object... objArr) {
        log(Level.ERROR, str, objArr);
    }

    public void i(String str) {
        i(str, null);
    }

    public void v(String str) {
        v(str, null);
    }

    public void d(String str) {
        d(str, null);
    }

    public void w(String str) {
        w(str, null);
    }

    public void e(String str) {
        e(str, null);
    }

    public void log(Level level, String str, Object... objArr) {
        doLog(false, level, str, objArr);
    }

    public void forceLog(Level level, String str, Object... objArr) {
        doLog(true, level, str, objArr);
    }

    private void doLog(boolean z, Level level, String str, Object... objArr) {
        if (canLog() || z) {
            for (String str2 : formatAndSplit(str, objArr)) {
                switch (level) {
                    case DEBUG:
                        this.logger.d(str2);
                        break;
                    case ERROR:
                        this.logger.e(str2);
                        break;
                    case INFO:
                        this.logger.i(str2);
                        break;
                    case VERBOSE:
                        this.logger.v(str2);
                        break;
                    case WARN:
                        this.logger.w(str2);
                        break;
                }
            }
        }
    }

    private Iterable<String> formatAndSplit(String str, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str = String.format(str, objArr);
        }
        return split(str, this.maxLength);
    }

    private Iterable<String> split(String str, int i) {
        ArrayList arrayList = new ArrayList();
        if (str == null || str.length() == 0) {
            return arrayList;
        }
        int i2 = 0;
        while (i2 < str.length()) {
            int i3 = i2 + i;
            arrayList.add(str.substring(i2, Math.min(str.length(), i3)));
            i2 = i3;
        }
        return arrayList;
    }
}
