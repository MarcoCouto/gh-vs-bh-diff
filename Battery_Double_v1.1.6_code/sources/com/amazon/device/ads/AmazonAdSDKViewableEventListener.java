package com.amazon.device.ads;

import com.amazon.device.ads.SDKEvent.SDKEventType;

class AmazonAdSDKViewableEventListener implements SDKEventListener {
    private static final String LOGTAG = "AmazonAdSDKViewableEventListener";
    private final MobileAdsLogger logger;

    /* renamed from: com.amazon.device.ads.AmazonAdSDKViewableEventListener$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$amazon$device$ads$SDKEvent$SDKEventType = new int[SDKEventType.values().length];

        static {
            try {
                $SwitchMap$com$amazon$device$ads$SDKEvent$SDKEventType[SDKEventType.VIEWABLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public AmazonAdSDKViewableEventListener() {
        this(new MobileAdsLoggerFactory());
    }

    AmazonAdSDKViewableEventListener(MobileAdsLoggerFactory mobileAdsLoggerFactory) {
        this.logger = mobileAdsLoggerFactory.createMobileAdsLogger(LOGTAG);
    }

    public void onSDKEvent(SDKEvent sDKEvent, AdControlAccessor adControlAccessor) {
        this.logger.d(sDKEvent.getEventType().toString());
        if (AnonymousClass1.$SwitchMap$com$amazon$device$ads$SDKEvent$SDKEventType[sDKEvent.getEventType().ordinal()] == 1) {
            handleViewableEvent(adControlAccessor, sDKEvent);
        }
    }

    public void handleViewableEvent(AdControlAccessor adControlAccessor, SDKEvent sDKEvent) {
        String parameter = sDKEvent.getParameter(ViewabilityObserver.VIEWABLE_PARAMS_KEY);
        StringBuilder sb = new StringBuilder();
        sb.append("viewableBridge.viewabilityChange('");
        sb.append(parameter);
        sb.append("');");
        adControlAccessor.injectJavascript(sb.toString());
    }
}
