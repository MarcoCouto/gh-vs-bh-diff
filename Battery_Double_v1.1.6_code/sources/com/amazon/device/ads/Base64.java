package com.amazon.device.ads;

import com.ironsource.sdk.constants.Constants.RequestParameters;

class Base64 {
    private static final String ENCODE_CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    Base64() {
    }

    public static byte[] decode(String str) {
        int i;
        if (!StringUtils.isNullOrWhiteSpace(str)) {
            int decodedLength = getDecodedLength(str);
            if (decodedLength > 0) {
                byte[] bArr = new byte[decodedLength];
                int i2 = 0;
                int i3 = 0;
                while (i2 < str.length() && i3 < decodedLength) {
                    int i4 = i2 % 4;
                    if (i4 != 0 || str.length() >= i2 + 4) {
                        int indexOf = ENCODE_CHARSET.indexOf(str.charAt(i2));
                        if (indexOf != -1) {
                            switch (i4) {
                                case 0:
                                    bArr[i3] = (byte) (indexOf << 2);
                                    continue;
                                case 1:
                                    i = i3 + 1;
                                    bArr[i3] = (byte) (bArr[i3] | ((byte) ((indexOf >> 4) & 3)));
                                    if (i < decodedLength) {
                                        bArr[i] = (byte) (indexOf << 4);
                                        break;
                                    }
                                    break;
                                case 2:
                                    i = i3 + 1;
                                    bArr[i3] = (byte) (bArr[i3] | ((byte) ((indexOf >> 2) & 15)));
                                    if (i < decodedLength) {
                                        bArr[i] = (byte) (indexOf << 6);
                                        break;
                                    }
                                    break;
                                case 3:
                                    i = i3 + 1;
                                    bArr[i3] = (byte) (((byte) (indexOf & 63)) | bArr[i3]);
                                    break;
                            }
                            i3 = i;
                            i2++;
                        }
                    }
                }
                return bArr;
            }
            throw new IllegalArgumentException("Encoded String decodes to zero bytes");
        }
        throw new IllegalArgumentException("Encoded String must not be null or white space");
    }

    private static int getDecodedLength(String str) {
        int indexOf = str.indexOf(RequestParameters.EQUAL);
        return (((str.length() + 3) / 4) * 3) - (indexOf > -1 ? str.length() - indexOf : 0);
    }
}
