package com.iab.omid.library.startapp.publisher;

import android.os.Handler;
import android.webkit.WebView;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.iab.omid.library.startapp.b.c;
import com.iab.omid.library.startapp.b.d;
import com.startapp.sdk.ads.banner.banner3d.Banner3DSize;
import java.util.List;

public final class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */
    public WebView a;
    private List<Banner3DSize> b;
    private final String c;

    public b(List<Banner3DSize> list, String str) {
        this.b = list;
        this.c = str;
    }

    public final void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {
            private WebView a = b.this.a;

            public final void run() {
                this.a.destroy();
            }
        }, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
        this.a = null;
    }

    public final void a() {
        super.a();
        this.a = new WebView(c.a().b());
        this.a.getSettings().setJavaScriptEnabled(true);
        a(this.a);
        d.a();
        d.a(this.a, this.c);
        for (Banner3DSize b2 : this.b) {
            String externalForm = b2.b().toExternalForm();
            d.a();
            WebView webView = this.a;
            if (externalForm != null) {
                d.a(webView, "var script=document.createElement('script');script.setAttribute(\"type\",\"text/javascript\");script.setAttribute(\"src\",\"%SCRIPT_SRC%\");document.body.appendChild(script);".replace("%SCRIPT_SRC%", externalForm));
            }
        }
    }
}
