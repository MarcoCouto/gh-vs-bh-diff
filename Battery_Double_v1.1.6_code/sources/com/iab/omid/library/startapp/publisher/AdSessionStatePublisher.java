package com.iab.omid.library.startapp.publisher;

import android.webkit.WebView;
import com.iab.omid.library.startapp.b.d;
import com.startapp.sdk.ads.banner.bannerstandard.b;
import org.json.JSONObject;

public abstract class AdSessionStatePublisher {
    private com.iab.omid.library.startapp.e.a a = new com.iab.omid.library.startapp.e.a((WebView) null);
    private com.iab.omid.library.startapp.adsession.a b;
    private b c;
    private a d;
    private double e;

    enum a {
        AD_STATE_IDLE,
        AD_STATE_VISIBLE,
        AD_STATE_HIDDEN
    }

    public AdSessionStatePublisher() {
        f();
    }

    public void a() {
    }

    public final void a(float f) {
        d.a().a(c(), f);
    }

    /* access modifiers changed from: 0000 */
    public final void a(WebView webView) {
        this.a = new com.iab.omid.library.startapp.e.a(webView);
    }

    public final void a(com.iab.omid.library.startapp.adsession.a aVar) {
        this.b = aVar;
    }

    public final void a(b bVar) {
        this.c = bVar;
    }

    public final void a(String str) {
        d.a().a(c(), str, (JSONObject) null);
    }

    public final void a(String str, double d2) {
        if (d2 > this.e) {
            this.d = a.AD_STATE_VISIBLE;
            d.a().b(c(), str);
        }
    }

    public final void a(String str, JSONObject jSONObject) {
        d.a().a(c(), str, jSONObject);
    }

    public void b() {
        this.a.clear();
    }

    public final void b(String str, double d2) {
        if (d2 > this.e && this.d != a.AD_STATE_HIDDEN) {
            this.d = a.AD_STATE_HIDDEN;
            d.a().b(c(), str);
        }
    }

    public final WebView c() {
        return (WebView) this.a.get();
    }

    public final com.iab.omid.library.startapp.adsession.a d() {
        return this.b;
    }

    public final b e() {
        return this.c;
    }

    public final void f() {
        this.e = com.iab.omid.library.startapp.b.b();
        this.d = a.AD_STATE_IDLE;
    }

    public final void a(boolean z) {
        if (this.a.get() != null) {
            d.a().c(c(), z ? "foregrounded" : "backgrounded");
        }
    }
}
