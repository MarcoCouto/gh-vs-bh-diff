package com.iab.omid.library.startapp.b;

import android.content.Context;
import android.os.Handler;
import com.iab.omid.library.startapp.a.a;
import com.iab.omid.library.startapp.a.b;
import com.startapp.common.b.c;

public final class e implements a, b.a {
    private static e a;
    private float b = 0.0f;
    private final c c;
    private final com.iab.omid.library.startapp.d.c d;
    private b e;
    private a f;

    private e(c cVar, com.iab.omid.library.startapp.d.c cVar2) {
        this.c = cVar;
        this.d = cVar2;
    }

    public static e a() {
        if (a == null) {
            a = new e(new c(), new com.iab.omid.library.startapp.d.c());
        }
        return a;
    }

    public final void a(boolean z) {
        if (z) {
            com.iab.omid.library.startapp.walking.a.a();
            com.iab.omid.library.startapp.walking.a.b();
            return;
        }
        com.iab.omid.library.startapp.walking.a.a();
        com.iab.omid.library.startapp.walking.a.d();
    }

    public final void b() {
        b.a().a((b.a) this);
        b.a().b();
        if (b.a().d()) {
            com.iab.omid.library.startapp.walking.a.a();
            com.iab.omid.library.startapp.walking.a.b();
        }
        this.e.a();
    }

    public final void c() {
        com.iab.omid.library.startapp.walking.a.a().c();
        b.a().c();
        this.e.b();
    }

    public final float d() {
        return this.b;
    }

    public final void a(Context context) {
        this.e = new b(new Handler(), context, new com.iab.omid.library.startapp.d.a(), this);
    }

    public final void a(float f2) {
        this.b = f2;
        if (this.f == null) {
            this.f = a.a();
        }
        for (com.iab.omid.library.startapp.adsession.b e2 : this.f.c()) {
            e2.e().a(f2);
        }
    }
}
