package com.iab.omid.library.startapp.b;

import android.annotation.SuppressLint;
import android.content.Context;

public final class c {
    @SuppressLint({"StaticFieldLeak"})
    private static c a = new c();
    private Context b;

    private c() {
    }

    public static c a() {
        return a;
    }

    public final void a(Context context) {
        this.b = context != null ? context.getApplicationContext() : null;
    }

    public final Context b() {
        return this.b;
    }
}
