package com.iab.omid.library.startapp.c;

import android.view.View;
import com.iab.omid.library.startapp.b.a;
import com.iab.omid.library.startapp.c.a.C0042a;
import com.iab.omid.library.startapp.d.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import org.json.JSONObject;

public final class b implements a {
    private final a a;

    public b(a aVar) {
        this.a = aVar;
    }

    public final JSONObject a(View view) {
        return com.iab.omid.library.startapp.d.b.a(0, 0, 0, 0);
    }

    public final void a(View view, JSONObject jSONObject, C0042a aVar, boolean z) {
        ArrayList arrayList = new ArrayList();
        a a2 = a.a();
        if (a2 != null) {
            Collection<com.iab.omid.library.startapp.adsession.b> c = a2.c();
            IdentityHashMap identityHashMap = new IdentityHashMap((c.size() << 1) + 3);
            for (com.iab.omid.library.startapp.adsession.b g : c) {
                View g2 = g.g();
                if (g2 != null && c.b(g2)) {
                    View rootView = g2.getRootView();
                    if (rootView != null && !identityHashMap.containsKey(rootView)) {
                        identityHashMap.put(rootView, rootView);
                        float a3 = c.a(rootView);
                        int size = arrayList.size();
                        while (size > 0 && c.a((View) arrayList.get(size - 1)) > a3) {
                            size--;
                        }
                        arrayList.add(size, rootView);
                    }
                }
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            aVar.a((View) it.next(), this.a, jSONObject);
        }
    }
}
