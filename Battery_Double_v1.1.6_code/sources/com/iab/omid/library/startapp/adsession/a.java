package com.iab.omid.library.startapp.adsession;

import com.iab.omid.library.startapp.b;

public final class a {
    private final b a;

    private a(b bVar) {
        this.a = bVar;
    }

    public static a a(b bVar) {
        b bVar2 = bVar;
        b.a((Object) bVar, "AdSession is null");
        if (bVar2.e().d() == null) {
            b.a(bVar2);
            a aVar = new a(bVar2);
            bVar2.e().a(aVar);
            return aVar;
        }
        throw new IllegalStateException("AdEvents already exists for AdSession");
    }

    public final void a() {
        b.a(this.a);
        if (this.a.k()) {
            if (!this.a.h()) {
                try {
                    this.a.a();
                } catch (Exception unused) {
                }
            }
            if (this.a.h()) {
                this.a.d();
                return;
            }
            return;
        }
        throw new IllegalStateException("Impression event is not expected from the Native AdSession");
    }
}
