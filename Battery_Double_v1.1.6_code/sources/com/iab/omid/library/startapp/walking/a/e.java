package com.iab.omid.library.startapp.walking.a;

import com.iab.omid.library.startapp.adsession.b;
import com.iab.omid.library.startapp.b.a;
import com.iab.omid.library.startapp.walking.a.b.C0043b;
import java.util.HashSet;
import org.json.JSONObject;

public final class e extends a {
    public e(C0043b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void onPostExecute(String str) {
        a a = a.a();
        if (a != null) {
            for (b bVar : a.b()) {
                if (this.a.contains(bVar.f())) {
                    bVar.e().b(str, this.c);
                }
            }
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        return this.b.toString();
    }
}
