package com.iab.omid.library.startapp.walking.a;

import android.os.AsyncTask;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public abstract class b extends AsyncTask<Object, Void, String> {
    private a a;
    protected final C0043b d;

    public interface a {
        void a();
    }

    /* renamed from: com.iab.omid.library.startapp.walking.a.b$b reason: collision with other inner class name */
    public interface C0043b {
        JSONObject a();

        void a(JSONObject jSONObject);
    }

    public b(C0043b bVar) {
        this.d = bVar;
    }

    public final void a(a aVar) {
        this.a = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        if (this.a != null) {
            this.a.a();
        }
    }

    public final void a(ThreadPoolExecutor threadPoolExecutor) {
        executeOnExecutor(threadPoolExecutor, new Object[0]);
    }
}
