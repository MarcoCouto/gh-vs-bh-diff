package com.iab.omid.library.startapp.walking.a;

import com.iab.omid.library.startapp.walking.a.b.C0043b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class a extends b {
    protected final HashSet<String> a;
    protected final JSONObject b;
    protected final double c;

    public a(C0043b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar);
        this.a = new HashSet<>(hashSet);
        this.b = jSONObject;
        this.c = d;
    }
}
