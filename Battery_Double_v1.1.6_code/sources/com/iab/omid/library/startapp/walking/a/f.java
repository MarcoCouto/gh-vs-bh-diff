package com.iab.omid.library.startapp.walking.a;

import android.text.TextUtils;
import com.iab.omid.library.startapp.adsession.b;
import com.iab.omid.library.startapp.b.a;
import com.iab.omid.library.startapp.walking.a.b.C0043b;
import java.util.HashSet;
import org.json.JSONObject;

public final class f extends a {
    public f(C0043b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void onPostExecute(String str) {
        if (!TextUtils.isEmpty(str)) {
            a a = a.a();
            if (a != null) {
                for (b bVar : a.b()) {
                    if (this.a.contains(bVar.f())) {
                        bVar.e().a(str, this.c);
                    }
                }
            }
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        if (com.iab.omid.library.startapp.d.b.b(this.b, this.d.a())) {
            return null;
        }
        this.d.a(this.b);
        return this.b.toString();
    }
}
