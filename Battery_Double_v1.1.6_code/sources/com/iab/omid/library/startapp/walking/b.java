package com.iab.omid.library.startapp.walking;

import android.support.annotation.VisibleForTesting;
import com.iab.omid.library.startapp.walking.a.b.C0043b;
import com.iab.omid.library.startapp.walking.a.c;
import com.iab.omid.library.startapp.walking.a.d;
import com.iab.omid.library.startapp.walking.a.e;
import com.iab.omid.library.startapp.walking.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public final class b implements C0043b {
    private JSONObject a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    @VisibleForTesting
    public final JSONObject a() {
        return this.a;
    }

    @VisibleForTesting
    public final void a(JSONObject jSONObject) {
        this.a = jSONObject;
    }

    public final void a(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        c cVar = this.b;
        f fVar = new f(this, hashSet, jSONObject, d);
        cVar.a(fVar);
    }

    public final void b() {
        this.b.a(new d(this));
    }

    public final void b(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        c cVar = this.b;
        e eVar = new e(this, hashSet, jSONObject, d);
        cVar.a(eVar);
    }
}
