package com.iab.omid.library.startapp.walking.a;

import com.iab.omid.library.startapp.walking.a.b.a;
import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class c implements a {
    private final BlockingQueue<Runnable> a = new LinkedBlockingQueue();
    private final ThreadPoolExecutor b;
    private final ArrayDeque<b> c = new ArrayDeque<>();
    private b d = null;

    public c() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, this.a);
        this.b = threadPoolExecutor;
    }

    private void b() {
        this.d = (b) this.c.poll();
        if (this.d != null) {
            this.d.a(this.b);
        }
    }

    public final void a() {
        this.d = null;
        b();
    }

    public final void a(b bVar) {
        bVar.a((a) this);
        this.c.add(bVar);
        if (this.d == null) {
            b();
        }
    }
}
