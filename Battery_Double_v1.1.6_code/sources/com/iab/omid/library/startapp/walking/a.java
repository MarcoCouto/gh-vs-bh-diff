package com.iab.omid.library.startapp.walking;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.iab.omid.library.startapp.c.a.C0042a;
import com.iab.omid.library.startapp.d.b;
import com.iab.omid.library.startapp.walking.a.c;
import com.startapp.sdk.ads.video.VideoUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class a implements C0042a {
    private static a a = new a();
    private static Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static Handler c = null;
    /* access modifiers changed from: private */
    public static final Runnable j = new Runnable() {
        public final void run() {
            a.b(a.a());
        }
    };
    /* access modifiers changed from: private */
    public static final Runnable k = new Runnable() {
        public final void run() {
            if (a.c != null) {
                a.c.post(a.j);
                a.c.postDelayed(a.k, 200);
            }
        }
    };
    private List<Object> d = new ArrayList();
    private int e;
    private VideoUtil f = new VideoUtil();
    private com.startapp.sdk.adsbase.a g = new com.startapp.sdk.adsbase.a();
    /* access modifiers changed from: private */
    public b h = new b(new c());
    private double i;

    a() {
    }

    public static a a() {
        return a;
    }

    private void a(View view, com.iab.omid.library.startapp.c.a aVar, JSONObject jSONObject, c cVar) {
        aVar.a(view, jSONObject, this, cVar == c.PARENT_VIEW);
    }

    public static void d() {
        h();
    }

    private static void h() {
        if (c != null) {
            c.removeCallbacks(k);
            c = null;
        }
    }

    public static void b() {
        if (c == null) {
            Handler handler = new Handler(Looper.getMainLooper());
            c = handler;
            handler.post(j);
            c.postDelayed(k, 200);
        }
    }

    public final void c() {
        h();
        this.d.clear();
        b.post(new Runnable() {
            public final void run() {
                a.this.h.b();
            }
        });
    }

    public final void a(View view, com.iab.omid.library.startapp.c.a aVar, JSONObject jSONObject) {
        boolean z;
        if (com.iab.omid.library.startapp.d.c.c(view)) {
            c c2 = this.g.c(view);
            if (c2 != c.UNDERLYING_VIEW) {
                JSONObject a2 = aVar.a(view);
                b.a(jSONObject, a2);
                String a3 = this.g.a(view);
                if (a3 != null) {
                    b.a(a2, a3);
                    this.g.g();
                    z = true;
                } else {
                    z = false;
                }
                if (!z) {
                    ArrayList b2 = this.g.b(view);
                    if (b2 != null) {
                        b.a(a2, (List<String>) b2);
                    }
                    a(view, aVar, a2, c2);
                }
                this.e++;
            }
        }
    }

    static /* synthetic */ void b(a aVar) {
        aVar.e = 0;
        aVar.i = com.iab.omid.library.startapp.b.b();
        aVar.g.e();
        double b2 = com.iab.omid.library.startapp.b.b();
        com.iab.omid.library.startapp.c.a a2 = aVar.f.a();
        if (aVar.g.d().size() > 0) {
            aVar.h.b(a2.a(null), aVar.g.d(), b2);
        }
        if (aVar.g.c().size() > 0) {
            JSONObject a3 = a2.a(null);
            aVar.a(null, a2, a3, c.PARENT_VIEW);
            b.a(a3);
            aVar.h.a(a3, aVar.g.c(), b2);
        } else {
            aVar.h.b();
        }
        aVar.g.f();
        com.iab.omid.library.startapp.b.b();
        if (aVar.d.size() > 0) {
            Iterator it = aVar.d.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }
}
