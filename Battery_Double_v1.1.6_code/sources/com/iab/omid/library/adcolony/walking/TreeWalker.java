package com.iab.omid.library.adcolony.walking;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.iab.omid.library.adcolony.c.a.C0032a;
import com.iab.omid.library.adcolony.d.d;
import com.iab.omid.library.adcolony.d.f;
import com.iab.omid.library.adcolony.walking.a.C0033a;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class TreeWalker implements C0032a {
    private static TreeWalker g = new TreeWalker();
    private static Handler h = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static Handler i = null;
    /* access modifiers changed from: private */
    public static final Runnable j = new b();
    /* access modifiers changed from: private */
    public static final Runnable k = new c();
    private List<TreeWalkerTimeLogger> a = new ArrayList();
    private int b;
    private com.iab.omid.library.adcolony.c.b c = new com.iab.omid.library.adcolony.c.b();
    private a d = new a();
    /* access modifiers changed from: private */
    public b e = new b(new com.iab.omid.library.adcolony.walking.a.c());
    private long f;

    public interface TreeWalkerNanoTimeLogger extends TreeWalkerTimeLogger {
        void onTreeProcessedNano(int i, long j);
    }

    public interface TreeWalkerTimeLogger {
        void onTreeProcessed(int i, long j);
    }

    class a implements Runnable {
        a() {
        }

        public void run() {
            TreeWalker.this.e.a();
        }
    }

    static class b implements Runnable {
        b() {
        }

        public void run() {
            TreeWalker.getInstance().h();
        }
    }

    static class c implements Runnable {
        c() {
        }

        public void run() {
            if (TreeWalker.i != null) {
                TreeWalker.i.post(TreeWalker.j);
                TreeWalker.i.postDelayed(TreeWalker.k, 200);
            }
        }
    }

    TreeWalker() {
    }

    private void a(long j2) {
        if (this.a.size() > 0) {
            for (TreeWalkerTimeLogger treeWalkerTimeLogger : this.a) {
                treeWalkerTimeLogger.onTreeProcessed(this.b, TimeUnit.NANOSECONDS.toMillis(j2));
                if (treeWalkerTimeLogger instanceof TreeWalkerNanoTimeLogger) {
                    ((TreeWalkerNanoTimeLogger) treeWalkerTimeLogger).onTreeProcessedNano(this.b, j2);
                }
            }
        }
    }

    private void a(View view, com.iab.omid.library.adcolony.c.a aVar, JSONObject jSONObject, c cVar) {
        aVar.a(view, jSONObject, this, cVar == c.PARENT_VIEW);
    }

    private void a(String str, View view, JSONObject jSONObject) {
        com.iab.omid.library.adcolony.c.a b2 = this.c.b();
        String a2 = this.d.a(str);
        if (a2 != null) {
            JSONObject a3 = b2.a(view);
            com.iab.omid.library.adcolony.d.b.a(a3, str);
            com.iab.omid.library.adcolony.d.b.b(a3, a2);
            com.iab.omid.library.adcolony.d.b.a(jSONObject, a3);
        }
    }

    private boolean a(View view, JSONObject jSONObject) {
        String a2 = this.d.a(view);
        if (a2 == null) {
            return false;
        }
        com.iab.omid.library.adcolony.d.b.a(jSONObject, a2);
        this.d.e();
        return true;
    }

    private void b(View view, JSONObject jSONObject) {
        C0033a b2 = this.d.b(view);
        if (b2 != null) {
            com.iab.omid.library.adcolony.d.b.a(jSONObject, b2);
        }
    }

    public static TreeWalker getInstance() {
        return g;
    }

    /* access modifiers changed from: private */
    public void h() {
        i();
        d();
        j();
    }

    private void i() {
        this.b = 0;
        this.f = d.a();
    }

    private void j() {
        a(d.a() - this.f);
    }

    private void k() {
        if (i == null) {
            Handler handler = new Handler(Looper.getMainLooper());
            i = handler;
            handler.post(j);
            i.postDelayed(k, 200);
        }
    }

    private void l() {
        Handler handler = i;
        if (handler != null) {
            handler.removeCallbacks(k);
            i = null;
        }
    }

    public void a() {
        k();
    }

    public void a(View view, com.iab.omid.library.adcolony.c.a aVar, JSONObject jSONObject) {
        if (f.d(view)) {
            c c2 = this.d.c(view);
            if (c2 != c.UNDERLYING_VIEW) {
                JSONObject a2 = aVar.a(view);
                com.iab.omid.library.adcolony.d.b.a(jSONObject, a2);
                if (!a(view, a2)) {
                    b(view, a2);
                    a(view, aVar, a2, c2);
                }
                this.b++;
            }
        }
    }

    public void addTimeLogger(TreeWalkerTimeLogger treeWalkerTimeLogger) {
        if (!this.a.contains(treeWalkerTimeLogger)) {
            this.a.add(treeWalkerTimeLogger);
        }
    }

    public void b() {
        c();
        this.a.clear();
        h.post(new a());
    }

    public void c() {
        l();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void d() {
        this.d.c();
        long a2 = d.a();
        com.iab.omid.library.adcolony.c.a a3 = this.c.a();
        if (this.d.b().size() > 0) {
            Iterator it = this.d.b().iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                JSONObject a4 = a3.a(null);
                a(str, this.d.b(str), a4);
                com.iab.omid.library.adcolony.d.b.a(a4);
                HashSet hashSet = new HashSet();
                hashSet.add(str);
                this.e.b(a4, hashSet, a2);
            }
        }
        if (this.d.a().size() > 0) {
            JSONObject a5 = a3.a(null);
            a(null, a3, a5, c.PARENT_VIEW);
            com.iab.omid.library.adcolony.d.b.a(a5);
            this.e.a(a5, this.d.a(), a2);
        } else {
            this.e.a();
        }
        this.d.d();
    }

    public void removeTimeLogger(TreeWalkerTimeLogger treeWalkerTimeLogger) {
        if (this.a.contains(treeWalkerTimeLogger)) {
            this.a.remove(treeWalkerTimeLogger);
        }
    }
}
