package com.iab.omid.library.adcolony.adsession.video;

import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;

public enum Position {
    PREROLL(BreakId.PREROLL),
    MIDROLL(BreakId.MIDROLL),
    POSTROLL(BreakId.POSTROLL),
    STANDALONE("standalone");
    
    private final String a;

    private Position(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
