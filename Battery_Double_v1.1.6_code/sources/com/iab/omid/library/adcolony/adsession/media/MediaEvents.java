package com.iab.omid.library.adcolony.adsession.media;

import com.iab.omid.library.adcolony.adsession.AdSession;
import com.iab.omid.library.adcolony.adsession.a;
import com.iab.omid.library.adcolony.b.f;
import com.iab.omid.library.adcolony.d.b;
import com.iab.omid.library.adcolony.d.e;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public final class MediaEvents {
    private final a a;

    private MediaEvents(a aVar) {
        this.a = aVar;
    }

    private void a(float f) {
        if (f <= 0.0f) {
            throw new IllegalArgumentException("Invalid Media duration");
        }
    }

    private void b(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Invalid Media volume");
        }
    }

    public static MediaEvents createMediaEvents(AdSession adSession) {
        a aVar = (a) adSession;
        e.a((Object) adSession, "AdSession is null");
        e.g(aVar);
        e.a(aVar);
        e.b(aVar);
        e.e(aVar);
        MediaEvents mediaEvents = new MediaEvents(aVar);
        aVar.getAdSessionStatePublisher().a(mediaEvents);
        return mediaEvents;
    }

    public void adUserInteraction(InteractionType interactionType) {
        e.a((Object) interactionType, "InteractionType is null");
        e.c(this.a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "interactionType", interactionType);
        this.a.getAdSessionStatePublisher().a("adUserInteraction", jSONObject);
    }

    public void bufferFinish() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a("bufferFinish");
    }

    public void bufferStart() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a(String.VIDEO_BUFFER_START);
    }

    public void complete() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a("complete");
    }

    public void firstQuartile() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a("firstQuartile");
    }

    public void loaded(VastProperties vastProperties) {
        e.a((Object) vastProperties, "VastProperties is null");
        e.b(this.a);
        this.a.getAdSessionStatePublisher().a(ParametersKeys.LOADED, vastProperties.a());
    }

    public void midpoint() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a("midpoint");
    }

    public void pause() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a("pause");
    }

    public void playerStateChange(PlayerState playerState) {
        e.a((Object) playerState, "PlayerState is null");
        e.c(this.a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "state", playerState);
        this.a.getAdSessionStatePublisher().a("playerStateChange", jSONObject);
    }

    public void resume() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a("resume");
    }

    public void skipped() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a(String.VIDEO_SKIPPED);
    }

    public void start(float f, float f2) {
        a(f);
        b(f2);
        e.c(this.a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "duration", Float.valueOf(f));
        b.a(jSONObject, "mediaPlayerVolume", Float.valueOf(f2));
        b.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(f.a().d()));
        this.a.getAdSessionStatePublisher().a("start", jSONObject);
    }

    public void thirdQuartile() {
        e.c(this.a);
        this.a.getAdSessionStatePublisher().a("thirdQuartile");
    }

    public void volumeChange(float f) {
        b(f);
        e.c(this.a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "mediaPlayerVolume", Float.valueOf(f));
        b.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(f.a().d()));
        this.a.getAdSessionStatePublisher().a("volumeChange", jSONObject);
    }
}
