package com.iab.omid.library.adcolony.adsession;

import com.google.android.exoplayer2.util.MimeTypes;

public enum CreativeType {
    DEFINED_BY_JAVASCRIPT("definedByJavaScript"),
    HTML_DISPLAY("htmlDisplay"),
    NATIVE_DISPLAY("nativeDisplay"),
    VIDEO("video"),
    AUDIO(MimeTypes.BASE_TYPE_AUDIO);
    
    private final String a;

    private CreativeType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
