package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.e;
import java.net.URL;

public final class VerificationScriptResource {
    private final String a;
    private final URL b;
    private final String c;

    private VerificationScriptResource(String str, URL url, String str2) {
        this.a = str;
        this.b = url;
        this.c = str2;
    }

    public static VerificationScriptResource createVerificationScriptResourceWithParameters(String str, URL url, String str2) {
        e.a(str, "VendorKey is null or empty");
        e.a((Object) url, "ResourceURL is null");
        e.a(str2, "VerificationParameters is null or empty");
        return new VerificationScriptResource(str, url, str2);
    }

    public static VerificationScriptResource createVerificationScriptResourceWithoutParameters(String str, URL url) {
        e.a(str, "VendorKey is null or empty");
        e.a((Object) url, "ResourceURL is null");
        return new VerificationScriptResource(str, url, null);
    }

    public static VerificationScriptResource createVerificationScriptResourceWithoutParameters(URL url) {
        e.a((Object) url, "ResourceURL is null");
        return new VerificationScriptResource(null, url, null);
    }

    public URL getResourceUrl() {
        return this.b;
    }

    public String getVendorKey() {
        return this.a;
    }

    public String getVerificationParameters() {
        return this.c;
    }
}
