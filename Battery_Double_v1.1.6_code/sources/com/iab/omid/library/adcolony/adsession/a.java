package com.iab.omid.library.adcolony.adsession;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.iab.omid.library.adcolony.b.c;
import com.iab.omid.library.adcolony.b.f;
import com.iab.omid.library.adcolony.d.e;
import com.iab.omid.library.adcolony.publisher.AdSessionStatePublisher;
import com.iab.omid.library.adcolony.publisher.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class a extends AdSession {
    private static final Pattern k = Pattern.compile("^[a-zA-Z0-9 ]+$");
    private final AdSessionContext a;
    private final AdSessionConfiguration b;
    private final List<c> c = new ArrayList();
    private com.iab.omid.library.adcolony.e.a d;
    private AdSessionStatePublisher e;
    private boolean f = false;
    private boolean g = false;
    private String h;
    private boolean i;
    private boolean j;

    a(AdSessionConfiguration adSessionConfiguration, AdSessionContext adSessionContext) {
        this.b = adSessionConfiguration;
        this.a = adSessionContext;
        this.h = UUID.randomUUID().toString();
        c(null);
        this.e = (adSessionContext.getAdSessionContextType() == AdSessionContextType.HTML || adSessionContext.getAdSessionContextType() == AdSessionContextType.JAVASCRIPT) ? new com.iab.omid.library.adcolony.publisher.a(adSessionContext.getWebView()) : new b(adSessionContext.getInjectedResourcesMap(), adSessionContext.getOmidJsScriptContent());
        this.e.a();
        com.iab.omid.library.adcolony.b.a.a().a(this);
        this.e.a(adSessionConfiguration);
    }

    private c a(View view) {
        for (c cVar : this.c) {
            if (cVar.a().get() == view) {
                return cVar;
            }
        }
        return null;
    }

    private void a(String str) {
        if (str == null) {
            return;
        }
        if (str.length() > 50 || !k.matcher(str).matches()) {
            throw new IllegalArgumentException("FriendlyObstruction has improperly formatted detailed reason");
        }
    }

    private void b(View view) {
        if (view == null) {
            throw new IllegalArgumentException("FriendlyObstruction is null");
        }
    }

    private void c(View view) {
        this.d = new com.iab.omid.library.adcolony.e.a(view);
    }

    private void d(View view) {
        Collection<a> b2 = com.iab.omid.library.adcolony.b.a.a().b();
        if (b2 != null && b2.size() > 0) {
            for (a aVar : b2) {
                if (aVar != this && aVar.d() == view) {
                    aVar.d.clear();
                }
            }
        }
    }

    private void j() {
        if (this.i) {
            throw new IllegalStateException("Impression event can only be sent once");
        }
    }

    private void k() {
        if (this.j) {
            throw new IllegalStateException("Loaded event can only be sent once");
        }
    }

    public List<c> a() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull JSONObject jSONObject) {
        k();
        getAdSessionStatePublisher().a(jSONObject);
        this.j = true;
    }

    public void addFriendlyObstruction(View view) {
        addFriendlyObstruction(view, FriendlyObstructionPurpose.OTHER, null);
    }

    public void addFriendlyObstruction(View view, FriendlyObstructionPurpose friendlyObstructionPurpose, @Nullable String str) {
        if (!this.g) {
            b(view);
            a(str);
            if (a(view) == null) {
                this.c.add(new c(view, friendlyObstructionPurpose, str));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        j();
        getAdSessionStatePublisher().g();
        this.i = true;
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        k();
        getAdSessionStatePublisher().h();
        this.j = true;
    }

    public View d() {
        return (View) this.d.get();
    }

    public boolean e() {
        return this.f && !this.g;
    }

    public void error(ErrorType errorType, String str) {
        if (!this.g) {
            e.a((Object) errorType, "Error type is null");
            e.a(str, "Message is null");
            getAdSessionStatePublisher().a(errorType, str);
            return;
        }
        throw new IllegalStateException("AdSession is finished");
    }

    public boolean f() {
        return this.f;
    }

    public void finish() {
        if (!this.g) {
            this.d.clear();
            removeAllFriendlyObstructions();
            this.g = true;
            getAdSessionStatePublisher().f();
            com.iab.omid.library.adcolony.b.a.a().c(this);
            getAdSessionStatePublisher().b();
            this.e = null;
        }
    }

    public boolean g() {
        return this.g;
    }

    public String getAdSessionId() {
        return this.h;
    }

    public AdSessionStatePublisher getAdSessionStatePublisher() {
        return this.e;
    }

    public boolean h() {
        return this.b.isNativeImpressionOwner();
    }

    public boolean i() {
        return this.b.isNativeMediaEventsOwner();
    }

    public void registerAdView(View view) {
        if (!this.g) {
            e.a((Object) view, "AdView is null");
            if (d() != view) {
                c(view);
                getAdSessionStatePublisher().i();
                d(view);
            }
        }
    }

    public void removeAllFriendlyObstructions() {
        if (!this.g) {
            this.c.clear();
        }
    }

    public void removeFriendlyObstruction(View view) {
        if (!this.g) {
            b(view);
            c a2 = a(view);
            if (a2 != null) {
                this.c.remove(a2);
            }
        }
    }

    public void start() {
        if (!this.f) {
            this.f = true;
            com.iab.omid.library.adcolony.b.a.a().b(this);
            this.e.a(f.a().d());
            this.e.a(this, this.a);
        }
    }
}
