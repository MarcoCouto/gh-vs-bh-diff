package com.iab.omid.library.adcolony.adsession;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.ironsource.sdk.constants.Constants.ParametersKeys;

public enum ImpressionType {
    DEFINED_BY_JAVASCRIPT("definedByJavaScript"),
    UNSPECIFIED("unspecified"),
    LOADED(ParametersKeys.LOADED),
    BEGIN_TO_RENDER("beginToRender"),
    ONE_PIXEL("onePixel"),
    VIEWABLE("viewable"),
    AUDIBLE("audible"),
    OTHER(FacebookRequestErrorClassification.KEY_OTHER);
    
    private final String a;

    private ImpressionType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
