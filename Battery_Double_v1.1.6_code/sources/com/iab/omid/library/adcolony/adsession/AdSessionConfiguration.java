package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.b;
import com.iab.omid.library.adcolony.d.e;
import org.json.JSONObject;

public class AdSessionConfiguration {
    private final Owner a;
    private final Owner b;
    private final boolean c;
    private final CreativeType d;
    private final ImpressionType e;

    private AdSessionConfiguration(CreativeType creativeType, ImpressionType impressionType, Owner owner, Owner owner2, boolean z) {
        this.d = creativeType;
        this.e = impressionType;
        this.a = owner;
        if (owner2 == null) {
            this.b = Owner.NONE;
        } else {
            this.b = owner2;
        }
        this.c = z;
    }

    public static AdSessionConfiguration createAdSessionConfiguration(CreativeType creativeType, ImpressionType impressionType, Owner owner, Owner owner2, boolean z) {
        e.a((Object) creativeType, "CreativeType is null");
        e.a((Object) impressionType, "ImpressionType is null");
        e.a((Object) owner, "Impression owner is null");
        e.a(owner, creativeType, impressionType);
        AdSessionConfiguration adSessionConfiguration = new AdSessionConfiguration(creativeType, impressionType, owner, owner2, z);
        return adSessionConfiguration;
    }

    @Deprecated
    public static AdSessionConfiguration createAdSessionConfiguration(Owner owner, Owner owner2) {
        return createAdSessionConfiguration(owner, owner2, true);
    }

    public static AdSessionConfiguration createAdSessionConfiguration(Owner owner, Owner owner2, boolean z) {
        e.a((Object) owner, "Impression owner is null");
        e.a(owner, (CreativeType) null, (ImpressionType) null);
        AdSessionConfiguration adSessionConfiguration = new AdSessionConfiguration(null, null, owner, owner2, z);
        return adSessionConfiguration;
    }

    public boolean isNativeImpressionOwner() {
        return Owner.NATIVE == this.a;
    }

    public boolean isNativeMediaEventsOwner() {
        return Owner.NATIVE == this.b;
    }

    public boolean isNativeVideoEventsOwner() {
        return isNativeMediaEventsOwner();
    }

    public JSONObject toJsonObject() {
        String str;
        Object obj;
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "impressionOwner", this.a);
        if (this.d == null || this.e == null) {
            obj = this.b;
            str = "videoEventsOwner";
        } else {
            b.a(jSONObject, "mediaEventsOwner", this.b);
            b.a(jSONObject, "creativeType", this.d);
            obj = this.e;
            str = "impressionType";
        }
        b.a(jSONObject, str, obj);
        b.a(jSONObject, "isolateVerificationScripts", Boolean.valueOf(this.c));
        return jSONObject;
    }
}
