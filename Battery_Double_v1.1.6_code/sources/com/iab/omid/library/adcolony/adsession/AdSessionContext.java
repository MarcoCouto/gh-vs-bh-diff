package com.iab.omid.library.adcolony.adsession;

import android.support.annotation.Nullable;
import android.webkit.WebView;
import com.iab.omid.library.adcolony.d.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class AdSessionContext {
    private final Partner a;
    private final WebView b;
    private final List<VerificationScriptResource> c = new ArrayList();
    private final Map<String, VerificationScriptResource> d = new HashMap();
    private final String e;
    private final String f;
    @Nullable
    private final String g;
    private final AdSessionContextType h;

    private AdSessionContext(Partner partner, WebView webView, String str, List<VerificationScriptResource> list, @Nullable String str2, String str3, AdSessionContextType adSessionContextType) {
        this.a = partner;
        this.b = webView;
        this.e = str;
        this.h = adSessionContextType;
        if (list != null) {
            this.c.addAll(list);
            for (VerificationScriptResource verificationScriptResource : list) {
                this.d.put(UUID.randomUUID().toString(), verificationScriptResource);
            }
        }
        this.g = str2;
        this.f = str3;
    }

    public static AdSessionContext createHtmlAdSessionContext(Partner partner, WebView webView, String str) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) webView, "WebView is null");
        if (str != null) {
            e.a(str, 256, "CustomReferenceData is greater than 256 characters");
        }
        AdSessionContext adSessionContext = new AdSessionContext(partner, webView, null, null, null, str, AdSessionContextType.HTML);
        return adSessionContext;
    }

    public static AdSessionContext createHtmlAdSessionContext(Partner partner, WebView webView, @Nullable String str, String str2) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) webView, "WebView is null");
        if (str2 != null) {
            e.a(str2, 256, "CustomReferenceData is greater than 256 characters");
        }
        AdSessionContext adSessionContext = new AdSessionContext(partner, webView, null, null, str, str2, AdSessionContextType.HTML);
        return adSessionContext;
    }

    public static AdSessionContext createJavascriptAdSessionContext(Partner partner, WebView webView, @Nullable String str, String str2) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) webView, "WebView is null");
        if (str2 != null) {
            e.a(str2, 256, "CustomReferenceData is greater than 256 characters");
        }
        AdSessionContext adSessionContext = new AdSessionContext(partner, webView, null, null, str, str2, AdSessionContextType.JAVASCRIPT);
        return adSessionContext;
    }

    public static AdSessionContext createNativeAdSessionContext(Partner partner, String str, List<VerificationScriptResource> list, String str2) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) str, "OM SDK JS script content is null");
        e.a((Object) list, "VerificationScriptResources is null");
        if (str2 != null) {
            e.a(str2, 256, "CustomReferenceData is greater than 256 characters");
        }
        AdSessionContext adSessionContext = new AdSessionContext(partner, null, str, list, null, str2, AdSessionContextType.NATIVE);
        return adSessionContext;
    }

    public static AdSessionContext createNativeAdSessionContext(Partner partner, String str, List<VerificationScriptResource> list, @Nullable String str2, String str3) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) str, "OM SDK JS script content is null");
        e.a((Object) list, "VerificationScriptResources is null");
        if (str3 != null) {
            e.a(str3, 256, "CustomReferenceData is greater than 256 characters");
        }
        AdSessionContext adSessionContext = new AdSessionContext(partner, null, str, list, str2, str3, AdSessionContextType.NATIVE);
        return adSessionContext;
    }

    public AdSessionContextType getAdSessionContextType() {
        return this.h;
    }

    @Nullable
    public String getContentUrl() {
        return this.g;
    }

    public String getCustomReferenceData() {
        return this.f;
    }

    public Map<String, VerificationScriptResource> getInjectedResourcesMap() {
        return Collections.unmodifiableMap(this.d);
    }

    public String getOmidJsScriptContent() {
        return this.e;
    }

    public Partner getPartner() {
        return this.a;
    }

    public List<VerificationScriptResource> getVerificationScriptResources() {
        return Collections.unmodifiableList(this.c);
    }

    public WebView getWebView() {
        return this.b;
    }
}
