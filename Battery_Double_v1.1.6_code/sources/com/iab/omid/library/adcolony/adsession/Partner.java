package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.e;

public class Partner {
    private final String a;
    private final String b;

    private Partner(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public static Partner createPartner(String str, String str2) {
        e.a(str, "Name is null or empty");
        e.a(str2, "Version is null or empty");
        return new Partner(str, str2);
    }

    public String getName() {
        return this.a;
    }

    public String getVersion() {
        return this.b;
    }
}
