package com.iab.omid.library.adcolony.adsession.video;

import com.iab.omid.library.adcolony.adsession.AdSession;
import com.iab.omid.library.adcolony.adsession.media.InteractionType;
import com.iab.omid.library.adcolony.adsession.media.MediaEvents;
import com.iab.omid.library.adcolony.adsession.media.PlayerState;
import com.iab.omid.library.adcolony.d.e;

public final class VideoEvents {
    private final MediaEvents a;

    private VideoEvents(MediaEvents mediaEvents) {
        this.a = mediaEvents;
    }

    public static VideoEvents createVideoEvents(AdSession adSession) {
        try {
            return new VideoEvents(MediaEvents.createMediaEvents(adSession));
        } catch (IllegalStateException e) {
            if (!"Cannot create MediaEvents for JavaScript AdSession".equalsIgnoreCase(e.getMessage())) {
                if ("MediaEvents already exists for AdSession".equalsIgnoreCase(e.getMessage())) {
                    throw new IllegalStateException("VideoEvents already exists for AdSession");
                }
                throw e;
            }
            throw new IllegalStateException("Cannot create VideoEvents for JavaScript AdSession");
        }
    }

    public void adUserInteraction(InteractionType interactionType) {
        e.a((Object) interactionType, "InteractionType is null");
        this.a.adUserInteraction(InteractionType.valueOf(interactionType.toString().toUpperCase()));
    }

    public void bufferFinish() {
        this.a.bufferFinish();
    }

    public void bufferStart() {
        this.a.bufferStart();
    }

    public void complete() {
        this.a.complete();
    }

    public void firstQuartile() {
        this.a.firstQuartile();
    }

    public void loaded(VastProperties vastProperties) {
        e.a((Object) vastProperties, "VastProperties is null");
        this.a.loaded(vastProperties.a());
    }

    public void midpoint() {
        this.a.midpoint();
    }

    public void pause() {
        this.a.pause();
    }

    public void playerStateChange(PlayerState playerState) {
        e.a((Object) playerState, "PlayerState is null");
        this.a.playerStateChange(PlayerState.valueOf(playerState.toString().toUpperCase()));
    }

    public void resume() {
        this.a.resume();
    }

    public void skipped() {
        this.a.skipped();
    }

    public void start(float f, float f2) {
        this.a.start(f, f2);
    }

    public void thirdQuartile() {
        this.a.thirdQuartile();
    }

    public void volumeChange(float f) {
        this.a.volumeChange(f);
    }
}
