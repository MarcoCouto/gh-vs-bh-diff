package com.iab.omid.library.adcolony.adsession.media;

import com.tapjoy.TJAdUnitConstants.String;

public enum InteractionType {
    CLICK(String.CLICK),
    INVITATION_ACCEPTED("invitationAccept");
    
    String a;

    private InteractionType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
