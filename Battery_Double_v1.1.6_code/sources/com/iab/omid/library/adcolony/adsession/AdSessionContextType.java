package com.iab.omid.library.adcolony.adsession;

import com.tapjoy.TJAdUnitConstants.String;

public enum AdSessionContextType {
    HTML(String.HTML),
    NATIVE("native"),
    JAVASCRIPT("javascript");
    
    private final String a;

    private AdSessionContextType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
