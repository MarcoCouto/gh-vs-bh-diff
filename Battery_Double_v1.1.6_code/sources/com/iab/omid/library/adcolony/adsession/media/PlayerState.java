package com.iab.omid.library.adcolony.adsession.media;

import com.yandex.mobile.ads.video.tracking.Tracker.Events;

public enum PlayerState {
    MINIMIZED("minimized"),
    COLLAPSED("collapsed"),
    NORMAL("normal"),
    EXPANDED("expanded"),
    FULLSCREEN(Events.CREATIVE_FULLSCREEN);
    
    private final String a;

    private PlayerState(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
