package com.iab.omid.library.adcolony.adsession;

public enum Owner {
    NATIVE("native"),
    JAVASCRIPT("javascript"),
    NONE("none");
    
    private final String a;

    private Owner(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
