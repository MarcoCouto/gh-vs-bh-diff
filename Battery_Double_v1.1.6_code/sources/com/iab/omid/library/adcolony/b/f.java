package com.iab.omid.library.adcolony.b;

import android.content.Context;
import android.os.Handler;
import com.iab.omid.library.adcolony.a.b;
import com.iab.omid.library.adcolony.a.c;
import com.iab.omid.library.adcolony.a.d;
import com.iab.omid.library.adcolony.a.e;
import com.iab.omid.library.adcolony.b.b.a;
import com.iab.omid.library.adcolony.walking.TreeWalker;

public class f implements c, a {
    private static f f;
    private float a = 0.0f;
    private final e b;
    private final b c;
    private d d;
    private a e;

    public f(e eVar, b bVar) {
        this.b = eVar;
        this.c = bVar;
    }

    public static f a() {
        if (f == null) {
            f = new f(new e(), new b());
        }
        return f;
    }

    private a e() {
        if (this.e == null) {
            this.e = a.a();
        }
        return this.e;
    }

    public void a(float f2) {
        this.a = f2;
        for (com.iab.omid.library.adcolony.adsession.a adSessionStatePublisher : e().c()) {
            adSessionStatePublisher.getAdSessionStatePublisher().a(f2);
        }
    }

    public void a(Context context) {
        this.d = this.b.a(new Handler(), context, this.c.a(), this);
    }

    public void a(boolean z) {
        if (z) {
            TreeWalker.getInstance().a();
        } else {
            TreeWalker.getInstance().c();
        }
    }

    public void b() {
        b.a().a((a) this);
        b.a().b();
        if (b.a().d()) {
            TreeWalker.getInstance().a();
        }
        this.d.a();
    }

    public void c() {
        TreeWalker.getInstance().b();
        b.a().c();
        this.d.b();
    }

    public float d() {
        return this.a;
    }
}
