package com.iab.omid.library.adcolony.b;

import android.annotation.SuppressLint;
import android.content.Context;

public class d {
    @SuppressLint({"StaticFieldLeak"})
    private static d b = new d();
    private Context a;

    private d() {
    }

    public static d a() {
        return b;
    }

    public void a(Context context) {
        this.a = context != null ? context.getApplicationContext() : null;
    }

    public Context b() {
        return this.a;
    }
}
