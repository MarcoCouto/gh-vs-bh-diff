package com.iab.omid.library.adcolony.b;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;

public class b {
    @SuppressLint({"StaticFieldLeak"})
    private static b f = new b();
    private Context a;
    private BroadcastReceiver b;
    private boolean c;
    private boolean d;
    private a e;

    public interface a {
        void a(boolean z);
    }

    /* renamed from: com.iab.omid.library.adcolony.b.b$b reason: collision with other inner class name */
    class C0031b extends BroadcastReceiver {
        C0031b() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x003d, code lost:
            if (r3.inKeyguardRestrictedInputMode() == false) goto L_0x003f;
         */
        public void onReceive(Context context, Intent intent) {
            b bVar;
            boolean z;
            if (intent != null) {
                if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                    bVar = b.this;
                    z = true;
                } else {
                    if (!"android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                        if ("android.intent.action.SCREEN_ON".equals(intent.getAction())) {
                            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
                            if (keyguardManager != null) {
                            }
                        }
                    }
                    bVar = b.this;
                    z = false;
                }
                bVar.a(z);
            }
        }
    }

    private b() {
    }

    public static b a() {
        return f;
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (this.d != z) {
            this.d = z;
            if (this.c) {
                g();
                a aVar = this.e;
                if (aVar != null) {
                    aVar.a(d());
                }
            }
        }
    }

    private void e() {
        this.b = new C0031b();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        this.a.registerReceiver(this.b, intentFilter);
    }

    private void f() {
        Context context = this.a;
        if (context != null) {
            BroadcastReceiver broadcastReceiver = this.b;
            if (broadcastReceiver != null) {
                context.unregisterReceiver(broadcastReceiver);
                this.b = null;
            }
        }
    }

    private void g() {
        boolean z = !this.d;
        for (com.iab.omid.library.adcolony.adsession.a adSessionStatePublisher : a.a().b()) {
            adSessionStatePublisher.getAdSessionStatePublisher().a(z);
        }
    }

    public void a(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    public void a(a aVar) {
        this.e = aVar;
    }

    public void b() {
        e();
        this.c = true;
        g();
    }

    public void c() {
        f();
        this.c = false;
        this.d = false;
        this.e = null;
    }

    public boolean d() {
        return !this.d;
    }
}
