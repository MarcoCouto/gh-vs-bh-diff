package com.iab.omid.library.adcolony.b;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class a {
    private static a c = new a();
    private final ArrayList<com.iab.omid.library.adcolony.adsession.a> a = new ArrayList<>();
    private final ArrayList<com.iab.omid.library.adcolony.adsession.a> b = new ArrayList<>();

    private a() {
    }

    public static a a() {
        return c;
    }

    public void a(com.iab.omid.library.adcolony.adsession.a aVar) {
        this.a.add(aVar);
    }

    public Collection<com.iab.omid.library.adcolony.adsession.a> b() {
        return Collections.unmodifiableCollection(this.a);
    }

    public void b(com.iab.omid.library.adcolony.adsession.a aVar) {
        boolean d = d();
        this.b.add(aVar);
        if (!d) {
            f.a().b();
        }
    }

    public Collection<com.iab.omid.library.adcolony.adsession.a> c() {
        return Collections.unmodifiableCollection(this.b);
    }

    public void c(com.iab.omid.library.adcolony.adsession.a aVar) {
        boolean d = d();
        this.a.remove(aVar);
        this.b.remove(aVar);
        if (d && !d()) {
            f.a().c();
        }
    }

    public boolean d() {
        return this.b.size() > 0;
    }
}
