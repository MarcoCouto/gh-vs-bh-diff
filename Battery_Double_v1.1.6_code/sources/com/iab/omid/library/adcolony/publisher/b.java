package com.iab.omid.library.adcolony.publisher;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.webkit.WebView;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.iab.omid.library.adcolony.adsession.AdSessionContext;
import com.iab.omid.library.adcolony.adsession.VerificationScriptResource;
import com.iab.omid.library.adcolony.b.e;
import com.iab.omid.library.adcolony.d.d;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */
    public WebView f;
    private Long g = null;
    private Map<String, VerificationScriptResource> h;
    private final String i;

    class a implements Runnable {
        private WebView a = b.this.f;

        a() {
        }

        public void run() {
            this.a.destroy();
        }
    }

    public b(Map<String, VerificationScriptResource> map, String str) {
        this.h = map;
        this.i = str;
    }

    public void a() {
        super.a();
        j();
    }

    public void a(com.iab.omid.library.adcolony.adsession.a aVar, AdSessionContext adSessionContext) {
        JSONObject jSONObject = new JSONObject();
        Map injectedResourcesMap = adSessionContext.getInjectedResourcesMap();
        for (String str : injectedResourcesMap.keySet()) {
            com.iab.omid.library.adcolony.d.b.a(jSONObject, str, (VerificationScriptResource) injectedResourcesMap.get(str));
        }
        a(aVar, adSessionContext, jSONObject);
    }

    public void b() {
        super.b();
        new Handler().postDelayed(new a(), Math.max(4000 - (this.g == null ? 4000 : TimeUnit.MILLISECONDS.convert(d.a() - this.g.longValue(), TimeUnit.NANOSECONDS)), AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS));
        this.f = null;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void j() {
        WebView webView = new WebView(com.iab.omid.library.adcolony.b.d.a().b());
        this.f = webView;
        webView.getSettings().setJavaScriptEnabled(true);
        a(this.f);
        e.a().a(this.f, this.i);
        for (String str : this.h.keySet()) {
            e.a().a(this.f, ((VerificationScriptResource) this.h.get(str)).getResourceUrl().toExternalForm(), str);
        }
        this.g = Long.valueOf(d.a());
    }
}
