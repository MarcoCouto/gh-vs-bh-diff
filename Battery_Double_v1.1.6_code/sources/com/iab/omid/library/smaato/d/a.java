package com.iab.omid.library.smaato.d;

import android.os.Build;
import android.os.Build.VERSION;
import com.ironsource.sdk.constants.Constants;
import org.json.JSONObject;

public final class a {
    public static String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER);
        sb.append("; ");
        sb.append(Build.MODEL);
        return sb.toString();
    }

    public static String b() {
        return Integer.toString(VERSION.SDK_INT);
    }

    public static String c() {
        return Constants.JAVASCRIPT_INTERFACE_NAME;
    }

    public static JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "deviceType", a());
        b.a(jSONObject, "osVersion", b());
        b.a(jSONObject, "os", c());
        return jSONObject;
    }
}
