package com.iab.omid.library.smaato.walking.a;

import com.iab.omid.library.smaato.walking.a.b.C0041b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class a extends b {
    protected final HashSet<String> a;
    protected final JSONObject b;
    protected final double c;

    public a(C0041b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar);
        this.a = new HashSet<>(hashSet);
        this.b = jSONObject;
        this.c = d;
    }
}
