package com.iab.omid.library.smaato.walking;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.iab.omid.library.smaato.c.a;
import com.iab.omid.library.smaato.c.a.C0040a;
import com.iab.omid.library.smaato.c.b;
import com.iab.omid.library.smaato.d.d;
import com.iab.omid.library.smaato.d.f;
import com.iab.omid.library.smaato.walking.a.c;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class TreeWalker implements C0040a {
    private static TreeWalker a = new TreeWalker();
    private static Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static Handler c = null;
    /* access modifiers changed from: private */
    public static final Runnable j = new Runnable() {
        public final void run() {
            TreeWalker.getInstance().h();
        }
    };
    /* access modifiers changed from: private */
    public static final Runnable k = new Runnable() {
        public final void run() {
            if (TreeWalker.c != null) {
                TreeWalker.c.post(TreeWalker.j);
                TreeWalker.c.postDelayed(TreeWalker.k, 200);
            }
        }
    };
    private List<TreeWalkerTimeLogger> d = new ArrayList();
    private int e;
    private b f = new b();
    private a g = new a();
    /* access modifiers changed from: private */
    public b h = new b(new c());
    private double i;

    public interface TreeWalkerTimeLogger {
        void onTreeProcessed(int i, long j);
    }

    TreeWalker() {
    }

    private void a(long j2) {
        if (this.d.size() > 0) {
            for (TreeWalkerTimeLogger onTreeProcessed : this.d) {
                onTreeProcessed.onTreeProcessed(this.e, j2);
            }
        }
    }

    private void a(View view, a aVar, JSONObject jSONObject, c cVar) {
        aVar.a(view, jSONObject, this, cVar == c.PARENT_VIEW);
    }

    private boolean a(View view, JSONObject jSONObject) {
        String a2 = this.g.a(view);
        if (a2 == null) {
            return false;
        }
        com.iab.omid.library.smaato.d.b.a(jSONObject, a2);
        this.g.e();
        return true;
    }

    private void b(View view, JSONObject jSONObject) {
        ArrayList b2 = this.g.b(view);
        if (b2 != null) {
            com.iab.omid.library.smaato.d.b.a(jSONObject, (List<String>) b2);
        }
    }

    public static TreeWalker getInstance() {
        return a;
    }

    /* access modifiers changed from: private */
    public void h() {
        i();
        d();
        j();
    }

    private void i() {
        this.e = 0;
        this.i = d.a();
    }

    private void j() {
        a((long) (d.a() - this.i));
    }

    private void k() {
        if (c == null) {
            Handler handler = new Handler(Looper.getMainLooper());
            c = handler;
            handler.post(j);
            c.postDelayed(k, 200);
        }
    }

    private void l() {
        if (c != null) {
            c.removeCallbacks(k);
            c = null;
        }
    }

    public void a() {
        k();
    }

    public void a(View view, a aVar, JSONObject jSONObject) {
        if (f.d(view)) {
            c c2 = this.g.c(view);
            if (c2 != c.UNDERLYING_VIEW) {
                JSONObject a2 = aVar.a(view);
                com.iab.omid.library.smaato.d.b.a(jSONObject, a2);
                if (!a(view, a2)) {
                    b(view, a2);
                    a(view, aVar, a2, c2);
                }
                this.e++;
            }
        }
    }

    public void addTimeLogger(TreeWalkerTimeLogger treeWalkerTimeLogger) {
        if (!this.d.contains(treeWalkerTimeLogger)) {
            this.d.add(treeWalkerTimeLogger);
        }
    }

    public void b() {
        c();
        this.d.clear();
        b.post(new Runnable() {
            public void run() {
                TreeWalker.this.h.a();
            }
        });
    }

    public void c() {
        l();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void d() {
        this.g.c();
        double a2 = d.a();
        a a3 = this.f.a();
        if (this.g.b().size() > 0) {
            this.h.b(a3.a(null), this.g.b(), a2);
        }
        if (this.g.a().size() > 0) {
            JSONObject a4 = a3.a(null);
            a(null, a3, a4, c.PARENT_VIEW);
            com.iab.omid.library.smaato.d.b.a(a4);
            this.h.a(a4, this.g.a(), a2);
        } else {
            this.h.a();
        }
        this.g.d();
    }

    public void removeTimeLogger(TreeWalkerTimeLogger treeWalkerTimeLogger) {
        if (this.d.contains(treeWalkerTimeLogger)) {
            this.d.remove(treeWalkerTimeLogger);
        }
    }
}
