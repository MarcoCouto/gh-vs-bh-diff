package com.iab.omid.library.smaato;

import android.content.Context;
import com.iab.omid.library.smaato.b.b;
import com.iab.omid.library.smaato.b.c;
import com.iab.omid.library.smaato.d.e;

public class a {
    private boolean a;

    private void b(Context context) {
        e.a((Object) context, "Application Context cannot be null");
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return "1.2.13-Smaato";
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context) {
        b(context);
        if (!b()) {
            a(true);
            com.iab.omid.library.smaato.b.e.a().a(context);
            b.a().a(context);
            com.iab.omid.library.smaato.d.b.a(context);
            c.a().a(context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.a = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(String str) {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.a;
    }
}
