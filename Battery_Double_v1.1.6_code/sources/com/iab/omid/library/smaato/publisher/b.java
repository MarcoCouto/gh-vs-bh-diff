package com.iab.omid.library.smaato.publisher;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.webkit.WebView;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.iab.omid.library.smaato.adsession.VerificationScriptResource;
import com.iab.omid.library.smaato.b.c;
import com.iab.omid.library.smaato.b.d;
import java.util.List;

public class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */
    public WebView a;
    private List<VerificationScriptResource> b;
    private final String c;

    public b(List<VerificationScriptResource> list, String str) {
        this.b = list;
        this.c = str;
    }

    public void a() {
        super.a();
        i();
    }

    public void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {
            private WebView b = b.this.a;

            public void run() {
                this.b.destroy();
            }
        }, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
        this.a = null;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void i() {
        this.a = new WebView(c.a().b());
        this.a.getSettings().setJavaScriptEnabled(true);
        a(this.a);
        d.a().a(this.a, this.c);
        for (VerificationScriptResource resourceUrl : this.b) {
            d.a().b(this.a, resourceUrl.getResourceUrl().toExternalForm());
        }
    }
}
