package com.iab.omid.library.ironsrc;

import android.content.Context;
import com.iab.omid.library.ironsrc.b.b;
import com.iab.omid.library.ironsrc.b.c;
import com.iab.omid.library.ironsrc.d.e;

public class a {
    private boolean a;

    private void b(Context context) {
        e.a((Object) context, "Application Context cannot be null");
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return "1.2.22-Ironsrc";
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context) {
        b(context);
        if (!b()) {
            a(true);
            com.iab.omid.library.ironsrc.b.e.a().a(context);
            b.a().a(context);
            com.iab.omid.library.ironsrc.d.b.a(context);
            c.a().a(context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.a = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(String str) {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.a;
    }
}
