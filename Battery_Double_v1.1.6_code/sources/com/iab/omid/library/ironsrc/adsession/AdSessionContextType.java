package com.iab.omid.library.ironsrc.adsession;

import com.tapjoy.TJAdUnitConstants.String;

public enum AdSessionContextType {
    HTML(String.HTML),
    NATIVE("native");
    
    private final String typeString;

    private AdSessionContextType(String str) {
        this.typeString = str;
    }

    public String toString() {
        return this.typeString;
    }
}
