package com.iab.omid.library.ironsrc.adsession.video;

import com.tapjoy.TJAdUnitConstants.String;

public enum InteractionType {
    CLICK(String.CLICK),
    INVITATION_ACCEPTED("invitationAccept");
    
    String interactionType;

    private InteractionType(String str) {
        this.interactionType = str;
    }

    public String toString() {
        return this.interactionType;
    }
}
