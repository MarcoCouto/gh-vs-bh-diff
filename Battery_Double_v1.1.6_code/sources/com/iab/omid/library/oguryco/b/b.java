package com.iab.omid.library.oguryco.b;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;

public class b {
    @SuppressLint({"StaticFieldLeak"})
    private static b a = new b();
    private Context b;
    private BroadcastReceiver c;
    private boolean d;
    private boolean e;
    private a f;

    public interface a {
        void a(boolean z);
    }

    private b() {
    }

    public static b a() {
        return a;
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (this.e != z) {
            this.e = z;
            if (this.d) {
                g();
                if (this.f != null) {
                    this.f.a(d());
                }
            }
        }
    }

    private void e() {
        this.c = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                        b.this.a(true);
                    } else if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                        b.this.a(false);
                    } else {
                        if ("android.intent.action.SCREEN_ON".equals(intent.getAction())) {
                            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
                            if (keyguardManager != null && !keyguardManager.inKeyguardRestrictedInputMode()) {
                                b.this.a(false);
                            }
                        }
                    }
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        this.b.registerReceiver(this.c, intentFilter);
    }

    private void f() {
        if (this.b != null && this.c != null) {
            this.b.unregisterReceiver(this.c);
            this.c = null;
        }
    }

    private void g() {
        boolean z = !this.e;
        for (com.iab.omid.library.oguryco.adsession.a adSessionStatePublisher : a.a().b()) {
            adSessionStatePublisher.getAdSessionStatePublisher().a(z);
        }
    }

    public void a(@NonNull Context context) {
        this.b = context.getApplicationContext();
    }

    public void a(a aVar) {
        this.f = aVar;
    }

    public void b() {
        e();
        this.d = true;
        g();
    }

    public void c() {
        f();
        this.d = false;
        this.e = false;
        this.f = null;
    }

    public boolean d() {
        return !this.e;
    }
}
