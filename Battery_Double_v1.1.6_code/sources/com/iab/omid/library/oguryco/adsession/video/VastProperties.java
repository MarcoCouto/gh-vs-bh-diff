package com.iab.omid.library.oguryco.adsession.video;

import com.iab.omid.library.oguryco.adsession.media.Position;
import com.iab.omid.library.oguryco.d.e;

public final class VastProperties {
    private final boolean a;
    private final Float b;
    private final boolean c;
    private final Position d;
    private com.iab.omid.library.oguryco.adsession.media.VastProperties e;

    private VastProperties(boolean z, Float f, boolean z2, Position position, com.iab.omid.library.oguryco.adsession.media.VastProperties vastProperties) {
        this.a = z;
        this.b = f;
        this.c = z2;
        this.d = position;
        this.e = vastProperties;
    }

    public static VastProperties createVastPropertiesForNonSkippableVideo(boolean z, Position position) {
        e.a((Object) position, "Position is null");
        VastProperties vastProperties = new VastProperties(false, null, z, position, com.iab.omid.library.oguryco.adsession.media.VastProperties.createVastPropertiesForNonSkippableMedia(z, Position.valueOf(position.toString().toUpperCase())));
        return vastProperties;
    }

    public static VastProperties createVastPropertiesForSkippableVideo(float f, boolean z, Position position) {
        e.a((Object) position, "Position is null");
        VastProperties vastProperties = new VastProperties(true, Float.valueOf(f), z, position, com.iab.omid.library.oguryco.adsession.media.VastProperties.createVastPropertiesForSkippableMedia(f, z, Position.valueOf(position.toString().toUpperCase())));
        return vastProperties;
    }

    /* access modifiers changed from: 0000 */
    public final com.iab.omid.library.oguryco.adsession.media.VastProperties a() {
        return this.e;
    }

    public final Position getPosition() {
        return this.d;
    }

    public final Float getSkipOffset() {
        return this.b;
    }

    public final boolean isAutoPlay() {
        return this.c;
    }

    public final boolean isSkippable() {
        return this.a;
    }
}
