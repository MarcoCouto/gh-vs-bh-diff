package com.iab.omid.library.oguryco.adsession.media;

import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;

public enum Position {
    PREROLL(BreakId.PREROLL),
    MIDROLL(BreakId.MIDROLL),
    POSTROLL(BreakId.POSTROLL),
    STANDALONE("standalone");
    
    private final String position;

    private Position(String str) {
        this.position = str;
    }

    public final String toString() {
        return this.position;
    }
}
