package com.iab.omid.library.oguryco.adsession;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.ironsource.sdk.constants.Constants.ParametersKeys;

public enum ImpressionType {
    DEFINED_BY_JAVASCRIPT("definedByJavaScript"),
    UNSPECIFIED("unspecified"),
    LOADED(ParametersKeys.LOADED),
    BEGIN_TO_RENDER("beginToRender"),
    ONE_PIXEL("onePixel"),
    VIEWABLE("viewable"),
    AUDIBLE("audible"),
    OTHER(FacebookRequestErrorClassification.KEY_OTHER);
    
    private final String impressionType;

    private ImpressionType(String str) {
        this.impressionType = str;
    }

    public final String toString() {
        return this.impressionType;
    }
}
