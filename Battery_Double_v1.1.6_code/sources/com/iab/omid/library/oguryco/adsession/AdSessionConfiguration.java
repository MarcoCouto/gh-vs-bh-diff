package com.iab.omid.library.oguryco.adsession;

import com.iab.omid.library.oguryco.d.b;
import com.iab.omid.library.oguryco.d.e;
import org.json.JSONObject;

public class AdSessionConfiguration {
    private final CreativeType creativeType;
    private final Owner impressionOwner;
    private final ImpressionType impressionType;
    private final boolean isolateVerificationScripts;
    private final Owner mediaEventsOwner;

    private AdSessionConfiguration(CreativeType creativeType2, ImpressionType impressionType2, Owner owner, Owner owner2, boolean z) {
        this.creativeType = creativeType2;
        this.impressionType = impressionType2;
        this.impressionOwner = owner;
        if (owner2 == null) {
            this.mediaEventsOwner = Owner.NONE;
        } else {
            this.mediaEventsOwner = owner2;
        }
        this.isolateVerificationScripts = z;
    }

    public static AdSessionConfiguration createAdSessionConfiguration(CreativeType creativeType2, ImpressionType impressionType2, Owner owner, Owner owner2, boolean z) {
        e.a((Object) creativeType2, "CreativeType is null");
        e.a((Object) impressionType2, "ImpressionType is null");
        e.a((Object) owner, "Impression owner is null");
        e.a(owner, creativeType2, impressionType2);
        AdSessionConfiguration adSessionConfiguration = new AdSessionConfiguration(creativeType2, impressionType2, owner, owner2, z);
        return adSessionConfiguration;
    }

    @Deprecated
    public static AdSessionConfiguration createAdSessionConfiguration(Owner owner, Owner owner2) {
        return createAdSessionConfiguration(owner, owner2, true);
    }

    public static AdSessionConfiguration createAdSessionConfiguration(Owner owner, Owner owner2, boolean z) {
        e.a((Object) owner, "Impression owner is null");
        e.a(owner, (CreativeType) null, (ImpressionType) null);
        AdSessionConfiguration adSessionConfiguration = new AdSessionConfiguration(null, null, owner, owner2, z);
        return adSessionConfiguration;
    }

    public boolean isNativeImpressionOwner() {
        return Owner.NATIVE == this.impressionOwner;
    }

    public boolean isNativeMediaEventsOwner() {
        return Owner.NATIVE == this.mediaEventsOwner;
    }

    public boolean isNativeVideoEventsOwner() {
        return isNativeMediaEventsOwner();
    }

    public JSONObject toJsonObject() {
        Object obj;
        String str;
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "impressionOwner", this.impressionOwner);
        if (this.creativeType == null || this.impressionType == null) {
            str = "videoEventsOwner";
            obj = this.mediaEventsOwner;
        } else {
            b.a(jSONObject, "mediaEventsOwner", this.mediaEventsOwner);
            b.a(jSONObject, "creativeType", this.creativeType);
            str = "impressionType";
            obj = this.impressionType;
        }
        b.a(jSONObject, str, obj);
        b.a(jSONObject, "isolateVerificationScripts", Boolean.valueOf(this.isolateVerificationScripts));
        return jSONObject;
    }
}
