package com.iab.omid.library.oguryco;

import android.content.Context;
import com.iab.omid.library.oguryco.b.d;
import com.iab.omid.library.oguryco.b.f;
import com.iab.omid.library.oguryco.d.e;

public class b {
    private boolean a;

    private void b(Context context) {
        e.a((Object) context, "Application Context cannot be null");
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return "1.3.1-Oguryco";
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context) {
        b(context);
        if (!b()) {
            a(true);
            f.a().a(context);
            com.iab.omid.library.oguryco.b.b.a().a(context);
            com.iab.omid.library.oguryco.d.b.a(context);
            d.a().a(context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.a = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(String str) {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.a;
    }
}
