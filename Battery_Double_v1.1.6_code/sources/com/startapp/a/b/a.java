package com.startapp.a.b;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* compiled from: StartAppSDK */
public final class a {
    private final Context a;

    public a(Context context) {
        this.a = context;
    }

    public final boolean a() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(b.a));
        return a((List<String>) arrayList);
    }

    public final boolean b() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(b.b));
        return a((List<String>) arrayList);
    }

    public static boolean a(String str) {
        String[] strArr = b.c;
        boolean z = false;
        for (int i = 0; i < 14; i++) {
            String str2 = strArr[i];
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(str);
            if (new File(str2, str).exists()) {
                z = true;
            }
        }
        return z;
    }

    private static String[] f() {
        String[] strArr = new String[0];
        try {
            return new Scanner(Runtime.getRuntime().exec("getprop").getInputStream()).useDelimiter("\\A").next().split("\n");
        } catch (IOException | NoSuchElementException e) {
            e.printStackTrace();
            return strArr;
        }
    }

    private static String[] g() {
        String[] strArr = new String[0];
        try {
            return new Scanner(Runtime.getRuntime().exec("mount").getInputStream()).useDelimiter("\\A").next().split("\n");
        } catch (IOException | NoSuchElementException e) {
            e.printStackTrace();
            return strArr;
        }
    }

    private boolean a(List<String> list) {
        PackageManager packageManager = this.a.getPackageManager();
        boolean z = false;
        for (String packageInfo : list) {
            try {
                packageManager.getPackageInfo(packageInfo, 0);
                z = true;
            } catch (NameNotFoundException unused) {
            }
        }
        return z;
    }

    public static boolean c() {
        String[] f;
        HashMap hashMap = new HashMap();
        hashMap.put("ro.debuggable", "1");
        hashMap.put("ro.secure", "0");
        boolean z = false;
        for (String str : f()) {
            for (String str2 : hashMap.keySet()) {
                if (str.contains(str2)) {
                    String str3 = (String) hashMap.get(str2);
                    StringBuilder sb = new StringBuilder(RequestParameters.LEFT_BRACKETS);
                    sb.append(str3);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    if (str.contains(sb.toString())) {
                        z = true;
                    }
                }
            }
        }
        return z;
    }

    public static boolean d() {
        boolean z = false;
        for (String split : g()) {
            String[] split2 = split.split(" ");
            if (split2.length >= 4) {
                String str = split2[1];
                String str2 = split2[3];
                String[] strArr = b.d;
                boolean z2 = z;
                for (int i = 0; i < 7; i++) {
                    if (str.equalsIgnoreCase(strArr[i])) {
                        String[] split3 = str2.split(",");
                        int length = split3.length;
                        int i2 = 0;
                        while (true) {
                            if (i2 >= length) {
                                break;
                            } else if (split3[i2].equalsIgnoreCase("rw")) {
                                z2 = true;
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
                z = z2;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003f  */
    public static boolean e() {
        boolean z = false;
        Process process = null;
        try {
            Process exec = Runtime.getRuntime().exec(new String[]{"which", "su"});
            try {
                if (new BufferedReader(new InputStreamReader(exec.getInputStream())).readLine() != null) {
                    z = true;
                }
                if (exec != null) {
                    exec.destroy();
                }
                return z;
            } catch (Throwable th) {
                th = th;
                process = exec;
                if (process != null) {
                    process.destroy();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            if (process != null) {
            }
            throw th;
        }
    }
}
