package com.startapp.networkTest.controller;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.startapp.networkTest.c;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.enums.LocationProviders;
import java.util.List;

/* compiled from: StartAppSDK */
public class LocationController {
    private static final String a = "LocationController";
    private LocationManager b;
    /* access modifiers changed from: private */
    public long c;
    /* access modifiers changed from: private */
    public LocationInfo d;
    /* access modifiers changed from: private */
    public Location e;
    /* access modifiers changed from: private */
    public long f;
    private a g;
    private long h = 4000;
    private boolean i;
    /* access modifiers changed from: private */
    public b j;

    /* compiled from: StartAppSDK */
    public enum ProviderMode {
        Passive,
        Network,
        Gps,
        GpsAndNetwork,
        RailNet
    }

    /* compiled from: StartAppSDK */
    class a implements LocationListener {
        public final void onProviderDisabled(String str) {
        }

        public final void onProviderEnabled(String str) {
        }

        public final void onStatusChanged(String str, int i, Bundle bundle) {
        }

        private a() {
        }

        /* synthetic */ a(LocationController locationController, byte b) {
            this();
        }

        public final void onLocationChanged(Location location) {
            if (location != null && location.getProvider() != null) {
                if (LocationController.this.e == null || location.getProvider().equals("gps") || LocationController.this.e.getProvider() == null || !LocationController.this.e.getProvider().equals("gps") || SystemClock.elapsedRealtime() - LocationController.this.c >= DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS) {
                    LocationController.this.e = location;
                    LocationController.this.f = SystemClock.elapsedRealtime();
                    LocationController.this.d = LocationController.b(location);
                    LocationController.this.d.LocationAge = 0;
                    LocationController.this.c = SystemClock.elapsedRealtime();
                    if (LocationController.this.j != null) {
                        LocationController.this.j.a(LocationController.this.d);
                    }
                    if (location.getProvider().equals("gps")) {
                        c.b().a(location);
                    }
                }
            }
        }
    }

    /* compiled from: StartAppSDK */
    public interface b {
        void a(LocationInfo locationInfo);
    }

    public LocationController(Context context) {
        this.b = (LocationManager) context.getSystemService("location");
        this.g = new a(this, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x001b A[SYNTHETIC] */
    public final void a(ProviderMode providerMode) {
        boolean z;
        boolean z2;
        char c2;
        if (providerMode != null && this.b != null) {
            this.i = true;
            List<String> allProviders = this.b.getAllProviders();
            boolean z3 = false;
            if (allProviders != null) {
                boolean z4 = false;
                z2 = false;
                z = false;
                for (String str : allProviders) {
                    int hashCode = str.hashCode();
                    if (hashCode == -792039641) {
                        if (str.equals("passive")) {
                            c2 = 2;
                            switch (c2) {
                                case 0:
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    break;
                            }
                        }
                    } else if (hashCode == 102570) {
                        if (str.equals("gps")) {
                            c2 = 0;
                            switch (c2) {
                                case 0:
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    break;
                            }
                        }
                    } else if (hashCode == 1843485230 && str.equals("network")) {
                        c2 = 1;
                        switch (c2) {
                            case 0:
                                z4 = true;
                                break;
                            case 1:
                                z2 = true;
                                break;
                            case 2:
                                z = true;
                                break;
                        }
                    }
                    c2 = 65535;
                    switch (c2) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                    }
                }
                z3 = z4;
            } else {
                z2 = false;
                z = false;
            }
            try {
                switch (providerMode) {
                    case Gps:
                        if (z3) {
                            this.b.requestLocationUpdates("gps", 500, 5.0f, this.g);
                            return;
                        }
                        break;
                    case GpsAndNetwork:
                        if (z3) {
                            this.b.requestLocationUpdates("gps", 500, 5.0f, this.g);
                        }
                        if (z2) {
                            this.b.requestLocationUpdates("network", 0, 0.0f, this.g);
                            return;
                        }
                        break;
                    case Network:
                        if (z2) {
                            this.b.requestLocationUpdates("network", 0, 0.0f, this.g);
                            return;
                        }
                        break;
                    case Passive:
                        if (z) {
                            this.b.requestLocationUpdates("passive", 0, 0.0f, this.g);
                            break;
                        }
                        break;
                }
            } catch (Exception e2) {
                new StringBuilder("startListening: ").append(e2.toString());
            }
        }
    }

    public final void a() {
        if (!(this.b == null || this.g == null)) {
            try {
                this.b.removeUpdates(this.g);
            } catch (Exception e2) {
                new StringBuilder("stopListening: ").append(e2.toString());
            }
        }
        this.i = false;
    }

    public final LocationInfo b() {
        Location location;
        if (this.d == null) {
            List allProviders = this.b.getAllProviders();
            Location location2 = null;
            if (allProviders != null && allProviders.size() > 0) {
                Location location3 = null;
                for (int i2 = 0; i2 < allProviders.size(); i2++) {
                    try {
                        location = this.b.getLastKnownLocation((String) allProviders.get(i2));
                    } catch (SecurityException e2) {
                        new StringBuilder("getNewestCachedLocationFromDevice: ").append(e2.toString());
                        location = null;
                    }
                    if (location != null && (location3 == null || location.getTime() > location3.getTime())) {
                        location3 = location;
                    }
                }
                location2 = location3;
            }
            if (location2 != null) {
                this.e = location2;
                if (VERSION.SDK_INT >= 17) {
                    this.f = location2.getElapsedRealtimeNanos() / 1000000;
                } else {
                    this.f = SystemClock.elapsedRealtime() + (System.currentTimeMillis() - location2.getTime());
                }
                this.d = b(location2);
            }
        }
        if (this.d == null) {
            this.d = new LocationInfo();
            this.d.LocationProvider = LocationProviders.Unknown;
        }
        if (this.d.LocationProvider != LocationProviders.Unknown) {
            this.d.LocationAge = SystemClock.elapsedRealtime() - this.f;
        }
        try {
            return (LocationInfo) this.d.clone();
        } catch (CloneNotSupportedException e3) {
            Log.e(a, "getLastLocationInfo", e3);
            return this.d;
        }
    }

    /* access modifiers changed from: private */
    public static LocationInfo b(Location location) {
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.LocationAccuracyHorizontal = (double) location.getAccuracy();
        if (VERSION.SDK_INT < 26 || !location.hasVerticalAccuracy()) {
            locationInfo.LocationAccuracyVertical = (double) location.getAccuracy();
        } else {
            locationInfo.LocationAccuracyVertical = (double) location.getVerticalAccuracyMeters();
        }
        locationInfo.locationTimestampMillis = com.startapp.networkTest.e.b.b();
        locationInfo.LocationTimestamp = com.iab.omid.library.startapp.b.a(locationInfo.locationTimestampMillis);
        locationInfo.LocationAltitude = location.getAltitude();
        locationInfo.LocationBearing = (double) location.getBearing();
        locationInfo.LocationLatitude = location.getLatitude();
        locationInfo.LocationLongitude = location.getLongitude();
        if (location.getProvider() != null) {
            if (location.getProvider().equals("gps")) {
                locationInfo.LocationProvider = LocationProviders.Gps;
            } else if (location.getProvider().equals("network")) {
                locationInfo.LocationProvider = LocationProviders.Network;
            } else if (location.getProvider().equals("fused")) {
                locationInfo.LocationProvider = LocationProviders.Fused;
            }
            locationInfo.LocationSpeed = (double) location.getSpeed();
            return locationInfo;
        }
        locationInfo.LocationProvider = LocationProviders.Unknown;
        locationInfo.LocationSpeed = (double) location.getSpeed();
        return locationInfo;
    }

    public final void a(b bVar) {
        this.j = bVar;
    }
}
