package com.startapp.networkTest.controller;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.support.v4.view.InputDeviceCompat;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityNr;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrength;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthNr;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionManager.OnSubscriptionsChangedListener;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import com.facebook.places.model.PlaceFields;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.data.radio.ApnInfo;
import com.startapp.networkTest.data.radio.NetworkRegistrationInfo;
import com.startapp.networkTest.enums.CellConnectionStatus;
import com.startapp.networkTest.enums.CellNetworkTypes;
import com.startapp.networkTest.enums.ConnectionTypes;
import com.startapp.networkTest.enums.DuplexMode;
import com.startapp.networkTest.enums.FlightModeStates;
import com.startapp.networkTest.enums.NetworkGenerations;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.PreferredNetworkTypes;
import com.startapp.networkTest.enums.ServiceStates;
import com.startapp.networkTest.enums.ThreeState;
import com.startapp.networkTest.enums.ThreeStateShort;
import com.startapp.networkTest.enums.radio.DataConnectionStates;
import com.startapp.networkTest.enums.wifi.WifiDetailedStates;
import com.startapp.sdk.adsbase.cache.DiskAdCacheManager;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: StartAppSDK */
public class c {
    /* access modifiers changed from: private */
    public static final String c = "c";
    /* access modifiers changed from: private */
    public Field A;
    /* access modifiers changed from: private */
    public Field B;
    /* access modifiers changed from: private */
    public Method C;
    /* access modifiers changed from: private */
    public Method D;
    /* access modifiers changed from: private */
    public Method E;
    /* access modifiers changed from: private */
    public Field F;
    private Field G;
    private Field H;
    private Field I;
    private Field J;
    private Method K;
    private Method L;
    private Method M;
    private Method N;
    private Method O;
    private Method P;
    private Method Q;
    private ContentResolver R = this.e.getContentResolver();
    /* access modifiers changed from: private */
    public int[] S;
    /* access modifiers changed from: private */
    public boolean T;
    protected final Handler a = new Handler(Looper.getMainLooper());
    protected final List<com.startapp.networkTest.controller.a.a> b = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */
    public TelephonyManager d;
    /* access modifiers changed from: private */
    public Context e;
    private j f;
    private ArrayList<j> g;
    private ConnectivityManager h;
    /* access modifiers changed from: private */
    public d i = new d();
    private OnSubscriptionsChangedListener j;
    private com.startapp.networkTest.data.a.a k = new com.startapp.networkTest.data.a.a();
    private e l;
    /* access modifiers changed from: private */
    public List<CellInfo> m;
    /* access modifiers changed from: private */
    public Method n;
    /* access modifiers changed from: private */
    public Method o;
    /* access modifiers changed from: private */
    public Method p;
    /* access modifiers changed from: private */
    public Method q;
    /* access modifiers changed from: private */
    public Method r;
    /* access modifiers changed from: private */
    public Method s;
    /* access modifiers changed from: private */
    public Method t;
    /* access modifiers changed from: private */
    public Method u;
    private Method v;
    private Method w;
    /* access modifiers changed from: private */
    public Field x;
    private Field y;
    /* access modifiers changed from: private */
    public Field z;

    /* compiled from: StartAppSDK */
    class a {
        int a;
        String b;
        String c;
        String d;
        String e;
        int f;
        long g;
        long h;
        String i;
        WifiDetailedStates j;
        private String k;

        private a() {
            this.a = -1;
            this.b = "";
            this.k = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = -1;
            this.g = -1;
            this.h = -1;
            this.i = "";
            this.j = WifiDetailedStates.Unknown;
        }

        /* synthetic */ a(c cVar, byte b2) {
            this();
        }
    }

    /* compiled from: StartAppSDK */
    class b {
        CellLocation a;
        long b;

        private b() {
            this.b = 0;
        }

        /* synthetic */ b(c cVar, byte b2) {
            this();
        }
    }

    /* renamed from: com.startapp.networkTest.controller.c$c reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    class C0067c extends AsyncTask<Void, Void, Void> {
        C0067c() {
        }

        /* access modifiers changed from: protected */
        public final void onPreExecute() {
            c.this.T = true;
            c.this.b(false);
            c.this.S = new int[0];
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            c.this.a(c.this.S);
            c.this.a(false);
            c.this.T = false;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            c.this.j();
            return null;
        }
    }

    /* compiled from: StartAppSDK */
    class d {
        private SparseArray<i> a = new SparseArray<>();
        private SparseArray<h> b = new SparseArray<>();
        private SparseArray<b> c = new SparseArray<>();
        private HashMap<String, f> d = new HashMap<>();
        private SparseArray<NetworkRegistrationInfo[]> e = new SparseArray<>();
        private SparseArray<g> f = new SparseArray<>();
        private Map<String, String> g = new HashMap();

        d() {
        }

        /* access modifiers changed from: 0000 */
        public final void a(int i, i iVar) {
            this.a.put(i, iVar);
        }

        /* access modifiers changed from: 0000 */
        public final void a(int i, h hVar) {
            this.b.put(i, hVar);
        }

        /* access modifiers changed from: 0000 */
        public final void a(int i, b bVar) {
            this.c.put(i, bVar);
        }

        /* access modifiers changed from: 0000 */
        public final void a(int i, NetworkRegistrationInfo[] networkRegistrationInfoArr) {
            this.e.put(i, networkRegistrationInfoArr);
        }

        /* access modifiers changed from: 0000 */
        public final void a(String str, f fVar) {
            this.d.put(str, fVar);
        }

        /* access modifiers changed from: 0000 */
        public final void a(int i, String str, String str2) {
            Map<String, String> map = this.g;
            StringBuilder sb = new StringBuilder();
            sb.append(i);
            sb.append(str);
            map.put(sb.toString(), str2);
        }

        /* access modifiers changed from: 0000 */
        public final void a(int i, g gVar) {
            this.f.put(i, gVar);
        }

        /* access modifiers changed from: 0000 */
        public final i a(int i) {
            i iVar = (i) this.a.get(i);
            return iVar == null ? new i(c.this, 0) : iVar;
        }

        /* access modifiers changed from: 0000 */
        public final h b(int i) {
            h hVar = (h) this.b.get(i);
            return hVar == null ? new h(c.this, 0) : hVar;
        }

        /* access modifiers changed from: 0000 */
        public final b c(int i) {
            return (b) this.c.get(i);
        }

        /* access modifiers changed from: 0000 */
        public final f a(String str) {
            return (f) this.d.get(str);
        }

        /* access modifiers changed from: 0000 */
        public final NetworkRegistrationInfo[] d(int i) {
            return (NetworkRegistrationInfo[]) this.e.get(i);
        }

        /* access modifiers changed from: 0000 */
        public final String a(int i, String str) {
            Map<String, String> map = this.g;
            StringBuilder sb = new StringBuilder();
            sb.append(i);
            sb.append(str != null ? str.split(",")[0] : "");
            String str2 = (String) map.get(sb.toString());
            return str2 == null ? "" : str2;
        }

        /* access modifiers changed from: 0000 */
        public final g e(int i) {
            return (g) this.f.get(i);
        }
    }

    /* compiled from: StartAppSDK */
    class e extends BroadcastReceiver {
        private String a;
        private String b;

        private e() {
            this.a = "android.intent.action.ANY_DATA_STATE";
            this.b = "com.samsung.ims.action.IMS_REGISTRATION";
        }

        /* synthetic */ e(c cVar, byte b2) {
            this();
        }

        public final void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                try {
                    String action = intent.getAction();
                    Bundle extras = intent.getExtras();
                    int i = -1;
                    if (!action.equalsIgnoreCase("android.intent.action.ANY_DATA_STATE") || extras == null) {
                        if (action.equalsIgnoreCase("com.samsung.ims.action.IMS_REGISTRATION") && extras != null) {
                            String string = extras.getString("SERVICE");
                            int i2 = extras.getInt("PHONE_ID", -1);
                            int i3 = extras.getInt("SIP_ERROR", -1);
                            extras.getBoolean("VOWIFI", false);
                            extras.getBoolean("REGISTERED", false);
                            g gVar = new g(c.this, 0);
                            gVar.a = i3;
                            if (string != null) {
                                gVar.b = string.replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                            }
                            Iterator it = b.g(c.this.e).SimInfos.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                com.startapp.networkTest.data.a.b bVar = (com.startapp.networkTest.data.a.b) it.next();
                                if (bVar.SimSlotIndex == i2) {
                                    i = bVar.SubscriptionId;
                                    break;
                                }
                            }
                            c.this.i.a(i, gVar);
                        }
                        return;
                    }
                    String string2 = extras.getString(IronSourceConstants.EVENTS_ERROR_REASON, "");
                    String string3 = extras.getString("apnType", "");
                    int i4 = extras.getInt("subscription", -1);
                    if (string3.equalsIgnoreCase("default")) {
                        string3 = "supl";
                    }
                    c.this.i.a(i4, string3, string2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        private static String a(String str, String str2) throws Exception {
            String substring = str.substring(str.indexOf(str2));
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(" ");
            String replaceAll = substring.replaceAll(sb.toString(), "");
            return replaceAll.substring(0, replaceAll.contains(" ") ? replaceAll.indexOf(" ") : replaceAll.length() - 1);
        }

        @TargetApi(21)
        static a a(NetworkCapabilities networkCapabilities, LinkProperties linkProperties, a aVar) throws Exception {
            if (networkCapabilities != null) {
                aVar.d = a(networkCapabilities.toString(), "Capabilities:").replaceAll(RequestParameters.AMPERSAND, ",").toLowerCase();
                try {
                    aVar.f = Integer.parseInt(a(networkCapabilities.toString(), "Specifier:").replaceAll("<", "").replaceAll(">", ""));
                } catch (Exception unused) {
                }
            }
            if (linkProperties != null) {
                try {
                    String a2 = a(linkProperties.toString().replaceAll("\\[ ", "\\[").replaceAll(" \\]", "\\]"), "PcscfAddresses:");
                    if (a2 != null && !a2.isEmpty()) {
                        String replace = a2.replace(RequestParameters.LEFT_BRACKETS, "").replace(RequestParameters.RIGHT_BRACKETS, "");
                        if (replace.lastIndexOf(",") == replace.length() - 1) {
                            replace = replace.substring(0, replace.length() - 1);
                        }
                        aVar.e = replace;
                    }
                } catch (Exception unused2) {
                }
            }
            return aVar;
        }
    }

    /* compiled from: StartAppSDK */
    class f {
        long a;
        int b;
        int c;
        long d;

        private f() {
            this.a = 0;
            this.b = 0;
            this.c = 0;
            this.d = 0;
        }

        /* synthetic */ f(c cVar, byte b2) {
            this();
        }
    }

    /* compiled from: StartAppSDK */
    class g {
        int a;
        String b;

        private g() {
            this.a = -1;
            this.b = "";
        }

        /* synthetic */ g(c cVar, byte b2) {
            this();
        }
    }

    /* compiled from: StartAppSDK */
    class h {
        ServiceStates a;
        long b;
        DuplexMode c;
        ThreeStateShort d;
        int e;
        ThreeStateShort f;

        private h() {
            this.a = ServiceStates.Unknown;
            this.b = 0;
            this.c = DuplexMode.Unknown;
            this.d = ThreeStateShort.Unknown;
            this.e = -1;
            this.f = ThreeStateShort.Unknown;
        }

        /* synthetic */ h(c cVar, byte b2) {
            this();
        }
    }

    /* compiled from: StartAppSDK */
    class i {
        int a;
        int b;
        int c;
        int d;
        int e;
        int f;
        int g;
        int h;
        int i;
        int j;
        long k;
        int l;
        int m;
        int n;
        int o;
        int p;
        int q;

        private i() {
            this.a = RadioInfo.INVALID.intValue();
            this.b = RadioInfo.INVALID.intValue();
            this.c = RadioInfo.INVALID.intValue();
            this.d = RadioInfo.INVALID.intValue();
            this.e = RadioInfo.INVALID.intValue();
            this.f = RadioInfo.INVALID.intValue();
            this.g = RadioInfo.INVALID.intValue();
            this.h = RadioInfo.INVALID.intValue();
            this.i = RadioInfo.INVALID.intValue();
            this.j = RadioInfo.INVALID.intValue();
            this.l = RadioInfo.INVALID.intValue();
            this.m = RadioInfo.INVALID.intValue();
            this.n = RadioInfo.INVALID.intValue();
            this.o = RadioInfo.INVALID.intValue();
            this.p = RadioInfo.INVALID.intValue();
            this.q = RadioInfo.INVALID.intValue();
        }

        /* synthetic */ i(c cVar, byte b2) {
            this();
        }
    }

    /* compiled from: StartAppSDK */
    class j extends PhoneStateListener {
        private Field b;

        public j() {
        }

        public j(int i) {
            try {
                this.b = getClass().getSuperclass().getDeclaredField("mSubId");
                this.b.setAccessible(true);
                this.b.set(this, Integer.valueOf(i));
            } catch (Exception unused) {
            }
        }

        private int a() {
            if (this.b != null) {
                try {
                    return ((Integer) this.b.get(this)).intValue();
                } catch (IllegalAccessException e) {
                    String i = c.c;
                    StringBuilder sb = new StringBuilder("getHiddenSubscriptionId: ");
                    sb.append(e.toString());
                    Log.e(i, sb.toString());
                }
            }
            return -1;
        }

        /* JADX WARNING: Removed duplicated region for block: B:177:0x0522  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x011d A[Catch:{ Exception -> 0x0136 }] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0131  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x015a A[Catch:{ Exception -> 0x0171 }] */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x016e  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x0197  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x0199  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0236  */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0290  */
        public final void onSignalStrengthsChanged(SignalStrength signalStrength) {
            int i;
            int i2;
            int i3;
            int i4;
            int i5;
            int i6;
            int i7;
            int i8;
            int i9;
            int i10;
            int i11;
            int i12;
            int i13;
            int i14;
            int i15;
            int i16;
            int i17;
            int i18;
            int i19;
            int i20;
            int i21;
            int i22;
            int i23;
            int i24;
            int i25;
            int i26;
            int i27;
            int i28;
            int i29;
            int i30;
            int i31;
            int i32;
            int i33;
            int i34;
            int i35;
            SignalStrength signalStrength2 = signalStrength;
            int a2 = a();
            int intValue = RadioInfo.INVALID.intValue();
            int intValue2 = RadioInfo.INVALID.intValue();
            int intValue3 = RadioInfo.INVALID.intValue();
            int intValue4 = RadioInfo.INVALID.intValue();
            int intValue5 = RadioInfo.INVALID.intValue();
            int intValue6 = RadioInfo.INVALID.intValue();
            int intValue7 = RadioInfo.INVALID.intValue();
            int intValue8 = RadioInfo.INVALID.intValue();
            int intValue9 = RadioInfo.INVALID.intValue();
            int intValue10 = RadioInfo.INVALID.intValue();
            int intValue11 = RadioInfo.INVALID.intValue();
            int intValue12 = RadioInfo.INVALID.intValue();
            int intValue13 = RadioInfo.INVALID.intValue();
            int intValue14 = RadioInfo.INVALID.intValue();
            int intValue15 = RadioInfo.INVALID.intValue();
            int intValue16 = RadioInfo.INVALID.intValue();
            int i36 = intValue9;
            int i37 = intValue;
            NetworkGenerations a3 = c.a(c.b(c.this.d.getNetworkType()));
            boolean z = true;
            int i38 = intValue2;
            if (VERSION.SDK_INT >= 29) {
                Iterator it = signalStrength.getCellSignalStrengths().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    CellSignalStrengthNr cellSignalStrengthNr = (CellSignalStrength) it.next();
                    Iterator it2 = it;
                    if (a3 == NetworkGenerations.Gen2 && (cellSignalStrengthNr instanceof CellSignalStrengthGsm)) {
                        i8 = ((CellSignalStrengthGsm) cellSignalStrengthNr).getDbm();
                        i9 = i8;
                        break;
                    } else if (a3 != NetworkGenerations.Gen3 || !(cellSignalStrengthNr instanceof CellSignalStrengthWcdma)) {
                        i30 = intValue3;
                        i31 = intValue4;
                        i32 = intValue5;
                        i33 = intValue6;
                        if (a3 != NetworkGenerations.Gen4 || !(cellSignalStrengthNr instanceof CellSignalStrengthLte)) {
                            if (a3 != NetworkGenerations.Gen5 || !(cellSignalStrengthNr instanceof CellSignalStrengthNr)) {
                                if (a3 == NetworkGenerations.Gen2 && (cellSignalStrengthNr instanceof CellSignalStrengthCdma)) {
                                    CellSignalStrengthCdma cellSignalStrengthCdma = (CellSignalStrengthCdma) cellSignalStrengthNr;
                                    int cdmaEcio = cellSignalStrengthCdma.getCdmaEcio();
                                    i8 = cellSignalStrengthCdma.getDbm();
                                    i37 = cdmaEcio;
                                    i9 = i8;
                                    break;
                                }
                                it = it2;
                                intValue3 = i30;
                                intValue4 = i31;
                                intValue5 = i32;
                                intValue6 = i33;
                            } else {
                                CellSignalStrengthNr cellSignalStrengthNr2 = cellSignalStrengthNr;
                                i9 = cellSignalStrengthNr2.getDbm();
                                intValue11 = cellSignalStrengthNr2.getCsiRsrp();
                                intValue12 = cellSignalStrengthNr2.getCsiRsrq();
                                intValue13 = cellSignalStrengthNr2.getCsiSinr();
                                intValue14 = cellSignalStrengthNr2.getSsRsrp();
                                intValue15 = cellSignalStrengthNr2.getSsRsrq();
                                intValue16 = cellSignalStrengthNr2.getSsSinr();
                                i8 = i9;
                                break;
                            }
                        } else {
                            CellSignalStrengthLte cellSignalStrengthLte = (CellSignalStrengthLte) cellSignalStrengthNr;
                            i9 = cellSignalStrengthLte.getDbm();
                            intValue3 = cellSignalStrengthLte.getCqi();
                            intValue4 = cellSignalStrengthLte.getRsrp();
                            intValue5 = cellSignalStrengthLte.getRssnr();
                            intValue6 = cellSignalStrengthLte.getRsrq();
                            intValue7 = cellSignalStrengthLte.getRssi();
                            i8 = i9;
                            break;
                        }
                    } else {
                        CellSignalStrengthWcdma cellSignalStrengthWcdma = (CellSignalStrengthWcdma) cellSignalStrengthNr;
                        try {
                            if (c.this.D != null) {
                                i4 = intValue3;
                                i3 = intValue4;
                                try {
                                    i35 = ((Integer) c.this.D.invoke(cellSignalStrengthWcdma, new Object[0])).intValue();
                                } catch (Exception e) {
                                    e = e;
                                    String i39 = c.c;
                                    i2 = intValue5;
                                    StringBuilder sb = new StringBuilder("updateSignalStrengthData.cellSignalStrengthWcdma.getRssi: ");
                                    sb.append(e.toString());
                                    Log.e(i39, sb.toString());
                                    i34 = i38;
                                    intValue10 = c.this.C != null ? ((Integer) c.this.C.invoke(cellSignalStrengthWcdma, new Object[0])).intValue() : intValue10;
                                    i = intValue6;
                                    intValue8 = c.this.E != null ? ((Integer) c.this.E.invoke(cellSignalStrengthWcdma, new Object[0])).intValue() : intValue8;
                                    int dbm = cellSignalStrengthWcdma.getDbm();
                                    if (i34 == RadioInfo.INVALID.intValue()) {
                                    }
                                    i7 = intValue14;
                                    i6 = intValue15;
                                    i5 = intValue16;
                                    int i40 = r5;
                                    i8 = dbm;
                                    i9 = i40;
                                    if (z) {
                                    }
                                    long elapsedRealtime = SystemClock.elapsedRealtime();
                                    i iVar = new i(c.this, 0);
                                    iVar.c = i22;
                                    iVar.a = i21;
                                    iVar.b = i19;
                                    iVar.d = i18;
                                    iVar.e = i17;
                                    iVar.f = i16;
                                    iVar.g = i15;
                                    iVar.h = i14;
                                    iVar.l = intValue11;
                                    iVar.m = intValue12;
                                    iVar.n = intValue13;
                                    iVar.o = i7;
                                    iVar.p = i11;
                                    iVar.q = i10;
                                    iVar.j = i20;
                                    iVar.i = i12;
                                    iVar.k = elapsedRealtime;
                                    c.this.i.a(i13, iVar);
                                }
                            } else {
                                i4 = intValue3;
                                i3 = intValue4;
                                i35 = i38;
                            }
                            i34 = i35;
                            i2 = intValue5;
                        } catch (Exception e2) {
                            e = e2;
                            i4 = intValue3;
                            i3 = intValue4;
                            String i392 = c.c;
                            i2 = intValue5;
                            StringBuilder sb2 = new StringBuilder("updateSignalStrengthData.cellSignalStrengthWcdma.getRssi: ");
                            sb2.append(e.toString());
                            Log.e(i392, sb2.toString());
                            i34 = i38;
                            intValue10 = c.this.C != null ? ((Integer) c.this.C.invoke(cellSignalStrengthWcdma, new Object[0])).intValue() : intValue10;
                            i = intValue6;
                            intValue8 = c.this.E != null ? ((Integer) c.this.E.invoke(cellSignalStrengthWcdma, new Object[0])).intValue() : intValue8;
                            int dbm2 = cellSignalStrengthWcdma.getDbm();
                            if (i34 == RadioInfo.INVALID.intValue()) {
                            }
                            i7 = intValue14;
                            i6 = intValue15;
                            i5 = intValue16;
                            int i402 = r5;
                            i8 = dbm2;
                            i9 = i402;
                            if (z) {
                            }
                            long elapsedRealtime2 = SystemClock.elapsedRealtime();
                            i iVar2 = new i(c.this, 0);
                            iVar2.c = i22;
                            iVar2.a = i21;
                            iVar2.b = i19;
                            iVar2.d = i18;
                            iVar2.e = i17;
                            iVar2.f = i16;
                            iVar2.g = i15;
                            iVar2.h = i14;
                            iVar2.l = intValue11;
                            iVar2.m = intValue12;
                            iVar2.n = intValue13;
                            iVar2.o = i7;
                            iVar2.p = i11;
                            iVar2.q = i10;
                            iVar2.j = i20;
                            iVar2.i = i12;
                            iVar2.k = elapsedRealtime2;
                            c.this.i.a(i13, iVar2);
                        }
                        try {
                            intValue10 = c.this.C != null ? ((Integer) c.this.C.invoke(cellSignalStrengthWcdma, new Object[0])).intValue() : intValue10;
                            i = intValue6;
                        } catch (Exception e3) {
                            String i41 = c.c;
                            i = intValue6;
                            StringBuilder sb3 = new StringBuilder("updateSignalStrengthData.cellSignalStrengthWcdma.getRscp: ");
                            sb3.append(e3.toString());
                            Log.e(i41, sb3.toString());
                        }
                        try {
                            intValue8 = c.this.E != null ? ((Integer) c.this.E.invoke(cellSignalStrengthWcdma, new Object[0])).intValue() : intValue8;
                        } catch (Exception e4) {
                            String i42 = c.c;
                            StringBuilder sb4 = new StringBuilder("updateSignalStrengthData.cellSignalStrengthWcdma.getEcNo: ");
                            sb4.append(e4.toString());
                            Log.e(i42, sb4.toString());
                        }
                        int dbm22 = cellSignalStrengthWcdma.getDbm();
                        int i43 = i34 == RadioInfo.INVALID.intValue() ? dbm22 : i34;
                        i7 = intValue14;
                        i6 = intValue15;
                        i5 = intValue16;
                        int i4022 = i43;
                        i8 = dbm22;
                        i9 = i4022;
                    }
                }
                i30 = intValue3;
                i31 = intValue4;
                i32 = intValue5;
                i33 = intValue6;
                i7 = intValue14;
                i6 = intValue15;
                i5 = intValue16;
                if (z) {
                    if (signalStrength.isGsm()) {
                        if (c.this.x == null || a3 != NetworkGenerations.Gen3) {
                            i24 = i8;
                            i23 = intValue7;
                        } else {
                            try {
                                i29 = c.this.x.getInt(signalStrength2);
                                i24 = i8;
                                i23 = intValue7;
                            } catch (IllegalAccessException e5) {
                                String i44 = c.c;
                                i24 = i8;
                                i23 = intValue7;
                                StringBuilder sb5 = new StringBuilder("updateSignalStrengthData.WcdmaRscp: ");
                                sb5.append(e5.toString());
                                Log.e(i44, sb5.toString());
                            }
                            i25 = signalStrength.getGsmSignalStrength();
                            if (i25 == 0 || i29 == RadioInfo.INVALID.intValue()) {
                                if (i25 >= 0) {
                                    i25 = c.d(i25);
                                }
                                intValue10 = i29;
                            } else {
                                i25 = i29;
                                intValue10 = i25;
                            }
                        }
                        i29 = intValue10;
                        i25 = signalStrength.getGsmSignalStrength();
                        if (i25 == 0) {
                        }
                        if (i25 >= 0) {
                        }
                        intValue10 = i29;
                    } else {
                        i24 = i8;
                        i23 = intValue7;
                        int cdmaDbm = signalStrength.getCdmaDbm();
                        i37 = signalStrength.getCdmaEcio();
                        i25 = cdmaDbm;
                    }
                    try {
                        i26 = i25;
                        i27 = c.this.n != null ? ((Integer) c.this.n.invoke(signalStrength2, new Object[0])).intValue() : i24;
                    } catch (Exception e6) {
                        String i45 = c.c;
                        i26 = i25;
                        StringBuilder sb6 = new StringBuilder("updateSignalStrengthData.GetDbm: ");
                        sb6.append(e6.toString());
                        Log.e(i45, sb6.toString());
                        i27 = i24;
                    }
                    if (a3 == NetworkGenerations.Gen4) {
                        try {
                            if (c.this.p != null) {
                                i26 = ((Integer) c.this.p.invoke(signalStrength2, new Object[0])).intValue();
                            }
                            i28 = i27;
                        } catch (Exception e7) {
                            String i46 = c.c;
                            i28 = i27;
                            StringBuilder sb7 = new StringBuilder("updateSignalStrengthData.GetLTEDbm: ");
                            sb7.append(e7.toString());
                            Log.e(i46, sb7.toString());
                        }
                        if (c.this.o != null) {
                            try {
                                i23 = ((Integer) c.this.o.invoke(signalStrength2, new Object[0])).intValue();
                            } catch (Exception e8) {
                                String i47 = c.c;
                                StringBuilder sb8 = new StringBuilder("updateSignalStrengthData.GetLteSignalStrength: ");
                                sb8.append(e8.toString());
                                Log.e(i47, sb8.toString());
                            }
                        }
                        try {
                            if (c.this.r != null) {
                                i4 = ((Integer) c.this.r.invoke(signalStrength2, new Object[0])).intValue();
                            }
                        } catch (Exception e9) {
                            String i48 = c.c;
                            StringBuilder sb9 = new StringBuilder("updateSignalStrengthData.GetLteCqi: ");
                            sb9.append(e9.toString());
                            Log.e(i48, sb9.toString());
                        }
                        try {
                            if (c.this.s != null) {
                                i3 = ((Integer) c.this.s.invoke(signalStrength2, new Object[0])).intValue();
                            }
                        } catch (Exception e10) {
                            String i49 = c.c;
                            StringBuilder sb10 = new StringBuilder("updateSignalStrengthData.GetLteRsrp: ");
                            sb10.append(e10.toString());
                            Log.e(i49, sb10.toString());
                        }
                        try {
                            if (c.this.t != null) {
                                i = ((Integer) c.this.t.invoke(signalStrength2, new Object[0])).intValue();
                            }
                        } catch (Exception e11) {
                            String i50 = c.c;
                            StringBuilder sb11 = new StringBuilder("updateSignalStrengthData.GetLteRsrq: ");
                            sb11.append(e11.toString());
                            Log.e(i50, sb11.toString());
                        }
                        try {
                            i2 = c.this.u != null ? ((Integer) c.this.u.invoke(signalStrength2, new Object[0])).intValue() : i2;
                        } catch (Exception e12) {
                            String i51 = c.c;
                            StringBuilder sb12 = new StringBuilder("updateSignalStrengthData.GetLteRssnr: ");
                            sb12.append(e12.toString());
                            Log.e(i51, sb12.toString());
                        }
                    } else {
                        i28 = i27;
                    }
                    int i52 = i23;
                    if (a3 == NetworkGenerations.Gen5) {
                        if (c.this.z != null) {
                            try {
                                intValue11 = c.this.z.getInt(signalStrength2);
                            } catch (IllegalAccessException e13) {
                                String i53 = c.c;
                                StringBuilder sb13 = new StringBuilder("updateSignalStrengthData.NrCsiRsrp: ");
                                sb13.append(e13.toString());
                                Log.e(i53, sb13.toString());
                            }
                        }
                        if (c.this.A != null) {
                            try {
                                intValue12 = c.this.A.getInt(signalStrength2);
                            } catch (IllegalAccessException e14) {
                                String i54 = c.c;
                                StringBuilder sb14 = new StringBuilder("updateSignalStrengthData.NrCsiRsrq: ");
                                sb14.append(e14.toString());
                                Log.e(i54, sb14.toString());
                            }
                        }
                        if (c.this.B != null) {
                            try {
                                intValue13 = c.this.B.getInt(signalStrength2);
                            } catch (IllegalAccessException e15) {
                                String i55 = c.c;
                                StringBuilder sb15 = new StringBuilder("updateSignalStrengthData.NrCsiSinr: ");
                                sb15.append(e15.toString());
                                Log.e(i55, sb15.toString());
                            }
                        }
                    }
                    try {
                        if (c.this.q != null) {
                            intValue8 = ((Integer) c.this.q.invoke(signalStrength2, new Object[0])).intValue();
                        }
                    } catch (Exception e16) {
                        String i56 = c.c;
                        StringBuilder sb16 = new StringBuilder("updateSignalStrengthData.GetEcno: ");
                        sb16.append(e16.toString());
                        Log.e(i56, sb16.toString());
                    }
                    i13 = a2;
                    i11 = i6;
                    i10 = i5;
                    i12 = intValue8;
                    i20 = intValue10;
                    i22 = i37;
                    i18 = i4;
                    i17 = i3;
                    i15 = i;
                    i21 = i26;
                    i19 = i28;
                    i14 = i52;
                    i16 = i2;
                } else {
                    int i57 = i8;
                    int i58 = intValue7;
                    i21 = i9;
                    i13 = a2;
                    i11 = i6;
                    i10 = i5;
                    i12 = intValue8;
                    i20 = intValue10;
                    i22 = i37;
                    i18 = i4;
                    i17 = i3;
                    i16 = i2;
                    i15 = i;
                    i19 = i57;
                    i14 = i58;
                }
                long elapsedRealtime22 = SystemClock.elapsedRealtime();
                i iVar22 = new i(c.this, 0);
                iVar22.c = i22;
                iVar22.a = i21;
                iVar22.b = i19;
                iVar22.d = i18;
                iVar22.e = i17;
                iVar22.f = i16;
                iVar22.g = i15;
                iVar22.h = i14;
                iVar22.l = intValue11;
                iVar22.m = intValue12;
                iVar22.n = intValue13;
                iVar22.o = i7;
                iVar22.p = i11;
                iVar22.q = i10;
                iVar22.j = i20;
                iVar22.i = i12;
                iVar22.k = elapsedRealtime22;
                c.this.i.a(i13, iVar22);
            }
            i4 = intValue3;
            i3 = intValue4;
            i2 = intValue5;
            i = intValue6;
            i7 = intValue14;
            i6 = intValue15;
            i5 = intValue16;
            i8 = i36;
            i9 = i38;
            z = false;
            if (z) {
            }
            long elapsedRealtime222 = SystemClock.elapsedRealtime();
            i iVar222 = new i(c.this, 0);
            iVar222.c = i22;
            iVar222.a = i21;
            iVar222.b = i19;
            iVar222.d = i18;
            iVar222.e = i17;
            iVar222.f = i16;
            iVar222.g = i15;
            iVar222.h = i14;
            iVar222.l = intValue11;
            iVar222.m = intValue12;
            iVar222.n = intValue13;
            iVar222.o = i7;
            iVar222.p = i11;
            iVar222.q = i10;
            iVar222.j = i20;
            iVar222.i = i12;
            iVar222.k = elapsedRealtime222;
            c.this.i.a(i13, iVar222);
        }

        public final void onServiceStateChanged(final ServiceState serviceState) {
            ServiceStates serviceStates;
            DuplexMode duplexMode;
            final int a2 = a();
            h hVar = new h(c.this, 0);
            if (VERSION.SDK_INT >= 25) {
                if (c.this.F != null) {
                    try {
                        hVar.f = c.this.F.getBoolean(serviceState) ? ThreeStateShort.Yes : ThreeStateShort.No;
                    } catch (IllegalAccessException e) {
                        String i = c.c;
                        StringBuilder sb = new StringBuilder("mFieldIsUsingCarrierAggregation: ");
                        sb.append(e.toString());
                        Log.e(i, sb.toString());
                    }
                }
                if (VERSION.SDK_INT >= 28) {
                    switch (serviceState.getDuplexMode()) {
                        case 1:
                            duplexMode = DuplexMode.FDD;
                            break;
                        case 2:
                            duplexMode = DuplexMode.TDD;
                            break;
                        default:
                            duplexMode = DuplexMode.Unknown;
                            break;
                    }
                    hVar.c = duplexMode;
                    hVar.e = serviceState.getChannelNumber();
                }
            }
            hVar.d = serviceState.getIsManualSelection() ? ThreeStateShort.Yes : ThreeStateShort.No;
            switch (serviceState.getState()) {
                case 0:
                    serviceStates = ServiceStates.InService;
                    break;
                case 1:
                    serviceStates = ServiceStates.OutOfService;
                    break;
                case 2:
                    serviceStates = ServiceStates.EmergencyOnly;
                    break;
                case 3:
                    serviceStates = ServiceStates.PowerOff;
                    break;
                default:
                    serviceStates = ServiceStates.Unknown;
                    break;
            }
            hVar.a = serviceStates;
            hVar.b = SystemClock.elapsedRealtime();
            NetworkRegistrationInfo[] a3 = com.iab.omid.library.startapp.b.a(serviceState.toString());
            c.this.i.a(a2, hVar);
            c.this.i.a(a2, a3);
            c.this.a.post(new Runnable() {
                public final void run() {
                    for (com.startapp.networkTest.controller.a.a a2 : c.this.b) {
                        a2.a(serviceState, a2);
                    }
                }
            });
        }

        public final void onCellLocationChanged(final CellLocation cellLocation) {
            final int a2 = a();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            b bVar = new b(c.this, 0);
            bVar.a = cellLocation;
            bVar.b = elapsedRealtime;
            c.this.i.a(a2, bVar);
            c.this.a.post(new Runnable() {
                public final void run() {
                    for (com.startapp.networkTest.controller.a.a a2 : c.this.b) {
                        a2.a(cellLocation, a2);
                    }
                }
            });
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x007d  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0085  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0088  */
        public final void onCellInfoChanged(List<CellInfo> list) {
            int i;
            int i2;
            long nci;
            int tac;
            int pci;
            if (list != null) {
                c.this.m = list;
                if (VERSION.SDK_INT >= 29) {
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        CellInfoNr cellInfoNr = (CellInfo) it.next();
                        if (cellInfoNr.isRegistered() && (cellInfoNr instanceof CellInfoNr)) {
                            CellInfoNr cellInfoNr2 = cellInfoNr;
                            CellIdentityNr cellIdentity = cellInfoNr2.getCellIdentity();
                            if (cellIdentity instanceof CellIdentityNr) {
                                CellIdentityNr cellIdentityNr = cellIdentity;
                                try {
                                    i2 = Integer.parseInt(cellIdentityNr.getMccString());
                                    try {
                                        i = Integer.parseInt(cellIdentityNr.getMncString());
                                    } catch (NumberFormatException e) {
                                        e = e;
                                        c.c;
                                        new StringBuilder("updateCellInfo: ").append(e.getMessage());
                                        i = 0;
                                        nci = cellIdentityNr.getNci();
                                        tac = cellIdentityNr.getTac();
                                        pci = cellIdentityNr.getPci();
                                        StringBuilder sb = new StringBuilder();
                                        sb.append(i2);
                                        sb.append(i);
                                        String sb2 = sb.toString();
                                        if (nci == 2147483647L) {
                                        }
                                        if (tac == Integer.MAX_VALUE) {
                                        }
                                        if (pci == Integer.MAX_VALUE) {
                                        }
                                        f fVar = new f(c.this, 0);
                                        fVar.a = nci;
                                        fVar.b = tac;
                                        fVar.c = pci;
                                        fVar.d = cellInfoNr2.getTimeStamp();
                                        c.this.i.a(sb2, fVar);
                                    }
                                } catch (NumberFormatException e2) {
                                    e = e2;
                                    i2 = 0;
                                    c.c;
                                    new StringBuilder("updateCellInfo: ").append(e.getMessage());
                                    i = 0;
                                    nci = cellIdentityNr.getNci();
                                    tac = cellIdentityNr.getTac();
                                    pci = cellIdentityNr.getPci();
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append(i2);
                                    sb3.append(i);
                                    String sb22 = sb3.toString();
                                    if (nci == 2147483647L) {
                                    }
                                    if (tac == Integer.MAX_VALUE) {
                                    }
                                    if (pci == Integer.MAX_VALUE) {
                                    }
                                    f fVar2 = new f(c.this, 0);
                                    fVar2.a = nci;
                                    fVar2.b = tac;
                                    fVar2.c = pci;
                                    fVar2.d = cellInfoNr2.getTimeStamp();
                                    c.this.i.a(sb22, fVar2);
                                }
                                nci = cellIdentityNr.getNci();
                                tac = cellIdentityNr.getTac();
                                pci = cellIdentityNr.getPci();
                                StringBuilder sb32 = new StringBuilder();
                                sb32.append(i2);
                                sb32.append(i);
                                String sb222 = sb32.toString();
                                if (nci == 2147483647L) {
                                    nci = -1;
                                }
                                if (tac == Integer.MAX_VALUE) {
                                    tac = -1;
                                }
                                if (pci == Integer.MAX_VALUE) {
                                    pci = -1;
                                }
                                f fVar22 = new f(c.this, 0);
                                fVar22.a = nci;
                                fVar22.b = tac;
                                fVar22.c = pci;
                                fVar22.d = cellInfoNr2.getTimeStamp();
                                c.this.i.a(sb222, fVar22);
                            }
                        }
                    }
                }
            }
        }
    }

    static /* synthetic */ int d(int i2) {
        if (i2 == 99 || i2 < 0 || i2 > 31) {
            return 0;
        }
        return (i2 * 2) - 113;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(51:0|(3:2|3|4)|5|(2:7|8)|9|(2:11|12)|13|15|16|17|(2:19|20)|21|(2:23|24)|25|(2:27|28)|29|(2:31|32)|33|(2:35|36)|37|(2:39|40)|41|(2:43|44)|45|(9:48|49|50|51|53|54|55|57|58)|59|(9:62|63|64|65|67|68|69|71|72)|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|(1:90)|91|92|(1:94)|95|(12:98|99|100|101|103|104|105|107|108|109|111|112)|113|(2:116|117)(1:118)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(52:0|(3:2|3|4)|5|(2:7|8)|9|(2:11|12)|13|15|16|17|(2:19|20)|21|(2:23|24)|25|(2:27|28)|29|(2:31|32)|33|35|36|37|(2:39|40)|41|(2:43|44)|45|(9:48|49|50|51|53|54|55|57|58)|59|(9:62|63|64|65|67|68|69|71|72)|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|(1:90)|91|92|(1:94)|95|(12:98|99|100|101|103|104|105|107|108|109|111|112)|113|(2:116|117)(1:118)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(53:0|(3:2|3|4)|5|(2:7|8)|9|11|12|13|15|16|17|(2:19|20)|21|(2:23|24)|25|(2:27|28)|29|(2:31|32)|33|35|36|37|(2:39|40)|41|(2:43|44)|45|(9:48|49|50|51|53|54|55|57|58)|59|(9:62|63|64|65|67|68|69|71|72)|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|(1:90)|91|92|(1:94)|95|(12:98|99|100|101|103|104|105|107|108|109|111|112)|113|(2:116|117)(1:118)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(55:0|(3:2|3|4)|5|7|8|9|11|12|13|15|16|17|(2:19|20)|21|(2:23|24)|25|(2:27|28)|29|31|32|33|35|36|37|(2:39|40)|41|(2:43|44)|45|(9:48|49|50|51|53|54|55|57|58)|59|(9:62|63|64|65|67|68|69|71|72)|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|(1:90)|91|92|(1:94)|95|(12:98|99|100|101|103|104|105|107|108|109|111|112)|113|(2:116|117)(1:118)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(56:0|(3:2|3|4)|5|7|8|9|11|12|13|15|16|17|(2:19|20)|21|(2:23|24)|25|27|28|29|31|32|33|35|36|37|(2:39|40)|41|(2:43|44)|45|(9:48|49|50|51|53|54|55|57|58)|59|(9:62|63|64|65|67|68|69|71|72)|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|(1:90)|91|92|(1:94)|95|(12:98|99|100|101|103|104|105|107|108|109|111|112)|113|(2:116|117)(1:118)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(58:0|(3:2|3|4)|5|7|8|9|11|12|13|15|16|17|(2:19|20)|21|23|24|25|27|28|29|31|32|33|35|36|37|(2:39|40)|41|43|44|45|(9:48|49|50|51|53|54|55|57|58)|59|(9:62|63|64|65|67|68|69|71|72)|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|(1:90)|91|92|(1:94)|95|(12:98|99|100|101|103|104|105|107|108|109|111|112)|113|(2:116|117)(1:118)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(59:0|(3:2|3|4)|5|7|8|9|11|12|13|15|16|17|19|20|21|23|24|25|27|28|29|31|32|33|35|36|37|(2:39|40)|41|43|44|45|(9:48|49|50|51|53|54|55|57|58)|59|(9:62|63|64|65|67|68|69|71|72)|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|(1:90)|91|92|(1:94)|95|(12:98|99|100|101|103|104|105|107|108|109|111|112)|113|(2:116|117)(1:118)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(60:0|(3:2|3|4)|5|7|8|9|11|12|13|15|16|17|19|20|21|23|24|25|27|28|29|31|32|33|35|36|37|39|40|41|43|44|45|(9:48|49|50|51|53|54|55|57|58)|59|(9:62|63|64|65|67|68|69|71|72)|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|(1:90)|91|92|(1:94)|95|(12:98|99|100|101|103|104|105|107|108|109|111|112)|113|(2:116|117)(1:118)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x013b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:75:0x014b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:77:0x015f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:79:0x0173 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:81:0x0187 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:83:0x019b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:85:0x01af */
    /* JADX WARNING: Missing exception handler attribute for start block: B:87:0x01c3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:91:0x01e1 */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:118:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01da A[Catch:{ Exception -> 0x01e1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01fd A[Catch:{ Exception -> 0x0204 }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x020a  */
    public c(Context context) {
        this.e = context;
        this.d = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        this.h = (ConnectivityManager) context.getSystemService("connectivity");
        j();
        a(this.S);
        if (VERSION.SDK_INT >= 25) {
            try {
                this.F = ServiceState.class.getDeclaredField("mIsUsingCarrierAggregation");
                this.F.setAccessible(true);
            } catch (Exception unused) {
            }
        }
        try {
            this.o = SignalStrength.class.getDeclaredMethod("getLteSignalStrength", new Class[0]);
        } catch (Exception unused2) {
        }
        try {
            this.r = SignalStrength.class.getDeclaredMethod("getLteCqi", new Class[0]);
        } catch (Exception unused3) {
        }
        try {
            this.s = SignalStrength.class.getDeclaredMethod("getLteRsrp", new Class[0]);
        } catch (Exception unused4) {
        }
        try {
            this.t = SignalStrength.class.getDeclaredMethod("getLteRsrq", new Class[0]);
        } catch (Exception unused5) {
        }
        try {
            this.u = SignalStrength.class.getDeclaredMethod("getLteRssnr", new Class[0]);
        } catch (Exception unused6) {
        }
        try {
            this.p = SignalStrength.class.getDeclaredMethod("getLteDbm", new Class[0]);
        } catch (Exception unused7) {
        }
        try {
            this.n = SignalStrength.class.getDeclaredMethod("getDbm", new Class[0]);
        } catch (Exception unused8) {
        }
        try {
            this.q = SignalStrength.class.getDeclaredMethod("getGsmEcno", new Class[0]);
        } catch (Exception unused9) {
        }
        try {
            this.x = SignalStrength.class.getDeclaredField("mWcdmaRscp");
            this.x.setAccessible(true);
        } catch (NoSuchFieldException unused10) {
        }
        try {
            this.y = SignalStrength.class.getDeclaredField("mWcdmaEcio");
            this.y.setAccessible(true);
        } catch (NoSuchFieldException unused11) {
        }
        if (VERSION.SDK_INT >= 28) {
            try {
                this.z = SignalStrength.class.getDeclaredField("mNrRsrp");
                this.z.setAccessible(true);
            } catch (NoSuchFieldException unused12) {
            }
            try {
                this.A = SignalStrength.class.getDeclaredField("mNrRsrq");
                this.A.setAccessible(true);
            } catch (NoSuchFieldException unused13) {
            }
            try {
                this.B = SignalStrength.class.getDeclaredField("mNrRssnr");
                this.B.setAccessible(true);
            } catch (NoSuchFieldException unused14) {
            }
        }
        if (VERSION.SDK_INT >= 18) {
            try {
                this.D = CellSignalStrengthWcdma.class.getDeclaredMethod("getRssi", new Class[0]);
            } catch (Exception unused15) {
            }
            try {
                this.C = CellSignalStrengthWcdma.class.getDeclaredMethod("getRscp", new Class[0]);
            } catch (Exception unused16) {
            }
            this.E = CellSignalStrengthWcdma.class.getDeclaredMethod("getEcNo", new Class[0]);
        }
        this.K = this.d.getClass().getDeclaredMethod("getDataEnabled", new Class[0]);
        this.L = this.d.getClass().getDeclaredMethod("getDataEnabled", new Class[]{Integer.TYPE});
        this.M = this.d.getClass().getDeclaredMethod("isNetworkRoaming", new Class[]{Integer.TYPE});
        this.N = this.d.getClass().getDeclaredMethod("getNetworkType", new Class[]{Integer.TYPE});
        this.O = this.d.getClass().getDeclaredMethod("getNetworkOperatorName", new Class[]{Integer.TYPE});
        this.P = this.d.getClass().getDeclaredMethod("getNetworkOperator", new Class[]{Integer.TYPE});
        this.Q = this.d.getClass().getDeclaredMethod("getNetworkOperatorForSubscription", new Class[]{Integer.TYPE});
        Method declaredMethod = this.d.getClass().getDeclaredMethod("getVoiceNetworkType", null);
        if (!Modifier.isAbstract(declaredMethod.getModifiers())) {
            this.v = declaredMethod;
            this.v.setAccessible(true);
        }
        Method declaredMethod2 = this.d.getClass().getDeclaredMethod("getVoiceNetworkType", new Class[]{Integer.TYPE});
        if (!Modifier.isAbstract(declaredMethod2.getModifiers())) {
            this.w = declaredMethod2;
            this.w.setAccessible(true);
        }
        if (VERSION.SDK_INT >= 17) {
            try {
                this.G = CellSignalStrengthLte.class.getDeclaredField("mSignalStrength");
                this.G.setAccessible(true);
            } catch (NoSuchFieldException unused17) {
            }
            try {
                this.H = CellSignalStrengthLte.class.getDeclaredField("mRsrq");
                this.H.setAccessible(true);
            } catch (NoSuchFieldException unused18) {
            }
            try {
                this.I = CellSignalStrengthLte.class.getDeclaredField("mRssnr");
                this.I.setAccessible(true);
            } catch (NoSuchFieldException unused19) {
            }
            try {
                this.J = CellSignalStrengthLte.class.getDeclaredField("mCqi");
                this.J.setAccessible(true);
            } catch (NoSuchFieldException unused20) {
            }
        }
        if (VERSION.SDK_INT < 22) {
            this.j = new OnSubscriptionsChangedListener() {
                public final void onSubscriptionsChanged() {
                    super.onSubscriptionsChanged();
                    if (!c.this.T) {
                        new C0067c().executeOnExecutor(com.startapp.networkTest.threads.a.a().d(), new Void[0]);
                    }
                }
            };
        }
    }

    /* access modifiers changed from: private */
    public void a(int[] iArr) {
        this.g = new ArrayList<>();
        for (int jVar : iArr) {
            this.g.add(new j(jVar));
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        this.k = b.g(this.e);
        com.startapp.networkTest.data.a.b[] bVarArr = (com.startapp.networkTest.data.a.b[]) this.k.SimInfos.toArray(new com.startapp.networkTest.data.a.b[this.k.SimInfos.size()]);
        int[] iArr = new int[bVarArr.length];
        for (int i2 = 0; i2 < bVarArr.length; i2++) {
            iArr[i2] = bVarArr[i2].SubscriptionId;
        }
        this.S = iArr;
    }

    public final void a() {
        try {
            a(true);
            Context context = this.e;
            if (this.l == null) {
                this.l = new e(this, 0);
            }
            this.l.getClass();
            IntentFilter intentFilter = new IntentFilter("android.intent.action.ANY_DATA_STATE");
            this.l.getClass();
            intentFilter.addAction("com.samsung.ims.action.IMS_REGISTRATION");
            context.registerReceiver(this.l, intentFilter);
        } catch (Throwable th) {
            com.startapp.networkTest.startapp.a.a(th);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        if (z2 && this.j != null && this.e.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0 && VERSION.SDK_INT >= 22) {
            SubscriptionManager.from(this.e).addOnSubscriptionsChangedListener(this.j);
        }
        if (this.d != null) {
            int i2 = InputDeviceCompat.SOURCE_KEYBOARD;
            if (this.e.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                i2 = 273;
                if (VERSION.SDK_INT >= 17) {
                    i2 = 1297;
                }
            }
            if (this.g.size() == 0) {
                if (this.f == null) {
                    this.f = new j();
                }
                this.d.listen(this.f, i2);
                return;
            }
            Iterator it = this.g.iterator();
            while (it.hasNext()) {
                this.d.listen((j) it.next(), i2);
            }
        }
    }

    public final void b() {
        try {
            b(true);
            Context context = this.e;
            if (!(context == null || this.l == null)) {
                context.unregisterReceiver(this.l);
            }
        } catch (Throwable th) {
            com.startapp.networkTest.startapp.a.a(th);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (z2 && this.j != null && this.e.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0 && VERSION.SDK_INT >= 22) {
            SubscriptionManager.from(this.e).removeOnSubscriptionsChangedListener(this.j);
        }
        if (this.d != null) {
            if (this.f != null) {
                this.d.listen(this.f, 0);
            }
            Iterator it = this.g.iterator();
            while (it.hasNext()) {
                this.d.listen((j) it.next(), 0);
            }
        }
    }

    public final RadioInfo c() {
        return e(this.k.DefaultDataSimId);
    }

    @Deprecated
    private RadioInfo k() {
        RadioInfo radioInfo = new RadioInfo();
        if (this.d != null) {
            PreferredNetworkTypes preferredNetworkTypes = (PreferredNetworkTypes) a(this.e).get(0);
            if (preferredNetworkTypes != null) {
                radioInfo.PreferredNetwork = preferredNetworkTypes;
            }
            try {
                switch (this.d.getDataState()) {
                    case 0:
                        radioInfo.MobileDataConnectionState = DataConnectionStates.Disconnected;
                        break;
                    case 1:
                        radioInfo.MobileDataConnectionState = DataConnectionStates.Connecting;
                        break;
                    case 2:
                        radioInfo.MobileDataConnectionState = DataConnectionStates.Connected;
                        break;
                    case 3:
                        radioInfo.MobileDataConnectionState = DataConnectionStates.Suspended;
                        break;
                    default:
                        radioInfo.MobileDataConnectionState = DataConnectionStates.Unknown;
                        break;
                }
            } catch (SecurityException e2) {
                String str = c;
                StringBuilder sb = new StringBuilder("getRadioInfo: getDataState: ");
                sb.append(e2.toString());
                Log.e(str, sb.toString());
            }
            radioInfo.FlightMode = m() ? FlightModeStates.Enabled : FlightModeStates.Disabled;
            radioInfo.MobileDataEnabled = ThreeState.Unknown;
            if (this.K != null) {
                try {
                    radioInfo.MobileDataEnabled = ((Boolean) this.K.invoke(this.d, new Object[0])).booleanValue() ? ThreeState.Enabled : ThreeState.Disabled;
                } catch (Exception e3) {
                    String str2 = c;
                    StringBuilder sb2 = new StringBuilder("getRadioInfo: MobileDataEnabled: ");
                    sb2.append(e3.toString());
                    Log.e(str2, sb2.toString());
                }
            } else if (VERSION.SDK_INT >= 17) {
                try {
                    radioInfo.MobileDataEnabled = Global.getInt(this.R, "mobile_data") == 1 ? ThreeState.Enabled : ThreeState.Disabled;
                } catch (Throwable th) {
                    String str3 = c;
                    StringBuilder sb3 = new StringBuilder("getRadioInfo: MobileDataEnabled: ");
                    sb3.append(th.toString());
                    Log.e(str3, sb3.toString());
                }
            } else {
                try {
                    radioInfo.MobileDataEnabled = Secure.getInt(this.R, "mobile_data") == 1 ? ThreeState.Enabled : ThreeState.Disabled;
                } catch (Throwable th2) {
                    String str4 = c;
                    StringBuilder sb4 = new StringBuilder("getRadioInfo: MobileDataEnabled: ");
                    sb4.append(th2.toString());
                    Log.e(str4, sb4.toString());
                }
            }
            radioInfo.IsRoaming = this.d.isNetworkRoaming();
            radioInfo.IsMetered = l();
            radioInfo.ConnectionType = f();
            radioInfo.NetworkType = b(this.d.getNetworkType());
            radioInfo.OperatorName = com.startapp.networkTest.utils.g.a(this.d.getNetworkOperatorName());
            String networkOperator = this.d.getNetworkOperator();
            if (networkOperator != null && networkOperator.length() > 4) {
                radioInfo.MCC = networkOperator.substring(0, 3);
                radioInfo.MNC = networkOperator.substring(3);
            }
            b c2 = this.i.c(-1);
            if (c2 == null) {
                c2 = new b(this, 0);
            }
            if (this.e.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
                radioInfo.MissingPermission = true;
            } else if (c2.a == null) {
                c2.a = this.d.getCellLocation();
            }
            int i2 = Integer.MAX_VALUE;
            if (c2.a != null) {
                if (c2.a.getClass().equals(GsmCellLocation.class)) {
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) c2.a;
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(gsmCellLocation.getLac());
                    radioInfo.GsmLAC = sb5.toString();
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append(gsmCellLocation.getCid());
                    radioInfo.GsmCellId = sb6.toString();
                    StringBuilder sb7 = new StringBuilder();
                    sb7.append(gsmCellLocation.getPsc());
                    radioInfo.PrimaryScramblingCode = sb7.toString();
                } else if (c2.a.getClass().equals(CdmaCellLocation.class)) {
                    CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) c2.a;
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(cdmaCellLocation.getBaseStationId());
                    radioInfo.CdmaBaseStationId = sb8.toString();
                    StringBuilder sb9 = new StringBuilder();
                    sb9.append(cdmaCellLocation.getBaseStationLatitude());
                    radioInfo.CdmaBaseStationLatitude = sb9.toString();
                    StringBuilder sb10 = new StringBuilder();
                    sb10.append(cdmaCellLocation.getBaseStationLongitude());
                    radioInfo.CdmaBaseStationLongitude = sb10.toString();
                    StringBuilder sb11 = new StringBuilder();
                    sb11.append(cdmaCellLocation.getNetworkId());
                    radioInfo.CdmaNetworkId = sb11.toString();
                    StringBuilder sb12 = new StringBuilder();
                    sb12.append(cdmaCellLocation.getSystemId());
                    radioInfo.CdmaSystemId = sb12.toString();
                }
                if (c2.b > 0) {
                    long elapsedRealtime = SystemClock.elapsedRealtime() - c2.b;
                    radioInfo.GsmCellIdAge = elapsedRealtime > 2147483647L ? Integer.MAX_VALUE : (int) elapsedRealtime;
                }
            }
            h b2 = this.i.b(-1);
            radioInfo.ServiceState = b2.a;
            radioInfo.DuplexMode = b2.c;
            radioInfo.ManualSelection = b2.d;
            radioInfo.CarrierAggregation = b2.f;
            radioInfo.ARFCN = b2.e;
            if (b2.b > 0) {
                long elapsedRealtime2 = SystemClock.elapsedRealtime() - b2.b;
                radioInfo.ServiceStateAge = elapsedRealtime2 > 2147483647L ? Integer.MAX_VALUE : (int) elapsedRealtime2;
            }
            NetworkRegistrationInfo[] d2 = this.i.d(-1);
            radioInfo.NrState = a(d2);
            radioInfo.NrAvailable = b(d2);
            i a2 = this.i.a(-1);
            radioInfo.RSCP = a2.j;
            radioInfo.CdmaEcIo = a2.c;
            radioInfo.RXLevel = a2.a;
            radioInfo.NativeDbm = a2.b;
            radioInfo.EcN0 = a2.i;
            radioInfo.LteCqi = a2.d;
            radioInfo.LteRsrp = a2.e;
            radioInfo.LteRsrq = a2.g;
            radioInfo.LteRssnr = a2.f;
            radioInfo.LteRssi = a2.h;
            radioInfo.NrCsiRsrp = a2.l;
            radioInfo.NrCsiRsrq = a2.m;
            radioInfo.NrCsiSinr = a2.n;
            radioInfo.NrSsRsrp = a2.o;
            radioInfo.NrSsRsrq = a2.p;
            radioInfo.NrSsSinr = a2.q;
            if ((radioInfo.NetworkType == NetworkTypes.LTE || radioInfo.NetworkType == NetworkTypes.LTE_CA) && radioInfo.RXLevel >= 0) {
                radioInfo.RXLevel = radioInfo.LteRsrp;
            }
            if (radioInfo.NetworkType == NetworkTypes.LTE_CA) {
                radioInfo.CarrierAggregation = ThreeStateShort.Yes;
            }
            long elapsedRealtime3 = SystemClock.elapsedRealtime() - a2.k;
            if (elapsedRealtime3 <= 2147483647L) {
                i2 = (int) elapsedRealtime3;
            }
            radioInfo.RXLevelAge = i2;
        }
        return radioInfo;
    }

    public final NetworkRegistrationInfo[] a(int i2) {
        NetworkRegistrationInfo[] d2 = this.i.d(i2);
        if (d2 == null) {
            return new NetworkRegistrationInfo[0];
        }
        h b2 = this.i.b(i2);
        for (NetworkRegistrationInfo networkRegistrationInfo : d2) {
            if (b2 != null && b2.b > 0) {
                long elapsedRealtime = SystemClock.elapsedRealtime() - b2.b;
                networkRegistrationInfo.Age = elapsedRealtime > 2147483647L ? Integer.MAX_VALUE : (int) elapsedRealtime;
            }
        }
        return d2;
    }

    @TargetApi(18)
    public final com.startapp.networkTest.data.radio.CellInfo[] d() {
        CellConnectionStatus cellConnectionStatus;
        if (this.e.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
            return new com.startapp.networkTest.data.radio.CellInfo[0];
        }
        ArrayList arrayList = new ArrayList();
        if (this.h != null && VERSION.SDK_INT >= 21 && this.e.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
            List<CellInfo> allCellInfo = this.d.getAllCellInfo();
            if (this.m != null && (allCellInfo == null || allCellInfo.isEmpty())) {
                allCellInfo = this.m;
            }
            if (allCellInfo == null) {
                return new com.startapp.networkTest.data.radio.CellInfo[0];
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            Iterator it = allCellInfo.iterator();
            while (it.hasNext()) {
                CellInfoNr cellInfoNr = (CellInfo) it.next();
                com.startapp.networkTest.data.radio.CellInfo cellInfo = new com.startapp.networkTest.data.radio.CellInfo();
                if (VERSION.SDK_INT >= 28) {
                    switch (cellInfoNr.getCellConnectionStatus()) {
                        case 0:
                            cellConnectionStatus = CellConnectionStatus.None;
                            break;
                        case 1:
                            cellConnectionStatus = CellConnectionStatus.Primary;
                            break;
                        case 2:
                            cellConnectionStatus = CellConnectionStatus.Secondary;
                            break;
                        default:
                            cellConnectionStatus = CellConnectionStatus.Unknown;
                            break;
                    }
                    cellInfo.CellConnectionStatus = cellConnectionStatus;
                }
                if (cellInfoNr instanceof CellInfoGsm) {
                    CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfoNr;
                    cellInfo.IsRegistered = cellInfoGsm.isRegistered();
                    cellInfo.CellNetworkType = CellNetworkTypes.Gsm;
                    cellInfo.CellInfoAge = uptimeMillis - (cellInfoGsm.getTimeStamp() / 1000000);
                    CellIdentityGsm cellIdentity = cellInfoGsm.getCellIdentity();
                    if (cellIdentity.getMcc() != Integer.MAX_VALUE) {
                        cellInfo.Mcc = cellIdentity.getMcc();
                    }
                    if (cellIdentity.getMnc() != Integer.MAX_VALUE) {
                        cellInfo.Mnc = cellIdentity.getMnc();
                    }
                    if (cellIdentity.getCid() != Integer.MAX_VALUE) {
                        cellInfo.Cid = cellIdentity.getCid();
                        cellInfo.CellId = (long) cellInfo.Cid;
                    }
                    if (cellIdentity.getLac() != Integer.MAX_VALUE) {
                        cellInfo.Lac = cellIdentity.getLac();
                    }
                    if (cellIdentity.getPsc() != Integer.MAX_VALUE) {
                        cellInfo.Psc = cellIdentity.getPsc();
                    }
                    if (VERSION.SDK_INT >= 24) {
                        if (cellIdentity.getArfcn() != Integer.MAX_VALUE) {
                            cellInfo.Arfcn = cellIdentity.getArfcn();
                        }
                        if (cellIdentity.getBsic() != Integer.MAX_VALUE) {
                            cellInfo.GsmBsic = cellIdentity.getBsic();
                        }
                    }
                    cellInfo.Dbm = cellInfoGsm.getCellSignalStrength().getDbm();
                } else if (cellInfoNr instanceof CellInfoLte) {
                    CellInfoLte cellInfoLte = (CellInfoLte) cellInfoNr;
                    cellInfo.IsRegistered = cellInfoLte.isRegistered();
                    cellInfo.CellNetworkType = CellNetworkTypes.Lte;
                    cellInfo.CellInfoAge = uptimeMillis - (cellInfoLte.getTimeStamp() / 1000000);
                    CellIdentityLte cellIdentity2 = cellInfoLte.getCellIdentity();
                    if (cellIdentity2.getMcc() != Integer.MAX_VALUE) {
                        cellInfo.Mcc = cellIdentity2.getMcc();
                    }
                    if (cellIdentity2.getMnc() != Integer.MAX_VALUE) {
                        cellInfo.Mnc = cellIdentity2.getMnc();
                    }
                    if (cellIdentity2.getCi() != Integer.MAX_VALUE) {
                        cellInfo.Cid = cellIdentity2.getCi();
                        cellInfo.CellId = (long) cellInfo.Cid;
                    }
                    if (cellIdentity2.getPci() != Integer.MAX_VALUE) {
                        cellInfo.LtePci = cellIdentity2.getPci();
                    }
                    if (cellIdentity2.getTac() != Integer.MAX_VALUE) {
                        cellInfo.LteTac = cellIdentity2.getTac();
                    }
                    if (VERSION.SDK_INT >= 24 && cellIdentity2.getEarfcn() != Integer.MAX_VALUE) {
                        cellInfo.Arfcn = cellIdentity2.getEarfcn();
                        DiskAdCacheManager a2 = com.startapp.networkTest.utils.c.a(cellInfo.Arfcn);
                        if (a2 != null) {
                            cellInfo.LteBand = a2.a;
                            cellInfo.LteUploadEarfcn = a2.c;
                            cellInfo.LteDownloadEarfcn = a2.b;
                            cellInfo.LteUploadFrequency = a2.e;
                            cellInfo.LteDonwloadFrequency = a2.d;
                        }
                    }
                    CellSignalStrengthLte cellSignalStrength = cellInfoLte.getCellSignalStrength();
                    cellInfo.Dbm = cellSignalStrength.getDbm();
                    if (cellSignalStrength.getTimingAdvance() != Integer.MAX_VALUE) {
                        cellInfo.LteTimingAdvance = cellSignalStrength.getTimingAdvance();
                    }
                    if (VERSION.SDK_INT >= 29) {
                        int cqi = cellSignalStrength.getCqi();
                        if (cqi != Integer.MAX_VALUE) {
                            cellInfo.LteCqi = cqi;
                        }
                        cellInfo.LteRssnr = cellSignalStrength.getRssnr();
                        cellInfo.LteRsrq = cellSignalStrength.getRsrq();
                        cellInfo.LteRssi = cellSignalStrength.getRssi();
                    } else {
                        if (this.J != null) {
                            try {
                                int i2 = this.J.getInt(cellSignalStrength);
                                if (i2 != Integer.MAX_VALUE) {
                                    cellInfo.LteCqi = i2;
                                }
                            } catch (IllegalAccessException unused) {
                            }
                        }
                        if (this.H != null) {
                            try {
                                cellInfo.LteRsrq = this.H.getInt(cellSignalStrength);
                            } catch (IllegalAccessException unused2) {
                            }
                        }
                        if (this.I != null) {
                            try {
                                cellInfo.LteRssnr = this.I.getInt(cellSignalStrength);
                            } catch (IllegalAccessException unused3) {
                            }
                        }
                        if (this.G != null) {
                            try {
                                cellInfo.LteRssi = this.G.getInt(cellSignalStrength);
                            } catch (IllegalAccessException unused4) {
                            }
                        }
                    }
                } else if (cellInfoNr instanceof CellInfoWcdma) {
                    CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfoNr;
                    cellInfo.IsRegistered = cellInfoWcdma.isRegistered();
                    cellInfo.CellNetworkType = CellNetworkTypes.Wcdma;
                    cellInfo.CellInfoAge = uptimeMillis - (cellInfoWcdma.getTimeStamp() / 1000000);
                    CellIdentityWcdma cellIdentity3 = cellInfoWcdma.getCellIdentity();
                    if (cellIdentity3.getMcc() != Integer.MAX_VALUE) {
                        cellInfo.Mcc = cellIdentity3.getMcc();
                    }
                    if (cellIdentity3.getMnc() != Integer.MAX_VALUE) {
                        cellInfo.Mnc = cellIdentity3.getMnc();
                    }
                    if (cellIdentity3.getCid() != Integer.MAX_VALUE) {
                        cellInfo.Cid = cellIdentity3.getCid();
                        cellInfo.CellId = (long) cellInfo.Cid;
                    }
                    if (cellIdentity3.getLac() != Integer.MAX_VALUE) {
                        cellInfo.Lac = cellIdentity3.getLac();
                    }
                    if (cellIdentity3.getPsc() != Integer.MAX_VALUE) {
                        cellInfo.Psc = cellIdentity3.getPsc();
                    }
                    if (VERSION.SDK_INT >= 24 && cellIdentity3.getUarfcn() != Integer.MAX_VALUE) {
                        cellInfo.Arfcn = cellIdentity3.getUarfcn();
                    }
                    cellInfo.Dbm = cellInfoWcdma.getCellSignalStrength().getDbm();
                } else if (cellInfoNr instanceof CellInfoCdma) {
                    CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfoNr;
                    cellInfo.IsRegistered = cellInfoCdma.isRegistered();
                    cellInfo.CellNetworkType = CellNetworkTypes.Gsm;
                    cellInfo.CellInfoAge = uptimeMillis - (cellInfoCdma.getTimeStamp() / 1000000);
                    CellIdentityCdma cellIdentity4 = cellInfoCdma.getCellIdentity();
                    cellInfo.CdmaBaseStationLatitude = cellIdentity4.getLatitude();
                    cellInfo.CdmaBaseStationLongitude = cellIdentity4.getLongitude();
                    if (cellIdentity4.getSystemId() != Integer.MAX_VALUE) {
                        cellInfo.CdmaSystemId = cellIdentity4.getSystemId();
                    }
                    if (cellIdentity4.getNetworkId() != Integer.MAX_VALUE) {
                        cellInfo.CdmaNetworkId = cellIdentity4.getNetworkId();
                    }
                    if (cellIdentity4.getBasestationId() != Integer.MAX_VALUE) {
                        cellInfo.CdmaBaseStationId = cellIdentity4.getBasestationId();
                    }
                    CellSignalStrengthCdma cellSignalStrength2 = cellInfoCdma.getCellSignalStrength();
                    cellInfo.Dbm = cellSignalStrength2.getDbm();
                    cellInfo.CdmaDbm = cellSignalStrength2.getCdmaDbm();
                    cellInfo.CdmaEcio = cellSignalStrength2.getCdmaEcio();
                    cellInfo.EvdoDbm = cellSignalStrength2.getEvdoDbm();
                    cellInfo.EvdoEcio = cellSignalStrength2.getEvdoEcio();
                    cellInfo.EvdoSnr = cellSignalStrength2.getEvdoSnr();
                } else if (VERSION.SDK_INT >= 29 && (cellInfoNr instanceof CellInfoNr)) {
                    try {
                        CellInfoNr cellInfoNr2 = cellInfoNr;
                        cellInfo.IsRegistered = cellInfoNr2.isRegistered();
                        cellInfo.CellNetworkType = CellNetworkTypes.Nr;
                        cellInfo.CellInfoAge = uptimeMillis - (cellInfoNr2.getTimeStamp() / 1000000);
                        CellIdentityNr cellIdentity5 = cellInfoNr2.getCellIdentity();
                        if (cellIdentity5 instanceof CellIdentityNr) {
                            CellIdentityNr cellIdentityNr = cellIdentity5;
                            cellInfo.Arfcn = cellIdentityNr.getNrarfcn();
                            cellInfo.LtePci = cellIdentityNr.getPci();
                            cellInfo.LteTac = cellIdentityNr.getTac();
                            cellInfo.CellId = cellIdentityNr.getNci();
                            if (cellIdentityNr.getMccString() != null) {
                                try {
                                    cellInfo.Mcc = Integer.parseInt(cellIdentityNr.getMccString());
                                } catch (NumberFormatException e2) {
                                    new StringBuilder("cellIdentityNr.getMccString: ").append(e2.getMessage());
                                }
                            }
                            if (cellIdentityNr.getMncString() != null) {
                                try {
                                    cellInfo.Mnc = Integer.parseInt(cellIdentityNr.getMncString());
                                } catch (NumberFormatException e3) {
                                    new StringBuilder("cellIdentityNr.getMncString: ").append(e3.getMessage());
                                }
                            }
                        }
                        CellSignalStrengthNr cellSignalStrength3 = cellInfoNr2.getCellSignalStrength();
                        if (cellSignalStrength3 instanceof CellSignalStrengthNr) {
                            CellSignalStrengthNr cellSignalStrengthNr = cellSignalStrength3;
                            cellInfo.Dbm = cellSignalStrengthNr.getDbm();
                            cellInfo.NrCsiRsrp = cellSignalStrengthNr.getCsiRsrp();
                            cellInfo.NrCsiRsrq = cellSignalStrengthNr.getCsiRsrq();
                            cellInfo.NrCsiSinr = cellSignalStrengthNr.getCsiSinr();
                            cellInfo.NrSsRsrp = cellSignalStrengthNr.getSsRsrp();
                            cellInfo.NrSsRsrq = cellSignalStrengthNr.getSsRsrq();
                            cellInfo.NrSsSinr = cellSignalStrengthNr.getSsSinr();
                        }
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                }
                arrayList.add(cellInfo);
            }
        }
        return (com.startapp.networkTest.data.radio.CellInfo[]) arrayList.toArray(new com.startapp.networkTest.data.radio.CellInfo[arrayList.size()]);
    }

    @TargetApi(21)
    public final ApnInfo[] e() {
        ArrayList arrayList = new ArrayList();
        for (a aVar : n()) {
            ApnInfo apnInfo = new ApnInfo();
            apnInfo.Apn = aVar.b;
            apnInfo.TxBytes = aVar.g;
            apnInfo.RxBytes = aVar.h;
            apnInfo.ApnTypes = aVar.c;
            apnInfo.Capabilities = aVar.d;
            apnInfo.SubscriptionId = aVar.f;
            apnInfo.PcscfAddresses = aVar.e;
            apnInfo.MobileDataConnectionState = aVar.j;
            apnInfo.NetworkType = b(aVar.a);
            apnInfo.Reason = this.i.a(aVar.f, aVar.c);
            if (apnInfo.ApnTypes.contains("ims")) {
                g e2 = this.i.e(aVar.f);
                if (e2 != null) {
                    apnInfo.SamsungSipError = e2.a;
                    apnInfo.SamsungImsServices = e2.b;
                }
            }
            arrayList.add(apnInfo);
        }
        return (ApnInfo[]) arrayList.toArray(new ApnInfo[arrayList.size()]);
    }

    private ThreeStateShort l() {
        if (VERSION.SDK_INT < 16 || this.e.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            return ThreeStateShort.Unknown;
        }
        return this.h.isActiveNetworkMetered() ? ThreeStateShort.Yes : ThreeStateShort.No;
    }

    public final ConnectionTypes f() {
        ConnectionTypes connectionTypes = ConnectionTypes.Unknown;
        if (this.h == null || this.e.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            return connectionTypes;
        }
        NetworkInfo activeNetworkInfo = this.h.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return connectionTypes;
        }
        switch (activeNetworkInfo.getType()) {
            case 0:
                return ConnectionTypes.Mobile;
            case 1:
                return ConnectionTypes.WiFi;
            case 6:
                return ConnectionTypes.WiMAX;
            case 7:
                return ConnectionTypes.Bluetooth;
            case 9:
                return ConnectionTypes.Ethernet;
            default:
                return connectionTypes;
        }
    }

    public static NetworkTypes b(int i2) {
        switch (i2) {
            case 1:
                return NetworkTypes.GPRS;
            case 2:
                return NetworkTypes.EDGE;
            case 3:
                return NetworkTypes.UMTS;
            case 4:
                return NetworkTypes.CDMA;
            case 5:
                return NetworkTypes.EVDO_0;
            case 6:
                return NetworkTypes.EVDO_A;
            case 7:
                return NetworkTypes.Cdma1xRTT;
            case 8:
                return NetworkTypes.HSDPA;
            case 9:
                return NetworkTypes.HSUPA;
            case 10:
                return NetworkTypes.HSPA;
            case 11:
                return NetworkTypes.IDEN;
            case 12:
                return NetworkTypes.EVDO_B;
            case 13:
                return NetworkTypes.LTE;
            case 14:
                return NetworkTypes.EHRPD;
            case 15:
                return NetworkTypes.HSPAP;
            case 16:
                return NetworkTypes.GSM;
            case 17:
                return NetworkTypes.TD_SCDMA;
            case 18:
                return NetworkTypes.WiFi;
            case 19:
                return NetworkTypes.LTE_CA;
            case 20:
                return NetworkTypes.NR;
            default:
                return NetworkTypes.Unknown;
        }
    }

    @TargetApi(17)
    private boolean m() {
        return VERSION.SDK_INT < 17 ? System.getInt(this.R, "airplane_mode_on", 0) != 0 : Global.getInt(this.R, "airplane_mode_on", 0) != 0;
    }

    public final boolean c(int i2) {
        if (this.M != null) {
            try {
                return ((Boolean) this.M.invoke(this.d, new Object[]{Integer.valueOf(i2)})).booleanValue();
            } catch (Exception e2) {
                String str = c;
                StringBuilder sb = new StringBuilder("isRoaming: ");
                sb.append(e2.toString());
                Log.e(str, sb.toString());
            }
        }
        return this.d.isNetworkRoaming();
    }

    public static NetworkGenerations a(NetworkTypes networkTypes) {
        switch (networkTypes) {
            case GPRS:
            case EDGE:
            case GSM:
            case Cdma1xRTT:
            case CDMA:
            case IDEN:
                return NetworkGenerations.Gen2;
            case UMTS:
            case EVDO_0:
            case EVDO_A:
            case EVDO_B:
            case HSPA:
            case HSDPA:
            case HSPAP:
            case HSUPA:
            case EHRPD:
            case TD_SCDMA:
                return NetworkGenerations.Gen3;
            case LTE:
            case LTE_CA:
                return NetworkGenerations.Gen4;
            case NR:
                return NetworkGenerations.Gen5;
            default:
                return NetworkGenerations.Unknown;
        }
    }

    public final NetworkTypes g() {
        if (this.v != null) {
            if (this.e.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
                try {
                    return b(((Integer) this.v.invoke(this.d, new Object[0])).intValue());
                } catch (Exception e2) {
                    String str = c;
                    StringBuilder sb = new StringBuilder("getVoiceNetworkType: ");
                    sb.append(e2.toString());
                    Log.e(str, sb.toString());
                }
            }
        }
        return NetworkTypes.Unknown;
    }

    private List<a> n() {
        a aVar;
        ArrayList arrayList = new ArrayList();
        if (this.h != null && VERSION.SDK_INT >= 21 && this.e.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
            Network[] allNetworks = this.h.getAllNetworks();
            if (allNetworks != null && allNetworks.length > 0) {
                for (Network network : allNetworks) {
                    NetworkCapabilities networkCapabilities = this.h.getNetworkCapabilities(network);
                    if (networkCapabilities != null && networkCapabilities.hasTransport(0)) {
                        a aVar2 = new a(this, 0);
                        NetworkInfo networkInfo = this.h.getNetworkInfo(network);
                        LinkProperties linkProperties = this.h.getLinkProperties(network);
                        ArrayList arrayList2 = new ArrayList();
                        if (networkCapabilities.hasCapability(4)) {
                            arrayList2.add("ims");
                        }
                        if (networkCapabilities.hasCapability(1)) {
                            arrayList2.add("supl");
                        }
                        if (networkCapabilities.hasCapability(9)) {
                            arrayList2.add("xcap");
                        }
                        if (networkCapabilities.hasCapability(2)) {
                            arrayList2.add("dun");
                        }
                        if (networkCapabilities.hasCapability(5)) {
                            arrayList2.add("cbs");
                        }
                        if (networkCapabilities.hasCapability(3)) {
                            arrayList2.add("fota");
                        }
                        if (networkCapabilities.hasCapability(10)) {
                            arrayList2.add("emergency");
                        }
                        if (networkCapabilities.hasCapability(7)) {
                            arrayList2.add("ia");
                        }
                        if (networkCapabilities.hasCapability(0)) {
                            arrayList2.add("mms");
                        }
                        if (networkCapabilities.hasCapability(8)) {
                            arrayList2.add("rcs");
                        }
                        if (networkCapabilities.hasCapability(23)) {
                            arrayList2.add("mcx");
                        }
                        aVar2.c = TextUtils.join(",", arrayList2);
                        if (networkInfo != null) {
                            aVar2.b = networkInfo.getExtraInfo();
                            aVar2.a = networkInfo.getSubtype();
                            aVar2.j = WifiDetailedStates.a(networkInfo.getDetailedState());
                        }
                        try {
                            aVar = e.a(networkCapabilities, linkProperties, aVar2);
                            try {
                                String interfaceName = linkProperties.getInterfaceName();
                                aVar.g = com.startapp.networkTest.utils.h.a(interfaceName);
                                aVar.h = com.startapp.networkTest.utils.h.b(interfaceName);
                                aVar.i = interfaceName;
                            } catch (Exception unused) {
                            }
                        } catch (Exception unused2) {
                            aVar = aVar2;
                        }
                        arrayList.add(aVar);
                    }
                }
            }
        }
        return arrayList;
    }

    private static PreferredNetworkTypes a(Context context, int i2) {
        PreferredNetworkTypes preferredNetworkTypes = PreferredNetworkTypes.Unknown;
        if (VERSION.SDK_INT >= 17) {
            try {
                return f(Global.getInt(context.getContentResolver(), "preferred_network_mode".concat(String.valueOf(i2))));
            } catch (Exception unused) {
            }
        }
        return preferredNetworkTypes;
    }

    private static SparseArray<PreferredNetworkTypes> a(Context context) {
        SparseArray<PreferredNetworkTypes> sparseArray = new SparseArray<>();
        if (VERSION.SDK_INT >= 17) {
            try {
                String[] split = Global.getString(context.getContentResolver(), "preferred_network_mode").split(",");
                for (int i2 = 0; i2 < split.length; i2++) {
                    sparseArray.put(i2, f(Integer.valueOf(split[i2]).intValue()));
                }
            } catch (Exception unused) {
            }
        }
        return sparseArray;
    }

    private static PreferredNetworkTypes f(int i2) {
        switch (i2) {
            case 0:
                return PreferredNetworkTypes.WCDMA_PREF;
            case 1:
                return PreferredNetworkTypes.GSM_ONLY;
            case 2:
                return PreferredNetworkTypes.WCDMA_ONLY;
            case 3:
                return PreferredNetworkTypes.GSM_UMTS;
            case 4:
                return PreferredNetworkTypes.CDMA;
            case 5:
                return PreferredNetworkTypes.CDMA_NO_EVDO;
            case 6:
                return PreferredNetworkTypes.EVDO_NO_CDMA;
            case 7:
                return PreferredNetworkTypes.GLOBAL;
            case 8:
                return PreferredNetworkTypes.LTE_CDMA_EVDO;
            case 9:
                return PreferredNetworkTypes.LTE_GSM_WCDMA;
            case 10:
                return PreferredNetworkTypes.LTE_CDMA_EVDO_GSM_WCDMA;
            case 11:
                return PreferredNetworkTypes.LTE_ONLY;
            case 12:
                return PreferredNetworkTypes.LTE_WCDMA;
            case 13:
                return PreferredNetworkTypes.TDSCDMA_ONLY;
            case 14:
                return PreferredNetworkTypes.TDSCDMA_WCDMA;
            case 15:
                return PreferredNetworkTypes.LTE_TDSCDMA;
            case 16:
                return PreferredNetworkTypes.TDSCDMA_GSM;
            case 17:
                return PreferredNetworkTypes.LTE_TDSCDMA_GSM;
            case 18:
                return PreferredNetworkTypes.TDSCDMA_GSM_WCDMA;
            case 19:
                return PreferredNetworkTypes.LTE_TDSCDMA_WCDMA;
            case 20:
                return PreferredNetworkTypes.LTE_TDSCDMA_GSM_WCDMA;
            case 21:
                return PreferredNetworkTypes.TDSCDMA_CDMA_EVDO_GSM_WCDMA;
            case 22:
                return PreferredNetworkTypes.LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA;
            case 23:
                return PreferredNetworkTypes.NR_ONLY;
            case 24:
                return PreferredNetworkTypes.NR_LTE;
            case 25:
                return PreferredNetworkTypes.NR_LTE_CDMA_EVDO;
            case 26:
                return PreferredNetworkTypes.NR_LTE_GSM_WCDMA;
            case 27:
                return PreferredNetworkTypes.NR_LTE_CDMA_EVDO_GSM_WCDMA;
            case 28:
                return PreferredNetworkTypes.NR_LTE_WCDMA;
            case 29:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA;
            case 30:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA_GSM;
            case 31:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA_WCDMA;
            case 32:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA_GSM_WCDMA;
            case 33:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA;
            default:
                return PreferredNetworkTypes.Unknown;
        }
    }

    public final void a(final com.startapp.networkTest.controller.a.a aVar) {
        if (aVar != null) {
            if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
                this.a.post(new Runnable() {
                    public final void run() {
                        c.this.a(aVar);
                    }
                });
            } else if (!this.b.contains(aVar)) {
                this.b.add(aVar);
            }
        }
    }

    public final void b(final com.startapp.networkTest.controller.a.a aVar) {
        if (aVar != null) {
            if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
                this.b.remove(aVar);
                return;
            }
            this.a.post(new Runnable() {
                public final void run() {
                    c.this.b(aVar);
                }
            });
        }
    }

    public final com.startapp.networkTest.data.a.a h() {
        return this.k;
    }

    private static String a(NetworkRegistrationInfo[] networkRegistrationInfoArr) {
        if (networkRegistrationInfoArr != null) {
            for (NetworkRegistrationInfo networkRegistrationInfo : networkRegistrationInfoArr) {
                if (networkRegistrationInfo.Domain.equals("PS") && networkRegistrationInfo.TransportType.equals("WWAN")) {
                    return networkRegistrationInfo.NrState;
                }
            }
        }
        return "Unknown";
    }

    private static ThreeStateShort b(NetworkRegistrationInfo[] networkRegistrationInfoArr) {
        if (networkRegistrationInfoArr != null) {
            for (NetworkRegistrationInfo networkRegistrationInfo : networkRegistrationInfoArr) {
                if (networkRegistrationInfo.Domain.equals("PS") && networkRegistrationInfo.TransportType.equals("WWAN")) {
                    return networkRegistrationInfo.NrAvailable;
                }
            }
        }
        return ThreeStateShort.Unknown;
    }

    /* JADX WARNING: Removed duplicated region for block: B:138:0x03f9  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0443  */
    @TargetApi(22)
    private RadioInfo e(int i2) {
        String str;
        long elapsedRealtime;
        int i3;
        int i4;
        f a2;
        if (i2 != -1) {
            if ((this.k.a(i2).SubscriptionId != -1) && VERSION.SDK_INT >= 22 && this.S.length != 0) {
                RadioInfo radioInfo = new RadioInfo();
                radioInfo.SubscriptionId = i2;
                radioInfo.IsDefaultVoiceSim = i2 == this.k.DefaultVoiceSimId;
                radioInfo.IsDefaultDataSim = i2 == this.k.DefaultDataSimId;
                radioInfo.PreferredNetwork = a(this.e, i2);
                if (radioInfo.PreferredNetwork == PreferredNetworkTypes.Unknown) {
                    PreferredNetworkTypes preferredNetworkTypes = (PreferredNetworkTypes) a(this.e).get(this.k.a(i2).SimSlotIndex);
                    if (preferredNetworkTypes != null) {
                        radioInfo.PreferredNetwork = preferredNetworkTypes;
                    }
                }
                if (this.d != null) {
                    try {
                        switch (this.d.getDataState()) {
                            case 0:
                                radioInfo.MobileDataConnectionState = DataConnectionStates.Disconnected;
                                break;
                            case 1:
                                radioInfo.MobileDataConnectionState = DataConnectionStates.Connecting;
                                break;
                            case 2:
                                radioInfo.MobileDataConnectionState = DataConnectionStates.Connected;
                                break;
                            case 3:
                                radioInfo.MobileDataConnectionState = DataConnectionStates.Suspended;
                                break;
                            default:
                                radioInfo.MobileDataConnectionState = DataConnectionStates.Unknown;
                                break;
                        }
                    } catch (SecurityException e2) {
                        String str2 = c;
                        StringBuilder sb = new StringBuilder("getRadioInfo(subscriptionID): getDataState: ");
                        sb.append(e2.toString());
                        Log.e(str2, sb.toString());
                    }
                    radioInfo.FlightMode = m() ? FlightModeStates.Enabled : FlightModeStates.Disabled;
                    radioInfo.MobileDataEnabled = ThreeState.Unknown;
                    if (this.L != null) {
                        try {
                            radioInfo.MobileDataEnabled = ((Boolean) this.L.invoke(this.d, new Object[]{Integer.valueOf(i2)})).booleanValue() ? ThreeState.Enabled : ThreeState.Disabled;
                        } catch (Exception e3) {
                            String str3 = c;
                            StringBuilder sb2 = new StringBuilder("getRadioInfo(subscriptionID): MobileDataEnabled: ");
                            sb2.append(e3.toString());
                            Log.e(str3, sb2.toString());
                        }
                    }
                    if (this.M != null) {
                        try {
                            radioInfo.IsRoaming = ((Boolean) this.M.invoke(this.d, new Object[]{Integer.valueOf(i2)})).booleanValue();
                        } catch (Exception e4) {
                            String str4 = c;
                            StringBuilder sb3 = new StringBuilder("getRadioInfo(subscriptionID): IsRoaming: ");
                            sb3.append(e4.toString());
                            Log.e(str4, sb3.toString());
                        }
                    }
                    radioInfo.IsMetered = l();
                    radioInfo.ConnectionType = f();
                    if (this.N != null) {
                        try {
                            radioInfo.NetworkType = b(((Integer) this.N.invoke(this.d, new Object[]{Integer.valueOf(i2)})).intValue());
                        } catch (Exception e5) {
                            String str5 = c;
                            StringBuilder sb4 = new StringBuilder("getRadioInfo(subscriptionID): NetworkType: ");
                            sb4.append(e5.toString());
                            Log.e(str5, sb4.toString());
                        }
                    }
                    if (this.O != null) {
                        try {
                            radioInfo.OperatorName = com.startapp.networkTest.utils.g.a((String) this.O.invoke(this.d, new Object[]{Integer.valueOf(i2)}));
                        } catch (Exception e6) {
                            String str6 = c;
                            StringBuilder sb5 = new StringBuilder("getRadioInfo(subscriptionID): OperatorName: ");
                            sb5.append(e6.toString());
                            Log.e(str6, sb5.toString());
                        }
                    }
                    String str7 = "";
                    if (this.Q != null) {
                        try {
                            str = (String) this.Q.invoke(this.d, new Object[]{Integer.valueOf(i2)});
                        } catch (Exception e7) {
                            String str8 = c;
                            StringBuilder sb6 = new StringBuilder("getRadioInfo(subscriptionID): OperatorName: ");
                            sb6.append(e7.toString());
                            Log.e(str8, sb6.toString());
                        }
                    } else {
                        if (this.P != null) {
                            try {
                                str = (String) this.P.invoke(this.d, new Object[]{Integer.valueOf(i2)});
                            } catch (Exception e8) {
                                String str9 = c;
                                StringBuilder sb7 = new StringBuilder("getRadioInfo(subscriptionID): OperatorName: ");
                                sb7.append(e8.toString());
                                Log.e(str9, sb7.toString());
                            }
                        }
                        str = str7;
                    }
                    if (str.length() > 4) {
                        radioInfo.MCC = str.substring(0, 3);
                        radioInfo.MNC = str.substring(3);
                    }
                    b c2 = this.i.c(i2);
                    if (c2 == null) {
                        c2 = new b(this, 0);
                    }
                    if (this.e.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
                        radioInfo.MissingPermission = true;
                    } else if (c2.a == null) {
                        c2.a = this.d.getCellLocation();
                    }
                    int i5 = Integer.MAX_VALUE;
                    if (c2.a != null) {
                        if (c2.a.getClass().equals(GsmCellLocation.class)) {
                            GsmCellLocation gsmCellLocation = (GsmCellLocation) c2.a;
                            StringBuilder sb8 = new StringBuilder();
                            sb8.append(gsmCellLocation.getLac());
                            radioInfo.GsmLAC = sb8.toString();
                            StringBuilder sb9 = new StringBuilder();
                            sb9.append(gsmCellLocation.getCid());
                            radioInfo.GsmCellId = sb9.toString();
                            StringBuilder sb10 = new StringBuilder();
                            sb10.append(gsmCellLocation.getPsc());
                            radioInfo.PrimaryScramblingCode = sb10.toString();
                        } else if (c2.a.getClass().equals(CdmaCellLocation.class)) {
                            CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) c2.a;
                            StringBuilder sb11 = new StringBuilder();
                            sb11.append(cdmaCellLocation.getBaseStationId());
                            radioInfo.CdmaBaseStationId = sb11.toString();
                            StringBuilder sb12 = new StringBuilder();
                            sb12.append(cdmaCellLocation.getBaseStationLatitude());
                            radioInfo.CdmaBaseStationLatitude = sb12.toString();
                            StringBuilder sb13 = new StringBuilder();
                            sb13.append(cdmaCellLocation.getBaseStationLongitude());
                            radioInfo.CdmaBaseStationLongitude = sb13.toString();
                            StringBuilder sb14 = new StringBuilder();
                            sb14.append(cdmaCellLocation.getNetworkId());
                            radioInfo.CdmaNetworkId = sb14.toString();
                            StringBuilder sb15 = new StringBuilder();
                            sb15.append(cdmaCellLocation.getSystemId());
                            radioInfo.CdmaSystemId = sb15.toString();
                        }
                        if (c2.b > 0) {
                            long elapsedRealtime2 = SystemClock.elapsedRealtime() - c2.b;
                            radioInfo.GsmCellIdAge = elapsedRealtime2 > 2147483647L ? Integer.MAX_VALUE : (int) elapsedRealtime2;
                        }
                    }
                    h b2 = this.i.b(i2);
                    radioInfo.ServiceState = b2.a;
                    radioInfo.DuplexMode = b2.c;
                    radioInfo.ManualSelection = b2.d;
                    radioInfo.CarrierAggregation = b2.f;
                    radioInfo.ARFCN = b2.e;
                    if (b2.b > 0) {
                        long elapsedRealtime3 = SystemClock.elapsedRealtime() - b2.b;
                        radioInfo.ServiceStateAge = elapsedRealtime3 > 2147483647L ? Integer.MAX_VALUE : (int) elapsedRealtime3;
                    }
                    NetworkRegistrationInfo[] d2 = this.i.d(i2);
                    radioInfo.NrState = a(d2);
                    radioInfo.NrAvailable = b(d2);
                    i a3 = this.i.a(i2);
                    radioInfo.RSCP = a3.j;
                    radioInfo.CdmaEcIo = a3.c;
                    radioInfo.RXLevel = a3.a;
                    radioInfo.NativeDbm = a3.b;
                    radioInfo.EcN0 = a3.i;
                    radioInfo.LteCqi = a3.d;
                    radioInfo.LteRsrp = a3.e;
                    radioInfo.LteRsrq = a3.g;
                    radioInfo.LteRssnr = a3.f;
                    radioInfo.LteRssi = a3.h;
                    radioInfo.NrCsiRsrp = a3.l;
                    radioInfo.NrCsiRsrq = a3.m;
                    radioInfo.NrCsiSinr = a3.n;
                    radioInfo.NrSsRsrp = a3.o;
                    radioInfo.NrSsRsrq = a3.p;
                    radioInfo.NrSsSinr = a3.q;
                    if ((radioInfo.NetworkType == NetworkTypes.LTE || radioInfo.NetworkType == NetworkTypes.LTE_CA) && radioInfo.RXLevel >= 0) {
                        radioInfo.RXLevel = radioInfo.LteRsrp;
                    }
                    if (radioInfo.NetworkType == NetworkTypes.LTE_CA) {
                        radioInfo.CarrierAggregation = ThreeStateShort.Yes;
                    }
                    if (radioInfo.NetworkType == NetworkTypes.NR) {
                        try {
                            i4 = Integer.parseInt(radioInfo.MCC);
                            try {
                                i3 = Integer.parseInt(radioInfo.MNC);
                            } catch (NumberFormatException e9) {
                                e = e9;
                                new StringBuilder("radioInfo: ").append(e.getMessage());
                                i3 = 0;
                                StringBuilder sb16 = new StringBuilder();
                                sb16.append(i4);
                                sb16.append(i3);
                                a2 = this.i.a(sb16.toString());
                                if (a2 != null) {
                                }
                                elapsedRealtime = SystemClock.elapsedRealtime() - a3.k;
                                if (elapsedRealtime <= 2147483647L) {
                                }
                                radioInfo.RXLevelAge = i5;
                                return radioInfo;
                            }
                        } catch (NumberFormatException e10) {
                            e = e10;
                            i4 = 0;
                            new StringBuilder("radioInfo: ").append(e.getMessage());
                            i3 = 0;
                            StringBuilder sb162 = new StringBuilder();
                            sb162.append(i4);
                            sb162.append(i3);
                            a2 = this.i.a(sb162.toString());
                            if (a2 != null) {
                            }
                            elapsedRealtime = SystemClock.elapsedRealtime() - a3.k;
                            if (elapsedRealtime <= 2147483647L) {
                            }
                            radioInfo.RXLevelAge = i5;
                            return radioInfo;
                        }
                        StringBuilder sb1622 = new StringBuilder();
                        sb1622.append(i4);
                        sb1622.append(i3);
                        a2 = this.i.a(sb1622.toString());
                        if (a2 != null) {
                            StringBuilder sb17 = new StringBuilder();
                            sb17.append(a2.a);
                            radioInfo.GsmCellId = sb17.toString();
                            StringBuilder sb18 = new StringBuilder();
                            sb18.append(a2.b);
                            radioInfo.GsmLAC = sb18.toString();
                            StringBuilder sb19 = new StringBuilder();
                            sb19.append(a2.c);
                            radioInfo.PrimaryScramblingCode = sb19.toString();
                            radioInfo.GsmCellIdAge = (int) (SystemClock.elapsedRealtime() - (a2.d / 1000000));
                        }
                    }
                    elapsedRealtime = SystemClock.elapsedRealtime() - a3.k;
                    if (elapsedRealtime <= 2147483647L) {
                        i5 = (int) elapsedRealtime;
                    }
                    radioInfo.RXLevelAge = i5;
                }
                return radioInfo;
            }
        }
        return k();
    }
}
