package com.startapp.networkTest.controller;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.PowerManager;
import android.os.StatFs;
import android.os.SystemClock;
import android.provider.MediaStore.Audio.Media;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import android.provider.Settings.System;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.facebook.places.model.PlaceFields;
import com.startapp.networkTest.c;
import com.startapp.networkTest.data.a;
import com.startapp.networkTest.data.e;
import com.startapp.networkTest.enums.IdleStates;
import com.startapp.networkTest.enums.MemoryStates;
import com.startapp.networkTest.enums.MultiSimVariants;
import com.startapp.networkTest.enums.Os;
import com.startapp.networkTest.enums.PhoneTypes;
import com.startapp.networkTest.enums.ScreenStates;
import com.startapp.networkTest.enums.SimStates;
import com.startapp.networkTest.enums.ThreeState;
import com.startapp.networkTest.utils.f;
import com.startapp.networkTest.utils.g;
import com.startapp.networkTest.utils.h;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/* compiled from: StartAppSDK */
public class b {
    private static final String a = "b";

    public static a a(Context context) {
        String[] strArr;
        SimStates simStates;
        PhoneTypes phoneTypes;
        a aVar = new a();
        aVar.DeviceManufacturer = Build.MANUFACTURER;
        aVar.DeviceName = Build.MODEL;
        aVar.OS = Os.Android;
        aVar.OSVersion = VERSION.RELEASE;
        aVar.BuildFingerprint = Build.FINGERPRINT;
        aVar.DeviceUpTime = SystemClock.elapsedRealtime();
        aVar.UserLocal = Locale.getDefault().toString();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        if (telephonyManager != null) {
            aVar.SimOperator = g.a(telephonyManager.getSimOperator());
            aVar.SimOperatorName = g.a(telephonyManager.getSimOperatorName());
            if (VERSION.SDK_INT >= 29) {
                String manufacturerCode = telephonyManager.getManufacturerCode();
                if (manufacturerCode != null && !manufacturerCode.isEmpty()) {
                    aVar.TAC = manufacturerCode;
                }
            }
            SimStates simStates2 = SimStates.Unknown;
            switch (telephonyManager.getSimState()) {
                case 1:
                    simStates = SimStates.Absent;
                    break;
                case 2:
                    simStates = SimStates.PinRequired;
                    break;
                case 3:
                    simStates = SimStates.PukRequired;
                    break;
                case 4:
                    simStates = SimStates.NetworkLocked;
                    break;
                case 5:
                    simStates = SimStates.Ready;
                    break;
                default:
                    simStates = SimStates.Unknown;
                    break;
            }
            aVar.SimState = simStates;
            if (VERSION.SDK_INT >= 23) {
                try {
                    aVar.PhoneCount = ((Integer) telephonyManager.getClass().getDeclaredMethod("getPhoneCount", new Class[0]).invoke(telephonyManager, new Object[0])).intValue();
                } catch (Exception e) {
                    String str = a;
                    StringBuilder sb = new StringBuilder("getPhoneCount: ");
                    sb.append(e.getMessage());
                    Log.e(str, sb.toString());
                }
            }
            PhoneTypes phoneTypes2 = PhoneTypes.Unknown;
            switch (telephonyManager.getPhoneType()) {
                case 0:
                    phoneTypes = PhoneTypes.None;
                    break;
                case 1:
                    phoneTypes = PhoneTypes.GSM;
                    break;
                case 2:
                    phoneTypes = PhoneTypes.CDMA;
                    break;
                case 3:
                    phoneTypes = PhoneTypes.SIP;
                    break;
                default:
                    phoneTypes = PhoneTypes.Unknown;
                    break;
            }
            aVar.PhoneType = phoneTypes;
        }
        aVar.IsRooted = a();
        if (VERSION.SDK_INT <= 24) {
            strArr = f.a("/proc/version");
        } else {
            strArr = f.b("uname -a");
        }
        if (strArr.length > 0) {
            aVar.OsSystemVersion = g.a(strArr[0]);
        }
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo == null) {
            try {
                applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            } catch (NameNotFoundException unused) {
            }
        }
        if (applicationInfo != null) {
            aVar.AppTargetVersion = applicationInfo.targetSdkVersion;
        }
        com.startapp.sdk.adsbase.e.a aVar2 = new com.startapp.sdk.adsbase.e.a();
        aVar2.a = true;
        aVar.BluetoothInfo$3e5b9058 = aVar2;
        aVar.PowerSaveMode = h(context);
        aVar.MultiSimInfo = g(context);
        return aVar;
    }

    private static ThreeState h(Context context) {
        try {
            String string = System.getString(context.getContentResolver(), "user_powersaver_enable");
            if (string != null) {
                return string.equals("1") ? ThreeState.Enabled : ThreeState.Disabled;
            }
            if (VERSION.SDK_INT >= 21) {
                if (Build.MANUFACTURER.toLowerCase().startsWith("sony") && VERSION.SDK_INT < 23) {
                    return ThreeState.Unknown;
                }
                PowerManager powerManager = (PowerManager) context.getSystemService("power");
                if (powerManager != null) {
                    return powerManager.isPowerSaveMode() ? ThreeState.Enabled : ThreeState.Disabled;
                }
            }
            return ThreeState.Unknown;
        } catch (Exception e) {
            String str = a;
            StringBuilder sb = new StringBuilder("getPowerSaveMode: ");
            sb.append(e.getMessage());
            Log.e(str, sb.toString());
        }
    }

    private static boolean a() {
        String[] strArr = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (int i = 0; i < 10; i++) {
            if (new File(strArr[i]).exists()) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint({"NewApi"})
    public static com.startapp.networkTest.data.b b(Context context) {
        MemoryInfo memoryInfo = new MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        com.startapp.networkTest.data.b bVar = new com.startapp.networkTest.data.b();
        bVar.MemoryFree = memoryInfo.availMem;
        if (VERSION.SDK_INT >= 16) {
            bVar.MemoryTotal = memoryInfo.totalMem;
            bVar.MemoryUsed = memoryInfo.totalMem - memoryInfo.availMem;
        }
        if (memoryInfo.lowMemory) {
            bVar.MemoryState = MemoryStates.Low;
        } else {
            bVar.MemoryState = MemoryStates.Normal;
        }
        return bVar;
    }

    public static e c(Context context) {
        e eVar = new e();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        long blockSize = (long) statFs.getBlockSize();
        eVar.StorageInternalSize = ((long) statFs.getBlockCount()) * blockSize;
        eVar.StorageInternalAvailable = blockSize * ((long) statFs.getAvailableBlocks());
        eVar.StorageInternalAudio = a(context, Media.INTERNAL_CONTENT_URI);
        eVar.StorageInternalImages = a(context, Images.Media.INTERNAL_CONTENT_URI);
        eVar.StorageInternalVideo = a(context, Video.Media.INTERNAL_CONTENT_URI);
        if (b()) {
            try {
                StatFs statFs2 = new StatFs(Environment.getExternalStorageDirectory().getPath());
                long blockSize2 = (long) statFs2.getBlockSize();
                eVar.StorageExternalSize = ((long) statFs2.getBlockCount()) * blockSize2;
                eVar.StorageExternalAvailable = blockSize2 * ((long) statFs2.getAvailableBlocks());
            } catch (IllegalArgumentException unused) {
                eVar.StorageExternalSize = -1;
                eVar.StorageExternalAvailable = -1;
            }
            if (context.checkCallingOrSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == 0) {
                eVar.StorageExternalAudio = a(context, Media.EXTERNAL_CONTENT_URI);
                eVar.StorageExternalImages = a(context, Images.Media.EXTERNAL_CONTENT_URI);
                eVar.StorageExternalVideo = a(context, Video.Media.EXTERNAL_CONTENT_URI);
            }
        }
        return eVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0054  */
    private static long a(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            Cursor query = context.getContentResolver().query(uri, new String[]{"_size"}, null, null, null);
            long j = 0;
            if (query != null) {
                try {
                    if (query.getCount() == 0) {
                        if (query != null) {
                            query.close();
                        }
                        return 0;
                    }
                    while (query.moveToNext()) {
                        j += query.getLong(query.getColumnIndexOrThrow("_size"));
                    }
                } catch (Exception e) {
                    e = e;
                    cursor = query;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                            cursor.close();
                        }
                        return -1;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
            return j;
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
            }
            return -1;
        }
    }

    private static boolean b() {
        try {
            return Environment.getExternalStorageState().equals("mounted");
        } catch (Exception e) {
            new StringBuilder("isExternalMemoryAvailable: ").append(e.getMessage());
            return false;
        }
    }

    public static com.startapp.networkTest.data.f a(d dVar) {
        com.startapp.networkTest.data.f fVar = new com.startapp.networkTest.data.f();
        fVar.MobileRxBytes = h.b();
        fVar.MobileTxBytes = h.a();
        fVar.TotalRxBytes = TrafficStats.getTotalRxBytes();
        fVar.TotalTxBytes = TrafficStats.getTotalTxBytes();
        if (dVar != null) {
            fVar.WifiRxBytes = dVar.b();
            fVar.WifiTxBytes = dVar.c();
        } else {
            fVar.WifiRxBytes = -1;
            fVar.WifiTxBytes = -1;
        }
        return fVar;
    }

    public static ScreenStates d(Context context) {
        ScreenStates screenStates = ScreenStates.Unknown;
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return screenStates;
        }
        return powerManager.isScreenOn() ? ScreenStates.On : ScreenStates.Off;
    }

    public static IdleStates e(Context context) {
        IdleStates idleStates = IdleStates.Unknown;
        if (VERSION.SDK_INT < 23) {
            return idleStates;
        }
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return idleStates;
        }
        if (VERSION.SDK_INT >= 24) {
            try {
                if (((Boolean) powerManager.getClass().getDeclaredMethod("isLightDeviceIdleMode", new Class[0]).invoke(powerManager, new Object[0])).booleanValue()) {
                    idleStates = IdleStates.LightIdle;
                }
            } catch (Exception e) {
                String str = a;
                StringBuilder sb = new StringBuilder("getIdleState: ");
                sb.append(e.getMessage());
                Log.e(str, sb.toString());
            }
        }
        if (idleStates == IdleStates.LightIdle) {
            return idleStates;
        }
        return powerManager.isDeviceIdleMode() ? IdleStates.DeepIdle : IdleStates.NonIdle;
    }

    public static com.startapp.networkTest.data.a.b f(Context context) {
        com.startapp.networkTest.data.a.a g = g(context);
        Iterator it = g.SimInfos.iterator();
        while (it.hasNext()) {
            com.startapp.networkTest.data.a.b bVar = (com.startapp.networkTest.data.a.b) it.next();
            if (bVar.SubscriptionId == g.DefaultDataSimId) {
                return bVar;
            }
        }
        return new com.startapp.networkTest.data.a.b();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(12:172|173|174|(4:178|179|180|191)|192|193|(4:197|198|(1:200)(2:201|202)|208)|209|210|(3:214|215|224)(1:223)|221|169) */
    /* JADX WARNING: Can't wrap try/catch for region: R(21:6|(4:10|(27:13|14|15|(1:17)|20|21|(1:23)|26|27|(3:29|(1:31)(2:32|33)|39)|42|(1:44)(1:45)|46|(1:48)(1:49)|50|(1:52)(1:53)|54|(1:56)|57|58|(3:60|61|(2:63|64))|(2:218|76)|67|68|220|76|11)|217|84)|(2:85|86)|(18:90|91|(2:95|96)|97|98|(13:102|103|(2:107|108)|109|110|(8:114|115|(2:119|120)|121|122|(3:126|127|(2:131|132))|128|(0))|116|(0)|121|122|(0)|128|(0))|104|(0)|109|110|(0)|116|(0)|121|122|(0)|128|(0))|92|(0)|97|98|(0)|104|(0)|109|110|(0)|116|(0)|121|122|(0)|128|(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x027a, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0379, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x03f6, code lost:
        r6 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:109:0x023f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:121:0x026d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:192:0x0368 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:209:0x03e5 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:97:0x0211 */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0221 A[SYNTHETIC, Splitter:B:102:0x0221] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0231 A[SYNTHETIC, Splitter:B:107:0x0231] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x024f A[SYNTHETIC, Splitter:B:114:0x024f] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x025f A[SYNTHETIC, Splitter:B:119:0x025f] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x027d A[SYNTHETIC, Splitter:B:126:0x027d] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x028d A[SYNTHETIC, Splitter:B:131:0x028d] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x02f8 A[Catch:{ Exception -> 0x030c }] */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x02fb A[Catch:{ Exception -> 0x030c }] */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0300 A[Catch:{ Exception -> 0x030c }] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0305 A[Catch:{ Exception -> 0x030c }] */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x037c A[SYNTHETIC, Splitter:B:197:0x037c] */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x03f9 A[SYNTHETIC, Splitter:B:214:0x03f9] */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x01cc A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x0312 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01d4 A[SYNTHETIC, Splitter:B:80:0x01d4] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0203 A[SYNTHETIC, Splitter:B:95:0x0203] */
    public static com.startapp.networkTest.data.a.a g(Context context) {
        Method method;
        Method method2;
        SimStates simStates;
        Method method3;
        Method method4;
        Method method5;
        Method method6;
        Method method7;
        Method method8;
        Method method9;
        Method method10;
        Cursor cursor;
        Context context2 = context;
        com.startapp.networkTest.data.a.a aVar = new com.startapp.networkTest.data.a.a();
        if (VERSION.SDK_INT >= 22 && context2.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            SubscriptionManager from = SubscriptionManager.from(context);
            char c = 2;
            if (from != null) {
                aVar.ActiveSimCount = from.getActiveSubscriptionInfoCount();
                aVar.ActiveSimCountMax = from.getActiveSubscriptionInfoCountMax();
                List<SubscriptionInfo> activeSubscriptionInfoList = from.getActiveSubscriptionInfoList();
                if (activeSubscriptionInfoList != null && activeSubscriptionInfoList.size() > 0) {
                    com.startapp.networkTest.data.a.b[] bVarArr = new com.startapp.networkTest.data.a.b[activeSubscriptionInfoList.size()];
                    int i = 0;
                    for (SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
                        com.startapp.networkTest.data.a.b bVar = new com.startapp.networkTest.data.a.b();
                        try {
                            if (subscriptionInfo.getCarrierName() != null) {
                                bVar.CarrierName = g.a(subscriptionInfo.getCarrierName().toString());
                            }
                        } catch (Exception e) {
                            String str = a;
                            StringBuilder sb = new StringBuilder("getMultiSimInfo: ");
                            sb.append(e.getMessage());
                            Log.e(str, sb.toString());
                        }
                        try {
                            if (subscriptionInfo.getCountryIso() != null) {
                                bVar.CountryIso = g.a(subscriptionInfo.getCountryIso());
                            }
                        } catch (Exception e2) {
                            String str2 = a;
                            StringBuilder sb2 = new StringBuilder("getMultiSimInfo: ");
                            sb2.append(e2.getMessage());
                            Log.e(str2, sb2.toString());
                        }
                        try {
                            if (subscriptionInfo.getIccId() != null) {
                                String a2 = g.a(subscriptionInfo.getIccId());
                                if (a2.length() != 0) {
                                    switch (c.d().u()) {
                                        case Full:
                                            break;
                                        case Anonymized:
                                            if (a2.length() < 11) {
                                                a2 = a2.replaceAll("[\\d\\w]", "*");
                                                break;
                                            } else {
                                                String substring = a2.substring(0, 7);
                                                String replaceAll = a2.substring(7, a2.length()).replaceAll("[\\d\\w]", "*");
                                                StringBuilder sb3 = new StringBuilder();
                                                sb3.append(substring);
                                                sb3.append(replaceAll);
                                                a2 = sb3.toString();
                                                break;
                                            }
                                        default:
                                            a2 = "";
                                            break;
                                    }
                                }
                                bVar.IccId = a2;
                            }
                        } catch (Exception e3) {
                            String str3 = a;
                            StringBuilder sb4 = new StringBuilder("getMultiSimInfo: ");
                            sb4.append(e3.getMessage());
                            Log.e(str3, sb4.toString());
                        }
                        bVar.Mcc = subscriptionInfo.getMcc() == Integer.MAX_VALUE ? -1 : subscriptionInfo.getMcc();
                        bVar.Mnc = subscriptionInfo.getMnc() == Integer.MAX_VALUE ? -1 : subscriptionInfo.getMnc();
                        bVar.SimSlotIndex = subscriptionInfo.getSimSlotIndex();
                        bVar.SubscriptionId = subscriptionInfo.getSubscriptionId();
                        bVar.DataRoaming = subscriptionInfo.getDataRoaming() == 1;
                        int i2 = bVar.SubscriptionId;
                        String str4 = "content://telephony/carriers/preferapn";
                        if (i2 != -1) {
                            str4 = "content://telephony/carriers/preferapn/subId/".concat(String.valueOf(i2));
                        }
                        try {
                            cursor = context.getContentResolver().query(Uri.parse(str4), new String[]{"apn", "type"}, null, null, null);
                            if (cursor != null) {
                                try {
                                    if (cursor.moveToFirst()) {
                                        String string = cursor.getString(cursor.getColumnIndex("apn"));
                                        String string2 = cursor.getString(cursor.getColumnIndex("type"));
                                        bVar.Apn = string;
                                        bVar.ApnTypes = string2;
                                        cursor.close();
                                        cursor = null;
                                    }
                                } catch (Exception e4) {
                                    e = e4;
                                    try {
                                        new StringBuilder("saveApnItemsToSimInfo: ").append(e.toString());
                                        if (cursor == null) {
                                        }
                                        cursor.close();
                                        bVarArr[i] = bVar;
                                        i++;
                                    } catch (Throwable th) {
                                        th = th;
                                        if (cursor != null) {
                                        }
                                        throw th;
                                    }
                                }
                            }
                            if (cursor == null) {
                                bVarArr[i] = bVar;
                                i++;
                            }
                        } catch (Exception e5) {
                            e = e5;
                            cursor = null;
                            new StringBuilder("saveApnItemsToSimInfo: ").append(e.toString());
                            if (cursor == null) {
                                bVarArr[i] = bVar;
                                i++;
                            }
                            cursor.close();
                            bVarArr[i] = bVar;
                            i++;
                        } catch (Throwable th2) {
                            th = th2;
                            cursor = null;
                            if (cursor != null) {
                                try {
                                    cursor.close();
                                } catch (Exception unused) {
                                }
                            }
                            throw th;
                        }
                        try {
                            cursor.close();
                        } catch (Exception unused2) {
                        }
                        bVarArr[i] = bVar;
                        i++;
                    }
                    aVar.SimInfos = new ArrayList<>(Arrays.asList(bVarArr));
                }
                try {
                    method3 = from.getClass().getDeclaredMethod("getDefaultDataSubscriptionId", new Class[0]);
                } catch (NoSuchMethodException unused3) {
                    method3 = null;
                }
                if (method3 == null) {
                    try {
                        method4 = from.getClass().getDeclaredMethod("getDefaultDataSubId", new Class[0]);
                    } catch (NoSuchMethodException unused4) {
                    }
                    if (method4 != null) {
                        aVar.DefaultDataSimId = ((Integer) method4.invoke(from, new Object[0])).intValue();
                    }
                    method5 = from.getClass().getDeclaredMethod("getDefaultSmsSubscriptionId", new Class[0]);
                    if (method5 == null) {
                        try {
                            method6 = from.getClass().getDeclaredMethod("getDefaultSmsSubId", new Class[0]);
                        } catch (NoSuchMethodException unused5) {
                        }
                        if (method6 != null) {
                            aVar.DefaultSmsSimId = ((Integer) method6.invoke(from, new Object[0])).intValue();
                        }
                        method7 = from.getClass().getDeclaredMethod("getDefaultSubscriptionId", new Class[0]);
                        if (method7 == null) {
                            try {
                                method8 = from.getClass().getDeclaredMethod("getDefaultSubId", new Class[0]);
                            } catch (NoSuchMethodException unused6) {
                            }
                            if (method8 != null) {
                                aVar.DefaultSimId = ((Integer) method8.invoke(from, new Object[0])).intValue();
                            }
                            method9 = from.getClass().getDeclaredMethod("getDefaultVoiceSubscriptionId", new Class[0]);
                            if (method9 == null) {
                                try {
                                    method10 = from.getClass().getDeclaredMethod("getDefaultVoiceSubId", new Class[0]);
                                } catch (NoSuchMethodException unused7) {
                                }
                                if (method10 != null) {
                                    try {
                                        aVar.DefaultVoiceSimId = ((Integer) method10.invoke(from, new Object[0])).intValue();
                                    } catch (Exception unused8) {
                                    }
                                }
                            }
                            method10 = method9;
                            if (method10 != null) {
                            }
                        }
                        method8 = method7;
                        if (method8 != null) {
                        }
                        method9 = from.getClass().getDeclaredMethod("getDefaultVoiceSubscriptionId", new Class[0]);
                        if (method9 == null) {
                        }
                        method10 = method9;
                        if (method10 != null) {
                        }
                    }
                    method6 = method5;
                    if (method6 != null) {
                    }
                    method7 = from.getClass().getDeclaredMethod("getDefaultSubscriptionId", new Class[0]);
                    if (method7 == null) {
                    }
                    method8 = method7;
                    if (method8 != null) {
                    }
                    method9 = from.getClass().getDeclaredMethod("getDefaultVoiceSubscriptionId", new Class[0]);
                    if (method9 == null) {
                    }
                    method10 = method9;
                    if (method10 != null) {
                    }
                }
                method4 = method3;
                if (method4 != null) {
                }
                try {
                    method5 = from.getClass().getDeclaredMethod("getDefaultSmsSubscriptionId", new Class[0]);
                } catch (NoSuchMethodException unused9) {
                    method5 = null;
                }
                if (method5 == null) {
                }
                method6 = method5;
                if (method6 != null) {
                }
                try {
                    method7 = from.getClass().getDeclaredMethod("getDefaultSubscriptionId", new Class[0]);
                } catch (NoSuchMethodException unused10) {
                    method7 = null;
                }
                if (method7 == null) {
                }
                method8 = method7;
                if (method8 != null) {
                }
                method9 = from.getClass().getDeclaredMethod("getDefaultVoiceSubscriptionId", new Class[0]);
                if (method9 == null) {
                }
                method10 = method9;
                if (method10 != null) {
                }
            }
            TelephonyManager telephonyManager = (TelephonyManager) context2.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                try {
                    method = telephonyManager.getClass().getDeclaredMethod("getMultiSimConfiguration", new Class[0]);
                } catch (NoSuchMethodException unused11) {
                    method = null;
                }
                if (method != null) {
                    try {
                        Object invoke = method.invoke(telephonyManager, new Object[0]);
                        if (invoke instanceof Enum) {
                            String obj = invoke.toString();
                            int hashCode = obj.hashCode();
                            if (hashCode == 2107724) {
                                if (obj.equals("DSDA")) {
                                    c = 0;
                                    switch (c) {
                                        case 0:
                                            break;
                                        case 1:
                                            break;
                                        case 2:
                                            break;
                                    }
                                }
                            } else if (hashCode == 2107742) {
                                if (obj.equals("DSDS")) {
                                    c = 1;
                                    switch (c) {
                                        case 0:
                                            break;
                                        case 1:
                                            break;
                                        case 2:
                                            break;
                                    }
                                }
                            } else if (hashCode == 2584894) {
                                if (obj.equals("TSTS")) {
                                    switch (c) {
                                        case 0:
                                            aVar.MultiSimVariant = MultiSimVariants.DSDA;
                                            break;
                                        case 1:
                                            aVar.MultiSimVariant = MultiSimVariants.DSDS;
                                            break;
                                        case 2:
                                            aVar.MultiSimVariant = MultiSimVariants.TSTS;
                                            break;
                                        default:
                                            aVar.MultiSimVariant = MultiSimVariants.Unknown;
                                            break;
                                    }
                                }
                            }
                            c = 65535;
                            switch (c) {
                                case 0:
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    break;
                            }
                        }
                    } catch (Exception unused12) {
                    }
                }
                Iterator it = aVar.SimInfos.iterator();
                while (it.hasNext()) {
                    com.startapp.networkTest.data.a.b bVar2 = (com.startapp.networkTest.data.a.b) it.next();
                    try {
                        method2 = telephonyManager.getClass().getDeclaredMethod("getSimState", new Class[]{Integer.TYPE});
                    } catch (NoSuchMethodException unused13) {
                        method2 = null;
                    }
                    if (method2 != null) {
                        switch (((Integer) method2.invoke(telephonyManager, new Object[]{Integer.valueOf(bVar2.SimSlotIndex)})).intValue()) {
                            case 1:
                                simStates = SimStates.Absent;
                                break;
                            case 2:
                                simStates = SimStates.PinRequired;
                                break;
                            case 3:
                                simStates = SimStates.PukRequired;
                                break;
                            case 4:
                                simStates = SimStates.NetworkLocked;
                                break;
                            case 5:
                                simStates = SimStates.Ready;
                                break;
                            case 6:
                                simStates = SimStates.NotReady;
                                break;
                            case 7:
                                simStates = SimStates.PermanentlyDisabled;
                                break;
                            case 8:
                                simStates = SimStates.CardIoError;
                                break;
                            case 9:
                                simStates = SimStates.CardRestricted;
                                break;
                            default:
                                simStates = SimStates.Unknown;
                                break;
                        }
                        bVar2.SimState = simStates;
                    }
                    Method method11 = telephonyManager.getClass().getDeclaredMethod("getSubscriberId", new Class[]{Integer.TYPE});
                    if (method11 != null) {
                        String a3 = g.a((String) method11.invoke(telephonyManager, new Object[]{Integer.valueOf(bVar2.SubscriptionId)}));
                        if (a3.length() != 0) {
                            switch (c.d().v()) {
                                case Full:
                                    break;
                                case Anonymized:
                                    if (a3.length() < 14) {
                                        a3 = a3.replaceAll("[\\d\\w]", "*");
                                        break;
                                    } else {
                                        String substring2 = a3.substring(0, 10);
                                        String replaceAll2 = a3.substring(10, a3.length()).replaceAll("[\\d\\w]", "*");
                                        StringBuilder sb5 = new StringBuilder();
                                        sb5.append(substring2);
                                        sb5.append(replaceAll2);
                                        a3 = sb5.toString();
                                        break;
                                    }
                                default:
                                    a3 = "";
                                    break;
                            }
                        }
                        bVar2.IMSI = a3;
                    }
                    Method method12 = telephonyManager.getClass().getDeclaredMethod("getGroupIdLevel1", new Class[]{Integer.TYPE});
                    if (method12 != null) {
                        try {
                            bVar2.GroupIdentifierLevel1 = g.a((String) method12.invoke(telephonyManager, new Object[]{Integer.valueOf(bVar2.SubscriptionId)}));
                        } catch (Exception unused14) {
                        }
                    }
                }
            }
        }
        return aVar;
    }
}
