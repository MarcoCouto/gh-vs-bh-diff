package com.startapp.networkTest.controller;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build.VERSION;
import android.util.Log;
import com.startapp.networkTest.data.BatteryInfo;
import com.startapp.networkTest.enums.BatteryChargePlugTypes;
import com.startapp.networkTest.enums.BatteryHealthStates;
import com.startapp.networkTest.enums.BatteryStatusStates;
import com.startapp.networkTest.utils.g;

/* compiled from: StartAppSDK */
public class a {
    private static final String a = "a";
    private BatteryManager b;
    private Context c;

    public a(Context context) {
        if (VERSION.SDK_INT >= 21) {
            this.b = (BatteryManager) context.getSystemService("batterymanager");
        }
        this.c = context;
    }

    public final BatteryInfo a() {
        Intent intent;
        BatteryChargePlugTypes batteryChargePlugTypes;
        BatteryHealthStates batteryHealthStates;
        try {
            intent = this.c.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        } catch (Exception e) {
            Log.e(a, e.getMessage(), e);
            intent = null;
        }
        BatteryInfo batteryInfo = new BatteryInfo();
        if (intent == null) {
            batteryInfo.MissingPermission = true;
            return batteryInfo;
        }
        int intExtra = intent.getIntExtra("status", 1);
        BatteryStatusStates batteryStatusStates = BatteryStatusStates.Unknown;
        switch (intExtra) {
            case 1:
                batteryStatusStates = BatteryStatusStates.Unknown;
                break;
            case 2:
                batteryStatusStates = BatteryStatusStates.Charging;
                break;
            case 3:
                batteryStatusStates = BatteryStatusStates.Discharging;
                break;
            case 4:
                batteryStatusStates = BatteryStatusStates.NotCharging;
                break;
            case 5:
                batteryStatusStates = BatteryStatusStates.Full;
                break;
        }
        batteryInfo.BatteryStatus = batteryStatusStates;
        int intExtra2 = intent.getIntExtra("plugged", -1);
        if (intExtra2 != 4) {
            switch (intExtra2) {
                case 1:
                    batteryChargePlugTypes = BatteryChargePlugTypes.AC;
                    break;
                case 2:
                    batteryChargePlugTypes = BatteryChargePlugTypes.USB;
                    break;
                default:
                    batteryChargePlugTypes = BatteryChargePlugTypes.Unknown;
                    break;
            }
        } else {
            batteryChargePlugTypes = BatteryChargePlugTypes.Wireless;
        }
        batteryInfo.BatteryChargePlug = batteryChargePlugTypes;
        batteryInfo.BatteryLevel = (((float) intent.getIntExtra("level", -1)) / ((float) intent.getIntExtra("scale", -1))) * 100.0f;
        int intExtra3 = intent.getIntExtra("health", -1);
        if (intExtra3 != 7) {
            switch (intExtra3) {
                case 2:
                    batteryHealthStates = BatteryHealthStates.Good;
                    break;
                case 3:
                    batteryHealthStates = BatteryHealthStates.Overheat;
                    break;
                case 4:
                    batteryHealthStates = BatteryHealthStates.Dead;
                    break;
                case 5:
                    batteryHealthStates = BatteryHealthStates.OverVoltage;
                    break;
                default:
                    batteryHealthStates = BatteryHealthStates.Unknown;
                    break;
            }
        } else {
            batteryHealthStates = BatteryHealthStates.Cold;
        }
        batteryInfo.BatteryHealth = batteryHealthStates;
        int intExtra4 = intent.getIntExtra("temperature", -1);
        if (intExtra4 >= 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(((float) intExtra4) / 10.0f);
            batteryInfo.BatteryTemp = sb.toString();
        }
        int intExtra5 = intent.getIntExtra("voltage", -1);
        if (intExtra5 >= 0) {
            batteryInfo.BatteryVoltage = intExtra5;
        }
        batteryInfo.BatteryTechnology = g.a(intent.getStringExtra("technology"));
        if (VERSION.SDK_INT >= 21) {
            try {
                if (this.b != null) {
                    int intProperty = this.b.getIntProperty(1);
                    if (intProperty != Integer.MIN_VALUE) {
                        batteryInfo.BatteryCapacity = intProperty;
                    }
                    int intProperty2 = this.b.getIntProperty(2);
                    if (intProperty2 != Integer.MIN_VALUE) {
                        batteryInfo.BatteryCurrent = intProperty2;
                    }
                    long longProperty = this.b.getLongProperty(5);
                    if (longProperty != Long.MIN_VALUE) {
                        batteryInfo.BatteryRemainingEnergy = longProperty;
                    }
                }
            } catch (Exception e2) {
                Log.e(a, e2.toString());
            }
        }
        return batteryInfo;
    }
}
