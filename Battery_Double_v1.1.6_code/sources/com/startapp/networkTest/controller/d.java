package com.startapp.networkTest.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import com.startapp.networkTest.data.WifiInfo;
import com.startapp.networkTest.enums.WifiStates;
import com.startapp.networkTest.utils.h;

/* compiled from: StartAppSDK */
public class d {
    private WifiStates a;
    private ConnectivityManager b;
    private boolean c = false;
    private Context d;
    private String e = "";

    static {
        d.class.getSimpleName();
    }

    public d(Context context) {
        this.d = context.getApplicationContext();
        this.b = (ConnectivityManager) context.getSystemService("connectivity");
        this.a = WifiStates.Unknown;
    }

    public final WifiInfo a() {
        WifiInfo wifiInfo = new WifiInfo();
        wifiInfo.MissingPermission = true;
        try {
            if (this.d.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == -1) {
                return wifiInfo;
            }
            return wifiInfo;
        } catch (Exception e2) {
            new StringBuilder("getWifiInfo: ").append(e2.getMessage());
        }
    }

    public final long b() {
        if (this.e.length() == 0) {
            a();
        }
        if (this.e.length() == 0) {
            return -1;
        }
        return h.b(this.e);
    }

    public final long c() {
        if (this.e == null || this.e.length() == 0) {
            a();
        }
        if (this.e == null || this.e.length() == 0) {
            return -1;
        }
        return h.a(this.e);
    }
}
