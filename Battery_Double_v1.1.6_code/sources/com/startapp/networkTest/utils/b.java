package com.startapp.networkTest.utils;

/* compiled from: StartAppSDK */
public final class b {
    private static final char[] a = "0123456789abcdef".toCharArray();

    public static String a(byte[] bArr) {
        char[] cArr = new char[(bArr.length << 1)];
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i] & 255;
            int i2 = i << 1;
            cArr[i2] = a[b >>> 4];
            cArr[i2 + 1] = a[b & 15];
        }
        return new String(cArr);
    }
}
