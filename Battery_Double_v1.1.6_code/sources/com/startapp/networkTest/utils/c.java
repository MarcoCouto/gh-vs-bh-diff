package com.startapp.networkTest.utils;

import com.startapp.sdk.adsbase.cache.DiskAdCacheManager;
import java.util.List;

/* compiled from: StartAppSDK */
public final class c {
    private static final List<c> h = new LteFrequencyUtil$1();
    private float a;
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private DiskAdCacheManager g;

    /* synthetic */ c(int i, float f2, float f3, float f4, float f5, float f6, float f7, byte b2) {
        this(i, f2, f3, f4, f5, f6, f7);
    }

    private c(int i, float f2, float f3, float f4, float f5, float f6, float f7) {
        this.g = null;
        this.g = new DiskAdCacheManager();
        this.g.a = i;
        this.a = f5;
        this.b = f7;
        this.d = f2;
        this.e = f4;
        this.c = f6;
        this.f = f3;
    }

    public static DiskAdCacheManager a(int i) {
        DiskAdCacheManager diskAdCacheManager = null;
        for (c cVar : h) {
            float f2 = (float) i;
            boolean z = true;
            if (f2 >= cVar.f && f2 <= cVar.e) {
                cVar.g.d = cVar.d + ((f2 - cVar.f) * 0.1f);
                cVar.g.d = a(cVar.g.d);
                cVar.g.b = i;
                if (!(cVar.a == 0.0f && cVar.c == 0.0f)) {
                    cVar.g.e = cVar.a + (cVar.g.d - cVar.d);
                    cVar.g.e = a(cVar.g.e);
                    cVar.g.c = (int) (cVar.c + (f2 - cVar.f));
                }
                diskAdCacheManager = cVar.g;
            } else if (i > 0) {
                float f3 = (float) ((long) i);
                if (f3 < cVar.c || f3 > cVar.b) {
                    z = false;
                }
                if (z) {
                    cVar.g.e = cVar.a + ((f2 - cVar.c) * 0.1f);
                    cVar.g.e = a(cVar.g.e);
                    cVar.g.c = i;
                    cVar.g.d = cVar.d + (cVar.g.e - cVar.a);
                    cVar.g.d = a(cVar.g.d);
                    cVar.g.b = (int) (cVar.f + (f2 - cVar.c));
                    diskAdCacheManager = cVar.g;
                }
            }
        }
        return diskAdCacheManager;
    }

    private static float a(float f2) {
        float pow = (float) ((long) Math.pow(10.0d, 1.0d));
        return ((float) ((long) Math.round(f2 * pow))) / pow;
    }
}
