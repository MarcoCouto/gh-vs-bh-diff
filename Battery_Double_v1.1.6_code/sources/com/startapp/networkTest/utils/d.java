package com.startapp.networkTest.utils;

import com.iab.omid.library.startapp.b;

/* compiled from: StartAppSDK */
public final class d {
    private final String a;
    private final String b;

    private d(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public static d a(String str, String str2) {
        b.b(str, "Name is null or empty");
        b.b(str2, "Version is null or empty");
        return new d(str, str2);
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }
}
