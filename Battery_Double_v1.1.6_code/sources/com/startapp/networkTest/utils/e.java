package com.startapp.networkTest.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

/* compiled from: StartAppSDK */
public class e {
    static {
        e.class.getSimpleName();
    }

    public static String a(String str) {
        String str2;
        String str3 = "";
        try {
            str2 = InetAddress.getByName(str).getCanonicalHostName();
        } catch (UnknownHostException e) {
            new StringBuilder("serverResult: ").append(e.toString());
            str2 = null;
        }
        if (str2 == null || str2.equals(str) || !str2.contains("cloudfront")) {
            return str3;
        }
        String[] split = str2.split("\\.");
        return split.length > 0 ? b(split[1]) : str3;
    }

    public static String b(String str) {
        return (str == null || str.length() <= 2) ? "" : str.substring(0, 3);
    }
}
