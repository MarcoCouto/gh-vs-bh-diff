package com.startapp.networkTest.utils;

import com.startapp.networkTest.c;
import com.startapp.networkTest.d;
import com.startapp.networkTest.e.b;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Signature;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* compiled from: StartAppSDK */
public class a {
    static {
        a.class.getSimpleName();
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c0  */
    public static void a() {
        HttpURLConnection httpURLConnection;
        Throwable th;
        InputStream inputStream;
        HttpURLConnection httpURLConnection2 = null;
        try {
            httpURLConnection = (HttpURLConnection) new URL(c.d().m().replace("[PROJECTID]", c.d().a())).openConnection();
            try {
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setReadTimeout(10000);
                long h = c.c().h();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                httpURLConnection.setRequestProperty("If-Modified-Since", simpleDateFormat.format(Long.valueOf(h)));
                httpURLConnection.setRequestProperty("Connection", "close");
                if (httpURLConnection.getResponseCode() == 304) {
                    c.c().e(b.b());
                } else if (httpURLConnection.getResponseCode() == 200) {
                    long lastModified = httpURLConnection.getLastModified();
                    inputStream = httpURLConnection.getInputStream();
                    boolean a = a(inputStream);
                    inputStream.close();
                    if (a) {
                        c.c().e(b.b());
                        c.c().d(lastModified);
                    } else {
                        throw new IOException("Verification of downloaded cdn config failed");
                    }
                }
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            } catch (IOException unused) {
                httpURLConnection2 = httpURLConnection;
                if (httpURLConnection2 != null) {
                }
            } catch (Throwable th2) {
                th = th2;
                if (httpURLConnection != null) {
                }
                throw th;
            }
        } catch (IOException unused2) {
            if (httpURLConnection2 != null) {
                httpURLConnection2.disconnect();
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            httpURLConnection = null;
            th = th4;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    private static boolean a(InputStream inputStream) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        byte[] bArr = new byte[512];
        while (true) {
            try {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    break;
                } else if (!nextEntry.isDirectory()) {
                    if (nextEntry.getName().equalsIgnoreCase("cdnconfig.txt")) {
                        for (int read = zipInputStream.read(bArr); read != -1; read = zipInputStream.read(bArr)) {
                            byteArrayOutputStream.write(bArr, 0, read);
                        }
                        byteArrayOutputStream.flush();
                        zipInputStream.closeEntry();
                    } else if (nextEntry.getName().equalsIgnoreCase("cdnconfig.txt.sig")) {
                        for (int read2 = zipInputStream.read(bArr); read2 != -1; read2 = zipInputStream.read(bArr)) {
                            byteArrayOutputStream2.write(bArr, 0, read2);
                        }
                        byteArrayOutputStream2.flush();
                        zipInputStream.closeEntry();
                    }
                }
            } finally {
                zipInputStream.close();
                try {
                    byteArrayOutputStream2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        try {
            byteArrayOutputStream2.close();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        try {
            byteArrayOutputStream.close();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        boolean z = true;
        if (c.d().n()) {
            z = a(byteArrayOutputStream, byteArrayOutputStream2);
        }
        if (z && ((com.startapp.networkTest.d.a.a) com.startapp.common.parser.b.a(new String(byteArrayOutputStream.toByteArray(), "UTF-8"), com.startapp.networkTest.d.a.a.class)) != null) {
            d c = c.c();
            c.a(null);
            c.c((Set<String>) new HashSet<String>(null));
            c.k();
            c.d((Set<String>) new HashSet<String>(null));
            c.n();
        }
        return z;
    }

    private static boolean a(ByteArrayOutputStream byteArrayOutputStream, ByteArrayOutputStream byteArrayOutputStream2) {
        try {
            byte[] byteArray = byteArrayOutputStream2.toByteArray();
            byte[] byteArray2 = byteArrayOutputStream.toByteArray();
            Signature instance = Signature.getInstance("SHA256withRSA");
            instance.initVerify(null);
            instance.update(byteArray2);
            return instance.verify(byteArray);
        } catch (Exception unused) {
            return false;
        }
    }
}
