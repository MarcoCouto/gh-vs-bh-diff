package com.startapp.networkTest.utils;

import android.content.Context;
import com.startapp.networkTest.c;
import com.startapp.networkTest.e.b;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Signature;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* compiled from: StartAppSDK */
public class i {
    static {
        i.class.getSimpleName();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x012a, code lost:
        r1.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x013e, code lost:
        r1.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0143, code lost:
        r0.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x0119 */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x012a A[Catch:{ all -> 0x0116 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x013e A[Catch:{ all -> 0x0116 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0143  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x014a  */
    public static boolean a(Context context) {
        InputStream inputStream;
        HttpURLConnection httpURLConnection = null;
        HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(c.d().i().replace("[PROJECTID]", c.d().a())).openConnection();
        try {
            httpURLConnection2.setRequestMethod(HttpRequest.METHOD_GET);
            httpURLConnection2.setConnectTimeout(10000);
            httpURLConnection2.setReadTimeout(10000);
            long e = c.c().e();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            httpURLConnection2.setRequestProperty("If-Modified-Since", simpleDateFormat.format(Long.valueOf(e)));
            httpURLConnection2.setRequestProperty("Connection", "close");
            if (httpURLConnection2.getResponseCode() == 304) {
                c.c().b(b.b());
            } else if (httpURLConnection2.getResponseCode() == 200) {
                long lastModified = httpURLConnection2.getLastModified();
                inputStream = httpURLConnection2.getInputStream();
                a(inputStream, d(context));
                inputStream.close();
                if (!(c.d().j() ? a(new File(d(context), "truststore.bin"), new File(d(context), "truststore.bin.sig")) : true)) {
                    throw new IOException("Verification of downloaded truststore failed");
                } else if (!new File(d(context), "truststore.bin").renameTo(b(context)) || !new File(d(context), "truststore.bin.sig").renameTo(c(context))) {
                    throw new IOException("Moving of cached files failed.");
                } else {
                    c.c().b(b.b());
                    c.c().c(lastModified);
                    if (httpURLConnection2 != null) {
                        httpURLConnection2.disconnect();
                    }
                    return true;
                }
            }
            if (httpURLConnection2 != null) {
                httpURLConnection2.disconnect();
            }
        } catch (IOException unused) {
            httpURLConnection = httpURLConnection2;
            try {
                File file = new File(d(context), "truststore.bin");
                if (file.exists()) {
                }
                File file2 = new File(d(context), "truststore.bin.sig");
                if (file2.exists()) {
                }
                if (httpURLConnection != null) {
                }
                return false;
            } catch (Throwable th) {
                th = th;
                httpURLConnection2 = httpURLConnection;
                if (httpURLConnection2 != null) {
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            if (httpURLConnection2 != null) {
                httpURLConnection2.disconnect();
            }
            throw th;
        }
        return false;
    }

    public static File b(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getFilesDir());
        sb.append("/insight/truststore/");
        File file = new File(sb.toString());
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, "truststore.bin");
    }

    public static File c(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getFilesDir());
        sb.append("/insight/truststore/");
        File file = new File(sb.toString());
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, "truststore.bin.sig");
    }

    private static File d(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getCacheDir());
        sb.append("/insight/truststore/");
        File file = new File(sb.toString(), "truststoreunzip");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    private static void a(InputStream inputStream, File file) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        while (true) {
            try {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    return;
                }
                if (nextEntry.isDirectory()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(file);
                    sb.append(File.separator);
                    sb.append(nextEntry.getName());
                    File file2 = new File(sb.toString());
                    if (!file2.isDirectory()) {
                        file2.mkdirs();
                    }
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(file);
                    sb2.append(File.separator);
                    sb2.append(nextEntry.getName());
                    FileOutputStream fileOutputStream = new FileOutputStream(sb2.toString());
                    while (true) {
                        int read = zipInputStream.read();
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(read);
                    }
                    zipInputStream.closeEntry();
                    fileOutputStream.close();
                }
            } finally {
                zipInputStream.close();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0071 A[SYNTHETIC, Splitter:B:31:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x007b A[SYNTHETIC, Splitter:B:36:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0087 A[SYNTHETIC, Splitter:B:45:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0091 A[SYNTHETIC, Splitter:B:50:0x0091] */
    public static boolean a(File file, File file2) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2 = null;
        try {
            fileInputStream = new FileInputStream(file2);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[512];
                for (int read = fileInputStream.read(bArr); read != -1; read = fileInputStream.read(bArr)) {
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                byteArrayOutputStream.flush();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                FileInputStream fileInputStream3 = new FileInputStream(file);
                try {
                    ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                    for (int read2 = fileInputStream3.read(bArr); read2 != -1; read2 = fileInputStream3.read(bArr)) {
                        byteArrayOutputStream2.write(bArr, 0, read2);
                    }
                    byteArrayOutputStream2.flush();
                    byte[] byteArray2 = byteArrayOutputStream2.toByteArray();
                    Signature instance = Signature.getInstance("SHA256withRSA");
                    instance.initVerify(null);
                    instance.update(byteArray2);
                    boolean verify = instance.verify(byteArray);
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        fileInputStream3.close();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    return verify;
                } catch (Exception unused) {
                    fileInputStream2 = fileInputStream3;
                    if (fileInputStream != null) {
                    }
                    if (fileInputStream2 != null) {
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    fileInputStream2 = fileInputStream3;
                    if (fileInputStream != null) {
                    }
                    if (fileInputStream2 != null) {
                    }
                    throw th;
                }
            } catch (Exception unused2) {
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
                if (fileInputStream2 != null) {
                    try {
                        fileInputStream2.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
                return false;
            } catch (Throwable th2) {
                th = th2;
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                }
                if (fileInputStream2 != null) {
                    try {
                        fileInputStream2.close();
                    } catch (IOException e6) {
                        e6.printStackTrace();
                    }
                }
                throw th;
            }
        } catch (Exception unused3) {
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileInputStream2 != null) {
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileInputStream2 != null) {
            }
            throw th;
        }
    }
}
