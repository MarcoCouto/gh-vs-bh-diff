package com.startapp.networkTest.utils;

import android.net.TrafficStats;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: StartAppSDK */
public class h {
    private static final String a = "h";
    private static String[] b;
    private static Method c;
    private static Method d;
    private static Method e;

    static {
        try {
            Method declaredMethod = TrafficStats.class.getDeclaredMethod("getRxBytes", new Class[]{String.class});
            c = declaredMethod;
            declaredMethod.setAccessible(true);
        } catch (NoSuchMethodException unused) {
        }
        try {
            Method declaredMethod2 = TrafficStats.class.getDeclaredMethod("getTxBytes", new Class[]{String.class});
            d = declaredMethod2;
            declaredMethod2.setAccessible(true);
        } catch (NoSuchMethodException unused2) {
        }
        try {
            Method declaredMethod3 = TrafficStats.class.getDeclaredMethod("getMobileIfaces", new Class[0]);
            e = declaredMethod3;
            declaredMethod3.setAccessible(true);
        } catch (NoSuchMethodException unused3) {
        }
    }

    public static synchronized long a() {
        long j;
        String[] strArr;
        synchronized (h.class) {
            try {
                j = TrafficStats.getMobileTxBytes();
            } catch (Exception e2) {
                String str = a;
                StringBuilder sb = new StringBuilder("getMobileTxBytes: ");
                sb.append(e2.getMessage());
                Log.e(str, sb.toString());
                j = 0;
            }
            if (j > 0) {
                if (b == null) {
                    c();
                }
            } else if (b != null) {
                for (String str2 : b) {
                    StringBuilder sb2 = new StringBuilder("/sys/class/net/");
                    sb2.append(str2);
                    sb2.append("/statistics/tx_bytes");
                    long c2 = c(sb2.toString());
                    if (c2 > -1) {
                        j += c2;
                    }
                }
            }
        }
        return j;
    }

    public static synchronized long b() {
        long j;
        String[] strArr;
        synchronized (h.class) {
            try {
                j = TrafficStats.getMobileRxBytes();
            } catch (Exception e2) {
                String str = a;
                StringBuilder sb = new StringBuilder("getMobileRxBytes: ");
                sb.append(e2.getMessage());
                Log.e(str, sb.toString());
                j = 0;
            }
            if (j > 0) {
                if (b == null) {
                    c();
                }
            } else if (b != null) {
                for (String str2 : b) {
                    StringBuilder sb2 = new StringBuilder("/sys/class/net/");
                    sb2.append(str2);
                    sb2.append("/statistics/rx_bytes");
                    long c2 = c(sb2.toString());
                    if (c2 > -1) {
                        j += c2;
                    }
                }
            }
        }
        return j;
    }

    private static long c(String str) {
        String[] a2 = f.a(str);
        if (a2.length > 0) {
            return Long.parseLong(a2[0]);
        }
        return -1;
    }

    private static void c() {
        if (e != null) {
            try {
                String[] strArr = (String[]) e.invoke(null, new Object[0]);
                if (strArr != null) {
                    b = strArr;
                }
            } catch (Exception e2) {
                String str = a;
                StringBuilder sb = new StringBuilder("getMobileInterfaces: ");
                sb.append(e2.getMessage());
                Log.e(str, sb.toString());
                e2.printStackTrace();
            }
        }
    }

    public static long a(String str) {
        if (d != null) {
            try {
                return ((Long) d.invoke(null, new Object[]{str})).longValue();
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                e2.printStackTrace();
            }
        }
        StringBuilder sb = new StringBuilder("/sys/class/net/");
        sb.append(str);
        sb.append("/statistics/tx_bytes");
        return c(sb.toString());
    }

    public static long b(String str) {
        if (c != null) {
            try {
                return ((Long) c.invoke(null, new Object[]{str})).longValue();
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                e2.printStackTrace();
            }
        }
        StringBuilder sb = new StringBuilder("/sys/class/net/");
        sb.append(str);
        sb.append("/statistics/rx_bytes");
        return c(sb.toString());
    }
}
