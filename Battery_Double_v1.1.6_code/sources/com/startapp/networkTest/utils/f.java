package com.startapp.networkTest.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/* compiled from: StartAppSDK */
public class f {
    static {
        f.class.getSimpleName();
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0048 A[SYNTHETIC, Splitter:B:19:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005e A[SYNTHETIC, Splitter:B:26:0x005e] */
    public static String[] a(String str) {
        FileInputStream fileInputStream;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[10240];
        FileInputStream fileInputStream2 = null;
        try {
            fileInputStream = new FileInputStream(str);
            while (true) {
                try {
                    int read = fileInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                } catch (Exception unused) {
                    fileInputStream2 = fileInputStream;
                    if (fileInputStream2 != null) {
                    }
                    return new String[0];
                } catch (Throwable th) {
                    th = th;
                    if (fileInputStream != null) {
                    }
                    throw th;
                }
            }
            String[] split = new String(byteArrayOutputStream.toByteArray(), "UTF-8").split("\n");
            try {
                fileInputStream.close();
            } catch (IOException e) {
                new StringBuilder("cat: ").append(e.toString());
            }
            return split;
        } catch (Exception unused2) {
            if (fileInputStream2 != null) {
                try {
                    fileInputStream2.close();
                } catch (IOException e2) {
                    new StringBuilder("cat: ").append(e2.toString());
                }
            }
            return new String[0];
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e3) {
                    new StringBuilder("cat: ").append(e3.toString());
                }
            }
            throw th;
        }
    }

    public static String[] b(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            Process exec = Runtime.getRuntime().exec(str);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(exec.getInputStream()));
            boolean z = true;
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                if (!z) {
                    stringBuffer.append("\n");
                }
                stringBuffer.append(readLine);
                z = false;
            }
            bufferedReader.close();
            exec.waitFor();
        } catch (Exception e) {
            new StringBuilder("shellResult: ").append(e.toString());
        }
        return stringBuffer.toString().split("\\n");
    }
}
