package com.startapp.networkTest.b;

import com.startapp.common.parser.b;
import com.startapp.networkTest.c;
import com.startapp.networkTest.data.IspInfo;
import com.startapp.networkTest.data.WifiInfo;
import com.startapp.networkTest.net.WebApiClient;
import com.startapp.networkTest.net.WebApiClient.RequestMethod;
import com.startapp.networkTest.utils.g;
import com.startapp.sdk.adsbase.i.r;
import java.io.IOException;
import java.util.HashMap;

/* compiled from: StartAppSDK */
public class a {
    private static a a;
    private boolean b = false;
    private boolean c = false;
    private HashMap<String, IspInfo> d = new HashMap<>();
    private IspInfo e;

    static {
        a.class.getSimpleName();
    }

    private a() {
    }

    public static a a() {
        if (a == null) {
            a = new a();
        }
        return a;
    }

    public final IspInfo a(WifiInfo wifiInfo) {
        IspInfo ispInfo = new IspInfo();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(c.d().r());
            sb.append("ispinfo");
            r a2 = WebApiClient.a(RequestMethod.GET, sb.toString());
            if (a2.b.length() > 0) {
                b bVar = (b) b.a(a2.b, b.class);
                if (bVar != null) {
                    ispInfo.AutonomousSystemNumber = g.a(bVar.AutonomousSystemNumber);
                    ispInfo.AutonomousSystemOrganization = g.a(bVar.AutonomousSystemOrganization);
                    ispInfo.IpAddress = g.a(bVar.IpAddress);
                    ispInfo.IspName = g.a(bVar.IspName);
                    ispInfo.IspOrganizationalName = g.a(bVar.IspOrganizationalName);
                    ispInfo.SuccessfulIspLookup = true;
                    if (wifiInfo != null) {
                        synchronized (this.d) {
                            this.d.put(wifiInfo.WifiBSSID_Full, ispInfo);
                        }
                    } else {
                        this.e = ispInfo;
                    }
                }
            }
        } catch (IOException e2) {
            new StringBuilder("getIspInfo: ").append(e2.getMessage());
        }
        return ispInfo;
    }
}
