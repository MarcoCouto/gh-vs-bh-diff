package com.startapp.networkTest.net;

import android.util.Log;
import com.startapp.sdk.adsbase.i.i;
import com.startapp.sdk.adsbase.i.r;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: StartAppSDK */
public abstract class WebApiClient {
    private static final String a = "WebApiClient";

    /* compiled from: StartAppSDK */
    public enum RequestMethod {
        POST,
        GET,
        PUT,
        DELETE
    }

    private static r a(RequestMethod requestMethod, String str, i[] iVarArr) throws IOException {
        r rVar = new r();
        URL url = new URL(str);
        new URL(str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod(requestMethod.toString());
        for (int i = 0; i < 2; i++) {
            i iVar = iVarArr[i];
            httpURLConnection.setRequestProperty(iVar.a, iVar.b);
        }
        httpURLConnection.setConnectTimeout(10000);
        httpURLConnection.setReadTimeout(10000);
        rVar.a = httpURLConnection.getResponseCode();
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            bufferedReader.close();
        } catch (Exception e) {
            String str2 = a;
            StringBuilder sb2 = new StringBuilder("read content: ");
            sb2.append(e.getMessage());
            Log.e(str2, sb2.toString());
        }
        httpURLConnection.disconnect();
        rVar.b = sb.toString();
        return rVar;
    }

    public static r a(RequestMethod requestMethod, String str) throws IOException {
        return a(requestMethod, str, new i[]{new i("Content-Type", "application/json; charset=UTF-8"), new i("Accept", "application/json")});
    }
}
