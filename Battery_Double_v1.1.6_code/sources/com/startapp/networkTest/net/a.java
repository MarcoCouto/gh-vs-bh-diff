package com.startapp.networkTest.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

/* compiled from: StartAppSDK */
public class a {
    /* access modifiers changed from: private */
    public static final String a = "a";
    /* access modifiers changed from: private */
    public Object b;
    /* access modifiers changed from: private */
    public InetAddress c;

    public final String a(final String str) throws UnknownHostException {
        String hostAddress;
        this.b = new Object();
        Thread thread = new Thread(new Runnable() {
            public final void run() {
                try {
                    InetAddress byName = InetAddress.getByName(str);
                    synchronized (a.this.b) {
                        a.this.c = byName;
                    }
                } catch (Exception e) {
                    a.a;
                    new StringBuilder("resolveHostname: ").append(e.toString());
                }
            }
        });
        thread.start();
        try {
            thread.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (this.b) {
            if (this.c != null) {
                hostAddress = this.c.getHostAddress();
            } else {
                throw new UnknownHostException();
            }
        }
        return hostAddress;
    }
}
