package com.startapp.networkTest.c;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.startapp.networkTest.controller.LocationController;
import com.startapp.networkTest.controller.LocationController.ProviderMode;
import com.startapp.networkTest.controller.c;
import com.startapp.networkTest.d;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.TriggerEvents;
import com.startapp.networkTest.enums.voice.CallStates;
import com.startapp.networkTest.results.NetworkInformationResult;
import java.util.ArrayList;
import java.util.Arrays;

/* compiled from: StartAppSDK */
public class b {
    private SharedPreferences a;
    private Context b;
    private d c;
    private String d;
    private c e;
    private com.startapp.networkTest.controller.d f;
    private LocationController g;
    private TelephonyManager h;
    private a i;
    private C0066b j;
    private int k = 0;
    private int l;
    private boolean m;

    /* compiled from: StartAppSDK */
    class a {
        String a;
        double b;
        double c;

        a(String str, double d2, double d3) {
            this.a = str;
            this.b = d2;
            this.c = d3;
        }
    }

    /* renamed from: com.startapp.networkTest.c.b$b reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    class C0066b {
        String a;
        String b;
        String c;
        NetworkTypes d;
        String e;
        String f;
        int g;

        private C0066b() {
            this.a = "";
            this.b = "";
            this.c = "";
            this.d = NetworkTypes.Unknown;
            this.e = "";
            this.f = "";
        }

        /* synthetic */ C0066b(b bVar, byte b2) {
            this();
        }
    }

    static {
        b.class.getSimpleName();
    }

    public b(Context context) {
        this.b = context;
        this.c = new d(context);
        this.d = com.startapp.networkTest.c.d().a();
        this.a = context.getSharedPreferences("p3insnir", 0);
        this.h = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        if (VERSION.SDK_INT >= 24 && this.h != null) {
            int i2 = com.startapp.networkTest.controller.b.f(context).SubscriptionId;
            if (i2 != -1) {
                this.h = this.h.createForSubscriptionId(i2);
            }
        }
        this.e = new c(this.b);
        this.f = new com.startapp.networkTest.controller.d(this.b);
        this.g = new LocationController(this.b);
        this.j = new C0066b(this, 0);
        this.m = com.startapp.networkTest.c.d().x();
        this.l = com.startapp.networkTest.c.d().y();
        if (this.l <= 0) {
            this.l = 1;
        }
    }

    public final void a() {
        this.g.a(ProviderMode.Passive);
        this.e.a();
    }

    public final void b() {
        this.g.a();
        this.e.b();
    }

    public final NetworkInformationResult a(TriggerEvents triggerEvents, boolean z) {
        return a(this.g.b(), triggerEvents, z);
    }

    public final NetworkInformationResult a(LocationInfo locationInfo, TriggerEvents triggerEvents, boolean z) {
        CallStates callStates;
        NetworkInformationResult networkInformationResult = new NetworkInformationResult(this.d, this.c.a());
        if (locationInfo != null) {
            networkInformationResult.LocationInfo = locationInfo;
        } else {
            networkInformationResult.LocationInfo = this.g.b();
        }
        networkInformationResult.TimeInfo = com.startapp.networkTest.e.b.a();
        networkInformationResult.Timestamp = networkInformationResult.TimeInfo.TimestampTableau;
        networkInformationResult.timestampMillis = networkInformationResult.TimeInfo.TimestampMillis;
        networkInformationResult.NirId = com.iab.omid.library.startapp.b.a(networkInformationResult.TimeInfo, networkInformationResult.GUID);
        networkInformationResult.WifiInfo = this.f.a();
        networkInformationResult.TriggerEvent = triggerEvents;
        networkInformationResult.ScreenState = com.startapp.networkTest.controller.b.d(this.b);
        if (this.h != null) {
            switch (this.h.getCallState()) {
                case 0:
                    callStates = CallStates.Idle;
                    break;
                case 1:
                    callStates = CallStates.Ringing;
                    break;
                case 2:
                    callStates = CallStates.Offhook;
                    break;
                default:
                    callStates = CallStates.Unknown;
                    break;
            }
        } else {
            callStates = CallStates.Unknown;
        }
        networkInformationResult.CallState = callStates;
        if (this.m) {
            int i2 = this.k;
            this.k = i2 + 1;
            if (i2 % this.l == 0 || z) {
                networkInformationResult.CellInfo = new ArrayList<>(Arrays.asList(this.e.d()));
            }
        }
        networkInformationResult.RadioInfo = this.e.c();
        String str = "";
        synchronized (this) {
            if (this.i == null) {
                String string = this.a.getString("P3INS_PFK_NIR_FIRSTCELLID_GSMCELLID", "");
                if (!string.isEmpty()) {
                    a aVar = new a(string, Double.longBitsToDouble(this.a.getLong("P3INS_PFK_NIR_FIRSTCELLID_LATITUDE", 0)), Double.longBitsToDouble(this.a.getLong("P3INS_PFK_NIR_FIRSTCELLID_LONGITUDE", 0)));
                    this.i = aVar;
                }
            }
            if (!networkInformationResult.RadioInfo.GsmCellId.isEmpty()) {
                if (networkInformationResult.LocationInfo.LocationAge < 30000 && (this.i == null || !this.i.a.equals(networkInformationResult.RadioInfo.GsmCellId))) {
                    a aVar2 = new a(networkInformationResult.RadioInfo.GsmCellId, networkInformationResult.LocationInfo.LocationLatitude, networkInformationResult.LocationInfo.LocationLongitude);
                    this.i = aVar2;
                    networkInformationResult.CellIdDeltaDistance = Utils.DOUBLE_EPSILON;
                    a(this.i);
                }
                str = networkInformationResult.RadioInfo.GsmCellId;
            } else if (!networkInformationResult.RadioInfo.CdmaBaseStationId.isEmpty()) {
                if (networkInformationResult.LocationInfo.LocationAge < 30000 && (this.i == null || !this.i.a.equals(networkInformationResult.RadioInfo.CdmaBaseStationId))) {
                    a aVar3 = new a(networkInformationResult.RadioInfo.CdmaBaseStationId, networkInformationResult.LocationInfo.LocationLatitude, networkInformationResult.LocationInfo.LocationLongitude);
                    this.i = aVar3;
                    networkInformationResult.CellIdDeltaDistance = Utils.DOUBLE_EPSILON;
                    a(this.i);
                }
                str = networkInformationResult.RadioInfo.CdmaBaseStationId;
            }
        }
        if ((!networkInformationResult.RadioInfo.GsmCellId.isEmpty() && networkInformationResult.CellIdDeltaDistance == -1.0d && this.i.a.equals(networkInformationResult.RadioInfo.GsmCellId)) || (!networkInformationResult.RadioInfo.CdmaBaseStationId.isEmpty() && networkInformationResult.CellIdDeltaDistance == -1.0d && this.i.a.equals(networkInformationResult.RadioInfo.CdmaBaseStationId))) {
            double d2 = this.i.b;
            double d3 = this.i.c;
            double d4 = networkInformationResult.LocationInfo.LocationLatitude;
            double radians = Math.toRadians(networkInformationResult.LocationInfo.LocationLongitude - d3) * Math.cos(Math.toRadians(d2 + d4) / 2.0d);
            double radians2 = Math.toRadians(d4 - d2);
            networkInformationResult.CellIdDeltaDistance = Math.sqrt((radians * radians) + (radians2 * radians2)) * 6371000.0d;
        }
        if (!str.isEmpty() && !str.equals(this.j.a)) {
            networkInformationResult.PrevNirId = this.j.b;
            networkInformationResult.PrevCellId = this.j.a;
            networkInformationResult.PrevLAC = this.j.c;
            networkInformationResult.PrevNetworkType = this.j.d;
            networkInformationResult.PrevMCC = this.j.e;
            networkInformationResult.PrevMNC = this.j.f;
            networkInformationResult.PrevRXLevel = this.j.g;
        }
        C0066b bVar = this.j;
        String str2 = networkInformationResult.NirId;
        String str3 = networkInformationResult.RadioInfo.GsmLAC;
        NetworkTypes networkTypes = networkInformationResult.RadioInfo.NetworkType;
        String str4 = networkInformationResult.RadioInfo.MCC;
        String str5 = networkInformationResult.RadioInfo.MNC;
        int i3 = networkInformationResult.RadioInfo.RXLevel;
        bVar.b = str2;
        bVar.a = str;
        bVar.c = str3;
        bVar.d = networkTypes;
        bVar.e = str4;
        bVar.f = str5;
        bVar.g = i3;
        return networkInformationResult;
    }

    @SuppressLint({"ApplySharedPref"})
    private void a(a aVar) {
        this.a.edit().putString("P3INS_PFK_NIR_FIRSTCELLID_GSMCELLID", aVar.a).commit();
        this.a.edit().putLong("P3INS_PFK_NIR_FIRSTCELLID_LATITUDE", Double.doubleToRawLongBits(aVar.b)).commit();
        this.a.edit().putLong("P3INS_PFK_NIR_FIRSTCELLID_LONGITUDE", Double.doubleToRawLongBits(aVar.c)).commit();
    }

    public final void a(com.startapp.networkTest.controller.LocationController.b bVar) {
        if (this.g != null) {
            this.g.a(bVar);
        }
    }

    public final void c() {
        if (this.g != null) {
            this.g.a((com.startapp.networkTest.controller.LocationController.b) null);
        }
    }

    public final void a(com.startapp.networkTest.controller.a.a aVar) {
        if (this.e != null) {
            this.e.a(aVar);
        }
    }

    public final void b(com.startapp.networkTest.controller.a.a aVar) {
        if (this.e != null) {
            this.e.b(aVar);
        }
    }

    public final c d() {
        return this.e;
    }
}
