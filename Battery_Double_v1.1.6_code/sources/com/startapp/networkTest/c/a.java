package com.startapp.networkTest.c;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.util.Log;
import com.facebook.places.model.PlaceFields;
import com.startapp.networkTest.controller.LocationController;
import com.startapp.networkTest.controller.LocationController.ProviderMode;
import com.startapp.networkTest.controller.b;
import com.startapp.networkTest.controller.c;
import com.startapp.networkTest.controller.d;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.enums.LtrCriteriaTypes;
import com.startapp.networkTest.enums.MeasurementTypes;
import com.startapp.networkTest.enums.SpeedtestEndStates;
import com.startapp.networkTest.results.LatencyResult;
import com.startapp.networkTest.results.P3TestResult;
import com.startapp.networkTest.results.speedtest.MeasurementPointLatency;
import com.startapp.networkTest.speedtest.SpeedtestEngineError;
import com.startapp.networkTest.speedtest.SpeedtestEngineStatus;
import com.startapp.networkTest.utils.e;
import com.startapp.networkTest.utils.g;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/* compiled from: StartAppSDK */
public class a {
    public static final String a = "a";
    /* access modifiers changed from: private */
    public com.startapp.networkTest.speedtest.a b;
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public d e;
    /* access modifiers changed from: private */
    public LocationController f;
    /* access modifiers changed from: private */
    public com.startapp.networkTest.controller.a g;
    /* access modifiers changed from: private */
    public P3TestResult h;
    private ArrayList<com.startapp.networkTest.data.d> i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public com.startapp.networkTest.d k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public String m = "";
    private String n = "";
    private String o = "";
    private String p = "";
    private String q = "";
    private String r = "";
    /* access modifiers changed from: private */
    public String s = "";

    /* renamed from: com.startapp.networkTest.c.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    class C0065a extends AsyncTask<Void, Void, LatencyResult> {
        private String a;
        private int b = 10;
        private int c = 200;
        private int d = 30000;
        private int e = 56;
        private String[] f;
        private LtrCriteriaTypes g;
        private boolean h = true;

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            LatencyResult latencyResult = (LatencyResult) obj;
            super.onPostExecute(latencyResult);
            a.this.h = latencyResult;
            if (latencyResult != null) {
                if (a.this.b != null) {
                    com.startapp.networkTest.speedtest.a a2 = a.this.b;
                    SpeedtestEngineStatus speedtestEngineStatus = SpeedtestEngineStatus.END;
                    SpeedtestEngineError speedtestEngineError = SpeedtestEngineError.OK;
                    a2.a(speedtestEngineStatus);
                }
            } else if (a.this.b != null) {
                com.startapp.networkTest.speedtest.a a3 = a.this.b;
                SpeedtestEngineStatus speedtestEngineStatus2 = SpeedtestEngineStatus.ABORTED;
                SpeedtestEngineError speedtestEngineError2 = SpeedtestEngineError.OK;
                a3.a(speedtestEngineStatus2);
            }
        }

        public C0065a(String str) {
            this.a = str;
            if (this.c < 200) {
                this.c = 200;
            }
            if (a.this.b != null) {
                com.startapp.networkTest.speedtest.a a2 = a.this.b;
                SpeedtestEngineStatus speedtestEngineStatus = SpeedtestEngineStatus.CONNECT;
                SpeedtestEngineError speedtestEngineError = SpeedtestEngineError.OK;
                a2.a(speedtestEngineStatus);
            }
            com.startapp.networkTest.d c2 = com.startapp.networkTest.c.c();
            this.f = c2.m();
            this.g = LtrCriteriaTypes.valueOf(c2.o());
        }

        /* JADX WARNING: Removed duplicated region for block: B:102:0x030e A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0157 A[Catch:{ Exception -> 0x023e }] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0189 A[Catch:{ Exception -> 0x0238 }] */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x0272  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x02e9  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x02f0  */
        /* JADX WARNING: Removed duplicated region for block: B:92:0x0316 A[LOOP:0: B:9:0x0027->B:92:0x0316, LOOP_END] */
        private LatencyResult a() {
            List a2;
            String str;
            long j;
            boolean z;
            int i2;
            int i3;
            int i4;
            boolean z2;
            LatencyResult latencyResult = null;
            if (isCancelled()) {
                return null;
            }
            if (this.h) {
                a2 = a(this.f, this.g, this.a);
            } else {
                a2 = a(this.f, LtrCriteriaTypes.CTItem, this.a);
            }
            List list = a2;
            LatencyResult latencyResult2 = null;
            int i5 = 0;
            while (true) {
                if (i5 >= list.size()) {
                    break;
                }
                long elapsedRealtime = SystemClock.elapsedRealtime();
                long uptimeMillis = SystemClock.uptimeMillis();
                com.startapp.networkTest.d.a.d dVar = (com.startapp.networkTest.d.a.d) list.get(i5);
                dVar.totalTests++;
                this.a = dVar.address;
                ArrayList arrayList = new ArrayList();
                LatencyResult latencyResult3 = new LatencyResult(a.this.j, a.this.k.a());
                latencyResult3.BatteryInfoOnStart = a.this.g.a();
                latencyResult3.LocationInfoOnStart = a.this.f.b();
                latencyResult3.ScreenStateOnStart = b.d(a.this.c);
                latencyResult3.MeasurementType = MeasurementTypes.IPING;
                latencyResult3.TimeInfoOnStart = com.startapp.networkTest.e.b.a();
                latencyResult3.LtrId = com.iab.omid.library.startapp.b.a(latencyResult3.TimeInfoOnStart, latencyResult3.GUID);
                latencyResult3.MemoryInfoOnStart = b.b(a.this.c);
                latencyResult3.RadioInfoOnStart = a.this.d.c();
                latencyResult3.WifiInfoOnStart = a.this.e.a();
                latencyResult3.TrafficInfoOnStart = b.a(a.this.e);
                latencyResult3.DeviceInfo = b.a(a.this.c);
                latencyResult3.Pings = this.b;
                latencyResult3.Pause = this.c;
                latencyResult3.Server = this.a;
                String str2 = "ping";
                try {
                    InetAddress byName = InetAddress.getByName(this.a);
                    str = byName.getHostAddress();
                    try {
                        if (byName instanceof Inet6Address) {
                            str2 = "ping6";
                        }
                    } catch (UnknownHostException e2) {
                        e = e2;
                        e.printStackTrace();
                        String str3 = str;
                        StringBuilder sb = new StringBuilder();
                        sb.append(str2);
                        sb.append(" -i ");
                        int i6 = i5;
                        double d2 = (double) this.c;
                        Double.isNaN(d2);
                        sb.append(d2 / 1000.0d);
                        sb.append(" -W ");
                        double d3 = (double) this.d;
                        Double.isNaN(d3);
                        sb.append(d3 / 1000.0d);
                        sb.append(" -c ");
                        sb.append(this.b);
                        sb.append(" -s ");
                        sb.append(this.e);
                        sb.append(" ");
                        sb.append(str3);
                        String sb2 = sb.toString();
                        if (a.this.b != null) {
                        }
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(sb2).getInputStream()));
                        bufferedReader.readLine();
                        long elapsedRealtime2 = SystemClock.elapsedRealtime();
                        i4 = 0;
                        z = false;
                        i3 = 0;
                        while (i4 < this.b) {
                            try {
                            } catch (Exception e3) {
                                e = e3;
                                boolean z3 = z;
                                j = elapsedRealtime;
                                String str4 = a.a;
                                StringBuilder sb3 = new StringBuilder("IcmpTestAsyncTask: ");
                                sb3.append(e.getMessage());
                                Log.e(str4, sb3.toString());
                                e.printStackTrace();
                                i2 = i3;
                                latencyResult3.TestEndState = SpeedtestEndStates.Finish;
                                latencyResult3.TestErrorReason = SpeedtestEngineError.OK;
                                latencyResult3.Success = z;
                                latencyResult3.SuccessfulPings = i2;
                                if (arrayList.size() > 0) {
                                }
                                latencyResult3.BatteryInfoOnEnd = a.this.g.a();
                                latencyResult3.LocationInfoOnEnd = a.this.f.b();
                                latencyResult3.ScreenStateOnEnd = b.d(a.this.c);
                                latencyResult3.MemoryInfoOnEnd = b.b(a.this.c);
                                latencyResult3.RadioInfoOnEnd = a.this.d.c();
                                latencyResult3.TimeInfoOnEnd = com.startapp.networkTest.e.b.a();
                                latencyResult3.WifiInfoOnEnd = a.this.e.a();
                                latencyResult3.TrafficInfoOnEnd = b.a(a.this.e);
                                latencyResult3.DurationOverallNoSleep = SystemClock.uptimeMillis() - uptimeMillis;
                                latencyResult3.DurationOverall = SystemClock.elapsedRealtime() - j;
                                latencyResult3.AirportCode = this.g != LtrCriteriaTypes.CTItem ? a.this.s : e.a(this.a);
                                latencyResult3.Meta = a.this.m;
                                latencyResult3.QuestionnaireName = g.a(a.this.l);
                                if (!z) {
                                }
                            }
                        }
                        z2 = z;
                        j = elapsedRealtime;
                        bufferedReader.close();
                        i2 = i3;
                        z = z2;
                        latencyResult3.TestEndState = SpeedtestEndStates.Finish;
                        latencyResult3.TestErrorReason = SpeedtestEngineError.OK;
                        latencyResult3.Success = z;
                        latencyResult3.SuccessfulPings = i2;
                        if (arrayList.size() > 0) {
                        }
                        latencyResult3.BatteryInfoOnEnd = a.this.g.a();
                        latencyResult3.LocationInfoOnEnd = a.this.f.b();
                        latencyResult3.ScreenStateOnEnd = b.d(a.this.c);
                        latencyResult3.MemoryInfoOnEnd = b.b(a.this.c);
                        latencyResult3.RadioInfoOnEnd = a.this.d.c();
                        latencyResult3.TimeInfoOnEnd = com.startapp.networkTest.e.b.a();
                        latencyResult3.WifiInfoOnEnd = a.this.e.a();
                        latencyResult3.TrafficInfoOnEnd = b.a(a.this.e);
                        latencyResult3.DurationOverallNoSleep = SystemClock.uptimeMillis() - uptimeMillis;
                        latencyResult3.DurationOverall = SystemClock.elapsedRealtime() - j;
                        latencyResult3.AirportCode = this.g != LtrCriteriaTypes.CTItem ? a.this.s : e.a(this.a);
                        latencyResult3.Meta = a.this.m;
                        latencyResult3.QuestionnaireName = g.a(a.this.l);
                        if (!z) {
                        }
                    }
                } catch (UnknownHostException e4) {
                    e = e4;
                    str = this.a;
                    e.printStackTrace();
                    String str32 = str;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str2);
                    sb4.append(" -i ");
                    int i62 = i5;
                    double d22 = (double) this.c;
                    Double.isNaN(d22);
                    sb4.append(d22 / 1000.0d);
                    sb4.append(" -W ");
                    double d32 = (double) this.d;
                    Double.isNaN(d32);
                    sb4.append(d32 / 1000.0d);
                    sb4.append(" -c ");
                    sb4.append(this.b);
                    sb4.append(" -s ");
                    sb4.append(this.e);
                    sb4.append(" ");
                    sb4.append(str32);
                    String sb22 = sb4.toString();
                    if (a.this.b != null) {
                    }
                    BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(sb22).getInputStream()));
                    bufferedReader2.readLine();
                    long elapsedRealtime22 = SystemClock.elapsedRealtime();
                    i4 = 0;
                    z = false;
                    i3 = 0;
                    while (i4 < this.b) {
                    }
                    z2 = z;
                    j = elapsedRealtime;
                    bufferedReader2.close();
                    i2 = i3;
                    z = z2;
                    latencyResult3.TestEndState = SpeedtestEndStates.Finish;
                    latencyResult3.TestErrorReason = SpeedtestEngineError.OK;
                    latencyResult3.Success = z;
                    latencyResult3.SuccessfulPings = i2;
                    if (arrayList.size() > 0) {
                    }
                    latencyResult3.BatteryInfoOnEnd = a.this.g.a();
                    latencyResult3.LocationInfoOnEnd = a.this.f.b();
                    latencyResult3.ScreenStateOnEnd = b.d(a.this.c);
                    latencyResult3.MemoryInfoOnEnd = b.b(a.this.c);
                    latencyResult3.RadioInfoOnEnd = a.this.d.c();
                    latencyResult3.TimeInfoOnEnd = com.startapp.networkTest.e.b.a();
                    latencyResult3.WifiInfoOnEnd = a.this.e.a();
                    latencyResult3.TrafficInfoOnEnd = b.a(a.this.e);
                    latencyResult3.DurationOverallNoSleep = SystemClock.uptimeMillis() - uptimeMillis;
                    latencyResult3.DurationOverall = SystemClock.elapsedRealtime() - j;
                    latencyResult3.AirportCode = this.g != LtrCriteriaTypes.CTItem ? a.this.s : e.a(this.a);
                    latencyResult3.Meta = a.this.m;
                    latencyResult3.QuestionnaireName = g.a(a.this.l);
                    if (!z) {
                    }
                }
                String str322 = str;
                StringBuilder sb42 = new StringBuilder();
                sb42.append(str2);
                sb42.append(" -i ");
                int i622 = i5;
                double d222 = (double) this.c;
                Double.isNaN(d222);
                sb42.append(d222 / 1000.0d);
                sb42.append(" -W ");
                double d322 = (double) this.d;
                Double.isNaN(d322);
                sb42.append(d322 / 1000.0d);
                sb42.append(" -c ");
                sb42.append(this.b);
                sb42.append(" -s ");
                sb42.append(this.e);
                sb42.append(" ");
                sb42.append(str322);
                String sb222 = sb42.toString();
                try {
                    if (a.this.b != null) {
                        com.startapp.networkTest.speedtest.a a3 = a.this.b;
                        SpeedtestEngineStatus speedtestEngineStatus = SpeedtestEngineStatus.PING;
                        SpeedtestEngineError speedtestEngineError = SpeedtestEngineError.OK;
                        a3.a(speedtestEngineStatus);
                    }
                    BufferedReader bufferedReader22 = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(sb222).getInputStream()));
                    bufferedReader22.readLine();
                    long elapsedRealtime222 = SystemClock.elapsedRealtime();
                    i4 = 0;
                    z = false;
                    i3 = 0;
                    while (i4 < this.b) {
                        if (isCancelled()) {
                            try {
                                bufferedReader22.close();
                                return latencyResult;
                            } catch (Exception e5) {
                                e = e5;
                            }
                        } else {
                            String readLine = bufferedReader22.readLine();
                            MeasurementPointLatency measurementPointLatency = new MeasurementPointLatency();
                            z2 = z;
                            j = elapsedRealtime;
                            try {
                                measurementPointLatency.Delta = SystemClock.elapsedRealtime() - elapsedRealtime222;
                                RadioInfo c2 = a.this.d.c();
                                measurementPointLatency.ConnectionType = c2.ConnectionType;
                                measurementPointLatency.NetworkType = c2.NetworkType;
                                measurementPointLatency.NrAvailable = c2.NrAvailable;
                                measurementPointLatency.NrState = c2.NrState;
                                measurementPointLatency.RxLev = c2.RXLevel;
                                if (readLine != null && readLine.length() > 0) {
                                    String[] split = readLine.split(" ");
                                    int i7 = -1;
                                    if (split.length == 8 || split.length == 9) {
                                        i7 = split.length - 2;
                                    }
                                    if (i7 == 6 || i7 == 7) {
                                        measurementPointLatency.Rtt = (int) Math.round(Double.parseDouble(split[i7].replace("time=", "")));
                                        int i8 = i3 + 1;
                                        try {
                                            if (a.this.b != null) {
                                                a.this.b;
                                            }
                                            i3 = i8;
                                            z = true;
                                            arrayList.add(measurementPointLatency);
                                            i4++;
                                            elapsedRealtime = j;
                                            latencyResult = null;
                                        } catch (Exception e6) {
                                            e = e6;
                                            i3 = i8;
                                            z = true;
                                            String str42 = a.a;
                                            StringBuilder sb32 = new StringBuilder("IcmpTestAsyncTask: ");
                                            sb32.append(e.getMessage());
                                            Log.e(str42, sb32.toString());
                                            e.printStackTrace();
                                            i2 = i3;
                                            latencyResult3.TestEndState = SpeedtestEndStates.Finish;
                                            latencyResult3.TestErrorReason = SpeedtestEngineError.OK;
                                            latencyResult3.Success = z;
                                            latencyResult3.SuccessfulPings = i2;
                                            if (arrayList.size() > 0) {
                                            }
                                            latencyResult3.BatteryInfoOnEnd = a.this.g.a();
                                            latencyResult3.LocationInfoOnEnd = a.this.f.b();
                                            latencyResult3.ScreenStateOnEnd = b.d(a.this.c);
                                            latencyResult3.MemoryInfoOnEnd = b.b(a.this.c);
                                            latencyResult3.RadioInfoOnEnd = a.this.d.c();
                                            latencyResult3.TimeInfoOnEnd = com.startapp.networkTest.e.b.a();
                                            latencyResult3.WifiInfoOnEnd = a.this.e.a();
                                            latencyResult3.TrafficInfoOnEnd = b.a(a.this.e);
                                            latencyResult3.DurationOverallNoSleep = SystemClock.uptimeMillis() - uptimeMillis;
                                            latencyResult3.DurationOverall = SystemClock.elapsedRealtime() - j;
                                            latencyResult3.AirportCode = this.g != LtrCriteriaTypes.CTItem ? a.this.s : e.a(this.a);
                                            latencyResult3.Meta = a.this.m;
                                            latencyResult3.QuestionnaireName = g.a(a.this.l);
                                            if (!z) {
                                            }
                                        }
                                    }
                                }
                                z = z2;
                                try {
                                    arrayList.add(measurementPointLatency);
                                    i4++;
                                    elapsedRealtime = j;
                                    latencyResult = null;
                                } catch (Exception e7) {
                                    e = e7;
                                    String str422 = a.a;
                                    StringBuilder sb322 = new StringBuilder("IcmpTestAsyncTask: ");
                                    sb322.append(e.getMessage());
                                    Log.e(str422, sb322.toString());
                                    e.printStackTrace();
                                    i2 = i3;
                                    latencyResult3.TestEndState = SpeedtestEndStates.Finish;
                                    latencyResult3.TestErrorReason = SpeedtestEngineError.OK;
                                    latencyResult3.Success = z;
                                    latencyResult3.SuccessfulPings = i2;
                                    if (arrayList.size() > 0) {
                                    }
                                    latencyResult3.BatteryInfoOnEnd = a.this.g.a();
                                    latencyResult3.LocationInfoOnEnd = a.this.f.b();
                                    latencyResult3.ScreenStateOnEnd = b.d(a.this.c);
                                    latencyResult3.MemoryInfoOnEnd = b.b(a.this.c);
                                    latencyResult3.RadioInfoOnEnd = a.this.d.c();
                                    latencyResult3.TimeInfoOnEnd = com.startapp.networkTest.e.b.a();
                                    latencyResult3.WifiInfoOnEnd = a.this.e.a();
                                    latencyResult3.TrafficInfoOnEnd = b.a(a.this.e);
                                    latencyResult3.DurationOverallNoSleep = SystemClock.uptimeMillis() - uptimeMillis;
                                    latencyResult3.DurationOverall = SystemClock.elapsedRealtime() - j;
                                    latencyResult3.AirportCode = this.g != LtrCriteriaTypes.CTItem ? a.this.s : e.a(this.a);
                                    latencyResult3.Meta = a.this.m;
                                    latencyResult3.QuestionnaireName = g.a(a.this.l);
                                    if (!z) {
                                    }
                                }
                            } catch (Exception e8) {
                                e = e8;
                                z = z2;
                                String str4222 = a.a;
                                StringBuilder sb3222 = new StringBuilder("IcmpTestAsyncTask: ");
                                sb3222.append(e.getMessage());
                                Log.e(str4222, sb3222.toString());
                                e.printStackTrace();
                                i2 = i3;
                                latencyResult3.TestEndState = SpeedtestEndStates.Finish;
                                latencyResult3.TestErrorReason = SpeedtestEngineError.OK;
                                latencyResult3.Success = z;
                                latencyResult3.SuccessfulPings = i2;
                                if (arrayList.size() > 0) {
                                }
                                latencyResult3.BatteryInfoOnEnd = a.this.g.a();
                                latencyResult3.LocationInfoOnEnd = a.this.f.b();
                                latencyResult3.ScreenStateOnEnd = b.d(a.this.c);
                                latencyResult3.MemoryInfoOnEnd = b.b(a.this.c);
                                latencyResult3.RadioInfoOnEnd = a.this.d.c();
                                latencyResult3.TimeInfoOnEnd = com.startapp.networkTest.e.b.a();
                                latencyResult3.WifiInfoOnEnd = a.this.e.a();
                                latencyResult3.TrafficInfoOnEnd = b.a(a.this.e);
                                latencyResult3.DurationOverallNoSleep = SystemClock.uptimeMillis() - uptimeMillis;
                                latencyResult3.DurationOverall = SystemClock.elapsedRealtime() - j;
                                latencyResult3.AirportCode = this.g != LtrCriteriaTypes.CTItem ? a.this.s : e.a(this.a);
                                latencyResult3.Meta = a.this.m;
                                latencyResult3.QuestionnaireName = g.a(a.this.l);
                                if (!z) {
                                }
                            }
                        }
                    }
                    z2 = z;
                    j = elapsedRealtime;
                    bufferedReader22.close();
                    i2 = i3;
                    z = z2;
                } catch (Exception e9) {
                    e = e9;
                    j = elapsedRealtime;
                    z = false;
                    i3 = 0;
                    String str42222 = a.a;
                    StringBuilder sb32222 = new StringBuilder("IcmpTestAsyncTask: ");
                    sb32222.append(e.getMessage());
                    Log.e(str42222, sb32222.toString());
                    e.printStackTrace();
                    i2 = i3;
                    latencyResult3.TestEndState = SpeedtestEndStates.Finish;
                    latencyResult3.TestErrorReason = SpeedtestEngineError.OK;
                    latencyResult3.Success = z;
                    latencyResult3.SuccessfulPings = i2;
                    if (arrayList.size() > 0) {
                    }
                    latencyResult3.BatteryInfoOnEnd = a.this.g.a();
                    latencyResult3.LocationInfoOnEnd = a.this.f.b();
                    latencyResult3.ScreenStateOnEnd = b.d(a.this.c);
                    latencyResult3.MemoryInfoOnEnd = b.b(a.this.c);
                    latencyResult3.RadioInfoOnEnd = a.this.d.c();
                    latencyResult3.TimeInfoOnEnd = com.startapp.networkTest.e.b.a();
                    latencyResult3.WifiInfoOnEnd = a.this.e.a();
                    latencyResult3.TrafficInfoOnEnd = b.a(a.this.e);
                    latencyResult3.DurationOverallNoSleep = SystemClock.uptimeMillis() - uptimeMillis;
                    latencyResult3.DurationOverall = SystemClock.elapsedRealtime() - j;
                    latencyResult3.AirportCode = this.g != LtrCriteriaTypes.CTItem ? a.this.s : e.a(this.a);
                    latencyResult3.Meta = a.this.m;
                    latencyResult3.QuestionnaireName = g.a(a.this.l);
                    if (!z) {
                    }
                }
                latencyResult3.TestEndState = SpeedtestEndStates.Finish;
                latencyResult3.TestErrorReason = SpeedtestEngineError.OK;
                latencyResult3.Success = z;
                latencyResult3.SuccessfulPings = i2;
                if (arrayList.size() > 0) {
                    latencyResult3.a(arrayList);
                    latencyResult3.b(latencyResult3.MeasurementPoints);
                }
                latencyResult3.BatteryInfoOnEnd = a.this.g.a();
                latencyResult3.LocationInfoOnEnd = a.this.f.b();
                latencyResult3.ScreenStateOnEnd = b.d(a.this.c);
                latencyResult3.MemoryInfoOnEnd = b.b(a.this.c);
                latencyResult3.RadioInfoOnEnd = a.this.d.c();
                latencyResult3.TimeInfoOnEnd = com.startapp.networkTest.e.b.a();
                latencyResult3.WifiInfoOnEnd = a.this.e.a();
                latencyResult3.TrafficInfoOnEnd = b.a(a.this.e);
                latencyResult3.DurationOverallNoSleep = SystemClock.uptimeMillis() - uptimeMillis;
                latencyResult3.DurationOverall = SystemClock.elapsedRealtime() - j;
                latencyResult3.AirportCode = this.g != LtrCriteriaTypes.CTItem ? a.this.s : e.a(this.a);
                latencyResult3.Meta = a.this.m;
                latencyResult3.QuestionnaireName = g.a(a.this.l);
                if (!z) {
                    dVar.successfulTests++;
                    latencyResult2 = latencyResult3;
                    break;
                }
                i5 = i622 + 1;
                latencyResult2 = latencyResult3;
                latencyResult = null;
            }
            if (this.g != LtrCriteriaTypes.CTItem) {
                a(list);
            }
            if (com.startapp.networkTest.c.d().B() && latencyResult2 != null) {
                latencyResult2.LocationInfoOnStart = new LocationInfo();
                latencyResult2.LocationInfoOnEnd = new LocationInfo();
            }
            return latencyResult2;
        }

        private static void a(List<com.startapp.networkTest.d.a.d> list) {
            HashSet hashSet = new HashSet();
            for (com.startapp.networkTest.d.a.d dVar : list) {
                hashSet.add(dVar.toString());
            }
            com.startapp.networkTest.c.c().b((Set<String>) hashSet);
        }

        private List<com.startapp.networkTest.d.a.d> a(String[] strArr, LtrCriteriaTypes ltrCriteriaTypes, String str) {
            LinkedList linkedList = new LinkedList();
            LinkedList linkedList2 = new LinkedList();
            Set<String> g2 = com.startapp.networkTest.c.c().g();
            LinkedList<com.startapp.networkTest.d.a.d> linkedList3 = new LinkedList<>();
            if (g2 != null) {
                for (String a2 : g2) {
                    com.startapp.networkTest.d.a.d dVar = (com.startapp.networkTest.d.a.d) com.startapp.common.parser.b.a(a2, com.startapp.networkTest.d.a.d.class);
                    if (dVar != null) {
                        linkedList3.add(dVar);
                    }
                }
            }
            for (String str2 : strArr) {
                com.startapp.networkTest.d.a.d dVar2 = new com.startapp.networkTest.d.a.d();
                dVar2.address = str2;
                linkedList2.add(dVar2);
            }
            for (com.startapp.networkTest.d.a.d dVar3 : linkedList3) {
                for (int i2 = 0; i2 < strArr.length; i2++) {
                    if (strArr[i2].equals(dVar3.address)) {
                        linkedList2.set(i2, dVar3);
                    }
                }
            }
            switch (ltrCriteriaTypes) {
                case CTItem:
                    com.startapp.networkTest.d.a.d dVar4 = new com.startapp.networkTest.d.a.d();
                    dVar4.address = str;
                    linkedList.add(dVar4);
                    return linkedList;
                case NoChange:
                    return linkedList2;
                case Random:
                    Collections.shuffle(linkedList2, new Random(System.nanoTime()));
                    return new LinkedList(linkedList2);
                case FullSuccessful:
                    Collections.sort(linkedList2, new Comparator<com.startapp.networkTest.d.a.d>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((com.startapp.networkTest.d.a.d) obj).successfulTests - ((com.startapp.networkTest.d.a.d) obj2).successfulTests;
                        }
                    });
                    return new LinkedList(linkedList2);
                case TotalTests:
                    Collections.sort(linkedList2, new Comparator<com.startapp.networkTest.d.a.d>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((com.startapp.networkTest.d.a.d) obj).totalTests - ((com.startapp.networkTest.d.a.d) obj2).totalTests;
                        }
                    });
                    return new LinkedList(linkedList2);
                default:
                    return linkedList;
            }
        }
    }

    public a(com.startapp.networkTest.speedtest.a aVar, Context context) {
        this.b = aVar;
        this.c = context;
        com.startapp.networkTest.a d2 = com.startapp.networkTest.c.d();
        this.j = d2.a();
        this.k = new com.startapp.networkTest.d(this.c);
        this.d = new c(context);
        this.e = new d(context);
        this.f = new LocationController(this.c);
        this.g = new com.startapp.networkTest.controller.a(this.c);
        this.i = new ArrayList<>();
        if (d2.t()) {
            context.getSystemService(PlaceFields.PHONE);
        }
    }

    public final P3TestResult a() {
        return this.h;
    }

    public final void b() {
        if (this.f != null) {
            this.f.a();
        }
        if (this.d != null) {
            this.d.b();
        }
    }

    public final void a(ProviderMode providerMode) {
        if (this.f != null) {
            this.f.a(providerMode);
        }
        if (this.d != null) {
            this.d.a();
        }
    }

    public final void a(String str) {
        this.m = str;
    }

    public final void b(String str) {
        this.s = str;
    }

    public final void c(String str) {
        this.l = str;
    }

    public final void d(String str) {
        this.i = new ArrayList<>();
        if (VERSION.SDK_INT < 11) {
            new C0065a(str).execute(new Void[0]);
        } else {
            new C0065a(str).executeOnExecutor(com.startapp.networkTest.threads.a.a().b(), new Void[0]);
        }
    }
}
