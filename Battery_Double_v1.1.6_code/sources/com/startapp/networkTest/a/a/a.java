package com.startapp.networkTest.a.a;

import android.util.Log;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: StartAppSDK */
public class a {
    private static final String a = "a";

    public static byte[] a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance(CommonUtils.SHA256_INSTANCE);
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            String str = a;
            StringBuilder sb = new StringBuilder("hash: ");
            sb.append(e.getMessage());
            Log.e(str, sb.toString());
            e.printStackTrace();
            return null;
        }
    }
}
