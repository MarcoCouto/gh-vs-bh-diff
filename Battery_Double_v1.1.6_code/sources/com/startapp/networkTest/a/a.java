package com.startapp.networkTest.a;

import android.content.Context;
import com.startapp.networkTest.enums.CtTestTypes;
import com.startapp.networkTest.utils.i;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/* compiled from: StartAppSDK */
public class a implements X509TrustManager {
    private static String d = "";
    private static boolean f = false;
    private static X509TrustManager g;
    private static X509TrustManager h;
    private static final X509TrustManager i = new X509TrustManager() {
        public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public final X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    };
    private X509TrustManager[] a;
    private CtTestTypes[] b;
    private String c = "";
    private CtTestTypes e = CtTestTypes.Unknown;

    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
    }

    static {
        a.class.getSimpleName();
    }

    public a(Context context, boolean z) {
        a(context, z);
        this.a = new X509TrustManager[3];
        this.b = new CtTestTypes[3];
        this.a[0] = g;
        this.b[0] = CtTestTypes.SSLOwnTs;
        this.a[1] = h;
        this.b[1] = CtTestTypes.SSLDeviceTs;
        this.a[2] = i;
        this.b[2] = CtTestTypes.SSLTrustAll;
        this.c = d;
    }

    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        int i2 = 0;
        while (i2 < this.a.length) {
            X509TrustManager x509TrustManager = this.a[i2];
            if (x509TrustManager != null) {
                try {
                    this.e = this.b[i2];
                    x509TrustManager.checkServerTrusted(x509CertificateArr, str);
                    return;
                } catch (CertificateException e2) {
                    if (i2 == 0) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(this.c);
                        sb.append(e2.getMessage());
                        this.c = sb.toString();
                    }
                    if (i2 + 1 == this.a.length) {
                        throw e2;
                    }
                }
            } else {
                i2++;
            }
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        return h.getAcceptedIssuers();
    }

    public final CtTestTypes a() {
        return this.e;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x0082 */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0094 A[Catch:{ Exception -> 0x00b2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ca A[EDGE_INSN: B:55:0x00ca->B:48:0x00ca ?: BREAK  
EDGE_INSN: B:55:0x00ca->B:48:0x00ca ?: BREAK  , SYNTHETIC] */
    private static void a(Context context, boolean z) {
        if (!f || z) {
            synchronized (a.class) {
                if (!f || z) {
                    d = "";
                    int i2 = 0;
                    try {
                        TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                        instance.init(null);
                        TrustManager[] trustManagers = instance.getTrustManagers();
                        int length = trustManagers.length;
                        int i3 = 0;
                        while (true) {
                            if (i3 < length) {
                                TrustManager trustManager = trustManagers[i3];
                                if (trustManager instanceof X509TrustManager) {
                                    h = (X509TrustManager) trustManager;
                                    break;
                                }
                                i3++;
                            }
                        }
                    } catch (Exception e2) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(d);
                        sb.append(e2.getMessage());
                        d = sb.toString();
                    }
                    try {
                        File b2 = i.b(context);
                        File c2 = i.c(context);
                        if (!b2.exists() || !c2.exists()) {
                            throw new KeyStoreException("Downloaded truststore not available");
                        } else if (i.a(b2, c2)) {
                            FileInputStream fileInputStream = new FileInputStream(b2);
                            KeyStore instance2 = KeyStore.getInstance("BKS");
                            instance2.load(fileInputStream, "R_hqKukfFZxKn52".toCharArray());
                            fileInputStream.close();
                            TrustManagerFactory instance3 = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                            instance3.init(instance2);
                            TrustManager[] trustManagers2 = instance3.getTrustManagers();
                            int length2 = trustManagers2.length;
                            while (true) {
                                if (i2 < length2) {
                                    break;
                                }
                                TrustManager trustManager2 = trustManagers2[i2];
                                if (trustManager2 instanceof X509TrustManager) {
                                    g = (X509TrustManager) trustManager2;
                                    break;
                                }
                                i2++;
                            }
                            f = true;
                            return;
                        } else {
                            throw new KeyStoreException("Verification of downloaded truststore failed");
                        }
                    } catch (Exception e3) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(d);
                        sb2.append(e3.getMessage());
                        d = sb2.toString();
                    }
                } else {
                    return;
                }
            }
        } else {
            return;
        }
    }
}
