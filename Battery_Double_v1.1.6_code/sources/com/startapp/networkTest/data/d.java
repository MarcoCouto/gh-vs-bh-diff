package com.startapp.networkTest.data;

import com.startapp.networkTest.enums.bluetooth.BluetoothBondStates;
import com.startapp.networkTest.enums.bluetooth.BluetoothDeviceClasses;
import com.startapp.networkTest.enums.bluetooth.BluetoothMajorDeviceClasses;
import com.startapp.networkTest.enums.bluetooth.BluetoothTypes;

/* compiled from: StartAppSDK */
public final class d {
    public String Answer;
    public BluetoothBondStates BondState = BluetoothBondStates.Unknown;
    public BluetoothDeviceClasses DeviceClass = BluetoothDeviceClasses.Unknown;
    public int Index;
    public BluetoothMajorDeviceClasses MajorDeviceClass = BluetoothMajorDeviceClasses.Unknown;
    public String Name = "";
    public BluetoothTypes Type = BluetoothTypes.Unknown;

    public final Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
