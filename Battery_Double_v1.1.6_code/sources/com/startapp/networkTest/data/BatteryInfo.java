package com.startapp.networkTest.data;

import com.startapp.networkTest.enums.BatteryChargePlugTypes;
import com.startapp.networkTest.enums.BatteryHealthStates;
import com.startapp.networkTest.enums.BatteryStatusStates;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class BatteryInfo implements Serializable, Cloneable {
    private static final long serialVersionUID = -937846764179533362L;
    public int BatteryCapacity;
    public BatteryChargePlugTypes BatteryChargePlug = BatteryChargePlugTypes.Unknown;
    public int BatteryCurrent;
    public BatteryHealthStates BatteryHealth = BatteryHealthStates.Unknown;
    public float BatteryLevel;
    public long BatteryRemainingEnergy;
    public BatteryStatusStates BatteryStatus = BatteryStatusStates.Unknown;
    public String BatteryTechnology = "";
    public String BatteryTemp = "";
    public int BatteryVoltage;
    public boolean MissingPermission = false;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BatteryLevel: ");
        sb.append(this.BatteryLevel);
        sb.append("% BatteryStatus: ");
        sb.append(this.BatteryStatus);
        sb.append(" BatteryHealth: ");
        sb.append(this.BatteryHealth);
        sb.append(" BatteryVoltage: ");
        sb.append(this.BatteryVoltage);
        sb.append(" mV BatteryTemp: ");
        sb.append(this.BatteryTemp);
        sb.append(" °C BatteryChargePlug: ");
        sb.append(this.BatteryChargePlug);
        sb.append(" BatteryTechnology: ");
        sb.append(this.BatteryTechnology);
        sb.append(" Battery Current ");
        sb.append(this.BatteryCurrent);
        sb.append(" mA BatteryCapacity ");
        sb.append(this.BatteryCapacity);
        sb.append(" mAh BatteryRemainingEnergy ");
        sb.append(this.BatteryRemainingEnergy);
        sb.append(" nWh");
        return sb.toString();
    }
}
