package com.startapp.networkTest.data.radio;

import com.startapp.networkTest.enums.ThreeStateShort;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class NetworkRegistrationInfo implements Serializable, Cloneable {
    private static final long serialVersionUID = 9179460790954950419L;
    public int Age = -1;
    public int Arfcn = -1;
    public String AvailableServices = "";
    public int Bandwidth = -1;
    public String CellId = "";
    public String CellTechnology = "";
    public ThreeStateShort DcNrRestricted = ThreeStateShort.Unknown;
    public String Domain = "";
    public boolean EmergencyEnabled = false;
    public ThreeStateShort EnDcAvailable = ThreeStateShort.Unknown;
    public int MaxDataCalls = -1;
    public String Mcc = "";
    public String Mnc = "";
    public String NetworkTechnology = "";
    public ThreeStateShort NrAvailable = ThreeStateShort.Unknown;
    public String NrState = "Unknown";
    public String OperatorLong = "";
    public String OperatorShort = "";
    public String Pci = "";
    public String ReasonForDenial = "";
    public String RegState = "";
    public String Tac = "";
    public String TransportType = "";

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
