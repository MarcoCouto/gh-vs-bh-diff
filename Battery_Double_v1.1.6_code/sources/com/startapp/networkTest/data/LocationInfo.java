package com.startapp.networkTest.data;

import com.startapp.networkTest.enums.LocationProviders;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class LocationInfo implements Serializable, Cloneable {
    private static final long serialVersionUID = -937846764579533362L;
    public double LocationAccuracyHorizontal;
    public double LocationAccuracyVertical;
    public long LocationAge = -1;
    public double LocationAltitude;
    public double LocationBearing;
    public double LocationLatitude;
    public double LocationLongitude;
    public LocationProviders LocationProvider = LocationProviders.Unknown;
    public double LocationSpeed;
    public String LocationTimestamp = "";
    public transient long locationTimestampMillis;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
