package com.startapp.networkTest.data;

import com.startapp.networkTest.enums.HotspotStates;
import com.startapp.networkTest.enums.WifiStates;
import com.startapp.networkTest.enums.wifi.WifiAuthAlgorithms;
import com.startapp.networkTest.enums.wifi.WifiDetailedStates;
import com.startapp.networkTest.enums.wifi.WifiGroupCiphers;
import com.startapp.networkTest.enums.wifi.WifiKeyManagements;
import com.startapp.networkTest.enums.wifi.WifiPairwiseCiphers;
import com.startapp.networkTest.enums.wifi.WifiProtocols;
import com.startapp.networkTest.enums.wifi.WifiSupplicantStates;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class WifiInfo implements Serializable, Cloneable {
    private static final long serialVersionUID = 8111287616823344527L;
    public HotspotStates HotspotState = HotspotStates.Unknown;
    public boolean MissingPermission = false;
    public WifiAuthAlgorithms WifiAuthAlgorithm = WifiAuthAlgorithms.Unknown;
    public String WifiBSSID = "";
    public transient String WifiBSSID_Full;
    public WifiDetailedStates WifiDetailedState = WifiDetailedStates.Unknown;
    public int WifiFrequency = 0;
    public WifiGroupCiphers WifiGroupCipher = WifiGroupCiphers.Unknown;
    public WifiKeyManagements WifiKeyManagement = WifiKeyManagements.Unknown;
    public int WifiLinkQuality = -1;
    public String WifiLinkSpeed = "";
    public transient long WifiLinkSpeedBps;
    public transient String WifiMacAddress = "";
    public WifiPairwiseCiphers WifiPairwiseCipher = WifiPairwiseCiphers.Unknown;
    public WifiProtocols WifiProtocol = WifiProtocols.Unknown;
    public int WifiRxLev;
    public String WifiSSID = "";
    public transient String WifiSSID_Full;
    public WifiStates WifiState = WifiStates.Unknown;
    public WifiSupplicantStates WifiSupplicantState = WifiSupplicantStates.Unknown;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("WifiState: ");
        sb2.append(this.WifiState);
        sb2.append("\n");
        sb.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder("WifiDetailedState: ");
        sb3.append(this.WifiDetailedState);
        sb3.append("\n");
        sb.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder("WifiSupplicantState: ");
        sb4.append(this.WifiSupplicantState);
        sb4.append("\n");
        sb.append(sb4.toString());
        StringBuilder sb5 = new StringBuilder("WifiProtocol: ");
        sb5.append(this.WifiProtocol);
        sb5.append("\n");
        sb.append(sb5.toString());
        StringBuilder sb6 = new StringBuilder("WifiGroupCipher: ");
        sb6.append(this.WifiGroupCipher);
        sb6.append("\n");
        sb.append(sb6.toString());
        StringBuilder sb7 = new StringBuilder("WifiAuthAlgorithm: ");
        sb7.append(this.WifiAuthAlgorithm);
        sb7.append("\n");
        sb.append(sb7.toString());
        StringBuilder sb8 = new StringBuilder("WifiPairwiseCipher: ");
        sb8.append(this.WifiPairwiseCipher);
        sb8.append("\n");
        sb.append(sb8.toString());
        StringBuilder sb9 = new StringBuilder("WifiFrequency: ");
        sb9.append(this.WifiFrequency);
        sb9.append("\n");
        sb.append(sb9.toString());
        StringBuilder sb10 = new StringBuilder("WifiLinkQuality: ");
        sb10.append(this.WifiLinkQuality);
        sb10.append("\n");
        sb.append(sb10.toString());
        StringBuilder sb11 = new StringBuilder("WifiLinkSpeed: ");
        sb11.append(this.WifiLinkSpeed);
        sb11.append("\n");
        sb.append(sb11.toString());
        StringBuilder sb12 = new StringBuilder("WifiRxLev: ");
        sb12.append(this.WifiRxLev);
        sb12.append("\n");
        sb.append(sb12.toString());
        StringBuilder sb13 = new StringBuilder("WifiBSSID: ");
        sb13.append(this.WifiBSSID);
        sb13.append("\n");
        sb.append(sb13.toString());
        StringBuilder sb14 = new StringBuilder("WifiSSID: ");
        sb14.append(this.WifiSSID);
        sb14.append("\n");
        sb.append(sb14.toString());
        StringBuilder sb15 = new StringBuilder("WifiMacAddress: ");
        sb15.append(this.WifiMacAddress);
        sb15.append("\n");
        sb.append(sb15.toString());
        return sb.toString();
    }
}
