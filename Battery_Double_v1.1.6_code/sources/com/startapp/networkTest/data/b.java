package com.startapp.networkTest.data;

import com.startapp.networkTest.enums.MemoryStates;

/* compiled from: StartAppSDK */
public final class b implements Cloneable {
    public long MemoryFree;
    public MemoryStates MemoryState = MemoryStates.Unknown;
    public long MemoryTotal;
    public long MemoryUsed;

    public final Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
