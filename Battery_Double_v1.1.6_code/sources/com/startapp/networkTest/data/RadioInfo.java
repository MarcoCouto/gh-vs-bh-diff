package com.startapp.networkTest.data;

import com.startapp.common.parser.d;
import com.startapp.networkTest.data.radio.NeighboringCell;
import com.startapp.networkTest.enums.ConnectionTypes;
import com.startapp.networkTest.enums.DuplexMode;
import com.startapp.networkTest.enums.FlightModeStates;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.PreferredNetworkTypes;
import com.startapp.networkTest.enums.ServiceStates;
import com.startapp.networkTest.enums.ThreeState;
import com.startapp.networkTest.enums.ThreeStateShort;
import com.startapp.networkTest.enums.radio.DataConnectionStates;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: StartAppSDK */
public class RadioInfo implements Serializable, Cloneable {
    public static final Integer INVALID = Integer.valueOf(Integer.MAX_VALUE);
    private static final long serialVersionUID = 4817753379835440169L;
    public int ARFCN = -1;
    public ThreeStateShort CarrierAggregation = ThreeStateShort.Unknown;
    public String CdmaBaseStationId = "";
    public String CdmaBaseStationLatitude = "";
    public String CdmaBaseStationLongitude = "";
    public int CdmaEcIo = INVALID.intValue();
    public String CdmaNetworkId = "";
    public String CdmaSystemId = "";
    public ConnectionTypes ConnectionType = ConnectionTypes.Unknown;
    public DuplexMode DuplexMode = DuplexMode.Unknown;
    public int EcN0 = 0;
    public FlightModeStates FlightMode = FlightModeStates.Unknown;
    public String GsmCellId = "";
    public int GsmCellIdAge = -1;
    public String GsmLAC = "";
    public transient boolean IsDefaultDataSim = true;
    public transient boolean IsDefaultVoiceSim = true;
    public ThreeStateShort IsMetered = ThreeStateShort.Unknown;
    public boolean IsRoaming = false;
    public int LteCqi = INVALID.intValue();
    public int LteRsrp = INVALID.intValue();
    public int LteRsrq = INVALID.intValue();
    public int LteRssi = INVALID.intValue();
    public int LteRssnr = INVALID.intValue();
    public String MCC = "";
    public String MNC = "";
    public ThreeStateShort ManualSelection = ThreeStateShort.Unknown;
    public boolean MissingPermission = false;
    public DataConnectionStates MobileDataConnectionState = DataConnectionStates.Unknown;
    public ThreeState MobileDataEnabled = ThreeState.Unknown;
    public int NativeDbm = INVALID.intValue();
    @d(b = ArrayList.class, c = NeighboringCell.class)
    public ArrayList<NeighboringCell> NeighboringCells = new ArrayList<>();
    public NetworkTypes NetworkType = NetworkTypes.Unknown;
    public ThreeStateShort NrAvailable = ThreeStateShort.Unknown;
    public int NrCsiRsrp = INVALID.intValue();
    public int NrCsiRsrq = INVALID.intValue();
    public int NrCsiSinr = INVALID.intValue();
    public int NrSsRsrp = INVALID.intValue();
    public int NrSsRsrq = INVALID.intValue();
    public int NrSsSinr = INVALID.intValue();
    public String NrState = "Unknown";
    public String OperatorName = "";
    public PreferredNetworkTypes PreferredNetwork = PreferredNetworkTypes.Unknown;
    public String PrimaryScramblingCode = "";
    public int RSCP = INVALID.intValue();
    public int RXLevel;
    public int RXLevelAge = -1;
    public ServiceStates ServiceState = ServiceStates.Unknown;
    public int ServiceStateAge = -1;
    public int SubscriptionId = -1;

    public Object clone() throws CloneNotSupportedException {
        RadioInfo radioInfo = (RadioInfo) super.clone();
        radioInfo.NeighboringCells = new ArrayList<>();
        Iterator it = this.NeighboringCells.iterator();
        while (it.hasNext()) {
            radioInfo.NeighboringCells.add((NeighboringCell) ((NeighboringCell) it.next()).clone());
        }
        return radioInfo;
    }
}
