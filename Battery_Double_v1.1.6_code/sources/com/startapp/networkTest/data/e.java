package com.startapp.networkTest.data;

/* compiled from: StartAppSDK */
public final class e implements Cloneable {
    public long StorageExternalAudio = -1;
    public long StorageExternalAvailable;
    public long StorageExternalImages = -1;
    public long StorageExternalSize;
    public long StorageExternalVideo = -1;
    public long StorageInternalAudio = -1;
    public long StorageInternalAvailable;
    public long StorageInternalImages = -1;
    public long StorageInternalSize;
    public long StorageInternalVideo = -1;

    public final Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
