package com.startapp.networkTest.results;

import com.startapp.common.parser.d;
import com.startapp.networkTest.controller.c;
import com.startapp.networkTest.data.BatteryInfo;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.data.TimeInfo;
import com.startapp.networkTest.data.WifiInfo;
import com.startapp.networkTest.data.a;
import com.startapp.networkTest.data.b;
import com.startapp.networkTest.data.f;
import com.startapp.networkTest.enums.ConnectionTypes;
import com.startapp.networkTest.enums.IpVersions;
import com.startapp.networkTest.enums.MeasurementTypes;
import com.startapp.networkTest.enums.NetworkGenerations;
import com.startapp.networkTest.enums.SpeedtestEndStates;
import com.startapp.networkTest.results.speedtest.MeasurementPointBase;
import com.startapp.networkTest.speedtest.SpeedtestEngineError;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: StartAppSDK */
public class P3TestResult extends BaseResult {
    public int AvgValue;
    @d(a = true)
    public BatteryInfo BatteryInfoOnEnd = new BatteryInfo();
    @d(a = true)
    public BatteryInfo BatteryInfoOnStart = new BatteryInfo();
    public String CampaignId = "";
    public long ConnectingTimeControlServer = -1;
    public long ConnectingTimeTestServerControl = -1;
    public long ConnectingTimeTestServerSockets = -1;
    public String CustomerID = "";
    @d(a = true)
    public a DeviceInfo = new a();
    public String IMEI = "";
    public String IMSI = "";
    public IpVersions IpVersion = IpVersions.Unknown;
    @d(a = true)
    public LocationInfo LocationInfoOnEnd = new LocationInfo();
    @d(a = true)
    public LocationInfo LocationInfoOnStart = new LocationInfo();
    public int MaxValue;
    public MeasurementTypes MeasurementType = MeasurementTypes.Unknown;
    public int MedValue;
    @d(a = true)
    public b MemoryInfoOnEnd = new b();
    @d(a = true)
    public b MemoryInfoOnStart = new b();
    public String Meta = "";
    public int MinValue;
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    public ArrayList<com.startapp.networkTest.data.d> QuestionAnswerList = new ArrayList<>();
    public String QuestionnaireName = "";
    @d(a = true)
    public RadioInfo RadioInfoOnEnd = new RadioInfo();
    @d(a = true)
    public RadioInfo RadioInfoOnStart = new RadioInfo();
    public double RatShare2G;
    public double RatShare3G;
    public double RatShare4G;
    public double RatShare5G;
    public double RatShareUnknown;
    public double RatShareWiFi;
    public String SequenceID = "";
    public String Server = "";
    public boolean Success;
    public SpeedtestEndStates TestEndState = SpeedtestEndStates.Unknown;
    public SpeedtestEngineError TestErrorReason = SpeedtestEngineError.OK;
    @d(a = true)
    public TimeInfo TimeInfoOnEnd = new TimeInfo();
    @d(a = true)
    public TimeInfo TimeInfoOnStart = new TimeInfo();
    @d(a = true)
    public f TrafficInfoOnEnd = new f();
    @d(a = true)
    public f TrafficInfoOnStart = new f();
    @d(a = true)
    public WifiInfo WifiInfoOnEnd = new WifiInfo();
    @d(a = true)
    public WifiInfo WifiInfoOnStart = new WifiInfo();

    public P3TestResult(String str, String str2) {
        super(str, str2);
    }

    public final void b(ArrayList<? extends MeasurementPointBase> arrayList) {
        Iterator it = arrayList.iterator();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (it.hasNext()) {
            MeasurementPointBase measurementPointBase = (MeasurementPointBase) it.next();
            if (measurementPointBase.ConnectionType == ConnectionTypes.Unknown) {
                i2++;
            } else if (measurementPointBase.ConnectionType == ConnectionTypes.Mobile) {
                NetworkGenerations a = c.a(measurementPointBase.NetworkType);
                if (measurementPointBase.NrState.equals("CONNECTED")) {
                    a = NetworkGenerations.Gen5;
                }
                switch (a) {
                    case Gen2:
                        i3++;
                        break;
                    case Gen3:
                        i4++;
                        break;
                    case Gen4:
                        i5++;
                        break;
                    case Gen5:
                        i6++;
                        break;
                    default:
                        i2++;
                        break;
                }
            } else {
                i7++;
            }
            i++;
        }
        if (i > 0) {
            double d = (double) i3;
            double d2 = (double) i;
            Double.isNaN(d);
            Double.isNaN(d2);
            this.RatShare2G = d / d2;
            double d3 = (double) i4;
            Double.isNaN(d3);
            Double.isNaN(d2);
            this.RatShare3G = d3 / d2;
            double d4 = (double) i5;
            Double.isNaN(d4);
            Double.isNaN(d2);
            this.RatShare4G = d4 / d2;
            double d5 = (double) i6;
            Double.isNaN(d5);
            Double.isNaN(d2);
            this.RatShare5G = d5 / d2;
            double d6 = (double) i7;
            Double.isNaN(d6);
            Double.isNaN(d2);
            this.RatShareWiFi = d6 / d2;
            double d7 = (double) i2;
            Double.isNaN(d7);
            Double.isNaN(d2);
            this.RatShareUnknown = d7 / d2;
        }
    }
}
