package com.startapp.networkTest.results.speedtest;

import com.startapp.networkTest.enums.ConnectionTypes;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.ThreeStateShort;

/* compiled from: StartAppSDK */
public class MeasurementPointBase implements Cloneable {
    public ConnectionTypes ConnectionType = ConnectionTypes.Unknown;
    public long Delta;
    public NetworkTypes NetworkType = NetworkTypes.Unknown;
    public ThreeStateShort NrAvailable = ThreeStateShort.Unknown;
    public String NrState = "Unknown";
    public int RxLev;

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
