package com.startapp.networkTest.results;

import com.startapp.common.parser.d;
import com.startapp.networkTest.data.BatteryInfo;
import com.startapp.networkTest.data.IspInfo;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.data.TimeInfo;
import com.startapp.networkTest.data.WifiInfo;
import com.startapp.networkTest.data.a;
import com.startapp.networkTest.data.b;
import com.startapp.networkTest.data.c;
import com.startapp.networkTest.data.e;
import com.startapp.networkTest.data.f;
import com.startapp.networkTest.data.radio.ApnInfo;
import com.startapp.networkTest.data.radio.CellInfo;
import com.startapp.networkTest.data.radio.NetworkRegistrationInfo;
import com.startapp.networkTest.enums.CtTestTypes;
import com.startapp.networkTest.enums.IdleStates;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.ScreenStates;
import com.startapp.networkTest.enums.voice.CallStates;
import java.util.ArrayList;

/* compiled from: StartAppSDK */
public class ConnectivityTestResult extends BaseResult {
    public String AirportCode = "";
    public String AmazonId = "";
    @d(b = ArrayList.class, c = ApnInfo.class)
    public ArrayList<ApnInfo> ApnInfo = new ArrayList<>();
    @d(a = true)
    public BatteryInfo BatteryInfo = new BatteryInfo();
    public long BytesRead = -1;
    public CallStates CallState = CallStates.Unknown;
    @d(b = ArrayList.class, c = CellInfo.class)
    public ArrayList<CellInfo> CellInfo = new ArrayList<>();
    public String CtId = "";
    @d(a = true)
    public a DeviceInfo = new a();
    public long DurationDNS = -1;
    public long DurationHttpGetCommand = -1;
    public long DurationHttpReceive = -1;
    public long DurationOverall = -1;
    public long DurationOverallNoSleep = -1;
    public long DurationSSL = -1;
    public long DurationTcpConnect = -1;
    public String ErrorReason = "";
    public int HTTPStatus = -1;
    public long HeaderBytesRead = -1;
    public IdleStates IdleStateOnEnd = IdleStates.Unknown;
    public IdleStates IdleStateOnStart = IdleStates.Unknown;
    public boolean IsKeepAlive = false;
    @d(a = true)
    public IspInfo IspInfo = new IspInfo();
    public boolean LocalhostPingSuccess = false;
    @d(a = true)
    public LocationInfo LocationInfo = new LocationInfo();
    @d(a = true)
    public b MemoryInfo = new b();
    @d(b = ArrayList.class, c = c.class)
    public ArrayList<c> MultiCdnInfo = new ArrayList<>();
    @d(b = ArrayList.class, c = NetworkRegistrationInfo.class)
    public ArrayList<NetworkRegistrationInfo> NetworkRegistrationInfo = new ArrayList<>();
    @d(a = true)
    public RadioInfo RadioInfo = new RadioInfo();
    @d(a = true)
    public RadioInfo RadioInfoOnEnd = new RadioInfo();
    public ScreenStates ScreenState = ScreenStates.Unknown;
    public String ServerFilename = "";
    public String ServerHostname = "";
    public String ServerIp = "";
    public long ServerMultiSuccess = -1;
    @d(a = true)
    public com.startapp.networkTest.data.a.b SimInfo = new com.startapp.networkTest.data.a.b();
    public String SslException = "";
    @d(a = true)
    public e StorageInfo = new e();
    public boolean Success = false;
    public String TestTimestamp = "";
    public CtTestTypes TestType = CtTestTypes.Unknown;
    @d(a = true)
    public TimeInfo TimeInfo = new TimeInfo();
    @d(a = true)
    public f TrafficInfo = new f();
    public long TruststoreTimestamp;
    public NetworkTypes VoiceNetworkType = NetworkTypes.Unknown;
    @d(a = true)
    public WifiInfo WifiInfo = new WifiInfo();

    public ConnectivityTestResult(String str, String str2) {
        super(str, str2);
    }
}
