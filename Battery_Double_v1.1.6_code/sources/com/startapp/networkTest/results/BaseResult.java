package com.startapp.networkTest.results;

/* compiled from: StartAppSDK */
public class BaseResult implements Cloneable {
    public String GUID = "";
    public String ProjectId = "";
    public String Version = "20191223173000";

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public BaseResult(String str, String str2) {
        this.ProjectId = str;
        this.GUID = str2;
    }
}
