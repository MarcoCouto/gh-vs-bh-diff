package com.startapp.networkTest.startapp;

import com.startapp.networkTest.results.ConnectivityTestResult;
import com.startapp.networkTest.results.LatencyResult;

/* compiled from: StartAppSDK */
public interface ConnectivityTestListener {
    void onConnectivityTestFinished(Runnable runnable);

    void onConnectivityTestResult(ConnectivityTestResult connectivityTestResult);

    void onLatencyTestResult(LatencyResult latencyResult);
}
