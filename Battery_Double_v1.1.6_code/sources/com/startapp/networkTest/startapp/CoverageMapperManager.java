package com.startapp.networkTest.startapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;
import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.startapp.networkTest.controller.LocationController.b;
import com.startapp.networkTest.controller.a.a;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.enums.LocationProviders;
import com.startapp.networkTest.enums.TriggerEvents;
import com.startapp.networkTest.results.NetworkInformationResult;
import com.tapjoy.TapjoyConstants;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class CoverageMapperManager implements b, a {
    private Context a;
    private com.startapp.networkTest.c.b b;
    private boolean c = false;
    private long d;
    /* access modifiers changed from: private */
    public ScheduledFuture<?> e;
    /* access modifiers changed from: private */
    public ScheduledFuture<?> f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public long h;
    private int i = -1;
    /* access modifiers changed from: private */
    public long j;
    private int k = -1;
    private OnNetworkInfoResultListener l;
    private Runnable m = new Runnable() {
        public final void run() {
            CoverageMapperManager.a(CoverageMapperManager.this, null, TriggerEvents.OutOfService, true);
            if (CoverageMapperManager.this.g + TapjoyConstants.TIMER_INCREMENT < SystemClock.elapsedRealtime()) {
                CoverageMapperManager.this.e.cancel(false);
            }
        }
    };
    private Runnable n = new Runnable() {
        public final void run() {
            CoverageMapperManager.a(CoverageMapperManager.this, null, TriggerEvents.CellIdChange, true);
            if (CoverageMapperManager.this.j + 1 < SystemClock.elapsedRealtime()) {
                CoverageMapperManager.this.f.cancel(false);
            }
        }
    };
    private BroadcastReceiver o = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            CoverageMapperManager.this.h = SystemClock.elapsedRealtime();
        }
    };

    /* compiled from: StartAppSDK */
    public interface OnNetworkInfoResultListener {
        void onNetworkInfoResult(NetworkInformationResult networkInformationResult);
    }

    static {
        com.startapp.common.b.b.a(CoverageMapperManager.class);
    }

    public CoverageMapperManager(Context context) {
        this.b = new com.startapp.networkTest.c.b(context);
        this.a = context;
    }

    public final void a(OnNetworkInfoResultListener onNetworkInfoResultListener) {
        this.l = onNetworkInfoResultListener;
    }

    public final void a() {
        if (!this.c) {
            this.c = true;
            this.b.a((a) this);
            this.b.a((b) this);
            this.b.a();
            IntentFilter intentFilter = new IntentFilter("android.intent.action.AIRPLANE_MODE");
            intentFilter.addAction("android.intent.action.ACTION_SHUTDOWN");
            this.a.registerReceiver(this.o, intentFilter);
        }
    }

    public final void b() {
        if (this.c) {
            this.b.b(this);
            this.b.c();
            this.b.b();
            try {
                this.a.unregisterReceiver(this.o);
            } catch (Throwable th) {
                a.a(th);
            }
            this.c = false;
        }
    }

    public final void a(final LocationInfo locationInfo) {
        if (locationInfo.LocationProvider == LocationProviders.Gps) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (elapsedRealtime >= this.d + 500) {
                this.d = elapsedRealtime;
                com.startapp.networkTest.threads.a.a().b().execute(new Runnable() {
                    public final void run() {
                        CoverageMapperManager.a(CoverageMapperManager.this, locationInfo, TriggerEvents.LocationUpdateGps, false);
                    }
                });
            }
        }
    }

    public final void a(ServiceState serviceState, int i2) {
        if (this.b.d().h().DefaultDataSimId == i2) {
            int state = serviceState.getState();
            if (state == 1 && this.i == 0) {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (this.h + TapjoyConstants.TIMER_INCREMENT <= elapsedRealtime) {
                    this.g = elapsedRealtime;
                    if (this.e == null || this.e.isDone()) {
                        this.e = com.startapp.networkTest.threads.a.a().c().scheduleWithFixedDelay(this.m, 0, 1000, TimeUnit.MILLISECONDS);
                    }
                }
            }
            this.i = state;
        }
    }

    public final void a(CellLocation cellLocation, int i2) {
        if (this.b.d().h().DefaultDataSimId == i2 && cellLocation != null) {
            int i3 = cellLocation.getClass().equals(GsmCellLocation.class) ? ((GsmCellLocation) cellLocation).getCid() : cellLocation.getClass().equals(CdmaCellLocation.class) ? ((CdmaCellLocation) cellLocation).getBaseStationId() : -1;
            if (i3 == this.k || this.k == -1 || i3 <= 0 || i3 == Integer.MAX_VALUE) {
                if (i3 > 0 && i3 < Integer.MAX_VALUE) {
                    this.k = i3;
                }
                return;
            }
            this.k = i3;
            this.j = SystemClock.elapsedRealtime();
            if (this.f == null || this.f.isDone()) {
                this.f = com.startapp.networkTest.threads.a.a().c().scheduleWithFixedDelay(this.n, 0, 1000, TimeUnit.MILLISECONDS);
            }
        }
    }

    static /* synthetic */ void a(CoverageMapperManager coverageMapperManager, LocationInfo locationInfo, TriggerEvents triggerEvents, boolean z) {
        NetworkInformationResult networkInformationResult;
        if (locationInfo == null) {
            networkInformationResult = coverageMapperManager.b.a(triggerEvents, z);
        } else {
            networkInformationResult = coverageMapperManager.b.a(locationInfo, triggerEvents, z);
        }
        if (coverageMapperManager.l != null) {
            coverageMapperManager.l.onNetworkInfoResult(networkInformationResult);
        }
    }
}
