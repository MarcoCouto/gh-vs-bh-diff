package com.startapp.networkTest.threads;

import android.os.Build.VERSION;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: StartAppSDK */
public class a {
    private static final a a = new a();
    private final ScheduledThreadPoolExecutor b = new ScheduledThreadPoolExecutor(1);
    private final ExecutorService c;
    private final ExecutorService d;

    private a() {
        StringBuilder sb = new StringBuilder();
        sb.append(a.class.getSimpleName());
        sb.append("-Single");
        this.c = a(1, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(a.class.getSimpleName());
        sb2.append("-Cached");
        this.d = a(4, sb2.toString());
        this.b.setKeepAliveTime(60, TimeUnit.SECONDS);
        if (VERSION.SDK_INT >= 9) {
            this.b.allowCoreThreadTimeOut(true);
        }
    }

    public static a a() {
        return a;
    }

    public final ExecutorService b() {
        return this.d;
    }

    public final ScheduledExecutorService c() {
        return this.b;
    }

    public final ExecutorService d() {
        return this.c;
    }

    private static ExecutorService a(int i, final String str) {
        if (VERSION.SDK_INT >= 21) {
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, i, 30, TimeUnit.SECONDS, new ThreadManager$1(), new ThreadFactory() {
                private AtomicInteger a = new AtomicInteger();

                public final Thread newThread(Runnable runnable) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("-");
                    sb.append(this.a.incrementAndGet());
                    return new Thread(runnable, sb.toString());
                }
            }, new RejectedExecutionHandler() {
                public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                    try {
                        threadPoolExecutor.getQueue().put(runnable);
                    } catch (InterruptedException unused) {
                        Thread.currentThread().interrupt();
                    }
                }
            });
            threadPoolExecutor.allowCoreThreadTimeOut(true);
            return threadPoolExecutor;
        } else if (i < 2) {
            return Executors.newSingleThreadExecutor();
        } else {
            return Executors.newCachedThreadPool();
        }
    }
}
