package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum PhoneTypes {
    GSM,
    CDMA,
    SIP,
    None,
    Unknown
}
