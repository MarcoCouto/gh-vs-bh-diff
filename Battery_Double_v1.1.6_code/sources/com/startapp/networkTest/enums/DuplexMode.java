package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum DuplexMode {
    Unknown,
    FDD,
    TDD
}
