package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum LocationProviders {
    Unknown,
    Gps,
    Network,
    Fused,
    RailNet
}
