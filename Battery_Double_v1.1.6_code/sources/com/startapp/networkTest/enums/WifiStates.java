package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum WifiStates {
    Unknown,
    Disabled,
    Disabling,
    Enabled,
    Enabling
}
