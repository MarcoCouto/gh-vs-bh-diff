package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum CtTestTypes {
    SSLOwnTs,
    SSLDeviceTs,
    SSLTrustAll,
    Unknown
}
