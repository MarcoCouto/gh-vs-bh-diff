package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum BatteryChargePlugTypes {
    AC,
    USB,
    Unknown,
    Wireless
}
