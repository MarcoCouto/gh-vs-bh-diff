package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum ThreeState {
    Unknown,
    Enabled,
    Disabled
}
