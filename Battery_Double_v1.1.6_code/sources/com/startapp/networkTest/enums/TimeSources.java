package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum TimeSources {
    NTP,
    GPS,
    Device,
    Unknown
}
