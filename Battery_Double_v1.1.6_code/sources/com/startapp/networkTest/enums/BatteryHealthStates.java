package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum BatteryHealthStates {
    Cold,
    Dead,
    Good,
    OverVoltage,
    Overheat,
    Unknown
}
