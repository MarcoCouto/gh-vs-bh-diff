package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum ThreeStateShort {
    Yes,
    No,
    Unknown
}
