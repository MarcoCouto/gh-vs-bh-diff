package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum SpeedtestEndStates {
    Unknown,
    ConnectingToControlServer,
    ConnectedToControlServer,
    ConnectingToTestServer,
    ConnectedToTestServer,
    LatencyTestStart,
    LatencyTestEnd,
    DownloadTestStart,
    DownloadTestEnd,
    UploadTestStart,
    UploadTestEnd,
    TracerouteTestStart,
    TracerouteTestEnd,
    TestStart,
    TestEnd,
    Finish
}
