package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum CellConnectionStatus {
    None,
    Primary,
    Secondary,
    Unknown
}
