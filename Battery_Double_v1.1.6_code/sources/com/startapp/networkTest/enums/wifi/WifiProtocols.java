package com.startapp.networkTest.enums.wifi;

/* compiled from: StartAppSDK */
public enum WifiProtocols {
    Unknown,
    RSN,
    WPA
}
