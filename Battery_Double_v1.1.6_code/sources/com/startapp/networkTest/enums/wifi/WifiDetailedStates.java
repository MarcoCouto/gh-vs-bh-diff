package com.startapp.networkTest.enums.wifi;

import android.net.NetworkInfo.DetailedState;
import android.os.Build.VERSION;

/* compiled from: StartAppSDK */
public enum WifiDetailedStates {
    Unknown,
    IDLE,
    SCANNING,
    CONNECTING,
    AUTHENTICATING,
    OBTAINING_IPADDR,
    CONNECTED,
    SUSPENDED,
    DISCONNECTING,
    DISCONNECTED,
    FAILED,
    BLOCKED,
    VERIFYING_POOR_LINK,
    CAPTIVE_PORTAL_CHECK;

    /* renamed from: com.startapp.networkTest.enums.wifi.WifiDetailedStates$1 reason: invalid class name */
    /* compiled from: StartAppSDK */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = null;

        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|(3:21|22|24)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|24) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            a = new int[DetailedState.values().length];
            a[DetailedState.AUTHENTICATING.ordinal()] = 1;
            a[DetailedState.BLOCKED.ordinal()] = 2;
            a[DetailedState.CONNECTED.ordinal()] = 3;
            a[DetailedState.CONNECTING.ordinal()] = 4;
            a[DetailedState.DISCONNECTED.ordinal()] = 5;
            a[DetailedState.DISCONNECTING.ordinal()] = 6;
            a[DetailedState.FAILED.ordinal()] = 7;
            a[DetailedState.IDLE.ordinal()] = 8;
            a[DetailedState.OBTAINING_IPADDR.ordinal()] = 9;
            a[DetailedState.SCANNING.ordinal()] = 10;
            try {
                a[DetailedState.SUSPENDED.ordinal()] = 11;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public static WifiDetailedStates a(DetailedState detailedState) {
        switch (AnonymousClass1.a[detailedState.ordinal()]) {
            case 1:
                return AUTHENTICATING;
            case 2:
                return BLOCKED;
            case 3:
                return CONNECTED;
            case 4:
                return CONNECTING;
            case 5:
                return DISCONNECTED;
            case 6:
                return DISCONNECTING;
            case 7:
                return FAILED;
            case 8:
                return IDLE;
            case 9:
                return OBTAINING_IPADDR;
            case 10:
                return SCANNING;
            case 11:
                return SUSPENDED;
            default:
                if (VERSION.SDK_INT >= 17) {
                    if (detailedState.equals(VERIFYING_POOR_LINK)) {
                        return VERIFYING_POOR_LINK;
                    }
                    if (detailedState.equals(CAPTIVE_PORTAL_CHECK)) {
                        return CAPTIVE_PORTAL_CHECK;
                    }
                }
                return Unknown;
        }
    }
}
