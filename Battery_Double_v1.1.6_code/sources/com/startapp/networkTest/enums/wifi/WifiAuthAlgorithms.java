package com.startapp.networkTest.enums.wifi;

/* compiled from: StartAppSDK */
public enum WifiAuthAlgorithms {
    Unknown,
    LEAP,
    OPEN,
    SHARED
}
