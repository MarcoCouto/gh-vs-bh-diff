package com.startapp.networkTest.enums.wifi;

/* compiled from: StartAppSDK */
public enum WifiKeyManagements {
    Unknown,
    IEEE8021X,
    NONE,
    WPA_EAP,
    WPA_PSK
}
