package com.startapp.networkTest.enums.wifi;

/* compiled from: StartAppSDK */
public enum WifiPairwiseCiphers {
    Unknown,
    CCMP,
    NONE,
    TKIP
}
