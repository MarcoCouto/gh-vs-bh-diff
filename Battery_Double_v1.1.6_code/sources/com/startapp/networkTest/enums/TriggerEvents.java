package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum TriggerEvents {
    Unknown,
    PeriodicExternal,
    PeriodicPushNotification,
    PeriodicNetworkFeedback,
    PeriodicBackgroundService,
    PeriodicVoiceCall,
    LocationUpdateGps,
    LocationUpdateNetwork,
    Manual,
    Automatic,
    OutOfService,
    CellIdChange
}
