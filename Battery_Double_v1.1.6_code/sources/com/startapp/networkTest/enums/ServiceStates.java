package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum ServiceStates {
    Unknown,
    EmergencyOnly,
    InService,
    OutOfService,
    PowerOff
}
