package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum CtCriteriaTypes {
    TotalTests,
    DNSSuccessful,
    TCPSuccessful,
    FullSuccessful,
    Random,
    NoChange
}
