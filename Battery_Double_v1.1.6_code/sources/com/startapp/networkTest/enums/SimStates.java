package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum SimStates {
    Unknown,
    Absent,
    NetworkLocked,
    PinRequired,
    PukRequired,
    Ready,
    NotReady,
    PermanentlyDisabled,
    CardIoError,
    CardRestricted
}
