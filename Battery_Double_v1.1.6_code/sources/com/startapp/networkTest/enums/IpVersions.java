package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum IpVersions {
    Unknown,
    IPv4,
    IPv6
}
