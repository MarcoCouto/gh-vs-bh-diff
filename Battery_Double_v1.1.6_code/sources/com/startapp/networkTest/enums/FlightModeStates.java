package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum FlightModeStates {
    Unknown,
    Enabled,
    Disabled
}
