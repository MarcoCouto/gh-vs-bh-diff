package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum BatteryStatusStates {
    Charging,
    Full,
    Unknown,
    Discharging,
    NotCharging
}
