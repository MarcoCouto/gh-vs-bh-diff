package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum MeasurementTypes {
    Unknown,
    HTTP,
    HTTPS,
    HTTP_FD,
    HTTPS_FD,
    TCP20000,
    TCP20000_FD,
    IPING,
    TPING,
    FTP_FD,
    FTP_FT,
    FTPS_FD,
    FTPS_FT,
    UDP_FT,
    UDP_FP_RX,
    UDP_FP_TX
}
