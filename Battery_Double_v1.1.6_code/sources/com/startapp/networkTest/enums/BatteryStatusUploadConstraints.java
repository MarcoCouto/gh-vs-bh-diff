package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum BatteryStatusUploadConstraints {
    Charging,
    FullOrCharging,
    Always
}
