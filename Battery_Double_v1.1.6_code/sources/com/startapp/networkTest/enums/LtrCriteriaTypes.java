package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum LtrCriteriaTypes {
    TotalTests,
    FullSuccessful,
    Random,
    NoChange,
    CTItem
}
