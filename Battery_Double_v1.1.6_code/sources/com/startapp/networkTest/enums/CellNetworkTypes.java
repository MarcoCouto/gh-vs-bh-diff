package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum CellNetworkTypes {
    Gsm,
    Wcdma,
    Lte,
    Nr,
    Cdma,
    Unknown
}
