package com.startapp.networkTest.enums.radio;

/* compiled from: StartAppSDK */
public enum DataConnectionStates {
    Disconnected,
    Suspended,
    Connecting,
    Connected,
    Unknown,
    Disabled
}
