package com.startapp.networkTest.enums.bluetooth;

/* compiled from: StartAppSDK */
public enum BluetoothMajorDeviceClasses {
    AudioVideo,
    Computer,
    Health,
    Imaging,
    Misc,
    Networking,
    Peripheral,
    Phone,
    Toy,
    Uncategorized,
    Wearable,
    Unknown
}
