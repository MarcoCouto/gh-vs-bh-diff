package com.startapp.networkTest.enums.bluetooth;

/* compiled from: StartAppSDK */
public enum BluetoothTypes {
    Classic,
    LowEnergy,
    DualMode,
    Unknown
}
