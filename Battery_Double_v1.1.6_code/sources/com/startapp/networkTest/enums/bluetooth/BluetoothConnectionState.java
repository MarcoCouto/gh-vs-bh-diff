package com.startapp.networkTest.enums.bluetooth;

/* compiled from: StartAppSDK */
public enum BluetoothConnectionState {
    Disconnected,
    Connecting,
    Connected,
    Disconnecting,
    Unknown
}
