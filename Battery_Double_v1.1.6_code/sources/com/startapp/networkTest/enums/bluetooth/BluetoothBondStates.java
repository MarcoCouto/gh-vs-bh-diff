package com.startapp.networkTest.enums.bluetooth;

/* compiled from: StartAppSDK */
public enum BluetoothBondStates {
    None,
    Bonding,
    Bonded,
    Unknown
}
