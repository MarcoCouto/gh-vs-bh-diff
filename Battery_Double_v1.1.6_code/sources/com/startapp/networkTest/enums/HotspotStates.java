package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum HotspotStates {
    Unknown,
    Failed,
    Disabled,
    Disabling,
    Enabled,
    Enabling
}
