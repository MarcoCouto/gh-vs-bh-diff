package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum IdleStates {
    Unknown,
    DeepIdle,
    LightIdle,
    NonIdle
}
