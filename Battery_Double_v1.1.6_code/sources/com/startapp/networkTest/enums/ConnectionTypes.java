package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum ConnectionTypes {
    Unknown,
    Bluetooth,
    Ethernet,
    Mobile,
    WiFi,
    WiMAX
}
