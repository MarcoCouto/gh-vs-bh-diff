package com.startapp.networkTest.enums;

/* compiled from: StartAppSDK */
public enum NetworkGenerations {
    Gen2,
    Gen3,
    Gen4,
    Gen5,
    Unknown
}
