package com.startapp.networkTest.enums.voice;

/* compiled from: StartAppSDK */
public enum CallStates {
    Offhook,
    Ringing,
    Idle,
    Unknown
}
