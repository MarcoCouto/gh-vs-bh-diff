package com.startapp.networkTest.d.a;

import android.content.Context;
import android.net.SSLCertificateSocketFactory;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.startapp.networkTest.controller.LocationController;
import com.startapp.networkTest.controller.LocationController.ProviderMode;
import com.startapp.networkTest.controller.c;
import com.startapp.networkTest.controller.d;
import com.startapp.networkTest.data.BatteryInfo;
import com.startapp.networkTest.enums.ConnectionTypes;
import com.startapp.networkTest.enums.CtCriteriaTypes;
import com.startapp.networkTest.enums.voice.CallStates;
import com.startapp.networkTest.results.ConnectivityTestResult;
import com.startapp.networkTest.results.LatencyResult;
import com.startapp.networkTest.speedtest.SpeedtestEngineStatus;
import com.startapp.networkTest.utils.e;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;

/* compiled from: StartAppSDK */
public class b {
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public c b;
    /* access modifiers changed from: private */
    public d c;
    /* access modifiers changed from: private */
    public LocationController d;
    /* access modifiers changed from: private */
    public com.startapp.networkTest.d e;
    /* access modifiers changed from: private */
    public e f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public Random k = new Random();
    /* access modifiers changed from: private */
    public float l;
    /* access modifiers changed from: private */
    public boolean m;

    /* compiled from: StartAppSDK */
    class a extends AsyncTask<Void, String, ConnectivityTestResult> implements com.startapp.networkTest.speedtest.a {
        private ConnectivityTestResult a;
        private com.startapp.networkTest.c.a b;

        /* renamed from: com.startapp.networkTest.d.a.b$a$a reason: collision with other inner class name */
        /* compiled from: StartAppSDK */
        class C0068a {
            final int a;
            final String b;
            final boolean c;

            public C0068a(int i, String str, boolean z) {
                this.a = i;
                this.b = str;
                this.c = z;
            }
        }

        a() {
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            ConnectivityTestResult connectivityTestResult = (ConnectivityTestResult) obj;
            b.this.e.a(SystemClock.elapsedRealtime());
            if (b.this.f != null) {
                b.this.f.a(connectivityTestResult);
            }
            if (connectivityTestResult != null) {
                boolean z = false;
                if (b.this.e.b() && connectivityTestResult.ServerIp.length() > 0) {
                    this.b = new com.startapp.networkTest.c.a(this, b.this.a);
                    this.b.c(connectivityTestResult.CtId);
                    this.b.b(connectivityTestResult.AirportCode);
                    this.b.a(String.valueOf(connectivityTestResult.TimeInfo.TimestampMillis + connectivityTestResult.DurationDNS + connectivityTestResult.DurationTcpConnect + connectivityTestResult.DurationHttpReceive));
                    this.b.a(com.startapp.networkTest.c.d().o());
                    this.b.d(connectivityTestResult.ServerIp);
                    z = true;
                }
                if (!z && b.this.f != null) {
                    b.this.f.a();
                }
            }
        }

        private C0068a a(InputStream inputStream) throws IOException {
            boolean z;
            byte[] bArr = new byte[1024];
            int i = 0;
            int i2 = 0;
            while (true) {
                int read = inputStream.read();
                z = true;
                i++;
                if (read == 10) {
                    z = false;
                    break;
                } else if (read < 0) {
                    break;
                } else {
                    int i3 = i2 + 1;
                    bArr[i2] = (byte) read;
                    if (i3 == bArr.length) {
                        bArr = Arrays.copyOf(bArr, i3 + 1024);
                    }
                    i2 = i3;
                }
            }
            if (i2 > 0 && bArr[i2 - 1] == 13) {
                i2--;
            }
            return new C0068a(i, new String(bArr, 0, i2, "UTF-8"), z);
        }

        /*  JADX ERROR: IF instruction can be used only in fallback mode
            jadx.core.utils.exceptions.CodegenException: IF instruction can be used only in fallback mode
            	at jadx.core.codegen.InsnGen.fallbackOnlyInsn(InsnGen.java:568)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:474)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
            	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:193)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:66)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:148)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:62)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
            	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:299)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:68)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
            	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:299)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:68)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:138)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:62)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:138)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:62)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:138)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:62)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
            	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:299)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:68)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
            	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
            	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
            	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
            	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
            	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
            	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
            	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
            	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
            	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
            	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
            	at jadx.core.ProcessClass.process(ProcessClass.java:36)
            	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
            	at jadx.api.JavaClass.decompile(JavaClass.java:62)
            	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
            */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x03e7, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:310:0x0897, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:311:0x0898, code lost:
            r34 = r7;
            r16 = r3;
            r15 = r11;
            r19 = r17;
            r17 = r23;
            r11 = r26;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:312:0x08a6, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:313:0x08a7, code lost:
            r34 = r7;
            r31 = r9;
            r32 = r10;
            r16 = r3;
            r15 = r11;
            r19 = r17;
            r17 = r23;
            r11 = r26;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:314:0x08b9, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:315:0x08ba, code lost:
            r34 = r7;
            r31 = r9;
            r32 = r10;
            r16 = r3;
            r15 = r11;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:320:0x08dc, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:321:0x08dd, code lost:
            r16 = r3;
            r19 = r17;
            r17 = r23;
            r11 = r26;
            r13 = r27;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:323:0x08ee, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:334:0x0924, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:335:0x0925, code lost:
            r34 = r7;
            r10 = r3;
            r16 = r19;
            r11 = r26;
            r13 = r27;
            r5 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:336:0x0931, code lost:
            r3 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:337:0x0933, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:338:0x0934, code lost:
            r34 = r7;
            r3 = r0;
            r16 = r19;
            r11 = r26;
            r13 = r27;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:343:0x0952, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:344:0x0953, code lost:
            r34 = r7;
            r27 = r13;
            r3 = r0;
            r19 = r17;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:345:0x095d, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:346:0x095e, code lost:
            r34 = r7;
            r27 = r13;
            r3 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:347:0x0965, code lost:
            r17 = r23;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:348:0x0968, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:350:0x096b, code lost:
            r1 = r0;
            r30 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:351:0x096f, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:352:0x0970, code lost:
            r34 = r7;
            r27 = r13;
            r3 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:353:0x0977, code lost:
            r5 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:357:0x099c, code lost:
            r8.add(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:362:0x09b0, code lost:
            r8.add(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:85:0x0373, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x0375, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x0376, code lost:
            r28 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x0378, code lost:
            r3 = r0;
            r34 = r7;
            r19 = r17;
            r17 = r23;
            r11 = r26;
            r13 = r27;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:0x0384, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x0385, code lost:
            r28 = r3;
            r27 = r13;
            r3 = r0;
            r34 = r7;
            r19 = r17;
            r17 = r23;
            r11 = r26;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:0x0392, code lost:
            r16 = r28;
            r5 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x0399, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x039a, code lost:
            r1 = r0;
            r30 = true;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:107:0x03e7 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:101:0x03b8] */
        /* JADX WARNING: Removed duplicated region for block: B:160:0x0508 A[Catch:{ Exception -> 0x053f }] */
        /* JADX WARNING: Removed duplicated region for block: B:161:0x053c  */
        /* JADX WARNING: Removed duplicated region for block: B:169:0x0571  */
        /* JADX WARNING: Removed duplicated region for block: B:170:0x0573  */
        /* JADX WARNING: Removed duplicated region for block: B:186:0x05ff  */
        /* JADX WARNING: Removed duplicated region for block: B:195:0x064b  */
        /* JADX WARNING: Removed duplicated region for block: B:221:0x06bd  */
        /* JADX WARNING: Removed duplicated region for block: B:222:0x06c4  */
        /* JADX WARNING: Removed duplicated region for block: B:231:0x06de  */
        /* JADX WARNING: Removed duplicated region for block: B:235:0x06e9 A[SYNTHETIC, Splitter:B:235:0x06e9] */
        /* JADX WARNING: Removed duplicated region for block: B:241:0x06fe A[SYNTHETIC, Splitter:B:241:0x06fe] */
        /* JADX WARNING: Removed duplicated region for block: B:244:0x070e A[SYNTHETIC, Splitter:B:244:0x070e] */
        /* JADX WARNING: Removed duplicated region for block: B:259:0x0731 A[SYNTHETIC, Splitter:B:259:0x0731] */
        /* JADX WARNING: Removed duplicated region for block: B:281:0x079d A[SYNTHETIC, Splitter:B:281:0x079d] */
        /* JADX WARNING: Removed duplicated region for block: B:291:0x07b6  */
        /* JADX WARNING: Removed duplicated region for block: B:301:0x086c  */
        /* JADX WARNING: Removed duplicated region for block: B:304:0x087e  */
        /* JADX WARNING: Removed duplicated region for block: B:307:0x088b  */
        /* JADX WARNING: Removed duplicated region for block: B:323:0x08ee A[ExcHandler: all (th java.lang.Throwable), Splitter:B:119:0x0420] */
        /* JADX WARNING: Removed duplicated region for block: B:348:0x0968 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:63:0x02fa] */
        /* JADX WARNING: Removed duplicated region for block: B:357:0x099c  */
        /* JADX WARNING: Removed duplicated region for block: B:362:0x09b0  */
        /* JADX WARNING: Removed duplicated region for block: B:370:0x099f A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:378:0x06d0 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:92:0x0399 A[ExcHandler: all (r0v50 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:73:0x031b] */
        private com.startapp.networkTest.results.ConnectivityTestResult a() {
            /*
                r41 = this;
                r1 = r41
                com.startapp.networkTest.controller.a r2 = new com.startapp.networkTest.controller.a
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                android.content.Context r3 = r3.a
                r2.<init>(r3)
                com.startapp.networkTest.data.BatteryInfo r2 = r2.a()
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                float r3 = r3.l
                r4 = -1082130432(0xffffffffbf800000, float:-1.0)
                int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
                r4 = 0
                if (r3 == 0) goto L_0x002b
                float r3 = r2.BatteryLevel
                com.startapp.networkTest.d.a.b r5 = com.startapp.networkTest.d.a.b.this
                float r5 = r5.l
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x002b
                return r4
            L_0x002b:
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                android.content.Context r3 = r3.a
                com.startapp.networkTest.data.a.b r3 = com.startapp.networkTest.controller.b.f(r3)
                com.startapp.networkTest.d.a.b r5 = com.startapp.networkTest.d.a.b.this
                boolean r5 = r5.m
                if (r5 != 0) goto L_0x005a
                com.startapp.networkTest.d.a.b r5 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r5 = r5.b
                com.startapp.networkTest.enums.ConnectionTypes r5 = r5.f()
                com.startapp.networkTest.enums.ConnectionTypes r6 = com.startapp.networkTest.enums.ConnectionTypes.Mobile
                if (r5 != r6) goto L_0x005a
                com.startapp.networkTest.d.a.b r5 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r5 = r5.b
                int r6 = r3.SubscriptionId
                boolean r5 = r5.c(r6)
                if (r5 == 0) goto L_0x005a
                return r4
            L_0x005a:
                com.startapp.networkTest.d r6 = com.startapp.networkTest.c.c()     // Catch:{ Exception -> 0x0088 }
                long r6 = r6.d()     // Catch:{ Exception -> 0x0088 }
                long r8 = com.startapp.networkTest.e.b.b()     // Catch:{ Exception -> 0x0088 }
                com.startapp.networkTest.a r10 = com.startapp.networkTest.c.d()     // Catch:{ Exception -> 0x0088 }
                long r10 = r10.k()     // Catch:{ Exception -> 0x0088 }
                r12 = 0     // Catch:{ Exception -> 0x0088 }
                long r10 = r10 + r6     // Catch:{ Exception -> 0x0088 }
                int r12 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))     // Catch:{ Exception -> 0x0088 }
                if (r12 < 0) goto L_0x007b     // Catch:{ Exception -> 0x0088 }
                int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))     // Catch:{ Exception -> 0x0088 }
                if (r10 <= 0) goto L_0x0079     // Catch:{ Exception -> 0x0088 }
                goto L_0x007b     // Catch:{ Exception -> 0x0088 }
            L_0x0079:
                r6 = 0     // Catch:{ Exception -> 0x0088 }
                goto L_0x0085     // Catch:{ Exception -> 0x0088 }
            L_0x007b:
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this     // Catch:{ Exception -> 0x0088 }
                android.content.Context r6 = r6.a     // Catch:{ Exception -> 0x0088 }
                boolean r6 = com.startapp.networkTest.utils.i.a(r6)     // Catch:{ Exception -> 0x0088 }
            L_0x0085:
                r7 = r6
                r6 = r4
                goto L_0x009d
            L_0x0088:
                r0 = move-exception
                r6 = r0
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                java.lang.String r8 = "checkAndLoadTruststore: "
                r7.<init>(r8)
                java.lang.String r6 = r6.toString()
                r7.append(r6)
                java.lang.String r6 = r7.toString()
                r7 = 0
            L_0x009d:
                com.startapp.networkTest.d r8 = com.startapp.networkTest.c.c()     // Catch:{ Exception -> 0x00be }
                long r8 = r8.i()     // Catch:{ Exception -> 0x00be }
                long r10 = com.startapp.networkTest.e.b.b()     // Catch:{ Exception -> 0x00be }
                com.startapp.networkTest.a r12 = com.startapp.networkTest.c.d()     // Catch:{ Exception -> 0x00be }
                long r12 = r12.l()     // Catch:{ Exception -> 0x00be }
                r14 = 0     // Catch:{ Exception -> 0x00be }
                long r12 = r12 + r8     // Catch:{ Exception -> 0x00be }
                int r14 = (r12 > r10 ? 1 : (r12 == r10 ? 0 : -1))     // Catch:{ Exception -> 0x00be }
                if (r14 < 0) goto L_0x00bb     // Catch:{ Exception -> 0x00be }
                int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))     // Catch:{ Exception -> 0x00be }
                if (r12 <= 0) goto L_0x00be     // Catch:{ Exception -> 0x00be }
            L_0x00bb:
                com.startapp.networkTest.utils.a.a()     // Catch:{ Exception -> 0x00be }
            L_0x00be:
                boolean r8 = com.startapp.networkTest.c.a()
                if (r8 == 0) goto L_0x09b4
                com.startapp.networkTest.d.a.b r8 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.d r8 = r8.c
                if (r8 != 0) goto L_0x00ce
                goto L_0x09b4
            L_0x00ce:
                com.startapp.networkTest.results.ConnectivityTestResult r8 = new com.startapp.networkTest.results.ConnectivityTestResult
                com.startapp.networkTest.d.a.b r9 = com.startapp.networkTest.d.a.b.this
                java.lang.String r9 = r9.g
                com.startapp.networkTest.d.a.b r10 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.d r10 = r10.e
                java.lang.String r10 = r10.a()
                r8.<init>(r9, r10)
                r1.a = r8
                com.startapp.networkTest.results.ConnectivityTestResult r8 = r1.a
                com.startapp.networkTest.d.a.b r9 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.LocationController r9 = r9.d
                com.startapp.networkTest.data.LocationInfo r9 = r9.b()
                r8.LocationInfo = r9
                com.startapp.networkTest.d r8 = com.startapp.networkTest.c.c()
                java.lang.String[] r8 = r8.j()
                com.startapp.networkTest.d r9 = com.startapp.networkTest.c.c()
                java.lang.String r9 = r9.l()
                com.startapp.networkTest.enums.CtCriteriaTypes r9 = com.startapp.networkTest.enums.CtCriteriaTypes.valueOf(r9)
                if (r6 == 0) goto L_0x011e
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                com.startapp.networkTest.results.ConnectivityTestResult r11 = r1.a
                java.lang.String r12 = r11.ErrorReason
                r10.append(r12)
                r10.append(r6)
                java.lang.String r6 = r10.toString()
                r11.ErrorReason = r6
            L_0x011e:
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a
                java.lang.String r10 = "20191223173000"
                r6.Version = r10
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                com.startapp.networkTest.d.a.b r11 = com.startapp.networkTest.d.a.b.this
                java.lang.String r11 = r11.i
                r10.append(r11)
                java.lang.String r11 = "?id="
                r10.append(r11)
                com.startapp.networkTest.d.a.b r11 = com.startapp.networkTest.d.a.b.this
                java.util.Random r11 = r11.k
                long r11 = r11.nextLong()
                r10.append(r11)
                java.lang.String r10 = r10.toString()
                r6.ServerFilename = r10
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a
                r6.BatteryInfo = r2
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                android.content.Context r6 = r6.a
                com.startapp.networkTest.data.a r6 = com.startapp.networkTest.controller.b.a(r6)
                r2.DeviceInfo = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                android.content.Context r6 = r6.a
                com.startapp.networkTest.data.b r6 = com.startapp.networkTest.controller.b.b(r6)
                r2.MemoryInfo = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r6 = r6.b
                com.startapp.networkTest.data.RadioInfo r6 = r6.c()
                r2.RadioInfo = r6
                com.startapp.networkTest.a r2 = com.startapp.networkTest.c.d()
                boolean r2 = r2.z()
                if (r2 == 0) goto L_0x019b
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                java.util.ArrayList r6 = new java.util.ArrayList
                com.startapp.networkTest.d.a.b r10 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r10 = r10.b
                com.startapp.networkTest.data.radio.CellInfo[] r10 = r10.d()
                java.util.List r10 = java.util.Arrays.asList(r10)
                r6.<init>(r10)
                r2.CellInfo = r6
            L_0x019b:
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                java.util.ArrayList r6 = new java.util.ArrayList
                com.startapp.networkTest.d.a.b r10 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r10 = r10.b
                com.startapp.networkTest.data.radio.ApnInfo[] r10 = r10.e()
                java.util.List r10 = java.util.Arrays.asList(r10)
                r6.<init>(r10)
                r2.ApnInfo = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                java.util.ArrayList r6 = new java.util.ArrayList
                com.startapp.networkTest.d.a.b r10 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r10 = r10.b
                com.startapp.networkTest.d.a.b r11 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r11 = r11.b
                com.startapp.networkTest.data.a.a r11 = r11.h()
                int r11 = r11.DefaultDataSimId
                com.startapp.networkTest.data.radio.NetworkRegistrationInfo[] r10 = r10.a(r11)
                java.util.List r10 = java.util.Arrays.asList(r10)
                r6.<init>(r10)
                r2.NetworkRegistrationInfo = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r6 = r6.b
                com.startapp.networkTest.enums.NetworkTypes r6 = r6.g()
                r2.VoiceNetworkType = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                android.content.Context r6 = r6.a
                java.lang.String r10 = "phone"
                java.lang.Object r6 = r6.getSystemService(r10)
                android.telephony.TelephonyManager r6 = (android.telephony.TelephonyManager) r6
                if (r6 == 0) goto L_0x0208
                int r6 = r6.getCallState()
                switch(r6) {
                    case 0: goto L_0x0205;
                    case 1: goto L_0x0202;
                    case 2: goto L_0x01ff;
                    default: goto L_0x01fc;
                }
            L_0x01fc:
                com.startapp.networkTest.enums.voice.CallStates r6 = com.startapp.networkTest.enums.voice.CallStates.Unknown
                goto L_0x020a
            L_0x01ff:
                com.startapp.networkTest.enums.voice.CallStates r6 = com.startapp.networkTest.enums.voice.CallStates.Offhook
                goto L_0x020a
            L_0x0202:
                com.startapp.networkTest.enums.voice.CallStates r6 = com.startapp.networkTest.enums.voice.CallStates.Ringing
                goto L_0x020a
            L_0x0205:
                com.startapp.networkTest.enums.voice.CallStates r6 = com.startapp.networkTest.enums.voice.CallStates.Idle
                goto L_0x020a
            L_0x0208:
                com.startapp.networkTest.enums.voice.CallStates r6 = com.startapp.networkTest.enums.voice.CallStates.Unknown
            L_0x020a:
                r2.CallState = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                android.content.Context r6 = r6.a
                com.startapp.networkTest.data.e r6 = com.startapp.networkTest.controller.b.c(r6)
                r2.StorageInfo = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.d r6 = r6.c
                com.startapp.networkTest.data.WifiInfo r6 = r6.a()
                r2.WifiInfo = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.d r6 = r6.c
                com.startapp.networkTest.data.f r6 = com.startapp.networkTest.controller.b.a(r6)
                r2.TrafficInfo = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                android.content.Context r6 = r6.a
                com.startapp.networkTest.enums.ScreenStates r6 = com.startapp.networkTest.controller.b.d(r6)
                r2.ScreenState = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                android.content.Context r6 = r6.a
                com.startapp.networkTest.enums.IdleStates r6 = com.startapp.networkTest.controller.b.e(r6)
                r2.IdleStateOnStart = r6
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                r2.SimInfo = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.data.TimeInfo r3 = com.startapp.networkTest.e.b.a()
                r2.TimeInfo = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a
                com.startapp.networkTest.data.TimeInfo r3 = r3.TimeInfo
                java.lang.String r3 = r3.TimestampTableau
                r2.TestTimestamp = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d r3 = com.startapp.networkTest.c.c()
                long r10 = r3.e()
                r2.TruststoreTimestamp = r10
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a
                com.startapp.networkTest.data.TimeInfo r3 = r3.TimeInfo
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a
                java.lang.String r6 = r6.GUID
                java.lang.String r3 = com.iab.omid.library.startapp.b.a(r3, r6)
                r2.CtId = r3
                com.startapp.networkTest.d.a.b r2 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.d r2 = r2.e
                boolean r2 = r2.c()
                r3 = 1
                if (r2 == 0) goto L_0x02a1
                com.startapp.networkTest.d.a.b r2 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.d r2 = r2.e
                boolean r2 = r2.b()
                if (r2 != 0) goto L_0x02a1
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                r2.IsKeepAlive = r3
            L_0x02a1:
                long r10 = android.os.SystemClock.elapsedRealtime()
                long r12 = android.os.SystemClock.uptimeMillis()
                javax.net.ssl.HostnameVerifier r2 = javax.net.ssl.HttpsURLConnection.getDefaultHostnameVerifier()
                com.startapp.networkTest.a.a r6 = new com.startapp.networkTest.a.a
                com.startapp.networkTest.d.a.b r14 = com.startapp.networkTest.d.a.b.this
                android.content.Context r14 = r14.a
                r6.<init>(r14, r7)
                java.util.List r7 = r1.a(r8, r9)
                java.util.LinkedList r8 = new java.util.LinkedList
                r8.<init>()
                com.startapp.networkTest.d.a.b r9 = com.startapp.networkTest.d.a.b.this
                java.lang.String r9 = r9.h
                com.startapp.networkTest.d.a.b r14 = com.startapp.networkTest.d.a.b.this
                java.lang.String r14 = r14.j
                r15 = r4
                r16 = r15
                r17 = r10
                r19 = r12
                r11 = 0
                r12 = 0
                r13 = r16
                r10 = r9
                r9 = 0
            L_0x02da:
                int r4 = r7.size()
                r22 = r6
                if (r9 < r4) goto L_0x02f5
                boolean r4 = r7.isEmpty()
                if (r4 == 0) goto L_0x02ef
                int r4 = r10.length()
                if (r4 <= 0) goto L_0x02ef
                goto L_0x02f5
            L_0x02ef:
                r3 = r16
                r4 = 0
                r5 = 0
                goto L_0x0482
            L_0x02f5:
                com.startapp.networkTest.data.c r4 = new com.startapp.networkTest.data.c
                r4.<init>()
                long r23 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x096f, all -> 0x0968 }
                long r17 = android.os.SystemClock.uptimeMillis()     // Catch:{ Exception -> 0x095d, all -> 0x0968 }
                com.startapp.networkTest.d.a.c r19 = new com.startapp.networkTest.d.a.c     // Catch:{ Exception -> 0x0952, all -> 0x0968 }
                r19.<init>()     // Catch:{ Exception -> 0x0952, all -> 0x0968 }
                int r11 = r11 + 1
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.a     // Catch:{ Exception -> 0x0940, all -> 0x0968 }
                boolean r6 = b()     // Catch:{ Exception -> 0x0940, all -> 0x0968 }
                r5.LocalhostPingSuccess = r6     // Catch:{ Exception -> 0x0940, all -> 0x0968 }
                long r5 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0940, all -> 0x0968 }
                boolean r16 = r7.isEmpty()     // Catch:{ Exception -> 0x0940, all -> 0x0968 }
                if (r16 != 0) goto L_0x03ae
                java.lang.Object r16 = r7.get(r9)     // Catch:{ Exception -> 0x039f, all -> 0x0399 }
                r3 = r16     // Catch:{ Exception -> 0x039f, all -> 0x0399 }
                com.startapp.networkTest.d.a.c r3 = (com.startapp.networkTest.d.a.c) r3     // Catch:{ Exception -> 0x039f, all -> 0x0399 }
                r26 = r11
                com.startapp.networkTest.results.ConnectivityTestResult r11 = r1.a     // Catch:{ Exception -> 0x0384, all -> 0x0399 }
                r27 = r13
                java.lang.String r13 = r3.address     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r11.ServerHostname = r13     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                int r11 = r3.totalTests     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r13 = 1     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                int r11 = r11 + r13     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r3.totalTests = r11     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                int r11 = r9 + 1     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r4.Try = r11     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r11.<init>()     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                com.startapp.networkTest.results.ConnectivityTestResult r13 = r1.a     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                java.lang.String r13 = r13.ServerHostname     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r11.append(r13)     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                com.startapp.networkTest.results.ConnectivityTestResult r13 = r1.a     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                java.lang.String r13 = r13.ServerFilename     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r11.append(r13)     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r4.HostFile = r11     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                com.startapp.networkTest.results.ConnectivityTestResult r11 = r1.a     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                com.startapp.networkTest.net.a r13 = new com.startapp.networkTest.net.a     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r13.<init>()     // Catch:{ Exception -> 0x0375, all -> 0x0399 }
                r28 = r3
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                java.lang.String r3 = r3.ServerHostname     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                java.lang.String r3 = r13.a(r3)     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                r11.ServerIp = r3     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                long r19 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                r11 = 0     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                long r5 = r19 - r5     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                r3.DurationDNS = r5     // Catch:{ Exception -> 0x0373, all -> 0x0399 }
                r3 = r28
                r5 = 1
                goto L_0x041c
            L_0x0373:
                r0 = move-exception
                goto L_0x0378
            L_0x0375:
                r0 = move-exception
                r28 = r3
            L_0x0378:
                r3 = r0
                r34 = r7
                r19 = r17
                r17 = r23
                r11 = r26
                r13 = r27
                goto L_0x0392
            L_0x0384:
                r0 = move-exception
                r28 = r3
                r27 = r13
                r3 = r0
                r34 = r7
                r19 = r17
                r17 = r23
                r11 = r26
            L_0x0392:
                r16 = r28
                r5 = 1
                r21 = 0
                goto L_0x0978
            L_0x0399:
                r0 = move-exception
                r1 = r0
                r30 = 1
                goto L_0x09ae
            L_0x039f:
                r0 = move-exception
                r26 = r11
                r27 = r13
                r3 = r0
                r34 = r7
                r16 = r19
                r5 = 1
            L_0x03aa:
                r21 = 0
                goto L_0x094d
            L_0x03ae:
                r26 = r11
                r27 = r13
                int r3 = r14.length()     // Catch:{ Exception -> 0x0933, all -> 0x0968 }
                if (r3 <= 0) goto L_0x03f6
                int r3 = r10.length()     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                if (r3 <= 0) goto L_0x03f6     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                r3.ServerIp = r14     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                r3.ServerHostname = r10     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                r5 = 0     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                r3.DurationDNS = r5     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                java.lang.String r3 = ""     // Catch:{ Exception -> 0x03ea, all -> 0x03e7 }
                java.lang.String r5 = ""
                int r9 = r9 + -1
                r14 = r3
                r10 = r5
            L_0x03d4:
                r3 = r19
                r5 = 0
                goto L_0x041c
            L_0x03d8:
                r0 = move-exception
                r14 = r3
                r34 = r7
                r16 = r19
                r11 = r26
                r13 = r27
                r5 = 0
                r21 = 0
                goto L_0x0931
            L_0x03e7:
                r0 = move-exception
                goto L_0x096b
            L_0x03ea:
                r0 = move-exception
                r3 = r0
                r34 = r7
                r16 = r19
                r11 = r26
                r13 = r27
                r5 = 0
                goto L_0x03aa
            L_0x03f6:
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a     // Catch:{ Exception -> 0x0933, all -> 0x0968 }
                r3.ServerHostname = r10     // Catch:{ Exception -> 0x0933, all -> 0x0968 }
                java.lang.String r3 = ""     // Catch:{ Exception -> 0x0933, all -> 0x0968 }
                int r9 = r9 + -1
                com.startapp.networkTest.results.ConnectivityTestResult r10 = r1.a     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                com.startapp.networkTest.net.a r11 = new com.startapp.networkTest.net.a     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                r11.<init>()     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                com.startapp.networkTest.results.ConnectivityTestResult r13 = r1.a     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                java.lang.String r13 = r13.ServerHostname     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                java.lang.String r11 = r11.a(r13)     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                r10.ServerIp = r11     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                com.startapp.networkTest.results.ConnectivityTestResult r10 = r1.a     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                long r28 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                r11 = 0     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                long r5 = r28 - r5     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                r10.DurationDNS = r5     // Catch:{ Exception -> 0x0924, all -> 0x0968 }
                r10 = r3
                goto L_0x03d4
            L_0x041c:
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a     // Catch:{ Exception -> 0x090c, all -> 0x0906 }
                r30 = r5
                long r5 = r6.DurationDNS     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                r19 = 30000(0x7530, double:1.4822E-319)     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                int r11 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                if (r11 > 0) goto L_0x08c6     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.a     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                java.lang.String r5 = r5.ServerIp     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                r4.ServerIp = r5     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.a     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                long r5 = r5.DurationDNS     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                r4.DurationDNS = r5     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                int r5 = r3.DNSSuccess     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                r6 = 1     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                int r5 = r5 + r6     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                r3.DNSSuccess = r5     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                int r12 = r12 + 1     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                r5 = 30000(0x7530, float:4.2039E-41)     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                javax.net.SocketFactory r11 = android.net.SSLCertificateSocketFactory.getDefault(r5)     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                android.net.SSLCertificateSocketFactory r11 = (android.net.SSLCertificateSocketFactory) r11     // Catch:{ Exception -> 0x08f0, all -> 0x08ee }
                javax.net.ssl.TrustManager[] r13 = new javax.net.ssl.TrustManager[r6]     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                r6 = 0     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                r13[r6] = r22     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                r11.setTrustManagers(r13)     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                java.net.InetSocketAddress r6 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                com.startapp.networkTest.results.ConnectivityTestResult r13 = r1.a     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                java.lang.String r13 = r13.ServerIp     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                r15 = 443(0x1bb, float:6.21E-43)     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                r6.<init>(r13, r15)     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                java.net.Socket r13 = r11.createSocket()     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                javax.net.ssl.SSLSocket r13 = (javax.net.ssl.SSLSocket) r13     // Catch:{ Exception -> 0x08b9, all -> 0x08ee }
                long r15 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x08a6, all -> 0x08ee }
                r13.connect(r6, r5)     // Catch:{ Exception -> 0x08a6, all -> 0x08ee }
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.a     // Catch:{ Exception -> 0x08a6, all -> 0x08ee }
                long r19 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x08a6, all -> 0x08ee }
                r6 = 0
                r31 = r9
                r32 = r10
                long r9 = r19 - r15
                r5.DurationTcpConnect = r9     // Catch:{ Exception -> 0x0897, all -> 0x08ee }
                int r5 = r3.TCPSuccess     // Catch:{ Exception -> 0x0897, all -> 0x08ee }
                r6 = 1     // Catch:{ Exception -> 0x0897, all -> 0x08ee }
                int r5 = r5 + r6     // Catch:{ Exception -> 0x0897, all -> 0x08ee }
                r3.TCPSuccess = r5     // Catch:{ Exception -> 0x0897, all -> 0x08ee }
                r15 = r11
                r19 = r17
                r17 = r23
                r11 = r26
                r4 = 1
                r5 = 1
            L_0x0482:
                if (r5 == 0) goto L_0x07a7
                long r9 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x076a }
                int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x076a }
                r14 = 17
                if (r6 < r14) goto L_0x04a8
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a     // Catch:{ Exception -> 0x049c }
                java.lang.String r6 = r6.ServerHostname     // Catch:{ Exception -> 0x049c }
                r15.setHostname(r13, r6)     // Catch:{ Exception -> 0x049c }
                r33 = r5
                r34 = r7
                r21 = 0
                goto L_0x04fa
            L_0x049c:
                r0 = move-exception
                r2 = r0
                r40 = r4
                r34 = r7
                r38 = r11
                r39 = r12
                goto L_0x0776
            L_0x04a8:
                java.lang.Class r6 = r15.getClass()     // Catch:{ Exception -> 0x04cf }
                java.lang.String r14 = "setHostname"     // Catch:{ Exception -> 0x04cf }
                r33 = r5
                r34 = r7
                r5 = 1
                java.lang.Class[] r7 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x04cd }
                java.lang.Class<java.lang.String> r16 = java.lang.String.class     // Catch:{ Exception -> 0x04cd }
                r21 = 0
                r7[r21] = r16     // Catch:{ Exception -> 0x04cb }
                java.lang.reflect.Method r6 = r6.getMethod(r14, r7)     // Catch:{ Exception -> 0x04cb }
                java.lang.Object[] r7 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x04cb }
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.a     // Catch:{ Exception -> 0x04cb }
                java.lang.String r5 = r5.ServerHostname     // Catch:{ Exception -> 0x04cb }
                r7[r21] = r5     // Catch:{ Exception -> 0x04cb }
                r6.invoke(r15, r7)     // Catch:{ Exception -> 0x04cb }
                goto L_0x04fa
            L_0x04cb:
                r0 = move-exception
                goto L_0x04d6
            L_0x04cd:
                r0 = move-exception
                goto L_0x04d4
            L_0x04cf:
                r0 = move-exception
                r33 = r5
                r34 = r7
            L_0x04d4:
                r21 = 0
            L_0x04d6:
                r5 = r0
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x075f }
                r6.<init>()     // Catch:{ Exception -> 0x075f }
                com.startapp.networkTest.results.ConnectivityTestResult r7 = r1.a     // Catch:{ Exception -> 0x075f }
                java.lang.String r14 = r7.SslException     // Catch:{ Exception -> 0x075f }
                r6.append(r14)     // Catch:{ Exception -> 0x075f }
                java.lang.String r14 = "SNI not available:"     // Catch:{ Exception -> 0x075f }
                r6.append(r14)     // Catch:{ Exception -> 0x075f }
                java.lang.String r5 = r5.getMessage()     // Catch:{ Exception -> 0x075f }
                r6.append(r5)     // Catch:{ Exception -> 0x075f }
                java.lang.String r5 = "; "     // Catch:{ Exception -> 0x075f }
                r6.append(r5)     // Catch:{ Exception -> 0x075f }
                java.lang.String r5 = r6.toString()     // Catch:{ Exception -> 0x075f }
                r7.SslException = r5     // Catch:{ Exception -> 0x075f }
            L_0x04fa:
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.a     // Catch:{ Exception -> 0x053f }
                java.lang.String r5 = r5.ServerHostname     // Catch:{ Exception -> 0x053f }
                javax.net.ssl.SSLSession r6 = r13.getSession()     // Catch:{ Exception -> 0x053f }
                boolean r2 = r2.verify(r5, r6)     // Catch:{ Exception -> 0x053f }
                if (r2 != 0) goto L_0x053c     // Catch:{ Exception -> 0x053f }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x053f }
                r2.<init>()     // Catch:{ Exception -> 0x053f }
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.a     // Catch:{ Exception -> 0x053f }
                java.lang.String r6 = r5.SslException     // Catch:{ Exception -> 0x053f }
                r2.append(r6)     // Catch:{ Exception -> 0x053f }
                java.lang.String r6 = "Expected "     // Catch:{ Exception -> 0x053f }
                r2.append(r6)     // Catch:{ Exception -> 0x053f }
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a     // Catch:{ Exception -> 0x053f }
                java.lang.String r6 = r6.ServerHostname     // Catch:{ Exception -> 0x053f }
                r2.append(r6)     // Catch:{ Exception -> 0x053f }
                java.lang.String r6 = " found "     // Catch:{ Exception -> 0x053f }
                r2.append(r6)     // Catch:{ Exception -> 0x053f }
                javax.net.ssl.SSLSession r6 = r13.getSession()     // Catch:{ Exception -> 0x053f }
                java.security.Principal r6 = r6.getPeerPrincipal()     // Catch:{ Exception -> 0x053f }
                r2.append(r6)     // Catch:{ Exception -> 0x053f }
                java.lang.String r6 = "; "     // Catch:{ Exception -> 0x053f }
                r2.append(r6)     // Catch:{ Exception -> 0x053f }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x053f }
                r5.SslException = r2     // Catch:{ Exception -> 0x053f }
                goto L_0x0564
            L_0x053c:
                r5 = r33
                goto L_0x0565
            L_0x053f:
                r0 = move-exception
                r2 = r0
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x075f }
                r5.<init>()     // Catch:{ Exception -> 0x075f }
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a     // Catch:{ Exception -> 0x075f }
                java.lang.String r7 = r6.SslException     // Catch:{ Exception -> 0x075f }
                r5.append(r7)     // Catch:{ Exception -> 0x075f }
                java.lang.String r7 = "Cannot validate hostname: "     // Catch:{ Exception -> 0x075f }
                r5.append(r7)     // Catch:{ Exception -> 0x075f }
                java.lang.String r2 = r2.getMessage()     // Catch:{ Exception -> 0x075f }
                r5.append(r2)     // Catch:{ Exception -> 0x075f }
                java.lang.String r2 = "; "     // Catch:{ Exception -> 0x075f }
                r5.append(r2)     // Catch:{ Exception -> 0x075f }
                java.lang.String r2 = r5.toString()     // Catch:{ Exception -> 0x075f }
                r6.SslException = r2     // Catch:{ Exception -> 0x075f }
            L_0x0564:
                r5 = 0     // Catch:{ Exception -> 0x075f }
            L_0x0565:
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a     // Catch:{ Exception -> 0x075f }
                long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x075f }
                r14 = 0     // Catch:{ Exception -> 0x075f }
                long r6 = r6 - r9     // Catch:{ Exception -> 0x075f }
                r2.DurationSSL = r6     // Catch:{ Exception -> 0x075f }
                if (r5 == 0) goto L_0x0573
                r2 = 1
                goto L_0x0574
            L_0x0573:
                r2 = 0
            L_0x0574:
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.a     // Catch:{ Exception -> 0x0752 }
                com.startapp.networkTest.enums.CtTestTypes r7 = r22.a()     // Catch:{ Exception -> 0x0752 }
                r6.TestType = r7     // Catch:{ Exception -> 0x0752 }
                r6 = 2048(0x800, float:2.87E-42)
                byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x0743 }
                java.io.PrintWriter r7 = new java.io.PrintWriter     // Catch:{ Exception -> 0x0743 }
                java.io.OutputStream r9 = r13.getOutputStream()     // Catch:{ Exception -> 0x0743 }
                r7.<init>(r9)     // Catch:{ Exception -> 0x0743 }
                long r9 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = "GET "     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                com.startapp.networkTest.results.ConnectivityTestResult r14 = r1.a     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = r14.ServerFilename     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = " HTTP/1.1"     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = "\r\n"     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = "HOST: "     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                com.startapp.networkTest.results.ConnectivityTestResult r14 = r1.a     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = r14.ServerHostname     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = "\r\n"     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = "Connection: close"     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = "\r\n"     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = "\r\n"     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                java.lang.String r14 = "\r\n"     // Catch:{ Exception -> 0x0743 }
                r7.print(r14)     // Catch:{ Exception -> 0x0743 }
                r7.flush()     // Catch:{ Exception -> 0x0743 }
                com.startapp.networkTest.results.ConnectivityTestResult r7 = r1.a     // Catch:{ Exception -> 0x0743 }
                long r14 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0743 }
                r16 = 0     // Catch:{ Exception -> 0x0743 }
                long r14 = r14 - r9     // Catch:{ Exception -> 0x0743 }
                r7.DurationHttpGetCommand = r14     // Catch:{ Exception -> 0x0743 }
                long r9 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0743 }
                java.io.InputStream r7 = r13.getInputStream()     // Catch:{ Exception -> 0x0743 }
                r22 = r5
                r5 = -1
                r15 = 0
            L_0x05e3:
                com.startapp.networkTest.d.a.b$a$a r14 = r1.a(r7)     // Catch:{ all -> 0x0720 }
                r37 = r2
                int r2 = r14.a     // Catch:{ all -> 0x071e }
                r38 = r11
                r39 = r12
                long r11 = (long) r2
                long r11 = r11 + r15
                java.lang.String r2 = r14.b     // Catch:{ all -> 0x0719 }
                java.lang.String r2 = r2.toUpperCase()     // Catch:{ all -> 0x0719 }
                java.lang.String r15 = "HTTP"     // Catch:{ all -> 0x0719 }
                boolean r15 = r2.startsWith(r15)     // Catch:{ all -> 0x0719 }
                if (r15 == 0) goto L_0x064b
                java.lang.String r14 = " "
                java.lang.String[] r2 = r2.split(r14)     // Catch:{ all -> 0x0645 }
                com.startapp.networkTest.results.ConnectivityTestResult r14 = r1.a     // Catch:{ all -> 0x0645 }
                r15 = 1     // Catch:{ all -> 0x0645 }
                r2 = r2[r15]     // Catch:{ all -> 0x0645 }
                int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ all -> 0x0645 }
                r14.HTTPStatus = r2     // Catch:{ all -> 0x0645 }
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a     // Catch:{ all -> 0x0645 }
                int r2 = r2.HTTPStatus     // Catch:{ all -> 0x0645 }
                r14 = 200(0xc8, float:2.8E-43)     // Catch:{ all -> 0x0645 }
                if (r2 == r14) goto L_0x063d     // Catch:{ all -> 0x0645 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0645 }
                r2.<init>()     // Catch:{ all -> 0x0645 }
                com.startapp.networkTest.results.ConnectivityTestResult r14 = r1.a     // Catch:{ all -> 0x0645 }
                java.lang.String r15 = r14.ErrorReason     // Catch:{ all -> 0x0645 }
                r2.append(r15)     // Catch:{ all -> 0x0645 }
                java.lang.String r15 = "Request failed! Unexcepted HTTP code: "     // Catch:{ all -> 0x0645 }
                r2.append(r15)     // Catch:{ all -> 0x0645 }
                com.startapp.networkTest.results.ConnectivityTestResult r15 = r1.a     // Catch:{ all -> 0x0645 }
                int r15 = r15.HTTPStatus     // Catch:{ all -> 0x0645 }
                r2.append(r15)     // Catch:{ all -> 0x0645 }
                java.lang.String r15 = "; "     // Catch:{ all -> 0x0645 }
                r2.append(r15)     // Catch:{ all -> 0x0645 }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0645 }
                r14.ErrorReason = r2     // Catch:{ all -> 0x0645 }
                r22 = 0
            L_0x063d:
                r15 = r11
                r2 = r37
                r11 = r38
                r12 = r39
                goto L_0x05e3
            L_0x0645:
                r0 = move-exception
                r2 = r0
                r40 = r4
                goto L_0x072b
            L_0x064b:
                java.lang.String r15 = "CONTENT-LENGTH:"
                boolean r15 = r2.startsWith(r15)     // Catch:{ all -> 0x0719 }
                r40 = r4
                r4 = 15
                if (r15 == 0) goto L_0x0668
                java.lang.String r2 = r2.substring(r4)     // Catch:{ NumberFormatException -> 0x06ab }
                java.lang.String r2 = r2.trim()     // Catch:{ NumberFormatException -> 0x06ab }
                int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ NumberFormatException -> 0x06ab }
                r5 = r2
                goto L_0x06ab
            L_0x0665:
                r0 = move-exception
                goto L_0x071c
            L_0x0668:
                java.lang.String r15 = "X-AMZ-CF-ID:"
                boolean r15 = r2.startsWith(r15)     // Catch:{ all -> 0x0665 }
                if (r15 == 0) goto L_0x067f     // Catch:{ all -> 0x0665 }
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a     // Catch:{ all -> 0x0665 }
                java.lang.String r14 = r14.b     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = r14.substring(r4)     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = r4.trim()     // Catch:{ all -> 0x0665 }
                r2.AmazonId = r4     // Catch:{ all -> 0x0665 }
                goto L_0x06ab     // Catch:{ all -> 0x0665 }
            L_0x067f:
                boolean r4 = r14.c     // Catch:{ all -> 0x0665 }
                if (r4 != 0) goto L_0x06b6     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = ""     // Catch:{ all -> 0x0665 }
                boolean r4 = r2.equals(r4)     // Catch:{ all -> 0x0665 }
                if (r4 != 0) goto L_0x06b6     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = "X-AMZ-CF-POP:"     // Catch:{ all -> 0x0665 }
                boolean r2 = r2.startsWith(r4)     // Catch:{ all -> 0x0665 }
                if (r2 == 0) goto L_0x06ab     // Catch:{ all -> 0x0665 }
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = r14.b     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = r4.toLowerCase()     // Catch:{ all -> 0x0665 }
                r14 = 13     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = r4.substring(r14)     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = r4.trim()     // Catch:{ all -> 0x0665 }
                java.lang.String r4 = com.startapp.networkTest.utils.e.b(r4)     // Catch:{ all -> 0x0665 }
                r2.AirportCode = r4     // Catch:{ all -> 0x0665 }
            L_0x06ab:
                r15 = r11     // Catch:{ all -> 0x0665 }
                r2 = r37     // Catch:{ all -> 0x0665 }
                r11 = r38     // Catch:{ all -> 0x0665 }
                r12 = r39     // Catch:{ all -> 0x0665 }
                r4 = r40     // Catch:{ all -> 0x0665 }
                goto L_0x05e3     // Catch:{ all -> 0x0665 }
            L_0x06b6:
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a     // Catch:{ all -> 0x0665 }
                r2.HeaderBytesRead = r11     // Catch:{ all -> 0x0665 }
                r2 = -1
                if (r5 == r2) goto L_0x06c4
                int r4 = (int) r11
                int r4 = r4 + r5
                long r14 = (long) r4
                r35 = r14
                r15 = r11
                goto L_0x06ca
            L_0x06c4:
                r15 = r11
                r35 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            L_0x06ca:
                int r4 = r7.read(r6)     // Catch:{ all -> 0x0717 }
                if (r4 != r2) goto L_0x06de     // Catch:{ all -> 0x0717 }
                int r2 = (r15 > r35 ? 1 : (r15 == r35 ? 0 : -1))     // Catch:{ all -> 0x0717 }
                if (r2 < 0) goto L_0x06d6     // Catch:{ all -> 0x0717 }
            L_0x06d4:
                r4 = r15     // Catch:{ all -> 0x0717 }
                goto L_0x06e7     // Catch:{ all -> 0x0717 }
            L_0x06d6:
                java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x0717 }
                java.lang.String r3 = "Could not read all bytes"     // Catch:{ all -> 0x0717 }
                r2.<init>(r3)     // Catch:{ all -> 0x0717 }
                throw r2     // Catch:{ all -> 0x0717 }
            L_0x06de:
                long r11 = (long) r4
                long r15 = r15 + r11
                int r5 = (r15 > r35 ? 1 : (r15 == r35 ? 0 : -1))
                if (r5 >= 0) goto L_0x06d4
                if (r4 > 0) goto L_0x06ca
                goto L_0x06d4
            L_0x06e7:
                if (r22 == 0) goto L_0x06f8
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a     // Catch:{ all -> 0x06f4 }
                r6 = 1     // Catch:{ all -> 0x06f4 }
                r2.Success = r6     // Catch:{ all -> 0x06f4 }
                int r2 = r3.successfulTests     // Catch:{ all -> 0x06f4 }
                int r2 = r2 + r6     // Catch:{ all -> 0x06f4 }
                r3.successfulTests = r2     // Catch:{ all -> 0x06f4 }
                goto L_0x06f8
            L_0x06f4:
                r0 = move-exception
                r2 = r0
                r11 = r4
                goto L_0x072b
            L_0x06f8:
                r2 = 0
                int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
                if (r6 <= 0) goto L_0x070c
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a     // Catch:{ Exception -> 0x0740 }
                long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0740 }
                r3 = 0     // Catch:{ Exception -> 0x0740 }
                long r6 = r6 - r9     // Catch:{ Exception -> 0x0740 }
                r2.DurationHttpReceive = r6     // Catch:{ Exception -> 0x0740 }
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a     // Catch:{ Exception -> 0x0740 }
                r2.BytesRead = r4     // Catch:{ Exception -> 0x0740 }
            L_0x070c:
                if (r13 == 0) goto L_0x0711
                r13.close()     // Catch:{ Exception -> 0x0711 }
            L_0x0711:
                r5 = r37
                r25 = 1
                goto L_0x07b4
            L_0x0717:
                r0 = move-exception
                goto L_0x0729
            L_0x0719:
                r0 = move-exception
                r40 = r4
            L_0x071c:
                r2 = r0
                goto L_0x072b
            L_0x071e:
                r0 = move-exception
                goto L_0x0723
            L_0x0720:
                r0 = move-exception
                r37 = r2
            L_0x0723:
                r40 = r4
                r38 = r11
                r39 = r12
            L_0x0729:
                r2 = r0
                r11 = r15
            L_0x072b:
                r3 = 0
                int r5 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
                if (r5 <= 0) goto L_0x0742
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a     // Catch:{ Exception -> 0x0740 }
                long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0740 }
                r6 = 0     // Catch:{ Exception -> 0x0740 }
                long r4 = r4 - r9     // Catch:{ Exception -> 0x0740 }
                r3.DurationHttpReceive = r4     // Catch:{ Exception -> 0x0740 }
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a     // Catch:{ Exception -> 0x0740 }
                r3.BytesRead = r11     // Catch:{ Exception -> 0x0740 }
                goto L_0x0742     // Catch:{ Exception -> 0x0740 }
            L_0x0740:
                r0 = move-exception     // Catch:{ Exception -> 0x0740 }
                goto L_0x074c     // Catch:{ Exception -> 0x0740 }
            L_0x0742:
                throw r2     // Catch:{ Exception -> 0x0740 }
            L_0x0743:
                r0 = move-exception
                r37 = r2
                r40 = r4
                r38 = r11
                r39 = r12
            L_0x074c:
                r2 = r0
                r5 = r37
                r25 = 1
                goto L_0x0779
            L_0x0752:
                r0 = move-exception
                r37 = r2
                r40 = r4
                r38 = r11
                r39 = r12
                r2 = r0
                r5 = r37
                goto L_0x0777
            L_0x075f:
                r0 = move-exception
                r40 = r4
                r38 = r11
                r39 = r12
                goto L_0x0775
            L_0x0767:
                r0 = move-exception
                r2 = r0
                goto L_0x07a1
            L_0x076a:
                r0 = move-exception
                r40 = r4
                r34 = r7
                r38 = r11
                r39 = r12
                r21 = 0
            L_0x0775:
                r2 = r0
            L_0x0776:
                r5 = 0
            L_0x0777:
                r25 = 0
            L_0x0779:
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0767 }
                java.lang.String r2 = com.startapp.networkTest.d.a.b.a(r2)     // Catch:{ all -> 0x0767 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0767 }
                r3.<init>()     // Catch:{ all -> 0x0767 }
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.a     // Catch:{ all -> 0x0767 }
                java.lang.String r6 = r4.ErrorReason     // Catch:{ all -> 0x0767 }
                r3.append(r6)     // Catch:{ all -> 0x0767 }
                r3.append(r2)     // Catch:{ all -> 0x0767 }
                java.lang.String r2 = "; "     // Catch:{ all -> 0x0767 }
                r3.append(r2)     // Catch:{ all -> 0x0767 }
                java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x0767 }
                r4.ErrorReason = r2     // Catch:{ all -> 0x0767 }
                if (r13 == 0) goto L_0x07b4
                r13.close()     // Catch:{ Exception -> 0x07b4 }
                goto L_0x07b4
            L_0x07a1:
                if (r13 == 0) goto L_0x07a6
                r13.close()     // Catch:{ Exception -> 0x07a6 }
            L_0x07a6:
                throw r2
            L_0x07a7:
                r40 = r4
                r34 = r7
                r38 = r11
                r39 = r12
                r21 = 0
                r5 = 0
                r25 = 0
            L_0x07b4:
                if (r25 == 0) goto L_0x07f2
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.data.RadioInfo r2 = r2.RadioInfo
                com.startapp.networkTest.enums.ConnectionTypes r2 = r2.ConnectionType
                com.startapp.networkTest.enums.ConnectionTypes r3 = com.startapp.networkTest.enums.ConnectionTypes.WiFi
                if (r2 != r3) goto L_0x07d1
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.b.a r3 = com.startapp.networkTest.b.a.a()
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.a
                com.startapp.networkTest.data.WifiInfo r4 = r4.WifiInfo
                com.startapp.networkTest.data.IspInfo r3 = r3.a(r4)
                r2.IspInfo = r3
                goto L_0x07f2
            L_0x07d1:
                com.startapp.networkTest.a r2 = com.startapp.networkTest.c.d()
                boolean r2 = r2.s()
                if (r2 == 0) goto L_0x07f2
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.data.RadioInfo r2 = r2.RadioInfo
                com.startapp.networkTest.enums.ConnectionTypes r2 = r2.ConnectionType
                com.startapp.networkTest.enums.ConnectionTypes r3 = com.startapp.networkTest.enums.ConnectionTypes.Mobile
                if (r2 != r3) goto L_0x07f2
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.b.a r3 = com.startapp.networkTest.b.a.a()
                r4 = 0
                com.startapp.networkTest.data.IspInfo r3 = r3.a(r4)
                r2.IspInfo = r3
            L_0x07f2:
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r3 = r3.b
                com.startapp.networkTest.data.RadioInfo r3 = r3.c()
                r2.RadioInfoOnEnd = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                long r3 = android.os.SystemClock.uptimeMillis()
                long r3 = r3 - r19
                r2.DurationOverallNoSleep = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                long r3 = android.os.SystemClock.elapsedRealtime()
                long r3 = r3 - r17
                r2.DurationOverall = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                android.content.Context r3 = r3.a
                com.startapp.networkTest.enums.IdleStates r3 = com.startapp.networkTest.controller.b.e(r3)
                r2.IdleStateOnEnd = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                java.util.ArrayList r3 = new java.util.ArrayList
                r3.<init>(r8)
                r2.MultiCdnInfo = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                r11 = r38
                long r3 = (long) r11
                r12 = r39
                long r6 = (long) r12
                r8 = 4611686018427387904(0x4000000000000000, double:2.0)
                r10 = 4621819117588971520(0x4024000000000000, double:10.0)
                double r8 = java.lang.Math.pow(r10, r8)
                long r8 = java.lang.Math.round(r8)
                long r6 = r6 * r8
                long r3 = r3 + r6
                r6 = r40
                long r6 = (long) r6
                r8 = 4616189618054758400(0x4010000000000000, double:4.0)
                double r8 = java.lang.Math.pow(r10, r8)
                long r8 = java.lang.Math.round(r8)
                long r6 = r6 * r8
                long r3 = r3 + r6
                long r5 = (long) r5
                r7 = 4618441417868443648(0x4018000000000000, double:6.0)
                double r7 = java.lang.Math.pow(r10, r7)
                long r7 = java.lang.Math.round(r7)
                long r5 = r5 * r7
                long r3 = r3 + r5
                r2.ServerMultiSuccess = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                java.lang.String r2 = r2.AirportCode
                boolean r2 = r2.isEmpty()
                if (r2 == 0) goto L_0x0878
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.a
                java.lang.String r3 = r3.ServerIp
                java.lang.String r3 = com.startapp.networkTest.utils.e.a(r3)
                r2.AirportCode = r3
            L_0x0878:
                int r2 = r34.size()
                if (r2 <= 0) goto L_0x0881
                a(r34)
            L_0x0881:
                com.startapp.networkTest.a r2 = com.startapp.networkTest.c.d()
                boolean r2 = r2.A()
                if (r2 == 0) goto L_0x0894
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                com.startapp.networkTest.data.LocationInfo r3 = new com.startapp.networkTest.data.LocationInfo
                r3.<init>()
                r2.LocationInfo = r3
            L_0x0894:
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.a
                return r2
            L_0x0897:
                r0 = move-exception
                r34 = r7
                r21 = 0
                r16 = r3
                r15 = r11
                r19 = r17
                r17 = r23
                r11 = r26
                goto L_0x08e7
            L_0x08a6:
                r0 = move-exception
                r34 = r7
                r31 = r9
                r32 = r10
                r21 = 0
                r16 = r3
                r15 = r11
                r19 = r17
                r17 = r23
                r11 = r26
                goto L_0x0903
            L_0x08b9:
                r0 = move-exception
                r34 = r7
                r31 = r9
                r32 = r10
                r21 = 0
                r16 = r3
                r15 = r11
                goto L_0x08fb
            L_0x08c6:
                r34 = r7
                r31 = r9
                r32 = r10
                r21 = 0
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.a     // Catch:{ Exception -> 0x08dc, all -> 0x08ee }
                r6 = -1     // Catch:{ Exception -> 0x08dc, all -> 0x08ee }
                r5.DurationDNS = r6     // Catch:{ Exception -> 0x08dc, all -> 0x08ee }
                java.util.concurrent.TimeoutException r5 = new java.util.concurrent.TimeoutException     // Catch:{ Exception -> 0x08dc, all -> 0x08ee }
                java.lang.String r6 = "DNS Timeout"     // Catch:{ Exception -> 0x08dc, all -> 0x08ee }
                r5.<init>(r6)     // Catch:{ Exception -> 0x08dc, all -> 0x08ee }
                throw r5     // Catch:{ Exception -> 0x08dc, all -> 0x08ee }
            L_0x08dc:
                r0 = move-exception
                r16 = r3
                r19 = r17
                r17 = r23
                r11 = r26
                r13 = r27
            L_0x08e7:
                r5 = r30
                r9 = r31
                r10 = r32
                goto L_0x0921
            L_0x08ee:
                r0 = move-exception
                goto L_0x0909
            L_0x08f0:
                r0 = move-exception
                r34 = r7
                r31 = r9
                r32 = r10
                r21 = 0
                r16 = r3
            L_0x08fb:
                r19 = r17
                r17 = r23
                r11 = r26
                r13 = r27
            L_0x0903:
                r5 = r30
                goto L_0x0921
            L_0x0906:
                r0 = move-exception
                r30 = r5
            L_0x0909:
                r1 = r0
                goto L_0x09ae
            L_0x090c:
                r0 = move-exception
                r30 = r5
                r34 = r7
                r31 = r9
                r32 = r10
                r21 = 0
                r16 = r3
                r19 = r17
                r17 = r23
                r11 = r26
                r13 = r27
            L_0x0921:
                r3 = r0
                goto L_0x0978
            L_0x0924:
                r0 = move-exception
                r34 = r7
                r21 = 0
                r10 = r3
                r16 = r19
                r11 = r26
                r13 = r27
                r5 = 0
            L_0x0931:
                r3 = r0
                goto L_0x094d
            L_0x0933:
                r0 = move-exception
                r34 = r7
                r21 = 0
                r3 = r0
                r16 = r19
                r11 = r26
                r13 = r27
                goto L_0x094c
            L_0x0940:
                r0 = move-exception
                r34 = r7
                r26 = r11
                r27 = r13
                r21 = 0
                r3 = r0
                r16 = r19
            L_0x094c:
                r5 = 0
            L_0x094d:
                r19 = r17
                r17 = r23
                goto L_0x0978
            L_0x0952:
                r0 = move-exception
                r34 = r7
                r27 = r13
                r21 = 0
                r3 = r0
                r19 = r17
                goto L_0x0965
            L_0x095d:
                r0 = move-exception
                r34 = r7
                r27 = r13
                r21 = 0
                r3 = r0
            L_0x0965:
                r17 = r23
                goto L_0x0977
            L_0x0968:
                r0 = move-exception
                r21 = 0
            L_0x096b:
                r1 = r0
                r30 = 0
                goto L_0x09ae
            L_0x096f:
                r0 = move-exception
                r34 = r7
                r27 = r13
                r21 = 0
                r3 = r0
            L_0x0977:
                r5 = 0
            L_0x0978:
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x09aa }
                java.lang.String r3 = com.startapp.networkTest.d.a.b.a(r3)     // Catch:{ all -> 0x09aa }
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x09aa }
                r6.<init>()     // Catch:{ all -> 0x09aa }
                com.startapp.networkTest.results.ConnectivityTestResult r7 = r1.a     // Catch:{ all -> 0x09aa }
                java.lang.String r1 = r7.ErrorReason     // Catch:{ all -> 0x09aa }
                r6.append(r1)     // Catch:{ all -> 0x09aa }
                r6.append(r3)     // Catch:{ all -> 0x09aa }
                java.lang.String r1 = "; "     // Catch:{ all -> 0x09aa }
                r6.append(r1)     // Catch:{ all -> 0x09aa }
                java.lang.String r1 = r6.toString()     // Catch:{ all -> 0x09aa }
                r7.ErrorReason = r1     // Catch:{ all -> 0x09aa }
                if (r5 == 0) goto L_0x099f
                r8.add(r4)
            L_0x099f:
                r1 = 1
                int r9 = r9 + r1
                r6 = r22
                r7 = r34
                r1 = r41
                r3 = 1
                goto L_0x02da
            L_0x09aa:
                r0 = move-exception
                r1 = r0
                r30 = r5
            L_0x09ae:
                if (r30 == 0) goto L_0x09b3
                r8.add(r4)
            L_0x09b3:
                throw r1
            L_0x09b4:
                r1 = r4
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.d.a.b.a.a():com.startapp.networkTest.results.ConnectivityTestResult");
        }

        public final void a(SpeedtestEngineStatus speedtestEngineStatus) {
            if (speedtestEngineStatus == SpeedtestEngineStatus.END || speedtestEngineStatus == SpeedtestEngineStatus.ABORTED) {
                this.b.b();
                if (b.this.f != null) {
                    b.this.f.a((LatencyResult) this.b.a());
                    b.this.f.a();
                }
            }
        }

        private List<c> a(String[] strArr, CtCriteriaTypes ctCriteriaTypes) {
            LinkedList linkedList = new LinkedList();
            LinkedList linkedList2 = new LinkedList();
            Set<String> f = com.startapp.networkTest.c.c().f();
            LinkedList<c> linkedList3 = new LinkedList<>();
            if (f != null) {
                for (String a2 : f) {
                    c cVar = (c) com.startapp.common.parser.b.a(a2, c.class);
                    if (cVar != null) {
                        linkedList3.add(cVar);
                    }
                }
            }
            for (String str : strArr) {
                c cVar2 = new c();
                cVar2.address = str;
                linkedList2.add(cVar2);
            }
            for (c cVar3 : linkedList3) {
                for (int i = 0; i < linkedList2.size(); i++) {
                    if (((c) linkedList2.get(i)).address.equals(cVar3.address)) {
                        linkedList2.set(i, cVar3);
                    }
                }
            }
            switch (ctCriteriaTypes) {
                case NoChange:
                    return linkedList2;
                case Random:
                    Collections.shuffle(linkedList2, new Random(System.nanoTime()));
                    return new LinkedList(linkedList2);
                case DNSSuccessful:
                    Collections.sort(linkedList2, new Comparator<c>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((c) obj).DNSSuccess - ((c) obj2).DNSSuccess;
                        }
                    });
                    return new LinkedList(linkedList2);
                case TCPSuccessful:
                    Collections.sort(linkedList2, new Comparator<c>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((c) obj).TCPSuccess - ((c) obj2).TCPSuccess;
                        }
                    });
                    return new LinkedList(linkedList2);
                case FullSuccessful:
                    Collections.sort(linkedList2, new Comparator<c>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((c) obj).successfulTests - ((c) obj2).successfulTests;
                        }
                    });
                    return new LinkedList(linkedList2);
                case TotalTests:
                    Collections.sort(linkedList2, new Comparator<c>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((c) obj).totalTests - ((c) obj2).totalTests;
                        }
                    });
                    return new LinkedList(linkedList2);
                default:
                    return linkedList;
            }
        }

        private static void a(List<c> list) {
            HashSet hashSet = new HashSet();
            for (c cVar : list) {
                hashSet.add(cVar.toString());
            }
            com.startapp.networkTest.c.c().a((Set<String>) hashSet);
        }

        /* JADX WARNING: Removed duplicated region for block: B:28:0x004f A[SYNTHETIC, Splitter:B:28:0x004f] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x005b A[SYNTHETIC, Splitter:B:35:0x005b] */
        private static boolean b() {
            BufferedReader bufferedReader = null;
            try {
                BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("ping -W 3 -c 1 -s 56 127.0.0.1").getInputStream()));
                try {
                    bufferedReader2.readLine();
                    String readLine = bufferedReader2.readLine();
                    if (readLine == null || readLine.length() <= 0 || readLine.split(" ").length != 8) {
                        try {
                            bufferedReader2.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                    try {
                        bufferedReader2.close();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    return true;
                } catch (IOException e3) {
                    e = e3;
                    bufferedReader = bufferedReader2;
                    try {
                        e.printStackTrace();
                        if (bufferedReader != null) {
                        }
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (IOException e4) {
                                e4.printStackTrace();
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedReader = bufferedReader2;
                    if (bufferedReader != null) {
                    }
                    throw th;
                }
            } catch (IOException e5) {
                e = e5;
                e.printStackTrace();
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                return false;
            }
        }
    }

    static {
        b.class.getSimpleName();
    }

    public b(Context context) {
        this.a = context;
        this.e = new com.startapp.networkTest.d(context);
        com.startapp.networkTest.a d2 = com.startapp.networkTest.c.d();
        this.g = d2.a();
        this.h = d2.d();
        this.i = d2.e();
        this.j = d2.f();
        this.l = d2.h();
        this.m = d2.g();
        this.d = new LocationController(context);
        this.b = new c(context);
        this.c = new d(context);
    }

    public final void a() {
        this.d.a(ProviderMode.Passive);
        this.b.a();
    }

    public final void b() {
        this.d.a();
        this.b.b();
    }

    public final void a(e eVar) {
        this.f = eVar;
        if (VERSION.SDK_INT < 11) {
            new a().execute(new Void[0]);
        } else {
            new a().executeOnExecutor(com.startapp.networkTest.threads.a.a().b(), new Void[0]);
        }
    }

    static /* synthetic */ String a(String str) {
        if (str == null || str.isEmpty()) {
            return "";
        }
        return str.replaceAll("(?:[0-9]{1,3}\\.){3}[0-9]{1,3}", "XXX").replaceAll("([A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4}", "XXX");
    }
}
