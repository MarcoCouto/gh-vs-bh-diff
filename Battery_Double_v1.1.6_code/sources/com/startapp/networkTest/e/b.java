package com.startapp.networkTest.e;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.SystemClock;
import com.startapp.networkTest.c;
import com.startapp.networkTest.data.TimeInfo;
import com.startapp.networkTest.enums.TimeSources;
import java.util.Date;
import java.util.TimeZone;

/* compiled from: StartAppSDK */
public class b {
    /* access modifiers changed from: private */
    public static final String a = "b";
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    private boolean d = false;
    /* access modifiers changed from: private */
    public long e;
    /* access modifiers changed from: private */
    public long f = -1;
    /* access modifiers changed from: private */
    public long g = -1;
    private long h = -1;
    private long i = -1;
    /* access modifiers changed from: private */
    public a j = new a();

    /* compiled from: StartAppSDK */
    class a extends AsyncTask<Void, Void, Void> {
        a() {
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final void onPreExecute() {
            b.this.b = true;
        }

        private Void a() {
            try {
                b.a;
                if (b.this.j.a("0.de.pool.ntp.org")) {
                    long a2 = b.this.j.a();
                    if (a2 > 1458564533202L && a2 < 3468524400000L) {
                        b.this.f = SystemClock.elapsedRealtime();
                        b.this.g = a2;
                        b.a;
                        new StringBuilder("Time: ").append(new Date(b.this.g).toString());
                        b.this.c = true;
                    }
                } else {
                    b.a;
                    b.this.e = SystemClock.elapsedRealtime();
                }
            } catch (Exception unused) {
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            b.this.b = false;
        }
    }

    public b() {
        if (c.d().w()) {
            d();
        }
    }

    private void d() {
        if (VERSION.SDK_INT < 11) {
            new a().execute(new Void[0]);
        } else {
            new a().executeOnExecutor(com.startapp.networkTest.threads.a.a().b(), new Void[0]);
        }
    }

    private void e() {
        if (c.d().w() && !this.b && SystemClock.elapsedRealtime() - this.e > 30000) {
            d();
        }
    }

    public static TimeInfo a() {
        long j2;
        b b2 = c.b();
        TimeInfo timeInfo = new TimeInfo();
        timeInfo.IsSynced = b2.c || b2.d;
        if (b2.d && b2.h > b2.f) {
            j2 = b2.i + (SystemClock.elapsedRealtime() - b2.h);
            timeInfo.DeviceDriftMillis = System.currentTimeMillis() - j2;
            timeInfo.MillisSinceLastSync = j2 - b2.i;
            timeInfo.TimeSource = TimeSources.GPS;
            if (SystemClock.elapsedRealtime() - b2.f > 28800000) {
                b2.e();
            }
        } else if (b2.c) {
            if (SystemClock.elapsedRealtime() - b2.f > 28800000) {
                b2.e();
            }
            long elapsedRealtime = b2.g + (SystemClock.elapsedRealtime() - b2.f);
            timeInfo.DeviceDriftMillis = System.currentTimeMillis() - elapsedRealtime;
            timeInfo.MillisSinceLastSync = elapsedRealtime - b2.g;
            timeInfo.TimeSource = TimeSources.NTP;
            j2 = elapsedRealtime;
        } else {
            b2.e();
            j2 = System.currentTimeMillis();
            timeInfo.TimeSource = TimeSources.Device;
        }
        timeInfo.TimestampTableau = com.iab.omid.library.startapp.b.a(j2);
        timeInfo.TimestampDateTime = com.iab.omid.library.startapp.b.b(j2);
        timeInfo.TimestampOffset = (double) (((((float) TimeZone.getDefault().getOffset(j2)) / 1000.0f) / 60.0f) / 60.0f);
        timeInfo.TimestampMillis = j2;
        com.startapp.sdk.a.b c2 = com.iab.omid.library.startapp.b.c(j2);
        timeInfo.year = c2.a;
        timeInfo.month = c2.b;
        timeInfo.day = c2.c;
        timeInfo.hour = c2.d;
        timeInfo.minute = c2.e;
        timeInfo.second = c2.f;
        timeInfo.millisecond = c2.g;
        return timeInfo;
    }

    public static long b() {
        b b2 = c.b();
        if (b2.d && b2.h > b2.f) {
            if (SystemClock.elapsedRealtime() - b2.f > 28800000) {
                b2.e();
            }
            return b2.i + (SystemClock.elapsedRealtime() - b2.h);
        } else if (b2.c) {
            if (SystemClock.elapsedRealtime() - b2.f > 28800000) {
                b2.e();
            }
            return b2.g + (SystemClock.elapsedRealtime() - b2.f);
        } else {
            b2.e();
            return System.currentTimeMillis();
        }
    }

    public final void a(Location location) {
        this.i = location.getTime();
        this.h = SystemClock.elapsedRealtime();
        this.d = true;
    }
}
