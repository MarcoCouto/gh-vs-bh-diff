package com.startapp.networkTest.e;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/* compiled from: StartAppSDK */
class a {
    private long a;

    a() {
    }

    static {
        a.class.getSimpleName();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0072  */
    public final boolean a(String str) {
        DatagramSocket datagramSocket = null;
        try {
            DatagramSocket datagramSocket2 = new DatagramSocket();
            try {
                datagramSocket2.setSoTimeout(10000);
                byte[] bArr = new byte[48];
                DatagramPacket datagramPacket = new DatagramPacket(bArr, 48, InetAddress.getByName(str), 123);
                bArr[0] = 27;
                a(bArr);
                datagramSocket2.send(datagramPacket);
                datagramSocket2.receive(new DatagramPacket(bArr, 48));
                datagramSocket2.close();
                this.a = ((a(bArr, 32) - 2208988800L) * 1000) + ((a(bArr, 36) * 1000) / 4294967296L);
                datagramSocket2.close();
                return true;
            } catch (Exception e) {
                e = e;
                datagramSocket = datagramSocket2;
                try {
                    new StringBuilder("request time failed: ").append(e);
                    if (datagramSocket != null) {
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    datagramSocket2 = datagramSocket;
                    if (datagramSocket2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (datagramSocket2 != null) {
                    datagramSocket2.close();
                }
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            new StringBuilder("request time failed: ").append(e);
            if (datagramSocket != null) {
                datagramSocket.close();
            }
            return false;
        }
    }

    public final long a() {
        return this.a;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r0v0, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r1v1, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r2v1, types: [byte] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r5v1, types: [byte] */
    private static long a(byte[] bArr, int i) {
        int i2 = bArr[i];
        int i3 = bArr[i + 1];
        int i4 = bArr[i + 2];
        int i5 = bArr[i + 3];
        if (i2 & true) {
            i2 = (i2 & 127) + 128;
        }
        if (i3 & true) {
            i3 = (i3 & 127) + 128;
        }
        if (i4 & true) {
            i4 = (i4 & 127) + 128;
        }
        if (i5 & true) {
            i5 = (i5 & 127) + 128;
        }
        return (((long) i2) << 24) + (((long) i3) << 16) + (((long) i4) << 8) + ((long) i5);
    }

    private static void a(byte[] bArr) {
        for (int i = 40; i < 48; i++) {
            bArr[i] = 0;
        }
    }
}
