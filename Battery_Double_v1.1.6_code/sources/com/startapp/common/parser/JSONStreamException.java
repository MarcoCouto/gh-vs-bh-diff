package com.startapp.common.parser;

/* compiled from: StartAppSDK */
public class JSONStreamException extends Exception {
    public JSONStreamException(String str) {
        super(str);
    }

    public JSONStreamException(String str, Throwable th) {
        super(str, th);
    }
}
