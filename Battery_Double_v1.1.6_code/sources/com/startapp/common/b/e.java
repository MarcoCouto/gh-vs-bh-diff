package com.startapp.common.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.iab.omid.library.startapp.adsession.Owner;
import com.iab.omid.library.startapp.b;
import com.startapp.common.SDKException;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class e {
    private final Owner a;
    private final Owner b;
    private final boolean c;

    /* compiled from: StartAppSDK */
    public static class a {
        private String a;
        private String b;

        public final String a() {
            return this.a;
        }

        public final void a(String str) {
            this.a = str;
        }

        public final String b() {
            return this.b;
        }

        public final void b(String str) {
            this.b = str;
        }

        @NonNull
        public final String toString() {
            StringBuilder sb = new StringBuilder("HttpResult: ");
            sb.append(this.b);
            sb.append(" ");
            sb.append(this.a);
            return sb.toString();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0021, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0022, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0083, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0084, code lost:
        r5 = r9;
        r7 = r11;
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b1, code lost:
        r10.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0021 A[Catch:{ IOException -> 0x0025, all -> 0x0021 }, ExcHandler: all (th java.lang.Throwable), Splitter:B:4:0x0008] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00b1  */
    @Nullable
    public static String a(@NonNull String str, @Nullable byte[] bArr, @Nullable Map<String, String> map, @Nullable String str2, boolean z, @Nullable String str3) throws SDKException {
        InputStream inputStream;
        HttpURLConnection httpURLConnection;
        Throwable th;
        int i;
        String str4;
        OutputStream outputStream;
        try {
            httpURLConnection = b(str, bArr, map, str2, z, str3);
            if (bArr != null) {
                try {
                    if (bArr.length > 0) {
                        try {
                            outputStream = httpURLConnection.getOutputStream();
                            try {
                                outputStream.write(bArr);
                                outputStream.flush();
                                b.a((Closeable) outputStream);
                            } catch (Throwable th2) {
                                th = th2;
                                b.a((Closeable) outputStream);
                                throw th;
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            outputStream = null;
                            b.a((Closeable) outputStream);
                            throw th;
                        }
                    }
                } catch (IOException e) {
                    th = e;
                    inputStream = null;
                    i = 0;
                    try {
                        SDKException sDKException = new SDKException(HttpRequest.METHOD_POST, Uri.parse(str).buildUpon().query(null).build(), i, false, th);
                        throw sDKException;
                    } catch (Throwable th4) {
                        th = th4;
                        b.a((Closeable) inputStream);
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th5) {
                }
            }
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                inputStream = httpURLConnection.getInputStream();
                if (inputStream != null) {
                    try {
                        StringWriter stringWriter = new StringWriter();
                        char[] cArr = new char[1024];
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                        while (true) {
                            int read = bufferedReader.read(cArr);
                            if (read == -1) {
                                break;
                            }
                            stringWriter.write(cArr, 0, read);
                        }
                        str4 = stringWriter.toString();
                    } catch (IOException e2) {
                        i = responseCode;
                        th = e2;
                        SDKException sDKException2 = new SDKException(HttpRequest.METHOD_POST, Uri.parse(str).buildUpon().query(null).build(), i, false, th);
                        throw sDKException2;
                    }
                } else {
                    str4 = null;
                }
                b.a((Closeable) inputStream);
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                return str4;
            }
            throw new SDKException(HttpRequest.METHOD_POST, Uri.parse(str).buildUpon().query(null).build(), responseCode, false);
        } catch (IOException e3) {
            th = e3;
            httpURLConnection = null;
            inputStream = null;
            i = 0;
            SDKException sDKException22 = new SDKException(HttpRequest.METHOD_POST, Uri.parse(str).buildUpon().query(null).build(), i, false, th);
            throw sDKException22;
        } catch (Throwable th6) {
            th = th6;
            httpURLConnection = null;
            inputStream = null;
            b.a((Closeable) inputStream);
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x008a, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008b, code lost:
        r3 = r10;
        r5 = r11;
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008f, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0090, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bf, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x000c] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00bf  */
    @NonNull
    public static a a(@NonNull String str, @Nullable Map<String, String> map, @Nullable String str2, boolean z) throws SDKException {
        InputStream inputStream;
        HttpURLConnection httpURLConnection;
        Throwable th;
        int i;
        try {
            httpURLConnection = b(str, null, map, str2, z, null);
            try {
                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 200) {
                    if (VERSION.SDK_INT >= 9) {
                        CookieManager a2 = com.startapp.common.c.a.a();
                        if (a2 != null) {
                            a2.put(URI.create(str), httpURLConnection.getHeaderFields());
                        }
                    }
                    inputStream = httpURLConnection.getInputStream();
                    try {
                        a aVar = new a();
                        aVar.b(httpURLConnection.getContentType());
                        if (inputStream != null) {
                            StringWriter stringWriter = new StringWriter();
                            char[] cArr = new char[1024];
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                            while (true) {
                                int read = bufferedReader.read(cArr);
                                if (read == -1) {
                                    break;
                                }
                                stringWriter.write(cArr, 0, read);
                            }
                            aVar.a(stringWriter.toString());
                        }
                        b.a((Closeable) inputStream);
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        return aVar;
                    } catch (IOException e) {
                        i = responseCode;
                        th = e;
                        try {
                            SDKException sDKException = new SDKException(HttpRequest.METHOD_GET, Uri.parse(str).buildUpon().query(null).build(), i, false, th);
                            throw sDKException;
                        } catch (Throwable th2) {
                            th = th2;
                            b.a((Closeable) inputStream);
                            if (httpURLConnection != null) {
                            }
                            throw th;
                        }
                    }
                } else {
                    throw new SDKException(HttpRequest.METHOD_GET, Uri.parse(str).buildUpon().query(null).build(), responseCode, true);
                }
            } catch (IOException e2) {
                th = e2;
                inputStream = null;
                i = 0;
                SDKException sDKException2 = new SDKException(HttpRequest.METHOD_GET, Uri.parse(str).buildUpon().query(null).build(), i, false, th);
                throw sDKException2;
            } catch (Throwable th3) {
            }
        } catch (IOException e3) {
            th = e3;
            httpURLConnection = null;
            inputStream = null;
            i = 0;
            SDKException sDKException22 = new SDKException(HttpRequest.METHOD_GET, Uri.parse(str).buildUpon().query(null).build(), i, false, th);
            throw sDKException22;
        } catch (Throwable th4) {
            th = th4;
            httpURLConnection = null;
            inputStream = null;
            b.a((Closeable) inputStream);
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    @NonNull
    private static HttpURLConnection b(@NonNull String str, @Nullable byte[] bArr, @Nullable Map<String, String> map, @Nullable String str2, boolean z, @Nullable String str3) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.addRequestProperty(HttpRequest.HEADER_CACHE_CONTROL, "no-cache");
        com.startapp.common.c.a.a(httpURLConnection, str);
        if (!(str2 == null || str2.compareTo("-1") == 0)) {
            httpURLConnection.addRequestProperty("User-Agent", str2);
        }
        httpURLConnection.setRequestProperty("Accept", "application/json;text/html;text/plain");
        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.setConnectTimeout(10000);
        if (bArr != null) {
            httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setFixedLengthStreamingMode(bArr.length);
            if (str3 != null) {
                httpURLConnection.setRequestProperty("Content-Type", str3);
            }
            if (z) {
                httpURLConnection.setRequestProperty(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
            }
        } else {
            httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
        }
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String str4 = (String) entry.getKey();
                String str5 = (String) entry.getValue();
                if (!(str4 == null || str5 == null)) {
                    httpURLConnection.setRequestProperty(str4, str5);
                }
            }
        }
        return httpURLConnection;
    }

    @SuppressLint({"MissingPermission"})
    public static String a(Context context) {
        String str = "e100";
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                if (b.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
                    str = "e102";
                    if (VERSION.SDK_INT >= 23) {
                        Network activeNetwork = connectivityManager.getActiveNetwork();
                        if (activeNetwork != null) {
                            NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
                            if (networkCapabilities != null) {
                                if (networkCapabilities.hasTransport(1)) {
                                    return "WIFI";
                                }
                                if (networkCapabilities.hasTransport(0)) {
                                    str = c(context);
                                }
                            }
                        }
                    } else {
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                            String typeName = activeNetworkInfo.getTypeName();
                            if (typeName.toLowerCase().compareTo("WIFI".toLowerCase()) == 0) {
                                return "WIFI";
                            }
                            if (typeName.toLowerCase().compareTo("MOBILE".toLowerCase()) == 0) {
                                str = c(context);
                            }
                        }
                    }
                } else {
                    str = "e105";
                }
            }
            return str;
        } catch (Exception unused) {
            return "e105";
        }
    }

    private static String c(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        return telephonyManager != null ? Integer.toString(telephonyManager.getNetworkType()) : "e101";
    }

    @Nullable
    public static Boolean b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null && b.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                return Boolean.valueOf(activeNetworkInfo.isRoaming());
            }
        }
        return null;
    }

    private e(Owner owner, Owner owner2) {
        this.a = owner;
        if (owner2 == null) {
            this.b = Owner.NONE;
        } else {
            this.b = owner2;
        }
        this.c = false;
    }

    public static e a(Owner owner, Owner owner2) {
        b.a((Object) owner, "Impression owner is null");
        if (!owner.equals(Owner.NONE)) {
            return new e(owner, owner2);
        }
        throw new IllegalArgumentException("Impression owner is none");
    }

    public final boolean a() {
        return Owner.NATIVE == this.a;
    }

    public final boolean b() {
        return Owner.NATIVE == this.b;
    }

    public final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        com.iab.omid.library.startapp.d.b.a(jSONObject, "impressionOwner", this.a);
        com.iab.omid.library.startapp.d.b.a(jSONObject, "videoEventsOwner", this.b);
        com.iab.omid.library.startapp.d.b.a(jSONObject, "isolateVerificationScripts", Boolean.FALSE);
        return jSONObject;
    }
}
