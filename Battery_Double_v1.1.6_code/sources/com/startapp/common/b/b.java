package com.startapp.common.b;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.ServiceConnection;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build.VERSION;
import android.os.StatFs;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.GravityCompat;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.webkit.WebView;
import com.startapp.common.Constants;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/* compiled from: StartAppSDK */
public class b {
    private static final String a = a(b.class);

    /* compiled from: StartAppSDK */
    public interface a {
        void a();
    }

    @NonNull
    public static String a(@NonNull Class<?> cls) {
        StringBuilder sb = new StringBuilder("startapp.");
        sb.append(cls.getSimpleName());
        return sb.toString();
    }

    @SuppressLint({"NewApi"})
    public static void a(Editor editor) {
        if (VERSION.SDK_INT < 9) {
            editor.commit();
        } else {
            editor.apply();
        }
    }

    public static void a(Activity activity) {
        if (VERSION.SDK_INT >= 9) {
            a(activity, 7);
        } else {
            a(activity, 1);
        }
    }

    public static void b(Activity activity) {
        if (VERSION.SDK_INT >= 9) {
            a(activity, 6);
        } else {
            a(activity, 0);
        }
    }

    public static boolean a() {
        return VERSION.SDK_INT >= 12;
    }

    public static void a(View view, float f) {
        if (VERSION.SDK_INT >= 11) {
            view.setAlpha(f);
        }
    }

    public static void a(View view) {
        if (VERSION.SDK_INT >= 12) {
            view.animate().alpha(1.0f).setDuration(1500).setListener(null);
        }
    }

    public static void a(ViewTreeObserver viewTreeObserver, OnGlobalLayoutListener onGlobalLayoutListener) {
        if (VERSION.SDK_INT >= 16) {
            viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener);
        } else {
            viewTreeObserver.removeGlobalOnLayoutListener(onGlobalLayoutListener);
        }
    }

    public static boolean a(Context context) {
        try {
            if (VERSION.SDK_INT >= 17) {
                if (VERSION.SDK_INT < 21) {
                    if (Global.getInt(context.getContentResolver(), "install_non_market_apps") != 1) {
                        return false;
                    }
                    return true;
                }
            }
            if (Secure.getInt(context.getContentResolver(), "install_non_market_apps") != 1) {
                return false;
            }
            return true;
        } catch (SettingNotFoundException e) {
            Log.w(a, e);
            return false;
        }
    }

    public static void a(WebView webView) {
        if (VERSION.SDK_INT >= 17) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
    }

    public static int a(Activity activity, int i, boolean z) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int i2 = 0;
        switch (i) {
            case 1:
                if (VERSION.SDK_INT > 8 && !z && (rotation == 1 || rotation == 2)) {
                    i2 = 9;
                    break;
                }
            case 2:
                if (VERSION.SDK_INT > 8 && !z && rotation != 0 && rotation != 1) {
                    i2 = 8;
                    break;
                }
            default:
                i2 = 1;
                break;
        }
        a(activity, i2);
        return i2;
    }

    public static void a(Activity activity, int i) {
        try {
            activity.setRequestedOrientation(i);
        } catch (Exception e) {
            Log.w(a, e);
        }
    }

    public static boolean b() {
        return VERSION.SDK_INT >= 14;
    }

    public static void b(WebView webView) {
        if (VERSION.SDK_INT >= 11) {
            webView.onPause();
            return;
        }
        try {
            Class.forName("android.webkit.WebView").getMethod("onPause", new Class[0]).invoke(webView, new Object[0]);
        } catch (Exception e) {
            Log.w(a, e);
        }
    }

    public static void c(WebView webView) {
        if (VERSION.SDK_INT >= 11) {
            webView.onResume();
            return;
        }
        try {
            Class.forName("android.webkit.WebView").getMethod("onResume", new Class[0]).invoke(webView, new Object[0]);
        } catch (Exception e) {
            Log.w(a, e);
        }
    }

    public static void d(WebView webView) {
        if (VERSION.SDK_INT >= 11) {
            webView.setLayerType(1, null);
        }
    }

    public static void a(View view, final a aVar) {
        if (VERSION.SDK_INT >= 11) {
            view.addOnLayoutChangeListener(new OnLayoutChangeListener() {
                public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    aVar.a();
                }
            });
        }
    }

    public static Long a(MemoryInfo memoryInfo) {
        if (VERSION.SDK_INT >= 16) {
            return Long.valueOf(memoryInfo.totalMem);
        }
        return null;
    }

    public static boolean a(View view, boolean z) {
        if (VERSION.SDK_INT >= 11 && 1 != view.getLayerType() && z) {
            return view.isHardwareAccelerated();
        }
        return false;
    }

    public static long a(File file) {
        if (file != null) {
            try {
                if (file.isDirectory()) {
                    if (VERSION.SDK_INT >= 9) {
                        return file.getFreeSpace();
                    }
                    StatFs statFs = new StatFs(file.getPath());
                    return ((long) statFs.getBlockSize()) * ((long) statFs.getFreeBlocks());
                }
            } catch (Exception e) {
                Log.w(a, e);
                return -1;
            }
        }
        return -1;
    }

    public static boolean b(Context context) {
        return VERSION.SDK_INT >= 17 ? Global.getInt(context.getContentResolver(), "auto_time", 0) > 0 : System.getInt(context.getContentResolver(), "auto_time", 0) > 0;
    }

    public static boolean a(Context context, String str) {
        try {
            return VERSION.SDK_INT >= 23 ? context.checkSelfPermission(str) == 0 : context.checkCallingOrSelfPermission(str) == 0;
        } catch (Exception e) {
            Log.w(a, e);
            return false;
        }
    }

    public static List<CellInfo> a(Context context, TelephonyManager telephonyManager) {
        if (context == null || telephonyManager == null) {
            return null;
        }
        if ((a(context, "android.permission.ACCESS_FINE_LOCATION") || a(context, "android.permission.ACCESS_COARSE_LOCATION")) && VERSION.SDK_INT >= 17) {
            return telephonyManager.getAllCellInfo();
        }
        return null;
    }

    @SuppressLint({"RtlHardcoded"})
    public static int a(int i) {
        if (VERSION.SDK_INT >= 17) {
            return i;
        }
        switch (i) {
            case 16:
                return 0;
            case 17:
                return 1;
            case 20:
                return 9;
            case 21:
                return 11;
            case GravityCompat.START /*8388611*/:
                if (VERSION.SDK_INT < 14) {
                    return 3;
                }
                return i;
            case GravityCompat.END /*8388613*/:
                if (VERSION.SDK_INT < 14) {
                    return 5;
                }
                return i;
            default:
                return i;
        }
    }

    public static String b(Context context, TelephonyManager telephonyManager) {
        if (VERSION.SDK_INT >= 17 && (a(context, "android.permission.ACCESS_FINE_LOCATION") || a(context, "android.permission.ACCESS_COARSE_LOCATION"))) {
            List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
            if (allCellInfo != null) {
                for (CellInfo cellInfo : allCellInfo) {
                    if (cellInfo != null && cellInfo.isRegistered()) {
                        try {
                            Object invoke = cellInfo.getClass().getMethod("getCellSignalStrength", new Class[0]).invoke(cellInfo, new Object[0]);
                            Object invoke2 = invoke.getClass().getMethod("getTimingAdvance", new Class[0]).invoke(invoke, new Object[0]);
                            if (invoke2 instanceof Integer) {
                                return invoke2.toString();
                            }
                        } catch (Exception e) {
                            Log.w(a, e);
                        }
                    }
                }
            }
        }
        return null;
    }

    public static int c(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            Log.w(a, e);
            return 0;
        }
    }

    public static String d(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            Log.w(a, e);
            return null;
        }
    }

    public static String a(String str, Context context) throws FileNotFoundException {
        String str2;
        try {
            str2 = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).sourceDir;
        } catch (Exception e) {
            Log.w(a, e);
            str2 = null;
        }
        if (str2 != null) {
            return a((InputStream) new FileInputStream(str2), str);
        }
        return null;
    }

    private static int k(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.targetSdkVersion;
        } catch (Exception e) {
            Log.w(a, e);
            return VERSION.SDK_INT;
        }
    }

    public static boolean e(Context context) {
        return VERSION.SDK_INT < 26 || k(context) < 26;
    }

    @VisibleForTesting
    private static String a(InputStream inputStream, String str) {
        int i;
        StringBuilder sb = new StringBuilder();
        try {
            byte[] bArr = new byte[8192];
            MessageDigest instance = MessageDigest.getInstance(str);
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                } else if (read > 0) {
                    instance.update(bArr, 0, read);
                }
            }
            for (byte b : instance.digest()) {
                sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
            }
        } catch (Exception e) {
            Log.w(a, e);
        } catch (Throwable th) {
            a((Closeable) inputStream);
            throw th;
        }
        a((Closeable) inputStream);
        return sb.toString().toUpperCase(Locale.ENGLISH);
    }

    public static int f(Context context) {
        int i = 0;
        try {
            for (PackageInfo packageInfo : a(context.getPackageManager())) {
                if (!a(packageInfo) || packageInfo.packageName.equals(Constants.a)) {
                    i++;
                }
            }
        } catch (Exception e) {
            Log.w(a, e);
        }
        return i;
    }

    public static boolean a(PackageInfo packageInfo) {
        return ((packageInfo.applicationInfo.flags & 1) == 0 && (packageInfo.applicationInfo.flags & 128) == 0) ? false : true;
    }

    public static boolean a(Context context, String str, int i) {
        try {
            if (context.getPackageManager().getPackageInfo(str, 128).versionCode >= i) {
                return true;
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    @VisibleForTesting
    public static List<PackageInfo> a(PackageManager packageManager) throws Exception {
        String str = new String("getInstalledPackages");
        return (List) packageManager.getClass().getMethod(str, new Class[]{Integer.TYPE}).invoke(packageManager, new Object[]{Integer.valueOf(8192)});
    }

    public static boolean g(Context context) {
        int i;
        try {
            if (VERSION.SDK_INT < 17) {
                i = Secure.getInt(context.getContentResolver(), "adb_enabled", 0);
            } else {
                i = Global.getInt(context.getContentResolver(), "adb_enabled", 0);
            }
            if (i != 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            Log.w(a, e);
            return false;
        }
    }

    public static boolean h(Context context) {
        try {
            return com.iab.omid.library.startapp.d.a.a(context);
        } catch (Exception e) {
            Log.w(a, e);
            return false;
        }
    }

    public static boolean i(Context context) {
        try {
            return com.startapp.a.a.a.a(context);
        } catch (Exception e) {
            Log.w(a, e);
            return false;
        }
    }

    @SuppressLint({"PackageManagerGetSignatures"})
    public static String j(Context context) {
        try {
            Signature[] signatureArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures;
            if (signatureArr != null && signatureArr.length > 0) {
                if (signatureArr.length == 1) {
                    return signatureArr[0].toCharsString();
                }
                Arrays.sort(signatureArr, new Comparator<Signature>() {
                    public final /* synthetic */ int compare(Object obj, Object obj2) {
                        return ((Signature) obj).toCharsString().compareTo(((Signature) obj2).toCharsString());
                    }
                });
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < signatureArr.length; i++) {
                    sb.append(signatureArr[i].toCharsString());
                    if (i < signatureArr.length - 1) {
                        sb.append(';');
                    }
                }
                return sb.toString();
            }
        } catch (Exception e) {
            Log.w(a, e);
        }
        return null;
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception unused) {
            }
        }
    }

    @AnyThread
    public static void a(@NonNull Context context, @Nullable ServiceConnection serviceConnection) {
        if (serviceConnection != null) {
            try {
                context.unbindService(serviceConnection);
            } catch (Throwable unused) {
            }
        }
    }
}
