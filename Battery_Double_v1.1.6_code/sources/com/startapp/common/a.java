package com.startapp.common;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.common.b.c;

/* compiled from: StartAppSDK */
public final class a {
    String a;
    C0063a b;
    int c;

    /* renamed from: com.startapp.common.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public interface C0063a {
        void a(Bitmap bitmap, int i);
    }

    public a(String str, C0063a aVar, int i) {
        this.a = str;
        this.b = aVar;
        this.c = i;
    }

    public final void a() {
        ThreadManager.a(Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                final Bitmap b = c.b(a.this.a);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        if (a.this.b != null) {
                            a.this.b.a(b, a.this.c);
                        }
                    }
                });
            }
        });
    }
}
