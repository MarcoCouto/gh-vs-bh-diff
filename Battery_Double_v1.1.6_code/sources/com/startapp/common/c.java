package com.startapp.common;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;

/* compiled from: StartAppSDK */
public final class c {
    private static volatile c a;
    private volatile PhoneStateListener b = null;
    /* access modifiers changed from: private */
    public volatile String c = "e106";

    public final void a(Context context) {
        a(context, 0);
    }

    private void a(Context context, int i) {
        ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).listen(this.b, i);
    }

    public static synchronized void b(final Context context) {
        synchronized (c.class) {
            if (a == null) {
                a = new c();
                new Thread(new Runnable() {
                    public final void run() {
                        Looper.prepare();
                        try {
                            c.a(c.a(), context);
                        } catch (Throwable unused) {
                            c.a().c = "e107";
                        }
                        Looper.loop();
                    }
                }).start();
            }
        }
    }

    public static c a() {
        return a;
    }

    public final String b() {
        return this.c;
    }

    static /* synthetic */ void a(c cVar, Context context) {
        cVar.b = new PhoneStateListener() {
            /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
                com.startapp.common.c.a(r4.a, "e104");
             */
            /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
                return;
             */
            /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
            /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x002f */
            public final void onSignalStrengthsChanged(SignalStrength signalStrength) {
                try {
                    if (VERSION.SDK_INT >= 23) {
                        c.this.c = String.valueOf(signalStrength.getLevel());
                        return;
                    }
                    c.this.c = String.valueOf(SignalStrength.class.getMethod("getLevel", new Class[0]).invoke(signalStrength, new Object[0]));
                } catch (Exception unused) {
                    c.this.c = "e105";
                }
            }
        };
        cVar.a(context, 256);
    }
}
