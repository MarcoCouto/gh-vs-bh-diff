package com.startapp.common.c;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.webkit.WebView;
import com.iab.omid.library.startapp.adsession.AdSessionContextType;
import com.iab.omid.library.startapp.b;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.networkTest.utils.d;
import com.startapp.sdk.ads.banner.banner3d.Banner3DSize;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class a {
    private static CookieManager a;
    private final d b;
    private final WebView c;
    private final List<Banner3DSize> d = new ArrayList();
    private final String e;
    private final String f;
    private final AdSessionContextType g;

    public static void a(Context context) {
        if (VERSION.SDK_INT >= 9) {
            a = new CookieManager(new b(context), CookiePolicy.ACCEPT_ALL);
        }
    }

    public static void a(HttpURLConnection httpURLConnection, String str) throws IOException {
        if (VERSION.SDK_INT >= 9) {
            CookieManager cookieManager = a;
            if (cookieManager != null) {
                Map map = cookieManager.get(URI.create(str), httpURLConnection.getRequestProperties());
                if (map != null && map.size() > 0 && ((List) map.get("Cookie")).size() > 0) {
                    httpURLConnection.addRequestProperty("Cookie", TextUtils.join(RequestParameters.EQUAL, (Iterable) map.get("Cookie")));
                }
            }
        }
    }

    public static CookieManager a() {
        return a;
    }

    private a(d dVar, WebView webView, String str, List<Banner3DSize> list, String str2) {
        AdSessionContextType adSessionContextType;
        this.b = dVar;
        this.c = webView;
        this.e = str;
        if (list != null) {
            this.d.addAll(list);
            adSessionContextType = AdSessionContextType.NATIVE;
        } else {
            adSessionContextType = AdSessionContextType.HTML;
        }
        this.g = adSessionContextType;
        this.f = str2;
    }

    public static a a(d dVar, WebView webView, String str) {
        b.a((Object) dVar, "Partner is null");
        b.a((Object) webView, "WebView is null");
        b.c(str, "CustomReferenceData is greater than 256 characters");
        a aVar = new a(dVar, webView, null, null, str);
        return aVar;
    }

    public static a a(d dVar, String str, List<Banner3DSize> list, String str2) {
        b.a((Object) dVar, "Partner is null");
        b.a((Object) str, "OMID JS script content is null");
        b.a((Object) list, "VerificationScriptResources is null");
        b.c(str2, "CustomReferenceData is greater than 256 characters");
        a aVar = new a(dVar, null, str, list, str2);
        return aVar;
    }

    public final d b() {
        return this.b;
    }

    public final List<Banner3DSize> c() {
        return Collections.unmodifiableList(this.d);
    }

    public final WebView d() {
        return this.c;
    }

    public final String e() {
        return this.f;
    }

    public final String f() {
        return this.e;
    }

    public final AdSessionContextType g() {
        return this.g;
    }
}
