package com.startapp.common;

import android.os.Build.VERSION;
import android.util.Log;
import com.startapp.common.b.b;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: StartAppSDK */
public final class ThreadManager {
    /* access modifiers changed from: private */
    public static final String a = b.a(ThreadManager.class);
    private static final int b = (VERSION.SDK_INT < 22 ? 10 : 20);
    private static final int c = (VERSION.SDK_INT < 22 ? 4 : 8);
    private static final ThreadFactory d = new ThreadFactory() {
        private final AtomicInteger a = new AtomicInteger(1);

        public final Thread newThread(Runnable runnable) {
            StringBuilder sb = new StringBuilder("highPriorityThreadFactory #");
            sb.append(this.a.getAndIncrement());
            return new Thread(runnable, sb.toString());
        }
    };
    private static final ThreadFactory e = new ThreadFactory() {
        private final AtomicInteger a = new AtomicInteger(1);

        public final Thread newThread(Runnable runnable) {
            StringBuilder sb = new StringBuilder("defaultPriorityThreadFactory #");
            sb.append(this.a.getAndIncrement());
            return new Thread(runnable, sb.toString());
        }
    };
    private static final RejectedExecutionHandler f = new RejectedExecutionHandler() {
        public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            Log.e(ThreadManager.a, "ThreadPoolExecutor rejected execution! ".concat(String.valueOf(threadPoolExecutor)));
        }
    };
    private static final Executor g;
    private static final Executor h;
    private static final ScheduledExecutorService i = new ScheduledThreadPoolExecutor(1);

    /* compiled from: StartAppSDK */
    public enum Priority {
        DEFAULT,
        HIGH
    }

    static {
        int i2 = b;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(i2, i2, 20, TimeUnit.SECONDS, new LinkedBlockingQueue(), d, f);
        g = threadPoolExecutor;
        int i3 = c;
        ThreadPoolExecutor threadPoolExecutor2 = new ThreadPoolExecutor(i3, i3, 20, TimeUnit.SECONDS, new LinkedBlockingQueue(), e, f);
        h = threadPoolExecutor2;
    }

    public static ScheduledFuture<?> a(Runnable runnable, long j) {
        return i.schedule(runnable, j, TimeUnit.MILLISECONDS);
    }

    public static void a(Priority priority, Runnable runnable) {
        Executor executor;
        try {
            if (priority.equals(Priority.HIGH)) {
                executor = g;
            } else {
                executor = h;
            }
            executor.execute(runnable);
        } catch (Exception e2) {
            Log.e(a, "executeWithPriority failed to execute! ".concat(String.valueOf(null)), e2);
        }
    }
}
