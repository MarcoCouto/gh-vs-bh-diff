package com.startapp.common;

import android.net.Uri;

/* compiled from: StartAppSDK */
public class SDKException extends Exception {
    private String b;
    private Uri c;
    private int d;
    private boolean e;

    public final int a() {
        return this.d;
    }

    public final boolean b() {
        return this.e;
    }

    public SDKException(String str, Uri uri, int i, boolean z) {
        this(str, uri, i, z, null);
    }

    public SDKException(String str, Uri uri, int i, boolean z, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(' ');
        sb.append(uri);
        sb.append(i != 0 ? ", status ".concat(String.valueOf(i)) : "");
        sb.append(z ? ", retry" : "");
        super(sb.toString(), th);
        this.b = str;
        this.c = uri;
        this.d = i;
        this.e = z;
    }

    public SDKException() {
    }

    public SDKException(String str, Throwable th) {
        super(str, th);
    }
}
