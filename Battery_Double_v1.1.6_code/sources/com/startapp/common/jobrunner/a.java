package com.startapp.common.jobrunner;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ServiceInfo;
import android.os.Build.VERSION;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.startapp.common.b.b;
import com.startapp.common.jobrunner.RunnerRequest.NetworkType;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.common.jobrunner.interfaces.RunnerJob.Result;
import com.startapp.sdk.adsbase.InfoEventService;
import com.startapp.sdk.adsbase.PeriodicJobService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: StartAppSDK */
public class a {
    /* access modifiers changed from: private */
    public static final String a = b.a(a.class);
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static volatile a b;
    private static volatile int c = 60000;
    private static final ExecutorService i = Executors.newSingleThreadExecutor();
    private static final ScheduledExecutorService j = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */
    public Context d;
    private List<com.startapp.common.jobrunner.interfaces.a> e = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public Map<Integer, Integer> f = new ConcurrentHashMap();
    private AtomicInteger g = new AtomicInteger(0);
    private boolean h;

    private static int b(int i2, boolean z) {
        return z ? i2 | Integer.MIN_VALUE : i2;
    }

    public static void b() {
    }

    public static void c() {
    }

    private a(Context context) {
        this.d = context.getApplicationContext();
        this.h = d(context);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:4|5|(7:7|(1:9)|10|11|12|13|(5:15|(5:17|18|19|(4:21|(1:23)|24|42)(1:43)|25)|27|28|(3:30|(1:32)|33)))|34|35) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x008b */
    public static a a(Context context) {
        if (b == null) {
            synchronized (a.class) {
                if (b == null) {
                    if (context.getApplicationContext() != null) {
                        context = context.getApplicationContext();
                    }
                    b = new a(context);
                    SharedPreferences sharedPreferences = context.getSharedPreferences("com.startapp.android.publish.RunnerPrefsFile", 0);
                    String str = null;
                    String string = sharedPreferences.getString("RegisteredClassesNames", null);
                    if (string != null) {
                        String[] split = string.split(",");
                        StringBuilder sb = new StringBuilder(string.length());
                        for (String str2 : split) {
                            try {
                                Class cls = Class.forName(str2);
                                if (com.startapp.common.jobrunner.interfaces.a.class.isAssignableFrom(cls)) {
                                    b.e.add((com.startapp.common.jobrunner.interfaces.a) cls.newInstance());
                                    if (sb.length() > 0) {
                                        sb.append(',');
                                    }
                                    sb.append(str2);
                                }
                            } catch (Throwable unused) {
                            }
                        }
                        if (!sb.toString().equals(string)) {
                            Editor edit = sharedPreferences.edit();
                            String str3 = "RegisteredClassesNames";
                            if (sb.length() > 0) {
                                str = sb.toString();
                            }
                            edit.putString(str3, str).commit();
                        }
                    }
                }
            }
        }
        return b;
    }

    public static void a(com.startapp.common.jobrunner.interfaces.a aVar) {
        b.e.add(aVar);
        String name = aVar.getClass().getName();
        SharedPreferences sharedPreferences = b.d.getSharedPreferences("com.startapp.android.publish.RunnerPrefsFile", 0);
        String string = sharedPreferences.getString("RegisteredClassesNames", null);
        if (string == null) {
            sharedPreferences.edit().putString("RegisteredClassesNames", name).commit();
            return;
        }
        if (!string.contains(name)) {
            StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(",");
            sb.append(name);
            sharedPreferences.edit().putString("RegisteredClassesNames", sb.toString()).commit();
        }
    }

    public final void a() {
        if (b.e(this.d)) {
            Intent intent = new Intent(this.d, InfoEventService.class);
            intent.putExtra("__RUNNER_TASK_ID__", Integer.MAX_VALUE);
            this.d.startService(intent);
            return;
        }
        JobScheduler c2 = c(this.d);
        if (c2 != null) {
            Builder minimumLatency = new Builder(Integer.MAX_VALUE, new ComponentName(this.d, PeriodicJobService.class)).setMinimumLatency(ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
            if (b.a(this.d, "android.permission.RECEIVE_BOOT_COMPLETED")) {
                minimumLatency.setPersisted(true);
            }
            JobInfo pendingJob = c2.getPendingJob(Integer.MAX_VALUE);
            JobInfo build = minimumLatency.build();
            if (pendingJob == null || !pendingJob.getService().equals(build.getService())) {
                c2.schedule(build);
            }
        }
    }

    public static boolean a(final RunnerRequest runnerRequest) {
        try {
            final int b2 = b(runnerRequest.a(), runnerRequest.e());
            StringBuilder sb = new StringBuilder("schedule ");
            sb.append(b2);
            sb.append(" ");
            sb.append(runnerRequest);
            if (!b.h) {
                final int incrementAndGet = b.g.incrementAndGet();
                AnonymousClass1 r3 = new Runnable() {
                    public final void run() {
                        Integer num = (Integer) a.b.f.get(Integer.valueOf(b2));
                        if (num != null && num.intValue() == incrementAndGet) {
                            if (!runnerRequest.e()) {
                                a.b.f.remove(Integer.valueOf(b2));
                            }
                            a.b(runnerRequest, (com.startapp.common.jobrunner.interfaces.RunnerJob.a) new com.startapp.common.jobrunner.interfaces.RunnerJob.a() {
                                public final void a(Result result) {
                                }
                            });
                        }
                    }
                };
                if (runnerRequest.e()) {
                    j.scheduleAtFixedRate(r3, runnerRequest.d(), runnerRequest.d(), TimeUnit.MILLISECONDS);
                } else {
                    j.schedule(r3, runnerRequest.c(), TimeUnit.MILLISECONDS);
                }
                b.f.put(Integer.valueOf(b2), Integer.valueOf(incrementAndGet));
                return true;
            } else if (f()) {
                return a(b2, runnerRequest);
            } else {
                return b(b2, runnerRequest);
            }
        } catch (Exception unused) {
            return false;
        }
    }

    @TargetApi(21)
    private static boolean a(int i2, RunnerRequest runnerRequest) {
        JobScheduler c2 = c(b.d);
        if (c2 == null) {
            return false;
        }
        PersistableBundle persistableBundle = new PersistableBundle();
        Map b2 = runnerRequest.b();
        for (String str : b2.keySet()) {
            persistableBundle.putString(str, (String) b2.get(str));
        }
        persistableBundle.putInt("__RUNNER_RECURRING_ID__", runnerRequest.e() ? 1 : 0);
        persistableBundle.putLong("__RUNNER_TRIGGER_ID__", runnerRequest.c());
        Builder builder = new Builder(i2, new ComponentName(b.d, PeriodicJobService.class));
        builder.setExtras(persistableBundle);
        if (runnerRequest.e()) {
            builder.setPeriodic(runnerRequest.c());
        } else {
            builder.setMinimumLatency(runnerRequest.c()).setOverrideDeadline(runnerRequest.c() + ((long) c));
        }
        int i3 = runnerRequest.f() == NetworkType.WIFI ? 2 : runnerRequest.f() == NetworkType.ANY ? 1 : 0;
        builder.setRequiredNetworkType(i3);
        if (runnerRequest.g() && b.a(b.d, "android.permission.RECEIVE_BOOT_COMPLETED")) {
            builder.setPersisted(true);
        }
        if (c2.schedule(builder.build()) == 1) {
            return true;
        }
        return false;
    }

    private static boolean b(int i2, RunnerRequest runnerRequest) {
        AlarmManager b2 = b(b.d);
        if (b2 == null) {
            return false;
        }
        Intent intent = new Intent(b.d, InfoEventService.class);
        Map b3 = runnerRequest.b();
        for (String str : b3.keySet()) {
            intent.putExtra(str, (String) b3.get(str));
        }
        intent.putExtra("__RUNNER_TASK_ID__", i2);
        intent.putExtra("__RUNNER_RECURRING_ID__", runnerRequest.e());
        intent.putExtra("__RUNNER_TRIGGER_ID__", runnerRequest.c());
        PendingIntent service = PendingIntent.getService(b.d, i2, intent, 134217728);
        b2.cancel(service);
        if (runnerRequest.e()) {
            b2.setRepeating(0, System.currentTimeMillis() + runnerRequest.d(), runnerRequest.c(), service);
        } else {
            b2.set(3, SystemClock.elapsedRealtime() + runnerRequest.c(), service);
        }
        return true;
    }

    @SuppressLint({"NewApi"})
    public static void a(int i2, boolean z) {
        try {
            int b2 = b(i2, z);
            if (!b.h) {
                b.f.remove(Integer.valueOf(b2));
            } else if (f()) {
                JobScheduler c2 = c(b.d);
                if (c2 != null) {
                    c2.cancel(b2);
                }
            } else {
                AlarmManager b3 = b(b.d);
                if (b3 != null) {
                    Intent intent = new Intent(b.d, InfoEventService.class);
                    Context context = b.d;
                    PendingIntent service = PendingIntent.getService(context, b2, intent, 134217728);
                    if (PendingIntent.getService(context, 0, intent, 268435456) != null) {
                        b3.cancel(service);
                        service.cancel();
                    }
                }
            }
        } catch (Exception unused) {
        }
    }

    public static boolean a(Intent intent, @NonNull com.startapp.common.jobrunner.interfaces.RunnerJob.a aVar) {
        new StringBuilder("runJob ").append(intent != 0 ? intent : "NULL");
        if (intent == 0) {
            return false;
        }
        b(a(intent), aVar);
        return true;
    }

    @TargetApi(21)
    public static boolean a(@NonNull JobParameters jobParameters, @NonNull com.startapp.common.jobrunner.interfaces.RunnerJob.a aVar) {
        new StringBuilder("runJob ").append(jobParameters);
        return b(a(jobParameters), aVar);
    }

    /* access modifiers changed from: private */
    public static boolean b(final RunnerRequest runnerRequest, @NonNull final com.startapp.common.jobrunner.interfaces.RunnerJob.a aVar) {
        StringBuilder sb = new StringBuilder("RunnerJob ");
        sb.append(runnerRequest.a());
        sb.append(" ");
        sb.append(runnerRequest.a() & Integer.MAX_VALUE);
        final int a2 = runnerRequest.a() & Integer.MAX_VALUE;
        final RunnerJob a3 = a(a2);
        if (a3 != null) {
            i.execute(new Runnable() {
                public final void run() {
                    a3.a(a.b.d, new com.startapp.common.jobrunner.interfaces.RunnerJob.a() {
                        public final void a(Result result) {
                            a.a;
                            StringBuilder sb = new StringBuilder("job.execute ");
                            sb.append(runnerRequest.a());
                            sb.append(" ");
                            sb.append(result);
                            a.b();
                            if (result == Result.RESCHEDULE && !runnerRequest.e()) {
                                a.a(runnerRequest);
                            }
                            aVar.a(result);
                        }
                    });
                }
            });
        } else {
            i.execute(new Runnable() {
                public final void run() {
                    a.a;
                    new StringBuilder("runJob: failed to get job for ID ").append(runnerRequest.a());
                    a.b();
                    aVar.a(Result.FAILED);
                }
            });
        }
        return true;
    }

    private static RunnerJob a(int i2) {
        RunnerJob runnerJob = null;
        for (com.startapp.common.jobrunner.interfaces.a a2 : b.e) {
            runnerJob = a2.a(i2);
            if (runnerJob != null) {
                break;
            }
        }
        return runnerJob;
    }

    private static RunnerRequest a(@NonNull Intent intent) {
        HashMap hashMap;
        int intExtra = intent.getIntExtra("__RUNNER_TASK_ID__", -1);
        boolean booleanExtra = intent.getBooleanExtra("__RUNNER_RECURRING_ID__", false);
        long longExtra = intent.getLongExtra("__RUNNER_TRIGGER_ID__", 0);
        if (intent.getExtras() != null) {
            hashMap = new HashMap(intent.getExtras().keySet().size());
            for (String str : intent.getExtras().keySet()) {
                Object obj = intent.getExtras().get(str);
                if (obj instanceof String) {
                    hashMap.put(str, (String) obj);
                }
            }
        } else {
            hashMap = null;
        }
        return new com.startapp.common.jobrunner.RunnerRequest.a(intExtra).a((Map<String, String>) hashMap).a(booleanExtra).a(longExtra).b();
    }

    @TargetApi(21)
    private static RunnerRequest a(@NonNull JobParameters jobParameters) {
        PersistableBundle extras = jobParameters.getExtras();
        boolean z = true;
        if (extras.getInt("__RUNNER_RECURRING_ID__") != 1) {
            z = false;
        }
        long j2 = extras.getLong("__RUNNER_TRIGGER_ID__", 0);
        HashMap hashMap = new HashMap(extras.keySet().size());
        for (String str : extras.keySet()) {
            Object obj = extras.get(str);
            if (obj instanceof String) {
                hashMap.put(str, (String) obj);
            }
        }
        return new com.startapp.common.jobrunner.RunnerRequest.a(jobParameters.getJobId()).a((Map<String, String>) hashMap).a(z).a(j2).b();
    }

    @Nullable
    private static AlarmManager b(Context context) {
        return (AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM);
    }

    @Nullable
    @TargetApi(21)
    private static JobScheduler c(Context context) {
        return (JobScheduler) context.getSystemService("jobscheduler");
    }

    private static boolean f() {
        return VERSION.SDK_INT >= 21;
    }

    private static boolean d(Context context) {
        try {
            for (ServiceInfo serviceInfo : context.getPackageManager().getPackageInfo(context.getPackageName(), 4).services) {
                if (serviceInfo.name.equals(InfoEventService.class.getName())) {
                    return true;
                }
            }
        } catch (Throwable unused) {
        }
        return false;
    }
}
