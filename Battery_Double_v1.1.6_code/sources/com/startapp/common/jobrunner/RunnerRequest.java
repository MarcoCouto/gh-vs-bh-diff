package com.startapp.common.jobrunner;

import java.util.HashMap;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class RunnerRequest {
    private final a a;

    /* compiled from: StartAppSDK */
    public enum NetworkType {
        NONE,
        ANY,
        WIFI
    }

    /* compiled from: StartAppSDK */
    public static class a {
        /* access modifiers changed from: private */
        public int a;
        /* access modifiers changed from: private */
        public Map<String, String> b = new HashMap();
        /* access modifiers changed from: private */
        public long c;
        /* access modifiers changed from: private */
        public long d = 100;
        /* access modifiers changed from: private */
        public boolean e = false;
        /* access modifiers changed from: private */
        public boolean f = false;
        /* access modifiers changed from: private */
        public NetworkType g = NetworkType.NONE;

        public a(int i) {
            this.a = i;
        }

        public final a a(Map<String, String> map) {
            if (map != null) {
                this.b.putAll(map);
            }
            return this;
        }

        public final a a(long j) {
            this.c = j;
            return this;
        }

        public final a a(boolean z) {
            this.e = z;
            return this;
        }

        public final a a(NetworkType networkType) {
            this.g = networkType;
            return this;
        }

        public final a a() {
            this.f = true;
            return this;
        }

        public final RunnerRequest b() {
            return new RunnerRequest(this, 0);
        }
    }

    /* synthetic */ RunnerRequest(a aVar, byte b) {
        this(aVar);
    }

    private RunnerRequest(a aVar) {
        this.a = aVar;
    }

    public final int a() {
        return this.a.a;
    }

    public final Map<String, String> b() {
        return this.a.b;
    }

    public final long c() {
        return this.a.c;
    }

    public final long d() {
        return this.a.d;
    }

    public final boolean e() {
        return this.a.e;
    }

    public final NetworkType f() {
        return this.a.g;
    }

    public final boolean g() {
        return this.a.f;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("RunnerRequest: ");
        sb.append(this.a.a);
        sb.append(" ");
        sb.append(this.a.c);
        sb.append(" ");
        sb.append(this.a.e);
        sb.append(" ");
        sb.append(this.a.d);
        sb.append(" ");
        sb.append(this.a.b);
        return sb.toString();
    }
}
