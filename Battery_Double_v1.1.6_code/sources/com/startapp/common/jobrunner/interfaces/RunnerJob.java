package com.startapp.common.jobrunner.interfaces;

import android.content.Context;

/* compiled from: StartAppSDK */
public interface RunnerJob {

    /* compiled from: StartAppSDK */
    public enum Result {
        SUCCESS,
        FAILED,
        RESCHEDULE
    }

    /* compiled from: StartAppSDK */
    public interface a {
        void a(Result result);
    }

    void a(Context context, a aVar);
}
