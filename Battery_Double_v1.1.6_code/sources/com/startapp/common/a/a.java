package com.startapp.common.a;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: StartAppSDK */
public final class a {
    @NonNull
    private final String a;
    @NonNull
    private final String b;
    private final boolean c;
    @Nullable
    private final Throwable[] d;

    @NonNull
    public final String a() {
        return this.a;
    }

    @NonNull
    public final String b() {
        return this.b;
    }

    public final boolean c() {
        return this.c;
    }

    @Nullable
    public final Throwable[] d() {
        return this.d;
    }

    a(@NonNull String str, @NonNull String str2, boolean z) {
        this.a = str;
        this.b = str2;
        this.c = z;
        this.d = null;
    }

    a(@NonNull Throwable... thArr) {
        this.a = "0";
        this.b = "";
        this.c = false;
        this.d = thArr;
    }
}
