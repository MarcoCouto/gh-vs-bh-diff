package com.startapp.common.a;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.NonNull;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: StartAppSDK */
class b implements ServiceConnection {
    @NonNull
    private final LinkedBlockingQueue<IBinder> a = new LinkedBlockingQueue<>(1);
    private boolean b = false;

    public void onServiceDisconnected(ComponentName componentName) {
    }

    b() {
    }

    static {
        b.class.getSimpleName();
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.a.put(iBinder);
        } catch (InterruptedException unused) {
        }
    }

    @NonNull
    public final IBinder a() throws InterruptedException {
        if (!this.b) {
            IBinder iBinder = (IBinder) this.a.take();
            if (iBinder != null) {
                this.b = true;
                return iBinder;
            }
            throw new IllegalStateException("Binder is null");
        }
        throw new IllegalStateException("Binder already retrieved");
    }
}
