package com.startapp.common.a;

import android.os.Build.VERSION;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.annotation.NonNull;

/* compiled from: StartAppSDK */
final class c implements IInterface {
    @NonNull
    private final IBinder a;

    public c(@NonNull IBinder iBinder) {
        this.a = iBinder;
    }

    @NonNull
    public final IBinder asBinder() {
        return this.a;
    }

    @NonNull
    public final String a() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
            this.a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            String readString = obtain2.readString();
            if (readString != null) {
                return readString;
            }
            if (VERSION.SDK_INT < 15) {
                throw new RemoteException();
            }
            throw new RemoteException("Receive null from remote service");
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public final boolean b() throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
            boolean z = true;
            obtain.writeInt(1);
            this.a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = false;
            }
            return z;
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }
}
