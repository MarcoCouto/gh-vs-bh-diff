package com.startapp.common.a;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.b.b;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: StartAppSDK */
public final class d {
    @NonNull
    protected final Context a;
    @NonNull
    protected final Object b = new Object();
    @Nullable
    protected volatile a c;
    @Nullable
    protected volatile Future<a> d;

    @AnyThread
    public d(@NonNull Context context) {
        this.a = context.getApplicationContext();
    }

    @AnyThread
    @NonNull
    public final Future<a> a() {
        synchronized (this.b) {
            Future<a> future = this.d;
            if (future != null) {
                return future;
            }
            final ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
            Future<a> submit = newSingleThreadExecutor.submit(new Callable<a>() {
                /* access modifiers changed from: private */
                @WorkerThread
                @NonNull
                /* renamed from: a */
                public a call() {
                    a aVar;
                    try {
                        Context context = d.this.a;
                        Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{context});
                        String str = (String) invoke.getClass().getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
                        if (str == null || str.length() <= 0) {
                            throw new Exception("Local advertising id not found");
                        }
                        aVar = new a(str, "APP", Boolean.TRUE.equals((Boolean) invoke.getClass().getMethod(RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0])));
                        synchronized (d.this.b) {
                            d.this.d = null;
                        }
                        newSingleThreadExecutor.shutdown();
                        d.this.c = aVar;
                        return aVar;
                    } catch (Throwable th) {
                        aVar = new a(th, th);
                    }
                }
            });
            this.d = submit;
            return submit;
        }
    }

    @AnyThread
    @NonNull
    public final a b() {
        a aVar;
        a aVar2 = this.c;
        if (aVar2 != null) {
            return aVar2;
        }
        try {
            aVar = (a) a().get(1, TimeUnit.SECONDS);
            if (aVar == null) {
                aVar = new a(new Exception("impossible null returned"));
            }
        } catch (TimeoutException unused) {
            aVar = new a(new TimeoutException("Getting advertisingId took too much time."));
        } catch (Throwable th) {
            return new a(th);
        }
        return aVar;
    }

    @WorkerThread
    @NonNull
    public static a a(@NonNull Context context) throws Exception {
        b bVar;
        Throwable th;
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
            intent.setPackage("com.google.android.gms");
            bVar = new b();
            try {
                if (context.bindService(intent, bVar, 1)) {
                    c cVar = new c(bVar.a());
                    a aVar = new a(cVar.a(), "DEVICE", cVar.b());
                    b.a(context, (ServiceConnection) bVar);
                    return aVar;
                }
                throw new Exception("Google Play connection failed");
            } catch (Throwable th2) {
                th = th2;
                b.a(context, (ServiceConnection) bVar);
                throw th;
            }
        } catch (Throwable th3) {
            bVar = null;
            th = th3;
            b.a(context, (ServiceConnection) bVar);
            throw th;
        }
    }
}
