package com.startapp.sdk.adsbase.h;

import android.content.Context;
import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.BaseResponse;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

/* compiled from: StartAppSDK */
public class a {
    static {
        a.class.getSimpleName();
    }

    public static <T extends BaseResponse> T a(Context context, String str, c cVar, Class<T> cls) throws SDKException {
        return (BaseResponse) s.a(b(context, str, cVar).a(), cls);
    }

    public static com.startapp.common.b.e.a a(Context context, String str, c cVar) throws SDKException {
        return b(context, str, cVar);
    }

    public static boolean a(Context context, String str) throws SDKException {
        b(context, str, null);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0057, code lost:
        return com.startapp.common.b.e.a(r3, r1, com.startapp.sdk.adsbase.j.a(r7, "User-Agent", "-1"), com.startapp.sdk.adsbase.remoteconfig.MetaData.D().w());
     */
    private static com.startapp.common.b.e.a b(Context context, String str, c cVar) throws SDKException {
        String str2;
        if (cVar != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(cVar.a().toString());
            str2 = sb.toString();
        } else {
            str2 = str;
        }
        Map a = a(context);
        String str3 = str2;
        int i = 1;
        while (true) {
            if (cVar == null || i <= 1) {
                break;
            }
            try {
                cVar.b(i - 1);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(cVar.a().toString());
                str3 = sb2.toString();
                break;
            } catch (SDKException e) {
                if (!e.b() || i >= 3) {
                    break;
                }
                if (!(e.a() == 0 || !MetaData.D().n().contains(Integer.valueOf(e.a())))) {
                    break;
                }
                i++;
                throw e;
            }
        }
        throw e;
    }

    private static byte[] a(byte[] bArr) throws IOException {
        GZIPOutputStream gZIPOutputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(byteArrayOutputStream);
            try {
                gZIPOutputStream2.write(bArr);
                gZIPOutputStream2.flush();
                gZIPOutputStream2.close();
                return byteArrayOutputStream.toByteArray();
            } catch (IOException e) {
                e = e;
                gZIPOutputStream = gZIPOutputStream2;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                gZIPOutputStream = gZIPOutputStream2;
                if (gZIPOutputStream != null) {
                    try {
                        gZIPOutputStream.close();
                    } catch (Exception unused) {
                    }
                }
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            throw e;
        }
    }

    private static Map<String, String> a(Context context) {
        HashMap hashMap = new HashMap();
        if (!MetaData.D().O()) {
            String str = null;
            try {
                String a = com.startapp.sdk.b.c.a(context).d().b().a();
                try {
                    str = URLEncoder.encode(a, "UTF-8");
                } catch (Throwable th) {
                    Throwable th2 = th;
                    str = a;
                    th = th2;
                    new e(th).a(context);
                    hashMap.put("device-id", str);
                    hashMap.put("Accept-Language", ((com.startapp.sdk.c.b.a) com.startapp.sdk.b.c.a(context).a().c()).c());
                    return hashMap;
                }
            } catch (Throwable th3) {
                th = th3;
                new e(th).a(context);
                hashMap.put("device-id", str);
                hashMap.put("Accept-Language", ((com.startapp.sdk.c.b.a) com.startapp.sdk.b.c.a(context).a().c()).c());
                return hashMap;
            }
            hashMap.put("device-id", str);
        }
        hashMap.put("Accept-Language", ((com.startapp.sdk.c.b.a) com.startapp.sdk.b.c.a(context).a().c()).c());
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0038, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003b, code lost:
        r13 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0063, code lost:
        throw new com.startapp.common.SDKException("failed compressing json to gzip", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006b, code lost:
        throw new com.startapp.common.SDKException("failed encoding json to UTF-8", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006e, code lost:
        r2 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0079, code lost:
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007f, code lost:
        if (r18 > 0) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        java.lang.Thread.sleep(r18);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0036 A[ExcHandler: IOException (r0v7 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:8:0x0016] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0038 A[ExcHandler: UnsupportedEncodingException (r0v6 'e' java.io.UnsupportedEncodingException A[CUSTOM_DECLARE]), Splitter:B:8:0x0016] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0085 A[ADDED_TO_REGION, SYNTHETIC] */
    public static boolean a(Context context, String str, c cVar, int i, long j) throws SDKException {
        byte[] bArr;
        byte[] bytes;
        Object f = cVar != null ? cVar.f() : null;
        Map a = a(context);
        byte[] bArr2 = null;
        boolean z = false;
        int i2 = 1;
        while (!z) {
            if (f != null) {
                try {
                    bytes = f.toString().getBytes("UTF-8");
                    bArr = MetaData.D().w() ? a(bytes) : bytes;
                } catch (SDKException e) {
                    e = e;
                    Context context2 = context;
                    bArr2 = bytes;
                    if (!e.b()) {
                    }
                    throw e;
                } catch (UnsupportedEncodingException e2) {
                } catch (IOException e3) {
                }
            } else {
                bArr = bArr2;
            }
            Context context3 = context;
            com.startapp.common.b.e.a(str, bArr, a, j.a(context, "User-Agent", "-1"), MetaData.D().w(), "application/json");
            bArr2 = bArr;
            z = true;
        }
        return true;
    }
}
