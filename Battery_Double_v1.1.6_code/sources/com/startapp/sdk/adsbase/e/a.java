package com.startapp.sdk.adsbase.e;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.finsky.externalreferrer.a.C0017a;
import com.startapp.common.parser.d;
import com.startapp.networkTest.enums.bluetooth.BluetoothConnectionState;
import com.startapp.sdk.adsbase.infoevents.e;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class a {
    /* access modifiers changed from: private */
    public static CountDownLatch b;
    /* access modifiers changed from: private */
    public static volatile b c;
    public boolean a = false;
    private BluetoothConnectionState d = BluetoothConnectionState.Unknown;
    private BluetoothConnectionState e = BluetoothConnectionState.Unknown;
    private BluetoothConnectionState f = BluetoothConnectionState.Unknown;
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    private ArrayList<com.startapp.networkTest.data.d> g = new ArrayList<>();
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    private ArrayList<com.startapp.networkTest.data.d> h = new ArrayList<>();
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    private ArrayList<com.startapp.networkTest.data.d> i = new ArrayList<>();
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    private ArrayList<com.startapp.networkTest.data.d> j = new ArrayList<>();

    /* renamed from: com.startapp.sdk.adsbase.e.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    static final class C0076a implements ServiceConnection {
        private String a;

        /* synthetic */ C0076a(String str, byte b) {
            this(str);
        }

        private C0076a(String str) {
            this.a = str;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            com.google.android.finsky.externalreferrer.a a2 = C0017a.a(iBinder);
            Bundle bundle = new Bundle();
            bundle.putString("package_name", this.a);
            try {
                a.c = new b(a2.a(bundle));
            } catch (RemoteException unused) {
            }
            a.b.countDown();
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            a.b.countDown();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:16|17|18|19) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x006f */
    public static b a(Context context) {
        if (c == null) {
            try {
                b = new CountDownLatch(1);
                C0076a aVar = new C0076a(context.getPackageName(), 0);
                Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
                intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
                List queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
                if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
                    ResolveInfo resolveInfo = (ResolveInfo) queryIntentServices.get(0);
                    if (resolveInfo.serviceInfo != null) {
                        String str = resolveInfo.serviceInfo.packageName;
                        String str2 = resolveInfo.serviceInfo.name;
                        if ("com.android.vending".equals(str) && str2 != null && b(context)) {
                            if (context.bindService(new Intent(intent), aVar, 1)) {
                                b.await(1, TimeUnit.SECONDS);
                                context.unbindService(aVar);
                            } else {
                                throw new Exception("failed to connect to referrer service");
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
        return c;
    }

    private static boolean b(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            }
            return false;
        } catch (NameNotFoundException unused) {
            return false;
        }
    }
}
