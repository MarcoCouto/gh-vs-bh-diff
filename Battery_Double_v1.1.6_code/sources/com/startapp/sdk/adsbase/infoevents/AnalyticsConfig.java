package com.startapp.sdk.adsbase.infoevents;

import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class AnalyticsConfig implements Serializable {
    private static final String a = new String("https://infoevent.startappservice.com/tracking/infoEvent");
    private static final long serialVersionUID = 1;
    public boolean dns = false;
    public String hostPeriodic = a;
    public String hostSecured = a;
    private int retryNum = 3;
    private int retryTime = 10;
    private boolean sendHopsOnFirstSucceededSmartRedirect = false;
    private float succeededSmartRedirectInfoProbability = 0.01f;

    public final String a() {
        return this.hostPeriodic != null ? this.hostPeriodic : a;
    }

    public final int b() {
        return this.retryNum;
    }

    public final long c() {
        return TimeUnit.SECONDS.toMillis((long) this.retryTime);
    }

    public final float d() {
        return this.succeededSmartRedirectInfoProbability;
    }

    public final boolean e() {
        return this.sendHopsOnFirstSucceededSmartRedirect;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AnalyticsConfig analyticsConfig = (AnalyticsConfig) obj;
        return this.dns == analyticsConfig.dns && this.retryNum == analyticsConfig.retryNum && this.retryTime == analyticsConfig.retryTime && Float.compare(analyticsConfig.succeededSmartRedirectInfoProbability, this.succeededSmartRedirectInfoProbability) == 0 && this.sendHopsOnFirstSucceededSmartRedirect == analyticsConfig.sendHopsOnFirstSucceededSmartRedirect && s.b(this.hostSecured, analyticsConfig.hostSecured) && s.b(this.hostPeriodic, analyticsConfig.hostPeriodic);
    }

    public int hashCode() {
        return s.a(this.hostSecured, this.hostPeriodic, Boolean.valueOf(this.dns), Integer.valueOf(this.retryNum), Integer.valueOf(this.retryTime), Float.valueOf(this.succeededSmartRedirectInfoProbability), Boolean.valueOf(this.sendHopsOnFirstSucceededSmartRedirect));
    }
}
