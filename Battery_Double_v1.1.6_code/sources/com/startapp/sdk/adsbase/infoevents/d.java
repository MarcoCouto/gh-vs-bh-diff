package com.startapp.sdk.adsbase.infoevents;

import android.content.Context;
import android.os.Build.VERSION;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.h.a;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.b.c;
import java.io.File;

/* compiled from: StartAppSDK */
public class d implements Runnable {
    private final Context a;
    private final e b;
    private final c c;
    private final Exception d = new Exception();

    static {
        d.class.getSimpleName();
    }

    public d(Context context, e eVar, c cVar) {
        this.a = context;
        this.b = eVar;
        this.c = cVar;
    }

    private Throwable a(Throwable th) {
        if (VERSION.SDK_INT >= 19) {
            Exception exc = this.d;
        }
        return th;
    }

    public final void a() {
        AnalyticsConfig analyticsConfig = MetaData.D().analytics;
        if (analyticsConfig == null || analyticsConfig.dns) {
            if (this.c != null) {
                this.c.a();
            }
            return;
        }
        ThreadManager.a(Priority.DEFAULT, (Runnable) this);
    }

    public void run() {
        e eVar = this.b;
        String[] strArr = null;
        String str = null;
        String[] strArr2 = null;
        while (eVar != null) {
            if (eVar.g().e()) {
                if (strArr == null) {
                    strArr = new String[]{s.j(this.a)};
                }
                eVar.k(strArr[0]);
            }
            String[] strArr3 = strArr;
            if (eVar.g().b()) {
                if (str == null) {
                    str = s.e(this.a);
                }
                eVar.h(str);
                if (strArr2 == null) {
                    strArr2 = s.k(this.a);
                }
                eVar.j(strArr2[0]);
                eVar.i(strArr2[1]);
            }
            String str2 = str;
            String[] strArr4 = strArr2;
            InfoEventCategory g = eVar.g();
            if (g.h()) {
                SimpleTokenUtils.b(this.a);
            }
            eVar.d(this.a);
            if (g.b()) {
                try {
                    eVar.b(this.a, null);
                } catch (Throwable unused) {
                }
            }
            if (g.c()) {
                try {
                    eVar.c(this.a);
                } catch (Throwable th) {
                    new e(a(th)).a(this.a);
                }
            }
            if (g.d()) {
                try {
                    eVar.e(this.a);
                } catch (Throwable th2) {
                    new e(a(th2)).a(this.a);
                }
            }
            if (g.f()) {
                try {
                    eVar.a((AdPreferences) null, this.a);
                } catch (Throwable th3) {
                    new e(a(th3)).a(this.a);
                }
            }
            if (g.g()) {
                try {
                    eVar.b(this.a);
                } catch (Throwable th4) {
                    new e(a(th4)).a(this.a);
                }
            }
            try {
                eVar.a(c.a(this.a).e().a((Object) eVar));
            } catch (Throwable th5) {
                new e(a(th5)).a(this.a);
            }
            File i = eVar.i();
            if (i != null) {
                try {
                    eVar.f(s.b(i));
                } catch (Throwable th6) {
                    new e(a(th6)).a(this.a);
                }
            }
            try {
                AnalyticsConfig analyticsConfig = MetaData.D().analytics;
                String j = eVar.j();
                if (j == null) {
                    if (InfoEventCategory.PERIODIC.equals(g)) {
                        j = analyticsConfig.a();
                    } else {
                        j = analyticsConfig.hostSecured;
                    }
                }
                a.a(this.a, j, eVar, analyticsConfig.b(), analyticsConfig.c());
                if (this.c != null) {
                    this.c.a(eVar, true);
                }
            } catch (Throwable th7) {
                if (this.c != null) {
                    this.c.a(eVar, false);
                }
                throw th7;
            }
            eVar = eVar.k();
            strArr = strArr3;
            str = str2;
            strArr2 = strArr4;
        }
        if (this.c != null) {
            this.c.a();
        }
    }
}
