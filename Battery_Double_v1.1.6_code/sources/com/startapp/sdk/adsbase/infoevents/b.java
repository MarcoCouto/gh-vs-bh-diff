package com.startapp.sdk.adsbase.infoevents;

import android.content.Context;
import com.startapp.common.d;
import com.startapp.sdk.adsbase.d.a;
import com.startapp.sdk.adsbase.d.c;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: StartAppSDK */
public class b {
    private Context a;
    private d b;
    private ArrayList<a> c;
    private final a d;
    private final AtomicBoolean e;
    private int f;

    static /* synthetic */ int a(b bVar) {
        int i = bVar.f - 1;
        bVar.f = i;
        return i;
    }

    static {
        b.class.getSimpleName();
    }

    public b(Context context) {
        this(context, false, null);
    }

    public b(Context context, boolean z, d dVar) {
        this.d = new a(InfoEventCategory.PERIODIC);
        int i = 0;
        this.e = new AtomicBoolean(false);
        this.a = context;
        this.b = dVar;
        this.d.a(z);
        AnonymousClass1 r5 = new Runnable() {
            public final void run() {
                synchronized (b.this) {
                    if (b.a(b.this) == 0) {
                        b.this.c();
                    }
                }
            }
        };
        if (MetaData.D().z().b()) {
            if (this.c == null) {
                this.c = new ArrayList<>();
            }
            this.c.add(new c(context, r5, this.d));
        }
        if (MetaData.D().A().b()) {
            if (this.c == null) {
                this.c = new ArrayList<>();
            }
            this.c.add(new com.startapp.sdk.adsbase.d.b(context, r5, this.d));
        }
        if (this.c != null) {
            i = this.c.size();
        }
        this.f = i;
    }

    public final synchronized void a() {
        if (this.f > 0) {
            if (this.e.compareAndSet(false, true)) {
                for (int i = 0; i < this.f; i++) {
                    if (this.c != null) {
                        ((a) this.c.get(i)).a();
                    }
                }
                return;
            }
        }
        c();
    }

    /* access modifiers changed from: private */
    public void c() {
        this.d.a(this.a);
        this.e.set(false);
        if (this.b != null) {
            this.b.a(null);
        }
    }

    public final a b() {
        return this.d;
    }
}
