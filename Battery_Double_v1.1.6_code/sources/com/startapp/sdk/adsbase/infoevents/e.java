package com.startapp.sdk.adsbase.infoevents;

import android.content.Context;
import android.util.Pair;
import com.startapp.common.SDKException;
import com.startapp.common.b.a;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.c.b;
import com.startapp.sdk.adsbase.i.h;
import com.startapp.sdk.adsbase.i.l;
import com.startapp.sdk.adsbase.i.s;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;
import org.json.JSONArray;

/* compiled from: StartAppSDK */
public class e extends c {
    private final InfoEventCategory b;
    private String c;
    private String d;
    private boolean e;
    private String f;
    private String g;
    private String h;
    private String i;
    private Long j;
    private String k;
    private String l;
    private Object m;
    private File n;
    private String o;
    private e p;
    private Object q;
    private Throwable r;

    static {
        e.class.getSimpleName();
    }

    public final InfoEventCategory g() {
        return this.b;
    }

    public final e e(String str) {
        if (this.b != InfoEventCategory.EXCEPTION) {
            this.c = str;
        }
        return this;
    }

    public final e f(String str) {
        this.d = str;
        return this;
    }

    public final e h() {
        this.e = false;
        return this;
    }

    public final e g(String str) {
        this.f = str;
        return this;
    }

    public final e h(String str) {
        this.g = str;
        return this;
    }

    public final e i(String str) {
        this.h = str;
        return this;
    }

    public final e j(String str) {
        this.i = str;
        return this;
    }

    public final void a(long j2) {
        this.j = Long.valueOf(j2);
    }

    public final e k(String str) {
        this.l = str;
        return this;
    }

    public final e a(JSONArray jSONArray) {
        this.m = jSONArray;
        return this;
    }

    public final File i() {
        return this.n;
    }

    public final e a(File file) {
        this.n = file;
        return this;
    }

    public final String j() {
        return this.o;
    }

    public final e l(String str) {
        this.o = str;
        return this;
    }

    public final e k() {
        return this.p;
    }

    public final e a(e eVar) {
        this.p = eVar;
        return this;
    }

    public final Object l() {
        return this.q;
    }

    public final void a(Object obj) {
        this.q = obj;
    }

    public e(InfoEventCategory infoEventCategory) {
        super(8);
        this.e = true;
        if (infoEventCategory != InfoEventCategory.EXCEPTION) {
            this.b = infoEventCategory;
        } else {
            this.b = InfoEventCategory.ERROR;
        }
        if (this.b == InfoEventCategory.ERROR || this.b == InfoEventCategory.GENERAL) {
            this.k = s.a(s.f());
        }
    }

    public e(Throwable th) {
        super(8);
        this.e = true;
        this.b = InfoEventCategory.EXCEPTION;
        this.d = a(th);
        this.c = s.a(b.a(th));
        this.k = s.a(s.f());
        this.r = th;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:3|4|5) */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001d, code lost:
        return r3.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        return r3.getMessage();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0019 */
    private static String a(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintWriter printWriter = new PrintWriter(s.a((OutputStream) byteArrayOutputStream));
        b.a(th, printWriter);
        printWriter.close();
        return byteArrayOutputStream.toString();
    }

    public final void a(Context context) {
        Context l2 = s.l(context);
        if (l2 != null) {
            new d(l2, this, null).a();
        }
    }

    public final void a(Context context, c cVar) {
        Context l2 = s.l(context);
        if (l2 == null) {
            cVar.a();
        } else {
            new d(l2, this, cVar).a();
        }
    }

    public l f() throws SDKException {
        l f2 = super.f();
        if (f2 == null) {
            f2 = new h();
        }
        String l2 = this.j != null ? this.j.toString() : a.d();
        f2.a(a.a(), l2, true);
        f2.a(a.b(), a.b(l2), true);
        f2.a("category", this.b.a(), true);
        f2.a("value", this.c, false);
        f2.a("d", this.f, false);
        f2.a("orientation", this.g, false);
        f2.a("usedRam", this.h, false);
        f2.a("freeRam", this.i, false);
        f2.a("sessionTime", null, false);
        f2.a("appActivity", this.k, false);
        f2.a("details", this.d, false, this.e);
        f2.a("details_json", this.m, false);
        f2.a("cellScanRes", this.l, false);
        Pair c2 = SimpleTokenUtils.c();
        Pair d2 = SimpleTokenUtils.d();
        f2.a((String) c2.first, c2.second, false);
        f2.a((String) d2.first, d2.second, false);
        return f2;
    }

    public String toString() {
        return super.toString();
    }
}
