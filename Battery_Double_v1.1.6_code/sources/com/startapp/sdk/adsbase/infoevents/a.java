package com.startapp.sdk.adsbase.infoevents;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.i.h;
import com.startapp.sdk.adsbase.i.l;

/* compiled from: StartAppSDK */
public final class a extends e {
    private String b;
    private String c;
    private boolean d;
    private String e;

    public a(InfoEventCategory infoEventCategory) {
        super(infoEventCategory);
    }

    public final l f() throws SDKException {
        l f = super.f();
        if (f == null) {
            f = new h();
        }
        f.a("sens", this.b, false);
        f.a("bt", this.c, false);
        f.a("isService", Boolean.valueOf(this.d), false);
        f.a("packagingType", this.e, false);
        return f;
    }

    public final void b(String str) {
        if (str != null) {
            this.b = com.startapp.common.b.a.c(str);
        }
    }

    public final void c(String str) {
        if (str != null) {
            this.c = com.startapp.common.b.a.c(str);
        }
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final void d(String str) {
        this.e = str;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(" DataEventRequest [sensors=");
        sb.append(this.b);
        sb.append(", bluetooth=");
        sb.append(this.c);
        sb.append(", isService=");
        sb.append(this.d);
        sb.append(", packagingType=");
        sb.append(this.e);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
