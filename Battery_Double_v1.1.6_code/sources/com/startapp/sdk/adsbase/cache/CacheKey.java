package com.startapp.sdk.adsbase.cache;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.sdk.adsbase.Ad.AdType;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import java.io.Serializable;
import java.util.Set;

/* compiled from: StartAppSDK */
public class CacheKey implements Serializable {
    private static final long serialVersionUID = 1;
    private String adTag;
    private String advertiserId;
    private Set<String> categories;
    private Set<String> categoriesExclude;
    private String country;
    private boolean forceFullpage;
    private boolean forceOfferWall2D;
    private boolean forceOfferWall3D;
    private boolean forceOverlay;
    private Double minCpm;
    private Placement placement;
    private String template;
    private boolean testMode;
    private AdType type;
    private boolean videoMuted;

    public CacheKey(Placement placement2, AdPreferences adPreferences) {
        this.placement = placement2;
        this.categories = adPreferences.getCategories();
        this.categoriesExclude = adPreferences.getCategoriesExclude();
        this.videoMuted = adPreferences.isVideoMuted();
        this.minCpm = adPreferences.getMinCpm();
        this.forceOfferWall3D = adPreferences.isForceOfferWall3D();
        this.forceOfferWall2D = adPreferences.isForceOfferWall2D();
        this.forceFullpage = adPreferences.isForceFullpage();
        this.forceOverlay = adPreferences.isForceOverlay();
        this.testMode = adPreferences.isTestMode();
        this.adTag = adPreferences.getAdTag();
        this.country = adPreferences.getCountry();
        this.advertiserId = adPreferences.getAdvertiserId();
        this.template = adPreferences.getTemplate();
        this.type = adPreferences.getType();
    }

    public final Placement a() {
        return this.placement;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CacheKey cacheKey = (CacheKey) obj;
        return this.forceOfferWall3D == cacheKey.forceOfferWall3D && this.forceOfferWall2D == cacheKey.forceOfferWall2D && this.forceFullpage == cacheKey.forceFullpage && this.forceOverlay == cacheKey.forceOverlay && this.testMode == cacheKey.testMode && this.videoMuted == cacheKey.videoMuted && this.placement == cacheKey.placement && s.b(this.categories, cacheKey.categories) && s.b(this.categoriesExclude, cacheKey.categoriesExclude) && s.b(this.minCpm, cacheKey.minCpm) && s.b(this.adTag, cacheKey.adTag) && s.b(this.country, cacheKey.country) && s.b(this.advertiserId, cacheKey.advertiserId) && s.b(this.template, cacheKey.template) && this.type == cacheKey.type;
    }

    public int hashCode() {
        return s.a(this.placement, this.categories, this.categoriesExclude, this.minCpm, Boolean.valueOf(this.forceOfferWall3D), Boolean.valueOf(this.forceOfferWall2D), Boolean.valueOf(this.forceFullpage), Boolean.valueOf(this.forceOverlay), Boolean.valueOf(this.testMode), Boolean.valueOf(this.videoMuted), this.adTag, this.country, this.advertiserId, this.template, this.type);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CacheKey [placement=");
        sb.append(this.placement);
        sb.append(", categories=");
        sb.append(this.categories);
        sb.append(", categoriesExclude=");
        sb.append(this.categoriesExclude);
        sb.append(", forceOfferWall3D=");
        sb.append(this.forceOfferWall3D);
        sb.append(", forceOfferWall2D=");
        sb.append(this.forceOfferWall2D);
        sb.append(", forceFullpage=");
        sb.append(this.forceFullpage);
        sb.append(", forceOverlay=");
        sb.append(this.forceOverlay);
        sb.append(", testMode=");
        sb.append(this.testMode);
        sb.append(", minCpm=");
        sb.append(this.minCpm);
        sb.append(", country=");
        sb.append(this.country);
        sb.append(", advertiserId=");
        sb.append(this.advertiserId);
        sb.append(", template=");
        sb.append(this.template);
        sb.append(", type=");
        sb.append(this.type);
        sb.append(", adTag=");
        sb.append(this.adTag);
        sb.append(", videoMuted=");
        sb.append(this.videoMuted);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
