package com.startapp.sdk.adsbase.cache;

import android.content.Context;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class CacheMetaData implements Serializable {
    private static volatile CacheMetaData a = new CacheMetaData();
    private static final long serialVersionUID = 1;
    @d(a = true)
    private ACMConfig ACM = new ACMConfig();
    private String cacheMetaDataUpdateVersion = AdsConstants.d;
    private float sendCacheSizeProb = 20.0f;

    public static CacheMetaData a() {
        return a;
    }

    public final ACMConfig b() {
        return this.ACM;
    }

    public static void a(Context context, CacheMetaData cacheMetaData) {
        cacheMetaData.cacheMetaDataUpdateVersion = AdsConstants.d;
        a = cacheMetaData;
        com.startapp.common.b.d.a(context, "StartappCacheMetadata", (Serializable) cacheMetaData);
    }

    public static void a(Context context) {
        CacheMetaData cacheMetaData = (CacheMetaData) com.startapp.common.b.d.a(context, "StartappCacheMetadata");
        CacheMetaData cacheMetaData2 = new CacheMetaData();
        if (cacheMetaData != null) {
            boolean a2 = s.a(cacheMetaData, cacheMetaData2);
            if (!(!AdsConstants.d.equals(cacheMetaData.cacheMetaDataUpdateVersion)) && a2) {
                new e(InfoEventCategory.ERROR).e("metadata_null").a(context);
            }
            a = cacheMetaData;
            return;
        }
        a = cacheMetaData2;
    }

    public final float c() {
        return this.sendCacheSizeProb;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CacheMetaData cacheMetaData = (CacheMetaData) obj;
        return Float.compare(cacheMetaData.sendCacheSizeProb, this.sendCacheSizeProb) == 0 && s.b(this.ACM, cacheMetaData.ACM) && s.b(this.cacheMetaDataUpdateVersion, cacheMetaData.cacheMetaDataUpdateVersion);
    }

    public int hashCode() {
        return s.a(this.ACM, Float.valueOf(this.sendCacheSizeProb), this.cacheMetaDataUpdateVersion);
    }
}
