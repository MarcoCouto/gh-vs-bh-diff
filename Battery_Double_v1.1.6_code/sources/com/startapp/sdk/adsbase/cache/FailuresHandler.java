package com.startapp.sdk.adsbase.cache;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: StartAppSDK */
public class FailuresHandler implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean infiniteLastRetry = true;
    @d(b = ArrayList.class, c = Integer.class)
    private List<Integer> intervals = Arrays.asList(new Integer[]{Integer.valueOf(10), Integer.valueOf(30), Integer.valueOf(60), Integer.valueOf(300)});

    public final List<Integer> a() {
        return this.intervals;
    }

    public final boolean b() {
        return this.infiniteLastRetry;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        FailuresHandler failuresHandler = (FailuresHandler) obj;
        return this.infiniteLastRetry == failuresHandler.infiniteLastRetry && s.b(this.intervals, failuresHandler.intervals);
    }

    public int hashCode() {
        return s.a(this.intervals, Boolean.valueOf(this.infiniteLastRetry));
    }
}
