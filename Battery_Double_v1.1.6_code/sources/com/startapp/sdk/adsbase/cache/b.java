package com.startapp.sdk.adsbase.cache;

import com.startapp.sdk.adsbase.k;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class b extends c {
    private final FailuresHandler b = CacheMetaData.a().b().f();
    private int c = 0;
    private boolean d = false;

    /* access modifiers changed from: protected */
    public final String e() {
        return "CacheErrorReloadTimer";
    }

    public b(e eVar) {
        super(eVar);
    }

    public final void a() {
        super.a();
        this.c = 0;
        this.d = false;
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        if (!k.a().l()) {
            return false;
        }
        if (!((this.b == null || this.b.a() == null) ? false : true)) {
            return false;
        }
        if (this.d) {
            return this.b.b();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final long d() {
        if (this.c >= this.b.a().size()) {
            return -1;
        }
        Long i = i();
        if (i == null) {
            return -1;
        }
        long millis = TimeUnit.SECONDS.toMillis((long) ((Integer) this.b.a().get(this.c)).intValue()) - (System.currentTimeMillis() - i.longValue());
        if (millis >= 0) {
            return millis;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.c == this.b.a().size() - 1) {
            this.d = true;
            new StringBuilder("Reached end index: ").append(this.c);
        } else {
            this.c++;
            new StringBuilder("Advanced to index: ").append(this.c);
        }
        super.b();
    }
}
