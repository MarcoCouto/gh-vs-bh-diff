package com.startapp.sdk.adsbase.cache;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.StartAppAd.AdMode;
import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class ACMConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private long adCacheTTL = 3600;
    @d(b = EnumSet.class, c = AdMode.class)
    private Set<AdMode> autoLoad = EnumSet.of(AdMode.FULLPAGE);
    @d(a = true)
    private FailuresHandler failuresHandler = new FailuresHandler();
    private boolean localCache = true;
    private int maxCacheSize = 7;
    private long returnAdCacheTTL = 3600;
    private boolean returnAdShouldLoadInBg = true;

    public final long a() {
        return TimeUnit.SECONDS.toMillis(this.adCacheTTL);
    }

    public final long b() {
        return TimeUnit.SECONDS.toMillis(this.returnAdCacheTTL);
    }

    public final Set<AdMode> c() {
        return this.autoLoad;
    }

    public final boolean d() {
        return this.localCache;
    }

    public final boolean e() {
        return this.returnAdShouldLoadInBg;
    }

    public final FailuresHandler f() {
        return this.failuresHandler;
    }

    public final int g() {
        return this.maxCacheSize;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ACMConfig aCMConfig = (ACMConfig) obj;
        return this.adCacheTTL == aCMConfig.adCacheTTL && this.returnAdCacheTTL == aCMConfig.returnAdCacheTTL && this.localCache == aCMConfig.localCache && this.returnAdShouldLoadInBg == aCMConfig.returnAdShouldLoadInBg && this.maxCacheSize == aCMConfig.maxCacheSize && s.b(this.autoLoad, aCMConfig.autoLoad) && s.b(this.failuresHandler, aCMConfig.failuresHandler);
    }

    public int hashCode() {
        return s.a(Long.valueOf(this.adCacheTTL), Long.valueOf(this.returnAdCacheTTL), this.autoLoad, Boolean.valueOf(this.localCache), Boolean.valueOf(this.returnAdShouldLoadInBg), this.failuresHandler, Integer.valueOf(this.maxCacheSize));
    }
}
