package com.startapp.sdk.adsbase.cache;

import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.k;

/* compiled from: StartAppSDK */
public final class d extends c {
    /* access modifiers changed from: protected */
    public final String e() {
        return "CacheTTLReloadTimer";
    }

    public d(e eVar) {
        super(eVar);
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return k.a().a(this.a.c());
    }

    /* access modifiers changed from: protected */
    public final long d() {
        f b = this.a.b();
        if (b == null) {
            return -1;
        }
        Long c = b.c();
        Long b2 = b.b();
        if (c == null || b2 == null) {
            return -1;
        }
        long longValue = c.longValue() - (System.currentTimeMillis() - b2.longValue());
        if (longValue >= 0) {
            return longValue;
        }
        return 0;
    }
}
