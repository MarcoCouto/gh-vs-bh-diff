package com.startapp.sdk.adsbase.cache;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.sdk.ads.interstitials.OverlayAd;
import com.startapp.sdk.ads.interstitials.ReturnAd;
import com.startapp.sdk.ads.offerWall.offerWallHtml.OfferWallAd;
import com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd;
import com.startapp.sdk.ads.splash.SplashAd;
import com.startapp.sdk.ads.video.VideoEnabledAd;
import com.startapp.sdk.adsbase.ActivityExtra;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.JsonAd;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.apppresence.AppPresenceDetails;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: StartAppSDK */
public class e {
    /* access modifiers changed from: private */
    public static final String g = "e";
    protected f a;
    protected AtomicBoolean b;
    protected long c;
    protected d d;
    protected b e;
    protected final Map<com.startapp.sdk.adsbase.adlisteners.b, List<StartAppAd>> f;
    /* access modifiers changed from: private */
    public final Placement h;
    private Context i;
    private ActivityExtra j;
    private AdPreferences k;
    private String l;
    private boolean m;
    private int n;
    private boolean o;
    private Long p;
    private b q;

    /* compiled from: StartAppSDK */
    class a implements AdEventListener {
        private boolean a = false;
        private boolean b = false;

        a() {
        }

        public final void onReceiveAd(Ad ad) {
            boolean z = e.this.a != null && e.this.a.e();
            if (!this.a && !z) {
                this.a = true;
                synchronized (e.this.f) {
                    for (com.startapp.sdk.adsbase.adlisteners.b bVar : e.this.f.keySet()) {
                        if (bVar != null) {
                            List<StartAppAd> a2 = e.this.a(e.this.f, bVar);
                            if (a2 != null) {
                                for (StartAppAd startAppAd : a2) {
                                    startAppAd.setErrorMessage(ad.getErrorMessage());
                                    bVar.a(startAppAd);
                                }
                            }
                        }
                    }
                    e.this.f.clear();
                }
            }
            e.this.d.f();
            e.this.e.a();
            e.this.b.set(false);
        }

        public final void onFailedToReceiveAd(Ad ad) {
            ConcurrentHashMap concurrentHashMap;
            Map map = null;
            if (!this.b) {
                synchronized (e.this.f) {
                    concurrentHashMap = new ConcurrentHashMap(e.this.f);
                    e.this.a = null;
                    e.this.f.clear();
                }
                map = concurrentHashMap;
            }
            if (map != null) {
                for (com.startapp.sdk.adsbase.adlisteners.b bVar : map.keySet()) {
                    if (bVar != null) {
                        List<StartAppAd> a2 = e.this.a(map, bVar);
                        if (a2 != null) {
                            for (StartAppAd startAppAd : a2) {
                                startAppAd.setErrorMessage(ad.getErrorMessage());
                                bVar.b(startAppAd);
                            }
                        }
                    }
                }
            }
            this.b = true;
            e.this.e.f();
            e.this.d.a();
            e.this.b.set(false);
        }
    }

    /* compiled from: StartAppSDK */
    public interface b {
        void a(e eVar);
    }

    public e(Context context, Placement placement, AdPreferences adPreferences) {
        this.a = null;
        this.b = new AtomicBoolean(false);
        this.l = null;
        this.m = false;
        this.d = null;
        this.e = null;
        this.f = new ConcurrentHashMap();
        this.o = true;
        this.h = placement;
        this.k = adPreferences;
        if (context instanceof Activity) {
            this.i = context.getApplicationContext();
            this.j = new ActivityExtra((Activity) context);
        } else {
            this.i = context;
            this.j = null;
        }
        this.d = new d(this);
        this.e = new b(this);
    }

    public e(Context context, Placement placement, AdPreferences adPreferences, byte b2) {
        this(context, placement, adPreferences);
        this.o = false;
    }

    public final AdPreferences a() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences) {
        this.k = adPreferences;
    }

    public final f b() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final Placement c() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.l = str;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.m = true;
    }

    /* access modifiers changed from: protected */
    public final List<StartAppAd> a(Map<com.startapp.sdk.adsbase.adlisteners.b, List<StartAppAd>> map, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        try {
            return (List) map.get(bVar);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.i);
            return null;
        }
    }

    private void a(Map<com.startapp.sdk.adsbase.adlisteners.b, List<StartAppAd>> map, com.startapp.sdk.adsbase.adlisteners.b bVar, List<StartAppAd> list) {
        try {
            map.put(bVar, list);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.i);
        }
    }

    public final void a(StartAppAd startAppAd, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        a(startAppAd, bVar, false, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0070, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0019 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059  */
    private void a(StartAppAd startAppAd, com.startapp.sdk.adsbase.adlisteners.b bVar, boolean z, boolean z2) {
        boolean z3;
        synchronized (this.f) {
            if (l() && !p()) {
                if (!z) {
                    z3 = false;
                    if (!z3) {
                        if (!(startAppAd == null || bVar == null)) {
                            List a2 = a(this.f, bVar);
                            if (a2 == null) {
                                a2 = new ArrayList();
                                a(this.f, bVar, a2);
                            }
                            a2.add(startAppAd);
                        }
                        if (this.b.compareAndSet(false, true)) {
                            this.d.g();
                            this.e.g();
                            a(z2);
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append(this.h);
                            sb.append(" ad is currently loading");
                            return;
                        }
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(this.h);
                        sb2.append(" ad already loaded");
                        if (!(startAppAd == null || bVar == null)) {
                            bVar.a(startAppAd);
                        }
                    }
                }
            }
            z3 = true;
            if (!z3) {
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        boolean z;
        new StringBuilder("Invalidating: ").append(this.h);
        if (l()) {
            Context context = this.i;
            Ad ad = (Ad) this.a;
            if (ad != null) {
                HashSet hashSet = new HashSet();
                if (ad instanceof HtmlAd) {
                    z = com.iab.omid.library.startapp.b.a(context, com.iab.omid.library.startapp.b.a(((HtmlAd) ad).j(), 0), 0, (Set<String>) hashSet, (List<AppPresenceDetails>) new ArrayList<AppPresenceDetails>()).booleanValue();
                } else if ((ad instanceof JsonAd) && com.iab.omid.library.startapp.b.a(context, ((JsonAd) ad).g(), 0, (Set<String>) hashSet, false).size() == 0) {
                    z = true;
                }
                if (!z || p()) {
                    a(null, null, true, false);
                } else if (!this.b.get()) {
                    this.d.f();
                    return;
                }
            }
            z = false;
            if (!z) {
            }
            a(null, null, true, false);
        } else if (!this.b.get()) {
            this.e.f();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        this.e.h();
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        this.d.h();
    }

    private boolean l() {
        return this.a != null && this.a.isReady();
    }

    public final f h() {
        if (!l()) {
            return null;
        }
        f fVar = this.a;
        this.n = 0;
        this.p = null;
        if (!AdsConstants.b.booleanValue() && this.o) {
            new StringBuilder("Ad shown, reloading ").append(this.h);
            a(null, null, true, true);
            return fVar;
        } else if (this.o) {
            return fVar;
        } else {
            if (this.q != null) {
                this.q.a(this);
            }
            if (this.d == null) {
                return fVar;
            }
            this.d.a();
            return fVar;
        }
    }

    private f m() {
        f fVar;
        switch (this.h) {
            case INAPP_FULL_SCREEN:
                fVar = new OverlayAd(this.i);
                break;
            case INAPP_OVERLAY:
                if (!s.a(4)) {
                    fVar = new OverlayAd(this.i);
                    break;
                } else {
                    fVar = new VideoEnabledAd(this.i);
                    break;
                }
            case INAPP_OFFER_WALL:
                boolean z = false;
                boolean z2 = new Random().nextInt(100) < AdsCommonMetaData.a().d();
                boolean isForceOfferWall3D = this.k.isForceOfferWall3D();
                boolean z3 = !this.k.isForceOfferWall2D();
                boolean a2 = s.a(64);
                if (s.a(64) && !s.a(128)) {
                    z = true;
                }
                if (!z && (!a2 || ((!z2 && !isForceOfferWall3D) || !z3))) {
                    fVar = new OfferWallAd(this.i);
                    break;
                } else {
                    fVar = new OfferWall3DAd(this.i);
                    break;
                }
            case INAPP_RETURN:
                fVar = new ReturnAd(this.i);
                break;
            case INAPP_SPLASH:
                fVar = new SplashAd(this.i);
                break;
            default:
                fVar = new OverlayAd(this.i);
                break;
        }
        StringBuilder sb = new StringBuilder("ad Type: [");
        sb.append(fVar.getClass().toString());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return fVar;
    }

    private void a(boolean z) {
        if (this.a != null) {
            this.a.a(false);
        }
        if (n()) {
            this.m = false;
            b(z);
            return;
        }
        c(z);
    }

    private boolean n() {
        return this.m && this.l != null;
    }

    private void b(final boolean z) {
        StringBuilder sb = new StringBuilder("Loading ");
        sb.append(this.h);
        sb.append(" from disk file name: ");
        sb.append(this.l);
        final a aVar = new a();
        DiskAdCacheManager.a(this.i, this.l, (com.startapp.sdk.adsbase.cache.DiskAdCacheManager.a) new com.startapp.sdk.adsbase.cache.DiskAdCacheManager.a() {
            public final void a(f fVar) {
                e.g;
                new StringBuilder("Success loading from disk: ").append(e.this.h);
                e.this.a = fVar;
            }
        }, (com.startapp.sdk.adsbase.adlisteners.b) new com.startapp.sdk.adsbase.adlisteners.b() {
            public final void a(Ad ad) {
                aVar.onReceiveAd(ad);
            }

            public final void b(Ad ad) {
                e.g;
                StringBuilder sb = new StringBuilder("Failed to load ");
                sb.append(e.this.h);
                sb.append(" from disk");
                e.this.a = null;
                e.this.c(z);
            }
        });
    }

    public final void a(b bVar) {
        this.q = bVar;
    }

    public final int j() {
        return this.n;
    }

    public final void a(int i2) {
        this.n = i2;
    }

    private boolean o() {
        Long h2 = AdsCommonMetaData.a().h();
        if (h2 == null || this.p == null || SystemClock.elapsedRealtime() - this.p.longValue() >= h2.longValue()) {
            this.p = Long.valueOf(SystemClock.elapsedRealtime());
            return false;
        }
        new a().onFailedToReceiveAd(new CachedAd$3(this, this.i, this.h));
        Context context = this.i;
        StringBuilder sb = new StringBuilder("Failed to load ");
        sb.append(this.h.name());
        sb.append(" ad: NO FILL");
        s.a(context, true, sb.toString());
        return true;
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        if (!z || !o()) {
            this.a = m();
            this.a.setActivityExtra(this.j);
            this.k.setAutoLoadAmount(this.n);
            this.a.load(this.k, new a());
            this.c = System.currentTimeMillis();
        }
    }

    private boolean p() {
        if (this.a == null) {
            return false;
        }
        return this.a.e_();
    }

    public final boolean i() {
        if (this.n < MetaData.D().K()) {
            this.n++;
            a(null, null, true, false);
            return true;
        }
        if (this.q != null) {
            this.q.a(this);
        }
        return false;
    }
}
