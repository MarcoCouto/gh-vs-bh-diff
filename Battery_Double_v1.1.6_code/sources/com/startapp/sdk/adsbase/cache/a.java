package com.startapp.sdk.adsbase.cache;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.common.b.d;
import com.startapp.sdk.ads.interstitials.ReturnAd;
import com.startapp.sdk.adsbase.Ad.AdType;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppAd.AdMode;
import com.startapp.sdk.adsbase.cache.DiskAdCacheManager.c;
import com.startapp.sdk.adsbase.cache.e.b;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.k;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: StartAppSDK */
public class a {
    /* access modifiers changed from: private */
    public static final String d = "a";
    private static a e = new a();
    final Map<CacheKey, e> a = new ConcurrentHashMap();
    protected boolean b = false;
    protected Context c;
    private Map<String, String> f = new WeakHashMap();
    private boolean g = false;
    private Queue<C0075a> h = new ConcurrentLinkedQueue();
    private b i;

    /* renamed from: com.startapp.sdk.adsbase.cache.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    class C0075a {
        StartAppAd a;
        Placement b;
        AdPreferences c;
        com.startapp.sdk.adsbase.adlisteners.b d;

        C0075a(StartAppAd startAppAd, Placement placement, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar) {
            this.a = startAppAd;
            this.b = placement;
            this.c = adPreferences;
            this.d = bVar;
        }
    }

    private a() {
    }

    public static a a() {
        return e;
    }

    public final CacheKey a(Context context, StartAppAd startAppAd, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        if (!c(Placement.INAPP_SPLASH)) {
            return null;
        }
        return a(context, startAppAd, Placement.INAPP_SPLASH, adPreferences, bVar);
    }

    public final CacheKey a(Context context, AdPreferences adPreferences) {
        if (!c(Placement.INAPP_RETURN)) {
            return null;
        }
        return a(context, (StartAppAd) null, Placement.INAPP_RETURN, adPreferences, (com.startapp.sdk.adsbase.adlisteners.b) null);
    }

    public final CacheKey a(Context context, StartAppAd startAppAd, AdMode adMode, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        Placement placement;
        if (adPreferences == null) {
            adPreferences = new AdPreferences();
        }
        AdPreferences adPreferences2 = adPreferences;
        switch (adMode) {
            case OFFERWALL:
                if (!s.a(128) && !s.a(64)) {
                    placement = Placement.INAPP_FULL_SCREEN;
                    break;
                } else {
                    placement = Placement.INAPP_OFFER_WALL;
                    break;
                }
            case OVERLAY:
            case FULLPAGE:
            case VIDEO:
            case REWARDED_VIDEO:
                placement = Placement.INAPP_OVERLAY;
                break;
            case AUTOMATIC:
                boolean z = s.a(128) || s.a(64);
                boolean a2 = s.a(4);
                boolean a3 = s.a(2);
                if (!a2 || !a3 || !z) {
                    if (!a2 && !a3) {
                        if (z) {
                            placement = Placement.INAPP_OFFER_WALL;
                            break;
                        }
                    } else {
                        placement = Placement.INAPP_OVERLAY;
                        break;
                    }
                } else {
                    if (new Random().nextInt(100) >= AdsCommonMetaData.a().b()) {
                        placement = Placement.INAPP_FULL_SCREEN;
                        break;
                    } else {
                        if ((new Random().nextInt(100) >= AdsCommonMetaData.a().c() && !adPreferences2.isForceFullpage()) || adPreferences2.isForceOverlay()) {
                            placement = Placement.INAPP_OVERLAY;
                            break;
                        }
                    }
                }
                break;
            default:
                placement = Placement.INAPP_FULL_SCREEN;
                break;
        }
        Placement placement2 = placement;
        if (adMode.equals(AdMode.REWARDED_VIDEO)) {
            adPreferences2.setType(AdType.REWARDED_VIDEO);
        } else if (adMode.equals(AdMode.VIDEO)) {
            adPreferences2.setType(AdType.VIDEO);
        }
        return a(context, startAppAd, placement2, adPreferences2, bVar, false, 0);
    }

    public final void a(final Context context) {
        this.c = context.getApplicationContext();
        if (f()) {
            this.g = true;
            ThreadManager.a(Priority.HIGH, (Runnable) new Runnable(context, new DiskAdCacheManager.b() {
                public final void a(List<DiskCacheKey> list) {
                    if (list != null) {
                        try {
                            for (DiskCacheKey diskCacheKey : list) {
                                if (a.c(diskCacheKey.placement)) {
                                    a.d;
                                    new StringBuilder("Loading from disk: ").append(diskCacheKey.placement);
                                    a.this.a(context, null, diskCacheKey.a(), diskCacheKey.b(), null, true, diskCacheKey.c());
                                }
                            }
                        } catch (Throwable th) {
                            new e(th).a(context);
                        }
                    }
                    a.this.d(context);
                }
            }) {
                final /* synthetic */ b a;
                private /* synthetic */ Context b;

                {
                    this.b = r1;
                    this.a = r2;
                }

                public final void run() {
                    try {
                        Class<DiskCacheKey> cls = DiskCacheKey.class;
                        final List b2 = d.b(this.b, DiskAdCacheManager.a());
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public final void run() {
                                AnonymousClass2.this.a.a(b2);
                            }
                        });
                    } catch (Throwable th) {
                        new e(th).a(this.b);
                    }
                }
            });
        }
    }

    public final void a(Placement placement) {
        if (!this.g) {
            synchronized (this.a) {
                for (e eVar : this.a.values()) {
                    if (eVar.c() == placement) {
                        eVar.e();
                    }
                }
            }
        }
    }

    public final void b() {
        if (!this.g) {
            synchronized (this.a) {
                for (e e2 : this.a.values()) {
                    e2.e();
                }
            }
        }
    }

    public final void b(Context context) {
        this.b = true;
        ThreadManager.a(Priority.DEFAULT, (Runnable) new Runnable(context, new c() {
            public final void a() {
                a.this.b = false;
            }
        }) {
            final /* synthetic */ c a;
            private /* synthetic */ Context b;

            {
                this.b = r1;
                this.a = r2;
            }

            public final void run() {
                try {
                    d.c(this.b, "startapp_ads");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            AnonymousClass1.this.a.a();
                        }
                    });
                } catch (Throwable th) {
                    new e(th).a(this.b);
                }
            }
        });
    }

    public final void c(final Context context) {
        AnonymousClass3 r0 = new com.startapp.sdk.adsbase.remoteconfig.b() {
            public final void a(RequestReason requestReason, boolean z) {
                a.d;
                if (z) {
                    Set c = CacheMetaData.a().b().c();
                    if (c != null) {
                        for (AdMode adMode : a.this.a(c)) {
                            new StringBuilder("preCacheAds load ").append(adMode.name());
                            int b2 = AdsCommonMetaData.a().b();
                            if (adMode == AdMode.FULLPAGE) {
                                if (b2 > 0) {
                                    a.this.a(context, (StartAppAd) null, AdMode.FULLPAGE, new AdPreferences(), (com.startapp.sdk.adsbase.adlisteners.b) null);
                                }
                            } else if (adMode != AdMode.OFFERWALL) {
                                a.this.a(context, (StartAppAd) null, adMode, new AdPreferences(), (com.startapp.sdk.adsbase.adlisteners.b) null);
                            } else if (b2 < 100) {
                                a.this.a(context, (StartAppAd) null, AdMode.OFFERWALL, new AdPreferences(), (com.startapp.sdk.adsbase.adlisteners.b) null);
                            }
                            String a2 = a.a(adMode);
                            if (a2 != null) {
                                j.b(context, a2, Integer.valueOf(j.a(context, a2, Integer.valueOf(0)).intValue() + 1));
                            }
                        }
                    }
                }
            }

            public final void a() {
                a.d;
            }
        };
        synchronized (MetaData.g()) {
            MetaData.D().a((com.startapp.sdk.adsbase.remoteconfig.b) r0);
        }
    }

    /* access modifiers changed from: protected */
    public final Set<AdMode> a(Set<AdMode> set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AdMode adMode = (AdMode) it.next();
            boolean z = false;
            if (j.a(this.c, a(adMode), Integer.valueOf(0)).intValue() >= MetaData.D().L()) {
                z = true;
            }
            if (z) {
                new StringBuilder("preCacheAds.remove ").append(adMode.name());
                it.remove();
            }
        }
        if (!s.a(128) && !s.a(64)) {
            set.remove(AdMode.OFFERWALL);
        }
        if (!s.a(2) && !s.a(4)) {
            set.remove(AdMode.FULLPAGE);
        }
        if (!s.a(4)) {
            set.remove(AdMode.REWARDED_VIDEO);
            set.remove(AdMode.VIDEO);
        }
        return set;
    }

    private int e() {
        return this.a.size();
    }

    public final f a(CacheKey cacheKey) {
        if (cacheKey == null) {
            return null;
        }
        new StringBuilder("Retrieving ad with ").append(cacheKey);
        e eVar = (e) this.a.get(cacheKey);
        if (eVar != null) {
            return eVar.h();
        }
        return null;
    }

    public final f b(CacheKey cacheKey) {
        e eVar = cacheKey != null ? (e) this.a.get(cacheKey) : null;
        if (eVar != null) {
            return eVar.b();
        }
        return null;
    }

    public final synchronized List<e> c() {
        return new ArrayList(this.a.values());
    }

    public final String a(String str, String str2) {
        this.f.put(str2, str);
        return str2;
    }

    public final String a(String str) {
        return (String) this.f.get(str);
    }

    public final String b(String str) {
        StringBuilder sb = new StringBuilder("cache size: ");
        sb.append(this.f.size());
        sb.append(" - removing ");
        sb.append(str);
        return (String) this.f.remove(str);
    }

    private boolean f() {
        return !this.b && CacheMetaData.a().b().d();
    }

    /* access modifiers changed from: protected */
    public final void d(Context context) {
        this.g = false;
        for (C0075a aVar : this.h) {
            if (c(aVar.b)) {
                new StringBuilder("Loading pending request for: ").append(aVar.b);
                a(context, aVar.a, aVar.b, aVar.c, aVar.d);
            }
        }
        this.h.clear();
    }

    protected static String c(CacheKey cacheKey) {
        return String.valueOf(cacheKey.hashCode()).replace('-', '_');
    }

    public final CacheKey a(Context context, StartAppAd startAppAd, Placement placement, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        return a(context, startAppAd, placement, adPreferences, bVar, false, 0);
    }

    /* access modifiers changed from: protected */
    public final CacheKey a(Context context, StartAppAd startAppAd, Placement placement, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar, boolean z, int i2) {
        e eVar;
        this.c = context.getApplicationContext();
        if (adPreferences == null) {
            adPreferences = new AdPreferences();
        }
        AdPreferences adPreferences2 = adPreferences;
        CacheKey cacheKey = new CacheKey(placement, adPreferences2);
        if (!this.g || z) {
            AdPreferences adPreferences3 = new AdPreferences(adPreferences2);
            synchronized (this.a) {
                eVar = (e) this.a.get(cacheKey);
                if (eVar == null) {
                    StringBuilder sb = new StringBuilder("CachedAd for ");
                    sb.append(placement);
                    sb.append(" not found. Adding new CachedAd with ");
                    sb.append(cacheKey);
                    if (AnonymousClass6.a[placement.ordinal()] != 1) {
                        eVar = new e(context, placement, adPreferences3);
                    } else {
                        eVar = new e(context, placement, adPreferences3, 0);
                    }
                    if (this.i == null) {
                        this.i = new b() {
                            public final void a(e eVar) {
                                synchronized (a.this.a) {
                                    CacheKey cacheKey = null;
                                    Iterator it = a.this.a.keySet().iterator();
                                    while (true) {
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        CacheKey cacheKey2 = (CacheKey) it.next();
                                        if (((e) a.this.a.get(cacheKey2)) == eVar) {
                                            cacheKey = cacheKey2;
                                            break;
                                        }
                                    }
                                    if (cacheKey != null) {
                                        a.this.a.remove(cacheKey);
                                        if (eVar.c() != Placement.INAPP_SPLASH) {
                                            new e(InfoEventCategory.ERROR).e("Stop reload in cache").f(cacheKey.toString()).a(a.this.c);
                                        }
                                    }
                                }
                            }
                        };
                    }
                    eVar.a(this.i);
                    if (z) {
                        eVar.a(c(cacheKey));
                        eVar.d();
                        eVar.a(i2);
                    }
                    a(cacheKey, eVar, context);
                } else {
                    StringBuilder sb2 = new StringBuilder("CachedAd for ");
                    sb2.append(placement);
                    sb2.append(" already exists.");
                    eVar.a(adPreferences3);
                }
            }
            eVar.a(startAppAd, bVar);
            return cacheKey;
        }
        new StringBuilder("Adding to pending queue: ").append(placement);
        Queue<C0075a> queue = this.h;
        C0075a aVar = new C0075a(startAppAd, placement, adPreferences2, bVar);
        queue.add(aVar);
        return cacheKey;
    }

    public final void b(Placement placement) {
        synchronized (this.a) {
            Iterator it = this.a.entrySet().iterator();
            while (it.hasNext()) {
                if (((CacheKey) ((Entry) it.next()).getKey()).a() == placement) {
                    it.remove();
                }
            }
        }
    }

    private void a(CacheKey cacheKey, e eVar, Context context) {
        synchronized (this.a) {
            int g2 = CacheMetaData.a().b().g();
            if (g2 != 0 && e() >= g2) {
                long j = Long.MAX_VALUE;
                Object obj = null;
                for (CacheKey cacheKey2 : this.a.keySet()) {
                    e eVar2 = (e) this.a.get(cacheKey2);
                    if (eVar2.c() == eVar.c() && eVar2.c < j) {
                        j = eVar2.c;
                        obj = cacheKey2;
                    }
                }
                if (obj != null) {
                    this.a.remove(obj);
                }
            }
            this.a.put(cacheKey, eVar);
            if (Math.random() * 100.0d < ((double) CacheMetaData.a().c())) {
                new e(InfoEventCategory.GENERAL).e("Cache Size").f(String.valueOf(e())).a(context);
            }
        }
    }

    protected static boolean c(Placement placement) {
        switch (placement) {
            case INAPP_SPLASH:
                return k.a().k() && !AdsCommonMetaData.a().A();
            case INAPP_RETURN:
                return k.a().j() && !AdsCommonMetaData.a().z();
            default:
                return true;
        }
    }

    public static String a(AdMode adMode) {
        if (adMode == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder("autoLoadNotShownAdPrefix");
        sb.append(adMode.name());
        return sb.toString();
    }

    public final void a(final Context context, boolean z) {
        if (f()) {
            ThreadManager.a(Priority.DEFAULT, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        d.c(context, DiskAdCacheManager.a());
                        d.c(context, DiskAdCacheManager.b());
                        CacheKey cacheKey = null;
                        for (Entry entry : a.this.a.entrySet()) {
                            CacheKey cacheKey2 = (CacheKey) entry.getKey();
                            if (cacheKey2.a() == Placement.INAPP_SPLASH) {
                                cacheKey = cacheKey2;
                            } else {
                                e eVar = (e) entry.getValue();
                                a.d;
                                new StringBuilder("Saving to disk: ").append(cacheKey2.toString());
                                DiskAdCacheManager.a(context, cacheKey2.a(), eVar.a(), a.c(cacheKey2), eVar.j());
                                DiskAdCacheManager.a(context, eVar, a.c(cacheKey2));
                            }
                        }
                        if (cacheKey != null) {
                            a.this.a.remove(cacheKey);
                        }
                    } catch (Throwable th) {
                        new e(th).a(context);
                    }
                }
            });
        }
        for (e eVar : this.a.values()) {
            if (eVar.b() == null || !s.a(2) || !(eVar.b() instanceof ReturnAd) || z || !CacheMetaData.a().b().e()) {
                eVar.g();
            }
            eVar.f();
        }
    }
}
