package com.startapp.sdk.adsbase.cache;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.common.b.d;
import com.startapp.sdk.ads.interstitials.InterstitialAd;
import com.startapp.sdk.ads.list3d.g;
import com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.apppresence.AppPresenceDetails;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class DiskAdCacheManager {
    public int a;
    public int b;
    public int c;
    public float d;
    public float e;

    /* compiled from: StartAppSDK */
    protected static class DiskCacheKey implements Serializable {
        private static final long serialVersionUID = 1;
        protected AdPreferences adPreferences;
        private int numberOfLoadedAd;
        protected Placement placement;

        protected DiskCacheKey(Placement placement2, AdPreferences adPreferences2) {
            this.placement = placement2;
            this.adPreferences = adPreferences2;
        }

        /* access modifiers changed from: protected */
        public final Placement a() {
            return this.placement;
        }

        /* access modifiers changed from: protected */
        public final AdPreferences b() {
            return this.adPreferences;
        }

        /* access modifiers changed from: protected */
        public final int c() {
            return this.numberOfLoadedAd;
        }

        /* access modifiers changed from: protected */
        public final void a(int i) {
            this.numberOfLoadedAd = i;
        }
    }

    /* compiled from: StartAppSDK */
    protected static class DiskCachedAd implements Serializable {
        private static final long serialVersionUID = 1;
        private f ad;
        private String html;

        protected DiskCachedAd(f fVar) {
            this.ad = fVar;
            if (this.ad != null && (this.ad instanceof HtmlAd)) {
                this.html = ((HtmlAd) this.ad).j();
            }
        }

        /* access modifiers changed from: protected */
        public final f a() {
            return this.ad;
        }

        /* access modifiers changed from: protected */
        public final String b() {
            return this.html;
        }
    }

    /* compiled from: StartAppSDK */
    public interface a {
        void a(f fVar);
    }

    /* compiled from: StartAppSDK */
    public interface b {
        void a(List<DiskCacheKey> list);
    }

    /* compiled from: StartAppSDK */
    public interface c {
        void a();
    }

    protected static void a(Context context, Placement placement, AdPreferences adPreferences, String str, int i) {
        DiskCacheKey diskCacheKey = new DiskCacheKey(placement, adPreferences);
        diskCacheKey.a(i);
        d.a(context, a(), str, diskCacheKey);
    }

    protected static void a(Context context, e eVar, String str) {
        d.a(context, b(), str, new DiskCachedAd(eVar.b()));
    }

    protected static void a(final Context context, final String str, final a aVar, final com.startapp.sdk.adsbase.adlisteners.b bVar) {
        ThreadManager.a(Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                try {
                    Class<DiskCachedAd> cls = DiskCachedAd.class;
                    final DiskCachedAd diskCachedAd = (DiskCachedAd) d.a(context, DiskAdCacheManager.b(), str);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            try {
                                if (diskCachedAd == null) {
                                    new StringBuilder("File not found or error: ").append(str);
                                    bVar.b(null);
                                    return;
                                }
                                if (diskCachedAd.a() != null) {
                                    if (diskCachedAd.a().isReady()) {
                                        if (diskCachedAd.a().e_()) {
                                            bVar.b(null);
                                            return;
                                        } else {
                                            DiskAdCacheManager.a(context, diskCachedAd, aVar, bVar);
                                            return;
                                        }
                                    }
                                }
                                bVar.b(null);
                            } catch (Throwable th) {
                                new e(th).a(context);
                                bVar.b(null);
                            }
                        }
                    });
                } catch (Throwable th) {
                    new e(th).a(context);
                    bVar.b(null);
                }
            }
        });
    }

    protected static String a() {
        return "startapp_ads".concat(File.separator).concat("keys");
    }

    protected static String b() {
        return "startapp_ads".concat(File.separator).concat("interstitials");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    protected static void a(Context context, DiskCachedAd diskCachedAd, a aVar, final com.startapp.sdk.adsbase.adlisteners.b bVar) {
        f a2 = diskCachedAd.a();
        a2.setContext(context);
        boolean z = false;
        if (s.a(2) && (a2 instanceof InterstitialAd)) {
            final InterstitialAd interstitialAd = (InterstitialAd) a2;
            String b2 = diskCachedAd.b();
            if (b2 != null && !b2.equals("")) {
                if (AdsCommonMetaData.a().F()) {
                    List a3 = com.iab.omid.library.startapp.b.a(b2, 0);
                    ArrayList arrayList = new ArrayList();
                    if (com.iab.omid.library.startapp.b.a(context, a3, 0, (Set<String>) new HashSet<String>(), (List<AppPresenceDetails>) arrayList).booleanValue()) {
                        new com.startapp.sdk.adsbase.apppresence.a(context, arrayList).a();
                        if (z) {
                            a.a().a(b2, interstitialAd.k());
                            aVar.a(interstitialAd);
                            s.a(context, b2, (com.startapp.sdk.adsbase.i.s.a) new com.startapp.sdk.adsbase.i.s.a() {
                                public final void a() {
                                    bVar.a(interstitialAd);
                                }

                                public final void a(String str) {
                                    bVar.b(interstitialAd);
                                }
                            });
                            return;
                        }
                    }
                }
                z = true;
                if (z) {
                }
            }
            bVar.b(null);
        } else if (!s.a(64) || !(a2 instanceof OfferWall3DAd)) {
            bVar.b(null);
        } else {
            OfferWall3DAd offerWall3DAd = (OfferWall3DAd) a2;
            List g = offerWall3DAd.g();
            if (g != null) {
                if (AdsCommonMetaData.a().F()) {
                    g = com.iab.omid.library.startapp.b.a(context, g, 0, (Set<String>) new HashSet<String>());
                }
                if (g != null && g.size() > 0) {
                    aVar.a(offerWall3DAd);
                    a(offerWall3DAd, bVar, g);
                    return;
                }
            }
            bVar.b(null);
        }
    }

    private static void a(OfferWall3DAd offerWall3DAd, com.startapp.sdk.adsbase.adlisteners.b bVar, List<AdDetails> list) {
        com.startapp.sdk.ads.list3d.f a2 = g.a().a(offerWall3DAd.a());
        a2.a();
        for (AdDetails a3 : list) {
            a2.a(a3);
        }
        bVar.a(offerWall3DAd);
    }
}
