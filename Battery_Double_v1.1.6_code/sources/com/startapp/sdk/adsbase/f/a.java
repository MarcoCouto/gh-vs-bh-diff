package com.startapp.sdk.adsbase.f;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import com.ironsource.sdk.constants.LocationConst;
import com.smaato.sdk.video.vast.model.Verification;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class a {
    private HashMap<Integer, SensorEvent> a = new HashMap<>();

    public final int a(SensorEvent sensorEvent) {
        int size;
        synchronized (this) {
            int type = sensorEvent.sensor.getType();
            SensorEvent sensorEvent2 = (SensorEvent) this.a.get(Integer.valueOf(type));
            if (sensorEvent2 == null || sensorEvent2.accuracy <= sensorEvent.accuracy) {
                this.a.put(Integer.valueOf(type), sensorEvent);
            }
            size = this.a.size();
        }
        return size;
    }

    public final JSONArray a() throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (SensorEvent sensorEvent : this.a.values()) {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            Sensor sensor = sensorEvent.sensor;
            jSONObject2.put("name", sensor.getName());
            jSONObject2.put(Verification.VENDOR, sensor.getVendor());
            jSONObject2.put("version", sensor.getVersion());
            jSONObject2.put("maximum range", (double) sensor.getMaximumRange());
            jSONObject2.put("power", (double) sensor.getPower());
            jSONObject2.put("resolution", (double) sensor.getResolution());
            jSONObject2.put(LocationConst.ACCURACY, sensorEvent.accuracy);
            jSONObject2.put("timestamp", sensorEvent.timestamp);
            JSONArray jSONArray2 = new JSONArray();
            for (float f : sensorEvent.values) {
                jSONArray2.put((double) f);
            }
            jSONObject2.put(String.USAGE_TRACKER_VALUES, jSONArray2);
            jSONObject.put(String.valueOf(sensor.getType()), jSONObject2);
            jSONArray.put(jSONObject);
        }
        if (jSONArray.length() > 0) {
            return jSONArray;
        }
        return null;
    }
}
