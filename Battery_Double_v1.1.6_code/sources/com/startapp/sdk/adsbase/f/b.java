package com.startapp.sdk.adsbase.f;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import com.startapp.common.d;
import com.startapp.sdk.adsbase.remoteconfig.BaseSensorConfig;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.SensorsConfig;
import java.util.HashMap;
import org.json.JSONArray;

/* compiled from: StartAppSDK */
public final class b {
    protected a a = new a();
    protected d b;
    private HashMap<Integer, a> c = null;
    private SensorManager d;
    /* access modifiers changed from: private */
    public int e;
    private SensorEventListener f = new SensorEventListener() {
        public final void onAccuracyChanged(Sensor sensor, int i) {
        }

        public final void onSensorChanged(SensorEvent sensorEvent) {
            if (b.this.a.a(sensorEvent) == b.this.e) {
                b.this.b();
                if (b.this.b != null) {
                    b.this.b.a(b.this.c());
                }
            }
        }
    };

    /* compiled from: StartAppSDK */
    class a {
        private int a;
        private int b;

        public a(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public final int a() {
            return this.a;
        }

        public final int b() {
            return this.b;
        }
    }

    public b(Context context, d dVar) {
        this.d = (SensorManager) context.getSystemService("sensor");
        this.b = dVar;
        this.e = 0;
        this.c = new HashMap<>();
        SensorsConfig z = MetaData.D().z();
        a(13, z.c());
        a(9, z.d());
        a(5, z.e());
        a(10, z.f());
        a(2, z.g());
        a(6, z.h());
        a(12, z.i());
        a(11, z.j());
        a(16, z.k());
    }

    public final void a() {
        for (Integer intValue : this.c.keySet()) {
            int intValue2 = intValue.intValue();
            a aVar = (a) this.c.get(Integer.valueOf(intValue2));
            if (VERSION.SDK_INT >= aVar.a()) {
                Sensor defaultSensor = this.d.getDefaultSensor(intValue2);
                if (defaultSensor != null) {
                    this.d.registerListener(this.f, defaultSensor, aVar.b());
                    this.e++;
                }
            }
        }
    }

    public final void b() {
        this.d.unregisterListener(this.f);
    }

    public final JSONArray c() {
        try {
            return this.a.a();
        } catch (Exception unused) {
            return null;
        }
    }

    private void a(int i, BaseSensorConfig baseSensorConfig) {
        if (baseSensorConfig.c()) {
            this.c.put(Integer.valueOf(i), new a(baseSensorConfig.b(), baseSensorConfig.a()));
        }
    }
}
