package com.startapp.sdk.adsbase;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings;
import android.support.customtabs.CustomTabsIntent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewParent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.constants.LocationConst;
import com.smaato.sdk.core.api.VideoType;
import com.startapp.common.Constants;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.common.b;
import com.startapp.sdk.adsbase.activities.OverlayActivity;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.b.c;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class a {
    private static ProgressDialog a;
    private final HashMap<View, String> b = new HashMap<>();
    private final HashMap<View, ArrayList<String>> c = new HashMap<>();
    private final HashSet<View> d = new HashSet<>();
    private final HashSet<String> e = new HashSet<>();
    private final HashSet<String> f = new HashSet<>();
    private boolean g;

    /* renamed from: com.startapp.sdk.adsbase.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    static class C0073a extends WebViewClient {
        protected String a = "";
        protected String b;
        protected boolean c = false;
        protected boolean d = true;
        protected Runnable e;
        protected boolean f = false;
        protected boolean g = false;
        private boolean h = false;
        private long i;
        private long j;
        private Boolean k = null;
        private String l;
        private ProgressDialog m;
        private LinkedHashMap<String, Float> n = new LinkedHashMap<>();
        private long o;
        private Timer p;

        public C0073a(long j2, long j3, boolean z, Boolean bool, ProgressDialog progressDialog, String str, String str2, String str3, Runnable runnable) {
            this.i = j2;
            this.j = j3;
            this.d = z;
            this.k = bool;
            this.m = progressDialog;
            this.a = str;
            this.l = str2;
            this.b = str3;
            this.e = runnable;
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            StringBuilder sb = new StringBuilder("MyWebViewClientSmartRedirect::onPageStarted - [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            super.onPageStarted(webView, str, bitmap);
            if (!this.h) {
                this.o = System.currentTimeMillis();
                this.n.put(str, Float.valueOf(-1.0f));
                final Context context = webView.getContext();
                ThreadManager.a((Runnable) new Runnable() {
                    public final void run() {
                        if (!C0073a.this.c) {
                            try {
                                e eVar = new e(InfoEventCategory.ERROR);
                                StringBuilder sb = new StringBuilder("Failed smart redirect hop info: ");
                                sb.append(C0073a.this.g ? "Page Finished" : "Timeout");
                                eVar.e(sb.toString()).a(C0073a.this.a()).g(C0073a.this.b).a(context);
                            } catch (Throwable th) {
                                new e(th).a(context);
                            }
                            try {
                                C0073a.this.f = true;
                                a.a(context);
                                C0073a.this.b();
                                if (!C0073a.this.d || !MetaData.D().x()) {
                                    a.d(context, C0073a.this.a);
                                } else {
                                    a.a(context, C0073a.this.a, C0073a.this.b);
                                }
                                if (C0073a.this.e != null) {
                                    C0073a.this.e.run();
                                }
                            } catch (Throwable th2) {
                                new e(th2).a(context);
                            }
                        }
                    }
                }, this.i);
                this.h = true;
            }
            this.g = false;
            b();
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (webView == null) {
                return true;
            }
            try {
                long currentTimeMillis = System.currentTimeMillis();
                Float valueOf = Float.valueOf(((float) (currentTimeMillis - this.o)) / 1000.0f);
                this.o = currentTimeMillis;
                this.n.put(this.a, valueOf);
                this.n.put(str, Float.valueOf(-1.0f));
                this.a = str;
                String lowerCase = str.toLowerCase();
                boolean z = false;
                if (!a.b(lowerCase) && !lowerCase.startsWith("intent://")) {
                    return false;
                }
                if (!this.f) {
                    this.c = true;
                    a.a(webView.getContext());
                    b();
                    Context context = webView.getContext();
                    if (lowerCase.startsWith("intent://")) {
                        str = webView.getUrl();
                    }
                    a.c(context, str);
                    if (this.l == null || this.l.equals("") || this.a.toLowerCase().contains(this.l.toLowerCase())) {
                        if (MetaData.D().analytics.e() && j.a(webView.getContext(), "firstSucceededSmartRedirect", Boolean.TRUE).booleanValue()) {
                            z = true;
                        }
                        float f2 = this.k == null ? MetaData.D().analytics.d() : this.k.booleanValue() ? 100.0f : 0.0f;
                        if (z || Math.random() * 100.0d < ((double) f2)) {
                            new e(InfoEventCategory.SUCCESS_SMART_REDIRECT_HOP_INFO).a(a()).g(this.b).a(webView.getContext());
                            j.b(webView.getContext(), "firstSucceededSmartRedirect", Boolean.FALSE);
                        }
                    } else {
                        e eVar = new e(InfoEventCategory.ERROR);
                        StringBuilder sb = new StringBuilder("Wrong package reached, expected: ");
                        sb.append(this.l);
                        e e2 = eVar.e(sb.toString());
                        StringBuilder sb2 = new StringBuilder("Link: ");
                        sb2.append(this.a);
                        e2.f(sb2.toString()).g(this.b).a(webView.getContext());
                    }
                    if (this.e != null) {
                        this.e.run();
                    }
                }
                return true;
            } catch (Throwable th) {
                new e(th).a(webView.getContext());
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            if (!this.c && !this.f && this.a.equals(str) && str != null && !a.b(str) && (str.startsWith("http://") || str.startsWith("https://"))) {
                this.g = true;
                try {
                    a(str);
                } catch (Exception unused) {
                }
                final Context context = webView.getContext();
                b();
                try {
                    this.p = new Timer();
                    this.p.schedule(new TimerTask() {
                        public final void run() {
                            if (!C0073a.this.f && !C0073a.this.c) {
                                try {
                                    C0073a.this.c = true;
                                    a.a(context);
                                    if (!C0073a.this.d || !MetaData.D().x()) {
                                        a.d(context, C0073a.this.a);
                                    } else {
                                        a.a(context, C0073a.this.a, C0073a.this.b);
                                    }
                                    if (C0073a.this.e != null) {
                                        C0073a.this.e.run();
                                    }
                                } catch (Throwable th) {
                                    new e(th).a(context);
                                }
                            }
                        }
                    }, this.j);
                } catch (Exception unused2) {
                    this.p = null;
                }
            }
            super.onPageFinished(webView, str);
        }

        public final void onReceivedError(WebView webView, int i2, String str, String str2) {
            b();
            if (str2 != null && !a.b(str2) && a.c(str2)) {
                new e(InfoEventCategory.ERROR).e("Failed smart redirect: ".concat(String.valueOf(i2))).f(str2).g(this.b).a(webView.getContext());
            }
            super.onReceivedError(webView, i2, str, str2);
        }

        /* access modifiers changed from: private */
        public void b() {
            if (this.p != null) {
                this.p.cancel();
                this.p = null;
            }
        }

        private void a(String str) {
            if (((Float) this.n.get(str)).floatValue() < 0.0f) {
                this.n.put(str, Float.valueOf(((float) (System.currentTimeMillis() - this.o)) / 1000.0f));
            }
        }

        /* access modifiers changed from: protected */
        public final JSONArray a() {
            JSONArray jSONArray = new JSONArray();
            for (String str : this.n.keySet()) {
                JSONObject jSONObject = new JSONObject();
                try {
                    a(str);
                    jSONObject.put(LocationConst.TIME, ((Float) this.n.get(str)).toString());
                    jSONObject.put("url", str);
                    jSONArray.put(jSONObject);
                } catch (JSONException unused) {
                    StringBuilder sb = new StringBuilder("error puting url into json [");
                    sb.append(str);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                }
            }
            return jSONArray;
        }
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.String, code=java.lang.Object, for r4v0, types: [java.lang.Object, java.lang.String] */
    public static String a(Context context, Object obj) {
        ApplicationInfo applicationInfo;
        try {
            return context.getResources().getString(context.getApplicationInfo().labelRes);
        } catch (NotFoundException unused) {
            PackageManager packageManager = context.getPackageManager();
            try {
                applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
            } catch (NameNotFoundException unused2) {
                applicationInfo = null;
            }
            if (applicationInfo != null) {
                obj = packageManager.getApplicationLabel(applicationInfo);
            }
            return (String) obj;
        }
    }

    public static boolean a(Activity activity) {
        boolean z = activity.getTheme().obtainStyledAttributes(new int[]{16843277}).getBoolean(0, false);
        if ((activity.getWindow().getAttributes().flags & 1024) != 0) {
            return true;
        }
        return z;
    }

    public static int a(String str) {
        String[] split = str.split(RequestParameters.AMPERSAND);
        return Integer.parseInt(split[split.length - 1].split(RequestParameters.EQUAL)[1]);
    }

    public static void a(Context context, String[] strArr, String str, String str2) {
        a(context, strArr, str, 0, str2);
    }

    public static void a(Context context, String[] strArr, String str, int i, String str2) {
        s.a(context, true, "Dropped impression because ".concat(String.valueOf(str2)));
        TrackingParams c2 = new TrackingParams(str).a(i).c(str2);
        if (strArr == null || strArr.length == 0) {
            new e(InfoEventCategory.ERROR).e("Non-impression without trackingUrls: ".concat(String.valueOf(str2))).f(c2.g()).a(context);
            return;
        }
        for (String str3 : strArr) {
            if (str3 != null && !str3.equalsIgnoreCase("")) {
                a(context, str3, c2, false);
            }
        }
    }

    public static void a(Context context, String str, TrackingParams trackingParams) {
        if (str != null && !str.equalsIgnoreCase("")) {
            s.a(context, false, "Sending impression");
            if (trackingParams != null) {
                trackingParams.a(context);
            }
            a(context, str, trackingParams, true);
        }
    }

    public static void a(Context context, String[] strArr, TrackingParams trackingParams) {
        if (strArr != null) {
            for (String a2 : strArr) {
                a(context, a2, trackingParams);
            }
        }
    }

    public static List<String> a(List<String> list, String str, String str2) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i < list.size()) {
            int i2 = i + 5;
            List subList = list.subList(i, Math.min(i2, list.size()));
            StringBuilder sb = new StringBuilder();
            sb.append(AdsConstants.a);
            sb.append("?");
            sb.append(TextUtils.join(RequestParameters.AMPERSAND, subList));
            sb.append("&isShown=");
            sb.append(str);
            sb.append("&appPresence=".concat(String.valueOf(str2)));
            arrayList.add(sb.toString());
            i = i2;
        }
        new StringBuilder("newUrlList size = ").append(arrayList.size());
        return arrayList;
    }

    public static final void a(Context context, String str, String str2, TrackingParams trackingParams, boolean z, boolean z2) {
        if (!TextUtils.isEmpty(str2)) {
            b(context, str2, trackingParams);
        }
        k.a().e();
        String str3 = null;
        if (!z2) {
            try {
                str3 = a(str, str2);
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(d(str2) ? com.startapp.common.b.a.a(str3) : "");
            String sb2 = sb.toString();
            if (MetaData.D().x() && z) {
                a(context, sb2, str3);
            } else if (!TextUtils.isEmpty(str2) || !d(context)) {
                d(context, sb2);
            } else {
                j.b(context, "shared_prefs_CookieFeatureTS", Long.valueOf(System.currentTimeMillis()));
                new StringBuilder("forceExternal - write to sp - TS : ").append(SimpleDateFormat.getDateInstance().format(new Date()));
                StringBuilder sb3 = new StringBuilder();
                sb3.append(sb2);
                sb3.append("&cki=1");
                d(context, sb3.toString());
            }
        } catch (Throwable th2) {
            new e(th2).a(context);
        }
    }

    private static boolean d(String str) {
        return AdsCommonMetaData.a().E() || TextUtils.isEmpty(str);
    }

    public static final void a(Context context, String str, String str2, String str3, TrackingParams trackingParams, long j, long j2, boolean z, Boolean bool) {
        a(context, str, str2, str3, trackingParams, j, j2, z, bool, false, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004a  */
    public static final void a(Context context, String str, String str2, String str3, TrackingParams trackingParams, long j, long j2, boolean z, Boolean bool, boolean z2, Runnable runnable) {
        String str4;
        Context context2 = context;
        String str5 = str2;
        if (AdsCommonMetaData.a().D()) {
            k.a().e();
            if (!z2) {
                try {
                    str4 = a(str, str2);
                } catch (Throwable th) {
                    new e(th).a(context);
                }
                if (str5 != null && !str2.equals("")) {
                    b(context, str2, trackingParams);
                }
                StringBuilder sb = new StringBuilder();
                String str6 = str;
                sb.append(str);
                sb.append(!d(str2) ? com.startapp.common.b.a.a(str4) : "");
                a(context, sb.toString(), str3, str4, j, j2, z, bool, runnable);
                return;
            }
            str4 = null;
            b(context, str2, trackingParams);
            StringBuilder sb2 = new StringBuilder();
            String str62 = str;
            sb2.append(str);
            sb2.append(!d(str2) ? com.startapp.common.b.a.a(str4) : "");
            a(context, sb2.toString(), str3, str4, j, j2, z, bool, runnable);
            return;
        }
        TrackingParams trackingParams2 = trackingParams;
        a(context, str, str2, trackingParams, z, z2);
    }

    public static void b(Context context, String str, TrackingParams trackingParams) {
        a(context, str, trackingParams, true);
        s.a(context, false, TextUtils.isEmpty(str) ? "Closed Ad" : "Clicked Ad");
    }

    private static void a(final Context context, final String str, final TrackingParams trackingParams, final boolean z) {
        if (!str.equals("")) {
            ThreadManager.a(Priority.HIGH, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        if (z) {
                            Context context = context;
                            StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append(com.startapp.common.b.a.a(a.a(str, (String) null)));
                            sb.append(trackingParams.a());
                            com.startapp.sdk.adsbase.h.a.a(context, sb.toString());
                            return;
                        }
                        Context context2 = context;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append(trackingParams.a());
                        com.startapp.sdk.adsbase.h.a.a(context2, sb2.toString());
                    } catch (Throwable th) {
                        new e(th).a(context);
                    }
                }
            });
        }
    }

    public static void b(final Context context, final String str) {
        ThreadManager.a(Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                try {
                    com.startapp.sdk.adsbase.h.a.a(context, str);
                } catch (Throwable th) {
                    new e(th).a(context);
                }
            }
        });
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0138  */
    private static final void a(Context context, String str, String str2, String str3, long j, long j2, boolean z, Boolean bool, Runnable runnable) {
        Context context2 = context;
        String str4 = str2;
        b.a(context).a(new Intent("com.startapp.android.OnClickCallback"));
        if (b(str)) {
            if (str4 != null && !str4.equals("") && !str.toLowerCase().contains(str2.toLowerCase())) {
                new e(InfoEventCategory.ERROR).e("Wrong package reached, expected: ".concat(String.valueOf(str2))).f("Link: ".concat(String.valueOf(str))).g(str3).a(context);
            }
            d(context, str);
            if (runnable != null) {
                runnable.run();
            }
            return;
        }
        String str5 = str3;
        if (context2 instanceof Activity) {
            s.a((Activity) context2, true);
        }
        try {
            final WebView webView = new WebView(context);
            if (a == null) {
                if (VERSION.SDK_INT >= 22) {
                    a = new ProgressDialog(context, 16974545);
                } else {
                    a = new ProgressDialog(context);
                }
                a.setTitle(null);
                a.setMessage("Loading....");
                a.setIndeterminate(false);
                a.setCancelable(false);
                a.setOnCancelListener(new OnCancelListener() {
                    public final void onCancel(DialogInterface dialogInterface) {
                        webView.stopLoading();
                    }
                });
                if (!(context2 instanceof Activity) || ((Activity) context2).isFinishing()) {
                    if (!(context2 instanceof Activity) && b(context) && a.getWindow() != null) {
                        if (VERSION.SDK_INT >= 26) {
                            a.getWindow().setType(2038);
                        } else {
                            a.getWindow().setType(2003);
                        }
                    }
                }
                a.show();
            }
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebChromeClient(new WebChromeClient());
            C0073a aVar = r2;
            C0073a aVar2 = new C0073a(j, j2, z, bool, a, str, str2, str3, runnable);
            webView.setWebViewClient(aVar);
            try {
                webView.loadUrl(str);
            } catch (Throwable th) {
                th = th;
                new e(th).a(context);
                d(context, str);
                if (runnable != null) {
                    runnable.run();
                }
            }
        } catch (Throwable th2) {
            th = th2;
            String str6 = str;
            new e(th).a(context);
            d(context, str);
            if (runnable != null) {
            }
        }
    }

    private static boolean b(Context context) {
        if (VERSION.SDK_INT >= 23) {
            return Settings.canDrawOverlays(context);
        }
        return com.startapp.common.b.b.a(context, "android.permission.SYSTEM_ALERT_WINDOW");
    }

    public static boolean b(String str) {
        return str.startsWith("market") || str.startsWith("http://play.google.com") || str.startsWith("https://play.google.com");
    }

    public static boolean c(String str) {
        return str != null && (str.startsWith("http://") || str.startsWith("https://"));
    }

    public static final void a(Context context) {
        if (context != null && (context instanceof Activity)) {
            s.a((Activity) context, false);
        }
        if (a != null) {
            synchronized (a) {
                if (a != null && a.isShowing()) {
                    try {
                        a.cancel();
                    } catch (Throwable th) {
                        new e(th).a(context);
                    }
                    a = null;
                }
            }
        }
    }

    public static void c(Context context, String str) {
        d(context, str);
    }

    public static void d(Context context, String str) {
        a(context, str, c(str));
    }

    private static void a(Context context, String str, boolean z) {
        if (context != null) {
            int i = 76021760;
            if (AdsCommonMetaData.a().H() || !(context instanceof Activity)) {
                i = 344457216;
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(i);
            boolean a2 = a(context, intent);
            if (!a2) {
                try {
                    if (VERSION.SDK_INT >= 18 && MetaData.D().N() && c(context)) {
                        b(context, str, z);
                        return;
                    }
                } catch (Throwable th) {
                    new e(th).a(context);
                    a(context, str, i);
                    return;
                }
            }
            if (z && !a2) {
                a(context, intent, i);
            }
            context.startActivity(intent);
        }
    }

    private static void a(Context context, Intent intent, int i) {
        String[] strArr = {"com.android.chrome", "com.android.browser", "com.opera.mini.native", "org.mozilla.firefox", "com.opera.browser"};
        try {
            List queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, i);
            if (queryIntentActivities != null && queryIntentActivities.size() > 1) {
                for (int i2 = 0; i2 < 5; i2++) {
                    String str = strArr[i2];
                    if (com.startapp.common.b.b.a(context, str, 0)) {
                        intent.setPackage(str);
                        return;
                    }
                }
            }
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    private static void a(Context context, String str, int i) {
        try {
            Intent parseUri = Intent.parseUri(str, i);
            a(context, parseUri);
            if (!(context instanceof Activity)) {
                parseUri.addFlags(268435456);
            }
            context.startActivity(parseUri);
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public static void a(Context context, String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            new e(InfoEventCategory.ERROR).e("Can not open in app browser, clickUrl is empty").g(str2).a(context);
        } else if (b(str) || !s.a(256)) {
            d(context, str);
        } else {
            try {
                if (VERSION.SDK_INT >= 18 && MetaData.D().M() && c(context)) {
                    b(context, str, true);
                    return;
                }
            } catch (Throwable th) {
                new e(th).a(context);
            }
            Intent intent = new Intent(context, OverlayActivity.class);
            if (VERSION.SDK_INT >= 21) {
                intent.addFlags(524288);
            }
            if (VERSION.SDK_INT >= 11) {
                intent.addFlags(32768);
            }
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            intent.setData(Uri.parse(str));
            intent.putExtra(IronSourceConstants.EVENTS_PLACEMENT_NAME, Placement.INAPP_BROWSER.a());
            intent.putExtra("activityShouldLockOrientation", false);
            try {
                context.startActivity(intent);
            } catch (Throwable th2) {
                new e(th2).a(context);
            }
        }
    }

    private static void b(Context context, String str, boolean z) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        Bundle bundle = new Bundle();
        bundle.putBinder(CustomTabsIntent.EXTRA_SESSION, null);
        intent.putExtras(bundle);
        if (z) {
            try {
                List queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
                if (queryIntentActivities != null && queryIntentActivities.size() > 1) {
                    intent.setPackage(((ResolveInfo) queryIntentActivities.get(0)).activityInfo.packageName);
                }
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
        if (!(context instanceof Activity)) {
            intent.addFlags(268435456);
        }
        context.startActivity(intent);
    }

    private static boolean c(Context context) {
        return j.a(context, "chromeTabs", Boolean.FALSE).booleanValue();
    }

    /* JADX INFO: used method not loaded: com.startapp.sdk.adsbase.infoevents.e.<init>(java.lang.Throwable):null, types can be incorrect */
    /* JADX INFO: used method not loaded: com.startapp.sdk.adsbase.infoevents.e.a(android.content.Context):null, types can be incorrect */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:0|(3:2|3|(2:6|4))|7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        new com.startapp.sdk.adsbase.infoevents.e(r1).a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0031 */
    public static void a(String str, String str2, String str3, Context context, TrackingParams trackingParams) {
        a(context, str3, trackingParams, true);
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
        if (str2 != null) {
            JSONObject jSONObject = new JSONObject(str2);
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String valueOf = String.valueOf(keys.next());
                launchIntentForPackage.putExtra(valueOf, String.valueOf(jSONObject.get(valueOf)));
            }
        }
        context.startActivity(launchIntentForPackage);
    }

    public static String a(String str, String str2) {
        String str3 = "";
        if (str2 != null) {
            try {
                if (!str2.equals("")) {
                    str = str2;
                }
            } catch (Exception unused) {
                return str3;
            }
        }
        String[] split = str.split("[?&]d=");
        return split.length >= 2 ? split[1].split("[?&]")[0] : str3;
    }

    private static boolean a(Context context, Intent intent) {
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 0)) {
            if (resolveInfo.activityInfo.packageName.equalsIgnoreCase(Constants.a)) {
                intent.setComponent(new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name));
                return true;
            }
        }
        return false;
    }

    public static String a() {
        StringBuilder sb = new StringBuilder("&position=");
        sb.append(b());
        return sb.toString();
    }

    public static String b() {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        int i = 0;
        while (i < 8) {
            if (stackTrace[i].getMethodName().compareTo("doHome") == 0) {
                return "home";
            }
            if (stackTrace[i].getMethodName().compareTo("onBackPressed") != 0) {
                i++;
            } else if (!k.a().i() && !k.p()) {
                return VideoType.INTERSTITIAL;
            } else {
                k.a().m();
                return "back";
            }
        }
        return VideoType.INTERSTITIAL;
    }

    public static String[] a(f fVar) {
        if (fVar instanceof HtmlAd) {
            return ((HtmlAd) fVar).trackingUrls;
        }
        if (fVar instanceof JsonAd) {
            return a(((JsonAd) fVar).g());
        }
        return new String[0];
    }

    public static String[] a(List<AdDetails> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (AdDetails d2 : list) {
                arrayList.add(d2.d());
            }
        }
        return (String[]) arrayList.toArray(new String[0]);
    }

    public static boolean a(Context context, Placement placement) {
        new StringBuilder("forceExternal - check -placement is : ").append(placement);
        if (placement.equals(Placement.INAPP_SPLASH) || !AdsCommonMetaData.a().N()) {
            return false;
        }
        return d(context);
    }

    private static boolean d(Context context) {
        return !c.a(context).d().b().c() && a(j.a(context, "shared_prefs_CookieFeatureTS", Long.valueOf(0)).longValue(), System.currentTimeMillis());
    }

    private static boolean a(long j, long j2) {
        return j == 0 || j + (((long) AdsCommonMetaData.a().M()) * 86400000) <= j2;
    }

    public final HashSet<String> c() {
        return this.e;
    }

    public final HashSet<String> d() {
        return this.f;
    }

    public final void e() {
        com.iab.omid.library.startapp.b.a a2 = com.iab.omid.library.startapp.b.a.a();
        if (a2 != null) {
            for (com.iab.omid.library.startapp.adsession.b bVar : a2.c()) {
                View g2 = bVar.g();
                if (bVar.h()) {
                    if (g2 != null) {
                        boolean z = false;
                        if (g2.hasWindowFocus()) {
                            HashSet hashSet = new HashSet();
                            View view = g2;
                            while (true) {
                                if (view != null) {
                                    if (!com.iab.omid.library.startapp.d.c.c(view)) {
                                        break;
                                    }
                                    hashSet.add(view);
                                    ViewParent parent = view.getParent();
                                    view = parent instanceof View ? (View) parent : null;
                                } else {
                                    this.d.addAll(hashSet);
                                    z = true;
                                    break;
                                }
                            }
                        }
                        if (z) {
                            this.e.add(bVar.f());
                            this.b.put(g2, bVar.f());
                            a(bVar);
                        }
                    }
                    this.f.add(bVar.f());
                }
            }
        }
    }

    private void a(com.iab.omid.library.startapp.adsession.b bVar) {
        for (com.iab.omid.library.startapp.e.a aVar : bVar.c()) {
            View view = (View) aVar.get();
            if (view != null) {
                ArrayList arrayList = (ArrayList) this.c.get(view);
                if (arrayList == null) {
                    arrayList = new ArrayList();
                    this.c.put(view, arrayList);
                }
                arrayList.add(bVar.f());
            }
        }
    }

    public final void f() {
        this.b.clear();
        this.c.clear();
        this.d.clear();
        this.e.clear();
        this.f.clear();
        this.g = false;
    }

    public final void g() {
        this.g = true;
    }

    public final String a(View view) {
        if (this.b.size() == 0) {
            return null;
        }
        String str = (String) this.b.get(view);
        if (str != null) {
            this.b.remove(view);
        }
        return str;
    }

    public final ArrayList<String> b(View view) {
        if (this.c.size() == 0) {
            return null;
        }
        ArrayList<String> arrayList = (ArrayList) this.c.get(view);
        if (arrayList != null) {
            this.c.remove(view);
            Collections.sort(arrayList);
        }
        return arrayList;
    }

    public final com.iab.omid.library.startapp.walking.c c(View view) {
        if (this.d.contains(view)) {
            return com.iab.omid.library.startapp.walking.c.PARENT_VIEW;
        }
        return this.g ? com.iab.omid.library.startapp.walking.c.OBSTRUCTION_VIEW : com.iab.omid.library.startapp.walking.c.UNDERLYING_VIEW;
    }
}
