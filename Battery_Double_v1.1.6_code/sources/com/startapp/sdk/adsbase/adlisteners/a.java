package com.startapp.sdk.adsbase.adlisteners;

import android.os.Handler;
import android.os.Looper;
import com.startapp.sdk.adsbase.Ad;

/* compiled from: StartAppSDK */
public final class a implements AdDisplayListener {
    AdDisplayListener a;

    public a(AdDisplayListener adDisplayListener) {
        this.a = adDisplayListener;
    }

    public final void adHidden(final Ad ad) {
        if (this.a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.a.adHidden(ad);
                }
            });
        }
    }

    public final void adDisplayed(final Ad ad) {
        if (this.a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.a.adDisplayed(ad);
                }
            });
        }
    }

    public final void adClicked(final Ad ad) {
        if (this.a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.a.adClicked(ad);
                }
            });
        }
    }

    public final void adNotDisplayed(final Ad ad) {
        if (this.a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.a.adNotDisplayed(ad);
                }
            });
        }
    }
}
