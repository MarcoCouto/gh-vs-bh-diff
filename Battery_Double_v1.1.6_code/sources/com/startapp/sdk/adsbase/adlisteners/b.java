package com.startapp.sdk.adsbase.adlisteners;

import android.content.Context;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.infoevents.e;

/* compiled from: StartAppSDK */
public abstract class b {

    /* compiled from: StartAppSDK */
    static class a extends b {
        public static final a a = new a();

        public final void a(Ad ad) {
        }

        public final void b(Ad ad) {
        }

        a() {
        }
    }

    /* renamed from: com.startapp.sdk.adsbase.adlisteners.b$b reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    static class C0074b extends b implements Callback {
        private Context a;
        private Handler b = new Handler(Looper.getMainLooper(), this);
        private AdEventListener c;

        public C0074b(Context context, AdEventListener adEventListener) {
            this.a = context;
            this.c = adEventListener;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return this.c == ((C0074b) obj).c;
        }

        public final int hashCode() {
            try {
                return System.identityHashCode(this.c);
            } catch (Throwable th) {
                new e(th).a(this.a);
                return 0;
            }
        }

        public final void a(Ad ad) {
            Message.obtain(this.b, 1, ad).sendToTarget();
        }

        public final void b(Ad ad) {
            Message.obtain(this.b, 2, ad).sendToTarget();
        }

        public final boolean handleMessage(Message message) {
            try {
                switch (message.what) {
                    case 1:
                        this.c.onReceiveAd((Ad) message.obj);
                        break;
                    case 2:
                        this.c.onFailedToReceiveAd((Ad) message.obj);
                        break;
                }
            } catch (Throwable th) {
                new e(th).a(this.a);
            }
            return false;
        }
    }

    public abstract void a(Ad ad);

    public abstract void b(Ad ad);

    public static b a(Context context, AdEventListener adEventListener) {
        if (adEventListener instanceof b) {
            return (b) adEventListener;
        }
        if (adEventListener != null) {
            return new C0074b(context, adEventListener);
        }
        return a.a;
    }
}
