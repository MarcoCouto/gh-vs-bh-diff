package com.startapp.sdk.adsbase.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.startapp.sdk.ads.a.b;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.b.c;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class OverlayActivity extends Activity {
    private b a;
    private boolean b;
    private int c;
    private boolean d;
    private Bundle e;
    private boolean f = false;
    private int g = -1;

    static {
        OverlayActivity.class.getSimpleName();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = false;
        overridePendingTransition(0, 0);
        super.onCreate(bundle);
        int intExtra = getIntent().getIntExtra(IronSourceConstants.EVENTS_PLACEMENT_NAME, -1);
        Serializable serializableExtra = getIntent().getSerializableExtra("ad");
        if (intExtra >= 0 && (serializableExtra instanceof Ad)) {
            c.a(getApplicationContext()).h().a(Placement.a(intExtra), ((Ad) serializableExtra).getAdId());
        }
        boolean booleanExtra = getIntent().getBooleanExtra("videoAd", false);
        requestWindowFeature(1);
        if (getIntent().getBooleanExtra(Events.CREATIVE_FULLSCREEN, false) || booleanExtra) {
            getWindow().setFlags(1024, 1024);
        }
        this.d = getIntent().getBooleanExtra("activityShouldLockOrientation", true);
        if (bundle == null && !booleanExtra) {
            com.startapp.common.b.a((Context) this).a(new Intent("com.startapp.android.ShowDisplayBroadcastListener"));
        }
        if (bundle != null) {
            this.g = bundle.getInt("activityLockedOrientation", -1);
            this.d = bundle.getBoolean("activityShouldLockOrientation", true);
        }
        this.c = getIntent().getIntExtra("orientation", getResources().getConfiguration().orientation);
        if (getResources().getConfiguration().orientation != this.c) {
            z = true;
        }
        this.b = z;
        if (!this.b) {
            a();
            this.a.a(bundle);
            return;
        }
        this.e = bundle;
    }

    private void a() {
        this.a = b.a(this, getIntent(), Placement.a(getIntent().getIntExtra(IronSourceConstants.EVENTS_PLACEMENT_NAME, 0)));
        if (this.a == null) {
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.b) {
            a();
            this.a.a(this.e);
            this.a.u();
            this.b = false;
        }
        this.a.v();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.a == null || this.a.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.b) {
            this.a.s();
            a.a((Context) this);
        }
        overridePendingTransition(0, 0);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (!this.b) {
            this.a.b(bundle);
            bundle.putInt("activityLockedOrientation", this.g);
            bundle.putBoolean("activityShouldLockOrientation", this.d);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.g == -1) {
            this.g = s.a((Activity) this, this.c, this.d);
        } else {
            com.startapp.common.b.b.a((Activity) this, this.g);
        }
        if (!this.b) {
            this.a.u();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (!this.b) {
            this.a.t();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (!this.b) {
            this.a.w();
            this.a = null;
            s.a((Activity) this, false);
        }
        super.onDestroy();
    }

    public void onBackPressed() {
        if (!this.a.r()) {
            super.onBackPressed();
        }
    }

    public void finish() {
        if (this.a != null) {
            this.a.q();
        }
        super.finish();
    }
}
