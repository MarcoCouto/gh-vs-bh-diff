package com.startapp.sdk.adsbase.j;

import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Region.Op;
import android.graphics.RegionIterator;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import com.iab.omid.library.startapp.b;

/* compiled from: StartAppSDK */
public final class a {
    public static boolean a(View view, int i) {
        if (view == null || view.getParent() == null || view.getRootView() == null || view.getRootView().getParent() == null || !view.hasWindowFocus() || !view.isShown() || view.getWidth() <= 0 || view.getHeight() <= 0) {
            return false;
        }
        Rect rect = new Rect();
        if (view.getGlobalVisibleRect(rect) && !rect.isEmpty() && a(rect, view) >= ((view.getWidth() * view.getHeight()) * Math.min(Math.max(1, i), 100)) / 100) {
            return true;
        }
        return false;
    }

    private static int a(Rect rect, View view) {
        Region region = new Region(rect);
        Rect rect2 = new Rect();
        r6 = view;
        while (true) {
            int i = 0;
            if (!(r6.getParent() instanceof ViewGroup)) {
                while (new RegionIterator(region).next(rect2)) {
                    i += rect2.width() * rect2.height();
                }
                return i;
            } else if (VERSION.SDK_INT >= 11 && r6.getAlpha() < 1.0f) {
                return 0;
            } else {
                ViewGroup viewGroup = (ViewGroup) r6.getParent();
                int childCount = viewGroup.getChildCount();
                for (int indexOfChild = viewGroup.indexOfChild(r6) + 1; indexOfChild < childCount; indexOfChild++) {
                    View childAt = viewGroup.getChildAt(indexOfChild);
                    if (childAt != null && childAt.getVisibility() == 0 && childAt.getGlobalVisibleRect(rect2)) {
                        region.op(rect2, Op.DIFFERENCE);
                    }
                }
                r6 = viewGroup;
            }
        }
    }

    public final String toString() {
        return b.b((Object) this);
    }
}
