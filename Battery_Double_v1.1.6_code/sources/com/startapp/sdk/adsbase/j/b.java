package com.startapp.sdk.adsbase.j;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.startapp.sdk.adsbase.h;
import java.lang.ref.WeakReference;

/* compiled from: StartAppSDK */
public class b implements Runnable {
    private a a;
    private Handler b = new Handler(Looper.getMainLooper());
    private WeakReference<View> c;
    private final h d;
    private final int e;
    private boolean f = true;

    /* compiled from: StartAppSDK */
    public interface a {
        void a();
    }

    static {
        b.class.getSimpleName();
    }

    public b(View view, h hVar, int i) {
        this.c = new WeakReference<>(view);
        this.d = hVar;
        this.e = i;
    }

    public b(WeakReference<View> weakReference, h hVar, int i) {
        this.c = weakReference;
        this.d = hVar;
        this.e = i;
    }

    public final void a(a aVar) {
        this.a = aVar;
    }

    public final void a() {
        if (c()) {
            run();
        }
    }

    public final void b() {
        try {
            if (this.d != null) {
                this.d.a(false);
            }
            if (this.b != null) {
                this.b.removeCallbacksAndMessages(null);
            }
        } catch (Exception e2) {
            new StringBuilder("ViewabilityRunner - clearVisibilityHandler failed ").append(e2.getMessage());
        }
    }

    public void run() {
        try {
            if (c()) {
                boolean a2 = a.a((View) this.c.get(), this.e);
                if (a2 && this.f) {
                    this.f = false;
                    this.d.a();
                    a aVar = this.a;
                } else if (!a2 && !this.f) {
                    this.f = true;
                    this.d.b();
                    if (this.a != null) {
                        this.a.a();
                    }
                }
                this.b.postDelayed(this, 100);
                return;
            }
            b();
        } catch (Exception e2) {
            new StringBuilder("ViewabilityRunner.run - runnable error ").append(e2.getMessage());
            b();
        }
    }

    private boolean c() {
        return (this.d == null || this.d.c() || this.c.get() == null) ? false : true;
    }
}
