package com.startapp.sdk.adsbase.j;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import java.lang.ref.WeakReference;

/* compiled from: StartAppSDK */
public final class c {
    /* access modifiers changed from: private */
    public final Handler a = new Handler(Looper.getMainLooper());
    private final WeakReference<View> b;
    private final int c;

    /* compiled from: StartAppSDK */
    public interface a {
        boolean onUpdate(boolean z);
    }

    public c(View view, int i, final a aVar) {
        this.b = new WeakReference<>(view);
        this.c = i;
        this.a.postDelayed(new Runnable() {
            public final void run() {
                if (aVar.onUpdate(a.a((View) c.this.b.get(), c.this.c))) {
                    c.this.a.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    public final void a() {
        this.a.removeCallbacksAndMessages(null);
    }
}
