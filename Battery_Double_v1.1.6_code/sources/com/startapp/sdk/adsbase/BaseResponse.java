package com.startapp.sdk.adsbase;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* compiled from: StartAppSDK */
public abstract class BaseResponse implements Serializable {
    private static final long serialVersionUID = 1;
    private String errorMessage = null;
    protected Map<String, String> parameters = new HashMap();
    private boolean validResponse = true;

    public final boolean a() {
        return this.validResponse;
    }

    public final String b() {
        return this.errorMessage;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BaseResponse [parameters=");
        sb.append(this.parameters);
        sb.append(", validResponse=");
        sb.append(this.validResponse);
        sb.append(", errorMessage=");
        sb.append(this.errorMessage);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
