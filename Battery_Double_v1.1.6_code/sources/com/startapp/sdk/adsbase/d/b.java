package com.startapp.sdk.adsbase.d;

import android.content.Context;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.startapp.sdk.adsbase.infoevents.a;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class b extends a {
    static {
        b.class.getSimpleName();
    }

    public b(Context context, Runnable runnable, a aVar) {
        super(context, runnable, aVar);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            long millis = TimeUnit.SECONDS.toMillis((long) MetaData.D().A().a());
            final com.startapp.sdk.adsbase.b.b bVar = new com.startapp.sdk.adsbase.b.b(this.a, this);
            a(new Runnable() {
                public final void run() {
                    bVar.a();
                    b.this.a(bVar.b());
                }
            }, millis);
            long currentTimeMillis = System.currentTimeMillis();
            boolean z = currentTimeMillis - j.a(this.a, "lastBtDiscoveringTime", Long.valueOf(0)).longValue() >= ((long) MetaData.D().A().c()) * ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
            if (z) {
                j.b(this.a, "lastBtDiscoveringTime", Long.valueOf(currentTimeMillis));
            }
            bVar.a(z);
        } catch (Throwable th) {
            new e(th).a(this.a);
            a(null);
        }
    }

    public final void a(Object obj) {
        if (obj != null) {
            this.b.c(obj.toString());
        }
        super.a(obj);
    }
}
