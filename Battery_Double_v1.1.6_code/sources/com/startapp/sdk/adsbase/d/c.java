package com.startapp.sdk.adsbase.d;

import android.content.Context;
import com.startapp.sdk.adsbase.f.b;
import com.startapp.sdk.adsbase.infoevents.a;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class c extends a {
    public c(Context context, Runnable runnable, a aVar) {
        super(context, runnable, aVar);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            long millis = TimeUnit.SECONDS.toMillis((long) MetaData.D().z().a());
            final b bVar = new b(this.a, this);
            a(new Runnable() {
                public final void run() {
                    bVar.b();
                    c.this.a(bVar.c());
                }
            }, millis);
            bVar.a();
        } catch (Exception unused) {
            a(null);
        }
    }

    public final void a(Object obj) {
        if (obj != null) {
            this.b.b(obj.toString());
        }
        super.a(obj);
    }
}
