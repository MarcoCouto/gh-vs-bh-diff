package com.startapp.sdk.adsbase.d;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.common.d;

/* compiled from: StartAppSDK */
public abstract class a implements d {
    protected final Context a;
    protected com.startapp.sdk.adsbase.infoevents.a b;
    private Runnable c;
    private Handler d = new Handler(Looper.getMainLooper());

    /* access modifiers changed from: protected */
    public abstract void b();

    public a(Context context, Runnable runnable, com.startapp.sdk.adsbase.infoevents.a aVar) {
        this.a = context;
        this.c = runnable;
        this.b = aVar;
    }

    public final void a() {
        ThreadManager.a(Priority.DEFAULT, (Runnable) new Runnable() {
            public final void run() {
                a.this.b();
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(Runnable runnable, long j) {
        this.d.postDelayed(runnable, j);
    }

    public void a(Object obj) {
        if (this.d != null) {
            this.d.removeCallbacksAndMessages(null);
        }
        this.c.run();
    }
}
