package com.startapp.sdk.adsbase.mraid.a;

import android.content.Context;
import android.os.Build.VERSION;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.startapp.common.b.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a {
    private Context a;
    private List<String> b = new ArrayList();

    public a(Context context) {
        this.a = context.getApplicationContext();
    }

    public final boolean a() {
        return this.b.contains(MRAIDNativeFeature.CALENDAR) && VERSION.SDK_INT >= 14 && b.a(this.a, "android.permission.WRITE_CALENDAR");
    }

    public final boolean b() {
        return this.b.contains(MRAIDNativeFeature.INLINE_VIDEO);
    }

    public final boolean c() {
        return this.b.contains("sms") && b.a(this.a, "android.permission.SEND_SMS");
    }

    public final boolean d() {
        return this.b.contains(MRAIDNativeFeature.STORE_PICTURE);
    }

    public final boolean e() {
        return this.b.contains("tel") && b.a(this.a, "android.permission.CALL_PHONE");
    }

    public final boolean a(String str) {
        return this.b.contains(str);
    }
}
