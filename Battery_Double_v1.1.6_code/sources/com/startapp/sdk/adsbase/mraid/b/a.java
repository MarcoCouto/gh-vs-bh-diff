package com.startapp.sdk.adsbase.mraid.b;

import java.util.Arrays;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a {
    private static final List<String> c = Arrays.asList(new String[]{"portrait", "landscape", "none"});
    public boolean a;
    public int b;

    public a() {
        this(0);
    }

    private a(byte b2) {
        this.a = true;
        this.b = 2;
    }

    public static int a(String str) {
        int indexOf = c.indexOf(str);
        if (indexOf != -1) {
            return indexOf;
        }
        return 2;
    }
}
