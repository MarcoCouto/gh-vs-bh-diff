package com.startapp.sdk.adsbase.mraid.bridge;

/* compiled from: StartAppSDK */
public enum MraidState {
    LOADING,
    DEFAULT,
    EXPANDED,
    RESIZED,
    HIDDEN;

    public final String toString() {
        return name().toLowerCase();
    }
}
