package com.startapp.sdk.adsbase.mraid.bridge;

import android.app.Activity;
import android.content.Context;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.infoevents.e;
import java.net.URLDecoder;
import java.util.Map;

/* compiled from: StartAppSDK */
public abstract class a implements b {
    private static final String TAG = "BaseMraidController";
    protected C0078a openListener;

    /* renamed from: com.startapp.sdk.adsbase.mraid.bridge.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public interface C0078a {
        boolean onClickEvent(String str);
    }

    public abstract void close();

    public void expand(String str) {
    }

    public abstract boolean isFeatureSupported(String str);

    public void resize() {
    }

    public abstract void setOrientationProperties(Map<String, String> map);

    public abstract void useCustomClose(String str);

    public a(C0078a aVar) {
        this.openListener = aVar;
    }

    public boolean open(String str) {
        Exception e;
        String str2;
        try {
            str2 = URLDecoder.decode(str, "UTF-8").trim();
            try {
                if (str2.startsWith("sms")) {
                    return openSMS(str2);
                }
                if (str2.startsWith("tel")) {
                    return openTel(str2);
                }
                return this.openListener.onClickEvent(str2);
            } catch (Exception e2) {
                e = e2;
                e.getMessage();
                return this.openListener.onClickEvent(str2);
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str2 = str;
            e = exc;
            e.getMessage();
            return this.openListener.onClickEvent(str2);
        }
    }

    /* access modifiers changed from: protected */
    public void applyOrientationProperties(Activity activity, com.startapp.sdk.adsbase.mraid.b.a aVar) {
        try {
            int i = 0;
            boolean z = activity.getResources().getConfiguration().orientation == 1;
            if (aVar.b != 0) {
                if (aVar.b != 1) {
                    if (aVar.a) {
                        i = -1;
                    } else if (z) {
                    }
                }
                b.a(activity, i);
            }
            i = 1;
            b.a(activity, i);
        } catch (Throwable th) {
            new e(th).a((Context) activity);
        }
    }

    public void setResizeProperties(Map<String, String> map) {
        new StringBuilder("setResizeProperties ").append(map);
    }

    public void setExpandProperties(Map<String, String> map) {
        new StringBuilder("setExpandProperties ").append(map);
    }

    public void createCalendarEvent(String str) {
        isFeatureSupported(MRAIDNativeFeature.CALENDAR);
    }

    public void playVideo(String str) {
        isFeatureSupported(MRAIDNativeFeature.INLINE_VIDEO);
    }

    public void storePicture(String str) {
        isFeatureSupported(MRAIDNativeFeature.STORE_PICTURE);
    }

    public boolean openSMS(String str) {
        isFeatureSupported("sms");
        return true;
    }

    public boolean openTel(String str) {
        isFeatureSupported("tel");
        return true;
    }
}
