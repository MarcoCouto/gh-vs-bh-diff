package com.startapp.sdk.adsbase;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class AutoInterstitialPreferences implements Serializable {
    private static final long serialVersionUID = 1;
    private int activitiesBetweenAds;
    private int secondsBetweenAds;

    public AutoInterstitialPreferences() {
        setActivitiesBetweenAds(AdsCommonMetaData.a().K());
        setSecondsBetweenAds(AdsCommonMetaData.a().L());
    }

    public int getActivitiesBetweenAds() {
        return this.activitiesBetweenAds;
    }

    public int getSecondsBetweenAds() {
        return this.secondsBetweenAds;
    }

    public AutoInterstitialPreferences setActivitiesBetweenAds(int i) {
        if (i <= 0) {
            i = 1;
        }
        this.activitiesBetweenAds = i;
        return this;
    }

    public AutoInterstitialPreferences setSecondsBetweenAds(int i) {
        if (i < 0) {
            i = 0;
        }
        this.secondsBetweenAds = i;
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AutoInterstitialPreferences [activitiesBetweenAds=");
        sb.append(this.activitiesBetweenAds);
        sb.append(", secondsBetweenAds=");
        sb.append(this.secondsBetweenAds);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
