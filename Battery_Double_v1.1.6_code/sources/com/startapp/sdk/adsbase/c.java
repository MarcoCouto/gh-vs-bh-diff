package com.startapp.sdk.adsbase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.facebook.places.model.PlaceFields;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.SDKException;
import com.startapp.common.a.a;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.i.h;
import com.startapp.sdk.adsbase.i.j;
import com.startapp.sdk.adsbase.i.l;
import com.startapp.sdk.adsbase.i.o;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: StartAppSDK */
public abstract class c {
    private String A;
    private String B;
    private String C;
    private String D;
    private String E;
    private String F;
    private String G;
    private String H;
    private String I = "android";
    private int J;
    private int K;
    private float L;
    private Boolean M;
    private int N = 3;
    private String O;
    private String P;
    private int Q;
    private boolean R;
    private boolean S;
    private boolean T;
    private boolean U;
    private String V;
    private final int W;
    protected Integer a;
    private Map<String, String> b = new HashMap();
    private String c;
    private String d;
    private String e = AdsConstants.d;
    private long f = new BigInteger(AdsConstants.e, 2).longValue();
    private Map<String, String> g = new TreeMap();
    private String h;
    private String i;
    private a j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private Boolean t;
    private Boolean u;
    private String v;
    private String w;
    private String x;
    private String y;
    private String z;

    static {
        c.class.getSimpleName();
    }

    public c(int i2) {
        this.W = i2;
    }

    public final int b() {
        return this.W;
    }

    public final String c() {
        return this.d;
    }

    public void b(int i2) {
        this.a = null;
    }

    public final void c(int i2) {
        this.J = i2;
    }

    public final void d(int i2) {
        this.K = i2;
    }

    public final String d() {
        return this.e;
    }

    public final String e() {
        if (this.O == null) {
            return "";
        }
        return this.O;
    }

    private void a(Context context) {
        this.M = Boolean.valueOf(s.h(context));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BaseRequest [parameters=");
        sb.append(this.b);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public final void b(Context context) {
        if (!MetaData.D().O()) {
            a b2 = com.startapp.sdk.b.c.a(context).d().b();
            this.j = b2;
            if (TextUtils.isEmpty(b2.a()) || "0".equals(b2.a())) {
                if (!j.a(context, "advertising_id_retrieving_failed", Boolean.FALSE).booleanValue()) {
                    j.b(context, "advertising_id_retrieving_failed", Boolean.TRUE);
                    Throwable[] d2 = b2.d();
                    if (d2 != null) {
                        e eVar = null;
                        for (Throwable th : d2) {
                            if (th != null) {
                                if (eVar == null) {
                                    eVar = new e(th);
                                } else {
                                    eVar.a(new e(th));
                                }
                            }
                        }
                        if (eVar != null) {
                            eVar.a(context);
                        }
                    }
                }
                this.m = com.startapp.sdk.b.c.a(context).g().a();
            }
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0006 */
    public final void a(Context context, AdPreferences adPreferences) {
        d(context);
        b(context, adPreferences);
        try {
            c(context);
        } catch (Throwable th) {
            new e(th).a(context);
        }
        try {
            e(context);
        } catch (Throwable th2) {
            new e(th2).a(context);
        }
        try {
            this.H = com.startapp.sdk.b.c.a(context).e().a((Object) this);
        } catch (Throwable th3) {
            new e(th3).a(context);
        }
        try {
            a(adPreferences, context);
        } catch (Throwable th4) {
            new e(th4).a(context);
        }
        try {
            b(context);
        } catch (Throwable th5) {
            new e(th5).a(context);
        }
    }

    public final void c(Context context) {
        this.v = com.startapp.common.b.e.a(context);
        this.w = "e106";
        this.F = "e106";
        com.startapp.common.c a2 = com.startapp.common.c.a();
        if (a2 != null) {
            String b2 = a2.b();
            this.F = b2;
            this.w = b2;
        }
    }

    public final void d(Context context) {
        b k2 = com.startapp.sdk.b.c.a(context).k();
        this.c = k2.a();
        this.d = k2.b();
    }

    public final void b(Context context, AdPreferences adPreferences) {
        this.o = Build.MANUFACTURER;
        this.n = Build.MODEL;
        this.p = Integer.toString(VERSION.SDK_INT);
        if (adPreferences != null) {
            this.i = adPreferences.getAge(context);
        }
        this.k = context.getPackageName();
        this.l = s.g(context);
        this.P = b.d(context);
        this.Q = b.c(context);
        this.t = Boolean.valueOf(b.a(context));
        this.R = b.g(context);
        this.S = b.h(context);
        this.T = b.i(context);
        this.u = com.startapp.common.b.e.b(context);
        this.U = s.m(context);
        Resources resources = context.getResources();
        if (resources != null) {
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            if (displayMetrics != null) {
                this.J = displayMetrics.widthPixels;
                this.K = displayMetrics.heightPixels;
                this.L = displayMetrics.density;
            }
        }
        com.startapp.sdk.b.c a2 = com.startapp.sdk.b.c.a(context);
        com.startapp.sdk.c.b.a aVar = (com.startapp.sdk.c.b.a) a2.a().c();
        this.q = aVar.a();
        this.r = aVar.b();
        this.s = ((com.startapp.sdk.c.a.a) a2.b().c()).a();
        this.V = j.a(context, "USER_CONSENT_PERSONALIZED_ADS_SERVING", (String) null);
        j.b(context, "sharedPrefsWrappers", this.g);
        a(context);
        this.O = o.d().a();
    }

    public final void e(Context context) {
        Object systemService = context.getSystemService(PlaceFields.PHONE);
        if (systemService instanceof TelephonyManager) {
            TelephonyManager telephonyManager = (TelephonyManager) systemService;
            a(telephonyManager);
            b(telephonyManager);
            f(context);
            this.G = b.b(context, telephonyManager);
        }
    }

    public l f() throws SDKException {
        h hVar = new h();
        a((l) hVar);
        return hVar;
    }

    public l a() throws SDKException {
        j jVar = new j();
        a((l) jVar);
        return jVar;
    }

    private void a(l lVar) throws SDKException {
        lVar.a("publisherId", this.c, false);
        lVar.a("productId", this.d, true);
        lVar.a("os", this.I, true);
        lVar.a(GeneralPropertiesWorker.SDK_VERSION, this.e, false);
        lVar.a("flavor", Long.valueOf(this.f), false);
        if (this.g != null && !this.g.isEmpty()) {
            String str = "";
            for (String str2 : this.g.keySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(str2);
                sb.append(":");
                sb.append((String) this.g.get(str2));
                sb.append(";");
                str = sb.toString();
            }
            lVar.a("frameworksData", str.substring(0, str.length() - 1), false, false);
        }
        lVar.a("packageId", this.k, false);
        lVar.a("installerPkg", this.l, false);
        lVar.a(IronSourceSegment.AGE, this.i, false);
        if (this.j != null) {
            lVar.a("userAdvertisingId", this.j.a(), false);
            if (this.j.c()) {
                lVar.a("limat", Boolean.valueOf(this.j.c()), false);
            }
            lVar.a("advertisingIdSource", this.j.b(), false);
        } else if (this.m != null) {
            lVar.a(ServerResponseWrapper.USER_ID_FIELD, this.m, false);
        }
        lVar.a("model", this.n, false);
        lVar.a("manufacturer", this.o, false);
        lVar.a("deviceVersion", this.p, false);
        lVar.a("locale", this.q, false);
        lVar.a("localeList", this.r, false);
        lVar.a("inputLangs", this.s, false);
        lVar.a("isp", this.x, false);
        lVar.a("ispName", this.y, false);
        lVar.a("netOper", this.z, false);
        lVar.a("networkOperName", this.A, false);
        lVar.a("cid", this.B, false);
        lVar.a("lac", this.C, false);
        lVar.a("blat", this.D, false);
        lVar.a("blon", this.E, false);
        lVar.a("rsc", this.H, false, true);
        lVar.a("subPublisherId", null, false);
        lVar.a("subProductId", null, false);
        lVar.a("retryCount", this.a, false);
        lVar.a("roaming", this.u, false);
        lVar.a("grid", this.v, false);
        lVar.a("silev", this.w, false);
        lVar.a("cellSignalLevel", this.F, false);
        lVar.a("cellTimingAdv", this.G, false);
        lVar.a("outsource", this.t, false);
        lVar.a("width", String.valueOf(this.J), false);
        lVar.a("height", String.valueOf(this.K), false);
        lVar.a("density", String.valueOf(this.L), false);
        lVar.a("fgApp", this.M, false);
        lVar.a("sdkId", String.valueOf(this.N), true);
        lVar.a("clientSessionId", this.O, false);
        lVar.a(RequestParameters.APPLICATION_VERSION_NAME, this.P, false);
        lVar.a("appCode", Integer.valueOf(this.Q), false);
        lVar.a("timeSinceBoot", Long.valueOf(SystemClock.elapsedRealtime()), false);
        lVar.a("locations", this.h, false);
        lVar.a("udbg", Boolean.valueOf(this.R), false);
        lVar.a("root", Boolean.valueOf(this.S), false);
        lVar.a("smltr", Boolean.valueOf(this.T), false);
        lVar.a("isddbg", Boolean.valueOf(this.U), false);
        lVar.a("pas", this.V, false);
    }

    private void a(TelephonyManager telephonyManager) {
        if (telephonyManager.getSimState() == 5) {
            this.x = telephonyManager.getSimOperator();
            this.y = telephonyManager.getSimOperatorName();
        }
    }

    private void b(TelephonyManager telephonyManager) {
        int phoneType = telephonyManager.getPhoneType();
        if (phoneType != 0 && phoneType != 2) {
            String networkOperator = telephonyManager.getNetworkOperator();
            if (networkOperator != null) {
                this.z = com.startapp.common.b.a.c(networkOperator);
            }
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            if (networkOperatorName != null) {
                this.A = com.startapp.common.b.a.c(networkOperatorName);
            }
        }
    }

    @SuppressLint({"MissingPermission"})
    private void f(Context context) {
        com.startapp.sdk.c.d.a aVar = (com.startapp.sdk.c.d.a) com.startapp.sdk.b.c.a(context).c().c();
        this.B = aVar.d();
        this.C = aVar.c();
        this.D = aVar.a();
        this.E = aVar.b();
    }

    public final void a(AdPreferences adPreferences, Context context) {
        Collection collection;
        String str = null;
        if (adPreferences == null || adPreferences.getLatitude() == null || adPreferences.getLongitude() == null) {
            collection = null;
        } else {
            Location location = new Location("loc");
            location.setLongitude(adPreferences.getLongitude().doubleValue());
            location.setLongitude(adPreferences.getLongitude().doubleValue());
            location.setProvider("API");
            collection = new LinkedList();
            collection.add(location);
        }
        k.a();
        k.h(context);
        Collection collection2 = (Collection) com.startapp.sdk.b.c.a(context).i().c();
        if (collection2.size() > 0) {
            if (collection == null) {
                collection = collection2;
            } else {
                collection.addAll(collection2);
            }
        }
        if (collection != null) {
            str = com.startapp.common.b.a.c(com.startapp.sdk.c.c.a.a(collection));
        }
        this.h = str;
        a(context, collection != null);
    }

    private static void a(Context context, boolean z2) {
        j.b(context, "shared_prefs_using_location", Boolean.valueOf(z2));
    }

    public final void a(String str) {
        this.H = str;
    }
}
