package com.startapp.sdk.adsbase;

import android.content.Context;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public abstract class JsonAd extends Ad {
    private static final long serialVersionUID = 1;
    private List<AdDetails> adsDetails = null;
    private Long consentTimeStamp;
    private Integer consentType;

    public JsonAd(Context context, Placement placement) {
        super(context, placement);
    }

    public final void a(List<AdDetails> list) {
        this.adsDetails = list;
        Long l = null;
        if (this.adsDetails != null) {
            for (AdDetails adDetails : this.adsDetails) {
                if (!(adDetails == null || adDetails.x() == null)) {
                    if (l == null || adDetails.x().longValue() < l.longValue()) {
                        l = adDetails.x();
                    }
                }
            }
        }
        if (l != null) {
            this.adCacheTtl = Long.valueOf(TimeUnit.SECONDS.toMillis(l.longValue()));
        }
        boolean z = true;
        for (AdDetails i : this.adsDetails) {
            if (!i.i()) {
                z = false;
            }
        }
        this.belowMinCPM = z;
    }

    public final List<AdDetails> g() {
        return this.adsDetails;
    }

    public String getAdId() {
        if (this.adsDetails == null || this.adsDetails.size() <= 0) {
            return null;
        }
        return ((AdDetails) this.adsDetails.get(0)).a();
    }

    public Integer getConsentType() {
        return this.consentType;
    }

    public void setConsentType(Integer num) {
        this.consentType = num;
    }

    public Long getConsentTimestamp() {
        return this.consentTimeStamp;
    }

    public void setConsentTimestamp(Long l) {
        this.consentTimeStamp = l;
    }
}
