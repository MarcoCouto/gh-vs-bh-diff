package com.startapp.sdk.adsbase;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.startapp.common.b;
import com.startapp.sdk.ads.splash.SplashConfig;
import com.startapp.sdk.ads.splash.SplashHideListener;
import com.startapp.sdk.adsbase.Ad.AdState;
import com.startapp.sdk.adsbase.Ad.AdType;
import com.startapp.sdk.adsbase.activities.AppWallActivity;
import com.startapp.sdk.adsbase.activities.OverlayActivity;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adlisteners.a;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.b.c;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class StartAppAd extends Ad {
    private static final long serialVersionUID = 1;
    public f ad = null;
    private CacheKey adKey = null;
    private AdMode adMode = AdMode.AUTOMATIC;
    private AdPreferences adPreferences = null;
    AdDisplayListener callback = null;
    private BroadcastReceiver callbackBroadcastReceiver = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                action = "";
            }
            if (action.equals("com.startapp.android.ShowFailedDisplayBroadcastListener")) {
                Bundle extras = intent.getExtras();
                if (extras == null) {
                    extras = Bundle.EMPTY;
                }
                if (extras.containsKey("showFailedReason")) {
                    StartAppAd.this.a((NotDisplayedReason) extras.getSerializable("showFailedReason"));
                }
                if (StartAppAd.this.callback != null) {
                    StartAppAd.this.callback.adNotDisplayed(StartAppAd.this);
                }
                a(context);
            } else if (action.equals("com.startapp.android.ShowDisplayBroadcastListener")) {
                if (StartAppAd.this.callback != null) {
                    StartAppAd.this.callback.adDisplayed(StartAppAd.this);
                }
            } else if (action.equals("com.startapp.android.OnClickCallback")) {
                if (StartAppAd.this.callback != null) {
                    StartAppAd.this.callback.adClicked(StartAppAd.this);
                }
            } else if (!action.equals("com.startapp.android.OnVideoCompleted")) {
                if (StartAppAd.this.callback != null) {
                    StartAppAd.this.callback.adHidden(StartAppAd.this);
                }
                a(context);
            } else if (StartAppAd.this.videoListener != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        StartAppAd.this.videoListener.onVideoCompleted();
                    }
                });
            }
            StartAppAd.this.ad = null;
        }

        private void a(Context context) {
            b.a(context).a((BroadcastReceiver) this);
        }
    };
    VideoListener videoListener = null;

    /* compiled from: StartAppSDK */
    public enum AdMode {
        AUTOMATIC,
        FULLPAGE,
        OFFERWALL,
        REWARDED_VIDEO,
        VIDEO,
        OVERLAY
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences2, com.startapp.sdk.adsbase.adlisteners.b bVar) {
    }

    public void onPause() {
    }

    public StartAppAd(Context context) {
        super(context, null);
    }

    public static void init(Context context, String str, String str2) {
        StartAppSDK.init(context, str, str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:66:0x016d, code lost:
        if (r9 == false) goto L_0x0170;
     */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01a7  */
    @Deprecated
    public boolean show(String str, AdDisplayListener adDisplayListener) {
        AdRulesResult adRulesResult;
        boolean z;
        AdPreferences adPreferences2;
        a((NotDisplayedReason) null);
        this.callback = new a(adDisplayListener);
        boolean z2 = false;
        if (!MetaData.D().J()) {
            a(NotDisplayedReason.SERVING_ADS_DISABLED);
            this.callback.adNotDisplayed(this);
            return false;
        }
        if (this.adKey == null) {
            loadAd(this.adMode, this.adPreferences);
        }
        boolean z3 = true;
        if (!AdsCommonMetaData.a().O() || s.h(this.a)) {
            if (!isNetworkAvailable()) {
                a(NotDisplayedReason.NETWORK_PROBLEM);
                adRulesResult = null;
            } else if (isReady()) {
                Placement i = i();
                adRulesResult = a(str, i);
                if (adRulesResult.a()) {
                    this.ad = com.startapp.sdk.adsbase.cache.a.a().a(this.adKey);
                    if (this.ad != null) {
                        if (this.placement != Placement.INAPP_SPLASH || !k.a().n()) {
                            boolean a = this.ad.a(str);
                            if (a) {
                                com.startapp.sdk.adsbase.adrules.b.a().a(new com.startapp.sdk.adsbase.adrules.a(i, str));
                                if (this.adMode == null || this.placement == Placement.INAPP_SPLASH || (this.adPreferences != null && !this.adPreferences.equals(new AdPreferences()))) {
                                    z3 = false;
                                }
                                if (z3) {
                                    com.startapp.sdk.adsbase.cache.a.a();
                                    j.b(this.a, com.startapp.sdk.adsbase.cache.a.a(this.adMode), Integer.valueOf(0));
                                    if (this.adMode == AdMode.AUTOMATIC) {
                                        j.b(this.a, com.startapp.sdk.adsbase.cache.a.a(AdMode.FULLPAGE), Integer.valueOf(0));
                                        j.b(this.a, com.startapp.sdk.adsbase.cache.a.a(AdMode.OFFERWALL), Integer.valueOf(0));
                                    }
                                }
                            } else if (this.ad instanceof Ad) {
                                a(((Ad) this.ad).getNotDisplayedReason());
                            }
                            loadAd(this.adMode, this.adPreferences, null);
                            z2 = a;
                        }
                    }
                } else {
                    a(NotDisplayedReason.AD_RULES);
                }
            } else {
                if (!(this.adMode == AdMode.REWARDED_VIDEO || this.adMode == AdMode.VIDEO)) {
                    if (MetaData.D().J() && AdsCommonMetaData.a().I().h()) {
                        if (this.adPreferences == null) {
                            adPreferences2 = new AdPreferences();
                        } else {
                            adPreferences2 = this.adPreferences;
                        }
                        adPreferences2.setType(AdType.NON_VIDEO);
                        Placement i2 = i();
                        f b = com.startapp.sdk.adsbase.cache.a.a().b(new CacheKey(i2, adPreferences2));
                        if (b != null && b.isReady() && a(str, i2).a()) {
                            b.a(true);
                            z = b.a(str);
                        }
                    }
                    z = false;
                }
                z3 = false;
                if (!z3) {
                    a(NotDisplayedReason.AD_NOT_READY);
                }
                adRulesResult = null;
                if (z2 || z3) {
                    a("com.startapp.android.HideDisplayBroadcastListener");
                    a("com.startapp.android.ShowDisplayBroadcastListener");
                    a("com.startapp.android.ShowFailedDisplayBroadcastListener");
                    a("com.startapp.android.OnClickCallback");
                    a("com.startapp.android.OnVideoCompleted");
                }
                if (!z2) {
                    if (getNotDisplayedReason() == null) {
                        a(NotDisplayedReason.INTERNAL_ERROR);
                    }
                    if (getNotDisplayedReason() != NotDisplayedReason.NETWORK_PROBLEM) {
                        if (getNotDisplayedReason() != NotDisplayedReason.AD_RULES) {
                            if (z3) {
                                a.a(this.a, a.a(this.ad != null ? this.ad : com.startapp.sdk.adsbase.cache.a.a().b(this.adKey)), str, NotDisplayedReason.AD_NOT_READY_VIDEO_FALLBACK.toString());
                            } else {
                                a.a(this.a, a.a(this.ad != null ? this.ad : com.startapp.sdk.adsbase.cache.a.a().b(this.adKey)), str, getNotDisplayedReason().toString());
                            }
                        } else if (adRulesResult != null) {
                            a.a(this.a, a.a(com.startapp.sdk.adsbase.cache.a.a().b(this.adKey)), str, adRulesResult.c());
                        }
                    }
                    this.ad = null;
                    if (!z3 && this.callback != null) {
                        this.callback.adNotDisplayed(this);
                    }
                }
                return z2;
            }
            z3 = false;
            a("com.startapp.android.HideDisplayBroadcastListener");
            a("com.startapp.android.ShowDisplayBroadcastListener");
            a("com.startapp.android.ShowFailedDisplayBroadcastListener");
            a("com.startapp.android.OnClickCallback");
            a("com.startapp.android.OnVideoCompleted");
            if (!z2) {
            }
            return z2;
        }
        adRulesResult = null;
        a(NotDisplayedReason.APP_IN_BACKGROUND);
        z3 = false;
        a("com.startapp.android.HideDisplayBroadcastListener");
        a("com.startapp.android.ShowDisplayBroadcastListener");
        a("com.startapp.android.ShowFailedDisplayBroadcastListener");
        a("com.startapp.android.OnClickCallback");
        a("com.startapp.android.OnVideoCompleted");
        if (!z2) {
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public AdRulesResult a(String str, Placement placement) {
        return AdsCommonMetaData.a().G().a(placement, str);
    }

    /* access modifiers changed from: protected */
    public final Placement i() {
        Placement i = super.i();
        return (i != null || this.adKey == null || com.startapp.sdk.adsbase.cache.a.a().b(this.adKey) == null) ? i : ((Ad) com.startapp.sdk.adsbase.cache.a.a().b(this.adKey)).i();
    }

    public String getAdId() {
        f b = com.startapp.sdk.adsbase.cache.a.a().b(this.adKey);
        if (b instanceof HtmlAd) {
            return ((HtmlAd) b).getAdId();
        }
        return null;
    }

    private void a(String str) {
        b.a(this.a).a(this.callbackBroadcastReceiver, new IntentFilter(str));
    }

    @Deprecated
    public boolean show() {
        return show(null, null);
    }

    public void loadAd() {
        loadAd(AdMode.AUTOMATIC, new AdPreferences(), null);
    }

    public void loadAd(AdPreferences adPreferences2) {
        loadAd(AdMode.AUTOMATIC, adPreferences2, null);
    }

    public void loadAd(AdEventListener adEventListener) {
        loadAd(AdMode.AUTOMATIC, new AdPreferences(), adEventListener);
    }

    public void loadAd(AdPreferences adPreferences2, AdEventListener adEventListener) {
        loadAd(AdMode.AUTOMATIC, adPreferences2, adEventListener);
    }

    public void loadAd(AdMode adMode2) {
        loadAd(adMode2, new AdPreferences(), null);
    }

    public void loadAd(AdMode adMode2, AdPreferences adPreferences2) {
        loadAd(adMode2, adPreferences2, null);
    }

    public void loadAd(AdMode adMode2, AdEventListener adEventListener) {
        loadAd(adMode2, new AdPreferences(), adEventListener);
    }

    public boolean showAd() {
        return showAd(null, null);
    }

    public boolean showAd(String str) {
        return showAd(str, null);
    }

    public boolean showAd(AdDisplayListener adDisplayListener) {
        return showAd(null, adDisplayListener);
    }

    public boolean showAd(String str, AdDisplayListener adDisplayListener) {
        try {
            return show(str, adDisplayListener);
        } catch (Throwable th) {
            new e(th).a(this.a);
            a(NotDisplayedReason.INTERNAL_ERROR);
            if (adDisplayListener != null) {
                adDisplayListener.adNotDisplayed(null);
            }
            return false;
        }
    }

    public void setVideoListener(VideoListener videoListener2) {
        this.videoListener = videoListener2;
    }

    public void onResume() {
        if (!isReady()) {
            loadAd();
        }
    }

    public void onBackPressed() {
        showAd("exit_ad");
        k.a().m();
    }

    public void onSaveInstanceState(Bundle bundle) {
        int i;
        switch (this.adMode) {
            case FULLPAGE:
                i = 1;
                break;
            case OFFERWALL:
                i = 2;
                break;
            case OVERLAY:
                i = 3;
                break;
            case REWARDED_VIDEO:
                i = 4;
                break;
            default:
                i = 0;
                break;
        }
        if (this.adPreferences != null) {
            bundle.putSerializable("AdPrefs", this.adPreferences);
        }
        bundle.putInt("AdMode", i);
    }

    public void onRestoreInstanceState(Bundle bundle) {
        int i = bundle.getInt("AdMode");
        this.adMode = AdMode.AUTOMATIC;
        if (i == 1) {
            this.adMode = AdMode.FULLPAGE;
        } else if (i == 2) {
            this.adMode = AdMode.OFFERWALL;
        } else if (i == 3) {
            this.adMode = AdMode.OVERLAY;
        } else if (i == 4) {
            this.adMode = AdMode.REWARDED_VIDEO;
        } else if (i == 5) {
            this.adMode = AdMode.VIDEO;
        }
        Serializable serializable = bundle.getSerializable("AdPrefs");
        if (serializable != null) {
            this.adPreferences = (AdPreferences) serializable;
        }
    }

    public void close() {
        if (this.callbackBroadcastReceiver != null) {
            b.a(this.a).a(this.callbackBroadcastReceiver);
        }
        b.a(this.a).a(new Intent("com.startapp.android.CloseAdActivity"));
    }

    public boolean isReady() {
        f b = com.startapp.sdk.adsbase.cache.a.a().b(this.adKey);
        if (b != null) {
            return b.isReady();
        }
        return false;
    }

    public boolean isNetworkAvailable() {
        return s.c(this.a);
    }

    public static void enableConsent(Context context, boolean z) {
        c.a(context).f().a(z);
    }

    public static void disableSplash() {
        k.a().c(false);
    }

    public CacheKey loadSplash(AdPreferences adPreferences2, AdEventListener adEventListener) {
        this.adKey = com.startapp.sdk.adsbase.cache.a.a().a(this.a, this, adPreferences2, com.startapp.sdk.adsbase.adlisteners.b.a(this.a, adEventListener));
        return this.adKey;
    }

    public static void setReturnAdsPreferences(AdPreferences adPreferences2) {
        k.a().a(adPreferences2);
    }

    public static void setCommonAdsPreferences(Context context, SDKAdPreferences sDKAdPreferences) {
        Context l = s.l(context);
        if (l != null) {
            k.a().a(l, sDKAdPreferences);
        }
    }

    public static void showSplash(Activity activity, Bundle bundle) {
        showSplash(activity, bundle, new SplashConfig());
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig) {
        showSplash(activity, bundle, splashConfig, new AdPreferences());
    }

    public static void showSplash(Activity activity, Bundle bundle, AdPreferences adPreferences2) {
        showSplash(activity, bundle, new SplashConfig(), adPreferences2);
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2) {
        showSplash(activity, bundle, splashConfig, adPreferences2, null);
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2, SplashHideListener splashHideListener) {
        a(activity, bundle, splashConfig, adPreferences2, splashHideListener, true);
    }

    static void a(final Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2, final SplashHideListener splashHideListener, boolean z) {
        if (bundle == null && MetaData.D().J() && s.h(activity)) {
            try {
                k a = k.a();
                if (!a.k() && z) {
                    a.c(true);
                }
                a.b(z);
                if (!z) {
                    if (adPreferences2 == null) {
                        adPreferences2 = new AdPreferences();
                    }
                    adPreferences2.setAs(Boolean.TRUE);
                }
                splashConfig.setDefaults(activity);
                s.a(activity, true);
                Intent intent = new Intent(activity, s.a((Context) activity, OverlayActivity.class, AppWallActivity.class));
                intent.putExtra("SplashConfig", splashConfig);
                intent.putExtra("AdPreference", adPreferences2);
                int i = 0;
                intent.putExtra("testMode", false);
                intent.putExtra(Events.CREATIVE_FULLSCREEN, a.a(activity));
                intent.putExtra(IronSourceConstants.EVENTS_PLACEMENT_NAME, Placement.INAPP_SPLASH.a());
                if (VERSION.SDK_INT >= 11) {
                    i = 32768;
                }
                intent.addFlags(67108864 | i | 1073741824);
                activity.startActivity(intent);
                b.a((Context) activity).a(new BroadcastReceiver() {
                    public final void onReceive(Context context, Intent intent) {
                        s.a(activity, false);
                        if (splashHideListener != null) {
                            splashHideListener.splashHidden();
                        }
                        b.a((Context) activity).a((BroadcastReceiver) this);
                    }
                }, new IntentFilter("com.startapp.android.splashHidden"));
            } catch (Throwable th) {
                new e(th).a((Context) activity);
                if (splashHideListener != null) {
                    splashHideListener.splashHidden();
                }
            }
        }
    }

    public AdState getState() {
        f b = com.startapp.sdk.adsbase.cache.a.a().b(this.adKey);
        if (b != null) {
            return b.getState();
        }
        return AdState.UN_INITIALIZED;
    }

    public boolean isBelowMinCPM() {
        f b = com.startapp.sdk.adsbase.cache.a.a().b(this.adKey);
        if (b != null) {
            return b.isBelowMinCPM();
        }
        return false;
    }

    public static boolean showAd(Context context) {
        if (context == null) {
            return false;
        }
        try {
            return new StartAppAd(context).showAd();
        } catch (Throwable th) {
            new e(th).a(context);
            return false;
        }
    }

    public static void onBackPressed(Context context) {
        new StartAppAd(context).onBackPressed();
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences2, AdEventListener adEventListener) {
        if (!MetaData.D().J()) {
            if (adEventListener != null) {
                setErrorMessage("serving ads disabled");
                adEventListener.onFailedToReceiveAd(this);
            }
            return false;
        }
        this.adKey = com.startapp.sdk.adsbase.cache.a.a().a(this.a, this, this.adMode, adPreferences2, com.startapp.sdk.adsbase.adlisteners.b.a(this.a, adEventListener));
        if (this.adKey != null) {
            return true;
        }
        return false;
    }

    public void loadAd(AdMode adMode2, AdPreferences adPreferences2, AdEventListener adEventListener) {
        this.adMode = adMode2;
        this.adPreferences = adPreferences2;
        try {
            load(adPreferences2, adEventListener);
        } catch (Throwable th) {
            new e(th).a(this.a);
            if (adEventListener != null) {
                adEventListener.onFailedToReceiveAd(this);
            }
        }
    }

    public static void enableAutoInterstitial() {
        a.a.a();
    }

    public static void disableAutoInterstitial() {
        a.a.b();
    }

    public static void setAutoInterstitialPreferences(AutoInterstitialPreferences autoInterstitialPreferences) {
        a.a.a(autoInterstitialPreferences);
    }
}
