package com.startapp.sdk.adsbase;

import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;

/* compiled from: StartAppSDK */
public final class AdsConstants {
    public static final String a = new String("https://imp.startappservice.com/tracking/adImpression");
    public static final Boolean b = Boolean.FALSE;
    public static final Boolean c = Boolean.FALSE;
    public static final String d = s.b();
    public static final String e = s.c();
    public static final String f = s.d();
    public static final String[] g = {"back_", "back_dark", "browser_icon_dark", "forward_", "forward_dark", "x_dark"};
    public static final String[] h = {"empty_star", "filled_star", "half_star"};
    private static String i = new String("get");
    private static String j;
    private static String k;
    private static String l = new String("trackdownload");
    private static String m;
    private static Boolean n = Boolean.FALSE;

    /* compiled from: StartAppSDK */
    public enum AdApiType {
        HTML,
        JSON
    }

    /* compiled from: StartAppSDK */
    public enum ServiceApiType {
        METADATA,
        DOWNLOAD
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(new String("ads"));
        j = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(i);
        sb2.append(new String("htmlad"));
        k = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(i);
        sb3.append(new String("adsmetadata"));
        m = sb3.toString();
        Boolean bool = Boolean.FALSE;
    }

    public static String a(ServiceApiType serviceApiType) {
        String str;
        String str2 = null;
        switch (serviceApiType) {
            case METADATA:
                str = m;
                str2 = MetaData.D().metaDataHostSecured;
                break;
            case DOWNLOAD:
                str = l;
                MetaData D = MetaData.D();
                if (D.trackDownloadHost == null) {
                    str2 = D.p();
                    break;
                } else {
                    str2 = D.trackDownloadHost;
                    break;
                }
            default:
                str = null;
                break;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append(str);
        return sb.toString();
    }

    public static String a(AdApiType adApiType, Placement placement) {
        String str;
        String str2 = null;
        switch (adApiType) {
            case HTML:
                str2 = k;
                str = MetaData.D().a(placement);
                break;
            case JSON:
                str2 = j;
                str = MetaData.D().a(placement);
                break;
            default:
                str = null;
                break;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        return sb.toString();
    }

    public static Boolean a() {
        return n;
    }
}
