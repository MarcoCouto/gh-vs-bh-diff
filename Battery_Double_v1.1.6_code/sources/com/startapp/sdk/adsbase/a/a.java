package com.startapp.sdk.adsbase.a;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

/* compiled from: StartAppSDK */
public class a implements ActivityLifecycleCallbacks {
    private final b a;
    private int b;
    private boolean c;
    private boolean d;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    static {
        a.class.getSimpleName();
    }

    public a(b bVar) {
        this.a = bVar;
    }

    public void onActivityStarted(Activity activity) {
        if (activity != null) {
            this.b++;
            if (this.b == 1 && !this.c) {
                if (!this.d) {
                    this.d = true;
                    this.a.b();
                }
                this.a.c();
            }
        }
    }

    public void onActivityStopped(Activity activity) {
        if (activity != null) {
            this.b--;
            this.c = activity.isChangingConfigurations();
            if (this.b == 0 && !this.c) {
                this.a.d();
            }
        }
    }
}
