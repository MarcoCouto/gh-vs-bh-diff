package com.startapp.sdk.adsbase.adrules;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class a implements Comparable<a> {
    private long a = System.currentTimeMillis();
    private Placement b;
    private String c;

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        long j = this.a - ((a) obj).a;
        if (j > 0) {
            return 1;
        }
        return j == 0 ? 0 : -1;
    }

    public a(Placement placement, String str) {
        this.b = placement;
        if (str == null) {
            str = "";
        }
        this.c = str;
    }

    public final Placement a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdDisplayEvent [displayTime=");
        sb.append(this.a);
        sb.append(", placement=");
        sb.append(this.b);
        sb.append(", adTag=");
        sb.append(this.c);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
