package com.startapp.sdk.adsbase.adrules;

import com.startapp.common.parser.c;
import java.io.Serializable;

@c(a = "type", b = "com.startapp.sdk.adsbase.adrules")
/* compiled from: StartAppSDK */
public abstract class AdRule implements Serializable {
    private static final long serialVersionUID = 1;

    public abstract boolean a();
}
