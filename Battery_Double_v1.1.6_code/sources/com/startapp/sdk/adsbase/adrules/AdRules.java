package com.startapp.sdk.adsbase.adrules;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: StartAppSDK */
public class AdRules implements Serializable {
    private static final long serialVersionUID = 1;
    private transient Set<Class<? extends AdRule>> a = new HashSet();
    private boolean applyOnBannerRefresh = true;
    @d(b = HashMap.class, c = ArrayList.class, d = Placement.class, e = AdRule.class)
    private Map<Placement, List<AdRule>> placements = new HashMap();
    @d(b = ArrayList.class, c = AdRule.class)
    private List<AdRule> session = new ArrayList();
    @d(b = HashMap.class, c = ArrayList.class, e = AdRule.class)
    private Map<String, List<AdRule>> tags = new HashMap();

    public final boolean a() {
        return this.applyOnBannerRefresh;
    }

    public final synchronized AdRulesResult a(Placement placement, String str) {
        AdRulesResult a2;
        String str2;
        this.a.clear();
        List list = (List) this.tags.get(str);
        b.a().a(str);
        a2 = a(list, AdRuleLevel.TAG);
        if (a2.a()) {
            List list2 = (List) this.placements.get(placement);
            b.a().a(placement);
            a2 = a(list2, AdRuleLevel.PLACEMENT);
            if (a2.a()) {
                List<AdRule> list3 = this.session;
                b.a();
                a2 = a(list3, AdRuleLevel.SESSION);
            }
        }
        StringBuilder sb = new StringBuilder("shouldDisplayAd result: ");
        sb.append(a2.a());
        if (a2.a()) {
            str2 = "";
        } else {
            StringBuilder sb2 = new StringBuilder(" because of rule ");
            sb2.append(a2.b());
            str2 = sb2.toString();
        }
        sb.append(str2);
        return a2;
    }

    private AdRulesResult a(List<AdRule> list, AdRuleLevel adRuleLevel) {
        if (list == null) {
            return new AdRulesResult();
        }
        for (AdRule adRule : list) {
            if (!this.a.contains(adRule.getClass())) {
                if (!adRule.a()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(adRule.getClass().getSimpleName());
                    sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(adRuleLevel);
                    return new AdRulesResult(false, sb.toString());
                }
                this.a.add(adRule.getClass());
            }
        }
        return new AdRulesResult();
    }

    public final void b() {
        this.a = new HashSet();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdRules adRules = (AdRules) obj;
        return this.applyOnBannerRefresh == adRules.applyOnBannerRefresh && s.b(this.session, adRules.session) && s.b(this.placements, adRules.placements) && s.b(this.tags, adRules.tags);
    }

    public int hashCode() {
        return s.a(this.session, this.placements, this.tags, Boolean.valueOf(this.applyOnBannerRefresh));
    }
}
