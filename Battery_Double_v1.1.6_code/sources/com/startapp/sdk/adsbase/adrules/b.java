package com.startapp.sdk.adsbase.adrules;

import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class b {
    private static b a = new b();
    private List<a> b = new ArrayList();
    private Map<Placement, List<a>> c = new HashMap();
    private Map<String, List<a>> d = new HashMap();

    public static b a() {
        return a;
    }

    public final void b() {
        this.b.clear();
        this.c.clear();
        this.d.clear();
    }

    public final List<a> a(Placement placement) {
        return (List) this.c.get(placement);
    }

    public final List<a> a(String str) {
        return (List) this.d.get(str);
    }

    public final synchronized void a(a aVar) {
        this.b.add(0, aVar);
        List list = (List) this.c.get(aVar.a());
        if (list == null) {
            list = new ArrayList();
            this.c.put(aVar.a(), list);
        }
        list.add(0, aVar);
        List list2 = (List) this.d.get(aVar.b());
        if (list2 == null) {
            list2 = new ArrayList();
            this.d.put(aVar.b(), list2);
        }
        list2.add(0, aVar);
    }

    public final int c() {
        return this.b.size();
    }
}
