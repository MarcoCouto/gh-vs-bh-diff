package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class LocationConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean coEnabled = false;
    private boolean fiEnabled = false;
    private int ief = 0;

    public final boolean a() {
        return this.fiEnabled;
    }

    public final boolean b() {
        return this.coEnabled;
    }

    public final int c() {
        return this.ief;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LocationConfig locationConfig = (LocationConfig) obj;
        return this.fiEnabled == locationConfig.fiEnabled && this.coEnabled == locationConfig.coEnabled && this.ief == locationConfig.ief;
    }

    public int hashCode() {
        return s.a(Boolean.valueOf(this.fiEnabled), Boolean.valueOf(this.coEnabled), Integer.valueOf(this.ief));
    }
}
