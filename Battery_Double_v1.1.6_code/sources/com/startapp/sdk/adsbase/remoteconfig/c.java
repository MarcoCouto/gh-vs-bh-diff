package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import com.startapp.common.d;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.common.jobrunner.interfaces.RunnerJob.Result;
import com.startapp.common.jobrunner.interfaces.RunnerJob.a;
import com.startapp.sdk.adsbase.i.e;
import com.startapp.sdk.adsbase.infoevents.b;

/* compiled from: StartAppSDK */
public final class c implements RunnerJob {
    public final void a(final Context context, final a aVar) {
        try {
            com.startapp.common.c.b(context);
            MetaData.a(context);
            MetaData.D().i();
            if (MetaData.D().m()) {
                new b(context, true, new d() {
                    public final void a(Object obj) {
                        if (aVar != null) {
                            e.d(context);
                            aVar.a(Result.SUCCESS);
                        }
                    }
                }).a();
                return;
            }
            e.d(context);
            aVar.a(Result.SUCCESS);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
        }
    }
}
