package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.startapp.common.Constants;
import com.startapp.common.a.C0063a;
import com.startapp.common.b.b;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.consent.ConsentConfig;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.AnalyticsConfig;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;
import com.startapp.sdk.insight.NetworkTestsMetaData;
import com.startapp.sdk.triggeredlinks.TriggeredLinksMetadata;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class MetaData implements Serializable {
    private static final Object a = new Object();
    private static Set<String> b = new HashSet(Arrays.asList(new String[]{Constants.a}));
    private static String c = new String("https://adsmetadata.startappservice.com/1.5/");
    private static String d = new String("https://req.startappservice.com/1.5/");
    private static int[] e = {60, 60, PsExtractor.VIDEO_STREAM_MASK};
    private static Set<String> f = new HashSet(Arrays.asList(new String[]{new String("com.facebook.katana"), new String("com.yandex.browser")}));
    private static Set<Integer> g = new HashSet(Arrays.asList(new Integer[]{Integer.valueOf(204)}));
    private static volatile MetaData h = new MetaData();
    private static a k = null;
    private static final long serialVersionUID = 1;
    private long IABDisplayImpressionDelayInSeconds = 1;
    private long IABVideoImpressionDelayInSeconds = 2;
    @d(a = true)
    private SimpleTokenConfig SimpleToken = new SimpleTokenConfig();
    private boolean SupportIABViewability = true;
    private String adPlatformBannerHostSecured;
    public String adPlatformHostSecured = d;
    private String adPlatformNativeHostSecured;
    private String adPlatformOverlayHostSecured;
    private String adPlatformReturnHostSecured;
    private String adPlatformSplashHostSecured;
    private boolean alwaysSendToken = true;
    @d(a = true)
    public AnalyticsConfig analytics = new AnalyticsConfig();
    private String assetsBaseUrlSecured = "";
    @d(a = true)
    private BluetoothConfig btConfig = new BluetoothConfig();
    private boolean btEnabled = false;
    private boolean chromeCustomeTabsExternal = true;
    private boolean chromeCustomeTabsInternal = true;
    private boolean compressionEnabled = false;
    @d(a = true)
    private ConsentConfig consentDetails;
    private boolean disableSendAdvertisingId = AdsConstants.c.booleanValue();
    private boolean dns = false;
    private transient boolean i = false;
    private boolean inAppBrowser = true;
    @d(b = inAppBrowserPreLoad.class)
    private inAppBrowserPreLoad inAppBrowserPreLoad;
    @d(b = HashSet.class)
    private Set<String> installersList = b;
    @d(b = HashSet.class)
    private Set<Integer> invalidForRetry = new HashSet();
    @d(b = HashSet.class)
    private Set<Integer> invalidNetworkCodesInfoEvents = g;
    private boolean isToken1Mandatory = true;
    private transient boolean j = false;
    private transient List<b> l = new ArrayList();
    @d(a = true)
    private LocationConfig location = new LocationConfig();
    public String metaDataHostSecured = c;
    private String metadataUpdateVersion = AdsConstants.d;
    @d(a = true)
    private NetworkTestsMetaData networkTests = new NetworkTestsMetaData();
    private int notVisibleBannerReloadInterval = 3600;
    private boolean omSdkEnabled = false;
    private int[] periodicEventIntMin = e;
    private boolean periodicInfoEventEnabled = false;
    private boolean periodicMetaDataEnabled = false;
    private int periodicMetaDataIntervalInMinutes = 360;
    @d(b = HashSet.class)
    private Set<String> preInstalledPackages = f;
    private String profileId = null;
    @d(a = true)
    private RscMetadata rsc;
    @d(a = true)
    private SensorsConfig sensorsConfig = new SensorsConfig();
    private int sessionMaxBackgroundTime = 1800;
    private boolean simpleToken2 = true;
    private int stopAutoLoadAmount = 3;
    private int stopAutoLoadPreCacheAmount = 3;
    public String trackDownloadHost = d;
    @d(a = true)
    private TriggeredLinksMetadata triggeredLinks;
    private boolean trueNetEnabled = false;
    private long userAgentDelayInSeconds = 5;
    private boolean userAgentEnabled = true;
    private boolean webViewSecured = true;

    /* compiled from: StartAppSDK */
    public static class a implements C0063a {
        private Context a;
        private String b;

        public a(Context context, String str) {
            this.a = context;
            this.b = str;
        }

        public final void a(Bitmap bitmap, int i) {
            if (bitmap != null) {
                com.startapp.sdk.adsbase.i.a.a(this.a, bitmap, this.b, ".png");
            }
        }
    }

    /* compiled from: StartAppSDK */
    public enum inAppBrowserPreLoad {
        DISABLED,
        CONTENT,
        FULL
    }

    static {
        MetaData.class.getSimpleName();
    }

    public final RscMetadata a() {
        return this.rsc;
    }

    public final NetworkTestsMetaData b() {
        return this.networkTests;
    }

    public final TriggeredLinksMetadata c() {
        return this.triggeredLinks;
    }

    public final SimpleTokenConfig d() {
        return this.SimpleToken;
    }

    public final ConsentConfig e() {
        return this.consentDetails;
    }

    public static void a(Context context) {
        MetaData metaData = (MetaData) com.startapp.common.b.d.a(context, "StartappMetadata");
        MetaData metaData2 = new MetaData();
        if (metaData != null) {
            boolean a2 = s.a(metaData, metaData2);
            if (!(!AdsConstants.d.equals(metaData.metadataUpdateVersion)) && a2) {
                new e(InfoEventCategory.ERROR).e("metadata_null").a(context);
            }
            metaData.i = false;
            metaData.j = false;
            metaData.l = new ArrayList();
            h = metaData;
        } else {
            h = metaData2;
        }
        h.Q();
    }

    public static void a(Context context, MetaData metaData, RequestReason requestReason, boolean z) {
        List<b> list;
        synchronized (a) {
            if (h.l != null) {
                list = new ArrayList<>(h.l);
                h.l.clear();
            } else {
                list = null;
            }
            metaData.l = h.l;
            metaData.Q();
            metaData.metadataUpdateVersion = AdsConstants.d;
            com.startapp.common.b.d.b(context, "StartappMetadata", metaData);
            metaData.i = false;
            metaData.j = true;
            if (!s.b(h, metaData)) {
                z = true;
            }
            h = metaData;
            if (s.h(context)) {
                try {
                    j.b(context, "totalSessions", Integer.valueOf(j.a(context, "totalSessions", Integer.valueOf(0)).intValue() + 1));
                } catch (Throwable th) {
                    new e(th).a(context);
                }
            }
            k = null;
        }
        if (list != null) {
            for (b a2 : list) {
                a2.a(requestReason, z);
            }
        }
    }

    public static void f() {
        ArrayList<b> arrayList;
        synchronized (a) {
            if (h.l != null) {
                arrayList = new ArrayList<>(h.l);
                h.l.clear();
            } else {
                arrayList = null;
            }
            h.i = false;
        }
        if (arrayList != null) {
            for (b a2 : arrayList) {
                a2.a();
            }
        }
    }

    public static boolean b(Context context) {
        return context.getFileStreamPath("StartappMetadata").exists();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0015, code lost:
        if (r7 == false) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        if (r8 == null) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0019, code lost:
        r8.a(r6, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0047, code lost:
        return;
     */
    public final void a(Context context, AdPreferences adPreferences, RequestReason requestReason, boolean z, b bVar, boolean z2) {
        if (!z && bVar != null) {
            bVar.a(requestReason, false);
        }
        synchronized (a) {
            if (h.j) {
                if (z2) {
                }
            }
            if (!h.i || z2) {
                this.i = true;
                this.j = false;
                if (k != null) {
                    k.b();
                }
                a aVar = new a(context, adPreferences, requestReason);
                k = aVar;
                aVar.a();
            }
            if (z && bVar != null) {
                h.a(bVar);
            }
        }
    }

    public final void a(b bVar) {
        synchronized (a) {
            this.l.add(bVar);
        }
    }

    public static Object g() {
        return a;
    }

    public final boolean h() {
        return this.j;
    }

    public final void i() {
        this.j = true;
    }

    public final String j() {
        return this.assetsBaseUrlSecured != null ? this.assetsBaseUrlSecured : "";
    }

    public final boolean k() {
        return this.periodicMetaDataEnabled;
    }

    public final int l() {
        return this.periodicMetaDataIntervalInMinutes;
    }

    public final boolean m() {
        return this.periodicInfoEventEnabled;
    }

    public final int c(Context context) {
        if (this.periodicEventIntMin == null || this.periodicEventIntMin.length < 3) {
            this.periodicEventIntMin = e;
        }
        if (b.a(context, "android.permission.ACCESS_FINE_LOCATION")) {
            int i2 = this.periodicEventIntMin[0];
            if (i2 <= 0) {
                return e[0];
            }
            return i2;
        } else if (!b.a(context, "android.permission.ACCESS_COARSE_LOCATION")) {
            return this.periodicEventIntMin[2];
        } else {
            int i3 = this.periodicEventIntMin[1];
            if (i3 <= 0) {
                return e[1];
            }
            return i3;
        }
    }

    public final Set<Integer> n() {
        if (this.invalidForRetry != null) {
            return this.invalidForRetry;
        }
        return new HashSet();
    }

    public final Set<Integer> o() {
        if (this.invalidNetworkCodesInfoEvents != null) {
            return this.invalidNetworkCodesInfoEvents;
        }
        return g;
    }

    public final String p() {
        return this.adPlatformHostSecured != null ? this.adPlatformHostSecured : d;
    }

    public final String a(Placement placement) {
        switch (placement) {
            case INAPP_BANNER:
                return this.adPlatformBannerHostSecured != null ? this.adPlatformBannerHostSecured : p();
            case INAPP_OVERLAY:
                return this.adPlatformOverlayHostSecured != null ? this.adPlatformOverlayHostSecured : p();
            case INAPP_NATIVE:
                return this.adPlatformNativeHostSecured != null ? this.adPlatformNativeHostSecured : p();
            case INAPP_RETURN:
                return this.adPlatformReturnHostSecured != null ? this.adPlatformReturnHostSecured : p();
            case INAPP_SPLASH:
                return this.adPlatformSplashHostSecured != null ? this.adPlatformSplashHostSecured : p();
            default:
                return p();
        }
    }

    public final long r() {
        return TimeUnit.SECONDS.toMillis((long) this.sessionMaxBackgroundTime);
    }

    public final Set<String> s() {
        return this.installersList;
    }

    public final Set<String> t() {
        Set<String> set = this.preInstalledPackages;
        if (set == null) {
            set = f;
        }
        return Collections.unmodifiableSet(set);
    }

    public final boolean u() {
        return this.alwaysSendToken;
    }

    public final boolean v() {
        return this.isToken1Mandatory;
    }

    public final boolean w() {
        return this.compressionEnabled;
    }

    public final boolean x() {
        return s.a(256) && this.inAppBrowser;
    }

    public final String y() {
        return this.profileId;
    }

    public final SensorsConfig z() {
        return this.sensorsConfig;
    }

    public final BluetoothConfig A() {
        return this.btConfig;
    }

    public final LocationConfig B() {
        return this.location;
    }

    public final int C() {
        return this.notVisibleBannerReloadInterval;
    }

    public static MetaData D() {
        return h;
    }

    public final long E() {
        return this.IABDisplayImpressionDelayInSeconds;
    }

    public final long F() {
        return this.IABVideoImpressionDelayInSeconds;
    }

    public final long G() {
        return this.userAgentDelayInSeconds;
    }

    public final boolean H() {
        return this.userAgentEnabled;
    }

    public final boolean I() {
        return this.SupportIABViewability;
    }

    private void Q() {
        this.adPlatformHostSecured = a(this.adPlatformHostSecured, d);
        this.metaDataHostSecured = a(this.metaDataHostSecured, c);
        this.adPlatformBannerHostSecured = a(this.adPlatformBannerHostSecured, (String) null);
        this.adPlatformSplashHostSecured = a(this.adPlatformSplashHostSecured, (String) null);
        this.adPlatformReturnHostSecured = a(this.adPlatformReturnHostSecured, (String) null);
        this.adPlatformOverlayHostSecured = a(this.adPlatformOverlayHostSecured, (String) null);
        this.adPlatformNativeHostSecured = a(this.adPlatformNativeHostSecured, (String) null);
    }

    public final boolean J() {
        return !this.dns;
    }

    public final int K() {
        return this.stopAutoLoadAmount;
    }

    public final int L() {
        return this.stopAutoLoadPreCacheAmount;
    }

    public final boolean M() {
        return this.chromeCustomeTabsInternal;
    }

    public final boolean N() {
        return this.chromeCustomeTabsExternal;
    }

    public final boolean O() {
        return this.disableSendAdvertisingId;
    }

    private static String a(String str, String str2) {
        return str != null ? str.replace("%AdPlatformProtocol%", "1.5") : str2;
    }

    public static void a(Context context, String str) {
        if (str != null && !str.equals("")) {
            if (!com.startapp.sdk.adsbase.i.a.a(context, "close_button", ".png") && !s.a()) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("close_button.png");
                new com.startapp.common.a(sb.toString(), new a(context, "close_button"), 0).a();
            }
            if (s.a(256)) {
                String[] strArr = AdsConstants.g;
                for (int i2 = 0; i2 < 6; i2++) {
                    String str2 = strArr[i2];
                    if (!com.startapp.sdk.adsbase.i.a.a(context, str2, ".png")) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append(str2);
                        sb2.append(".png");
                        new com.startapp.common.a(sb2.toString(), new a(context, str2), 0).a();
                    }
                }
            }
            if (s.a(64)) {
                String[] strArr2 = AdsConstants.h;
                for (int i3 = 0; i3 < 3; i3++) {
                    String str3 = strArr2[i3];
                    if (!com.startapp.sdk.adsbase.i.a.a(context, str3, ".png")) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(str);
                        sb3.append(str3);
                        sb3.append(".png");
                        new com.startapp.common.a(sb3.toString(), new a(context, str3), 0).a();
                    }
                }
                if (!com.startapp.sdk.adsbase.i.a.a(context, "logo", ".png")) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str);
                    sb4.append("logo.png");
                    new com.startapp.common.a(sb4.toString(), new a(context, "logo"), 0).a();
                }
            } else if (s.a(32)) {
                String[] strArr3 = AdsConstants.h;
                for (int i4 = 0; i4 < 3; i4++) {
                    String str4 = strArr3[i4];
                    if (!com.startapp.sdk.adsbase.i.a.a(context, str4, ".png")) {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append(str);
                        sb5.append(str4);
                        sb5.append(".png");
                        new com.startapp.common.a(sb5.toString(), new a(context, str4), 0).a();
                    }
                }
            }
        }
    }

    public final boolean P() {
        return this.omSdkEnabled;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MetaData metaData = (MetaData) obj;
        return this.sessionMaxBackgroundTime == metaData.sessionMaxBackgroundTime && this.simpleToken2 == metaData.simpleToken2 && this.alwaysSendToken == metaData.alwaysSendToken && this.isToken1Mandatory == metaData.isToken1Mandatory && this.compressionEnabled == metaData.compressionEnabled && this.btEnabled == metaData.btEnabled && this.periodicMetaDataEnabled == metaData.periodicMetaDataEnabled && this.periodicMetaDataIntervalInMinutes == metaData.periodicMetaDataIntervalInMinutes && this.periodicInfoEventEnabled == metaData.periodicInfoEventEnabled && this.inAppBrowser == metaData.inAppBrowser && this.SupportIABViewability == metaData.SupportIABViewability && this.IABDisplayImpressionDelayInSeconds == metaData.IABDisplayImpressionDelayInSeconds && this.IABVideoImpressionDelayInSeconds == metaData.IABVideoImpressionDelayInSeconds && this.userAgentDelayInSeconds == metaData.userAgentDelayInSeconds && this.userAgentEnabled == metaData.userAgentEnabled && this.notVisibleBannerReloadInterval == metaData.notVisibleBannerReloadInterval && this.dns == metaData.dns && this.stopAutoLoadAmount == metaData.stopAutoLoadAmount && this.stopAutoLoadPreCacheAmount == metaData.stopAutoLoadPreCacheAmount && this.trueNetEnabled == metaData.trueNetEnabled && this.webViewSecured == metaData.webViewSecured && this.omSdkEnabled == metaData.omSdkEnabled && this.chromeCustomeTabsInternal == metaData.chromeCustomeTabsInternal && this.chromeCustomeTabsExternal == metaData.chromeCustomeTabsExternal && this.disableSendAdvertisingId == metaData.disableSendAdvertisingId && s.b(this.SimpleToken, metaData.SimpleToken) && s.b(this.consentDetails, metaData.consentDetails) && s.b(this.metaDataHostSecured, metaData.metaDataHostSecured) && s.b(this.adPlatformHostSecured, metaData.adPlatformHostSecured) && s.b(this.trackDownloadHost, metaData.trackDownloadHost) && s.b(this.adPlatformBannerHostSecured, metaData.adPlatformBannerHostSecured) && s.b(this.adPlatformSplashHostSecured, metaData.adPlatformSplashHostSecured) && s.b(this.adPlatformReturnHostSecured, metaData.adPlatformReturnHostSecured) && s.b(this.adPlatformOverlayHostSecured, metaData.adPlatformOverlayHostSecured) && s.b(this.adPlatformNativeHostSecured, metaData.adPlatformNativeHostSecured) && s.b(this.profileId, metaData.profileId) && s.b(this.installersList, metaData.installersList) && s.b(this.preInstalledPackages, metaData.preInstalledPackages) && Arrays.equals(this.periodicEventIntMin, metaData.periodicEventIntMin) && s.b(this.sensorsConfig, metaData.sensorsConfig) && s.b(this.btConfig, metaData.btConfig) && s.b(this.assetsBaseUrlSecured, metaData.assetsBaseUrlSecured) && this.inAppBrowserPreLoad == metaData.inAppBrowserPreLoad && s.b(this.invalidForRetry, metaData.invalidForRetry) && s.b(this.invalidNetworkCodesInfoEvents, metaData.invalidNetworkCodesInfoEvents) && s.b(this.analytics, metaData.analytics) && s.b(this.location, metaData.location) && s.b(this.metadataUpdateVersion, metaData.metadataUpdateVersion) && s.b(this.networkTests, metaData.networkTests) && s.b(this.triggeredLinks, metaData.triggeredLinks) && s.b(this.rsc, metaData.rsc);
    }

    public int hashCode() {
        return s.a(this.SimpleToken, this.consentDetails, this.metaDataHostSecured, this.adPlatformHostSecured, this.trackDownloadHost, this.adPlatformBannerHostSecured, this.adPlatformSplashHostSecured, this.adPlatformReturnHostSecured, this.adPlatformOverlayHostSecured, this.adPlatformNativeHostSecured, Integer.valueOf(this.sessionMaxBackgroundTime), this.profileId, this.installersList, this.preInstalledPackages, Boolean.valueOf(this.simpleToken2), Boolean.valueOf(this.alwaysSendToken), Boolean.valueOf(this.isToken1Mandatory), Boolean.valueOf(this.compressionEnabled), Boolean.valueOf(this.btEnabled), Boolean.valueOf(this.periodicMetaDataEnabled), Integer.valueOf(this.periodicMetaDataIntervalInMinutes), Boolean.valueOf(this.periodicInfoEventEnabled), this.periodicEventIntMin, Boolean.valueOf(this.inAppBrowser), Boolean.valueOf(this.SupportIABViewability), Long.valueOf(this.IABDisplayImpressionDelayInSeconds), Long.valueOf(this.IABVideoImpressionDelayInSeconds), Long.valueOf(this.userAgentDelayInSeconds), Boolean.valueOf(this.userAgentEnabled), this.sensorsConfig, this.btConfig, this.assetsBaseUrlSecured, this.inAppBrowserPreLoad, this.invalidForRetry, this.invalidNetworkCodesInfoEvents, Integer.valueOf(this.notVisibleBannerReloadInterval), this.analytics, this.location, this.metadataUpdateVersion, Boolean.valueOf(this.dns), Integer.valueOf(this.stopAutoLoadAmount), Integer.valueOf(this.stopAutoLoadPreCacheAmount), Boolean.valueOf(this.trueNetEnabled), Boolean.valueOf(this.webViewSecured), Boolean.valueOf(this.omSdkEnabled), Boolean.valueOf(this.chromeCustomeTabsInternal), Boolean.valueOf(this.chromeCustomeTabsExternal), Boolean.valueOf(this.disableSendAdvertisingId), this.networkTests, this.triggeredLinks, this.rsc);
    }

    public final String q() {
        String p = h.p();
        String str = (VERSION.SDK_INT > 26 || this.webViewSecured) ? "https" : "http";
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("://");
        if (p.startsWith(sb.toString())) {
            return p;
        }
        int indexOf = p.indexOf(58);
        if (indexOf == -1) {
            return p;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(p.substring(indexOf));
        return sb2.toString();
    }
}
