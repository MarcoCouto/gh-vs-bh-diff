package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* compiled from: StartAppSDK */
public class MetaDataStyle implements Serializable {
    public static final Integer a = Integer.valueOf(18);
    public static final Integer b = Integer.valueOf(-1);
    public static final Set<String> c = new HashSet(Arrays.asList(new String[]{"BOLD"}));
    public static final Integer d = Integer.valueOf(14);
    public static final Integer e = Integer.valueOf(-1);
    public static final Set<String> f = new HashSet();
    private static final long serialVersionUID = 1;
    private Integer itemDescriptionTextColor = e;
    @d(b = HashSet.class)
    private Set<String> itemDescriptionTextDecoration = f;
    private Integer itemDescriptionTextSize = d;
    private Integer itemGradientBottom = Integer.valueOf(-8750199);
    private Integer itemGradientTop = Integer.valueOf(-14014151);
    private Integer itemTitleTextColor = b;
    @d(b = HashSet.class)
    private Set<String> itemTitleTextDecoration = c;
    private Integer itemTitleTextSize = a;
    private String name = "";

    public final Integer a() {
        return this.itemGradientTop;
    }

    public final Integer b() {
        return this.itemGradientBottom;
    }

    public final Integer c() {
        return this.itemTitleTextSize;
    }

    public final Integer d() {
        return this.itemTitleTextColor;
    }

    public final Set<String> e() {
        return this.itemTitleTextDecoration;
    }

    public final Integer f() {
        return this.itemDescriptionTextSize;
    }

    public final Integer g() {
        return this.itemDescriptionTextColor;
    }

    public final Set<String> h() {
        return this.itemDescriptionTextDecoration;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MetaDataStyle metaDataStyle = (MetaDataStyle) obj;
        return s.b(this.name, metaDataStyle.name) && s.b(this.itemGradientTop, metaDataStyle.itemGradientTop) && s.b(this.itemGradientBottom, metaDataStyle.itemGradientBottom) && s.b(this.itemTitleTextSize, metaDataStyle.itemTitleTextSize) && s.b(this.itemTitleTextColor, metaDataStyle.itemTitleTextColor) && s.b(this.itemTitleTextDecoration, metaDataStyle.itemTitleTextDecoration) && s.b(this.itemDescriptionTextSize, metaDataStyle.itemDescriptionTextSize) && s.b(this.itemDescriptionTextColor, metaDataStyle.itemDescriptionTextColor) && s.b(this.itemDescriptionTextDecoration, metaDataStyle.itemDescriptionTextDecoration);
    }

    public int hashCode() {
        return s.a(this.name, this.itemGradientTop, this.itemGradientBottom, this.itemTitleTextSize, this.itemTitleTextColor, this.itemTitleTextDecoration, this.itemDescriptionTextSize, this.itemDescriptionTextColor, this.itemDescriptionTextDecoration);
    }
}
