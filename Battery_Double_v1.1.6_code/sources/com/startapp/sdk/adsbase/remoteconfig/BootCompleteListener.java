package com.startapp.sdk.adsbase.remoteconfig;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.startapp.sdk.adsbase.i.e;
import com.startapp.sdk.adsbase.k;

/* compiled from: StartAppSDK */
public class BootCompleteListener extends BroadcastReceiver {
    static {
        BootCompleteListener.class.getSimpleName();
    }

    public void onReceive(Context context, Intent intent) {
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime() + ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
            e.a(context);
            e.a(context, Long.valueOf(elapsedRealtime));
            e.a(context, elapsedRealtime);
            k.a(context.getApplicationContext());
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
        }
    }
}
