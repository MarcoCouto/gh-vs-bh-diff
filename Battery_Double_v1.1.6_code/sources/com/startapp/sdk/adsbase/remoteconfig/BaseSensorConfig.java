package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class BaseSensorConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private int delay = 3;
    private boolean enabled = true;
    private int minApiLevel = 18;

    public BaseSensorConfig() {
    }

    public BaseSensorConfig(int i) {
        this.minApiLevel = i;
    }

    public final int a() {
        return this.delay;
    }

    public final int b() {
        return this.minApiLevel;
    }

    public final boolean c() {
        return this.enabled;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BaseSensorConfig baseSensorConfig = (BaseSensorConfig) obj;
        return this.delay == baseSensorConfig.delay && this.minApiLevel == baseSensorConfig.minApiLevel && this.enabled == baseSensorConfig.enabled;
    }

    public int hashCode() {
        return s.a(Integer.valueOf(this.delay), Integer.valueOf(this.minApiLevel), Boolean.valueOf(this.enabled));
    }
}
