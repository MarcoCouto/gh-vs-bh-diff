package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import com.startapp.common.c;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.common.jobrunner.interfaces.RunnerJob.Result;
import com.startapp.common.jobrunner.interfaces.RunnerJob.a;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.AdsConstants.ServiceApiType;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;

/* compiled from: StartAppSDK */
public class d implements RunnerJob {
    static {
        d.class.getSimpleName();
    }

    public final void a(Context context, a aVar) {
        try {
            c.b(context);
            MetaData.a(context);
            if (MetaData.D().k()) {
                final AdPreferences adPreferences = new AdPreferences();
                final Context context2 = context;
                final a aVar2 = aVar;
                AnonymousClass1 r1 = new a(context, adPreferences, RequestReason.PERIODIC) {
                    private MetaData b = null;

                    /* access modifiers changed from: protected */
                    public final Boolean c() {
                        try {
                            SimpleTokenUtils.b(context2);
                            MetaDataRequest metaDataRequest = new MetaDataRequest(context2, RequestReason.PERIODIC);
                            metaDataRequest.a(context2, adPreferences);
                            this.b = (MetaData) s.a(com.startapp.sdk.adsbase.h.a.a(context2, AdsConstants.a(ServiceApiType.METADATA), metaDataRequest).a(), MetaData.class);
                            return Boolean.TRUE;
                        } catch (Throwable th) {
                            new e(th).a(context2);
                            return Boolean.FALSE;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public final void a(Boolean bool) {
                        try {
                            if (!(!bool.booleanValue() || this.b == null || context2 == null)) {
                                MetaData.a(context2, this.b, RequestReason.PERIODIC, this.a);
                            }
                            com.startapp.sdk.adsbase.i.e.c(context2);
                            if (aVar2 != null) {
                                aVar2.a(Result.SUCCESS);
                            }
                        } catch (Throwable th) {
                            new e(th).a(context2);
                        }
                    }
                };
                r1.a();
            }
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }
}
