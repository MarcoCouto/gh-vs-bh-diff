package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class BluetoothConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private int discoveryIntervalInMinutes = 1440;
    private boolean enabled = false;
    private int timeoutInSec = 20;

    public final int a() {
        return this.timeoutInSec;
    }

    public final boolean b() {
        return this.enabled;
    }

    public final int c() {
        return this.discoveryIntervalInMinutes;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BluetoothConfig bluetoothConfig = (BluetoothConfig) obj;
        return this.timeoutInSec == bluetoothConfig.timeoutInSec && this.enabled == bluetoothConfig.enabled && this.discoveryIntervalInMinutes == bluetoothConfig.discoveryIntervalInMinutes;
    }

    public int hashCode() {
        return s.a(Integer.valueOf(this.timeoutInSec), Boolean.valueOf(this.enabled), Integer.valueOf(this.discoveryIntervalInMinutes));
    }
}
