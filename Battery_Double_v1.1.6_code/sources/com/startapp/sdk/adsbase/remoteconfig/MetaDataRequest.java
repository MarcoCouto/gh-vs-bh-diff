package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.appevents.UserDataStore;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.SDKException;
import com.startapp.common.b.a;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.i.l;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.k;
import io.fabric.sdk.android.services.common.CommonUtils;

/* compiled from: StartAppSDK */
public final class MetaDataRequest extends c {
    private int b;
    private int c;
    private boolean d;
    private float e;
    private RequestReason f;
    private String g = MetaData.D().y();
    private String h;
    private Integer i;
    private Pair<String, String> j;
    private Integer k;
    private long l;

    /* compiled from: StartAppSDK */
    public enum RequestReason {
        LAUNCH(1),
        APP_IDLE(2),
        IN_APP_PURCHASE(3),
        CUSTOM(4),
        PERIODIC(5),
        PAS(6),
        CONSENT(7);
        
        private int index;

        private RequestReason(int i) {
            this.index = i;
        }
    }

    public MetaDataRequest(Context context, RequestReason requestReason) {
        super(2);
        this.b = j.a(context, "totalSessions", Integer.valueOf(0)).intValue();
        this.c = (int) ((System.currentTimeMillis() - j.a(context, "firstSessionTime", Long.valueOf(System.currentTimeMillis())).longValue()) / 86400000);
        this.e = j.a(context, "inAppPurchaseAmount", Float.valueOf(0.0f)).floatValue();
        this.d = j.a(context, "payingUser", Boolean.FALSE).booleanValue();
        this.f = requestReason;
        SharedPreferences a = j.a(context);
        boolean g2 = k.a().g();
        new s();
        String string = a.getString("shared_prefs_app_apk_hash", null);
        if (TextUtils.isEmpty(string) || g2) {
            string = s.a(CommonUtils.SHA256_INSTANCE, context);
            a.edit().putString("shared_prefs_app_apk_hash", string).commit();
        }
        this.h = string;
        SimpleTokenConfig d2 = MetaData.D().d();
        if (d2 != null && d2.a(context)) {
            int f2 = b.f(context);
            if (f2 > 0) {
                this.i = Integer.valueOf(f2);
            }
        }
        this.j = SimpleTokenUtils.c();
        this.l = SimpleTokenUtils.a();
        this.k = com.startapp.sdk.b.c.a(context).f().d();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("MetaDataRequest [totalSessions=");
        sb.append(this.b);
        sb.append(", daysSinceFirstSession=");
        sb.append(this.c);
        sb.append(", payingUser=");
        sb.append(this.d);
        sb.append(", paidAmount=");
        sb.append(this.e);
        sb.append(", reason=");
        sb.append(this.f);
        sb.append(", ct=");
        sb.append(this.k);
        sb.append(", profileId=");
        sb.append(this.g);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public final l a() throws SDKException {
        l a = super.a();
        if (a == null) {
            a = new com.startapp.sdk.adsbase.i.j();
        }
        a.a(a.a(), a.d(), true);
        a.a("totalSessions", Integer.valueOf(this.b), true);
        a.a("daysSinceFirstSession", Integer.valueOf(this.c), true);
        a.a("payingUser", Boolean.valueOf(this.d), true);
        a.a("profileId", this.g, false);
        a.a("paidAmount", Float.valueOf(this.e), true);
        a.a(IronSourceConstants.EVENTS_ERROR_REASON, this.f, true);
        a.a(UserDataStore.CITY, this.k, false);
        a.a("testAdsEnabled", k.a().o() ? Boolean.TRUE : null, false);
        a.a("apkHash", this.h, false);
        a.a("ian", this.i, false);
        a.a((String) this.j.first, this.j.second, false);
        if (VERSION.SDK_INT >= 9 && this.l != 0) {
            a.a("firstInstalledAppTS", Long.valueOf(this.l), false);
        }
        return a;
    }
}
