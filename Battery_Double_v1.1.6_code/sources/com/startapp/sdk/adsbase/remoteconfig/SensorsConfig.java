package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class SensorsConfig implements Serializable {
    private static final long serialVersionUID = 1;
    @d(a = true)
    private BaseSensorConfig ambientTemperatureSensor = new BaseSensorConfig(14);
    private boolean enabled = false;
    @d(a = true)
    private BaseSensorConfig gravitySensor = new BaseSensorConfig(9);
    @d(a = true)
    private BaseSensorConfig gyroscopeUncalibratedSensor = new BaseSensorConfig(18);
    @d(a = true)
    private BaseSensorConfig lightSensor = new BaseSensorConfig(3);
    @d(a = true)
    private BaseSensorConfig linearAccelerationSensor = new BaseSensorConfig(9);
    @d(a = true)
    private BaseSensorConfig magneticFieldSensor = new BaseSensorConfig(3);
    @d(a = true)
    private BaseSensorConfig pressureSensor = new BaseSensorConfig(9);
    @d(a = true)
    private BaseSensorConfig relativeHumiditySensor = new BaseSensorConfig(14);
    @d(a = true)
    private BaseSensorConfig rotationVectorSensor = new BaseSensorConfig(9);
    private int timeoutInSec = 10;

    public final int a() {
        return this.timeoutInSec;
    }

    public final boolean b() {
        return this.enabled;
    }

    public final BaseSensorConfig c() {
        return this.ambientTemperatureSensor;
    }

    public final BaseSensorConfig d() {
        return this.gravitySensor;
    }

    public final BaseSensorConfig e() {
        return this.lightSensor;
    }

    public final BaseSensorConfig f() {
        return this.linearAccelerationSensor;
    }

    public final BaseSensorConfig g() {
        return this.magneticFieldSensor;
    }

    public final BaseSensorConfig h() {
        return this.pressureSensor;
    }

    public final BaseSensorConfig i() {
        return this.relativeHumiditySensor;
    }

    public final BaseSensorConfig j() {
        return this.rotationVectorSensor;
    }

    public final BaseSensorConfig k() {
        return this.gyroscopeUncalibratedSensor;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SensorsConfig sensorsConfig = (SensorsConfig) obj;
        return this.timeoutInSec == sensorsConfig.timeoutInSec && this.enabled == sensorsConfig.enabled && s.b(this.ambientTemperatureSensor, sensorsConfig.ambientTemperatureSensor) && s.b(this.gravitySensor, sensorsConfig.gravitySensor) && s.b(this.lightSensor, sensorsConfig.lightSensor) && s.b(this.linearAccelerationSensor, sensorsConfig.linearAccelerationSensor) && s.b(this.magneticFieldSensor, sensorsConfig.magneticFieldSensor) && s.b(this.pressureSensor, sensorsConfig.pressureSensor) && s.b(this.relativeHumiditySensor, sensorsConfig.relativeHumiditySensor) && s.b(this.rotationVectorSensor, sensorsConfig.rotationVectorSensor) && s.b(this.gyroscopeUncalibratedSensor, sensorsConfig.gyroscopeUncalibratedSensor);
    }

    public int hashCode() {
        return s.a(Integer.valueOf(this.timeoutInSec), Boolean.valueOf(this.enabled), this.ambientTemperatureSensor, this.gravitySensor, this.lightSensor, this.linearAccelerationSensor, this.magneticFieldSensor, this.pressureSensor, this.relativeHumiditySensor, this.rotationVectorSensor, this.gyroscopeUncalibratedSensor);
    }
}
