package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public class RscMetadata implements Serializable {
    private static final long serialVersionUID = 1389056033245684328L;
    private boolean enabled;
    private int ief;
    @d(b = ArrayList.class, c = RscMetadataItem.class)
    private List<RscMetadataItem> items;
    private String triggers;

    public final boolean a() {
        return this.enabled;
    }

    public final String b() {
        return this.triggers;
    }

    public final List<RscMetadataItem> c() {
        return this.items;
    }

    public final int d() {
        return this.ief;
    }

    public final int a(RscMetadataItem rscMetadataItem) {
        return rscMetadataItem.h() != null ? rscMetadataItem.h().intValue() : this.ief;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RscMetadata rscMetadata = (RscMetadata) obj;
        return this.enabled == rscMetadata.enabled && this.ief == rscMetadata.ief && s.b(this.triggers, rscMetadata.triggers) && s.b(this.items, rscMetadata.items);
    }

    public int hashCode() {
        return s.a(Boolean.valueOf(this.enabled), this.triggers, this.items, Integer.valueOf(this.ief));
    }
}
