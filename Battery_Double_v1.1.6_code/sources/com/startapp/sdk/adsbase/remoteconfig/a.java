package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.splash.SplashMetaData;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.AdsConstants.ServiceApiType;
import com.startapp.sdk.adsbase.adinformation.AdInformationMetaData;
import com.startapp.sdk.adsbase.cache.CacheMetaData;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;

/* compiled from: StartAppSDK */
public class a {
    protected boolean a = false;
    private final Context b;
    private final AdPreferences c;
    private RequestReason d;
    private MetaData e = null;
    private BannerMetaData f = null;
    private SplashMetaData g = null;
    private CacheMetaData h = null;
    private AdInformationMetaData i = null;
    private AdsCommonMetaData j = null;
    private boolean k = false;

    static {
        a.class.getSimpleName();
    }

    public a(Context context, AdPreferences adPreferences, RequestReason requestReason) {
        this.b = context;
        this.c = adPreferences;
        this.d = requestReason;
    }

    public final void a() {
        ThreadManager.a(Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                final Boolean c = a.this.c();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        a.this.a(c);
                    }
                });
            }
        });
    }

    public final void b() {
        this.k = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:20|21|(4:27|(12:29|30|31|(1:33)|37|(3:41|42|(1:44))|48|(4:50|51|52|(1:54))|58|(3:60|61|(1:63))|67|(3:69|70|(1:72)))|76|77)|78|79) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:78:0x0179 */
    public Boolean c() {
        MetaDataRequest metaDataRequest = new MetaDataRequest(this.b, this.d);
        try {
            metaDataRequest.a(this.b, this.c);
            String a2 = com.startapp.sdk.adsbase.h.a.a(this.b, AdsConstants.a(ServiceApiType.METADATA), metaDataRequest).a();
            this.e = (MetaData) s.a(a2, MetaData.class);
            if (!s.a()) {
                this.j = (AdsCommonMetaData) s.a(a2, AdsCommonMetaData.class);
                if (s.a(16) || s.a(32)) {
                    this.f = (BannerMetaData) s.a(a2, BannerMetaData.class);
                }
                if (s.a(8)) {
                    this.g = (SplashMetaData) s.a(a2, SplashMetaData.class);
                }
                if (s.a(512)) {
                    this.h = (CacheMetaData) s.a(a2, CacheMetaData.class);
                }
                if (s.e()) {
                    this.i = (AdInformationMetaData) s.a(a2, AdInformationMetaData.class);
                }
            }
            synchronized (MetaData.g()) {
                if (!(this.k || this.e == null || this.b == null)) {
                    if (!s.a()) {
                        try {
                            if (!s.b(AdsCommonMetaData.a(), this.j)) {
                                this.a = true;
                                AdsCommonMetaData.a(this.b, this.j);
                            }
                        } catch (Throwable th) {
                            new e(th).a(this.b);
                        }
                        if (s.a(16) || s.a(32)) {
                            try {
                                if (!s.b(BannerMetaData.a(), this.f)) {
                                    this.a = true;
                                    BannerMetaData.a(this.b, this.f);
                                }
                            } catch (Throwable th2) {
                                new e(th2).a(this.b);
                            }
                        }
                        if (s.a(8)) {
                            this.g.a().setDefaults(this.b);
                            try {
                                if (!s.b(SplashMetaData.b(), this.g)) {
                                    this.a = true;
                                    SplashMetaData.a(this.b, this.g);
                                }
                            } catch (Throwable th3) {
                                new e(th3).a(this.b);
                            }
                        }
                        if (s.a(512)) {
                            try {
                                if (!s.b(CacheMetaData.a(), this.h)) {
                                    this.a = true;
                                    CacheMetaData.a(this.b, this.h);
                                }
                            } catch (Throwable th4) {
                                new e(th4).a(this.b);
                            }
                        }
                        if (s.e()) {
                            try {
                                if (!s.b(AdInformationMetaData.b(), this.i)) {
                                    this.a = true;
                                    AdInformationMetaData.a(this.b, this.i);
                                }
                            } catch (Throwable th5) {
                                new e(th5).a(this.b);
                            }
                        }
                    }
                    MetaData.a(this.b, this.e.j());
                }
            }
            return Boolean.TRUE;
        } catch (Throwable th6) {
            new e(th6).a(this.b);
            return Boolean.FALSE;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        synchronized (MetaData.g()) {
            if (!this.k) {
                if (!bool.booleanValue() || this.e == null || this.b == null) {
                    MetaData.f();
                } else {
                    try {
                        MetaData.a(this.b, this.e, this.d, this.a);
                    } catch (Throwable th) {
                        new e(th).a(this.b);
                    }
                }
            }
        }
    }
}
