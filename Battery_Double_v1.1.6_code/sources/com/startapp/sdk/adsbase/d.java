package com.startapp.sdk.adsbase;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.util.Pair;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.sdk.adsbase.Ad.AdState;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.model.GetAdRequest;

/* compiled from: StartAppSDK */
public abstract class d {
    protected final Context a;
    /* access modifiers changed from: protected */
    public final Ad b;
    protected final AdPreferences c;
    /* access modifiers changed from: protected */
    public final b d;
    protected Placement e;
    protected String f = null;

    /* access modifiers changed from: protected */
    public abstract boolean a(Object obj);

    /* access modifiers changed from: protected */
    public abstract Object e();

    public d(Context context, Ad ad, AdPreferences adPreferences, b bVar, Placement placement) {
        this.a = context;
        this.b = ad;
        this.c = adPreferences;
        this.d = bVar;
        this.e = placement;
    }

    public final void c() {
        ThreadManager.a(Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                Process.setThreadPriority(10);
                final Boolean d = d.this.d();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        d.this.a(d);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public final Boolean d() {
        return Boolean.valueOf(a(e()));
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        b(bool);
        if (!bool.booleanValue()) {
            this.b.setErrorMessage(this.f);
            this.d.b(this.b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(Boolean bool) {
        this.b.setState(bool.booleanValue() ? AdState.READY : AdState.UN_INITIALIZED);
    }

    /* access modifiers changed from: protected */
    public GetAdRequest a() {
        GetAdRequest b2 = b(new GetAdRequest());
        if (b2 != null) {
            b2.a(this.a);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest b(GetAdRequest getAdRequest) {
        Pair d2 = SimpleTokenUtils.d(this.a);
        try {
            getAdRequest.a(this.a, this.c, this.e, d2);
            getAdRequest.a(this.b.getConsentType(), this.b.getConsentTimestamp());
            if (!AdsCommonMetaData.a().E() && a.a(this.a, this.e)) {
                getAdRequest.h();
            }
            try {
                getAdRequest.a(this.a, this.c);
            } catch (Throwable th) {
                new e(th).a(this.a);
            }
            return getAdRequest;
        } catch (Throwable th2) {
            new e(th2).a(this.a);
            SimpleTokenUtils.a(d2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final Placement f() {
        return this.e;
    }
}
