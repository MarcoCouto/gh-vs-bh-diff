package com.startapp.sdk.adsbase;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.customtabs.CustomTabsService;
import android.text.TextUtils;
import android.webkit.WebView;
import com.adcolony.sdk.AdColonyAppOptions;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.common.b.d;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.interstitials.ReturnAd;
import com.startapp.sdk.ads.splash.SplashConfig;
import com.startapp.sdk.ads.splash.SplashMetaData;
import com.startapp.sdk.adsbase.AdsConstants.ServiceApiType;
import com.startapp.sdk.adsbase.a.b;
import com.startapp.sdk.adsbase.adinformation.AdInformationMetaData;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.cache.CacheMetaData;
import com.startapp.sdk.adsbase.i.o;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;
import com.startapp.sdk.triggeredlinks.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class k implements b {
    /* access modifiers changed from: private */
    public static final String a = "k";
    private boolean A;
    private f B;
    private com.startapp.sdk.adsbase.a.a C;
    private c D;
    private com.startapp.sdk.adsbase.c.b E;
    private boolean F;
    private SDKAdPreferences b;
    private boolean c;
    private boolean d;
    private boolean e;
    private boolean f;
    private boolean g;
    private long h;
    private Application i;
    private HashMap<Integer, Integer> j;
    private Object k;
    private Activity l;
    private boolean m;
    private int n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private Map<String, String> s;
    private Bundle t;
    private AdPreferences u;
    private CacheKey v;
    private boolean w;
    private boolean x;
    private boolean y;
    private boolean z;

    /* compiled from: StartAppSDK */
    static class a {
        /* access modifiers changed from: private */
        public static final k a = new k(0);
    }

    static /* synthetic */ void q() {
    }

    /* synthetic */ k(byte b2) {
        this();
    }

    private k() {
        this.c = s.a(512);
        this.e = false;
        this.f = false;
        this.g = false;
        this.i = null;
        this.j = new HashMap<>();
        this.m = false;
        this.o = false;
        this.p = true;
        this.q = false;
        this.r = false;
        this.t = null;
        this.x = false;
        this.y = false;
        this.z = false;
        this.A = false;
        this.B = null;
    }

    public static k a() {
        return a.a;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:110|(1:112)(2:113|(1:115))|116|117|(1:121)|122|123) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:122:0x0293 */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x024f A[Catch:{ Throwable -> 0x00ae, Throwable -> 0x02af }] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0253 A[Catch:{ Throwable -> 0x00ae, Throwable -> 0x02af }] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0260 A[Catch:{ Throwable -> 0x00ae, Throwable -> 0x02af }] */
    public final void a(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences, boolean z2) {
        com.startapp.sdk.b.c a2;
        Application application;
        if (!this.m) {
            final Context applicationContext = context.getApplicationContext();
            if (!TextUtils.isEmpty(str2)) {
                try {
                    a2 = com.startapp.sdk.b.c.a(applicationContext);
                    a2.k().a(str, str2);
                    if (!com.startapp.common.b.b.a(applicationContext, "android.permission.INTERNET") || !com.startapp.common.b.b.a(applicationContext, "android.permission.ACCESS_NETWORK_STATE")) {
                        s.a(applicationContext, true, "Please grant the mandatory permissions : INTERNET & ACCESS_NETWORK_STATE, SDK could not be initialized.");
                    }
                    this.p = !s.i(applicationContext);
                    TreeMap treeMap = new TreeMap();
                    if (c("org.apache.cordova.CordovaPlugin")) {
                        treeMap.put("Cordova", s.b());
                    }
                    if (c("com.startapp.android.mediation.admob.StartAppCustomEvent")) {
                        treeMap.put(AdColonyAppOptions.ADMOB, d("com.startapp.android.mediation.admob"));
                    }
                    if (c("com.mopub.mobileads.StartAppCustomEventInterstitial")) {
                        treeMap.put(AdColonyAppOptions.MOPUB, d("com.mopub.mobileads"));
                    }
                    if (c("anywheresoftware.b4a.BA") && !a.a.s.containsKey("B4A")) {
                        treeMap.put(AdColonyAppOptions.MOPUB, "0");
                    }
                    if (!treeMap.isEmpty()) {
                        j.a(applicationContext, "sharedPrefsWrappers", (Map<String, String>) treeMap);
                    }
                    if (this.E == null) {
                        com.startapp.sdk.adsbase.c.b bVar = new com.startapp.sdk.adsbase.c.b(applicationContext, Thread.getDefaultUncaughtExceptionHandler());
                        this.E = bVar;
                        Thread.setDefaultUncaughtExceptionHandler(bVar);
                    }
                } catch (Throwable th) {
                    new e(th).a(applicationContext);
                    return;
                }
                a2.d().a();
                if (VERSION.SDK_INT >= 14) {
                    if (applicationContext instanceof Application) {
                        application = (Application) applicationContext;
                    } else if (applicationContext instanceof Activity) {
                        application = ((Activity) applicationContext).getApplication();
                    } else if (applicationContext instanceof Service) {
                        application = ((Service) applicationContext).getApplication();
                    } else {
                        Context applicationContext2 = applicationContext.getApplicationContext();
                        if (applicationContext2 instanceof Application) {
                            application = (Application) applicationContext2;
                        } else {
                            application = null;
                        }
                    }
                    if (application != null && this.C == null) {
                        com.startapp.sdk.adsbase.a.a aVar = new com.startapp.sdk.adsbase.a.a(this);
                        this.C = aVar;
                        application.registerActivityLifecycleCallbacks(aVar);
                    }
                }
                com.startapp.common.c.b(applicationContext);
                com.startapp.sdk.adsbase.i.e.a(applicationContext);
                MetaData.a(applicationContext);
                if (!s.a()) {
                    AdsCommonMetaData.a(applicationContext);
                    if (s.a(16) || s.a(32)) {
                        BannerMetaData.a(applicationContext);
                    }
                    if (s.a(8)) {
                        SplashMetaData.a(applicationContext);
                    }
                    if (this.c) {
                        CacheMetaData.a(applicationContext);
                    }
                    if (s.e()) {
                        AdInformationMetaData.a(applicationContext);
                    }
                }
                SimpleTokenUtils.a(applicationContext);
                MetaData.D().a((com.startapp.sdk.adsbase.remoteconfig.b) a2.f());
                a(applicationContext, sDKAdPreferences);
                com.startapp.common.c.a.a(applicationContext);
                Integer a3 = j.a(applicationContext, "shared_prefs_app_version_id", Integer.valueOf(-1));
                int c2 = com.startapp.common.b.b.c(applicationContext);
                if (a3.intValue() > 0 && c2 > a3.intValue()) {
                    this.r = true;
                }
                j.b(applicationContext, "shared_prefs_app_version_id", Integer.valueOf(c2));
                this.w = false;
                this.d = false;
                if (com.startapp.common.b.b.b() && s.a(2)) {
                    this.w = z2;
                    this.d = true;
                    StringBuilder sb = new StringBuilder("Return Ads: [");
                    sb.append(z2);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                }
                if (this.c) {
                    if (!this.r) {
                        if (CacheMetaData.a().b().d()) {
                            if (this.d) {
                                com.startapp.sdk.adsbase.cache.a.a().a(applicationContext);
                            }
                            i(applicationContext);
                            com.startapp.sdk.adsbase.cache.a.a().c(applicationContext);
                        }
                    }
                    com.startapp.sdk.adsbase.cache.a.a().b(applicationContext);
                    i(applicationContext);
                    com.startapp.sdk.adsbase.cache.a.a().c(applicationContext);
                }
                a(applicationContext, RequestReason.LAUNCH);
                String j2 = j(applicationContext);
                if (VERSION.SDK_INT >= 18) {
                    if (j2 != null) {
                        Intent intent = new Intent(CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION);
                        intent.setPackage(j2);
                        List queryIntentServices = applicationContext.getPackageManager().queryIntentServices(intent, 0);
                        j.b(applicationContext, "chromeTabs", Boolean.valueOf(queryIntentServices != null && !queryIntentServices.isEmpty()));
                        e e2 = new e(InfoEventCategory.GENERAL).e("initialize");
                        int i2 = this.n + 1;
                        this.n = i2;
                        e2.f(String.valueOf(i2)).a(applicationContext);
                        j.b(applicationContext, "periodicInfoEventPaused", Boolean.FALSE);
                        j.b(applicationContext, "periodicMetadataPaused", Boolean.FALSE);
                        AnonymousClass1 r10 = new com.startapp.sdk.adsbase.remoteconfig.b() {
                            public final void a(RequestReason requestReason, boolean z) {
                                if (MetaData.D().H()) {
                                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable(applicationContext) {
                                        public final void run() {
                                            try {
                                                j.b(r3, "User-Agent", new WebView(r3).getSettings().getUserAgentString());
                                            } catch (Throwable th) {
                                                new e(th).a(r3);
                                            }
                                        }
                                    }, TimeUnit.SECONDS.toMillis(MetaData.D().G()));
                                }
                                com.startapp.sdk.adsbase.i.e.c(applicationContext);
                                com.startapp.sdk.adsbase.i.e.d(applicationContext);
                                k kVar = k.this;
                                Context context = applicationContext;
                                if (j.a(context, "shared_prefs_first_init", Boolean.TRUE).booleanValue()) {
                                    j.b(context, "totalSessions", Integer.valueOf(0));
                                    j.b(context, "firstSessionTime", Long.valueOf(System.currentTimeMillis()));
                                    ThreadManager.a(Priority.DEFAULT, (Runnable) new Runnable(context) {
                                        private /* synthetic */ Context a;

                                        {
                                            this.a = r2;
                                        }

                                        public final void run() {
                                            try {
                                                i iVar = new i(this.a);
                                                iVar.a(this.a, new AdPreferences());
                                                com.startapp.common.b.e.a a2 = com.startapp.sdk.adsbase.h.a.a(this.a, AdsConstants.a(ServiceApiType.DOWNLOAD), iVar);
                                                if (a2 != null) {
                                                    String a3 = a2.a();
                                                    if (!TextUtils.isEmpty(a3)) {
                                                        String a4 = s.a(a3, "@ct@", "@ct@");
                                                        String a5 = s.a(a3, "@tsc@", "@tsc@");
                                                        if (!TextUtils.isEmpty(a4) && !TextUtils.isEmpty(a5)) {
                                                            com.startapp.sdk.b.c.a(this.a).f().a(Integer.valueOf(Integer.parseInt(a4)), Long.valueOf(Long.parseLong(a5)), false, true);
                                                        }
                                                    }
                                                }
                                            } catch (Throwable th) {
                                                new e(th).a(this.a);
                                                return;
                                            }
                                            j.b(this.a, "shared_prefs_first_init", Boolean.FALSE);
                                        }
                                    });
                                }
                                k.this.f(applicationContext);
                                k.q();
                                k.a(applicationContext);
                                k.a(k.this, applicationContext);
                                com.startapp.sdk.b.c.a(applicationContext).e().a();
                            }

                            public final void a() {
                                k.a;
                                if (MetaData.D().H()) {
                                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable(applicationContext) {
                                        public final void run() {
                                            try {
                                                j.b(r3, "User-Agent", new WebView(r3).getSettings().getUserAgentString());
                                            } catch (Throwable th) {
                                                new e(th).a(r3);
                                            }
                                        }
                                    }, TimeUnit.SECONDS.toMillis(10));
                                }
                            }
                        };
                        if (!MetaData.D().h()) {
                            r10.a(null, false);
                        } else {
                            MetaData.D().a((com.startapp.sdk.adsbase.remoteconfig.b) r10);
                        }
                        if (com.startapp.common.b.b.b()) {
                            if (applicationContext instanceof Activity) {
                                this.l = (Activity) applicationContext;
                                this.i = this.l.getApplication();
                            } else if (applicationContext.getApplicationContext() instanceof Application) {
                                this.i = (Application) applicationContext.getApplicationContext();
                            }
                            if (!(this.k == null || this.i == null)) {
                                this.i.unregisterActivityLifecycleCallbacks((ActivityLifecycleCallbacks) this.k);
                            }
                            Application application2 = this.i;
                            AnonymousClass3 r11 = new ActivityLifecycleCallbacks() {
                                public final void onActivityStopped(Activity activity) {
                                    k.a;
                                    StringBuilder sb = new StringBuilder("onActivityStopped [");
                                    sb.append(activity.getClass().getName());
                                    sb.append(RequestParameters.RIGHT_BRACKETS);
                                    k.a().c(activity);
                                }

                                public final void onActivityStarted(Activity activity) {
                                    k.a;
                                    StringBuilder sb = new StringBuilder("onActivityStarted [");
                                    sb.append(activity.getClass().getName());
                                    sb.append(RequestParameters.RIGHT_BRACKETS);
                                    k.a().a(activity);
                                }

                                public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                                    k.a;
                                    StringBuilder sb = new StringBuilder("onActivitySaveInstanceState [");
                                    sb.append(activity.getClass().getName());
                                    sb.append(RequestParameters.RIGHT_BRACKETS);
                                    k.a();
                                }

                                public final void onActivityResumed(Activity activity) {
                                    k.a;
                                    StringBuilder sb = new StringBuilder("onActivityResumed [");
                                    sb.append(activity.getClass().getName());
                                    sb.append(RequestParameters.RIGHT_BRACKETS);
                                    k.a().b(activity);
                                }

                                public final void onActivityPaused(Activity activity) {
                                    k.a;
                                    StringBuilder sb = new StringBuilder("onActivityPaused [");
                                    sb.append(activity.getClass().getName());
                                    sb.append(RequestParameters.RIGHT_BRACKETS);
                                    k.a().h();
                                }

                                public final void onActivityDestroyed(Activity activity) {
                                    k.a;
                                    StringBuilder sb = new StringBuilder("onActivityDestroyed [");
                                    sb.append(activity.getClass().getName());
                                    sb.append(RequestParameters.RIGHT_BRACKETS);
                                    k.a().d(activity);
                                }

                                public final void onActivityCreated(Activity activity, Bundle bundle) {
                                    k.a;
                                    StringBuilder sb = new StringBuilder("onActivityCreated [");
                                    sb.append(activity.getClass().getName());
                                    sb.append(", ");
                                    sb.append(bundle);
                                    sb.append(RequestParameters.RIGHT_BRACKETS);
                                    k.a().a(activity, bundle);
                                    if (s.a(2)) {
                                        a.a.a(activity, bundle);
                                    }
                                }
                            };
                            application2.registerActivityLifecycleCallbacks(r11);
                            this.k = r11;
                        }
                        this.m = true;
                        s.a(applicationContext, false, "StartApp SDK initialized with App ID: ".concat(String.valueOf(str2)));
                        return;
                    }
                }
                j.b(applicationContext, "chromeTabs", Boolean.FALSE);
                e e22 = new e(InfoEventCategory.GENERAL).e("initialize");
                int i22 = this.n + 1;
                this.n = i22;
                e22.f(String.valueOf(i22)).a(applicationContext);
                j.b(applicationContext, "periodicInfoEventPaused", Boolean.FALSE);
                j.b(applicationContext, "periodicMetadataPaused", Boolean.FALSE);
                AnonymousClass1 r102 = new com.startapp.sdk.adsbase.remoteconfig.b() {
                    public final void a(RequestReason requestReason, boolean z) {
                        if (MetaData.D().H()) {
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable(applicationContext) {
                                public final void run() {
                                    try {
                                        j.b(r3, "User-Agent", new WebView(r3).getSettings().getUserAgentString());
                                    } catch (Throwable th) {
                                        new e(th).a(r3);
                                    }
                                }
                            }, TimeUnit.SECONDS.toMillis(MetaData.D().G()));
                        }
                        com.startapp.sdk.adsbase.i.e.c(applicationContext);
                        com.startapp.sdk.adsbase.i.e.d(applicationContext);
                        k kVar = k.this;
                        Context context = applicationContext;
                        if (j.a(context, "shared_prefs_first_init", Boolean.TRUE).booleanValue()) {
                            j.b(context, "totalSessions", Integer.valueOf(0));
                            j.b(context, "firstSessionTime", Long.valueOf(System.currentTimeMillis()));
                            ThreadManager.a(Priority.DEFAULT, (Runnable) new Runnable(context) {
                                private /* synthetic */ Context a;

                                {
                                    this.a = r2;
                                }

                                public final void run() {
                                    try {
                                        i iVar = new i(this.a);
                                        iVar.a(this.a, new AdPreferences());
                                        com.startapp.common.b.e.a a2 = com.startapp.sdk.adsbase.h.a.a(this.a, AdsConstants.a(ServiceApiType.DOWNLOAD), iVar);
                                        if (a2 != null) {
                                            String a3 = a2.a();
                                            if (!TextUtils.isEmpty(a3)) {
                                                String a4 = s.a(a3, "@ct@", "@ct@");
                                                String a5 = s.a(a3, "@tsc@", "@tsc@");
                                                if (!TextUtils.isEmpty(a4) && !TextUtils.isEmpty(a5)) {
                                                    com.startapp.sdk.b.c.a(this.a).f().a(Integer.valueOf(Integer.parseInt(a4)), Long.valueOf(Long.parseLong(a5)), false, true);
                                                }
                                            }
                                        }
                                    } catch (Throwable th) {
                                        new e(th).a(this.a);
                                        return;
                                    }
                                    j.b(this.a, "shared_prefs_first_init", Boolean.FALSE);
                                }
                            });
                        }
                        k.this.f(applicationContext);
                        k.q();
                        k.a(applicationContext);
                        k.a(k.this, applicationContext);
                        com.startapp.sdk.b.c.a(applicationContext).e().a();
                    }

                    public final void a() {
                        k.a;
                        if (MetaData.D().H()) {
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable(applicationContext) {
                                public final void run() {
                                    try {
                                        j.b(r3, "User-Agent", new WebView(r3).getSettings().getUserAgentString());
                                    } catch (Throwable th) {
                                        new e(th).a(r3);
                                    }
                                }
                            }, TimeUnit.SECONDS.toMillis(10));
                        }
                    }
                };
                if (!MetaData.D().h()) {
                }
                if (com.startapp.common.b.b.b()) {
                }
                this.m = true;
                s.a(applicationContext, false, "StartApp SDK initialized with App ID: ".concat(String.valueOf(str2)));
                return;
            }
            throw new IllegalArgumentException("\n+-------------------------------------------------------------+\n|                S   T   A   R   T   A   P   P                |\n| - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |\n| Invalid App ID passed to init, please provide valid App ID  |\n|                                                             |\n| https://support.startapp.com/hc/en-us/articles/360002411114 |\n+-------------------------------------------------------------+\n");
        }
    }

    public final void b() {
        if (this.D != null) {
            this.D.a();
        }
    }

    public final void c() {
        if (this.D != null) {
            this.D.b();
        }
    }

    public final void d() {
        if (this.D != null) {
            this.D.c();
        }
    }

    public static void a(Context context) {
        com.startapp.sdk.insight.a.a(context, MetaData.D().b());
    }

    static void b(Context context) {
        j.b(context, "periodicInfoEventPaused", Boolean.TRUE);
        com.startapp.sdk.adsbase.i.e.a(786564404);
    }

    static void c(Context context) {
        j.b(context, "periodicMetadataPaused", Boolean.TRUE);
        com.startapp.sdk.adsbase.i.e.a(586482792);
    }

    static void d(Context context) {
        j.b(context, "periodicInfoEventPaused", Boolean.FALSE);
        com.startapp.sdk.adsbase.i.e.a(context, j.a(context, "periodicInfoEventTriggerTime", Long.valueOf(com.startapp.sdk.adsbase.i.e.b(context))).longValue());
    }

    static void e(Context context) {
        j.b(context, "periodicMetadataPaused", Boolean.FALSE);
        com.startapp.sdk.adsbase.i.e.a(context, Long.valueOf(j.a(context, "periodicMetadataTriggerTime", Long.valueOf(com.startapp.sdk.adsbase.i.e.a())).longValue()));
    }

    /* access modifiers changed from: 0000 */
    public final void f(Context context) {
        com.startapp.sdk.adsbase.infoevents.b bVar = new com.startapp.sdk.adsbase.infoevents.b(context);
        bVar.b().d(AdsConstants.f);
        bVar.a();
        if (this.r) {
            new e(InfoEventCategory.GENERAL).e("packagingType").f(AdsConstants.f).a(context);
        }
    }

    private static boolean e(Activity activity) {
        return activity.getClass().getName().equals(s.b((Context) activity));
    }

    private boolean f(Activity activity) {
        return this.A || e(activity);
    }

    public final void a(Activity activity, Bundle bundle) {
        if (e(activity)) {
            this.A = true;
        }
        this.t = bundle;
    }

    public final void a(Activity activity) {
        StringBuilder sb = new StringBuilder("onActivityStarted [");
        sb.append(activity.getClass().getName());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        boolean f2 = f(activity);
        boolean z2 = !this.z && f2 && this.t == null && this.j.size() == 0;
        if (z2) {
            com.startapp.sdk.b.c.a(activity).f().f();
        }
        if (s.a(8) && !com.startapp.sdk.b.c.a(activity).f().b() && !AdsCommonMetaData.a().A() && !this.y && !b(AdColonyAppOptions.MOPUB) && !b(AdColonyAppOptions.ADMOB) && !this.x && z2) {
            StartAppAd.a(activity, this.t, new SplashConfig(), new AdPreferences(), null, false);
        }
        if (f2) {
            this.A = false;
            this.z = true;
        }
        if (this.e) {
            if (MetaData.D().J() && this.w && !AdsCommonMetaData.a().z() && !s.a() && !this.q) {
                if (System.currentTimeMillis() - this.h > AdsCommonMetaData.a().y()) {
                    this.B = com.startapp.sdk.adsbase.cache.a.a().a(this.v);
                    if (this.B != null && this.B.isReady()) {
                        AdRulesResult a2 = AdsCommonMetaData.a().G().a(Placement.INAPP_RETURN, (String) null);
                        if (!a2.a()) {
                            a.a((Context) activity, ((ReturnAd) this.B).trackingUrls, (String) null, a2.c());
                        } else if (this.B.a((String) null)) {
                            com.startapp.sdk.adsbase.adrules.b.a().a(new com.startapp.sdk.adsbase.adrules.a(Placement.INAPP_RETURN, null));
                        }
                    }
                }
            }
            if (System.currentTimeMillis() - this.h > MetaData.D().r()) {
                a((Context) activity, RequestReason.APP_IDLE);
            }
        }
        this.g = false;
        this.e = false;
        if (((Integer) this.j.get(Integer.valueOf(activity.hashCode()))) == null) {
            this.j.put(Integer.valueOf(activity.hashCode()), Integer.valueOf(1));
            StringBuilder sb2 = new StringBuilder("Activity Added:[");
            sb2.append(activity.getClass().getName());
            sb2.append(RequestParameters.RIGHT_BRACKETS);
            return;
        }
        StringBuilder sb3 = new StringBuilder("Activity [");
        sb3.append(activity.getClass().getName());
        sb3.append("] already exists");
    }

    public final void b(Activity activity) {
        if (this.c && this.f) {
            this.f = false;
            com.startapp.sdk.adsbase.cache.a.a().b();
        }
        if (this.o) {
            this.o = false;
            SimpleTokenUtils.c(activity.getApplicationContext());
        }
        this.l = activity;
    }

    public final void e() {
        this.o = true;
        this.f = true;
    }

    public final void a(boolean z2) {
        this.q = z2;
    }

    public final boolean f() {
        return this.p;
    }

    public final boolean g() {
        return this.r;
    }

    public final void h() {
        this.h = System.currentTimeMillis();
        this.l = null;
    }

    public final void c(Activity activity) {
        StringBuilder sb = new StringBuilder("onActivityStopped [");
        sb.append(activity.getClass().getName());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        Integer num = (Integer) this.j.get(Integer.valueOf(activity.hashCode()));
        if (num != null) {
            Integer valueOf = Integer.valueOf(num.intValue() - 1);
            if (valueOf.intValue() == 0) {
                this.j.remove(Integer.valueOf(activity.hashCode()));
            } else {
                this.j.put(Integer.valueOf(activity.hashCode()), valueOf);
            }
            StringBuilder sb2 = new StringBuilder("Activity removed:[");
            sb2.append(activity.getClass().getName());
            sb2.append(RequestParameters.RIGHT_BRACKETS);
            if (this.j.size() == 0) {
                if (!this.g) {
                    this.e = true;
                    i(activity);
                    if (com.startapp.common.c.a() != null) {
                        com.startapp.common.c.a().a(activity);
                    }
                }
                if (this.c) {
                    com.startapp.sdk.adsbase.cache.a.a().a(activity.getApplicationContext(), this.g);
                    this.f = true;
                }
            }
        } else {
            StringBuilder sb3 = new StringBuilder("Activity hadn't been found:[");
            sb3.append(activity.getClass().getName());
            sb3.append(RequestParameters.RIGHT_BRACKETS);
        }
    }

    public final void d(Activity activity) {
        if (f(activity)) {
            this.z = false;
        }
        if (this.j.size() == 0) {
            this.e = false;
        }
    }

    public final boolean i() {
        if (this.l != null) {
            return this.l.isTaskRoot();
        }
        return true;
    }

    public final boolean j() {
        return this.w;
    }

    public final void b(boolean z2) {
        this.x = z2;
    }

    public final void c(boolean z2) {
        this.y = !z2;
        if (!z2) {
            com.startapp.sdk.adsbase.cache.a.a().b(Placement.INAPP_SPLASH);
        }
    }

    public final boolean k() {
        return !this.y;
    }

    public final boolean l() {
        return this.d && !this.e && !this.g;
    }

    public final boolean a(Placement placement) {
        if (!this.d || this.g) {
            return false;
        }
        if (!this.e) {
            return true;
        }
        if (placement != Placement.INAPP_RETURN || !CacheMetaData.a().b().e()) {
            return false;
        }
        return true;
    }

    public final void m() {
        this.e = false;
        this.g = true;
    }

    public final boolean n() {
        return this.d && this.e;
    }

    public final void a(Context context, String str, String str2) {
        if (this.s == null) {
            this.s = new TreeMap();
        }
        this.s.put(str, str2);
        j.a(context, "sharedPrefsWrappers", this.s);
    }

    private String a(String str) {
        if (this.s == null) {
            return null;
        }
        return (String) this.s.get(str);
    }

    private boolean b(String str) {
        return a(str) != null;
    }

    public final SDKAdPreferences g(Context context) {
        if (this.b == null) {
            Class<SDKAdPreferences> cls = SDKAdPreferences.class;
            SDKAdPreferences sDKAdPreferences = (SDKAdPreferences) d.a(context, "shared_prefs_sdk_ad_prefs");
            if (sDKAdPreferences == null) {
                this.b = new SDKAdPreferences();
            } else {
                this.b = sDKAdPreferences;
            }
        }
        return this.b;
    }

    public final void a(Context context, SDKAdPreferences sDKAdPreferences) {
        this.b = sDKAdPreferences;
        d.b(context, "shared_prefs_sdk_ad_prefs", this.b);
    }

    protected static void a(Context context, RequestReason requestReason) {
        o.d().a(context, requestReason);
    }

    /* access modifiers changed from: protected */
    public final void d(boolean z2) {
        boolean z3 = z2 && com.startapp.common.b.b.b();
        this.w = z3;
        if (!z3) {
            com.startapp.sdk.adsbase.cache.a.a().b(Placement.INAPP_RETURN);
        }
    }

    public final boolean o() {
        return this.F;
    }

    /* access modifiers changed from: 0000 */
    public final void e(boolean z2) {
        this.F = z2;
    }

    protected static void a(Context context, String str, boolean z2, boolean z3) {
        if ("pas".equalsIgnoreCase(str)) {
            String string = context.getSharedPreferences("com.startapp.sdk", 0).getString("USER_CONSENT_PERSONALIZED_ADS_SERVING", null);
            if (!TextUtils.isEmpty(string)) {
                if (string.equalsIgnoreCase(z2 ? "1" : "0")) {
                    return;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append(z2 ? "1" : "0");
            sb.append(z3 ? "M" : "A");
            new e(InfoEventCategory.GENERAL).e("User consent: ".concat(String.valueOf(str))).f(sb.toString()).a(context);
            j.b(context, "USER_CONSENT_PERSONALIZED_ADS_SERVING", z2 ? "1" : "0");
            o.d().a(context, RequestReason.PAS);
        }
    }

    public static void h(Context context) {
        b(context, "android.permission.ACCESS_FINE_LOCATION", "USER_CONSENT_FINE_LOCATION");
        b(context, "android.permission.ACCESS_COARSE_LOCATION", "USER_CONSENT_COARSE_LOCATION");
    }

    private static void b(Context context, String str, String str2) {
        boolean booleanValue = j.a(context, str2, Boolean.FALSE).booleanValue();
        boolean a2 = com.startapp.common.b.b.a(context, str);
        if (booleanValue != a2) {
            j.b(context, str2, Boolean.valueOf(a2));
            System.currentTimeMillis();
            a(context, str, a2, false);
        }
    }

    private static boolean c(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        } catch (Exception unused2) {
            return false;
        }
    }

    private static String d(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".StartAppConstants");
            return (String) Class.forName(sb.toString()).getField("WRAPPER_VERSION").get(null);
        } catch (Exception unused) {
            return "0";
        }
    }

    private static String j(Context context) {
        String str;
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.example.com"));
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
            String str2 = resolveActivity != null ? resolveActivity.activityInfo.packageName : null;
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
            ArrayList arrayList = new ArrayList();
            for (ResolveInfo resolveInfo : queryIntentActivities) {
                Intent intent2 = new Intent();
                intent2.setAction(CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION);
                intent2.setPackage(resolveInfo.activityInfo.packageName);
                if (packageManager.resolveService(intent2, 0) != null) {
                    arrayList.add(resolveInfo.activityInfo.packageName);
                }
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            if (arrayList.size() == 1) {
                str = (String) arrayList.get(0);
            } else if (!TextUtils.isEmpty(str2) && !a(context, intent) && arrayList.contains(str2)) {
                return str2;
            } else {
                if (!arrayList.contains("com.android.chrome")) {
                    return null;
                }
                str = "com.android.chrome";
            }
            return str;
        } catch (Exception unused) {
            return null;
        }
    }

    private static boolean a(Context context, Intent intent) {
        try {
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 64);
            if (queryIntentActivities != null) {
                if (queryIntentActivities.size() != 0) {
                    for (ResolveInfo resolveInfo : queryIntentActivities) {
                        IntentFilter intentFilter = resolveInfo.filter;
                        if (intentFilter != null && intentFilter.countDataAuthorities() != 0 && intentFilter.countDataPaths() != 0 && resolveInfo.activityInfo != null) {
                            return true;
                        }
                    }
                    return false;
                }
            }
            return false;
        } catch (RuntimeException unused) {
        }
    }

    private AdPreferences s() {
        AdPreferences adPreferences = this.u;
        return adPreferences != null ? new AdPreferences(adPreferences) : new AdPreferences();
    }

    public final void a(AdPreferences adPreferences) {
        boolean z2 = !s.b(this.u, adPreferences);
        this.u = adPreferences != null ? new AdPreferences(adPreferences) : null;
        if (z2) {
            com.startapp.sdk.adsbase.cache.a.a().a(Placement.INAPP_RETURN);
        }
    }

    private void i(Context context) {
        if (this.w && !AdsCommonMetaData.a().z()) {
            this.v = com.startapp.sdk.adsbase.cache.a.a().a(context, s());
        }
    }

    public static boolean p() {
        return a.a.a(AdColonyAppOptions.UNITY) != null;
    }

    static /* synthetic */ void a(k kVar, Context context) {
        if (kVar.D == null) {
            kVar.D = com.startapp.sdk.b.c.a(context).j();
            kVar.D.d();
        }
    }
}
