package com.startapp.sdk.adsbase;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;

/* compiled from: StartAppSDK */
public class StartAppInitProvider extends ContentProvider {

    /* compiled from: StartAppSDK */
    interface a {
        String a();

        boolean b();
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    public boolean onCreate() {
        Context context = getContext();
        if (context == null) {
            return false;
        }
        Context applicationContext = context.getApplicationContext();
        g gVar = new g(applicationContext);
        String a2 = gVar.a();
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        k.a().a(applicationContext, null, a2, new SDKAdPreferences(), gVar.b());
        if (applicationContext.getSharedPreferences("com.startapp.sdk", 0).getBoolean("shared_prefs_first_init", true)) {
            new e(InfoEventCategory.GENERAL).e("ManifestInit").a(applicationContext);
        }
        return true;
    }
}
