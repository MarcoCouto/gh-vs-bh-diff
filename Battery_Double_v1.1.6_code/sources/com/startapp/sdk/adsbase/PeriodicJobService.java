package com.startapp.sdk.adsbase;

import android.annotation.TargetApi;
import android.app.job.JobInfo.Builder;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.startapp.common.b.b;
import com.startapp.common.jobrunner.a;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.common.jobrunner.interfaces.RunnerJob.Result;

@TargetApi(21)
/* compiled from: StartAppSDK */
public class PeriodicJobService extends JobService {
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }

    static {
        b.a(PeriodicJobService.class);
    }

    public boolean onStartJob(final JobParameters jobParameters) {
        if (jobParameters.getJobId() == Integer.MAX_VALUE) {
            JobScheduler jobScheduler = (JobScheduler) getApplicationContext().getSystemService("jobscheduler");
            if (jobScheduler != null) {
                jobScheduler.schedule(new Builder(Integer.MAX_VALUE, new ComponentName(getApplicationContext(), PeriodicJobService.class)).setPersisted(true).setMinimumLatency(ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS).build());
            }
            return false;
        }
        a.a((Context) this);
        boolean a = a.a(jobParameters, (RunnerJob.a) new RunnerJob.a() {
            public final void a(Result result) {
                PeriodicJobService.this.jobFinished(jobParameters, false);
            }
        });
        "onStartJob: RunnerManager.runJob".concat(String.valueOf(a));
        a.c();
        return a;
    }
}
