package com.startapp.sdk.adsbase.c;

import android.content.Context;
import android.util.Pair;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/* compiled from: StartAppSDK */
class c implements Runnable {
    private static final Comparator<File> a = new Comparator<File>() {
        public final /* synthetic */ int compare(Object obj, Object obj2) {
            return ((File) obj2).getName().compareTo(((File) obj).getName());
        }
    };
    private final Context b;
    private final File[] c;

    static {
        c.class.getSimpleName();
    }

    public c(Context context, File[] fileArr) {
        this.b = context;
        this.c = fileArr;
    }

    public void run() {
        File[] fileArr;
        Arrays.sort(this.c, a);
        e eVar = null;
        e eVar2 = null;
        int i = 0;
        for (File file : this.c) {
            if (i >= 5) {
                s.d(file);
            } else {
                List c2 = s.c(file);
                Pair pair = (c2 == null || c2.size() != 2) ? null : new Pair(c2.get(0), c2.get(1));
                if (pair == null || pair.first == null || pair.second == null) {
                    s.d(file);
                } else {
                    e eVar3 = new e(InfoEventCategory.EXCEPTION_FATAL);
                    eVar3.e((String) pair.first);
                    eVar3.f((String) pair.second);
                    eVar3.a((Object) file);
                    if (eVar == null) {
                        eVar = eVar3;
                    }
                    if (eVar2 != null) {
                        eVar2.a(eVar3);
                    }
                    i++;
                    eVar2 = eVar3;
                }
            }
        }
        if (eVar != null) {
            eVar.a(this.b, new a());
        }
    }
}
