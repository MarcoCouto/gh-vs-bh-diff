package com.startapp.sdk.adsbase.c;

import android.content.Context;
import android.os.SystemClock;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.sdk.adsbase.i.q;
import com.startapp.sdk.adsbase.i.s;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.Thread.UncaughtExceptionHandler;

/* compiled from: StartAppSDK */
public class b implements UncaughtExceptionHandler {
    private final File a;
    private final UncaughtExceptionHandler b;

    static {
        b.class.getSimpleName();
    }

    public b(Context context, UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.b = uncaughtExceptionHandler;
        this.a = new File(context.getCacheDir(), "StartApp-767b8b9bfc82ce39");
        this.a.mkdirs();
        File[] listFiles = this.a.listFiles();
        if (listFiles != null && listFiles.length > 0) {
            ThreadManager.a(Priority.DEFAULT, (Runnable) new c(context, listFiles));
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        StackTraceElement a2 = a(th);
        if (a2 != null) {
            OutputStream outputStream = null;
            try {
                File file = this.a;
                StringBuilder sb = new StringBuilder();
                sb.append(System.currentTimeMillis());
                sb.append("-");
                sb.append(SystemClock.uptimeMillis());
                OutputStream fileOutputStream = new FileOutputStream(new File(file, sb.toString()));
                try {
                    new PrintStream(fileOutputStream).println(s.a(a2));
                    outputStream = s.a(fileOutputStream);
                    PrintWriter printWriter = new PrintWriter(outputStream);
                    a(th, printWriter);
                    printWriter.close();
                } catch (Throwable th2) {
                    th = th2;
                    outputStream = fileOutputStream;
                    s.a((Closeable) outputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                s.a((Closeable) outputStream);
                throw th;
            }
            s.a((Closeable) outputStream);
        }
        if (this.b != null) {
            this.b.uncaughtException(thread, th);
        }
    }

    public static void a(Throwable th, PrintWriter printWriter) {
        q qVar = new q(th);
        while (qVar.hasNext()) {
            Throwable b2 = qVar.next();
            if (qVar.a()) {
                printWriter.println('-');
            }
            printWriter.println(b2.toString().trim());
            StackTraceElement[] stackTrace = b2.getStackTrace();
            if (stackTrace != null) {
                int length = stackTrace.length;
                StackTraceElement stackTraceElement = null;
                int i = 0;
                int i2 = 0;
                boolean z = false;
                while (i < length) {
                    StackTraceElement stackTraceElement2 = stackTrace[i];
                    if (stackTraceElement2 != null) {
                        String className = stackTraceElement2.getClassName();
                        if (className != null) {
                            boolean z2 = i < 3;
                            boolean startsWith = className.startsWith("com.startapp.");
                            if (z2 || startsWith || z) {
                                if (i2 > 0) {
                                    printWriter.print(' ');
                                    printWriter.println(i2);
                                    i2 = 0;
                                }
                                if (stackTraceElement != null) {
                                    printWriter.print(' ');
                                    printWriter.println(stackTraceElement.toString().trim());
                                    stackTraceElement = null;
                                }
                                printWriter.print(' ');
                                printWriter.println(stackTraceElement2.toString().trim());
                            } else {
                                if (stackTraceElement != null) {
                                    i2++;
                                }
                                stackTraceElement = stackTraceElement2;
                            }
                            z = startsWith;
                        }
                    }
                    i++;
                }
                if (stackTraceElement != null) {
                    i2++;
                }
                if (i2 > 0) {
                    printWriter.print(' ');
                    printWriter.println(i2);
                }
            }
        }
    }

    public static StackTraceElement a(Throwable th) {
        for (Throwable th2 = th; th2 != null; th2 = th2.getCause()) {
            StackTraceElement[] stackTrace = th.getStackTrace();
            if (stackTrace != null) {
                for (StackTraceElement stackTraceElement : stackTrace) {
                    if (stackTraceElement != null) {
                        String className = stackTraceElement.getClassName();
                        if (className != null && className.startsWith("com.startapp.")) {
                            return stackTraceElement;
                        }
                    }
                }
                continue;
            }
        }
        return null;
    }
}
