package com.startapp.sdk.adsbase.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class AdDetails implements Parcelable, Serializable {
    public static final Creator<AdDetails> CREATOR = new Creator<AdDetails>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new AdDetails[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new AdDetails(parcel);
        }
    };
    private static final long serialVersionUID = 1;
    private String adId;
    private boolean app;
    private String appPresencePackage;
    private boolean belowMinCPM;
    private String category;
    private String clickUrl;
    private String closeUrl;
    private Long delayImpressionInSeconds;
    private String description;
    private String imageUrl;
    private String installs;
    private String intentDetails;
    private String intentPackageName;
    private int minAppVersion;
    private String packageName;
    private float rating;
    private String secondaryImageUrl;
    private Boolean sendRedirectHops;
    private boolean smartRedirect;
    private boolean startappBrowserEnabled;
    private String template;
    private String title;
    private String trackingClickUrl;
    private String trackingUrl;
    private Long ttl;

    public int describeContents() {
        return 0;
    }

    public AdDetails() {
        this.rating = 5.0f;
        this.belowMinCPM = false;
    }

    public final String a() {
        return this.adId;
    }

    public final String b() {
        return this.closeUrl;
    }

    public final String c() {
        return this.clickUrl;
    }

    public final String d() {
        return this.trackingUrl;
    }

    public final String e() {
        return this.trackingClickUrl;
    }

    public final String f() {
        return this.title;
    }

    public final String g() {
        return this.description;
    }

    public final String h() {
        return this.imageUrl;
    }

    public final boolean i() {
        return this.belowMinCPM;
    }

    public final String j() {
        return this.secondaryImageUrl;
    }

    public final float k() {
        return this.rating;
    }

    public final boolean l() {
        return this.smartRedirect;
    }

    public final String m() {
        return this.template;
    }

    public final String n() {
        return this.packageName;
    }

    public final String o() {
        return this.appPresencePackage;
    }

    public final String p() {
        return this.intentDetails;
    }

    public final String q() {
        return this.intentPackageName;
    }

    public final boolean r() {
        return this.intentPackageName != null;
    }

    public final String s() {
        return this.installs;
    }

    public final String t() {
        return this.category;
    }

    public final boolean u() {
        return this.app;
    }

    public final int v() {
        return this.minAppVersion;
    }

    public final boolean w() {
        return this.startappBrowserEnabled;
    }

    public final Long x() {
        return this.ttl;
    }

    public final Long y() {
        return this.delayImpressionInSeconds;
    }

    public final Boolean z() {
        return this.sendRedirectHops;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AdDetails [adId=");
        sb.append(this.adId);
        sb.append(", clickUrl=");
        sb.append(this.clickUrl);
        sb.append(", trackingUrl=");
        sb.append(this.trackingUrl);
        sb.append(", trackingClickUrl=");
        sb.append(this.trackingClickUrl);
        sb.append(", closeUrl=");
        sb.append(this.closeUrl);
        sb.append(", title=");
        sb.append(this.title);
        sb.append(", description=");
        sb.append(this.description);
        sb.append(", imageUrl=");
        sb.append(this.imageUrl);
        sb.append(", secondaryImageUrl=");
        sb.append(this.secondaryImageUrl);
        sb.append(", rating=");
        sb.append(this.rating);
        sb.append(", smartRedirect=");
        sb.append(this.smartRedirect);
        sb.append(", template=");
        sb.append(this.template);
        sb.append(", packageName=");
        sb.append(this.packageName);
        sb.append(", appPresencePackage=");
        sb.append(this.appPresencePackage);
        sb.append(", intentDetails=");
        sb.append(this.intentDetails);
        sb.append(", intentPackageName=");
        sb.append(this.intentPackageName);
        sb.append(", minAppVersion=");
        sb.append(this.minAppVersion);
        sb.append(", startappBrowserEnabled=");
        sb.append(this.startappBrowserEnabled);
        sb.append(", ttl=");
        sb.append(this.ttl);
        sb.append(", app=");
        sb.append(this.app);
        sb.append(", belowMinCPM=");
        sb.append(this.belowMinCPM);
        sb.append(", installs=");
        sb.append(this.installs);
        sb.append(", category=");
        sb.append(this.category);
        sb.append(", delayImpressionInSeconds=");
        sb.append(this.delayImpressionInSeconds);
        sb.append(", sendRedirectHops=");
        sb.append(this.sendRedirectHops);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public AdDetails(Parcel parcel) {
        this.rating = 5.0f;
        boolean z = false;
        this.belowMinCPM = false;
        this.adId = parcel.readString();
        this.clickUrl = parcel.readString();
        this.trackingUrl = parcel.readString();
        this.trackingClickUrl = parcel.readString();
        this.closeUrl = parcel.readString();
        this.title = parcel.readString();
        this.description = parcel.readString();
        this.imageUrl = parcel.readString();
        this.secondaryImageUrl = parcel.readString();
        this.rating = parcel.readFloat();
        int readInt = parcel.readInt();
        int readInt2 = parcel.readInt();
        this.smartRedirect = false;
        if (readInt == 1) {
            this.smartRedirect = true;
        }
        this.startappBrowserEnabled = true;
        if (readInt2 == 0) {
            this.startappBrowserEnabled = false;
        }
        this.template = parcel.readString();
        this.packageName = parcel.readString();
        this.appPresencePackage = parcel.readString();
        this.intentPackageName = parcel.readString();
        this.intentDetails = parcel.readString();
        this.minAppVersion = parcel.readInt();
        this.installs = parcel.readString();
        this.category = parcel.readString();
        int readInt3 = parcel.readInt();
        this.app = false;
        if (readInt3 == 1) {
            this.app = true;
        }
        int readInt4 = parcel.readInt();
        this.belowMinCPM = false;
        if (readInt4 == 1) {
            this.belowMinCPM = true;
        }
        this.ttl = Long.valueOf(parcel.readLong());
        if (this.ttl.longValue() == -1) {
            this.ttl = null;
        }
        this.delayImpressionInSeconds = Long.valueOf(parcel.readLong());
        if (this.delayImpressionInSeconds.longValue() == -1) {
            this.delayImpressionInSeconds = null;
        }
        int readInt5 = parcel.readInt();
        if (readInt5 == 0) {
            this.sendRedirectHops = null;
            return;
        }
        if (readInt5 == 1) {
            z = true;
        }
        this.sendRedirectHops = Boolean.valueOf(z);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.adId);
        parcel.writeString(this.clickUrl);
        parcel.writeString(this.trackingUrl);
        parcel.writeString(this.trackingClickUrl);
        parcel.writeString(this.closeUrl);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeString(this.imageUrl);
        parcel.writeString(this.secondaryImageUrl);
        parcel.writeFloat(this.rating);
        boolean z = this.smartRedirect;
        boolean z2 = this.startappBrowserEnabled;
        parcel.writeInt(z ? 1 : 0);
        parcel.writeInt(z2 ? 1 : 0);
        parcel.writeString(this.template);
        parcel.writeString(this.packageName);
        parcel.writeString(this.appPresencePackage);
        parcel.writeString(this.intentPackageName);
        parcel.writeString(this.intentDetails);
        parcel.writeInt(this.minAppVersion);
        parcel.writeString(this.installs);
        parcel.writeString(this.category);
        parcel.writeInt(this.app ? 1 : 0);
        parcel.writeInt(this.belowMinCPM ? 1 : 0);
        if (this.ttl != null) {
            parcel.writeLong(this.ttl.longValue());
        } else {
            parcel.writeLong(-1);
        }
        if (this.delayImpressionInSeconds != null) {
            parcel.writeLong(this.delayImpressionInSeconds.longValue());
        } else {
            parcel.writeLong(-1);
        }
        int i2 = this.sendRedirectHops == null ? 0 : this.sendRedirectHops.booleanValue() ? 1 : -1;
        parcel.writeInt(i2);
    }
}
