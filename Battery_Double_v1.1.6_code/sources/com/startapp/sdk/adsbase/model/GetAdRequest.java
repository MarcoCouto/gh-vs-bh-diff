package com.startapp.sdk.adsbase.model;

import android.content.Context;
import android.util.Pair;
import com.appodeal.ads.as;
import com.facebook.appevents.UserDataStore;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.startapp.common.SDKException;
import com.startapp.common.b.a;
import com.startapp.sdk.adsbase.Ad.AdType;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.SDKAdPreferences.Gender;
import com.startapp.sdk.adsbase.adrules.b;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.i.j;
import com.startapp.sdk.adsbase.i.l;
import com.startapp.sdk.adsbase.i.o;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.k;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.Set;

/* compiled from: StartAppSDK */
public class GetAdRequest extends c {
    private int A;
    private String B;
    private String C;
    private String D;
    private boolean E;
    private Boolean F;
    private Boolean G;
    private String H = null;
    private String I = null;
    private AdType J = null;
    protected String b;
    private Placement c;
    private boolean d;
    private Integer e;
    private Long f;
    private Gender g;
    private String h;
    private String i;
    private int j = 1;
    private boolean k = true;
    private Boolean l;
    private boolean m = AdsCommonMetaData.a().E();
    private Double n;
    private String o;
    private Integer p;
    private boolean q = true;
    private int r = 0;
    private Set<String> s = null;
    private Set<String> t = null;
    private Set<String> u = null;
    private Set<String> v = null;
    private Set<String> w = null;
    private Pair<String, String> x;
    private boolean y = true;
    private long z = (System.currentTimeMillis() - o.d().b());

    /* compiled from: StartAppSDK */
    protected enum VideoRequestMode {
        INTERSTITIAL,
        REWARDED
    }

    /* compiled from: StartAppSDK */
    protected enum VideoRequestType {
        ENABLED,
        DISABLED,
        FORCED,
        FORCED_NONVAST
    }

    public GetAdRequest() {
        super(4);
        if (!s.a()) {
            this.A = b.a().c();
        }
        this.B = MetaData.D().y();
    }

    public void a(Context context) {
        this.b = com.startapp.sdk.b.c.a(context).h().a(this.c);
    }

    public final Placement g() {
        return this.c;
    }

    public final void h() {
        this.m = true;
    }

    public final void e(int i2) {
        this.j = i2;
    }

    public final void b(String str) {
        this.C = str;
    }

    public final void c(String str) {
        this.D = str;
    }

    public final void b(boolean z2) {
        this.E = z2;
    }

    public final void a(Set<String> set) {
        this.u = set;
    }

    public final boolean i() {
        return this.w != null && this.w.size() > 0;
    }

    public final void b(Set<String> set) {
        this.w = set;
    }

    public final void f(int i2) {
        this.r = i2;
    }

    public final void c(boolean z2) {
        this.y = z2;
    }

    public final void b(int i2) {
        this.a = Integer.valueOf(i2);
    }

    public String toString() {
        return super.toString();
    }

    public final void a(Integer num, Long l2) {
        this.e = num;
        this.f = l2;
    }

    public void a(Context context, AdPreferences adPreferences, Placement placement, Pair<String, String> pair) {
        this.c = placement;
        this.x = pair;
        this.F = adPreferences.getAi();
        this.G = adPreferences.getAs();
        this.g = adPreferences.getGender(context);
        this.h = adPreferences.getKeywords();
        this.d = adPreferences.isTestMode();
        this.s = adPreferences.getCategories();
        this.t = adPreferences.getCategoriesExclude();
        this.k = adPreferences.b();
        this.p = adPreferences.a();
        this.l = Boolean.valueOf(com.startapp.common.b.b.b(context));
        this.n = adPreferences.getMinCpm();
        this.o = adPreferences.getAdTag();
        this.q = !MetaData.b(context);
        this.H = adPreferences.country;
        this.I = adPreferences.advertiserId;
        this.i = adPreferences.template;
        this.J = adPreferences.type;
        this.v = adPreferences.packageInclude;
    }

    public l a() throws SDKException {
        l a = super.a();
        if (a == null) {
            a = new j();
        }
        a.a(IronSourceConstants.EVENTS_PLACEMENT_NAME, this.c.name(), true);
        a.a("testMode", Boolean.toString(this.d), false);
        a.a("gender", this.g, false);
        a.a("keywords", this.h, false);
        a.a(MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE, this.i, false);
        a.a("adsNumber", Integer.toString(this.j), false);
        a.a("category", this.s);
        a.a("categoryExclude", this.t);
        a.a("packageExclude", this.u);
        a.a("campaignExclude", this.w);
        a.a("offset", Integer.toString(this.r), false);
        a.a("ai", this.F, false);
        a.a(as.a, this.G, false);
        a.a("minCPM", s.a(this.n), false);
        a.a("adTag", this.o, false);
        a.a("previousAdId", this.b, false);
        a.a("twoClicks", Boolean.valueOf(!this.m), false);
        a.a("engInclude", Boolean.toString(this.y), false);
        if (this.J == AdType.INTERSTITIAL || this.J == AdType.RICH_TEXT) {
            a.a("type", this.J, false);
        }
        a.a("timeSinceSessionStart", Long.valueOf(this.z), true);
        a.a("adsDisplayed", Integer.valueOf(this.A), true);
        a.a("profileId", this.B, false);
        a.a("hardwareAccelerated", Boolean.valueOf(this.k), false);
        a.a("autoLoadAmount", this.p, false);
        a.a("dts", this.l, false);
        a.a("downloadingMode", "CACHE", false);
        a.a("primaryImg", this.C, false);
        a.a("moreImg", this.D, false);
        a.a("contentAd", Boolean.toString(this.E), false);
        a.a(UserDataStore.CITY, this.e, false);
        a.a("tsc", this.f, false);
        a.a("testAdsEnabled", k.a().o() ? Boolean.TRUE : null, false);
        String d2 = a.d();
        a.a(a.a(), d2, true);
        String c2 = a.c();
        StringBuilder sb = new StringBuilder();
        sb.append(c());
        sb.append(this.c.name());
        sb.append(e());
        sb.append(d());
        sb.append(d2);
        a.a(c2, a.b(sb.toString()), true, false);
        if (this.H != null) {
            a.a(UserDataStore.COUNTRY, this.H, false);
        }
        if (this.I != null) {
            a.a("advertiserId", this.I, false);
        }
        if (this.v != null) {
            a.a("packageInclude", this.v);
        }
        a.a("defaultMetaData", Boolean.valueOf(this.q), true);
        a.a((String) this.x.first, this.x.second, false);
        return a;
    }

    public final AdType k() {
        return this.J;
    }

    public final boolean j() {
        return this.J == AdType.VIDEO || this.J == AdType.REWARDED_VIDEO;
    }
}
