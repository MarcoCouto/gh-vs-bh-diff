package com.startapp.sdk.adsbase;

import android.content.SharedPreferences;

/* compiled from: StartAppSDK */
public final class b {
    private final Object a = new Object();
    private final SharedPreferences b;
    private volatile String c;
    private volatile String d;

    public b(SharedPreferences sharedPreferences) {
        this.b = sharedPreferences;
    }

    public final void a(String str, String str2) {
        if (str != null) {
            str = str.trim();
        }
        if (str2 != null) {
            str2 = str2.trim();
        }
        synchronized (this.a) {
            this.c = str;
            this.d = str2;
            this.b.edit().putString("c88d4eab540fab77", str).putString("2696a7f502faed4b", str2).commit();
        }
    }

    public final String a() {
        String str = this.c;
        if (str == null) {
            synchronized (this.a) {
                str = this.c;
                if (str == null) {
                    str = this.b.getString("c88d4eab540fab77", null);
                }
            }
        }
        return str;
    }

    public final String b() {
        String str = this.d;
        if (str == null) {
            synchronized (this.a) {
                str = this.d;
                if (str == null) {
                    str = this.b.getString("2696a7f502faed4b", null);
                }
            }
        }
        return str;
    }
}
