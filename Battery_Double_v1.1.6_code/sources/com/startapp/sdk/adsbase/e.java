package com.startapp.sdk.adsbase;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.sdk.adsbase.StartAppAd.AdMode;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;

/* compiled from: StartAppSDK */
public final class e {
    protected StartAppAd a;
    private boolean b;
    private AutoInterstitialPreferences c;
    private long d;
    private int e;

    /* compiled from: StartAppSDK */
    static class a {
        /* access modifiers changed from: private */
        public static final e a = new e(0);
    }

    /* synthetic */ e(byte b2) {
        this();
    }

    private e() {
        this.b = false;
        this.c = null;
        this.d = -1;
        this.e = -1;
        this.a = null;
    }

    public final void a() {
        this.b = true;
    }

    public final void b() {
        this.b = false;
    }

    public final void a(AutoInterstitialPreferences autoInterstitialPreferences) {
        this.c = autoInterstitialPreferences;
        this.d = -1;
        this.e = -1;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.d = System.currentTimeMillis();
        this.e = 0;
    }

    public final void a(Activity activity, Bundle bundle) {
        boolean z;
        boolean equals = activity.getClass().getName().equals(s.b((Context) activity));
        if (bundle == null) {
            String str = "com.startapp.sdk.";
            String name = activity.getClass().getName();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("adsbase.activities.OverlayActivity");
            boolean z2 = false;
            if (!name.startsWith(sb.toString())) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append("adsbase.activities.FullScreenActivity");
                if (!name.startsWith(sb2.toString())) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append("ads.list3d.List3DActivity");
                    if (!name.startsWith(sb3.toString())) {
                        z = false;
                        if (!z && !equals) {
                            this.e++;
                            if (this.b && AdsCommonMetaData.a().J()) {
                                if (this.c == null) {
                                    this.c = new AutoInterstitialPreferences();
                                }
                                boolean z3 = this.d <= 0 || System.currentTimeMillis() >= this.d + ((long) (this.c.getSecondsBetweenAds() * 1000));
                                boolean z4 = this.e <= 0 || this.e >= this.c.getActivitiesBetweenAds();
                                if (z3 && z4) {
                                    z2 = true;
                                }
                                if (z2) {
                                    if (this.a == null) {
                                        this.a = new StartAppAd(activity);
                                    }
                                    this.a.loadAd(AdMode.AUTOMATIC, new AdPreferences().setAi(Boolean.TRUE), new AdEventListener() {
                                        public final void onReceiveAd(Ad ad) {
                                            if (e.this.a.showAd()) {
                                                e.this.c();
                                            }
                                        }

                                        public final void onFailedToReceiveAd(Ad ad) {
                                            StringBuilder sb = new StringBuilder("FailedToShowInterActivityAd, error: [");
                                            sb.append(ad.errorMessage);
                                            sb.append(RequestParameters.RIGHT_BRACKETS);
                                        }
                                    });
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                    }
                }
            }
            z = true;
            if (!z) {
            }
        }
    }
}
