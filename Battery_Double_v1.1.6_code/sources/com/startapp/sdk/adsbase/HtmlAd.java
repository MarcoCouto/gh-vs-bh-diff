package com.startapp.sdk.adsbase;

import android.content.Context;
import android.text.TextUtils;
import com.iab.omid.library.startapp.b;
import com.startapp.sdk.ads.splash.SplashConfig.Orientation;
import com.startapp.sdk.adsbase.adinformation.AdInformationPositions.Position;
import com.startapp.sdk.adsbase.apppresence.AppPresenceDetails;
import com.startapp.sdk.adsbase.cache.a;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public abstract class HtmlAd extends Ad {
    private static String b = null;
    private static final long serialVersionUID = 1;
    private List<AppPresenceDetails> apps;
    private String[] closingUrl = {""};
    private Long consentTimeStamp;
    private Integer consentType;
    private Long delayImpressionInSeconds;
    private int height;
    private String htmlUuid = "";
    public boolean[] inAppBrowserEnabled = {true};
    private boolean isMraidAd = false;
    private int orientation = 0;
    private String[] packageNames = {""};
    private int rewardDuration = 0;
    private boolean rewardedHideTimer = false;
    private Boolean[] sendRedirectHops = null;
    public boolean[] smartRedirect = {false};
    private String[] trackingClickUrls = {""};
    public String[] trackingUrls = {""};
    private int width;

    static {
        HtmlAd.class.getSimpleName();
    }

    public Integer getConsentType() {
        return this.consentType;
    }

    public void setConsentType(Integer num) {
        this.consentType = num;
    }

    public Long getConsentTimestamp() {
        return this.consentTimeStamp;
    }

    public void setConsentTimestamp(Long l) {
        this.consentTimeStamp = l;
    }

    public final String j() {
        return a.a().a(this.htmlUuid);
    }

    public final String k() {
        return this.htmlUuid;
    }

    public final void b(int i) {
        this.width = i;
    }

    public final int l() {
        return this.width;
    }

    public final void c(int i) {
        this.height = i;
    }

    public final String[] m() {
        return this.closingUrl;
    }

    public final int n() {
        return this.height;
    }

    public final int o() {
        return this.rewardDuration;
    }

    public final boolean p() {
        return this.rewardedHideTimer;
    }

    public HtmlAd(Context context, Placement placement) {
        super(context, placement);
        if (b == null) {
            b = s.f(getContext());
        }
    }

    public String getAdId() {
        return s.a(j(), "@adId@", "@adId@");
    }

    private String d(String str) {
        try {
            return b.a(com.startapp.sdk.omsdk.b.a(), str);
        } catch (Throwable th) {
            new e(th).a(this.a);
            return str;
        }
    }

    public void b(String str) {
        if (com.startapp.sdk.adsbase.mraid.c.a.b(str)) {
            str = com.startapp.sdk.adsbase.mraid.c.a.a(str);
            this.isMraidAd = true;
        }
        if (MetaData.D().P()) {
            str = d(str);
        }
        if (s.a(512)) {
            this.htmlUuid = a.a().a(str, UUID.randomUUID().toString());
        }
        String str2 = "@smartRedirect@";
        String a = s.a(str, str2, str2);
        if (a != null) {
            String[] split = a.split(",");
            this.smartRedirect = new boolean[split.length];
            for (int i = 0; i < split.length; i++) {
                if (split[i].compareTo("true") == 0) {
                    this.smartRedirect[i] = true;
                } else {
                    this.smartRedirect[i] = false;
                }
            }
        }
        String str3 = "@trackingClickUrl@";
        String a2 = s.a(str, str3, str3);
        if (a2 != null) {
            this.trackingClickUrls = a2.split(",");
        }
        String str4 = "@closeUrl@";
        String a3 = s.a(str, str4, str4);
        if (a3 != null) {
            this.closingUrl = a3.split(",");
        }
        String str5 = "@tracking@";
        String a4 = s.a(str, str5, str5);
        if (a4 != null) {
            this.trackingUrls = a4.split(",");
        }
        String str6 = "@packageName@";
        String a5 = s.a(str, str6, str6);
        if (a5 != null) {
            this.packageNames = a5.split(",");
        }
        String str7 = "@startappBrowserEnabled@";
        String a6 = s.a(str, str7, str7);
        if (a6 != null) {
            String[] split2 = a6.split(",");
            this.inAppBrowserEnabled = new boolean[split2.length];
            for (int i2 = 0; i2 < split2.length; i2++) {
                if (split2[i2].compareTo("false") == 0) {
                    this.inAppBrowserEnabled[i2] = false;
                } else {
                    this.inAppBrowserEnabled[i2] = true;
                }
            }
        }
        String str8 = "@orientation@";
        String a7 = s.a(str, str8, str8);
        if (a7 != null && s.a(8)) {
            a(Orientation.getByName(a7));
        }
        String str9 = "@adInfoEnable@";
        String a8 = s.a(str, str9, str9);
        if (a8 != null) {
            getAdInfoOverride().a(Boolean.parseBoolean(a8));
        }
        String str10 = "@adInfoPosition@";
        String a9 = s.a(str, str10, str10);
        if (a9 != null) {
            getAdInfoOverride().a(Position.getByName(a9));
        }
        String str11 = "@ttl@";
        String a10 = s.a(str, str11, str11);
        if (a10 != null) {
            c(a10);
        }
        String str12 = "@belowMinCPM@";
        String a11 = s.a(str, str12, str12);
        if (a11 != null) {
            if (Arrays.asList(a11.split(",")).contains("false")) {
                this.belowMinCPM = false;
            } else {
                this.belowMinCPM = true;
            }
        }
        String str13 = "@delayImpressionInSeconds@";
        String a12 = s.a(str, str13, str13);
        if (!(a12 == null || a12 == null || a12.equals(""))) {
            try {
                this.delayImpressionInSeconds = Long.valueOf(Long.parseLong(a12));
            } catch (NumberFormatException unused) {
            }
        }
        String str14 = "@rewardDuration@";
        String a13 = s.a(str, str14, str14);
        if (a13 != null) {
            try {
                this.rewardDuration = Integer.parseInt(a13);
            } catch (Throwable th) {
                new e(th).a(this.a);
            }
        }
        String str15 = "@rewardedHideTimer@";
        String a14 = s.a(str, str15, str15);
        if (a14 != null) {
            try {
                this.rewardedHideTimer = Boolean.parseBoolean(a14);
            } catch (Throwable th2) {
                new e(th2).a(this.a);
            }
        }
        String str16 = "@sendRedirectHops@";
        String a15 = s.a(str, str16, str16);
        if (!(a15 == null || a15 == null || a15.equals(""))) {
            String[] split3 = a15.split(",");
            this.sendRedirectHops = new Boolean[split3.length];
            for (int i3 = 0; i3 < split3.length; i3++) {
                if (split3[i3].compareTo("true") == 0) {
                    this.sendRedirectHops[i3] = Boolean.TRUE;
                } else if (split3[i3].compareTo("false") == 0) {
                    this.sendRedirectHops[i3] = Boolean.FALSE;
                } else {
                    this.sendRedirectHops[i3] = null;
                }
            }
        }
        String str17 = "@ct@";
        String a16 = s.a(str, str17, str17);
        String str18 = "@tsc@";
        String a17 = s.a(str, str18, str18);
        if (!TextUtils.isEmpty(a16) && !TextUtils.isEmpty(a17)) {
            try {
                this.consentType = Integer.valueOf(Integer.parseInt(a16));
                this.consentTimeStamp = Long.valueOf(Long.parseLong(a17));
            } catch (Throwable th3) {
                new e(th3).a(this.a);
            }
        }
        if (this.smartRedirect.length < this.trackingUrls.length) {
            boolean[] zArr = new boolean[this.trackingUrls.length];
            int i4 = 0;
            while (i4 < this.smartRedirect.length) {
                zArr[i4] = this.smartRedirect[i4];
                i4++;
            }
            while (i4 < this.trackingUrls.length) {
                zArr[i4] = false;
                i4++;
            }
            this.smartRedirect = zArr;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Orientation orientation2) {
        this.orientation = 0;
        boolean a = s.a(8);
        if (orientation2 != null) {
            if (a && orientation2.equals(Orientation.PORTRAIT)) {
                this.orientation = 1;
            } else if (a && orientation2.equals(Orientation.LANDSCAPE)) {
                this.orientation = 2;
            }
        }
    }

    public final boolean d(int i) {
        if (i < 0 || i >= this.smartRedirect.length) {
            return false;
        }
        return this.smartRedirect[i];
    }

    public final boolean e(int i) {
        if (this.inAppBrowserEnabled == null || i < 0 || i >= this.inAppBrowserEnabled.length) {
            return true;
        }
        return this.inAppBrowserEnabled[i];
    }

    public final String[] q() {
        return this.trackingClickUrls;
    }

    public final int r() {
        return this.orientation;
    }

    public final String[] s() {
        return this.packageNames;
    }

    public final void a(List<AppPresenceDetails> list) {
        this.apps = list;
    }

    public final void c(String str) {
        String[] split;
        Long l = null;
        for (String str2 : str.split(",")) {
            if (!str2.equals("")) {
                try {
                    long parseLong = Long.parseLong(str2);
                    if (parseLong > 0 && (l == null || parseLong < l.longValue())) {
                        l = Long.valueOf(parseLong);
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        if (l != null) {
            this.adCacheTtl = Long.valueOf(TimeUnit.SECONDS.toMillis(l.longValue()));
        }
    }

    public final Long t() {
        return this.delayImpressionInSeconds;
    }

    public final Boolean[] u() {
        return this.sendRedirectHops;
    }

    public final Boolean f(int i) {
        if (this.sendRedirectHops == null || i < 0 || i >= this.sendRedirectHops.length) {
            return null;
        }
        return this.sendRedirectHops[i];
    }

    public final boolean v() {
        return this.isMraidAd;
    }
}
