package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.adinformation.AdInformationPositions.Position;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: StartAppSDK */
public class AdInformationConfig implements Serializable {
    private static final long serialVersionUID = 1;
    @d(b = ArrayList.class, c = ImageResourceConfig.class)
    private List<ImageResourceConfig> ImageResources = new ArrayList();
    @d(b = HashMap.class, c = Position.class, d = Placement.class)
    protected HashMap<Placement, Position> Positions = new HashMap<>();
    private transient EnumMap<ImageResourceType, ImageResourceConfig> a = new EnumMap<>(ImageResourceType.class);
    private String dialogUrlSecured = "https://d1byvlfiet2h9q.cloudfront.net/InApp/resources/adInformationDialog3.html";
    private boolean enabled = true;
    private String eulaUrlSecured = "https://www.com.startapp.com/policy/sdk-policy/";
    private float fatFingersFactor = 200.0f;

    /* compiled from: StartAppSDK */
    public enum ImageResourceType {
        INFO_S(17, 14),
        INFO_EX_S(88, 14),
        INFO_L(25, 21),
        INFO_EX_L(TsExtractor.TS_STREAM_TYPE_HDMV_DTS, 21);
        
        private int height;
        private int width;

        private ImageResourceType(int i, int i2) {
            this.width = i;
            this.height = i2;
        }

        public final int getDefaultWidth() {
            return this.width;
        }

        public final int getDefaultHeight() {
            return this.height;
        }

        public static ImageResourceType getByName(String str) {
            ImageResourceType imageResourceType = INFO_S;
            ImageResourceType[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].name().toLowerCase().compareTo(str.toLowerCase()) == 0) {
                    imageResourceType = values[i];
                }
            }
            return imageResourceType;
        }
    }

    private AdInformationConfig() {
    }

    public static AdInformationConfig a() {
        AdInformationConfig adInformationConfig = new AdInformationConfig();
        a(adInformationConfig);
        return adInformationConfig;
    }

    public static void a(AdInformationConfig adInformationConfig) {
        adInformationConfig.i();
        adInformationConfig.h();
    }

    public final String b() {
        return (this.eulaUrlSecured == null || this.eulaUrlSecured.equals("")) ? "https://www.com.startapp.com/policy/sdk-policy/" : this.eulaUrlSecured;
    }

    public final String c() {
        return (!this.a.containsKey(ImageResourceType.INFO_L) || ((ImageResourceConfig) this.a.get(ImageResourceType.INFO_L)).d().equals("")) ? "https://info.startappservice.com/InApp/resources/info_l.png" : ((ImageResourceConfig) this.a.get(ImageResourceType.INFO_L)).d();
    }

    public final boolean a(Context context) {
        return !j.a(context, "userDisabledAdInformation", Boolean.FALSE).booleanValue() && this.enabled;
    }

    public static void b(Context context) {
        j.b(context, "userDisabledAdInformation", Boolean.FALSE);
    }

    public final float d() {
        return this.fatFingersFactor / 100.0f;
    }

    public final String e() {
        return this.dialogUrlSecured != null ? this.dialogUrlSecured : "https://d1byvlfiet2h9q.cloudfront.net/InApp/resources/adInformationDialog3.html";
    }

    public final Position a(Placement placement) {
        Position position = (Position) this.Positions.get(placement);
        if (position != null) {
            return position;
        }
        Position position2 = Position.BOTTOM_LEFT;
        this.Positions.put(placement, position2);
        return position2;
    }

    public final void f() {
        for (ImageResourceConfig imageResourceConfig : this.ImageResources) {
            this.a.put(ImageResourceType.getByName(imageResourceConfig.a()), imageResourceConfig);
            imageResourceConfig.e();
        }
    }

    private void h() {
        ImageResourceType[] values = ImageResourceType.values();
        int length = values.length;
        int i = 0;
        while (i < length) {
            ImageResourceType imageResourceType = values[i];
            if (this.a.get(imageResourceType) != null) {
                i++;
            } else {
                StringBuilder sb = new StringBuilder("AdInformation error in ImageResource [");
                sb.append(imageResourceType);
                sb.append("] cannot be found in MetaData");
                throw new IllegalArgumentException(sb.toString());
            }
        }
    }

    private void i() {
        ImageResourceType[] values;
        for (ImageResourceType imageResourceType : ImageResourceType.values()) {
            ImageResourceConfig imageResourceConfig = (ImageResourceConfig) this.a.get(imageResourceType);
            Boolean bool = Boolean.TRUE;
            if (imageResourceConfig == null) {
                imageResourceConfig = ImageResourceConfig.b(imageResourceType.name());
                Iterator it = this.ImageResources.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (ImageResourceType.getByName(((ImageResourceConfig) it.next()).a()).equals(imageResourceType)) {
                            bool = Boolean.FALSE;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                this.a.put(imageResourceType, imageResourceConfig);
                if (bool.booleanValue()) {
                    this.ImageResources.add(imageResourceConfig);
                }
            }
            imageResourceConfig.a(imageResourceType.getDefaultWidth());
            imageResourceConfig.b(imageResourceType.getDefaultHeight());
            StringBuilder sb = new StringBuilder();
            sb.append(imageResourceType.name().toLowerCase());
            sb.append(".png");
            imageResourceConfig.a(sb.toString());
        }
    }

    public final void g() {
        this.a = new EnumMap<>(ImageResourceType.class);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdInformationConfig adInformationConfig = (AdInformationConfig) obj;
        return this.enabled == adInformationConfig.enabled && Float.compare(adInformationConfig.fatFingersFactor, this.fatFingersFactor) == 0 && s.b(this.dialogUrlSecured, adInformationConfig.dialogUrlSecured) && s.b(this.eulaUrlSecured, adInformationConfig.eulaUrlSecured) && s.b(this.Positions, adInformationConfig.Positions) && s.b(this.ImageResources, adInformationConfig.ImageResources);
    }

    public int hashCode() {
        return s.a(Boolean.valueOf(this.enabled), Float.valueOf(this.fatFingersFactor), this.dialogUrlSecured, this.eulaUrlSecured, this.Positions, this.ImageResources);
    }

    public final ImageResourceConfig a(ImageResourceType imageResourceType) {
        return (ImageResourceConfig) this.a.get(imageResourceType);
    }
}
