package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject.Size;
import com.startapp.sdk.adsbase.adinformation.AdInformationPositions.Position;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.tapjoy.TJAdUnitConstants.String;

/* compiled from: StartAppSDK */
public class AdInformationView extends RelativeLayout {
    private ImageView a;
    private RelativeLayout b;
    private OnClickListener c = null;
    private AdInformationConfig d;
    private ImageResourceConfig e;
    private Placement f;
    private Position g;

    public AdInformationView(Context context, Size size, Placement placement, AdInformationOverrides adInformationOverrides, final OnClickListener onClickListener) {
        super(context);
        this.f = placement;
        this.c = new OnClickListener() {
            public final void onClick(View view) {
                onClickListener.onClick(view);
            }
        };
        getContext();
        this.d = AdInformationObject.c();
        if (this.d == null) {
            this.d = AdInformationConfig.a();
        }
        this.e = this.d.a(size.a());
        if (adInformationOverrides == null || !adInformationOverrides.d()) {
            this.g = this.d.a(this.f);
        } else {
            this.g = adInformationOverrides.c();
        }
        this.a = new ImageView(getContext());
        this.a.setContentDescription(String.VIDEO_INFO);
        this.a.setId(1475346433);
        this.a.setImageBitmap(this.e.a(getContext()));
        this.b = new RelativeLayout(getContext());
        LayoutParams layoutParams = new LayoutParams(r.a(getContext(), (int) (((float) this.e.b()) * this.d.d())), r.a(getContext(), (int) (((float) this.e.c()) * this.d.d())));
        this.b.setBackgroundColor(0);
        LayoutParams layoutParams2 = new LayoutParams(r.a(getContext(), this.e.b()), r.a(getContext(), this.e.c()));
        layoutParams2.setMargins(0, 0, 0, 0);
        this.a.setPadding(0, 0, 0, 0);
        this.g.addRules(layoutParams2);
        this.b.addView(this.a, layoutParams2);
        this.b.setOnClickListener(this.c);
        addView(this.b, layoutParams);
    }
}
