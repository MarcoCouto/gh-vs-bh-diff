package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import android.webkit.JavascriptInterface;
import com.startapp.sdk.b.c;

/* compiled from: StartAppSDK */
public final class a {
    private Context a = null;
    private boolean b = false;
    private Runnable c = null;
    private Runnable d = null;
    private Runnable e = null;

    public a(Context context, Runnable runnable, Runnable runnable2, Runnable runnable3) {
        this.a = context;
        this.d = runnable;
        this.c = runnable2;
        this.e = runnable3;
    }

    @JavascriptInterface
    public final void decline() {
        if (!this.b) {
            this.b = true;
            this.c.run();
        }
    }

    @JavascriptInterface
    public final void accept() {
        if (!this.b) {
            this.b = true;
            this.d.run();
        }
    }

    @JavascriptInterface
    public final void fullPrivacyPolicy() {
        if (!this.b) {
            this.b = true;
            this.e.run();
        }
    }

    @JavascriptInterface
    public final String getAppId() {
        if (this.a == null) {
            return null;
        }
        try {
            String b2 = c.a(this.a).k().b();
            if (b2 == null) {
                return null;
            }
            return String.valueOf(Long.parseLong(b2) ^ 121212121);
        } catch (Exception unused) {
            return null;
        }
    }
}
