package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class AdInformationMetaData implements Serializable {
    private static volatile AdInformationMetaData a = new AdInformationMetaData();
    private static Object b = new Object();
    private static final long serialVersionUID = 1;
    @d(a = true)
    private AdInformationConfig AdInformation = AdInformationConfig.a();
    private String adInformationMetadataUpdateVersion = AdsConstants.d;

    public AdInformationMetaData() {
        this.AdInformation.f();
    }

    public final AdInformationConfig a() {
        return this.AdInformation;
    }

    public static void a(Context context) {
        AdInformationMetaData adInformationMetaData = (AdInformationMetaData) com.startapp.common.b.d.a(context, "StartappAdInfoMetadata");
        AdInformationMetaData adInformationMetaData2 = new AdInformationMetaData();
        if (adInformationMetaData != null) {
            boolean a2 = s.a(adInformationMetaData, adInformationMetaData2);
            if (!(!AdsConstants.d.equals(adInformationMetaData.adInformationMetadataUpdateVersion)) && a2) {
                new e(InfoEventCategory.ERROR).e("metadata_null").a(context);
            }
            adInformationMetaData.AdInformation.g();
            a = adInformationMetaData;
        } else {
            a = adInformationMetaData2;
        }
        a.AdInformation.f();
    }

    public static AdInformationMetaData b() {
        return a;
    }

    public static void a(Context context, AdInformationMetaData adInformationMetaData) {
        synchronized (b) {
            adInformationMetaData.adInformationMetadataUpdateVersion = AdsConstants.d;
            a = adInformationMetaData;
            AdInformationConfig.a(adInformationMetaData.AdInformation);
            a.AdInformation.f();
            com.startapp.common.b.d.a(context, "StartappAdInfoMetadata", (Serializable) adInformationMetaData);
        }
    }

    public final String c() {
        return this.AdInformation.b();
    }

    public final String d() {
        return this.AdInformation.c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdInformationMetaData adInformationMetaData = (AdInformationMetaData) obj;
        return s.b(this.AdInformation, adInformationMetaData.AdInformation) && s.b(this.adInformationMetadataUpdateVersion, adInformationMetaData.adInformationMetadataUpdateVersion);
    }

    public int hashCode() {
        return s.a(this.AdInformation, this.adInformationMetadataUpdateVersion);
    }
}
