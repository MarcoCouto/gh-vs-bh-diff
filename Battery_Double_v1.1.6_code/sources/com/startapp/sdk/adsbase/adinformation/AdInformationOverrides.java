package com.startapp.sdk.adsbase.adinformation;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.adinformation.AdInformationPositions.Position;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class AdInformationOverrides implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean enable = true;
    private boolean enableOverride = false;
    @d(b = Position.class)
    private Position position = Position.getByName(AdInformationPositions.a);
    private boolean positionOverride = false;

    private AdInformationOverrides() {
    }

    public static AdInformationOverrides a() {
        return new AdInformationOverrides();
    }

    public final boolean b() {
        return this.enable;
    }

    public final void a(boolean z) {
        this.enable = z;
        this.enableOverride = true;
    }

    public final Position c() {
        return this.position;
    }

    public final void a(Position position2) {
        this.position = position2;
        if (position2 != null) {
            this.positionOverride = true;
        } else {
            this.positionOverride = false;
        }
    }

    public final boolean d() {
        return this.positionOverride;
    }

    public final boolean e() {
        return this.enableOverride;
    }
}
