package com.startapp.sdk.adsbase.adinformation;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.adinformation.AdInformationConfig.ImageResourceType;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.SimpleTokenConfig;

/* compiled from: StartAppSDK */
public class AdInformationObject implements OnClickListener {
    Context a;
    RelativeLayout b;
    RelativeLayout c;
    private AdInformationView d;
    private WebView e;
    private Dialog f = null;
    private Placement g;
    private final Handler h = new Handler(Looper.getMainLooper());
    private DisplayMode i = DisplayMode.REGULAR;
    private boolean j = false;
    private AdInformationOverrides k;
    private AdInformationConfig l;
    private SimpleTokenConfig m;
    private Runnable n = new Runnable() {
        public final void run() {
            try {
                AdInformationObject.this.e();
                SimpleTokenConfig.a(AdInformationObject.this.a, true);
                AdInformationConfig.b(AdInformationObject.this.a);
                AdInformationObject.this.a(false);
            } catch (Throwable th) {
                new e(th).a(AdInformationObject.this.a);
            }
        }
    };
    private Runnable o = new Runnable() {
        public final void run() {
            try {
                AdInformationObject.this.e();
                SimpleTokenUtils.b();
                SimpleTokenConfig.a(AdInformationObject.this.a, false);
                AdInformationConfig.b(AdInformationObject.this.a);
                AdInformationObject.this.a(false);
            } catch (Throwable th) {
                new e(th).a(AdInformationObject.this.a);
            }
        }
    };
    private Runnable p = new Runnable() {
        public final void run() {
            try {
                AdInformationObject.this.e();
                AdInformationObject.this.d();
                AdInformationObject.this.a(false);
            } catch (Throwable th) {
                new e(th).a(AdInformationObject.this.a);
            }
        }
    };

    /* compiled from: StartAppSDK */
    public enum DisplayMode {
        REGULAR,
        LAYOUT
    }

    /* compiled from: StartAppSDK */
    public enum Size {
        SMALL(ImageResourceType.INFO_S, ImageResourceType.INFO_EX_S),
        LARGE(ImageResourceType.INFO_L, ImageResourceType.INFO_EX_L);
        
        private ImageResourceType infoExtendedType;
        private ImageResourceType infoType;

        private Size(ImageResourceType imageResourceType, ImageResourceType imageResourceType2) {
            this.infoType = imageResourceType;
            this.infoExtendedType = imageResourceType2;
        }

        public final ImageResourceType a() {
            return this.infoType;
        }
    }

    static {
        AdInformationObject.class.getSimpleName();
    }

    public AdInformationObject(Context context, Size size, Placement placement, AdInformationOverrides adInformationOverrides) {
        this.a = context;
        this.g = placement;
        this.k = adInformationOverrides;
        this.l = AdInformationMetaData.b().a();
        this.m = MetaData.D().d();
        AdInformationView adInformationView = new AdInformationView(context, size, placement, adInformationOverrides, this);
        this.d = adInformationView;
    }

    public final View a() {
        return this.d;
    }

    public final boolean b() {
        return this.j;
    }

    public static AdInformationConfig c() {
        return AdInformationMetaData.b().a();
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        Placement placement = this.g;
        if (!(placement == Placement.INAPP_FULL_SCREEN || placement == Placement.INAPP_OFFER_WALL || placement == Placement.INAPP_SPLASH || placement == Placement.INAPP_OVERLAY) && (this.a instanceof Activity)) {
            s.a((Activity) this.a, z);
        }
    }

    public void onClick(View view) {
        if (!this.m.a(this.a) || !(this.a instanceof Activity) || ((Activity) this.a).isFinishing()) {
            d();
            return;
        }
        a(true);
        this.b = new RelativeLayout(this.a);
        try {
            this.e = new WebView(this.a);
            this.e.setWebViewClient(new WebViewClient());
            this.e.setWebChromeClient(new WebChromeClient());
            this.e.getSettings().setJavaScriptEnabled(true);
            this.e.setHorizontalScrollBarEnabled(false);
            this.e.setVerticalScrollBarEnabled(false);
            WebView webView = this.e;
            StringBuilder sb = new StringBuilder(this.l.e());
            if (j.a(this.a, "shared_prefs_using_location", Boolean.FALSE).booleanValue()) {
                sb.append("?le=true");
            }
            webView.loadUrl(sb.toString());
            this.e.addJavascriptInterface(new a(this.a, this.n, this.o, this.p), "startappwall");
            Point point = new Point(1, 1);
            try {
                WindowManager windowManager = (WindowManager) this.a.getSystemService("window");
                if (VERSION.SDK_INT >= 13) {
                    windowManager.getDefaultDisplay().getSize(point);
                } else {
                    point.x = windowManager.getDefaultDisplay().getWidth();
                    point.y = windowManager.getDefaultDisplay().getHeight();
                }
                LayoutParams layoutParams = new LayoutParams(-1, -1);
                layoutParams.addRule(13);
                this.e.setPadding(0, 0, 0, 0);
                layoutParams.setMargins(0, 0, 0, 0);
                this.b.addView(this.e, layoutParams);
                String a2 = a.a(this.a, (String) null);
                if (a2 != null) {
                    WebView webView2 = this.e;
                    StringBuilder sb2 = new StringBuilder("javascript:window.onload=function(){document.getElementById('titlePlacement').innerText='");
                    sb2.append(a2);
                    sb2.append("';}");
                    webView2.loadUrl(sb2.toString());
                }
                switch (this.i) {
                    case LAYOUT:
                        final RelativeLayout relativeLayout = this.b;
                        this.j = true;
                        final LayoutParams layoutParams2 = new LayoutParams((int) (((float) point.x) * 0.9f), (int) (((float) point.y) * 0.85f));
                        layoutParams2.addRule(13);
                        this.h.post(new Runnable() {
                            public final void run() {
                                AdInformationObject.this.c.addView(relativeLayout, layoutParams2);
                            }
                        });
                        return;
                    case REGULAR:
                        RelativeLayout relativeLayout2 = this.b;
                        this.j = true;
                        this.f = new Dialog(this.a);
                        this.f.requestWindowFeature(1);
                        this.f.setContentView(relativeLayout2);
                        WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams();
                        layoutParams3.copyFrom(this.f.getWindow().getAttributes());
                        layoutParams3.width = (int) (((float) point.x) * 0.9f);
                        layoutParams3.height = (int) (((float) point.y) * 0.85f);
                        this.f.show();
                        this.f.getWindow().setAttributes(layoutParams3);
                        break;
                }
            } catch (Throwable th) {
                new e(th).a(this.a);
                a(false);
            }
        } catch (Throwable th2) {
            new e(th2).a(this.a);
            a(false);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        if (!s.a(256) || !MetaData.D().x()) {
            a.c(this.a, this.l.b());
        } else {
            a.a(this.a, this.l.b(), "");
        }
    }

    public final void e() {
        this.j = false;
        switch (this.i) {
            case LAYOUT:
                this.h.post(new Runnable() {
                    public final void run() {
                        AdInformationObject.this.c.removeView(AdInformationObject.this.b);
                    }
                });
                return;
            case REGULAR:
                this.f.dismiss();
                break;
        }
    }

    public final void a(RelativeLayout relativeLayout) {
        boolean z;
        if (this.k == null || !this.k.e()) {
            z = AdInformationMetaData.b().a().a(this.a);
        } else {
            z = this.k.b();
        }
        if (z) {
            this.c = relativeLayout;
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            if (this.k == null || !this.k.d()) {
                AdInformationMetaData.b().a().a(this.g).addRules(layoutParams);
            } else {
                this.k.c().addRules(layoutParams);
            }
            this.c.addView(this.d, layoutParams);
        }
    }
}
