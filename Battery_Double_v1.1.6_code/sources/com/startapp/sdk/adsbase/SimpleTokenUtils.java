package com.startapp.sdk.adsbase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.util.Pair;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.startapp.common.Constants;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;
import com.startapp.sdk.adsbase.remoteconfig.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: StartAppSDK */
public class SimpleTokenUtils {
    private static List<PackageInfo> a = null;
    private static List<PackageInfo> b = null;
    private static long c = 0;
    private static volatile Pair<TokenType, String> d = null;
    private static volatile Pair<TokenType, String> e = null;
    private static boolean f = true;
    private static boolean g = false;
    private static TokenType h = TokenType.UNDEFINED;

    /* compiled from: StartAppSDK */
    private enum TokenType {
        T1(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY),
        T2("token2"),
        UNDEFINED("");
        
        private final String text;

        private TokenType(String str) {
            this.text = str;
        }

        public final String toString() {
            return this.text;
        }
    }

    static {
        SimpleTokenUtils.class.getSimpleName();
    }

    public static long a() {
        return c;
    }

    public static void a(final Context context) {
        c(context);
        f = true;
        g = false;
        h = TokenType.UNDEFINED;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                SimpleTokenUtils.b();
                SimpleTokenUtils.c(context);
            }
        }, intentFilter);
        MetaData.D().a((b) new b() {
            public final void a(RequestReason requestReason, boolean z) {
                if (z) {
                    SimpleTokenUtils.b();
                    SimpleTokenUtils.c(context);
                }
                MetaData.D().a((b) this);
            }

            public final void a() {
                MetaData.D().a((b) this);
            }
        });
    }

    public static void b(Context context) {
        a(context, MetaData.D().d().a(context));
    }

    public static void c(final Context context) {
        try {
            if ((d == null || e == null) && MetaData.D().d().a(context)) {
                ThreadManager.a(Priority.HIGH, (Runnable) new Runnable() {
                    public final void run() {
                        SimpleTokenUtils.b(context);
                    }
                });
            }
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    private static synchronized void a(Context context, boolean z) {
        synchronized (SimpleTokenUtils.class) {
            if ((d == null || e == null) && z) {
                try {
                    PackageManager packageManager = context.getPackageManager();
                    Set s = MetaData.D().s();
                    Set t = MetaData.D().t();
                    a = new CopyOnWriteArrayList();
                    b = new CopyOnWriteArrayList();
                    try {
                        List<PackageInfo> a2 = com.startapp.common.b.b.a(packageManager);
                        c = System.currentTimeMillis();
                        PackageInfo packageInfo = null;
                        for (PackageInfo packageInfo2 : a2) {
                            if (!com.startapp.common.b.b.a(packageInfo2)) {
                                if (VERSION.SDK_INT >= 9) {
                                    long j = packageInfo2.firstInstallTime;
                                    if (j < c && j >= 1291593600000L) {
                                        c = j;
                                    }
                                }
                                a.add(packageInfo2);
                                String installerPackageName = packageManager.getInstallerPackageName(packageInfo2.packageName);
                                if (s != null && s.contains(installerPackageName)) {
                                    b.add(packageInfo2);
                                }
                            } else if (t.contains(packageInfo2.packageName)) {
                                a.add(packageInfo2);
                            } else if (packageInfo2.packageName.equals(Constants.a)) {
                                packageInfo = packageInfo2;
                            }
                        }
                        a = b(a);
                        b = b(b);
                        if (packageInfo != null) {
                            a.add(0, packageInfo);
                        }
                    } catch (Throwable th) {
                        new e(th).a(context);
                    }
                    d = new Pair<>(TokenType.T1, com.iab.omid.library.startapp.b.a(a(a)));
                    e = new Pair<>(TokenType.T2, com.iab.omid.library.startapp.b.a(a(b)));
                } catch (Throwable th2) {
                    new e(th2).a(context);
                }
            }
        }
    }

    public static void b() {
        d = null;
        e = null;
    }

    static Pair<String, String> d(Context context) {
        return a(context, MetaData.D().d().a(context), MetaData.D().u(), MetaData.D().v());
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[Catch:{ Throwable -> 0x005a }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002b A[Catch:{ Throwable -> 0x005a }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[Catch:{ Throwable -> 0x005a }] */
    private static synchronized Pair<String, String> a(Context context, boolean z, boolean z2, boolean z3) {
        Pair<String, String> pair;
        Pair pair2;
        synchronized (SimpleTokenUtils.class) {
            Pair pair3 = new Pair(TokenType.T1, "");
            if (z) {
                try {
                    if (h == TokenType.UNDEFINED) {
                        boolean z4 = f;
                        if (g) {
                            if (!f) {
                                pair2 = f(context);
                                if (z3) {
                                    z4 = !g;
                                }
                                g = z4;
                                if (!z2) {
                                    if (!j.a(context, "shared_prefs_simple_token", "").equals(pair2.second)) {
                                    }
                                }
                                pair3 = pair2;
                            }
                        }
                        pair2 = e(context);
                        if (z3) {
                        }
                        g = z4;
                        if (!z2) {
                        }
                        pair3 = pair2;
                    } else {
                        pair3 = h == TokenType.T1 ? e(context) : f(context);
                    }
                } catch (Throwable th) {
                    new e(th).a(context);
                }
            }
            pair = new Pair<>(((TokenType) pair3.first).toString(), pair3.second);
        }
        return pair;
    }

    static void a(Pair<String, String> pair) {
        h = TokenType.valueOf((String) pair.first);
    }

    public static Pair<String, String> c() {
        if (d != null) {
            return new Pair<>(((TokenType) d.first).toString(), d.second);
        }
        return new Pair<>(TokenType.T1.toString(), "");
    }

    public static Pair<String, String> d() {
        if (e != null) {
            return new Pair<>(((TokenType) e.first).toString(), e.second);
        }
        return new Pair<>(TokenType.T2.toString(), "");
    }

    private static Pair<TokenType, String> e(Context context) {
        if (d == null) {
            b(context);
        }
        j.b(context, "shared_prefs_simple_token", (String) d.second);
        f = false;
        h = TokenType.UNDEFINED;
        return new Pair<>(TokenType.T1, d.second);
    }

    private static Pair<TokenType, String> f(Context context) {
        if (e == null) {
            b(context);
        }
        j.b(context, "shared_prefs_simple_token2", (String) e.second);
        f = false;
        h = TokenType.UNDEFINED;
        return new Pair<>(TokenType.T2, e.second);
    }

    private static List<String> a(List<PackageInfo> list) {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo packageInfo : list) {
            arrayList.add(packageInfo.packageName);
        }
        return arrayList;
    }

    private static List<PackageInfo> b(List<PackageInfo> list) {
        if (list.size() <= 100) {
            return list;
        }
        ArrayList arrayList = new ArrayList(list);
        c((List<PackageInfo>) arrayList);
        return arrayList.subList(0, 100);
    }

    private static void c(List<PackageInfo> list) {
        if (VERSION.SDK_INT >= 9) {
            Collections.sort(list, new Comparator<PackageInfo>() {
                public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                    PackageInfo packageInfo = (PackageInfo) obj2;
                    long j = ((PackageInfo) obj).firstInstallTime;
                    long j2 = packageInfo.firstInstallTime;
                    if (j > j2) {
                        return -1;
                    }
                    return j == j2 ? 0 : 1;
                }
            });
        }
    }
}
