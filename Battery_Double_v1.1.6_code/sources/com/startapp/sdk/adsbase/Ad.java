package com.startapp.sdk.adsbase;

import android.content.Context;
import com.startapp.sdk.adsbase.adinformation.AdInformationOverrides;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.cache.CacheMetaData;
import com.startapp.sdk.adsbase.consent.a;
import com.startapp.sdk.adsbase.i.o;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;
import com.startapp.sdk.b.c;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/* compiled from: StartAppSDK */
public abstract class Ad implements Serializable {
    private static boolean b = false;
    private static final long serialVersionUID = 1;
    protected transient Context a;
    protected ActivityExtra activityExtra;
    protected Long adCacheTtl = null;
    private AdInformationOverrides adInfoOverride;
    protected boolean belowMinCPM = false;
    protected String errorMessage;
    protected Serializable extraData = null;
    /* access modifiers changed from: private */
    public Long lastLoadTime = null;
    private NotDisplayedReason notDisplayedReason;
    protected Placement placement;
    private AdState state = AdState.UN_INITIALIZED;
    private AdType type;
    private boolean videoCancelCallBack;

    /* compiled from: StartAppSDK */
    public enum AdState {
        UN_INITIALIZED,
        PROCESSING,
        READY
    }

    /* compiled from: StartAppSDK */
    public enum AdType {
        INTERSTITIAL,
        RICH_TEXT,
        VIDEO,
        REWARDED_VIDEO,
        NON_VIDEO,
        VIDEO_NO_VAST
    }

    /* access modifiers changed from: protected */
    public abstract void a(AdPreferences adPreferences, b bVar);

    public abstract String getAdId();

    public Long getConsentTimestamp() {
        return null;
    }

    public Integer getConsentType() {
        return null;
    }

    public void setConsentTimestamp(Long l) {
    }

    public void setConsentType(Integer num) {
    }

    @Deprecated
    public boolean show() {
        return false;
    }

    public Ad(Context context, Placement placement2) {
        this.a = context;
        this.placement = placement2;
        if (s.e()) {
            this.adInfoOverride = AdInformationOverrides.a();
        }
    }

    public Serializable getExtraData() {
        return this.extraData;
    }

    public Context getContext() {
        return this.a;
    }

    public void setContext(Context context) {
        this.a = context;
    }

    public void setActivityExtra(ActivityExtra activityExtra2) {
        this.activityExtra = activityExtra2;
    }

    public void setExtraData(Serializable serializable) {
        this.extraData = serializable;
    }

    public AdState getState() {
        return this.state;
    }

    public boolean isBelowMinCPM() {
        return this.belowMinCPM;
    }

    public void setState(AdState adState) {
        this.state = adState;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public AdInformationOverrides getAdInfoOverride() {
        return this.adInfoOverride;
    }

    public void setAdInfoOverride(AdInformationOverrides adInformationOverrides) {
        this.adInfoOverride = adInformationOverrides;
    }

    /* access modifiers changed from: protected */
    public Placement i() {
        return this.placement;
    }

    @Deprecated
    public boolean load() {
        return load(new AdPreferences(), null);
    }

    @Deprecated
    public boolean load(AdEventListener adEventListener) {
        return load(new AdPreferences(), adEventListener);
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences) {
        return load(adPreferences, null);
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences, AdEventListener adEventListener) {
        return load(adPreferences, b.a(this.a, adEventListener), true);
    }

    public boolean load(final AdPreferences adPreferences, final b bVar, boolean z) {
        boolean z2;
        String str;
        final a f = c.a(this.a).f();
        final AnonymousClass1 r1 = new b() {
            public final void a(Ad ad) {
                Ad.this.lastLoadTime = Long.valueOf(System.currentTimeMillis());
                bVar.a(ad);
                f.a(ad.getConsentType(), ad.getConsentTimestamp(), false, true);
                Context context = Ad.this.a;
                StringBuilder sb = new StringBuilder("Loaded ");
                sb.append(s.a(ad));
                sb.append(" ad with creative ID - ");
                sb.append(ad.getAdId());
                s.a(context, false, sb.toString());
            }

            public final void b(Ad ad) {
                bVar.b(ad);
                String errorMessage = ad.getErrorMessage();
                if (errorMessage == null) {
                    errorMessage = "";
                } else if (errorMessage.contains("204")) {
                    errorMessage = "NO FILL";
                }
                Context context = Ad.this.a;
                StringBuilder sb = new StringBuilder("Failed to load ");
                sb.append(s.a(ad));
                sb.append(" ad: ");
                sb.append(errorMessage);
                s.a(context, true, sb.toString());
            }
        };
        if (!b) {
            SimpleTokenUtils.c(this.a);
            b = true;
        }
        String str2 = "";
        if (this.state != AdState.UN_INITIALIZED) {
            str = "load() was already called.";
            z2 = true;
        } else {
            str = str2;
            z2 = false;
        }
        if (!s.c(this.a)) {
            str = "network not available.";
            z2 = true;
        }
        if (!MetaData.D().J()) {
            str = "serving ads disabled";
            z2 = true;
        }
        if (z2) {
            setErrorMessage("Ad wasn't loaded: ".concat(String.valueOf(str)));
            r1.b(this);
            return false;
        }
        setConsentType(f.d());
        setConsentTimestamp(f.e());
        setState(AdState.PROCESSING);
        AnonymousClass2 r8 = new com.startapp.sdk.adsbase.remoteconfig.b() {
            public final void a(RequestReason requestReason, boolean z) {
                Ad.this.a(adPreferences, r1);
            }

            public final void a() {
                Ad.this.a(adPreferences, r1);
            }

            public static String a(Node node) {
                try {
                    Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
                    newTransformer.setOutputProperty("omit-xml-declaration", "yes");
                    newTransformer.setOutputProperty("method", "xml");
                    newTransformer.setOutputProperty("indent", "no");
                    newTransformer.setOutputProperty("encoding", "UTF-8");
                    StringWriter stringWriter = new StringWriter();
                    newTransformer.transform(new DOMSource(node), new StreamResult(stringWriter));
                    return stringWriter.toString();
                } catch (Exception unused) {
                    return null;
                }
            }

            public static Document a(String str) {
                try {
                    DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
                    newInstance.setIgnoringElementContentWhitespace(true);
                    newInstance.setIgnoringComments(true);
                    DocumentBuilder newDocumentBuilder = newInstance.newDocumentBuilder();
                    InputSource inputSource = new InputSource();
                    inputSource.setCharacterStream(new StringReader(str));
                    return newDocumentBuilder.parse(inputSource);
                } catch (Exception unused) {
                    return null;
                }
            }

            public static String b(Node node) {
                NodeList childNodes = node.getChildNodes();
                String str = null;
                for (int i = 0; i < childNodes.getLength(); i++) {
                    str = ((CharacterData) childNodes.item(i)).getData().trim();
                    if (str.length() != 0) {
                        return str;
                    }
                }
                return str;
            }
        };
        if (adPreferences.getType() != null) {
            this.type = adPreferences.getType();
        }
        MetaData.D().a(this.a, adPreferences, o.d().c(), z, r8, false);
        return true;
    }

    public boolean isReady() {
        return this.state == AdState.READY && !e_();
    }

    public NotDisplayedReason getNotDisplayedReason() {
        return this.notDisplayedReason;
    }

    /* access modifiers changed from: protected */
    public final void a(NotDisplayedReason notDisplayedReason2) {
        this.notDisplayedReason = notDisplayedReason2;
    }

    /* access modifiers changed from: protected */
    public Long c() {
        long f = f();
        if (this.adCacheTtl != null) {
            f = Math.min(this.adCacheTtl.longValue(), f);
        }
        return Long.valueOf(f);
    }

    /* access modifiers changed from: protected */
    public long f() {
        return CacheMetaData.a().b().a();
    }

    /* access modifiers changed from: protected */
    public Long b() {
        return this.lastLoadTime;
    }

    /* access modifiers changed from: protected */
    public boolean e_() {
        if (this.lastLoadTime != null && System.currentTimeMillis() - this.lastLoadTime.longValue() > c().longValue()) {
            return true;
        }
        return false;
    }

    public AdType getType() {
        return this.type;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return this.videoCancelCallBack;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        this.videoCancelCallBack = z;
    }
}
