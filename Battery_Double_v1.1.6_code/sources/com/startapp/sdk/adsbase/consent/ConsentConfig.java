package com.startapp.sdk.adsbase.consent;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class ConsentConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean allowCT = false;
    private String clickUrl;
    private Integer consentType;
    @d(a = true)
    private ConsentTypeInfoConfig consentTypeInfo;
    private String dParam;
    private String impressionUrl;
    private String template;
    private Integer templateId;
    private Integer templateName;
    private long timeStamp = 0;

    public final boolean a() {
        return this.allowCT;
    }

    public final Integer b() {
        return this.consentType;
    }

    public final String c() {
        return this.template;
    }

    public final long d() {
        return this.timeStamp;
    }

    public final String e() {
        return this.impressionUrl;
    }

    public final String f() {
        return this.clickUrl;
    }

    public final Integer g() {
        return this.templateName;
    }

    public final Integer h() {
        return this.templateId;
    }

    public final String i() {
        return this.dParam;
    }

    public final ConsentTypeInfoConfig j() {
        return this.consentTypeInfo;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ConsentConfig consentConfig = (ConsentConfig) obj;
        return this.allowCT == consentConfig.allowCT && this.timeStamp == consentConfig.timeStamp && s.b(this.template, consentConfig.template) && s.b(this.impressionUrl, consentConfig.impressionUrl) && s.b(this.clickUrl, consentConfig.clickUrl) && s.b(this.templateName, consentConfig.templateName) && s.b(this.templateId, consentConfig.templateId) && s.b(this.dParam, consentConfig.dParam) && s.b(this.consentTypeInfo, consentConfig.consentTypeInfo);
    }

    public final int hashCode() {
        return s.a(Boolean.valueOf(this.allowCT), this.template, Long.valueOf(this.timeStamp), this.impressionUrl, this.clickUrl, this.templateName, this.templateId, this.dParam, this.consentTypeInfo);
    }
}
