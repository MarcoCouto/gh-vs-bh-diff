package com.startapp.sdk.adsbase.consent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;
import com.startapp.sdk.adsbase.remoteconfig.b;
import com.startapp.sdk.b.c;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;

/* compiled from: StartAppSDK */
public final class a implements b {
    private final Context a;
    private final SharedPreferences b;
    private Intent c;
    private boolean d = false;
    private boolean e = true;

    static {
        a.class.getSimpleName();
    }

    public final void a(Intent intent) {
        this.c = intent;
    }

    public final boolean b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        this.d = false;
        if (this.c != null) {
            this.a.startActivity(this.c);
        }
    }

    public a(Context context) {
        this.a = context;
        this.b = context.getSharedPreferences("com.startapp.sdk", 0);
    }

    public final void a(Integer num, Long l, boolean z, boolean z2) {
        StringBuilder sb = new StringBuilder("SET ct=");
        sb.append(num);
        sb.append(":timestamp=");
        sb.append(l);
        if (num != null && l != null && g()) {
            int i = this.b.getInt("consentType", -1);
            long j = this.b.getLong("consentTimestamp", 0);
            if (i != num.intValue() && (z || j < l.longValue())) {
                this.b.edit().putInt("consentType", num.intValue()).putLong("consentTimestamp", l.longValue()).commit();
                if (z2) {
                    MetaData.D().a(this.a, new AdPreferences(), RequestReason.CONSENT, false, null, true);
                }
            }
        }
    }

    public final void a(boolean z) {
        this.e = z;
    }

    private boolean g() {
        ConsentConfig e2 = MetaData.D().e();
        return this.e && e2 != null && e2.a();
    }

    public final Long e() {
        if (!g() || !this.b.contains("consentTimestamp")) {
            return null;
        }
        return Long.valueOf(this.b.getLong("consentTimestamp", 0));
    }

    public final void f() {
        ConsentConfig e2 = MetaData.D().e();
        if (e2 != null && g() && !this.d && s.h(this.a) && e2.c() != null && e2.g() != null) {
            Intent intent = new Intent(this.a, ConsentActivity.class);
            intent.setFlags(276824064);
            intent.putExtra("allowCT", e2.a());
            intent.putExtra("timestamp", e2.d());
            intent.putExtra("templateName", e2.g());
            intent.putExtra("templateId", e2.h());
            if (!TextUtils.isEmpty(e2.c())) {
                intent.setData(Uri.parse(e2.c()));
            }
            if (!TextUtils.isEmpty(e2.i())) {
                intent.putExtra("dParam", e2.i());
            }
            if (!TextUtils.isEmpty(e2.f())) {
                intent.putExtra("clickUrl", e2.f());
            }
            if (!TextUtils.isEmpty(e2.e())) {
                intent.putExtra("impressionUrl", e2.e());
            }
            ConsentTypeInfoConfig j = e2.j();
            if (j != null) {
                intent.putExtra(Events.AD_IMPRESSION, j.a());
                intent.putExtra("trueClick", j.b());
                intent.putExtra("falseClick", j.c());
            }
            new StringBuilder("Dialog becomes visible with ts=").append(e2.d());
            this.d = true;
            this.a.startActivity(intent);
        }
    }

    public final void a(RequestReason requestReason, boolean z) {
        MetaData.D().a((b) this);
        ConsentConfig e2 = MetaData.D().e();
        if (e2 != null && g()) {
            Integer b2 = e2.b();
            if (b2 != null) {
                a(b2, Long.valueOf(e2.d()), false, false);
            }
            if (requestReason == RequestReason.CONSENT) {
                this.b.edit().putLong("consentTimestamp", e2.d()).commit();
                return;
            }
            if (requestReason == RequestReason.LAUNCH) {
                f();
            }
        }
    }

    public final void a() {
        MetaData.D().a((b) this);
    }

    public final Integer d() {
        if (g()) {
            int hashCode = c.a(this.a).d().b().a().hashCode();
            if (!this.b.contains("advIdHash") || this.b.getInt("advIdHash", 0) != hashCode) {
                this.b.edit().remove("consentType").remove("consentTimestamp").putInt("advIdHash", hashCode).commit();
            }
        }
        if (!g() || !this.b.contains("consentType")) {
            return null;
        }
        return Integer.valueOf(this.b.getInt("consentType", -1));
    }
}
