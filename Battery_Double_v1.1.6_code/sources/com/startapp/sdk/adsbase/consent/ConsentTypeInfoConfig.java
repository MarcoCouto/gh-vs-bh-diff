package com.startapp.sdk.adsbase.consent;

import com.startapp.sdk.adsbase.i.s;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class ConsentTypeInfoConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private Integer falseClick;
    private Integer impression;
    private Integer trueClick;

    public final Integer a() {
        return this.impression;
    }

    public final Integer b() {
        return this.trueClick;
    }

    public final Integer c() {
        return this.falseClick;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ConsentTypeInfoConfig consentTypeInfoConfig = (ConsentTypeInfoConfig) obj;
        return s.b(this.impression, consentTypeInfoConfig.impression) && s.b(this.trueClick, consentTypeInfoConfig.trueClick) && s.b(this.falseClick, consentTypeInfoConfig.falseClick);
    }

    public final int hashCode() {
        return s.a(this.impression, this.trueClick, this.falseClick);
    }
}
