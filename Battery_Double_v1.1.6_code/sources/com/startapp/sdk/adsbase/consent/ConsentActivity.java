package com.startapp.sdk.adsbase.consent;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings.TextSize;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.b.c;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.net.URI;

/* compiled from: StartAppSDK */
public class ConsentActivity extends Activity {
    private final String a = ConsentActivity.class.getSimpleName();
    private WebView b;
    private String c;

    /* compiled from: StartAppSDK */
    class a extends WebViewClient {
        a() {
        }

        public final void onPageFinished(WebView webView, String str) {
            Bundle extras = ConsentActivity.this.getIntent().getExtras();
            if (extras != null) {
                StringBuilder sb = new StringBuilder("javascript:var obj = {};");
                if (!TextUtils.isEmpty(str)) {
                    StringBuilder sb2 = new StringBuilder("obj.template = '");
                    sb2.append(str);
                    sb2.append("';");
                    sb.append(sb2.toString());
                }
                if (extras.containsKey("allowCT")) {
                    boolean z = extras.getBoolean("allowCT");
                    StringBuilder sb3 = new StringBuilder("obj.allowCT = ");
                    sb3.append(z);
                    sb3.append(";");
                    sb.append(sb3.toString());
                }
                String a2 = s.a((Context) ConsentActivity.this);
                if (!TextUtils.isEmpty(a2)) {
                    StringBuilder sb4 = new StringBuilder("obj.imageBase64 = '");
                    sb4.append(a2);
                    sb4.append("';");
                    sb.append(sb4.toString());
                }
                if (extras.containsKey("dParam")) {
                    String string = extras.getString("dParam");
                    if (!TextUtils.isEmpty(string)) {
                        StringBuilder sb5 = new StringBuilder("obj.dParam = '");
                        sb5.append(string);
                        sb5.append("';");
                        sb.append(sb5.toString());
                    }
                }
                if (extras.containsKey("clickUrl")) {
                    String string2 = extras.getString("clickUrl");
                    if (!TextUtils.isEmpty(string2)) {
                        StringBuilder sb6 = new StringBuilder("obj.clickUrl = '");
                        sb6.append(string2);
                        sb6.append("';");
                        sb.append(sb6.toString());
                    }
                }
                if (extras.containsKey("impressionUrl")) {
                    String string3 = extras.getString("impressionUrl");
                    if (!TextUtils.isEmpty(string3)) {
                        StringBuilder sb7 = new StringBuilder("obj.impressionUrl = '");
                        sb7.append(string3);
                        sb7.append("';");
                        sb.append(sb7.toString());
                    }
                }
                String c = ((com.startapp.sdk.c.b.a) c.a(ConsentActivity.this).a().c()).c();
                if (!TextUtils.isEmpty(c)) {
                    StringBuilder sb8 = new StringBuilder("obj.locales = '");
                    sb8.append(c);
                    sb8.append("';");
                    sb.append(sb8.toString());
                }
                if (extras.containsKey("timestamp")) {
                    long j = extras.getLong("timestamp");
                    StringBuilder sb9 = new StringBuilder("obj.timeStamp = ");
                    sb9.append(j);
                    sb9.append(";");
                    sb.append(sb9.toString());
                }
                if (extras.containsKey("templateName")) {
                    int i = extras.getInt("templateName");
                    StringBuilder sb10 = new StringBuilder("obj.templateName = ");
                    sb10.append(i);
                    sb10.append(";");
                    sb.append(sb10.toString());
                }
                if (extras.containsKey("templateId")) {
                    int i2 = extras.getInt("templateId");
                    StringBuilder sb11 = new StringBuilder("obj.templateId = ");
                    sb11.append(i2);
                    sb11.append(";");
                    sb.append(sb11.toString());
                }
                sb.append("obj.os = 'android';");
                sb.append("obj.consentTypeInfo = {};");
                if (extras.containsKey(Events.AD_IMPRESSION)) {
                    long j2 = (long) extras.getInt(Events.AD_IMPRESSION);
                    StringBuilder sb12 = new StringBuilder("obj.consentTypeInfo.impression = ");
                    sb12.append(j2);
                    sb12.append(";");
                    sb.append(sb12.toString());
                }
                if (extras.containsKey("trueClick")) {
                    long j3 = (long) extras.getInt("trueClick");
                    StringBuilder sb13 = new StringBuilder("obj.consentTypeInfo.trueClick = ");
                    sb13.append(j3);
                    sb13.append(";");
                    sb.append(sb13.toString());
                }
                if (extras.containsKey("falseClick")) {
                    long j4 = (long) extras.getInt("falseClick");
                    StringBuilder sb14 = new StringBuilder("obj.consentTypeInfo.falseClick = ");
                    sb14.append(j4);
                    sb14.append(";");
                    sb.append(sb14.toString());
                }
                sb.append("startappInit(obj);");
                webView.loadUrl(sb.toString());
            }
        }

        @TargetApi(24)
        public final boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return a(webResourceRequest.getUrl());
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return a(Uri.parse(str));
        }

        private boolean a(Uri uri) {
            String scheme = uri.getScheme();
            if (scheme == null || !scheme.equalsIgnoreCase("startappad")) {
                return false;
            }
            String host = uri.getHost();
            if (host == null) {
                return false;
            }
            if (host.equalsIgnoreCase("setconsent")) {
                ConsentConfig e = MetaData.D().e();
                String queryParameter = uri.getQueryParameter("status");
                if (!TextUtils.isEmpty(queryParameter) && e != null) {
                    try {
                        c.a(ConsentActivity.this).f().a(Integer.valueOf(Integer.parseInt(queryParameter)), Long.valueOf(e.d()), true, true);
                    } catch (Throwable th) {
                        new e(th).a((Context) ConsentActivity.this);
                    }
                }
                return true;
            } else if (!host.equalsIgnoreCase("close")) {
                return false;
            } else {
                ConsentActivity.this.finish();
                return true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        String dataString = getIntent().getDataString();
        if (!TextUtils.isEmpty(dataString)) {
            try {
                URI uri = new URI(dataString);
                URI uri2 = new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), null, null);
                this.c = uri2.toString();
                this.b = new WebView(this);
                this.b.setWebViewClient(new a());
                this.b.getSettings().setJavaScriptEnabled(true);
                this.b.setHorizontalScrollBarEnabled(false);
                this.b.setVerticalScrollBarEnabled(false);
                if (VERSION.SDK_INT >= 15) {
                    this.b.getSettings().setTextZoom(100);
                } else {
                    this.b.getSettings().setTextSize(TextSize.NORMAL);
                }
                this.b.loadUrl(dataString);
                this.b.setBackgroundColor(0);
                b.d(this.b);
                LayoutParams layoutParams2 = new LayoutParams(-1, -1);
                layoutParams2.addRule(13);
                relativeLayout.addView(this.b, layoutParams2);
            } catch (Throwable th) {
                new e(th).a((Context) this);
            }
        }
        setContentView(relativeLayout, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        c.a(this).f().c();
        super.onDestroy();
    }

    public void onBackPressed() {
        if (this.b == null) {
            super.onBackPressed();
            return;
        }
        String url = this.b.getUrl();
        if (this.c != null && url != null && url.contains(this.c)) {
            this.b.loadUrl("javascript:startappBackPressed();");
        } else if (this.b.canGoBack()) {
            this.b.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
