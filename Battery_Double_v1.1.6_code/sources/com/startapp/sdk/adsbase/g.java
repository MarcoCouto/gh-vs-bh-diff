package com.startapp.sdk.adsbase;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.startapp.sdk.adsbase.infoevents.e;

/* compiled from: StartAppSDK */
public class g implements a {
    private static String a = "g";
    private static String b = "com.startapp.sdk.APPLICATION_ID";
    private static String c = "com.startapp.sdk.RETURN_ADS_ENABLED";
    private String d;
    private boolean e = true;

    g(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager != null) {
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo.metaData != null) {
                    if (applicationInfo.metaData.containsKey(b)) {
                        int i = applicationInfo.metaData.getInt(b);
                        this.d = i != 0 ? String.valueOf(i) : applicationInfo.metaData.getString(b);
                        String str = a;
                        StringBuilder sb = new StringBuilder("appId is ");
                        sb.append(this.d);
                        Log.i(str, sb.toString());
                        if (applicationInfo.metaData.containsKey(c)) {
                            this.e = applicationInfo.metaData.getBoolean(c);
                            String str2 = a;
                            StringBuilder sb2 = new StringBuilder("returnAds enabled: ");
                            sb2.append(this.e);
                            Log.i(str2, sb2.toString());
                        }
                        return;
                    }
                    Log.i(a, "appId hasn't been provided in the Manifest");
                }
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
    }

    public final String a() {
        return this.d;
    }

    public final boolean b() {
        return this.e;
    }
}
