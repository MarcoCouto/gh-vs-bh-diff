package com.startapp.sdk.adsbase.i;

import android.content.Context;
import android.os.Environment;
import com.startapp.sdk.adsbase.j;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/* compiled from: StartAppSDK */
public final class m {
    private static boolean a = true;

    public static void a(Context context, boolean z) {
        if (z) {
            a = true;
            j.b(context, "copyDrawables", Boolean.TRUE);
        }
        if (a) {
            boolean booleanValue = j.a(context, "copyDrawables", Boolean.TRUE).booleanValue();
            a = booleanValue;
            if (booleanValue) {
                String str = "drawable-hdpi.zip";
                try {
                    String str2 = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir;
                    String a2 = a(context);
                    if (!a(context, str2, "", str) && !a(context, str2, "assets/", str) && !a(context, a2, "", str)) {
                        a(context, a2, "assets/", str);
                    }
                } catch (Exception unused) {
                }
            }
        }
    }

    private static String a(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(Environment.getExternalStorageDirectory());
        sb.append("/Android/obb/");
        sb.append(context.getPackageName());
        sb.append("/");
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder("main.1.");
        sb3.append(context.getPackageName());
        sb3.append(".obb");
        String sb4 = sb3.toString();
        File file = new File(sb2);
        if (file.exists() && file.isDirectory()) {
            StringBuilder sb5 = new StringBuilder("main[.][1-9][0-9]*[.]");
            sb5.append(context.getPackageName());
            sb5.append("[.]obb");
            final Pattern compile = Pattern.compile(sb5.toString());
            File[] listFiles = file.listFiles(new FileFilter() {
                public final boolean accept(File file) {
                    return compile.matcher(file.getName()).matches();
                }
            });
            if (listFiles.length > 0) {
                int i = 0;
                int i2 = 0;
                for (int i3 = 0; i3 < listFiles.length; i3++) {
                    try {
                        int parseInt = Integer.parseInt(listFiles[i3].getName().split("[.]")[1]);
                        if (parseInt > i2) {
                            i = i3;
                            i2 = parseInt;
                        }
                    } catch (Exception unused) {
                    }
                }
                sb4 = listFiles[i].getName();
            }
        }
        StringBuilder sb6 = new StringBuilder();
        sb6.append(sb2);
        sb6.append(sb4);
        return sb6.toString();
    }

    private static boolean a(Context context, String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder("Trying to copy resources from ");
        sb.append(str);
        sb.append(" in /");
        sb.append(str2);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str2);
        sb2.append(str3);
        String sb3 = sb2.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append(context.getFilesDir().getPath());
        sb4.append("/");
        sb4.append(str3);
        if (!a(str, sb3, sb4.toString())) {
            StringBuilder sb5 = new StringBuilder("Failed copying resources from ");
            sb5.append(str);
            sb5.append(" in /");
            sb5.append(str2);
            return false;
        }
        StringBuilder sb6 = new StringBuilder();
        sb6.append(context.getFilesDir().getPath());
        sb6.append("/");
        sb6.append(str3);
        a(context, sb6.toString());
        StringBuilder sb7 = new StringBuilder();
        sb7.append(str2);
        sb7.append("drawable.zip");
        String sb8 = sb7.toString();
        StringBuilder sb9 = new StringBuilder();
        sb9.append(context.getFilesDir().getPath());
        sb9.append("/drawable.zip");
        a(str, sb8, sb9.toString());
        StringBuilder sb10 = new StringBuilder();
        sb10.append(context.getFilesDir().getPath());
        sb10.append("/drawable.zip");
        a(context, sb10.toString());
        j.b(context, "copyDrawables", Boolean.FALSE);
        StringBuilder sb11 = new StringBuilder("Copy from ");
        sb11.append(str);
        sb11.append(" in /");
        sb11.append(str2);
        sb11.append(" succeeded");
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0050, code lost:
        r1 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0056, code lost:
        r5 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0058, code lost:
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r5.close();
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x006d, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x004b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x005f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:50:0x006a */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0056 A[ExcHandler: all (th java.lang.Throwable), PHI: r2 
  PHI: (r2v4 java.util.zip.ZipFile) = (r2v1 java.util.zip.ZipFile), (r2v1 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile) binds: [B:44:0x005f, B:45:?, B:3:0x0007, B:15:0x002f, B:16:?, B:18:0x0036, B:24:0x0045, B:25:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0007] */
    private static boolean a(String str, String str2, String str3) {
        ZipFile zipFile;
        InputStream inputStream;
        ZipEntry zipEntry;
        FileOutputStream fileOutputStream = null;
        try {
            zipFile = new ZipFile(str);
            try {
                Enumeration entries = zipFile.entries();
                while (true) {
                    if (!entries.hasMoreElements()) {
                        zipEntry = null;
                        break;
                    }
                    zipEntry = (ZipEntry) entries.nextElement();
                    if (!zipEntry.isDirectory() && zipEntry.getName().equals(str2)) {
                        break;
                    }
                }
                if (zipEntry != null) {
                    inputStream = zipFile.getInputStream(zipEntry);
                    FileOutputStream fileOutputStream2 = new FileOutputStream(str3);
                    byte[] bArr = new byte[256];
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        fileOutputStream2.write(bArr, 0, read);
                    }
                    fileOutputStream2.flush();
                    inputStream.close();
                    fileOutputStream2.close();
                    try {
                        zipFile.close();
                    } catch (Exception unused) {
                    }
                    return true;
                }
                try {
                    zipFile.close();
                } catch (Exception unused2) {
                }
                return false;
            } catch (IOException ) {
                zipFile.close();
                return true;
            } catch (Throwable th) {
            }
        } catch (IOException unused3) {
            inputStream = null;
            zipFile = null;
        } catch (Throwable th2) {
            th = th2;
            zipFile = null;
            try {
                zipFile.close();
            } catch (Exception unused4) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:14|15|30|31|32|33|34) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:17|16|38|39|40|41|42) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:19|18|47|48|49|50|51) */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0073, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x007c, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x005b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x0067 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x006c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x006f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x0075 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x0078 */
    private static void a(Context context, String str) {
        FileOutputStream fileOutputStream;
        ZipInputStream zipInputStream;
        byte[] bArr = new byte[1024];
        fileOutputStream = null;
        try {
            zipInputStream = new ZipInputStream(new FileInputStream(str));
            try {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                while (nextEntry != null) {
                    String name = nextEntry.getName();
                    StringBuilder sb = new StringBuilder();
                    sb.append(context.getFilesDir().getPath());
                    sb.append("/");
                    sb.append(name);
                    FileOutputStream fileOutputStream2 = new FileOutputStream(sb.toString());
                    while (true) {
                        try {
                            int read = zipInputStream.read(bArr, 0, 1024);
                            if (read < 0) {
                                break;
                            }
                            fileOutputStream2.write(bArr, 0, read);
                        } catch (FileNotFoundException unused) {
                            fileOutputStream = fileOutputStream2;
                            fileOutputStream.close();
                            zipInputStream.close();
                        } catch (IOException unused2) {
                            fileOutputStream = fileOutputStream2;
                            fileOutputStream.close();
                            zipInputStream.close();
                        } catch (Throwable th) {
                            th = th;
                            fileOutputStream = fileOutputStream2;
                            fileOutputStream.close();
                            zipInputStream.close();
                            throw th;
                        }
                    }
                    fileOutputStream2.close();
                    zipInputStream.closeEntry();
                    nextEntry = zipInputStream.getNextEntry();
                    fileOutputStream = fileOutputStream2;
                }
                fileOutputStream.close();
                try {
                    zipInputStream.close();
                } catch (IOException unused3) {
                }
            } catch (FileNotFoundException ) {
                fileOutputStream.close();
                zipInputStream.close();
            } catch (IOException ) {
                fileOutputStream.close();
                zipInputStream.close();
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream.close();
                zipInputStream.close();
                throw th;
            }
        } catch (FileNotFoundException unused4) {
            zipInputStream = null;
        } catch (IOException unused5) {
            zipInputStream = null;
        } catch (Throwable th3) {
            th = th3;
            zipInputStream = null;
            fileOutputStream.close();
            zipInputStream.close();
            throw th;
        }
    }
}
