package com.startapp.sdk.adsbase.i;

import java.util.Comparator;

/* compiled from: StartAppSDK */
public final class d<T> implements Comparator<T> {
    private final Comparator<T> a;
    private final Comparator<T> b;

    public d(Comparator<T> comparator, Comparator<T> comparator2) {
        this.a = comparator;
        this.b = comparator2;
    }

    public final int compare(T t, T t2) {
        int compare = this.a.compare(t, t2);
        return compare == 0 ? this.b.compare(t, t2) : compare;
    }
}
