package com.startapp.sdk.adsbase.i;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class r {
    public int a;
    public String b;

    public static int a(Context context, int i) {
        return Math.round(TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics()));
    }

    public static int b(Context context, int i) {
        return Math.round(((float) i) / context.getResources().getDisplayMetrics().density);
    }

    public static void a(TextView textView, Set<String> set) {
        if (set.contains("UNDERLINE")) {
            textView.setPaintFlags(textView.getPaintFlags() | 8);
        }
        int i = 0;
        if (set.contains("BOLD") && set.contains("ITALIC")) {
            i = 3;
        } else if (set.contains("BOLD")) {
            i = 1;
        } else if (set.contains("ITALIC")) {
            i = 2;
        }
        textView.setTypeface(null, i);
    }

    public static TextView a(Context context, Typeface typeface, float f, int i, int i2) {
        TextView textView = new TextView(context);
        textView.setTypeface(typeface, 1);
        textView.setTextSize(1, f);
        textView.setSingleLine(true);
        textView.setEllipsize(TruncateAt.END);
        textView.setTextColor(i);
        textView.setId(i2);
        return textView;
    }

    public static LayoutParams a(Context context, int[] iArr, int[] iArr2) {
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        for (int addRule : iArr2) {
            layoutParams.addRule(addRule);
        }
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = iArr[i] == 0 ? 0 : a(context, iArr[i]);
        }
        layoutParams.setMargins(iArr[0], iArr[1], iArr[2], iArr[3]);
        return layoutParams;
    }

    public static LayoutParams a(Context context, int[] iArr, int[] iArr2, int i, int i2) {
        LayoutParams a2 = a(context, iArr, iArr2);
        a2.addRule(i, i2);
        return a2;
    }

    public static ImageView a(Context context, Bitmap bitmap, int i) {
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(bitmap);
        imageView.setId(i);
        return imageView;
    }
}
