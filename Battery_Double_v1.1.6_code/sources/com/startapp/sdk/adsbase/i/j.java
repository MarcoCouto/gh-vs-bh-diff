package com.startapp.sdk.adsbase.i;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.SDKException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public final class j extends l {
    private List<k> a;

    public j() {
        this.a = null;
        this.a = new ArrayList();
    }

    public final void a(String str, Object obj, boolean z, boolean z2) throws SDKException {
        if (!z || obj != null) {
            if (obj != null && !obj.toString().equals("")) {
                try {
                    k kVar = new k();
                    kVar.a(str);
                    String obj2 = obj.toString();
                    if (z2) {
                        obj2 = URLEncoder.encode(obj2, "UTF-8");
                    }
                    kVar.b(obj2);
                    this.a.add(kVar);
                    return;
                } catch (UnsupportedEncodingException e) {
                    if (z) {
                        StringBuilder sb = new StringBuilder("failed encoding value: [");
                        sb.append(obj);
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        throw new SDKException(sb.toString(), e);
                    }
                }
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder("Required key: [");
        sb2.append(str);
        sb2.append("] is missing");
        throw new SDKException(sb2.toString(), null);
    }

    public final void a(String str, Set<String> set) throws SDKException {
        if (set != null) {
            k kVar = new k();
            kVar.a(str);
            HashSet hashSet = new HashSet();
            for (String encode : set) {
                try {
                    hashSet.add(URLEncoder.encode(encode, "UTF-8"));
                } catch (UnsupportedEncodingException unused) {
                }
            }
            kVar.a((Set<String>) hashSet);
            this.a.add(kVar);
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.a == null) {
            return sb.toString();
        }
        sb.append('?');
        for (k kVar : this.a) {
            if (kVar.b() != null) {
                sb.append(kVar.a());
                sb.append('=');
                sb.append(kVar.b());
                sb.append('&');
            } else if (kVar.c() != null) {
                Set<String> c = kVar.c();
                if (c != null) {
                    for (String str : c) {
                        sb.append(kVar.a());
                        sb.append('=');
                        sb.append(str);
                        sb.append('&');
                    }
                }
            }
        }
        if (sb.length() != 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString().replace("+", "%20");
    }
}
