package com.startapp.sdk.adsbase.i;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.ShareConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.SDKException;
import com.startapp.sdk.GeneratedConstants;
import com.startapp.sdk.ads.banner.banner3d.Banner3DAd;
import com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd;
import com.startapp.sdk.ads.interstitials.ReturnAd;
import com.startapp.sdk.ads.nativead.NativeAd;
import com.startapp.sdk.ads.offerWall.offerWallHtml.OfferWallAd;
import com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd;
import com.startapp.sdk.ads.splash.SplashAd;
import com.startapp.sdk.ads.video.VideoEnabledAd;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.Ad.AdType;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterOutputStream;

/* compiled from: StartAppSDK */
public class s {
    protected static int a = 0;
    /* access modifiers changed from: private */
    public static final String b = "s";
    private static Map<Activity, Integer> c = new WeakHashMap();

    /* compiled from: StartAppSDK */
    public interface a {
        void a();

        void a(String str);
    }

    /* compiled from: StartAppSDK */
    static class b {
        b() {
        }

        static StackTraceElement[] a() {
            return Thread.currentThread().getStackTrace();
        }
    }

    public static boolean a() {
        return new BigInteger(AdsConstants.e, 10).intValue() == 0;
    }

    public static String a(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return null;
        }
        try {
            Drawable loadIcon = packageManager.getApplicationInfo(context.getPackageName(), 128).loadIcon(packageManager);
            if (loadIcon instanceof BitmapDrawable) {
                Bitmap bitmap = ((BitmapDrawable) loadIcon).getBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(CompressFormat.PNG, 100, byteArrayOutputStream);
                return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 2);
            }
            new e(InfoEventCategory.ERROR).e("Util.getBase64AppIcon(): app icon is not BitmapDrawable").a(context);
            return null;
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public static String b(Context context) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        if (launchIntentForPackage == null) {
            return null;
        }
        ComponentName component = launchIntentForPackage.getComponent();
        if (component != null) {
            return component.getClassName();
        }
        return null;
    }

    public static String b() {
        String str = GeneratedConstants.INAPP_VERSION;
        if (str.equals("${version}")) {
            str = "0";
        }
        StringBuilder sb = new StringBuilder("SDK version: [");
        sb.append(str);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return str;
    }

    public static String c() {
        String str = GeneratedConstants.INAPP_FLAVOR;
        if (str.equals("${flavor}")) {
            str = GeneratedConstants.INAPP_FLAVOR;
        }
        StringBuilder sb = new StringBuilder("SDK Flavor: [");
        sb.append(str);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return str;
    }

    public static String d() {
        String str = GeneratedConstants.INAPP_PACKAGING;
        return str.equals("${packaging}") ? "" : str;
    }

    public static boolean a(long j) {
        String str = AdsConstants.e;
        if (str.equals("${flavor}") || (j & new BigInteger(str, 2).longValue()) != 0) {
            return true;
        }
        return false;
    }

    public static boolean e() {
        return a(2) || a(16) || a(32) || a(4);
    }

    public static String a(Double d) {
        if (d == null) {
            return null;
        }
        return String.format(Locale.US, "%.2f", new Object[]{d});
    }

    public static boolean c(Context context) {
        if (AdsConstants.b.booleanValue()) {
            return true;
        }
        if (com.startapp.common.b.b.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            try {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                    return false;
                }
                return true;
            } catch (Throwable th) {
                new e(th).a(context);
                return false;
            }
        }
        return false;
    }

    public static void a(Editor editor) {
        com.startapp.common.b.b.a(editor);
    }

    public static String a(String str, String str2, String str3) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf(str2);
        if (indexOf != -1) {
            int indexOf2 = str.indexOf(str3, str2.length() + indexOf);
            if (indexOf2 != -1) {
                return str.substring(indexOf + str2.length(), indexOf2);
            }
        }
        return null;
    }

    public static String e(Context context) {
        Resources resources = context.getResources();
        if (resources != null) {
            Configuration configuration = resources.getConfiguration();
            if (configuration != null) {
                if (configuration.orientation == 2) {
                    return "landscape";
                }
                if (configuration.orientation == 1) {
                    return "portrait";
                }
            }
        }
        return "undefined";
    }

    public static int a(Activity activity, int i, boolean z) {
        if (z) {
            if (!c.containsKey(activity)) {
                c.put(activity, Integer.valueOf(activity.getRequestedOrientation()));
            }
            if (i == activity.getResources().getConfiguration().orientation) {
                return com.startapp.common.b.b.a(activity, i, false);
            }
            return com.startapp.common.b.b.a(activity, i, true);
        }
        int i2 = -1;
        if (c.containsKey(activity)) {
            i2 = ((Integer) c.get(activity)).intValue();
            com.startapp.common.b.b.a(activity, i2);
            c.remove(activity);
        }
        return i2;
    }

    public static void a(Activity activity, boolean z) {
        a(activity, activity.getResources().getConfiguration().orientation, z);
    }

    private static List<Field> a(List<Field> list, Class<?> cls) {
        list.addAll(Arrays.asList(cls.getDeclaredFields()));
        if (cls.getSuperclass() != null) {
            a(list, cls.getSuperclass());
        }
        return list;
    }

    public static <T> boolean a(T t, T t2) {
        boolean z = false;
        try {
            for (Field field : a((List<Field>) new LinkedList<Field>(), t2.getClass())) {
                int modifiers = field.getModifiers();
                if (!Modifier.isTransient(modifiers) && !Modifier.isStatic(modifiers)) {
                    field.setAccessible(true);
                    if (field.get(t) == null) {
                        Object obj = field.get(t2);
                        if (obj != null) {
                            field.set(t, obj);
                            z = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            new StringBuilder("Util.mergeDefaultValues failed: ").append(e.getMessage());
        }
        return z;
    }

    public static void a(Context context, String str, final a aVar) {
        if ("true".equals(a(str, "@doNotRender@", "@doNotRender@"))) {
            aVar.a();
            return;
        }
        try {
            final WebView webView = new WebView(context);
            final Handler handler = new Handler(Looper.getMainLooper());
            if (AdsConstants.b.booleanValue()) {
                a = 25000;
                webView.getSettings().setBlockNetworkImage(false);
                webView.getSettings().setLoadsImagesAutomatically(true);
                webView.getSettings().setJavaScriptEnabled(true);
            } else {
                a = 0;
            }
            webView.setWebChromeClient(new WebChromeClient());
            webView.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    super.onPageFinished(webView, str);
                    s.b;
                    StringBuilder sb = new StringBuilder("onPageFinished url=[");
                    sb.append(str);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    handler.removeCallbacksAndMessages(null);
                    handler.postDelayed(new Runnable() {
                        public final void run() {
                            webView.destroy();
                            s.b;
                            aVar.a();
                        }
                    }, (long) s.a);
                }

                public final void onReceivedError(final WebView webView, int i, final String str, String str2) {
                    super.onReceivedError(webView, i, str, str2);
                    s.b;
                    StringBuilder sb = new StringBuilder("onReceivedError failingUrl=[");
                    sb.append(str2);
                    sb.append("], description=[");
                    sb.append(str);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    handler.removeCallbacksAndMessages(null);
                    handler.post(new Runnable() {
                        public final void run() {
                            webView.destroy();
                            aVar.a(str);
                        }
                    });
                }

                public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    s.b;
                    StringBuilder sb = new StringBuilder("shouldOverrideUrlLoading url=[");
                    sb.append(str);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    return super.shouldOverrideUrlLoading(webView, str);
                }
            });
            a(context, webView, str);
            handler.postDelayed(new Runnable() {
                public final void run() {
                    webView.destroy();
                    aVar.a();
                    s.b;
                }
            }, 25000);
        } catch (Throwable th) {
            new e(th).a(context);
            aVar.a("WebView instantiation Error");
        }
    }

    public static void a(Context context, WebView webView, String str) {
        try {
            webView.loadDataWithBaseURL(MetaData.D().q(), str, WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public static String f(Context context) {
        String str = "";
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
            if (resolveActivity == null || resolveActivity.activityInfo == null) {
                return str;
            }
            String str2 = resolveActivity.activityInfo.packageName;
            if (str2 != null) {
                try {
                    return str2.toLowerCase();
                } catch (Exception unused) {
                }
            }
            return str2;
        } catch (Exception unused2) {
            return str;
        }
    }

    public static String g(Context context) {
        return context.getPackageManager().getInstallerPackageName(context.getPackageName());
    }

    public static void a(WebView webView, String str, Object... objArr) {
        a(webView, true, str, objArr);
    }

    public static void a(WebView webView, boolean z, String str, Object... objArr) {
        if (webView != null) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("(");
                if (objArr != null) {
                    for (int i = 0; i < objArr.length; i++) {
                        if (!z || !(objArr[i] instanceof String)) {
                            sb.append(objArr[i]);
                        } else {
                            sb.append("\"");
                            sb.append(objArr[i]);
                            sb.append("\"");
                        }
                        if (i < objArr.length - 1) {
                            sb.append(",");
                        }
                    }
                }
                sb.append(")");
                new StringBuilder("runJavascript: ").append(sb.toString());
                StringBuilder sb2 = new StringBuilder("javascript:");
                sb2.append(sb.toString());
                webView.loadUrl(sb2.toString());
            } catch (Exception e) {
                new StringBuilder("runJavascript Exception: ").append(e.getMessage());
            }
        }
    }

    public static Class<?> a(Context context, Class<? extends Activity> cls, Class<? extends Activity> cls2) {
        if (a(context, cls) || !a(context, cls2)) {
            return cls;
        }
        String str = b;
        StringBuilder sb = new StringBuilder("Expected activity ");
        sb.append(cls.getName());
        sb.append(" is missing from AndroidManifest.xml");
        Log.w(str, sb.toString());
        return cls2;
    }

    public static boolean a(Context context, Class<? extends Activity> cls) {
        try {
            for (ActivityInfo activityInfo : context.getPackageManager().getPackageInfo(context.getPackageName(), 1).activities) {
                if (activityInfo.name.equals(cls.getName())) {
                    return true;
                }
            }
        } catch (Exception unused) {
        }
        return false;
    }

    public static boolean h(Context context) {
        try {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (activityManager == null) {
                return false;
            }
            List<RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses == null) {
                return false;
            }
            String packageName = context.getPackageName();
            for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo != null && runningAppProcessInfo.importance == 100 && packageName.equals(runningAppProcessInfo.processName)) {
                    return true;
                }
            }
            return false;
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public static boolean i(Context context) {
        try {
            ActivityInfo[] activityInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 1).activities;
            boolean z = false;
            int i = 0;
            while (!z) {
                try {
                    if (i >= activityInfoArr.length) {
                        return z;
                    }
                    int i2 = i + 1;
                    ActivityInfo activityInfo = activityInfoArr[i];
                    if (activityInfo.name.equals("com.startapp.sdk.adsbase.activities.AppWallActivity") || activityInfo.name.equals("com.startapp.sdk.adsbase.activities.OverlayActivity") || activityInfo.name.equals("com.startapp.sdk.adsbase.activities.FullScreenActivity")) {
                        z = (activityInfo.flags & 512) == 0;
                    }
                    i = i2;
                } catch (NameNotFoundException | Exception unused) {
                    return z;
                }
            }
            return z;
        } catch (NameNotFoundException | Exception unused2) {
            return false;
        }
    }

    public static String a(String str, Context context) {
        try {
            new com.startapp.common.b.b();
            return com.startapp.common.b.b.a(str, context);
        } catch (Throwable th) {
            new e(th).a(context);
            return null;
        }
    }

    public static long a(File file) {
        return com.startapp.common.b.b.a(file);
    }

    public static String a(Context context, int i) {
        try {
            Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), i);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            decodeResource.compress(CompressFormat.PNG, 100, byteArrayOutputStream);
            return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 2);
        } catch (Exception unused) {
            return "";
        }
    }

    public static <T> T a(String str, Class<T> cls) throws SDKException {
        T a2 = com.startapp.common.parser.b.a(str, cls);
        if (a2 != null) {
            return a2;
        }
        throw new SDKException();
    }

    public static void a(Object obj) {
        new Handler(Looper.getMainLooper()).postAtTime(null, obj, SystemClock.uptimeMillis() + 1000);
    }

    public static int a(Object... objArr) {
        return Arrays.deepHashCode(objArr);
    }

    public static <T> boolean b(T t, T t2) {
        if (t == null) {
            return t2 == null;
        }
        return t.equals(t2);
    }

    public static String b(Object obj) throws IOException {
        String a2 = com.startapp.common.parser.b.a(obj);
        if (a2 != null) {
            return Base64.encodeToString(b(a2.getBytes()), 11);
        }
        return null;
    }

    public static byte[] a(String str) throws IOException {
        return b(str.getBytes());
    }

    private static byte[] b(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream, new Deflater(9, true));
        deflaterOutputStream.write(bArr);
        deflaterOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] a(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        InflaterOutputStream inflaterOutputStream = new InflaterOutputStream(byteArrayOutputStream, new Inflater(true));
        inflaterOutputStream.write(bArr);
        inflaterOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    public static String j(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                List a2 = com.startapp.common.b.b.a(context, telephonyManager);
                if (a2 != null && a2.size() > 0) {
                    return com.startapp.common.b.a.c(a2.toString());
                }
            }
        } catch (SecurityException unused) {
        } catch (Throwable th) {
            new e(th).a(context);
        }
        return null;
    }

    public static String[] k(Context context) {
        String[] strArr = {null, null};
        try {
            MemoryInfo memoryInfo = new MemoryInfo();
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (activityManager != null) {
                activityManager.getMemoryInfo(memoryInfo);
                strArr[0] = Long.toString(memoryInfo.availMem / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED);
                Long a2 = com.startapp.common.b.b.a(memoryInfo);
                if (a2 != null) {
                    strArr[1] = Long.toString((a2.longValue() - memoryInfo.availMem) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED);
                }
            }
        } catch (SecurityException unused) {
        } catch (Throwable th) {
            new e(th).a(context);
        }
        return strArr;
    }

    public static String b(File file) {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        sb.append(readLine);
                    } else {
                        String sb2 = sb.toString();
                        a((Closeable) bufferedReader);
                        return sb2;
                    }
                }
            } catch (Exception unused) {
                a((Closeable) bufferedReader);
                return null;
            } catch (Throwable th) {
                th = th;
                a((Closeable) bufferedReader);
                throw th;
            }
        } catch (Exception unused2) {
            bufferedReader = null;
            a((Closeable) bufferedReader);
            return null;
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            a((Closeable) bufferedReader);
            throw th;
        }
    }

    public static List<String> c(File file) {
        BufferedReader bufferedReader;
        BufferedReader bufferedReader2 = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            List<String> list = null;
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        if (list == null) {
                            list = new ArrayList<>();
                        }
                        list.add(readLine);
                    } else {
                        a((Closeable) bufferedReader);
                        return list;
                    }
                } catch (Exception unused) {
                    a((Closeable) bufferedReader);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    bufferedReader2 = bufferedReader;
                    a((Closeable) bufferedReader2);
                    throw th;
                }
            }
        } catch (Exception unused2) {
            bufferedReader = null;
            a((Closeable) bufferedReader);
            return null;
        } catch (Throwable th2) {
            th = th2;
            a((Closeable) bufferedReader2);
            throw th;
        }
    }

    public static OutputStream a(OutputStream outputStream) {
        return new DeflaterOutputStream(new Base64OutputStream(outputStream, 10), new Deflater(9, true));
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception unused) {
            }
        }
    }

    public static void d(File file) {
        if (file != null) {
            try {
                if (!file.delete()) {
                    file.deleteOnExit();
                }
            } catch (Exception unused) {
            }
        }
    }

    public static <T> String a(Iterable<T> iterable, String str) {
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        for (Object next : iterable) {
            if (z) {
                sb.append(str);
            }
            sb.append(next);
            z = true;
        }
        return sb.toString();
    }

    public static StackTraceElement f() {
        StackTraceElement[] a2 = b.a();
        if (a2 != null) {
            String name = b.class.getName();
            int i = 0;
            int length = a2.length;
            while (true) {
                if (i >= length) {
                    break;
                }
                StackTraceElement stackTraceElement = a2[i];
                if (stackTraceElement == null || !name.equals(stackTraceElement.getClassName())) {
                    i++;
                } else {
                    int i2 = i + 3;
                    if (i2 < length) {
                        return a2[i2];
                    }
                }
            }
        }
        return null;
    }

    public static String a(StackTraceElement stackTraceElement) {
        if (stackTraceElement == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(stackTraceElement.getClassName());
        sb.append('.');
        sb.append(stackTraceElement.getMethodName());
        sb.append("()");
        return sb.toString();
    }

    public static Context l(Context context) {
        if (context instanceof Application) {
            return context;
        }
        try {
            if (context instanceof ContextWrapper) {
                return l(((ContextWrapper) context).getBaseContext());
            }
            if (context != null) {
                return context.getApplicationContext();
            }
            return null;
        } catch (Throwable unused) {
        }
    }

    public static boolean b(String str) {
        if (str == null) {
            return false;
        }
        try {
            String[] split = new URL(MetaData.D().p()).getHost().split("\\.");
            if (split.length > 1) {
                return str.toLowerCase(Locale.ENGLISH).contains(split[1].toLowerCase(Locale.ENGLISH));
            }
        } catch (MalformedURLException unused) {
        }
        return false;
    }

    public static boolean m(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo == null || (applicationInfo.flags & 2) == 0) {
            return false;
        }
        return true;
    }

    public static void a(Context context, boolean z, String str) {
        if (z) {
            Log.e("StartAppSDK", str);
        } else {
            Log.i("StartAppSDK", str);
        }
        if (m(context) || com.startapp.common.b.b.i(context)) {
            new e(InfoEventCategory.GENERAL).e("Log for a publisher").f(str).a(context);
        }
    }

    public static String a(Ad ad) {
        if (ad instanceof VideoEnabledAd) {
            VideoEnabledAd videoEnabledAd = (VideoEnabledAd) ad;
            if (videoEnabledAd.getType() == AdType.VIDEO) {
                return ShareConstants.VIDEO_URL;
            }
            return videoEnabledAd.getType() == AdType.REWARDED_VIDEO ? "REWARDED_VIDEO" : "INTERSTITIAL";
        } else if (ad instanceof ReturnAd) {
            return "RETURN";
        } else {
            if (ad instanceof OfferWallAd) {
                return "OFFER_WALL";
            }
            if (ad instanceof OfferWall3DAd) {
                return "OFFER_WALL_3D";
            }
            if (ad instanceof BannerStandardAd) {
                BannerStandardAd bannerStandardAd = (BannerStandardAd) ad;
                if (bannerStandardAd.d() == 0) {
                    return AdPreferences.TYPE_BANNER;
                }
                if (bannerStandardAd.d() == 1) {
                    return "MREC";
                }
                return bannerStandardAd.d() == 2 ? "COVER" : "BANNER_UNDEFINED";
            } else if (ad instanceof Banner3DAd) {
                return "BANNER_3D";
            } else {
                if (ad instanceof NativeAd) {
                    return "NATIVE";
                }
                return ad instanceof SplashAd ? "SPLASH" : "UNDEFINED";
            }
        }
    }

    public static boolean d(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }
}
