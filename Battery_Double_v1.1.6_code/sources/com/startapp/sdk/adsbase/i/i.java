package com.startapp.sdk.adsbase.i;

import android.util.JsonReader;
import android.util.JsonToken;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: StartAppSDK */
public final class i {
    public String a;
    public String b;

    /* renamed from: com.startapp.sdk.adsbase.i.i$1 reason: invalid class name */
    /* compiled from: StartAppSDK */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[JsonToken.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            a[JsonToken.BEGIN_ARRAY.ordinal()] = 1;
            a[JsonToken.BEGIN_OBJECT.ordinal()] = 2;
            a[JsonToken.STRING.ordinal()] = 3;
            a[JsonToken.NUMBER.ordinal()] = 4;
            a[JsonToken.BOOLEAN.ordinal()] = 5;
            try {
                a[JsonToken.NULL.ordinal()] = 6;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public static Object a(JsonReader jsonReader) throws IOException {
        switch (AnonymousClass1.a[jsonReader.peek().ordinal()]) {
            case 1:
                return b(jsonReader);
            case 2:
                HashMap hashMap = new HashMap();
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    hashMap.put(jsonReader.nextName(), a(jsonReader));
                }
                jsonReader.endObject();
                return hashMap;
            case 3:
                return jsonReader.nextString();
            case 4:
                return new BigDecimal(jsonReader.nextString());
            case 5:
                return Boolean.valueOf(jsonReader.nextBoolean());
            case 6:
                jsonReader.nextNull();
                return null;
            default:
                throw new IOException(String.valueOf(jsonReader.peek()));
        }
    }

    public static List<Object> b(JsonReader jsonReader) throws IOException {
        ArrayList arrayList = new ArrayList();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            arrayList.add(a(jsonReader));
        }
        jsonReader.endArray();
        return arrayList;
    }

    public i(String str, String str2) {
        this.a = str;
        this.b = str2;
    }
}
