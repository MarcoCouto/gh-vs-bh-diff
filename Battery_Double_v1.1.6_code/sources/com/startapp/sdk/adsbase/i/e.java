package com.startapp.sdk.adsbase.i;

import android.content.Context;
import android.os.SystemClock;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.c;
import com.startapp.sdk.adsbase.remoteconfig.d;

/* compiled from: StartAppSDK */
public final class e {
    private static volatile boolean a = false;

    /* compiled from: StartAppSDK */
    public static final class a implements com.startapp.common.jobrunner.interfaces.a {
        public final RunnerJob a(int i) {
            if (i == 586482792) {
                return new d();
            }
            if (i != 786564404) {
                return null;
            }
            return new c();
        }
    }

    public static void a(Context context) {
        if (!a) {
            a = true;
            com.startapp.common.jobrunner.a.a(context);
            com.startapp.common.jobrunner.a.a((com.startapp.common.jobrunner.interfaces.a) new a());
        }
    }

    public static long a() {
        return SystemClock.elapsedRealtime() + (((long) MetaData.D().l()) * ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
    }

    public static long b(Context context) {
        return SystemClock.elapsedRealtime() + (((long) MetaData.D().c(context)) * ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
    }

    public static void c(Context context) {
        a(context, Long.valueOf(a()));
    }

    public static void a(Context context, Long l) {
        new StringBuilder("setMetaDataPeriodicAlarm executes ").append(l);
        if (!j.a(context, "periodicMetadataPaused", Boolean.FALSE).booleanValue() && MetaData.D().k()) {
            a(context, 586482792, l.longValue() - SystemClock.elapsedRealtime(), "periodicMetadataTriggerTime");
        }
    }

    public static void d(Context context) {
        a(context, b(context));
    }

    public static void a(Context context, long j) {
        if (!j.a(context, "periodicInfoEventPaused", Boolean.FALSE).booleanValue() && MetaData.D().m()) {
            a(context, 786564404, j - SystemClock.elapsedRealtime(), "periodicInfoEventTriggerTime");
        }
    }

    public static void a(int i) {
        com.startapp.common.jobrunner.a.a(i, false);
    }

    private static void a(Context context, int i, long j, String str) {
        if (com.startapp.common.jobrunner.a.a(new com.startapp.common.jobrunner.RunnerRequest.a(i).a(j).b())) {
            j.b(context, str, Long.valueOf(j + SystemClock.elapsedRealtime()));
        } else {
            new com.startapp.sdk.adsbase.infoevents.e(InfoEventCategory.ERROR).e("Util.setPeriodicAlarm - failed setting alarm ".concat(String.valueOf(i))).a(context);
        }
    }
}
