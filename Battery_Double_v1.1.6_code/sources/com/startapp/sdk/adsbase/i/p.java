package com.startapp.sdk.adsbase.i;

import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import java.util.concurrent.Executor;

/* compiled from: StartAppSDK */
public final class p implements Executor {
    private final Priority a;

    public p(Priority priority) {
        this.a = priority;
    }

    public final void execute(Runnable runnable) {
        ThreadManager.a(this.a, runnable);
    }
}
