package com.startapp.sdk.adsbase.i;

import android.content.Context;
import com.startapp.sdk.adsbase.adrules.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;
import java.util.UUID;

/* compiled from: StartAppSDK */
public final class o {
    private static o a = new o();
    private String b = "";
    private long c = 0;
    private RequestReason d = RequestReason.LAUNCH;

    public final String a() {
        return this.b;
    }

    public final long b() {
        return this.c;
    }

    public final RequestReason c() {
        return this.d;
    }

    public final synchronized void a(Context context, RequestReason requestReason) {
        this.b = UUID.randomUUID().toString();
        this.c = System.currentTimeMillis();
        this.d = requestReason;
        StringBuilder sb = new StringBuilder("Starting new session: reason=");
        sb.append(requestReason);
        sb.append(" sessionId=");
        sb.append(this.b);
        if (!s.a()) {
            b.a().b();
        }
        MetaData.D().a(context, new AdPreferences(), requestReason, false, null, true);
    }

    public static o d() {
        return a;
    }
}
