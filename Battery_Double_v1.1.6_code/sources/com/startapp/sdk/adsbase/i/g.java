package com.startapp.sdk.adsbase.i;

import java.util.Comparator;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class g implements Comparator<JSONObject> {
    private final String a;

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        JSONObject jSONObject = (JSONObject) obj2;
        Object opt = ((JSONObject) obj).opt(this.a);
        Object opt2 = jSONObject.opt(this.a);
        if ((opt instanceof Comparable) && (opt2 instanceof Comparable)) {
            if (opt.getClass() == opt2.getClass()) {
                return ((Comparable) opt).compareTo(opt2);
            }
            if ((opt instanceof Number) && (opt2 instanceof Number)) {
                return Double.compare(((Number) opt).doubleValue(), ((Number) opt2).doubleValue());
            }
        }
        if (opt == JSONObject.NULL) {
            opt = null;
        }
        if (opt2 == JSONObject.NULL) {
            opt2 = null;
        }
        if (opt != null && opt2 != null) {
            return opt.toString().compareTo(opt2.toString());
        }
        if (opt != null) {
            return 1;
        }
        return opt2 != null ? -1 : 0;
    }

    public g(String str) {
        this.a = str;
    }
}
