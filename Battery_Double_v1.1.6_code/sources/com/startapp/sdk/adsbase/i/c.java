package com.startapp.sdk.adsbase.i;

import android.os.Build.VERSION;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;

/* compiled from: StartAppSDK */
public class c {
    private final Queue<Runnable> a;
    private final Executor b;
    private Runnable c;

    static {
        c.class.getSimpleName();
    }

    public c(Executor executor) {
        if (VERSION.SDK_INT >= 9) {
            this.a = new ArrayDeque();
        } else {
            this.a = new LinkedList();
        }
        this.b = executor;
    }

    public final synchronized void a(final b bVar) {
        this.a.offer(new Runnable() {
            public final void run() {
                bVar.a(new Runnable() {
                    private boolean a;

                    public final void run() {
                        synchronized (this) {
                            if (!this.a) {
                                this.a = true;
                                c.this.a();
                            }
                        }
                    }
                });
            }
        });
        if (this.c == null) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void a() {
        Runnable runnable = (Runnable) this.a.poll();
        this.c = runnable;
        if (runnable != null) {
            this.b.execute(this.c);
        }
    }
}
