package com.startapp.sdk.adsbase.i;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: StartAppSDK */
public final class a {
    /* access modifiers changed from: private */
    public static final Map<String, Bitmap> a = new ConcurrentHashMap();

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0059 A[SYNTHETIC, Splitter:B:20:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0060 A[SYNTHETIC, Splitter:B:28:0x0060] */
    private static Bitmap a(Context context, String str, boolean z) {
        InputStream inputStream;
        Bitmap bitmap = (Bitmap) a.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        m.a(context, z);
        InputStream inputStream2 = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(context.getFilesDir().getPath());
            sb.append("/");
            sb.append(str);
            inputStream = new FileInputStream(sb.toString());
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
                decodeStream.setDensity(context.getResources() != null ? context.getResources().getDisplayMetrics().densityDpi : 160);
                a.put(str, decodeStream);
                try {
                    inputStream.close();
                } catch (IOException unused) {
                }
                return decodeStream;
            } catch (Exception unused2) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused3) {
                    }
                }
                return null;
            } catch (Throwable th) {
                th = th;
                inputStream2 = inputStream;
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    } catch (IOException unused4) {
                    }
                }
                throw th;
            }
        } catch (Exception unused5) {
            inputStream = null;
            if (inputStream != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            if (inputStream2 != null) {
            }
            throw th;
        }
    }

    public static Bitmap a(Context context, String str) {
        Bitmap a2 = a(context, str, false);
        return a2 == null ? a(context, str, true) : a2;
    }

    public static void a(final Context context, final Bitmap bitmap, final String str, final String str2) {
        ThreadManager.a(Priority.DEFAULT, (Runnable) new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:20:0x006b A[SYNTHETIC, Splitter:B:20:0x006b] */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0077 A[SYNTHETIC, Splitter:B:27:0x0077] */
            public final void run() {
                Map a2 = a.a;
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(str2);
                a2.put(sb.toString(), bitmap);
                FileOutputStream fileOutputStream = null;
                try {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(context.getFilesDir().getPath());
                    sb2.append("/");
                    sb2.append(str);
                    sb2.append(str2);
                    FileOutputStream fileOutputStream2 = new FileOutputStream(sb2.toString());
                    try {
                        bitmap.compress(CompressFormat.PNG, 100, fileOutputStream2);
                        try {
                            fileOutputStream2.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e2) {
                        FileOutputStream fileOutputStream3 = fileOutputStream2;
                        e = e2;
                        fileOutputStream = fileOutputStream3;
                        try {
                            e.printStackTrace();
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e3) {
                                    e3.printStackTrace();
                                }
                            }
                        } catch (Throwable th) {
                            th = th;
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e4) {
                                    e4.printStackTrace();
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        FileOutputStream fileOutputStream4 = fileOutputStream2;
                        th = th2;
                        fileOutputStream = fileOutputStream4;
                        if (fileOutputStream != null) {
                        }
                        throw th;
                    }
                } catch (Exception e5) {
                    e = e5;
                    e.printStackTrace();
                    if (fileOutputStream != null) {
                    }
                }
            }
        });
    }

    public static boolean a(Context context, String str, String str2) {
        if (!str.endsWith(str2)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            str = sb.toString();
        }
        if (!a.containsKey(str)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(context.getFilesDir().getPath());
            sb2.append("/");
            sb2.append(str);
            if (!new File(sb2.toString()).exists()) {
                return false;
            }
        }
        return true;
    }
}
