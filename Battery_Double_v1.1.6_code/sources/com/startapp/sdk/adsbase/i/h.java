package com.startapp.sdk.adsbase.i;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.SDKException;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class h extends l {
    private JSONObject a;

    public h() {
        this.a = null;
        this.a = new JSONObject();
    }

    public final void a(String str, Object obj, boolean z, boolean z2) throws SDKException {
        if (!z || obj != null) {
            if (obj != null && !obj.toString().equals("")) {
                try {
                    this.a.put(str, obj);
                    return;
                } catch (JSONException e) {
                    if (z) {
                        StringBuilder sb = new StringBuilder("failed converting to json object value: [");
                        sb.append(obj);
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        throw new SDKException(sb.toString(), e);
                    }
                }
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder("Required key: [");
        sb2.append(str);
        sb2.append("] is missing");
        throw new SDKException(sb2.toString(), null);
    }

    public final void a(String str, Set<String> set) throws SDKException {
        if (set != null && set.size() > 0) {
            try {
                this.a.put(str, new JSONArray(set));
            } catch (JSONException unused) {
            }
        }
    }

    public final String toString() {
        return this.a.toString();
    }
}
