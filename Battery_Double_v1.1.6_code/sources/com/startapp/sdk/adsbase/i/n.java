package com.startapp.sdk.adsbase.i;

import android.os.Build.VERSION;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;

/* compiled from: StartAppSDK */
public final class n implements Executor {
    private final Queue<Runnable> a;
    private final Executor b;
    private Runnable c;

    public n(Executor executor) {
        if (VERSION.SDK_INT >= 9) {
            this.a = new ArrayDeque();
        } else {
            this.a = new LinkedList();
        }
        this.b = executor;
    }

    public final synchronized void execute(final Runnable runnable) {
        this.a.offer(new Runnable() {
            public final void run() {
                try {
                    runnable.run();
                } finally {
                    n.this.a();
                }
            }
        });
        if (this.c == null) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void a() {
        Runnable runnable = (Runnable) this.a.poll();
        this.c = runnable;
        if (runnable != null) {
            this.b.execute(this.c);
        }
    }
}
