package com.startapp.sdk.adsbase.i;

import android.os.Build.VERSION;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: StartAppSDK */
public final class q implements Iterator<Throwable> {
    private Throwable a;
    private Throwable[] b;
    private int c;
    private boolean d;

    public final boolean a() {
        return this.d;
    }

    public q(Throwable th) {
        this.a = th;
        if (VERSION.SDK_INT >= 19) {
            this.b = new Throwable[0];
        }
    }

    public final boolean hasNext() {
        return this.a != null || (this.b != null && this.c < this.b.length);
    }

    /* renamed from: b */
    public final Throwable next() {
        Throwable th = this.a;
        boolean z = false;
        this.d = false;
        if (th != null) {
            this.a = th.getCause();
        } else if (this.b != null && this.c < this.b.length) {
            if (this.c == 0) {
                z = true;
            }
            this.d = z;
            Throwable[] thArr = this.b;
            int i = this.c;
            this.c = i + 1;
            th = thArr[i];
        }
        if (th != null) {
            return th;
        }
        throw new NoSuchElementException();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
