package com.startapp.sdk.adsbase.i;

import com.startapp.common.SDKException;
import java.util.Set;

/* compiled from: StartAppSDK */
public abstract class l {
    public abstract void a(String str, Object obj, boolean z, boolean z2) throws SDKException;

    public abstract void a(String str, Set<String> set) throws SDKException;

    public final void a(String str, Object obj, boolean z) throws SDKException {
        a(str, obj, z, true);
    }
}
