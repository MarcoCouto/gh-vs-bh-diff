package com.startapp.sdk.adsbase;

import android.content.Context;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.e.a;
import com.startapp.sdk.adsbase.e.b;
import com.startapp.sdk.adsbase.i.j;
import com.startapp.sdk.adsbase.i.l;

/* compiled from: StartAppSDK */
public final class i extends c {
    private b b;
    private String c;

    public i(Context context) {
        super(1);
        this.b = a.a(context);
        this.c = com.startapp.common.b.b.j(context);
    }

    public final l a() throws SDKException {
        l a = super.a();
        if (a == null) {
            a = new j();
        }
        a.a(IronSourceConstants.EVENTS_PLACEMENT_NAME, "INAPP_DOWNLOAD", true);
        if (this.b != null) {
            a.a("install_referrer", this.b.a(), true);
            a.a("referrer_click_timestamp_seconds", Long.valueOf(this.b.b()), true);
            a.a("install_begin_timestamp_seconds", Long.valueOf(this.b.c()), true);
        }
        a.a("apkSig", this.c, true);
        return a;
    }
}
