package com.startapp.sdk.adsbase.commontracking;

import android.content.Context;
import com.startapp.common.b.a;
import com.startapp.sdk.adsbase.i.o;
import com.startapp.sdk.adsbase.k;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.b.c;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;

/* compiled from: StartAppSDK */
public class TrackingParams implements Serializable {
    private static final long serialVersionUID = 1;
    private String adTag;
    private String clientSessionId;
    private String location;
    private String nonImpressionReason;
    private int offset;
    private String profileId;

    public TrackingParams() {
        this(null);
    }

    public TrackingParams(String str) {
        this.adTag = str;
        this.clientSessionId = o.d().a();
        this.profileId = MetaData.D().y();
        this.offset = 0;
    }

    public final String f() {
        return this.adTag;
    }

    public final String g() {
        return this.profileId;
    }

    public final int h() {
        return this.offset;
    }

    public final TrackingParams a(int i) {
        this.offset = i;
        return this;
    }

    public final TrackingParams c(String str) {
        this.nonImpressionReason = str;
        return this;
    }

    public final void a(Context context) {
        k.a();
        k.h(context);
        Collection collection = (Collection) c.a(context).i().c();
        if (collection.size() > 0) {
            StringBuilder sb = new StringBuilder("&locations=");
            sb.append(d(a.c(com.startapp.sdk.c.c.a.a(collection))));
            this.location = sb.toString();
            return;
        }
        this.location = null;
    }

    /* access modifiers changed from: protected */
    public String d() {
        if (this.offset <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder("&offset=");
        sb.append(this.offset);
        return sb.toString();
    }

    protected static String d(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException unused) {
            return "";
        }
    }

    public String a() {
        String str;
        String str2;
        String str3;
        String str4;
        StringBuilder sb = new StringBuilder();
        if (this.adTag == null || this.adTag.equals("")) {
            str = "";
        } else {
            int i = 200;
            if (this.adTag.length() < 200) {
                i = this.adTag.length();
            }
            StringBuilder sb2 = new StringBuilder("&adTag=");
            sb2.append(d(this.adTag.substring(0, i)));
            str = sb2.toString();
        }
        sb.append(str);
        if (this.clientSessionId != null) {
            StringBuilder sb3 = new StringBuilder("&clientSessionId=");
            sb3.append(d(this.clientSessionId));
            str2 = sb3.toString();
        } else {
            str2 = "";
        }
        sb.append(str2);
        if (this.profileId != null) {
            StringBuilder sb4 = new StringBuilder("&profileId=");
            sb4.append(d(this.profileId));
            str3 = sb4.toString();
        } else {
            str3 = "";
        }
        sb.append(str3);
        sb.append(d());
        if (this.nonImpressionReason == null || this.nonImpressionReason.equals("")) {
            str4 = "";
        } else {
            StringBuilder sb5 = new StringBuilder("&isShown=false&reason=");
            sb5.append(d(this.nonImpressionReason));
            str4 = sb5.toString();
        }
        sb.append(str4);
        String str5 = this.location;
        if (str5 == null) {
            str5 = "";
        }
        sb.append(str5);
        return sb.toString();
    }
}
