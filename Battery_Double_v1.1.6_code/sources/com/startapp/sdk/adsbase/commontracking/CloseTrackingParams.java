package com.startapp.sdk.adsbase.commontracking;

/* compiled from: StartAppSDK */
public class CloseTrackingParams extends TrackingParams {
    private static final long serialVersionUID = 1;
    private final String DURATION_PARAM_NAME = "&displayDuration=";
    private String duration;

    public CloseTrackingParams(String str, String str2) {
        super(str2);
        this.duration = str;
    }

    public final String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.a());
        sb.append("&displayDuration=");
        sb.append(d(this.duration));
        return sb.toString();
    }
}
