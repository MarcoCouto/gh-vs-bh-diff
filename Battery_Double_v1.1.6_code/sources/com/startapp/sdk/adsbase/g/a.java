package com.startapp.sdk.adsbase.g;

import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: StartAppSDK */
public class a {
    private final Map<C0077a, String> a = new ConcurrentHashMap();

    /* renamed from: com.startapp.sdk.adsbase.g.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    static class C0077a {
        private Placement a;
        private int b;

        C0077a(Placement placement) {
            this(placement, -1);
        }

        C0077a(Placement placement, int i) {
            this.a = placement;
            this.b = i;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C0077a aVar = (C0077a) obj;
            return this.b == aVar.b && this.a == aVar.a;
        }

        public final int hashCode() {
            return s.a(this.a, Integer.valueOf(this.b));
        }
    }

    static {
        a.class.getSimpleName();
    }

    public final String a(Placement placement) {
        if (placement == null) {
            return null;
        }
        return (String) this.a.get(new C0077a(placement));
    }

    public final String a(Placement placement, int i) {
        if (placement == null) {
            return null;
        }
        return (String) this.a.get(new C0077a(placement, i));
    }

    public final void a(Placement placement, String str) {
        if (str != null) {
            this.a.put(new C0077a(placement), str);
        }
    }

    public final void a(Placement placement, int i, String str) {
        if (str != null) {
            this.a.put(new C0077a(placement, i), str);
        }
    }
}
