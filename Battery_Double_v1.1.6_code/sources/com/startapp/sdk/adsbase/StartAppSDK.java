package com.startapp.sdk.adsbase;

import android.content.Context;
import com.github.mikephil.charting.utils.Utils;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason;

public class StartAppSDK {
    public static void init(Context context, String str) {
        init(context, str, new SDKAdPreferences());
    }

    public static void init(Context context, String str, SDKAdPreferences sDKAdPreferences) {
        init(context, (String) null, str, sDKAdPreferences);
    }

    public static void init(Context context, String str, String str2) {
        init(context, str, str2, new SDKAdPreferences());
    }

    public static void init(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences) {
        init(context, str, str2, sDKAdPreferences, true);
    }

    public static void init(Context context, String str, boolean z) {
        init(context, (String) null, str, z);
    }

    public static void init(Context context, String str, String str2, boolean z) {
        init(context, str, str2, new SDKAdPreferences(), z);
    }

    public static void init(Context context, String str, SDKAdPreferences sDKAdPreferences, boolean z) {
        init(context, null, str, sDKAdPreferences, z);
    }

    public static void init(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences, boolean z) {
        k.a().a(context, str, str2, sDKAdPreferences, z);
    }

    public static void inAppPurchaseMade(Context context) {
        inAppPurchaseMade(context, Utils.DOUBLE_EPSILON);
    }

    public static void inAppPurchaseMade(Context context, double d) {
        j.b(context, "payingUser", Boolean.TRUE);
        double floatValue = (double) j.a(context, "inAppPurchaseAmount", Float.valueOf(0.0f)).floatValue();
        Double.isNaN(floatValue);
        j.b(context, "inAppPurchaseAmount", Float.valueOf((float) (floatValue + d)));
        k.a();
        k.a(context, RequestReason.IN_APP_PURCHASE);
    }

    public static void startNewSession(Context context) {
        k.a();
        k.a(context, RequestReason.CUSTOM);
    }

    public static void addWrapper(Context context, String str, String str2) {
        k.a().a(context, str, str2);
    }

    public static void enableReturnAds(boolean z) {
        k.a().d(z);
    }

    public static void setTestAdsEnabled(boolean z) {
        k.a().e(z);
    }

    private static void pauseServices(Context context) {
        k.a();
        k.b(context);
        k.a();
        k.c(context);
    }

    private static void resumeServices(Context context) {
        k.a();
        k.d(context);
        k.a();
        k.e(context);
    }

    public static void setUserConsent(Context context, String str, long j, boolean z) {
        k.a();
        k.a(context, str, z, true);
    }
}
