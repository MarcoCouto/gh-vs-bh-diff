package com.startapp.sdk.inappbrowser;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.sdk.ads.a.b;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.infoevents.e;

/* compiled from: StartAppSDK */
public final class a extends b implements OnClickListener {
    protected static boolean e = false;
    protected NavigationBarLayout c;
    protected AnimatingProgressBar d;
    private RelativeLayout f;
    private WebView g;
    private FrameLayout h;
    private String i;

    /* renamed from: com.startapp.sdk.inappbrowser.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    static class C0079a extends WebViewClient {
        private Context a;
        private a b;
        private NavigationBarLayout c;
        private AnimatingProgressBar d;
        private int e = 0;
        private boolean f = false;

        public C0079a(Context context, NavigationBarLayout navigationBarLayout, AnimatingProgressBar animatingProgressBar, a aVar) {
            this.a = context;
            this.d = animatingProgressBar;
            this.c = navigationBarLayout;
            this.b = aVar;
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            if (!a.e) {
                StringBuilder sb = new StringBuilder("IABWebViewClient::onPageStarted - [");
                sb.append(str);
                sb.append("]REDIRECTED  -> ");
                sb.append(this.e);
                sb.append(" Can go back ");
                sb.append(webView.canGoBack());
                if (this.f) {
                    this.e = 1;
                    this.d.a();
                    this.c.a(webView);
                } else {
                    this.e = Math.max(this.e, 1);
                }
                this.d.setVisibility(0);
                this.c.c().setText(str);
                this.c.a(webView);
                super.onPageStarted(webView, str, bitmap);
            }
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            StringBuilder sb = new StringBuilder("IABWebViewClient::shouldOverrideUrlLoading - [");
            sb.append(str);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            if (!a.e) {
                if (!this.f) {
                    this.f = true;
                    this.d.a();
                    this.e = 0;
                }
                this.e++;
                if (com.startapp.sdk.adsbase.a.c(str) && !com.startapp.sdk.adsbase.a.b(str)) {
                    return false;
                }
                this.e = 1;
                com.startapp.sdk.adsbase.a.c(this.a, str);
                if (this.b != null) {
                    this.b.a();
                }
            }
            return true;
        }

        public final void onPageFinished(WebView webView, String str) {
            if (!a.e) {
                StringBuilder sb = new StringBuilder("IABWebViewClient::onPageFinished - [");
                sb.append(str);
                sb.append(RequestParameters.RIGHT_BRACKETS);
                this.c.a(webView);
                int i = this.e - 1;
                this.e = i;
                if (i == 0) {
                    this.f = false;
                    this.d.a();
                    if (this.d.isShown()) {
                        this.d.setVisibility(8);
                    }
                    this.c.a(webView);
                }
                super.onPageFinished(webView, str);
            }
        }

        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            StringBuilder sb = new StringBuilder("IABWebViewClient::onReceivedError - [");
            sb.append(str);
            sb.append("], [");
            sb.append(str2);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            this.d.a();
            super.onReceivedError(webView, i, str, str2);
        }
    }

    public final void s() {
    }

    public final void u() {
    }

    public a(String str) {
        this.i = str;
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        e = false;
        this.f = new RelativeLayout(c());
        String str = this.i;
        if (this.c == null) {
            this.c = new NavigationBarLayout(c());
            this.c.a();
            this.c.b();
            this.c.setButtonsListener(this);
        }
        this.f.addView(this.c);
        this.d = new AnimatingProgressBar(c(), null, 16842872);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RectShape());
        shapeDrawable.getPaint().setColor(Color.parseColor("#45d200"));
        this.d.setProgressDrawable(new ClipDrawable(shapeDrawable, 3, 1));
        this.d.setBackgroundColor(-1);
        this.d.setId(2108);
        LayoutParams layoutParams = new LayoutParams(-1, r.a((Context) c(), 4));
        layoutParams.addRule(3, IronSourceConstants.IS_CHECK_READY_TRUE);
        this.f.addView(this.d, layoutParams);
        this.h = new FrameLayout(c());
        if (this.g == null) {
            try {
                this.g = new WebView(c());
                this.g.getSettings().setJavaScriptEnabled(true);
                this.g.getSettings().setUseWideViewPort(true);
                this.g.getSettings().setLoadWithOverviewMode(true);
                this.g.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                this.g.getSettings().setBuiltInZoomControls(true);
                if (VERSION.SDK_INT >= 11) {
                    this.g.getSettings().setDisplayZoomControls(false);
                }
                this.g.setWebViewClient(new C0079a(c(), this.c, this.d, this));
                this.g.setWebChromeClient(new WebChromeClient() {
                    public final void onProgressChanged(WebView webView, int i) {
                        a.this.d.setProgress(i);
                    }

                    public final void onReceivedTitle(WebView webView, String str) {
                        if (str != null && !str.equals("")) {
                            a.this.c.d().setText(str);
                        }
                    }
                });
                this.g.loadUrl(str);
            } catch (Throwable th) {
                new e(th).a((Context) c());
                this.c.e();
                com.startapp.sdk.adsbase.a.c(c(), str);
                c().finish();
            }
        }
        this.h.addView(this.g);
        this.h.setBackgroundColor(-1);
        LayoutParams layoutParams2 = new LayoutParams(-1, -1);
        layoutParams2.addRule(15);
        layoutParams2.addRule(3, 2108);
        this.f.addView(this.h, layoutParams2);
        if (bundle != null) {
            this.g.restoreState(bundle);
        }
        c().setContentView(this.f, new LayoutParams(-2, -2));
    }

    public final void b(Bundle bundle) {
        this.g.saveState(bundle);
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case 2104:
                if (this.g != null) {
                    com.startapp.sdk.adsbase.a.c(c(), this.g.getUrl());
                }
                break;
            case IronSourceConstants.IS_CHECK_CAPPED_TRUE /*2103*/:
                a();
                break;
            case 2105:
                if (this.g != null && this.g.canGoBack()) {
                    this.d.a();
                    this.g.goBack();
                    return;
                }
            case 2106:
                if (this.g != null && this.g.canGoForward()) {
                    this.d.a();
                    this.g.goForward();
                    return;
                }
        }
    }

    public final boolean a(int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0 || i2 != 4) {
            return super.a(i2, keyEvent);
        }
        if (this.g == null || !this.g.canGoBack()) {
            a();
        } else {
            this.d.a();
            this.g.goBack();
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        y();
        this.c.e();
        c().finish();
    }

    private void y() {
        try {
            e = true;
            this.g.stopLoading();
            this.g.removeAllViews();
            this.g.postInvalidate();
            com.startapp.common.b.b.b(this.g);
            this.g.destroy();
            this.g = null;
        } catch (Exception e2) {
            new StringBuilder("IABrowserMode::destroyWebview error ").append(e2.getMessage());
        }
    }
}
