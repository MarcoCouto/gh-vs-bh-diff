package com.startapp.sdk.inappbrowser;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ProgressBar;

/* compiled from: StartAppSDK */
public class AnimatingProgressBar extends ProgressBar {
    private static final Interpolator a = new AccelerateDecelerateInterpolator();
    private ValueAnimator b;
    private boolean c = false;

    public AnimatingProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        boolean z = false;
        if (VERSION.SDK_INT >= 11) {
            z = true;
        }
        this.c = z;
    }

    public void setProgress(int i) {
        if (!this.c) {
            super.setProgress(i);
            return;
        }
        if (this.b != null) {
            this.b.cancel();
            if (getProgress() >= i) {
                return;
            }
        } else {
            this.b = ValueAnimator.ofInt(new int[]{getProgress(), i});
            this.b.setInterpolator(a);
            this.b.addUpdateListener(new AnimatorUpdateListener() {
                private Integer a;

                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    this.a = (Integer) valueAnimator.getAnimatedValue();
                    AnimatingProgressBar.super.setProgress(this.a.intValue());
                }
            });
        }
        this.b.setIntValues(new int[]{getProgress(), i});
        this.b.start();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.b != null) {
            this.b.cancel();
        }
    }

    public final void a() {
        super.setProgress(0);
        if (this.b != null) {
            this.b.cancel();
        }
    }
}
