package com.startapp.sdk.json;

import android.content.Context;
import android.content.Intent;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.AdsConstants.AdApiType;
import com.startapp.sdk.adsbase.JsonAd;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.d;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.adsbase.model.GetAdResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public abstract class a extends d {
    private int g = 0;
    private Set<String> h = new HashSet();

    /* access modifiers changed from: protected */
    public abstract void a(Ad ad);

    public a(Context context, Ad ad, AdPreferences adPreferences, b bVar, Placement placement) {
        super(context, ad, adPreferences, bVar, placement);
    }

    /* access modifiers changed from: protected */
    public final Object e() {
        GetAdRequest a = a();
        if (a == null) {
            return null;
        }
        if (this.h.size() == 0) {
            this.h.add(this.a.getPackageName());
        }
        boolean z = false;
        if (this.g > 0) {
            a.c(false);
        }
        a.a(this.h);
        if (this.g == 0) {
            z = true;
        }
        a.c(z);
        try {
            return (GetAdResponse) com.startapp.sdk.adsbase.h.a.a(this.a, AdsConstants.a(AdApiType.JSON, f()), a, GetAdResponse.class);
        } catch (SDKException e) {
            this.f = e.getMessage();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Object obj) {
        GetAdResponse getAdResponse = (GetAdResponse) obj;
        boolean z = false;
        if (obj == null) {
            this.f = "Empty Response";
            return false;
        } else if (!getAdResponse.a()) {
            this.f = getAdResponse.b();
            StringBuilder sb = new StringBuilder("Error msg = [");
            sb.append(this.f);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            return false;
        } else {
            JsonAd jsonAd = (JsonAd) this.b;
            List a = com.iab.omid.library.startapp.b.a(this.a, getAdResponse.c(), this.g, this.h);
            jsonAd.a(a);
            jsonAd.setAdInfoOverride(getAdResponse.d());
            if (getAdResponse.c() != null && getAdResponse.c().size() > 0) {
                z = true;
            }
            if (!z) {
                this.f = "Empty Response";
            } else if (a.size() == 0 && this.g == 0) {
                this.g++;
                return d().booleanValue();
            }
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        Intent intent = new Intent("com.startapp.android.OnReceiveResponseBroadcastListener");
        intent.putExtra("adHashcode", this.b.hashCode());
        intent.putExtra("adResult", bool);
        com.startapp.common.b.a(this.a).a(intent);
        if (bool.booleanValue()) {
            a((Ad) (JsonAd) this.b);
            this.d.a(this.b);
        }
    }
}
