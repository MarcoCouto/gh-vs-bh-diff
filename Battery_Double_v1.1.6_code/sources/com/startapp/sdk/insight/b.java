package com.startapp.sdk.insight;

import android.content.Context;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.networkTest.results.BaseResult;
import com.startapp.networkTest.results.ConnectivityTestResult;
import com.startapp.networkTest.results.LatencyResult;
import com.startapp.networkTest.results.NetworkInformationResult;
import com.startapp.networkTest.startapp.ConnectivityTestListener;
import com.startapp.networkTest.startapp.CoverageMapperManager.OnNetworkInfoResultListener;
import com.startapp.sdk.adsbase.i.c;
import com.startapp.sdk.adsbase.i.p;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.Executor;

/* compiled from: StartAppSDK */
public class b implements ConnectivityTestListener, OnNetworkInfoResultListener {
    private static final Comparator<File> d = new Comparator<File>() {
        public final /* synthetic */ int compare(Object obj, Object obj2) {
            return ((File) obj2).getName().compareTo(((File) obj).getName());
        }
    };
    final Context a;
    final c b;
    final File c;
    private File e;

    static {
        b.class.getSimpleName();
    }

    public static b a(Context context) {
        j.b(context, "SuccessfulSentTimeKey", Long.valueOf(System.currentTimeMillis()));
        return new b(context.getApplicationContext(), new p(Priority.DEFAULT), new File(context.getFilesDir(), "StartApp-Events"));
    }

    private b(Context context, Executor executor, File file) {
        this.a = context;
        this.b = new c(executor);
        this.c = new File(file, "saved");
        this.e = new File(file, "sending");
    }

    private static NetworkTestsMetaData a() {
        NetworkTestsMetaData b2 = MetaData.D().b();
        return b2 == null ? new NetworkTestsMetaData() : b2;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(InfoEventCategory infoEventCategory, BaseResult baseResult, long j, Runnable runnable) {
        String str;
        try {
            str = s.b((Object) baseResult);
        } catch (Throwable th) {
            new e(th).a(this.a);
            str = null;
        }
        final String str2 = str;
        if (str2 != null) {
            if (s.d(this.a)) {
                e eVar = new e(infoEventCategory);
                eVar.a(j);
                eVar.f(str2);
                eVar.l(a(infoEventCategory));
                Context context = this.a;
                final InfoEventCategory infoEventCategory2 = infoEventCategory;
                final long j2 = j;
                final Runnable runnable2 = runnable;
                AnonymousClass5 r0 = new Object() {
                    public final void a(e eVar, boolean z) {
                        if (!z) {
                            b.this.b.a(this);
                        }
                    }

                    public final void a() {
                        runnable2.run();
                    }

                    public final void a(Runnable runnable) {
                        try {
                            b.this.a(infoEventCategory2, str2, j2);
                        } catch (Throwable th) {
                            new e(th).a(b.this.a);
                        } finally {
                            runnable.run();
                        }
                    }
                };
                eVar.a(context, r0);
                return false;
            }
            try {
                a(infoEventCategory, str2, j);
            } catch (Throwable th2) {
                new e(th2).a(this.a);
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public final void a(InfoEventCategory infoEventCategory, String str, long j) throws IOException {
        if (this.c.exists() || this.c.mkdirs()) {
            File file = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append(j);
            sb.append("-");
            sb.append(infoEventCategory.a());
            PrintStream printStream = new PrintStream(new File(file, sb.toString()));
            printStream.print(str);
            printStream.close();
            File[] listFiles = this.c.listFiles();
            if (listFiles != null) {
                int r = a().r();
                if (listFiles.length > r && r > 10) {
                    Arrays.sort(listFiles, d);
                    int length = listFiles.length;
                    for (int min = Math.min(Math.max(10, a().q()), r); min < length; min++) {
                        a(listFiles[min]);
                    }
                    return;
                }
                return;
            }
            return;
        }
        throw new IOException();
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(final Runnable runnable) {
        File[] listFiles = this.c.listFiles();
        e eVar = null;
        if (listFiles != null) {
            Arrays.sort(listFiles, d);
            long currentTimeMillis = System.currentTimeMillis() - a().m();
            e eVar2 = null;
            e eVar3 = null;
            for (File file : listFiles) {
                int indexOf = file.getName().indexOf("-");
                if (indexOf < 0) {
                    a(file);
                } else {
                    InfoEventCategory a2 = InfoEventCategory.a(file.getName().substring(indexOf + 1));
                    if (a2 == null) {
                        a(file);
                    } else {
                        try {
                            long parseLong = Long.parseLong(file.getName().substring(0, indexOf));
                            if (parseLong < currentTimeMillis) {
                                a(file);
                            } else {
                                File a3 = a(file, this.e);
                                if (a3 == null) {
                                    a(file);
                                } else {
                                    e eVar4 = new e(a2);
                                    eVar4.a(parseLong);
                                    eVar4.a(a3);
                                    eVar4.l(a(a2));
                                    if (eVar2 == null) {
                                        eVar2 = eVar4;
                                    }
                                    if (eVar3 != null) {
                                        eVar3.a(eVar4);
                                    }
                                    eVar3 = eVar4;
                                }
                            }
                        } catch (NumberFormatException unused) {
                            a(file);
                        }
                    }
                }
            }
            eVar = eVar2;
        }
        if (eVar == null) {
            return true;
        }
        eVar.a(this.a, new com.startapp.sdk.adsbase.infoevents.c() {
            public final void a(e eVar, final boolean z) {
                final File i = eVar.i();
                if (i != null) {
                    b.this.b.a(new com.startapp.sdk.adsbase.i.b() {
                        public final void a(Runnable runnable) {
                            try {
                                if (z) {
                                    b.a(i);
                                } else if (b.a(i, b.this.c) == null) {
                                    b.a(i);
                                }
                            } finally {
                                runnable.run();
                            }
                        }
                    });
                }
            }

            public final void a() {
                runnable.run();
            }
        });
        return false;
    }

    private static String a(InfoEventCategory infoEventCategory) {
        switch (infoEventCategory) {
            case INSIGHT_CORE_CT:
                return a().n();
            case INSIGHT_CORE_LT:
                return a().o();
            case INSIGHT_CORE_NIR:
                return a().p();
            default:
                return null;
        }
    }

    static void a(File file) {
        if (!file.delete()) {
            file.deleteOnExit();
        }
    }

    private void a(InfoEventCategory infoEventCategory, BaseResult baseResult, long j) {
        c cVar = this.b;
        final InfoEventCategory infoEventCategory2 = infoEventCategory;
        final BaseResult baseResult2 = baseResult;
        final long j2 = j;
        AnonymousClass2 r1 = new com.startapp.sdk.adsbase.i.b() {
            public final void a(Runnable runnable) {
                try {
                    if (!b.this.a(infoEventCategory2, baseResult2, j2, runnable)) {
                    }
                } finally {
                    runnable.run();
                }
            }
        };
        cVar.a(r1);
    }

    public void onConnectivityTestResult(ConnectivityTestResult connectivityTestResult) {
        if (connectivityTestResult != null) {
            a(InfoEventCategory.INSIGHT_CORE_CT, (BaseResult) connectivityTestResult, System.currentTimeMillis());
        }
    }

    public void onLatencyTestResult(LatencyResult latencyResult) {
        if (latencyResult != null) {
            a(InfoEventCategory.INSIGHT_CORE_LT, (BaseResult) latencyResult, System.currentTimeMillis());
        }
    }

    public void onNetworkInfoResult(NetworkInformationResult networkInformationResult) {
        if (networkInformationResult != null) {
            a(InfoEventCategory.INSIGHT_CORE_NIR, (BaseResult) networkInformationResult, System.currentTimeMillis());
        }
    }

    public void onConnectivityTestFinished(final Runnable runnable) {
        this.b.a(new com.startapp.sdk.adsbase.i.b() {
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0052 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:9:0x004e A[DONT_GENERATE] */
            public final void a(Runnable runnable) {
                boolean z;
                try {
                    long currentTimeMillis = System.currentTimeMillis();
                    long longValue = currentTimeMillis - j.a(b.this.a, "SuccessfulSentTimeKey", Long.valueOf(currentTimeMillis)).longValue();
                    if (!s.d(b.this.a)) {
                        if (longValue <= MetaData.D().b().k()) {
                            z = true;
                            if (z) {
                                return;
                            }
                            return;
                        }
                    }
                    j.b(b.this.a, "SuccessfulSentTimeKey", Long.valueOf(System.currentTimeMillis()));
                    z = b.this.a(runnable);
                    if (z) {
                    }
                } finally {
                    runnable.run();
                }
            }
        });
        if (runnable != null) {
            this.b.a(new com.startapp.sdk.adsbase.i.b() {
                public final void a(Runnable runnable) {
                    try {
                        runnable.run();
                    } finally {
                        runnable.run();
                    }
                }
            });
        }
    }

    static File a(File file, File file2) {
        if (!file2.exists() && !file2.mkdirs()) {
            return null;
        }
        File file3 = new File(file2, file.getName());
        if (file.renameTo(file3)) {
            return file3;
        }
        return null;
    }
}
