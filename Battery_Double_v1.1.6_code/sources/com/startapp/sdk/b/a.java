package com.startapp.sdk.b;

/* compiled from: StartAppSDK */
public abstract class a<T> {
    private volatile T a;

    /* access modifiers changed from: protected */
    public abstract T a();

    public final T b() {
        T t = this.a;
        if (t == null) {
            synchronized (this) {
                t = this.a;
                if (t == null) {
                    t = a();
                    this.a = t;
                }
            }
        }
        return t;
    }
}
