package com.startapp.sdk.b;

import android.content.Context;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.common.a.d;
import com.startapp.sdk.adsbase.i.f;
import com.startapp.sdk.adsbase.i.p;
import com.startapp.sdk.adsbase.remoteconfig.LocationConfig;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.RscMetadata;
import com.startapp.sdk.c.b.b;
import com.startapp.sdk.f.a;
import com.startapp.sdk.triggeredlinks.TriggeredLinksMetadata;

/* compiled from: StartAppSDK */
public final class c {
    private static final b<c, Context> a = new b<c, Context>() {
        /* access modifiers changed from: protected */
        public final /* synthetic */ Object a(Object obj) {
            return new c((Context) obj);
        }
    };
    private final a<b> b;
    private final a<com.startapp.sdk.c.a.b> c;
    private final a<com.startapp.sdk.c.d.c> d;
    private final a<d> e;
    private final a<a> f;
    private final a<com.startapp.sdk.adsbase.consent.a> g;
    private final a<com.startapp.sdk.e.a> h;
    private final a<com.startapp.sdk.adsbase.g.a> i = new a<com.startapp.sdk.adsbase.g.a>() {
        /* access modifiers changed from: protected */
        public final /* synthetic */ Object a() {
            return new com.startapp.sdk.adsbase.g.a();
        }
    };
    private final a<com.startapp.sdk.c.c.a> j;
    private final a<com.startapp.sdk.triggeredlinks.c> k;
    private final a<com.startapp.sdk.adsbase.b> l;

    static {
        c.class.getSimpleName();
    }

    public static c a(Context context) {
        return (c) a.b(context.getApplicationContext());
    }

    public final b a() {
        return (b) this.b.b();
    }

    public final com.startapp.sdk.c.a.b b() {
        return (com.startapp.sdk.c.a.b) this.c.b();
    }

    public final com.startapp.sdk.c.d.c c() {
        return (com.startapp.sdk.c.d.c) this.d.b();
    }

    public final d d() {
        return (d) this.e.b();
    }

    public final a e() {
        return (a) this.f.b();
    }

    public final com.startapp.sdk.adsbase.consent.a f() {
        return (com.startapp.sdk.adsbase.consent.a) this.g.b();
    }

    public final com.startapp.sdk.e.a g() {
        return (com.startapp.sdk.e.a) this.h.b();
    }

    public final com.startapp.sdk.adsbase.g.a h() {
        return (com.startapp.sdk.adsbase.g.a) this.i.b();
    }

    public final com.startapp.sdk.c.c.a i() {
        return (com.startapp.sdk.c.c.a) this.j.b();
    }

    public final com.startapp.sdk.triggeredlinks.c j() {
        return (com.startapp.sdk.triggeredlinks.c) this.k.b();
    }

    public final com.startapp.sdk.adsbase.b k() {
        return (com.startapp.sdk.adsbase.b) this.l.b();
    }

    protected c(final Context context) {
        this.b = new a<b>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new b(context);
            }
        };
        this.c = new a<com.startapp.sdk.c.a.b>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.c.a.b(context);
            }
        };
        this.d = new a<com.startapp.sdk.c.d.c>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.c.d.c(context);
            }
        };
        this.e = new a<d>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new d(context);
            }
        };
        this.f = new a<a>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new a(context, new f<RscMetadata>() {
                    public final /* synthetic */ Object a() {
                        return MetaData.D().a();
                    }
                });
            }
        };
        this.g = new a<com.startapp.sdk.adsbase.consent.a>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.adsbase.consent.a(context);
            }
        };
        this.h = new a<com.startapp.sdk.e.a>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.e.a(context.getSharedPreferences("StartApp-54ff24db2aee60b9", 0));
            }
        };
        this.j = new a<com.startapp.sdk.c.c.a>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.c.c.a(context, new f<LocationConfig>() {
                    public final /* synthetic */ Object a() {
                        return MetaData.D().B();
                    }
                });
            }
        };
        this.k = new a<com.startapp.sdk.triggeredlinks.c>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                com.startapp.sdk.triggeredlinks.c cVar = new com.startapp.sdk.triggeredlinks.c(context, context.getSharedPreferences("StartApp-fba1a5307d96ef31", 0), new p(Priority.DEFAULT), c.a(context).d(), new f<TriggeredLinksMetadata>() {
                    public final /* synthetic */ Object a() {
                        return MetaData.D().c();
                    }
                });
                return cVar;
            }
        };
        this.l = new a<com.startapp.sdk.adsbase.b>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.adsbase.b(context.getSharedPreferences("StartApp-790ba54ab8e69f2f", 0));
            }
        };
    }
}
