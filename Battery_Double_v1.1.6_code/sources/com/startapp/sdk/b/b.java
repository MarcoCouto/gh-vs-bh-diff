package com.startapp.sdk.b;

/* compiled from: StartAppSDK */
public abstract class b<T, A> {
    private volatile T a;

    /* access modifiers changed from: protected */
    public abstract T a(A a2);

    public final T b(A a2) {
        T t = this.a;
        if (t == null) {
            synchronized (this) {
                t = this.a;
                if (t == null) {
                    T a3 = a(a2);
                    this.a = a3;
                    t = a3;
                }
            }
        }
        return t;
    }
}
