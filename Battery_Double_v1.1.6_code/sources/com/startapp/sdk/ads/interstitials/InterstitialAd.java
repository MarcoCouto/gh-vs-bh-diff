package com.startapp.sdk.ads.interstitials;

import android.content.Context;
import android.content.Intent;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.startapp.sdk.ads.splash.SplashAd;
import com.startapp.sdk.adsbase.Ad.AdState;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.VideoConfig.BackMode;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.activities.AppWallActivity;
import com.startapp.sdk.adsbase.activities.FullScreenActivity;
import com.startapp.sdk.adsbase.activities.OverlayActivity;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.b.c;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;

/* compiled from: StartAppSDK */
public abstract class InterstitialAd extends HtmlAd implements f {
    private static final long serialVersionUID = 1;

    /* access modifiers changed from: protected */
    public boolean a() {
        return false;
    }

    public InterstitialAd(Context context, Placement placement) {
        super(context, placement);
    }

    /* JADX WARNING: type inference failed for: r1v14, types: [java.lang.Boolean[], java.io.Serializable] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v14, types: [java.lang.Boolean[], java.io.Serializable]
  assigns: [java.lang.Boolean[]]
  uses: [java.io.Serializable]
  mth insns count: 179
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final boolean a(String str) {
        Class<FullScreenActivity> cls;
        String b = a.b();
        if (!a() || !AdsCommonMetaData.a().I().a().equals(BackMode.DISABLED) || !b.equals("back")) {
            if (!AdsConstants.b.booleanValue()) {
                setState(AdState.UN_INITIALIZED);
            }
            if (j() == null) {
                a(NotDisplayedReason.INTERNAL_ERROR);
                return false;
            } else if (super.e_()) {
                a(NotDisplayedReason.AD_EXPIRED);
                return false;
            } else {
                boolean a = this.activityExtra != null ? this.activityExtra.a() : false;
                Context context = this.a;
                if (((r() != 0 && r() != this.a.getResources().getConfiguration().orientation) || a() || v() || b.equals("back")) && s.a(getContext(), FullScreenActivity.class)) {
                    cls = FullScreenActivity.class;
                } else {
                    cls = s.a(getContext(), OverlayActivity.class, AppWallActivity.class);
                }
                Intent intent = new Intent(context, cls);
                intent.putExtra("fileUrl", "exit.html");
                String[] strArr = this.trackingUrls;
                String a2 = a.a();
                for (int i = 0; i < strArr.length; i++) {
                    if (strArr[i] != null && !"".equals(strArr[i])) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(strArr[i]);
                        sb.append(a2);
                        strArr[i] = sb.toString();
                    }
                }
                intent.putExtra("tracking", strArr);
                intent.putExtra("trackingClickUrl", q());
                intent.putExtra("packageNames", s());
                intent.putExtra("htmlUuid", k());
                intent.putExtra("smartRedirect", this.smartRedirect);
                intent.putExtra("browserEnabled", this.inAppBrowserEnabled);
                intent.putExtra(IronSourceConstants.EVENTS_PLACEMENT_NAME, this.placement.a());
                intent.putExtra("adInfoOverride", getAdInfoOverride());
                intent.putExtra("ad", this);
                intent.putExtra("videoAd", a());
                intent.putExtra(Events.CREATIVE_FULLSCREEN, a);
                intent.putExtra("orientation", r() == 0 ? this.a.getResources().getConfiguration().orientation : r());
                intent.putExtra("adTag", str);
                intent.putExtra("lastLoadTime", super.b());
                intent.putExtra("adCacheTtl", super.c());
                intent.putExtra("closingUrl", m());
                intent.putExtra("rewardDuration", o());
                intent.putExtra("rewardedHideTimer", p());
                if (t() != null) {
                    intent.putExtra("delayImpressionSeconds", t());
                }
                intent.putExtra("sendRedirectHops", u());
                intent.putExtra("mraidAd", v());
                if (v()) {
                    intent.putExtra("activityShouldLockOrientation", false);
                }
                if (s.a(8) && (this instanceof SplashAd)) {
                    intent.putExtra("isSplash", true);
                }
                intent.putExtra(ParametersKeys.POSITION, b);
                intent.addFlags(343932928);
                com.startapp.sdk.adsbase.consent.a f = c.a(this.a).f();
                if (f.b()) {
                    f.a(intent);
                } else {
                    this.a.startActivity(intent);
                }
                return true;
            }
        } else {
            a(NotDisplayedReason.VIDEO_BACK);
            return false;
        }
    }

    public final Long b() {
        return super.b();
    }

    public final Long c() {
        return super.c();
    }

    public final boolean e_() {
        return super.e_();
    }

    public final boolean e() {
        return super.e();
    }

    public final void a(boolean z) {
        super.a(z);
    }
}
