package com.startapp.sdk.ads.interstitials;

import android.content.Context;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.cache.CacheMetaData;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public class ReturnAd extends InterstitialAd {
    private static final long serialVersionUID = 1;

    public ReturnAd(Context context) {
        super(context, Placement.INAPP_RETURN);
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        new b(this.a, this, adPreferences, bVar).c();
    }

    /* access modifiers changed from: protected */
    public final long f() {
        return CacheMetaData.a().b().b();
    }
}
