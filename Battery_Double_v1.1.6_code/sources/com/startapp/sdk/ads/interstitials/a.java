package com.startapp.sdk.ads.interstitials;

import android.content.Context;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class a extends com.startapp.sdk.d.a {
    public a(Context context, HtmlAd htmlAd, AdPreferences adPreferences, b bVar) {
        super(context, htmlAd, adPreferences, bVar, Placement.INAPP_OVERLAY, true);
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        a(bool.booleanValue());
    }
}
