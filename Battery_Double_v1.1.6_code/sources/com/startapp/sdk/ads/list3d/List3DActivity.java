package com.startapp.sdk.ads.list3d;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.startapp.common.b;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject.Size;
import com.startapp.sdk.adsbase.adinformation.AdInformationOverrides;
import com.startapp.sdk.adsbase.commontracking.CloseTrackingParams;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.k;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.List;

/* compiled from: StartAppSDK */
public class List3DActivity extends Activity implements h {
    String a;
    String b;
    List<d> c;
    private List3DView d;
    private ProgressDialog e = null;
    private WebView f = null;
    private int g;
    private AdInformationObject h;
    private Long i;
    private Long j;
    private String k;
    private long l = 0;
    private long m = 0;
    private BroadcastReceiver n = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            List3DActivity.this.finish();
        }
    };

    public void onCreate(Bundle bundle) {
        View view;
        try {
            overridePendingTransition(0, 0);
            super.onCreate(bundle);
            if (getIntent().getBooleanExtra(Events.CREATIVE_FULLSCREEN, false)) {
                requestWindowFeature(1);
                getWindow().setFlags(1024, 1024);
            }
            if (bundle == null) {
                b.a((Context) this).a(new Intent("com.startapp.android.ShowDisplayBroadcastListener"));
                this.i = (Long) getIntent().getSerializableExtra("lastLoadTime");
                this.j = (Long) getIntent().getSerializableExtra("adCacheTtl");
            } else {
                if (bundle.containsKey("lastLoadTime")) {
                    this.i = (Long) bundle.getSerializable("lastLoadTime");
                }
                if (bundle.containsKey("adCacheTtl")) {
                    this.j = (Long) bundle.getSerializable("adCacheTtl");
                }
            }
            this.k = getIntent().getStringExtra(ParametersKeys.POSITION);
            this.a = getIntent().getStringExtra("listModelUuid");
            b.a((Context) this).a(this.n, new IntentFilter("com.startapp.android.CloseAdActivity"));
            this.g = getResources().getConfiguration().orientation;
            s.a((Activity) this, true);
            boolean booleanExtra = getIntent().getBooleanExtra("overlay", false);
            requestWindowFeature(1);
            this.b = getIntent().getStringExtra("adTag");
            int e2 = AdsCommonMetaData.a().e();
            int f2 = AdsCommonMetaData.a().f();
            this.d = new List3DView(this, this.b, this.a);
            this.d.setBackgroundDrawable(new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{e2, f2}));
            this.c = g.a().a(this.a).e();
            if (this.c == null) {
                finish();
                return;
            }
            if (booleanExtra) {
                b.a((Context) this).a(this.d.p, new IntentFilter("com.startapp.android.Activity3DGetValues"));
            } else {
                this.d.setStarted();
                this.d.setHint(true);
                this.d.setFade(true);
            }
            c cVar = new c(this, this.c, this.b, this.a);
            g.a().a(this.a).a(this, !booleanExtra);
            this.d.setAdapter(cVar);
            this.d.setDynamics(new i());
            this.d.setOnItemClickListener(new OnItemClickListener() {
                public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    int i2 = i;
                    String b = ((d) List3DActivity.this.c.get(i2)).b();
                    String e = ((d) List3DActivity.this.c.get(i2)).e();
                    String f = ((d) List3DActivity.this.c.get(i2)).f();
                    boolean k = ((d) List3DActivity.this.c.get(i2)).k();
                    boolean l = ((d) List3DActivity.this.c.get(i2)).l();
                    String o = ((d) List3DActivity.this.c.get(i2)).o();
                    String n = ((d) List3DActivity.this.c.get(i2)).n();
                    Boolean r = ((d) List3DActivity.this.c.get(i2)).r();
                    g.a().a(List3DActivity.this.a).a(((d) List3DActivity.this.c.get(i2)).c());
                    if (o == null || TextUtils.isEmpty(o)) {
                        if (!TextUtils.isEmpty(b)) {
                            boolean a2 = a.a(List3DActivity.this.getApplicationContext(), Placement.INAPP_OFFER_WALL);
                            if (!k || a2) {
                                a.a(List3DActivity.this, b, e, List3DActivity.this.a(), l && !a2, false);
                                List3DActivity.this.finish();
                                return;
                            }
                            a.a(List3DActivity.this, b, e, f, List3DActivity.this.a(), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), l, r, false, new Runnable() {
                                public final void run() {
                                    List3DActivity.this.finish();
                                }
                            });
                        }
                        return;
                    }
                    a.a(o, n, b, (Context) List3DActivity.this, new TrackingParams(List3DActivity.this.b));
                    List3DActivity.this.finish();
                }
            });
            RelativeLayout relativeLayout = new RelativeLayout(this);
            relativeLayout.setContentDescription("StartApp Ad");
            relativeLayout.setId(1475346432);
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(1);
            relativeLayout.addView(linearLayout, layoutParams2);
            RelativeLayout relativeLayout2 = new RelativeLayout(this);
            relativeLayout2.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
            relativeLayout2.setBackgroundColor(AdsCommonMetaData.a().i().intValue());
            linearLayout.addView(relativeLayout2);
            TextView textView = new TextView(this);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams3.addRule(13);
            textView.setLayoutParams(layoutParams3);
            textView.setPadding(0, r.a((Context) this, 2), 0, r.a((Context) this, 5));
            textView.setTextColor(AdsCommonMetaData.a().l().intValue());
            textView.setTextSize((float) AdsCommonMetaData.a().k().intValue());
            textView.setSingleLine(true);
            textView.setEllipsize(TruncateAt.END);
            textView.setText(AdsCommonMetaData.a().j());
            textView.setShadowLayer(2.5f, -2.0f, 2.0f, -11513776);
            r.a(textView, AdsCommonMetaData.a().m());
            relativeLayout2.addView(textView);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams4.addRule(11);
            layoutParams4.addRule(15);
            Bitmap a2 = com.startapp.sdk.adsbase.i.a.a(this, "close_button.png");
            if (a2 != null) {
                view = new ImageButton(this, null, 16973839);
                ((ImageButton) view).setImageBitmap(Bitmap.createScaledBitmap(a2, r.a((Context) this, 36), r.a((Context) this, 36), true));
            } else {
                view = new TextView(this);
                ((TextView) view).setText("   x   ");
                ((TextView) view).setTextSize(20.0f);
            }
            view.setLayoutParams(layoutParams4);
            view.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    a.b((Context) List3DActivity.this, List3DActivity.this.b(), List3DActivity.this.a());
                    List3DActivity.this.finish();
                }
            });
            view.setContentDescription(AvidJSONUtil.KEY_X);
            view.setId(1475346435);
            relativeLayout2.addView(view);
            View view2 = new View(this);
            view2.setLayoutParams(new LinearLayout.LayoutParams(-1, r.a((Context) this, 2)));
            view2.setBackgroundColor(AdsCommonMetaData.a().n().intValue());
            linearLayout.addView(view2);
            LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, 0);
            layoutParams5.weight = 1.0f;
            this.d.setLayoutParams(layoutParams5);
            linearLayout.addView(this.d);
            LinearLayout linearLayout2 = new LinearLayout(this);
            LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams6.gravity = 80;
            linearLayout2.setLayoutParams(layoutParams6);
            linearLayout2.setBackgroundColor(AdsCommonMetaData.a().w().intValue());
            linearLayout2.setGravity(17);
            linearLayout.addView(linearLayout2);
            TextView textView2 = new TextView(this);
            textView2.setTextColor(AdsCommonMetaData.a().x().intValue());
            textView2.setPadding(0, r.a((Context) this, 2), 0, r.a((Context) this, 3));
            textView2.setText("Powered By ");
            textView2.setTextSize(16.0f);
            linearLayout2.addView(textView2);
            ImageView imageView = new ImageView(this);
            imageView.setImageBitmap(Bitmap.createScaledBitmap(com.startapp.sdk.adsbase.i.a.a(this, "logo.png"), r.a((Context) this, 56), r.a((Context) this, 12), true));
            linearLayout2.addView(imageView);
            this.h = new AdInformationObject(this, Size.LARGE, Placement.INAPP_OFFER_WALL, (AdInformationOverrides) getIntent().getSerializableExtra("adInfoOverride"));
            this.h.a(relativeLayout);
            setContentView(relativeLayout, layoutParams);
            new Handler().postDelayed(new Runnable() {
                public final void run() {
                    List3DActivity.this.sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
                }
            }, 500);
        } catch (Throwable th) {
            new e(th).a((Context) this);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final TrackingParams a() {
        this.l = System.currentTimeMillis();
        double d2 = (double) (this.l - this.m);
        Double.isNaN(d2);
        return new CloseTrackingParams(String.valueOf(d2 / 1000.0d), this.b);
    }

    /* access modifiers changed from: protected */
    public final String b() {
        if (this.c == null || this.c.isEmpty()) {
            return "";
        }
        return ((d) this.c.get(0)).d() != null ? ((d) this.c.get(0)).d() : "";
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        boolean z = false;
        if (!(this.i == null || this.j == null || System.currentTimeMillis() - this.i.longValue() <= this.j.longValue())) {
            z = true;
        }
        if (z) {
            finish();
            return;
        }
        k.a().a(true);
        this.m = System.currentTimeMillis();
        g.a().a(this.a).c();
    }

    public void onBackPressed() {
        g.a().a(this.a).d();
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        s.a((Activity) this, false);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        g.a().a(this.a).b();
        if (this.h != null && this.h.b()) {
            this.h.e();
        }
        overridePendingTransition(0, 0);
        if (this.k != null && this.k.equals("back")) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.i != null) {
            bundle.putSerializable("lastLoadTime", this.i);
        }
        if (this.j != null) {
            bundle.putSerializable("adCacheTtl", this.j);
        }
    }

    public final void a(int i2) {
        View childAt = this.d.getChildAt(i2 - this.d.a());
        if (childAt != null) {
            e eVar = (e) childAt.getTag();
            f a2 = g.a().a(this.a);
            if (a2 != null && a2.e() != null && i2 < a2.e().size()) {
                d dVar = (d) a2.e().get(i2);
                eVar.b().setImageBitmap(a2.a(i2, dVar.a(), dVar.i()));
                eVar.b().requestLayout();
                eVar.a(dVar.p());
            }
        }
    }

    public void finish() {
        try {
            this.l = System.currentTimeMillis();
            a.b((Context) this, b(), a());
            k.a().a(false);
            if (this.g == getResources().getConfiguration().orientation) {
                b.a((Context) this).a(new Intent("com.startapp.android.HideDisplayBroadcastListener"));
            }
            synchronized (this) {
                if (this.n != null) {
                    b.a((Context) this).a(this.n);
                    this.n = null;
                }
            }
            if (this.a != null) {
                g.a().a(this.a).d();
                if (!AdsConstants.b.booleanValue()) {
                    g.a().b(this.a);
                }
            }
        } catch (Throwable th) {
            new e(th).a((Context) this);
        }
        super.finish();
    }
}
