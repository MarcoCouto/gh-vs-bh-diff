package com.startapp.sdk.ads.list3d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ironsource.sdk.constants.Constants.RequestParameters;

/* compiled from: StartAppSDK */
final class i extends a implements Parcelable {
    public static final Creator<i> CREATOR = new Creator<i>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new i[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new i(parcel);
        }
    };
    private float c;
    private float d;

    public final int describeContents() {
        return 0;
    }

    public i() {
        this.c = 0.9f;
        this.d = 0.6f;
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        this.b += d() * this.d;
        this.a += (this.b * ((float) i)) / 1000.0f;
        this.b *= this.c;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeFloat(this.c);
        parcel.writeFloat(this.d);
    }

    public i(Parcel parcel) {
        super(parcel);
        this.c = parcel.readFloat();
        this.d = parcel.readFloat();
    }

    public final void a(double d2) {
        super.a(d2);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(", Friction: [");
        sb.append(this.c);
        sb.append("], Snap:[");
        sb.append(this.d);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
