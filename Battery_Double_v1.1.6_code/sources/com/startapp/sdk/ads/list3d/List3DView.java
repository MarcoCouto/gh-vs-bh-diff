package com.startapp.sdk.ads.list3d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.b;
import java.util.LinkedList;

/* compiled from: StartAppSDK */
public class List3DView extends AdapterView<Adapter> {
    private int A = Integer.MIN_VALUE;
    private boolean B = false;
    private boolean C = false;
    private boolean D = false;
    protected int a = 0;
    protected int b;
    protected int c;
    protected int d;
    protected int e;
    protected int f;
    protected int g;
    protected int h;
    protected int i;
    protected a j;
    protected float k = 0.0f;
    protected boolean l = false;
    protected boolean m = false;
    protected String n;
    protected String o;
    public BroadcastReceiver p = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            double height = (double) List3DView.this.getHeight();
            double intExtra = (double) intent.getIntExtra("getHeight", List3DView.this.getHeight());
            Double.isNaN(height);
            Double.isNaN(intExtra);
            double d = height / intExtra;
            StringBuilder sb = new StringBuilder();
            sb.append(List3DView.this.q);
            sb.append("Updating Position with Ratio: [");
            sb.append(d);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            List3DView.this.a = intent.getIntExtra("mTouchState", List3DView.this.a);
            List3DView.this.b = intent.getIntExtra("mTouchStartX", List3DView.this.b);
            List3DView.this.c = intent.getIntExtra("mTouchStartY", List3DView.this.c);
            List3DView.this.g = intent.getIntExtra("mListRotation", List3DView.this.g);
            List3DView list3DView = List3DView.this;
            double intExtra2 = (double) intent.getIntExtra("mFirstItemPosition", List3DView.this.h);
            Double.isNaN(intExtra2);
            list3DView.h = (int) (intExtra2 * d);
            List3DView.this.h--;
            List3DView list3DView2 = List3DView.this;
            double intExtra3 = (double) intent.getIntExtra("mLastItemPosition", List3DView.this.i);
            Double.isNaN(intExtra3);
            list3DView2.i = (int) (intExtra3 * d);
            List3DView.this.i--;
            List3DView list3DView3 = List3DView.this;
            double intExtra4 = (double) intent.getIntExtra("mListTop", List3DView.this.e);
            Double.isNaN(intExtra4);
            list3DView3.e = (int) (intExtra4 * d);
            List3DView list3DView4 = List3DView.this;
            double intExtra5 = (double) intent.getIntExtra("mListTopStart", List3DView.this.d);
            Double.isNaN(intExtra5);
            list3DView4.d = (int) (intExtra5 * d);
            List3DView list3DView5 = List3DView.this;
            double intExtra6 = (double) intent.getIntExtra("mListTopOffset", List3DView.this.f);
            Double.isNaN(intExtra6);
            list3DView5.f = (int) (intExtra6 * d);
            List3DView.this.j = (a) intent.getParcelableExtra("mDynamics");
            List3DView.this.k = intent.getFloatExtra("mLastVelocity", List3DView.this.k);
            List3DView.this.j.a(d);
            List3DView.this.setAdapter(new c(List3DView.this.getContext(), intent.getParcelableArrayListExtra("list"), List3DView.this.n, List3DView.this.o));
            List3DView.this.l = true;
            List3DView.this.m = true;
            List3DView.this.a(List3DView.this.k, true);
            b.a(context).a((BroadcastReceiver) this);
        }
    };
    /* access modifiers changed from: private */
    public String q = "List3DView";
    private Adapter r;
    private VelocityTracker s;
    private Runnable t;
    private final LinkedList<View> u = new LinkedList<>();
    private Runnable v;
    private Rect w;
    private Camera x;
    private Matrix y;
    private Paint z;

    public View getSelectedView() {
        return null;
    }

    public List3DView(Context context, String str, String str2) {
        super(context, null);
        this.n = str;
        this.o = str2;
    }

    public void setTag(String str) {
        this.q = str;
    }

    public void setStarted() {
        this.l = true;
    }

    public void setHint(boolean z2) {
        this.C = z2;
    }

    public void setFade(boolean z2) {
        this.B = z2;
    }

    public Adapter getAdapter() {
        return this.r;
    }

    public void setSelection(int i2) {
        throw new UnsupportedOperationException("Not supported");
    }

    public void setDynamics(a aVar) {
        if (this.j != null) {
            aVar.a(this.j.a(), this.j.b(), AnimationUtils.currentAnimationTimeMillis());
        }
        this.j = aVar;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (getChildCount() == 0) {
            return false;
        }
        float f2 = 0.0f;
        switch (motionEvent.getAction()) {
            case 0:
                if (com.startapp.common.b.b.a()) {
                    com.startapp.common.b.b.a((View) this);
                }
                removeCallbacks(this.t);
                this.b = (int) motionEvent.getX();
                this.c = (int) motionEvent.getY();
                this.d = a(getChildAt(0)) - this.f;
                if (this.v == null) {
                    this.v = new Runnable() {
                        public final void run() {
                            if (List3DView.this.a == 1) {
                                int a2 = List3DView.this.a(List3DView.this.b, List3DView.this.c);
                                if (a2 != -1) {
                                    List3DView.this.b(a2);
                                }
                            }
                        }
                    };
                }
                postDelayed(this.v, (long) ViewConfiguration.getLongPressTimeout());
                this.s = VelocityTracker.obtain();
                this.s.addMovement(motionEvent);
                this.a = 1;
                break;
            case 1:
                if (this.a == 1) {
                    int a2 = a((int) motionEvent.getX(), (int) motionEvent.getY());
                    if (a2 != -1) {
                        int i2 = this.h + a2;
                        performItemClick(getChildAt(a2), i2, this.r.getItemId(i2));
                    }
                } else if (this.a == 2) {
                    this.s.addMovement(motionEvent);
                    this.s.computeCurrentVelocity(1000);
                    f2 = this.s.getYVelocity();
                    this.k = f2;
                }
                a(f2, false);
                break;
            case 2:
                if (this.a == 1) {
                    int x2 = (int) motionEvent.getX();
                    int y2 = (int) motionEvent.getY();
                    if (x2 < this.b - 10 || x2 > this.b + 10 || y2 < this.c - 10 || y2 > this.c + 10) {
                        removeCallbacks(this.v);
                        this.a = 2;
                    }
                }
                if (this.a == 2) {
                    this.s.addMovement(motionEvent);
                    a(((int) motionEvent.getY()) - this.c);
                    break;
                }
                break;
            default:
                a(0.0f, false);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.l && this.r != null) {
            if (getChildCount() == 0) {
                if (this.C) {
                    this.e = getHeight() / 3;
                }
                if (!this.m) {
                    this.i = -1;
                } else {
                    this.i = this.h;
                    this.h++;
                }
                b(this.e, 0);
            } else {
                int a2 = (this.e + this.f) - a(getChildAt(0));
                int childCount = getChildCount();
                if (this.i != this.r.getCount() - 1 && childCount > 1) {
                    View childAt = getChildAt(0);
                    while (childAt != null && c(childAt) + a2 < 0) {
                        removeViewInLayout(childAt);
                        childCount--;
                        this.u.addLast(childAt);
                        this.h++;
                        this.f += d(childAt);
                        childAt = childCount > 1 ? getChildAt(0) : null;
                    }
                }
                if (this.h != 0 && childCount > 1) {
                    View childAt2 = getChildAt(childCount - 1);
                    while (childAt2 != null && a(childAt2) + a2 > getHeight()) {
                        removeViewInLayout(childAt2);
                        childCount--;
                        this.u.addLast(childAt2);
                        this.i--;
                        if (childCount > 1) {
                            childAt2 = getChildAt(childCount - 1);
                        } else {
                            childAt2 = null;
                        }
                    }
                }
                b(c(getChildAt(getChildCount() - 1)), a2);
                int a3 = a(getChildAt(0));
                while (a3 + a2 > 0 && this.h > 0) {
                    this.h--;
                    View view = this.r.getView(this.h, c(), this);
                    a(view, 1);
                    int d2 = d(view);
                    a3 -= d2;
                    this.f -= d2;
                }
            }
            int i6 = this.e + this.f;
            float width = ((float) getWidth()) * 0.0f;
            float height = 1.0f / (((float) getHeight()) * 0.9f);
            for (int i7 = 0; i7 < getChildCount(); i7++) {
                View childAt3 = getChildAt(i7);
                double d3 = (double) width;
                double d4 = (double) height;
                Double.isNaN(d4);
                double d5 = d4 * 6.283185307179586d;
                double d6 = (double) i6;
                Double.isNaN(d6);
                double sin = Math.sin(d5 * d6);
                Double.isNaN(d3);
                int i8 = (int) (d3 * sin);
                int measuredWidth = childAt3.getMeasuredWidth();
                int measuredHeight = childAt3.getMeasuredHeight();
                int width2 = i8 + ((getWidth() - measuredWidth) / 2);
                int b2 = b(childAt3);
                int i9 = i6 + b2;
                childAt3.layout(width2, i9, measuredWidth + width2, i9 + measuredHeight);
                i6 += measuredHeight + (b2 * 2);
            }
            if (this.C && !this.D) {
                this.D = true;
                dispatchTouchEvent(MotionEvent.obtain(System.currentTimeMillis(), System.currentTimeMillis(), 0, 0.0f, 0.0f, 0));
                postDelayed(new Runnable() {
                    public final void run() {
                        List3DView.this.dispatchTouchEvent(MotionEvent.obtain(System.currentTimeMillis(), System.currentTimeMillis(), 2, 0.0f, -20.0f, 0));
                        List3DView.this.dispatchTouchEvent(MotionEvent.obtain(System.currentTimeMillis(), System.currentTimeMillis(), 1, 0.0f, -20.0f, 0));
                    }
                }, 5);
            }
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        Bitmap drawingCache = view.getDrawingCache();
        if (drawingCache == null) {
            return super.drawChild(canvas, view, j2);
        }
        int top = view.getTop();
        int left = view.getLeft();
        int width = view.getWidth() / 2;
        int height = view.getHeight() / 2;
        float height2 = (float) (getHeight() / 2);
        float f2 = (((float) (top + height)) - height2) / height2;
        float cos = (float) (1.0d - ((1.0d - Math.cos((double) f2)) * 0.15000000596046448d));
        float f3 = (((float) this.g) - (f2 * 20.0f)) % 90.0f;
        if (f3 < 0.0f) {
            f3 += 90.0f;
        }
        float f4 = f3;
        if (f4 < 45.0f) {
            Canvas canvas2 = canvas;
            Bitmap bitmap = drawingCache;
            int i2 = top;
            int i3 = left;
            int i4 = width;
            int i5 = height;
            float f5 = cos;
            a(canvas2, bitmap, i2, i3, i4, i5, f5, f4 - 90.0f);
            a(canvas2, bitmap, i2, i3, i4, i5, f5, f4);
        } else {
            Canvas canvas3 = canvas;
            Bitmap bitmap2 = drawingCache;
            int i6 = top;
            int i7 = left;
            int i8 = width;
            int i9 = height;
            float f6 = cos;
            a(canvas3, bitmap2, i6, i7, i8, i9, f6, f4);
            a(canvas3, bitmap2, i6, i7, i8, i9, f6, f4 - 90.0f);
        }
        return false;
    }

    private void a(Canvas canvas, Bitmap bitmap, int i2, int i3, int i4, int i5, float f2, float f3) {
        if (this.x == null) {
            this.x = new Camera();
        }
        this.x.save();
        this.x.translate(0.0f, 0.0f, (float) i5);
        this.x.rotateX(f3);
        float f4 = (float) (-i5);
        this.x.translate(0.0f, 0.0f, f4);
        if (this.y == null) {
            this.y = new Matrix();
        }
        this.x.getMatrix(this.y);
        this.x.restore();
        this.y.preTranslate((float) (-i4), f4);
        this.y.postScale(f2, f2);
        this.y.postTranslate((float) (i3 + i4), (float) (i2 + i5));
        if (this.z == null) {
            this.z = new Paint();
            this.z.setAntiAlias(true);
            this.z.setFilterBitmap(true);
        }
        this.z.setColorFilter(a(f3));
        canvas.drawBitmap(bitmap, this.y, this.z);
    }

    private static LightingColorFilter a(float f2) {
        double d2 = (double) f2;
        Double.isNaN(d2);
        double cos = Math.cos((d2 * 3.141592653589793d) / 180.0d);
        int i2 = ((int) (cos * 200.0d)) + 55;
        int pow = (int) (Math.pow(cos, 200.0d) * 70.0d);
        if (i2 > 255) {
            i2 = 255;
        }
        if (pow > 255) {
            pow = 255;
        }
        return new LightingColorFilter(Color.rgb(i2, i2, i2), Color.rgb(pow, pow, pow));
    }

    /* access modifiers changed from: protected */
    public final void a(float f2, boolean z2) {
        if (this.s != null || z2) {
            if (this.s != null) {
                this.s.recycle();
            }
            this.s = null;
            removeCallbacks(this.v);
            if (this.t == null) {
                this.t = new Runnable() {
                    public final void run() {
                        if (List3DView.this.j != null) {
                            View childAt = List3DView.this.getChildAt(0);
                            if (childAt != null) {
                                List3DView.this.d = List3DView.a(childAt) - List3DView.this.f;
                                List3DView.this.j.a(AnimationUtils.currentAnimationTimeMillis());
                                List3DView.this.a(((int) List3DView.this.j.a()) - List3DView.this.d);
                            }
                            if (!List3DView.this.j.c()) {
                                List3DView.this.postDelayed(this, 16);
                            }
                        }
                    }
                };
            }
            if (this.j != null) {
                if (!z2) {
                    this.j.a((float) this.e, f2, AnimationUtils.currentAnimationTimeMillis());
                }
                post(this.t);
            }
            this.a = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.t);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        this.e = this.d + i2;
        this.g = (-(this.e * 270)) / getHeight();
        b();
        requestLayout();
    }

    private void b() {
        int i2;
        int i3 = this.g % 90;
        if (i3 < 45) {
            i2 = ((-(this.g - i3)) * getHeight()) / 270;
        } else {
            i2 = ((-((this.g + 90) - i3)) * getHeight()) / 270;
        }
        if (this.A == Integer.MIN_VALUE && this.i == this.r.getCount() - 1 && c(getChildAt(getChildCount() - 1)) < getHeight()) {
            this.A = i2;
        }
        if (i2 > 0) {
            i2 = 0;
        } else if (i2 < this.A) {
            i2 = this.A;
        }
        float f2 = (float) i2;
        this.j.a(f2);
        this.j.b(f2);
    }

    /* access modifiers changed from: protected */
    public final int a(int i2, int i3) {
        if (this.w == null) {
            this.w = new Rect();
        }
        for (int i4 = 0; i4 < getChildCount(); i4++) {
            getChildAt(i4).getHitRect(this.w);
            if (this.w.contains(i2, i3)) {
                return i4;
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public final void b(int i2) {
        View childAt = getChildAt(i2);
        int i3 = this.h + i2;
        long itemId = this.r.getItemId(i3);
        OnItemLongClickListener onItemLongClickListener = getOnItemLongClickListener();
        if (onItemLongClickListener != null) {
            onItemLongClickListener.onItemLongClick(this, childAt, i3, itemId);
        }
    }

    private void b(int i2, int i3) {
        while (i2 + i3 < getHeight() && this.i < this.r.getCount() - 1) {
            this.i++;
            View view = this.r.getView(this.i, c(), this);
            a(view, 0);
            i2 += d(view);
        }
    }

    private void a(View view, int i2) {
        LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new LayoutParams(-2, -2);
        }
        int i3 = i2 == 1 ? 0 : -1;
        view.setDrawingCacheEnabled(true);
        addViewInLayout(view, i3, layoutParams, true);
        view.measure(((int) (((float) getWidth()) * 0.85f)) | 1073741824, 0);
    }

    private View c() {
        if (this.u.size() != 0) {
            return (View) this.u.removeFirst();
        }
        return null;
    }

    private static int b(View view) {
        return (int) ((((float) view.getMeasuredHeight()) * 0.35000002f) / 2.0f);
    }

    protected static int a(View view) {
        return view.getTop() - b(view);
    }

    private static int c(View view) {
        return view.getBottom() + b(view);
    }

    private static int d(View view) {
        return view.getMeasuredHeight() + (b(view) * 2);
    }

    public final int a() {
        return this.h;
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        return super.dispatchKeyShortcutEvent(keyEvent);
    }

    public void setAdapter(Adapter adapter) {
        if (com.startapp.common.b.b.a() && this.B) {
            com.startapp.common.b.b.a((View) this, 0.0f);
        }
        this.r = adapter;
        removeAllViewsInLayout();
        requestLayout();
    }
}
