package com.startapp.sdk.ads.list3d;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import com.startapp.common.b.c;
import com.startapp.sdk.adsbase.h;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: StartAppSDK */
public final class b {
    HashMap<String, h> a = new HashMap<>();
    Hashtable<String, Bitmap> b = new Hashtable<>();
    Set<String> c = new HashSet();
    h d;
    int e = 0;
    ConcurrentLinkedQueue<C0071b> f = new ConcurrentLinkedQueue<>();

    /* compiled from: StartAppSDK */
    class a extends AsyncTask<Void, Void, Bitmap> {
        private int a = -1;
        private String b;
        private String c;

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            Bitmap bitmap = (Bitmap) obj;
            b.this.e--;
            if (bitmap != null) {
                b.this.b.put(this.b, bitmap);
                if (b.this.d != null) {
                    b.this.d.a(this.a);
                }
                b bVar = b.this;
                if (!bVar.f.isEmpty()) {
                    C0071b bVar2 = (C0071b) bVar.f.poll();
                    new a(bVar2.a, bVar2.b, bVar2.c).execute(new Void[0]);
                }
            }
        }

        public a(int i, String str, String str2) {
            this.a = i;
            this.b = str;
            this.c = str2;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return c.b(this.c);
        }
    }

    /* renamed from: com.startapp.sdk.ads.list3d.b$b reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    class C0071b {
        int a;
        String b;
        String c;

        public C0071b(int i, String str, String str2) {
            this.a = i;
            this.b = str;
            this.c = str2;
        }
    }

    public final Bitmap a(int i, String str, String str2) {
        Bitmap bitmap = (Bitmap) this.b.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        if (!this.c.contains(str)) {
            this.c.add(str);
            if (this.e >= 15) {
                this.f.add(new C0071b(i, str, str2));
            } else {
                this.e++;
                new a(i, str, str2).execute(new Void[0]);
            }
        }
        return null;
    }
}
