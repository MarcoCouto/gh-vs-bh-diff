package com.startapp.sdk.ads.list3d;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: StartAppSDK */
public final class g {
    private static g a = new g();
    private Map<String, f> b = new ConcurrentHashMap();

    private g() {
    }

    public static g a() {
        return a;
    }

    public final f a(String str) {
        if (this.b.containsKey(str)) {
            return (f) this.b.get(str);
        }
        f fVar = new f();
        this.b.put(str, fVar);
        StringBuilder sb = new StringBuilder("Created new model for uuid ");
        sb.append(str);
        sb.append(", Size = ");
        sb.append(this.b.size());
        return fVar;
    }

    public final void b(String str) {
        this.b.remove(str);
        StringBuilder sb = new StringBuilder("Model for ");
        sb.append(str);
        sb.append(" was removed, Size = ");
        sb.append(this.b.size());
    }
}
