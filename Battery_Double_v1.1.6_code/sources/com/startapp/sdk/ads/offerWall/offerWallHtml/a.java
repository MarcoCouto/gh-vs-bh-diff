package com.startapp.sdk.ads.offerWall.offerWallHtml;

import android.content.Context;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.model.GetAdRequest;

/* compiled from: StartAppSDK */
public final class a extends com.startapp.sdk.d.a {
    public a(Context context, OfferWallAd offerWallAd, AdPreferences adPreferences, b bVar) {
        super(context, offerWallAd, adPreferences, bVar, Placement.INAPP_OFFER_WALL, true);
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        GetAdRequest a = super.a();
        if (a == null) {
            return null;
        }
        a.e(AdsCommonMetaData.a().g());
        return a;
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        a(bool.booleanValue());
    }
}
