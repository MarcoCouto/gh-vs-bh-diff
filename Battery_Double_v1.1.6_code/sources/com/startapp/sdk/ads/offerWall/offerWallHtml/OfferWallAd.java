package com.startapp.sdk.ads.offerWall.offerWallHtml;

import android.content.Context;
import com.startapp.sdk.ads.interstitials.InterstitialAd;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public class OfferWallAd extends InterstitialAd {
    private static final long serialVersionUID = 1;

    public OfferWallAd(Context context) {
        super(context, Placement.INAPP_OFFER_WALL);
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        new a(this.a, this, adPreferences, bVar).c();
    }
}
