package com.startapp.sdk.ads.offerWall.offerWallJson;

import android.content.Context;
import android.content.Intent;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.startapp.sdk.ads.list3d.List3DActivity;
import com.startapp.sdk.ads.list3d.g;
import com.startapp.sdk.adsbase.Ad.AdState;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.JsonAd;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.UUID;

/* compiled from: StartAppSDK */
public class OfferWall3DAd extends JsonAd implements f {
    private static String b = null;
    private static final long serialVersionUID = 1;
    private final String uuid = UUID.randomUUID().toString();

    public OfferWall3DAd(Context context) {
        super(context, Placement.INAPP_OFFER_WALL);
        if (b == null) {
            b = s.f(context);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        new a(this.a, this, adPreferences, bVar).c();
    }

    public final boolean a(String str) {
        g.a().a(this.uuid).b(a.a());
        boolean a = this.activityExtra != null ? this.activityExtra.a() : false;
        if (super.e_()) {
            a(NotDisplayedReason.AD_EXPIRED);
            return false;
        }
        Intent intent = new Intent(this.a, List3DActivity.class);
        intent.putExtra("adInfoOverride", getAdInfoOverride());
        intent.putExtra(Events.CREATIVE_FULLSCREEN, a);
        intent.putExtra("adTag", str);
        intent.putExtra("lastLoadTime", super.b());
        intent.putExtra("adCacheTtl", super.c());
        intent.putExtra(ParametersKeys.POSITION, a.b());
        intent.putExtra("listModelUuid", this.uuid);
        intent.addFlags(343932928);
        this.a.startActivity(intent);
        if (!AdsConstants.b.booleanValue()) {
            setState(AdState.UN_INITIALIZED);
        }
        return true;
    }

    public final String a() {
        return this.uuid;
    }

    public final Long b() {
        return super.b();
    }

    public final Long c() {
        return super.c();
    }

    public final boolean e_() {
        return super.e_();
    }

    public final boolean e() {
        return super.e();
    }

    public final void a(boolean z) {
        super.a(z);
    }
}
