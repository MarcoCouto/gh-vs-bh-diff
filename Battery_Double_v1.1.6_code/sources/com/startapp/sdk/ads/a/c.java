package com.startapp.sdk.ads.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.iab.omid.library.startapp.adsession.b;
import com.startapp.sdk.ads.interstitials.InterstitialAd;
import com.startapp.sdk.ads.splash.SplashAd;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.commontracking.CloseTrackingParams;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.k;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class c extends b {
    protected WebView c;
    protected b d;
    protected RelativeLayout e;
    protected long f = 0;
    protected boolean g = true;
    protected boolean h = false;
    /* access modifiers changed from: protected */
    public int i = 0;
    protected boolean j = false;
    protected Runnable k = new Runnable() {
        public final void run() {
            c.this.B();
            c.this.p();
        }
    };
    private Long l;
    private Long m;
    private h n;
    private Runnable o = new Runnable() {
        public final void run() {
            c.this.g = true;
            WebView webView = c.this.c;
            if (webView != null) {
                webView.setOnTouchListener(null);
            }
        }
    };

    /* compiled from: StartAppSDK */
    class a extends WebViewClient {
        a() {
        }

        public final void onPageFinished(WebView webView, String str) {
            c.this.b(webView);
            c.this.a("gClientInterface.setMode", c.this.h());
            c.this.a("enableScheme", "externalLinks");
            c.this.a((View) null);
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!c.this.g) {
                e g = new e(InfoEventCategory.ERROR).e("fake_click").g(c.this.n());
                StringBuilder sb = new StringBuilder("jsTag=");
                sb.append(c.this.j);
                g.f(sb.toString()).a((Context) c.this.c());
            }
            if (!c.this.j || c.this.g) {
                return c.this.a(str, false);
            }
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void G() {
    }

    /* access modifiers changed from: protected */
    public boolean H() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void b(WebView webView) {
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle == null) {
            if (b().hasExtra("lastLoadTime")) {
                this.l = (Long) b().getSerializableExtra("lastLoadTime");
            }
            if (b().hasExtra("adCacheTtl")) {
                this.m = (Long) b().getSerializableExtra("adCacheTtl");
            }
        } else {
            if (bundle.containsKey("postrollHtml")) {
                a(bundle.getString("postrollHtml"));
            }
            if (bundle.containsKey("lastLoadTime")) {
                this.l = (Long) bundle.getSerializable("lastLoadTime");
            }
            if (bundle.containsKey("adCacheTtl")) {
                this.m = (Long) bundle.getSerializable("adCacheTtl");
            }
            this.h = bundle.getBoolean("videoCompletedBroadcastSent", false);
            this.i = bundle.getInt("replayNum");
        }
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        if (g() != null) {
            bundle.putString("postrollHtml", g());
        }
        if (this.l != null) {
            bundle.putLong("lastLoadTime", this.l.longValue());
        }
        if (this.m != null) {
            bundle.putLong("adCacheTtl", this.m.longValue());
        }
        bundle.putBoolean("videoCompletedBroadcastSent", this.h);
        bundle.putInt("replayNum", this.i);
    }

    public final void w() {
        super.w();
        if (this.d != null) {
            this.d.b();
            this.d = null;
        }
        s.a((Object) this.c);
    }

    /* access modifiers changed from: protected */
    public void y() {
        this.c.setWebViewClient(new a());
        this.c.setWebChromeClient(new WebChromeClient());
    }

    /* access modifiers changed from: protected */
    public com.startapp.sdk.d.b z() {
        com.startapp.sdk.d.b bVar = new com.startapp.sdk.d.b(c(), this.k, this.k, this.o, a(), a(0));
        return bVar;
    }

    /* access modifiers changed from: protected */
    public void A() {
        this.n.a();
    }

    public void a(WebView webView) {
        this.g = false;
        webView.setOnTouchListener(new OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                c.this.g = true;
                if (motionEvent.getAction() == 2) {
                    return true;
                }
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        if (MetaData.D().P() && this.d == null) {
            this.d = com.startapp.sdk.omsdk.a.a(this.c);
            if (this.d != null && this.c != null) {
                if (this.a != null) {
                    View a2 = this.a.a();
                    if (a2 != null) {
                        this.d.b(a2);
                    }
                }
                if (view != null) {
                    this.d.b(view);
                }
                this.d.a(this.c);
                this.d.a();
                com.iab.omid.library.startapp.adsession.a.a(this.d).a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, Object... objArr) {
        s.a(this.c, str, objArr);
    }

    /* access modifiers changed from: protected */
    public static long a(long j2) {
        long j3 = j2 % 1000;
        if (j3 == 0) {
            return 1000;
        }
        return j3;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0035 A[SYNTHETIC, Splitter:B:13:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004c  */
    public boolean a(String str, boolean z) {
        boolean z2;
        this.n.a(true);
        Ad x = x();
        if (com.startapp.sdk.adsbase.a.a(c().getApplicationContext(), this.b)) {
            if (!(s.a(8) && (x instanceof SplashAd))) {
                z2 = true;
                if (!b(str)) {
                    try {
                        int a2 = com.startapp.sdk.adsbase.a.a(str);
                        if (!d()[a2] || z2) {
                            b(str, a2, z);
                        } else {
                            a(str, a2, z);
                        }
                    } catch (Exception unused) {
                        return false;
                    }
                } else if (!d()[0] || z2) {
                    b(str, 0, z);
                } else {
                    a(str, 0, z);
                }
                return true;
            }
        }
        z2 = false;
        if (!b(str)) {
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        return !this.j && str.contains("index=");
    }

    /* access modifiers changed from: protected */
    public void B() {
        String[] l2 = l();
        if (l2 != null && l2.length > 0 && l()[0] != null) {
            com.startapp.sdk.adsbase.a.b((Context) c(), l()[0], a());
        }
    }

    private void a(String str, int i2, boolean z) {
        int i3 = i2;
        Activity c2 = c();
        String str2 = null;
        String str3 = i3 < j().length ? j()[i3] : null;
        if (i3 < k().length) {
            str2 = k()[i3];
        }
        com.startapp.sdk.adsbase.a.a(c2, str, str3, str2, a(), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), a(i3), b(i3), z, new Runnable() {
            public final void run() {
                c.this.p();
            }
        });
    }

    private void b(String str, int i2, boolean z) {
        com.startapp.common.b.a((Context) c()).a(new Intent("com.startapp.android.OnClickCallback"));
        com.startapp.sdk.adsbase.a.a(c(), str, i2 < j().length ? j()[i2] : null, a(), a(i2) && !com.startapp.sdk.adsbase.a.a(c().getApplicationContext(), this.b), z);
        p();
    }

    public void p() {
        super.p();
        k.a().a(false);
        if (this.n != null) {
            this.n.a(false);
        }
        c().runOnUiThread(new Runnable() {
            public final void run() {
                if (c.this.c != null) {
                    com.startapp.common.b.b.b(c.this.c);
                }
            }
        });
    }

    public void s() {
        if (this.n != null) {
            this.n.b();
        }
        if (this.a != null && this.a.b()) {
            this.a.e();
        }
        if (this.c != null) {
            com.startapp.common.b.b.b(this.c);
        }
        if (h().equals("back")) {
            p();
        }
    }

    private TrackingParams a() {
        return new CloseTrackingParams(D(), m());
    }

    /* access modifiers changed from: protected */
    public TrackingParams C() {
        return new TrackingParams(m());
    }

    /* access modifiers changed from: protected */
    public String D() {
        double uptimeMillis = (double) (SystemClock.uptimeMillis() - this.f);
        Double.isNaN(uptimeMillis);
        return String.valueOf(uptimeMillis / 1000.0d);
    }

    public boolean r() {
        B();
        k.a().a(false);
        this.n.a(false);
        return false;
    }

    /* access modifiers changed from: protected */
    public long E() {
        if (o() != null) {
            return TimeUnit.SECONDS.toMillis(o().longValue());
        }
        return TimeUnit.SECONDS.toMillis(MetaData.D().E());
    }

    /* access modifiers changed from: protected */
    public final void F() {
        if (H() && !this.h && this.i == 0) {
            this.h = true;
            com.startapp.common.b.a((Context) c()).a(new Intent("com.startapp.android.OnVideoCompleted"));
            G();
        }
    }

    public void u() {
        if (x() instanceof InterstitialAd ? ((InterstitialAd) x()).e_() : false) {
            p();
            return;
        }
        k.a().a(true);
        if (this.n == null) {
            h hVar = new h(c(), i(), C(), E());
            this.n = hVar;
        }
        if (this.c == null) {
            this.e = new RelativeLayout(c());
            this.e.setContentDescription("StartApp Ad");
            this.e.setId(1475346432);
            c().setContentView(this.e);
            try {
                this.c = new WebView(c().getApplicationContext());
                this.c.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                c().getWindow().getDecorView().findViewById(16908290).setBackgroundColor(7829367);
                this.c.setVerticalScrollBarEnabled(false);
                this.c.setHorizontalScrollBarEnabled(false);
                this.c.getSettings().setJavaScriptEnabled(true);
                com.startapp.common.b.b.a(this.c);
                this.c.setOnLongClickListener(new OnLongClickListener() {
                    public final boolean onLongClick(View view) {
                        return true;
                    }
                });
                this.c.setLongClickable(false);
                this.c.addJavascriptInterface(z(), "startappwall");
                A();
                a(this.c);
                s.a((Context) c(), this.c, g());
                this.j = "true".equals(s.a(g(), "@jsTag@", "@jsTag@"));
                y();
                this.e.addView(this.c, new LayoutParams(-1, -1));
                a(this.e);
            } catch (Throwable th) {
                new e(th).a((Context) c());
                p();
            }
        } else {
            com.startapp.common.b.b.c(this.c);
            this.n.a();
        }
        this.f = SystemClock.uptimeMillis();
    }
}
