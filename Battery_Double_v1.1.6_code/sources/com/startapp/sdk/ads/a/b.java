package com.startapp.sdk.ads.a;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.RelativeLayout;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.sdk.ads.splash.d;
import com.startapp.sdk.ads.video.VideoMode;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject.Size;
import com.startapp.sdk.adsbase.adinformation.AdInformationOverrides;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.inappbrowser.a;

/* compiled from: StartAppSDK */
public abstract class b {
    protected AdInformationObject a = null;
    protected Placement b;
    private Intent c;
    private Activity d;
    private BroadcastReceiver e = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            b.this.p();
        }
    };
    private String[] f;
    private boolean[] g;
    private boolean[] h = {true};
    private String i;
    private String[] j;
    private String[] k;
    private String[] l;
    private Ad m;
    private String n;
    private boolean o;
    private AdInformationOverrides p;
    private String q;
    private Long r;
    private Boolean[] s = null;
    private int t = 0;
    private boolean u = false;
    private boolean v = false;

    public boolean a(int i2, KeyEvent keyEvent) {
        return false;
    }

    public void b(Bundle bundle) {
    }

    public boolean r() {
        return false;
    }

    public void t() {
    }

    public abstract void u();

    public void v() {
    }

    public static b a(Activity activity, Intent intent, Placement placement) {
        b bVar = null;
        switch (placement) {
            case INAPP_OFFER_WALL:
                if (s.a(128) || s.a(64)) {
                    bVar = new e();
                    break;
                }
            case INAPP_RETURN:
            case INAPP_OVERLAY:
                if (!s.a(4) || !intent.getBooleanExtra("videoAd", false)) {
                    if (!intent.getBooleanExtra("mraidAd", false)) {
                        bVar = new f();
                        break;
                    } else {
                        bVar = new d();
                        break;
                    }
                } else {
                    bVar = new VideoMode();
                    break;
                }
                break;
            case INAPP_SPLASH:
                if (s.a(8)) {
                    bVar = new d();
                    break;
                }
                break;
            case INAPP_FULL_SCREEN:
            case INAPP_BROWSER:
                if (s.a(256)) {
                    Uri data = intent.getData();
                    if (data != null) {
                        bVar = new a(data.toString());
                        break;
                    } else {
                        return null;
                    }
                }
                break;
            default:
                bVar = new a();
                break;
        }
        bVar.c = intent;
        bVar.d = activity;
        bVar.i = intent.getStringExtra(ParametersKeys.POSITION);
        bVar.j = intent.getStringArrayExtra("tracking");
        bVar.k = intent.getStringArrayExtra("trackingClickUrl");
        bVar.l = intent.getStringArrayExtra("packageNames");
        bVar.f = intent.getStringArrayExtra("closingUrl");
        bVar.g = intent.getBooleanArrayExtra("smartRedirect");
        bVar.h = intent.getBooleanArrayExtra("browserEnabled");
        bVar.q = intent.getStringExtra("adTag");
        String stringExtra = intent.getStringExtra("htmlUuid");
        if (stringExtra != null) {
            if (AdsConstants.b.booleanValue()) {
                bVar.a(com.startapp.sdk.adsbase.cache.a.a().a(stringExtra));
            } else {
                bVar.a(com.startapp.sdk.adsbase.cache.a.a().b(stringExtra));
            }
        }
        bVar.o = intent.getBooleanExtra("isSplash", false);
        bVar.p = (AdInformationOverrides) intent.getSerializableExtra("adInfoOverride");
        bVar.b = placement;
        bVar.f = intent.getStringArrayExtra("closingUrl");
        bVar.t = intent.getIntExtra("rewardDuration", 0);
        bVar.u = intent.getBooleanExtra("rewardedHideTimer", false);
        if (bVar.g == null) {
            bVar.g = new boolean[]{true};
        }
        if (bVar.h == null) {
            bVar.h = new boolean[]{true};
        }
        bVar.m = (Ad) intent.getSerializableExtra("ad");
        long longExtra = intent.getLongExtra("delayImpressionSeconds", -1);
        if (longExtra != -1) {
            bVar.r = Long.valueOf(longExtra);
        }
        bVar.s = (Boolean[]) intent.getSerializableExtra("sendRedirectHops");
        StringBuilder sb = new StringBuilder("Placement=[");
        sb.append(bVar.b);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return bVar;
    }

    public final Intent b() {
        return this.c;
    }

    public final Activity c() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final boolean[] d() {
        return this.g;
    }

    public final int e() {
        return this.t;
    }

    public final boolean f() {
        return this.u;
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i2) {
        if (this.h == null || i2 < 0 || i2 >= this.h.length) {
            return true;
        }
        return this.h[i2];
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final String h() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final String[] i() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final String[] j() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public final String[] k() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final String[] l() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final String m() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public final void a(RelativeLayout relativeLayout) {
        this.a = new AdInformationObject(this.d, Size.LARGE, this.b, this.p);
        this.a.a(relativeLayout);
    }

    public final Long o() {
        return this.r;
    }

    public final Boolean b(int i2) {
        if (this.s == null || i2 < 0 || i2 >= this.s.length) {
            return null;
        }
        return this.s[i2];
    }

    public void q() {
        com.startapp.common.b.a((Context) this.d).a(new Intent("com.startapp.android.HideDisplayBroadcastListener"));
    }

    public void s() {
        p();
    }

    public void w() {
        if (this.e != null) {
            com.startapp.common.b.a((Context) this.d).a(this.e);
        }
        this.e = null;
    }

    public final Ad x() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        if (str == null || this.q == null || this.q.length() <= 0) {
            this.n = str;
        } else {
            this.n = str.replaceAll("startapp_adtag_placeholder", this.q);
        }
    }

    /* access modifiers changed from: protected */
    public final String n() {
        try {
            String[] strArr = this.j;
            if (strArr != null && strArr.length > 0) {
                return com.startapp.sdk.adsbase.a.a(strArr[0], (String) null);
            }
        } catch (Exception unused) {
        }
        return "";
    }

    public void p() {
        this.d.runOnUiThread(new Runnable() {
            public final void run() {
                b.this.c().finish();
            }
        });
    }

    public void a(Bundle bundle) {
        com.startapp.common.b.a((Context) this.d).a(this.e, new IntentFilter("com.startapp.android.CloseAdActivity"));
    }
}
