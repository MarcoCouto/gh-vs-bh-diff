package com.startapp.sdk.ads.splash;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.graphics.drawable.PathInterpolatorCompat;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.tapjoy.TapjoyConstants;
import java.io.Serializable;
import java.util.Arrays;

/* compiled from: StartAppSDK */
public class SplashConfig implements Serializable {
    private static long a = 7500;
    private static final Theme b = Theme.OCEAN;
    private static final MinSplashTime c = MinSplashTime.REGULAR;
    private static final long d = a;
    private static final MaxAdDisplayTime e = MaxAdDisplayTime.FOR_EVER;
    private static final Orientation f = Orientation.AUTO;
    private static final long serialVersionUID = 1;
    private String appName = "";
    private int customScreen = -1;
    @d(b = MaxAdDisplayTime.class)
    private MaxAdDisplayTime defaultMaxAdDisplayTime = e;
    private Long defaultMaxLoadTime = Long.valueOf(d);
    @d(b = MinSplashTime.class)
    private MinSplashTime defaultMinSplashTime = c;
    @d(b = Orientation.class)
    private Orientation defaultOrientation = f;
    @d(b = Theme.class)
    private Theme defaultTheme = b;
    private boolean forceNative = false;
    private transient Drawable g = null;
    private transient String h = "";
    private boolean htmlSplash = true;
    private byte[] logoByteArray = null;
    private int logoRes = -1;
    private String splashBgColor = "#066CAA";
    private String splashFontColor = "ffffff";
    private String splashLoadingType = "LoadingDots";

    /* compiled from: StartAppSDK */
    public enum MaxAdDisplayTime {
        SHORT(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS),
        LONG(TapjoyConstants.TIMER_INCREMENT),
        FOR_EVER(86400000);
        
        private long index;

        private MaxAdDisplayTime(long j) {
            this.index = j;
        }

        public final long getIndex() {
            return this.index;
        }

        public static MaxAdDisplayTime getByIndex(long j) {
            MaxAdDisplayTime maxAdDisplayTime = SHORT;
            MaxAdDisplayTime[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].getIndex() == j) {
                    maxAdDisplayTime = values[i];
                }
            }
            return maxAdDisplayTime;
        }

        public static MaxAdDisplayTime getByName(String str) {
            MaxAdDisplayTime maxAdDisplayTime = FOR_EVER;
            MaxAdDisplayTime[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].name().toLowerCase().compareTo(str.toLowerCase()) == 0) {
                    maxAdDisplayTime = values[i];
                }
            }
            return maxAdDisplayTime;
        }
    }

    /* compiled from: StartAppSDK */
    public enum MinSplashTime {
        REGULAR(PathInterpolatorCompat.MAX_NUM_POINTS),
        SHORT(2000),
        LONG(5000);
        
        private long index;

        private MinSplashTime(int i) {
            this.index = (long) i;
        }

        public final long getIndex() {
            return this.index;
        }

        public static MinSplashTime getByIndex(long j) {
            MinSplashTime minSplashTime = SHORT;
            MinSplashTime[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].getIndex() == j) {
                    minSplashTime = values[i];
                }
            }
            return minSplashTime;
        }

        public static MinSplashTime getByName(String str) {
            MinSplashTime minSplashTime = LONG;
            MinSplashTime[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].name().toLowerCase().compareTo(str.toLowerCase()) == 0) {
                    minSplashTime = values[i];
                }
            }
            return minSplashTime;
        }
    }

    /* compiled from: StartAppSDK */
    public enum Orientation {
        PORTRAIT(1),
        LANDSCAPE(2),
        AUTO(3);
        
        private int index;

        private Orientation(int i) {
            this.index = i;
        }

        public final int getIndex() {
            return this.index;
        }

        public static Orientation getByIndex(int i) {
            Orientation orientation = PORTRAIT;
            Orientation[] values = values();
            for (int i2 = 0; i2 < values.length; i2++) {
                if (values[i2].getIndex() == i) {
                    orientation = values[i2];
                }
            }
            return orientation;
        }

        public static Orientation getByName(String str) {
            Orientation orientation = AUTO;
            Orientation[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].name().toLowerCase().compareTo(str.toLowerCase()) == 0) {
                    orientation = values[i];
                }
            }
            return orientation;
        }
    }

    /* compiled from: StartAppSDK */
    public enum Theme {
        DEEP_BLUE(1),
        SKY(2),
        ASHEN_SKY(3),
        BLAZE(4),
        GLOOMY(5),
        OCEAN(6),
        USER_DEFINED(0);
        
        private int index;

        private Theme(int i) {
            this.index = i;
        }

        public final int getIndex() {
            return this.index;
        }

        public static Theme getByIndex(int i) {
            Theme theme = DEEP_BLUE;
            Theme[] values = values();
            for (int i2 = 0; i2 < values.length; i2++) {
                if (values[i2].getIndex() == i) {
                    theme = values[i2];
                }
            }
            return theme;
        }

        public static Theme getByName(String str) {
            Theme theme = DEEP_BLUE;
            Theme[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].name().toLowerCase().compareTo(str.toLowerCase()) == 0) {
                    theme = values[i];
                }
            }
            return theme;
        }
    }

    static {
        SplashConfig.class.getSimpleName();
    }

    public static SplashConfig getDefaultSplashConfig() {
        SplashConfig splashConfig = new SplashConfig();
        splashConfig.setTheme(b).setMinSplashTime(c).a(d).setMaxAdDisplayTime(e).setOrientation(f).setLoadingType("LoadingDots").setAppName("");
        return splashConfig;
    }

    public SplashConfig setTheme(Theme theme) {
        this.defaultTheme = theme;
        return this;
    }

    public SplashConfig setCustomScreen(int i) {
        this.customScreen = i;
        return this;
    }

    public SplashConfig setAppName(String str) {
        this.appName = str;
        return this;
    }

    public SplashConfig setLogo(int i) {
        this.logoRes = i;
        return this;
    }

    public SplashConfig setLogo(byte[] bArr) {
        this.logoByteArray = bArr;
        return this;
    }

    private SplashConfig a(long j) {
        this.defaultMaxLoadTime = Long.valueOf(j);
        return this;
    }

    public SplashConfig setOrientation(Orientation orientation) {
        this.defaultOrientation = orientation;
        return this;
    }

    public SplashConfig setMinSplashTime(MinSplashTime minSplashTime) {
        this.defaultMinSplashTime = minSplashTime;
        return this;
    }

    public SplashConfig setMaxAdDisplayTime(MaxAdDisplayTime maxAdDisplayTime) {
        this.defaultMaxAdDisplayTime = maxAdDisplayTime;
        return this;
    }

    public int getCustomScreen() {
        return this.customScreen;
    }

    public String getAppName() {
        return this.appName;
    }

    public Drawable getLogo() {
        return this.g;
    }

    public int getLogoRes() {
        return this.logoRes;
    }

    public byte[] getLogoByteArray() {
        return this.logoByteArray;
    }

    /* access modifiers changed from: protected */
    public final Long a() {
        return this.defaultMaxLoadTime;
    }

    public String getErrorMessage() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final Theme b() {
        return this.defaultTheme;
    }

    public Orientation getOrientation() {
        return this.defaultOrientation;
    }

    public MinSplashTime getMinSplashTime() {
        return this.defaultMinSplashTime;
    }

    public MaxAdDisplayTime getMaxAdDisplayTime() {
        return this.defaultMaxAdDisplayTime;
    }

    public boolean isHtmlSplash() {
        if (this.forceNative) {
            return false;
        }
        return this.htmlSplash;
    }

    public String getBgColor() {
        return this.splashBgColor;
    }

    public String getFontColor() {
        return this.splashFontColor;
    }

    public String getLoadingType() {
        return this.splashLoadingType;
    }

    public SplashConfig setLoadingType(String str) {
        this.splashLoadingType = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public final boolean a(Context context) {
        if (AnonymousClass1.$SwitchMap$com$startapp$sdk$ads$splash$SplashConfig$Theme[this.defaultTheme.ordinal()] != 7) {
            if (getAppName().equals("")) {
                setAppName(a.a(context, "Welcome!"));
            }
            if (getLogo() == null && getLogoByteArray() == null) {
                if (getLogoRes() == -1) {
                    setLogo(context.getApplicationInfo().icon);
                    this.g = context.getResources().getDrawable(context.getApplicationInfo().icon);
                } else {
                    this.g = context.getResources().getDrawable(getLogoRes());
                }
            }
        } else if (getCustomScreen() == -1) {
            this.h = "StartApp: Exception getting custom screen resource id, make sure it is set";
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final View b(Context context) {
        if (AnonymousClass1.$SwitchMap$com$startapp$sdk$ads$splash$SplashConfig$Theme[this.defaultTheme.ordinal()] != 7) {
            return e.a(context, this);
        }
        try {
            return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(getCustomScreen(), null);
        } catch (NotFoundException unused) {
            throw new NotFoundException("StartApp: Can't find Custom layout resource");
        } catch (InflateException unused2) {
            throw new InflateException("StartApp: Can't inflate layout in Custom mode, Are you sure layout resource is valid?");
        } catch (Throwable th) {
            new e(th).a(context);
            return null;
        }
    }

    public void setDefaults(Context context) {
        SplashConfig a2 = SplashMetaData.b().a();
        if (a2 == null) {
            a2 = getDefaultSplashConfig();
        } else {
            this.htmlSplash = a2.isHtmlSplash();
        }
        SplashConfig defaultSplashConfig = getDefaultSplashConfig();
        if (a2.defaultTheme == null) {
            a2.setTheme(defaultSplashConfig.defaultTheme);
        }
        if (a2.getMinSplashTime() == null) {
            a2.setMinSplashTime(defaultSplashConfig.getMinSplashTime());
        }
        if (a2.defaultMaxLoadTime == null) {
            a2.a(defaultSplashConfig.defaultMaxLoadTime.longValue());
        }
        if (a2.getMaxAdDisplayTime() == null) {
            a2.setMaxAdDisplayTime(defaultSplashConfig.getMaxAdDisplayTime());
        }
        if (a2.getOrientation() == null) {
            a2.setOrientation(defaultSplashConfig.getOrientation());
        }
        if (a2.getLoadingType() == null) {
            a2.setLoadingType(defaultSplashConfig.getLoadingType());
        }
        if (a2.getAppName().equals("")) {
            a2.setAppName(a.a(context, "Welcome!"));
        }
        if (getMaxAdDisplayTime() == null) {
            setMaxAdDisplayTime(a2.getMaxAdDisplayTime());
        }
        if (this.defaultMaxLoadTime == null) {
            a(a2.defaultMaxLoadTime.longValue());
        }
        if (getMinSplashTime() == null) {
            setMinSplashTime(a2.getMinSplashTime());
        }
        if (getOrientation() == null) {
            setOrientation(a2.getOrientation());
        }
        if (this.defaultTheme == null) {
            setTheme(a2.defaultTheme);
        }
        if (getLogoRes() == -1) {
            setLogo(context.getApplicationInfo().icon);
        }
        if (getAppName().equals("")) {
            setAppName(a2.getAppName());
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Activity activity) {
        if (getLogo() == null && getLogoRes() == -1 && getLogoByteArray() != null) {
            byte[] logoByteArray2 = getLogoByteArray();
            this.g = new BitmapDrawable(activity.getResources(), BitmapFactory.decodeByteArray(logoByteArray2, 0, logoByteArray2.length));
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SplashConfig splashConfig = (SplashConfig) obj;
        return this.forceNative == splashConfig.forceNative && this.customScreen == splashConfig.customScreen && this.logoRes == splashConfig.logoRes && this.htmlSplash == splashConfig.htmlSplash && s.b(this.appName, splashConfig.appName) && Arrays.equals(this.logoByteArray, splashConfig.logoByteArray) && this.defaultTheme == splashConfig.defaultTheme && this.defaultMinSplashTime == splashConfig.defaultMinSplashTime && s.b(this.defaultMaxLoadTime, splashConfig.defaultMaxLoadTime) && this.defaultMaxAdDisplayTime == splashConfig.defaultMaxAdDisplayTime && this.defaultOrientation == splashConfig.defaultOrientation && s.b(this.splashBgColor, splashConfig.splashBgColor) && s.b(this.splashFontColor, splashConfig.splashFontColor) && s.b(this.splashLoadingType, splashConfig.splashLoadingType);
    }

    public int hashCode() {
        return (s.a(Boolean.valueOf(this.forceNative), Integer.valueOf(this.customScreen), this.appName, Integer.valueOf(this.logoRes), this.defaultTheme, this.defaultMinSplashTime, this.defaultMaxLoadTime, this.defaultMaxAdDisplayTime, this.defaultOrientation, Boolean.valueOf(this.htmlSplash), this.splashBgColor, this.splashFontColor, this.splashLoadingType) * 31) + Arrays.hashCode(this.logoByteArray);
    }

    /* access modifiers changed from: protected */
    public final SplashHtml b(Activity activity) {
        String str = "#066CAA";
        String str2 = "ffffff";
        switch (this.defaultTheme) {
            case DEEP_BLUE:
                str2 = "#FFFFFF";
                str = "#066CAA";
                break;
            case SKY:
                str2 = "#333333";
                str = "#a3d4e5";
                break;
            case ASHEN_SKY:
                str2 = "#333333";
                str = "#E3E3E3";
                break;
            case BLAZE:
                str2 = "#FFFFFF";
                str = "#FF6600";
                break;
            case GLOOMY:
                str2 = "#33B5E5";
                str = "#2F353F";
                break;
            case OCEAN:
                str2 = "#063D51";
                str = "#237C9A";
                break;
        }
        this.splashBgColor = str;
        this.splashFontColor = str2;
        SplashHtml splashHtml = new SplashHtml(activity);
        splashHtml.a(this);
        splashHtml.a();
        return splashHtml;
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        return this.defaultTheme == Theme.USER_DEFINED || getCustomScreen() != -1;
    }
}
