package com.startapp.sdk.ads.splash;

import android.content.Context;
import android.webkit.JavascriptInterface;

/* compiled from: StartAppSDK */
public final class b {
    private boolean a = false;
    private Runnable b = null;
    private Context c;

    public b(Context context, Runnable runnable) {
        this.b = runnable;
        this.c = context;
    }

    @JavascriptInterface
    public final void closeSplash() {
        if (!this.a) {
            this.a = true;
            this.b.run();
        }
    }
}
