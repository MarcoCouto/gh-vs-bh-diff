package com.startapp.sdk.ads.splash;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.b.b;
import com.startapp.sdk.ads.splash.SplashConfig.MaxAdDisplayTime;
import com.startapp.sdk.ads.splash.SplashConfig.Orientation;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public final class SplashScreen {
    Activity a;
    SplashEventHandler b;
    CacheKey c;
    SplashHtml d = null;
    boolean e = false;
    SplashStartAppAd f;
    Runnable g = new Runnable() {
        public final void run() {
            SplashEventHandler.a(SplashScreen.this.d, (c) new c() {
                public final void h() {
                    if (!SplashScreen.this.e && SplashScreen.this.f != null) {
                        SplashScreen.this.f.showAd((AdDisplayListener) new AdDisplayListener() {
                            public final void adNotDisplayed(Ad ad) {
                            }

                            public final void adHidden(Ad ad) {
                                SplashScreen.this.b.c();
                            }

                            public final void adDisplayed(Ad ad) {
                                SplashScreen.this.b.c = SplashState.DISPLAYED;
                            }

                            public final void adClicked(Ad ad) {
                                SplashScreen.this.b.f();
                            }
                        });
                        SplashScreen.this.f();
                        SplashScreen.this.a.finish();
                    }
                }
            });
        }
    };
    private SplashConfig h;
    private Handler i = new Handler();
    private AdPreferences j;
    private Runnable k = new Runnable() {
        public final void run() {
            if (SplashScreen.this.c()) {
                SplashScreen.this.d();
                SplashScreen.this.e();
                return;
            }
            SplashScreen.this.a.finish();
        }
    };
    private AdEventListener l = new AdEventListener() {
        public final void onReceiveAd(Ad ad) {
            SplashScreen.this.b.a(SplashScreen.this.g);
        }

        public final void onFailedToReceiveAd(Ad ad) {
            if (SplashScreen.this.f != null) {
                SplashScreen.this.b.b();
            }
        }
    };

    /* compiled from: StartAppSDK */
    private static class SplashStartAppAd extends StartAppAd {
        private static final long serialVersionUID = 1;

        public SplashStartAppAd(Context context) {
            super(context);
            this.placement = Placement.INAPP_SPLASH;
        }

        /* access modifiers changed from: protected */
        public final AdRulesResult a(String str, Placement placement) {
            return new AdRulesResult();
        }
    }

    public SplashScreen(Activity activity, SplashConfig splashConfig, AdPreferences adPreferences) {
        this.a = activity;
        this.h = splashConfig;
        this.j = adPreferences;
        try {
            this.h.a(this.a);
            if (!g()) {
                this.d = this.h.b(this.a);
            }
            this.b = new SplashEventHandler(activity, this.d);
        } catch (Throwable th) {
            this.b = new SplashEventHandler(activity);
            this.b.a();
            this.b.b();
            new e(th).a((Context) activity);
        }
    }

    public final void a() {
        this.b.g();
        int i2 = this.a.getResources().getConfiguration().orientation;
        if (this.h.getOrientation() == Orientation.AUTO) {
            if (i2 == 2) {
                this.h.setOrientation(Orientation.LANDSCAPE);
            } else {
                this.h.setOrientation(Orientation.PORTRAIT);
            }
        }
        boolean z = false;
        switch (this.h.getOrientation()) {
            case PORTRAIT:
                if (i2 == 2) {
                    z = true;
                }
                b.a(this.a);
                break;
            case LANDSCAPE:
                if (i2 == 1) {
                    z = true;
                }
                b.b(this.a);
                break;
        }
        StringBuilder sb = new StringBuilder("Set Orientation: [");
        sb.append(this.h.getOrientation().toString());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        if (!z) {
            this.i.post(this.k);
        } else {
            this.i.postDelayed(this.k, 100);
        }
    }

    public final void b() {
        this.i.removeCallbacks(this.k);
        this.b.d();
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        if (this.h.a((Context) this.a)) {
            View view = null;
            if (g()) {
                view = this.h.b((Context) this.a);
            } else if (this.d != null) {
                view = this.d.c();
            }
            if (view == null) {
                return false;
            }
            this.a.setContentView(view, new LayoutParams(-1, -1));
            return true;
        }
        throw new IllegalArgumentException(this.h.getErrorMessage());
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        this.f = new SplashStartAppAd(this.a.getApplicationContext());
        this.c = this.f.loadSplash(this.j, this.l);
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        this.i.postDelayed(new Runnable() {
            public final void run() {
                if (SplashScreen.this.b.b(SplashScreen.this.g, SplashScreen.this.c)) {
                    SplashScreen.this.f = null;
                    SplashScreen.this.c = null;
                }
            }
        }, this.h.a().longValue());
        this.i.postDelayed(new Runnable() {
            public final void run() {
                SplashScreen.this.b.a(SplashScreen.this.g, SplashScreen.this.c);
            }
        }, this.h.getMinSplashTime().getIndex());
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        if (this.h.getMaxAdDisplayTime() != MaxAdDisplayTime.FOR_EVER) {
            this.i.postDelayed(new Runnable() {
                public final void run() {
                    SplashScreen.this.b.a((StartAppAd) SplashScreen.this.f);
                }
            }, this.h.getMaxAdDisplayTime().getIndex());
        }
    }

    private boolean g() {
        return !this.h.isHtmlSplash() || this.h.c();
    }
}
