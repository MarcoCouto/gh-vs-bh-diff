package com.startapp.sdk.ads.splash;

import android.content.Context;
import com.startapp.sdk.ads.interstitials.InterstitialAd;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public class SplashAd extends InterstitialAd {
    private static final long serialVersionUID = 1;

    public SplashAd(Context context) {
        super(context, Placement.INAPP_OVERLAY);
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences, AdEventListener adEventListener) {
        return super.load(adPreferences, b.a(this.a, adEventListener), false);
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        new a(this.a, this, adPreferences, bVar).c();
    }
}
