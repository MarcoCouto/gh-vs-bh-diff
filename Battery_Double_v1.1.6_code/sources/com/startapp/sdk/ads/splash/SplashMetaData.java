package com.startapp.sdk.ads.splash;

import android.content.Context;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class SplashMetaData implements Serializable {
    private static volatile SplashMetaData a = new SplashMetaData();
    private static Object b = new Object();
    private static final long serialVersionUID = 1;
    @d(a = true)
    private SplashConfig SplashConfig = new SplashConfig();
    private String splashMetadataUpdateVersion = AdsConstants.d;

    public final SplashConfig a() {
        return this.SplashConfig;
    }

    public static SplashMetaData b() {
        return a;
    }

    public static void a(Context context, SplashMetaData splashMetaData) {
        synchronized (b) {
            splashMetaData.splashMetadataUpdateVersion = AdsConstants.d;
            a = splashMetaData;
            com.startapp.common.b.d.a(context, "StartappSplashMetadata", (Serializable) splashMetaData);
        }
    }

    public static void a(Context context) {
        SplashMetaData splashMetaData = (SplashMetaData) com.startapp.common.b.d.a(context, "StartappSplashMetadata");
        SplashMetaData splashMetaData2 = new SplashMetaData();
        if (splashMetaData != null) {
            boolean a2 = s.a(splashMetaData, splashMetaData2);
            if (!(!AdsConstants.d.equals(splashMetaData.splashMetadataUpdateVersion)) && a2) {
                new e(InfoEventCategory.ERROR).e("metadata_null").a(context);
            }
            a = splashMetaData;
            return;
        }
        a = splashMetaData2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SplashMetaData splashMetaData = (SplashMetaData) obj;
        return s.b(this.SplashConfig, splashMetaData.SplashConfig) && s.b(this.splashMetadataUpdateVersion, splashMetaData.splashMetadataUpdateVersion);
    }

    public int hashCode() {
        return s.a(this.SplashConfig, this.splashMetadataUpdateVersion);
    }
}
