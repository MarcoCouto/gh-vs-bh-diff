package com.startapp.sdk.ads.banner;

import android.content.Context;
import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.i.j;
import com.startapp.sdk.adsbase.i.l;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.b.c;

/* compiled from: StartAppSDK */
public final class a extends GetAdRequest {
    private boolean c;
    private int d;

    public final void a(boolean z) {
        this.c = z;
    }

    public final void a(int i) {
        this.d = i;
    }

    public final l a() throws SDKException {
        l a = super.a();
        if (a == null) {
            a = new j();
        }
        a.a("fixedSize", Boolean.valueOf(this.c), false);
        a.a("bnrt", Integer.valueOf(this.d), false);
        return a;
    }

    public final void a(Context context) {
        this.b = c.a(context).h().a(g(), this.d);
    }
}
