package com.startapp.sdk.ads.banner;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.ThreadManager;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.adrules.AdaptMetaData;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.j.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledFuture;

/* compiled from: StartAppSDK */
public abstract class BannerBase extends RelativeLayout {
    protected AdRulesResult a;
    protected boolean b;
    protected Point c;
    protected boolean d;
    protected int e;
    protected int f;
    protected String g;
    private boolean h;
    private AdPreferences i;
    private int j;
    private boolean k;
    private b l;
    private boolean m;
    private boolean n;
    private String o;
    private ScheduledFuture<?> p;
    private Timer q;
    private a r;

    /* compiled from: StartAppSDK */
    class a extends TimerTask {
        a() {
        }

        public final void run() {
            BannerBase.this.post(new Runnable() {
                public final void run() {
                    if (BannerBase.this.isShown() || (BannerBase.this.a != null && !BannerBase.this.a.a())) {
                        BannerBase.this.l();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i2);

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public abstract int c();

    /* access modifiers changed from: protected */
    public abstract String d();

    /* access modifiers changed from: protected */
    public abstract void e();

    /* access modifiers changed from: protected */
    public abstract int f();

    /* access modifiers changed from: protected */
    public abstract int g();

    /* access modifiers changed from: protected */
    public abstract int h();

    /* access modifiers changed from: protected */
    public abstract void hideBanner();

    /* access modifiers changed from: protected */
    public View n() {
        return this;
    }

    public abstract void setAdTag(String str);

    public BannerBase(Context context) {
        super(context);
        this.h = false;
        this.j = 0;
        this.k = true;
        this.d = false;
        this.e = new Random().nextInt(DefaultOggSeeker.MATCH_BYTE_RANGE) + 159868227;
        this.f = this.e + 1;
        this.g = null;
        this.m = false;
        this.n = false;
        this.p = null;
        this.q = new Timer();
        this.r = new a();
    }

    public BannerBase(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BannerBase(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.h = false;
        this.j = 0;
        this.k = true;
        this.d = false;
        this.e = new Random().nextInt(DefaultOggSeeker.MATCH_BYTE_RANGE) + 159868227;
        this.f = this.e + 1;
        this.g = null;
        this.m = false;
        this.n = false;
        this.p = null;
        this.q = new Timer();
        this.r = new a();
        setAdTag(new b(context, attributeSet).a());
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!isInEditMode()) {
            b();
            return;
        }
        setMinimumWidth(r.a(getContext(), 300));
        setMinimumHeight(r.a(getContext(), c()));
        setBackgroundColor(Color.rgb(169, 169, 169));
        TextView textView = new TextView(getContext());
        textView.setText(d());
        textView.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        addView(textView, layoutParams);
    }

    /* access modifiers changed from: protected */
    public int i() {
        return f();
    }

    /* access modifiers changed from: protected */
    public final String j() {
        return this.g;
    }

    public void loadAd(int i2, int i3) {
        if (getParent() == null) {
            this.b = true;
            this.c = new Point(i2, i3);
            k();
        }
    }

    public void loadAd() {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        loadAd(r.b(getContext(), displayMetrics.widthPixels), r.b(getContext(), displayMetrics.heightPixels));
    }

    /* access modifiers changed from: protected */
    public final void k() {
        o();
        l();
    }

    /* access modifiers changed from: protected */
    public final void l() {
        r();
        if (this.a == null || AdaptMetaData.a().b().a()) {
            this.a = AdaptMetaData.a().b().a(Placement.INAPP_BANNER, this.g);
            if (this.a.a()) {
                e();
            } else {
                hideBanner();
            }
        } else {
            if (this.a.a()) {
                e();
            }
        }
    }

    private void r() {
        if (this.l != null) {
            this.l.b();
            this.l = null;
        }
    }

    protected static int m() {
        return BannerMetaData.a().b().q();
    }

    /* access modifiers changed from: protected */
    public final void a(h hVar) {
        if (this.l == null) {
            this.l = new b(n(), hVar, m());
            this.l.a();
        }
    }

    /* access modifiers changed from: protected */
    public void o() {
        if (this.h && !isInEditMode()) {
            if (this.r != null) {
                this.r.cancel();
            }
            if (this.q != null) {
                this.q.cancel();
            }
            this.r = new a();
            this.q = new Timer();
            this.q.schedule(this.r, (long) i());
            if (this.p != null) {
                this.p.cancel(false);
            }
            this.p = ThreadManager.a((Runnable) new Runnable() {
                public final void run() {
                    BannerBase.this.k();
                }
            }, (long) (MetaData.D().C() * 1000));
        }
    }

    /* access modifiers changed from: protected */
    public final void p() {
        if (!isInEditMode()) {
            if (this.r != null) {
                this.r.cancel();
            }
            if (this.q != null) {
                this.q.cancel();
            }
            if (this.p != null) {
                this.p.cancel(false);
            }
            this.r = null;
            this.q = null;
            this.p = null;
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        if (isClicked()) {
            setClicked(false);
            this.n = true;
        }
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putInt(RequestParameters.BANNER_ID, h());
        bundle.putParcelable("upperState", onSaveInstanceState);
        bundle.putSerializable("adRulesResult", this.a);
        bundle.putSerializable("adPreferences", this.i);
        bundle.putInt("offset", this.j);
        bundle.putBoolean("firstLoad", this.k);
        bundle.putBoolean("shouldReloadBanner", this.n);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof Bundle)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        Bundle bundle = (Bundle) parcelable;
        a(bundle.getInt(RequestParameters.BANNER_ID));
        this.a = (AdRulesResult) bundle.getSerializable("adRulesResult");
        this.i = (AdPreferences) bundle.getSerializable("adPreferences");
        this.j = bundle.getInt("offset");
        this.k = bundle.getBoolean("firstLoad");
        this.n = bundle.getBoolean("shouldReloadBanner");
        super.onRestoreInstanceState(bundle.getParcelable("upperState"));
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.h = true;
        o();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.h = false;
        p();
        r();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            if (this.n) {
                this.n = false;
                l();
            }
            this.h = true;
            o();
            return;
        }
        this.h = false;
        p();
    }

    public boolean isFirstLoad() {
        return this.k;
    }

    public void setFirstLoad(boolean z) {
        this.k = z;
    }

    /* access modifiers changed from: protected */
    public final void q() {
        if (isFirstLoad() || AdaptMetaData.a().b().a()) {
            setFirstLoad(false);
            com.startapp.sdk.adsbase.adrules.b.a().a(new com.startapp.sdk.adsbase.adrules.a(Placement.INAPP_BANNER, this.g));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences) {
        adPreferences.setHardwareAccelerated(com.startapp.common.b.b.a((View) this, this.h));
    }

    public boolean isClicked() {
        return this.m;
    }

    public void setClicked(boolean z) {
        this.m = z;
    }

    public void setErrorMessage(String str) {
        this.o = str;
    }

    public String getErrorMessage() {
        return this.o;
    }
}
