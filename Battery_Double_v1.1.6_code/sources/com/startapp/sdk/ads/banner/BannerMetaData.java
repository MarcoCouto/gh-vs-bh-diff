package com.startapp.sdk.ads.banner;

import android.content.Context;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class BannerMetaData implements Serializable {
    private static Object a = new Object();
    private static volatile BannerMetaData b = new BannerMetaData();
    private static final long serialVersionUID = 1;
    @d(a = true)
    private BannerOptions BannerOptions = new BannerOptions();
    private String bannerMetadataUpdateVersion = AdsConstants.d;

    public static BannerMetaData a() {
        return b;
    }

    public final BannerOptions b() {
        return this.BannerOptions;
    }

    public final BannerOptions c() {
        return new BannerOptions(this.BannerOptions);
    }

    public static void a(Context context, BannerMetaData bannerMetaData) {
        synchronized (a) {
            bannerMetaData.bannerMetadataUpdateVersion = AdsConstants.d;
            b = bannerMetaData;
            com.startapp.common.b.d.a(context, "StartappBannerMetadata", (Serializable) bannerMetaData);
        }
    }

    public static void a(Context context) {
        BannerMetaData bannerMetaData = (BannerMetaData) com.startapp.common.b.d.a(context, "StartappBannerMetadata");
        BannerMetaData bannerMetaData2 = new BannerMetaData();
        if (bannerMetaData != null) {
            boolean a2 = s.a(bannerMetaData, bannerMetaData2);
            if (!(!AdsConstants.d.equals(bannerMetaData.bannerMetadataUpdateVersion)) && a2) {
                new e(InfoEventCategory.ERROR).e("metadata_null").a(context);
            }
            b = bannerMetaData;
            return;
        }
        b = bannerMetaData2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BannerMetaData bannerMetaData = (BannerMetaData) obj;
        return s.b(this.BannerOptions, bannerMetaData.BannerOptions) && s.b(this.bannerMetadataUpdateVersion, bannerMetaData.bannerMetadataUpdateVersion);
    }

    public int hashCode() {
        return s.a(this.BannerOptions, this.bannerMetadataUpdateVersion);
    }
}
