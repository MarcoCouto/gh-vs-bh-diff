package com.startapp.sdk.ads.banner;

import android.content.Context;
import android.util.AttributeSet;

/* compiled from: StartAppSDK */
final class b {
    private Context a;
    private String b;

    b(Context context, AttributeSet attributeSet) {
        this.a = context;
        this.b = a(attributeSet, "adTag");
    }

    private String a(AttributeSet attributeSet, String str) {
        try {
            int attributeResourceValue = attributeSet.getAttributeResourceValue(null, str, -1);
            if (attributeResourceValue != -1) {
                return this.a.getResources().getString(attributeResourceValue);
            }
            return attributeSet.getAttributeValue(null, str);
        } catch (Exception unused) {
            return null;
        }
    }

    public final String a() {
        return this.b;
    }
}
