package com.startapp.sdk.ads.banner.banner3d;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowManager;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.iab.omid.library.startapp.b;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.ads.banner.BannerOptions;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.f.a.f;
import java.net.URL;

/* compiled from: StartAppSDK */
public final class Banner3DSize {
    private final String a;
    private final URL b;
    private final String c;

    /* compiled from: StartAppSDK */
    public enum Size {
        XXSMALL(new f(280, 50)),
        XSMALL(new f(300, 50)),
        SMALL(new f(ModuleDescriptor.MODULE_VERSION, 50)),
        MEDIUM(new f(468, 60)),
        LARGE(new f(728, 90)),
        XLARGE(new f(1024, 90));
        
        private f size$6f4f468b;

        private Size(f fVar) {
            this.size$6f4f468b = fVar;
        }

        public final f getSize$3e27e976() {
            return this.size$6f4f468b;
        }
    }

    public static boolean a(Context context, ViewParent viewParent, BannerOptions bannerOptions, Banner3D banner3D, f fVar) {
        Size[] values;
        f a2 = a(context, viewParent, bannerOptions, banner3D);
        fVar.a(a2.a(), a2.b());
        boolean z = false;
        for (Size size : Size.values()) {
            if (size.getSize$3e27e976().a() <= a2.a() && size.getSize$3e27e976().b() <= a2.b()) {
                StringBuilder sb = new StringBuilder("BannerSize [");
                sb.append(size.getSize$3e27e976().a());
                sb.append(",");
                sb.append(size.getSize$3e27e976().b());
                sb.append(RequestParameters.RIGHT_BRACKETS);
                bannerOptions.a(size.getSize$3e27e976().a(), size.getSize$3e27e976().b());
                z = true;
            }
        }
        if (!z) {
            bannerOptions.a(0, 0);
        }
        StringBuilder sb2 = new StringBuilder("============== Optimize Size [");
        sb2.append(z);
        sb2.append("] ==========");
        return z;
    }

    private static f a(Context context, ViewParent viewParent, BannerOptions bannerOptions, Banner3D banner3D) {
        Point point = new Point();
        point.x = bannerOptions.d();
        point.y = bannerOptions.e();
        if (banner3D.getLayoutParams() != null && banner3D.getLayoutParams().width > 0) {
            point.x = r.b(context, banner3D.getLayoutParams().width + 1);
        }
        if (banner3D.getLayoutParams() != null && banner3D.getLayoutParams().height > 0) {
            point.y = r.b(context, banner3D.getLayoutParams().height + 1);
        }
        if (banner3D.getLayoutParams() == null || banner3D.getLayoutParams().width <= 0 || banner3D.getLayoutParams().height <= 0) {
            if (context instanceof Activity) {
                View decorView = ((Activity) context).getWindow().getDecorView();
                try {
                    View view = (View) viewParent;
                    if (view instanceof Banner) {
                        view = (View) view.getParent();
                    }
                    boolean z = false;
                    boolean z2 = false;
                    while (view != null && (view.getMeasuredWidth() <= 0 || view.getMeasuredHeight() <= 0)) {
                        if (view.getMeasuredWidth() > 0 && !z) {
                            b(context, point, view);
                            z = true;
                        }
                        if (view.getMeasuredHeight() > 0 && !z2) {
                            a(context, point, view);
                            z2 = true;
                        }
                        view = (View) view.getParent();
                    }
                    if (view == null) {
                        c(context, point, decorView);
                    } else {
                        if (!z) {
                            b(context, point, view);
                        }
                        if (!z2) {
                            a(context, point, view);
                        }
                    }
                } catch (Exception unused) {
                    c(context, point, decorView);
                }
            } else {
                try {
                    WindowManager windowManager = (WindowManager) context.getSystemService("window");
                    if (windowManager != null) {
                        if (VERSION.SDK_INT >= 13) {
                            windowManager.getDefaultDisplay().getSize(point);
                        } else {
                            point.x = windowManager.getDefaultDisplay().getWidth();
                            point.y = windowManager.getDefaultDisplay().getHeight();
                        }
                        point.x = r.b(context, point.x);
                        point.y = r.b(context, point.y);
                    }
                } catch (Throwable th) {
                    new e(th).a(context);
                }
            }
        }
        StringBuilder sb = new StringBuilder("============ exit Application Size [");
        sb.append(point.x);
        sb.append(",");
        sb.append(point.y);
        sb.append("] =========");
        return new f(point.x, point.y);
    }

    private static void a(Context context, Point point, View view) {
        point.y = r.b(context, (view.getMeasuredHeight() - view.getPaddingBottom()) - view.getPaddingTop());
    }

    private static void b(Context context, Point point, View view) {
        point.x = r.b(context, (view.getMeasuredWidth() - view.getPaddingLeft()) - view.getPaddingRight());
    }

    private static void c(Context context, Point point, View view) {
        point.x = r.b(context, view.getMeasuredWidth());
        point.y = r.b(context, view.getMeasuredHeight());
    }

    private Banner3DSize(String str, URL url, String str2) {
        this.a = str;
        this.b = url;
        this.c = str2;
    }

    public static Banner3DSize a(String str, URL url, String str2) {
        b.b(str, "VendorKey is null or empty");
        b.a((Object) url, "ResourceURL is null");
        b.b(str2, "VerificationParameters is null or empty");
        return new Banner3DSize(str, url, str2);
    }

    public final String a() {
        return this.a;
    }

    public final URL b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }
}
