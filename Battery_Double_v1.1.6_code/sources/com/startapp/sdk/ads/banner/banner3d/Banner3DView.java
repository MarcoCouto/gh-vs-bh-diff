package com.startapp.sdk.ads.banner.banner3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.common.b.b;
import com.startapp.sdk.ads.banner.banner3d.Banner3DSize.Size;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.json.RatingBar;

/* compiled from: StartAppSDK */
public class Banner3DView extends RelativeLayout {
    private TextView a;
    private TextView b;
    private ImageView c;
    private RatingBar d;
    private TextView e;
    private Point f;

    /* compiled from: StartAppSDK */
    private enum Template {
        XS,
        S,
        M,
        L,
        XL
    }

    public Banner3DView(Context context) {
        super(context);
        a();
    }

    public Banner3DView(Context context, Point point) {
        super(context);
        this.f = point;
        a();
    }

    public Banner3DView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public Banner3DView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        int i;
        int i2;
        Context context = getContext();
        Template b2 = b();
        setBackgroundDrawable(new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{AdsCommonMetaData.a().o(), AdsCommonMetaData.a().p()}));
        setLayoutParams(new LayoutParams(-2, -2));
        int a2 = r.a(context, 2);
        int a3 = r.a(context, 3);
        r.a(context, 4);
        int a4 = r.a(context, 5);
        int a5 = r.a(context, 6);
        int a6 = r.a(context, 8);
        r.a(context, 10);
        int a7 = r.a(context, 20);
        r.a(context, 84);
        int a8 = r.a(context, 90);
        setPadding(a4, 0, a4, 0);
        setTag(this);
        this.c = new ImageView(context);
        this.c.setId(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a8, a8);
        layoutParams.addRule(15);
        this.c.setLayoutParams(layoutParams);
        this.a = new TextView(context);
        this.a.setId(2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(b.a(17), 1);
        layoutParams2.addRule(14);
        this.a.setLayoutParams(layoutParams2);
        this.a.setTextColor(AdsCommonMetaData.a().r().intValue());
        this.a.setGravity(b.a((int) GravityCompat.START));
        this.a.setBackgroundColor(0);
        switch (b2) {
            case XS:
            case S:
                this.a.setTextSize(17.0f);
                this.a.setPadding(a3, 0, 0, a2);
                Context context2 = getContext();
                double d2 = (double) this.f.x;
                Double.isNaN(d2);
                layoutParams2.width = r.a(context2, (int) (d2 * 0.55d));
                break;
            case M:
                this.a.setTextSize(17.0f);
                this.a.setPadding(a3, 0, 0, a2);
                Context context3 = getContext();
                double d3 = (double) this.f.x;
                Double.isNaN(d3);
                layoutParams2.width = r.a(context3, (int) (d3 * 0.65d));
                break;
            case L:
            case XL:
                this.a.setTextSize(22.0f);
                this.a.setPadding(a3, 0, 0, a4);
                break;
        }
        this.a.setSingleLine(true);
        this.a.setEllipsize(TruncateAt.END);
        r.a(this.a, AdsCommonMetaData.a().s());
        this.b = new TextView(context);
        this.b.setId(3);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(b.a(17), 1);
        layoutParams3.addRule(3, 2);
        layoutParams3.setMargins(0, 0, 0, a4);
        this.b.setLayoutParams(layoutParams3);
        this.b.setTextColor(AdsCommonMetaData.a().u().intValue());
        this.b.setTextSize(18.0f);
        this.b.setMaxLines(2);
        this.b.setLines(2);
        this.b.setSingleLine(false);
        this.b.setEllipsize(TruncateAt.MARQUEE);
        this.b.setHorizontallyScrolling(true);
        this.b.setPadding(a3, 0, 0, 0);
        this.d = new RatingBar(getContext());
        this.d.setId(5);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        switch (b2) {
            case XS:
            case S:
            case M:
                i2 = a3;
                layoutParams4.addRule(b.a(17), 1);
                layoutParams4.addRule(8, 1);
                break;
            case L:
            case XL:
                layoutParams4.addRule(b.a(17), 2);
                Context context4 = getContext();
                i2 = a3;
                double d4 = (double) this.f.x;
                Double.isNaN(d4);
                layoutParams3.width = r.a(context4, (int) (d4 * 0.6d));
                break;
            default:
                i = a3;
                break;
        }
        i = i2;
        layoutParams4.setMargins(i, a6, i, 0);
        this.d.setLayoutParams(layoutParams4);
        this.e = new TextView(context);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        switch (b2) {
            case XS:
            case S:
            case M:
                this.e.setTextSize(13.0f);
                layoutParams5.addRule(b.a(17), 2);
                layoutParams5.addRule(15);
                break;
            case L:
                layoutParams5.addRule(b.a(17), 3);
                layoutParams5.addRule(15);
                layoutParams5.setMargins(a7, 0, 0, 0);
                this.e.setTextSize(26.0f);
                break;
            case XL:
                layoutParams5.addRule(b.a(17), 3);
                layoutParams5.addRule(15);
                layoutParams5.setMargins(a7 * 7, 0, 0, 0);
                this.e.setTextSize(26.0f);
                break;
        }
        this.e.setPadding(a5, a5, a5, a5);
        this.e.setLayoutParams(layoutParams5);
        setButtonText(false);
        this.e.setTextColor(-1);
        this.e.setTypeface(null, 1);
        this.e.setId(4);
        this.e.setShadowLayer(2.5f, -3.0f, 3.0f, -9013642);
        this.e.setBackgroundDrawable(new ShapeDrawable(new RoundRectShape(new float[]{10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f}, null, null)) {
            /* access modifiers changed from: protected */
            public final void onDraw(Shape shape, Canvas canvas, Paint paint) {
                paint.setColor(-11363070);
                paint.setMaskFilter(new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 5.0f, 3.0f));
                super.onDraw(shape, canvas, paint);
            }
        });
        addView(this.c);
        addView(this.a);
        switch (b2) {
            case XS:
            case S:
            case M:
                addView(this.e);
                break;
            case L:
            case XL:
                addView(this.e);
                addView(this.b);
                break;
        }
        addView(this.d);
    }

    public void setText(String str) {
        this.a.setText(str);
    }

    public void setImage(Bitmap bitmap) {
        this.c.setImageBitmap(bitmap);
    }

    public void setImage(int i, int i2, int i3) {
        this.c.setImageResource(i);
        LayoutParams layoutParams = this.c.getLayoutParams();
        layoutParams.width = i2;
        layoutParams.height = i3;
        this.c.setLayoutParams(layoutParams);
    }

    public void setRating(float f2) {
        try {
            this.d.setRating(f2);
        } catch (NullPointerException unused) {
        }
    }

    public void setImage(Bitmap bitmap, int i, int i2) {
        this.c.setImageBitmap(bitmap);
        LayoutParams layoutParams = this.c.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        this.c.setLayoutParams(layoutParams);
    }

    public void setDescription(String str) {
        if (str != null && str.compareTo("") != 0) {
            String[] a2 = a(str);
            String str2 = a2[0];
            String str3 = "";
            if (a2[1] != null) {
                str3 = a(a2[1])[0];
            }
            if (str.length() >= 110) {
                StringBuilder sb = new StringBuilder();
                sb.append(str3);
                sb.append("...");
                str3 = sb.toString();
            }
            TextView textView = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append("\n");
            sb2.append(str3);
            textView.setText(sb2.toString());
        }
    }

    private static String[] a(String str) {
        boolean z;
        String[] strArr = new String[2];
        int i = 55;
        if (str.length() > 55) {
            char[] charArray = str.substring(0, 55).toCharArray();
            int length = charArray.length - 1;
            int i2 = length - 1;
            while (true) {
                if (i2 <= 0) {
                    z = false;
                    break;
                } else if (charArray[i2] == ' ') {
                    length = i2;
                    z = true;
                    break;
                } else {
                    i2--;
                }
            }
            if (z) {
                i = length;
            }
            strArr[0] = str.substring(0, i);
            strArr[1] = str.substring(i + 1, str.length());
        } else {
            strArr[0] = str;
            strArr[1] = null;
        }
        return strArr;
    }

    private Template b() {
        Template template = Template.S;
        if (this.f.x > Size.SMALL.getSize$3e27e976().a() || this.f.y > Size.SMALL.getSize$3e27e976().b()) {
            template = Template.M;
        }
        if (this.f.x > Size.MEDIUM.getSize$3e27e976().a() || this.f.y > Size.MEDIUM.getSize$3e27e976().b()) {
            template = Template.L;
        }
        return (this.f.x > Size.LARGE.getSize$3e27e976().a() || this.f.y > Size.LARGE.getSize$3e27e976().b()) ? Template.XL : template;
    }

    public void setButtonText(boolean z) {
        if (z) {
            this.e.setText("OPEN");
        } else {
            this.e.setText("DOWNLOAD");
        }
    }
}
