package com.startapp.sdk.ads.banner.banner3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.a.C0063a;
import com.startapp.sdk.ads.banner.BannerOptions;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: StartAppSDK */
public class a implements Parcelable, C0063a {
    public static final Creator<a> CREATOR = new Creator<a>() {
        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new a[i];
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new a(parcel);
        }
    };
    private AdDetails a;
    private Point b;
    private Bitmap c = null;
    private Bitmap d = null;
    private AtomicBoolean e = new AtomicBoolean(false);
    private TrackingParams f;
    private h g = null;
    private Banner3DView h = null;

    public int describeContents() {
        return 0;
    }

    public a(Context context, ViewGroup viewGroup, AdDetails adDetails, BannerOptions bannerOptions, TrackingParams trackingParams) {
        this.a = adDetails;
        this.f = trackingParams;
        a(context, bannerOptions, viewGroup);
    }

    public final Bitmap a() {
        return this.d;
    }

    public final void a(Context context, BannerOptions bannerOptions, ViewGroup viewGroup) {
        int a2 = r.a(context, bannerOptions.e() - 5);
        this.b = new Point((int) (((float) r.a(context, bannerOptions.d())) * bannerOptions.j()), (int) (((float) r.a(context, bannerOptions.e())) * bannerOptions.k()));
        this.h = new Banner3DView(context, new Point(bannerOptions.d(), bannerOptions.e()));
        this.h.setText(this.a.f());
        this.h.setRating(this.a.k());
        this.h.setDescription(this.a.g());
        this.h.setButtonText(this.a.r());
        if (this.c != null) {
            this.h.setImage(this.c, a2, a2);
        } else {
            this.h.setImage(17301651, a2, a2);
            new com.startapp.common.a(this.a.h(), this, 0).a();
            StringBuilder sb = new StringBuilder(" Banner Face Image Async Request: [");
            sb.append(this.a.f());
            sb.append(RequestParameters.RIGHT_BRACKETS);
        }
        LayoutParams layoutParams = new LayoutParams(this.b.x, this.b.y);
        layoutParams.addRule(13);
        viewGroup.addView(this.h, layoutParams);
        this.h.setVisibility(8);
        d();
    }

    private void d() {
        this.d = a((View) this.h);
        if (this.b.x > 0 && this.b.y > 0) {
            this.d = Bitmap.createScaledBitmap(this.d, this.b.x, this.b.y, false);
        }
    }

    private static Bitmap a(View view) {
        view.measure(view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap createBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.draw(canvas);
        return createBitmap;
    }

    public final void a(Bitmap bitmap, int i) {
        if (bitmap != null && this.h != null) {
            this.c = bitmap;
            this.h.setImage(bitmap);
            d();
        }
    }

    public final void b() {
        if (this.g != null) {
            this.g.a(false);
        }
    }

    public a(Parcel parcel) {
        this.a = (AdDetails) parcel.readParcelable(AdDetails.class.getClassLoader());
        this.b = new Point(1, 1);
        this.b.x = parcel.readInt();
        this.b.y = parcel.readInt();
        this.c = (Bitmap) parcel.readParcelable(Bitmap.class.getClassLoader());
        boolean[] zArr = new boolean[1];
        parcel.readBooleanArray(zArr);
        this.e.set(zArr[0]);
        this.f = (TrackingParams) parcel.readSerializable();
    }

    private static void a(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
        }
    }

    public final h a(Context context) {
        long millis;
        if (this.a.d() == null || !this.e.compareAndSet(false, true)) {
            return null;
        }
        String[] strArr = {this.a.d()};
        TrackingParams trackingParams = this.f;
        if (this.a.y() != null) {
            millis = TimeUnit.SECONDS.toMillis(this.a.y().longValue());
        } else {
            millis = TimeUnit.SECONDS.toMillis(MetaData.D().E());
        }
        h hVar = new h(context, strArr, trackingParams, millis);
        this.g = hVar;
        return this.g;
    }

    public final void b(Context context) {
        String q = this.a.q();
        boolean a2 = com.startapp.sdk.adsbase.a.a(context, Placement.INAPP_BANNER);
        if (this.g != null) {
            this.g.a(true);
        }
        if (q != null && !"null".equals(q) && !TextUtils.isEmpty(q)) {
            com.startapp.sdk.adsbase.a.a(q, this.a.p(), this.a.c(), context, this.f);
        } else if (!this.a.l() || a2) {
            com.startapp.sdk.adsbase.a.a(context, this.a.c(), this.a.e(), this.f, this.a.w() && !a2, false);
        } else {
            com.startapp.sdk.adsbase.a.a(context, this.a.c(), this.a.e(), this.a.n(), this.f, AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), this.a.w(), this.a.z());
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, i);
        parcel.writeInt(this.b.x);
        parcel.writeInt(this.b.y);
        parcel.writeParcelable(this.c, i);
        parcel.writeBooleanArray(new boolean[]{this.e.get()});
        parcel.writeSerializable(this.f);
    }

    public final void c() {
        a(this.c);
        a(this.d);
        this.c = null;
        this.d = null;
        if (this.g != null) {
            this.g.a(false);
        }
        if (this.h != null) {
            this.h.removeAllViews();
            this.h = null;
        }
    }
}
