package com.startapp.sdk.ads.banner.banner3d;

import android.content.Context;
import com.startapp.sdk.adsbase.JsonAd;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public class Banner3DAd extends JsonAd {
    private static final long serialVersionUID = 1;
    private boolean fixedSize;
    private int offset;

    public Banner3DAd(Context context, int i) {
        super(context, Placement.INAPP_BANNER);
        this.offset = i;
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        b bVar2 = new b(this.a, this, this.offset, adPreferences, bVar);
        bVar2.c();
        this.offset++;
    }

    public final int a() {
        return this.offset;
    }

    public final void a_() {
        this.fixedSize = true;
    }

    public final boolean b_() {
        return this.fixedSize;
    }
}
