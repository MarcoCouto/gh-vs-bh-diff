package com.startapp.sdk.ads.banner.banner3d;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View.BaseSavedState;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.startapp.common.b.b;
import com.startapp.sdk.ads.banner.BannerBase;
import com.startapp.sdk.ads.banner.BannerInterface;
import com.startapp.sdk.ads.banner.BannerListener;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.banner.BannerOptions;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.JsonAd;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject.Size;
import com.startapp.sdk.adsbase.adinformation.AdInformationOverrides;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.b.c;
import com.startapp.sdk.f.a.f;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: StartAppSDK */
public class Banner3D extends BannerBase implements BannerInterface, AdEventListener {
    private boolean A;
    private boolean B;
    private boolean C;
    private boolean D;
    private AdInformationOverrides E;
    private int F;
    private BannerListener G;
    private Runnable H;
    protected BannerOptions h;
    protected List<AdDetails> i;
    protected AdPreferences j;
    protected float k;
    protected boolean l;
    protected boolean m;
    protected boolean n;
    protected boolean o;
    protected boolean p;
    protected boolean q;
    protected boolean r;
    protected boolean s;
    protected boolean t;
    protected List<a> u;
    private Banner3DAd v;
    private Camera w;
    private Matrix x;
    private Paint y;
    private float z;

    /* compiled from: StartAppSDK */
    private static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            public final SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public final SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        public AdRulesResult adRulesResult;
        public boolean bDefaultLoad;
        public boolean bIsVisible;
        private int currentImage;
        private AdDetails[] details;
        public a[] faces;
        private int firstRotation;
        private int firstRotationFinished;
        public boolean loaded;
        public boolean loading;
        public BannerOptions options;
        public AdInformationOverrides overrides;
        private float rotation;

        public int describeContents() {
            return 0;
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void setCurrentImage(int i) {
            this.currentImage = i;
        }

        public int getCurrentImage() {
            return this.currentImage;
        }

        public void setRotation(float f) {
            this.rotation = f;
        }

        public float getRotation() {
            return this.rotation;
        }

        public boolean isFirstRotation() {
            return this.firstRotation == 1;
        }

        public void setFirstRotation(boolean z) {
            this.firstRotation = z ? 1 : 0;
        }

        public boolean isFirstRotationFinished() {
            return this.firstRotationFinished == 1;
        }

        public void setFirstRotationFinished(boolean z) {
            this.firstRotationFinished = z ? 1 : 0;
        }

        public void setDetails(List<AdDetails> list) {
            this.details = new AdDetails[list.size()];
            for (int i = 0; i < list.size(); i++) {
                this.details[i] = (AdDetails) list.get(i);
            }
        }

        public List<AdDetails> getDetails() {
            return Arrays.asList(this.details);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            if (parcel.readInt() == 1) {
                this.bIsVisible = true;
                this.currentImage = parcel.readInt();
                this.rotation = parcel.readFloat();
                this.firstRotation = parcel.readInt();
                this.firstRotationFinished = parcel.readInt();
                Parcelable[] readParcelableArray = parcel.readParcelableArray(AdDetails.class.getClassLoader());
                if (readParcelableArray != null) {
                    this.details = new AdDetails[readParcelableArray.length];
                    System.arraycopy(readParcelableArray, 0, this.details, 0, readParcelableArray.length);
                }
                int readInt = parcel.readInt();
                this.loaded = false;
                if (readInt == 1) {
                    this.loaded = true;
                }
                int readInt2 = parcel.readInt();
                this.loading = false;
                if (readInt2 == 1) {
                    this.loading = true;
                }
                int readInt3 = parcel.readInt();
                this.bDefaultLoad = false;
                if (readInt3 == 1) {
                    this.bDefaultLoad = true;
                }
                int readInt4 = parcel.readInt();
                if (readInt4 > 0) {
                    this.faces = new a[readInt4];
                    for (int i = 0; i < readInt4; i++) {
                        this.faces[i] = (a) parcel.readParcelable(a.class.getClassLoader());
                    }
                }
                this.overrides = (AdInformationOverrides) parcel.readSerializable();
                this.options = (BannerOptions) parcel.readSerializable();
                this.adRulesResult = (AdRulesResult) parcel.readSerializable();
                return;
            }
            this.bIsVisible = false;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            if (!this.bIsVisible) {
                parcel.writeInt(0);
                return;
            }
            parcel.writeInt(1);
            parcel.writeInt(this.currentImage);
            parcel.writeFloat(this.rotation);
            parcel.writeInt(this.firstRotation);
            parcel.writeInt(this.firstRotationFinished);
            parcel.writeParcelableArray(this.details, i);
            parcel.writeInt(this.loaded ? 1 : 0);
            parcel.writeInt(this.loading ? 1 : 0);
            parcel.writeInt(this.bDefaultLoad ? 1 : 0);
            parcel.writeInt(this.faces.length);
            for (a writeParcelable : this.faces) {
                parcel.writeParcelable(writeParcelable, i);
            }
            parcel.writeSerializable(this.overrides);
            parcel.writeSerializable(this.options);
            parcel.writeSerializable(this.adRulesResult);
        }
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return 50;
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return "StartApp Banner3D";
    }

    public Banner3D(Activity activity) {
        this((Context) activity);
    }

    public Banner3D(Activity activity, AdPreferences adPreferences) {
        this((Context) activity, adPreferences);
    }

    public Banner3D(Activity activity, BannerListener bannerListener) {
        this((Context) activity, bannerListener);
    }

    public Banner3D(Activity activity, AdPreferences adPreferences, BannerListener bannerListener) {
        this((Context) activity, adPreferences, bannerListener);
    }

    public Banner3D(Activity activity, boolean z2) {
        this((Context) activity, z2);
    }

    public Banner3D(Activity activity, boolean z2, AdPreferences adPreferences) {
        this((Context) activity, z2, adPreferences);
    }

    public Banner3D(Activity activity, AttributeSet attributeSet) {
        this((Context) activity, attributeSet);
    }

    public Banner3D(Activity activity, AttributeSet attributeSet, int i2) {
        this((Context) activity, attributeSet, i2);
    }

    @Deprecated
    public Banner3D(Context context) {
        this(context, true, (AdPreferences) null);
    }

    @Deprecated
    public Banner3D(Context context, AdPreferences adPreferences) {
        this(context, true, adPreferences);
    }

    @Deprecated
    public Banner3D(Context context, BannerListener bannerListener) {
        this(context, true, (AdPreferences) null);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public Banner3D(Context context, AdPreferences adPreferences, BannerListener bannerListener) {
        this(context, true, adPreferences);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public Banner3D(Context context, boolean z2) {
        this(context, z2, (AdPreferences) null);
    }

    @Deprecated
    public Banner3D(Context context, boolean z2, AdPreferences adPreferences) {
        super(context);
        this.w = null;
        this.x = null;
        this.y = null;
        this.k = 45.0f;
        this.z = 0.0f;
        this.l = true;
        this.m = false;
        this.n = true;
        this.o = false;
        this.p = false;
        this.A = false;
        this.B = false;
        this.q = true;
        this.r = true;
        this.s = false;
        this.C = false;
        this.t = false;
        this.D = true;
        this.u = new ArrayList();
        this.F = 0;
        this.H = new Runnable() {
            public void run() {
                if (Banner3D.this.s && Banner3D.this.u.size() != 0) {
                    if (Banner3D.this.q && Banner3D.this.isShown() && Banner3D.this.d) {
                        Banner3D.a(Banner3D.this, (a) Banner3D.this.u.get(Banner3D.this.s()));
                        if (!Banner3D.this.p) {
                            Banner3D.this.p = true;
                            Banner3D.this.q();
                        }
                    }
                    if (Banner3D.this.l) {
                        Banner3D.this.a((float) (Banner3D.this.r().b() * (!Banner3D.this.o ? Banner3D.this.h.p() : 1)));
                    }
                    if (Banner3D.this.k <= ((float) (90 - Banner3D.this.r().b())) || Banner3D.this.k >= ((float) (Banner3D.this.r().b() + 90)) || Banner3D.this.n) {
                        Banner3D.this.postDelayed(this, (long) Banner3D.this.r().a());
                        Banner3D.this.m = true;
                    } else {
                        if (Banner3D.this.t) {
                            Banner3D.this.postDelayed(this, (long) Banner3D.this.r().c());
                        }
                        Banner3D.this.m = false;
                    }
                    if (Banner3D.this.t() == 0) {
                        Banner3D.this.n = false;
                    }
                }
            }
        };
        try {
            this.r = z2;
            if (adPreferences == null) {
                this.j = new AdPreferences();
            } else {
                this.j = adPreferences;
            }
            a();
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    @Deprecated
    public Banner3D(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Deprecated
    public Banner3D(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.w = null;
        this.x = null;
        this.y = null;
        this.k = 45.0f;
        this.z = 0.0f;
        this.l = true;
        this.m = false;
        this.n = true;
        this.o = false;
        this.p = false;
        this.A = false;
        this.B = false;
        this.q = true;
        this.r = true;
        this.s = false;
        this.C = false;
        this.t = false;
        this.D = true;
        this.u = new ArrayList();
        this.F = 0;
        this.H = new Runnable() {
            public void run() {
                if (Banner3D.this.s && Banner3D.this.u.size() != 0) {
                    if (Banner3D.this.q && Banner3D.this.isShown() && Banner3D.this.d) {
                        Banner3D.a(Banner3D.this, (a) Banner3D.this.u.get(Banner3D.this.s()));
                        if (!Banner3D.this.p) {
                            Banner3D.this.p = true;
                            Banner3D.this.q();
                        }
                    }
                    if (Banner3D.this.l) {
                        Banner3D.this.a((float) (Banner3D.this.r().b() * (!Banner3D.this.o ? Banner3D.this.h.p() : 1)));
                    }
                    if (Banner3D.this.k <= ((float) (90 - Banner3D.this.r().b())) || Banner3D.this.k >= ((float) (Banner3D.this.r().b() + 90)) || Banner3D.this.n) {
                        Banner3D.this.postDelayed(this, (long) Banner3D.this.r().a());
                        Banner3D.this.m = true;
                    } else {
                        if (Banner3D.this.t) {
                            Banner3D.this.postDelayed(this, (long) Banner3D.this.r().c());
                        }
                        Banner3D.this.m = false;
                    }
                    if (Banner3D.this.t() == 0) {
                        Banner3D.this.n = false;
                    }
                }
            }
        };
        try {
            a();
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public void hideBanner() {
        this.q = false;
        setVisibility(8);
    }

    public void showBanner() {
        this.q = true;
        u();
    }

    private void u() {
        setVisibility(0);
        if (this.v != null) {
            c.a(getContext()).h().a(Placement.INAPP_BANNER, this.v.getAdId());
        }
    }

    /* access modifiers changed from: protected */
    public final BannerOptions r() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (!this.C) {
            this.h = BannerMetaData.a().c();
            this.i = new ArrayList();
            if (this.j == null) {
                this.j = new AdPreferences();
            }
            this.E = AdInformationOverrides.a();
            D();
            this.u = new ArrayList();
            this.C = true;
            setBackgroundColor(0);
            if (getId() == -1) {
                setId(this.e);
            }
            getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    b.a(Banner3D.this.getViewTreeObserver(), (OnGlobalLayoutListener) this);
                    Banner3D.this.a(Banner3D.this.j);
                    Banner3D.this.b = false;
                    if (Banner3D.this.s) {
                        Banner3D.this.a(Banner3D.this.i, false);
                    } else if (Banner3D.this.r) {
                        Banner3D.this.k();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.s = false;
        this.C = true;
        this.B = false;
        this.l = true;
        this.n = true;
        this.o = false;
        this.p = false;
        this.d = false;
        this.a = null;
        D();
        this.u = new ArrayList();
        this.v = new Banner3DAd(getContext(), g());
        if (this.j == null) {
            this.j = new AdPreferences();
        }
        this.v.load(this.j, this);
    }

    /* access modifiers changed from: protected */
    public final void a(List<AdDetails> list, boolean z2) {
        this.i = list;
        if (list != null) {
            f fVar = new f();
            if (Banner3DSize.a(getContext(), getParent(), this.h, this, fVar)) {
                setMinimumWidth(r.a(getContext(), this.h.d()));
                setMinimumHeight(r.a(getContext(), this.h.e()));
                if (getLayoutParams() != null && getLayoutParams().width == -1) {
                    setMinimumWidth(r.a(getContext(), fVar.a()));
                }
                if (getLayoutParams() != null && getLayoutParams().height == -1) {
                    setMinimumHeight(r.a(getContext(), fVar.b()));
                }
                if (getLayoutParams() != null) {
                    if (getLayoutParams().width > 0) {
                        setMinimumWidth(getLayoutParams().width);
                    }
                    if (getLayoutParams().height > 0) {
                        setMinimumHeight(getLayoutParams().height);
                    }
                    if (getLayoutParams().width > 0 && getLayoutParams().height > 0) {
                        this.v.a_();
                    }
                }
                if (w()) {
                    D();
                    removeAllViews();
                    this.u = new ArrayList();
                    for (AdDetails adDetails : list) {
                        List<a> list2 = this.u;
                        a aVar = new a(getContext(), this, adDetails, this.h, new TrackingParams(j()));
                        list2.add(aVar);
                    }
                    this.F = 0;
                } else {
                    v();
                }
                RelativeLayout relativeLayout = new RelativeLayout(getContext());
                LayoutParams layoutParams = new LayoutParams(C(), B());
                layoutParams.addRule(13);
                int A2 = A();
                layoutParams.rightMargin = A2;
                layoutParams.leftMargin = A2;
                int z3 = z();
                layoutParams.topMargin = z3;
                layoutParams.bottomMargin = z3;
                addView(relativeLayout, layoutParams);
                new AdInformationObject(getContext(), Size.SMALL, Placement.INAPP_BANNER, this.E).a(relativeLayout);
                if (this.y == null) {
                    this.y = new Paint();
                    this.y.setAntiAlias(true);
                    this.y.setFilterBitmap(true);
                }
                if (!this.B) {
                    this.B = true;
                    E();
                }
                if (this.q) {
                    u();
                }
                if (this.G != null && z2) {
                    this.G.onReceiveAd(this);
                    return;
                }
            } else {
                setErrorMessage("Error in banner screen size");
                setVisibility(8);
                if (this.G != null && z2) {
                    this.G.onFailedToReceiveAd(this);
                }
            }
            return;
        }
        setErrorMessage("No ads to load");
        if (this.G != null && z2) {
            this.G.onFailedToReceiveAd(this);
        }
    }

    private void v() {
        for (a a : this.u) {
            a.a(getContext(), this.h, this);
        }
    }

    private boolean w() {
        return this.u == null || this.u.size() == 0;
    }

    /* access modifiers changed from: protected */
    public final int s() {
        return this.F;
    }

    /* access modifiers changed from: protected */
    public final int t() {
        return (this.F + 1) % x();
    }

    private int x() {
        return this.u.size();
    }

    private void y() {
        this.F = ((this.F - 1) + x()) % x();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.d && !this.C) {
            this.d = true;
            E();
        }
        if (!isInEditMode() && this.q && !w()) {
            try {
                int C2 = C();
                int B2 = B();
                int A2 = A();
                int z2 = z();
                float l2 = this.h.l() + (((float) Math.pow((double) (Math.abs(this.k - 45.0f) / 45.0f), (double) this.h.m())) * (1.0f - this.h.l()));
                if (!this.o) {
                    l2 = this.h.l();
                }
                float f = l2;
                Bitmap a = ((a) this.u.get(((this.F - 1) + this.u.size()) % this.u.size())).a();
                Bitmap a2 = ((a) this.u.get(this.F)).a();
                if (!(a2 == null || a == null)) {
                    if (this.k < 45.0f) {
                        if (this.k > 3.0f) {
                            a(canvas, a2, z2, A2, C2 / 2, B2 / 2, f, (this.k - 90.0f) * ((float) this.h.n().getRotationMultiplier()));
                        }
                        a(canvas, a, z2, A2, C2 / 2, B2 / 2, f, this.k * ((float) this.h.n().getRotationMultiplier()));
                        return;
                    }
                    if (this.k < 87.0f) {
                        a(canvas, a, z2, A2, C2 / 2, B2 / 2, f, this.k * ((float) this.h.n().getRotationMultiplier()));
                    }
                    a(canvas, a2, z2, A2, C2 / 2, B2 / 2, f, (this.k - 90.0f) * ((float) this.h.n().getRotationMultiplier()));
                    if (!this.n) {
                        this.o = true;
                    }
                }
            } catch (Exception unused) {
            }
        }
    }

    private int z() {
        return (getHeight() - B()) / 2;
    }

    private int A() {
        return (getWidth() - C()) / 2;
    }

    private int B() {
        return (int) (((float) r.a(getContext(), this.h.e())) * this.h.k());
    }

    private int C() {
        return (int) (((float) r.a(getContext(), this.h.d())) * this.h.j());
    }

    private void a(Canvas canvas, Bitmap bitmap, int i2, int i3, int i4, int i5, float f, float f2) {
        if (this.w == null) {
            this.w = new Camera();
        }
        this.w.save();
        this.w.translate(0.0f, 0.0f, (float) i5);
        this.w.rotateX(f2);
        float f3 = (float) (-i5);
        this.w.translate(0.0f, 0.0f, f3);
        if (this.x == null) {
            this.x = new Matrix();
        }
        this.w.getMatrix(this.x);
        this.w.restore();
        this.x.preTranslate((float) (-i4), f3);
        this.x.postScale(f, f);
        this.x.postTranslate((float) (i3 + i4), (float) (i2 + i5));
        canvas.drawBitmap(bitmap, this.x, this.y);
    }

    private void D() {
        if (this.u != null && !this.u.isEmpty()) {
            for (a aVar : this.u) {
                if (aVar != null) {
                    aVar.c();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(float f) {
        this.k += f;
        if (this.k >= 90.0f) {
            this.F = (this.F + 1) % x();
            this.k -= 90.0f;
        }
        if (this.k <= 0.0f) {
            y();
            this.k += 90.0f;
        }
        invalidate();
    }

    public void onReceiveAd(Ad ad) {
        this.s = true;
        this.C = false;
        this.E = this.v.getAdInfoOverride();
        this.i = ((JsonAd) ad).g();
        a(this.i, this.D);
        this.D = false;
    }

    public void onFailedToReceiveAd(Ad ad) {
        setErrorMessage(ad.getErrorMessage());
        if (this.G != null) {
            this.G.onFailedToReceiveAd(this);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.bIsVisible = this.q;
        savedState.setDetails(this.i);
        savedState.setRotation(this.k);
        savedState.setFirstRotation(this.n);
        savedState.setFirstRotationFinished(this.o);
        savedState.setCurrentImage(this.F);
        savedState.options = this.h;
        savedState.faces = new a[this.u.size()];
        savedState.loaded = this.s;
        savedState.loading = this.C;
        savedState.overrides = this.E;
        for (int i2 = 0; i2 < this.u.size(); i2++) {
            savedState.faces[i2] = (a) this.u.get(i2);
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.q = savedState.bIsVisible;
        if (this.q) {
            this.i = savedState.getDetails();
            this.k = savedState.getRotation();
            this.n = savedState.isFirstRotation();
            this.o = savedState.isFirstRotationFinished();
            this.F = savedState.getCurrentImage();
            a[] aVarArr = savedState.faces;
            D();
            this.u = new ArrayList();
            if (aVarArr != null) {
                for (a add : aVarArr) {
                    this.u.add(add);
                }
            }
            this.s = savedState.loaded;
            this.C = savedState.loading;
            this.r = savedState.bDefaultLoad;
            this.E = savedState.overrides;
            this.h = savedState.options;
            if (this.i.size() == 0) {
                this.r = true;
                a();
                return;
            }
            post(new Runnable() {
                public void run() {
                    Banner3D.this.a(Banner3D.this.i, false);
                }
            });
        }
    }

    private void E() {
        if (this.t && this.d) {
            removeCallbacks(this.H);
            post(this.H);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.t = true;
        if (this.h == null || !this.h.o()) {
            this.n = false;
            this.o = true;
        }
        E();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.t = false;
        removeCallbacks(this.H);
        if (this.u != null) {
            for (a b : this.u) {
                b.b();
            }
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            this.t = true;
            if (this.h == null || !this.h.o()) {
                this.n = false;
                this.o = true;
            }
            E();
            return;
        }
        this.t = false;
        if (!this.m) {
            removeCallbacks(this.H);
        }
    }

    public void setBannerListener(BannerListener bannerListener) {
        this.G = bannerListener;
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return BannerMetaData.a().b().h();
    }

    /* access modifiers changed from: protected */
    public final int g() {
        if (this.v == null) {
            return 0;
        }
        return this.v.a();
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        this.e = i2;
    }

    public void setAdTag(String str) {
        this.g = str;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int C2 = C();
        int B2 = B();
        int A2 = A();
        int z2 = z();
        if (!(motionEvent.getX() >= ((float) A2) && motionEvent.getY() >= ((float) z2) && motionEvent.getX() <= ((float) (A2 + C2)) && motionEvent.getY() <= ((float) (z2 + B2))) || this.u == null || this.u.size() == 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.A = true;
                this.z = motionEvent.getY();
                break;
            case 1:
                if (this.A) {
                    if (this.k < 45.0f) {
                        y();
                    }
                    this.A = false;
                    this.l = false;
                    setClicked(true);
                    postDelayed(new Runnable() {
                        public void run() {
                            Banner3D.this.l = true;
                        }
                    }, AdsCommonMetaData.a().B());
                    ((a) this.u.get(this.F)).b(getContext());
                    if (this.G != null) {
                        this.G.onClick(this);
                        break;
                    }
                }
                break;
            case 2:
                if (this.z - motionEvent.getY() >= 10.0f) {
                    this.A = false;
                    this.z = motionEvent.getY();
                    break;
                }
                break;
        }
        return true;
    }

    static /* synthetic */ void a(Banner3D banner3D, a aVar) {
        h a = aVar.a(banner3D.getContext());
        if (a != null) {
            banner3D.a(a);
        }
    }
}
