package com.startapp.sdk.ads.banner;

/* compiled from: StartAppSDK */
public interface BannerInterface {
    void hideBanner();

    void setBannerListener(BannerListener bannerListener);

    void showBanner();
}
