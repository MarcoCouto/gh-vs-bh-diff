package com.startapp.sdk.ads.banner.bannerstandard;

import android.content.Context;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.model.GetAdRequest;

/* compiled from: StartAppSDK */
public final class a extends com.startapp.sdk.d.a {
    private int i = 0;

    public a(Context context, HtmlAd htmlAd, int i2, AdPreferences adPreferences, b bVar) {
        super(context, htmlAd, adPreferences, bVar, Placement.INAPP_BANNER, false);
        this.i = i2;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        BannerStandardAd bannerStandardAd = (BannerStandardAd) this.b;
        com.startapp.sdk.ads.banner.a aVar = new com.startapp.sdk.ads.banner.a();
        b((GetAdRequest) aVar);
        aVar.c(bannerStandardAd.l());
        aVar.d(bannerStandardAd.n());
        aVar.f(this.i);
        aVar.e(BannerMetaData.a().b().g());
        aVar.a(bannerStandardAd.d_());
        aVar.a(bannerStandardAd.d());
        aVar.a(this.a);
        return aVar;
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        a(bool.booleanValue());
        StringBuilder sb = new StringBuilder("Html onPostExecute, result=[");
        sb.append(bool);
        sb.append(RequestParameters.RIGHT_BRACKETS);
    }
}
