package com.startapp.sdk.ads.banner.bannerstandard;

import com.iab.omid.library.startapp.b.e;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public final class b {
    private final com.iab.omid.library.startapp.adsession.b a;

    private b(com.iab.omid.library.startapp.adsession.b bVar) {
        this.a = bVar;
    }

    public static b a(com.iab.omid.library.startapp.adsession.b bVar) {
        com.iab.omid.library.startapp.adsession.b bVar2 = bVar;
        com.iab.omid.library.startapp.b.a((Object) bVar, "AdSession is null");
        if (!bVar2.l()) {
            throw new IllegalStateException("Cannot create VideoEvents for JavaScript AdSession");
        } else if (!bVar2.i()) {
            com.iab.omid.library.startapp.b.a(bVar2);
            if (bVar2.e().e() == null) {
                b bVar3 = new b(bVar2);
                bVar2.e().a(bVar3);
                return bVar3;
            }
            throw new IllegalStateException("VideoEvents already exists for AdSession");
        } else {
            throw new IllegalStateException("AdSession is started");
        }
    }

    public final void a(float f, float f2) {
        if (f > 0.0f) {
            b(f2);
            com.iab.omid.library.startapp.b.b(this.a);
            JSONObject jSONObject = new JSONObject();
            com.iab.omid.library.startapp.d.b.a(jSONObject, "duration", Float.valueOf(f));
            com.iab.omid.library.startapp.d.b.a(jSONObject, "videoPlayerVolume", Float.valueOf(f2));
            com.iab.omid.library.startapp.d.b.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(e.a().d()));
            this.a.e().a("start", jSONObject);
            return;
        }
        throw new IllegalArgumentException("Invalid Video duration");
    }

    public final void a() {
        com.iab.omid.library.startapp.b.b(this.a);
        this.a.e().a("firstQuartile");
    }

    public final void b() {
        com.iab.omid.library.startapp.b.b(this.a);
        this.a.e().a("midpoint");
    }

    public final void c() {
        com.iab.omid.library.startapp.b.b(this.a);
        this.a.e().a("thirdQuartile");
    }

    public final void d() {
        com.iab.omid.library.startapp.b.b(this.a);
        this.a.e().a("complete");
    }

    public final void e() {
        com.iab.omid.library.startapp.b.b(this.a);
        this.a.e().a("pause");
    }

    public final void f() {
        com.iab.omid.library.startapp.b.b(this.a);
        this.a.e().a(String.VIDEO_BUFFER_START);
    }

    public final void g() {
        com.iab.omid.library.startapp.b.b(this.a);
        this.a.e().a("bufferFinish");
    }

    public final void h() {
        com.iab.omid.library.startapp.b.b(this.a);
        this.a.e().a(String.VIDEO_SKIPPED);
    }

    public final void a(float f) {
        b(f);
        com.iab.omid.library.startapp.b.b(this.a);
        JSONObject jSONObject = new JSONObject();
        com.iab.omid.library.startapp.d.b.a(jSONObject, "videoPlayerVolume", Float.valueOf(f));
        com.iab.omid.library.startapp.d.b.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(e.a().d()));
        this.a.e().a("volumeChange", jSONObject);
    }

    private static void b(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Invalid Video volume");
        }
    }
}
