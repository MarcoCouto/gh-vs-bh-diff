package com.startapp.sdk.ads.banner.bannerstandard;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.iab.omid.library.startapp.adsession.b;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.startapp.sdk.ads.banner.BannerBase;
import com.startapp.sdk.ads.banner.BannerInterface;
import com.startapp.sdk.ads.banner.BannerListener;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.banner.BannerOptions;
import com.startapp.sdk.ads.banner.bannerstandard.CloseableLayout.ClosePosition;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.Ad.AdState;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject.Size;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.c;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.mraid.bridge.MraidState;
import com.startapp.sdk.adsbase.mraid.bridge.a;
import com.startapp.sdk.adsbase.mraid.bridge.a.C0078a;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.f.a.f;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class BannerStandard extends BannerBase implements BannerInterface, AdEventListener {
    private c A;
    private c B;
    /* access modifiers changed from: private */
    public MraidBannerController C;
    /* access modifiers changed from: private */
    public MraidBannerController D;
    private ViewGroup E;
    protected BannerStandardAd h;
    protected boolean i;
    protected boolean j;
    protected BannerListener k;
    private boolean l;
    private boolean m;
    private boolean n;
    private final Handler o;
    /* access modifiers changed from: private */
    public long p;
    private BannerOptions q;
    private AdPreferences r;
    /* access modifiers changed from: private */
    public final f s;
    private boolean t;
    public WebView twoPartWebView;
    private AdInformationObject u;
    private RelativeLayout v;
    private RelativeLayout w;
    public WebView webView;
    private CloseableLayout x;
    private h y;
    private b z;

    /* compiled from: StartAppSDK */
    private class MraidBannerController extends a {
        private WebView activeWebView;
        /* access modifiers changed from: private */
        public MraidState mraidState = MraidState.LOADING;
        private boolean mraidVisibility = false;
        /* access modifiers changed from: private */
        public com.startapp.sdk.adsbase.mraid.a.a nativeFeatureManager;
        private com.startapp.sdk.adsbase.mraid.b.a orientationProperties;
        private com.startapp.sdk.adsbase.mraid.b.b resizeProperties;

        /* compiled from: StartAppSDK */
        class BannerWebViewClient extends com.startapp.sdk.adsbase.mraid.bridge.c {
            BannerWebViewClient(com.startapp.sdk.adsbase.mraid.bridge.b bVar) {
                super(bVar);
            }

            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
                if (MraidBannerController.this.mraidState == MraidState.LOADING) {
                    com.iab.omid.library.startapp.b.a(String.INLINE, webView);
                    com.iab.omid.library.startapp.b.a(BannerStandard.this.getContext(), webView, MraidBannerController.this.nativeFeatureManager);
                    MraidBannerController.this.updateDisplayMetrics(webView);
                    MraidBannerController.this.mraidState = MraidState.DEFAULT;
                    com.iab.omid.library.startapp.b.a(MraidBannerController.this.mraidState, webView);
                    com.iab.omid.library.startapp.b.a(webView);
                }
                BannerStandard.this.a(webView);
            }
        }

        MraidBannerController(WebView webView, C0078a aVar) {
            super(aVar);
            this.activeWebView = webView;
            this.activeWebView.setWebViewClient(new BannerWebViewClient(this));
            this.nativeFeatureManager = new com.startapp.sdk.adsbase.mraid.a.a(BannerStandard.this.getContext());
            this.orientationProperties = new com.startapp.sdk.adsbase.mraid.b.a();
        }

        /* access modifiers changed from: 0000 */
        public com.startapp.sdk.adsbase.mraid.b.b getResizeProperties() {
            return this.resizeProperties;
        }

        public void setResizeProperties(Map<String, String> map) {
            boolean z;
            try {
                int parseInt = Integer.parseInt((String) map.get("width"));
                int parseInt2 = Integer.parseInt((String) map.get("height"));
                int parseInt3 = Integer.parseInt((String) map.get("offsetX"));
                int parseInt4 = Integer.parseInt((String) map.get("offsetY"));
                String str = (String) map.get("allowOffscreen");
                String str2 = (String) map.get("customClosePosition");
                if (str != null) {
                    if (!Boolean.parseBoolean(str)) {
                        z = false;
                        com.startapp.sdk.adsbase.mraid.b.b bVar = new com.startapp.sdk.adsbase.mraid.b.b(parseInt, parseInt2, parseInt3, parseInt4, str2, z);
                        this.resizeProperties = bVar;
                    }
                }
                z = true;
                com.startapp.sdk.adsbase.mraid.b.b bVar2 = new com.startapp.sdk.adsbase.mraid.b.b(parseInt, parseInt2, parseInt3, parseInt4, str2, z);
                this.resizeProperties = bVar2;
            } catch (Exception unused) {
                com.iab.omid.library.startapp.b.a(this.activeWebView, "wrong format", "setResizeProperties");
            }
        }

        /* access modifiers changed from: 0000 */
        public MraidState getState() {
            return this.mraidState;
        }

        /* access modifiers changed from: 0000 */
        public void setState(MraidState mraidState2) {
            this.mraidState = mraidState2;
            com.iab.omid.library.startapp.b.a(this.mraidState, this.activeWebView);
        }

        public void close() {
            BannerStandard.a(BannerStandard.this);
        }

        public void expand(String str) {
            BannerStandard.b(BannerStandard.this, str);
        }

        public void resize() {
            BannerStandard.f(BannerStandard.this);
        }

        public void useCustomClose(String str) {
            BannerStandard.a(BannerStandard.this, Boolean.parseBoolean(str));
        }

        public void setOrientationProperties(Map<String, String> map) {
            boolean parseBoolean = Boolean.parseBoolean((String) map.get("allowOrientationChange"));
            String str = (String) map.get("forceOrientation");
            if (this.orientationProperties.a != parseBoolean || this.orientationProperties.b != com.startapp.sdk.adsbase.mraid.b.a.a(str)) {
                this.orientationProperties.a = parseBoolean;
                this.orientationProperties.b = com.startapp.sdk.adsbase.mraid.b.a.a(str);
                applyOrientationProperties((Activity) BannerStandard.this.getContext(), this.orientationProperties);
            }
        }

        public void setExpandProperties(Map<String, String> map) {
            String str = (String) map.get("useCustomClose");
            if (str != null) {
                BannerStandard.a(BannerStandard.this, Boolean.parseBoolean(str));
            }
        }

        public boolean isFeatureSupported(String str) {
            return this.nativeFeatureManager.a(str);
        }

        /* access modifiers changed from: private */
        public void fireViewableChangeEvent(boolean z) {
            if (this.mraidVisibility != z) {
                this.mraidVisibility = z;
                com.iab.omid.library.startapp.b.a(this.activeWebView, this.mraidVisibility);
            }
        }

        /* access modifiers changed from: private */
        public void updateDisplayMetrics(WebView webView) {
            Context context = BannerStandard.this.getContext();
            try {
                DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                int i = displayMetrics.widthPixels;
                int i2 = displayMetrics.heightPixels;
                int[] iArr = new int[2];
                BannerStandard.this.getLocationOnScreen(iArr);
                int i3 = iArr[0];
                int i4 = iArr[1];
                com.iab.omid.library.startapp.b.a(context, i, i2, webView);
                com.iab.omid.library.startapp.b.b(context, i3, i4, BannerStandard.this.s.a(), BannerStandard.this.s.b(), webView);
                com.iab.omid.library.startapp.b.b(context, i, i2, webView);
                com.iab.omid.library.startapp.b.a(context, i3, i4, BannerStandard.this.s.a(), BannerStandard.this.s.b(), webView);
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int c() {
        return 50;
    }

    /* access modifiers changed from: protected */
    public String d() {
        return "StartApp Banner";
    }

    /* access modifiers changed from: protected */
    public int r() {
        return 0;
    }

    public BannerStandard(Activity activity) {
        this((Context) activity);
    }

    public BannerStandard(Activity activity, AdPreferences adPreferences) {
        this((Context) activity, adPreferences);
    }

    public BannerStandard(Activity activity, BannerListener bannerListener) {
        this((Context) activity, bannerListener);
    }

    public BannerStandard(Activity activity, AdPreferences adPreferences, BannerListener bannerListener) {
        this((Context) activity, adPreferences, bannerListener);
    }

    public BannerStandard(Activity activity, boolean z2) {
        this((Context) activity, z2);
    }

    public BannerStandard(Activity activity, boolean z2, AdPreferences adPreferences) {
        this((Context) activity, z2, adPreferences);
    }

    public BannerStandard(Activity activity, AttributeSet attributeSet) {
        this((Context) activity, attributeSet);
    }

    public BannerStandard(Activity activity, AttributeSet attributeSet, int i2) {
        this((Context) activity, attributeSet, i2);
    }

    @Deprecated
    public BannerStandard(Context context) {
        this(context, true, (AdPreferences) null);
    }

    @Deprecated
    public BannerStandard(Context context, AdPreferences adPreferences) {
        this(context, true, adPreferences);
    }

    @Deprecated
    public BannerStandard(Context context, BannerListener bannerListener) {
        this(context, true, (AdPreferences) null);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public BannerStandard(Context context, AdPreferences adPreferences, BannerListener bannerListener) {
        this(context, true, adPreferences);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public BannerStandard(Context context, boolean z2) {
        this(context, z2, (AdPreferences) null);
    }

    @Deprecated
    public BannerStandard(Context context, boolean z2, AdPreferences adPreferences) {
        super(context);
        this.l = false;
        this.i = true;
        this.j = false;
        this.m = true;
        this.n = true;
        this.o = new Handler(Looper.getMainLooper());
        this.s = new f(300, c());
        this.t = false;
        this.u = null;
        this.v = null;
        try {
            this.m = z2;
            this.r = adPreferences;
            a();
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    @Deprecated
    public BannerStandard(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Deprecated
    public BannerStandard(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.l = false;
        this.i = true;
        this.j = false;
        this.m = true;
        this.n = true;
        this.o = new Handler(Looper.getMainLooper());
        this.s = new f(300, c());
        this.t = false;
        this.u = null;
        this.v = null;
        try {
            a();
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public void hideBanner() {
        this.n = false;
        s();
    }

    private void s() {
        if (this.w != null) {
            this.w.setVisibility(4);
        }
    }

    public void showBanner() {
        this.n = true;
        t();
    }

    private void t() {
        if (this.w != null) {
            this.w.setVisibility(0);
        }
        if (this.h != null) {
            com.startapp.sdk.b.c.a(getContext()).h().a(Placement.INAPP_BANNER, r(), this.h.getAdId());
        }
    }

    private void b(WebView webView2) {
        webView2.setBackgroundColor(0);
        webView2.setHorizontalScrollBarEnabled(false);
        webView2.getSettings().setJavaScriptEnabled(true);
        webView2.setVerticalScrollBarEnabled(false);
        webView2.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                BannerStandard.this.i = true;
                if (motionEvent.getAction() == 2) {
                    return true;
                }
                return false;
            }
        });
        webView2.setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View view) {
                return true;
            }
        });
        webView2.setLongClickable(false);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            Context context = getContext();
            this.x = new CloseableLayout(context);
            this.x.setOnCloseListener(new CloseableLayout.a() {
                public void onClose() {
                    BannerStandard.a(BannerStandard.this);
                }
            });
            this.webView = new WebView(context);
            this.C = new MraidBannerController(this.webView, new C0078a() {
                public boolean onClickEvent(String str) {
                    if (!BannerStandard.this.i) {
                        e g = new e(InfoEventCategory.ERROR).e("fake_click").g(com.startapp.sdk.adsbase.a.a(str, (String) null));
                        StringBuilder sb = new StringBuilder("jsTag=");
                        sb.append(BannerStandard.this.j);
                        g.f(sb.toString()).a(BannerStandard.this.getContext());
                    }
                    if ((!BannerStandard.this.j || BannerStandard.this.i) && str != null) {
                        return BannerStandard.this.b(str);
                    }
                    return false;
                }
            });
            this.q = new BannerOptions();
            this.h = new BannerStandardAd(context, g());
            if (this.r == null) {
                this.r = new AdPreferences();
            }
            if (getId() == -1) {
                setId(this.f);
            }
            this.webView.setId(159868225);
            b(this.webView);
            this.q = BannerMetaData.a().c();
            a(this.r);
            setMinimumWidth(r.a(getContext(), this.s.a()));
            setMinimumHeight(r.a(getContext(), this.s.b()));
            this.webView.addJavascriptInterface(new com.startapp.sdk.d.b(getContext(), (Runnable) new Runnable() {
                public void run() {
                }
            }, new TrackingParams(j()), this.h.e(0)), "startappwall");
            this.w = new RelativeLayout(getContext());
            a((View) this.webView);
            s();
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(13);
            addView(this.w, layoutParams);
            if (this.l || this.b) {
                onReceiveAd(this.h);
            } else if (this.m) {
                k();
            }
        } catch (Throwable th) {
            new e(th).a(getContext());
            hideBanner();
            a("BannerStandard.init - webview failed");
        }
    }

    /* access modifiers changed from: protected */
    public final View n() {
        if (this.w != null) {
            return this.w;
        }
        return super.n();
    }

    private void a(View view) {
        LayoutParams layoutParams = new LayoutParams(r.a(getContext(), this.s.a()), r.a(getContext(), this.s.b()));
        layoutParams.addRule(13);
        this.w.addView(view, layoutParams);
    }

    /* access modifiers changed from: protected */
    public final void o() {
        if (this.y != null && this.y.c()) {
            super.o();
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (this.z != null) {
            this.z.b();
            this.z = null;
        }
        if (this.r == null) {
            this.r = new AdPreferences();
        }
        Point u2 = this.b ? this.c : u();
        BannerStandardAd bannerStandardAd = this.h;
        int i2 = u2.x;
        int i3 = u2.y;
        bannerStandardAd.b(i2);
        bannerStandardAd.c(i3);
        this.h.setState(AdState.UN_INITIALIZED);
        this.h.a(r());
        this.h.load(this.r, this);
    }

    private Point u() {
        Point point = new Point();
        if (getLayoutParams() != null && getLayoutParams().width > 0) {
            point.x = r.b(getContext(), getLayoutParams().width + 1);
        }
        if (getLayoutParams() != null && getLayoutParams().height > 0) {
            point.y = r.b(getContext(), getLayoutParams().height + 1);
        }
        if (getLayoutParams() != null && getLayoutParams().width > 0 && getLayoutParams().height > 0) {
            this.h.c_();
        }
        if (getLayoutParams() == null || getLayoutParams().width <= 0 || getLayoutParams().height <= 0) {
            DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
            try {
                View view = (View) getParent();
                while (view != null && (view.getMeasuredWidth() <= 0 || view.getMeasuredHeight() <= 0)) {
                    if (view.getMeasuredWidth() > 0) {
                        a(point, r.b(getContext(), (view.getMeasuredWidth() - view.getPaddingLeft()) - view.getPaddingRight()));
                    }
                    if (view.getMeasuredHeight() > 0) {
                        b(point, r.b(getContext(), (view.getMeasuredHeight() - view.getPaddingBottom()) - view.getPaddingTop()));
                    }
                    view = (View) view.getParent();
                }
                if (view == null) {
                    a(point, displayMetrics);
                } else {
                    a(point, r.b(getContext(), (view.getMeasuredWidth() - view.getPaddingLeft()) - view.getPaddingRight()));
                    b(point, r.b(getContext(), (view.getMeasuredHeight() - view.getPaddingBottom()) - view.getPaddingTop()));
                }
            } catch (Throwable th) {
                new e(th).a(getContext());
                a(point, displayMetrics);
            }
        }
        return point;
    }

    private void a(Point point, DisplayMetrics displayMetrics) {
        a(point, r.b(getContext(), displayMetrics.widthPixels));
        b(point, r.b(getContext(), displayMetrics.heightPixels));
    }

    private static void a(Point point, int i2) {
        if (point.x <= 0) {
            point.x = i2;
        }
    }

    private static void b(Point point, int i2) {
        if (point.y <= 0) {
            point.y = i2;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(14:23|(1:27)|28|29|(1:31)|32|33|(1:35)(1:36)|37|(2:39|(1:41)(1:42))|43|(1:45)|46|(2:50|51)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x0118 */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0145 A[Catch:{ NumberFormatException -> 0x01ce, Throwable -> 0x01b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0156 A[Catch:{ NumberFormatException -> 0x01ce, Throwable -> 0x01b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0181 A[Catch:{ NumberFormatException -> 0x01ce, Throwable -> 0x01b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01a0 A[Catch:{ NumberFormatException -> 0x01ce, Throwable -> 0x01b9 }] */
    public void onReceiveAd(Ad ad) {
        long j2;
        boolean z2 = false;
        this.b = false;
        this.i = false;
        removeView(this.v);
        if (this.h == null || this.h.j() == null || this.h.j().compareTo("") == 0) {
            a("No Banner received");
            return;
        }
        this.j = "true".equals(s.a(this.h.j(), "@jsTag@", "@jsTag@"));
        loadHtml();
        String a = s.a(this.h.j(), "@width@", "@width@");
        String a2 = s.a(this.h.j(), "@height@", "@height@");
        try {
            int parseInt = Integer.parseInt(a);
            int parseInt2 = Integer.parseInt(a2);
            Point u2 = u();
            if (u2.x < parseInt || u2.y < parseInt2) {
                Point point = new Point(0, 0);
                ViewGroup.LayoutParams layoutParams = this.webView.getLayoutParams();
                if (layoutParams == null) {
                    layoutParams = new LayoutParams(point.x, point.y);
                } else {
                    layoutParams.width = point.x;
                    layoutParams.height = point.y;
                }
                this.webView.setLayoutParams(layoutParams);
            } else {
                this.s.a(parseInt, parseInt2);
                int a3 = r.a(getContext(), this.s.a());
                int a4 = r.a(getContext(), this.s.b());
                this.w.setMinimumWidth(a3);
                this.w.setMinimumHeight(a4);
                ViewGroup.LayoutParams layoutParams2 = this.webView.getLayoutParams();
                if (layoutParams2 == null) {
                    layoutParams2 = new LayoutParams(a3, a4);
                } else {
                    layoutParams2.width = a3;
                    layoutParams2.height = a4;
                }
                this.webView.setLayoutParams(layoutParams2);
                z2 = true;
            }
            if (z2) {
                this.l = true;
                if (this.u == null && this.v == null) {
                    this.v = new RelativeLayout(getContext());
                    this.u = new AdInformationObject(getContext(), Size.SMALL, Placement.INAPP_BANNER, this.h.getAdInfoOverride());
                    this.u.a(this.v);
                }
                ViewGroup viewGroup = (ViewGroup) this.v.getParent();
                if (viewGroup != null) {
                    viewGroup.removeView(this.v);
                }
                LayoutParams layoutParams3 = new LayoutParams(-2, -2);
                layoutParams3.addRule(13);
                this.webView.addView(this.v, layoutParams3);
                Context context = getContext();
                String[] strArr = this.h.trackingUrls;
                TrackingParams trackingParams = new TrackingParams(j());
                if (this.h.t() == null) {
                    j2 = TimeUnit.SECONDS.toMillis(this.h.t().longValue());
                } else {
                    j2 = TimeUnit.SECONDS.toMillis(MetaData.D().E());
                }
                h hVar = new h(context, strArr, trackingParams, j2);
                this.y = hVar;
                this.y.a((h.a) new h.a() {
                    public void onSent() {
                        if (BannerStandard.this.k != null) {
                            BannerStandard.this.k.onImpression(BannerStandard.this);
                        }
                        BannerStandard.this.p = System.currentTimeMillis() - BannerStandard.this.p;
                        BannerStandard.this.o();
                    }
                });
                a(this.y);
                q();
                if (this.h != null) {
                    if (this.h.v()) {
                        this.A = new c(this.webView, m(), new c.a() {
                            public boolean onUpdate(boolean z) {
                                BannerStandard.this.C.fireViewableChangeEvent(z);
                                return BannerStandard.this.h.v();
                            }
                        });
                    }
                }
                if (this.n) {
                    t();
                }
                if (this.k != null && !this.t) {
                    this.t = true;
                    this.k.onReceiveAd(this);
                }
            } else {
                a("Banner cannot be displayed (not enough room)");
            }
        } catch (NumberFormatException unused) {
            a("Error Casting width & height from HTML");
        } catch (Throwable th) {
            new e(th).a(getContext());
            a(th.getMessage());
        }
    }

    private void a(String str) {
        setErrorMessage(str);
        this.b = false;
        if (this.k != null && !this.t) {
            this.t = true;
            this.k.onFailedToReceiveAd(this);
        }
    }

    public void loadHtml() {
        if (this.h != null) {
            String j2 = this.h.j();
            if (j2 != null) {
                if (j() != null && j().length() > 0) {
                    j2 = j2.replaceAll("startapp_adtag_placeholder", j());
                }
                this.o.postDelayed(new Runnable() {
                    public void run() {
                        BannerStandard.this.l();
                    }
                }, (long) this.q.i());
                this.p = System.currentTimeMillis();
                s.a(getContext(), this.webView, j2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        int a = r.a(getContext(), this.s.a());
        int a2 = r.a(getContext(), this.s.b());
        if (i2 < a || i3 < a2) {
            s();
            return;
        }
        if (this.n && this.l) {
            t();
        }
    }

    public void onFailedToReceiveAd(Ad ad) {
        a(ad.getErrorMessage());
    }

    /* access modifiers changed from: protected */
    public final void a(WebView webView2) {
        y();
        if (MetaData.D().P()) {
            this.z = com.startapp.sdk.omsdk.a.a(webView2);
            if (this.z != null) {
                if (this.v != null) {
                    this.z.b(this.v);
                }
                if (this.x != null) {
                    this.z.b(this.x);
                }
                this.z.a(webView2);
                this.z.a();
                com.iab.omid.library.startapp.adsession.a.a(this.z).a();
            }
        }
    }

    private void v() {
        if (this.webView != null) {
            com.startapp.common.b.b.c(this.webView);
        }
        if (this.twoPartWebView != null) {
            com.startapp.common.b.b.c(this.twoPartWebView);
        }
    }

    private void w() {
        if (this.webView != null) {
            com.startapp.common.b.b.b(this.webView);
        }
        if (this.twoPartWebView != null) {
            com.startapp.common.b.b.b(this.twoPartWebView);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        v();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        w();
        a(false);
        x();
        y();
        if (this.z != null) {
            this.z.b();
            this.z = null;
            s.a((Object) this.webView);
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            v();
        } else {
            w();
        }
    }

    public void setBannerListener(BannerListener bannerListener) {
        this.k = bannerListener;
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return this.q.i();
    }

    /* access modifiers changed from: protected */
    public final int g() {
        if (this.h == null) {
            return 0;
        }
        return this.h.a();
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        this.f = i2;
    }

    public void setAdTag(String str) {
        this.g = str;
    }

    private void a(boolean z2) {
        if (this.y != null) {
            this.y.a(z2);
        }
    }

    private void x() {
        if (this.A != null) {
            this.A.a();
        }
        if (this.B != null) {
            this.B.a();
        }
    }

    private void y() {
        this.o.removeCallbacksAndMessages(null);
    }

    /* access modifiers changed from: private */
    public boolean b(String str) {
        if (this.k != null) {
            this.k.onClick(this);
        }
        a(true);
        x();
        y();
        boolean a = com.startapp.sdk.adsbase.a.a(getContext(), Placement.INAPP_BANNER);
        String[] q2 = this.h.q();
        String[] s2 = this.h.s();
        if (!this.j) {
            if (str.contains("index=")) {
                try {
                    int a2 = com.startapp.sdk.adsbase.a.a(str);
                    if (a2 < 0) {
                        e e = new e(InfoEventCategory.ERROR).e("Wrong index extracted from URL");
                        StringBuilder sb = new StringBuilder("adId: ");
                        sb.append(this.h.getAdId());
                        e.f(sb.toString()).a(getContext());
                        return false;
                    }
                    String str2 = null;
                    if (!this.h.d(a2) || a) {
                        Context context = getContext();
                        if (a2 < q2.length) {
                            str2 = q2[a2];
                        }
                        com.startapp.sdk.adsbase.a.a(context, str, str2, new TrackingParams(j()), this.h.e(a2) && !a, false);
                        this.webView.stopLoading();
                        setClicked(true);
                        return true;
                    }
                    com.startapp.sdk.adsbase.a.a(getContext(), str, a2 < q2.length ? q2[a2] : null, a2 < s2.length ? s2[a2] : null, new TrackingParams(j()), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), this.h.e(a2), this.h.f(a2));
                    this.webView.stopLoading();
                    setClicked(true);
                    return true;
                } catch (Throwable th) {
                    new e(th).a(getContext());
                    return false;
                }
            }
        } else {
            String str3 = str;
        }
        if (q2.length <= 0) {
            e e2 = new e(InfoEventCategory.ERROR).e("No tracking URLs");
            StringBuilder sb2 = new StringBuilder("adId: ");
            sb2.append(this.h.getAdId());
            e2.f(sb2.toString()).a(getContext());
            return false;
        } else if (!this.h.d(0) || a) {
            com.startapp.sdk.adsbase.a.a(getContext(), str, q2[0], new TrackingParams(j()), this.h.e(0) && !a, false);
            this.webView.stopLoading();
            setClicked(true);
            return true;
        } else if (s2.length <= 0) {
            e e3 = new e(InfoEventCategory.ERROR).e("No package names");
            StringBuilder sb3 = new StringBuilder("adId: ");
            sb3.append(this.h.getAdId());
            e3.f(sb3.toString()).a(getContext());
            return false;
        } else {
            com.startapp.sdk.adsbase.a.a(getContext(), str, q2[0], s2[0], new TrackingParams(j()), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), this.h.e(0), this.h.f(0));
            this.webView.stopLoading();
            setClicked(true);
            return true;
        }
    }

    private static int a(int i2, int i3, int i4) {
        return Math.max(i2, Math.min(i3, i4));
    }

    private ViewGroup z() {
        View view;
        if (this.E != null) {
            return this.E;
        }
        Context context = getContext();
        RelativeLayout relativeLayout = this.w;
        View view2 = null;
        if (!(context instanceof Activity)) {
            view = null;
        } else {
            view = ((Activity) context).getWindow().getDecorView().findViewById(16908290);
        }
        if (relativeLayout != null) {
            View rootView = relativeLayout.getRootView();
            if (rootView != null) {
                view2 = rootView.findViewById(16908290);
                if (view2 == null) {
                    view2 = rootView;
                }
            }
        }
        if (view == null) {
            view = view2;
        }
        return view instanceof ViewGroup ? (ViewGroup) view : this.w;
    }

    private ViewGroup A() {
        if (this.E == null) {
            this.E = z();
        }
        return this.E;
    }

    /* access modifiers changed from: protected */
    public final int i() {
        return Math.max(this.q.i() - ((int) this.p), 0);
    }

    static /* synthetic */ void a(BannerStandard bannerStandard) {
        if (bannerStandard.C.getState() != MraidState.LOADING && bannerStandard.C.getState() != MraidState.HIDDEN) {
            if (bannerStandard.C.getState() == MraidState.RESIZED || bannerStandard.C.getState() == MraidState.EXPANDED) {
                if (bannerStandard.D != null) {
                    bannerStandard.x.removeView(bannerStandard.twoPartWebView);
                    bannerStandard.B.a();
                    bannerStandard.B = null;
                    bannerStandard.D = null;
                    bannerStandard.twoPartWebView.stopLoading();
                    bannerStandard.twoPartWebView = null;
                } else {
                    bannerStandard.x.removeView(bannerStandard.webView);
                    bannerStandard.a((View) bannerStandard.webView);
                    bannerStandard.t();
                }
                CloseableLayout closeableLayout = bannerStandard.x;
                if (!(closeableLayout == null || closeableLayout.getParent() == null || !(closeableLayout.getParent() instanceof ViewGroup))) {
                    ((ViewGroup) closeableLayout.getParent()).removeView(closeableLayout);
                }
                bannerStandard.C.setState(MraidState.DEFAULT);
            } else if (bannerStandard.C.getState() == MraidState.DEFAULT) {
                bannerStandard.s();
                bannerStandard.C.setState(MraidState.HIDDEN);
            }
            bannerStandard.o();
        }
    }

    static /* synthetic */ void b(BannerStandard bannerStandard, String str) {
        bannerStandard.p();
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        boolean z2 = str != null && !TextUtils.isEmpty(str);
        if (z2) {
            bannerStandard.i = false;
            if (bannerStandard.twoPartWebView == null) {
                bannerStandard.twoPartWebView = new WebView(bannerStandard.getContext());
            }
            bannerStandard.D = new MraidBannerController(bannerStandard.twoPartWebView, new C0078a() {
                public boolean onClickEvent(String str) {
                    if (!BannerStandard.this.i) {
                        e g = new e(InfoEventCategory.ERROR).e("fake_click").g(com.startapp.sdk.adsbase.a.a(str, (String) null));
                        StringBuilder sb = new StringBuilder("jsTag=");
                        sb.append(BannerStandard.this.j);
                        g.f(sb.toString()).a(BannerStandard.this.getContext());
                    }
                    if ((!BannerStandard.this.j || BannerStandard.this.i) && str != null) {
                        return BannerStandard.this.b(str);
                    }
                    return false;
                }
            });
            bannerStandard.B = new c(bannerStandard.twoPartWebView, m(), new c.a() {
                public boolean onUpdate(boolean z) {
                    BannerStandard.this.C.fireViewableChangeEvent(z);
                    BannerStandard.this.D.fireViewableChangeEvent(z);
                    return BannerStandard.this.h.v();
                }
            });
            bannerStandard.twoPartWebView.setId(159868226);
            bannerStandard.b(bannerStandard.twoPartWebView);
            bannerStandard.twoPartWebView.loadUrl(str);
        }
        if (bannerStandard.C.getState() == MraidState.DEFAULT) {
            if (z2) {
                bannerStandard.x.addView(bannerStandard.twoPartWebView, layoutParams);
            } else {
                if (bannerStandard.w != null) {
                    bannerStandard.w.removeView(bannerStandard.webView);
                    bannerStandard.w.setVisibility(4);
                }
                bannerStandard.x.addView(bannerStandard.webView, layoutParams);
            }
            bannerStandard.A().addView(bannerStandard.x, new FrameLayout.LayoutParams(-1, -1));
        } else if (bannerStandard.C.getState() == MraidState.RESIZED && z2) {
            bannerStandard.x.removeView(bannerStandard.webView);
            if (bannerStandard.w != null) {
                bannerStandard.w.addView(bannerStandard.webView, layoutParams);
                bannerStandard.w.setVisibility(4);
            }
            bannerStandard.x.addView(bannerStandard.twoPartWebView, layoutParams);
        }
        bannerStandard.x.setLayoutParams(layoutParams);
        bannerStandard.C.setState(MraidState.EXPANDED);
    }

    static /* synthetic */ void f(BannerStandard bannerStandard) {
        com.startapp.sdk.adsbase.mraid.b.b resizeProperties = bannerStandard.C.getResizeProperties();
        if (resizeProperties == null) {
            com.iab.omid.library.startapp.b.a(bannerStandard.webView, "requires: setResizeProperties first", MraidJsMethods.RESIZE);
            return;
        }
        bannerStandard.p();
        if (bannerStandard.C.getState() != MraidState.LOADING && bannerStandard.C.getState() != MraidState.HIDDEN) {
            if (bannerStandard.C.getState() == MraidState.EXPANDED) {
                com.iab.omid.library.startapp.b.a(bannerStandard.webView, "Not allowed to resize from an already expanded ad", MraidJsMethods.RESIZE);
                return;
            }
            int i2 = resizeProperties.a;
            int i3 = resizeProperties.b;
            int i4 = resizeProperties.c;
            int i5 = resizeProperties.d;
            int[] iArr = new int[2];
            bannerStandard.webView.getLocationOnScreen(iArr);
            Context context = bannerStandard.getContext();
            int b = r.b(context, iArr[0]) + i4;
            int b2 = r.b(context, iArr[1]) + i5;
            Rect rect = new Rect(b, b2, i2 + b, i3 + b2);
            ViewGroup z2 = bannerStandard.z();
            int b3 = r.b(context, z2.getWidth());
            int b4 = r.b(context, z2.getHeight());
            int[] iArr2 = new int[2];
            z2.getLocationOnScreen(iArr2);
            int b5 = r.b(context, iArr2[0]);
            int b6 = r.b(context, iArr2[1]);
            if (!resizeProperties.f) {
                if (rect.width() > b3 || rect.height() > b4) {
                    com.iab.omid.library.startapp.b.a(bannerStandard.webView, "Not enough room for the ad", MraidJsMethods.RESIZE);
                    return;
                }
                rect.offsetTo(a(b5, rect.left, (b5 + b3) - rect.width()), a(b6, rect.top, (b6 + b4) - rect.height()));
            }
            Rect rect2 = new Rect();
            try {
                ClosePosition a = ClosePosition.a(resizeProperties.e, ClosePosition.TOP_RIGHT);
                bannerStandard.x.a(a, rect, rect2);
                if (!new Rect(b5, b6, b3 + b5, b4 + b6).contains(rect2)) {
                    com.iab.omid.library.startapp.b.a(bannerStandard.webView, "The close region to appear within the max allowed size", MraidJsMethods.RESIZE);
                } else if (!rect.contains(rect2)) {
                    com.iab.omid.library.startapp.b.a(bannerStandard.webView, "The close region to appear within the max allowed size", MraidJsMethods.RESIZE);
                } else {
                    bannerStandard.x.setCloseVisible(false);
                    bannerStandard.x.setClosePosition(a);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(rect.width(), rect.height());
                    layoutParams.leftMargin = rect.left - b5;
                    layoutParams.topMargin = rect.top - b6;
                    if (bannerStandard.C.getState() == MraidState.DEFAULT) {
                        if (bannerStandard.w != null) {
                            bannerStandard.w.removeView(bannerStandard.webView);
                            bannerStandard.w.setVisibility(4);
                        }
                        bannerStandard.x.addView(bannerStandard.webView, new FrameLayout.LayoutParams(-1, -1));
                        bannerStandard.A().addView(bannerStandard.x, layoutParams);
                    } else if (bannerStandard.C.getState() == MraidState.RESIZED) {
                        bannerStandard.x.setLayoutParams(layoutParams);
                    }
                    bannerStandard.x.setClosePosition(a);
                    bannerStandard.C.setState(MraidState.RESIZED);
                }
            } catch (Exception e) {
                com.iab.omid.library.startapp.b.a(bannerStandard.webView, e.getMessage(), MraidJsMethods.RESIZE);
            }
        }
    }

    static /* synthetic */ void a(BannerStandard bannerStandard, boolean z2) {
        if (z2 != (!bannerStandard.x.a())) {
            bannerStandard.x.setCloseVisible(!z2);
        }
    }
}
