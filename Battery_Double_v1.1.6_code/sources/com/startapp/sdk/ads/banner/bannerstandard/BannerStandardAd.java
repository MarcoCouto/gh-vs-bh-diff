package com.startapp.sdk.ads.banner.bannerstandard;

import android.content.Context;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public class BannerStandardAd extends HtmlAd {
    private static final long serialVersionUID = 1;
    private int bannerType;
    private boolean fixedSize;
    private int offset = 0;

    public BannerStandardAd(Context context, int i) {
        super(context, Placement.INAPP_BANNER);
        this.offset = i;
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        a aVar = new a(this.a, this, this.offset, adPreferences, bVar);
        aVar.c();
        this.offset++;
    }

    public final int a() {
        return this.offset;
    }

    public final void c_() {
        this.fixedSize = true;
    }

    public final boolean d_() {
        return this.fixedSize;
    }

    public final int d() {
        return this.bannerType;
    }

    public final void a(int i) {
        this.bannerType = i;
    }
}
