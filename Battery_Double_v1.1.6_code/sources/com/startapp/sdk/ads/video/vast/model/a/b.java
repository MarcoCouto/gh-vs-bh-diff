package com.startapp.sdk.ads.video.vast.model.a;

import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;

/* compiled from: StartAppSDK */
public final class b {
    private String a;
    private String b;
    private String c;
    private String d;
    private Integer e;
    private Integer f;
    private Integer g;
    private Boolean h;
    private Boolean i;
    private String j;

    private static boolean a(int i2) {
        return i2 > 0 && i2 < 5000;
    }

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final String b() {
        return this.d;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final Integer c() {
        return this.e;
    }

    public final void a(Integer num) {
        this.e = num;
    }

    public final Integer d() {
        return this.f;
    }

    public final void b(Integer num) {
        this.f = num;
    }

    public final Integer e() {
        return this.g;
    }

    public final void c(Integer num) {
        this.g = num;
    }

    public final void a(Boolean bool) {
        this.h = bool;
    }

    public final void b(Boolean bool) {
        this.i = bool;
    }

    public final void e(String str) {
        this.j = str;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("MediaFile [url=");
        sb.append(this.a);
        sb.append(", id=");
        sb.append(this.b);
        sb.append(", delivery=");
        sb.append(this.c);
        sb.append(", type=");
        sb.append(this.d);
        sb.append(", bitrate=");
        sb.append(this.e);
        sb.append(", width=");
        sb.append(this.f);
        sb.append(", height=");
        sb.append(this.g);
        sb.append(", scalable=");
        sb.append(this.h);
        sb.append(", maintainAspectRatio=");
        sb.append(this.i);
        sb.append(", apiFramework=");
        sb.append(this.j);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public final boolean f() {
        if (TextUtils.isEmpty(this.d)) {
            return false;
        }
        Integer num = this.g;
        Integer num2 = this.f;
        if (num == null || num2 == null || !a(num.intValue()) || !a(num2.intValue()) || TextUtils.isEmpty(this.a)) {
            return false;
        }
        return true;
    }
}
