package com.startapp.sdk.ads.video.vast.a;

import android.content.Context;
import com.explorestack.iab.vast.tags.VastTagName;
import com.startapp.common.b.e;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.ads.video.vast.model.c;
import com.startapp.sdk.adsbase.Ad.AnonymousClass2;
import com.startapp.sdk.adsbase.j;
import com.tapjoy.TJAdUnitConstants;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/* compiled from: StartAppSDK */
public final class a {
    private final int a;
    private final int b;
    private c c;
    private StringBuilder d = new StringBuilder(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
    private long e = -1;

    public a(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public final c a() {
        return this.c;
    }

    public final VASTErrorCodes a(Context context, String str, com.startapp.sdk.ads.video.vast.model.a aVar) {
        Document document = null;
        this.c = null;
        this.e = System.currentTimeMillis();
        VASTErrorCodes a2 = a(context, str, 0);
        if (a2 == VASTErrorCodes.XMLParsingError) {
            new StringBuilder("processXml error ").append(a2);
            return VASTErrorCodes.XMLParsingError;
        }
        String sb = this.d.toString();
        if (sb != null && sb.length() > 0) {
            StringBuilder sb2 = new StringBuilder("<VASTS>");
            sb2.append(sb);
            sb2.append("</VASTS>");
            document = AnonymousClass2.a(sb2.toString());
        }
        if (document == null) {
            new StringBuilder("wrapMergedVastDocWithVasts error ").append(a2);
            return VASTErrorCodes.XMLParsingError;
        }
        this.c = new c(document);
        if (!this.c.a(aVar)) {
            new StringBuilder("validate error ").append(a2);
            if (a2 == VASTErrorCodes.ErrorNone) {
                a2 = VASTErrorCodes.MediaNotSupported;
            }
        }
        return a2;
    }

    private VASTErrorCodes a(Context context, String str, int i) {
        String str2;
        if (i >= this.a) {
            new StringBuilder("VAST wrapping exceeded max limit of ").append(this.a);
            return VASTErrorCodes.WrapperLimitReached;
        } else if (System.currentTimeMillis() - this.e > ((long) this.b) && this.e > 0) {
            return VASTErrorCodes.WrapperTimeout;
        } else {
            Document a2 = AnonymousClass2.a(str);
            if (a2 != null) {
                a2.getDocumentElement().normalize();
            }
            if (a2 == null) {
                return VASTErrorCodes.XMLParsingError;
            }
            if (a2 != null) {
                NodeList elementsByTagName = a2.getElementsByTagName("VAST");
                if (elementsByTagName != null && elementsByTagName.getLength() > 0) {
                    str2 = AnonymousClass2.a(elementsByTagName.item(0));
                    if (a2.getChildNodes().getLength() != 0 || a2.getChildNodes().item(0).getChildNodes().getLength() == 0 || str2 == null) {
                        return VASTErrorCodes.WrapperNoReponse;
                    }
                    this.d.append(str2);
                    NodeList elementsByTagName2 = a2.getElementsByTagName(VastTagName.VAST_AD_TAG_URI);
                    if (elementsByTagName2 == null || elementsByTagName2.getLength() == 0) {
                        return VASTErrorCodes.ErrorNone;
                    }
                    try {
                        com.startapp.common.b.e.a a3 = e.a(AnonymousClass2.b(elementsByTagName2.item(0)).replace(" ", "%20"), null, j.a(context, "User-Agent", "-1"), false);
                        if (a3.a() != null) {
                            return a(context, a3.a(), i + 1);
                        }
                        return VASTErrorCodes.WrapperNoReponse;
                    } catch (Exception unused) {
                        return VASTErrorCodes.GeneralWrapperError;
                    }
                }
            }
            str2 = null;
            if (a2.getChildNodes().getLength() != 0) {
            }
            return VASTErrorCodes.WrapperNoReponse;
        }
    }
}
