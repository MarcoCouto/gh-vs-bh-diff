package com.startapp.sdk.ads.video.vast.model.a;

import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public final class e {
    private String a;
    private List<String> b;
    private List<String> c;

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final List<String> b() {
        if (this.b == null) {
            this.b = new ArrayList();
        }
        return this.b;
    }

    public final List<String> c() {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        return this.c;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("VASTVideoClicks [clickThrough=");
        sb.append(this.a);
        sb.append(", clickTracking=[");
        sb.append(this.b);
        sb.append("], customClick=[");
        sb.append(this.c);
        sb.append("] ]");
        return sb.toString();
    }
}
