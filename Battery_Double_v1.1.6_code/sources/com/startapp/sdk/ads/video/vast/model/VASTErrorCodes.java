package com.startapp.sdk.ads.video.vast.model;

import com.ironsource.mediationsdk.logger.IronSourceError;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.tapjoy.TJAdUnitConstants;

/* compiled from: StartAppSDK */
public enum VASTErrorCodes {
    ErrorNone(0),
    XMLParsingError(100),
    SchemaValidationError(101),
    VersionOfResponseNotSupported(102),
    TraffickingError(200),
    VideoPlayerExpectingDifferentLinearity(ErrorCode.DIFFERENT_LINEARITY_EXPECTED_ERROR),
    VideoPlayerExpectingDifferentDuration(202),
    VideoPlayerExpectingDifferentSize(ErrorCode.DIFFERENT_SIZE_EXPECTED_ERROR),
    AdCategoryRequired(204),
    GeneralWrapperError(300),
    WrapperTimeout(301),
    WrapperLimitReached(302),
    WrapperNoReponse(303),
    InlineResponseTimeout(ErrorCode.INLINE_AD_DISPLAY_TIMEOUT_ERROR),
    GeneralLinearError(ErrorCode.GENERAL_LINEAR_ERROR),
    FileNotFound(401),
    TimeoutMediaFileURI(ErrorCode.MEDIAFILE_REQUEST_TIMEOUT_ERROR),
    MediaNotSupported(403),
    MediaFileDisplayError(405),
    MezzanineNotPovided(406),
    MezzanineDownloadInProgrees(407),
    ConditionalAdRejected(ErrorCode.CONDITIONAL_AD_REJECTED_ERROR),
    InteractiveCreativeFileNotExecuted(ErrorCode.INTERACTIVE_UNIT_NOT_EXECUTED_ERROR),
    VerificationNotExecuted(ErrorCode.VERIFICATION_UNIT_NOT_EXECUTED_ERROR),
    MezzanineNotAsExpected(411),
    GeneralNonLinearAdsError(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL),
    CreativeTooLarge(IronSourceError.ERROR_CODE_NO_CONFIGURATION_AVAILABLE),
    ResourceDownloadFailed(IronSourceError.ERROR_CODE_USING_CACHED_CONFIGURATION),
    NonLinearResourceNotSupported(503),
    GeneralCompanionAdsError(600),
    CompanionTooLarge(601),
    CompanionNotDisplay(602),
    CompanionFetchFailed(603),
    CompanionNotSupported(604),
    UndefinedError(900),
    GeneralVPAIDerror(ErrorCode.GENERAL_VPAID_ERROR),
    SAShowBeforeVast(10000),
    SAProcessSuccess(20000);
    
    private int value;

    private VASTErrorCodes(int i) {
        this.value = i;
    }

    public final int a() {
        return this.value;
    }
}
