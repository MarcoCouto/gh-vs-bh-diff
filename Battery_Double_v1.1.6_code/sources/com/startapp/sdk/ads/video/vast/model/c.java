package com.startapp.sdk.ads.video.vast.model;

import android.text.TextUtils;
import com.smaato.sdk.video.vast.model.Icon;
import com.smaato.sdk.video.vast.model.Verification;
import com.startapp.sdk.ads.video.vast.model.a.a;
import com.startapp.sdk.ads.video.vast.model.a.b;
import com.startapp.sdk.ads.video.vast.model.a.d;
import com.startapp.sdk.ads.video.vast.model.a.e;
import com.startapp.sdk.adsbase.Ad.AnonymousClass2;
import com.startapp.sdk.omsdk.AdVerification;
import com.startapp.sdk.omsdk.VerificationDetails;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* compiled from: StartAppSDK */
public final class c {
    private HashMap<VASTEventType, List<com.startapp.sdk.ads.video.vast.model.a.c>> a;
    private List<b> b;
    private int c;
    private e d;
    private List<String> e;
    private List<String> f;
    private int g;
    private b h = null;
    private List<a> i;
    private AdVerification j;

    public c(Document document) {
        this.c = c(document);
        this.a = a(document);
        this.b = b(document);
        this.d = d(document);
        this.e = a(document, "//Impression");
        this.f = a(document, "//Error");
        this.g = e(document);
        this.i = f(document);
        this.j = g(document);
    }

    public final HashMap<VASTEventType, List<com.startapp.sdk.ads.video.vast.model.a.c>> a() {
        return this.a;
    }

    public final e b() {
        return this.d;
    }

    public final List<String> c() {
        return this.e;
    }

    public final List<String> d() {
        return this.f;
    }

    public final int e() {
        return this.g;
    }

    public final b f() {
        return this.h;
    }

    public final AdVerification g() {
        return this.j;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(13:7|8|9|10|11|(2:13|(4:15|16|17|(1:19)(2:20|21)))|22|23|(1:25)(1:26)|27|37|30|5) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x006a */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0070 A[Catch:{ Exception -> 0x009b }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0077 A[Catch:{ Exception -> 0x009b }] */
    private HashMap<VASTEventType, List<com.startapp.sdk.ads.video.vast.model.a.c>> a(Document document) {
        List list;
        HashMap<VASTEventType, List<com.startapp.sdk.ads.video.vast.model.a.c>> hashMap = new HashMap<>();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("/VASTS/VAST/Ad/InLine/Creatives/Creative/Linear/TrackingEvents/Tracking|/VASTS/VAST/Ad/InLine/Creatives/Creative/NonLinearAds/TrackingEvents/Tracking|/VASTS/VAST/Ad/Wrapper/Creatives/Creative/Linear/TrackingEvents/Tracking|/VASTS/VAST/Ad/Wrapper/Creatives/Creative/NonLinearAds/TrackingEvents/Tracking", document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    Node item = nodeList.item(i2);
                    NamedNodeMap attributes = item.getAttributes();
                    String nodeValue = attributes.getNamedItem("event").getNodeValue();
                    try {
                        VASTEventType valueOf = VASTEventType.valueOf(nodeValue);
                        String b2 = AnonymousClass2.b(item);
                        int i3 = -1;
                        Node namedItem = attributes.getNamedItem("offset");
                        if (namedItem != null) {
                            String nodeValue2 = namedItem.getNodeValue();
                            if (nodeValue2 != null) {
                                if (nodeValue2.contains("%")) {
                                    i3 = (this.c / 100) * Integer.parseInt(nodeValue2.replace("%", ""));
                                } else {
                                    i3 = a(nodeValue2) * 1000;
                                }
                            }
                        }
                        if (!hashMap.containsKey(valueOf)) {
                            list = (List) hashMap.get(valueOf);
                        } else {
                            list = new ArrayList();
                            hashMap.put(valueOf, list);
                        }
                        list.add(new com.startapp.sdk.ads.video.vast.model.a.c(b2, i3));
                    } catch (IllegalArgumentException unused) {
                        StringBuilder sb = new StringBuilder("Event:");
                        sb.append(nodeValue);
                        sb.append(" is not valid. Skipping it.");
                    }
                }
            }
            return hashMap;
        } catch (Exception e2) {
            e2.getMessage();
            return null;
        }
    }

    private static List<b> b(Document document) {
        String str;
        Integer num;
        String str2;
        Integer num2;
        Integer num3;
        String str3;
        Boolean bool;
        Boolean bool2;
        String str4;
        ArrayList arrayList = new ArrayList();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//MediaFile", document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    b bVar = new b();
                    Node item = nodeList.item(i2);
                    NamedNodeMap attributes = item.getAttributes();
                    Node namedItem = attributes.getNamedItem("apiFramework");
                    if (namedItem == null) {
                        str = null;
                    } else {
                        str = namedItem.getNodeValue();
                    }
                    bVar.e(str);
                    Node namedItem2 = attributes.getNamedItem("bitrate");
                    if (namedItem2 == null) {
                        num = null;
                    } else {
                        num = Integer.valueOf(namedItem2.getNodeValue());
                    }
                    bVar.a(num);
                    Node namedItem3 = attributes.getNamedItem("delivery");
                    if (namedItem3 == null) {
                        str2 = null;
                    } else {
                        str2 = namedItem3.getNodeValue();
                    }
                    bVar.c(str2);
                    Node namedItem4 = attributes.getNamedItem("height");
                    if (namedItem4 == null) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(namedItem4.getNodeValue());
                    }
                    bVar.c(num2);
                    Node namedItem5 = attributes.getNamedItem("width");
                    if (namedItem5 == null) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(namedItem5.getNodeValue());
                    }
                    bVar.b(num3);
                    Node namedItem6 = attributes.getNamedItem("id");
                    if (namedItem6 == null) {
                        str3 = null;
                    } else {
                        str3 = namedItem6.getNodeValue();
                    }
                    bVar.b(str3);
                    Node namedItem7 = attributes.getNamedItem("maintainAspectRatio");
                    if (namedItem7 == null) {
                        bool = null;
                    } else {
                        bool = Boolean.valueOf(namedItem7.getNodeValue());
                    }
                    bVar.b(bool);
                    Node namedItem8 = attributes.getNamedItem("scalable");
                    if (namedItem8 == null) {
                        bool2 = null;
                    } else {
                        bool2 = Boolean.valueOf(namedItem8.getNodeValue());
                    }
                    bVar.a(bool2);
                    Node namedItem9 = attributes.getNamedItem("type");
                    if (namedItem9 == null) {
                        str4 = null;
                    } else {
                        str4 = namedItem9.getNodeValue();
                    }
                    bVar.d(str4);
                    bVar.a(AnonymousClass2.b(item));
                    if (bVar.f()) {
                        arrayList.add(bVar);
                    }
                }
            }
            return arrayList;
        } catch (Exception e2) {
            e2.getMessage();
            return null;
        }
    }

    private static int c(Document document) {
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//Duration", document, XPathConstants.NODESET);
            if (nodeList != null && nodeList.getLength() > 0) {
                return a(AnonymousClass2.b(nodeList.item(0)));
            }
        } catch (Exception e2) {
            e2.getMessage();
        }
        return Integer.MAX_VALUE;
    }

    private static e d(Document document) {
        e eVar = new e();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//VideoClicks", document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    NodeList childNodes = nodeList.item(i2).getChildNodes();
                    for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                        Node item = childNodes.item(i3);
                        String nodeName = item.getNodeName();
                        String b2 = AnonymousClass2.b(item);
                        if (nodeName.equalsIgnoreCase("ClickTracking")) {
                            eVar.b().add(b2);
                        } else if (nodeName.equalsIgnoreCase("ClickThrough")) {
                            eVar.a(b2);
                        } else if (nodeName.equalsIgnoreCase("CustomClick")) {
                            eVar.c().add(b2);
                        }
                    }
                }
            }
            return eVar;
        } catch (Exception e2) {
            e2.getMessage();
            return null;
        }
    }

    private static int e(Document document) {
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//Linear", document, XPathConstants.NODESET);
            if (nodeList != null) {
                int i2 = 0;
                while (i2 < nodeList.getLength()) {
                    try {
                        if (nodeList.item(i2).getAttributes().getNamedItem("skipoffset") != null) {
                            return a(nodeList.item(i2).getAttributes().getNamedItem("skipoffset").getNodeValue());
                        }
                        i2++;
                    } catch (Exception e2) {
                        e2.getMessage();
                    }
                }
            }
        } catch (Exception e3) {
            e3.getMessage();
        }
        return Integer.MAX_VALUE;
    }

    private static List<a> f(Document document) {
        String str;
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4;
        Integer num5;
        Integer num6;
        String str2;
        Integer num7;
        String str3;
        ArrayList arrayList = new ArrayList();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//Icon", document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    a aVar = new a();
                    Node item = nodeList.item(i2);
                    NamedNodeMap attributes = item.getAttributes();
                    Node namedItem = attributes.getNamedItem(Icon.PROGRAM);
                    if (namedItem == null) {
                        str = null;
                    } else {
                        str = namedItem.getNodeValue();
                    }
                    aVar.a(str);
                    Node namedItem2 = attributes.getNamedItem("width");
                    if (namedItem2 == null) {
                        num = null;
                    } else {
                        num = Integer.valueOf(namedItem2.getNodeValue());
                    }
                    aVar.a(num);
                    Node namedItem3 = attributes.getNamedItem("height");
                    if (namedItem3 == null) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(namedItem3.getNodeValue());
                    }
                    aVar.b(num2);
                    Node namedItem4 = attributes.getNamedItem(Icon.X_POSITION);
                    if (namedItem4 == null) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(namedItem4.getNodeValue());
                    }
                    aVar.c(num3);
                    Node namedItem5 = attributes.getNamedItem(Icon.Y_POSITION);
                    if (namedItem5 == null) {
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(namedItem5.getNodeValue());
                    }
                    aVar.d(num4);
                    Node namedItem6 = attributes.getNamedItem("duration");
                    if (namedItem6 == null) {
                        num5 = null;
                    } else {
                        num5 = Integer.valueOf(namedItem6.getNodeValue());
                    }
                    aVar.e(num5);
                    Node namedItem7 = attributes.getNamedItem("offset");
                    if (namedItem7 == null) {
                        num6 = null;
                    } else {
                        num6 = Integer.valueOf(namedItem7.getNodeValue());
                    }
                    aVar.f(num6);
                    Node namedItem8 = attributes.getNamedItem("apiFramework");
                    if (namedItem8 == null) {
                        str2 = null;
                    } else {
                        str2 = namedItem8.getNodeValue();
                    }
                    aVar.b(str2);
                    Node namedItem9 = attributes.getNamedItem("pxratio");
                    if (namedItem9 == null) {
                        num7 = null;
                    } else {
                        num7 = Integer.valueOf(namedItem9.getNodeValue());
                    }
                    aVar.g(num7);
                    NodeList childNodes = item.getChildNodes();
                    for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                        Node item2 = childNodes.item(i3);
                        String nodeName = item2.getNodeName();
                        String b2 = AnonymousClass2.b(item2);
                        if (nodeName.equalsIgnoreCase("IconClicks")) {
                            NodeList childNodes2 = item.getChildNodes();
                            for (int i4 = 0; i4 < childNodes2.getLength(); i4++) {
                                Node item3 = childNodes.item(i3);
                                String nodeName2 = item3.getNodeName();
                                String b3 = AnonymousClass2.b(item3);
                                if (nodeName2.equalsIgnoreCase("ClickThrough")) {
                                    aVar.c(b3);
                                } else if (nodeName2.equalsIgnoreCase(Icon.ICON_VIEW_TRACKING)) {
                                    aVar.c().add(b3);
                                }
                            }
                        } else if (nodeName.equalsIgnoreCase("ClickTracking")) {
                            aVar.b().add(b2);
                        } else if (nodeName.equalsIgnoreCase("StaticResource")) {
                            d dVar = new d();
                            dVar.b(b2);
                            Node namedItem10 = item.getAttributes().getNamedItem("creativeType");
                            if (namedItem10 == null) {
                                str3 = null;
                            } else {
                                str3 = namedItem10.getNodeValue();
                            }
                            dVar.a(str3);
                            if (dVar.a()) {
                                aVar.a().add(dVar);
                            }
                        }
                    }
                    if (aVar.d()) {
                        arrayList.add(aVar);
                    }
                }
            }
            return arrayList;
        } catch (Exception e2) {
            e2.getMessage();
            return null;
        }
    }

    private static List<String> a(Document document, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate(str, document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    arrayList.add(AnonymousClass2.b(nodeList.item(i2)));
                }
            }
            return arrayList;
        } catch (Exception e2) {
            e2.getMessage();
            return null;
        }
    }

    private static AdVerification g(Document document) {
        AdVerification adVerification;
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//AdVerifications", document, XPathConstants.NODESET);
            if (nodeList != null) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    NodeList childNodes = nodeList.item(i2).getChildNodes();
                    for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                        Node item = childNodes.item(i3);
                        if (item.getNodeName().equalsIgnoreCase(Verification.NAME)) {
                            String str = "";
                            NamedNodeMap attributes = item.getAttributes();
                            String nodeValue = (attributes == null || attributes.getNamedItem(Verification.VENDOR) == null) ? null : attributes.getNamedItem(Verification.VENDOR).getNodeValue();
                            NodeList childNodes2 = item.getChildNodes();
                            String str2 = null;
                            String str3 = null;
                            String str4 = str;
                            for (int i4 = 0; i4 < childNodes2.getLength(); i4++) {
                                Node item2 = childNodes2.item(i4);
                                if (item2.getNodeName().equalsIgnoreCase("JavaScriptResource")) {
                                    Node namedItem = item2.getAttributes().getNamedItem("apiFramework");
                                    if (namedItem != null) {
                                        str4 = namedItem.getNodeValue();
                                    }
                                    str2 = AnonymousClass2.b(item2);
                                } else if (item2.getNodeName().equalsIgnoreCase(Verification.VERIFICATION_PARAMETERS)) {
                                    str3 = AnonymousClass2.b(item2);
                                }
                            }
                            if (!TextUtils.isEmpty(nodeValue) && !TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3) && "omid".equalsIgnoreCase(str4)) {
                                arrayList.add(new VerificationDetails(nodeValue, str2, str3));
                            }
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    adVerification = new AdVerification((VerificationDetails[]) arrayList.toArray(new VerificationDetails[arrayList.size()]));
                    return adVerification;
                }
            }
            adVerification = null;
            return adVerification;
        } catch (Exception e2) {
            e2.getMessage();
            return null;
        }
    }

    private static int a(String str) {
        String[] split = str.split(":");
        return (Integer.parseInt(split[0]) * 3600) + (Integer.parseInt(split[1]) * 60) + Integer.parseInt(split[2]);
    }

    public final boolean a(a aVar) {
        List<String> list = this.e;
        boolean z = (list == null || list.size() == 0) ? false : true;
        List<b> list2 = this.b;
        if (list2 == null || list2.size() == 0) {
            z = false;
        }
        b bVar = null;
        if (z) {
            if (aVar != null) {
                b a2 = aVar.a(this.b);
                if (a2 != null && !TextUtils.isEmpty(a2.a())) {
                    bVar = a2;
                }
            }
            new StringBuilder("Validator returns: ").append(bVar != null ? "valid" : "not valid (no media file)");
        }
        this.h = bVar;
        return this.h != null;
    }
}
