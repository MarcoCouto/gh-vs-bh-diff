package com.startapp.sdk.ads.video.vast.model;

/* compiled from: StartAppSDK */
public enum VASTEventType {
    mute,
    unmute,
    pause,
    resume,
    rewind,
    skip,
    playerExpand,
    fullscreen,
    playerCollapse,
    exitFullscreen,
    start,
    firstQuartile,
    midpoint,
    thirdQuartile,
    complete,
    acceptInvitationLinear,
    timeSpentViewing,
    otherAdInteraction,
    progress,
    creativeView,
    acceptInvitation,
    adExpand,
    adCollapse,
    minimize,
    close,
    overlayViewDuration
}
