package com.startapp.sdk.ads.video.vast.model;

import android.content.Context;
import android.util.DisplayMetrics;
import com.startapp.common.b.e;
import com.startapp.sdk.ads.video.vast.model.a.b;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/* compiled from: StartAppSDK */
public class a {
    protected int a;
    protected int b;
    /* access modifiers changed from: private */
    public int c = (this.a * this.b);

    /* renamed from: com.startapp.sdk.ads.video.vast.model.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    class C0072a implements Comparator<b> {
        private C0072a() {
        }

        /* synthetic */ C0072a(a aVar, byte b) {
            this();
        }

        public final /* synthetic */ int compare(Object obj, Object obj2) {
            b bVar = (b) obj;
            b bVar2 = (b) obj2;
            int intValue = bVar.d().intValue() * bVar.e().intValue();
            int intValue2 = bVar2.d().intValue() * bVar2.e().intValue();
            int abs = Math.abs(intValue - a.this.c);
            int abs2 = Math.abs(intValue2 - a.this.c);
            if (abs < abs2) {
                return -1;
            }
            return abs > abs2 ? 1 : 0;
        }
    }

    public a(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.a = displayMetrics.widthPixels;
        this.b = displayMetrics.heightPixels;
        if (!e.a(context).equals("WIFI")) {
            this.c = (int) (((float) this.c) * 0.75f);
        }
    }

    /* access modifiers changed from: protected */
    public Comparator<b> a() {
        return new C0072a(this, 0);
    }

    public final b a(List<b> list) {
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                if (!bVar.f() || !bVar.b().matches("video/.*(?i)(mp4|3gpp|mp2t|webm|matroska)")) {
                    it.remove();
                }
            }
            if (list.size() != 0) {
                Collections.sort(list, a());
                if (list == null || list.size() <= 0) {
                    return null;
                }
                return (b) list.get(0);
            }
        }
        return null;
    }
}
