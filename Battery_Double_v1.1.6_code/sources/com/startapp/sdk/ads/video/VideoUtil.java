package com.startapp.sdk.ads.video;

import android.content.Context;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.iab.omid.library.startapp.c.b;
import com.iab.omid.library.startapp.c.c;
import com.startapp.sdk.ads.video.a.a;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.activities.FullScreenActivity;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.j;
import java.io.DataInputStream;
import java.io.File;
import java.net.URL;

/* compiled from: StartAppSDK */
public final class VideoUtil {
    private final b a = new b(new c());

    /* compiled from: StartAppSDK */
    public enum VideoEligibility {
        ELIGIBLE(""),
        INELIGIBLE_NO_STORAGE("Not enough storage for video"),
        INELIGIBLE_MISSING_ACTIVITY("FullScreenActivity not declared in AndroidManifest.xml"),
        INELIGIBLE_ERRORS_THRESHOLD_REACHED("Video errors threshold reached.");
        
        private String desctiption;

        private VideoEligibility(String str) {
            this.desctiption = str;
        }

        public final String a() {
            return this.desctiption;
        }
    }

    /* JADX WARNING: type inference failed for: r0v1, types: [java.io.DataInputStream, java.io.FileOutputStream, java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r6v0, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r4v0, types: [java.io.DataInputStream] */
    /* JADX WARNING: type inference failed for: r3v0, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r0v2, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r6v1, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r4v1, types: [java.io.DataInputStream] */
    /* JADX WARNING: type inference failed for: r3v1, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r4v2 */
    /* JADX WARNING: type inference failed for: r3v2 */
    /* JADX WARNING: type inference failed for: r6v2 */
    /* JADX WARNING: type inference failed for: r3v3 */
    /* JADX WARNING: type inference failed for: r4v3 */
    /* JADX WARNING: type inference failed for: r4v4 */
    /* JADX WARNING: type inference failed for: r3v4 */
    /* JADX WARNING: type inference failed for: r6v3 */
    /* JADX WARNING: type inference failed for: r3v5 */
    /* JADX WARNING: type inference failed for: r4v5 */
    /* JADX WARNING: type inference failed for: r1v5, types: [java.lang.String] */
    /* JADX WARNING: type inference failed for: r3v7, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r4v6 */
    /* JADX WARNING: type inference failed for: r4v7 */
    /* JADX WARNING: type inference failed for: r4v8, types: [java.io.DataInputStream] */
    /* JADX WARNING: type inference failed for: r6v4 */
    /* JADX WARNING: type inference failed for: r6v5 */
    /* JADX WARNING: type inference failed for: r6v8, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r0v3 */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r6v9 */
    /* JADX WARNING: type inference failed for: r4v9 */
    /* JADX WARNING: type inference failed for: r3v8 */
    /* JADX WARNING: type inference failed for: r4v10 */
    /* JADX WARNING: type inference failed for: r3v9 */
    /* JADX WARNING: type inference failed for: r3v10 */
    /* JADX WARNING: type inference failed for: r4v11 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: type inference failed for: r3v12 */
    /* JADX WARNING: type inference failed for: r3v13 */
    /* JADX WARNING: type inference failed for: r3v14 */
    /* JADX WARNING: type inference failed for: r3v15 */
    /* JADX WARNING: type inference failed for: r3v16 */
    /* JADX WARNING: type inference failed for: r3v17 */
    /* JADX WARNING: type inference failed for: r4v12 */
    /* JADX WARNING: type inference failed for: r4v13 */
    /* JADX WARNING: type inference failed for: r4v14 */
    /* JADX WARNING: type inference failed for: r6v10 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v1, types: [java.io.DataInputStream, java.io.FileOutputStream, java.io.InputStream]
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [?[OBJECT, ARRAY], java.io.InputStream, java.io.DataInputStream, java.io.FileOutputStream]
  mth insns count: 99
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 20 */
    public static String a(Context context, URL url, String str) {
        ? r6;
        ? r4;
        ? r3;
        ? r0;
        ? r62;
        ? r42;
        ? r32;
        ? r43;
        ? r33;
        ? r44;
        ? r45;
        ? r34;
        ? r46;
        new StringBuilder("Downloading video from ").append(url);
        ? r02 = 0;
        try {
            ? a2 = a(context, str);
            File file = new File(a2);
            if (file.exists()) {
                try {
                    r02.close();
                    r02.close();
                    r02.close();
                } catch (Exception unused) {
                }
                return a2;
            }
            ? openStream = url.openStream();
            try {
                ? dataInputStream = new DataInputStream(openStream);
                try {
                    byte[] bArr = new byte[4096];
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(".temp");
                    ? openFileOutput = context.openFileOutput(sb.toString(), 0);
                    while (true) {
                        try {
                            int read = dataInputStream.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            openFileOutput.write(bArr, 0, read);
                        } catch (Exception e) {
                            e = e;
                            r32 = openStream;
                            r42 = dataInputStream;
                            r62 = openFileOutput;
                            try {
                                Log.e("StartAppWall.VideoUtil", "Error downloading video from ".concat(String.valueOf(url)), e);
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(str);
                                sb2.append(".temp");
                                new File(a(context, sb2.toString())).delete();
                                try {
                                    r32.close();
                                    r42.close();
                                    r62.close();
                                    r0 = r02;
                                } catch (Exception unused2) {
                                    r0 = r02;
                                }
                                return r0;
                            } catch (Throwable th) {
                                th = th;
                                r6 = r62;
                                r4 = r42;
                                r3 = r32;
                                try {
                                    r3.close();
                                    r4.close();
                                    r6.close();
                                } catch (Exception unused3) {
                                }
                                throw th;
                            }
                        }
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append(".temp");
                    new File(a(context, sb3.toString())).renameTo(file);
                    try {
                        openStream.close();
                        dataInputStream.close();
                        openFileOutput.close();
                    } catch (Exception unused4) {
                    }
                    r0 = a2;
                } catch (Exception e2) {
                    e = e2;
                    r62 = r02;
                    r32 = openStream;
                    r42 = dataInputStream;
                    Log.e("StartAppWall.VideoUtil", "Error downloading video from ".concat(String.valueOf(url)), e);
                    StringBuilder sb22 = new StringBuilder();
                    sb22.append(str);
                    sb22.append(".temp");
                    new File(a(context, sb22.toString())).delete();
                    r32.close();
                    r42.close();
                    r62.close();
                    r0 = r02;
                    return r0;
                } catch (Throwable th2) {
                    th = th2;
                    r6 = r02;
                    r3 = openStream;
                    r4 = dataInputStream;
                    r3.close();
                    r4.close();
                    r6.close();
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                r44 = r02;
                r33 = openStream;
                r62 = r43;
                r42 = r43;
                r32 = r33;
                Log.e("StartAppWall.VideoUtil", "Error downloading video from ".concat(String.valueOf(url)), e);
                StringBuilder sb222 = new StringBuilder();
                sb222.append(str);
                sb222.append(".temp");
                new File(a(context, sb222.toString())).delete();
                r32.close();
                r42.close();
                r62.close();
                r0 = r02;
                return r0;
            } catch (Throwable th3) {
                th = th3;
                r46 = r02;
                r34 = openStream;
                r6 = r45;
                r4 = r45;
                r3 = r34;
                r3.close();
                r4.close();
                r6.close();
                throw th;
            }
            return r0;
        } catch (Exception e4) {
            e = e4;
            ? r35 = r02;
            r44 = r35;
            r33 = r35;
            r62 = r43;
            r42 = r43;
            r32 = r33;
            Log.e("StartAppWall.VideoUtil", "Error downloading video from ".concat(String.valueOf(url)), e);
            StringBuilder sb2222 = new StringBuilder();
            sb2222.append(str);
            sb2222.append(".temp");
            new File(a(context, sb2222.toString())).delete();
            r32.close();
            r42.close();
            r62.close();
            r0 = r02;
            return r0;
        } catch (Throwable th4) {
            th = th4;
            ? r36 = r02;
            r46 = r36;
            r34 = r36;
            r6 = r45;
            r4 = r45;
            r3 = r34;
            r3.close();
            r4.close();
            r6.close();
            throw th;
        }
    }

    public static VideoEligibility a(Context context) {
        VideoEligibility videoEligibility = VideoEligibility.ELIGIBLE;
        boolean z = true;
        if (AdsCommonMetaData.a().I().e() >= 0 && j.a(context, "videoErrorsCount", Integer.valueOf(0)).intValue() >= AdsCommonMetaData.a().I().e()) {
            videoEligibility = VideoEligibility.INELIGIBLE_ERRORS_THRESHOLD_REACHED;
        }
        if (!s.a(context, FullScreenActivity.class)) {
            videoEligibility = VideoEligibility.INELIGIBLE_MISSING_ACTIVITY;
        }
        long a2 = s.a(context.getFilesDir());
        if (a2 < 0 || a2 / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID <= (AdsCommonMetaData.a().I().c() << 10)) {
            z = false;
        }
        return !z ? VideoEligibility.INELIGIBLE_NO_STORAGE : videoEligibility;
    }

    public static void b(Context context) {
        j.b(context, "videoErrorsCount", Integer.valueOf(j.a(context, "videoErrorsCount", Integer.valueOf(0)).intValue() + 1));
    }

    public static void a(Context context, a aVar) {
        if (aVar != null) {
            for (String b : aVar.a()) {
                com.startapp.sdk.adsbase.a.b(context, b);
            }
        }
    }

    static String a(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getFilesDir());
        sb.append("/");
        sb.append(str);
        return sb.toString();
    }

    public final com.iab.omid.library.startapp.c.a a() {
        return this.a;
    }
}
