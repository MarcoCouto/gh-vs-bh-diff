package com.startapp.sdk.ads.video;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import java.net.URL;

/* compiled from: StartAppSDK */
public final class g {
    protected a a;
    private Context b;
    private URL c;
    private String d;
    private com.startapp.sdk.ads.video.c.a e;

    /* compiled from: StartAppSDK */
    public interface a {
        void a(String str);
    }

    public g(Context context, URL url, String str, a aVar, com.startapp.sdk.ads.video.c.a aVar2) {
        this.b = context;
        this.c = url;
        this.d = str;
        this.a = aVar;
        this.e = aVar2;
    }

    public final void a() {
        final String str;
        try {
            str = AdsCommonMetaData.a().I().i() ? b.a.a(this.b, this.c, this.d, this.e) : VideoUtil.a(this.b, this.c, this.d);
        } catch (Exception unused) {
            str = null;
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                if (g.this.a != null) {
                    g.this.a.a(str);
                }
            }
        });
    }
}
