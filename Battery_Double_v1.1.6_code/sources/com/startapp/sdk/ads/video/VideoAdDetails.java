package com.startapp.sdk.ads.video;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.parser.d;
import com.startapp.sdk.ads.video.tracking.VideoTrackingDetails;
import com.startapp.sdk.ads.video.vast.model.c;
import com.startapp.sdk.omsdk.AdVerification;
import com.startapp.sdk.omsdk.VerificationDetails;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public class VideoAdDetails implements Serializable {
    private static final long serialVersionUID = 1;
    @d(b = VerificationDetails.class, f = "adVerifications")
    private VerificationDetails[] adVerifications;
    private String clickUrl;
    private boolean clickable;
    private boolean closeable;
    private boolean isVideoMuted;
    private String localVideoPath;
    @d(b = PostRollType.class)
    private PostRollType postRoll;
    private boolean skippable;
    private int skippableAfter;
    @d(a = true)
    private VideoTrackingDetails videoTrackingDetails;
    private String videoUrl;

    /* compiled from: StartAppSDK */
    public enum PostRollType {
        IMAGE,
        LAST_FRAME,
        NONE
    }

    public VideoAdDetails() {
    }

    public VideoAdDetails(c cVar, boolean z) {
        if (cVar != null) {
            this.videoTrackingDetails = new VideoTrackingDetails(cVar);
            if (cVar.f() != null) {
                this.videoUrl = cVar.f().a();
            }
            boolean z2 = true;
            if (z) {
                this.skippableAfter = cVar.e();
                this.skippable = this.skippableAfter != Integer.MAX_VALUE;
            } else {
                this.skippable = false;
            }
            this.clickUrl = cVar.b().a();
            if (this.clickUrl == null) {
                z2 = false;
            }
            this.clickable = z2;
            this.postRoll = PostRollType.NONE;
            AdVerification g = cVar.g();
            if (g != null && g.a() != null) {
                this.adVerifications = (VerificationDetails[]) g.a().toArray(new VerificationDetails[g.a().size()]);
            }
        }
    }

    public final String a() {
        return this.videoUrl;
    }

    public final String b() {
        return this.localVideoPath;
    }

    public final void a(String str) {
        this.localVideoPath = str;
    }

    public final PostRollType c() {
        return this.postRoll;
    }

    public final boolean d() {
        return this.closeable;
    }

    public final boolean e() {
        return this.skippable;
    }

    public final int f() {
        return this.skippableAfter;
    }

    public final boolean g() {
        return this.clickable;
    }

    public final VideoTrackingDetails h() {
        return this.videoTrackingDetails;
    }

    public final boolean i() {
        return this.isVideoMuted;
    }

    public final void a(boolean z) {
        this.isVideoMuted = z;
    }

    public final String j() {
        return this.clickUrl;
    }

    public final AdVerification k() {
        return new AdVerification(this.adVerifications);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VideoAdDetails [videoUrl=");
        sb.append(this.videoUrl);
        sb.append(", localVideoPath=");
        sb.append(this.localVideoPath);
        sb.append(", postRoll=");
        sb.append(this.postRoll);
        sb.append(", closeable=");
        sb.append(this.closeable);
        sb.append(", skippable=");
        sb.append(this.skippable);
        sb.append(", skippableAfter=");
        sb.append(this.skippableAfter);
        sb.append(", videoTrackingDetails= ");
        sb.append(this.videoTrackingDetails);
        sb.append(", isVideoMuted= ");
        sb.append(this.isVideoMuted);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
