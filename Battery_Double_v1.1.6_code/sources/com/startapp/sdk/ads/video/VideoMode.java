package com.startapp.sdk.ads.video;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
import com.facebook.appevents.codeless.internal.Constants;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.b.b.a;
import com.startapp.sdk.ads.a.c;
import com.startapp.sdk.ads.video.VideoAdDetails.PostRollType;
import com.startapp.sdk.ads.video.player.NativeVideoPlayer;
import com.startapp.sdk.ads.video.player.NativeVideoPlayer.MediaErrorExtra;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.VideoPlayerErrorType;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.b;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.d;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.e;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.f;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.g;
import com.startapp.sdk.ads.video.tracking.AbsoluteTrackingLink;
import com.startapp.sdk.ads.video.tracking.ActionTrackingLink;
import com.startapp.sdk.ads.video.tracking.FractionTrackingLink;
import com.startapp.sdk.ads.video.tracking.VideoClickedTrackingParams;
import com.startapp.sdk.ads.video.tracking.VideoClickedTrackingParams.ClickOrigin;
import com.startapp.sdk.ads.video.tracking.VideoPausedTrackingParams;
import com.startapp.sdk.ads.video.tracking.VideoPausedTrackingParams.PauseOrigin;
import com.startapp.sdk.ads.video.tracking.VideoProgressTrackingParams;
import com.startapp.sdk.ads.video.tracking.VideoTrackingLink;
import com.startapp.sdk.ads.video.tracking.VideoTrackingParams;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.adsbase.Ad.AdType;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.VideoConfig.BackMode;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.i.r;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class VideoMode extends c implements a, VideoPlayerInterface.a, b, VideoPlayerInterface.c, d, e, f {
    private int A = 0;
    private int B = 0;
    private boolean C = false;
    private boolean D = false;
    private HashMap<Integer, Boolean> E = new HashMap<>();
    private HashMap<Integer, Boolean> F = new HashMap<>();
    private int G = 1;
    private boolean H = false;
    private boolean I = false;
    private int J = 0;
    private boolean K = false;
    private boolean L = false;
    private boolean M = false;
    private int N = 0;
    private int O;
    private String P = null;
    private Handler Q = new Handler();
    private Map<Integer, List<FractionTrackingLink>> R = new HashMap();
    private Map<Integer, List<AbsoluteTrackingLink>> S = new HashMap();
    private long T;
    private ClickOrigin U;
    private long V;
    /* access modifiers changed from: private */
    public com.startapp.sdk.ads.banner.bannerstandard.b W;
    private boolean X = false;
    /* access modifiers changed from: private */
    public BroadcastReceiver Y = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            if (!VideoMode.this.Y.isInitialStickyBroadcast() && VideoMode.this.o != VideoMode.this.Y()) {
                VideoMode.this.o = !VideoMode.this.o;
                VideoMode.this.R();
                VideoMode.this.a(VideoMode.this.o);
            }
        }
    };
    protected VideoPlayerInterface l;
    protected VideoView m;
    protected ProgressBar n;
    protected boolean o = false;
    protected int p = 0;
    protected boolean q;
    protected boolean r = false;
    protected boolean s = false;
    protected boolean t = false;
    protected boolean u = false;
    protected Handler v = new Handler();
    protected Handler w = new Handler();
    protected Handler x = new Handler();
    private RelativeLayout y;
    private RelativeLayout z;

    /* compiled from: StartAppSDK */
    private enum HtmlMode {
        PLAYER,
        POST_ROLL
    }

    /* compiled from: StartAppSDK */
    private enum Sound {
        ON,
        OFF
    }

    /* compiled from: StartAppSDK */
    private enum VideoFinishedReason {
        COMPLETE,
        CLICKED,
        SKIPPED
    }

    /* access modifiers changed from: protected */
    public final void A() {
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        try {
            this.T = System.currentTimeMillis();
            this.O = 100 / AdsCommonMetaData.a().I().j();
            boolean z2 = true;
            if (h().equals("back")) {
                if (AdsCommonMetaData.a().I().a().equals(BackMode.BOTH)) {
                    this.H = true;
                    this.I = true;
                } else if (AdsCommonMetaData.a().I().a().equals(BackMode.SKIP)) {
                    this.H = true;
                    this.I = false;
                } else if (AdsCommonMetaData.a().I().a().equals(BackMode.CLOSE)) {
                    this.H = false;
                    this.I = true;
                } else {
                    AdsCommonMetaData.a().I().a().equals(BackMode.DISABLED);
                    this.H = false;
                    this.I = false;
                }
            }
            FractionTrackingLink[] a = P().h().a();
            if (a != null) {
                for (FractionTrackingLink fractionTrackingLink : a) {
                    List list = (List) this.R.get(Integer.valueOf(fractionTrackingLink.a()));
                    if (list == null) {
                        list = new ArrayList();
                        this.R.put(Integer.valueOf(fractionTrackingLink.a()), list);
                    }
                    list.add(fractionTrackingLink);
                }
            }
            AbsoluteTrackingLink[] b = P().h().b();
            if (b != null) {
                for (AbsoluteTrackingLink absoluteTrackingLink : b) {
                    List list2 = (List) this.S.get(Integer.valueOf(absoluteTrackingLink.a()));
                    if (list2 == null) {
                        list2 = new ArrayList();
                        this.S.put(Integer.valueOf(absoluteTrackingLink.a()), list2);
                    }
                    list2.add(absoluteTrackingLink);
                }
            }
            if (!Y() && !P().i()) {
                if (!AdsCommonMetaData.a().I().m().equals("muted")) {
                    z2 = false;
                }
            }
            this.o = z2;
            if (bundle != null && bundle.containsKey("currentPosition")) {
                this.p = bundle.getInt("currentPosition");
                this.A = bundle.getInt("latestPosition");
                this.E = (HashMap) bundle.getSerializable("fractionProgressImpressionsSent");
                this.F = (HashMap) bundle.getSerializable("absoluteProgressImpressionsSent");
                this.o = bundle.getBoolean(String.IS_MUTED);
                this.q = bundle.getBoolean("shouldSetBg");
                this.G = bundle.getInt("pauseNum");
            }
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a((Context) c());
            am();
            p();
        }
    }

    public final void a(WebView webView) {
        super.a(webView);
        webView.setBackgroundColor(33554431);
        com.startapp.common.b.b.d(webView);
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        this.C = true;
        if (this.D && an()) {
            I();
        } else if (ah()) {
            b((View) this.c);
        }
        if (ao()) {
            V();
        }
        if (ah()) {
            aa();
        }
        VideoAdDetails P2 = P();
        if (MetaData.D().P() && this.d == null && P2 != null) {
            P2.k();
            if (P2.k().a() != null) {
                this.d = com.startapp.sdk.omsdk.a.a(this.c.getContext(), P().k());
                if (this.d != null) {
                    this.W = com.startapp.sdk.ads.banner.bannerstandard.b.a(this.d);
                    View a = this.a.a();
                    if (a != null) {
                        this.d.b(a);
                    }
                    this.d.b(this.c);
                    this.d.b(this.z);
                    this.d.a(this.m);
                    this.d.a();
                    com.iab.omid.library.startapp.adsession.a.a(this.d).a();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void I() {
        if (this.r) {
            b((View) this.m);
            if (!ah()) {
                ab();
            }
        }
    }

    public final void u() {
        super.u();
        c().registerReceiver(this.Y, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
        this.X = true;
        if (!c().isFinishing()) {
            if (this.m == null) {
                Context applicationContext = c().getApplicationContext();
                this.V = System.currentTimeMillis();
                this.z = (RelativeLayout) c().findViewById(1475346432);
                LayoutParams layoutParams = new LayoutParams(-1, -1);
                this.m = new VideoView(applicationContext);
                this.m.setId(100);
                LayoutParams layoutParams2 = new LayoutParams(-1, -1);
                layoutParams2.addRule(13);
                this.n = new ProgressBar(applicationContext, null, 16843399);
                this.n.setVisibility(4);
                LayoutParams layoutParams3 = new LayoutParams(-2, -2);
                layoutParams3.addRule(14);
                layoutParams3.addRule(15);
                this.y = new RelativeLayout(applicationContext);
                this.y.setId(1475346436);
                c().setContentView(this.y);
                this.y.addView(this.m, layoutParams2);
                this.y.addView(this.z, layoutParams);
                this.y.addView(this.n, layoutParams3);
                if (AdsConstants.a().booleanValue()) {
                    LayoutParams layoutParams4 = new LayoutParams(-2, -2);
                    layoutParams4.addRule(12);
                    layoutParams4.addRule(14);
                    RelativeLayout relativeLayout = this.y;
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sb2 = new StringBuilder("url=");
                    sb2.append(P().a());
                    sb.append(sb2.toString());
                    TextView textView = new TextView(applicationContext);
                    textView.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                    com.startapp.common.b.b.a((View) textView, 0.5f);
                    textView.setTextColor(-7829368);
                    textView.setSingleLine(false);
                    textView.setText(sb.toString());
                    relativeLayout.addView(textView, layoutParams4);
                }
                this.a.a().setVisibility(4);
            }
            if (this.l == null) {
                this.l = new NativeVideoPlayer(this.m);
            }
            this.D = false;
            this.y.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            J();
            if (ah()) {
                this.a.a().setVisibility(0);
                this.m.setVisibility(4);
            } else if (this.p != 0) {
                this.l.a(this.p);
                PauseOrigin pauseOrigin = PauseOrigin.EXTERNAL;
                new StringBuilder("Sending resume event with pause origin: ").append(pauseOrigin);
                ActionTrackingLink[] h = P().h().h();
                VideoPausedTrackingParams videoPausedTrackingParams = new VideoPausedTrackingParams(m(), h(this.A), this.i, this.G, pauseOrigin, this.P);
                a(h, videoPausedTrackingParams, this.A, "resumed");
                this.G++;
            }
            this.l.a((f) this);
            this.l.a((d) this);
            this.l.a((e) this);
            this.l.a((b) this);
            this.l.a((VideoPlayerInterface.c) this);
            this.l.a((VideoPlayerInterface.a) this);
            com.startapp.common.b.b.a((View) this.m, (a) this);
        }
    }

    /* access modifiers changed from: protected */
    public final void J() {
        boolean i = AdsCommonMetaData.a().I().i();
        String b = P().b();
        if (b != null) {
            this.l.a(b);
            if (i) {
                b.a;
                if (c.b(b)) {
                    this.u = true;
                    this.M = true;
                    this.J = AdsCommonMetaData.a().I().k();
                }
            }
        } else if (i) {
            String a = P().a();
            b.a.a(a);
            this.l.a(a);
            this.u = true;
            W();
        } else {
            a(VideoFinishedReason.SKIPPED);
        }
        if (this.P == null) {
            this.P = this.u ? "2" : "1";
        }
    }

    private void V() {
        this.L = true;
        ac();
        if (ah()) {
            this.l.b();
            return;
        }
        new Handler().postDelayed(new Runnable() {
            public final void run() {
                if (VideoMode.this.l != null) {
                    VideoMode.this.l.a();
                    if (VideoMode.this.W != null) {
                        VideoMode.this.W.a((float) VideoMode.this.l.e(), VideoMode.this.o ? 0.0f : 1.0f);
                    }
                    VideoMode.this.r = true;
                    VideoMode.this.K();
                    new Handler().post(new Runnable() {
                        public final void run() {
                            VideoMode.this.I();
                        }
                    });
                }
            }
        }, Z());
        if (this.p == 0) {
            this.v.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (VideoMode.this.l != null) {
                            if (VideoMode.this.l.d() > 0) {
                                VideoMode.this.e(0);
                                VideoMode.this.f(0);
                                if (VideoMode.this.i == 0) {
                                    VideoMode.this.S();
                                    com.startapp.common.b.a((Context) VideoMode.this.c()).a(new Intent("com.startapp.android.ShowDisplayBroadcastListener"));
                                }
                            } else if (!VideoMode.this.s) {
                                VideoMode.this.v.postDelayed(this, 100);
                            }
                        }
                    } catch (Throwable th) {
                        new com.startapp.sdk.adsbase.infoevents.e(th).a((Context) VideoMode.this.c());
                        VideoMode.this.p();
                    }
                }
            }, 100);
        }
        ai();
        al();
        ad();
        ae();
        this.a.a().setVisibility(4);
        R();
    }

    private void W() {
        if (!X()) {
            this.t = false;
            this.x.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        VideoMode.this.n.setVisibility(0);
                        if (VideoMode.this.W != null) {
                            VideoMode.this.W.f();
                        }
                        VideoMode.this.x.postDelayed(new Runnable() {
                            public final void run() {
                                try {
                                    VideoMode.this.K();
                                    VideoMode.this.t = true;
                                    VideoMode.this.a(new g(VideoPlayerErrorType.BUFFERING_TIMEOUT, "Buffering timeout reached", VideoMode.this.p));
                                } catch (Throwable th) {
                                    new com.startapp.sdk.adsbase.infoevents.e(th).a((Context) VideoMode.this.c());
                                }
                            }
                        }, AdsCommonMetaData.a().I().g());
                    } catch (Throwable th) {
                        VideoMode.this.K();
                        new com.startapp.sdk.adsbase.infoevents.e(th).a((Context) VideoMode.this.c());
                    }
                }
            }, AdsCommonMetaData.a().I().f());
        }
    }

    /* access modifiers changed from: protected */
    public final void K() {
        this.x.removeCallbacksAndMessages(null);
        if (X()) {
            this.n.setVisibility(8);
            if (this.W != null) {
                this.W.g();
            }
        }
    }

    private boolean X() {
        return this.n != null && this.n.isShown();
    }

    /* access modifiers changed from: private */
    public boolean Y() {
        AudioManager audioManager = (AudioManager) c().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager == null || (audioManager.getRingerMode() != 0 && audioManager.getRingerMode() != 1)) {
            return false;
        }
        return true;
    }

    private long Z() {
        long currentTimeMillis = System.currentTimeMillis() - this.T;
        if (this.p == 0 && this.i == 0 && currentTimeMillis < 500) {
            return Math.max(200, 500 - currentTimeMillis);
        }
        return 0;
    }

    private void aa() {
        String str = "videoApi.setReplayEnabled";
        Object[] objArr = new Object[1];
        objArr[0] = Boolean.valueOf(this.l != null);
        a(str, objArr);
        StringBuilder sb = new StringBuilder();
        sb.append(HtmlMode.POST_ROLL);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(P().c());
        a("videoApi.setMode", sb.toString());
        a("videoApi.setCloseable", Boolean.TRUE);
    }

    private void ab() {
        a("videoApi.setClickableVideo", Boolean.valueOf(P().g()));
        a("videoApi.setMode", HtmlMode.PLAYER.toString());
        String str = "videoApi.setCloseable";
        Object[] objArr = new Object[1];
        objArr[0] = Boolean.valueOf(P().d() || this.I);
        a(str, objArr);
        a("videoApi.setSkippable", Boolean.valueOf(ap()));
    }

    private void ac() {
        a("videoApi.setVideoDuration", Integer.valueOf(this.l.e() / 1000));
        M();
        af();
        a("videoApi.setVideoCurrentPosition", Integer.valueOf(this.p / 1000));
    }

    /* access modifiers changed from: protected */
    public final void L() {
        a("videoApi.setVideoCurrentPosition", Integer.valueOf(0));
        a("videoApi.setSkipTimer", Integer.valueOf(0));
    }

    private void b(View view) {
        a("videoApi.setVideoFrame", Integer.valueOf(r.b(c(), view.getLeft())), Integer.valueOf(r.b(c(), view.getTop())), Integer.valueOf(r.b(c(), view.getWidth())), Integer.valueOf(r.b(c(), view.getHeight())));
    }

    private void ad() {
        this.w.post(new Runnable() {
            public final void run() {
                int M = VideoMode.this.M();
                if (M >= 1000) {
                    VideoMode.this.w.postDelayed(this, VideoMode.a((long) M) + 50);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final int M() {
        int ag = ag();
        int i = ag / 1000;
        if (i > 0 && ag % 1000 < 100) {
            i--;
        }
        a("videoApi.setVideoRemainingTimer", Integer.valueOf(i));
        return ag;
    }

    private void ae() {
        af();
        this.w.post(new Runnable() {
            private boolean a;
            private final int b = VideoMode.this.d(AdsCommonMetaData.a().I().d());

            public final void run() {
                try {
                    int c2 = VideoMode.this.c(VideoMode.this.l.d() + 50);
                    if (c2 >= 0 && !this.a) {
                        if (c2 != 0) {
                            if (VideoMode.this.p < VideoMode.this.P().f() * 1000) {
                                VideoMode.this.a("videoApi.setSkipTimer", Integer.valueOf(c2));
                            }
                        }
                        this.a = true;
                        VideoMode.this.a("videoApi.setSkipTimer", Integer.valueOf(0));
                    }
                    if (VideoMode.this.u && VideoMode.this.l.d() >= this.b) {
                        VideoMode.this.F();
                    }
                    int d = (VideoMode.this.l.d() + 50) / 1000;
                    VideoMode.this.a("videoApi.setVideoCurrentPosition", Integer.valueOf(d));
                    if (d < VideoMode.this.l.e() / 1000) {
                        VideoMode.this.w.postDelayed(this, VideoMode.this.N());
                    }
                } catch (Exception unused) {
                }
            }
        });
    }

    private void af() {
        a("videoApi.setSkipTimer", Integer.valueOf(c(this.p + 50)));
    }

    private int ag() {
        if (this.l.d() != this.l.e() || ah()) {
            return this.l.e() - this.l.d();
        }
        return this.l.e();
    }

    /* access modifiers changed from: protected */
    public final long N() {
        return (long) (1000 - (this.l.d() % 1000));
    }

    /* access modifiers changed from: protected */
    public final int c(int i) {
        if (this.H || this.i > 0) {
            return 0;
        }
        int f = (P().f() * 1000) - i;
        if (f <= 0) {
            return 0;
        }
        return (f / 1000) + 1;
    }

    private void a(VideoFinishedReason videoFinishedReason) {
        if (videoFinishedReason == VideoFinishedReason.COMPLETE && this.W != null) {
            this.W.d();
        }
        if (videoFinishedReason == VideoFinishedReason.SKIPPED && this.W != null) {
            this.W.h();
        }
        if (videoFinishedReason == VideoFinishedReason.SKIPPED || videoFinishedReason == VideoFinishedReason.CLICKED) {
            this.v.removeCallbacksAndMessages(null);
            this.Q.removeCallbacksAndMessages(null);
            if (this.l != null) {
                this.A = this.l.d();
                this.l.b();
            }
        } else {
            this.A = this.B;
            F();
        }
        this.w.removeCallbacksAndMessages(null);
        this.E.clear();
        this.F.clear();
        if (videoFinishedReason == VideoFinishedReason.CLICKED) {
            this.p = -1;
            return;
        }
        if (P().c() != PostRollType.NONE) {
            aa();
            this.a.a().setVisibility(0);
        }
        if (P().c() == PostRollType.IMAGE) {
            new Handler().postDelayed(new Runnable() {
                public final void run() {
                    if (!VideoMode.this.t) {
                        VideoMode.this.m.setVisibility(4);
                    }
                }
            }, 1000);
        } else if (P().c() == PostRollType.NONE) {
            p();
        }
        this.p = -1;
        if (P().c() != PostRollType.NONE) {
            aq();
        }
    }

    /* access modifiers changed from: protected */
    public final void O() {
        this.p = 0;
    }

    private boolean ah() {
        return this.p == -1;
    }

    private void ai() {
        this.B = this.l.e();
        aj();
        ak();
    }

    private void aj() {
        for (Integer intValue : this.R.keySet()) {
            final int intValue2 = intValue.intValue();
            a(d(intValue2), this.v, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        VideoMode.this.e(intValue2);
                    } catch (Throwable th) {
                        new com.startapp.sdk.adsbase.infoevents.e(th).a((Context) VideoMode.this.c());
                    }
                }
            });
        }
    }

    private void ak() {
        for (Integer intValue : this.S.keySet()) {
            final int intValue2 = intValue.intValue();
            a(intValue2, this.v, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        VideoMode.this.f(intValue2);
                    } catch (Throwable th) {
                        new com.startapp.sdk.adsbase.infoevents.e(th).a((Context) VideoMode.this.c());
                    }
                }
            });
        }
    }

    private void al() {
        if (!this.u) {
            a(d(AdsCommonMetaData.a().I().d()), this.Q, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        VideoMode.this.F();
                    } catch (Throwable th) {
                        new com.startapp.sdk.adsbase.infoevents.e(th).a((Context) VideoMode.this.c());
                    }
                }
            });
        }
    }

    private void a(int i, Handler handler, Runnable runnable) {
        if (this.p < i) {
            handler.postDelayed(runnable, (long) (i - this.p));
        }
    }

    /* access modifiers changed from: protected */
    public final int d(int i) {
        return (this.B * i) / 100;
    }

    /* access modifiers changed from: protected */
    public final boolean H() {
        return x().getType() == AdType.REWARDED_VIDEO;
    }

    public final void b(Bundle bundle) {
        super.b(bundle);
        bundle.putInt("currentPosition", this.p);
        bundle.putInt("latestPosition", this.A);
        bundle.putSerializable("fractionProgressImpressionsSent", this.E);
        bundle.putSerializable("absoluteProgressImpressionsSent", this.F);
        bundle.putBoolean(String.IS_MUTED, this.o);
        bundle.putBoolean("shouldSetBg", this.q);
        bundle.putInt("pauseNum", this.G);
    }

    /* access modifiers changed from: protected */
    public final VideoAdDetails P() {
        return ((VideoEnabledAd) x()).g();
    }

    public final void s() {
        if (!ah() && !c().isFinishing() && !this.I && !this.H) {
            PauseOrigin pauseOrigin = PauseOrigin.EXTERNAL;
            if (this.l != null) {
                int d = this.l.d();
                this.p = d;
                this.A = d;
                this.l.b();
                if (this.W != null) {
                    this.W.e();
                }
            }
            new StringBuilder("Sending pause event with origin: ").append(pauseOrigin);
            ActionTrackingLink[] g = P().h().g();
            VideoPausedTrackingParams videoPausedTrackingParams = new VideoPausedTrackingParams(m(), h(this.A), this.i, this.G, pauseOrigin, this.P);
            a(g, videoPausedTrackingParams, this.A, "paused");
        }
        if (this.l != null) {
            this.l.g();
            this.l = null;
        }
        this.v.removeCallbacksAndMessages(null);
        this.w.removeCallbacksAndMessages(null);
        this.Q.removeCallbacksAndMessages(null);
        K();
        this.q = true;
        if (this.X) {
            c().unregisterReceiver(this.Y);
            this.X = false;
        }
        super.s();
    }

    public final void p() {
        super.p();
        if (this.M) {
            b.a;
            c.c(P().b());
        }
    }

    /* access modifiers changed from: protected */
    public final com.startapp.sdk.d.b z() {
        f fVar = new f(c(), this.k, this.k, new Runnable() {
            public final void run() {
                VideoMode.this.i = VideoMode.this.i + 1;
                VideoMode.this.m.setVisibility(0);
                VideoMode.this.q = false;
                VideoMode.this.O();
                VideoMode.this.L();
                VideoMode.this.J();
            }
        }, new Runnable() {
            public final void run() {
                VideoMode.this.Q();
            }
        }, new Runnable() {
            public final void run() {
                VideoMode.this.o = !VideoMode.this.o;
                VideoMode.this.R();
                VideoMode.this.a(VideoMode.this.o);
            }
        }, new TrackingParams(m()), a(0));
        return fVar;
    }

    /* access modifiers changed from: protected */
    public final void Q() {
        if (X()) {
            K();
        }
        a(VideoFinishedReason.SKIPPED);
        a(P().h().i(), new VideoTrackingParams(m(), h(this.A), this.i, this.P), this.A, String.VIDEO_SKIPPED);
    }

    /* access modifiers changed from: protected */
    public final void R() {
        String str;
        if (this.l != null) {
            try {
                if (this.o) {
                    this.l.a(true);
                } else {
                    this.l.a(false);
                }
            } catch (IllegalStateException unused) {
            }
        }
        String str2 = "videoApi.setSound";
        Object[] objArr = new Object[1];
        if (this.o) {
            str = Sound.OFF.toString();
        } else {
            str = Sound.ON.toString();
        }
        objArr[0] = str;
        a(str2, objArr);
    }

    /* access modifiers changed from: protected */
    public final void a(g gVar) {
        VASTErrorCodes vASTErrorCodes;
        com.startapp.sdk.adsbase.infoevents.e eVar = new com.startapp.sdk.adsbase.infoevents.e(InfoEventCategory.ERROR);
        StringBuilder sb = new StringBuilder("Video player error: ");
        sb.append(gVar.a());
        eVar.e(sb.toString()).f(gVar.b()).g(n()).a((Context) c());
        switch (gVar.a()) {
            case SERVER_DIED:
                vASTErrorCodes = VASTErrorCodes.GeneralLinearError;
                break;
            case BUFFERING_TIMEOUT:
                vASTErrorCodes = VASTErrorCodes.TimeoutMediaFileURI;
                break;
            case PLAYER_CREATION:
                vASTErrorCodes = VASTErrorCodes.MediaFileDisplayError;
                break;
            default:
                vASTErrorCodes = VASTErrorCodes.UndefinedError;
                break;
        }
        VideoUtil.a((Context) c(), new com.startapp.sdk.ads.video.a.b(P().h().o(), new VideoTrackingParams(m(), h(this.A), this.i, this.P), P().a(), this.A).a(vASTErrorCodes).a("error").a());
        if ((this.u ? this.l.d() : this.p) == 0) {
            com.startapp.sdk.adsbase.a.a((Context) c(), i(), m(), this.i, NotDisplayedReason.VIDEO_ERROR.toString());
            if (!this.u) {
                VideoUtil.b(c());
            } else if (!gVar.a().equals(VideoPlayerErrorType.BUFFERING_TIMEOUT)) {
                VideoUtil.b(c());
            }
        }
        if ((!H() || this.h) && P().c() != PostRollType.NONE) {
            a(VideoFinishedReason.SKIPPED);
            return;
        }
        am();
        p();
    }

    private void am() {
        Intent intent = new Intent("com.startapp.android.ShowFailedDisplayBroadcastListener");
        intent.putExtra("showFailedReason", NotDisplayedReason.VIDEO_ERROR);
        com.startapp.common.b.a((Context) c()).a(intent);
        this.s = true;
    }

    private boolean an() {
        return this.l != null && this.l.f();
    }

    private boolean ao() {
        return !this.u ? an() && this.C : this.J >= AdsCommonMetaData.a().I().k() && an() && this.C;
    }

    public final boolean r() {
        if (ah()) {
            B();
            return false;
        } else if (this.l == null) {
            return false;
        } else {
            int c = c(this.l.d() + 50);
            if (ap() && c == 0) {
                Q();
                return true;
            } else if (!P().d() && !this.I) {
                return true;
            } else {
                B();
                return false;
            }
        }
    }

    private boolean ap() {
        return this.i > 0 || P().e() || this.H;
    }

    private int h(int i) {
        if (this.B > 0) {
            return (i * 100) / this.B;
        }
        return 0;
    }

    public final void q() {
        if (!this.s) {
            super.q();
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str, boolean z2) {
        if (!TextUtils.isEmpty(P().j())) {
            str = P().j();
            z2 = true;
        }
        this.U = ah() ? ClickOrigin.POSTROLL : ClickOrigin.VIDEO;
        new StringBuilder("Video clicked from: ").append(this.U);
        if (this.U == ClickOrigin.VIDEO) {
            a(VideoFinishedReason.CLICKED);
        }
        ClickOrigin clickOrigin = this.U;
        new StringBuilder("Sending video clicked event with origin: ").append(clickOrigin.toString());
        ActionTrackingLink[] n2 = P().h().n();
        VideoClickedTrackingParams videoClickedTrackingParams = new VideoClickedTrackingParams(m(), h(this.A), this.i, clickOrigin, this.P);
        a(n2, videoClickedTrackingParams, this.A, "clicked");
        return super.a(str, z2);
    }

    /* access modifiers changed from: protected */
    public final void B() {
        if (!this.s) {
            if (ah() || this.m == null) {
                ar();
                super.B();
                return;
            }
            as();
        }
    }

    /* access modifiers changed from: protected */
    public final String D() {
        double currentTimeMillis = (double) (System.currentTimeMillis() - this.V);
        Double.isNaN(currentTimeMillis);
        return String.valueOf(currentTimeMillis / 1000.0d);
    }

    /* access modifiers changed from: protected */
    public final TrackingParams C() {
        return new VideoTrackingParams(m(), 0, this.i, this.P);
    }

    /* access modifiers changed from: protected */
    public final long E() {
        if (o() != null) {
            return TimeUnit.SECONDS.toMillis(o().longValue());
        }
        return TimeUnit.SECONDS.toMillis(MetaData.D().F());
    }

    /* access modifiers changed from: protected */
    public final void S() {
        super.A();
        a(P().h().c(), new VideoTrackingParams(m(), 0, this.i, this.P), 0, Events.AD_IMPRESSION);
        a(P().h().e(), new VideoTrackingParams(m(), 0, this.i, this.P), 0, "creativeView");
    }

    /* access modifiers changed from: protected */
    public final void e(int i) {
        if (this.E.get(Integer.valueOf(i)) == null) {
            if (this.R.containsKey(Integer.valueOf(i))) {
                List list = (List) this.R.get(Integer.valueOf(i));
                StringBuilder sb = new StringBuilder("Sending fraction progress event with fraction: ");
                sb.append(i);
                sb.append(", total: ");
                sb.append(list.size());
                a((VideoTrackingLink[]) list.toArray(new FractionTrackingLink[list.size()]), new VideoProgressTrackingParams(m(), i, this.i, this.P), d(i), "fraction");
                if (this.W != null) {
                    if (i == 25) {
                        this.W.a();
                    } else if (i == 50) {
                        this.W.b();
                    } else if (i == 75) {
                        this.W.c();
                    }
                }
            }
            this.E.put(Integer.valueOf(i), Boolean.TRUE);
        }
    }

    /* access modifiers changed from: protected */
    public final void f(int i) {
        if (this.F.get(Integer.valueOf(i)) == null) {
            if (this.S.containsKey(Integer.valueOf(i))) {
                List list = (List) this.S.get(Integer.valueOf(i));
                StringBuilder sb = new StringBuilder("Sending absolute progress event with video progress: ");
                sb.append(i);
                sb.append(", total: ");
                sb.append(list.size());
                a((VideoTrackingLink[]) list.toArray(new AbsoluteTrackingLink[list.size()]), new VideoProgressTrackingParams(m(), i, this.i, this.P), i, Constants.PATH_TYPE_ABSOLUTE);
            }
            this.F.put(Integer.valueOf(i), Boolean.TRUE);
        }
    }

    private void aq() {
        a(P().h().k(), new VideoTrackingParams(m(), h(this.A), this.i, this.P), this.A, "postrollImression");
    }

    /* access modifiers changed from: protected */
    public final void G() {
        a(P().h().m(), new VideoTrackingParams(m(), AdsCommonMetaData.a().I().d(), this.i, this.P), d(AdsCommonMetaData.a().I().d()), "rewarded");
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z2) {
        ActionTrackingLink[] actionTrackingLinkArr;
        StringBuilder sb = new StringBuilder("Sending sound ");
        sb.append(z2 ? "muted " : "unmuted ");
        sb.append("event");
        if (z2) {
            actionTrackingLinkArr = P().h().f();
        } else {
            actionTrackingLinkArr = P().h().d();
        }
        a(actionTrackingLinkArr, new VideoTrackingParams(m(), h(this.l.d()), this.i, this.P), this.l.d(), "sound");
        if (this.W != null) {
            this.W.a(z2 ? 0.0f : 1.0f);
        }
    }

    private void ar() {
        a(P().h().l(), new VideoTrackingParams(m(), h(this.A), this.i, this.P), this.A, "postrollClosed");
    }

    private void as() {
        a(P().h().j(), new VideoTrackingParams(m(), h(this.l.d()), this.i, this.P), this.l.d(), "closed");
    }

    private void a(VideoTrackingLink[] videoTrackingLinkArr, VideoTrackingParams videoTrackingParams, int i, String str) {
        VideoUtil.a((Context) c(), new com.startapp.sdk.ads.video.a.b(videoTrackingLinkArr, videoTrackingParams, P().a(), i).a(str).a());
    }

    public final void T() {
        this.K = true;
        if (this.C && this.D) {
            I();
        }
        if (ao()) {
            V();
        }
    }

    public final void U() {
        if (!ah()) {
            a(VideoFinishedReason.COMPLETE);
        }
        if (this.l != null) {
            this.l.c();
        }
    }

    public final void g(int i) {
        if (this.u && this.K && this.l != null && this.l.e() != 0) {
            StringBuilder sb = new StringBuilder("buffered percent = [");
            sb.append(i);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            this.J = i;
            int d = (this.l.d() * 100) / this.l.e();
            if (X()) {
                if (!this.L && ao()) {
                    V();
                } else if (this.J == 100 || this.J - d > AdsCommonMetaData.a().I().j()) {
                    StringBuilder sb2 = new StringBuilder("progressive video resumed, buffered percent: [");
                    sb2.append(Integer.toString(this.J));
                    sb2.append(RequestParameters.RIGHT_BRACKETS);
                    this.l.a();
                    K();
                }
            } else if (this.J < 100 && this.J - d <= AdsCommonMetaData.a().I().k()) {
                StringBuilder sb3 = new StringBuilder("progressive video paused, buffered percent: [");
                sb3.append(Integer.toString(this.J));
                sb3.append(RequestParameters.RIGHT_BRACKETS);
                this.l.b();
                W();
            }
        }
    }

    public final boolean b(g gVar) {
        this.K = false;
        if (!this.u || this.N > this.O || gVar.c() <= 0 || !gVar.b().equals(MediaErrorExtra.MEDIA_ERROR_IO.toString())) {
            a(gVar);
        } else {
            this.N++;
            W();
            this.l.a(P().b());
            this.l.a(gVar.c());
        }
        return true;
    }

    public final void a() {
        this.D = true;
        if (this.C && an()) {
            I();
        }
    }
}
