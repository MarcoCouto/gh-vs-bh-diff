package com.startapp.sdk.ads.video.a;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a {
    private List<String> a;
    private String b;

    public a(List<String> list, String str) {
        this.a = list;
        this.b = str;
    }

    public final List<String> a() {
        return this.a;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("[VideoEvent: tag=");
        sb.append(this.b);
        sb.append(", fullUrls=");
        sb.append(this.a.toString());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
