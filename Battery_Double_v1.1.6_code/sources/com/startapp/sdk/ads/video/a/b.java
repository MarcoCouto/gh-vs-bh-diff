package com.startapp.sdk.ads.video.a;

import android.text.TextUtils;
import com.startapp.common.b.a;
import com.startapp.sdk.ads.video.tracking.VideoTrackingLink;
import com.startapp.sdk.ads.video.tracking.VideoTrackingLink.TrackingSource;
import com.startapp.sdk.ads.video.tracking.VideoTrackingParams;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.adsbase.i.s;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public final class b {
    private VideoTrackingLink[] a;
    private VideoTrackingParams b;
    private String c;
    private int d;
    private String e = "";
    private VASTErrorCodes f;

    public b(VideoTrackingLink[] videoTrackingLinkArr, VideoTrackingParams videoTrackingParams, String str, int i) {
        this.a = videoTrackingLinkArr;
        this.b = videoTrackingParams;
        this.c = str;
        this.d = i;
    }

    public final b a(VASTErrorCodes vASTErrorCodes) {
        this.f = vASTErrorCodes;
        return this;
    }

    public final b a(String str) {
        this.e = str;
        return this;
    }

    public final a a() {
        VideoTrackingLink[] videoTrackingLinkArr;
        Object obj;
        String str;
        Object obj2 = null;
        if (!((this.a == null || this.b == null) ? false : true)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        VideoTrackingLink[] videoTrackingLinkArr2 = this.a;
        int length = videoTrackingLinkArr2.length;
        int i = 0;
        while (i < length) {
            VideoTrackingLink videoTrackingLink = videoTrackingLinkArr2[i];
            if (videoTrackingLink.b() == null) {
                new StringBuilder("Ignoring tracking link - tracking url is null: tracking link = ").append(videoTrackingLink);
            } else if (this.b.h() <= 0 || videoTrackingLink.c()) {
                StringBuilder sb = new StringBuilder();
                TrackingSource f2 = videoTrackingLink.f();
                if (f2 == null) {
                    f2 = s.b(videoTrackingLink.b()) ? TrackingSource.STARTAPP : TrackingSource.EXTERNAL;
                }
                VideoTrackingParams a2 = this.b.b(f2 == TrackingSource.STARTAPP).a(videoTrackingLink.c()).a(videoTrackingLink.e());
                String b2 = videoTrackingLink.b();
                String str2 = "[ASSETURI]";
                if (this.c != null) {
                    str = TextUtils.htmlEncode(this.c);
                } else {
                    str = "";
                }
                int i2 = this.d;
                videoTrackingLinkArr = videoTrackingLinkArr2;
                long convert = TimeUnit.SECONDS.convert((long) i2, TimeUnit.MILLISECONDS);
                Object[] objArr = {Long.valueOf(convert / 3600), Long.valueOf((convert % 3600) / 60), Long.valueOf(convert % 60), Long.valueOf((long) (i2 % 1000))};
                String format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format(new Date());
                int length2 = format.length() - 2;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(format.substring(0, length2));
                sb2.append(":");
                sb2.append(format.substring(length2));
                String replace = b2.replace(str2, str).replace("[CONTENTPLAYHEAD]", TextUtils.htmlEncode(String.format(Locale.US, "%02d:%02d:%02d.%03d", objArr))).replace("[CACHEBUSTING]", TextUtils.htmlEncode(String.valueOf(new SecureRandom().nextInt(90000000) + 10000000))).replace("[TIMESTAMP]", TextUtils.htmlEncode(sb2.toString()));
                if (this.f != null) {
                    replace = replace.replace("[ERRORCODE]", String.valueOf(this.f.a()));
                }
                sb.append(replace);
                sb.append(a2.a());
                if (a2.c()) {
                    obj = null;
                    sb.append(a.a(com.startapp.sdk.adsbase.a.a(b2, (String) null)));
                } else {
                    obj = null;
                }
                arrayList.add(sb.toString());
                i++;
                obj2 = obj;
                videoTrackingLinkArr2 = videoTrackingLinkArr;
            } else {
                new StringBuilder("Ignoring tracking link - external replay event: tracking link = ").append(videoTrackingLink);
            }
            obj = obj2;
            videoTrackingLinkArr = videoTrackingLinkArr2;
            i++;
            obj2 = obj;
            videoTrackingLinkArr2 = videoTrackingLinkArr;
        }
        return new a(arrayList, this.e);
    }
}
