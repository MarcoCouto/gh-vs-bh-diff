package com.startapp.sdk.ads.video;

import android.content.Context;
import android.util.Base64;
import com.startapp.common.b.d;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.cache.CachedVideoAd;
import com.startapp.sdk.adsbase.cache.a;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: StartAppSDK */
public final class e {
    private static e a = new e();
    private LinkedList<CachedVideoAd> b = new LinkedList<>();

    private e() {
    }

    private boolean a(int i) {
        boolean z;
        Iterator it = this.b.iterator();
        boolean z2 = false;
        while (it.hasNext() && this.b.size() > i) {
            CachedVideoAd cachedVideoAd = (CachedVideoAd) it.next();
            String b2 = cachedVideoAd.b();
            Iterator it2 = a.a().c().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                com.startapp.sdk.adsbase.cache.e eVar = (com.startapp.sdk.adsbase.cache.e) it2.next();
                if (eVar.b() instanceof VideoEnabledAd) {
                    VideoEnabledAd videoEnabledAd = (VideoEnabledAd) eVar.b();
                    if (!(videoEnabledAd.g() == null || videoEnabledAd.g().b() == null || !videoEnabledAd.g().b().equals(b2))) {
                        z = true;
                        break;
                    }
                }
            }
            if (!z) {
                it.remove();
                if (cachedVideoAd.b() != null) {
                    new File(cachedVideoAd.b()).delete();
                    StringBuilder sb = new StringBuilder("cachedVideoAds reached the maximum of ");
                    sb.append(i);
                    sb.append(" videos - removed ");
                    sb.append(cachedVideoAd.a());
                    sb.append(" Size = ");
                    sb.append(this.b.size());
                }
                z2 = true;
            }
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, CachedVideoAd cachedVideoAd) {
        if (this.b.contains(cachedVideoAd)) {
            this.b.remove(cachedVideoAd);
            StringBuilder sb = new StringBuilder("cachedVideoAds already contained ");
            sb.append(cachedVideoAd.a());
            sb.append(" - removed. Size = ");
            sb.append(this.b.size());
        }
        a(AdsCommonMetaData.a().I().b() - 1);
        this.b.add(cachedVideoAd);
        a(context);
        StringBuilder sb2 = new StringBuilder("Added ");
        sb2.append(cachedVideoAd.a());
        sb2.append(" to cachedVideoAds. Size = ");
        sb2.append(this.b.size());
    }

    private void a(Context context) {
        d.b(context, "CachedAds", this.b);
    }

    public static e a() {
        return a;
    }

    static /* synthetic */ void a(e eVar, final Context context, String str, final g.a aVar, final c.a aVar2) {
        String str2;
        if (eVar.b == null) {
            Class<LinkedList> cls = LinkedList.class;
            eVar.b = (LinkedList) d.a(context, "CachedAds");
            if (eVar.b == null) {
                eVar.b = new LinkedList<>();
            }
            if (eVar.a(AdsCommonMetaData.a().I().b())) {
                eVar.a(context);
            }
        }
        try {
            URL url = new URL(str);
            StringBuilder sb = new StringBuilder();
            sb.append(url.getHost());
            sb.append(url.getPath().replace("/", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR));
            String sb2 = sb.toString();
            try {
                String substring = sb2.substring(0, sb2.lastIndexOf(46));
                String substring2 = sb2.substring(sb2.lastIndexOf(46));
                StringBuilder sb3 = new StringBuilder();
                sb3.append(new String(Base64.encodeToString(MessageDigest.getInstance("MD5").digest(substring.getBytes()), 0)).replaceAll("[^a-zA-Z0-9]+", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR));
                sb3.append(substring2);
                str2 = sb3.toString();
            } catch (NoSuchAlgorithmException unused) {
                str2 = sb2;
            }
            final CachedVideoAd cachedVideoAd = new CachedVideoAd(str2);
            g gVar = new g(context, url, str2, new g.a() {
                public final void a(String str) {
                    if (aVar != null) {
                        aVar.a(str);
                    }
                    if (str != null) {
                        cachedVideoAd.a(System.currentTimeMillis());
                        cachedVideoAd.a(str);
                        e.this.a(context, cachedVideoAd);
                    }
                }
            }, new c.a() {
                public final void a(String str) {
                    if (aVar2 != null) {
                        aVar2.a(str);
                    }
                }
            });
            gVar.a();
        } catch (MalformedURLException unused2) {
            if (aVar != null) {
                aVar.a(null);
            }
        }
    }
}
