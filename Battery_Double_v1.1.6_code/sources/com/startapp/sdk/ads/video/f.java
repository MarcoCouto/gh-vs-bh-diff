package com.startapp.sdk.ads.video;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.webkit.JavascriptInterface;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.d.b;

/* compiled from: StartAppSDK */
public final class f extends b {
    private Runnable b = null;
    private Runnable c = null;
    private Runnable d = null;

    public f(Context context, Runnable runnable, Runnable runnable2, Runnable runnable3, Runnable runnable4, Runnable runnable5, TrackingParams trackingParams, boolean z) {
        super(context, runnable, runnable2, trackingParams);
        this.b = runnable3;
        this.c = runnable4;
        this.d = runnable5;
        this.a = z;
    }

    @JavascriptInterface
    public final void replayVideo() {
        if (this.b != null) {
            new Handler(Looper.getMainLooper()).post(this.b);
        }
    }

    @JavascriptInterface
    public final void skipVideo() {
        if (this.c != null) {
            new Handler(Looper.getMainLooper()).post(this.c);
        }
    }

    @JavascriptInterface
    public final void toggleSound() {
        if (this.d != null) {
            new Handler(Looper.getMainLooper()).post(this.d);
        }
    }
}
