package com.startapp.sdk.ads.video;

import android.content.Context;
import com.startapp.common.ThreadManager;
import com.startapp.common.ThreadManager.Priority;
import com.startapp.common.b.e;
import com.startapp.sdk.ads.video.VideoUtil.VideoEligibility;
import com.startapp.sdk.ads.video.tracking.VideoTrackingLink;
import com.startapp.sdk.ads.video.tracking.VideoTrackingParams;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.ads.video.vast.model.c;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.Ad.AdType;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.d.a;
import java.util.ArrayList;

/* compiled from: StartAppSDK */
public final class b extends a {
    private long i = System.currentTimeMillis();
    private volatile CacheKey j;
    private com.startapp.sdk.ads.video.vast.model.a k;
    private int l = 0;

    public b(Context context, Ad ad, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        com.startapp.sdk.ads.video.vast.model.a aVar;
        super(context, ad, adPreferences, bVar, Placement.INAPP_OVERLAY, true);
        if (AdsCommonMetaData.a().I().r() == 0) {
            aVar = new com.startapp.sdk.ads.video.vast.model.a(context);
        } else {
            aVar = new com.startapp.sdk.ads.video.vast.model.b(context, AdsCommonMetaData.a().I().s());
        }
        this.k = aVar;
    }

    /* access modifiers changed from: protected */
    public final boolean a(Object obj) {
        boolean z;
        e.a aVar = (e.a) obj;
        String str = null;
        boolean z2 = false;
        boolean z3 = true;
        if (aVar == null || !aVar.b().toLowerCase().contains("json")) {
            if (aVar != null) {
                str = aVar.a();
            }
            if (AdsCommonMetaData.a().I().h()) {
                if (s.a(str, "@videoJson@", "@videoJson@") == null) {
                    z3 = false;
                }
                if (z3) {
                    b(false);
                }
            }
            return super.a(obj);
        }
        if (AdsCommonMetaData.a().I().h() && !this.h.i()) {
            b(true);
        }
        try {
            d dVar = (d) com.startapp.common.parser.b.a(aVar.a(), d.class);
            if (!(dVar == null || dVar.getVastTag() == null)) {
                com.startapp.sdk.ads.video.vast.a.a aVar2 = new com.startapp.sdk.ads.video.vast.a.a(AdsCommonMetaData.a().I().n(), AdsCommonMetaData.a().I().o());
                VASTErrorCodes a = aVar2.a(this.a, dVar.getVastTag(), this.k);
                c a2 = aVar2.a();
                VideoEnabledAd videoEnabledAd = (VideoEnabledAd) this.b;
                if (this.b.getType() != AdType.REWARDED_VIDEO) {
                    z2 = true;
                }
                videoEnabledAd.a(a2, z2);
                if (dVar.getTtlSec() != null) {
                    ((VideoEnabledAd) this.b).c(dVar.getTtlSec());
                }
                if (a == VASTErrorCodes.ErrorNone) {
                    a(VASTErrorCodes.SAProcessSuccess);
                    aVar.a(dVar.getAdmTag());
                    aVar.b(WebRequest.CONTENT_TYPE_HTML);
                    z = super.a((Object) aVar);
                } else {
                    a(a);
                    if (dVar.getCampaignId() != null) {
                        this.g.add(dVar.getCampaignId());
                    }
                    this.l++;
                    ((VideoEnabledAd) this.b).h();
                    if (System.currentTimeMillis() - this.i < ((long) AdsCommonMetaData.a().I().p()) && this.l <= AdsCommonMetaData.a().I().q()) {
                        z = d().booleanValue();
                    }
                }
                return z;
            }
            z = a((Throwable) null);
            return z;
        } catch (Exception e) {
            return a((Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(final Boolean bool) {
        super.a(bool);
        if (!bool.booleanValue() || !g()) {
            a(bool.booleanValue());
            return;
        }
        if (AdsCommonMetaData.a().I().i()) {
            super.b(bool);
        }
        b().a(this.c.isVideoMuted());
        AnonymousClass1 r6 = new g.a() {
            public final void a(String str) {
                if (str != null) {
                    if (!str.equals("downloadInterrupted")) {
                        b.super.b(bool);
                        b.this.b().a(str);
                    }
                    b.this.a(bool.booleanValue());
                    return;
                }
                b.this.a(false);
                b.this.d.b(b.this.b);
                b.this.a(VASTErrorCodes.FileNotFound);
            }
        };
        AnonymousClass2 r7 = new c.a() {
            public final void a(String str) {
                b.this.b().a(str);
            }
        };
        e a = e.a();
        Context applicationContext = this.a.getApplicationContext();
        String a2 = b().a();
        Priority priority = Priority.HIGH;
        AnonymousClass1 r2 = new Runnable(applicationContext, a2, r6, r7) {
            private /* synthetic */ Context a;
            private /* synthetic */ String b;
            private /* synthetic */ g.a c;
            private /* synthetic */ c.a d;

            {
                this.a = r2;
                this.b = r3;
                this.c = r4;
                this.d = r5;
            }

            public final void run() {
                e.a(e.this, this.a, this.b, this.c, this.d);
            }
        };
        ThreadManager.a(priority, (Runnable) r2);
    }

    /* access modifiers changed from: protected */
    public final boolean a(GetAdRequest getAdRequest) {
        if (!super.a(getAdRequest)) {
            return false;
        }
        if (getAdRequest.j()) {
            VideoEligibility a = VideoUtil.a(this.a);
            if (a != VideoEligibility.ELIGIBLE) {
                this.f = a.a();
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        GetAdRequest b = b((GetAdRequest) new a());
        if (b != null) {
            b.a(this.a);
        }
        return b;
    }

    private boolean g() {
        return b() != null;
    }

    /* access modifiers changed from: protected */
    public final void b(Boolean bool) {
        if (!g()) {
            super.b(bool);
        }
    }

    /* access modifiers changed from: 0000 */
    public final VideoAdDetails b() {
        return ((VideoEnabledAd) this.b).g();
    }

    private void b(boolean z) {
        AdPreferences adPreferences;
        if ((this.b.getType() != AdType.REWARDED_VIDEO && this.b.getType() != AdType.VIDEO) || z) {
            if (this.c == null) {
                adPreferences = new AdPreferences();
            } else {
                adPreferences = new AdPreferences(this.c);
            }
            AdPreferences adPreferences2 = adPreferences;
            adPreferences2.setType((this.b.getType() == AdType.REWARDED_VIDEO || this.b.getType() == AdType.VIDEO) ? AdType.VIDEO_NO_VAST : AdType.NON_VIDEO);
            CacheKey a = com.startapp.sdk.adsbase.cache.a.a().a(this.a, (StartAppAd) null, this.e, adPreferences2, (com.startapp.sdk.adsbase.adlisteners.b) null);
            if (z) {
                this.j = a;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(VASTErrorCodes vASTErrorCodes) {
        VideoTrackingLink[] videoTrackingLinkArr = null;
        try {
            if (!(b() == null || b().h() == null)) {
                videoTrackingLinkArr = b().h().o();
            }
            if (videoTrackingLinkArr != null && videoTrackingLinkArr.length > 0) {
                if (vASTErrorCodes == VASTErrorCodes.SAShowBeforeVast || vASTErrorCodes == VASTErrorCodes.SAProcessSuccess) {
                    ArrayList arrayList = new ArrayList();
                    for (VideoTrackingLink videoTrackingLink : videoTrackingLinkArr) {
                        if (s.b(videoTrackingLink.b())) {
                            arrayList.add(videoTrackingLink);
                        }
                    }
                    if (arrayList.size() > 0) {
                        videoTrackingLinkArr = (VideoTrackingLink[]) arrayList.toArray(new VideoTrackingLink[arrayList.size()]);
                    } else {
                        return;
                    }
                }
                VideoUtil.a(this.a, new com.startapp.sdk.ads.video.a.b(videoTrackingLinkArr, new VideoTrackingParams("", 0, 0, "1"), b().a(), 0).a("error").a(vASTErrorCodes).a());
            }
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.a);
        }
    }

    private boolean a(Throwable th) {
        new com.startapp.sdk.adsbase.infoevents.e(th).a(this.a);
        f a = com.startapp.sdk.adsbase.cache.a.a().a(this.j);
        if (a instanceof HtmlAd) {
            e.a aVar = new e.a();
            aVar.b(WebRequest.CONTENT_TYPE_HTML);
            aVar.a(((HtmlAd) a).j());
            return super.a((Object) aVar);
        }
        this.b.setErrorMessage(this.f);
        return false;
    }
}
