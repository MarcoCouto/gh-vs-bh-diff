package com.startapp.sdk.ads.video;

import android.content.Context;
import android.util.Pair;
import com.startapp.common.SDKException;
import com.startapp.sdk.ads.video.VideoUtil.VideoEligibility;
import com.startapp.sdk.adsbase.Ad.AdType;
import com.startapp.sdk.adsbase.i.j;
import com.startapp.sdk.adsbase.i.l;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.model.GetAdRequest;

/* compiled from: StartAppSDK */
public final class a extends GetAdRequest {
    private VideoRequestType c;
    private VideoRequestMode d = VideoRequestMode.INTERSTITIAL;

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    public final void a(Context context, AdPreferences adPreferences, Placement placement, Pair<String, String> pair) {
        super.a(context, adPreferences, placement, pair);
        if (k() != null) {
            if (k() != AdType.NON_VIDEO) {
                if (k() == AdType.VIDEO_NO_VAST) {
                    this.c = VideoRequestType.FORCED_NONVAST;
                } else if (j()) {
                    this.c = VideoRequestType.FORCED;
                }
                if (k() == AdType.REWARDED_VIDEO) {
                    this.d = VideoRequestMode.REWARDED;
                }
                if (k() == AdType.VIDEO) {
                    this.d = VideoRequestMode.INTERSTITIAL;
                    return;
                }
                return;
            }
        } else if (VideoUtil.a(context) == VideoEligibility.ELIGIBLE) {
            if (!s.a(2)) {
                this.c = VideoRequestType.FORCED;
            } else {
                this.c = VideoRequestType.ENABLED;
            }
            if (k() == AdType.REWARDED_VIDEO) {
            }
            if (k() == AdType.VIDEO) {
            }
        }
        this.c = VideoRequestType.DISABLED;
        if (k() == AdType.REWARDED_VIDEO) {
        }
        if (k() == AdType.VIDEO) {
        }
    }

    public final l a() throws SDKException {
        l a = super.a();
        if (a == null) {
            a = new j();
        }
        a.a("video", this.c, false);
        a.a("videoMode", this.d, false);
        return a;
    }
}
