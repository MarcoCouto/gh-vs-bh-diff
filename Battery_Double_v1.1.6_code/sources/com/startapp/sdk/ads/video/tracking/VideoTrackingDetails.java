package com.startapp.sdk.ads.video.tracking;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.parser.d;
import com.startapp.sdk.ads.video.vast.model.VASTEventType;
import com.startapp.sdk.ads.video.vast.model.c;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/* compiled from: StartAppSDK */
public class VideoTrackingDetails implements Serializable {
    private static final long serialVersionUID = 1;
    @d(b = AbsoluteTrackingLink.class)
    private AbsoluteTrackingLink[] absoluteTrackingUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] creativeViewUrls;
    @d(b = FractionTrackingLink.class)
    private FractionTrackingLink[] fractionTrackingUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] impressionUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] inlineErrorTrackingUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] soundMuteUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] soundUnmuteUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoClickTrackingUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoClosedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPausedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPostRollClosedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPostRollImpressionUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoResumedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoRewardedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoSkippedUrls;

    public VideoTrackingDetails() {
    }

    public VideoTrackingDetails(c cVar) {
        if (cVar != null) {
            HashMap a = cVar.a();
            ArrayList arrayList = new ArrayList();
            a((List) a.get(VASTEventType.start), 0, arrayList);
            a((List) a.get(VASTEventType.firstQuartile), 25, arrayList);
            a((List) a.get(VASTEventType.midpoint), 50, arrayList);
            a((List) a.get(VASTEventType.thirdQuartile), 75, arrayList);
            a((List) a.get(VASTEventType.complete), 100, arrayList);
            this.fractionTrackingUrls = (FractionTrackingLink[]) arrayList.toArray(new FractionTrackingLink[arrayList.size()]);
            this.impressionUrls = b(cVar.c());
            this.soundMuteUrls = a((List) a.get(VASTEventType.mute));
            this.soundUnmuteUrls = a((List) a.get(VASTEventType.unmute));
            this.videoPausedUrls = a((List) a.get(VASTEventType.pause));
            this.videoResumedUrls = a((List) a.get(VASTEventType.resume));
            this.videoSkippedUrls = a((List) a.get(VASTEventType.skip));
            this.videoPausedUrls = a((List) a.get(VASTEventType.pause));
            this.videoClosedUrls = a((List) a.get(VASTEventType.close));
            this.inlineErrorTrackingUrls = b(cVar.d());
            this.videoClickTrackingUrls = b(cVar.b().b());
            this.absoluteTrackingUrls = c((List) a.get(VASTEventType.progress));
        }
    }

    public final FractionTrackingLink[] a() {
        return this.fractionTrackingUrls;
    }

    public final AbsoluteTrackingLink[] b() {
        return this.absoluteTrackingUrls;
    }

    public final ActionTrackingLink[] c() {
        return this.impressionUrls;
    }

    public final ActionTrackingLink[] d() {
        return this.soundUnmuteUrls;
    }

    public final ActionTrackingLink[] e() {
        return this.creativeViewUrls;
    }

    public final ActionTrackingLink[] f() {
        return this.soundMuteUrls;
    }

    public final ActionTrackingLink[] g() {
        return this.videoPausedUrls;
    }

    public final ActionTrackingLink[] h() {
        return this.videoResumedUrls;
    }

    public final ActionTrackingLink[] i() {
        return this.videoSkippedUrls;
    }

    public final ActionTrackingLink[] j() {
        return this.videoClosedUrls;
    }

    public final ActionTrackingLink[] k() {
        return this.videoPostRollImpressionUrls;
    }

    public final ActionTrackingLink[] l() {
        return this.videoPostRollClosedUrls;
    }

    public final ActionTrackingLink[] m() {
        return this.videoRewardedUrls;
    }

    public final ActionTrackingLink[] n() {
        return this.videoClickTrackingUrls;
    }

    public final ActionTrackingLink[] o() {
        return this.inlineErrorTrackingUrls;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VideoTrackingDetails [fractionTrackingUrls=");
        sb.append(a((VideoTrackingLink[]) this.fractionTrackingUrls));
        sb.append(", absoluteTrackingUrls=");
        sb.append(a((VideoTrackingLink[]) this.absoluteTrackingUrls));
        sb.append(", impressionUrls=");
        sb.append(a((VideoTrackingLink[]) this.impressionUrls));
        sb.append(", creativeViewUrls=");
        sb.append(a((VideoTrackingLink[]) this.creativeViewUrls));
        sb.append(", soundMuteUrls=");
        sb.append(a((VideoTrackingLink[]) this.soundMuteUrls));
        sb.append(", soundUnmuteUrls=");
        sb.append(a((VideoTrackingLink[]) this.soundUnmuteUrls));
        sb.append(", videoPausedUrls=");
        sb.append(a((VideoTrackingLink[]) this.videoPausedUrls));
        sb.append(", videoResumedUrls=");
        sb.append(a((VideoTrackingLink[]) this.videoResumedUrls));
        sb.append(", videoSkippedUrls=");
        sb.append(a((VideoTrackingLink[]) this.videoSkippedUrls));
        sb.append(", videoClosedUrls=");
        sb.append(a((VideoTrackingLink[]) this.videoClosedUrls));
        sb.append(", videoPostRollImpressionUrls=");
        sb.append(a((VideoTrackingLink[]) this.videoPostRollImpressionUrls));
        sb.append(", videoPostRollClosedUrls=");
        sb.append(a((VideoTrackingLink[]) this.videoPostRollClosedUrls));
        sb.append(", videoRewardedUrls=");
        sb.append(a((VideoTrackingLink[]) this.videoRewardedUrls));
        sb.append(", videoClickTrackingUrls=");
        sb.append(a((VideoTrackingLink[]) this.videoClickTrackingUrls));
        sb.append(", inlineErrorTrackingUrls=");
        sb.append(a((VideoTrackingLink[]) this.inlineErrorTrackingUrls));
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    private static String a(VideoTrackingLink[] videoTrackingLinkArr) {
        return videoTrackingLinkArr != null ? Arrays.toString(videoTrackingLinkArr) : "";
    }

    private static void a(List<com.startapp.sdk.ads.video.vast.model.a.c> list, int i, List<FractionTrackingLink> list2) {
        if (list != null) {
            for (com.startapp.sdk.ads.video.vast.model.a.c cVar : list) {
                FractionTrackingLink fractionTrackingLink = new FractionTrackingLink();
                fractionTrackingLink.a(cVar.a());
                fractionTrackingLink.a(i);
                fractionTrackingLink.d();
                fractionTrackingLink.b("");
                list2.add(fractionTrackingLink);
            }
        }
    }

    private static ActionTrackingLink[] a(List<com.startapp.sdk.ads.video.vast.model.a.c> list) {
        if (list == null) {
            return new ActionTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (com.startapp.sdk.ads.video.vast.model.a.c cVar : list) {
            ActionTrackingLink actionTrackingLink = new ActionTrackingLink();
            actionTrackingLink.a(cVar.a());
            actionTrackingLink.d();
            actionTrackingLink.b("");
            arrayList.add(actionTrackingLink);
        }
        return (ActionTrackingLink[]) arrayList.toArray(new ActionTrackingLink[arrayList.size()]);
    }

    private static ActionTrackingLink[] b(List<String> list) {
        if (list == null) {
            return new ActionTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (String str : list) {
            ActionTrackingLink actionTrackingLink = new ActionTrackingLink();
            actionTrackingLink.a(str);
            actionTrackingLink.d();
            actionTrackingLink.b("");
            arrayList.add(actionTrackingLink);
        }
        return (ActionTrackingLink[]) arrayList.toArray(new ActionTrackingLink[arrayList.size()]);
    }

    private static AbsoluteTrackingLink[] c(List<com.startapp.sdk.ads.video.vast.model.a.c> list) {
        if (list == null) {
            return new AbsoluteTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (com.startapp.sdk.ads.video.vast.model.a.c cVar : list) {
            AbsoluteTrackingLink absoluteTrackingLink = new AbsoluteTrackingLink();
            absoluteTrackingLink.a(cVar.a());
            if (cVar.b() != -1) {
                absoluteTrackingLink.a(cVar.b());
            }
            absoluteTrackingLink.d();
            absoluteTrackingLink.b("");
            arrayList.add(absoluteTrackingLink);
        }
        return (AbsoluteTrackingLink[]) arrayList.toArray(new AbsoluteTrackingLink[arrayList.size()]);
    }
}
