package com.startapp.sdk.ads.video.tracking;

import com.startapp.common.parser.c;
import java.io.Serializable;

@c(c = true)
/* compiled from: StartAppSDK */
public class FractionTrackingLink extends VideoTrackingLink implements Serializable {
    private static final long serialVersionUID = 1;
    private int fraction;

    public final int a() {
        return this.fraction;
    }

    public final void a(int i) {
        this.fraction = i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(", fraction=");
        sb.append(this.fraction);
        return sb.toString();
    }
}
