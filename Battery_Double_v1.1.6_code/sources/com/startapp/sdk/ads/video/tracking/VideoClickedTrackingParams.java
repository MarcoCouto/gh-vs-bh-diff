package com.startapp.sdk.ads.video.tracking;

/* compiled from: StartAppSDK */
public class VideoClickedTrackingParams extends VideoTrackingParams {
    private static final long serialVersionUID = 1;
    private ClickOrigin clickOrigin;

    /* compiled from: StartAppSDK */
    public enum ClickOrigin {
        POSTROLL,
        VIDEO
    }

    public VideoClickedTrackingParams(String str, int i, int i2, ClickOrigin clickOrigin2, String str2) {
        super(str, i, i2, str2);
        this.clickOrigin = clickOrigin2;
    }

    public final String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(b());
        StringBuilder sb2 = new StringBuilder("&co=");
        sb2.append(this.clickOrigin.toString());
        sb.append(sb2.toString());
        sb.append(e());
        return b(sb.toString());
    }
}
