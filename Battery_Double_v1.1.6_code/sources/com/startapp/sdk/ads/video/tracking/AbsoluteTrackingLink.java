package com.startapp.sdk.ads.video.tracking;

import com.startapp.common.parser.c;
import java.io.Serializable;

@c(c = true)
/* compiled from: StartAppSDK */
public class AbsoluteTrackingLink extends VideoTrackingLink implements Serializable {
    private static final long serialVersionUID = 1;
    private int videoOffsetMillis;

    public final int a() {
        return this.videoOffsetMillis;
    }

    public final void a(int i) {
        this.videoOffsetMillis = i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(", videoOffsetMillis=");
        sb.append(this.videoOffsetMillis);
        return sb.toString();
    }
}
