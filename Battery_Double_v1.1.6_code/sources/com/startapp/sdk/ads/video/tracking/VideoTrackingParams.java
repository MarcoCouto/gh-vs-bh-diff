package com.startapp.sdk.ads.video.tracking;

import com.startapp.sdk.adsbase.commontracking.TrackingParams;

/* compiled from: StartAppSDK */
public class VideoTrackingParams extends TrackingParams {
    private static final long serialVersionUID = 1;
    private int completed;
    protected boolean internalParamsIndicator;
    private String replayParameter;
    private boolean shouldAppendOffset;
    private String videoPlayingMode;

    public VideoTrackingParams(String str, int i, int i2, String str2) {
        super(str);
        a(i2);
        this.completed = i;
        this.videoPlayingMode = str2;
    }

    public final boolean c() {
        return this.internalParamsIndicator;
    }

    public final VideoTrackingParams a(boolean z) {
        this.shouldAppendOffset = z;
        return this;
    }

    public final VideoTrackingParams a(String str) {
        this.replayParameter = str;
        return this;
    }

    public final VideoTrackingParams b(boolean z) {
        this.internalParamsIndicator = z;
        return this;
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(b());
        sb.append(e());
        return b(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final String d() {
        if (!this.shouldAppendOffset) {
            return "";
        }
        if (this.replayParameter != null) {
            return this.replayParameter.replace("%startapp_replay_count%", new Integer(h()).toString());
        }
        return super.d();
    }

    /* access modifiers changed from: protected */
    public String b() {
        StringBuilder sb = new StringBuilder("&cp=");
        sb.append(this.completed);
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final String e() {
        StringBuilder sb = new StringBuilder("&vpm=");
        sb.append(this.videoPlayingMode);
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final String b(String str) {
        if (!this.internalParamsIndicator) {
            return d();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(super.a());
        sb.append(str);
        return sb.toString();
    }
}
