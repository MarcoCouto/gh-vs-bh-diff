package com.startapp.sdk.ads.video.tracking;

/* compiled from: StartAppSDK */
public class VideoPausedTrackingParams extends VideoTrackingParams {
    private static final long serialVersionUID = 1;
    private int pauseNum;
    private PauseOrigin pauseOrigin;

    /* compiled from: StartAppSDK */
    public enum PauseOrigin {
        INAPP,
        EXTERNAL
    }

    public VideoPausedTrackingParams(String str, int i, int i2, int i3, PauseOrigin pauseOrigin2, String str2) {
        super(str, i, i2, str2);
        this.pauseNum = i3;
        this.pauseOrigin = pauseOrigin2;
    }

    public final String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(b());
        StringBuilder sb2 = new StringBuilder("&po=");
        sb2.append(this.pauseOrigin.toString());
        sb.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder("&pn=");
        sb3.append(this.pauseNum);
        sb.append(sb3.toString());
        sb.append(e());
        return b(sb.toString());
    }
}
