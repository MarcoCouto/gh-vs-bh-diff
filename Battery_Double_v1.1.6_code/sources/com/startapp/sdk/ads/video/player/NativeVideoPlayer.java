package com.startapp.sdk.ads.video.player;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.widget.VideoView;
import com.startapp.sdk.ads.video.c;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.VideoPlayerErrorType;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.g;
import com.startapp.sdk.adsbase.a;

/* compiled from: StartAppSDK */
public final class NativeVideoPlayer extends a implements OnCompletionListener, OnErrorListener, OnPreparedListener {
    private MediaPlayer f;
    private VideoView g;

    /* compiled from: StartAppSDK */
    public enum MediaErrorExtra {
        MEDIA_ERROR_IO,
        MEDIA_ERROR_MALFORMED,
        MEDIA_ERROR_UNSUPPORTED,
        MEDIA_ERROR_TIMED_OUT;

        public static MediaErrorExtra a(int i) {
            if (i == -1010) {
                return MEDIA_ERROR_UNSUPPORTED;
            }
            if (i == -1007) {
                return MEDIA_ERROR_MALFORMED;
            }
            if (i == -1004) {
                return MEDIA_ERROR_IO;
            }
            if (i != -110) {
                return MEDIA_ERROR_IO;
            }
            return MEDIA_ERROR_TIMED_OUT;
        }
    }

    /* compiled from: StartAppSDK */
    public enum MediaErrorType {
        MEDIA_ERROR_UNKNOWN,
        MEDIA_ERROR_SERVER_DIED;

        public static MediaErrorType a(int i) {
            if (i == 100) {
                return MEDIA_ERROR_SERVER_DIED;
            }
            return MEDIA_ERROR_UNKNOWN;
        }
    }

    public NativeVideoPlayer(VideoView videoView) {
        this.g = videoView;
        this.g.setOnPreparedListener(this);
        this.g.setOnCompletionListener(this);
        this.g.setOnErrorListener(this);
    }

    public final void a() {
        this.g.start();
    }

    public final void a(int i) {
        StringBuilder sb = new StringBuilder("seekTo(");
        sb.append(i);
        sb.append(")");
        this.g.seekTo(i);
    }

    public final void b() {
        this.g.pause();
    }

    public final void c() {
        this.g.stopPlayback();
    }

    public final void a(boolean z) {
        StringBuilder sb = new StringBuilder("setMute(");
        sb.append(z);
        sb.append(")");
        if (this.f != null) {
            if (z) {
                this.f.setVolume(0.0f, 0.0f);
                return;
            }
            this.f.setVolume(1.0f, 1.0f);
        }
    }

    public final int d() {
        return this.g.getCurrentPosition();
    }

    public final int e() {
        return this.g.getDuration();
    }

    public final boolean f() {
        return this.f != null;
    }

    public final void a(String str) {
        StringBuilder sb = new StringBuilder("setVideoLocation(");
        sb.append(str);
        sb.append(")");
        super.a(str);
        this.g.setVideoPath(this.a);
    }

    public final void g() {
        if (this.f != null) {
            this.f = null;
        }
        c.a().a((VideoPlayerInterface.c) null);
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        this.f = mediaPlayer;
        if (this.b != null) {
            this.b.T();
        }
        if (!a.c(this.a) || this.f == null) {
            if (!a.c(this.a)) {
                c.a().a(this.e);
            }
            return;
        }
        this.f.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
            public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
                if (NativeVideoPlayer.this.e != null) {
                    NativeVideoPlayer.this.e.g(i);
                }
            }
        });
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (this.d != null) {
            this.d.U();
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        StringBuilder sb = new StringBuilder("onError(");
        sb.append(i);
        sb.append(", ");
        sb.append(i2);
        sb.append(")");
        if (this.c == null) {
            return false;
        }
        return this.c.b(new g(MediaErrorType.a(i) == MediaErrorType.MEDIA_ERROR_SERVER_DIED ? VideoPlayerErrorType.SERVER_DIED : VideoPlayerErrorType.UNKNOWN, MediaErrorExtra.a(i2).toString(), mediaPlayer != null ? mediaPlayer.getCurrentPosition() : -1));
    }
}
