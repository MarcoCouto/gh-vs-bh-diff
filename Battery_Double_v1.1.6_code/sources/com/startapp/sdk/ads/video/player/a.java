package com.startapp.sdk.ads.video.player;

import com.startapp.sdk.ads.video.player.VideoPlayerInterface.b;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.c;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.d;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.e;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface.f;

/* compiled from: StartAppSDK */
public abstract class a implements VideoPlayerInterface {
    protected String a;
    protected f b;
    protected e c;
    protected d d;
    protected c e;
    private b f;
    private com.startapp.sdk.ads.video.player.VideoPlayerInterface.a g;

    public void a(String str) {
        this.a = str;
    }

    public final void a(f fVar) {
        this.b = fVar;
    }

    public final void a(e eVar) {
        this.c = eVar;
    }

    public final void a(d dVar) {
        this.d = dVar;
    }

    public final void a(b bVar) {
        this.f = bVar;
    }

    public final void a(c cVar) {
        this.e = cVar;
    }

    public final void a(com.startapp.sdk.ads.video.player.VideoPlayerInterface.a aVar) {
        this.g = aVar;
    }
}
