package com.startapp.sdk.ads.video;

import android.content.Context;
import com.startapp.sdk.ads.interstitials.InterstitialAd;
import com.startapp.sdk.ads.splash.SplashConfig.Orientation;
import com.startapp.sdk.ads.video.vast.model.c;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;

/* compiled from: StartAppSDK */
public class VideoEnabledAd extends InterstitialAd {
    private static final long serialVersionUID = 1;
    private VideoAdDetails videoAdDetails = null;

    public VideoEnabledAd(Context context) {
        super(context, Placement.INAPP_OVERLAY);
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        new b(this.a, this, adPreferences, bVar).c();
    }

    public final void b(String str) {
        super.b(str);
        String str2 = "@videoJson@";
        String a = s.a(str, str2, str2);
        if (a != null) {
            this.videoAdDetails = (VideoAdDetails) com.startapp.common.parser.b.a(a, VideoAdDetails.class);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return this.videoAdDetails != null;
    }

    public final VideoAdDetails g() {
        return this.videoAdDetails;
    }

    public final void a(c cVar, boolean z) {
        if (cVar != null) {
            this.videoAdDetails = new VideoAdDetails(cVar, z);
            com.startapp.sdk.ads.video.vast.model.a.b f = cVar.f();
            if (f != null) {
                if (f.d().intValue() > f.e().intValue()) {
                    a(Orientation.LANDSCAPE);
                    return;
                }
                a(Orientation.PORTRAIT);
            }
        }
    }

    public final void h() {
        this.videoAdDetails = null;
    }
}
