package com.startapp.sdk.ads.nativead;

import android.os.Handler;
import android.os.Looper;

/* compiled from: StartAppSDK */
final class b implements NativeAdDisplayListener {
    /* access modifiers changed from: private */
    public NativeAdDisplayListener a;

    b(NativeAdDisplayListener nativeAdDisplayListener) {
        this.a = nativeAdDisplayListener;
    }

    public final void adHidden(final NativeAdInterface nativeAdInterface) {
        if (this.a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    b.this.a.adHidden(nativeAdInterface);
                }
            });
        }
    }

    public final void adDisplayed(final NativeAdInterface nativeAdInterface) {
        if (this.a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    b.this.a.adDisplayed(nativeAdInterface);
                }
            });
        }
    }

    public final void adClicked(final NativeAdInterface nativeAdInterface) {
        if (this.a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    b.this.a.adClicked(nativeAdInterface);
                }
            });
        }
    }

    public final void adNotDisplayed(final NativeAdInterface nativeAdInterface) {
        if (this.a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    b.this.a.adNotDisplayed(nativeAdInterface);
                }
            });
        }
    }
}
