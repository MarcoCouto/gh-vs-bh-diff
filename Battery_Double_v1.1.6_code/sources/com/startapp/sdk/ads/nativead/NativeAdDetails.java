package com.startapp.sdk.ads.nativead;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Handler;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnClickListener;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.a.C0063a;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.nativead.StartAppNativeAd.CampaignAction;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* compiled from: StartAppSDK */
public class NativeAdDetails implements NativeAdInterface {
    private AdDetails a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public Bitmap c;
    /* access modifiers changed from: private */
    public Bitmap d;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public a g;
    private String h;
    private com.startapp.sdk.adsbase.j.b i;
    private WeakReference<View> j = new WeakReference<>(null);
    /* access modifiers changed from: private */
    public OnAttachStateChangeListener k;
    /* access modifiers changed from: private */
    public b l;

    /* compiled from: StartAppSDK */
    public interface a {
        void onNativeAdDetailsLoaded(int i);
    }

    /* compiled from: StartAppSDK */
    class b implements OnClickListener {
        private b() {
        }

        /* synthetic */ b(NativeAdDetails nativeAdDetails, byte b) {
            this();
        }

        public final void onClick(View view) {
            NativeAdDetails.a(NativeAdDetails.this, view);
        }
    }

    public NativeAdDetails(AdDetails adDetails, NativeAdPreferences nativeAdPreferences, int i2, a aVar) {
        StringBuilder sb = new StringBuilder("Initializiang SingleAd [");
        sb.append(i2);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        this.a = adDetails;
        this.b = i2;
        this.g = aVar;
        if (nativeAdPreferences.isAutoBitmapDownload()) {
            new com.startapp.common.a(getImageUrl(), new C0063a() {
                public final void a(Bitmap bitmap, int i) {
                    NativeAdDetails.this.c = bitmap;
                    new com.startapp.common.a(NativeAdDetails.this.getSecondaryImageUrl(), new C0063a() {
                        public final void a(Bitmap bitmap, int i) {
                            NativeAdDetails.this.d = bitmap;
                            NativeAdDetails.this.b();
                        }
                    }, i).a();
                }
            }, i2).a();
        } else {
            b();
        }
    }

    public String toString() {
        String description = getDescription();
        if (description != null) {
            description = description.substring(0, Math.min(30, description.length()));
        }
        StringBuilder sb = new StringBuilder("         Title: [");
        sb.append(getTitle());
        sb.append("]\n         Description: [");
        sb.append(description);
        sb.append("]...\n         Rating: [");
        sb.append(getRating());
        sb.append("]\n         Installs: [");
        sb.append(getInstalls());
        sb.append("]\n         Category: [");
        sb.append(getCategory());
        sb.append("]\n         PackageName: [");
        sb.append(getPackacgeName());
        sb.append("]\n         CampaginAction: [");
        sb.append(getCampaignAction());
        sb.append("]\n");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        unregisterView();
    }

    public int getIdentifier() {
        return this.b;
    }

    public boolean isBelowMinCPM() {
        return this.a != null && this.a.i();
    }

    /* access modifiers changed from: private */
    public void b() {
        new Handler().post(new Runnable() {
            public final void run() {
                StringBuilder sb = new StringBuilder("SingleAd [");
                sb.append(NativeAdDetails.this.b);
                sb.append("] Loaded");
                if (NativeAdDetails.this.g != null) {
                    NativeAdDetails.this.g.onNativeAdDetailsLoaded(NativeAdDetails.this.b);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.h = str;
    }

    public String getTitle() {
        return this.a != null ? this.a.f() : "";
    }

    public String getDescription() {
        return this.a != null ? this.a.g() : "";
    }

    public float getRating() {
        if (this.a != null) {
            return this.a.k();
        }
        return 5.0f;
    }

    public String getImageUrl() {
        return this.a != null ? this.a.h() : "";
    }

    public String getSecondaryImageUrl() {
        return this.a != null ? this.a.j() : "";
    }

    public Bitmap getImageBitmap() {
        return this.c;
    }

    public Bitmap getSecondaryImageBitmap() {
        return this.d;
    }

    public String getInstalls() {
        return this.a != null ? this.a.s() : "";
    }

    public String getCategory() {
        return this.a != null ? this.a.t() : "";
    }

    public String getPackacgeName() {
        return this.a != null ? this.a.n() : "";
    }

    public CampaignAction getCampaignAction() {
        CampaignAction campaignAction = CampaignAction.OPEN_MARKET;
        return (this.a == null || !this.a.r()) ? campaignAction : CampaignAction.LAUNCH_APP;
    }

    public boolean isApp() {
        if (this.a != null) {
            return this.a.u();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final AdDetails a() {
        return this.a;
    }

    public void registerViewForInteraction(View view, List<View> list) {
        registerViewForInteraction(view, list, null);
    }

    public void registerViewForInteraction(View view, List<View> list, NativeAdDisplayListener nativeAdDisplayListener) {
        if (list == null || list.isEmpty() || this.j.get() != null) {
            registerViewForInteraction(view);
        } else {
            b bVar = new b(this, 0);
            for (View onClickListener : list) {
                onClickListener.setOnClickListener(bVar);
            }
        }
        if (nativeAdDisplayListener != null) {
            this.l = new b(nativeAdDisplayListener);
        }
    }

    public void unregisterView() {
        e();
        View view = (View) this.j.get();
        this.j.clear();
        if (!(view == null || VERSION.SDK_INT < 12 || this.k == null)) {
            view.removeOnAttachStateChangeListener(this.k);
        }
        if (this.c != null) {
            this.c.recycle();
            this.c = null;
        }
        if (this.d != null) {
            this.d.recycle();
            this.d = null;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.i == null && !this.e) {
            View view = (View) this.j.get();
            if (view == null) {
                if (this.l != null) {
                    this.l.adNotDisplayed(this);
                }
                return;
            }
            h hVar = new h(view.getContext(), new String[]{this.a.d()}, new TrackingParams(this.h), f());
            hVar.a((com.startapp.sdk.adsbase.h.a) new com.startapp.sdk.adsbase.h.a() {
                public final void onSent() {
                    NativeAdDetails.this.e = true;
                    if (NativeAdDetails.this.l != null) {
                        NativeAdDetails.this.l.adDisplayed(NativeAdDetails.this);
                    }
                }
            });
            this.i = new com.startapp.sdk.adsbase.j.b(this.j, hVar, d());
            this.i.a(new com.startapp.sdk.adsbase.j.b.a() {
                public final void a() {
                    if (NativeAdDetails.this.l != null && !NativeAdDetails.this.f) {
                        NativeAdDetails.this.l.adHidden(NativeAdDetails.this);
                        NativeAdDetails.this.f = true;
                    }
                }
            });
            this.i.a();
        }
    }

    private static int d() {
        return BannerMetaData.a().b().q();
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.i != null) {
            this.i.b();
            this.i = null;
        }
    }

    private long f() {
        if (this.a.y() != null) {
            return TimeUnit.SECONDS.toMillis(this.a.y().longValue());
        }
        return TimeUnit.SECONDS.toMillis(MetaData.D().E());
    }

    public void registerViewForInteraction(View view) {
        this.j = new WeakReference<>(view);
        if (view.hasWindowFocus() || VERSION.SDK_INT < 12) {
            c();
        } else {
            if (this.k == null) {
                this.k = new OnAttachStateChangeListener() {
                    public final void onViewAttachedToWindow(View view) {
                        NativeAdDetails.this.c();
                    }

                    public final void onViewDetachedFromWindow(View view) {
                        NativeAdDetails.this.e();
                        view.removeOnAttachStateChangeListener(NativeAdDetails.this.k);
                    }
                };
            }
            view.addOnAttachStateChangeListener(this.k);
        }
        ((View) this.j.get()).setOnClickListener(new b(this, 0));
    }

    static /* synthetic */ void a(NativeAdDetails nativeAdDetails, View view) {
        Context context = view.getContext();
        switch (nativeAdDetails.getCampaignAction()) {
            case OPEN_MARKET:
                boolean a2 = com.startapp.sdk.adsbase.a.a(context, Placement.INAPP_NATIVE);
                if (nativeAdDetails.a.l() && !a2) {
                    com.startapp.sdk.adsbase.a.a(context, nativeAdDetails.a.c(), nativeAdDetails.a.e(), nativeAdDetails.a.n(), new TrackingParams(nativeAdDetails.h), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), nativeAdDetails.a.w(), nativeAdDetails.a.z());
                    break;
                } else {
                    com.startapp.sdk.adsbase.a.a(context, nativeAdDetails.a.c(), nativeAdDetails.a.e(), new TrackingParams(nativeAdDetails.h), nativeAdDetails.a.w() && !a2, false);
                    break;
                }
            case LAUNCH_APP:
                com.startapp.sdk.adsbase.a.a(nativeAdDetails.getPackacgeName(), nativeAdDetails.a.p(), nativeAdDetails.a.c(), context, new TrackingParams(nativeAdDetails.h));
                break;
        }
        if (nativeAdDetails.l != null) {
            nativeAdDetails.l.adClicked(nativeAdDetails);
        }
    }
}
