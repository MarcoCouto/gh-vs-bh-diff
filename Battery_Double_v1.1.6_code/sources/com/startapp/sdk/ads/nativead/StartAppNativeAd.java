package com.startapp.sdk.ads.nativead;

import android.content.Context;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.adinformation.AdInformationMetaData;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.adrules.AdaptMetaData;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StartAppSDK */
public class StartAppNativeAd extends Ad implements com.startapp.sdk.ads.nativead.NativeAdDetails.a {
    private static final long serialVersionUID = 1;
    private a adEventDelegate;
    boolean isLoading = false;
    private List<NativeAdDetails> listNativeAds = new ArrayList();
    private NativeAd nativeAd;
    private NativeAdPreferences preferences;
    private int totalObjectsLoaded = 0;

    /* compiled from: StartAppSDK */
    public enum CampaignAction {
        LAUNCH_APP,
        OPEN_MARKET
    }

    /* compiled from: StartAppSDK */
    class a extends b {
        private b a;

        public a(b bVar) {
            this.a = bVar;
        }

        public final void a(Ad ad) {
            StartAppNativeAd.this.a();
        }

        public final void b(Ad ad) {
            StartAppNativeAd.this.setErrorMessage(ad.getErrorMessage());
            if (this.a != null) {
                this.a.b(StartAppNativeAd.this);
                this.a = null;
            }
            StartAppNativeAd.this.isLoading = false;
            StartAppNativeAd.this.a();
        }

        public final b a() {
            return this.a;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
    }

    public StartAppNativeAd(Context context) {
        super(context, Placement.INAPP_NATIVE);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n===== StartAppNativeAd =====\n");
        for (int i = 0; i < getNumberOfAds(); i++) {
            stringBuffer.append(this.listNativeAds.get(i));
        }
        stringBuffer.append("===== End StartAppNativeAd =====");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.totalObjectsLoaded = 0;
        if (this.listNativeAds == null) {
            this.listNativeAds = new ArrayList();
        }
        this.listNativeAds.clear();
        if (this.nativeAd != null && this.nativeAd.g() != null) {
            for (int i = 0; i < this.nativeAd.g().size(); i++) {
                this.listNativeAds.add(new NativeAdDetails((AdDetails) this.nativeAd.g().get(i), this.preferences, i, this));
            }
        }
    }

    public void onNativeAdDetailsLoaded(int i) {
        this.totalObjectsLoaded++;
        if (this.nativeAd.g() != null && this.totalObjectsLoaded == this.nativeAd.g().size()) {
            this.isLoading = false;
            setErrorMessage(null);
            if (this.adEventDelegate != null) {
                b a2 = this.adEventDelegate.a();
                if (a2 != null) {
                    a2.a(this);
                }
            }
        }
    }

    public int getNumberOfAds() {
        if (this.listNativeAds != null) {
            return this.listNativeAds.size();
        }
        return 0;
    }

    public boolean isBelowMinCPM() {
        return this.nativeAd.isBelowMinCPM();
    }

    public boolean loadAd() {
        return loadAd(new NativeAdPreferences(), null);
    }

    public boolean loadAd(AdEventListener adEventListener) {
        return loadAd(new NativeAdPreferences(), adEventListener);
    }

    public boolean loadAd(NativeAdPreferences nativeAdPreferences) {
        return loadAd(nativeAdPreferences, null);
    }

    public boolean loadAd(NativeAdPreferences nativeAdPreferences, AdEventListener adEventListener) {
        this.adEventDelegate = new a(b.a(this.a, adEventListener));
        this.preferences = nativeAdPreferences;
        if (this.isLoading) {
            setErrorMessage("Ad is currently being loaded");
            return false;
        }
        this.isLoading = true;
        this.nativeAd = new NativeAd(this.a, this.preferences);
        return this.nativeAd.load(nativeAdPreferences, this.adEventDelegate, true);
    }

    public ArrayList<NativeAdDetails> getNativeAds() {
        return getNativeAds(null);
    }

    public ArrayList<NativeAdDetails> getNativeAds(String str) {
        ArrayList<NativeAdDetails> arrayList = new ArrayList<>();
        AdRulesResult a2 = AdaptMetaData.a().b().a(Placement.INAPP_NATIVE, str);
        if (!a2.a()) {
            com.startapp.sdk.adsbase.a.a(this.a, com.startapp.sdk.adsbase.a.a(g()), str, a2.c());
        } else if (this.listNativeAds != null) {
            for (NativeAdDetails nativeAdDetails : this.listNativeAds) {
                nativeAdDetails.a(str);
                arrayList.add(nativeAdDetails);
            }
            com.startapp.sdk.adsbase.adrules.b.a().a(new com.startapp.sdk.adsbase.adrules.a(Placement.INAPP_NATIVE, str));
        }
        return arrayList;
    }

    private List<AdDetails> g() {
        ArrayList arrayList = new ArrayList();
        if (this.listNativeAds != null) {
            for (NativeAdDetails a2 : this.listNativeAds) {
                arrayList.add(a2.a());
            }
        }
        return arrayList;
    }

    public String getAdId() {
        if (this.listNativeAds != null && this.listNativeAds.size() > 0) {
            NativeAdDetails nativeAdDetails = (NativeAdDetails) this.listNativeAds.get(0);
            if (!(nativeAdDetails == null || nativeAdDetails.a() == null)) {
                return nativeAdDetails.a().a();
            }
        }
        return null;
    }

    public static String getPrivacyURL() {
        if (AdInformationMetaData.b().c() == null) {
            return "";
        }
        String c = AdInformationMetaData.b().c();
        if (c.contains("http://") || c.contains("https://")) {
            return AdInformationMetaData.b().c();
        }
        StringBuilder sb = new StringBuilder("https://");
        sb.append(AdInformationMetaData.b().c());
        return sb.toString();
    }

    public static String getPrivacyImageUrl() {
        return AdInformationMetaData.b().d();
    }
}
