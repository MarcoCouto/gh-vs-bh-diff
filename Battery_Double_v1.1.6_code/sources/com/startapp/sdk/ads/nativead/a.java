package com.startapp.sdk.ads.nativead;

import android.content.Context;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.model.GetAdRequest;

/* compiled from: StartAppSDK */
public final class a extends com.startapp.sdk.json.a {
    private NativeAdPreferences g;

    /* access modifiers changed from: protected */
    public final void a(Ad ad) {
    }

    public a(Context context, Ad ad, AdPreferences adPreferences, b bVar, NativeAdPreferences nativeAdPreferences) {
        super(context, ad, adPreferences, bVar, Placement.INAPP_NATIVE);
        this.g = nativeAdPreferences;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        GetAdRequest a = super.a();
        if (a == null) {
            return null;
        }
        a.e(this.g.getAdsNumber());
        if (this.g.getImageSize() != null) {
            a.c(this.g.getImageSize().getWidth());
            a.d(this.g.getImageSize().getHeight());
        } else {
            int primaryImageSize = this.g.getPrimaryImageSize();
            if (primaryImageSize == -1) {
                primaryImageSize = 2;
            }
            a.b(Integer.toString(primaryImageSize));
            int secondaryImageSize = this.g.getSecondaryImageSize();
            if (secondaryImageSize == -1) {
                secondaryImageSize = 2;
            }
            a.c(Integer.toString(secondaryImageSize));
        }
        if (this.g.isContentAd()) {
            a.b(this.g.isContentAd());
        }
        return a;
    }
}
