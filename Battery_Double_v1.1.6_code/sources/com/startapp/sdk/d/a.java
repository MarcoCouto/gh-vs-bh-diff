package com.startapp.sdk.d;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.AdsConstants.AdApiType;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.apppresence.AppPresenceDetails;
import com.startapp.sdk.adsbase.d;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.AdPreferences.Placement;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: StartAppSDK */
public abstract class a extends d {
    protected Set<String> g = new HashSet();
    protected GetAdRequest h;
    private Set<String> i = new HashSet();
    private int j = 0;
    private boolean k;

    /* access modifiers changed from: protected */
    public boolean a(GetAdRequest getAdRequest) {
        return getAdRequest != null;
    }

    static {
        a.class.getSimpleName();
    }

    public a(Context context, Ad ad, AdPreferences adPreferences, b bVar, Placement placement, boolean z) {
        super(context, ad, adPreferences, bVar, placement);
        this.k = z;
    }

    /* access modifiers changed from: protected */
    public final Object e() {
        Object obj;
        this.h = a();
        if (!a(this.h)) {
            return null;
        }
        if (this.i.size() == 0) {
            this.i.add(this.a.getPackageName());
        }
        this.h.a(this.i);
        this.h.b(this.g);
        if (this.j > 0) {
            this.h.c(false);
            if (MetaData.D().d().a(this.a)) {
                SimpleTokenUtils.b(this.a);
            }
        }
        try {
            obj = com.startapp.sdk.adsbase.h.a.a(this.a, AdsConstants.a(AdApiType.HTML, f()), this.h);
        } catch (SDKException e) {
            if (!MetaData.D().o().contains(Integer.valueOf(e.a()))) {
                new e((Throwable) e).a(this.a);
            }
            this.f = e.getMessage();
            obj = null;
        } catch (Throwable th) {
            new e(th).a(this.a);
            this.f = th.getMessage();
            return null;
        }
        return obj;
    }

    /* access modifiers changed from: protected */
    public boolean a(Object obj) {
        if (obj == null) {
            if (this.f == null) {
                this.f = "No response";
            }
            return false;
        }
        try {
            ArrayList arrayList = new ArrayList();
            String a = ((com.startapp.common.b.e.a) obj).a();
            if (TextUtils.isEmpty(a)) {
                if (this.f == null) {
                    if (this.h == null || !this.h.j()) {
                        this.f = "Empty Ad";
                    } else {
                        this.f = "Video isn't available";
                    }
                }
                return false;
            }
            List a2 = com.iab.omid.library.startapp.b.a(a, this.j);
            if (!(AdsCommonMetaData.a().F() ? com.iab.omid.library.startapp.b.a(this.a, a2, this.j, this.i, (List<AppPresenceDetails>) arrayList).booleanValue() : false)) {
                ((HtmlAd) this.b).a(a2);
                ((HtmlAd) this.b).b(a);
                return true;
            }
            this.j++;
            new com.startapp.sdk.adsbase.apppresence.a(this.a, arrayList).a();
            return d().booleanValue();
        } catch (Throwable th) {
            new e(th).a(this.a);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        super.a(bool);
        StringBuilder sb = new StringBuilder("Html onPostExecute, result=[");
        sb.append(bool);
        sb.append(RequestParameters.RIGHT_BRACKETS);
    }

    /* access modifiers changed from: protected */
    public void b(Boolean bool) {
        super.b(bool);
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        Intent intent = new Intent("com.startapp.android.OnReceiveResponseBroadcastListener");
        intent.putExtra("adHashcode", this.b.hashCode());
        intent.putExtra("adResult", z);
        com.startapp.common.b.a(this.a).a(intent);
        if (!z || this.b == null) {
            StringBuilder sb = new StringBuilder("Html onPostExecute failed error=[");
            sb.append(this.f);
            sb.append(RequestParameters.RIGHT_BRACKETS);
        } else if (this.k) {
            s.a(this.a, ((HtmlAd) this.b).j(), (com.startapp.sdk.adsbase.i.s.a) new com.startapp.sdk.adsbase.i.s.a() {
                public final void a() {
                    a.this.d.a(a.this.b);
                }

                public final void a(String str) {
                    a.this.b.setErrorMessage(str);
                    a.this.d.b(a.this.b);
                }
            });
        } else if (z) {
            this.d.a(this.b);
        } else {
            this.d.b(this.b);
        }
    }
}
