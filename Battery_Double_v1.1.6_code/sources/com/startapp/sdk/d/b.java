package com.startapp.sdk.d;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.i.s;
import java.util.Iterator;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public class b {
    protected boolean a;
    private boolean b;
    private Runnable c;
    private Runnable d;
    private Runnable e;
    private Context f;
    private TrackingParams g;

    static {
        b.class.getSimpleName();
    }

    public b(Context context, Runnable runnable, TrackingParams trackingParams, boolean z) {
        this(context, runnable, trackingParams);
        this.a = z;
    }

    private b(Context context, Runnable runnable, TrackingParams trackingParams) {
        this.b = false;
        this.a = true;
        this.c = null;
        this.d = null;
        this.e = null;
        this.c = runnable;
        this.f = context;
        this.g = trackingParams;
    }

    public b(Context context, Runnable runnable, Runnable runnable2, Runnable runnable3, TrackingParams trackingParams, boolean z) {
        this(context, runnable, trackingParams, z);
        this.d = runnable2;
        this.e = runnable3;
    }

    public b(Context context, Runnable runnable, Runnable runnable2, TrackingParams trackingParams) {
        this(context, runnable, trackingParams);
        this.d = runnable2;
    }

    @JavascriptInterface
    public void closeAd() {
        if (!this.b) {
            this.b = true;
            this.c.run();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:(1:3)|4|(3:6|7|(2:10|8))|11|12|15|(2:17|18)(1:19)) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0044, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0045, code lost:
        new com.startapp.sdk.adsbase.infoevents.e(r3).a(r2.f);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @JavascriptInterface
    public void openApp(String str, String str2, String str3) {
        if (str != null && !TextUtils.isEmpty(str)) {
            a.b(this.f, str, this.g);
        }
        Intent launchIntentForPackage = this.f.getPackageManager().getLaunchIntentForPackage(str2);
        if (str3 != null) {
            JSONObject jSONObject = new JSONObject(str3);
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String valueOf = String.valueOf(keys.next());
                launchIntentForPackage.putExtra(valueOf, String.valueOf(jSONObject.get(valueOf)));
            }
        }
        this.f.startActivity(launchIntentForPackage);
        if (this.d == null) {
            this.d.run();
        }
    }

    @JavascriptInterface
    public void externalLinks(String str) {
        if (!this.a || !s.a(256)) {
            a.c(this.f, str);
        } else {
            a.a(this.f, str, (String) null);
        }
    }

    @JavascriptInterface
    public void enableScroll(String str) {
        if (this.e != null) {
            this.e.run();
        }
    }
}
