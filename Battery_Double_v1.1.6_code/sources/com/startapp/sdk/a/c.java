package com.startapp.sdk.a;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: StartAppSDK */
final class c {
    private final Object a;
    private final Method b;

    public c(Object obj, Method method) {
        this.a = obj;
        this.b = method;
    }

    public final Object a(Object[] objArr) throws InvocationTargetException, IllegalAccessException {
        return this.b.invoke(this.a, objArr);
    }
}
