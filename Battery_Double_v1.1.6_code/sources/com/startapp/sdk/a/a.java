package com.startapp.sdk.a;

import android.content.Context;
import com.startapp.sdk.adsbase.i.d;
import com.startapp.sdk.adsbase.i.g;
import com.startapp.sdk.adsbase.i.s;
import java.lang.ref.SoftReference;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public class a {
    private final String a;
    private final String b;
    private final String[] c;
    private final Object[] d;
    private final String[] e;
    private transient SoftReference<c> f;
    private final transient Map<String, SoftReference<Map<String, Object>>> g = new ConcurrentHashMap();

    /* renamed from: com.startapp.sdk.a.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    static class C0070a implements Iterator<Object> {
        private Object a;
        private int b;
        private int c;

        public C0070a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        public final boolean hasNext() {
            return this.c < this.b;
        }

        public final Object next() {
            Object obj = this.a;
            int i = this.c;
            this.c = i + 1;
            return Array.get(obj, i);
        }
    }

    /* compiled from: StartAppSDK */
    static class b implements Iterator<Object> {
        static final b a = new b();

        public final boolean hasNext() {
            return false;
        }

        public final Object next() {
            return null;
        }

        b() {
        }
    }

    static {
        a.class.getSimpleName();
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final String[] c() {
        return this.c;
    }

    public final Object[] d() {
        return this.d;
    }

    public a(String str, String str2, String[] strArr, Object[] objArr, String[] strArr2) {
        this.a = str;
        this.b = str2;
        this.c = strArr;
        this.d = objArr;
        this.e = strArr2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return s.b(this.a, aVar.a) && s.b(this.b, aVar.b) && Arrays.equals(this.c, aVar.c) && Arrays.equals(this.d, aVar.d) && Arrays.equals(this.e, aVar.e);
    }

    public int hashCode() {
        return s.a(this.a, this.b, this.c, this.d, this.e);
    }

    public String toString() {
        return super.toString();
    }

    public final JSONArray a(Context context, int[] iArr, Integer num) {
        Iterator it;
        try {
            Object a2 = a(context).a(this.d);
            if (a2 == null) {
                it = b.a;
            } else if (a2 instanceof Collection) {
                it = ((Collection) a2).iterator();
            } else if (a2.getClass().isArray()) {
                it = new C0070a(a2, Array.getLength(a2));
            } else {
                it = Collections.singleton(a2).iterator();
            }
            List<JSONObject> arrayList = new ArrayList<>();
            while (true) {
                Map map = null;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                if (next != null) {
                    JSONObject jSONObject = new JSONObject();
                    Class cls = next.getClass();
                    SoftReference softReference = (SoftReference) this.g.get(cls.getName());
                    if (softReference != null) {
                        map = (Map) softReference.get();
                    }
                    if (map == null) {
                        map = a(cls, this.e);
                        this.g.put(cls.getName(), new SoftReference(map));
                    }
                    for (Entry entry : map.entrySet()) {
                        String str = (String) entry.getKey();
                        Object value = entry.getValue();
                        try {
                            if (value instanceof Field) {
                                jSONObject.put(str, a(((Field) value).get(next)));
                            } else if (value instanceof Method) {
                                jSONObject.put(str, a(((Method) value).invoke(next, new Object[0])));
                            }
                        } catch (Throwable unused) {
                        }
                    }
                    arrayList.add(jSONObject);
                }
            }
            if (iArr != null && iArr.length > 0) {
                int length = this.e.length;
                Comparator comparator = null;
                for (int i : iArr) {
                    if (i != 0 && Math.abs(i) <= length) {
                        Comparator gVar = new g(this.e[Math.abs(i) - 1]);
                        if (i < 0) {
                            gVar = Collections.reverseOrder(gVar);
                        }
                        if (comparator == null) {
                            comparator = gVar;
                        } else {
                            comparator = new d(comparator, gVar);
                        }
                    }
                }
                if (comparator != null) {
                    Collections.sort(arrayList, comparator);
                }
            }
            if (num != null && num.intValue() > 0) {
                arrayList = arrayList.subList(0, Math.min(num.intValue(), arrayList.size()));
            }
            JSONArray jSONArray = new JSONArray();
            for (JSONObject put : arrayList) {
                jSONArray.put(put);
            }
            return jSONArray;
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("5", e2);
        } catch (IllegalAccessException e3) {
            throw new RuntimeException("5", e3);
        }
    }

    private static Object a(Object obj) {
        if (obj instanceof Short) {
            return Integer.valueOf(((Short) obj).intValue());
        }
        if ((obj instanceof Integer) || (obj instanceof Long)) {
            return obj;
        }
        if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        }
        if ((obj instanceof Double) || (obj instanceof Boolean) || (obj instanceof String)) {
            return obj;
        }
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    private c a(Context context) {
        c cVar = this.f != null ? (c) this.f.get() : null;
        if (cVar != null) {
            return cVar;
        }
        Object systemService = context.getApplicationContext().getSystemService(this.a);
        if (systemService != null) {
            try {
                try {
                    Method a2 = a(systemService.getClass(), this.b, e());
                    if (!a2.isAccessible()) {
                        try {
                            a2.setAccessible(true);
                        } catch (SecurityException e2) {
                            throw new RuntimeException("4", e2);
                        }
                    }
                    c cVar2 = new c(systemService, a2);
                    this.f = new SoftReference<>(cVar2);
                    return cVar2;
                } catch (NoSuchMethodException e3) {
                    throw new RuntimeException("3", e3);
                }
            } catch (ClassNotFoundException e4) {
                throw new RuntimeException("2", e4);
            }
        } else {
            throw new RuntimeException("1");
        }
    }

    private Class[] e() throws ClassNotFoundException {
        Class<String> cls;
        Class[] clsArr = new Class[this.c.length];
        int length = clsArr.length;
        for (int i = 0; i < length; i++) {
            String str = this.c[i];
            char c2 = 65535;
            switch (str.hashCode()) {
                case -1808118735:
                    if (str.equals("String")) {
                        c2 = 9;
                        break;
                    }
                    break;
                case -1325958191:
                    if (str.equals("double")) {
                        c2 = 5;
                        break;
                    }
                    break;
                case -891985903:
                    if (str.equals("string")) {
                        c2 = 8;
                        break;
                    }
                    break;
                case 104431:
                    if (str.equals("int")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case 3039496:
                    if (str.equals("byte")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 3052374:
                    if (str.equals("char")) {
                        c2 = 7;
                        break;
                    }
                    break;
                case 3327612:
                    if (str.equals("long")) {
                        c2 = 3;
                        break;
                    }
                    break;
                case 64711720:
                    if (str.equals("boolean")) {
                        c2 = 6;
                        break;
                    }
                    break;
                case 97526364:
                    if (str.equals("float")) {
                        c2 = 4;
                        break;
                    }
                    break;
                case 109413500:
                    if (str.equals("short")) {
                        c2 = 1;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    cls = Byte.TYPE;
                    break;
                case 1:
                    cls = Short.TYPE;
                    break;
                case 2:
                    cls = Integer.TYPE;
                    break;
                case 3:
                    cls = Long.TYPE;
                    break;
                case 4:
                    cls = Float.TYPE;
                    break;
                case 5:
                    cls = Double.TYPE;
                    break;
                case 6:
                    cls = Boolean.TYPE;
                    break;
                case 7:
                    cls = Character.TYPE;
                    break;
                case 8:
                case 9:
                    cls = String.class;
                    break;
                default:
                    cls = Class.forName(str);
                    break;
            }
            clsArr[i] = cls;
        }
        return clsArr;
    }

    private static Method a(Class<?> cls, String str, Class[] clsArr) throws NoSuchMethodException {
        Throwable th = null;
        while (cls != null) {
            try {
                return cls.getDeclaredMethod(str, clsArr);
            } catch (NoSuchMethodException e2) {
                if (th == null) {
                    th = e2;
                }
                cls = cls.getSuperclass();
            }
        }
        throw th;
    }

    private static Field a(Class<?> cls, String str) throws NoSuchFieldException {
        Throwable th = null;
        while (cls != null) {
            try {
                return cls.getDeclaredField(str);
            } catch (NoSuchFieldException e2) {
                if (th == null) {
                    th = e2;
                }
                cls = cls.getSuperclass();
            }
        }
        throw th;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0076, code lost:
        r0.put(r4, r6);
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[ExcHandler: SecurityException (unused java.lang.SecurityException), SYNTHETIC, Splitter:B:17:0x005b] */
    private static Map<String, Object> a(Class<?> cls, String[] strArr) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            String str = strArr[i];
            try {
                Field a2 = a(cls, str);
                if (!a2.isAccessible()) {
                    a2.setAccessible(true);
                }
                linkedHashMap.put(str, a2);
            } catch (NoSuchFieldException | SecurityException e2) {
                StringBuilder sb = new StringBuilder();
                sb.append(Character.toUpperCase(str.charAt(0)));
                sb.append(str.substring(1));
                String sb2 = sb.toString();
                try {
                    Method a3 = a(cls, "get".concat(String.valueOf(sb2)), new Class[0]);
                    if (!a3.isAccessible()) {
                        a3.setAccessible(true);
                    }
                    linkedHashMap.put(str, a3);
                } catch (NoSuchMethodException unused) {
                    try {
                        Method a4 = a(cls, "is".concat(String.valueOf(sb2)), new Class[0]);
                        if (!a4.isAccessible()) {
                            a4.setAccessible(true);
                        }
                        linkedHashMap.put(str, a4);
                    } catch (SecurityException unused2) {
                    }
                }
            }
        }
        return linkedHashMap;
    }
}
