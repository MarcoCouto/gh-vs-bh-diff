package com.startapp.sdk.a;

import android.util.JsonReader;
import com.startapp.sdk.adsbase.i.i;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* compiled from: StartAppSDK */
public final class b {
    public int a;
    public int b;
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;

    public static a a(String str) {
        try {
            List b2 = i.b(new JsonReader(new StringReader(str)));
            int i = 0;
            String str2 = (String) b2.get(0);
            if (str2 != null) {
                String str3 = (String) b2.get(1);
                if (str3 != null) {
                    List list = (List) b2.get(2);
                    if (list != null) {
                        String[] strArr = new String[list.size()];
                        Object[] objArr = new Object[list.size()];
                        int size = list.size();
                        int i2 = 0;
                        while (i2 < size) {
                            Map map = (Map) list.get(i2);
                            if (map == null) {
                                throw new IllegalArgumentException(str);
                            } else if (map.size() == 1) {
                                Entry entry = (Entry) map.entrySet().iterator().next();
                                String str4 = (String) entry.getKey();
                                if (str4 != null) {
                                    strArr[i2] = str4;
                                    objArr[i2] = entry.getValue();
                                    i2++;
                                } else {
                                    throw new IllegalArgumentException(str);
                                }
                            } else {
                                throw new IllegalArgumentException(str);
                            }
                        }
                        List list2 = (List) b2.get(3);
                        if (list2 != null) {
                            String[] strArr2 = new String[list2.size()];
                            int size2 = list2.size();
                            while (i < size2) {
                                String str5 = (String) list2.get(i);
                                if (str5 != null) {
                                    strArr2[i] = str5;
                                    i++;
                                } else {
                                    throw new IllegalArgumentException(str);
                                }
                            }
                            a aVar = new a(str2, str3, strArr, objArr, strArr2);
                            return aVar;
                        }
                        throw new IllegalArgumentException(str);
                    }
                    throw new IllegalArgumentException(str);
                }
                throw new IllegalArgumentException(str);
            }
            throw new IllegalArgumentException(str);
        } catch (ClassCastException e2) {
            throw new IllegalArgumentException(str, e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException(str, e3);
        }
    }

    public b(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
        this.g = i7;
        this.h = i8;
    }
}
