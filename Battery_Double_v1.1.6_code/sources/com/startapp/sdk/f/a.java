package com.startapp.sdk.f;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.util.Base64;
import android.util.JsonReader;
import android.util.Pair;
import com.startapp.sdk.a.b;
import com.startapp.sdk.adsbase.i.f;
import com.startapp.sdk.adsbase.i.i;
import com.startapp.sdk.adsbase.i.s;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.remoteconfig.RscMetadata;
import com.startapp.sdk.adsbase.remoteconfig.RscMetadataItem;
import com.startapp.sdk.f.a.e;
import java.io.IOException;
import java.io.StringReader;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StartAppSDK */
public class a {
    private final Context a;
    private final f<RscMetadata> b;
    private RscMetadata c;
    private List<e> d;
    private List<b> e;
    private final Map<com.startapp.sdk.a.a, Pair<Long, SoftReference<JSONObject>>> f = new WeakHashMap();

    static {
        a.class.getSimpleName();
    }

    public a(Context context, f<RscMetadata> fVar) {
        this.a = context;
        this.b = fVar;
    }

    public final void a() {
        List c2 = c();
        com.startapp.sdk.adsbase.infoevents.e e2 = new com.startapp.sdk.adsbase.infoevents.e(InfoEventCategory.GENERAL).e("RSC init");
        StringBuilder sb = new StringBuilder("targets: ");
        sb.append(c2 != null ? Integer.valueOf(c2.size()) : null);
        e2.f(sb.toString()).a(this.a);
    }

    private RscMetadata b() {
        RscMetadata rscMetadata = (RscMetadata) this.b.a();
        if (rscMetadata == null || !rscMetadata.a()) {
            return null;
        }
        return rscMetadata;
    }

    private static boolean a(RscMetadata rscMetadata, int i) {
        return (rscMetadata == null || (rscMetadata.d() & i) == 0) ? false : true;
    }

    private static boolean a(RscMetadata rscMetadata, RscMetadataItem rscMetadataItem, int i) {
        return (rscMetadata.a(rscMetadataItem) & i) != 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0021, code lost:
        r2 = a(r14.a, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0027, code lost:
        if (r2 == null) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
        if (r2.size() > 0) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
        r3 = r0.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        if (r3 == null) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003b, code lost:
        if (r3.size() > 0) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003f, code lost:
        r1 = new java.util.LinkedList();
        r3 = r3.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004c, code lost:
        if (r3.hasNext() == false) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004e, code lost:
        r4 = (com.startapp.sdk.adsbase.remoteconfig.RscMetadataItem) r3.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0054, code lost:
        if (r4 == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        r7 = a(r14.a, r0, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005c, code lost:
        if (r7 == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005e, code lost:
        r8 = a(r2, r4.b(), r4.c());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006e, code lost:
        if (r8.size() <= 0) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0076, code lost:
        if (r4.d() == null) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0078, code lost:
        r9 = r4.d().intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0082, code lost:
        r9 = 300;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        r10 = r4.e();
        r11 = r4.f();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0092, code lost:
        if (r4.g() == null) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0094, code lost:
        r12 = r4.g().intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009e, code lost:
        r12 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a0, code lost:
        r6 = new com.startapp.sdk.f.b(r7, r8, r9, r10, r11, r12, r0.a(r4));
        r1.add(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b0, code lost:
        return a(r0, r2, (java.util.List<com.startapp.sdk.f.b>) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b5, code lost:
        return a(r0, null, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00ba, code lost:
        return a(r0, null, null);
     */
    private List<b> c() {
        if (VERSION.SDK_INT < 11) {
            return null;
        }
        RscMetadata b2 = b();
        if (b2 == null) {
            return a((RscMetadata) null, null, null);
        }
        synchronized (this) {
            if (b2.equals(this.c)) {
                List<b> list = this.e;
                return list;
            }
        }
    }

    private synchronized List<b> a(RscMetadata rscMetadata, List<e> list, List<b> list2) {
        if (this.d != null) {
            for (e a2 : this.d) {
                try {
                    a2.a(this.a);
                } catch (Throwable th) {
                    if (a(this.c, 64)) {
                        new com.startapp.sdk.adsbase.infoevents.e(th).a(this.a);
                    }
                }
            }
        }
        this.c = rscMetadata;
        this.d = list;
        this.e = list2;
        if (list != null) {
            for (e a3 : this.d) {
                try {
                    a3.a(this.a, this);
                } catch (Throwable th2) {
                    if (a(rscMetadata, 128)) {
                        new com.startapp.sdk.adsbase.infoevents.e(th2).a(this.a);
                    }
                }
            }
        }
        return list2;
    }

    private static List<e> a(Context context, RscMetadata rscMetadata) {
        String b2 = rscMetadata.b();
        if (b2 == null || b2.length() <= 0) {
            return null;
        }
        try {
            try {
                List<Object> b3 = i.b(new JsonReader(new StringReader(a(b2))));
                ArrayList arrayList = new ArrayList();
                for (Object a2 : b3) {
                    arrayList.add(com.startapp.sdk.f.a.f.a(a2));
                }
                return arrayList;
            } catch (Throwable th) {
                if (a(rscMetadata, 1)) {
                    new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
                }
                return null;
            }
        } catch (Throwable th2) {
            if (a(rscMetadata, 1)) {
                new com.startapp.sdk.adsbase.infoevents.e(th2).a(context);
            }
            return null;
        }
    }

    private static com.startapp.sdk.a.a a(Context context, RscMetadata rscMetadata, RscMetadataItem rscMetadataItem) {
        com.startapp.sdk.a.a aVar;
        String a2 = rscMetadataItem.a();
        if (a2 == null || a2.length() <= 0) {
            return null;
        }
        try {
            try {
                aVar = b.a(a(a2));
            } catch (Throwable th) {
                if (a(rscMetadata, rscMetadataItem, 4)) {
                    new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
                }
                aVar = null;
            }
            return aVar;
        } catch (Throwable th2) {
            if (a(rscMetadata, rscMetadataItem, 2)) {
                new com.startapp.sdk.adsbase.infoevents.e(th2).a(context);
            }
            return null;
        }
    }

    public final String a(Object obj) {
        int i;
        String str;
        JSONArray jSONArray;
        List c2 = c();
        if (c2 == null) {
            return null;
        }
        Iterator it = c2.iterator();
        JSONObject jSONObject = null;
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            b bVar = (b) it.next();
            try {
                i = bVar.a(obj);
            } catch (Throwable th) {
                if (bVar.b(256)) {
                    new com.startapp.sdk.adsbase.infoevents.e(th).a(this.a);
                }
            }
            if ((i & 1) != 0) {
                com.startapp.sdk.a.a a2 = bVar.a();
                JSONObject a3 = (i & 2) == 0 ? a(a2, bVar.b()) : null;
                if (a3 == null) {
                    try {
                        jSONArray = a2.a(this.a, bVar.c(), bVar.d());
                    } catch (Throwable th2) {
                        if (bVar.b(8)) {
                            new com.startapp.sdk.adsbase.infoevents.e(th2).a(this.a);
                        }
                        jSONArray = null;
                    }
                    if (jSONArray != null && jSONArray.length() > 0) {
                        a3 = new JSONObject();
                        try {
                            if (bVar.a(1)) {
                                a3.put("currentTimeMillis", System.currentTimeMillis());
                            }
                            if (bVar.a(2)) {
                                a3.put("bootTimeMillis", SystemClock.elapsedRealtime());
                            }
                            JSONArray a4 = a(this.a, bVar);
                            if (a4 != null) {
                                a3.put("params", a4);
                            }
                            a3.put("items", jSONArray);
                        } catch (JSONException e2) {
                            if (bVar.b(32)) {
                                new com.startapp.sdk.adsbase.infoevents.e((Throwable) e2).a(this.a);
                            }
                        }
                        a(a2, a3);
                    }
                }
                if (a3 != null) {
                    if (jSONObject == null) {
                        jSONObject = new JSONObject();
                    }
                    try {
                        JSONObject optJSONObject = jSONObject.optJSONObject(a2.a());
                        if (optJSONObject == null) {
                            optJSONObject = new JSONObject();
                            jSONObject.put(a2.a(), optJSONObject);
                        }
                        JSONArray optJSONArray = optJSONObject.optJSONArray(a2.b());
                        if (optJSONArray == null) {
                            optJSONArray = new JSONArray();
                            optJSONObject.put(a2.b(), optJSONArray);
                        }
                        optJSONArray.put(a3);
                    } catch (JSONException e3) {
                        if (bVar.b(32)) {
                            new com.startapp.sdk.adsbase.infoevents.e((Throwable) e3).a(this.a);
                        }
                    }
                }
            }
        }
        if (jSONObject == null) {
            return null;
        }
        try {
            str = Base64.encodeToString(com.startapp.common.b.a.a(s.a(jSONObject.toString())), 10);
        } catch (Throwable th3) {
            RscMetadata b2 = b();
            if (!(b2 == null || (b2.d() & 16) == 0)) {
                i = 1;
            }
            if (i != 0) {
                new com.startapp.sdk.adsbase.infoevents.e(th3).a(this.a);
            }
            str = null;
        }
        return str;
    }

    private static JSONArray a(Context context, b bVar) {
        String[] c2 = bVar.a().c();
        Object[] d2 = bVar.a().d();
        if (c2.length == d2.length) {
            int length = c2.length;
            if (length == 0) {
                return null;
            }
            try {
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put(c2[i], d2[i]);
                    jSONArray.put(jSONObject);
                }
                return jSONArray;
            } catch (JSONException e2) {
                if (bVar.b(32)) {
                    new com.startapp.sdk.adsbase.infoevents.e((Throwable) e2).a(context);
                }
            }
        } else {
            if (bVar.b(512)) {
                com.startapp.sdk.adsbase.infoevents.e e3 = new com.startapp.sdk.adsbase.infoevents.e(InfoEventCategory.ERROR).e("c690e4ef5365d88b");
                StringBuilder sb = new StringBuilder();
                sb.append(Arrays.toString(c2));
                sb.append(", ");
                sb.append(Arrays.toString(d2));
                e3.f(sb.toString()).a(context);
            }
            return null;
        }
    }

    private JSONObject a(com.startapp.sdk.a.a aVar, int i) {
        Pair pair;
        synchronized (this) {
            pair = (Pair) this.f.get(aVar);
        }
        if (pair == null) {
            return null;
        }
        JSONObject jSONObject = (JSONObject) ((SoftReference) pair.second).get();
        if (jSONObject == null) {
            return null;
        }
        if (((Long) pair.first).longValue() + ((long) (i * 1000)) < SystemClock.elapsedRealtime()) {
            return null;
        }
        return jSONObject;
    }

    private synchronized void a(com.startapp.sdk.a.a aVar, JSONObject jSONObject) {
        this.f.put(aVar, new Pair(Long.valueOf(SystemClock.elapsedRealtime()), new SoftReference(jSONObject)));
    }

    private static List<Pair<e, Boolean>> a(List<e> list, int i, int i2) {
        ArrayList arrayList = new ArrayList(Math.min(list.size(), Integer.bitCount(i)));
        int i3 = 0;
        for (e eVar : list) {
            boolean z = true;
            int i4 = 1 << i3;
            if ((i & i4) != 0) {
                if ((i4 & i2) == 0) {
                    z = false;
                }
                arrayList.add(new Pair(eVar, Boolean.valueOf(z)));
            }
            i3++;
        }
        return arrayList;
    }

    private static String a(String str) throws IOException {
        return new String(s.a(com.startapp.common.b.a.a(Base64.decode(str, 8))));
    }
}
