package com.startapp.sdk.f.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Pair;
import com.startapp.sdk.f.a;
import java.util.Map;

/* compiled from: StartAppSDK */
class b extends d {
    private BroadcastReceiver b;

    static {
        b.class.getSimpleName();
    }

    public b(String str, Map<String, String> map) {
        super(str, map);
    }

    public final void a(Context context, final a aVar) throws Exception {
        super.a(context, aVar);
        if (this.b == null) {
            AnonymousClass1 r0 = new BroadcastReceiver() {
                public final void onReceive(Context context, Intent intent) {
                    aVar.a((Object) new Pair(b.this, intent));
                }
            };
            this.b = r0;
            context.registerReceiver(r0, new IntentFilter(this.a));
            return;
        }
        throw new IllegalStateException();
    }

    public final void a(Context context) throws Exception {
        super.a(context);
        if (this.b != null) {
            context.unregisterReceiver(this.b);
            this.b = null;
            return;
        }
        throw new IllegalStateException();
    }
}
