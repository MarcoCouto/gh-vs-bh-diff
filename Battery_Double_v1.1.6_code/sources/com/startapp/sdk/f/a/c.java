package com.startapp.sdk.f.a;

import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import java.util.List;

/* compiled from: StartAppSDK */
final class c extends e {
    private final List<InfoEventCategory> a;

    public c(List<InfoEventCategory> list) {
        this.a = list;
    }

    public final boolean a(Object obj) {
        if (obj instanceof e) {
            return this.a.contains(((e) obj).g());
        }
        return false;
    }
}
