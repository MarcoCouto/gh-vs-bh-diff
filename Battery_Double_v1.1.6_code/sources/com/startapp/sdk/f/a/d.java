package com.startapp.sdk.f.a;

import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import java.util.Map;
import java.util.Map.Entry;

/* compiled from: StartAppSDK */
class d extends e {
    protected final String a;
    private final Map<String, String> b;

    public d(String str, Map<String, String> map) {
        this.a = str;
        this.b = map;
    }

    public final boolean a(Object obj) {
        if (!(obj instanceof Pair)) {
            return false;
        }
        Pair pair = (Pair) obj;
        if (pair.first != this || !(pair.second instanceof Intent)) {
            return false;
        }
        Intent intent = (Intent) pair.second;
        if (!this.a.equals(intent.getAction())) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            extras = Bundle.EMPTY;
        }
        for (Entry entry : this.b.entrySet()) {
            if (!((String) entry.getValue()).equals(String.valueOf(extras.get((String) entry.getKey())))) {
                return false;
            }
        }
        return true;
    }
}
