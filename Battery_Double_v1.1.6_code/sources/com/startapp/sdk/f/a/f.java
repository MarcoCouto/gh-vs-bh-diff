package com.startapp.sdk.f.a;

import android.graphics.Point;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* compiled from: StartAppSDK */
public final class f {
    private Point a = new Point();

    public static e a(Object obj) {
        if (!(obj instanceof Map)) {
            return new e();
        }
        Map map = (Map) obj;
        Object obj2 = map.get("type");
        Object obj3 = map.get("params");
        if (obj2 instanceof Number) {
            switch (((Number) obj2).intValue()) {
                case 1:
                    if (obj3 instanceof Number) {
                        return new a(((Number) obj3).intValue());
                    }
                    break;
                case 2:
                    if (obj3 instanceof List) {
                        LinkedList linkedList = new LinkedList();
                        for (Object next : (List) obj3) {
                            if (next instanceof String) {
                                InfoEventCategory a2 = InfoEventCategory.a((String) next);
                                if (a2 != null) {
                                    linkedList.add(a2);
                                }
                            }
                        }
                        if (linkedList.size() > 0) {
                            return new c(linkedList);
                        }
                    }
                    break;
                case 3:
                    if (obj3 instanceof Map) {
                        Map map2 = (Map) obj3;
                        Object obj4 = map2.get("action");
                        if (obj4 instanceof String) {
                            Object obj5 = map2.get("extras");
                            HashMap hashMap = new HashMap();
                            if (obj5 instanceof Map) {
                                for (Entry entry : ((Map) obj5).entrySet()) {
                                    Object key = entry.getKey();
                                    if (key instanceof String) {
                                        hashMap.put((String) key, String.valueOf(entry.getValue()));
                                    }
                                }
                            }
                            return new b((String) obj4, hashMap);
                        }
                    }
                    break;
            }
        }
        return new e();
    }

    public f() {
    }

    public f(int i, int i2) {
        a(i, i2);
    }

    private void a(int i) {
        this.a.x = i;
    }

    private void b(int i) {
        this.a.y = i;
    }

    public final void a(int i, int i2) {
        a(i);
        b(i2);
    }

    public final int a() {
        return this.a.x;
    }

    public final int b() {
        return this.a.y;
    }
}
