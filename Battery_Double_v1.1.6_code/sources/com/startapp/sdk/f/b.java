package com.startapp.sdk.f;

import android.util.Pair;
import com.startapp.sdk.a.a;
import com.startapp.sdk.f.a.e;
import java.util.Iterator;
import java.util.List;

/* compiled from: StartAppSDK */
public final class b {
    private final a a;
    private final List<Pair<e, Boolean>> b;
    private final int c;
    private final int[] d;
    private final Integer e;
    private final int f;
    private final int g;

    public final a a() {
        return this.a;
    }

    public final int b() {
        return this.c;
    }

    public final int[] c() {
        return this.d;
    }

    public final Integer d() {
        return this.e;
    }

    public b(a aVar, List<Pair<e, Boolean>> list, int i, int[] iArr, Integer num, int i2, int i3) {
        this.a = aVar;
        this.b = list;
        this.c = i;
        this.d = iArr;
        this.e = num;
        this.f = i2;
        this.g = i3;
    }

    public final int a(Object obj) {
        int i;
        Pair pair;
        Iterator it = this.b.iterator();
        do {
            i = 0;
            if (!it.hasNext()) {
                return 0;
            }
            pair = (Pair) it.next();
        } while (!((e) pair.first).a(obj));
        if (((Boolean) pair.second).booleanValue()) {
            i = 2;
        }
        return i | 1;
    }

    public final boolean a(int i) {
        return (i & this.f) != 0;
    }

    public final boolean b(int i) {
        return (i & this.g) != 0;
    }
}
