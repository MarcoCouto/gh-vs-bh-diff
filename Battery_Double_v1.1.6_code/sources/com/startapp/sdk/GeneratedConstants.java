package com.startapp.sdk;

/* compiled from: StartAppSDK */
public class GeneratedConstants {
    public static final String INAPP_FLAVOR = "1111111111";
    public static final String INAPP_PACKAGING = "aar";
    public static final String INAPP_VERSION = "4.5.0";
    public static final String INAPP_VERSION_DECLARATION = "!SDK-VERSION-STRING!:com.startapp.startappsdk:inapp-sdk:4.5.0";
}
