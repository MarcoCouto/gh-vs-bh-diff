package com.startapp.sdk.c.b;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.LocaleList;
import com.startapp.sdk.c.a;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Locale.Category;

/* compiled from: StartAppSDK */
public final class b extends a<a> {
    public b(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object b() {
        return a.a;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0079  */
    public final /* synthetic */ Object a() {
        Locale locale;
        Locale locale2;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Resources resources = this.a.getResources();
        if (resources != null) {
            Configuration configuration = resources.getConfiguration();
            if (configuration != null) {
                locale = configuration.locale;
                if (VERSION.SDK_INT >= 24) {
                    LocaleList locales = configuration.getLocales();
                    if (locales != null && locales.size() > 0) {
                        int size = locales.size();
                        Locale locale3 = locale;
                        boolean z = true;
                        for (int i = 0; i < size; i++) {
                            Locale locale4 = locales.get(i);
                            if (locale4 != null) {
                                if (linkedHashSet.size() < 11) {
                                    linkedHashSet.add(locale4);
                                }
                                if (z) {
                                    locale3 = locale4;
                                    z = false;
                                }
                            }
                        }
                        locale = locale3;
                    }
                }
                if (VERSION.SDK_INT >= 24) {
                    Locale locale5 = Locale.getDefault(Category.DISPLAY);
                    if (locale5 != null) {
                        if (locale == null) {
                            locale = locale5;
                        }
                        if (linkedHashSet.size() < 11) {
                            linkedHashSet.add(locale5);
                        }
                    }
                }
                locale2 = Locale.getDefault();
                if (locale2 != null) {
                    if (locale == null) {
                        locale = locale2;
                    }
                    if (linkedHashSet.size() < 11) {
                        linkedHashSet.add(locale2);
                    }
                }
                if (locale == null) {
                    locale = new Locale("en");
                }
                linkedHashSet.remove(locale);
                return new a(locale, linkedHashSet);
            }
        }
        locale = null;
        if (VERSION.SDK_INT >= 24) {
        }
        locale2 = Locale.getDefault();
        if (locale2 != null) {
        }
        if (locale == null) {
        }
        linkedHashSet.remove(locale);
        return new a(locale, linkedHashSet);
    }
}
