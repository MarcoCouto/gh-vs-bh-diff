package com.startapp.sdk.c.b;

import java.util.Collection;
import java.util.Locale;

/* compiled from: StartAppSDK */
public final class a {
    protected static final a a = new a();
    private final String b;
    private final String c;
    private final String d;

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public a(Locale locale, Collection<Locale> collection) {
        this.b = locale.toString();
        this.c = a(null, collection, ';');
        this.d = a(locale, collection, ',');
    }

    private a() {
        this.b = null;
        this.c = null;
        this.d = null;
    }

    private static String a(Locale locale, Iterable<Locale> iterable, char c2) {
        boolean z;
        StringBuilder sb;
        if (locale != null) {
            sb = new StringBuilder();
            sb.append(locale);
            z = true;
        } else {
            z = false;
            sb = null;
        }
        if (iterable != null) {
            for (Locale locale2 : iterable) {
                if (locale2 != null) {
                    if (sb == null) {
                        sb = new StringBuilder();
                    }
                    if (z) {
                        sb.append(c2);
                    }
                    sb.append(locale2);
                    z = true;
                }
            }
        }
        if (sb != null) {
            return sb.toString();
        }
        return null;
    }
}
