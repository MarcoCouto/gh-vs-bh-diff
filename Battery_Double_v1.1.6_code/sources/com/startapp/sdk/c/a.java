package com.startapp.sdk.c;

import android.content.Context;
import android.os.SystemClock;
import com.startapp.sdk.adsbase.infoevents.e;
import com.tapjoy.TapjoyConstants;

/* compiled from: StartAppSDK */
public abstract class a<T> {
    protected final Context a;
    private volatile T b;
    private volatile long c;
    private final long d;

    /* access modifiers changed from: protected */
    public abstract T a();

    /* access modifiers changed from: protected */
    public abstract T b();

    public a(Context context) {
        this(context, TapjoyConstants.PAID_APP_TIME);
    }

    public a(Context context, long j) {
        this.a = context;
        this.d = j;
    }

    private boolean d() {
        return this.c + this.d < SystemClock.uptimeMillis();
    }

    public final T c() {
        T t = this.b;
        if (t == null || d()) {
            synchronized (this) {
                t = this.b;
                if (t == null || d()) {
                    try {
                        t = a();
                    } catch (Throwable th) {
                        new e(th).a(this.a);
                    }
                    if (t != null) {
                        this.b = t;
                        this.c = SystemClock.uptimeMillis();
                    }
                }
            }
        }
        if (t != null) {
            return t;
        }
        return b();
    }
}
