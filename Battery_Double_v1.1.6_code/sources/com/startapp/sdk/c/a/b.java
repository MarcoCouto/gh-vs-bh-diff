package com.startapp.sdk.c.a;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import com.startapp.sdk.c.a;
import java.util.LinkedHashSet;
import java.util.List;

/* compiled from: StartAppSDK */
public final class b extends a<a> {
    public b(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object b() {
        return a.a;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009d  */
    public final /* synthetic */ Object a() {
        LinkedHashSet linkedHashSet;
        List<InputMethodInfo> inputMethodList;
        if (VERSION.SDK_INT >= 11) {
            Object systemService = this.a.getSystemService("input_method");
            if (systemService instanceof InputMethodManager) {
                InputMethodManager inputMethodManager = (InputMethodManager) systemService;
                InputMethodSubtype currentInputMethodSubtype = inputMethodManager.getCurrentInputMethodSubtype();
                if (currentInputMethodSubtype != null && "keyboard".equals(currentInputMethodSubtype.getMode())) {
                    String locale = currentInputMethodSubtype.getLocale();
                    if (!TextUtils.isEmpty(locale)) {
                        linkedHashSet = new LinkedHashSet();
                        if (linkedHashSet.size() < 10) {
                            linkedHashSet.add(locale);
                        }
                        inputMethodList = inputMethodManager.getInputMethodList();
                        if (inputMethodList != null) {
                            for (InputMethodInfo inputMethodInfo : inputMethodList) {
                                if (inputMethodInfo != null) {
                                    List<InputMethodSubtype> enabledInputMethodSubtypeList = inputMethodManager.getEnabledInputMethodSubtypeList(inputMethodInfo, true);
                                    if (enabledInputMethodSubtypeList != null) {
                                        for (InputMethodSubtype inputMethodSubtype : enabledInputMethodSubtypeList) {
                                            if (inputMethodSubtype != null && "keyboard".equals(inputMethodSubtype.getMode())) {
                                                String locale2 = inputMethodSubtype.getLocale();
                                                if (!TextUtils.isEmpty(locale2)) {
                                                    if (linkedHashSet == null) {
                                                        linkedHashSet = new LinkedHashSet();
                                                    }
                                                    if (linkedHashSet.size() < 10) {
                                                        linkedHashSet.add(locale2);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (linkedHashSet != null) {
                            return new a(linkedHashSet);
                        }
                    }
                }
                linkedHashSet = null;
                inputMethodList = inputMethodManager.getInputMethodList();
                if (inputMethodList != null) {
                }
                if (linkedHashSet != null) {
                }
            }
        }
        return null;
    }
}
