package com.startapp.sdk.c.c;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.SparseArray;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.i.f;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.remoteconfig.LocationConfig;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a extends com.startapp.sdk.c.a<Collection<Location>> {
    private final f<LocationConfig> b;
    private final LocationManager c;

    public a(Context context, f<LocationConfig> fVar) {
        super(context, 120000);
        this.b = fVar;
        this.c = a(context);
        if (a(1)) {
            new e(InfoEventCategory.GENERAL).e("7336e5f982b43e78").f(String.valueOf(d())).a(context);
        }
    }

    private boolean a(int i) {
        LocationConfig locationConfig = (LocationConfig) this.b.a();
        return locationConfig != null && (locationConfig.c() & i) == i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030  */
    private LocationManager a(Context context) {
        LocationManager locationManager;
        Throwable th;
        try {
            locationManager = (LocationManager) context.getSystemService("location");
            if (locationManager == null) {
                try {
                    if (a(4)) {
                        new e(InfoEventCategory.ERROR).e("531467b658bff412").a(context);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (a(2)) {
                        new e(th).a(context);
                    }
                    return locationManager;
                }
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            locationManager = null;
            th = th4;
            if (a(2)) {
            }
            return locationManager;
        }
        return locationManager;
    }

    private List<String> d() {
        if (this.c == null) {
            return null;
        }
        try {
            return this.c.getAllProviders();
        } catch (Throwable th) {
            if (a(8)) {
                new e(th).a(this.a);
            }
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0039  */
    private boolean a(String str, int i) {
        boolean z;
        if (this.c == null) {
            return false;
        }
        try {
            z = this.c.isProviderEnabled(str);
            if (!z) {
                try {
                    if (a(i | 32)) {
                        new e(InfoEventCategory.ERROR).e("90f6b8edddc1d7fc").f(str).a(this.a);
                    }
                } catch (Throwable th) {
                    th = th;
                    if (a(i | 16)) {
                        new e(th).a(this.a);
                    }
                    return z;
                }
            }
        } catch (Throwable th2) {
            th = th2;
            z = false;
            if (a(i | 16)) {
            }
            return z;
        }
        return z;
    }

    @SuppressLint({"MissingPermission"})
    private Location b(String str, int i) {
        if (this.c == null) {
            return null;
        }
        try {
            return this.c.getLastKnownLocation(str);
        } catch (Throwable th) {
            if (a(i | 64)) {
                new e(th).a(this.a);
            }
            return null;
        }
    }

    public static String a(Collection<Location> collection) {
        StringBuilder sb = new StringBuilder();
        for (Location location : collection) {
            if (sb.length() > 0) {
                sb.append(';');
            }
            sb.append(location.getLongitude());
            sb.append(",");
            sb.append(location.getLatitude());
            sb.append(",");
            sb.append(location.getAccuracy());
            sb.append(",");
            sb.append(location.getProvider());
            sb.append(",");
            sb.append(location.getTime());
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object b() {
        return Collections.emptySet();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a() {
        SparseArray sparseArray;
        if (this.c == null) {
            return null;
        }
        LocationConfig locationConfig = (LocationConfig) this.b.a();
        if (locationConfig == null) {
            return null;
        }
        if (locationConfig.a() && b.a(this.a, "android.permission.ACCESS_FINE_LOCATION")) {
            sparseArray = new SparseArray(3);
            sparseArray.put(1048576, "passive");
            sparseArray.put(2097152, "network");
            sparseArray.put(4194304, "gps");
        } else if (!locationConfig.b() || !b.a(this.a, "android.permission.ACCESS_COARSE_LOCATION")) {
            return null;
        } else {
            sparseArray = new SparseArray(1);
            sparseArray.put(2097152, "network");
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(3);
        int size = sparseArray.size();
        for (int i = 0; i < size; i++) {
            String str = (String) sparseArray.valueAt(i);
            int keyAt = sparseArray.keyAt(i);
            if (a(str, keyAt)) {
                Location b2 = b(str, keyAt);
                if (b2 != null) {
                    if (b2.getProvider() != null) {
                        str = b2.getProvider();
                    }
                    linkedHashMap.put(str, b2);
                }
            }
        }
        return Collections.unmodifiableCollection(linkedHashMap.values());
    }
}
