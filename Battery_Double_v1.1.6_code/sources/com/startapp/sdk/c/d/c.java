package com.startapp.sdk.c.d;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityNr;
import android.telephony.CellIdentityTdscdma;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoTdscdma;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.facebook.places.model.PlaceFields;
import com.startapp.sdk.c.a;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* compiled from: StartAppSDK */
public final class c extends a<a> {
    private static final Comparator<CellInfo> b = new Comparator<CellInfo>() {
        public final /* synthetic */ int compare(Object obj, Object obj2) {
            CellInfo cellInfo = (CellInfo) obj;
            CellInfo cellInfo2 = (CellInfo) obj2;
            if (!(cellInfo == null && cellInfo2 == null)) {
                if (cellInfo == null) {
                    return -1;
                }
                if (cellInfo2 == null) {
                    return 1;
                }
                long timeStamp = cellInfo.getTimeStamp();
                long timeStamp2 = cellInfo2.getTimeStamp();
                if (timeStamp < timeStamp2) {
                    return -1;
                }
                if (timeStamp > timeStamp2) {
                    return 1;
                }
            }
            return 0;
        }
    };

    public c(Context context) {
        super(context, 120000);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object b() {
        return b.a;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"MissingPermission"})
    public final /* synthetic */ Object a() {
        boolean z = false;
        if (VERSION.SDK_INT < 23 ? this.a.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0 || this.a.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 : this.a.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0 || this.a.checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            z = true;
        }
        if (!z) {
            return null;
        }
        Object systemService = this.a.getSystemService(PlaceFields.PHONE);
        if (!(systemService instanceof TelephonyManager)) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager) systemService;
        b bVar = new b();
        if (VERSION.SDK_INT >= 17) {
            List<CellInfoNr> allCellInfo = telephonyManager.getAllCellInfo();
            if (allCellInfo != null) {
                Collections.sort(allCellInfo, b);
                for (CellInfoNr cellInfoNr : allCellInfo) {
                    if (cellInfoNr instanceof CellInfoCdma) {
                        CellIdentityCdma cellIdentity = ((CellInfoCdma) cellInfoNr).getCellIdentity();
                        if (cellIdentity != null) {
                            if (cellIdentity.getLatitude() != Integer.MAX_VALUE) {
                                bVar.a(com.startapp.common.b.a.c(String.valueOf(cellIdentity.getLatitude())));
                            }
                            if (cellIdentity.getLongitude() != Integer.MAX_VALUE) {
                                bVar.b(com.startapp.common.b.a.c(String.valueOf(cellIdentity.getLongitude())));
                            }
                        }
                    } else if (cellInfoNr instanceof CellInfoGsm) {
                        CellIdentityGsm cellIdentity2 = ((CellInfoGsm) cellInfoNr).getCellIdentity();
                        if (cellIdentity2 != null) {
                            if (cellIdentity2.getLac() != Integer.MAX_VALUE) {
                                bVar.c(com.startapp.common.b.a.c(String.valueOf(cellIdentity2.getLac())));
                            }
                            if (cellIdentity2.getCid() != Integer.MAX_VALUE) {
                                bVar.d(com.startapp.common.b.a.c(String.valueOf(cellIdentity2.getCid())));
                            }
                        }
                    } else if (cellInfoNr instanceof CellInfoLte) {
                        CellIdentityLte cellIdentity3 = ((CellInfoLte) cellInfoNr).getCellIdentity();
                        if (!(cellIdentity3 == null || cellIdentity3.getTac() == Integer.MAX_VALUE)) {
                            bVar.e(com.startapp.common.b.a.c(String.valueOf(cellIdentity3.getTac())));
                        }
                    } else if (VERSION.SDK_INT >= 29 && (cellInfoNr instanceof CellInfoNr)) {
                        CellIdentityNr cellIdentity4 = cellInfoNr.getCellIdentity();
                        CellIdentityNr cellIdentityNr = cellIdentity4 instanceof CellIdentityNr ? cellIdentity4 : null;
                        if (!(cellIdentityNr == null || cellIdentityNr.getTac() == Integer.MAX_VALUE)) {
                            bVar.e(com.startapp.common.b.a.c(String.valueOf(cellIdentityNr.getTac())));
                        }
                    } else if (VERSION.SDK_INT >= 29 && (cellInfoNr instanceof CellInfoTdscdma)) {
                        CellIdentityTdscdma cellIdentity5 = ((CellInfoTdscdma) cellInfoNr).getCellIdentity();
                        if (cellIdentity5 != null) {
                            if (cellIdentity5.getLac() != Integer.MAX_VALUE) {
                                bVar.c(com.startapp.common.b.a.c(String.valueOf(cellIdentity5.getLac())));
                            }
                            if (cellIdentity5.getCid() != Integer.MAX_VALUE) {
                                bVar.d(com.startapp.common.b.a.c(String.valueOf(cellIdentity5.getCid())));
                            }
                        }
                    } else if (VERSION.SDK_INT >= 18 && (cellInfoNr instanceof CellInfoWcdma)) {
                        CellIdentityWcdma cellIdentity6 = ((CellInfoWcdma) cellInfoNr).getCellIdentity();
                        if (cellIdentity6 != null) {
                            if (cellIdentity6.getLac() != Integer.MAX_VALUE) {
                                bVar.c(com.startapp.common.b.a.c(String.valueOf(cellIdentity6.getLac())));
                            }
                            if (cellIdentity6.getCid() != Integer.MAX_VALUE) {
                                bVar.d(com.startapp.common.b.a.c(String.valueOf(cellIdentity6.getCid())));
                            }
                        }
                    }
                }
            }
        } else {
            CellLocation cellLocation = telephonyManager.getCellLocation();
            if (cellLocation instanceof GsmCellLocation) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                bVar.d(com.startapp.common.b.a.c(String.valueOf(gsmCellLocation.getCid())));
                bVar.c(com.startapp.common.b.a.c(String.valueOf(gsmCellLocation.getLac())));
            } else if (cellLocation instanceof CdmaCellLocation) {
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                bVar.a(com.startapp.common.b.a.c(String.valueOf(cdmaCellLocation.getBaseStationLatitude())));
                bVar.b(com.startapp.common.b.a.c(String.valueOf(cdmaCellLocation.getBaseStationLongitude())));
            }
        }
        return bVar;
    }
}
