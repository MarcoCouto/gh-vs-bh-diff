package com.startapp.b.a.d;

import com.startapp.b.a.c.d;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;

/* compiled from: StartAppSDK */
public final class a implements c {
    private final c a;

    public a(c cVar) {
        this.a = cVar;
    }

    public final String a(String str) {
        GZIPOutputStream gZIPOutputStream;
        GZIPOutputStream gZIPOutputStream2 = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            try {
                gZIPOutputStream.write(str.getBytes());
                d.a(gZIPOutputStream);
                String a2 = this.a.a(com.startapp.b.a.c.a.a(byteArrayOutputStream.toByteArray()));
                d.a(gZIPOutputStream);
                return a2;
            } catch (Exception unused) {
                gZIPOutputStream2 = gZIPOutputStream;
                String str2 = "";
                d.a(gZIPOutputStream2);
                return str2;
            } catch (Throwable th) {
                th = th;
                d.a(gZIPOutputStream);
                throw th;
            }
        } catch (Exception unused2) {
            String str22 = "";
            d.a(gZIPOutputStream2);
            return str22;
        } catch (Throwable th2) {
            th = th2;
            gZIPOutputStream = null;
            d.a(gZIPOutputStream);
            throw th;
        }
    }
}
