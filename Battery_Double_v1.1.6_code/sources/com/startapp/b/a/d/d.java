package com.startapp.b.a.d;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.regex.Pattern;

/* compiled from: StartAppSDK */
public final class d implements c {
    private final Pattern a = Pattern.compile("\\+");
    private final Pattern b = Pattern.compile("/");
    private final Pattern c = Pattern.compile(RequestParameters.EQUAL);
    private final Pattern d = Pattern.compile(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
    private final Pattern e = Pattern.compile("\\*");
    private final Pattern f = Pattern.compile("#");

    public final String a(String str) {
        return this.c.matcher(this.b.matcher(this.a.matcher(str).replaceAll(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR)).replaceAll("*")).replaceAll("#");
    }
}
