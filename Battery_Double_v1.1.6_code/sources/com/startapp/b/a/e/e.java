package com.startapp.b.a.e;

import com.startapp.b.a.a.f;
import java.io.DataInput;
import java.io.IOException;

/* compiled from: StartAppSDK */
public final class e extends d {
    /* access modifiers changed from: protected */
    public final DataInput a(byte[] bArr) {
        DataInput a = super.a(bArr);
        try {
            a.readInt();
            return a;
        } catch (IOException e) {
            throw new RuntimeException("problem incrementInputStreamForBackwordCompatability", e);
        }
    }

    /* access modifiers changed from: protected */
    public final f a(DataInput dataInput) throws IOException {
        long readInt = (long) dataInput.readInt();
        f fVar = new f(readInt << 6);
        a(dataInput, fVar, readInt);
        return fVar;
    }
}
