package com.startapp.b.a.e;

import com.startapp.b.a.a.f;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/* compiled from: StartAppSDK */
public final class b {
    private final c a = new c();

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0054 A[SYNTHETIC, Splitter:B:31:0x0054] */
    public static String a(f fVar) {
        int b = fVar.b();
        int c = fVar.c();
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
            try {
                DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream2);
                int i = b;
                for (int i2 = 0; i2 < c; i2++) {
                    long[] a2 = fVar.a(i2);
                    int i3 = i;
                    int i4 = 0;
                    while (true) {
                        if (i4 >= 4096) {
                            i = i3;
                            break;
                        }
                        int i5 = i3 - 1;
                        if (i3 <= 0) {
                            i = i5;
                            break;
                        }
                        dataOutputStream.writeLong(a2[i4]);
                        i4++;
                        i3 = i5;
                    }
                }
                try {
                    byteArrayOutputStream2.close();
                } catch (IOException unused) {
                }
                return c.a(byteArrayOutputStream2.toByteArray());
            } catch (Exception e) {
                e = e;
                byteArrayOutputStream = byteArrayOutputStream2;
                try {
                    throw new RuntimeException("problem serializing bitSet", e);
                } catch (Throwable th) {
                    th = th;
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (IOException unused2) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                byteArrayOutputStream = byteArrayOutputStream2;
                if (byteArrayOutputStream != null) {
                }
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            throw new RuntimeException("problem serializing bitSet", e);
        }
    }
}
