package com.startapp.b.a.e;

import com.startapp.b.a.a.f;
import java.io.DataInput;
import java.io.IOException;

/* compiled from: StartAppSDK */
public final class a extends d {
    private final int a;
    private final int b;

    public a(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    /* access modifiers changed from: protected */
    public final f a(DataInput dataInput) throws IOException {
        f fVar = new f((long) (this.a * this.b));
        a(dataInput, fVar, (long) fVar.b());
        return fVar;
    }
}
