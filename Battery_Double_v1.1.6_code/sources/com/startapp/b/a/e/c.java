package com.startapp.b.a.e;

/* compiled from: StartAppSDK */
final class c {
    private static final char[] a = "0123456789abcdef".toCharArray();

    c() {
    }

    static String a(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = i * 2;
            cArr[i2] = a[(bArr[i] & 240) >>> 4];
            cArr[i2 + 1] = a[bArr[i] & 15];
        }
        return new String(cArr);
    }

    static byte[] a(String str) {
        if (str.length() % 2 != 0) {
            return null;
        }
        byte[] bArr = new byte[(str.length() / 2)];
        int length = str.length();
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }
}
