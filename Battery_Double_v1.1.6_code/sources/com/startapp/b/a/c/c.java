package com.startapp.b.a.c;

import com.google.android.exoplayer2.C;
import java.nio.charset.Charset;

/* compiled from: StartAppSDK */
public final class c {
    public static final Charset a = Charset.forName("UTF-8");

    static {
        Charset.forName("ISO-8859-1");
        Charset.forName(C.ASCII_NAME);
        Charset.forName("UTF-16");
        Charset.forName("UTF-16BE");
        Charset.forName("UTF-16LE");
    }
}
