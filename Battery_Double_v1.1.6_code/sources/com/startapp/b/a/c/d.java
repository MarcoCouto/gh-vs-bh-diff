package com.startapp.b.a.c;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/* compiled from: StartAppSDK */
public final class d {
    static {
        char c = File.separatorChar;
        PrintWriter printWriter = new PrintWriter(new f(0));
        printWriter.println();
        printWriter.close();
    }

    public static void a(OutputStream outputStream) {
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException unused) {
            }
        }
    }
}
