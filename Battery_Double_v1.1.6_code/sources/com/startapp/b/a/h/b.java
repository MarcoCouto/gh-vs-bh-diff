package com.startapp.b.a.h;

import com.startapp.b.a.a.a;
import com.startapp.b.a.d.c;
import com.startapp.b.a.e.d;

/* compiled from: StartAppSDK */
abstract class b {
    private final a a;
    private final c b;
    private final d c;
    private final a d;

    protected b(a aVar, c cVar, d dVar, a aVar2) {
        this.a = aVar;
        this.b = cVar;
        this.c = dVar;
        this.d = aVar2;
    }

    /* access modifiers changed from: 0000 */
    public final c a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final a b() {
        return this.d;
    }
}
