package com.startapp.b.a.h;

import com.startapp.b.a.a.a;
import java.util.HashMap;
import java.util.Map;

/* compiled from: StartAppSDK */
public final class c {
    private final Map<a, b> a = new HashMap();

    public c() {
        this.a.put(a.ZERO, new g());
        this.a.put(a.THREE, new f());
        this.a.put(a.FOUR, new e());
        this.a.put(a.FIVE, new d());
    }

    public final a a(a aVar) {
        return ((b) this.a.get(aVar)).b();
    }

    public final com.startapp.b.a.d.c b(a aVar) {
        return ((b) this.a.get(aVar)).a();
    }
}
