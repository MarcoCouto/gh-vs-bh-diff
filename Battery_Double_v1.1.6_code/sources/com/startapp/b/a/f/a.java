package com.startapp.b.a.f;

import com.startapp.b.a.a.f;
import com.startapp.b.a.e.b;
import com.startapp.b.a.h.c;

/* compiled from: StartAppSDK */
public final class a {
    private final b a;
    private final c b;

    public a(b bVar, c cVar) {
        this.b = cVar;
        this.a = bVar;
    }

    public final String a(com.startapp.b.a.h.a aVar, f fVar, long j) {
        try {
            String a2 = b.a(fVar);
            com.startapp.b.a.d.c b2 = this.b.b(aVar);
            StringBuilder sb = new StringBuilder();
            sb.append(j);
            sb.append("-");
            sb.append(aVar.a());
            sb.append("-");
            sb.append(b2.a(a2));
            return sb.toString();
        } catch (Throwable unused) {
            return null;
        }
    }
}
