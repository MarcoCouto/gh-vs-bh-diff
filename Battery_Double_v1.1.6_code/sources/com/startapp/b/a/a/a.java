package com.startapp.b.a.a;

import com.startapp.b.a.c.c;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;

/* compiled from: StartAppSDK */
public final class a {
    private final int a;
    private final int b;

    public a(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public final f a(List<String> list) {
        f fVar = new f((long) (this.a * this.b));
        for (String bytes : list) {
            ByteBuffer wrap = ByteBuffer.wrap(bytes.getBytes());
            long[] jArr = new long[this.a];
            long a2 = fVar.a() / ((long) this.a);
            long a3 = a(wrap, wrap.position(), wrap.remaining(), 0);
            long a4 = a(wrap, wrap.position(), wrap.remaining(), a3);
            for (int i = 0; i < this.a; i++) {
                long j = (long) i;
                jArr[i] = (j * a2) + Math.abs(((j * a4) + a3) % a2);
            }
            for (long a5 : jArr) {
                fVar.a(a5);
            }
        }
        return fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x00d1, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 3)) << 24);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x00dd, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 2)) << 16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00e9, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 1)) << 8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00f5, code lost:
        r5 = -4132994306676758123L;
        r3 = (((long) r0.get((r20 + r2) - r5)) ^ r3) * -4132994306676758123L;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0105, code lost:
        r1 = ((r3 >>> 47) ^ r3) * r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x010f, code lost:
        return r1 ^ (r1 >>> 47);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x00b5, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 5)) << 40);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00c3, code lost:
        r3 = r3 ^ (((long) r0.get(((r20 + r2) - r5) + 4)) << 32);
     */
    private static long a(ByteBuffer byteBuffer, int i, int i2, long j) {
        ByteBuffer byteBuffer2 = byteBuffer;
        int i3 = i2;
        long j2 = (j & 4294967295L) ^ (((long) i3) * -4132994306676758123L);
        for (int i4 = 0; i4 < (i3 >> 3); i4++) {
            int i5 = i + (i4 << 3);
            long j3 = ((((long) byteBuffer2.get(i5)) & 255) + ((((long) byteBuffer2.get(i5 + 1)) & 255) << 8) + ((((long) byteBuffer2.get(i5 + 2)) & 255) << 16) + ((((long) byteBuffer2.get(i5 + 3)) & 255) << 24) + ((((long) byteBuffer2.get(i5 + 4)) & 255) << 32) + ((((long) byteBuffer2.get(i5 + 5)) & 255) << 40) + ((((long) byteBuffer2.get(i5 + 6)) & 255) << 48) + ((((long) byteBuffer2.get(i5 + 7)) & 255) << 56)) * -4132994306676758123L;
            j2 = (j2 ^ ((j3 ^ (j3 >>> 47)) * -4132994306676758123L)) * -4132994306676758123L;
        }
        int i6 = i3 & 7;
        switch (i6) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                j2 ^= ((long) byteBuffer2.get(((i + i3) - i6) + 6)) << 48;
                break;
            default:
                long j4 = -4132994306676758123L;
                break;
        }
    }

    public static String a(byte[] bArr) {
        Charset charset = c.a;
        if (bArr == null) {
            return null;
        }
        return new String(bArr, charset);
    }
}
