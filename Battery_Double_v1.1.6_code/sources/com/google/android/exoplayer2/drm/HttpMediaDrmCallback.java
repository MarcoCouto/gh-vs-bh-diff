package com.google.android.exoplayer2.drm;

import android.annotation.TargetApi;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.ExoMediaDrm.KeyRequest;
import com.google.android.exoplayer2.drm.ExoMediaDrm.ProvisionRequest;
import com.google.android.exoplayer2.upstream.DataSourceInputStream;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource.Factory;
import com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;
import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

@TargetApi(18)
public final class HttpMediaDrmCallback implements MediaDrmCallback {
    private static final int MAX_MANUAL_REDIRECTS = 5;
    private final Factory dataSourceFactory;
    private final String defaultLicenseUrl;
    private final boolean forceDefaultLicenseUrl;
    private final Map<String, String> keyRequestProperties;

    public HttpMediaDrmCallback(String str, Factory factory) {
        this(str, false, factory);
    }

    public HttpMediaDrmCallback(String str, boolean z, Factory factory) {
        this.dataSourceFactory = factory;
        this.defaultLicenseUrl = str;
        this.forceDefaultLicenseUrl = z;
        this.keyRequestProperties = new HashMap();
    }

    public void setKeyRequestProperty(String str, String str2) {
        Assertions.checkNotNull(str);
        Assertions.checkNotNull(str2);
        synchronized (this.keyRequestProperties) {
            this.keyRequestProperties.put(str, str2);
        }
    }

    public void clearKeyRequestProperty(String str) {
        Assertions.checkNotNull(str);
        synchronized (this.keyRequestProperties) {
            this.keyRequestProperties.remove(str);
        }
    }

    public void clearAllKeyRequestProperties() {
        synchronized (this.keyRequestProperties) {
            this.keyRequestProperties.clear();
        }
    }

    public byte[] executeProvisionRequest(UUID uuid, ProvisionRequest provisionRequest) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(provisionRequest.getDefaultUrl());
        sb.append("&signedRequest=");
        sb.append(Util.fromUtf8Bytes(provisionRequest.getData()));
        return executePost(this.dataSourceFactory, sb.toString(), new byte[0], null);
    }

    public byte[] executeKeyRequest(UUID uuid, KeyRequest keyRequest, @Nullable String str) throws Exception {
        String defaultUrl = keyRequest.getDefaultUrl();
        if (!TextUtils.isEmpty(defaultUrl)) {
            str = defaultUrl;
        }
        if (this.forceDefaultLicenseUrl || TextUtils.isEmpty(str)) {
            str = this.defaultLicenseUrl;
        }
        HashMap hashMap = new HashMap();
        String str2 = C.PLAYREADY_UUID.equals(uuid) ? "text/xml" : C.CLEARKEY_UUID.equals(uuid) ? "application/json" : "application/octet-stream";
        hashMap.put("Content-Type", str2);
        if (C.PLAYREADY_UUID.equals(uuid)) {
            hashMap.put("SOAPAction", "http://schemas.microsoft.com/DRM/2007/03/protocols/AcquireLicense");
        }
        synchronized (this.keyRequestProperties) {
            hashMap.putAll(this.keyRequestProperties);
        }
        return executePost(this.dataSourceFactory, str, keyRequest.getData(), hashMap);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006c A[Catch:{ InvalidResponseCodeException -> 0x0051, all -> 0x004f }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0074 A[LOOP:1: B:7:0x002e->B:30:0x0074, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007d A[EDGE_INSN: B:31:0x007d->B:32:? ?: BREAK  , SYNTHETIC, Splitter:B:31:0x007d] */
    private static byte[] executePost(Factory factory, String str, byte[] bArr, Map<String, String> map) throws IOException {
        boolean z;
        int i;
        String redirectUrl;
        HttpDataSource createDataSource = factory.createDataSource();
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                createDataSource.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
        }
        String str2 = str;
        int i2 = 0;
        while (true) {
            DataSpec dataSpec = new DataSpec(Uri.parse(str2), bArr, 0, 0, -1, null, 1);
            DataSourceInputStream dataSourceInputStream = new DataSourceInputStream(createDataSource, dataSpec);
            try {
                byte[] byteArray = Util.toByteArray(dataSourceInputStream);
                Util.closeQuietly((Closeable) dataSourceInputStream);
                return byteArray;
            } catch (InvalidResponseCodeException e) {
                InvalidResponseCodeException invalidResponseCodeException = e;
                if (invalidResponseCodeException.responseCode != 307) {
                    if (invalidResponseCodeException.responseCode != 308) {
                        i = i2;
                        z = false;
                        redirectUrl = z ? getRedirectUrl(invalidResponseCodeException) : null;
                        if (redirectUrl != null) {
                            Util.closeQuietly((Closeable) dataSourceInputStream);
                            String str3 = redirectUrl;
                            i2 = i;
                            str2 = str3;
                        } else {
                            throw invalidResponseCodeException;
                        }
                    }
                }
                i = i2 + 1;
                if (i2 < 5) {
                    z = true;
                    if (z) {
                    }
                    if (redirectUrl != null) {
                    }
                }
                z = false;
                if (z) {
                }
                if (redirectUrl != null) {
                }
            } catch (Throwable th) {
                Util.closeQuietly((Closeable) dataSourceInputStream);
                throw th;
            }
        }
    }

    private static String getRedirectUrl(InvalidResponseCodeException invalidResponseCodeException) {
        Map<String, List<String>> map = invalidResponseCodeException.headerFields;
        if (map != null) {
            List list = (List) map.get("Location");
            if (list != null && !list.isEmpty()) {
                return (String) list.get(0);
            }
        }
        return null;
    }
}
