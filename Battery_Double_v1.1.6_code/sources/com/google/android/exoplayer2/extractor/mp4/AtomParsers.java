package com.google.android.exoplayer2.extractor.mp4;

import android.util.Log;
import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ParserException;
import com.google.android.exoplayer2.audio.Ac3Util;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.GaplessInfoHolder;
import com.google.android.exoplayer2.extractor.mp4.FixedSampleSizeRechunker.Results;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.Metadata.Entry;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.CodecSpecificDataUtil;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.ParsableByteArray;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.AvcConfig;
import com.google.android.exoplayer2.video.HevcConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class AtomParsers {
    private static final int MAX_GAPLESS_TRIM_SIZE_SAMPLES = 3;
    private static final String TAG = "AtomParsers";
    private static final int TYPE_clcp = Util.getIntegerCodeForString("clcp");
    private static final int TYPE_meta = Util.getIntegerCodeForString("meta");
    private static final int TYPE_sbtl = Util.getIntegerCodeForString("sbtl");
    private static final int TYPE_soun = Util.getIntegerCodeForString("soun");
    private static final int TYPE_subt = Util.getIntegerCodeForString("subt");
    private static final int TYPE_text = Util.getIntegerCodeForString(MimeTypes.BASE_TYPE_TEXT);
    private static final int TYPE_vide = Util.getIntegerCodeForString("vide");

    private static final class ChunkIterator {
        private final ParsableByteArray chunkOffsets;
        private final boolean chunkOffsetsAreLongs;
        public int index;
        public final int length;
        private int nextSamplesPerChunkChangeIndex;
        public int numSamples;
        public long offset;
        private int remainingSamplesPerChunkChanges;
        private final ParsableByteArray stsc;

        public ChunkIterator(ParsableByteArray parsableByteArray, ParsableByteArray parsableByteArray2, boolean z) {
            this.stsc = parsableByteArray;
            this.chunkOffsets = parsableByteArray2;
            this.chunkOffsetsAreLongs = z;
            parsableByteArray2.setPosition(12);
            this.length = parsableByteArray2.readUnsignedIntToInt();
            parsableByteArray.setPosition(12);
            this.remainingSamplesPerChunkChanges = parsableByteArray.readUnsignedIntToInt();
            boolean z2 = true;
            if (parsableByteArray.readInt() != 1) {
                z2 = false;
            }
            Assertions.checkState(z2, "first_chunk must be 1");
            this.index = -1;
        }

        public boolean moveNext() {
            long j;
            int i = this.index + 1;
            this.index = i;
            if (i == this.length) {
                return false;
            }
            if (this.chunkOffsetsAreLongs) {
                j = this.chunkOffsets.readUnsignedLongToLong();
            } else {
                j = this.chunkOffsets.readUnsignedInt();
            }
            this.offset = j;
            if (this.index == this.nextSamplesPerChunkChangeIndex) {
                this.numSamples = this.stsc.readUnsignedIntToInt();
                this.stsc.skipBytes(4);
                int i2 = this.remainingSamplesPerChunkChanges - 1;
                this.remainingSamplesPerChunkChanges = i2;
                this.nextSamplesPerChunkChangeIndex = i2 > 0 ? this.stsc.readUnsignedIntToInt() - 1 : -1;
            }
            return true;
        }
    }

    private interface SampleSizeBox {
        int getSampleCount();

        boolean isFixedSampleSize();

        int readNextSampleSize();
    }

    private static final class StsdData {
        public static final int STSD_HEADER_SIZE = 8;
        public Format format;
        public int nalUnitLengthFieldLength;
        public int requiredSampleTransformation = 0;
        public final TrackEncryptionBox[] trackEncryptionBoxes;

        public StsdData(int i) {
            this.trackEncryptionBoxes = new TrackEncryptionBox[i];
        }
    }

    static final class StszSampleSizeBox implements SampleSizeBox {
        private final ParsableByteArray data;
        private final int fixedSampleSize = this.data.readUnsignedIntToInt();
        private final int sampleCount = this.data.readUnsignedIntToInt();

        public StszSampleSizeBox(LeafAtom leafAtom) {
            this.data = leafAtom.data;
            this.data.setPosition(12);
        }

        public int getSampleCount() {
            return this.sampleCount;
        }

        public int readNextSampleSize() {
            return this.fixedSampleSize == 0 ? this.data.readUnsignedIntToInt() : this.fixedSampleSize;
        }

        public boolean isFixedSampleSize() {
            return this.fixedSampleSize != 0;
        }
    }

    static final class Stz2SampleSizeBox implements SampleSizeBox {
        private int currentByte;
        private final ParsableByteArray data;
        private final int fieldSize = (this.data.readUnsignedIntToInt() & 255);
        private final int sampleCount = this.data.readUnsignedIntToInt();
        private int sampleIndex;

        public boolean isFixedSampleSize() {
            return false;
        }

        public Stz2SampleSizeBox(LeafAtom leafAtom) {
            this.data = leafAtom.data;
            this.data.setPosition(12);
        }

        public int getSampleCount() {
            return this.sampleCount;
        }

        public int readNextSampleSize() {
            if (this.fieldSize == 8) {
                return this.data.readUnsignedByte();
            }
            if (this.fieldSize == 16) {
                return this.data.readUnsignedShort();
            }
            int i = this.sampleIndex;
            this.sampleIndex = i + 1;
            if (i % 2 != 0) {
                return this.currentByte & 15;
            }
            this.currentByte = this.data.readUnsignedByte();
            return (this.currentByte & PsExtractor.VIDEO_STREAM_MASK) >> 4;
        }
    }

    private static final class TkhdData {
        /* access modifiers changed from: private */
        public final long duration;
        /* access modifiers changed from: private */
        public final int id;
        /* access modifiers changed from: private */
        public final int rotationDegrees;

        public TkhdData(int i, long j, int i2) {
            this.id = i;
            this.duration = j;
            this.rotationDegrees = i2;
        }
    }

    public static final class UnhandledEditListException extends ParserException {
    }

    public static Track parseTrak(ContainerAtom containerAtom, LeafAtom leafAtom, long j, DrmInitData drmInitData, boolean z, boolean z2) throws ParserException {
        long j2;
        LeafAtom leafAtom2;
        long[] jArr;
        long[] jArr2;
        Track track;
        ContainerAtom containerAtom2 = containerAtom;
        ContainerAtom containerAtomOfType = containerAtom2.getContainerAtomOfType(Atom.TYPE_mdia);
        int parseHdlr = parseHdlr(containerAtomOfType.getLeafAtomOfType(Atom.TYPE_hdlr).data);
        if (parseHdlr == -1) {
            return null;
        }
        TkhdData parseTkhd = parseTkhd(containerAtom2.getLeafAtomOfType(Atom.TYPE_tkhd).data);
        long j3 = C.TIME_UNSET;
        if (j == C.TIME_UNSET) {
            j2 = parseTkhd.duration;
            leafAtom2 = leafAtom;
        } else {
            leafAtom2 = leafAtom;
            j2 = j;
        }
        long parseMvhd = parseMvhd(leafAtom2.data);
        if (j2 != C.TIME_UNSET) {
            j3 = Util.scaleLargeTimestamp(j2, 1000000, parseMvhd);
        }
        long j4 = j3;
        ContainerAtom containerAtomOfType2 = containerAtomOfType.getContainerAtomOfType(Atom.TYPE_minf).getContainerAtomOfType(Atom.TYPE_stbl);
        Pair parseMdhd = parseMdhd(containerAtomOfType.getLeafAtomOfType(Atom.TYPE_mdhd).data);
        StsdData parseStsd = parseStsd(containerAtomOfType2.getLeafAtomOfType(Atom.TYPE_stsd).data, parseTkhd.id, parseTkhd.rotationDegrees, (String) parseMdhd.second, drmInitData, z2);
        if (!z) {
            Pair parseEdts = parseEdts(containerAtom2.getContainerAtomOfType(Atom.TYPE_edts));
            jArr = (long[]) parseEdts.second;
            jArr2 = (long[]) parseEdts.first;
        } else {
            jArr2 = null;
            jArr = null;
        }
        if (parseStsd.format == null) {
            track = null;
        } else {
            int access$100 = parseTkhd.id;
            long longValue = ((Long) parseMdhd.first).longValue();
            Format format = parseStsd.format;
            int i = parseStsd.requiredSampleTransformation;
            TrackEncryptionBox[] trackEncryptionBoxArr = parseStsd.trackEncryptionBoxes;
            int i2 = parseStsd.nalUnitLengthFieldLength;
            track = new Track(access$100, parseHdlr, longValue, parseMvhd, j4, format, i, trackEncryptionBoxArr, i2, jArr2, jArr);
        }
        return track;
    }

    /* JADX WARNING: Removed duplicated region for block: B:134:0x033c  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x033e  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x034a  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0389  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x038b  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x038f  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0392  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x0395  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0398  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x039c  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x03a0  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x03a3  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x03ad  */
    public static TrackSampleTable parseStbl(Track track, ContainerAtom containerAtom, GaplessInfoHolder gaplessInfoHolder) throws ParserException {
        SampleSizeBox sampleSizeBox;
        boolean z;
        int i;
        int i2;
        long j;
        int[] iArr;
        long[] jArr;
        int i3;
        int[] iArr2;
        long[] jArr2;
        long j2;
        int i4;
        boolean z2;
        int i5;
        long[] jArr3;
        int i6;
        long[] jArr4;
        int[] iArr3;
        int[] iArr4;
        int[] iArr5;
        int i7;
        int i8;
        int i9;
        Track track2 = track;
        ContainerAtom containerAtom2 = containerAtom;
        GaplessInfoHolder gaplessInfoHolder2 = gaplessInfoHolder;
        LeafAtom leafAtomOfType = containerAtom2.getLeafAtomOfType(Atom.TYPE_stsz);
        if (leafAtomOfType != null) {
            sampleSizeBox = new StszSampleSizeBox(leafAtomOfType);
        } else {
            LeafAtom leafAtomOfType2 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stz2);
            if (leafAtomOfType2 != null) {
                sampleSizeBox = new Stz2SampleSizeBox(leafAtomOfType2);
            } else {
                throw new ParserException("Track has no sample table size information");
            }
        }
        int sampleCount = sampleSizeBox.getSampleCount();
        if (sampleCount == 0) {
            TrackSampleTable trackSampleTable = new TrackSampleTable(track, new long[0], new int[0], 0, new long[0], new int[0], C.TIME_UNSET);
            return trackSampleTable;
        }
        LeafAtom leafAtomOfType3 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stco);
        if (leafAtomOfType3 == null) {
            leafAtomOfType3 = containerAtom2.getLeafAtomOfType(Atom.TYPE_co64);
            z = true;
        } else {
            z = false;
        }
        ParsableByteArray parsableByteArray = leafAtomOfType3.data;
        ParsableByteArray parsableByteArray2 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stsc).data;
        ParsableByteArray parsableByteArray3 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stts).data;
        LeafAtom leafAtomOfType4 = containerAtom2.getLeafAtomOfType(Atom.TYPE_stss);
        ParsableByteArray parsableByteArray4 = leafAtomOfType4 != null ? leafAtomOfType4.data : null;
        LeafAtom leafAtomOfType5 = containerAtom2.getLeafAtomOfType(Atom.TYPE_ctts);
        ParsableByteArray parsableByteArray5 = leafAtomOfType5 != null ? leafAtomOfType5.data : null;
        ChunkIterator chunkIterator = new ChunkIterator(parsableByteArray2, parsableByteArray, z);
        parsableByteArray3.setPosition(12);
        int readUnsignedIntToInt = parsableByteArray3.readUnsignedIntToInt() - 1;
        int readUnsignedIntToInt2 = parsableByteArray3.readUnsignedIntToInt();
        int readUnsignedIntToInt3 = parsableByteArray3.readUnsignedIntToInt();
        if (parsableByteArray5 != null) {
            parsableByteArray5.setPosition(12);
            i = parsableByteArray5.readUnsignedIntToInt();
        } else {
            i = 0;
        }
        int i10 = -1;
        if (parsableByteArray4 != null) {
            parsableByteArray4.setPosition(12);
            i2 = parsableByteArray4.readUnsignedIntToInt();
            if (i2 > 0) {
                i10 = parsableByteArray4.readUnsignedIntToInt() - 1;
            } else {
                parsableByteArray4 = null;
            }
        } else {
            i2 = 0;
        }
        long j3 = 0;
        if (!(sampleSizeBox.isFixedSampleSize() && MimeTypes.AUDIO_RAW.equals(track2.format.sampleMimeType) && readUnsignedIntToInt == 0 && i == 0 && i2 == 0)) {
            long[] jArr5 = new long[sampleCount];
            int[] iArr6 = new int[sampleCount];
            long[] jArr6 = new long[sampleCount];
            int i11 = i2;
            int[] iArr7 = new int[sampleCount];
            int i12 = readUnsignedIntToInt;
            int i13 = readUnsignedIntToInt2;
            ParsableByteArray parsableByteArray6 = parsableByteArray3;
            int i14 = readUnsignedIntToInt3;
            int i15 = i;
            int i16 = i10;
            long j4 = 0;
            int i17 = i11;
            int i18 = 0;
            int i19 = 0;
            int i20 = 0;
            int i21 = 0;
            int i22 = 0;
            long j5 = 0;
            while (i19 < sampleCount) {
                while (i21 == 0) {
                    Assertions.checkState(chunkIterator.moveNext());
                    int i23 = i17;
                    int i24 = i14;
                    long j6 = chunkIterator.offset;
                    i21 = chunkIterator.numSamples;
                    i17 = i23;
                    i14 = i24;
                    j5 = j6;
                }
                int i25 = i17;
                int i26 = i14;
                if (parsableByteArray5 != null) {
                    while (i20 == 0 && i15 > 0) {
                        i20 = parsableByteArray5.readUnsignedIntToInt();
                        i22 = parsableByteArray5.readInt();
                        i15--;
                    }
                    i20--;
                }
                int i27 = i22;
                jArr5[i19] = j5;
                iArr6[i19] = sampleSizeBox.readNextSampleSize();
                if (iArr6[i19] > i18) {
                    i18 = iArr6[i19];
                }
                SampleSizeBox sampleSizeBox2 = sampleSizeBox;
                long[] jArr7 = jArr5;
                jArr6[i19] = ((long) i27) + j4;
                iArr7[i19] = parsableByteArray4 == null ? 1 : 0;
                if (i19 == i16) {
                    iArr7[i19] = 1;
                    int i28 = i25 - 1;
                    if (i28 > 0) {
                        i16 = parsableByteArray4.readUnsignedIntToInt() - 1;
                    }
                    i8 = i16;
                    i7 = i28;
                } else {
                    i8 = i16;
                    i7 = i25;
                }
                int i29 = i26;
                j4 += (long) i29;
                i13--;
                if (i13 == 0) {
                    i9 = i12;
                    if (i9 > 0) {
                        i12 = i9 - 1;
                        i13 = parsableByteArray6.readUnsignedIntToInt();
                        i14 = parsableByteArray6.readInt();
                        j5 += (long) iArr6[i19];
                        i21--;
                        i19++;
                        i22 = i27;
                        sampleSizeBox = sampleSizeBox2;
                        jArr5 = jArr7;
                        i16 = i8;
                        i17 = i7;
                    }
                } else {
                    i9 = i12;
                }
                i14 = i29;
                i12 = i9;
                j5 += (long) iArr6[i19];
                i21--;
                i19++;
                i22 = i27;
                sampleSizeBox = sampleSizeBox2;
                jArr5 = jArr7;
                i16 = i8;
                i17 = i7;
            }
            long[] jArr8 = jArr5;
            int i30 = i17;
            int i31 = i12;
            j = j4 + ((long) i22);
            Assertions.checkArgument(i20 == 0);
            while (i15 > 0) {
                Assertions.checkArgument(parsableByteArray5.readUnsignedIntToInt() == 0);
                parsableByteArray5.readInt();
                i15--;
            }
            if (i30 == 0 && i13 == 0 && i21 == 0 && i31 == 0) {
                i3 = i18;
                track2 = track;
            } else {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Inconsistent stbl box for track ");
                i3 = i18;
                track2 = track;
                sb.append(track2.id);
                sb.append(": remainingSynchronizationSamples ");
                sb.append(i30);
                sb.append(", remainingSamplesAtTimestampDelta ");
                sb.append(i13);
                sb.append(", remainingSamplesInChunk ");
                sb.append(i21);
                sb.append(", remainingTimestampDeltaChanges ");
                sb.append(i31);
                Log.w(str, sb.toString());
            }
            iArr2 = iArr6;
            jArr2 = jArr8;
            long[] jArr9 = jArr6;
            iArr = iArr7;
            jArr = jArr9;
        } else {
            long[] jArr10 = new long[chunkIterator.length];
            int[] iArr8 = new int[chunkIterator.length];
            while (chunkIterator.moveNext()) {
                jArr10[chunkIterator.index] = chunkIterator.offset;
                iArr8[chunkIterator.index] = chunkIterator.numSamples;
            }
            Results rechunk = FixedSampleSizeRechunker.rechunk(Util.getPcmFrameSize(track2.format.pcmEncoding, track2.format.channelCount), jArr10, iArr8, (long) readUnsignedIntToInt3);
            jArr2 = rechunk.offsets;
            iArr2 = rechunk.sizes;
            i3 = rechunk.maximumSize;
            jArr = rechunk.timestamps;
            iArr = rechunk.flags;
            j = rechunk.duration;
        }
        long scaleLargeTimestamp = Util.scaleLargeTimestamp(j, 1000000, track2.timescale);
        if (track2.editListDurations == null || gaplessInfoHolder.hasGaplessInfo()) {
            long[] jArr11 = jArr2;
            int[] iArr9 = iArr2;
            int i32 = i3;
            int[] iArr10 = iArr;
            long j7 = scaleLargeTimestamp;
            Util.scaleLargeTimestampsInPlace(jArr, 1000000, track2.timescale);
            TrackSampleTable trackSampleTable2 = new TrackSampleTable(track, jArr11, iArr9, i32, jArr, iArr10, j7);
            return trackSampleTable2;
        }
        if (track2.editListDurations.length == 1 && track2.type == 1 && jArr.length >= 2) {
            long j8 = track2.editListMediaTimes[0];
            long scaleLargeTimestamp2 = j8 + Util.scaleLargeTimestamp(track2.editListDurations[0], track2.timescale, track2.movieTimescale);
            if (canApplyEditWithGaplessInfo(jArr, j, j8, scaleLargeTimestamp2)) {
                long j9 = j - scaleLargeTimestamp2;
                long scaleLargeTimestamp3 = Util.scaleLargeTimestamp(j8 - jArr[0], (long) track2.format.sampleRate, track2.timescale);
                j2 = j;
                long scaleLargeTimestamp4 = Util.scaleLargeTimestamp(j9, (long) track2.format.sampleRate, track2.timescale);
                if (!(scaleLargeTimestamp3 == 0 && scaleLargeTimestamp4 == 0) && scaleLargeTimestamp3 <= 2147483647L && scaleLargeTimestamp4 <= 2147483647L) {
                    GaplessInfoHolder gaplessInfoHolder3 = gaplessInfoHolder;
                    gaplessInfoHolder3.encoderDelay = (int) scaleLargeTimestamp3;
                    gaplessInfoHolder3.encoderPadding = (int) scaleLargeTimestamp4;
                    Util.scaleLargeTimestampsInPlace(jArr, 1000000, track2.timescale);
                    TrackSampleTable trackSampleTable3 = new TrackSampleTable(track, jArr2, iArr2, i3, jArr, iArr, scaleLargeTimestamp);
                    return trackSampleTable3;
                }
                if (track2.editListDurations.length == 1 || track2.editListDurations[0] != 0) {
                    boolean z3 = track2.type != 1;
                    i4 = 0;
                    boolean z4 = false;
                    int i33 = 0;
                    int i34 = 0;
                    while (i4 < track2.editListDurations.length) {
                        int[] iArr11 = iArr2;
                        int i35 = i3;
                        long j10 = track2.editListMediaTimes[i4];
                        if (j10 != -1) {
                            long scaleLargeTimestamp5 = Util.scaleLargeTimestamp(track2.editListDurations[i4], track2.timescale, track2.movieTimescale);
                            int binarySearchCeil = Util.binarySearchCeil(jArr, j10, true, true);
                            int binarySearchCeil2 = Util.binarySearchCeil(jArr, j10 + scaleLargeTimestamp5, z3, false);
                            i33 += binarySearchCeil2 - binarySearchCeil;
                            boolean z5 = i34 != binarySearchCeil;
                            i34 = binarySearchCeil2;
                            z4 = z5 | z4;
                        }
                        i4++;
                        iArr2 = iArr11;
                        i3 = i35;
                    }
                    int[] iArr12 = iArr2;
                    int i36 = i3;
                    z2 = (i33 == sampleCount) | z4;
                    long[] jArr12 = !z2 ? new long[i33] : jArr2;
                    int[] iArr13 = !z2 ? new int[i33] : iArr12;
                    if (z2) {
                        i36 = 0;
                    }
                    int[] iArr14 = !z2 ? new int[i33] : iArr;
                    long[] jArr13 = new long[i33];
                    i5 = 0;
                    int i37 = 0;
                    while (i5 < track2.editListDurations.length) {
                        long j11 = track2.editListMediaTimes[i5];
                        long j12 = track2.editListDurations[i5];
                        if (j11 != -1) {
                            jArr4 = jArr13;
                            i6 = i5;
                            int[] iArr15 = iArr;
                            int[] iArr16 = iArr14;
                            long scaleLargeTimestamp6 = Util.scaleLargeTimestamp(j12, track2.timescale, track2.movieTimescale) + j11;
                            int binarySearchCeil3 = Util.binarySearchCeil(jArr, j11, true, true);
                            int binarySearchCeil4 = Util.binarySearchCeil(jArr, scaleLargeTimestamp6, z3, false);
                            if (z2) {
                                int i38 = binarySearchCeil4 - binarySearchCeil3;
                                System.arraycopy(jArr2, binarySearchCeil3, jArr12, i37, i38);
                                iArr3 = iArr12;
                                System.arraycopy(iArr3, binarySearchCeil3, iArr13, i37, i38);
                                jArr3 = jArr2;
                                iArr4 = iArr15;
                                iArr5 = iArr16;
                                System.arraycopy(iArr4, binarySearchCeil3, iArr5, i37, i38);
                            } else {
                                jArr3 = jArr2;
                                iArr3 = iArr12;
                                iArr4 = iArr15;
                                iArr5 = iArr16;
                            }
                            if (binarySearchCeil3 < binarySearchCeil4) {
                                if ((iArr5[i37] & 1) == 0) {
                                    Log.w(TAG, "Ignoring edit list: edit does not start with a sync sample.");
                                    throw new UnhandledEditListException();
                                }
                            }
                            int i39 = i36;
                            while (binarySearchCeil3 < binarySearchCeil4) {
                                int i40 = binarySearchCeil4;
                                int i41 = i39;
                                long j13 = j11;
                                jArr4[i37] = Util.scaleLargeTimestamp(j3, 1000000, track2.movieTimescale) + Util.scaleLargeTimestamp(jArr[binarySearchCeil3] - j11, 1000000, track2.timescale);
                                if (z2) {
                                    i39 = i41;
                                    if (iArr13[i37] > i39) {
                                        i39 = iArr3[binarySearchCeil3];
                                    }
                                } else {
                                    i39 = i41;
                                }
                                i37++;
                                binarySearchCeil3++;
                                binarySearchCeil4 = i40;
                                j11 = j13;
                            }
                            i36 = i39;
                        } else {
                            jArr3 = jArr2;
                            iArr5 = iArr14;
                            jArr4 = jArr13;
                            i6 = i5;
                            iArr3 = iArr12;
                            iArr4 = iArr;
                        }
                        j3 += j12;
                        i5 = i6 + 1;
                        iArr14 = iArr5;
                        iArr = iArr4;
                        iArr12 = iArr3;
                        jArr13 = jArr4;
                        jArr2 = jArr3;
                    }
                    TrackSampleTable trackSampleTable4 = new TrackSampleTable(track, jArr12, iArr13, i36, jArr13, iArr14, Util.scaleLargeTimestamp(j3, 1000000, track2.timescale));
                    return trackSampleTable4;
                }
                long j14 = track2.editListMediaTimes[0];
                for (int i42 = 0; i42 < jArr.length; i42++) {
                    jArr[i42] = Util.scaleLargeTimestamp(jArr[i42] - j14, 1000000, track2.timescale);
                }
                TrackSampleTable trackSampleTable5 = new TrackSampleTable(track, jArr2, iArr2, i3, jArr, iArr, Util.scaleLargeTimestamp(j2 - j14, 1000000, track2.timescale));
                return trackSampleTable5;
            }
        }
        j2 = j;
        if (track2.editListDurations.length == 1) {
        }
        if (track2.type != 1) {
        }
        i4 = 0;
        boolean z42 = false;
        int i332 = 0;
        int i342 = 0;
        while (i4 < track2.editListDurations.length) {
        }
        int[] iArr122 = iArr2;
        int i362 = i3;
        z2 = (i332 == sampleCount) | z42;
        if (!z2) {
        }
        if (!z2) {
        }
        if (z2) {
        }
        if (!z2) {
        }
        long[] jArr132 = new long[i332];
        i5 = 0;
        int i372 = 0;
        while (i5 < track2.editListDurations.length) {
        }
        TrackSampleTable trackSampleTable42 = new TrackSampleTable(track, jArr12, iArr13, i362, jArr132, iArr14, Util.scaleLargeTimestamp(j3, 1000000, track2.timescale));
        return trackSampleTable42;
    }

    public static Metadata parseUdta(LeafAtom leafAtom, boolean z) {
        if (z) {
            return null;
        }
        ParsableByteArray parsableByteArray = leafAtom.data;
        parsableByteArray.setPosition(8);
        while (parsableByteArray.bytesLeft() >= 8) {
            int position = parsableByteArray.getPosition();
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_meta) {
                parsableByteArray.setPosition(position);
                return parseMetaAtom(parsableByteArray, position + readInt);
            }
            parsableByteArray.skipBytes(readInt - 8);
        }
        return null;
    }

    private static Metadata parseMetaAtom(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.skipBytes(12);
        while (parsableByteArray.getPosition() < i) {
            int position = parsableByteArray.getPosition();
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_ilst) {
                parsableByteArray.setPosition(position);
                return parseIlst(parsableByteArray, position + readInt);
            }
            parsableByteArray.skipBytes(readInt - 8);
        }
        return null;
    }

    private static Metadata parseIlst(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.skipBytes(8);
        ArrayList arrayList = new ArrayList();
        while (parsableByteArray.getPosition() < i) {
            Entry parseIlstElement = MetadataUtil.parseIlstElement(parsableByteArray);
            if (parseIlstElement != null) {
                arrayList.add(parseIlstElement);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new Metadata((List<? extends Entry>) arrayList);
    }

    private static long parseMvhd(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.setPosition(8);
        if (Atom.parseFullAtomVersion(parsableByteArray.readInt()) != 0) {
            i = 16;
        }
        parsableByteArray.skipBytes(i);
        return parsableByteArray.readUnsignedInt();
    }

    private static TkhdData parseTkhd(ParsableByteArray parsableByteArray) {
        boolean z;
        int i = 8;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        parsableByteArray.skipBytes(parseFullAtomVersion == 0 ? 8 : 16);
        int readInt = parsableByteArray.readInt();
        parsableByteArray.skipBytes(4);
        int position = parsableByteArray.getPosition();
        if (parseFullAtomVersion == 0) {
            i = 4;
        }
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i3 >= i) {
                z = true;
                break;
            } else if (parsableByteArray.data[position + i3] != -1) {
                z = false;
                break;
            } else {
                i3++;
            }
        }
        long j = C.TIME_UNSET;
        if (z) {
            parsableByteArray.skipBytes(i);
        } else {
            long readUnsignedInt = parseFullAtomVersion == 0 ? parsableByteArray.readUnsignedInt() : parsableByteArray.readUnsignedLongToLong();
            if (readUnsignedInt != 0) {
                j = readUnsignedInt;
            }
        }
        parsableByteArray.skipBytes(16);
        int readInt2 = parsableByteArray.readInt();
        int readInt3 = parsableByteArray.readInt();
        parsableByteArray.skipBytes(4);
        int readInt4 = parsableByteArray.readInt();
        int readInt5 = parsableByteArray.readInt();
        if (readInt2 == 0 && readInt3 == 65536 && readInt4 == -65536 && readInt5 == 0) {
            i2 = 90;
        } else if (readInt2 == 0 && readInt3 == -65536 && readInt4 == 65536 && readInt5 == 0) {
            i2 = 270;
        } else if (readInt2 == -65536 && readInt3 == 0 && readInt4 == 0 && readInt5 == -65536) {
            i2 = 180;
        }
        return new TkhdData(readInt, j, i2);
    }

    private static int parseHdlr(ParsableByteArray parsableByteArray) {
        parsableByteArray.setPosition(16);
        int readInt = parsableByteArray.readInt();
        if (readInt == TYPE_soun) {
            return 1;
        }
        if (readInt == TYPE_vide) {
            return 2;
        }
        if (readInt == TYPE_text || readInt == TYPE_sbtl || readInt == TYPE_subt || readInt == TYPE_clcp) {
            return 3;
        }
        return readInt == TYPE_meta ? 4 : -1;
    }

    private static Pair<Long, String> parseMdhd(ParsableByteArray parsableByteArray) {
        int i = 8;
        parsableByteArray.setPosition(8);
        int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
        parsableByteArray.skipBytes(parseFullAtomVersion == 0 ? 8 : 16);
        long readUnsignedInt = parsableByteArray.readUnsignedInt();
        if (parseFullAtomVersion == 0) {
            i = 4;
        }
        parsableByteArray.skipBytes(i);
        int readUnsignedShort = parsableByteArray.readUnsignedShort();
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append((char) (((readUnsignedShort >> 10) & 31) + 96));
        sb.append((char) (((readUnsignedShort >> 5) & 31) + 96));
        sb.append((char) ((readUnsignedShort & 31) + 96));
        return Pair.create(Long.valueOf(readUnsignedInt), sb.toString());
    }

    private static StsdData parseStsd(ParsableByteArray parsableByteArray, int i, int i2, String str, DrmInitData drmInitData, boolean z) throws ParserException {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        parsableByteArray2.setPosition(12);
        int readInt = parsableByteArray.readInt();
        StsdData stsdData = new StsdData(readInt);
        for (int i3 = 0; i3 < readInt; i3++) {
            int position = parsableByteArray.getPosition();
            int readInt2 = parsableByteArray.readInt();
            Assertions.checkArgument(readInt2 > 0, "childAtomSize should be positive");
            int readInt3 = parsableByteArray.readInt();
            if (readInt3 == Atom.TYPE_avc1 || readInt3 == Atom.TYPE_avc3 || readInt3 == Atom.TYPE_encv || readInt3 == Atom.TYPE_mp4v || readInt3 == Atom.TYPE_hvc1 || readInt3 == Atom.TYPE_hev1 || readInt3 == Atom.TYPE_s263 || readInt3 == Atom.TYPE_vp08 || readInt3 == Atom.TYPE_vp09) {
                parseVideoSampleEntry(parsableByteArray, readInt3, position, readInt2, i, i2, drmInitData, stsdData, i3);
            } else if (readInt3 == Atom.TYPE_mp4a || readInt3 == Atom.TYPE_enca || readInt3 == Atom.TYPE_ac_3 || readInt3 == Atom.TYPE_ec_3 || readInt3 == Atom.TYPE_dtsc || readInt3 == Atom.TYPE_dtse || readInt3 == Atom.TYPE_dtsh || readInt3 == Atom.TYPE_dtsl || readInt3 == Atom.TYPE_samr || readInt3 == Atom.TYPE_sawb || readInt3 == Atom.TYPE_lpcm || readInt3 == Atom.TYPE_sowt || readInt3 == Atom.TYPE__mp3 || readInt3 == Atom.TYPE_alac) {
                parseAudioSampleEntry(parsableByteArray, readInt3, position, readInt2, i, str, z, drmInitData, stsdData, i3);
            } else if (readInt3 == Atom.TYPE_TTML || readInt3 == Atom.TYPE_tx3g || readInt3 == Atom.TYPE_wvtt || readInt3 == Atom.TYPE_stpp || readInt3 == Atom.TYPE_c608) {
                parseTextSampleEntry(parsableByteArray, readInt3, position, readInt2, i, str, stsdData);
            } else if (readInt3 == Atom.TYPE_camm) {
                stsdData.format = Format.createSampleFormat(Integer.toString(i), MimeTypes.APPLICATION_CAMERA_MOTION, null, -1, null);
            }
            parsableByteArray2.setPosition(position + readInt2);
        }
        return stsdData;
    }

    private static void parseTextSampleEntry(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, String str, StsdData stsdData) throws ParserException {
        String str2;
        String str3;
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int i5 = i;
        StsdData stsdData2 = stsdData;
        parsableByteArray2.setPosition(i2 + 8 + 8);
        List list = null;
        long j = Long.MAX_VALUE;
        if (i5 == Atom.TYPE_TTML) {
            str2 = MimeTypes.APPLICATION_TTML;
        } else if (i5 == Atom.TYPE_tx3g) {
            String str4 = MimeTypes.APPLICATION_TX3G;
            int i6 = (i3 - 8) - 8;
            byte[] bArr = new byte[i6];
            parsableByteArray2.readBytes(bArr, 0, i6);
            list = Collections.singletonList(bArr);
            str3 = str4;
            stsdData2.format = Format.createTextSampleFormat(Integer.toString(i4), str3, null, -1, 0, str, -1, null, j, list);
        } else if (i5 == Atom.TYPE_wvtt) {
            str2 = MimeTypes.APPLICATION_MP4VTT;
        } else if (i5 == Atom.TYPE_stpp) {
            str2 = MimeTypes.APPLICATION_TTML;
            j = 0;
        } else if (i5 == Atom.TYPE_c608) {
            str2 = MimeTypes.APPLICATION_MP4CEA608;
            stsdData2.requiredSampleTransformation = 1;
        } else {
            throw new IllegalStateException();
        }
        str3 = str2;
        stsdData2.format = Format.createTextSampleFormat(Integer.toString(i4), str3, null, -1, 0, str, -1, null, j, list);
    }

    /* JADX WARNING: Removed duplicated region for block: B:74:0x0154 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0155  */
    private static void parseVideoSampleEntry(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, int i5, DrmInitData drmInitData, StsdData stsdData, int i6) throws ParserException {
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int i7 = i2;
        int i8 = i3;
        DrmInitData drmInitData2 = drmInitData;
        StsdData stsdData2 = stsdData;
        parsableByteArray2.setPosition(i7 + 8 + 8);
        parsableByteArray2.skipBytes(16);
        int readUnsignedShort = parsableByteArray.readUnsignedShort();
        int readUnsignedShort2 = parsableByteArray.readUnsignedShort();
        parsableByteArray2.skipBytes(50);
        int position = parsableByteArray.getPosition();
        String str = null;
        int i9 = i;
        if (i9 == Atom.TYPE_encv) {
            Pair parseSampleEntryEncryptionData = parseSampleEntryEncryptionData(parsableByteArray2, i7, i8);
            if (parseSampleEntryEncryptionData != null) {
                i9 = ((Integer) parseSampleEntryEncryptionData.first).intValue();
                if (drmInitData2 == null) {
                    drmInitData2 = null;
                } else {
                    drmInitData2 = drmInitData2.copyWithSchemeType(((TrackEncryptionBox) parseSampleEntryEncryptionData.second).schemeType);
                }
                stsdData2.trackEncryptionBoxes[i6] = (TrackEncryptionBox) parseSampleEntryEncryptionData.second;
            }
            parsableByteArray2.setPosition(position);
        }
        DrmInitData drmInitData3 = drmInitData2;
        List list = null;
        byte[] bArr = null;
        boolean z = false;
        float f = 1.0f;
        int i10 = -1;
        while (position - i7 < i8) {
            parsableByteArray2.setPosition(position);
            int position2 = parsableByteArray.getPosition();
            int readInt = parsableByteArray.readInt();
            if (readInt != 0 || parsableByteArray.getPosition() - i7 != i8) {
                Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
                int readInt2 = parsableByteArray.readInt();
                if (readInt2 == Atom.TYPE_avcC) {
                    Assertions.checkState(str == null);
                    str = MimeTypes.VIDEO_H264;
                    parsableByteArray2.setPosition(position2 + 8);
                    AvcConfig parse = AvcConfig.parse(parsableByteArray);
                    list = parse.initializationData;
                    stsdData2.nalUnitLengthFieldLength = parse.nalUnitLengthFieldLength;
                    if (!z) {
                        f = parse.pixelWidthAspectRatio;
                    }
                } else if (readInt2 == Atom.TYPE_hvcC) {
                    Assertions.checkState(str == null);
                    str = MimeTypes.VIDEO_H265;
                    parsableByteArray2.setPosition(position2 + 8);
                    HevcConfig parse2 = HevcConfig.parse(parsableByteArray);
                    list = parse2.initializationData;
                    stsdData2.nalUnitLengthFieldLength = parse2.nalUnitLengthFieldLength;
                } else if (readInt2 == Atom.TYPE_vpcC) {
                    Assertions.checkState(str == null);
                    str = i9 == Atom.TYPE_vp08 ? MimeTypes.VIDEO_VP8 : MimeTypes.VIDEO_VP9;
                } else if (readInt2 == Atom.TYPE_d263) {
                    Assertions.checkState(str == null);
                    str = MimeTypes.VIDEO_H263;
                } else if (readInt2 == Atom.TYPE_esds) {
                    Assertions.checkState(str == null);
                    Pair parseEsdsFromParent = parseEsdsFromParent(parsableByteArray2, position2);
                    str = (String) parseEsdsFromParent.first;
                    list = Collections.singletonList(parseEsdsFromParent.second);
                } else if (readInt2 == Atom.TYPE_pasp) {
                    f = parsePaspFromParent(parsableByteArray2, position2);
                    z = true;
                } else if (readInt2 == Atom.TYPE_sv3d) {
                    bArr = parseProjFromParent(parsableByteArray2, position2, readInt);
                } else if (readInt2 == Atom.TYPE_st3d) {
                    int readUnsignedByte = parsableByteArray.readUnsignedByte();
                    parsableByteArray2.skipBytes(3);
                    if (readUnsignedByte == 0) {
                        switch (parsableByteArray.readUnsignedByte()) {
                            case 0:
                                i10 = 0;
                                break;
                            case 1:
                                i10 = 1;
                                break;
                            case 2:
                                i10 = 2;
                                break;
                            case 3:
                                i10 = 3;
                                break;
                        }
                    }
                }
                position += readInt;
            } else if (str == null) {
                stsdData2.format = Format.createVideoSampleFormat(Integer.toString(i4), str, null, -1, -1, readUnsignedShort, readUnsignedShort2, -1.0f, list, i5, f, bArr, i10, null, drmInitData3);
                return;
            } else {
                return;
            }
        }
        if (str == null) {
        }
    }

    private static Pair<long[], long[]> parseEdts(ContainerAtom containerAtom) {
        if (containerAtom != null) {
            LeafAtom leafAtomOfType = containerAtom.getLeafAtomOfType(Atom.TYPE_elst);
            if (leafAtomOfType != null) {
                ParsableByteArray parsableByteArray = leafAtomOfType.data;
                parsableByteArray.setPosition(8);
                int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
                int readUnsignedIntToInt = parsableByteArray.readUnsignedIntToInt();
                long[] jArr = new long[readUnsignedIntToInt];
                long[] jArr2 = new long[readUnsignedIntToInt];
                int i = 0;
                while (i < readUnsignedIntToInt) {
                    jArr[i] = parseFullAtomVersion == 1 ? parsableByteArray.readUnsignedLongToLong() : parsableByteArray.readUnsignedInt();
                    jArr2[i] = parseFullAtomVersion == 1 ? parsableByteArray.readLong() : (long) parsableByteArray.readInt();
                    if (parsableByteArray.readShort() == 1) {
                        parsableByteArray.skipBytes(2);
                        i++;
                    } else {
                        throw new IllegalArgumentException("Unsupported media rate.");
                    }
                }
                return Pair.create(jArr, jArr2);
            }
        }
        return Pair.create(null, null);
    }

    private static float parsePaspFromParent(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.setPosition(i + 8);
        return ((float) parsableByteArray.readUnsignedIntToInt()) / ((float) parsableByteArray.readUnsignedIntToInt());
    }

    private static void parseAudioSampleEntry(ParsableByteArray parsableByteArray, int i, int i2, int i3, int i4, String str, boolean z, DrmInitData drmInitData, StsdData stsdData, int i5) throws ParserException {
        int i6;
        int i7;
        int i8;
        List list;
        String str2;
        DrmInitData drmInitData2;
        int i9;
        int i10;
        StsdData stsdData2;
        int i11;
        String str3;
        int i12;
        int i13;
        ParsableByteArray parsableByteArray2 = parsableByteArray;
        int i14 = i2;
        int i15 = i3;
        String str4 = str;
        DrmInitData drmInitData3 = drmInitData;
        StsdData stsdData3 = stsdData;
        parsableByteArray2.setPosition(i14 + 8 + 8);
        if (z) {
            i6 = parsableByteArray.readUnsignedShort();
            parsableByteArray2.skipBytes(6);
        } else {
            parsableByteArray2.skipBytes(8);
            i6 = 0;
        }
        if (i6 == 0 || i6 == 1) {
            i7 = parsableByteArray.readUnsignedShort();
            parsableByteArray2.skipBytes(6);
            i8 = parsableByteArray.readUnsignedFixedPoint1616();
            if (i6 == 1) {
                parsableByteArray2.skipBytes(16);
            }
        } else if (i6 == 2) {
            parsableByteArray2.skipBytes(16);
            i8 = (int) Math.round(parsableByteArray.readDouble());
            int readUnsignedIntToInt = parsableByteArray.readUnsignedIntToInt();
            parsableByteArray2.skipBytes(20);
            i7 = readUnsignedIntToInt;
        } else {
            return;
        }
        int position = parsableByteArray.getPosition();
        int i16 = i;
        if (i16 == Atom.TYPE_enca) {
            Pair parseSampleEntryEncryptionData = parseSampleEntryEncryptionData(parsableByteArray2, i14, i15);
            if (parseSampleEntryEncryptionData != null) {
                i16 = ((Integer) parseSampleEntryEncryptionData.first).intValue();
                if (drmInitData3 == null) {
                    drmInitData3 = null;
                } else {
                    drmInitData3 = drmInitData3.copyWithSchemeType(((TrackEncryptionBox) parseSampleEntryEncryptionData.second).schemeType);
                }
                stsdData3.trackEncryptionBoxes[i5] = (TrackEncryptionBox) parseSampleEntryEncryptionData.second;
            }
            parsableByteArray2.setPosition(position);
        }
        DrmInitData drmInitData4 = drmInitData3;
        String str5 = i16 == Atom.TYPE_ac_3 ? MimeTypes.AUDIO_AC3 : i16 == Atom.TYPE_ec_3 ? MimeTypes.AUDIO_E_AC3 : i16 == Atom.TYPE_dtsc ? MimeTypes.AUDIO_DTS : (i16 == Atom.TYPE_dtsh || i16 == Atom.TYPE_dtsl) ? MimeTypes.AUDIO_DTS_HD : i16 == Atom.TYPE_dtse ? MimeTypes.AUDIO_DTS_EXPRESS : i16 == Atom.TYPE_samr ? MimeTypes.AUDIO_AMR_NB : i16 == Atom.TYPE_sawb ? MimeTypes.AUDIO_AMR_WB : (i16 == Atom.TYPE_lpcm || i16 == Atom.TYPE_sowt) ? MimeTypes.AUDIO_RAW : i16 == Atom.TYPE__mp3 ? MimeTypes.AUDIO_MPEG : i16 == Atom.TYPE_alac ? MimeTypes.AUDIO_ALAC : null;
        int i17 = i8;
        int i18 = position;
        int i19 = i7;
        byte[] bArr = null;
        String str6 = str5;
        while (i18 - i14 < i15) {
            parsableByteArray2.setPosition(i18);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            int readInt2 = parsableByteArray.readInt();
            if (readInt2 == Atom.TYPE_esds || (z && readInt2 == Atom.TYPE_wave)) {
                i10 = readInt;
                String str7 = str6;
                i9 = i18;
                drmInitData2 = drmInitData4;
                stsdData2 = stsdData3;
                if (readInt2 == Atom.TYPE_esds) {
                    i11 = i9;
                } else {
                    i11 = findEsdsPosition(parsableByteArray2, i9, i10);
                }
                if (i11 != -1) {
                    Pair parseEsdsFromParent = parseEsdsFromParent(parsableByteArray2, i11);
                    str3 = (String) parseEsdsFromParent.first;
                    bArr = (byte[]) parseEsdsFromParent.second;
                    if (MimeTypes.AUDIO_AAC.equals(str3)) {
                        Pair parseAacAudioSpecificConfig = CodecSpecificDataUtil.parseAacAudioSpecificConfig(bArr);
                        i17 = ((Integer) parseAacAudioSpecificConfig.first).intValue();
                        i19 = ((Integer) parseAacAudioSpecificConfig.second).intValue();
                    }
                } else {
                    str3 = str7;
                }
                str2 = str3;
            } else {
                if (readInt2 == Atom.TYPE_dac3) {
                    parsableByteArray2.setPosition(i18 + 8);
                    stsdData3.format = Ac3Util.parseAc3AnnexFFormat(parsableByteArray2, Integer.toString(i4), str4, drmInitData4);
                } else if (readInt2 == Atom.TYPE_dec3) {
                    parsableByteArray2.setPosition(i18 + 8);
                    stsdData3.format = Ac3Util.parseEAc3AnnexFFormat(parsableByteArray2, Integer.toString(i4), str4, drmInitData4);
                } else {
                    if (readInt2 == Atom.TYPE_ddts) {
                        i12 = readInt;
                        str2 = str6;
                        i13 = i18;
                        drmInitData2 = drmInitData4;
                        stsdData2 = stsdData3;
                        stsdData2.format = Format.createAudioSampleFormat(Integer.toString(i4), str6, null, -1, -1, i19, i17, null, drmInitData2, 0, str);
                    } else {
                        i12 = readInt;
                        str2 = str6;
                        i13 = i18;
                        drmInitData2 = drmInitData4;
                        stsdData2 = stsdData3;
                        if (readInt2 == Atom.TYPE_alac) {
                            i10 = i12;
                            byte[] bArr2 = new byte[i10];
                            i9 = i13;
                            parsableByteArray2.setPosition(i9);
                            parsableByteArray2.readBytes(bArr2, 0, i10);
                            bArr = bArr2;
                        }
                    }
                    i10 = i12;
                    i9 = i13;
                }
                i10 = readInt;
                str2 = str6;
                i9 = i18;
                drmInitData2 = drmInitData4;
                stsdData2 = stsdData3;
            }
            i18 = i9 + i10;
            stsdData3 = stsdData2;
            drmInitData4 = drmInitData2;
            str6 = str2;
            i15 = i3;
        }
        String str8 = str6;
        DrmInitData drmInitData5 = drmInitData4;
        StsdData stsdData4 = stsdData3;
        if (stsdData4.format == null) {
            String str9 = str8;
            if (str9 != null) {
                int i20 = MimeTypes.AUDIO_RAW.equals(str9) ? 2 : -1;
                String num = Integer.toString(i4);
                if (bArr == null) {
                    list = null;
                } else {
                    list = Collections.singletonList(bArr);
                }
                stsdData4.format = Format.createAudioSampleFormat(num, str9, null, -1, -1, i19, i17, i20, list, drmInitData5, 0, str);
            }
        }
    }

    private static int findEsdsPosition(ParsableByteArray parsableByteArray, int i, int i2) {
        int position = parsableByteArray.getPosition();
        while (position - i < i2) {
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (parsableByteArray.readInt() == Atom.TYPE_esds) {
                return position;
            }
            position += readInt;
        }
        return -1;
    }

    private static Pair<String, byte[]> parseEsdsFromParent(ParsableByteArray parsableByteArray, int i) {
        parsableByteArray.setPosition(i + 8 + 4);
        parsableByteArray.skipBytes(1);
        parseExpandableClassSize(parsableByteArray);
        parsableByteArray.skipBytes(2);
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        if ((readUnsignedByte & 128) != 0) {
            parsableByteArray.skipBytes(2);
        }
        if ((readUnsignedByte & 64) != 0) {
            parsableByteArray.skipBytes(parsableByteArray.readUnsignedShort());
        }
        if ((readUnsignedByte & 32) != 0) {
            parsableByteArray.skipBytes(2);
        }
        parsableByteArray.skipBytes(1);
        parseExpandableClassSize(parsableByteArray);
        String mimeTypeFromMp4ObjectType = MimeTypes.getMimeTypeFromMp4ObjectType(parsableByteArray.readUnsignedByte());
        if (MimeTypes.AUDIO_MPEG.equals(mimeTypeFromMp4ObjectType) || MimeTypes.AUDIO_DTS.equals(mimeTypeFromMp4ObjectType) || MimeTypes.AUDIO_DTS_HD.equals(mimeTypeFromMp4ObjectType)) {
            return Pair.create(mimeTypeFromMp4ObjectType, null);
        }
        parsableByteArray.skipBytes(12);
        parsableByteArray.skipBytes(1);
        int parseExpandableClassSize = parseExpandableClassSize(parsableByteArray);
        byte[] bArr = new byte[parseExpandableClassSize];
        parsableByteArray.readBytes(bArr, 0, parseExpandableClassSize);
        return Pair.create(mimeTypeFromMp4ObjectType, bArr);
    }

    private static Pair<Integer, TrackEncryptionBox> parseSampleEntryEncryptionData(ParsableByteArray parsableByteArray, int i, int i2) {
        int position = parsableByteArray.getPosition();
        while (position - i < i2) {
            parsableByteArray.setPosition(position);
            int readInt = parsableByteArray.readInt();
            Assertions.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (parsableByteArray.readInt() == Atom.TYPE_sinf) {
                Pair<Integer, TrackEncryptionBox> parseCommonEncryptionSinfFromParent = parseCommonEncryptionSinfFromParent(parsableByteArray, position, readInt);
                if (parseCommonEncryptionSinfFromParent != null) {
                    return parseCommonEncryptionSinfFromParent;
                }
            }
            position += readInt;
        }
        return null;
    }

    static Pair<Integer, TrackEncryptionBox> parseCommonEncryptionSinfFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        String str = null;
        Object obj = null;
        int i4 = -1;
        int i5 = 0;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            int readInt2 = parsableByteArray.readInt();
            if (readInt2 == Atom.TYPE_frma) {
                obj = Integer.valueOf(parsableByteArray.readInt());
            } else if (readInt2 == Atom.TYPE_schm) {
                parsableByteArray.skipBytes(4);
                str = parsableByteArray.readString(4);
            } else if (readInt2 == Atom.TYPE_schi) {
                i4 = i3;
                i5 = readInt;
            }
            i3 += readInt;
        }
        if (!C.CENC_TYPE_cenc.equals(str) && !C.CENC_TYPE_cbc1.equals(str) && !C.CENC_TYPE_cens.equals(str) && !C.CENC_TYPE_cbcs.equals(str)) {
            return null;
        }
        boolean z = true;
        Assertions.checkArgument(obj != null, "frma atom is mandatory");
        Assertions.checkArgument(i4 != -1, "schi atom is mandatory");
        TrackEncryptionBox parseSchiFromParent = parseSchiFromParent(parsableByteArray, i4, i5, str);
        if (parseSchiFromParent == null) {
            z = false;
        }
        Assertions.checkArgument(z, "tenc atom is mandatory");
        return Pair.create(obj, parseSchiFromParent);
    }

    private static TrackEncryptionBox parseSchiFromParent(ParsableByteArray parsableByteArray, int i, int i2, String str) {
        int i3;
        int i4;
        int i5 = i + 8;
        while (true) {
            byte[] bArr = null;
            if (i5 - i >= i2) {
                return null;
            }
            parsableByteArray.setPosition(i5);
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_tenc) {
                int parseFullAtomVersion = Atom.parseFullAtomVersion(parsableByteArray.readInt());
                parsableByteArray.skipBytes(1);
                if (parseFullAtomVersion == 0) {
                    parsableByteArray.skipBytes(1);
                    i4 = 0;
                    i3 = 0;
                } else {
                    int readUnsignedByte = parsableByteArray.readUnsignedByte();
                    i3 = readUnsignedByte & 15;
                    i4 = (readUnsignedByte & PsExtractor.VIDEO_STREAM_MASK) >> 4;
                }
                boolean z = parsableByteArray.readUnsignedByte() == 1;
                int readUnsignedByte2 = parsableByteArray.readUnsignedByte();
                byte[] bArr2 = new byte[16];
                parsableByteArray.readBytes(bArr2, 0, bArr2.length);
                if (z && readUnsignedByte2 == 0) {
                    int readUnsignedByte3 = parsableByteArray.readUnsignedByte();
                    bArr = new byte[readUnsignedByte3];
                    parsableByteArray.readBytes(bArr, 0, readUnsignedByte3);
                }
                TrackEncryptionBox trackEncryptionBox = new TrackEncryptionBox(z, str, readUnsignedByte2, bArr2, i4, i3, bArr);
                return trackEncryptionBox;
            }
            i5 += readInt;
        }
    }

    private static byte[] parseProjFromParent(ParsableByteArray parsableByteArray, int i, int i2) {
        int i3 = i + 8;
        while (i3 - i < i2) {
            parsableByteArray.setPosition(i3);
            int readInt = parsableByteArray.readInt();
            if (parsableByteArray.readInt() == Atom.TYPE_proj) {
                return Arrays.copyOfRange(parsableByteArray.data, i3, readInt + i3);
            }
            i3 += readInt;
        }
        return null;
    }

    private static int parseExpandableClassSize(ParsableByteArray parsableByteArray) {
        int readUnsignedByte = parsableByteArray.readUnsignedByte();
        int i = readUnsignedByte & 127;
        while ((readUnsignedByte & 128) == 128) {
            readUnsignedByte = parsableByteArray.readUnsignedByte();
            i = (i << 7) | (readUnsignedByte & 127);
        }
        return i;
    }

    private static boolean canApplyEditWithGaplessInfo(long[] jArr, long j, long j2, long j3) {
        int length = jArr.length - 1;
        int constrainValue = Util.constrainValue(3, 0, length);
        int constrainValue2 = Util.constrainValue(jArr.length - 3, 0, length);
        if (jArr[0] > j2 || j2 >= jArr[constrainValue] || jArr[constrainValue2] >= j3 || j3 > j) {
            return false;
        }
        return true;
    }

    private AtomParsers() {
    }
}
