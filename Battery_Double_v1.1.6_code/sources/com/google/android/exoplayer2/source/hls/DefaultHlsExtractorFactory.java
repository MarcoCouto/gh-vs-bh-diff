package com.google.android.exoplayer2.source.hls;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor;
import com.google.android.exoplayer2.extractor.ts.Ac3Extractor;
import com.google.android.exoplayer2.extractor.ts.AdtsExtractor;
import com.google.android.exoplayer2.extractor.ts.DefaultTsPayloadReaderFactory;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.TimestampAdjuster;
import java.util.Collections;
import java.util.List;

public final class DefaultHlsExtractorFactory implements HlsExtractorFactory {
    public static final String AAC_FILE_EXTENSION = ".aac";
    public static final String AC3_FILE_EXTENSION = ".ac3";
    public static final String EC3_FILE_EXTENSION = ".ec3";
    public static final String M4_FILE_EXTENSION_PREFIX = ".m4";
    public static final String MP3_FILE_EXTENSION = ".mp3";
    public static final String MP4_FILE_EXTENSION = ".mp4";
    public static final String MP4_FILE_EXTENSION_PREFIX = ".mp4";
    public static final String VTT_FILE_EXTENSION = ".vtt";
    public static final String WEBVTT_FILE_EXTENSION = ".webvtt";

    /* JADX WARNING: type inference failed for: r9v0, types: [com.google.android.exoplayer2.extractor.Extractor] */
    /* JADX WARNING: type inference failed for: r9v1, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v3, types: [com.google.android.exoplayer2.source.hls.WebvttExtractor] */
    /* JADX WARNING: type inference failed for: r9v4, types: [com.google.android.exoplayer2.extractor.ts.Ac3Extractor] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor] */
    /* JADX WARNING: type inference failed for: r10v6, types: [com.google.android.exoplayer2.extractor.ts.TsExtractor] */
    /* JADX WARNING: type inference failed for: r9v16 */
    /* JADX WARNING: type inference failed for: r9v21, types: [com.google.android.exoplayer2.extractor.mp3.Mp3Extractor] */
    /* JADX WARNING: type inference failed for: r9v22 */
    /* JADX WARNING: type inference failed for: r9v23, types: [com.google.android.exoplayer2.extractor.ts.AdtsExtractor] */
    /* JADX WARNING: type inference failed for: r9v24 */
    /* JADX WARNING: type inference failed for: r9v25 */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor] */
    /* JADX WARNING: type inference failed for: r9v26 */
    /* JADX WARNING: type inference failed for: r9v27 */
    /* JADX WARNING: type inference failed for: r9v28 */
    /* JADX WARNING: Incorrect type for immutable var: ssa=com.google.android.exoplayer2.extractor.Extractor, code=null, for r9v0, types: [com.google.android.exoplayer2.extractor.Extractor] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r9v16
  assigns: [?[OBJECT, ARRAY], com.google.android.exoplayer2.extractor.Extractor, com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor]
  uses: [java.lang.Object, ?[int, boolean, OBJECT, ARRAY, byte, short, char], com.google.android.exoplayer2.extractor.mp4.FragmentedMp4Extractor]
  mth insns count: 82
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 6 */
    public Pair<Extractor, Boolean> createExtractor(Extractor r9, Uri uri, Format format, List<Format> list, DrmInitData drmInitData, TimestampAdjuster timestampAdjuster) {
        String lastPathSegment = uri.getLastPathSegment();
        if (lastPathSegment == null) {
            lastPathSegment = "";
        }
        boolean z = false;
        if (MimeTypes.TEXT_VTT.equals(format.sampleMimeType) || lastPathSegment.endsWith(WEBVTT_FILE_EXTENSION) || lastPathSegment.endsWith(VTT_FILE_EXTENSION)) {
            r9 = new WebvttExtractor(format.language, timestampAdjuster);
        } else {
            if (lastPathSegment.endsWith(AAC_FILE_EXTENSION)) {
                r9 = new AdtsExtractor();
            } else if (lastPathSegment.endsWith(AC3_FILE_EXTENSION) || lastPathSegment.endsWith(EC3_FILE_EXTENSION)) {
                r9 = new Ac3Extractor();
            } else if (lastPathSegment.endsWith(MP3_FILE_EXTENSION)) {
                r9 = new Mp3Extractor(0, 0);
            } else if (r9 == 0) {
                if (lastPathSegment.endsWith(".mp4") || lastPathSegment.startsWith(M4_FILE_EXTENSION_PREFIX, lastPathSegment.length() - 4) || lastPathSegment.startsWith(".mp4", lastPathSegment.length() - 5)) {
                    if (list == null) {
                        list = Collections.emptyList();
                    }
                    r2 = new FragmentedMp4Extractor(0, timestampAdjuster, null, drmInitData, list);
                    r9 = r2;
                } else {
                    int i = 16;
                    if (list != null) {
                        i = 48;
                    } else {
                        list = Collections.emptyList();
                    }
                    String str = format.codecs;
                    if (!TextUtils.isEmpty(str)) {
                        if (!MimeTypes.AUDIO_AAC.equals(MimeTypes.getAudioMediaMimeType(str))) {
                            i |= 2;
                        }
                        if (!MimeTypes.VIDEO_H264.equals(MimeTypes.getVideoMediaMimeType(str))) {
                            i |= 4;
                        }
                    }
                    r9 = new TsExtractor(2, timestampAdjuster, new DefaultTsPayloadReaderFactory(i, list));
                }
            }
            z = true;
            r9 = r9;
        }
        return Pair.create(r9, Boolean.valueOf(z));
    }
}
