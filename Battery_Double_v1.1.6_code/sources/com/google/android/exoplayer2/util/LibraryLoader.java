package com.google.android.exoplayer2.util;

public final class LibraryLoader {
    private boolean isAvailable;
    private boolean loadAttempted;
    private String[] nativeLibraries;

    public LibraryLoader(String... strArr) {
        this.nativeLibraries = strArr;
    }

    public synchronized void setLibraries(String... strArr) {
        Assertions.checkState(!this.loadAttempted, "Cannot set libraries after loading");
        this.nativeLibraries = strArr;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(12:7|8|9|10|11|(1:13)|22|14|15|16|17|18) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x001c */
    public synchronized boolean isAvailable() {
        if (this.loadAttempted) {
            return this.isAvailable;
        }
        this.loadAttempted = true;
        for (String loadLibrary : this.nativeLibraries) {
            System.loadLibrary(loadLibrary);
        }
        this.isAvailable = true;
        return this.isAvailable;
    }
}
