package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.FrameLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.google.android.gms.ads.internal.gmsg.zzb;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.ads.internal.zzw;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaan;
import com.google.android.gms.internal.ads.zzaok;
import com.google.android.gms.internal.ads.zzaoq;
import com.google.android.gms.internal.ads.zzark;
import com.google.android.gms.internal.ads.zzaxz;
import com.google.android.gms.internal.ads.zzayh;
import com.google.android.gms.internal.ads.zzayp;
import com.google.android.gms.internal.ads.zzbgg;
import com.google.android.gms.internal.ads.zzbgm;
import com.google.android.gms.internal.ads.zzbhn;
import com.google.android.gms.internal.ads.zzbho;
import com.google.android.gms.internal.ads.zzum;
import com.google.android.gms.internal.ads.zzwu;
import java.util.Collections;

@zzark
public class zzd extends zzaoq implements zzw {
    @VisibleForTesting
    private static final int zzdqt = Color.argb(0, 0, 0, 0);
    protected final Activity mActivity;
    @VisibleForTesting
    zzbgg zzdin;
    @VisibleForTesting
    AdOverlayInfoParcel zzdqu;
    @VisibleForTesting
    private zzi zzdqv;
    @VisibleForTesting
    private zzo zzdqw;
    @VisibleForTesting
    private boolean zzdqx = false;
    @VisibleForTesting
    private FrameLayout zzdqy;
    @VisibleForTesting
    private CustomViewCallback zzdqz;
    @VisibleForTesting
    private boolean zzdra = false;
    @VisibleForTesting
    private boolean zzdrb = false;
    @VisibleForTesting
    private zzh zzdrc;
    @VisibleForTesting
    private boolean zzdrd = false;
    @VisibleForTesting
    int zzdre = 0;
    private final Object zzdrf = new Object();
    private Runnable zzdrg;
    private boolean zzdrh;
    private boolean zzdri;
    private boolean zzdrj = false;
    private boolean zzdrk = false;
    private boolean zzdrl = true;

    public zzd(Activity activity) {
        this.mActivity = activity;
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
    }

    public final void onRestart() {
    }

    public final void close() {
        this.zzdre = 2;
        this.mActivity.finish();
    }

    public final void zzvo() {
        if (this.zzdqu != null && this.zzdqx) {
            setRequestedOrientation(this.zzdqu.orientation);
        }
        if (this.zzdqy != null) {
            this.mActivity.setContentView(this.zzdrc);
            this.zzdri = true;
            this.zzdqy.removeAllViews();
            this.zzdqy = null;
        }
        if (this.zzdqz != null) {
            this.zzdqz.onCustomViewHidden();
            this.zzdqz = null;
        }
        this.zzdqx = false;
    }

    public final void zzvp() {
        this.zzdre = 1;
        this.mActivity.finish();
    }

    public final void onBackPressed() {
        this.zzdre = 0;
    }

    public final boolean zzvq() {
        this.zzdre = 0;
        if (this.zzdin == null) {
            return true;
        }
        boolean zzads = this.zzdin.zzads();
        if (!zzads) {
            this.zzdin.zza("onbackblocked", Collections.emptyMap());
        }
        return zzads;
    }

    public void onCreate(Bundle bundle) {
        this.mActivity.requestWindowFeature(1);
        this.zzdra = bundle != null ? bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false) : false;
        try {
            this.zzdqu = AdOverlayInfoParcel.zzc(this.mActivity.getIntent());
            if (this.zzdqu != null) {
                if (this.zzdqu.zzbsp.zzeov > 7500000) {
                    this.zzdre = 3;
                }
                if (this.mActivity.getIntent() != null) {
                    this.zzdrl = this.mActivity.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
                }
                if (this.zzdqu.zzdsc != null) {
                    this.zzdrb = this.zzdqu.zzdsc.zzbpa;
                } else {
                    this.zzdrb = false;
                }
                if (this.zzdrb && this.zzdqu.zzdsc.zzbpf != -1) {
                    new zzj(this, null).zzyz();
                }
                if (bundle == null) {
                    if (this.zzdqu.zzdru != null && this.zzdrl) {
                        this.zzdqu.zzdru.zziw();
                    }
                    if (!(this.zzdqu.zzdsa == 1 || this.zzdqu.zzdrt == null)) {
                        this.zzdqu.zzdrt.onAdClicked();
                    }
                }
                this.zzdrc = new zzh(this.mActivity, this.zzdqu.zzdsb, this.zzdqu.zzbsp.zzdp);
                this.zzdrc.setId(1000);
                switch (this.zzdqu.zzdsa) {
                    case 1:
                        zzae(false);
                        return;
                    case 2:
                        this.zzdqv = new zzi(this.zzdqu.zzdrv);
                        zzae(false);
                        return;
                    case 3:
                        zzae(true);
                        return;
                    default:
                        throw new zzg("Could not determine ad overlay type.");
                }
            } else {
                throw new zzg("Could not get info for ad overlay.");
            }
        } catch (zzg e) {
            zzaxz.zzeo(e.getMessage());
            this.zzdre = 3;
            this.mActivity.finish();
        }
    }

    public final void onStart() {
        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcwm)).booleanValue()) {
            if (this.zzdin == null || this.zzdin.isDestroyed()) {
                zzaxz.zzeo("The webview does not exist. Ignoring action.");
            } else {
                zzbv.zzlh();
                zzayp.zzj(this.zzdin);
            }
        }
    }

    public final void onResume() {
        if (this.zzdqu.zzdru != null) {
            this.zzdqu.zzdru.onResume();
        }
        if (!((Boolean) zzwu.zzpz().zzd(zzaan.zzcwm)).booleanValue()) {
            if (this.zzdin == null || this.zzdin.isDestroyed()) {
                zzaxz.zzeo("The webview does not exist. Ignoring action.");
            } else {
                zzbv.zzlh();
                zzayp.zzj(this.zzdin);
            }
        }
    }

    public final void onPause() {
        zzvo();
        if (this.zzdqu.zzdru != null) {
            this.zzdqu.zzdru.onPause();
        }
        if (!((Boolean) zzwu.zzpz().zzd(zzaan.zzcwm)).booleanValue() && this.zzdin != null && (!this.mActivity.isFinishing() || this.zzdqv == null)) {
            zzbv.zzlh();
            zzayp.zzi(this.zzdin);
        }
        zzvs();
    }

    public final void zzq(IObjectWrapper iObjectWrapper) {
        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcwl)).booleanValue() && PlatformVersion.isAtLeastN()) {
            Configuration configuration = (Configuration) ObjectWrapper.unwrap(iObjectWrapper);
            zzbv.zzlf();
            if (zzayh.zza(this.mActivity, configuration)) {
                this.mActivity.getWindow().addFlags(1024);
                this.mActivity.getWindow().clearFlags(2048);
                return;
            }
            this.mActivity.getWindow().addFlags(2048);
            this.mActivity.getWindow().clearFlags(1024);
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.zzdra);
    }

    public final void onStop() {
        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcwm)).booleanValue() && this.zzdin != null && (!this.mActivity.isFinishing() || this.zzdqv == null)) {
            zzbv.zzlh();
            zzayp.zzi(this.zzdin);
        }
        zzvs();
    }

    public final void onDestroy() {
        if (this.zzdin != null) {
            this.zzdrc.removeView(this.zzdin.getView());
        }
        zzvs();
    }

    private final void zzad(boolean z) {
        int intValue = ((Integer) zzwu.zzpz().zzd(zzaan.zzcwo)).intValue();
        zzp zzp = new zzp();
        zzp.size = 50;
        zzp.paddingLeft = z ? intValue : 0;
        zzp.paddingRight = z ? 0 : intValue;
        zzp.paddingTop = 0;
        zzp.paddingBottom = intValue;
        this.zzdqw = new zzo(this.mActivity, zzp, this);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(z ? 11 : 9);
        zza(z, this.zzdqu.zzdrx);
        this.zzdrc.addView(this.zzdqw, layoutParams);
    }

    public final void zzay() {
        this.zzdri = true;
    }

    public final void zza(boolean z, boolean z2) {
        boolean z3 = true;
        boolean z4 = ((Boolean) zzwu.zzpz().zzd(zzaan.zzcrt)).booleanValue() && this.zzdqu != null && this.zzdqu.zzdsc != null && this.zzdqu.zzdsc.zzbph;
        boolean z5 = ((Boolean) zzwu.zzpz().zzd(zzaan.zzcru)).booleanValue() && this.zzdqu != null && this.zzdqu.zzdsc != null && this.zzdqu.zzdsc.zzbpi;
        if (z && z2 && z4 && !z5) {
            new zzaok(this.zzdin, "useCustomClose").zzda("Custom close has been disabled for interstitial ads in this ad slot.");
        }
        if (this.zzdqw != null) {
            zzo zzo = this.zzdqw;
            if (!z5 && (!z2 || z4)) {
                z3 = false;
            }
            zzo.zzaf(z3);
        }
    }

    public final void zzvr() {
        this.zzdrc.removeView(this.zzdqw);
        zzad(true);
    }

    public final void setRequestedOrientation(int i) {
        if (this.mActivity.getApplicationInfo().targetSdkVersion >= ((Integer) zzwu.zzpz().zzd(zzaan.zzcyg)).intValue()) {
            if (this.mActivity.getApplicationInfo().targetSdkVersion <= ((Integer) zzwu.zzpz().zzd(zzaan.zzcyh)).intValue()) {
                if (VERSION.SDK_INT >= ((Integer) zzwu.zzpz().zzd(zzaan.zzcyi)).intValue()) {
                    if (VERSION.SDK_INT <= ((Integer) zzwu.zzpz().zzd(zzaan.zzcyj)).intValue()) {
                        return;
                    }
                }
            }
        }
        this.mActivity.setRequestedOrientation(i);
    }

    public final void zza(View view, CustomViewCallback customViewCallback) {
        this.zzdqy = new FrameLayout(this.mActivity);
        this.zzdqy.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.zzdqy.addView(view, -1, -1);
        this.mActivity.setContentView(this.zzdqy);
        this.zzdri = true;
        this.zzdqz = customViewCallback;
        this.zzdqx = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x023c  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:112:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0126 A[SYNTHETIC, Splitter:B:58:0x0126] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01fc  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0214  */
    private final void zzae(boolean z) throws zzg {
        boolean z2;
        boolean zzmu;
        ViewParent parent;
        if (!this.zzdri) {
            this.mActivity.requestWindowFeature(1);
        }
        Window window = this.mActivity.getWindow();
        if (window != null) {
            if (PlatformVersion.isAtLeastN()) {
                if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcwl)).booleanValue()) {
                    zzbv.zzlf();
                    z2 = zzayh.zza(this.mActivity, this.mActivity.getResources().getConfiguration());
                    boolean z3 = false;
                    boolean z4 = this.zzdqu.zzdsc == null && this.zzdqu.zzdsc.zzbpb;
                    if ((!this.zzdrb || z4) && z2) {
                        window.setFlags(1024, 1024);
                        if (PlatformVersion.isAtLeastKitKat() && this.zzdqu.zzdsc != null && this.zzdqu.zzdsc.zzbpg) {
                            window.getDecorView().setSystemUiVisibility(InputDeviceCompat.SOURCE_TOUCHSCREEN);
                        }
                    }
                    zzw zzw = null;
                    zzbhn zzadl = this.zzdqu.zzdrv == null ? this.zzdqu.zzdrv.zzadl() : null;
                    zzmu = zzadl == null ? zzadl.zzmu() : false;
                    this.zzdrd = false;
                    if (zzmu) {
                        if (this.zzdqu.orientation == zzbv.zzlh().zzzw()) {
                            if (this.mActivity.getResources().getConfiguration().orientation == 1) {
                                z3 = true;
                            }
                            this.zzdrd = z3;
                        } else if (this.zzdqu.orientation == zzbv.zzlh().zzzx()) {
                            if (this.mActivity.getResources().getConfiguration().orientation == 2) {
                                z3 = true;
                            }
                            this.zzdrd = z3;
                        }
                    }
                    boolean z5 = this.zzdrd;
                    StringBuilder sb = new StringBuilder(46);
                    sb.append("Delay onShow to next orientation change: ");
                    sb.append(z5);
                    zzaxz.zzdn(sb.toString());
                    setRequestedOrientation(this.zzdqu.orientation);
                    if (zzbv.zzlh().zza(window)) {
                        zzaxz.zzdn("Hardware acceleration on the AdActivity window enabled.");
                    }
                    if (this.zzdrb) {
                        this.zzdrc.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                    } else {
                        this.zzdrc.setBackgroundColor(zzdqt);
                    }
                    this.mActivity.setContentView(this.zzdrc);
                    this.zzdri = true;
                    if (!z) {
                        try {
                            zzbv.zzlg();
                            this.zzdin = zzbgm.zza(this.mActivity, this.zzdqu.zzdrv != null ? this.zzdqu.zzdrv.zzadj() : null, this.zzdqu.zzdrv != null ? this.zzdqu.zzdrv.zzadk() : null, true, zzmu, null, this.zzdqu.zzbsp, null, null, this.zzdqu.zzdrv != null ? this.zzdqu.zzdrv.zzid() : null, zzum.zzoi());
                            zzbhn zzadl2 = this.zzdin.zzadl();
                            zzb zzb = this.zzdqu.zzdsd;
                            com.google.android.gms.ads.internal.gmsg.zzd zzd = this.zzdqu.zzdrw;
                            zzt zzt = this.zzdqu.zzdrz;
                            if (this.zzdqu.zzdrv != null) {
                                zzw = this.zzdqu.zzdrv.zzadl().zzaea();
                            }
                            zzadl2.zza(null, zzb, null, zzd, zzt, true, null, zzw, null, null);
                            this.zzdin.zzadl().zza((zzbho) new zze(this));
                            if (this.zzdqu.url != null) {
                                this.zzdin.loadUrl(this.zzdqu.url);
                            } else if (this.zzdqu.zzdry != null) {
                                this.zzdin.loadDataWithBaseURL(this.zzdqu.zzbde, this.zzdqu.zzdry, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
                            } else {
                                throw new zzg("No URL or HTML to display in ad overlay.");
                            }
                            if (this.zzdqu.zzdrv != null) {
                                this.zzdqu.zzdrv.zzb(this);
                            }
                        } catch (Exception e) {
                            zzaxz.zzb("Error obtaining webview.", e);
                            throw new zzg("Could not obtain webview for the overlay.");
                        }
                    } else {
                        this.zzdin = this.zzdqu.zzdrv;
                        this.zzdin.zzbo(this.mActivity);
                    }
                    this.zzdin.zza(this);
                    if (this.zzdqu.zzdrv != null) {
                        zzb(this.zzdqu.zzdrv.zzadp(), this.zzdrc);
                    }
                    parent = this.zzdin.getParent();
                    if (parent != null && (parent instanceof ViewGroup)) {
                        ((ViewGroup) parent).removeView(this.zzdin.getView());
                    }
                    if (this.zzdrb) {
                        this.zzdin.zzady();
                    }
                    this.zzdrc.addView(this.zzdin.getView(), -1, -1);
                    if (!z && !this.zzdrd) {
                        zzvv();
                    }
                    zzad(zzmu);
                    if (!this.zzdin.zzadn()) {
                        zza(zzmu, true);
                        return;
                    }
                    return;
                }
            }
            z2 = true;
            boolean z32 = false;
            if (this.zzdqu.zzdsc == null) {
            }
            window.setFlags(1024, 1024);
            window.getDecorView().setSystemUiVisibility(InputDeviceCompat.SOURCE_TOUCHSCREEN);
            zzw zzw2 = null;
            if (this.zzdqu.zzdrv == null) {
            }
            if (zzadl == null) {
            }
            this.zzdrd = false;
            if (zzmu) {
            }
            boolean z52 = this.zzdrd;
            StringBuilder sb2 = new StringBuilder(46);
            sb2.append("Delay onShow to next orientation change: ");
            sb2.append(z52);
            zzaxz.zzdn(sb2.toString());
            setRequestedOrientation(this.zzdqu.orientation);
            if (zzbv.zzlh().zza(window)) {
            }
            if (this.zzdrb) {
            }
            this.mActivity.setContentView(this.zzdrc);
            this.zzdri = true;
            if (!z) {
            }
            this.zzdin.zza(this);
            if (this.zzdqu.zzdrv != null) {
            }
            parent = this.zzdin.getParent();
            ((ViewGroup) parent).removeView(this.zzdin.getView());
            if (this.zzdrb) {
            }
            this.zzdrc.addView(this.zzdin.getView(), -1, -1);
            zzvv();
            zzad(zzmu);
            if (!this.zzdin.zzadn()) {
            }
        } else {
            throw new zzg("Invalid activity, no window available.");
        }
    }

    private final void zzvs() {
        if (this.mActivity.isFinishing() && !this.zzdrj) {
            this.zzdrj = true;
            if (this.zzdin != null) {
                this.zzdin.zzdh(this.zzdre);
                synchronized (this.zzdrf) {
                    if (!this.zzdrh && this.zzdin.zzadu()) {
                        this.zzdrg = new zzf(this);
                        zzayh.zzelc.postDelayed(this.zzdrg, ((Long) zzwu.zzpz().zzd(zzaan.zzcrs)).longValue());
                        return;
                    }
                }
            }
            zzvt();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final void zzvt() {
        if (!this.zzdrk) {
            this.zzdrk = true;
            if (this.zzdin != null) {
                this.zzdrc.removeView(this.zzdin.getView());
                if (this.zzdqv != null) {
                    this.zzdin.zzbo(this.zzdqv.zzsp);
                    this.zzdin.zzav(false);
                    this.zzdqv.parent.addView(this.zzdin.getView(), this.zzdqv.index, this.zzdqv.zzdrp);
                    this.zzdqv = null;
                } else if (this.mActivity.getApplicationContext() != null) {
                    this.zzdin.zzbo(this.mActivity.getApplicationContext());
                }
                this.zzdin = null;
            }
            if (!(this.zzdqu == null || this.zzdqu.zzdru == null)) {
                this.zzdqu.zzdru.zziv();
            }
            if (!(this.zzdqu == null || this.zzdqu.zzdrv == null)) {
                zzb(this.zzdqu.zzdrv.zzadp(), this.zzdqu.zzdrv.getView());
            }
        }
    }

    private static void zzb(@Nullable IObjectWrapper iObjectWrapper, @Nullable View view) {
        if (iObjectWrapper != null && view != null) {
            zzbv.zzlw().zza(iObjectWrapper, view);
        }
    }

    public final void zzvu() {
        if (this.zzdrd) {
            this.zzdrd = false;
            zzvv();
        }
    }

    private final void zzvv() {
        this.zzdin.zzvv();
    }

    public final void zzvw() {
        this.zzdrc.zzdro = true;
    }

    public final void zzvx() {
        synchronized (this.zzdrf) {
            this.zzdrh = true;
            if (this.zzdrg != null) {
                zzayh.zzelc.removeCallbacks(this.zzdrg);
                zzayh.zzelc.post(this.zzdrg);
            }
        }
    }
}
