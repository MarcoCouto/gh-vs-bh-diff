package com.google.android.gms.ads.impl;

public final class R {

    public static final class string {
        public static final int s1 = 2131624202;
        public static final int s2 = 2131624203;
        public static final int s3 = 2131624204;
        public static final int s4 = 2131624205;
        public static final int s5 = 2131624206;
        public static final int s6 = 2131624207;
        public static final int s7 = 2131624208;

        private string() {
        }
    }

    private R() {
    }
}
