package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd.AdChoicesInfo;
import com.google.android.gms.ads.formats.NativeAd.Image;
import java.util.ArrayList;
import java.util.List;

@zzark
public final class zzada extends AdChoicesInfo {
    private final List<Image> zzdab = new ArrayList();
    private final zzacx zzddp;
    private String zzddq;

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f A[Catch:{ RemoteException -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0027 A[SYNTHETIC] */
    public zzada(zzacx zzacx) {
        zzadb zzadb;
        this.zzddp = zzacx;
        try {
            this.zzddq = this.zzddp.getText();
        } catch (RemoteException e) {
            zzbbd.zzb("", e);
            this.zzddq = "";
        }
        try {
            for (Object next : zzacx.zzro()) {
                if (next instanceof IBinder) {
                    IBinder iBinder = (IBinder) next;
                    if (iBinder != null) {
                        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeAdImage");
                        zzadb = queryLocalInterface instanceof zzadb ? (zzadb) queryLocalInterface : new zzadd(iBinder);
                        if (zzadb == null) {
                            this.zzdab.add(new zzade(zzadb));
                        }
                    }
                }
                zzadb = null;
                if (zzadb == null) {
                }
            }
        } catch (RemoteException e2) {
            zzbbd.zzb("", e2);
        }
    }

    public final CharSequence getText() {
        return this.zzddq;
    }

    public final List<Image> getImages() {
        return this.zzdab;
    }
}
