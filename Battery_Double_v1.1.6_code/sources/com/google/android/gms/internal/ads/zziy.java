package com.google.android.gms.internal.ads;

import android.util.Log;
import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.internal.ads.zzki.zza;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class zziy {
    private static final int zzaob = zzqe.zzam("meta");
    private static final int zzaot = zzqe.zzam("vide");
    private static final int zzaou = zzqe.zzam("soun");
    private static final int zzaov = zzqe.zzam(MimeTypes.BASE_TYPE_TEXT);
    private static final int zzaow = zzqe.zzam("sbtl");
    private static final int zzaox = zzqe.zzam("subt");
    private static final int zzaoy = zzqe.zzam("clcp");
    private static final int zzaoz = zzqe.zzam(C.CENC_TYPE_cenc);

    /* JADX WARNING: Removed duplicated region for block: B:192:0x0399  */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x03a3  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x03a9  */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x03ac  */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x0402  */
    /* JADX WARNING: Removed duplicated region for block: B:280:0x0541  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x071e  */
    /* JADX WARNING: Removed duplicated region for block: B:372:0x0751  */
    /* JADX WARNING: Removed duplicated region for block: B:376:0x0790  */
    /* JADX WARNING: Removed duplicated region for block: B:397:0x07f4 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:398:0x07f5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0157  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01b6  */
    public static zzjs zza(zziw zziw, zzix zzix, long j, zzhp zzhp, boolean z) throws zzfx {
        boolean z2;
        long j2;
        long j3;
        zzix zzix2;
        long j4;
        int readInt;
        int i;
        zzjb zzjb;
        zziw zzaj;
        Pair pair;
        zzjs zzjs;
        zzpx zzpx;
        Pair pair2;
        String str;
        int i2;
        int i3;
        int i4;
        int i5;
        zzje zzje;
        int i6;
        zzjb zzjb2;
        int i7;
        int i8;
        String str2;
        String str3;
        List<byte[]> list;
        Pair pair3;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        zzjb zzjb3;
        zzpx zzpx2;
        List list2;
        int i15;
        int i16;
        String str4;
        zzjb zzjb4;
        int i17;
        int i18;
        int position;
        String str5;
        int i19;
        int i20;
        List list3;
        String str6;
        zziw zziw2 = zziw;
        zzhp zzhp2 = zzhp;
        zziw zzaj2 = zziw2.zzaj(zziv.zzame);
        zzpx zzpx3 = zzaj2.zzai(zziv.zzams).zzaos;
        zzpx3.setPosition(16);
        int readInt2 = zzpx3.readInt();
        int i21 = readInt2 == zzaou ? 1 : readInt2 == zzaot ? 2 : (readInt2 == zzaov || readInt2 == zzaow || readInt2 == zzaox || readInt2 == zzaoy) ? 3 : readInt2 == zzaob ? 4 : -1;
        if (i21 == -1) {
            return null;
        }
        zzpx zzpx4 = zziw2.zzai(zziv.zzamo).zzaos;
        zzpx4.setPosition(8);
        int zzaf = zziv.zzaf(zzpx4.readInt());
        zzpx4.zzbl(zzaf == 0 ? 8 : 16);
        int readInt3 = zzpx4.readInt();
        zzpx4.zzbl(4);
        int position2 = zzpx4.getPosition();
        int i22 = zzaf == 0 ? 4 : 8;
        int i23 = 0;
        while (true) {
            if (i23 >= i22) {
                z2 = true;
                break;
            } else if (zzpx4.data[position2 + i23] != -1) {
                z2 = false;
                break;
            } else {
                i23++;
            }
        }
        if (z2) {
            zzpx4.zzbl(i22);
        } else {
            long zzhd = zzaf == 0 ? zzpx4.zzhd() : zzpx4.zzhh();
            if (zzhd != 0) {
                j2 = zzhd;
                zzpx4.zzbl(16);
                int readInt4 = zzpx4.readInt();
                int readInt5 = zzpx4.readInt();
                zzpx4.zzbl(4);
                int readInt6 = zzpx4.readInt();
                int readInt7 = zzpx4.readInt();
                int i24 = (readInt4 != 0 && readInt5 == 65536 && readInt6 == -65536 && readInt7 == 0) ? 90 : (readInt4 != 0 && readInt5 == -65536 && readInt6 == 65536 && readInt7 == 0) ? 270 : (readInt4 != -65536 && readInt5 == 0 && readInt6 == 0 && readInt7 == -65536) ? 180 : 0;
                zzje zzje2 = new zzje(readInt3, j2, i24);
                if (j != C.TIME_UNSET) {
                    j3 = zzje2.zzcs;
                    zzix2 = zzix;
                } else {
                    zzix2 = zzix;
                    j3 = j;
                }
                zzpx zzpx5 = zzix2.zzaos;
                zzpx5.setPosition(8);
                zzpx5.zzbl(zziv.zzaf(zzpx5.readInt()) != 0 ? 8 : 16);
                long zzhd2 = zzpx5.zzhd();
                if (j3 != C.TIME_UNSET) {
                    j4 = -9223372036854775807L;
                } else {
                    j4 = zzqe.zza(j3, 1000000, zzhd2);
                }
                zziw zzaj3 = zzaj2.zzaj(zziv.zzamf).zzaj(zziv.zzamg);
                zzpx zzpx6 = zzaj2.zzai(zziv.zzamr).zzaos;
                zzpx6.setPosition(8);
                int zzaf2 = zziv.zzaf(zzpx6.readInt());
                zzpx6.zzbl(zzaf2 != 0 ? 8 : 16);
                long zzhd3 = zzpx6.zzhd();
                zzpx6.zzbl(zzaf2 != 0 ? 4 : 8);
                int readUnsignedShort = zzpx6.readUnsignedShort();
                char c = (char) (((readUnsignedShort >> 10) & 31) + 96);
                char c2 = (char) (((readUnsignedShort >> 5) & 31) + 96);
                char c3 = (char) ((readUnsignedShort & 31) + 96);
                StringBuilder sb = new StringBuilder(3);
                sb.append(c);
                sb.append(c2);
                sb.append(c3);
                Pair create = Pair.create(Long.valueOf(zzhd3), sb.toString());
                zzpx zzpx7 = zzaj3.zzai(zziv.zzamt).zzaos;
                int zzb = zzje2.id;
                int zzc = zzje2.zzzo;
                String str7 = (String) create.second;
                zzpx7.setPosition(12);
                readInt = zzpx7.readInt();
                zzjb zzjb5 = new zzjb(readInt);
                i = 0;
                while (i < readInt) {
                    int position3 = zzpx7.getPosition();
                    int readInt8 = zzpx7.readInt();
                    zzpo.checkArgument(readInt8 > 0, "childAtomSize should be positive");
                    int readInt9 = zzpx7.readInt();
                    if (readInt9 == zziv.zzalb || readInt9 == zziv.zzalc || readInt9 == zziv.zzamz || readInt9 == zziv.zzanl || readInt9 == zziv.zzald || readInt9 == zziv.zzale || readInt9 == zziv.zzalf || readInt9 == zziv.zzaok || readInt9 == zziv.zzaol) {
                        int i25 = i;
                        zzjb zzjb6 = zzjb5;
                        i4 = readInt;
                        i6 = i21;
                        String str8 = str7;
                        Pair pair4 = create;
                        zzje zzje3 = zzje2;
                        int i26 = readInt8;
                        zzpx zzpx8 = zzpx7;
                        int i27 = position3;
                        zzpx8.setPosition(i27 + 8 + 8);
                        zzpx8.zzbl(16);
                        int readUnsignedShort2 = zzpx8.readUnsignedShort();
                        int readUnsignedShort3 = zzpx8.readUnsignedShort();
                        zzpx8.zzbl(50);
                        int position4 = zzpx8.getPosition();
                        if (readInt9 == zziv.zzamz) {
                            i7 = i25;
                            readInt9 = zza(zzpx8, i27, i26, zzjb6, i7);
                            zzpx8.setPosition(position4);
                        } else {
                            i7 = i25;
                        }
                        String str9 = null;
                        boolean z3 = false;
                        List list4 = null;
                        float f = 1.0f;
                        byte[] bArr = null;
                        int i28 = -1;
                        while (position4 - i27 < i26) {
                            zzpx8.setPosition(position4);
                            int position5 = zzpx8.getPosition();
                            int readInt10 = zzpx8.readInt();
                            if (readInt10 == 0 && zzpx8.getPosition() - i27 == i26) {
                                str = str8;
                                if (str9 == null) {
                                    i2 = i7;
                                    float f2 = f;
                                    i5 = i27;
                                    zzpx = zzpx8;
                                    pair2 = pair4;
                                    int i29 = i28;
                                    i3 = i26;
                                    zzje = zzje3;
                                    zzjb2 = zzjb6;
                                    zzjb2.zzaad = zzfs.zza(Integer.toString(zzb), str9, null, -1, -1, readUnsignedShort2, readUnsignedShort3, -1.0f, list4, zzc, f2, bArr, i29, null, zzhp);
                                    zzpx zzpx9 = zzpx;
                                    zzpx9.setPosition(i5 + i3);
                                    i = i2 + 1;
                                    zzhp2 = zzhp;
                                    zzjb5 = zzjb2;
                                    zzpx7 = zzpx9;
                                    i21 = i6;
                                    zzje2 = zzje;
                                    readInt = i4;
                                    str7 = str;
                                    create = pair2;
                                    zziw zziw3 = zziw;
                                } else {
                                    i2 = i7;
                                    i5 = i27;
                                    zzpx = zzpx8;
                                    i3 = i26;
                                    zzjb2 = zzjb6;
                                    zzje = zzje3;
                                    pair2 = pair4;
                                }
                            } else {
                                zzpo.checkArgument(readInt10 > 0, "childAtomSize should be positive");
                                int readInt11 = zzpx8.readInt();
                                if (readInt11 == zziv.zzamh) {
                                    zzpo.checkState(str9 == null);
                                    str9 = MimeTypes.VIDEO_H264;
                                    zzpx8.setPosition(position5 + 8);
                                    zzqh zzg = zzqh.zzg(zzpx8);
                                    list = zzg.zzzl;
                                    zzjb6.zzakx = zzg.zzakx;
                                    if (!z3) {
                                        f = zzg.zzbhq;
                                    }
                                } else if (readInt11 == zziv.zzami) {
                                    zzpo.checkState(str9 == null);
                                    str9 = MimeTypes.VIDEO_H265;
                                    zzpx8.setPosition(position5 + 8);
                                    zzqn zzi = zzqn.zzi(zzpx8);
                                    list = zzi.zzzl;
                                    zzjb6.zzakx = zzi.zzakx;
                                } else {
                                    if (readInt11 == zziv.zzaom) {
                                        zzpo.checkState(str9 == null);
                                        str3 = readInt9 == zziv.zzaok ? MimeTypes.VIDEO_VP8 : MimeTypes.VIDEO_VP9;
                                    } else if (readInt11 == zziv.zzalg) {
                                        zzpo.checkState(str9 == null);
                                        str3 = MimeTypes.VIDEO_H263;
                                    } else {
                                        if (readInt11 == zziv.zzamj) {
                                            zzpo.checkState(str9 == null);
                                            Pair zzb2 = zzb(zzpx8, position5);
                                            String str10 = (String) zzb2.first;
                                            list4 = Collections.singletonList((byte[]) zzb2.second);
                                            str2 = str8;
                                            i8 = readInt9;
                                            str9 = str10;
                                        } else if (readInt11 == zziv.zzani) {
                                            zzpx8.setPosition(position5 + 8);
                                            f = ((float) zzpx8.zzhg()) / ((float) zzpx8.zzhg());
                                            str2 = str8;
                                            i8 = readInt9;
                                            z3 = true;
                                        } else if (readInt11 == zziv.zzaoi) {
                                            int i30 = position5 + 8;
                                            while (true) {
                                                if (i30 - position5 < readInt10) {
                                                    zzpx8.setPosition(i30);
                                                    int readInt12 = zzpx8.readInt();
                                                    str2 = str8;
                                                    i8 = readInt9;
                                                    if (zzpx8.readInt() == zziv.zzaoj) {
                                                        bArr = Arrays.copyOfRange(zzpx8.data, i30, readInt12 + i30);
                                                    } else {
                                                        i30 += readInt12;
                                                        str8 = str2;
                                                        readInt9 = i8;
                                                    }
                                                } else {
                                                    str2 = str8;
                                                    i8 = readInt9;
                                                    bArr = null;
                                                }
                                            }
                                        } else {
                                            str2 = str8;
                                            i8 = readInt9;
                                            if (readInt11 == zziv.zzaoh) {
                                                int readUnsignedByte = zzpx8.readUnsignedByte();
                                                zzpx8.zzbl(3);
                                                if (readUnsignedByte == 0) {
                                                    switch (zzpx8.readUnsignedByte()) {
                                                        case 0:
                                                            i28 = 0;
                                                            break;
                                                        case 1:
                                                            i28 = 1;
                                                            break;
                                                        case 2:
                                                            i28 = 2;
                                                            break;
                                                        case 3:
                                                            i28 = 3;
                                                            break;
                                                    }
                                                }
                                                position4 += readInt10;
                                                str8 = str2;
                                                readInt9 = i8;
                                            }
                                        }
                                        position4 += readInt10;
                                        str8 = str2;
                                        readInt9 = i8;
                                    }
                                    str2 = str8;
                                    i8 = readInt9;
                                    position4 += readInt10;
                                    str8 = str2;
                                    readInt9 = i8;
                                }
                                str2 = str8;
                                i8 = readInt9;
                                list4 = list;
                                position4 += readInt10;
                                str8 = str2;
                                readInt9 = i8;
                            }
                        }
                        str = str8;
                        if (str9 == null) {
                        }
                    } else {
                        if (readInt9 == zziv.zzali || readInt9 == zziv.zzana || readInt9 == zziv.zzaln || readInt9 == zziv.zzalp || readInt9 == zziv.zzalr || readInt9 == zziv.zzalu || readInt9 == zziv.zzals || readInt9 == zziv.zzalt || readInt9 == zziv.zzany || readInt9 == zziv.zzanz || readInt9 == zziv.zzall || readInt9 == zziv.zzalm || readInt9 == zziv.zzalj || readInt9 == zziv.zzaoo) {
                            i9 = i;
                            i4 = readInt;
                            i6 = i21;
                            String str11 = str7;
                            pair3 = create;
                            zzje zzje4 = zzje2;
                            int i31 = readInt8;
                            zzjb zzjb7 = zzjb5;
                            int i32 = position3;
                            zzpx7.setPosition(i32 + 8 + 8);
                            if (z) {
                                i10 = zzpx7.readUnsignedShort();
                                zzpx7.zzbl(6);
                            } else {
                                zzpx7.zzbl(8);
                                i10 = 0;
                            }
                            if (i10 != 0) {
                                i20 = 1;
                                if (i10 != 1) {
                                    if (i10 == 2) {
                                        zzpx7.zzbl(16);
                                        i12 = (int) Math.round(Double.longBitsToDouble(zzpx7.readLong()));
                                        i11 = zzpx7.zzhg();
                                        zzpx7.zzbl(20);
                                        int position6 = zzpx7.getPosition();
                                        if (readInt9 != zziv.zzana) {
                                            i13 = i9;
                                            readInt9 = zza(zzpx7, i32, i31, zzjb7, i13);
                                            zzpx7.setPosition(position6);
                                        } else {
                                            i13 = i9;
                                        }
                                        String str12 = readInt9 != zziv.zzaln ? MimeTypes.AUDIO_AC3 : readInt9 == zziv.zzalp ? MimeTypes.AUDIO_E_AC3 : readInt9 == zziv.zzalr ? MimeTypes.AUDIO_DTS : (readInt9 == zziv.zzals || readInt9 == zziv.zzalt) ? MimeTypes.AUDIO_DTS_HD : readInt9 == zziv.zzalu ? MimeTypes.AUDIO_DTS_EXPRESS : readInt9 == zziv.zzany ? MimeTypes.AUDIO_AMR_NB : readInt9 == zziv.zzanz ? MimeTypes.AUDIO_AMR_WB : (readInt9 == zziv.zzall || readInt9 == zziv.zzalm) ? MimeTypes.AUDIO_RAW : readInt9 == zziv.zzalj ? MimeTypes.AUDIO_MPEG : readInt9 == zziv.zzaoo ? MimeTypes.AUDIO_ALAC : null;
                                        int i33 = i12;
                                        int i34 = i11;
                                        i14 = position6;
                                        byte[] bArr2 = null;
                                        String str13 = str12;
                                        while (i14 - i32 < i31) {
                                            zzpx7.setPosition(i14);
                                            int readInt13 = zzpx7.readInt();
                                            zzpo.checkArgument(readInt13 > 0, "childAtomSize should be positive");
                                            int readInt14 = zzpx7.readInt();
                                            if (readInt14 == zziv.zzamj || (z && readInt14 == zziv.zzalk)) {
                                                i17 = readInt13;
                                                String str14 = str13;
                                                i18 = i14;
                                                i16 = i13;
                                                i15 = i32;
                                                zzjb4 = zzjb7;
                                                if (readInt14 == zziv.zzamj) {
                                                    position = i18;
                                                } else {
                                                    position = zzpx7.getPosition();
                                                    while (true) {
                                                        if (position - i18 < i17) {
                                                            zzpx7.setPosition(position);
                                                            int readInt15 = zzpx7.readInt();
                                                            zzpo.checkArgument(readInt15 > 0, "childAtomSize should be positive");
                                                            if (zzpx7.readInt() != zziv.zzamj) {
                                                                position += readInt15;
                                                            }
                                                        } else {
                                                            position = -1;
                                                        }
                                                    }
                                                }
                                                if (position != -1) {
                                                    Pair zzb3 = zzb(zzpx7, position);
                                                    str5 = (String) zzb3.first;
                                                    bArr2 = (byte[]) zzb3.second;
                                                    if (MimeTypes.AUDIO_AAC.equals(str5)) {
                                                        Pair zzf = zzpp.zzf(bArr2);
                                                        int intValue = ((Integer) zzf.first).intValue();
                                                        i34 = ((Integer) zzf.second).intValue();
                                                        i33 = intValue;
                                                    }
                                                } else {
                                                    str5 = str14;
                                                }
                                                str4 = str5;
                                            } else {
                                                if (readInt14 == zziv.zzalo) {
                                                    zzpx7.setPosition(i14 + 8);
                                                    zzjb7.zzaad = zzgg.zza(zzpx7, Integer.toString(zzb), str11, zzhp2);
                                                } else if (readInt14 == zziv.zzalq) {
                                                    zzpx7.setPosition(i14 + 8);
                                                    zzjb7.zzaad = zzgg.zzb(zzpx7, Integer.toString(zzb), str11, zzhp2);
                                                } else {
                                                    if (readInt14 == zziv.zzalv) {
                                                        i17 = readInt13;
                                                        str4 = str13;
                                                        i19 = i14;
                                                        i16 = i13;
                                                        i15 = i32;
                                                        zzjb4 = zzjb7;
                                                        zzjb4.zzaad = zzfs.zza(Integer.toString(zzb), str13, null, -1, -1, i34, i33, null, zzhp, 0, str11);
                                                    } else {
                                                        i17 = readInt13;
                                                        str4 = str13;
                                                        i19 = i14;
                                                        i16 = i13;
                                                        i15 = i32;
                                                        zzjb4 = zzjb7;
                                                        if (readInt14 == zziv.zzaoo) {
                                                            byte[] bArr3 = new byte[i17];
                                                            i18 = i19;
                                                            zzpx7.setPosition(i18);
                                                            zzpx7.zze(bArr3, 0, i17);
                                                            bArr2 = bArr3;
                                                        }
                                                    }
                                                    i18 = i19;
                                                }
                                                i17 = readInt13;
                                                str4 = str13;
                                                i18 = i14;
                                                i16 = i13;
                                                i15 = i32;
                                                zzjb4 = zzjb7;
                                            }
                                            i14 = i18 + i17;
                                            zzjb7 = zzjb4;
                                            str13 = str4;
                                            i13 = i16;
                                            i32 = i15;
                                            zzhp2 = zzhp;
                                        }
                                        String str15 = str13;
                                        int i35 = i13;
                                        int i36 = i32;
                                        zzjb3 = zzjb7;
                                        if (zzjb3.zzaad == null) {
                                            String str16 = str15;
                                            if (str16 != null) {
                                                int i37 = MimeTypes.AUDIO_RAW.equals(str16) ? 2 : -1;
                                                String num = Integer.toString(zzb);
                                                if (bArr2 == null) {
                                                    list2 = null;
                                                } else {
                                                    list2 = Collections.singletonList(bArr2);
                                                }
                                                zzpx2 = zzpx7;
                                                zzjb3.zzaad = zzfs.zza(num, str16, null, -1, -1, i34, i33, i37, list2, zzhp, 0, str11);
                                                str = str11;
                                                zzpx = zzpx2;
                                                i3 = i31;
                                                zzjb2 = zzjb3;
                                                zzje = zzje4;
                                                pair2 = pair3;
                                                i2 = i35;
                                                i5 = i36;
                                            }
                                        }
                                        zzpx2 = zzpx7;
                                        str = str11;
                                        zzpx = zzpx2;
                                        i3 = i31;
                                        zzjb2 = zzjb3;
                                        zzje = zzje4;
                                        pair2 = pair3;
                                        i2 = i35;
                                        i5 = i36;
                                    } else {
                                        str = str11;
                                        i5 = i32;
                                        zzjb2 = zzjb7;
                                        zzpx = zzpx7;
                                        i3 = i31;
                                        zzje = zzje4;
                                    }
                                }
                            } else {
                                i20 = 1;
                            }
                            i11 = zzpx7.readUnsignedShort();
                            zzpx7.zzbl(6);
                            int zzhf = zzpx7.zzhf();
                            if (i10 == i20) {
                                zzpx7.zzbl(16);
                            }
                            i12 = zzhf;
                            int position62 = zzpx7.getPosition();
                            if (readInt9 != zziv.zzana) {
                            }
                            if (readInt9 != zziv.zzaln) {
                            }
                            int i332 = i12;
                            int i342 = i11;
                            i14 = position62;
                            byte[] bArr22 = null;
                            String str132 = str12;
                            while (i14 - i32 < i31) {
                            }
                            String str152 = str132;
                            int i352 = i13;
                            int i362 = i32;
                            zzjb3 = zzjb7;
                            if (zzjb3.zzaad == null) {
                            }
                            zzpx2 = zzpx7;
                            str = str11;
                            zzpx = zzpx2;
                            i3 = i31;
                            zzjb2 = zzjb3;
                            zzje = zzje4;
                            pair2 = pair3;
                            i2 = i352;
                            i5 = i362;
                        } else if (readInt9 == zziv.zzanj || readInt9 == zziv.zzanu || readInt9 == zziv.zzanv || readInt9 == zziv.zzanw || readInt9 == zziv.zzanx) {
                            zzpx7.setPosition(position3 + 8 + 8);
                            long j5 = Long.MAX_VALUE;
                            if (readInt9 == zziv.zzanj) {
                                str6 = MimeTypes.APPLICATION_TTML;
                                list3 = null;
                            } else if (readInt9 == zziv.zzanu) {
                                String str17 = MimeTypes.APPLICATION_TX3G;
                                int i38 = (readInt8 - 8) - 8;
                                byte[] bArr4 = new byte[i38];
                                zzpx7.zze(bArr4, 0, i38);
                                list3 = Collections.singletonList(bArr4);
                                str6 = str17;
                            } else {
                                if (readInt9 == zziv.zzanv) {
                                    str6 = MimeTypes.APPLICATION_MP4VTT;
                                } else if (readInt9 == zziv.zzanw) {
                                    str6 = MimeTypes.APPLICATION_TTML;
                                    j5 = 0;
                                } else if (readInt9 == zziv.zzanx) {
                                    String str18 = MimeTypes.APPLICATION_MP4CEA608;
                                    zzjb5.zzapi = 1;
                                    str6 = str18;
                                    list3 = null;
                                } else {
                                    throw new IllegalStateException();
                                }
                                list3 = null;
                            }
                            zzje zzje5 = zzje2;
                            int i39 = readInt8;
                            int i40 = position3;
                            i9 = i;
                            zzjb zzjb8 = zzjb5;
                            i4 = readInt;
                            i6 = i21;
                            String str19 = str7;
                            pair3 = create;
                            zzfs zza = zzfs.zza(Integer.toString(zzb), str6, (String) null, -1, 0, str7, -1, zzhp, j5, list3);
                            zzjb zzjb9 = zzjb8;
                            zzjb9.zzaad = zza;
                            str = str19;
                            zzjb2 = zzjb9;
                            zzpx = zzpx7;
                            i3 = i39;
                            zzje = zzje5;
                            i5 = i40;
                        } else {
                            if (readInt9 == zziv.zzaon) {
                                zzjb5.zzaad = zzfs.zza(Integer.toString(zzb), MimeTypes.APPLICATION_CAMERA_MOTION, null, -1, zzhp2);
                            }
                            i3 = readInt8;
                            i5 = position3;
                            i2 = i;
                            zzjb2 = zzjb5;
                            i4 = readInt;
                            i6 = i21;
                            str = str7;
                            zzpx = zzpx7;
                            pair2 = create;
                            zzje = zzje2;
                        }
                        i2 = i9;
                        pair2 = pair3;
                    }
                    zzpx zzpx92 = zzpx;
                    zzpx92.setPosition(i5 + i3);
                    i = i2 + 1;
                    zzhp2 = zzhp;
                    zzjb5 = zzjb2;
                    zzpx7 = zzpx92;
                    i21 = i6;
                    zzje2 = zzje;
                    readInt = i4;
                    str7 = str;
                    create = pair2;
                    zziw zziw32 = zziw;
                }
                zzjb = zzjb5;
                int i41 = i21;
                Pair pair5 = create;
                zzje zzje6 = zzje2;
                zzaj = zziw.zzaj(zziv.zzamp);
                if (zzaj != null) {
                    zzix zzai = zzaj.zzai(zziv.zzamq);
                    if (zzai != null) {
                        zzpx zzpx10 = zzai.zzaos;
                        zzpx10.setPosition(8);
                        int zzaf3 = zziv.zzaf(zzpx10.readInt());
                        int zzhg = zzpx10.zzhg();
                        long[] jArr = new long[zzhg];
                        long[] jArr2 = new long[zzhg];
                        int i42 = 0;
                        while (i42 < zzhg) {
                            jArr[i42] = zzaf3 == 1 ? zzpx10.zzhh() : zzpx10.zzhd();
                            jArr2[i42] = zzaf3 == 1 ? zzpx10.readLong() : (long) zzpx10.readInt();
                            if (zzpx10.readShort() == 1) {
                                zzpx10.zzbl(2);
                                i42++;
                            } else {
                                throw new IllegalArgumentException("Unsupported media rate.");
                            }
                        }
                        pair = Pair.create(jArr, jArr2);
                        zzjs = null;
                        if (zzjb.zzaad == null) {
                            return zzjs;
                        }
                        zzjs zzjs2 = new zzjs(zzje6.id, i41, ((Long) pair5.first).longValue(), zzhd2, j4, zzjb.zzaad, zzjb.zzapi, zzjb.zzaph, zzjb.zzakx, (long[]) pair.first, (long[]) pair.second);
                        return zzjs2;
                    }
                }
                zzjs = null;
                pair = Pair.create(null, null);
                if (zzjb.zzaad == null) {
                }
            }
        }
        j2 = -9223372036854775807L;
        zzpx4.zzbl(16);
        int readInt42 = zzpx4.readInt();
        int readInt52 = zzpx4.readInt();
        zzpx4.zzbl(4);
        int readInt62 = zzpx4.readInt();
        int readInt72 = zzpx4.readInt();
        if (readInt42 != 0) {
        }
        zzje zzje22 = new zzje(readInt3, j2, i24);
        if (j != C.TIME_UNSET) {
        }
        zzpx zzpx52 = zzix2.zzaos;
        zzpx52.setPosition(8);
        zzpx52.zzbl(zziv.zzaf(zzpx52.readInt()) != 0 ? 8 : 16);
        long zzhd22 = zzpx52.zzhd();
        if (j3 != C.TIME_UNSET) {
        }
        zziw zzaj32 = zzaj2.zzaj(zziv.zzamf).zzaj(zziv.zzamg);
        zzpx zzpx62 = zzaj2.zzai(zziv.zzamr).zzaos;
        zzpx62.setPosition(8);
        int zzaf22 = zziv.zzaf(zzpx62.readInt());
        zzpx62.zzbl(zzaf22 != 0 ? 8 : 16);
        long zzhd32 = zzpx62.zzhd();
        zzpx62.zzbl(zzaf22 != 0 ? 4 : 8);
        int readUnsignedShort4 = zzpx62.readUnsignedShort();
        char c4 = (char) (((readUnsignedShort4 >> 10) & 31) + 96);
        char c22 = (char) (((readUnsignedShort4 >> 5) & 31) + 96);
        char c32 = (char) ((readUnsignedShort4 & 31) + 96);
        StringBuilder sb2 = new StringBuilder(3);
        sb2.append(c4);
        sb2.append(c22);
        sb2.append(c32);
        Pair create2 = Pair.create(Long.valueOf(zzhd32), sb2.toString());
        zzpx zzpx72 = zzaj32.zzai(zziv.zzamt).zzaos;
        int zzb4 = zzje22.id;
        int zzc2 = zzje22.zzzo;
        String str72 = (String) create2.second;
        zzpx72.setPosition(12);
        readInt = zzpx72.readInt();
        zzjb zzjb52 = new zzjb(readInt);
        i = 0;
        while (i < readInt) {
        }
        zzjb = zzjb52;
        int i412 = i21;
        Pair pair52 = create2;
        zzje zzje62 = zzje22;
        zzaj = zziw.zzaj(zziv.zzamp);
        if (zzaj != null) {
        }
        zzjs = null;
        pair = Pair.create(null, null);
        if (zzjb.zzaad == null) {
        }
    }

    public static zzjv zza(zzjs zzjs, zziw zziw, zzid zzid) throws zzfx {
        zzja zzja;
        boolean z;
        int i;
        int i2;
        int i3;
        int i4;
        int[] iArr;
        long[] jArr;
        int[] iArr2;
        long[] jArr2;
        long j;
        int[] iArr3;
        long[] jArr3;
        boolean z2;
        long[] jArr4;
        int i5;
        long[] jArr5;
        int[] iArr4;
        int[] iArr5;
        int[] iArr6;
        int i6;
        zzja zzja2;
        zzjs zzjs2 = zzjs;
        zziw zziw2 = zziw;
        zzid zzid2 = zzid;
        zzix zzai = zziw2.zzai(zziv.zzanq);
        if (zzai != null) {
            zzja = new zzjc(zzai);
        } else {
            zzix zzai2 = zziw2.zzai(zziv.zzanr);
            if (zzai2 != null) {
                zzja = new zzjd(zzai2);
            } else {
                throw new zzfx("Track has no sample table size information");
            }
        }
        int zzef = zzja.zzef();
        if (zzef == 0) {
            zzjv zzjv = new zzjv(new long[0], new int[0], 0, new long[0], new int[0]);
            return zzjv;
        }
        zzix zzai3 = zziw2.zzai(zziv.zzans);
        if (zzai3 == null) {
            zzai3 = zziw2.zzai(zziv.zzant);
            z = true;
        } else {
            z = false;
        }
        zzpx zzpx = zzai3.zzaos;
        zzpx zzpx2 = zziw2.zzai(zziv.zzanp).zzaos;
        zzpx zzpx3 = zziw2.zzai(zziv.zzanm).zzaos;
        zzix zzai4 = zziw2.zzai(zziv.zzann);
        zzpx zzpx4 = zzai4 != null ? zzai4.zzaos : null;
        zzix zzai5 = zziw2.zzai(zziv.zzano);
        zzpx zzpx5 = zzai5 != null ? zzai5.zzaos : null;
        zziz zziz = new zziz(zzpx2, zzpx, z);
        zzpx3.setPosition(12);
        int zzhg = zzpx3.zzhg() - 1;
        int zzhg2 = zzpx3.zzhg();
        int zzhg3 = zzpx3.zzhg();
        if (zzpx5 != null) {
            zzpx5.setPosition(12);
            i = zzpx5.zzhg();
        } else {
            i = 0;
        }
        int i7 = -1;
        if (zzpx4 != null) {
            zzpx4.setPosition(12);
            i2 = zzpx4.zzhg();
            if (i2 > 0) {
                i7 = zzpx4.zzhg() - 1;
            } else {
                zzpx4 = null;
            }
        } else {
            i2 = 0;
        }
        long j2 = 0;
        if (!(zzja.zzeh() && MimeTypes.AUDIO_RAW.equals(zzjs2.zzaad.zzzj) && zzhg == 0 && i == 0 && i2 == 0)) {
            jArr2 = new long[zzef];
            iArr = new int[zzef];
            jArr = new long[zzef];
            int i8 = i2;
            iArr2 = new int[zzef];
            zzpx zzpx6 = zzpx3;
            int i9 = i;
            int i10 = i7;
            long j3 = 0;
            int i11 = i8;
            int i12 = 0;
            int i13 = 0;
            int i14 = 0;
            int i15 = 0;
            int i16 = zzhg;
            long j4 = 0;
            int i17 = 0;
            int i18 = zzhg3;
            int i19 = zzhg2;
            int i20 = i18;
            while (i17 < zzef) {
                while (i14 == 0) {
                    zzpo.checkState(zziz.zzee());
                    int i21 = i20;
                    int i22 = i16;
                    long j5 = zziz.zzapb;
                    i14 = zziz.zzapa;
                    i20 = i21;
                    i16 = i22;
                    j4 = j5;
                }
                int i23 = i20;
                int i24 = i16;
                if (zzpx5 != null) {
                    while (i15 == 0 && i9 > 0) {
                        i15 = zzpx5.zzhg();
                        i13 = zzpx5.readInt();
                        i9--;
                    }
                    i15--;
                }
                int i25 = i13;
                jArr2[i17] = j4;
                iArr[i17] = zzja.zzeg();
                if (iArr[i17] > i12) {
                    i6 = zzef;
                    zzja2 = zzja;
                    i12 = iArr[i17];
                } else {
                    i6 = zzef;
                    zzja2 = zzja;
                }
                jArr[i17] = j3 + ((long) i25);
                iArr2[i17] = zzpx4 == null ? 1 : 0;
                if (i17 == i10) {
                    iArr2[i17] = 1;
                    i11--;
                    if (i11 > 0) {
                        i10 = zzpx4.zzhg() - 1;
                    }
                }
                int i26 = i11;
                int i27 = i10;
                int i28 = i23;
                j3 += (long) i28;
                i19--;
                if (i19 != 0 || i24 <= 0) {
                    i16 = i24;
                } else {
                    i16 = i24 - 1;
                    i19 = zzpx6.zzhg();
                    i28 = zzpx6.zzhg();
                }
                j4 += (long) iArr[i17];
                i14--;
                i17++;
                i13 = i25;
                zzja = zzja2;
                zzef = i6;
                i10 = i27;
                i20 = i28;
                i11 = i26;
            }
            i3 = zzef;
            int i29 = i16;
            zzpo.checkArgument(i15 == 0);
            while (i9 > 0) {
                zzpo.checkArgument(zzpx5.zzhg() == 0);
                zzpx5.readInt();
                i9--;
            }
            if (i11 == 0 && i19 == 0 && i14 == 0 && i29 == 0) {
                zzjs2 = zzjs;
            } else {
                int i30 = i11;
                zzjs2 = zzjs;
                int i31 = zzjs2.id;
                StringBuilder sb = new StringBuilder(215);
                sb.append("Inconsistent stbl box for track ");
                sb.append(i31);
                sb.append(": remainingSynchronizationSamples ");
                sb.append(i30);
                sb.append(", remainingSamplesAtTimestampDelta ");
                sb.append(i19);
                sb.append(", remainingSamplesInChunk ");
                sb.append(i14);
                sb.append(", remainingTimestampDeltaChanges ");
                sb.append(i29);
                Log.w("AtomParsers", sb.toString());
            }
            j = j3;
            i4 = i12;
        } else {
            i3 = zzef;
            zzja zzja3 = zzja;
            long[] jArr6 = new long[zziz.length];
            int[] iArr7 = new int[zziz.length];
            while (zziz.zzee()) {
                jArr6[zziz.index] = zziz.zzapb;
                iArr7[zziz.index] = zziz.zzapa;
            }
            int zzeg = zzja3.zzeg();
            long j6 = (long) zzhg3;
            int i32 = 8192 / zzeg;
            int i33 = 0;
            for (int zzf : iArr7) {
                i33 += zzqe.zzf(zzf, i32);
            }
            long[] jArr7 = new long[i33];
            int[] iArr8 = new int[i33];
            long[] jArr8 = new long[i33];
            int[] iArr9 = new int[i33];
            int i34 = 0;
            int i35 = 0;
            int i36 = 0;
            for (int i37 = 0; i37 < iArr7.length; i37++) {
                int i38 = iArr7[i37];
                long j7 = jArr6[i37];
                while (i38 > 0) {
                    int min = Math.min(i32, i38);
                    jArr7[i35] = j7;
                    iArr8[i35] = zzeg * min;
                    long[] jArr9 = jArr6;
                    i36 = Math.max(i36, iArr8[i35]);
                    int[] iArr10 = iArr7;
                    jArr8[i35] = ((long) i34) * j6;
                    iArr9[i35] = 1;
                    j7 += (long) iArr8[i35];
                    i34 += min;
                    i38 -= min;
                    i35++;
                    jArr6 = jArr9;
                    iArr7 = iArr10;
                }
                long[] jArr10 = jArr6;
                int[] iArr11 = iArr7;
            }
            zzjh zzjh = new zzjh(jArr7, iArr8, i36, jArr8, iArr9);
            jArr2 = zzjh.zzagu;
            iArr = zzjh.zzagt;
            int i39 = zzjh.zzapp;
            jArr = zzjh.zzapq;
            iArr2 = zzjh.zzapr;
            i4 = i39;
            j = 0;
        }
        if (zzjs2.zzaso == null || zzid.zzea()) {
            long[] jArr11 = jArr2;
            int[] iArr12 = iArr2;
            int[] iArr13 = iArr;
            zzqe.zza(jArr, 1000000, zzjs2.zzcr);
            zzjv zzjv2 = new zzjv(jArr11, iArr13, i4, jArr, iArr12);
            return zzjv2;
        }
        if (zzjs2.zzaso.length == 1 && zzjs2.type == 1 && jArr.length >= 2) {
            long j8 = zzjs2.zzasp[0];
            long zza = zzqe.zza(zzjs2.zzaso[0], zzjs2.zzcr, zzjs2.zzasl) + j8;
            if (jArr[0] <= j8 && j8 < jArr[1] && jArr[jArr.length - 1] < zza && zza <= j) {
                long j9 = j - zza;
                long zza2 = zzqe.zza(j8 - jArr[0], (long) zzjs2.zzaad.zzzu, zzjs2.zzcr);
                long zza3 = zzqe.zza(j9, (long) zzjs2.zzaad.zzzu, zzjs2.zzcr);
                if (!(zza2 == 0 && zza3 == 0) && zza2 <= 2147483647L && zza3 <= 2147483647L) {
                    zzid zzid3 = zzid;
                    zzid3.zzzw = (int) zza2;
                    zzid3.zzzx = (int) zza3;
                    zzqe.zza(jArr, 1000000, zzjs2.zzcr);
                    zzjv zzjv3 = new zzjv(jArr2, iArr, i4, jArr, iArr2);
                    return zzjv3;
                }
            }
        }
        if (zzjs2.zzaso.length == 1) {
            char c = 0;
            if (zzjs2.zzaso[0] == 0) {
                int i40 = 0;
                while (i40 < jArr.length) {
                    jArr[i40] = zzqe.zza(jArr[i40] - zzjs2.zzasp[c], 1000000, zzjs2.zzcr);
                    i40++;
                    c = 0;
                }
                zzjv zzjv4 = new zzjv(jArr2, iArr, i4, jArr, iArr2);
                return zzjv4;
            }
        }
        boolean z3 = zzjs2.type == 1;
        int i41 = 0;
        boolean z4 = false;
        int i42 = 0;
        int i43 = 0;
        while (i41 < zzjs2.zzaso.length) {
            long j10 = zzjs2.zzasp[i41];
            if (j10 != -1) {
                iArr6 = iArr;
                long zza4 = zzqe.zza(zzjs2.zzaso[i41], zzjs2.zzcr, zzjs2.zzasl);
                int zzb = zzqe.zzb(jArr, j10, true, true);
                int zzb2 = zzqe.zzb(jArr, j10 + zza4, z3, false);
                i42 += zzb2 - zzb;
                z4 |= i43 != zzb;
                i43 = zzb2;
            } else {
                iArr6 = iArr;
            }
            i41++;
            iArr = iArr6;
        }
        int[] iArr14 = iArr;
        boolean z5 = (i42 != i3) | z4;
        long[] jArr12 = z5 ? new long[i42] : jArr2;
        int[] iArr15 = z5 ? new int[i42] : iArr14;
        if (z5) {
            i4 = 0;
        }
        int[] iArr16 = z5 ? new int[i42] : iArr2;
        long[] jArr13 = new long[i42];
        int i44 = i4;
        int i45 = 0;
        int i46 = 0;
        while (i45 < zzjs2.zzaso.length) {
            long j11 = zzjs2.zzasp[i45];
            long j12 = zzjs2.zzaso[i45];
            if (j11 != -1) {
                int[] iArr17 = iArr16;
                i5 = i45;
                long[] jArr14 = jArr12;
                jArr4 = jArr13;
                long zza5 = zzqe.zza(j12, zzjs2.zzcr, zzjs2.zzasl) + j11;
                int zzb3 = zzqe.zzb(jArr, j11, true, true);
                int zzb4 = zzqe.zzb(jArr, zza5, z3, false);
                if (z5) {
                    int i47 = zzb4 - zzb3;
                    jArr5 = jArr14;
                    System.arraycopy(jArr2, zzb3, jArr5, i46, i47);
                    iArr4 = iArr14;
                    System.arraycopy(iArr4, zzb3, iArr15, i46, i47);
                    z2 = z3;
                    iArr5 = iArr17;
                    System.arraycopy(iArr2, zzb3, iArr5, i46, i47);
                } else {
                    z2 = z3;
                    iArr4 = iArr14;
                    iArr5 = iArr17;
                    jArr5 = jArr14;
                }
                int i48 = i44;
                while (zzb3 < zzb4) {
                    long[] jArr15 = jArr2;
                    int[] iArr18 = iArr2;
                    long j13 = j11;
                    jArr4[i46] = zzqe.zza(j2, 1000000, zzjs2.zzasl) + zzqe.zza(jArr[zzb3] - j11, 1000000, zzjs2.zzcr);
                    if (z5 && iArr15[i46] > i48) {
                        i48 = iArr4[zzb3];
                    }
                    i46++;
                    zzb3++;
                    jArr2 = jArr15;
                    iArr2 = iArr18;
                    j11 = j13;
                }
                jArr3 = jArr2;
                iArr3 = iArr2;
                i44 = i48;
            } else {
                z2 = z3;
                jArr5 = jArr12;
                jArr4 = jArr13;
                jArr3 = jArr2;
                iArr3 = iArr2;
                iArr5 = iArr16;
                i5 = i45;
                iArr4 = iArr14;
            }
            j2 += j12;
            i45 = i5 + 1;
            iArr14 = iArr4;
            jArr12 = jArr5;
            jArr13 = jArr4;
            jArr2 = jArr3;
            iArr2 = iArr3;
            iArr16 = iArr5;
            z3 = z2;
        }
        long[] jArr16 = jArr12;
        long[] jArr17 = jArr13;
        int[] iArr19 = iArr16;
        boolean z6 = false;
        for (int i49 = 0; i49 < iArr19.length && !z6; i49++) {
            z6 |= (iArr19[i49] & 1) != 0;
        }
        if (z6) {
            zzjv zzjv5 = new zzjv(jArr16, iArr15, i44, jArr17, iArr19);
            return zzjv5;
        }
        throw new zzfx("The edited sample sequence does not contain a sync sample.");
    }

    public static zzki zza(zzix zzix, boolean z) {
        if (z) {
            return null;
        }
        zzpx zzpx = zzix.zzaos;
        zzpx.setPosition(8);
        while (zzpx.zzhb() >= 8) {
            int position = zzpx.getPosition();
            int readInt = zzpx.readInt();
            if (zzpx.readInt() == zziv.zzaob) {
                zzpx.setPosition(position);
                int i = position + readInt;
                zzpx.zzbl(12);
                while (true) {
                    if (zzpx.getPosition() >= i) {
                        break;
                    }
                    int position2 = zzpx.getPosition();
                    int readInt2 = zzpx.readInt();
                    if (zzpx.readInt() == zziv.zzaoc) {
                        zzpx.setPosition(position2);
                        int i2 = position2 + readInt2;
                        zzpx.zzbl(8);
                        ArrayList arrayList = new ArrayList();
                        while (zzpx.getPosition() < i2) {
                            zza zzd = zzjm.zzd(zzpx);
                            if (zzd != null) {
                                arrayList.add(zzd);
                            }
                        }
                        if (!arrayList.isEmpty()) {
                            return new zzki((List<? extends zza>) arrayList);
                        }
                    } else {
                        zzpx.zzbl(readInt2 - 8);
                    }
                }
                return null;
            }
            zzpx.zzbl(readInt - 8);
        }
        return null;
    }

    private static Pair<String, byte[]> zzb(zzpx zzpx, int i) {
        zzpx.setPosition(i + 8 + 4);
        zzpx.zzbl(1);
        zzc(zzpx);
        zzpx.zzbl(2);
        int readUnsignedByte = zzpx.readUnsignedByte();
        if ((readUnsignedByte & 128) != 0) {
            zzpx.zzbl(2);
        }
        if ((readUnsignedByte & 64) != 0) {
            zzpx.zzbl(zzpx.readUnsignedShort());
        }
        if ((readUnsignedByte & 32) != 0) {
            zzpx.zzbl(2);
        }
        zzpx.zzbl(1);
        zzc(zzpx);
        String str = null;
        switch (zzpx.readUnsignedByte()) {
            case 32:
                str = MimeTypes.VIDEO_MP4V;
                break;
            case 33:
                str = MimeTypes.VIDEO_H264;
                break;
            case 35:
                str = MimeTypes.VIDEO_H265;
                break;
            case 64:
            case 102:
            case 103:
            case 104:
                str = MimeTypes.AUDIO_AAC;
                break;
            case 107:
                return Pair.create(MimeTypes.AUDIO_MPEG, null);
            case 165:
                str = MimeTypes.AUDIO_AC3;
                break;
            case 166:
                str = MimeTypes.AUDIO_E_AC3;
                break;
            case 169:
            case 172:
                return Pair.create(MimeTypes.AUDIO_DTS, null);
            case 170:
            case 171:
                return Pair.create(MimeTypes.AUDIO_DTS_HD, null);
        }
        zzpx.zzbl(12);
        zzpx.zzbl(1);
        int zzc = zzc(zzpx);
        byte[] bArr = new byte[zzc];
        zzpx.zze(bArr, 0, zzc);
        return Pair.create(str, bArr);
    }

    private static int zza(zzpx zzpx, int i, int i2, zzjb zzjb, int i3) {
        Object obj;
        zzpx zzpx2 = zzpx;
        int position = zzpx.getPosition();
        while (true) {
            boolean z = false;
            if (position - i >= i2) {
                return 0;
            }
            zzpx2.setPosition(position);
            int readInt = zzpx.readInt();
            zzpo.checkArgument(readInt > 0, "childAtomSize should be positive");
            if (zzpx.readInt() == zziv.zzamv) {
                int i4 = position + 8;
                Pair pair = null;
                Object obj2 = null;
                Object obj3 = null;
                boolean z2 = false;
                while (i4 - position < readInt) {
                    zzpx2.setPosition(i4);
                    int readInt2 = zzpx.readInt();
                    int readInt3 = zzpx.readInt();
                    if (readInt3 == zziv.zzanb) {
                        obj2 = Integer.valueOf(zzpx.readInt());
                    } else if (readInt3 == zziv.zzamw) {
                        zzpx2.zzbl(4);
                        z2 = zzpx.readInt() == zzaoz;
                    } else if (readInt3 == zziv.zzamx) {
                        int i5 = i4 + 8;
                        while (true) {
                            if (i5 - i4 >= readInt2) {
                                obj = null;
                                break;
                            }
                            zzpx2.setPosition(i5);
                            int readInt4 = zzpx.readInt();
                            if (zzpx.readInt() == zziv.zzamy) {
                                zzpx2.zzbl(6);
                                boolean z3 = zzpx.readUnsignedByte() == 1;
                                int readUnsignedByte = zzpx.readUnsignedByte();
                                byte[] bArr = new byte[16];
                                zzpx2.zze(bArr, 0, 16);
                                obj = new zzjt(z3, readUnsignedByte, bArr);
                            } else {
                                i5 += readInt4;
                            }
                        }
                        obj3 = obj;
                    }
                    i4 += readInt2;
                }
                if (z2) {
                    zzpo.checkArgument(obj2 != null, "frma atom is mandatory");
                    if (obj3 != null) {
                        z = true;
                    }
                    zzpo.checkArgument(z, "schi->tenc atom is mandatory");
                    pair = Pair.create(obj2, obj3);
                }
                if (pair != null) {
                    zzjb.zzaph[i3] = (zzjt) pair.second;
                    return ((Integer) pair.first).intValue();
                }
            }
            zzjb zzjb2 = zzjb;
            position += readInt;
        }
    }

    private static int zzc(zzpx zzpx) {
        int readUnsignedByte = zzpx.readUnsignedByte();
        int i = readUnsignedByte & 127;
        while ((readUnsignedByte & 128) == 128) {
            readUnsignedByte = zzpx.readUnsignedByte();
            i = (i << 7) | (readUnsignedByte & 127);
        }
        return i;
    }
}
