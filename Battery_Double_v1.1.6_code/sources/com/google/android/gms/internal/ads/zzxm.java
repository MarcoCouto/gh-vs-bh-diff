package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public abstract class zzxm extends zzex implements zzxl {
    public zzxm() {
        super("com.google.android.gms.ads.internal.client.IAdManager");
    }

    public static zzxl zzb(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
        if (queryLocalInterface instanceof zzxl) {
            return (zzxl) queryLocalInterface;
        }
        return new zzxn(iBinder);
    }

    /* JADX WARNING: type inference failed for: r4v1 */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.google.android.gms.internal.ads.zzxa] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.google.android.gms.internal.ads.zzxc] */
    /* JADX WARNING: type inference failed for: r4v6, types: [com.google.android.gms.internal.ads.zzxa] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.google.android.gms.internal.ads.zzxt] */
    /* JADX WARNING: type inference failed for: r4v9, types: [com.google.android.gms.internal.ads.zzxv] */
    /* JADX WARNING: type inference failed for: r4v11, types: [com.google.android.gms.internal.ads.zzxt] */
    /* JADX WARNING: type inference failed for: r4v12, types: [com.google.android.gms.internal.ads.zzwx] */
    /* JADX WARNING: type inference failed for: r4v14, types: [com.google.android.gms.internal.ads.zzwz] */
    /* JADX WARNING: type inference failed for: r4v16, types: [com.google.android.gms.internal.ads.zzwx] */
    /* JADX WARNING: type inference failed for: r4v17, types: [com.google.android.gms.internal.ads.zzxz] */
    /* JADX WARNING: type inference failed for: r4v19, types: [com.google.android.gms.internal.ads.zzyb] */
    /* JADX WARNING: type inference failed for: r4v21, types: [com.google.android.gms.internal.ads.zzxz] */
    /* JADX WARNING: type inference failed for: r4v22, types: [com.google.android.gms.internal.ads.zzxq] */
    /* JADX WARNING: type inference failed for: r4v24, types: [com.google.android.gms.internal.ads.zzxs] */
    /* JADX WARNING: type inference failed for: r4v26, types: [com.google.android.gms.internal.ads.zzxq] */
    /* JADX WARNING: type inference failed for: r4v27 */
    /* JADX WARNING: type inference failed for: r4v28 */
    /* JADX WARNING: type inference failed for: r4v29 */
    /* JADX WARNING: type inference failed for: r4v30 */
    /* JADX WARNING: type inference failed for: r4v31 */
    /* JADX WARNING: type inference failed for: r4v32 */
    /* JADX WARNING: type inference failed for: r4v33 */
    /* JADX WARNING: type inference failed for: r4v34 */
    /* JADX WARNING: type inference failed for: r4v35 */
    /* JADX WARNING: type inference failed for: r4v36 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r4v1
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ads.zzxv, com.google.android.gms.internal.ads.zzxc, com.google.android.gms.internal.ads.zzxa, com.google.android.gms.internal.ads.zzxt, com.google.android.gms.internal.ads.zzwz, com.google.android.gms.internal.ads.zzwx, com.google.android.gms.internal.ads.zzyb, com.google.android.gms.internal.ads.zzxz, com.google.android.gms.internal.ads.zzxs, com.google.android.gms.internal.ads.zzxq]
  uses: [com.google.android.gms.internal.ads.zzxa, com.google.android.gms.internal.ads.zzxt, com.google.android.gms.internal.ads.zzwx, com.google.android.gms.internal.ads.zzxz, com.google.android.gms.internal.ads.zzxq]
  mth insns count: 164
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 11 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ? r4 = 0;
        switch (i) {
            case 1:
                IObjectWrapper zzie = zzie();
                parcel2.writeNoException();
                zzey.zza(parcel2, (IInterface) zzie);
                break;
            case 2:
                destroy();
                parcel2.writeNoException();
                break;
            case 3:
                boolean isReady = isReady();
                parcel2.writeNoException();
                zzey.writeBoolean(parcel2, isReady);
                break;
            case 4:
                boolean zzb = zzb((zzwb) zzey.zza(parcel, zzwb.CREATOR));
                parcel2.writeNoException();
                zzey.writeBoolean(parcel2, zzb);
                break;
            case 5:
                pause();
                parcel2.writeNoException();
                break;
            case 6:
                resume();
                parcel2.writeNoException();
                break;
            case 7:
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
                    if (queryLocalInterface instanceof zzxa) {
                        r4 = (zzxa) queryLocalInterface;
                    } else {
                        r4 = new zzxc(readStrongBinder);
                    }
                }
                zza((zzxa) r4);
                parcel2.writeNoException();
                break;
            case 8:
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
                    if (queryLocalInterface2 instanceof zzxt) {
                        r4 = (zzxt) queryLocalInterface2;
                    } else {
                        r4 = new zzxv(readStrongBinder2);
                    }
                }
                zza((zzxt) r4);
                parcel2.writeNoException();
                break;
            case 9:
                showInterstitial();
                parcel2.writeNoException();
                break;
            case 10:
                stopLoading();
                parcel2.writeNoException();
                break;
            case 11:
                zzih();
                parcel2.writeNoException();
                break;
            case 12:
                zzwf zzif = zzif();
                parcel2.writeNoException();
                zzey.zzb(parcel2, zzif);
                break;
            case 13:
                zza((zzwf) zzey.zza(parcel, zzwf.CREATOR));
                parcel2.writeNoException();
                break;
            case 14:
                zza(zzaox.zzy(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 15:
                zza(zzapd.zzaa(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                break;
            case 18:
                String mediationAdapterClassName = getMediationAdapterClassName();
                parcel2.writeNoException();
                parcel2.writeString(mediationAdapterClassName);
                break;
            case 19:
                zza(zzabh.zzh(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 20:
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdClickListener");
                    if (queryLocalInterface3 instanceof zzwx) {
                        r4 = (zzwx) queryLocalInterface3;
                    } else {
                        r4 = new zzwz(readStrongBinder3);
                    }
                }
                zza((zzwx) r4);
                parcel2.writeNoException();
                break;
            case 21:
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
                    if (queryLocalInterface4 instanceof zzxz) {
                        r4 = (zzxz) queryLocalInterface4;
                    } else {
                        r4 = new zzyb(readStrongBinder4);
                    }
                }
                zza((zzxz) r4);
                parcel2.writeNoException();
                break;
            case 22:
                setManualImpressionsEnabled(zzey.zza(parcel));
                parcel2.writeNoException();
                break;
            case 23:
                boolean isLoading = isLoading();
                parcel2.writeNoException();
                zzey.writeBoolean(parcel2, isLoading);
                break;
            case 24:
                zza(zzavc.zzac(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 25:
                setUserId(parcel.readString());
                parcel2.writeNoException();
                break;
            case 26:
                zzyp videoController = getVideoController();
                parcel2.writeNoException();
                zzey.zza(parcel2, (IInterface) videoController);
                break;
            case 29:
                zza((zzzw) zzey.zza(parcel, zzzw.CREATOR));
                parcel2.writeNoException();
                break;
            case 30:
                zza((zzyv) zzey.zza(parcel, zzyv.CREATOR));
                parcel2.writeNoException();
                break;
            case 31:
                String adUnitId = getAdUnitId();
                parcel2.writeNoException();
                parcel2.writeString(adUnitId);
                break;
            case 32:
                zzxt zzir = zzir();
                parcel2.writeNoException();
                zzey.zza(parcel2, (IInterface) zzir);
                break;
            case 33:
                zzxa zzis = zzis();
                parcel2.writeNoException();
                zzey.zza(parcel2, (IInterface) zzis);
                break;
            case 34:
                setImmersiveMode(zzey.zza(parcel));
                parcel2.writeNoException();
                break;
            case 35:
                String zzje = zzje();
                parcel2.writeNoException();
                parcel2.writeString(zzje);
                break;
            case 36:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdMetadataListener");
                    if (queryLocalInterface5 instanceof zzxq) {
                        r4 = (zzxq) queryLocalInterface5;
                    } else {
                        r4 = new zzxs(readStrongBinder5);
                    }
                }
                zza((zzxq) r4);
                parcel2.writeNoException();
                break;
            case 37:
                Bundle adMetadata = getAdMetadata();
                parcel2.writeNoException();
                zzey.zzb(parcel2, adMetadata);
                break;
            case 38:
                zzap(parcel.readString());
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
