package com.google.android.gms.internal.ads;

import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.internal.ads.zzhp.zza;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.UUID;

public final class zzji implements zzhz {
    private static final zzic zzahq = new zzjj();
    private static final int zzaps = zzqe.zzam("seig");
    private static final byte[] zzapt = {-94, 57, 79, 82, 90, -101, 79, 20, -94, 68, 108, 66, 124, 100, -115, -12};
    private final int flags;
    private long zzaan;
    private final zzpx zzahx;
    private int zzajm;
    private int zzajn;
    private zzib zzajq;
    private final zzjs zzapu;
    private final SparseArray<zzjl> zzapv;
    private final zzpx zzapw;
    private final zzpx zzapx;
    private final zzpx zzapy;
    private final zzqb zzapz;
    private final zzpx zzaqa;
    private final byte[] zzaqb;
    private final Stack<zziw> zzaqc;
    private final LinkedList<zzjk> zzaqd;
    private int zzaqe;
    private int zzaqf;
    private long zzaqg;
    private int zzaqh;
    private zzpx zzaqi;
    private long zzaqj;
    private int zzaqk;
    private long zzaql;
    private zzjl zzaqm;
    private int zzaqn;
    private boolean zzaqo;
    private zzii zzaqp;
    private zzii[] zzaqq;
    private boolean zzaqr;

    public zzji() {
        this(0);
    }

    public final void release() {
    }

    public zzji(int i) {
        this(i, null);
    }

    private zzji(int i, zzqb zzqb) {
        this(i, null, null);
    }

    private zzji(int i, zzqb zzqb, zzjs zzjs) {
        this.flags = i;
        this.zzapz = null;
        this.zzapu = null;
        this.zzaqa = new zzpx(16);
        this.zzahx = new zzpx(zzpu.zzbhi);
        this.zzapw = new zzpx(5);
        this.zzapx = new zzpx();
        this.zzapy = new zzpx(1);
        this.zzaqb = new byte[16];
        this.zzaqc = new Stack<>();
        this.zzaqd = new LinkedList<>();
        this.zzapv = new SparseArray<>();
        this.zzaan = C.TIME_UNSET;
        this.zzaql = C.TIME_UNSET;
        zzei();
    }

    public final boolean zza(zzia zzia) throws IOException, InterruptedException {
        return zzjr.zzd(zzia);
    }

    public final void zza(zzib zzib) {
        this.zzajq = zzib;
    }

    public final void zzc(long j, long j2) {
        int size = this.zzapv.size();
        for (int i = 0; i < size; i++) {
            ((zzjl) this.zzapv.valueAt(i)).reset();
        }
        this.zzaqd.clear();
        this.zzaqk = 0;
        this.zzaqc.clear();
        zzei();
    }

    /* JADX WARNING: Removed duplicated region for block: B:255:0x0610 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0370 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x0004 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x0004 A[SYNTHETIC] */
    public final int zza(zzia zzia, zzif zzif) throws IOException, InterruptedException {
        boolean z;
        long zzhh;
        long zzhh2;
        boolean z2;
        int i;
        zzjt zzjt;
        zzij zzij;
        zzjt zzjt2;
        int i2;
        int i3;
        zzjt zzjt3;
        int i4;
        zzia zzia2 = zzia;
        while (true) {
            zzjl zzjl = null;
            int i5 = 0;
            switch (this.zzaqe) {
                case 0:
                    if (this.zzaqh == 0) {
                        if (!zzia2.zza(this.zzaqa.data, 0, 8, true)) {
                            z = false;
                            if (!z) {
                                break;
                            } else {
                                return -1;
                            }
                        } else {
                            this.zzaqh = 8;
                            this.zzaqa.setPosition(0);
                            this.zzaqg = this.zzaqa.zzhd();
                            this.zzaqf = this.zzaqa.readInt();
                        }
                    }
                    if (this.zzaqg == 1) {
                        zzia2.readFully(this.zzaqa.data, 8, 8);
                        this.zzaqh += 8;
                        this.zzaqg = this.zzaqa.zzhh();
                    }
                    if (this.zzaqg >= ((long) this.zzaqh)) {
                        long position = zzia.getPosition() - ((long) this.zzaqh);
                        if (this.zzaqf == zziv.zzamk) {
                            int size = this.zzapv.size();
                            for (int i6 = 0; i6 < size; i6++) {
                                zzju zzju = ((zzjl) this.zzapv.valueAt(i6)).zzaqt;
                                zzju.zzasu = position;
                                zzju.zzasw = position;
                                zzju.zzasv = position;
                            }
                        }
                        if (this.zzaqf == zziv.zzalh) {
                            this.zzaqm = null;
                            this.zzaqj = position + this.zzaqg;
                            if (!this.zzaqr) {
                                this.zzajq.zza(new zzih(this.zzaan));
                                this.zzaqr = true;
                            }
                            this.zzaqe = 2;
                        } else {
                            int i7 = this.zzaqf;
                            if (i7 == zziv.zzamb || i7 == zziv.zzamd || i7 == zziv.zzame || i7 == zziv.zzamf || i7 == zziv.zzamg || i7 == zziv.zzamk || i7 == zziv.zzaml || i7 == zziv.zzamm || i7 == zziv.zzamp) {
                                long position2 = (zzia.getPosition() + this.zzaqg) - 8;
                                this.zzaqc.add(new zziw(this.zzaqf, position2));
                                if (this.zzaqg == ((long) this.zzaqh)) {
                                    zzt(position2);
                                } else {
                                    zzei();
                                }
                            } else {
                                int i8 = this.zzaqf;
                                if (i8 == zziv.zzams || i8 == zziv.zzamr || i8 == zziv.zzamc || i8 == zziv.zzama || i8 == zziv.zzamt || i8 == zziv.zzalw || i8 == zziv.zzalx || i8 == zziv.zzamo || i8 == zziv.zzaly || i8 == zziv.zzalz || i8 == zziv.zzamu || i8 == zziv.zzanc || i8 == zziv.zzand || i8 == zziv.zzanh || i8 == zziv.zzang || i8 == zziv.zzane || i8 == zziv.zzanf || i8 == zziv.zzamq || i8 == zziv.zzamn || i8 == zziv.zzaog) {
                                    if (this.zzaqh != 8) {
                                        throw new zzfx("Leaf atom defines extended atom size (unsupported).");
                                    } else if (this.zzaqg <= 2147483647L) {
                                        this.zzaqi = new zzpx((int) this.zzaqg);
                                        System.arraycopy(this.zzaqa.data, 0, this.zzaqi.data, 0, 8);
                                        this.zzaqe = 1;
                                    } else {
                                        throw new zzfx("Leaf atom with length > 2147483647 (unsupported).");
                                    }
                                } else if (this.zzaqg <= 2147483647L) {
                                    this.zzaqi = null;
                                    this.zzaqe = 1;
                                } else {
                                    throw new zzfx("Skipping atom with length > 2147483647 (unsupported).");
                                }
                            }
                        }
                        z = true;
                        if (!z) {
                        }
                    } else {
                        throw new zzfx("Atom size less than header length (unsupported).");
                    }
                    break;
                case 1:
                    int i9 = ((int) this.zzaqg) - this.zzaqh;
                    if (this.zzaqi != null) {
                        zzia2.readFully(this.zzaqi.data, 8, i9);
                        zzix zzix = new zzix(this.zzaqf, this.zzaqi);
                        long position3 = zzia.getPosition();
                        if (!this.zzaqc.isEmpty()) {
                            ((zziw) this.zzaqc.peek()).zza(zzix);
                        } else if (zzix.type == zziv.zzama) {
                            zzpx zzpx = zzix.zzaos;
                            zzpx.setPosition(8);
                            int zzaf = zziv.zzaf(zzpx.readInt());
                            zzpx.zzbl(4);
                            long zzhd = zzpx.zzhd();
                            if (zzaf == 0) {
                                zzhh = zzpx.zzhd();
                                zzhh2 = position3 + zzpx.zzhd();
                            } else {
                                zzhh = zzpx.zzhh();
                                zzhh2 = position3 + zzpx.zzhh();
                            }
                            long j = zzhh2;
                            long j2 = zzhh;
                            long zza = zzqe.zza(j2, 1000000, zzhd);
                            zzpx.zzbl(2);
                            int readUnsignedShort = zzpx.readUnsignedShort();
                            int[] iArr = new int[readUnsignedShort];
                            long[] jArr = new long[readUnsignedShort];
                            long[] jArr2 = new long[readUnsignedShort];
                            long[] jArr3 = new long[readUnsignedShort];
                            long j3 = j2;
                            long j4 = zza;
                            while (i5 < readUnsignedShort) {
                                int readInt = zzpx.readInt();
                                if ((readInt & Integer.MIN_VALUE) == 0) {
                                    long zzhd2 = zzpx.zzhd();
                                    iArr[i5] = readInt & Integer.MAX_VALUE;
                                    jArr[i5] = j;
                                    jArr3[i5] = j4;
                                    long j5 = j3 + zzhd2;
                                    long[] jArr4 = jArr3;
                                    long[] jArr5 = jArr;
                                    long[] jArr6 = jArr2;
                                    long zza2 = zzqe.zza(j5, 1000000, zzhd);
                                    jArr6[i5] = zza2 - jArr4[i5];
                                    zzpx.zzbl(4);
                                    j += (long) iArr[i5];
                                    i5++;
                                    j3 = j5;
                                    jArr2 = jArr6;
                                    j4 = zza2;
                                    jArr = jArr5;
                                    jArr3 = jArr4;
                                } else {
                                    throw new zzfx("Unhandled indirect reference");
                                }
                            }
                            Pair create = Pair.create(Long.valueOf(zza), new zzhw(iArr, jArr, jArr2, jArr3));
                            this.zzaql = ((Long) create.first).longValue();
                            this.zzajq.zza((zzig) create.second);
                            this.zzaqr = true;
                        } else if (zzix.type == zziv.zzaog) {
                            zzpx zzpx2 = zzix.zzaos;
                            if (this.zzaqp != null) {
                                zzpx2.setPosition(12);
                                zzpx2.zzhi();
                                zzpx2.zzhi();
                                long zza3 = zzqe.zza(zzpx2.zzhd(), 1000000, zzpx2.zzhd());
                                zzpx2.setPosition(12);
                                int zzhb = zzpx2.zzhb();
                                this.zzaqp.zza(zzpx2, zzhb);
                                if (this.zzaql != C.TIME_UNSET) {
                                    this.zzaqp.zza(this.zzaql + zza3, 1, zzhb, 0, null);
                                } else {
                                    this.zzaqd.addLast(new zzjk(zza3, zzhb));
                                    this.zzaqk += zzhb;
                                }
                            }
                        }
                    } else {
                        zzia2.zzw(i9);
                    }
                    zzt(zzia.getPosition());
                    break;
                case 2:
                    int size2 = this.zzapv.size();
                    long j6 = Long.MAX_VALUE;
                    for (int i10 = 0; i10 < size2; i10++) {
                        zzju zzju2 = ((zzjl) this.zzapv.valueAt(i10)).zzaqt;
                        if (zzju2.zzatj && zzju2.zzasw < j6) {
                            j6 = zzju2.zzasw;
                            zzjl = (zzjl) this.zzapv.valueAt(i10);
                        }
                    }
                    if (zzjl == null) {
                        this.zzaqe = 3;
                        break;
                    } else {
                        int position4 = (int) (j6 - zzia.getPosition());
                        if (position4 >= 0) {
                            zzia2.zzw(position4);
                            zzju zzju3 = zzjl.zzaqt;
                            zzia2.readFully(zzju3.zzati.data, 0, zzju3.zzath);
                            zzju3.zzati.setPosition(0);
                            zzju3.zzatj = false;
                            break;
                        } else {
                            throw new zzfx("Offset to encryption data was negative.");
                        }
                    }
                default:
                    if (this.zzaqe == 3) {
                        if (this.zzaqm == null) {
                            SparseArray<zzjl> sparseArray = this.zzapv;
                            int size3 = sparseArray.size();
                            long j7 = Long.MAX_VALUE;
                            zzjl zzjl2 = null;
                            for (int i11 = 0; i11 < size3; i11++) {
                                zzjl zzjl3 = (zzjl) sparseArray.valueAt(i11);
                                if (zzjl3.zzaqy != zzjl3.zzaqt.zzasx) {
                                    long j8 = zzjl3.zzaqt.zzasy[zzjl3.zzaqy];
                                    if (j8 < j7) {
                                        zzjl2 = zzjl3;
                                        j7 = j8;
                                    }
                                }
                            }
                            if (zzjl2 == null) {
                                int position5 = (int) (this.zzaqj - zzia.getPosition());
                                if (position5 >= 0) {
                                    zzia2.zzw(position5);
                                    zzei();
                                    i = 0;
                                    z2 = false;
                                    if (z2) {
                                        break;
                                    } else {
                                        return i;
                                    }
                                } else {
                                    throw new zzfx("Offset to end of mdat was negative.");
                                }
                            } else {
                                int position6 = (int) (zzjl2.zzaqt.zzasy[zzjl2.zzaqy] - zzia.getPosition());
                                if (position6 < 0) {
                                    Log.w("FragmentedMp4Extractor", "Ignoring negative offset to sample data.");
                                    position6 = 0;
                                }
                                zzia2.zzw(position6);
                                this.zzaqm = zzjl2;
                            }
                        }
                        this.zzaqn = this.zzaqm.zzaqt.zzata[this.zzaqm.zzaqw];
                        if (this.zzaqm.zzaqt.zzate) {
                            zzjl zzjl4 = this.zzaqm;
                            zzju zzju4 = zzjl4.zzaqt;
                            zzpx zzpx3 = zzju4.zzati;
                            int i12 = zzju4.zzast.zzapo;
                            if (zzju4.zzatg != null) {
                                zzjt3 = zzju4.zzatg;
                            } else {
                                zzjt3 = zzjl4.zzaqu.zzasn[i12];
                            }
                            int i13 = zzjt3.zzasr;
                            boolean z3 = zzju4.zzatf[zzjl4.zzaqw];
                            this.zzapy.data[0] = (byte) ((z3 ? 128 : 0) | i13);
                            this.zzapy.setPosition(0);
                            zzii zzii = zzjl4.zzakw;
                            zzii.zza(this.zzapy, 1);
                            zzii.zza(zzpx3, i13);
                            if (!z3) {
                                i4 = i13 + 1;
                            } else {
                                int readUnsignedShort2 = zzpx3.readUnsignedShort();
                                zzpx3.zzbl(-2);
                                int i14 = (readUnsignedShort2 * 6) + 2;
                                zzii.zza(zzpx3, i14);
                                i4 = i13 + 1 + i14;
                            }
                            this.zzajn = i4;
                            this.zzaqn += this.zzajn;
                        } else {
                            this.zzajn = 0;
                        }
                        if (this.zzaqm.zzaqu.zzasm == 1) {
                            this.zzaqn -= 8;
                            zzia2.zzw(8);
                        }
                        this.zzaqe = 4;
                        this.zzajm = 0;
                    }
                    zzju zzju5 = this.zzaqm.zzaqt;
                    zzjs zzjs = this.zzaqm.zzaqu;
                    zzii zzii2 = this.zzaqm.zzakw;
                    int i15 = this.zzaqm.zzaqw;
                    if (zzjs.zzakx != 0) {
                        byte[] bArr = this.zzapw.data;
                        bArr[0] = 0;
                        bArr[1] = 0;
                        bArr[2] = 0;
                        int i16 = zzjs.zzakx + 1;
                        int i17 = 4 - zzjs.zzakx;
                        while (this.zzajn < this.zzaqn) {
                            if (this.zzajm == 0) {
                                zzia2.readFully(bArr, i17, i16);
                                this.zzapw.setPosition(i5);
                                this.zzajm = this.zzapw.zzhg() - 1;
                                this.zzahx.setPosition(i5);
                                zzii2.zza(this.zzahx, 4);
                                zzii2.zza(this.zzapw, 1);
                                this.zzaqo = this.zzaqq != null && zzpu.zza(zzjs.zzaad.zzzj, bArr[4]);
                                this.zzajn += 5;
                                this.zzaqn += i17;
                            } else {
                                if (this.zzaqo) {
                                    this.zzapx.reset(this.zzajm);
                                    zzia2.readFully(this.zzapx.data, i5, this.zzajm);
                                    zzii2.zza(this.zzapx, this.zzajm);
                                    i3 = this.zzajm;
                                    int zzb = zzpu.zzb(this.zzapx.data, this.zzapx.limit());
                                    this.zzapx.setPosition(MimeTypes.VIDEO_H265.equals(zzjs.zzaad.zzzj) ? 1 : 0);
                                    this.zzapx.zzbk(zzb);
                                    i2 = i16;
                                    zzoc.zza(zzju5.zzal(i15) * 1000, this.zzapx, this.zzaqq);
                                } else {
                                    i2 = i16;
                                    i3 = zzii2.zza(zzia2, this.zzajm, false);
                                }
                                this.zzajn += i3;
                                this.zzajm -= i3;
                                i16 = i2;
                                i5 = 0;
                            }
                        }
                    } else {
                        while (this.zzajn < this.zzaqn) {
                            this.zzajn += zzii2.zza(zzia2, this.zzaqn - this.zzajn, false);
                        }
                    }
                    long zzal = zzju5.zzal(i15) * 1000;
                    if (this.zzapz == null) {
                        boolean z4 = (zzju5.zzate ? (char) 0 : 0) | zzju5.zzatd[i15];
                        if (zzju5.zzate) {
                            if (zzju5.zzatg != null) {
                                zzjt2 = zzju5.zzatg;
                            } else {
                                zzjt2 = zzjs.zzasn[zzju5.zzast.zzapo];
                            }
                            zzjt = zzjt2;
                            zzij = zzjt != this.zzaqm.zzara ? new zzij(1, zzjt.zzass) : this.zzaqm.zzaqz;
                        } else {
                            zzij = null;
                            zzjt = null;
                        }
                        this.zzaqm.zzaqz = zzij;
                        this.zzaqm.zzara = zzjt;
                        zzii2.zza(zzal, z4 ? 1 : 0, this.zzaqn, 0, zzij);
                        while (!this.zzaqd.isEmpty()) {
                            zzjk zzjk = (zzjk) this.zzaqd.removeFirst();
                            this.zzaqk -= zzjk.size;
                            this.zzaqp.zza(zzjk.zzaqs + zzal, 1, zzjk.size, this.zzaqk, null);
                        }
                        zzjl zzjl5 = this.zzaqm;
                        z2 = true;
                        zzjl5.zzaqw++;
                        this.zzaqm.zzaqx++;
                        if (this.zzaqm.zzaqx == zzju5.zzasz[this.zzaqm.zzaqy]) {
                            this.zzaqm.zzaqy++;
                            i = 0;
                            this.zzaqm.zzaqx = 0;
                            this.zzaqm = null;
                        } else {
                            i = 0;
                        }
                        this.zzaqe = 3;
                        if (z2) {
                        }
                    } else {
                        throw new NoSuchMethodError();
                    }
                    break;
            }
        }
    }

    private final void zzei() {
        this.zzaqe = 0;
        this.zzaqh = 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:161:0x03f4  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x03fb  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0405  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x065a  */
    private final void zzt(long j) throws zzfx {
        zzji zzji;
        int i;
        int i2;
        zziw zziw;
        SparseArray<zzjl> sparseArray;
        int i3;
        byte[] bArr;
        zzju zzju;
        int size;
        int i4;
        byte[] bArr2;
        int i5;
        int i6;
        int i7;
        int i8;
        zzjl zzjl;
        zziw zziw2;
        int i9;
        zziw zziw3;
        zzju zzju2;
        byte[] bArr3;
        int i10;
        List<zzix> list;
        int i11;
        long j2;
        int i12;
        int[] iArr;
        long j3;
        long j4;
        int i13;
        int i14;
        int i15;
        int i16;
        boolean z;
        boolean z2;
        boolean z3;
        SparseArray sparseArray2;
        zzji zzji2 = this;
        while (!zzji2.zzaqc.isEmpty() && ((zziw) zzji2.zzaqc.peek()).zzaop == j) {
            zziw zziw4 = (zziw) zzji2.zzaqc.pop();
            int i17 = 12;
            int i18 = 8;
            int i19 = 1;
            if (zziw4.type == zziv.zzamb) {
                zzpo.checkState(true, "Unexpected moov box.");
                zzhp zzb = zzb(zziw4.zzaoq);
                zziw zzaj = zziw4.zzaj(zziv.zzamm);
                SparseArray sparseArray3 = new SparseArray();
                int size2 = zzaj.zzaoq.size();
                long j5 = -9223372036854775807L;
                int i20 = 0;
                while (i20 < size2) {
                    zzix zzix = (zzix) zzaj.zzaoq.get(i20);
                    if (zzix.type == zziv.zzaly) {
                        zzpx zzpx = zzix.zzaos;
                        zzpx.setPosition(i17);
                        Pair create = Pair.create(Integer.valueOf(zzpx.readInt()), new zzjf(zzpx.zzhg() - i19, zzpx.zzhg(), zzpx.zzhg(), zzpx.readInt()));
                        sparseArray3.put(((Integer) create.first).intValue(), (zzjf) create.second);
                    } else if (zzix.type == zziv.zzamn) {
                        zzpx zzpx2 = zzix.zzaos;
                        zzpx2.setPosition(8);
                        j5 = zziv.zzaf(zzpx2.readInt()) == 0 ? zzpx2.zzhd() : zzpx2.zzhh();
                    }
                    i20++;
                    i17 = 12;
                    i19 = 1;
                }
                SparseArray sparseArray4 = new SparseArray();
                int size3 = zziw4.zzaor.size();
                int i21 = 0;
                while (i21 < size3) {
                    zziw zziw5 = (zziw) zziw4.zzaor.get(i21);
                    if (zziw5.type == zziv.zzamd) {
                        sparseArray2 = sparseArray3;
                        zzjs zza = zziy.zza(zziw5, zziw4.zzai(zziv.zzamc), j5, zzb, false);
                        if (zza != null) {
                            sparseArray4.put(zza.id, zza);
                        }
                    } else {
                        sparseArray2 = sparseArray3;
                    }
                    i21++;
                    sparseArray3 = sparseArray2;
                }
                SparseArray sparseArray5 = sparseArray3;
                int size4 = sparseArray4.size();
                if (zzji2.zzapv.size() == 0) {
                    for (int i22 = 0; i22 < size4; i22++) {
                        zzjs zzjs = (zzjs) sparseArray4.valueAt(i22);
                        zzjl zzjl2 = new zzjl(zzji2.zzajq.zzb(i22, zzjs.type));
                        zzjl2.zza(zzjs, (zzjf) sparseArray5.get(zzjs.id));
                        zzji2.zzapv.put(zzjs.id, zzjl2);
                        zzji2.zzaan = Math.max(zzji2.zzaan, zzjs.zzaan);
                    }
                    if ((zzji2.flags & 4) != 0 && zzji2.zzaqp == null) {
                        zzji2.zzaqp = zzji2.zzajq.zzb(zzji2.zzapv.size(), 4);
                        zzji2.zzaqp.zzf(zzfs.zza((String) null, MimeTypes.APPLICATION_EMSG, Long.MAX_VALUE));
                    }
                    if ((zzji2.flags & 8) != 0 && zzji2.zzaqq == null) {
                        zzii zzb2 = zzji2.zzajq.zzb(zzji2.zzapv.size() + 1, 3);
                        zzb2.zzf(zzfs.zza((String) null, MimeTypes.APPLICATION_CEA608, (String) null, -1, 0, (String) null, (zzhp) null));
                        zzji2.zzaqq = new zzii[]{zzb2};
                    }
                    zzji2.zzajq.zzdy();
                } else {
                    zzpo.checkState(zzji2.zzapv.size() == size4);
                    for (int i23 = 0; i23 < size4; i23++) {
                        zzjs zzjs2 = (zzjs) sparseArray4.valueAt(i23);
                        ((zzjl) zzji2.zzapv.get(zzjs2.id)).zza(zzjs2, (zzjf) sparseArray5.get(zzjs2.id));
                    }
                }
            } else {
                zzjl zzjl3 = null;
                if (zziw4.type == zziv.zzamk) {
                    SparseArray<zzjl> sparseArray6 = zzji2.zzapv;
                    int i24 = zzji2.flags;
                    byte[] bArr4 = zzji2.zzaqb;
                    int size5 = zziw4.zzaor.size();
                    int i25 = 0;
                    while (i25 < size5) {
                        zziw zziw6 = (zziw) zziw4.zzaor.get(i25);
                        if (zziw6.type == zziv.zzaml) {
                            zzpx zzpx3 = zziw6.zzai(zziv.zzalx).zzaos;
                            zzpx3.setPosition(i18);
                            int zzag = zziv.zzag(zzpx3.readInt());
                            int readInt = zzpx3.readInt();
                            if ((i24 & 16) != 0) {
                                readInt = 0;
                            }
                            zzjl zzjl4 = (zzjl) sparseArray6.get(readInt);
                            if (zzjl4 == null) {
                                zzjl4 = zzjl3;
                                i3 = size5;
                            } else {
                                if ((zzag & 1) != 0) {
                                    i3 = size5;
                                    long zzhh = zzpx3.zzhh();
                                    zzjl4.zzaqt.zzasv = zzhh;
                                    zzjl4.zzaqt.zzasw = zzhh;
                                } else {
                                    i3 = size5;
                                }
                                zzjf zzjf = zzjl4.zzaqv;
                                zzjl4.zzaqt.zzast = new zzjf((zzag & 2) != 0 ? zzpx3.zzhg() - 1 : zzjf.zzapo, (zzag & 8) != 0 ? zzpx3.zzhg() : zzjf.duration, (zzag & 16) != 0 ? zzpx3.zzhg() : zzjf.size, (zzag & 32) != 0 ? zzpx3.zzhg() : zzjf.flags);
                            }
                            if (zzjl4 != null) {
                                zzju zzju3 = zzjl4.zzaqt;
                                long j6 = zzju3.zzatk;
                                zzjl4.reset();
                                if (zziw6.zzai(zziv.zzalw) != null && (i24 & 2) == 0) {
                                    zzpx zzpx4 = zziw6.zzai(zziv.zzalw).zzaos;
                                    zzpx4.setPosition(8);
                                    j6 = zziv.zzaf(zzpx4.readInt()) == 1 ? zzpx4.zzhh() : zzpx4.zzhd();
                                }
                                List<zzix> list2 = zziw6.zzaoq;
                                int size6 = list2.size();
                                sparseArray = sparseArray6;
                                int i26 = 0;
                                int i27 = 0;
                                int i28 = 0;
                                while (i28 < size6) {
                                    zzix zzix2 = (zzix) list2.get(i28);
                                    long j7 = j6;
                                    if (zzix2.type == zziv.zzalz) {
                                        zzpx zzpx5 = zzix2.zzaos;
                                        zzpx5.setPosition(12);
                                        int zzhg = zzpx5.zzhg();
                                        if (zzhg > 0) {
                                            i27 += zzhg;
                                            i26++;
                                        }
                                    }
                                    i28++;
                                    j6 = j7;
                                }
                                long j8 = j6;
                                zzjl4.zzaqy = 0;
                                zzjl4.zzaqx = 0;
                                zzjl4.zzaqw = 0;
                                zzju zzju4 = zzjl4.zzaqt;
                                zzju4.zzasx = i26;
                                zzju4.zzapk = i27;
                                if (zzju4.zzasz == null || zzju4.zzasz.length < i26) {
                                    zzju4.zzasy = new long[i26];
                                    zzju4.zzasz = new int[i26];
                                }
                                if (zzju4.zzata == null || zzju4.zzata.length < i27) {
                                    int i29 = (i27 * 125) / 100;
                                    zzju4.zzata = new int[i29];
                                    zzju4.zzatb = new int[i29];
                                    zzju4.zzatc = new long[i29];
                                    zzju4.zzatd = new boolean[i29];
                                    zzju4.zzatf = new boolean[i29];
                                }
                                int i30 = 0;
                                int i31 = 0;
                                int i32 = 0;
                                while (i30 < size6) {
                                    zzix zzix3 = (zzix) list2.get(i30);
                                    if (zzix3.type == zziv.zzalz) {
                                        int i33 = i31 + 1;
                                        zzpx zzpx6 = zzix3.zzaos;
                                        zzpx6.setPosition(8);
                                        int zzag2 = zziv.zzag(zzpx6.readInt());
                                        int i34 = i33;
                                        zzjs zzjs3 = zzjl4.zzaqu;
                                        list = list2;
                                        zzju zzju5 = zzjl4.zzaqt;
                                        i10 = size6;
                                        zzjf zzjf2 = zzju5.zzast;
                                        zzju5.zzasz[i31] = zzpx6.zzhg();
                                        bArr3 = bArr4;
                                        zzju2 = zzju3;
                                        zzju5.zzasy[i31] = zzju5.zzasv;
                                        if ((zzag2 & 1) != 0) {
                                            long[] jArr = zzju5.zzasy;
                                            zziw3 = zziw4;
                                            i9 = i25;
                                            zziw2 = zziw6;
                                            jArr[i31] = jArr[i31] + ((long) zzpx6.readInt());
                                        } else {
                                            zziw3 = zziw4;
                                            i9 = i25;
                                            zziw2 = zziw6;
                                        }
                                        boolean z4 = (zzag2 & 4) != 0;
                                        int i35 = zzjf2.flags;
                                        if (z4) {
                                            i35 = zzpx6.zzhg();
                                        }
                                        boolean z5 = (zzag2 & 256) != 0;
                                        boolean z6 = (zzag2 & 512) != 0;
                                        boolean z7 = (zzag2 & 1024) != 0;
                                        boolean z8 = (zzag2 & 2048) != 0;
                                        if (zzjs3.zzaso != null) {
                                            i11 = i35;
                                            if (zzjs3.zzaso.length == 1 && zzjs3.zzaso[0] == 0) {
                                                zzjl = zzjl4;
                                                j2 = zzqe.zza(zzjs3.zzasp[0], 1000, zzjs3.zzcr);
                                                int[] iArr2 = zzju5.zzata;
                                                int[] iArr3 = zzju5.zzatb;
                                                i8 = i30;
                                                long[] jArr2 = zzju5.zzatc;
                                                int[] iArr4 = iArr2;
                                                boolean[] zArr = zzju5.zzatd;
                                                long[] jArr3 = jArr2;
                                                boolean z9 = (zzjs3.type == 2 || (i24 & 1) == 0) ? false : true;
                                                i12 = zzju5.zzasz[i31] + i32;
                                                int i36 = i32;
                                                i7 = i24;
                                                long j9 = zzjs3.zzcr;
                                                if (i31 <= 0) {
                                                    iArr = iArr3;
                                                    j3 = j2;
                                                    j4 = zzju5.zzatk;
                                                } else {
                                                    iArr = iArr3;
                                                    j3 = j2;
                                                    j4 = j8;
                                                }
                                                i13 = i36;
                                                while (i13 < i12) {
                                                    if (z5) {
                                                        i14 = zzpx6.zzhg();
                                                    } else {
                                                        i14 = zzjf2.duration;
                                                    }
                                                    if (z6) {
                                                        i16 = zzpx6.zzhg();
                                                        i15 = i12;
                                                    } else {
                                                        i15 = i12;
                                                        i16 = zzjf2.size;
                                                    }
                                                    int i37 = (i13 != 0 || !z4) ? z7 ? zzpx6.readInt() : zzjf2.flags : i11;
                                                    if (z8) {
                                                        z3 = z4;
                                                        z2 = z5;
                                                        z = z6;
                                                        iArr[i13] = (int) (((long) (zzpx6.readInt() * 1000)) / j9);
                                                    } else {
                                                        z3 = z4;
                                                        z2 = z5;
                                                        z = z6;
                                                        iArr[i13] = 0;
                                                    }
                                                    jArr3[i13] = zzqe.zza(j4, 1000, j9) - j3;
                                                    iArr4[i13] = i16;
                                                    zArr[i13] = ((i37 >> 16) & 1) == 0 && (!z9 || i13 == 0);
                                                    j4 += (long) i14;
                                                    i13++;
                                                    i12 = i15;
                                                    z4 = z3;
                                                    z5 = z2;
                                                    z6 = z;
                                                }
                                                int i38 = i12;
                                                zzju5.zzatk = j4;
                                                i31 = i34;
                                                i32 = i38;
                                            }
                                        } else {
                                            i11 = i35;
                                        }
                                        zzjl = zzjl4;
                                        j2 = 0;
                                        int[] iArr22 = zzju5.zzata;
                                        int[] iArr32 = zzju5.zzatb;
                                        i8 = i30;
                                        long[] jArr22 = zzju5.zzatc;
                                        int[] iArr42 = iArr22;
                                        boolean[] zArr2 = zzju5.zzatd;
                                        long[] jArr32 = jArr22;
                                        if (zzjs3.type == 2) {
                                        }
                                        i12 = zzju5.zzasz[i31] + i32;
                                        int i362 = i32;
                                        i7 = i24;
                                        long j92 = zzjs3.zzcr;
                                        if (i31 <= 0) {
                                        }
                                        i13 = i362;
                                        while (i13 < i12) {
                                        }
                                        int i382 = i12;
                                        zzju5.zzatk = j4;
                                        i31 = i34;
                                        i32 = i382;
                                    } else {
                                        zziw3 = zziw4;
                                        i8 = i30;
                                        int i39 = i32;
                                        i7 = i24;
                                        bArr3 = bArr4;
                                        zzju2 = zzju3;
                                        i9 = i25;
                                        zziw2 = zziw6;
                                        list = list2;
                                        i10 = size6;
                                        zzjl = zzjl4;
                                    }
                                    i30 = i8 + 1;
                                    list2 = list;
                                    size6 = i10;
                                    bArr4 = bArr3;
                                    zzju3 = zzju2;
                                    zziw4 = zziw3;
                                    i25 = i9;
                                    zziw6 = zziw2;
                                    zzjl4 = zzjl;
                                    i24 = i7;
                                }
                                zziw = zziw4;
                                i = i24;
                                byte[] bArr5 = bArr4;
                                zzju zzju6 = zzju3;
                                i2 = i25;
                                zziw zziw7 = zziw6;
                                zzjl zzjl5 = zzjl4;
                                zzix zzai = zziw6.zzai(zziv.zzanc);
                                if (zzai != null) {
                                    zzju = zzju6;
                                    zzjt zzjt = zzjl5.zzaqu.zzasn[zzju.zzast.zzapo];
                                    zzpx zzpx7 = zzai.zzaos;
                                    int i40 = zzjt.zzasr;
                                    zzpx7.setPosition(8);
                                    if ((zziv.zzag(zzpx7.readInt()) & 1) == 1) {
                                        zzpx7.zzbl(8);
                                    }
                                    int readUnsignedByte = zzpx7.readUnsignedByte();
                                    int zzhg2 = zzpx7.zzhg();
                                    if (zzhg2 == zzju.zzapk) {
                                        if (readUnsignedByte == 0) {
                                            boolean[] zArr3 = zzju.zzatf;
                                            i6 = 0;
                                            for (int i41 = 0; i41 < zzhg2; i41++) {
                                                int readUnsignedByte2 = zzpx7.readUnsignedByte();
                                                i6 += readUnsignedByte2;
                                                zArr3[i41] = readUnsignedByte2 > i40;
                                            }
                                        } else {
                                            i6 = (readUnsignedByte * zzhg2) + 0;
                                            Arrays.fill(zzju.zzatf, 0, zzhg2, readUnsignedByte > i40);
                                        }
                                        zzju.zzak(i6);
                                    } else {
                                        int i42 = zzju.zzapk;
                                        StringBuilder sb = new StringBuilder(41);
                                        sb.append("Length mismatch: ");
                                        sb.append(zzhg2);
                                        sb.append(", ");
                                        sb.append(i42);
                                        throw new zzfx(sb.toString());
                                    }
                                } else {
                                    zzju = zzju6;
                                }
                                zzix zzai2 = zziw6.zzai(zziv.zzand);
                                if (zzai2 != null) {
                                    zzpx zzpx8 = zzai2.zzaos;
                                    zzpx8.setPosition(8);
                                    int readInt2 = zzpx8.readInt();
                                    if ((zziv.zzag(readInt2) & 1) == 1) {
                                        zzpx8.zzbl(8);
                                    }
                                    int zzhg3 = zzpx8.zzhg();
                                    if (zzhg3 == 1) {
                                        zzju.zzasw += zziv.zzaf(readInt2) == 0 ? zzpx8.zzhd() : zzpx8.zzhh();
                                    } else {
                                        StringBuilder sb2 = new StringBuilder(40);
                                        sb2.append("Unexpected saio entry count: ");
                                        sb2.append(zzhg3);
                                        throw new zzfx(sb2.toString());
                                    }
                                }
                                zzix zzai3 = zziw6.zzai(zziv.zzanh);
                                if (zzai3 != null) {
                                    zza(zzai3.zzaos, 0, zzju);
                                }
                                zzix zzai4 = zziw6.zzai(zziv.zzane);
                                zzix zzai5 = zziw6.zzai(zziv.zzanf);
                                if (!(zzai4 == null || zzai5 == null)) {
                                    zzpx zzpx9 = zzai4.zzaos;
                                    zzpx zzpx10 = zzai5.zzaos;
                                    zzpx9.setPosition(8);
                                    int readInt3 = zzpx9.readInt();
                                    if (zzpx9.readInt() == zzaps) {
                                        if (zziv.zzaf(readInt3) == 1) {
                                            zzpx9.zzbl(4);
                                        }
                                        if (zzpx9.readInt() == 1) {
                                            zzpx10.setPosition(8);
                                            int readInt4 = zzpx10.readInt();
                                            if (zzpx10.readInt() == zzaps) {
                                                int zzaf = zziv.zzaf(readInt4);
                                                if (zzaf != 1) {
                                                    i5 = 2;
                                                    if (zzaf >= 2) {
                                                        zzpx10.zzbl(4);
                                                    }
                                                } else if (zzpx10.zzhd() != 0) {
                                                    i5 = 2;
                                                } else {
                                                    throw new zzfx("Variable length decription in sgpd found (unsupported)");
                                                }
                                                if (zzpx10.zzhd() == 1) {
                                                    zzpx10.zzbl(i5);
                                                    if (zzpx10.readUnsignedByte() == 1) {
                                                        int readUnsignedByte3 = zzpx10.readUnsignedByte();
                                                        byte[] bArr6 = new byte[16];
                                                        zzpx10.zze(bArr6, 0, 16);
                                                        zzju.zzate = true;
                                                        zzju.zzatg = new zzjt(true, readUnsignedByte3, bArr6);
                                                    }
                                                } else {
                                                    throw new zzfx("Entry count in sgpd != 1 (unsupported).");
                                                }
                                            }
                                            size = zziw6.zzaoq.size();
                                            i4 = 0;
                                            while (i4 < size) {
                                                zzix zzix4 = (zzix) zziw6.zzaoq.get(i4);
                                                if (zzix4.type == zziv.zzang) {
                                                    zzpx zzpx11 = zzix4.zzaos;
                                                    zzpx11.setPosition(8);
                                                    bArr2 = bArr5;
                                                    zzpx11.zze(bArr2, 0, 16);
                                                    if (Arrays.equals(bArr2, zzapt)) {
                                                        zza(zzpx11, 16, zzju);
                                                    }
                                                } else {
                                                    bArr2 = bArr5;
                                                }
                                                i4++;
                                                bArr5 = bArr2;
                                            }
                                            bArr = bArr5;
                                            i25 = i2 + 1;
                                            bArr4 = bArr;
                                            size5 = i3;
                                            sparseArray6 = sparseArray;
                                            zziw4 = zziw;
                                            i24 = i;
                                            zzjl3 = null;
                                            i18 = 8;
                                        } else {
                                            throw new zzfx("Entry count in sbgp != 1 (unsupported).");
                                        }
                                    }
                                }
                                size = zziw6.zzaoq.size();
                                i4 = 0;
                                while (i4 < size) {
                                }
                                bArr = bArr5;
                                i25 = i2 + 1;
                                bArr4 = bArr;
                                size5 = i3;
                                sparseArray6 = sparseArray;
                                zziw4 = zziw;
                                i24 = i;
                                zzjl3 = null;
                                i18 = 8;
                            } else {
                                zziw = zziw4;
                                sparseArray = sparseArray6;
                                i = i24;
                                bArr = bArr4;
                            }
                        } else {
                            zziw = zziw4;
                            sparseArray = sparseArray6;
                            i = i24;
                            bArr = bArr4;
                            i3 = size5;
                        }
                        i2 = i25;
                        i25 = i2 + 1;
                        bArr4 = bArr;
                        size5 = i3;
                        sparseArray6 = sparseArray;
                        zziw4 = zziw;
                        i24 = i;
                        zzjl3 = null;
                        i18 = 8;
                    }
                    zzhp zzb3 = zzb(zziw4.zzaoq);
                    if (zzb3 != null) {
                        zzji = this;
                        int size7 = zzji.zzapv.size();
                        for (int i43 = 0; i43 < size7; i43++) {
                            zzjl zzjl6 = (zzjl) zzji.zzapv.valueAt(i43);
                            zzjl6.zzakw.zzf(zzjl6.zzaqu.zzaad.zza(zzb3));
                        }
                    } else {
                        zzji = this;
                    }
                } else {
                    zzji = zzji2;
                    if (!zzji.zzaqc.isEmpty()) {
                        ((zziw) zzji.zzaqc.peek()).zza(zziw4);
                    }
                }
                zzji2 = zzji;
            }
        }
        zzji zzji3 = zzji2;
        zzei();
    }

    private static void zza(zzpx zzpx, int i, zzju zzju) throws zzfx {
        zzpx.setPosition(i + 8);
        int zzag = zziv.zzag(zzpx.readInt());
        if ((zzag & 1) == 0) {
            boolean z = (zzag & 2) != 0;
            int zzhg = zzpx.zzhg();
            if (zzhg == zzju.zzapk) {
                Arrays.fill(zzju.zzatf, 0, zzhg, z);
                zzju.zzak(zzpx.zzhb());
                zzpx.zze(zzju.zzati.data, 0, zzju.zzath);
                zzju.zzati.setPosition(0);
                zzju.zzatj = false;
                return;
            }
            int i2 = zzju.zzapk;
            StringBuilder sb = new StringBuilder(41);
            sb.append("Length mismatch: ");
            sb.append(zzhg);
            sb.append(", ");
            sb.append(i2);
            throw new zzfx(sb.toString());
        }
        throw new zzfx("Overriding TrackEncryptionBox parameters is unsupported.");
    }

    private static zzhp zzb(List<zzix> list) {
        int size = list.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            zzix zzix = (zzix) list.get(i);
            if (zzix.type == zziv.zzamu) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                byte[] bArr = zzix.zzaos.data;
                UUID zze = zzjq.zze(bArr);
                if (zze == null) {
                    Log.w("FragmentedMp4Extractor", "Skipped pssh atom (failed to extract uuid)");
                } else {
                    arrayList.add(new zza(zze, MimeTypes.VIDEO_MP4, bArr));
                }
            }
        }
        if (arrayList == null) {
            return null;
        }
        return new zzhp((List<zza>) arrayList);
    }
}
