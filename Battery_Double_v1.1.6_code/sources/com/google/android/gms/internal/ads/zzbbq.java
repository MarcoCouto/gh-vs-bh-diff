package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzbv;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@zzark
public final class zzbbq {
    public static <V> void zza(zzbcb<V> zzbcb, zzbbn<V> zzbbn, Executor executor) {
        zzbcb.zza(new zzbbr(zzbbn, zzbcb), executor);
    }

    public static <A, B> zzbcb<B> zza(zzbcb<A> zzbcb, zzbbm<A, B> zzbbm, Executor executor) {
        zzbcl zzbcl = new zzbcl();
        zzbcb.zza(new zzbbs(zzbcl, zzbbm, zzbcb), executor);
        zza((zzbcb<A>) zzbcl, (Future<B>) zzbcb);
        return zzbcl;
    }

    public static <A, B> zzbcb<B> zza(zzbcb<A> zzbcb, zzbbl<? super A, ? extends B> zzbbl, Executor executor) {
        zzbcl zzbcl = new zzbcl();
        zzbcb.zza(new zzbbt(zzbcl, zzbbl, zzbcb), executor);
        zza((zzbcb<A>) zzbcl, (Future<B>) zzbcb);
        return zzbcl;
    }

    public static <V> zzbcb<V> zza(zzbcb<V> zzbcb, long j, TimeUnit timeUnit, ScheduledExecutorService scheduledExecutorService) {
        zzbcl zzbcl = new zzbcl();
        zza((zzbcb<A>) zzbcl, (Future<B>) zzbcb);
        ScheduledFuture schedule = scheduledExecutorService.schedule(new zzbbu(zzbcl), j, timeUnit);
        zza(zzbcb, zzbcl);
        zzbcl.zza(new zzbbv(schedule), zzbcg.zzepp);
        return zzbcl;
    }

    public static <V, X extends Throwable> zzbcb<V> zza(zzbcb<? extends V> zzbcb, Class<X> cls, zzbbl<? super X, ? extends V> zzbbl, Executor executor) {
        zzbcl zzbcl = new zzbcl();
        zza((zzbcb<A>) zzbcl, (Future<B>) zzbcb);
        zzbbw zzbbw = new zzbbw(zzbcl, zzbcb, cls, zzbbl, executor);
        zzbcb.zza(zzbbw, zzbcg.zzepp);
        return zzbcl;
    }

    public static <T> T zza(Future<T> future, T t) {
        try {
            return future.get(((Long) zzwu.zzpz().zzd(zzaan.zzctd)).longValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            future.cancel(true);
            zzaxz.zzc("InterruptedException caught while resolving future.", e);
            Thread.currentThread().interrupt();
            zzbv.zzlj().zzb(e, "Futures.resolveFuture");
            return t;
        } catch (Exception e2) {
            future.cancel(true);
            zzaxz.zzb("Error waiting for future.", e2);
            zzbv.zzlj().zzb(e2, "Futures.resolveFuture");
            return t;
        }
    }

    public static <T> T zza(Future<T> future, T t, long j, TimeUnit timeUnit) {
        try {
            return future.get(j, timeUnit);
        } catch (InterruptedException e) {
            future.cancel(true);
            zzaxz.zzc("InterruptedException caught while resolving future.", e);
            Thread.currentThread().interrupt();
            zzbv.zzlj().zzb(e, "Futures.resolveFuture");
            return t;
        } catch (Exception e2) {
            future.cancel(true);
            zzaxz.zzb("Error waiting for future.", e2);
            zzbv.zzlj().zzb(e2, "Futures.resolveFuture");
            return t;
        }
    }

    public static <T> zzbca<T> zzm(T t) {
        return new zzbca<>(t);
    }

    public static <T> zzbbz<T> zzd(Throwable th) {
        return new zzbbz<>(th);
    }

    private static <V> void zza(zzbcb<? extends V> zzbcb, zzbcl<V> zzbcl) {
        zza((zzbcb<A>) zzbcl, (Future<B>) zzbcb);
        zzbcb.zza(new zzbbx(zzbcl, zzbcb), zzbcg.zzepp);
    }

    private static <A, B> void zza(zzbcb<A> zzbcb, Future<B> future) {
        zzbcb.zza(new zzbby(zzbcb, future), zzbcg.zzepp);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002a  */
    static final /* synthetic */ void zza(zzbcl zzbcl, zzbcb zzbcb, Class cls, zzbbl zzbbl, Executor executor) {
        Throwable e;
        try {
            zzbcl.set(zzbcb.get());
        } catch (ExecutionException e2) {
            e = e2.getCause();
            if (cls.isInstance(e)) {
                zza(zza((zzbcb<A>) zzm(e), zzbbl, executor), zzbcl);
            } else {
                zzbcl.setException(e);
            }
        } catch (InterruptedException e3) {
            e = e3;
            Thread.currentThread().interrupt();
            if (cls.isInstance(e)) {
            }
        } catch (Exception e4) {
            e = e4;
            if (cls.isInstance(e)) {
            }
        }
    }

    static final /* synthetic */ void zza(zzbcl zzbcl, zzbbl zzbbl, zzbcb zzbcb) {
        if (!zzbcl.isCancelled()) {
            try {
                zza(zzbbl.zzf(zzbcb.get()), zzbcl);
            } catch (CancellationException unused) {
                zzbcl.cancel(true);
            } catch (ExecutionException e) {
                zzbcl.setException(e.getCause());
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
                zzbcl.setException(e2);
            } catch (Exception e3) {
                zzbcl.setException(e3);
            }
        }
    }
}
