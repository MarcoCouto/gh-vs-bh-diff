package com.google.android.gms.internal.ads;

final class zzbqp {
    private static final Class<?> zzfmp = zzalz();

    private static Class<?> zzalz() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    public static zzbqq zzama() {
        if (zzfmp != null) {
            try {
                return zzga("getEmptyRegistry");
            } catch (Exception unused) {
            }
        }
        return zzbqq.zzfmt;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x000e  */
    static zzbqq zzamb() {
        zzbqq zzbqq;
        if (zzfmp != null) {
            try {
                zzbqq = zzga("loadGeneratedRegistry");
            } catch (Exception unused) {
            }
            if (zzbqq == null) {
                zzbqq = zzbqq.zzamb();
            }
            return zzbqq != null ? zzama() : zzbqq;
        }
        zzbqq = null;
        if (zzbqq == null) {
        }
        if (zzbqq != null) {
        }
    }

    private static final zzbqq zzga(String str) throws Exception {
        return (zzbqq) zzfmp.getDeclaredMethod(str, new Class[0]).invoke(null, new Object[0]);
    }
}
