package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class zzbsf<K, V> extends LinkedHashMap<K, V> {
    private static final zzbsf zzfsd;
    private boolean zzfla = true;

    private zzbsf() {
    }

    private zzbsf(Map<K, V> map) {
        super(map);
    }

    public static <K, V> zzbsf<K, V> zzant() {
        return zzfsd;
    }

    public final void zza(zzbsf<K, V> zzbsf) {
        zzanv();
        if (!zzbsf.isEmpty()) {
            putAll(zzbsf);
        }
    }

    public final Set<Entry<K, V>> entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    public final void clear() {
        zzanv();
        super.clear();
    }

    public final V put(K k, V v) {
        zzanv();
        zzbrf.checkNotNull(k);
        zzbrf.checkNotNull(v);
        return super.put(k, v);
    }

    public final void putAll(Map<? extends K, ? extends V> map) {
        zzanv();
        for (Object next : map.keySet()) {
            zzbrf.checkNotNull(next);
            zzbrf.checkNotNull(map.get(next));
        }
        super.putAll(map);
    }

    public final V remove(Object obj) {
        zzanv();
        return super.remove(obj);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[RETURN] */
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (this != map) {
                if (size() == map.size()) {
                    Iterator it = entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Entry entry = (Entry) it.next();
                        if (map.containsKey(entry.getKey())) {
                            Object value = entry.getValue();
                            Object obj2 = map.get(entry.getKey());
                            if (!(value instanceof byte[]) || !(obj2 instanceof byte[])) {
                                z2 = value.equals(obj2);
                                continue;
                            } else {
                                z2 = Arrays.equals((byte[]) value, (byte[]) obj2);
                                continue;
                            }
                            if (!z2) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                z = false;
                if (!z) {
                    return true;
                }
            }
            z = true;
            if (!z) {
                return false;
            }
        }
        return false;
    }

    private static int zzv(Object obj) {
        if (obj instanceof byte[]) {
            return zzbrf.hashCode((byte[]) obj);
        }
        if (!(obj instanceof zzbrg)) {
            return obj.hashCode();
        }
        throw new UnsupportedOperationException();
    }

    public final int hashCode() {
        int i = 0;
        for (Entry entry : entrySet()) {
            i += zzv(entry.getValue()) ^ zzv(entry.getKey());
        }
        return i;
    }

    public final zzbsf<K, V> zzanu() {
        return isEmpty() ? new zzbsf<>() : new zzbsf<>(this);
    }

    public final void zzakj() {
        this.zzfla = false;
    }

    public final boolean isMutable() {
        return this.zzfla;
    }

    private final void zzanv() {
        if (!this.zzfla) {
            throw new UnsupportedOperationException();
        }
    }

    static {
        zzbsf zzbsf = new zzbsf();
        zzfsd = zzbsf;
        zzbsf.zzfla = false;
    }
}
