package com.google.android.gms.internal.ads;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import javax.annotation.ParametersAreNonnullByDefault;

@zzark
@ParametersAreNonnullByDefault
public final class zztm extends zztd {
    private MessageDigest zzbze;
    private final int zzbzh;
    private final int zzbzi;

    public zztm(int i) {
        int i2 = i / 8;
        if (i % 8 > 0) {
            i2++;
        }
        this.zzbzh = i2;
        this.zzbzi = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
        return r1;
     */
    public final byte[] zzay(String str) {
        synchronized (this.mLock) {
            this.zzbze = zzoc();
            if (this.zzbze == null) {
                byte[] bArr = new byte[0];
                return bArr;
            }
            this.zzbze.reset();
            this.zzbze.update(str.getBytes(Charset.forName("UTF-8")));
            byte[] digest = this.zzbze.digest();
            byte[] bArr2 = new byte[(digest.length > this.zzbzh ? this.zzbzh : digest.length)];
            System.arraycopy(digest, 0, bArr2, 0, bArr2.length);
            if (this.zzbzi % 8 > 0) {
                long j = 0;
                for (int i = 0; i < bArr2.length; i++) {
                    if (i > 0) {
                        j <<= 8;
                    }
                    j += (long) (bArr2[i] & 255);
                }
                long j2 = j >>> (8 - (this.zzbzi % 8));
                for (int i2 = this.zzbzh - 1; i2 >= 0; i2--) {
                    bArr2[i2] = (byte) ((int) (255 & j2));
                    j2 >>>= 8;
                }
            }
        }
    }
}
