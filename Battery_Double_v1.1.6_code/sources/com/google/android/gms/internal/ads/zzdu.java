package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import java.lang.ref.WeakReference;

public final class zzdu implements ActivityLifecycleCallbacks, OnAttachStateChangeListener, OnGlobalLayoutListener, OnScrollChangedListener {
    private static final Handler zztu = new Handler(Looper.getMainLooper());
    private final zzdl zzqo;
    private Application zzsg;
    private final Context zztv;
    private final PowerManager zztw;
    private final KeyguardManager zztx;
    private BroadcastReceiver zzty;
    private WeakReference<ViewTreeObserver> zztz;
    private WeakReference<View> zzua;
    private zzcz zzub;
    private boolean zzuc = false;
    private int zzud = -1;
    private long zzue = -3;

    public zzdu(zzdl zzdl, View view) {
        this.zzqo = zzdl;
        this.zztv = zzdl.zzsp;
        this.zztw = (PowerManager) this.zztv.getSystemService("power");
        this.zztx = (KeyguardManager) this.zztv.getSystemService("keyguard");
        if (this.zztv instanceof Application) {
            this.zzsg = (Application) this.zztv;
            this.zzub = new zzcz((Application) this.zztv, this);
        }
        zzd(view);
    }

    /* access modifiers changed from: 0000 */
    public final void zzd(View view) {
        View view2 = this.zzua != null ? (View) this.zzua.get() : null;
        if (view2 != null) {
            view2.removeOnAttachStateChangeListener(this);
            zzf(view2);
        }
        this.zzua = new WeakReference<>(view);
        if (view != null) {
            if ((view.getWindowToken() == null && view.getWindowVisibility() == 8) ? false : true) {
                zze(view);
            }
            view.addOnAttachStateChangeListener(this);
            this.zzue = -2;
            return;
        }
        this.zzue = -3;
    }

    private final void zzap() {
        zztu.post(new zzdv(this));
    }

    public final void onViewAttachedToWindow(View view) {
        this.zzud = -1;
        zze(view);
        zzar();
    }

    public final void onViewDetachedFromWindow(View view) {
        this.zzud = -1;
        zzar();
        zzap();
        zzf(view);
    }

    private final void zza(Activity activity, int i) {
        if (this.zzua != null) {
            Window window = activity.getWindow();
            if (window != null) {
                View peekDecorView = window.peekDecorView();
                View view = (View) this.zzua.get();
                if (!(view == null || peekDecorView == null || view.getRootView() != peekDecorView.getRootView())) {
                    this.zzud = i;
                }
            }
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        zza(activity, 0);
        zzar();
    }

    public final void onActivityStarted(Activity activity) {
        zza(activity, 0);
        zzar();
    }

    public final void onActivityResumed(Activity activity) {
        zza(activity, 0);
        zzar();
        zzap();
    }

    public final void onActivityPaused(Activity activity) {
        zza(activity, 4);
        zzar();
    }

    public final void onActivityStopped(Activity activity) {
        zzar();
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        zzar();
    }

    public final void onActivityDestroyed(Activity activity) {
        zzar();
    }

    public final void onGlobalLayout() {
        zzar();
    }

    public final void onScrollChanged() {
        zzar();
    }

    public final long zzaq() {
        if (this.zzue == -2 && this.zzua.get() == null) {
            this.zzue = -3;
        }
        return this.zzue;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0058, code lost:
        if (r4 == false) goto L_0x005b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008a  */
    public final void zzar() {
        boolean z;
        boolean z2;
        LayoutParams layoutParams;
        if (this.zzua != null) {
            View view = (View) this.zzua.get();
            boolean z3 = false;
            if (view == null) {
                this.zzue = -3;
                this.zzuc = false;
                return;
            }
            boolean globalVisibleRect = view.getGlobalVisibleRect(new Rect());
            boolean localVisibleRect = view.getLocalVisibleRect(new Rect());
            if (!this.zzqo.zzaj()) {
                if (this.zztx.inKeyguardRestrictedInputMode()) {
                    Activity zzc = zzds.zzc(view);
                    if (zzc != null) {
                        Window window = zzc.getWindow();
                        if (window == null) {
                            layoutParams = null;
                        } else {
                            layoutParams = window.getAttributes();
                        }
                        if (!(layoutParams == null || (layoutParams.flags & 524288) == 0)) {
                            z2 = true;
                        }
                    }
                    z2 = false;
                }
                z = false;
                int windowVisibility = view.getWindowVisibility();
                if (this.zzud != -1) {
                    windowVisibility = this.zzud;
                }
                if (view.getVisibility() == 0 && view.isShown() && this.zztw.isScreenOn() && z && localVisibleRect && globalVisibleRect && windowVisibility == 0) {
                    z3 = true;
                }
                if (this.zzuc != z3) {
                    this.zzue = z3 ? SystemClock.elapsedRealtime() : -2;
                    this.zzuc = z3;
                }
            }
            z = true;
            int windowVisibility2 = view.getWindowVisibility();
            if (this.zzud != -1) {
            }
            z3 = true;
            if (this.zzuc != z3) {
            }
        }
    }

    private final void zze(View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            this.zztz = new WeakReference<>(viewTreeObserver);
            viewTreeObserver.addOnScrollChangedListener(this);
            viewTreeObserver.addOnGlobalLayoutListener(this);
        }
        if (this.zzty == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            this.zzty = new zzdw(this);
            this.zztv.registerReceiver(this.zzty, intentFilter);
        }
        if (this.zzsg != null) {
            try {
                this.zzsg.registerActivityLifecycleCallbacks(this.zzub);
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x001d */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0027 A[Catch:{ Exception -> 0x002d }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0031 A[SYNTHETIC, Splitter:B:17:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003e A[SYNTHETIC, Splitter:B:23:0x003e] */
    private final void zzf(View view) {
        if (this.zztz != null) {
            ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.zztz.get();
            if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnScrollChangedListener(this);
                viewTreeObserver.removeGlobalOnLayoutListener(this);
            }
            this.zztz = null;
        }
        try {
            ViewTreeObserver viewTreeObserver2 = view.getViewTreeObserver();
            if (viewTreeObserver2.isAlive()) {
                viewTreeObserver2.removeOnScrollChangedListener(this);
                viewTreeObserver2.removeGlobalOnLayoutListener(this);
            }
        } catch (Exception unused) {
        }
        if (this.zzty != null) {
            try {
                this.zztv.unregisterReceiver(this.zzty);
            } catch (Exception unused2) {
            }
            this.zzty = null;
        }
        if (this.zzsg != null) {
            try {
                this.zzsg.unregisterActivityLifecycleCallbacks(this.zzub);
            } catch (Exception unused3) {
            }
        }
    }
}
