package com.google.android.gms.internal.ads;

import com.github.mikephil.charting.utils.Utils;

public enum zzbuo {
    INT(Integer.valueOf(0)),
    LONG(Long.valueOf(0)),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(Utils.DOUBLE_EPSILON)),
    BOOLEAN(Boolean.valueOf(false)),
    STRING(""),
    BYTE_STRING(zzbpu.zzfli),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzfrh;

    private zzbuo(Object obj) {
        this.zzfrh = obj;
    }
}
