package com.google.android.gms.internal.ads;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.C;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

public final class zzjn implements zzhz, zzig {
    private static final zzic zzahq = new zzjo();
    private static final int zzasf = zzqe.zzam("qt  ");
    private long zzaan;
    private final zzpx zzahx = new zzpx(zzpu.zzbhi);
    private final zzpx zzahy = new zzpx(4);
    private int zzajm;
    private int zzajn;
    private zzib zzajq;
    private final zzpx zzaqa = new zzpx(16);
    private final Stack<zziw> zzaqc = new Stack<>();
    private int zzaqe;
    private int zzaqf;
    private long zzaqg;
    private int zzaqh;
    private zzpx zzaqi;
    private zzjp[] zzasg;
    private boolean zzash;

    public final void release() {
    }

    public final boolean zzdw() {
        return true;
    }

    public final boolean zza(zzia zzia) throws IOException, InterruptedException {
        return zzjr.zze(zzia);
    }

    public final void zza(zzib zzib) {
        this.zzajq = zzib;
    }

    public final void zzc(long j, long j2) {
        zzjp[] zzjpArr;
        this.zzaqc.clear();
        this.zzaqh = 0;
        this.zzajn = 0;
        this.zzajm = 0;
        if (j == 0) {
            zzei();
            return;
        }
        if (this.zzasg != null) {
            for (zzjp zzjp : this.zzasg) {
                zzjv zzjv = zzjp.zzasi;
                int zzu = zzjv.zzu(j2);
                if (zzu == -1) {
                    zzu = zzjv.zzv(j2);
                }
                zzjp.zzapm = zzu;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:149:0x019e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x02b3 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0006 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0006 A[SYNTHETIC] */
    public final int zza(zzia zzia, zzif zzif) throws IOException, InterruptedException {
        boolean z;
        boolean z2;
        boolean z3;
        zzia zzia2 = zzia;
        zzif zzif2 = zzif;
        while (true) {
            switch (this.zzaqe) {
                case 0:
                    if (this.zzaqh == 0) {
                        if (!zzia2.zza(this.zzaqa.data, 0, 8, true)) {
                            z = false;
                            if (!z) {
                                break;
                            } else {
                                return -1;
                            }
                        } else {
                            this.zzaqh = 8;
                            this.zzaqa.setPosition(0);
                            this.zzaqg = this.zzaqa.zzhd();
                            this.zzaqf = this.zzaqa.readInt();
                        }
                    }
                    if (this.zzaqg == 1) {
                        zzia2.readFully(this.zzaqa.data, 8, 8);
                        this.zzaqh += 8;
                        this.zzaqg = this.zzaqa.zzhh();
                    }
                    int i = this.zzaqf;
                    if (i == zziv.zzamb || i == zziv.zzamd || i == zziv.zzame || i == zziv.zzamf || i == zziv.zzamg || i == zziv.zzamp) {
                        long position = (zzia.getPosition() + this.zzaqg) - ((long) this.zzaqh);
                        this.zzaqc.add(new zziw(this.zzaqf, position));
                        if (this.zzaqg == ((long) this.zzaqh)) {
                            zzt(position);
                        } else {
                            zzei();
                        }
                        z = true;
                    } else {
                        int i2 = this.zzaqf;
                        if (i2 == zziv.zzamr || i2 == zziv.zzamc || i2 == zziv.zzams || i2 == zziv.zzamt || i2 == zziv.zzanm || i2 == zziv.zzann || i2 == zziv.zzano || i2 == zziv.zzamq || i2 == zziv.zzanp || i2 == zziv.zzanq || i2 == zziv.zzanr || i2 == zziv.zzans || i2 == zziv.zzant || i2 == zziv.zzamo || i2 == zziv.zzala || i2 == zziv.zzaoa) {
                            zzpo.checkState(this.zzaqh == 8);
                            zzpo.checkState(this.zzaqg <= 2147483647L);
                            this.zzaqi = new zzpx((int) this.zzaqg);
                            System.arraycopy(this.zzaqa.data, 0, this.zzaqi.data, 0, 8);
                            z = true;
                            this.zzaqe = 1;
                        } else {
                            z = true;
                            this.zzaqi = null;
                            this.zzaqe = 1;
                        }
                    }
                    if (!z) {
                    }
                    break;
                case 1:
                    long j = this.zzaqg - ((long) this.zzaqh);
                    long position2 = zzia.getPosition() + j;
                    if (this.zzaqi == null) {
                        if (j >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE) {
                            zzif2.zzaha = zzia.getPosition() + j;
                            z2 = true;
                            zzt(position2);
                            if (!z2 && this.zzaqe != 2) {
                                break;
                            } else {
                                return 1;
                            }
                        } else {
                            zzia2.zzw((int) j);
                        }
                    } else {
                        zzia2.readFully(this.zzaqi.data, this.zzaqh, (int) j);
                        if (this.zzaqf == zziv.zzala) {
                            zzpx zzpx = this.zzaqi;
                            zzpx.setPosition(8);
                            if (zzpx.readInt() == zzasf) {
                                z3 = true;
                            } else {
                                zzpx.zzbl(4);
                                while (true) {
                                    if (zzpx.zzhb() <= 0) {
                                        z3 = false;
                                    } else if (zzpx.readInt() == zzasf) {
                                    }
                                }
                            }
                            this.zzash = z3;
                        } else if (!this.zzaqc.isEmpty()) {
                            ((zziw) this.zzaqc.peek()).zza(new zzix(this.zzaqf, this.zzaqi));
                        }
                    }
                    z2 = false;
                    zzt(position2);
                    if (!z2 && this.zzaqe != 2) {
                    }
                case 2:
                    long j2 = Long.MAX_VALUE;
                    int i3 = -1;
                    for (int i4 = 0; i4 < this.zzasg.length; i4++) {
                        zzjp zzjp = this.zzasg[i4];
                        int i5 = zzjp.zzapm;
                        if (i5 != zzjp.zzasi.zzapk) {
                            long j3 = zzjp.zzasi.zzagu[i5];
                            if (j3 < j2) {
                                i3 = i4;
                                j2 = j3;
                            }
                        }
                    }
                    if (i3 == -1) {
                        return -1;
                    }
                    zzjp zzjp2 = this.zzasg[i3];
                    zzii zzii = zzjp2.zzasj;
                    int i6 = zzjp2.zzapm;
                    long j4 = zzjp2.zzasi.zzagu[i6];
                    int i7 = zzjp2.zzasi.zzagt[i6];
                    if (zzjp2.zzaqu.zzasm == 1) {
                        j4 += 8;
                        i7 -= 8;
                    }
                    long position3 = (j4 - zzia.getPosition()) + ((long) this.zzajn);
                    if (position3 < 0 || position3 >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE) {
                        zzif2.zzaha = j4;
                        return 1;
                    }
                    zzia2.zzw((int) position3);
                    if (zzjp2.zzaqu.zzakx != 0) {
                        byte[] bArr = this.zzahy.data;
                        bArr[0] = 0;
                        bArr[1] = 0;
                        bArr[2] = 0;
                        int i8 = zzjp2.zzaqu.zzakx;
                        int i9 = 4 - zzjp2.zzaqu.zzakx;
                        while (this.zzajn < i7) {
                            if (this.zzajm == 0) {
                                zzia2.readFully(this.zzahy.data, i9, i8);
                                this.zzahy.setPosition(0);
                                this.zzajm = this.zzahy.zzhg();
                                this.zzahx.setPosition(0);
                                zzii.zza(this.zzahx, 4);
                                this.zzajn += 4;
                                i7 += i9;
                            } else {
                                int zza = zzii.zza(zzia2, this.zzajm, false);
                                this.zzajn += zza;
                                this.zzajm -= zza;
                            }
                        }
                    } else {
                        while (this.zzajn < i7) {
                            int zza2 = zzii.zza(zzia2, i7 - this.zzajn, false);
                            this.zzajn += zza2;
                            this.zzajm -= zza2;
                        }
                    }
                    zzii.zza(zzjp2.zzasi.zzatl[i6], zzjp2.zzasi.zzapr[i6], i7, 0, null);
                    zzjp2.zzapm++;
                    this.zzajn = 0;
                    this.zzajm = 0;
                    return 0;
                default:
                    throw new IllegalStateException();
            }
        }
    }

    public final long getDurationUs() {
        return this.zzaan;
    }

    public final long zzr(long j) {
        long j2 = Long.MAX_VALUE;
        for (zzjp zzjp : this.zzasg) {
            zzjv zzjv = zzjp.zzasi;
            int zzu = zzjv.zzu(j);
            if (zzu == -1) {
                zzu = zzjv.zzv(j);
            }
            long j3 = zzjv.zzagu[zzu];
            if (j3 < j2) {
                j2 = j3;
            }
        }
        return j2;
    }

    private final void zzei() {
        this.zzaqe = 0;
        this.zzaqh = 0;
    }

    private final void zzt(long j) throws zzfx {
        while (!this.zzaqc.isEmpty() && ((zziw) this.zzaqc.peek()).zzaop == j) {
            zziw zziw = (zziw) this.zzaqc.pop();
            if (zziw.type == zziv.zzamb) {
                long j2 = C.TIME_UNSET;
                ArrayList arrayList = new ArrayList();
                zzki zzki = null;
                zzid zzid = new zzid();
                zzix zzai = zziw.zzai(zziv.zzaoa);
                if (zzai != null) {
                    zzki = zziy.zza(zzai, this.zzash);
                    if (zzki != null) {
                        zzid.zzb(zzki);
                    }
                }
                for (int i = 0; i < zziw.zzaor.size(); i++) {
                    zziw zziw2 = (zziw) zziw.zzaor.get(i);
                    if (zziw2.type == zziv.zzamd) {
                        zzjs zza = zziy.zza(zziw2, zziw.zzai(zziv.zzamc), (long) C.TIME_UNSET, (zzhp) null, this.zzash);
                        if (zza != null) {
                            zzjv zza2 = zziy.zza(zza, zziw2.zzaj(zziv.zzame).zzaj(zziv.zzamf).zzaj(zziv.zzamg), zzid);
                            if (zza2.zzapk != 0) {
                                zzjp zzjp = new zzjp(zza, zza2, this.zzajq.zzb(i, zza.type));
                                zzfs zzj = zza.zzaad.zzj(zza2.zzapp + 30);
                                if (zza.type == 1) {
                                    if (zzid.zzea()) {
                                        zzj = zzj.zza(zzid.zzzw, zzid.zzzx);
                                    }
                                    if (zzki != null) {
                                        zzj = zzj.zza(zzki);
                                    }
                                }
                                zzjp.zzasj.zzf(zzj);
                                j2 = Math.max(j2, zza.zzaan);
                                arrayList.add(zzjp);
                            }
                        }
                    }
                }
                this.zzaan = j2;
                this.zzasg = (zzjp[]) arrayList.toArray(new zzjp[arrayList.size()]);
                this.zzajq.zzdy();
                this.zzajq.zza(this);
                this.zzaqc.clear();
                this.zzaqe = 2;
            } else if (!this.zzaqc.isEmpty()) {
                ((zziw) this.zzaqc.peek()).zza(zziw);
            }
        }
        if (this.zzaqe != 2) {
            zzei();
        }
    }
}
