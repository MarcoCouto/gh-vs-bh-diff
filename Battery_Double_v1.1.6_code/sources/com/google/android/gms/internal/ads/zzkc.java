package com.google.android.gms.internal.ads;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.media.MediaCodecInfo;
import android.media.MediaCodecInfo.CodecCapabilities;
import android.media.MediaCodecInfo.CodecProfileLevel;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.util.SparseIntArray;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint({"InlinedApi"})
@TargetApi(16)
public final class zzkc {
    private static final zzjx zzavi = zzjx.zzt("OMX.google.raw.decoder");
    private static final Pattern zzavj = Pattern.compile("^\\D?(\\d+)$");
    private static final HashMap<zza, List<zzjx>> zzavk = new HashMap<>();
    private static final SparseIntArray zzavl;
    private static final SparseIntArray zzavm;
    private static final Map<String, Integer> zzavn;
    private static int zzavo = -1;

    static final class zza {
        public final String mimeType;
        public final boolean zzatr;

        public zza(String str, boolean z) {
            this.mimeType = str;
            this.zzatr = z;
        }

        public final int hashCode() {
            return (((this.mimeType == null ? 0 : this.mimeType.hashCode()) + 31) * 31) + (this.zzatr ? 1231 : 1237);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || obj.getClass() != zza.class) {
                return false;
            }
            zza zza = (zza) obj;
            return TextUtils.equals(this.mimeType, zza.mimeType) && this.zzatr == zza.zzatr;
        }
    }

    public static zzjx zzeq() {
        return zzavi;
    }

    public static zzjx zzb(String str, boolean z) throws zzke {
        List zzc = zzc(str, z);
        if (zzc.isEmpty()) {
            return null;
        }
        return (zzjx) zzc.get(0);
    }

    private static synchronized List<zzjx> zzc(String str, boolean z) throws zzke {
        synchronized (zzkc.class) {
            zza zza2 = new zza(str, z);
            List<zzjx> list = (List) zzavk.get(zza2);
            if (list != null) {
                return list;
            }
            List zza3 = zza(zza2, zzqe.SDK_INT >= 21 ? new zzkh(z) : new zzkg());
            if (z && zza3.isEmpty() && 21 <= zzqe.SDK_INT && zzqe.SDK_INT <= 23) {
                zza3 = zza(zza2, (zzkf) new zzkg());
                if (!zza3.isEmpty()) {
                    String str2 = ((zzjx) zza3.get(0)).name;
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 63 + String.valueOf(str2).length());
                    sb.append("MediaCodecList API didn't list secure decoder for: ");
                    sb.append(str);
                    sb.append(". Assuming: ");
                    sb.append(str2);
                    Log.w("MediaCodecUtil", sb.toString());
                }
            }
            List<zzjx> unmodifiableList = Collections.unmodifiableList(zza3);
            zzavk.put(zza2, unmodifiableList);
            return unmodifiableList;
        }
    }

    public static int zzer() throws zzke {
        int i;
        if (zzavo == -1) {
            int i2 = 0;
            zzjx zzb = zzb(MimeTypes.VIDEO_H264, false);
            if (zzb != null) {
                CodecProfileLevel[] zzej = zzb.zzej();
                int length = zzej.length;
                int i3 = 0;
                while (i2 < length) {
                    switch (zzej[i2].level) {
                        case 1:
                        case 2:
                            i = 25344;
                            break;
                        case 8:
                        case 16:
                        case 32:
                            i = 101376;
                            break;
                        case 64:
                            i = 202752;
                            break;
                        case 128:
                        case 256:
                            i = 414720;
                            break;
                        case 512:
                            i = 921600;
                            break;
                        case 1024:
                            i = 1310720;
                            break;
                        case 2048:
                        case 4096:
                            i = 2097152;
                            break;
                        case 8192:
                            i = 2228224;
                            break;
                        case 16384:
                            i = 5652480;
                            break;
                        case 32768:
                        case 65536:
                            i = 9437184;
                            break;
                        default:
                            i = -1;
                            break;
                    }
                    i3 = Math.max(i, i3);
                    i2++;
                }
                i2 = Math.max(i3, zzqe.SDK_INT >= 21 ? 345600 : 172800);
            }
            zzavo = i2;
        }
        return zzavo;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        if (r3.equals("hev1") != false) goto L_0x0041;
     */
    public static Pair<Integer, Integer> zzw(String str) {
        if (str == null) {
            return null;
        }
        String[] split = str.split("\\.");
        char c = 0;
        String str2 = split[0];
        int i = 2;
        switch (str2.hashCode()) {
            case 3006243:
                if (str2.equals("avc1")) {
                    c = 2;
                    break;
                }
            case 3006244:
                if (str2.equals("avc2")) {
                    c = 3;
                    break;
                }
            case 3199032:
                break;
            case 3214780:
                if (str2.equals("hvc1")) {
                    c = 1;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
            case 1:
                if (split.length < 4) {
                    String str3 = "MediaCodecUtil";
                    String str4 = "Ignoring malformed HEVC codec string: ";
                    String valueOf = String.valueOf(str);
                    Log.w(str3, valueOf.length() != 0 ? str4.concat(valueOf) : new String(str4));
                    return null;
                }
                Matcher matcher = zzavj.matcher(split[1]);
                if (!matcher.matches()) {
                    String str5 = "MediaCodecUtil";
                    String str6 = "Ignoring malformed HEVC codec string: ";
                    String valueOf2 = String.valueOf(str);
                    Log.w(str5, valueOf2.length() != 0 ? str6.concat(valueOf2) : new String(str6));
                    return null;
                }
                String group = matcher.group(1);
                if ("1".equals(group)) {
                    i = 1;
                } else if (!"2".equals(group)) {
                    String str7 = "MediaCodecUtil";
                    String str8 = "Unknown HEVC profile string: ";
                    String valueOf3 = String.valueOf(group);
                    Log.w(str7, valueOf3.length() != 0 ? str8.concat(valueOf3) : new String(str8));
                    return null;
                }
                Integer num = (Integer) zzavn.get(split[3]);
                if (num != null) {
                    return new Pair<>(Integer.valueOf(i), num);
                }
                String str9 = "MediaCodecUtil";
                String str10 = "Unknown HEVC level string: ";
                String valueOf4 = String.valueOf(matcher.group(1));
                Log.w(str9, valueOf4.length() != 0 ? str10.concat(valueOf4) : new String(str10));
                return null;
            case 2:
            case 3:
                return zza(str, split);
            default:
                return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x01b4 A[Catch:{ Exception -> 0x02a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x029e A[SYNTHETIC] */
    private static List<zzjx> zza(zza zza2, zzkf zzkf) throws zzke {
        boolean z;
        zza zza3 = zza2;
        zzkf zzkf2 = zzkf;
        try {
            ArrayList arrayList = new ArrayList();
            String str = zza3.mimeType;
            int codecCount = zzkf.getCodecCount();
            boolean zzes = zzkf.zzes();
            int i = 0;
            while (i < codecCount) {
                MediaCodecInfo codecInfoAt = zzkf2.getCodecInfoAt(i);
                String name = codecInfoAt.getName();
                if (!codecInfoAt.isEncoder()) {
                    if (zzes || !name.endsWith(".secure")) {
                        if (zzqe.SDK_INT < 21) {
                            if (!"CIPAACDecoder".equals(name) && !"CIPMP3Decoder".equals(name) && !"CIPVorbisDecoder".equals(name) && !"CIPAMRNBDecoder".equals(name) && !"AACDecoder".equals(name)) {
                                if ("MP3Decoder".equals(name)) {
                                }
                            }
                        }
                        if (zzqe.SDK_INT >= 18 || !"OMX.SEC.MP3.Decoder".equals(name)) {
                            if (zzqe.SDK_INT >= 18 || !"OMX.MTK.AUDIO.DECODER.AAC".equals(name) || !"a70".equals(zzqe.DEVICE)) {
                                if (zzqe.SDK_INT == 16 && "OMX.qcom.audio.decoder.mp3".equals(name)) {
                                    if (!"dlxu".equals(zzqe.DEVICE) && !"protou".equals(zzqe.DEVICE) && !"ville".equals(zzqe.DEVICE) && !"villeplus".equals(zzqe.DEVICE) && !"villec2".equals(zzqe.DEVICE) && !zzqe.DEVICE.startsWith("gee") && !"C6602".equals(zzqe.DEVICE) && !"C6603".equals(zzqe.DEVICE) && !"C6606".equals(zzqe.DEVICE) && !"C6616".equals(zzqe.DEVICE) && !"L36h".equals(zzqe.DEVICE)) {
                                        if ("SO-02E".equals(zzqe.DEVICE)) {
                                        }
                                    }
                                }
                                if (zzqe.SDK_INT == 16 && "OMX.qcom.audio.decoder.aac".equals(name)) {
                                    if (!"C1504".equals(zzqe.DEVICE) && !"C1505".equals(zzqe.DEVICE) && !"C1604".equals(zzqe.DEVICE)) {
                                        if ("C1605".equals(zzqe.DEVICE)) {
                                        }
                                    }
                                }
                                if (zzqe.SDK_INT <= 19 && "OMX.SEC.vp8.dec".equals(name) && "samsung".equals(zzqe.MANUFACTURER)) {
                                    if (!zzqe.DEVICE.startsWith("d2") && !zzqe.DEVICE.startsWith("serrano") && !zzqe.DEVICE.startsWith("jflte") && !zzqe.DEVICE.startsWith("santos")) {
                                        if (zzqe.DEVICE.startsWith("t0")) {
                                        }
                                    }
                                }
                                if (zzqe.SDK_INT > 19 || !zzqe.DEVICE.startsWith("jflte") || !"OMX.qcom.video.decoder.vp8".equals(name)) {
                                    z = true;
                                    if (!z) {
                                        String[] supportedTypes = codecInfoAt.getSupportedTypes();
                                        int length = supportedTypes.length;
                                        int i2 = 0;
                                        while (i2 < length) {
                                            String str2 = supportedTypes[i2];
                                            if (str2.equalsIgnoreCase(str)) {
                                                try {
                                                    CodecCapabilities capabilitiesForType = codecInfoAt.getCapabilitiesForType(str2);
                                                    boolean zza4 = zzkf2.zza(str, capabilitiesForType);
                                                    boolean z2 = zzqe.SDK_INT <= 22 && (zzqe.MODEL.equals("ODROID-XU3") || zzqe.MODEL.equals("Nexus 10")) && ("OMX.Exynos.AVC.Decoder".equals(name) || "OMX.Exynos.AVC.Decoder.secure".equals(name));
                                                    if (zzes) {
                                                        if (zza3.zzatr != zza4) {
                                                        }
                                                        arrayList.add(zzjx.zza(name, str, capabilitiesForType, z2, false));
                                                    }
                                                    if (!zzes && !zza3.zzatr) {
                                                        arrayList.add(zzjx.zza(name, str, capabilitiesForType, z2, false));
                                                    } else if (!zzes && zza4) {
                                                        try {
                                                            arrayList.add(zzjx.zza(String.valueOf(name).concat(".secure"), str, capabilitiesForType, z2, true));
                                                            return arrayList;
                                                        } catch (Exception e) {
                                                            e = e;
                                                        }
                                                    }
                                                } catch (Exception e2) {
                                                    e = e2;
                                                    if (zzqe.SDK_INT > 23 || arrayList.isEmpty()) {
                                                        StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 25 + String.valueOf(str2).length());
                                                        sb.append("Failed to query codec ");
                                                        sb.append(name);
                                                        sb.append(" (");
                                                        sb.append(str2);
                                                        sb.append(")");
                                                        Log.e("MediaCodecUtil", sb.toString());
                                                        throw e;
                                                    }
                                                    StringBuilder sb2 = new StringBuilder(String.valueOf(name).length() + 46);
                                                    sb2.append("Skipping codec ");
                                                    sb2.append(name);
                                                    sb2.append(" (failed to query capabilities)");
                                                    Log.e("MediaCodecUtil", sb2.toString());
                                                    i2++;
                                                    zzkf2 = zzkf;
                                                }
                                            }
                                            i2++;
                                            zzkf2 = zzkf;
                                        }
                                        continue;
                                    }
                                    i++;
                                    zzkf2 = zzkf;
                                }
                            }
                        }
                    }
                }
                z = false;
                if (!z) {
                }
                i++;
                zzkf2 = zzkf;
            }
            return arrayList;
        } catch (Exception e3) {
            throw new zzke(e3);
        }
    }

    private static Pair<Integer, Integer> zza(String str, String[] strArr) {
        Integer num;
        Integer num2;
        if (strArr.length < 2) {
            String str2 = "MediaCodecUtil";
            String str3 = "Ignoring malformed AVC codec string: ";
            String valueOf = String.valueOf(str);
            Log.w(str2, valueOf.length() != 0 ? str3.concat(valueOf) : new String(str3));
            return null;
        }
        try {
            if (strArr[1].length() == 6) {
                Integer valueOf2 = Integer.valueOf(Integer.parseInt(strArr[1].substring(0, 2), 16));
                num = Integer.valueOf(Integer.parseInt(strArr[1].substring(4), 16));
                num2 = valueOf2;
            } else if (strArr.length >= 3) {
                num2 = Integer.valueOf(Integer.parseInt(strArr[1]));
                num = Integer.valueOf(Integer.parseInt(strArr[2]));
            } else {
                String str4 = "MediaCodecUtil";
                String str5 = "Ignoring malformed AVC codec string: ";
                String valueOf3 = String.valueOf(str);
                Log.w(str4, valueOf3.length() != 0 ? str5.concat(valueOf3) : new String(str5));
                return null;
            }
            Integer valueOf4 = Integer.valueOf(zzavl.get(num2.intValue()));
            if (valueOf4 == null) {
                String valueOf5 = String.valueOf(num2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf5).length() + 21);
                sb.append("Unknown AVC profile: ");
                sb.append(valueOf5);
                Log.w("MediaCodecUtil", sb.toString());
                return null;
            }
            Integer valueOf6 = Integer.valueOf(zzavm.get(num.intValue()));
            if (valueOf6 != null) {
                return new Pair<>(valueOf4, valueOf6);
            }
            String valueOf7 = String.valueOf(num);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf7).length() + 19);
            sb2.append("Unknown AVC level: ");
            sb2.append(valueOf7);
            Log.w("MediaCodecUtil", sb2.toString());
            return null;
        } catch (NumberFormatException unused) {
            String str6 = "MediaCodecUtil";
            String str7 = "Ignoring malformed AVC codec string: ";
            String valueOf8 = String.valueOf(str);
            Log.w(str6, valueOf8.length() != 0 ? str7.concat(valueOf8) : new String(str7));
            return null;
        }
    }

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        zzavl = sparseIntArray;
        sparseIntArray.put(66, 1);
        zzavl.put(77, 2);
        zzavl.put(88, 4);
        zzavl.put(100, 8);
        SparseIntArray sparseIntArray2 = new SparseIntArray();
        zzavm = sparseIntArray2;
        sparseIntArray2.put(10, 1);
        zzavm.put(11, 4);
        zzavm.put(12, 8);
        zzavm.put(13, 16);
        zzavm.put(20, 32);
        zzavm.put(21, 64);
        zzavm.put(22, 128);
        zzavm.put(30, 256);
        zzavm.put(31, 512);
        zzavm.put(32, 1024);
        zzavm.put(40, 2048);
        zzavm.put(41, 4096);
        zzavm.put(42, 8192);
        zzavm.put(50, 16384);
        zzavm.put(51, 32768);
        zzavm.put(52, 65536);
        HashMap hashMap = new HashMap();
        zzavn = hashMap;
        hashMap.put("L30", Integer.valueOf(1));
        zzavn.put("L60", Integer.valueOf(4));
        zzavn.put("L63", Integer.valueOf(16));
        zzavn.put("L90", Integer.valueOf(64));
        zzavn.put("L93", Integer.valueOf(256));
        zzavn.put("L120", Integer.valueOf(1024));
        zzavn.put("L123", Integer.valueOf(4096));
        zzavn.put("L150", Integer.valueOf(16384));
        zzavn.put("L153", Integer.valueOf(65536));
        zzavn.put("L156", Integer.valueOf(262144));
        zzavn.put("L180", Integer.valueOf(1048576));
        zzavn.put("L183", Integer.valueOf(4194304));
        zzavn.put("L186", Integer.valueOf(16777216));
        zzavn.put("H30", Integer.valueOf(2));
        zzavn.put("H60", Integer.valueOf(8));
        zzavn.put("H63", Integer.valueOf(32));
        zzavn.put("H90", Integer.valueOf(128));
        zzavn.put("H93", Integer.valueOf(512));
        zzavn.put("H120", Integer.valueOf(2048));
        zzavn.put("H123", Integer.valueOf(8192));
        zzavn.put("H150", Integer.valueOf(32768));
        zzavn.put("H153", Integer.valueOf(131072));
        zzavn.put("H156", Integer.valueOf(524288));
        zzavn.put("H180", Integer.valueOf(2097152));
        zzavn.put("H183", Integer.valueOf(8388608));
        zzavn.put("H186", Integer.valueOf(33554432));
    }
}
