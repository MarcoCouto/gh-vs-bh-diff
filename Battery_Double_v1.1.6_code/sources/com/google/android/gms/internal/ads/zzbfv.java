package com.google.android.gms.internal.ads;

import android.net.Uri;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.Clock;
import java.nio.ByteBuffer;

@zzark
public final class zzbfv extends zzbfk implements zzpn<zzov> {
    private String url;
    private ByteBuffer zzaep;
    private final zzbdy zzeuo;
    private boolean zzexf;
    private final zzbfu zzexg = new zzbfu();
    private final zzbfc zzexh = new zzbfc();
    private boolean zzexi;
    private final Object zzexj = new Object();
    private boolean zzexk;

    public zzbfv(zzbdz zzbdz, zzbdy zzbdy) {
        super(zzbdz);
        this.zzeuo = zzbdy;
    }

    public final /* bridge */ /* synthetic */ void zzc(Object obj, int i) {
    }

    public final /* bridge */ /* synthetic */ void zze(Object obj) {
    }

    public final String getUrl() {
        return this.url;
    }

    public final boolean zzadc() {
        return this.zzexk;
    }

    /* access modifiers changed from: protected */
    public final String zzey(String str) {
        String valueOf = String.valueOf("cache:");
        String valueOf2 = String.valueOf(super.zzey(str));
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    private final void zzabm() {
        int zzl = (int) this.zzexh.zzl(this.zzaep);
        int round = Math.round(((float) zzl) * (((float) this.zzaep.position()) / ((float) ((int) this.zzexg.zzadb()))));
        zza(this.url, zzey(this.url), (long) round, (long) zzl, round > 0, zzbes.zzacx(), zzbes.zzacy());
    }

    /* JADX WARNING: type inference failed for: r1v29, types: [com.google.android.gms.internal.ads.zzbep] */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r9.zzexk = true;
        zzc(r10, r11, (long) ((int) r9.zzexh.zzl(r9.zzaep)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c2, code lost:
        if (r9.zzaep.remaining() > 0) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c4, code lost:
        zzabm();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00cb, code lost:
        if (r9.zzexf != false) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00cd, code lost:
        r12 = r1.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d6, code lost:
        if ((r12 - r16) < r4) goto L_0x00dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d8, code lost:
        zzabm();
        r16 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00e5, code lost:
        if ((r12 - r2) > (1000 * r6)) goto L_0x00ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ed, code lost:
        r12 = "downloadTimeout";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r1 = new java.lang.StringBuilder(49);
        r1.append("Timeout exceeded. Limit: ");
        r1.append(r6);
        r1.append(" sec");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x010c, code lost:
        throw new java.io.IOException(r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x010d, code lost:
        r12 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        r1 = r9.zzaep.limit();
        r3 = new java.lang.StringBuilder(35);
        r3.append("Precache abort at ");
        r3.append(r1);
        r3.append(" bytes");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0132, code lost:
        throw new java.io.IOException(r3.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0138, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0139, code lost:
        r12 = r18;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final boolean zzex(String str) {
        String str2;
        String str3 = str;
        this.url = str3;
        String zzey = zzey(str);
        String str4 = "error";
        int i = 0;
        zzpb zzpb = new zzpb(this.zzeiz, null, this, this.zzeuo.zzetn, this.zzeuo.zzetp, true, null);
        if (this.zzeuo.zzets) {
            try {
                zzpb = new zzbep(this.mContext, zzpb, null, null);
            } catch (Exception e) {
                e = e;
                String canonicalName = e.getClass().getCanonicalName();
                String message = e.getMessage();
                StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 1 + String.valueOf(message).length());
                sb.append(canonicalName);
                sb.append(":");
                sb.append(message);
                String sb2 = sb.toString();
                StringBuilder sb3 = new StringBuilder(String.valueOf(str).length() + 34 + String.valueOf(sb2).length());
                sb3.append("Failed to preload url ");
                sb3.append(str3);
                sb3.append(" Exception: ");
                sb3.append(sb2);
                zzaxz.zzeo(sb3.toString());
                zza(str3, zzey, str4, sb2);
                return false;
            }
        }
        try {
            zzpb.zza(new zzoz(Uri.parse(str)));
            zzbdz zzbdz = (zzbdz) this.zzewo.get();
            if (zzbdz != null) {
                zzbdz.zza(zzey, (zzbfk) this);
            }
            Clock zzlm = zzbv.zzlm();
            long currentTimeMillis = zzlm.currentTimeMillis();
            long longValue = ((Long) zzwu.zzpz().zzd(zzaan.zzcox)).longValue();
            long longValue2 = ((Long) zzwu.zzpz().zzd(zzaan.zzcow)).longValue();
            this.zzaep = ByteBuffer.allocate(this.zzeuo.zzetm);
            int i2 = 8192;
            byte[] bArr = new byte[8192];
            long j = currentTimeMillis;
            while (true) {
                int read = zzpb.read(bArr, i, Math.min(this.zzaep.remaining(), i2));
                if (read == -1) {
                    break;
                }
                synchronized (this.zzexj) {
                    try {
                        if (!this.zzexf) {
                            str2 = str4;
                            try {
                                this.zzaep.put(bArr, 0, read);
                            } catch (Throwable th) {
                                th = th;
                                throw th;
                            }
                        } else {
                            str2 = str4;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        str2 = str4;
                        throw th;
                    }
                }
                str4 = str2;
                i2 = 8192;
                i = 0;
            }
            return true;
        } catch (Exception e2) {
            e = e2;
            String str5 = str4;
            String canonicalName2 = e.getClass().getCanonicalName();
            String message2 = e.getMessage();
            StringBuilder sb4 = new StringBuilder(String.valueOf(canonicalName2).length() + 1 + String.valueOf(message2).length());
            sb4.append(canonicalName2);
            sb4.append(":");
            sb4.append(message2);
            String sb22 = sb4.toString();
            StringBuilder sb32 = new StringBuilder(String.valueOf(str).length() + 34 + String.valueOf(sb22).length());
            sb32.append("Failed to preload url ");
            sb32.append(str3);
            sb32.append(" Exception: ");
            sb32.append(sb22);
            zzaxz.zzeo(sb32.toString());
            zza(str3, zzey, str4, sb22);
            return false;
        }
    }

    public final void abort() {
        this.zzexf = true;
    }

    public final ByteBuffer getByteBuffer() {
        synchronized (this.zzexj) {
            if (this.zzaep != null && !this.zzexi) {
                this.zzaep.flip();
                this.zzexi = true;
            }
            this.zzexf = true;
        }
        return this.zzaep;
    }

    public final /* synthetic */ void zza(Object obj, zzoz zzoz) {
        zzov zzov = (zzov) obj;
        if (zzov instanceof zzpb) {
            this.zzexg.zza((zzpb) zzov);
        }
    }
}
