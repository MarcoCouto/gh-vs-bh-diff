package com.google.android.gms.internal.ads;

import android.util.Pair;
import android.util.SparseIntArray;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.MimeTypes;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

final class zzms implements zzlm, zzlx<zzmj<zzmq>> {
    final int id;
    private final int zzawn;
    private final zzot zzawq;
    private zzln zzawz;
    private final zzkt zzbah;
    private final zzmr zzbax;
    private final long zzbay;
    private final zzpk zzbaz;
    private final zzmt[] zzbba;
    private zzmj<zzmq>[] zzbbb = new zzmj[0];
    private zzla zzbbc = new zzla(this.zzbbb);
    private zznj zzbbd;
    private List<zzni> zzbbe;
    private final zzma zzxk;
    private int zzyr;

    public zzms(int i, zznj zznj, int i2, zzmr zzmr, int i3, zzkt zzkt, long j, zzpk zzpk, zzot zzot) {
        int i4;
        boolean z;
        boolean z2;
        zznm zznm;
        int i5;
        this.id = i;
        this.zzbbd = zznj;
        this.zzyr = i2;
        this.zzbax = zzmr;
        this.zzawn = i3;
        this.zzbah = zzkt;
        this.zzbay = j;
        this.zzbaz = zzpk;
        this.zzawq = zzot;
        this.zzbbe = zznj.zzba(i2).zzbbe;
        List<zzni> list = this.zzbbe;
        int size = list.size();
        SparseIntArray sparseIntArray = new SparseIntArray(size);
        for (int i6 = 0; i6 < size; i6++) {
            sparseIntArray.put(((zzni) list.get(i6)).id, i6);
        }
        int[][] iArr = new int[size][];
        boolean[] zArr = new boolean[size];
        int i7 = 0;
        for (int i8 = 0; i8 < size; i8++) {
            if (!zArr[i8]) {
                zArr[i8] = true;
                List<zznm> list2 = ((zzni) list.get(i8)).zzbcp;
                int i9 = 0;
                while (true) {
                    if (i9 >= list2.size()) {
                        zznm = null;
                        break;
                    }
                    zznm zznm2 = (zznm) list2.get(i9);
                    if ("urn:mpeg:dash:adaptation-set-switching:2016".equals(zznm2.zzbdi)) {
                        zznm = zznm2;
                        break;
                    }
                    i9++;
                }
                if (zznm == null) {
                    i5 = i7 + 1;
                    iArr[i7] = new int[]{i8};
                } else {
                    String[] split = zznm.value.split(",");
                    int[] iArr2 = new int[(split.length + 1)];
                    iArr2[0] = i8;
                    int i10 = 0;
                    while (i10 < split.length) {
                        int i11 = sparseIntArray.get(Integer.parseInt(split[i10]));
                        zArr[i11] = true;
                        i10++;
                        iArr2[i10] = i11;
                    }
                    i5 = i7 + 1;
                    iArr[i7] = iArr2;
                }
                i7 = i5;
            }
        }
        if (i7 < size) {
            iArr = (int[][]) Arrays.copyOf(iArr, i7);
        }
        int length = iArr.length;
        boolean[] zArr2 = new boolean[length];
        boolean[] zArr3 = new boolean[length];
        int i12 = length;
        for (int i13 = 0; i13 < length; i13++) {
            int[] iArr3 = iArr[i13];
            int length2 = iArr3.length;
            int i14 = 0;
            while (true) {
                if (i14 >= length2) {
                    z = false;
                    break;
                }
                List<zznp> list3 = ((zzni) list.get(iArr3[i14])).zzbcn;
                for (int i15 = 0; i15 < list3.size(); i15++) {
                    if (!((zznp) list3.get(i15)).zzbdo.isEmpty()) {
                        z = true;
                        break;
                    }
                }
                i14++;
            }
            if (z) {
                zArr2[i13] = true;
                i12++;
            }
            int[] iArr4 = iArr[i13];
            int length3 = iArr4.length;
            int i16 = 0;
            while (true) {
                if (i16 >= length3) {
                    z2 = false;
                    break;
                }
                List<zznm> list4 = ((zzni) list.get(iArr4[i16])).zzbco;
                for (int i17 = 0; i17 < list4.size(); i17++) {
                    if ("urn:scte:dash:cc:cea-608:2015".equals(((zznm) list4.get(i17)).zzbdi)) {
                        z2 = true;
                        break;
                    }
                }
                i16++;
            }
            if (z2) {
                zArr3[i13] = true;
                i12++;
            }
        }
        zzlz[] zzlzArr = new zzlz[i12];
        zzmt[] zzmtArr = new zzmt[i12];
        int i18 = 0;
        int i19 = 0;
        while (i18 < length) {
            int[] iArr5 = iArr[i18];
            ArrayList arrayList = new ArrayList();
            for (int i20 : iArr5) {
                arrayList.addAll(((zzni) list.get(i20)).zzbcn);
            }
            zzfs[] zzfsArr = new zzfs[arrayList.size()];
            for (int i21 = 0; i21 < zzfsArr.length; i21++) {
                zzfsArr[i21] = ((zznp) arrayList.get(i21)).zzaad;
            }
            zzni zzni = (zzni) list.get(iArr5[0]);
            boolean z3 = zArr2[i18];
            boolean z4 = zArr3[i18];
            zzlzArr[i19] = new zzlz(zzfsArr);
            int i22 = i19 + 1;
            List<zzni> list5 = list;
            zzmt zzmt = new zzmt(zzni.type, iArr5, i19, true, z3, z4);
            zzmtArr[i19] = zzmt;
            if (z3) {
                int i23 = zzni.id;
                StringBuilder sb = new StringBuilder(16);
                sb.append(i23);
                sb.append(":emsg");
                i4 = length;
                zzlzArr[i22] = new zzlz(zzfs.zza(sb.toString(), MimeTypes.APPLICATION_EMSG, null, -1, null));
                int i24 = i22 + 1;
                zzmt zzmt2 = new zzmt(4, iArr5, i19, false, false, false);
                zzmtArr[i22] = zzmt2;
                i22 = i24;
            } else {
                i4 = length;
            }
            if (z4) {
                int i25 = zzni.id;
                StringBuilder sb2 = new StringBuilder(18);
                sb2.append(i25);
                sb2.append(":cea608");
                zzlzArr[i22] = new zzlz(zzfs.zza(sb2.toString(), MimeTypes.APPLICATION_CEA608, (String) null, -1, 0, (String) null, (zzhp) null));
                int i26 = i22 + 1;
                zzmt zzmt3 = new zzmt(3, iArr5, i19, false, false, false);
                zzmtArr[i22] = zzmt3;
                i19 = i26;
            } else {
                i19 = i22;
            }
            i18++;
            list = list5;
            length = i4;
        }
        Pair create = Pair.create(new zzma(zzlzArr), zzmtArr);
        this.zzxk = (zzma) create.first;
        this.zzbba = (zzmt[]) create.second;
    }

    public final long zzey() {
        return C.TIME_UNSET;
    }

    public final void zza(zznj zznj, int i) {
        this.zzbbd = zznj;
        this.zzyr = i;
        this.zzbbe = zznj.zzba(i).zzbbe;
        if (this.zzbbb != null) {
            for (zzmj<zzmq> zzfy : this.zzbbb) {
                ((zzmq) zzfy.zzfy()).zza(zznj, i);
            }
            this.zzawz.zza(this);
        }
    }

    public final void release() {
        for (zzmj<zzmq> release : this.zzbbb) {
            release.release();
        }
    }

    public final void zza(zzln zzln, long j) {
        this.zzawz = zzln;
        zzln.zza(this);
    }

    public final void zzew() throws IOException {
        this.zzbaz.zzev();
    }

    public final zzma zzex() {
        return this.zzxk;
    }

    public final long zza(zzom[] zzomArr, boolean[] zArr, zzlv[] zzlvArr, boolean[] zArr2, long j) {
        zzlv zzlv;
        int i;
        int i2;
        zzom[] zzomArr2 = zzomArr;
        long j2 = j;
        HashMap hashMap = new HashMap();
        int i3 = 0;
        while (i3 < zzomArr2.length) {
            if (zzlvArr[i3] instanceof zzmj) {
                zzmj zzmj = (zzmj) zzlvArr[i3];
                if (zzomArr2[i3] == null || !zArr[i3]) {
                    zzmj.release();
                    zzlvArr[i3] = null;
                } else {
                    hashMap.put(Integer.valueOf(this.zzxk.zza(zzomArr2[i3].zzgk())), zzmj);
                }
            }
            if (zzlvArr[i3] == null && zzomArr2[i3] != null) {
                int zza = this.zzxk.zza(zzomArr2[i3].zzgk());
                zzmt zzmt = this.zzbba[zza];
                if (zzmt.zzbbg) {
                    zzom zzom = zzomArr2[i3];
                    int[] iArr = new int[2];
                    boolean z = zzmt.zzbbi;
                    if (z) {
                        iArr[0] = 4;
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    boolean z2 = zzmt.zzbbj;
                    if (z2) {
                        int i4 = i2 + 1;
                        iArr[i2] = 3;
                        i2 = i4;
                    }
                    if (i2 < 2) {
                        iArr = Arrays.copyOf(iArr, i2);
                    }
                    i = i3;
                    zzmj zzmj2 = new zzmj(zzmt.zzwg, iArr, this.zzbax.zza(this.zzbaz, this.zzbbd, this.zzyr, zzmt.zzbbf, zzom, zzmt.zzwg, this.zzbay, z, z2), this, this.zzawq, j, this.zzawn, this.zzbah);
                    hashMap.put(Integer.valueOf(zza), zzmj2);
                    zzlvArr[i] = zzmj2;
                    zArr2[i] = true;
                    i3 = i + 1;
                    long j3 = j;
                    zzomArr2 = zzomArr;
                }
            }
            i = i3;
            i3 = i + 1;
            long j32 = j;
            zzomArr2 = zzomArr;
        }
        zzom[] zzomArr3 = zzomArr;
        for (int i5 = 0; i5 < zzomArr3.length; i5++) {
            if (((zzlvArr[i5] instanceof zzmk) || (zzlvArr[i5] instanceof zzlb)) && (zzomArr3[i5] == null || !zArr[i5])) {
                zza(zzlvArr[i5]);
                zzlvArr[i5] = null;
            }
            if (zzomArr3[i5] != null) {
                zzmt zzmt2 = this.zzbba[this.zzxk.zza(zzomArr3[i5].zzgk())];
                if (!zzmt2.zzbbg) {
                    zzmj<T> zzmj3 = (zzmj) hashMap.get(Integer.valueOf(zzmt2.zzbbh));
                    zzlv zzlv2 = zzlvArr[i5];
                    boolean z3 = zzmj3 == null ? zzlv2 instanceof zzlb : (zzlv2 instanceof zzmk) && ((zzmk) zzlv2).zzbap == zzmj3;
                    if (!z3) {
                        zza(zzlv2);
                        if (zzmj3 == null) {
                            zzlv = new zzlb();
                            long j4 = j;
                        } else {
                            zzlv = zzmj3.zza(j, zzmt2.zzwg);
                        }
                        zzlvArr[i5] = zzlv;
                        zArr2[i5] = true;
                    }
                }
            }
            long j5 = j;
        }
        long j6 = j;
        this.zzbbb = new zzmj[hashMap.size()];
        hashMap.values().toArray(this.zzbbb);
        this.zzbbc = new zzla(this.zzbbb);
        return j6;
    }

    public final void zzaa(long j) {
        for (zzmj<zzmq> zzaf : this.zzbbb) {
            zzaf.zzaf(j);
        }
    }

    public final boolean zzy(long j) {
        return this.zzbbc.zzy(j);
    }

    public final long zzeu() {
        return this.zzbbc.zzeu();
    }

    public final long zzez() {
        long j = Long.MAX_VALUE;
        for (zzmj<zzmq> zzez : this.zzbbb) {
            long zzez2 = zzez.zzez();
            if (zzez2 != Long.MIN_VALUE) {
                j = Math.min(j, zzez2);
            }
        }
        if (j == Long.MAX_VALUE) {
            return Long.MIN_VALUE;
        }
        return j;
    }

    public final long zzab(long j) {
        for (zzmj<zzmq> zzag : this.zzbbb) {
            zzag.zzag(j);
        }
        return j;
    }

    private static void zza(zzlv zzlv) {
        if (zzlv instanceof zzmk) {
            ((zzmk) zzlv).release();
        }
    }

    public final /* synthetic */ void zza(zzlw zzlw) {
        this.zzawz.zza(this);
    }
}
