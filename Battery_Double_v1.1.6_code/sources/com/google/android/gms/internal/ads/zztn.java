package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

@zzark
public final class zztn {
    private final int zzbyz;
    private final zztd zzbzb;
    private String zzbzj;
    private String zzbzk;
    private final boolean zzbzl = false;
    private final int zzbzm;
    private final int zzbzn;

    public zztn(int i, int i2, int i3) {
        this.zzbyz = i;
        if (i2 > 64 || i2 < 0) {
            this.zzbzm = 64;
        } else {
            this.zzbzm = i2;
        }
        if (i3 <= 0) {
            this.zzbzn = 1;
        } else {
            this.zzbzn = i3;
        }
        this.zzbzb = new zztm(this.zzbzm);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0105, code lost:
        if (r2.size() < r1.zzbyz) goto L_0x0109;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0109 A[SYNTHETIC] */
    public final String zza(ArrayList<String> arrayList, ArrayList<zztc> arrayList2) {
        String str;
        String[] zze;
        boolean z;
        ArrayList<zztc> arrayList3 = arrayList2;
        Collections.sort(arrayList3, new zzto(this));
        HashSet hashSet = new HashSet();
        for (int i = 0; i < arrayList2.size(); i++) {
            String[] split = Normalizer.normalize((CharSequence) arrayList.get(((zztc) arrayList3.get(i)).zzob()), Form.NFKC).toLowerCase(Locale.US).split("\n");
            boolean z2 = true;
            if (split.length != 0) {
                int i2 = 0;
                while (true) {
                    if (i2 >= split.length) {
                        break;
                    }
                    String str2 = split[i2];
                    if (str2.indexOf("'") != -1) {
                        StringBuilder sb = new StringBuilder(str2);
                        int i3 = 1;
                        boolean z3 = false;
                        while (true) {
                            int i4 = i3 + 2;
                            if (i4 > sb.length()) {
                                break;
                            }
                            if (sb.charAt(i3) == '\'') {
                                if (sb.charAt(i3 - 1) != ' ') {
                                    int i5 = i3 + 1;
                                    if ((sb.charAt(i5) == 's' || sb.charAt(i5) == 'S') && (i4 == sb.length() || sb.charAt(i4) == ' ')) {
                                        sb.insert(i3, ' ');
                                        i3 = i4;
                                        z3 = true;
                                    }
                                }
                                sb.setCharAt(i3, ' ');
                                z3 = true;
                            }
                            i3++;
                        }
                        str = z3 ? sb.toString() : null;
                        if (str != null) {
                            this.zzbzk = str;
                            zze = zzth.zze(str, true);
                            if (zze.length < this.zzbzn) {
                                int i6 = 0;
                                while (true) {
                                    if (i6 >= zze.length) {
                                        break;
                                    }
                                    String str3 = "";
                                    int i7 = 0;
                                    while (true) {
                                        if (i7 >= this.zzbzn) {
                                            z = true;
                                            break;
                                        }
                                        int i8 = i6 + i7;
                                        if (i8 >= zze.length) {
                                            z = false;
                                            break;
                                        }
                                        if (i7 > 0) {
                                            str3 = String.valueOf(str3).concat(" ");
                                        }
                                        String valueOf = String.valueOf(str3);
                                        String valueOf2 = String.valueOf(zze[i8]);
                                        str3 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                                        i7++;
                                    }
                                    if (!z) {
                                        break;
                                    }
                                    hashSet.add(str3);
                                    if (hashSet.size() >= this.zzbyz) {
                                        break;
                                    }
                                    i6++;
                                }
                            }
                            i2++;
                        }
                    }
                    str = str2;
                    zze = zzth.zze(str, true);
                    if (zze.length < this.zzbzn) {
                    }
                    i2++;
                }
                z2 = false;
            }
            if (!z2) {
                break;
            }
        }
        zztg zztg = new zztg();
        this.zzbzj = "";
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            try {
                zztg.write(this.zzbzb.zzay((String) it.next()));
            } catch (IOException e) {
                zzaxz.zzb("Error while writing hash to byteStream", e);
            }
        }
        return zztg.toString();
    }
}
