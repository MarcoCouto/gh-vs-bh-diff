package com.google.android.gms.internal.ads;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzark
public final class zzbfs extends zzbfk {
    private static final Set<String> zzewz = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat zzexa = new DecimalFormat("#,###");
    private File zzexb;
    private boolean zzexc;

    public zzbfs(zzbdz zzbdz) {
        super(zzbdz);
        File cacheDir = this.mContext.getCacheDir();
        if (cacheDir == null) {
            zzaxz.zzeo("Context.getCacheDir() returned null");
            return;
        }
        this.zzexb = new File(cacheDir, "admobVideoStreams");
        if (!this.zzexb.isDirectory() && !this.zzexb.mkdirs()) {
            String str = "Could not create preload cache directory at ";
            String valueOf = String.valueOf(this.zzexb.getAbsolutePath());
            zzaxz.zzeo(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
            this.zzexb = null;
        } else if (!this.zzexb.setReadable(true, false) || !this.zzexb.setExecutable(true, false)) {
            String str2 = "Could not set cache file permissions at ";
            String valueOf2 = String.valueOf(this.zzexb.getAbsolutePath());
            zzaxz.zzeo(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2));
            this.zzexb = null;
        }
    }

    /* JADX WARNING: type inference failed for: r10v0 */
    /* JADX WARNING: type inference failed for: r10v1, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r10v2 */
    /* JADX WARNING: type inference failed for: r4v7, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r10v3 */
    /* JADX WARNING: type inference failed for: r25v0 */
    /* JADX WARNING: type inference failed for: r10v4 */
    /* JADX WARNING: type inference failed for: r25v1 */
    /* JADX WARNING: type inference failed for: r25v2 */
    /* JADX WARNING: type inference failed for: r4v8 */
    /* JADX WARNING: type inference failed for: r25v3 */
    /* JADX WARNING: type inference failed for: r4v9 */
    /* JADX WARNING: type inference failed for: r25v4, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r25v5 */
    /* JADX WARNING: type inference failed for: r10v8 */
    /* JADX WARNING: type inference failed for: r25v6 */
    /* JADX WARNING: type inference failed for: r25v7 */
    /* JADX WARNING: type inference failed for: r10v9 */
    /* JADX WARNING: type inference failed for: r10v11 */
    /* JADX WARNING: type inference failed for: r25v8, types: [int] */
    /* JADX WARNING: type inference failed for: r25v9 */
    /* JADX WARNING: type inference failed for: r10v15 */
    /* JADX WARNING: type inference failed for: r25v10 */
    /* JADX WARNING: type inference failed for: r25v11 */
    /* JADX WARNING: type inference failed for: r25v12 */
    /* JADX WARNING: type inference failed for: r4v10 */
    /* JADX WARNING: type inference failed for: r25v13 */
    /* JADX WARNING: type inference failed for: r25v14 */
    /* JADX WARNING: type inference failed for: r25v15 */
    /* JADX WARNING: type inference failed for: r10v18 */
    /* JADX WARNING: type inference failed for: r10v19 */
    /* JADX WARNING: type inference failed for: r4v28 */
    /* JADX WARNING: type inference failed for: r4v29 */
    /* JADX WARNING: type inference failed for: r10v20 */
    /* JADX WARNING: type inference failed for: r25v16 */
    /* JADX WARNING: type inference failed for: r4v30 */
    /* JADX WARNING: type inference failed for: r25v17 */
    /* JADX WARNING: type inference failed for: r25v18 */
    /* JADX WARNING: type inference failed for: r25v19 */
    /* JADX WARNING: type inference failed for: r25v20 */
    /* JADX WARNING: type inference failed for: r25v21 */
    /* JADX WARNING: type inference failed for: r25v22 */
    /* JADX WARNING: type inference failed for: r25v23 */
    /* JADX WARNING: type inference failed for: r25v24 */
    /* JADX WARNING: type inference failed for: r25v25 */
    /* JADX WARNING: type inference failed for: r25v26 */
    /* JADX WARNING: type inference failed for: r25v27 */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:211|212|213|214|215|216|217) */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01f2, code lost:
        if ((r5 instanceof java.net.HttpURLConnection) == false) goto L_0x024b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:?, code lost:
        r1 = r5.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01fd, code lost:
        if (r1 < 400) goto L_0x024b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01ff, code lost:
        r2 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0201, code lost:
        r0 = "HTTP request failed. Code: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        r3 = java.lang.String.valueOf(java.lang.Integer.toString(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x020f, code lost:
        if (r3.length() == 0) goto L_0x0217;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0211, code lost:
        r3 = r0.concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0217, code lost:
        r3 = new java.lang.String(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:?, code lost:
        r5 = new java.lang.StringBuilder(java.lang.String.valueOf(r31).length() + 32);
        r5.append("HTTP status code ");
        r5.append(r1);
        r5.append(" at ");
        r5.append(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0244, code lost:
        throw new java.io.IOException(r5.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0245, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0246, code lost:
        r15 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0248, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0249, code lost:
        r15 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:?, code lost:
        r7 = r5.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x024f, code lost:
        if (r7 >= 0) goto L_0x027a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0251, code lost:
        r0 = "Stream cache aborted, missing content-length header at ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:?, code lost:
        r1 = java.lang.String.valueOf(r31);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x025b, code lost:
        if (r1.length() == 0) goto L_0x0262;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x025d, code lost:
        r0 = r0.concat(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0262, code lost:
        r0 = new java.lang.String(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0268, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzeo(r0);
        zza(r9, r12.getAbsolutePath(), "contentLengthMissing", null);
        zzewz.remove(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0279, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:?, code lost:
        r1 = zzexa.format((long) r7);
        r3 = ((java.lang.Integer) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcou)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0291, code lost:
        if (r7 <= r3) goto L_0x02e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:?, code lost:
        r2 = new java.lang.StringBuilder((java.lang.String.valueOf(r1).length() + 33) + java.lang.String.valueOf(r31).length());
        r2.append("Content length ");
        r2.append(r1);
        r2.append(" exceeds limit at ");
        r2.append(r9);
        com.google.android.gms.internal.ads.zzaxz.zzeo(r2.toString());
        r0 = "File too big for full file cache. Size: ";
        r1 = java.lang.String.valueOf(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x02cc, code lost:
        if (r1.length() == 0) goto L_0x02d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x02ce, code lost:
        r0 = r0.concat(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x02d3, code lost:
        r0 = new java.lang.String(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x02d9, code lost:
        zza(r9, r12.getAbsolutePath(), "sizeExceeded", r0);
        zzewz.remove(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x02e7, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:?, code lost:
        r4 = new java.lang.StringBuilder((java.lang.String.valueOf(r1).length() + 20) + java.lang.String.valueOf(r31).length());
        r4.append("Caching ");
        r4.append(r1);
        r4.append(" bytes from ");
        r4.append(r9);
        com.google.android.gms.internal.ads.zzaxz.zzdn(r4.toString());
        r5 = java.nio.channels.Channels.newChannel(r5.getInputStream());
        r4 = new java.io.FileOutputStream
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:?, code lost:
        r2 = r4.getChannel();
        r1 = java.nio.ByteBuffer.allocate(1048576);
        r16 = com.google.android.gms.ads.internal.zzbv.zzlm();
        r17 = r16.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0341, code lost:
        r20 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0343, code lost:
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:?, code lost:
        r10 = new com.google.android.gms.internal.ads.zzbai(((java.lang.Long) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcox)).longValue());
        r13 = ((java.lang.Long) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcow)).longValue();
        r6 = 0;
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x035d, code lost:
        r21 = r5.read(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0361, code lost:
        if (r21 < 0) goto L_0x0469;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0363, code lost:
        r6 = r6 + r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0365, code lost:
        if (r6 <= r3) goto L_0x039e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0367, code lost:
        r1 = "sizeExceeded";
        r0 = "File too big for full file cache. Size: ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:?, code lost:
        r2 = java.lang.String.valueOf(java.lang.Integer.toString(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0377, code lost:
        if (r2.length() == 0) goto L_0x037f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0379, code lost:
        r10 = r0.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0384, code lost:
        r10 = new java.lang.String(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x038c, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x038d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x038e, code lost:
        r15 = r1;
        r3 = r10;
        r1 = r20;
        r10 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0395, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0396, code lost:
        r15 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0399, code lost:
        r10 = r4;
        r1 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x039e, code lost:
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:?, code lost:
        r1.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03a5, code lost:
        if (r2.write(r1) > 0) goto L_0x03a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03a7, code lost:
        r1.clear();
        r25 = ((r16.currentTimeMillis() - r17) > (1000 * r13) ? 1 : ((r16.currentTimeMillis() - r17) == (1000 * r13) ? 0 : -1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03b8, code lost:
        if (r25 > 0) goto L_0x042c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03bc, code lost:
        if (r8.zzexc != false) goto L_0x041e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x03c2, code lost:
        if (r10.tryAcquire() == false) goto L_0x03f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03c4, code lost:
        r11 = r12.getAbsolutePath();
        r26 = r10;
        r10 = com.google.android.gms.internal.ads.zzbat.zztu;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x03cc, code lost:
        r27 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x03ce, code lost:
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x03d0, code lost:
        r22 = r1;
        r1 = r1;
        r23 = r2;
        r24 = r3;
        r25 = r4;
        r4 = r11;
        r11 = r5;
        r19 = r6;
        r29 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x03eb, code lost:
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:?, code lost:
        r1 = new com.google.android.gms.internal.ads.zzbfl(r30, r31, r4, r6, r7, false);
        r10.post(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x03f2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x03f3, code lost:
        r25 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x03f7, code lost:
        r22 = r1;
        r23 = r2;
        r24 = r3;
        r25 = r4;
        r11 = r5;
        r19 = r6;
        r29 = r7;
        r26 = r10;
        r27 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x040a, code lost:
        r5 = r11;
        r6 = r19;
        r1 = r22;
        r2 = r23;
        r3 = r24;
        r4 = r25;
        r10 = r26;
        r15 = r27;
        r7 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x041e, code lost:
        r25 = r4;
        r27 = r15;
        r15 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x042b, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x042c, code lost:
        r25 = r4;
        r27 = r15;
        r15 = "downloadTimeout";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x0432, code lost:
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:?, code lost:
        r0 = java.lang.Long.toString(r13);
        r2 = new java.lang.StringBuilder(java.lang.String.valueOf(r0).length() + 29);
        r2.append("Timeout exceeded. Limit: ");
        r2.append(r0);
        r2.append(" sec");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x045d, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x045e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x045f, code lost:
        r3 = r2.toString();
        r1 = r20;
        r10 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x0466, code lost:
        r0 = e;
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0469, code lost:
        r25 = r4;
        r27 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x046f, code lost:
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:?, code lost:
        r25.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x0476, code lost:
        if (com.google.android.gms.internal.ads.zzaxz.isLoggable(3) == false) goto L_0x04ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x0478, code lost:
        r1 = zzexa.format((long) r6);
        r3 = new java.lang.StringBuilder((java.lang.String.valueOf(r1).length() + 22) + java.lang.String.valueOf(r31).length());
        r3.append("Preloaded ");
        r3.append(r1);
        r3.append(" bytes from ");
        r3.append(r9);
        com.google.android.gms.internal.ads.zzaxz.zzdn(r3.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x04ae, code lost:
        r12.setReadable(true, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x04b7, code lost:
        if (r0.isFile() == false) goto L_0x04c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x04b9, code lost:
        r0.setLastModified(java.lang.System.currentTimeMillis());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:?, code lost:
        r0.createNewFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x04c4, code lost:
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:?, code lost:
        zza(r9, r12.getAbsolutePath(), r6);
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x04cd, code lost:
        r1 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:?, code lost:
        zzewz.remove(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x04d3, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x04d4, code lost:
        r0 = e;
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x04d6, code lost:
        r0 = e;
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x04d7, code lost:
        r1 = r20;
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x04d9, code lost:
        r10 = r25;
        r15 = r27;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x04de, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:0x04df, code lost:
        r25 = r4;
        r27 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x04e3, code lost:
        r1 = r20;
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x04e6, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x04e7, code lost:
        r25 = r4;
        r1 = r14;
        r27 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x04ec, code lost:
        r10 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x04ee, code lost:
        r3 = null;
        r10 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x04f0, code lost:
        r1 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x04fa, code lost:
        throw new java.io.IOException("Invalid protocol.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x04fb, code lost:
        r1 = r14;
        r27 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x0505, code lost:
        throw new java.io.IOException("Too many redirects (20)");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x0506, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x0507, code lost:
        r15 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x050a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x050b, code lost:
        r1 = r14;
        r27 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x050e, code lost:
        r3 = null;
        r10 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x0512, code lost:
        if ((r0 instanceof java.lang.RuntimeException) != false) goto L_0x0514;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x0514, code lost:
        com.google.android.gms.ads.internal.zzbv.zzlj().zza(r0, "VideoStreamFullFileCache.preload");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x0522, code lost:
        if (r8.zzexc == false) goto L_0x0548;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x0524, code lost:
        r2 = new java.lang.StringBuilder(java.lang.String.valueOf(r31).length() + 26);
        r2.append("Preload aborted for URL \"");
        r2.append(r9);
        r2.append("\"");
        com.google.android.gms.internal.ads.zzaxz.zzen(r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x0548, code lost:
        r4 = new java.lang.StringBuilder(java.lang.String.valueOf(r31).length() + 25);
        r4.append("Preload failed for URL \"");
        r4.append(r9);
        r4.append("\"");
        com.google.android.gms.internal.ads.zzaxz.zzc(r4.toString(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x0577, code lost:
        r0 = "Could not delete partial cache file at ";
        r2 = java.lang.String.valueOf(r12.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0585, code lost:
        if (r2.length() == 0) goto L_0x058c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x0587, code lost:
        r0 = r0.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x058c, code lost:
        r0 = new java.lang.String(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x0592, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzeo(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x0595, code lost:
        zza(r9, r12.getAbsolutePath(), r15, r3);
        zzewz.remove(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x05a2, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:0x040a, code lost:
        r25 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0133, code lost:
        r15 = "error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        com.google.android.gms.ads.internal.zzbv.zzls();
        r1 = ((java.lang.Integer) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcoy)).intValue();
        r3 = new java.net.URL(r9);
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x014f, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0152, code lost:
        if (r2 > 20) goto L_0x04fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0154, code lost:
        r5 = r3.openConnection();
        r5.setConnectTimeout(r1);
        r5.setReadTimeout(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0160, code lost:
        if ((r5 instanceof java.net.HttpURLConnection) == false) goto L_0x04f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0162, code lost:
        r5 = (java.net.HttpURLConnection) r5;
        r6 = new com.google.android.gms.internal.ads.zzbax();
        r6.zza(r5, (byte[]) null);
        r5.setInstanceFollowRedirects(false);
        r7 = r5.getResponseCode();
        r6.zza(r5, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0179, code lost:
        if ((r7 / 100) != 3) goto L_0x01f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r4 = r5.getHeaderField("Location");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0181, code lost:
        if (r4 == null) goto L_0x01e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0183, code lost:
        r6 = new java.net.URL(r3, r4);
        r3 = r6.getProtocol();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x018c, code lost:
        if (r3 == null) goto L_0x01db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0194, code lost:
        if (r3.equals("http") != false) goto L_0x01bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x019c, code lost:
        if (r3.equals("https") != false) goto L_0x01bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x019e, code lost:
        r1 = "Unsupported scheme: ";
        r2 = java.lang.String.valueOf(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01aa, code lost:
        if (r2.length() == 0) goto L_0x01b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01ac, code lost:
        r1 = r1.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01b1, code lost:
        r1 = new java.lang.String(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01ba, code lost:
        throw new java.io.IOException(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01bb, code lost:
        r3 = "Redirecting to ";
        r4 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01c5, code lost:
        if (r4.length() == 0) goto L_0x01cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01c7, code lost:
        r3 = r3.concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01cc, code lost:
        r3 = new java.lang.String(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01d2, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzdn(r3);
        r5.disconnect();
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01e2, code lost:
        throw new java.io.IOException("Protocol is null");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01ea, code lost:
        throw new java.io.IOException("Missing Location header in redirect");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01eb, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01ec, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01ed, code lost:
        r1 = r14;
        r10 = r10;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:211:0x04c4 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r10v3
  assigns: []
  uses: []
  mth insns count: 559
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x0514  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0524  */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x0548  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0587  */
    /* JADX WARNING: Removed duplicated region for block: B:257:0x058c  */
    /* JADX WARNING: Unknown variable types count: 17 */
    public final boolean zzex(String str) {
        int i;
        boolean z;
        File[] listFiles;
        String str2 = str;
        ? r10 = 0;
        if (this.zzexb == null) {
            zza(str2, null, "noCacheDir", null);
            return false;
        }
        do {
            if (this.zzexb == null) {
                i = 0;
            } else {
                i = 0;
                for (File name : this.zzexb.listFiles()) {
                    if (!name.getName().endsWith(".done")) {
                        i++;
                    }
                }
            }
            if (i > ((Integer) zzwu.zzpz().zzd(zzaan.zzcot)).intValue()) {
                if (this.zzexb != null) {
                    long j = Long.MAX_VALUE;
                    File file = null;
                    for (File file2 : this.zzexb.listFiles()) {
                        if (!file2.getName().endsWith(".done")) {
                            long lastModified = file2.lastModified();
                            if (lastModified < j) {
                                file = file2;
                                j = lastModified;
                            }
                        }
                    }
                    if (file != null) {
                        z = file.delete();
                        File zzc = zzc(file);
                        if (zzc.isFile()) {
                            z &= zzc.delete();
                            continue;
                        } else {
                            continue;
                        }
                    }
                }
                z = false;
                continue;
            } else {
                File file3 = new File(this.zzexb, zzey(str));
                File zzc2 = zzc(file3);
                if (!file3.isFile() || !zzc2.isFile()) {
                    String valueOf = String.valueOf(this.zzexb.getAbsolutePath());
                    String valueOf2 = String.valueOf(str);
                    Object concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                    synchronized (zzewz) {
                        if (zzewz.contains(concat)) {
                            String str3 = "Stream cache already in progress at ";
                            String valueOf3 = String.valueOf(str);
                            zzaxz.zzeo(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3));
                            zza(str2, file3.getAbsolutePath(), "inProgress", null);
                            return false;
                        }
                        zzewz.add(concat);
                    }
                } else {
                    int length = (int) file3.length();
                    String str4 = "Stream cache hit at ";
                    String valueOf4 = String.valueOf(str);
                    zzaxz.zzdn(valueOf4.length() != 0 ? str4.concat(valueOf4) : new String(str4));
                    zza(str2, file3.getAbsolutePath(), length);
                    return true;
                }
            }
        } while (z);
        zzaxz.zzeo("Unable to expire stream cache");
        zza(str2, null, "expireFailed", null);
        return false;
    }

    public final void abort() {
        this.zzexc = true;
    }

    private final File zzc(File file) {
        return new File(this.zzexb, String.valueOf(file.getName()).concat(".done"));
    }
}
