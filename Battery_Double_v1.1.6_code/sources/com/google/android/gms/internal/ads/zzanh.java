package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.IObjectWrapper.Stub;

public abstract class zzanh extends zzex implements zzang {
    public zzanh() {
        super("com.google.android.gms.ads.internal.mediation.client.rtb.IRtbAdapter");
    }

    public static zzang zzw(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IRtbAdapter");
        if (queryLocalInterface instanceof zzang) {
            return (zzang) queryLocalInterface;
        }
        return new zzani(iBinder);
    }

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.google.android.gms.internal.ads.zzank] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.google.android.gms.internal.ads.zzanj] */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: type inference failed for: r7v1 */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.google.android.gms.internal.ads.zzanj] */
    /* JADX WARNING: type inference failed for: r1v9, types: [com.google.android.gms.internal.ads.zzamz] */
    /* JADX WARNING: type inference failed for: r1v10, types: [com.google.android.gms.internal.ads.zzamy] */
    /* JADX WARNING: type inference failed for: r1v11 */
    /* JADX WARNING: type inference failed for: r6v5 */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.google.android.gms.internal.ads.zzamy] */
    /* JADX WARNING: type inference failed for: r1v17, types: [com.google.android.gms.internal.ads.zzanb] */
    /* JADX WARNING: type inference failed for: r1v18, types: [com.google.android.gms.internal.ads.zzana] */
    /* JADX WARNING: type inference failed for: r1v19 */
    /* JADX WARNING: type inference failed for: r6v8 */
    /* JADX WARNING: type inference failed for: r5v9, types: [com.google.android.gms.internal.ads.zzana] */
    /* JADX WARNING: type inference failed for: r1v23, types: [com.google.android.gms.internal.ads.zzanf] */
    /* JADX WARNING: type inference failed for: r1v24, types: [com.google.android.gms.internal.ads.zzane] */
    /* JADX WARNING: type inference failed for: r1v25 */
    /* JADX WARNING: type inference failed for: r6v11 */
    /* JADX WARNING: type inference failed for: r5v12, types: [com.google.android.gms.internal.ads.zzane] */
    /* JADX WARNING: type inference failed for: r1v30, types: [com.google.android.gms.internal.ads.zzand] */
    /* JADX WARNING: type inference failed for: r1v31, types: [com.google.android.gms.internal.ads.zzanc] */
    /* JADX WARNING: type inference failed for: r1v32 */
    /* JADX WARNING: type inference failed for: r6v14 */
    /* JADX WARNING: type inference failed for: r5v15, types: [com.google.android.gms.internal.ads.zzanc] */
    /* JADX WARNING: type inference failed for: r1v34 */
    /* JADX WARNING: type inference failed for: r1v35 */
    /* JADX WARNING: type inference failed for: r1v36 */
    /* JADX WARNING: type inference failed for: r1v37 */
    /* JADX WARNING: type inference failed for: r1v38 */
    /* JADX WARNING: type inference failed for: r1v39 */
    /* JADX WARNING: type inference failed for: r1v40 */
    /* JADX WARNING: type inference failed for: r1v41 */
    /* JADX WARNING: type inference failed for: r1v42 */
    /* JADX WARNING: type inference failed for: r1v43 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 21 */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ? r1 = 0;
        switch (i) {
            case 1:
                IObjectWrapper asInterface = Stub.asInterface(parcel.readStrongBinder());
                String readString = parcel.readString();
                Bundle bundle = (Bundle) zzey.zza(parcel, Bundle.CREATOR);
                Bundle bundle2 = (Bundle) zzey.zza(parcel, Bundle.CREATOR);
                zzwf zzwf = (zzwf) zzey.zza(parcel, zzwf.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder != null) {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.ISignalsCallback");
                    if (queryLocalInterface instanceof zzanj) {
                        r1 = (zzanj) queryLocalInterface;
                    } else {
                        r1 = new zzank(readStrongBinder);
                    }
                }
                zza(asInterface, readString, bundle, bundle2, zzwf, (zzanj) r1);
                parcel2.writeNoException();
                break;
            case 2:
                zzans zzvi = zzvi();
                parcel2.writeNoException();
                zzey.zzb(parcel2, zzvi);
                break;
            case 3:
                zzans zzvj = zzvj();
                parcel2.writeNoException();
                zzey.zzb(parcel2, zzvj);
                break;
            case 4:
                String readString2 = parcel.readString();
                String readString3 = parcel.readString();
                Bundle bundle3 = (Bundle) zzey.zza(parcel, Bundle.CREATOR);
                IObjectWrapper asInterface2 = Stub.asInterface(parcel.readStrongBinder());
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IBannerCallback");
                    if (queryLocalInterface2 instanceof zzamy) {
                        r1 = (zzamy) queryLocalInterface2;
                    } else {
                        r1 = new zzamz(readStrongBinder2);
                    }
                }
                ? r6 = r1;
                zza(readString2, readString3, bundle3, asInterface2, r6, zzaln.zzv(parcel.readStrongBinder()), (zzwf) zzey.zza(parcel, zzwf.CREATOR));
                parcel2.writeNoException();
                break;
            case 5:
                zzyp videoController = getVideoController();
                parcel2.writeNoException();
                zzey.zza(parcel2, (IInterface) videoController);
                break;
            case 6:
                String readString4 = parcel.readString();
                String readString5 = parcel.readString();
                Bundle bundle4 = (Bundle) zzey.zza(parcel, Bundle.CREATOR);
                IObjectWrapper asInterface3 = Stub.asInterface(parcel.readStrongBinder());
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IInterstitialCallback");
                    if (queryLocalInterface3 instanceof zzana) {
                        r1 = (zzana) queryLocalInterface3;
                    } else {
                        r1 = new zzanb(readStrongBinder3);
                    }
                }
                ? r62 = r1;
                zza(readString4, readString5, bundle4, asInterface3, (zzana) r62, zzaln.zzv(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 7:
                showInterstitial();
                parcel2.writeNoException();
                break;
            case 8:
                String readString6 = parcel.readString();
                String readString7 = parcel.readString();
                Bundle bundle5 = (Bundle) zzey.zza(parcel, Bundle.CREATOR);
                IObjectWrapper asInterface4 = Stub.asInterface(parcel.readStrongBinder());
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.IRewardedCallback");
                    if (queryLocalInterface4 instanceof zzane) {
                        r1 = (zzane) queryLocalInterface4;
                    } else {
                        r1 = new zzanf(readStrongBinder4);
                    }
                }
                ? r63 = r1;
                zza(readString6, readString7, bundle5, asInterface4, (zzane) r63, zzaln.zzv(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 9:
                zzvk();
                parcel2.writeNoException();
                break;
            case 10:
                zzn(Stub.asInterface(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 11:
                zza(parcel.createStringArray(), (Bundle[]) parcel.createTypedArray(Bundle.CREATOR));
                parcel2.writeNoException();
                break;
            case 12:
                String readString8 = parcel.readString();
                String readString9 = parcel.readString();
                Bundle bundle6 = (Bundle) zzey.zza(parcel, Bundle.CREATOR);
                IObjectWrapper asInterface5 = Stub.asInterface(parcel.readStrongBinder());
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.rtb.INativeCallback");
                    if (queryLocalInterface5 instanceof zzanc) {
                        r1 = (zzanc) queryLocalInterface5;
                    } else {
                        r1 = new zzand(readStrongBinder5);
                    }
                }
                ? r64 = r1;
                zza(readString8, readString9, bundle6, asInterface5, (zzanc) r64, zzaln.zzv(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            default:
                return false;
        }
        return true;
    }
}
