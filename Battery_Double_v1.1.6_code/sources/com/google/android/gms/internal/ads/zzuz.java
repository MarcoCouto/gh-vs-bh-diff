package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzuw.zza.C0028zza;

final class zzuz implements zzbri {
    static final zzbri zzccw = new zzuz();

    private zzuz() {
    }

    public final boolean zzcb(int i) {
        return C0028zza.zzca(i) != null;
    }
}
