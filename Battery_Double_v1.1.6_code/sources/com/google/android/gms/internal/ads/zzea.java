package com.google.android.gms.internal.ads;

import android.content.Context;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;

public final class zzea extends zzeu {
    private static final zzev<zzbv> zzuk = new zzev<>();
    private final Context zzuj;
    private zzbi zzul = null;

    public zzea(zzdl zzdl, String str, String str2, zzbl zzbl, int i, int i2, Context context, zzbi zzbi) {
        super(zzdl, str, str2, zzbl, i, 27);
        this.zzuj = context;
        this.zzul = zzbi;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0086  */
    public final void zzas() throws IllegalAccessException, InvocationTargetException {
        boolean z;
        zzbv zzbv;
        char c;
        boolean z2;
        AtomicReference zzp = zzuk.zzp(this.zzuj.getPackageName());
        synchronized (zzp) {
            zzbv zzbv2 = (zzbv) zzp.get();
            boolean z3 = false;
            if (zzbv2 != null && !zzds.zzo(zzbv2.zzdq) && !zzbv2.zzdq.equals("E")) {
                if (!zzbv2.zzdq.equals("0000000000000000000000000000000000000000000000000000000000000000")) {
                    z = false;
                    if (z) {
                        zzbi zzbi = this.zzul;
                        if (!zzds.zzo(null)) {
                            c = 4;
                        } else {
                            zzbi zzbi2 = this.zzul;
                            zzds.zzo(null);
                            if (Boolean.valueOf(false).booleanValue()) {
                                if (this.zzqo.zzai()) {
                                    if (((Boolean) zzwu.zzpz().zzd(zzaan.zzctt)).booleanValue()) {
                                        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzctu)).booleanValue()) {
                                            z2 = true;
                                            if (z2) {
                                                c = 3;
                                            }
                                        }
                                    }
                                }
                                z2 = false;
                                if (z2) {
                                }
                            }
                            c = 2;
                        }
                        Method method = this.zzuw;
                        Object[] objArr = new Object[3];
                        objArr[0] = this.zzuj;
                        if (c == 2) {
                            z3 = true;
                        }
                        objArr[1] = Boolean.valueOf(z3);
                        objArr[2] = zzwu.zzpz().zzd(zzaan.zzctn);
                        zzbv zzbv3 = new zzbv((String) method.invoke(null, objArr));
                        if (zzds.zzo(zzbv3.zzdq) || zzbv3.zzdq.equals("E")) {
                            switch (c) {
                                case 3:
                                    String zzat = zzat();
                                    if (!zzds.zzo(zzat)) {
                                        zzbv3.zzdq = zzat;
                                        break;
                                    }
                                    break;
                                case 4:
                                    zzbv3.zzdq = null.zzdq;
                                    break;
                            }
                        }
                        zzp.set(zzbv3);
                    }
                    zzbv = (zzbv) zzp.get();
                }
            }
            z = true;
            if (z) {
            }
            zzbv = (zzbv) zzp.get();
        }
        synchronized (this.zzun) {
            if (zzbv != null) {
                try {
                    this.zzun.zzdq = zzbv.zzdq;
                    this.zzun.zzev = Long.valueOf(zzbv.zzit);
                    this.zzun.zzds = zzbv.zzds;
                    this.zzun.zzdt = zzbv.zzdt;
                    this.zzun.zzdu = zzbv.zzdu;
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    private final String zzat() {
        try {
            if (this.zzqo.zzal() != null) {
                this.zzqo.zzal().get();
            }
            zzbl zzak = this.zzqo.zzak();
            if (!(zzak == null || zzak.zzdq == null)) {
                return zzak.zzdq;
            }
        } catch (InterruptedException | ExecutionException unused) {
        }
        return null;
    }
}
