package com.google.android.gms.internal.ads;

import java.io.PrintStream;
import java.io.PrintWriter;

public final class zzbpe {
    private static final zzbpf zzfkn;
    private static final int zzfko;

    static final class zza extends zzbpf {
        zza() {
        }

        public final void zze(Throwable th) {
            th.printStackTrace();
        }

        public final void zza(Throwable th, PrintWriter printWriter) {
            th.printStackTrace(printWriter);
        }
    }

    public static void zze(Throwable th) {
        zzfkn.zze(th);
    }

    public static void zza(Throwable th, PrintWriter printWriter) {
        zzfkn.zza(th, printWriter);
    }

    private static Integer zzake() {
        try {
            return (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
        } catch (Exception e) {
            System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
            e.printStackTrace(System.err);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0068  */
    static {
        zzbpf zzbpf;
        Integer num;
        int i = 1;
        try {
            num = zzake();
            if (num != null) {
                try {
                    if (num.intValue() >= 19) {
                        zzbpf = new zzbpj();
                        zzfkn = zzbpf;
                        if (num != null) {
                            i = num.intValue();
                        }
                        zzfko = i;
                    }
                } catch (Throwable th) {
                    th = th;
                    PrintStream printStream = System.err;
                    String name = zza.class.getName();
                    StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 132);
                    sb.append("An error has occured when initializing the try-with-resources desuguring strategy. The default strategy ");
                    sb.append(name);
                    sb.append("will be used. The error is: ");
                    printStream.println(sb.toString());
                    th.printStackTrace(System.err);
                    zzbpf = new zza();
                    zzfkn = zzbpf;
                    if (num != null) {
                    }
                    zzfko = i;
                }
            }
            if (!Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic")) {
                zzbpf = new zzbpi();
            } else {
                zzbpf = new zza();
            }
        } catch (Throwable th2) {
            th = th2;
            num = null;
            PrintStream printStream2 = System.err;
            String name2 = zza.class.getName();
            StringBuilder sb2 = new StringBuilder(String.valueOf(name2).length() + 132);
            sb2.append("An error has occured when initializing the try-with-resources desuguring strategy. The default strategy ");
            sb2.append(name2);
            sb2.append("will be used. The error is: ");
            printStream2.println(sb2.toString());
            th.printStackTrace(System.err);
            zzbpf = new zza();
            zzfkn = zzbpf;
            if (num != null) {
            }
            zzfko = i;
        }
        zzfkn = zzbpf;
        if (num != null) {
        }
        zzfko = i;
    }
}
