package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbvd.zzb.zzf.C0025zzb;

final class zzbvi implements zzbri {
    static final zzbri zzccw = new zzbvi();

    private zzbvi() {
    }

    public final boolean zzcb(int i) {
        return C0025zzb.zzgj(i) != null;
    }
}
