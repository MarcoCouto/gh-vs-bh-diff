package com.google.android.gms.internal.ads;

import android.os.SystemClock;
import com.smaato.sdk.video.vast.model.ErrorCode;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

public class zzaj implements zzm {
    private static final boolean DEBUG = zzaf.DEBUG;
    @Deprecated
    private final zzar zzbo;
    private final zzai zzbp;
    private final zzak zzbq;

    @Deprecated
    public zzaj(zzar zzar) {
        this(zzar, new zzak(4096));
    }

    @Deprecated
    private zzaj(zzar zzar, zzak zzak) {
        this.zzbo = zzar;
        this.zzbp = new zzah(zzar);
        this.zzbq = zzak;
    }

    public zzaj(zzai zzai) {
        this(zzai, new zzak(4096));
    }

    private zzaj(zzai zzai, zzak zzak) {
        this.zzbp = zzai;
        this.zzbo = zzai;
        this.zzbq = zzak;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01e1, code lost:
        if (r0 < 400) goto L_0x01ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01ed, code lost:
        throw new com.google.android.gms.internal.ads.zzg(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x01f0, code lost:
        if (r0 < 500) goto L_0x01fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01fb, code lost:
        throw new com.google.android.gms.internal.ads.zzac(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0201, code lost:
        throw new com.google.android.gms.internal.ads.zzac(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0202, code lost:
        zza("auth", r2, new com.google.android.gms.internal.ads.zza(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x020e, code lost:
        zza("network", r2, new com.google.android.gms.internal.ads.zzo());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x021f, code lost:
        throw new com.google.android.gms.internal.ads.zzq(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0220, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0221, code lost:
        r4 = "Bad URL ";
        r2 = java.lang.String.valueOf(r22.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0231, code lost:
        if (r2.length() != 0) goto L_0x0233;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0233, code lost:
        r2 = r4.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0238, code lost:
        r2 = new java.lang.String(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0240, code lost:
        throw new java.lang.RuntimeException(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0241, code lost:
        zza("socket", r2, new com.google.android.gms.internal.ads.zzad());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0118, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0119, code lost:
        r17 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0154, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0155, code lost:
        r13 = r5;
        r17 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0197, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0199, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x019a, code lost:
        r8 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x019b, code lost:
        r13 = r5;
        r17 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01a4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01a5, code lost:
        r17 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01a8, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01a9, code lost:
        r17 = r5;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01af, code lost:
        r0 = r10.getStatusCode();
        com.google.android.gms.internal.ads.zzaf.e("Unexpected response code %d for %s", java.lang.Integer.valueOf(r0), r22.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01c6, code lost:
        if (r13 != null) goto L_0x01c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01c8, code lost:
        r11 = new com.google.android.gms.internal.ads.zzp(r0, r13, false, android.os.SystemClock.elapsedRealtime() - r3, r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01d8, code lost:
        if (r0 == 401) goto L_0x0202;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0220 A[ExcHandler: MalformedURLException (r0v1 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:128:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x021a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01af  */
    public zzp zzc(zzr<?> zzr) throws zzae {
        zzaq zzaq;
        Map map;
        List<zzl> zzr2;
        byte[] bArr;
        zzr<?> zzr3 = zzr;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            List emptyList = Collections.emptyList();
            try {
                zzc zzg = zzr.zzg();
                if (zzg == null) {
                    map = Collections.emptyMap();
                } else {
                    Map hashMap = new HashMap();
                    if (zzg.zza != null) {
                        hashMap.put(HttpRequest.HEADER_IF_NONE_MATCH, zzg.zza);
                    }
                    if (zzg.zzc > 0) {
                        hashMap.put("If-Modified-Since", zzap.zzb(zzg.zzc));
                    }
                    map = hashMap;
                }
                zzaq = this.zzbp.zza(zzr3, map);
                int statusCode = zzaq.getStatusCode();
                zzr2 = zzaq.zzr();
                if (statusCode == 304) {
                    zzc zzg2 = zzr.zzg();
                    if (zzg2 == null) {
                        zzp zzp = new zzp((int) ErrorCode.INLINE_AD_DISPLAY_TIMEOUT_ERROR, (byte[]) null, true, SystemClock.elapsedRealtime() - elapsedRealtime, zzr2);
                        return zzp;
                    }
                    TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
                    if (!zzr2.isEmpty()) {
                        for (zzl name : zzr2) {
                            treeSet.add(name.getName());
                        }
                    }
                    ArrayList arrayList = new ArrayList(zzr2);
                    if (zzg2.zzg != null) {
                        if (!zzg2.zzg.isEmpty()) {
                            for (zzl zzl : zzg2.zzg) {
                                if (!treeSet.contains(zzl.getName())) {
                                    arrayList.add(zzl);
                                }
                            }
                        }
                    } else if (!zzg2.zzf.isEmpty()) {
                        for (Entry entry : zzg2.zzf.entrySet()) {
                            if (!treeSet.contains(entry.getKey())) {
                                arrayList.add(new zzl((String) entry.getKey(), (String) entry.getValue()));
                            }
                        }
                    }
                    zzp zzp2 = new zzp((int) ErrorCode.INLINE_AD_DISPLAY_TIMEOUT_ERROR, zzg2.data, true, SystemClock.elapsedRealtime() - elapsedRealtime, (List<zzl>) arrayList);
                    return zzp2;
                }
                InputStream content = zzaq.getContent();
                if (content != null) {
                    bArr = zza(content, zzaq.getContentLength());
                } else {
                    bArr = new byte[0];
                }
                byte[] bArr2 = bArr;
                long elapsedRealtime2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                if (DEBUG || elapsedRealtime2 > 3000) {
                    String str = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]";
                    Object[] objArr = new Object[5];
                    objArr[0] = zzr3;
                    objArr[1] = Long.valueOf(elapsedRealtime2);
                    objArr[2] = bArr2 != null ? Integer.valueOf(bArr2.length) : "null";
                    objArr[3] = Integer.valueOf(statusCode);
                    objArr[4] = Integer.valueOf(zzr.zzk().zzd());
                    zzaf.d(str, objArr);
                }
                if (statusCode < 200 || statusCode > 299) {
                    List list = zzr2;
                } else {
                    List list2 = zzr2;
                    r11 = r11;
                    zzp zzp3 = new zzp(statusCode, bArr2, false, SystemClock.elapsedRealtime() - elapsedRealtime, list2);
                    return zzp3;
                }
            } catch (SocketTimeoutException unused) {
            } catch (MalformedURLException e) {
            } catch (IOException e2) {
                e = e2;
                List list3 = zzr2;
                byte[] bArr3 = null;
                if (zzaq != null) {
                }
            }
        }
        List list4 = zzr2;
        throw new IOException();
    }

    private static void zza(String str, zzr<?> zzr, zzae zzae) throws zzae {
        zzab zzk = zzr.zzk();
        int zzj = zzr.zzj();
        try {
            zzk.zza(zzae);
            zzr.zzb(String.format("%s-retry [timeout=%s]", new Object[]{str, Integer.valueOf(zzj)}));
        } catch (zzae e) {
            zzr.zzb(String.format("%s-timeout-giveup [timeout=%s]", new Object[]{str, Integer.valueOf(zzj)}));
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046 A[SYNTHETIC, Splitter:B:23:0x0046] */
    private final byte[] zza(InputStream inputStream, int i) throws IOException, zzac {
        zzav zzav = new zzav(this.zzbq, i);
        byte[] bArr = null;
        if (inputStream != null) {
            try {
                byte[] zzb = this.zzbq.zzb(1024);
                while (true) {
                    try {
                        int read = inputStream.read(zzb);
                        if (read == -1) {
                            break;
                        }
                        zzav.write(zzb, 0, read);
                    } catch (Throwable th) {
                        byte[] bArr2 = zzb;
                        th = th;
                        bArr = bArr2;
                        if (inputStream != null) {
                        }
                        this.zzbq.zza(bArr);
                        zzav.close();
                        throw th;
                    }
                }
                byte[] byteArray = zzav.toByteArray();
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused) {
                        zzaf.v("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.zzbq.zza(zzb);
                zzav.close();
                return byteArray;
            } catch (Throwable th2) {
                th = th2;
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException unused2) {
                        zzaf.v("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.zzbq.zza(bArr);
                zzav.close();
                throw th;
            }
        } else {
            throw new zzac();
        }
    }
}
