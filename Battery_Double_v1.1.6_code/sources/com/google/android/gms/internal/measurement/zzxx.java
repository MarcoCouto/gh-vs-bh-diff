package com.google.android.gms.internal.measurement;

import com.github.mikephil.charting.utils.Utils;

public enum zzxx {
    INT(Integer.valueOf(0)),
    LONG(Long.valueOf(0)),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(Utils.DOUBLE_EPSILON)),
    BOOLEAN(Boolean.valueOf(false)),
    STRING(""),
    BYTE_STRING(zzte.zzbtq),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzbzq;

    private zzxx(Object obj) {
        this.zzbzq = obj;
    }
}
