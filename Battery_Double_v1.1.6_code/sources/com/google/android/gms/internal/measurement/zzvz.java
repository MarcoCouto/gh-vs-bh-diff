package com.google.android.gms.internal.measurement;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.measurement.zzuo.zze;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import sun.misc.Unsafe;

final class zzvz<T> implements zzwl<T> {
    private static final int[] zzcao = new int[0];
    private static final Unsafe zzcap = zzxj.zzyq();
    private final int[] zzcaq;
    private final Object[] zzcar;
    private final int zzcas;
    private final int zzcat;
    private final zzvv zzcau;
    private final boolean zzcav;
    private final boolean zzcaw;
    private final boolean zzcax;
    private final boolean zzcay;
    private final int[] zzcaz;
    private final int zzcba;
    private final int zzcbb;
    private final zzwc zzcbc;
    private final zzvf zzcbd;
    private final zzxd<?, ?> zzcbe;
    private final zzuc<?> zzcbf;
    private final zzvq zzcbg;

    private zzvz(int[] iArr, Object[] objArr, int i, int i2, zzvv zzvv, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzwc zzwc, zzvf zzvf, zzxd<?, ?> zzxd, zzuc<?> zzuc, zzvq zzvq) {
        this.zzcaq = iArr;
        this.zzcar = objArr;
        this.zzcas = i;
        this.zzcat = i2;
        this.zzcaw = zzvv instanceof zzuo;
        this.zzcax = z;
        this.zzcav = zzuc != null && zzuc.zze(zzvv);
        this.zzcay = false;
        this.zzcaz = iArr2;
        this.zzcba = i3;
        this.zzcbb = i4;
        this.zzcbc = zzwc;
        this.zzcbd = zzvf;
        this.zzcbe = zzxd;
        this.zzcbf = zzuc;
        this.zzcau = zzvv;
        this.zzcbg = zzvq;
    }

    private static boolean zzbv(int i) {
        return (i & 536870912) != 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:170:0x037f  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x03ce  */
    static <T> zzvz<T> zza(Class<T> cls, zzvt zzvt, zzwc zzwc, zzvf zzvf, zzxd<?, ?> zzxd, zzuc<?> zzuc, zzvq zzvq) {
        int i;
        int i2;
        int[] iArr;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        char c;
        int i8;
        int i9;
        Object[] objArr;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        Field field;
        int i16;
        char charAt;
        int i17;
        int i18;
        int i19;
        char c2;
        Field zza;
        Field zza2;
        int i20;
        char charAt2;
        int i21;
        char charAt3;
        int i22;
        char charAt4;
        int i23;
        int i24;
        int i25;
        int i26;
        int i27;
        int i28;
        char charAt5;
        int i29;
        char charAt6;
        int i30;
        char charAt7;
        int i31;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        zzvt zzvt2 = zzvt;
        if (zzvt2 instanceof zzwj) {
            zzwj zzwj = (zzwj) zzvt2;
            char c3 = 0;
            boolean z = zzwj.zzxm() == zze.zzbyt;
            String zzxv = zzwj.zzxv();
            int length = zzxv.length();
            char charAt15 = zzxv.charAt(0);
            if (charAt15 >= 55296) {
                char c4 = charAt15 & 8191;
                int i32 = 1;
                int i33 = 13;
                while (true) {
                    i = i32 + 1;
                    charAt14 = zzxv.charAt(i32);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c4 |= (charAt14 & 8191) << i33;
                    i33 += 13;
                    i32 = i;
                }
                charAt15 = (charAt14 << i33) | c4;
            } else {
                i = 1;
            }
            int i34 = i + 1;
            char charAt16 = zzxv.charAt(i);
            if (charAt16 >= 55296) {
                char c5 = charAt16 & 8191;
                int i35 = 13;
                while (true) {
                    i2 = i34 + 1;
                    charAt13 = zzxv.charAt(i34);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c5 |= (charAt13 & 8191) << i35;
                    i35 += 13;
                    i34 = i2;
                }
                charAt16 = c5 | (charAt13 << i35);
            } else {
                i2 = i34;
            }
            if (charAt16 == 0) {
                iArr = zzcao;
                c = 0;
                i7 = 0;
                i6 = 0;
                i5 = 0;
                i4 = 0;
                i3 = 0;
            } else {
                int i36 = i2 + 1;
                i7 = zzxv.charAt(i2);
                if (i7 >= 55296) {
                    int i37 = i7 & 8191;
                    int i38 = 13;
                    while (true) {
                        i23 = i36 + 1;
                        charAt12 = zzxv.charAt(i36);
                        if (charAt12 < 55296) {
                            break;
                        }
                        i37 |= (charAt12 & 8191) << i38;
                        i38 += 13;
                        i36 = i23;
                    }
                    i7 = (charAt12 << i38) | i37;
                } else {
                    i23 = i36;
                }
                int i39 = i23 + 1;
                char charAt17 = zzxv.charAt(i23);
                if (charAt17 >= 55296) {
                    char c6 = charAt17 & 8191;
                    int i40 = 13;
                    while (true) {
                        i24 = i39 + 1;
                        charAt11 = zzxv.charAt(i39);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c6 |= (charAt11 & 8191) << i40;
                        i40 += 13;
                        i39 = i24;
                    }
                    charAt17 = c6 | (charAt11 << i40);
                } else {
                    i24 = i39;
                }
                int i41 = i24 + 1;
                int charAt18 = zzxv.charAt(i24);
                if (charAt18 >= 55296) {
                    int i42 = charAt18 & 8191;
                    int i43 = 13;
                    while (true) {
                        i25 = i41 + 1;
                        charAt10 = zzxv.charAt(i41);
                        if (charAt10 < 55296) {
                            break;
                        }
                        i42 |= (charAt10 & 8191) << i43;
                        i43 += 13;
                        i41 = i25;
                    }
                    charAt18 = (charAt10 << i43) | i42;
                } else {
                    i25 = i41;
                }
                int i44 = i25 + 1;
                i5 = zzxv.charAt(i25);
                if (i5 >= 55296) {
                    int i45 = i5 & 8191;
                    int i46 = 13;
                    while (true) {
                        i26 = i44 + 1;
                        charAt9 = zzxv.charAt(i44);
                        if (charAt9 < 55296) {
                            break;
                        }
                        i45 |= (charAt9 & 8191) << i46;
                        i46 += 13;
                        i44 = i26;
                    }
                    i5 = (charAt9 << i46) | i45;
                } else {
                    i26 = i44;
                }
                int i47 = i26 + 1;
                i4 = zzxv.charAt(i26);
                if (i4 >= 55296) {
                    int i48 = i4 & 8191;
                    int i49 = 13;
                    while (true) {
                        i31 = i47 + 1;
                        charAt8 = zzxv.charAt(i47);
                        if (charAt8 < 55296) {
                            break;
                        }
                        i48 |= (charAt8 & 8191) << i49;
                        i49 += 13;
                        i47 = i31;
                    }
                    i4 = (charAt8 << i49) | i48;
                    i47 = i31;
                }
                int i50 = i47 + 1;
                c = zzxv.charAt(i47);
                if (c >= 55296) {
                    char c7 = c & 8191;
                    int i51 = 13;
                    while (true) {
                        i30 = i50 + 1;
                        charAt7 = zzxv.charAt(i50);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c7 |= (charAt7 & 8191) << i51;
                        i51 += 13;
                        i50 = i30;
                    }
                    c = c7 | (charAt7 << i51);
                    i50 = i30;
                }
                int i52 = i50 + 1;
                char charAt19 = zzxv.charAt(i50);
                if (charAt19 >= 55296) {
                    int i53 = 13;
                    int i54 = i52;
                    int i55 = charAt19 & 8191;
                    int i56 = i54;
                    while (true) {
                        i29 = i56 + 1;
                        charAt6 = zzxv.charAt(i56);
                        if (charAt6 < 55296) {
                            break;
                        }
                        i55 |= (charAt6 & 8191) << i53;
                        i53 += 13;
                        i56 = i29;
                    }
                    charAt19 = i55 | (charAt6 << i53);
                    i27 = i29;
                } else {
                    i27 = i52;
                }
                int i57 = i27 + 1;
                c3 = zzxv.charAt(i27);
                if (c3 >= 55296) {
                    int i58 = 13;
                    int i59 = i57;
                    int i60 = c3 & 8191;
                    int i61 = i59;
                    while (true) {
                        i28 = i61 + 1;
                        charAt5 = zzxv.charAt(i61);
                        if (charAt5 < 55296) {
                            break;
                        }
                        i60 |= (charAt5 & 8191) << i58;
                        i58 += 13;
                        i61 = i28;
                    }
                    c3 = i60 | (charAt5 << i58);
                    i57 = i28;
                }
                int[] iArr2 = new int[(c3 + c + charAt19)];
                i3 = (i7 << 1) + charAt17;
                i6 = charAt18;
                i2 = i57;
                iArr = iArr2;
            }
            Unsafe unsafe = zzcap;
            Object[] zzxw = zzwj.zzxw();
            Class cls2 = zzwj.zzxo().getClass();
            int i62 = i2;
            int[] iArr3 = new int[(i4 * 3)];
            Object[] objArr2 = new Object[(i4 << 1)];
            int i63 = c3 + c;
            int i64 = i3;
            int i65 = i63;
            int i66 = i62;
            int i67 = 0;
            int i68 = 0;
            int i69 = c3;
            while (i66 < length) {
                int i70 = i66 + 1;
                int charAt20 = zzxv.charAt(i66);
                char c8 = 55296;
                if (charAt20 >= 55296) {
                    int i71 = 13;
                    int i72 = i70;
                    int i73 = charAt20 & 8191;
                    int i74 = i72;
                    while (true) {
                        i22 = i74 + 1;
                        charAt4 = zzxv.charAt(i74);
                        if (charAt4 < c8) {
                            break;
                        }
                        i73 |= (charAt4 & 8191) << i71;
                        i71 += 13;
                        i74 = i22;
                        c8 = 55296;
                    }
                    charAt20 = i73 | (charAt4 << i71);
                    i8 = i22;
                } else {
                    i8 = i70;
                }
                int i75 = i8 + 1;
                char charAt21 = zzxv.charAt(i8);
                int i76 = length;
                char c9 = 55296;
                if (charAt21 >= 55296) {
                    int i77 = 13;
                    int i78 = i75;
                    int i79 = charAt21 & 8191;
                    int i80 = i78;
                    while (true) {
                        i21 = i80 + 1;
                        charAt3 = zzxv.charAt(i80);
                        if (charAt3 < c9) {
                            break;
                        }
                        i79 |= (charAt3 & 8191) << i77;
                        i77 += 13;
                        i80 = i21;
                        c9 = 55296;
                    }
                    charAt21 = i79 | (charAt3 << i77);
                    i9 = i21;
                } else {
                    i9 = i75;
                }
                int i81 = c3;
                char c10 = charAt21 & 255;
                boolean z2 = z;
                if ((charAt21 & 1024) != 0) {
                    int i82 = i67 + 1;
                    iArr[i67] = i68;
                    i67 = i82;
                }
                int i83 = i67;
                if (c10 >= '3') {
                    int i84 = i9 + 1;
                    char charAt22 = zzxv.charAt(i9);
                    char c11 = 55296;
                    if (charAt22 >= 55296) {
                        char c12 = charAt22 & 8191;
                        int i85 = 13;
                        while (true) {
                            i20 = i84 + 1;
                            charAt2 = zzxv.charAt(i84);
                            if (charAt2 < c11) {
                                break;
                            }
                            c12 |= (charAt2 & 8191) << i85;
                            i85 += 13;
                            i84 = i20;
                            c11 = 55296;
                        }
                        charAt22 = c12 | (charAt2 << i85);
                        i84 = i20;
                    }
                    int i86 = c10 - '3';
                    int i87 = i84;
                    if (i86 == 9 || i86 == 17) {
                        c2 = 1;
                        i19 = i64 + 1;
                        objArr2[((i68 / 3) << 1) + 1] = zzxw[i64];
                    } else {
                        if (i86 == 12 && (charAt15 & 1) == 1) {
                            int i88 = i64 + 1;
                            objArr2[((i68 / 3) << 1) + 1] = zzxw[i64];
                            i19 = i88;
                        } else {
                            i19 = i64;
                        }
                        c2 = 1;
                    }
                    int i89 = charAt22 << c2;
                    Object obj = zzxw[i89];
                    if (obj instanceof Field) {
                        zza = (Field) obj;
                    } else {
                        zza = zza(cls2, (String) obj);
                        zzxw[i89] = zza;
                    }
                    i11 = i6;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(zza);
                    int i90 = i89 + 1;
                    Object obj2 = zzxw[i90];
                    if (obj2 instanceof Field) {
                        zza2 = (Field) obj2;
                    } else {
                        zza2 = zza(cls2, (String) obj2);
                        zzxw[i90] = zza2;
                    }
                    i10 = i5;
                    objArr = objArr2;
                    i64 = i19;
                    i13 = i87;
                    i14 = objectFieldOffset;
                    i12 = (int) unsafe.objectFieldOffset(zza2);
                    i15 = 0;
                } else {
                    i11 = i6;
                    int i91 = i64 + 1;
                    Field zza3 = zza(cls2, (String) zzxw[i64]);
                    if (c10 == 9 || c10 == 17) {
                        i17 = i5;
                        objArr2[((i68 / 3) << 1) + 1] = zza3.getType();
                    } else {
                        if (c10 == 27 || c10 == '1') {
                            i17 = i5;
                            i18 = i91 + 1;
                            objArr2[((i68 / 3) << 1) + 1] = zzxw[i91];
                        } else if (c10 == 12 || c10 == 30 || c10 == ',') {
                            i17 = i5;
                            if ((charAt15 & 1) == 1) {
                                i18 = i91 + 1;
                                objArr2[((i68 / 3) << 1) + 1] = zzxw[i91];
                            }
                        } else if (c10 == '2') {
                            int i92 = i69 + 1;
                            iArr[i69] = i68;
                            int i93 = (i68 / 3) << 1;
                            int i94 = i91 + 1;
                            objArr2[i93] = zzxw[i91];
                            if ((charAt21 & 2048) != 0) {
                                i91 = i94 + 1;
                                objArr2[i93 + 1] = zzxw[i94];
                                i10 = i5;
                                objArr = objArr2;
                            } else {
                                i10 = i5;
                                objArr = objArr2;
                                i91 = i94;
                            }
                            i69 = i92;
                            i14 = (int) unsafe.objectFieldOffset(zza3);
                            if ((charAt15 & 1) == 1 || c10 > 17) {
                                i13 = i9;
                                i15 = 0;
                                i12 = 0;
                            } else {
                                i13 = i9 + 1;
                                char charAt23 = zzxv.charAt(i9);
                                if (charAt23 >= 55296) {
                                    char c13 = charAt23 & 8191;
                                    int i95 = 13;
                                    while (true) {
                                        i16 = i13 + 1;
                                        charAt = zzxv.charAt(i13);
                                        if (charAt < 55296) {
                                            break;
                                        }
                                        c13 |= (charAt & 8191) << i95;
                                        i95 += 13;
                                        i13 = i16;
                                    }
                                    charAt23 = c13 | (charAt << i95);
                                    i13 = i16;
                                }
                                int i96 = (i7 << 1) + (charAt23 / ' ');
                                Object obj3 = zzxw[i96];
                                if (obj3 instanceof Field) {
                                    field = (Field) obj3;
                                } else {
                                    field = zza(cls2, (String) obj3);
                                    zzxw[i96] = field;
                                }
                                i12 = (int) unsafe.objectFieldOffset(field);
                                i15 = charAt23 % ' ';
                            }
                            if (c10 >= 18 || c10 > '1') {
                                i64 = i91;
                            } else {
                                int i97 = i65 + 1;
                                iArr[i65] = i14;
                                i64 = i91;
                                i65 = i97;
                            }
                        } else {
                            i17 = i5;
                        }
                        objArr = objArr2;
                        i91 = i18;
                        i14 = (int) unsafe.objectFieldOffset(zza3);
                        if ((charAt15 & 1) == 1) {
                        }
                        i13 = i9;
                        i15 = 0;
                        i12 = 0;
                        if (c10 >= 18) {
                        }
                        i64 = i91;
                    }
                    objArr = objArr2;
                    i14 = (int) unsafe.objectFieldOffset(zza3);
                    if ((charAt15 & 1) == 1) {
                    }
                    i13 = i9;
                    i15 = 0;
                    i12 = 0;
                    if (c10 >= 18) {
                    }
                    i64 = i91;
                }
                int i98 = i68 + 1;
                iArr3[i68] = charAt20;
                int i99 = i98 + 1;
                iArr3[i98] = (c10 << 20) | ((charAt21 & 256) != 0 ? 268435456 : 0) | ((charAt21 & 512) != 0 ? 536870912 : 0) | i14;
                i68 = i99 + 1;
                iArr3[i99] = (i15 << 20) | i12;
                i66 = i13;
                length = i76;
                c3 = i81;
                z = z2;
                i67 = i83;
                i6 = i11;
                i5 = i10;
                objArr2 = objArr;
            }
            boolean z3 = z;
            zzvz zzvz = new zzvz(iArr3, objArr2, i6, i5, zzwj.zzxo(), z, false, iArr, c3, i63, zzwc, zzvf, zzxd, zzuc, zzvq);
            return zzvz;
        }
        ((zzwy) zzvt2).zzxm();
        throw new NoSuchMethodError();
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    public final T newInstance() {
        return this.zzcbc.newInstance(this.zzcau);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.measurement.zzwn.zze(com.google.android.gms.internal.measurement.zzxj.zzp(r10, r6), com.google.android.gms.internal.measurement.zzxj.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.measurement.zzwn.zze(com.google.android.gms.internal.measurement.zzxj.zzp(r10, r6), com.google.android.gms.internal.measurement.zzxj.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.measurement.zzwn.zze(com.google.android.gms.internal.measurement.zzxj.zzp(r10, r6), com.google.android.gms.internal.measurement.zzxj.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.measurement.zzwn.zze(com.google.android.gms.internal.measurement.zzxj.zzp(r10, r6), com.google.android.gms.internal.measurement.zzxj.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzm(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzm(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.measurement.zzxj.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzxj.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.zzxj.zzn(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.zzxj.zzn(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.zzxj.zzo(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.zzxj.zzo(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01c1, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.measurement.zzwn.zze(com.google.android.gms.internal.measurement.zzxj.zzp(r10, r6), com.google.android.gms.internal.measurement.zzxj.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    public final boolean equals(T t, T t2) {
        int length = this.zzcaq.length;
        int i = 0;
        while (true) {
            boolean z = true;
            if (i < length) {
                int zzbt = zzbt(i);
                long j = (long) (zzbt & 1048575);
                switch ((zzbt & 267386880) >>> 20) {
                    case 0:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 1:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 2:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 3:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 4:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 5:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 6:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 7:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 8:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 9:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 10:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 11:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 12:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 13:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 14:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 15:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 16:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 17:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        z = zzwn.zze(zzxj.zzp(t, j), zzxj.zzp(t2, j));
                        break;
                    case 50:
                        z = zzwn.zze(zzxj.zzp(t, j), zzxj.zzp(t2, j));
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                        long zzbu = (long) (zzbu(i) & 1048575);
                        if (zzxj.zzk(t, zzbu) == zzxj.zzk(t2, zzbu)) {
                            break;
                        }
                }
                if (!z) {
                    return false;
                }
                i += 3;
            } else if (!this.zzcbe.zzal(t).equals(this.zzcbe.zzal(t2))) {
                return false;
            } else {
                if (this.zzcav) {
                    return this.zzcbf.zzw(t).equals(this.zzcbf.zzw(t2));
                }
                return true;
            }
        }
    }

    public final int hashCode(T t) {
        int length = this.zzcaq.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2 += 3) {
            int zzbt = zzbt(i2);
            int i3 = this.zzcaq[i2];
            long j = (long) (1048575 & zzbt);
            int i4 = 37;
            switch ((zzbt & 267386880) >>> 20) {
                case 0:
                    i = (i * 53) + zzuq.zzbd(Double.doubleToLongBits(zzxj.zzo(t, j)));
                    break;
                case 1:
                    i = (i * 53) + Float.floatToIntBits(zzxj.zzn(t, j));
                    break;
                case 2:
                    i = (i * 53) + zzuq.zzbd(zzxj.zzl(t, j));
                    break;
                case 3:
                    i = (i * 53) + zzuq.zzbd(zzxj.zzl(t, j));
                    break;
                case 4:
                    i = (i * 53) + zzxj.zzk(t, j);
                    break;
                case 5:
                    i = (i * 53) + zzuq.zzbd(zzxj.zzl(t, j));
                    break;
                case 6:
                    i = (i * 53) + zzxj.zzk(t, j);
                    break;
                case 7:
                    i = (i * 53) + zzuq.zzu(zzxj.zzm(t, j));
                    break;
                case 8:
                    i = (i * 53) + ((String) zzxj.zzp(t, j)).hashCode();
                    break;
                case 9:
                    Object zzp = zzxj.zzp(t, j);
                    if (zzp != null) {
                        i4 = zzp.hashCode();
                    }
                    i = (i * 53) + i4;
                    break;
                case 10:
                    i = (i * 53) + zzxj.zzp(t, j).hashCode();
                    break;
                case 11:
                    i = (i * 53) + zzxj.zzk(t, j);
                    break;
                case 12:
                    i = (i * 53) + zzxj.zzk(t, j);
                    break;
                case 13:
                    i = (i * 53) + zzxj.zzk(t, j);
                    break;
                case 14:
                    i = (i * 53) + zzuq.zzbd(zzxj.zzl(t, j));
                    break;
                case 15:
                    i = (i * 53) + zzxj.zzk(t, j);
                    break;
                case 16:
                    i = (i * 53) + zzuq.zzbd(zzxj.zzl(t, j));
                    break;
                case 17:
                    Object zzp2 = zzxj.zzp(t, j);
                    if (zzp2 != null) {
                        i4 = zzp2.hashCode();
                    }
                    i = (i * 53) + i4;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i = (i * 53) + zzxj.zzp(t, j).hashCode();
                    break;
                case 50:
                    i = (i * 53) + zzxj.zzp(t, j).hashCode();
                    break;
                case 51:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzuq.zzbd(Double.doubleToLongBits(zzf(t, j)));
                        break;
                    }
                case 52:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Float.floatToIntBits(zzg(t, j));
                        break;
                    }
                case 53:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzuq.zzbd(zzi(t, j));
                        break;
                    }
                case 54:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzuq.zzbd(zzi(t, j));
                        break;
                    }
                case 55:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 56:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzuq.zzbd(zzi(t, j));
                        break;
                    }
                case 57:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 58:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzuq.zzu(zzj(t, j));
                        break;
                    }
                case 59:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + ((String) zzxj.zzp(t, j)).hashCode();
                        break;
                    }
                case 60:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzxj.zzp(t, j).hashCode();
                        break;
                    }
                case 61:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzxj.zzp(t, j).hashCode();
                        break;
                    }
                case 62:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 63:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 64:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 65:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzuq.zzbd(zzi(t, j));
                        break;
                    }
                case 66:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 67:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzuq.zzbd(zzi(t, j));
                        break;
                    }
                case 68:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzxj.zzp(t, j).hashCode();
                        break;
                    }
            }
        }
        int hashCode = (i * 53) + this.zzcbe.zzal(t).hashCode();
        return this.zzcav ? (hashCode * 53) + this.zzcbf.zzw(t).hashCode() : hashCode;
    }

    public final void zzd(T t, T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.zzcaq.length; i += 3) {
                int zzbt = zzbt(i);
                long j = (long) (1048575 & zzbt);
                int i2 = this.zzcaq[i];
                switch ((zzbt & 267386880) >>> 20) {
                    case 0:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzo(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 1:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzn(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 2:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzl(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 3:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzl(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 4:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zzb((Object) t, j, zzxj.zzk(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 5:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzl(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 6:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zzb((Object) t, j, zzxj.zzk(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 7:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzm(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 8:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzp(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 9:
                        zza(t, t2, i);
                        break;
                    case 10:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzp(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 11:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zzb((Object) t, j, zzxj.zzk(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 12:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zzb((Object) t, j, zzxj.zzk(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 13:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zzb((Object) t, j, zzxj.zzk(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 14:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzl(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 15:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zzb((Object) t, j, zzxj.zzk(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 16:
                        if (!zzb(t2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzl(t2, j));
                            zzc(t, i);
                            break;
                        }
                    case 17:
                        zza(t, t2, i);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.zzcbd.zza(t, t2, j);
                        break;
                    case 50:
                        zzwn.zza(this.zzcbg, t, t2, j);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzp(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 60:
                        zzb(t, t2, i);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzxj.zza((Object) t, j, zzxj.zzp(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 68:
                        zzb(t, t2, i);
                        break;
                }
            }
            if (!this.zzcax) {
                zzwn.zza(this.zzcbe, t, t2);
                if (this.zzcav) {
                    zzwn.zza(this.zzcbf, t, t2);
                    return;
                }
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    private final void zza(T t, T t2, int i) {
        long zzbt = (long) (zzbt(i) & 1048575);
        if (zzb(t2, i)) {
            Object zzp = zzxj.zzp(t, zzbt);
            Object zzp2 = zzxj.zzp(t2, zzbt);
            if (zzp == null || zzp2 == null) {
                if (zzp2 != null) {
                    zzxj.zza((Object) t, zzbt, zzp2);
                    zzc(t, i);
                }
                return;
            }
            zzxj.zza((Object) t, zzbt, zzuq.zzb(zzp, zzp2));
            zzc(t, i);
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzbt = zzbt(i);
        int i2 = this.zzcaq[i];
        long j = (long) (zzbt & 1048575);
        if (zza(t2, i2, i)) {
            Object zzp = zzxj.zzp(t, j);
            Object zzp2 = zzxj.zzp(t2, j);
            if (zzp == null || zzp2 == null) {
                if (zzp2 != null) {
                    zzxj.zza((Object) t, j, zzp2);
                    zzb(t, i2, i);
                }
                return;
            }
            zzxj.zza((Object) t, j, zzuq.zzb(zzp, zzp2));
            zzb(t, i2, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:412:0x09cb, code lost:
        r18 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:472:0x0aef, code lost:
        r3 = r3 + 3;
        r9 = r18;
     */
    public final int zzai(T t) {
        int i;
        int i2;
        long j;
        T t2 = t;
        int i3 = 267386880;
        if (this.zzcax) {
            Unsafe unsafe = zzcap;
            int i4 = 0;
            int i5 = 0;
            while (i4 < this.zzcaq.length) {
                int zzbt = zzbt(i4);
                int i6 = (zzbt & i3) >>> 20;
                int i7 = this.zzcaq[i4];
                long j2 = (long) (zzbt & 1048575);
                int i8 = (i6 < zzui.DOUBLE_LIST_PACKED.id() || i6 > zzui.SINT64_LIST_PACKED.id()) ? 0 : this.zzcaq[i4 + 2] & 1048575;
                switch (i6) {
                    case 0:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzb(i7, (double) Utils.DOUBLE_EPSILON);
                            break;
                        }
                    case 1:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzb(i7, 0.0f);
                            break;
                        }
                    case 2:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzd(i7, zzxj.zzl(t2, j2));
                            break;
                        }
                    case 3:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zze(i7, zzxj.zzl(t2, j2));
                            break;
                        }
                    case 4:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzh(i7, zzxj.zzk(t2, j2));
                            break;
                        }
                    case 5:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzg(i7, 0);
                            break;
                        }
                    case 6:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzk(i7, 0);
                            break;
                        }
                    case 7:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzc(i7, true);
                            break;
                        }
                    case 8:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            Object zzp = zzxj.zzp(t2, j2);
                            if (!(zzp instanceof zzte)) {
                                i5 += zztv.zzc(i7, (String) zzp);
                                break;
                            } else {
                                i5 += zztv.zzc(i7, (zzte) zzp);
                                break;
                            }
                        }
                    case 9:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zzwn.zzc(i7, zzxj.zzp(t2, j2), zzbq(i4));
                            break;
                        }
                    case 10:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzc(i7, (zzte) zzxj.zzp(t2, j2));
                            break;
                        }
                    case 11:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzi(i7, zzxj.zzk(t2, j2));
                            break;
                        }
                    case 12:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzm(i7, zzxj.zzk(t2, j2));
                            break;
                        }
                    case 13:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzl(i7, 0);
                            break;
                        }
                    case 14:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzh(i7, 0);
                            break;
                        }
                    case 15:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzj(i7, zzxj.zzk(t2, j2));
                            break;
                        }
                    case 16:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzf(i7, zzxj.zzl(t2, j2));
                            break;
                        }
                    case 17:
                        if (!zzb(t2, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzc(i7, (zzvv) zzxj.zzp(t2, j2), zzbq(i4));
                            break;
                        }
                    case 18:
                        i5 += zzwn.zzw(i7, zze(t2, j2), false);
                        break;
                    case 19:
                        i5 += zzwn.zzv(i7, zze(t2, j2), false);
                        break;
                    case 20:
                        i5 += zzwn.zzo(i7, zze(t2, j2), false);
                        break;
                    case 21:
                        i5 += zzwn.zzp(i7, zze(t2, j2), false);
                        break;
                    case 22:
                        i5 += zzwn.zzs(i7, zze(t2, j2), false);
                        break;
                    case 23:
                        i5 += zzwn.zzw(i7, zze(t2, j2), false);
                        break;
                    case 24:
                        i5 += zzwn.zzv(i7, zze(t2, j2), false);
                        break;
                    case 25:
                        i5 += zzwn.zzx(i7, zze(t2, j2), false);
                        break;
                    case 26:
                        i5 += zzwn.zzc(i7, zze(t2, j2));
                        break;
                    case 27:
                        i5 += zzwn.zzc(i7, zze(t2, j2), zzbq(i4));
                        break;
                    case 28:
                        i5 += zzwn.zzd(i7, zze(t2, j2));
                        break;
                    case 29:
                        i5 += zzwn.zzt(i7, zze(t2, j2), false);
                        break;
                    case 30:
                        i5 += zzwn.zzr(i7, zze(t2, j2), false);
                        break;
                    case 31:
                        i5 += zzwn.zzv(i7, zze(t2, j2), false);
                        break;
                    case 32:
                        i5 += zzwn.zzw(i7, zze(t2, j2), false);
                        break;
                    case 33:
                        i5 += zzwn.zzu(i7, zze(t2, j2), false);
                        break;
                    case 34:
                        i5 += zzwn.zzq(i7, zze(t2, j2), false);
                        break;
                    case 35:
                        int zzag = zzwn.zzag((List) unsafe.getObject(t2, j2));
                        if (zzag > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzag);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzag) + zzag;
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        int zzaf = zzwn.zzaf((List) unsafe.getObject(t2, j2));
                        if (zzaf > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzaf);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzaf) + zzaf;
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        int zzy = zzwn.zzy((List) unsafe.getObject(t2, j2));
                        if (zzy > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzy);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzy) + zzy;
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        int zzz = zzwn.zzz((List) unsafe.getObject(t2, j2));
                        if (zzz > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzz);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzz) + zzz;
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        int zzac = zzwn.zzac((List) unsafe.getObject(t2, j2));
                        if (zzac > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzac);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzac) + zzac;
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        int zzag2 = zzwn.zzag((List) unsafe.getObject(t2, j2));
                        if (zzag2 > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzag2);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzag2) + zzag2;
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        int zzaf2 = zzwn.zzaf((List) unsafe.getObject(t2, j2));
                        if (zzaf2 > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzaf2);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzaf2) + zzaf2;
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        int zzah = zzwn.zzah((List) unsafe.getObject(t2, j2));
                        if (zzah > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzah);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzah) + zzah;
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        int zzad = zzwn.zzad((List) unsafe.getObject(t2, j2));
                        if (zzad > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzad);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzad) + zzad;
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        int zzab = zzwn.zzab((List) unsafe.getObject(t2, j2));
                        if (zzab > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzab);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzab) + zzab;
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        int zzaf3 = zzwn.zzaf((List) unsafe.getObject(t2, j2));
                        if (zzaf3 > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzaf3);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzaf3) + zzaf3;
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        int zzag3 = zzwn.zzag((List) unsafe.getObject(t2, j2));
                        if (zzag3 > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzag3);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzag3) + zzag3;
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        int zzae = zzwn.zzae((List) unsafe.getObject(t2, j2));
                        if (zzae > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzae);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzae) + zzae;
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        int zzaa = zzwn.zzaa((List) unsafe.getObject(t2, j2));
                        if (zzaa > 0) {
                            if (this.zzcay) {
                                unsafe.putInt(t2, (long) i8, zzaa);
                            }
                            i5 += zztv.zzbd(i7) + zztv.zzbf(zzaa) + zzaa;
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        i5 += zzwn.zzd(i7, zze(t2, j2), zzbq(i4));
                        break;
                    case 50:
                        i5 += this.zzcbg.zzb(i7, zzxj.zzp(t2, j2), zzbr(i4));
                        break;
                    case 51:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzb(i7, (double) Utils.DOUBLE_EPSILON);
                            break;
                        }
                    case 52:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzb(i7, 0.0f);
                            break;
                        }
                    case 53:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzd(i7, zzi(t2, j2));
                            break;
                        }
                    case 54:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zze(i7, zzi(t2, j2));
                            break;
                        }
                    case 55:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzh(i7, zzh(t2, j2));
                            break;
                        }
                    case 56:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzg(i7, 0);
                            break;
                        }
                    case 57:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzk(i7, 0);
                            break;
                        }
                    case 58:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzc(i7, true);
                            break;
                        }
                    case 59:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            Object zzp2 = zzxj.zzp(t2, j2);
                            if (!(zzp2 instanceof zzte)) {
                                i5 += zztv.zzc(i7, (String) zzp2);
                                break;
                            } else {
                                i5 += zztv.zzc(i7, (zzte) zzp2);
                                break;
                            }
                        }
                    case 60:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzwn.zzc(i7, zzxj.zzp(t2, j2), zzbq(i4));
                            break;
                        }
                    case 61:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzc(i7, (zzte) zzxj.zzp(t2, j2));
                            break;
                        }
                    case 62:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzi(i7, zzh(t2, j2));
                            break;
                        }
                    case 63:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzm(i7, zzh(t2, j2));
                            break;
                        }
                    case 64:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzl(i7, 0);
                            break;
                        }
                    case 65:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzh(i7, 0);
                            break;
                        }
                    case 66:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzj(i7, zzh(t2, j2));
                            break;
                        }
                    case 67:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzf(i7, zzi(t2, j2));
                            break;
                        }
                    case 68:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zztv.zzc(i7, (zzvv) zzxj.zzp(t2, j2), zzbq(i4));
                            break;
                        }
                }
                i4 += 3;
                i3 = 267386880;
            }
            return i5 + zza(this.zzcbe, t2);
        }
        Unsafe unsafe2 = zzcap;
        int i9 = 0;
        int i10 = 0;
        int i11 = -1;
        int i12 = 0;
        while (i9 < this.zzcaq.length) {
            int zzbt2 = zzbt(i9);
            int i13 = this.zzcaq[i9];
            int i14 = (zzbt2 & 267386880) >>> 20;
            if (i14 <= 17) {
                i2 = this.zzcaq[i9 + 2];
                int i15 = i2 & 1048575;
                i = 1 << (i2 >>> 20);
                if (i15 != i11) {
                    i12 = unsafe2.getInt(t2, (long) i15);
                    i11 = i15;
                }
            } else {
                i2 = (!this.zzcay || i14 < zzui.DOUBLE_LIST_PACKED.id() || i14 > zzui.SINT64_LIST_PACKED.id()) ? 0 : this.zzcaq[i9 + 2] & 1048575;
                i = 0;
            }
            long j3 = (long) (zzbt2 & 1048575);
            switch (i14) {
                case 0:
                    j = 0;
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzb(i13, (double) Utils.DOUBLE_EPSILON);
                        break;
                    }
                case 1:
                    j = 0;
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzb(i13, 0.0f);
                        break;
                    }
                case 2:
                    j = 0;
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzd(i13, unsafe2.getLong(t2, j3));
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i12 & i) != 0) {
                        i10 += zztv.zze(i13, unsafe2.getLong(t2, j3));
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzh(i13, unsafe2.getInt(t2, j3));
                    }
                    break;
                case 5:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzg(i13, 0);
                        j = 0;
                        break;
                    }
                    break;
                case 6:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzk(i13, 0);
                        break;
                    }
                case 7:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzc(i13, true);
                    }
                    break;
                case 8:
                    if ((i12 & i) != 0) {
                        Object object = unsafe2.getObject(t2, j3);
                        i10 = object instanceof zzte ? i10 + zztv.zzc(i13, (zzte) object) : i10 + zztv.zzc(i13, (String) object);
                    }
                    break;
                case 9:
                    if ((i12 & i) != 0) {
                        i10 += zzwn.zzc(i13, unsafe2.getObject(t2, j3), zzbq(i9));
                    }
                    break;
                case 10:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzc(i13, (zzte) unsafe2.getObject(t2, j3));
                    }
                    break;
                case 11:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzi(i13, unsafe2.getInt(t2, j3));
                    }
                    break;
                case 12:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzm(i13, unsafe2.getInt(t2, j3));
                    }
                    break;
                case 13:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzl(i13, 0);
                    }
                    break;
                case 14:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzh(i13, 0);
                    }
                    break;
                case 15:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzj(i13, unsafe2.getInt(t2, j3));
                    }
                    break;
                case 16:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzf(i13, unsafe2.getLong(t2, j3));
                    }
                    break;
                case 17:
                    if ((i12 & i) != 0) {
                        i10 += zztv.zzc(i13, (zzvv) unsafe2.getObject(t2, j3), zzbq(i9));
                    }
                    break;
                case 18:
                    i10 += zzwn.zzw(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 19:
                    i10 += zzwn.zzv(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 20:
                    i10 += zzwn.zzo(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 21:
                    i10 += zzwn.zzp(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 22:
                    i10 += zzwn.zzs(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 23:
                    i10 += zzwn.zzw(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 24:
                    i10 += zzwn.zzv(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 25:
                    i10 += zzwn.zzx(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 26:
                    i10 += zzwn.zzc(i13, (List) unsafe2.getObject(t2, j3));
                    break;
                case 27:
                    i10 += zzwn.zzc(i13, (List) unsafe2.getObject(t2, j3), zzbq(i9));
                    break;
                case 28:
                    i10 += zzwn.zzd(i13, (List) unsafe2.getObject(t2, j3));
                    break;
                case 29:
                    i10 += zzwn.zzt(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 30:
                    i10 += zzwn.zzr(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 31:
                    i10 += zzwn.zzv(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 32:
                    i10 += zzwn.zzw(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 33:
                    i10 += zzwn.zzu(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 34:
                    i10 += zzwn.zzq(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 35:
                    int zzag4 = zzwn.zzag((List) unsafe2.getObject(t2, j3));
                    if (zzag4 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzag4);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzag4) + zzag4;
                    }
                    break;
                case 36:
                    int zzaf4 = zzwn.zzaf((List) unsafe2.getObject(t2, j3));
                    if (zzaf4 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzaf4);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzaf4) + zzaf4;
                    }
                    break;
                case 37:
                    int zzy2 = zzwn.zzy((List) unsafe2.getObject(t2, j3));
                    if (zzy2 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzy2);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzy2) + zzy2;
                    }
                    break;
                case 38:
                    int zzz2 = zzwn.zzz((List) unsafe2.getObject(t2, j3));
                    if (zzz2 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzz2);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzz2) + zzz2;
                    }
                    break;
                case 39:
                    int zzac2 = zzwn.zzac((List) unsafe2.getObject(t2, j3));
                    if (zzac2 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzac2);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzac2) + zzac2;
                    }
                    break;
                case 40:
                    int zzag5 = zzwn.zzag((List) unsafe2.getObject(t2, j3));
                    if (zzag5 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzag5);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzag5) + zzag5;
                    }
                    break;
                case 41:
                    int zzaf5 = zzwn.zzaf((List) unsafe2.getObject(t2, j3));
                    if (zzaf5 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzaf5);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzaf5) + zzaf5;
                    }
                    break;
                case 42:
                    int zzah2 = zzwn.zzah((List) unsafe2.getObject(t2, j3));
                    if (zzah2 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzah2);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzah2) + zzah2;
                    }
                    break;
                case 43:
                    int zzad2 = zzwn.zzad((List) unsafe2.getObject(t2, j3));
                    if (zzad2 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzad2);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzad2) + zzad2;
                    }
                    break;
                case 44:
                    int zzab2 = zzwn.zzab((List) unsafe2.getObject(t2, j3));
                    if (zzab2 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzab2);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzab2) + zzab2;
                    }
                    break;
                case 45:
                    int zzaf6 = zzwn.zzaf((List) unsafe2.getObject(t2, j3));
                    if (zzaf6 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzaf6);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzaf6) + zzaf6;
                    }
                    break;
                case 46:
                    int zzag6 = zzwn.zzag((List) unsafe2.getObject(t2, j3));
                    if (zzag6 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzag6);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzag6) + zzag6;
                    }
                    break;
                case 47:
                    int zzae2 = zzwn.zzae((List) unsafe2.getObject(t2, j3));
                    if (zzae2 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzae2);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzae2) + zzae2;
                    }
                    break;
                case 48:
                    int zzaa2 = zzwn.zzaa((List) unsafe2.getObject(t2, j3));
                    if (zzaa2 > 0) {
                        if (this.zzcay) {
                            unsafe2.putInt(t2, (long) i2, zzaa2);
                        }
                        i10 += zztv.zzbd(i13) + zztv.zzbf(zzaa2) + zzaa2;
                    }
                    break;
                case 49:
                    i10 += zzwn.zzd(i13, (List) unsafe2.getObject(t2, j3), zzbq(i9));
                    break;
                case 50:
                    i10 += this.zzcbg.zzb(i13, unsafe2.getObject(t2, j3), zzbr(i9));
                    break;
                case 51:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzb(i13, (double) Utils.DOUBLE_EPSILON);
                    }
                    break;
                case 52:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzb(i13, 0.0f);
                    }
                    break;
                case 53:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzd(i13, zzi(t2, j3));
                    }
                    break;
                case 54:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zze(i13, zzi(t2, j3));
                    }
                    break;
                case 55:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzh(i13, zzh(t2, j3));
                    }
                    break;
                case 56:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzg(i13, 0);
                    }
                    break;
                case 57:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzk(i13, 0);
                    }
                    break;
                case 58:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzc(i13, true);
                    }
                    break;
                case 59:
                    if (zza(t2, i13, i9)) {
                        Object object2 = unsafe2.getObject(t2, j3);
                        i10 = object2 instanceof zzte ? i10 + zztv.zzc(i13, (zzte) object2) : i10 + zztv.zzc(i13, (String) object2);
                    }
                    break;
                case 60:
                    if (zza(t2, i13, i9)) {
                        i10 += zzwn.zzc(i13, unsafe2.getObject(t2, j3), zzbq(i9));
                    }
                    break;
                case 61:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzc(i13, (zzte) unsafe2.getObject(t2, j3));
                    }
                    break;
                case 62:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzi(i13, zzh(t2, j3));
                    }
                    break;
                case 63:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzm(i13, zzh(t2, j3));
                    }
                    break;
                case 64:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzl(i13, 0);
                    }
                    break;
                case 65:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzh(i13, 0);
                    }
                    break;
                case 66:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzj(i13, zzh(t2, j3));
                    }
                    break;
                case 67:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzf(i13, zzi(t2, j3));
                    }
                    break;
                case 68:
                    if (zza(t2, i13, i9)) {
                        i10 += zztv.zzc(i13, (zzvv) unsafe2.getObject(t2, j3), zzbq(i9));
                    }
                    break;
            }
        }
        int zza = i10 + zza(this.zzcbe, t2);
        if (this.zzcav) {
            zza += this.zzcbf.zzw(t2).zzvx();
        }
        return zza;
    }

    private static <UT, UB> int zza(zzxd<UT, UB> zzxd, T t) {
        return zzxd.zzai(zzxd.zzal(t));
    }

    private static <E> List<E> zze(Object obj, long j) {
        return (List) zzxj.zzp(obj, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0511  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x054f  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a27  */
    public final void zza(T t, zzxy zzxy) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        Entry entry2;
        int i;
        Entry entry3;
        Iterator it2;
        int length2;
        if (zzxy.zzvm() == zze.zzbyw) {
            zza(this.zzcbe, t, zzxy);
            if (this.zzcav) {
                zzuf zzw = this.zzcbf.zzw(t);
                if (!zzw.isEmpty()) {
                    it2 = zzw.descendingIterator();
                    entry3 = (Entry) it2.next();
                    for (length2 = this.zzcaq.length - 3; length2 >= 0; length2 -= 3) {
                        int zzbt = zzbt(length2);
                        int i2 = this.zzcaq[length2];
                        while (entry3 != null && this.zzcbf.zzb(entry3) > i2) {
                            this.zzcbf.zza(zzxy, entry3);
                            entry3 = it2.hasNext() ? (Entry) it2.next() : null;
                        }
                        switch ((zzbt & 267386880) >>> 20) {
                            case 0:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, zzxj.zzo(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 1:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, zzxj.zzn(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 2:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzi(i2, zzxj.zzl(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 3:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, zzxj.zzl(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 4:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzd(i2, zzxj.zzk(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 5:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzc(i2, zzxj.zzl(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 6:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzg(i2, zzxj.zzk(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 7:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzb(i2, zzxj.zzm(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 8:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zza(i2, zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy);
                                    break;
                                }
                            case 9:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, zzxj.zzp(t, (long) (zzbt & 1048575)), zzbq(length2));
                                    break;
                                }
                            case 10:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, (zzte) zzxj.zzp(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 11:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zze(i2, zzxj.zzk(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 12:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzo(i2, zzxj.zzk(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 13:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzn(i2, zzxj.zzk(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 14:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzj(i2, zzxj.zzl(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 15:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzf(i2, zzxj.zzk(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 16:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzb(i2, zzxj.zzl(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 17:
                                if (!zzb(t, length2)) {
                                    break;
                                } else {
                                    zzxy.zzb(i2, zzxj.zzp(t, (long) (zzbt & 1048575)), zzbq(length2));
                                    break;
                                }
                            case 18:
                                zzwn.zza(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 19:
                                zzwn.zzb(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 20:
                                zzwn.zzc(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 21:
                                zzwn.zzd(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 22:
                                zzwn.zzh(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 23:
                                zzwn.zzf(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 24:
                                zzwn.zzk(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 25:
                                zzwn.zzn(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 26:
                                zzwn.zza(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy);
                                break;
                            case 27:
                                zzwn.zza(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, zzbq(length2));
                                break;
                            case 28:
                                zzwn.zzb(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy);
                                break;
                            case 29:
                                zzwn.zzi(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 30:
                                zzwn.zzm(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 31:
                                zzwn.zzl(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 32:
                                zzwn.zzg(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 33:
                                zzwn.zzj(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 34:
                                zzwn.zze(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, false);
                                break;
                            case 35:
                                zzwn.zza(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 36:
                                zzwn.zzb(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 37:
                                zzwn.zzc(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 38:
                                zzwn.zzd(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 39:
                                zzwn.zzh(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 40:
                                zzwn.zzf(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 41:
                                zzwn.zzk(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 42:
                                zzwn.zzn(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 43:
                                zzwn.zzi(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 44:
                                zzwn.zzm(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 45:
                                zzwn.zzl(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 46:
                                zzwn.zzg(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 47:
                                zzwn.zzj(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 48:
                                zzwn.zze(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, true);
                                break;
                            case 49:
                                zzwn.zzb(this.zzcaq[length2], (List) zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy, zzbq(length2));
                                break;
                            case 50:
                                zza(zzxy, i2, zzxj.zzp(t, (long) (zzbt & 1048575)), length2);
                                break;
                            case 51:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, zzf(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 52:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, zzg(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 53:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzi(i2, zzi(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 54:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, zzi(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 55:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzd(i2, zzh(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 56:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzc(i2, zzi(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 57:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzg(i2, zzh(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 58:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzb(i2, zzj(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 59:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zza(i2, zzxj.zzp(t, (long) (zzbt & 1048575)), zzxy);
                                    break;
                                }
                            case 60:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, zzxj.zzp(t, (long) (zzbt & 1048575)), zzbq(length2));
                                    break;
                                }
                            case 61:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zza(i2, (zzte) zzxj.zzp(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 62:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zze(i2, zzh(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 63:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzo(i2, zzh(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 64:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzn(i2, zzh(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 65:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzj(i2, zzi(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 66:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzf(i2, zzh(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 67:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzb(i2, zzi(t, (long) (zzbt & 1048575)));
                                    break;
                                }
                            case 68:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzxy.zzb(i2, zzxj.zzp(t, (long) (zzbt & 1048575)), zzbq(length2));
                                    break;
                                }
                        }
                    }
                    while (entry3 != null) {
                        this.zzcbf.zza(zzxy, entry3);
                        entry3 = it2.hasNext() ? (Entry) it2.next() : null;
                    }
                }
            }
            it2 = null;
            entry3 = null;
            while (length2 >= 0) {
            }
            while (entry3 != null) {
            }
        } else if (this.zzcax) {
            if (this.zzcav) {
                zzuf zzw2 = this.zzcbf.zzw(t);
                if (!zzw2.isEmpty()) {
                    it = zzw2.iterator();
                    entry = (Entry) it.next();
                    length = this.zzcaq.length;
                    entry2 = entry;
                    for (i = 0; i < length; i += 3) {
                        int zzbt2 = zzbt(i);
                        int i3 = this.zzcaq[i];
                        while (entry2 != null && this.zzcbf.zzb(entry2) <= i3) {
                            this.zzcbf.zza(zzxy, entry2);
                            entry2 = it.hasNext() ? (Entry) it.next() : null;
                        }
                        switch ((zzbt2 & 267386880) >>> 20) {
                            case 0:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, zzxj.zzo(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 1:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, zzxj.zzn(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 2:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzi(i3, zzxj.zzl(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 3:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, zzxj.zzl(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 4:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzd(i3, zzxj.zzk(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 5:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzc(i3, zzxj.zzl(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 6:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzg(i3, zzxj.zzk(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 7:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzb(i3, zzxj.zzm(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 8:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zza(i3, zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy);
                                    break;
                                }
                            case 9:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzbq(i));
                                    break;
                                }
                            case 10:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, (zzte) zzxj.zzp(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 11:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zze(i3, zzxj.zzk(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 12:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzo(i3, zzxj.zzk(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 13:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzn(i3, zzxj.zzk(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 14:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzj(i3, zzxj.zzl(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 15:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzf(i3, zzxj.zzk(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 16:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzb(i3, zzxj.zzl(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 17:
                                if (!zzb(t, i)) {
                                    break;
                                } else {
                                    zzxy.zzb(i3, zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzbq(i));
                                    break;
                                }
                            case 18:
                                zzwn.zza(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 19:
                                zzwn.zzb(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 20:
                                zzwn.zzc(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 21:
                                zzwn.zzd(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 22:
                                zzwn.zzh(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 23:
                                zzwn.zzf(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 24:
                                zzwn.zzk(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 25:
                                zzwn.zzn(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 26:
                                zzwn.zza(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy);
                                break;
                            case 27:
                                zzwn.zza(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, zzbq(i));
                                break;
                            case 28:
                                zzwn.zzb(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy);
                                break;
                            case 29:
                                zzwn.zzi(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 30:
                                zzwn.zzm(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 31:
                                zzwn.zzl(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 32:
                                zzwn.zzg(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 33:
                                zzwn.zzj(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 34:
                                zzwn.zze(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, false);
                                break;
                            case 35:
                                zzwn.zza(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 36:
                                zzwn.zzb(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 37:
                                zzwn.zzc(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 38:
                                zzwn.zzd(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 39:
                                zzwn.zzh(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 40:
                                zzwn.zzf(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 41:
                                zzwn.zzk(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 42:
                                zzwn.zzn(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 43:
                                zzwn.zzi(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 44:
                                zzwn.zzm(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 45:
                                zzwn.zzl(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 46:
                                zzwn.zzg(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 47:
                                zzwn.zzj(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 48:
                                zzwn.zze(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, true);
                                break;
                            case 49:
                                zzwn.zzb(this.zzcaq[i], (List) zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy, zzbq(i));
                                break;
                            case 50:
                                zza(zzxy, i3, zzxj.zzp(t, (long) (zzbt2 & 1048575)), i);
                                break;
                            case 51:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, zzf(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 52:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, zzg(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 53:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzi(i3, zzi(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 54:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, zzi(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 55:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzd(i3, zzh(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 56:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzc(i3, zzi(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 57:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzg(i3, zzh(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 58:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzb(i3, zzj(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 59:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zza(i3, zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzxy);
                                    break;
                                }
                            case 60:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzbq(i));
                                    break;
                                }
                            case 61:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zza(i3, (zzte) zzxj.zzp(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 62:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zze(i3, zzh(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 63:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzo(i3, zzh(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 64:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzn(i3, zzh(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 65:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzj(i3, zzi(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 66:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzf(i3, zzh(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 67:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzb(i3, zzi(t, (long) (zzbt2 & 1048575)));
                                    break;
                                }
                            case 68:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzxy.zzb(i3, zzxj.zzp(t, (long) (zzbt2 & 1048575)), zzbq(i));
                                    break;
                                }
                        }
                    }
                    while (entry2 != null) {
                        this.zzcbf.zza(zzxy, entry2);
                        entry2 = it.hasNext() ? (Entry) it.next() : null;
                    }
                    zza(this.zzcbe, t, zzxy);
                }
            }
            it = null;
            entry = null;
            length = this.zzcaq.length;
            entry2 = entry;
            while (i < length) {
            }
            while (entry2 != null) {
            }
            zza(this.zzcbe, t, zzxy);
        } else {
            zzb(t, zzxy);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:172:0x0527  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    private final void zzb(T t, zzxy zzxy) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        Entry entry2;
        int i;
        int i2;
        int i3;
        int i4;
        T t2 = t;
        zzxy zzxy2 = zzxy;
        if (this.zzcav) {
            zzuf zzw = this.zzcbf.zzw(t2);
            if (!zzw.isEmpty()) {
                it = zzw.iterator();
                entry = (Entry) it.next();
                int i5 = -1;
                length = this.zzcaq.length;
                Unsafe unsafe = zzcap;
                entry2 = entry;
                int i6 = 0;
                for (i = 0; i < length; i = i4 + 3) {
                    int zzbt = zzbt(i);
                    int i7 = this.zzcaq[i];
                    int i8 = (267386880 & zzbt) >>> 20;
                    if (this.zzcax || i8 > 17) {
                        i2 = i;
                        i3 = 0;
                    } else {
                        int i9 = this.zzcaq[i + 2];
                        int i10 = i9 & 1048575;
                        if (i10 != i5) {
                            i2 = i;
                            i6 = unsafe.getInt(t2, (long) i10);
                            i5 = i10;
                        } else {
                            i2 = i;
                        }
                        i3 = 1 << (i9 >>> 20);
                    }
                    while (entry2 != null && this.zzcbf.zzb(entry2) <= i7) {
                        this.zzcbf.zza(zzxy2, entry2);
                        entry2 = it.hasNext() ? (Entry) it.next() : null;
                    }
                    long j = (long) (zzbt & 1048575);
                    switch (i8) {
                        case 0:
                            i4 = i2;
                            if ((i3 & i6) == 0) {
                                break;
                            } else {
                                zzxy2.zza(i7, zzxj.zzo(t2, j));
                                continue;
                            }
                        case 1:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zza(i7, zzxj.zzn(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 2:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzi(i7, unsafe.getLong(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 3:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zza(i7, unsafe.getLong(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 4:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzd(i7, unsafe.getInt(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 5:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzc(i7, unsafe.getLong(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 6:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzg(i7, unsafe.getInt(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 7:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzb(i7, zzxj.zzm(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 8:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zza(i7, unsafe.getObject(t2, j), zzxy2);
                                break;
                            } else {
                                continue;
                            }
                        case 9:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zza(i7, unsafe.getObject(t2, j), zzbq(i4));
                                break;
                            } else {
                                continue;
                            }
                        case 10:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zza(i7, (zzte) unsafe.getObject(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 11:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zze(i7, unsafe.getInt(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 12:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzo(i7, unsafe.getInt(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 13:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzn(i7, unsafe.getInt(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 14:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzj(i7, unsafe.getLong(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 15:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzf(i7, unsafe.getInt(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 16:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzb(i7, unsafe.getLong(t2, j));
                                break;
                            } else {
                                continue;
                            }
                        case 17:
                            i4 = i2;
                            if ((i3 & i6) != 0) {
                                zzxy2.zzb(i7, unsafe.getObject(t2, j), zzbq(i4));
                                break;
                            } else {
                                continue;
                            }
                        case 18:
                            i4 = i2;
                            zzwn.zza(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            continue;
                        case 19:
                            i4 = i2;
                            zzwn.zzb(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            continue;
                        case 20:
                            i4 = i2;
                            zzwn.zzc(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            continue;
                        case 21:
                            i4 = i2;
                            zzwn.zzd(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            continue;
                        case 22:
                            i4 = i2;
                            zzwn.zzh(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            continue;
                        case 23:
                            i4 = i2;
                            zzwn.zzf(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            continue;
                        case 24:
                            i4 = i2;
                            zzwn.zzk(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            continue;
                        case 25:
                            i4 = i2;
                            zzwn.zzn(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            continue;
                        case 26:
                            i4 = i2;
                            zzwn.zza(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2);
                            break;
                        case 27:
                            i4 = i2;
                            zzwn.zza(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, zzbq(i4));
                            break;
                        case 28:
                            i4 = i2;
                            zzwn.zzb(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2);
                            break;
                        case 29:
                            i4 = i2;
                            zzwn.zzi(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            break;
                        case 30:
                            i4 = i2;
                            zzwn.zzm(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            break;
                        case 31:
                            i4 = i2;
                            zzwn.zzl(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            break;
                        case 32:
                            i4 = i2;
                            zzwn.zzg(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            break;
                        case 33:
                            i4 = i2;
                            zzwn.zzj(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            break;
                        case 34:
                            i4 = i2;
                            zzwn.zze(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, false);
                            break;
                        case 35:
                            i4 = i2;
                            zzwn.zza(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 36:
                            i4 = i2;
                            zzwn.zzb(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 37:
                            i4 = i2;
                            zzwn.zzc(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 38:
                            i4 = i2;
                            zzwn.zzd(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 39:
                            i4 = i2;
                            zzwn.zzh(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 40:
                            i4 = i2;
                            zzwn.zzf(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 41:
                            i4 = i2;
                            zzwn.zzk(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 42:
                            i4 = i2;
                            zzwn.zzn(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 43:
                            i4 = i2;
                            zzwn.zzi(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 44:
                            i4 = i2;
                            zzwn.zzm(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 45:
                            i4 = i2;
                            zzwn.zzl(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 46:
                            i4 = i2;
                            zzwn.zzg(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 47:
                            i4 = i2;
                            zzwn.zzj(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 48:
                            i4 = i2;
                            zzwn.zze(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, true);
                            break;
                        case 49:
                            i4 = i2;
                            zzwn.zzb(this.zzcaq[i4], (List) unsafe.getObject(t2, j), zzxy2, zzbq(i4));
                            break;
                        case 50:
                            i4 = i2;
                            zza(zzxy2, i7, unsafe.getObject(t2, j), i4);
                            break;
                        case 51:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zza(i7, zzf(t2, j));
                                break;
                            }
                            break;
                        case 52:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zza(i7, zzg(t2, j));
                                break;
                            }
                            break;
                        case 53:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzi(i7, zzi(t2, j));
                                break;
                            }
                            break;
                        case 54:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zza(i7, zzi(t2, j));
                                break;
                            }
                            break;
                        case 55:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzd(i7, zzh(t2, j));
                                break;
                            }
                            break;
                        case 56:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzc(i7, zzi(t2, j));
                                break;
                            }
                            break;
                        case 57:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzg(i7, zzh(t2, j));
                                break;
                            }
                            break;
                        case 58:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzb(i7, zzj(t2, j));
                                break;
                            }
                            break;
                        case 59:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zza(i7, unsafe.getObject(t2, j), zzxy2);
                                break;
                            }
                            break;
                        case 60:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zza(i7, unsafe.getObject(t2, j), zzbq(i4));
                                break;
                            }
                            break;
                        case 61:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zza(i7, (zzte) unsafe.getObject(t2, j));
                                break;
                            }
                            break;
                        case 62:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zze(i7, zzh(t2, j));
                                break;
                            }
                            break;
                        case 63:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzo(i7, zzh(t2, j));
                                break;
                            }
                            break;
                        case 64:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzn(i7, zzh(t2, j));
                                break;
                            }
                            break;
                        case 65:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzj(i7, zzi(t2, j));
                                break;
                            }
                            break;
                        case 66:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzf(i7, zzh(t2, j));
                                break;
                            }
                            break;
                        case 67:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzb(i7, zzi(t2, j));
                                break;
                            }
                            break;
                        case 68:
                            i4 = i2;
                            if (zza(t2, i7, i4)) {
                                zzxy2.zzb(i7, unsafe.getObject(t2, j), zzbq(i4));
                                break;
                            }
                            break;
                        default:
                            i4 = i2;
                            break;
                    }
                }
                while (entry2 != null) {
                    this.zzcbf.zza(zzxy2, entry2);
                    entry2 = it.hasNext() ? (Entry) it.next() : null;
                }
                zza(this.zzcbe, t2, zzxy2);
            }
        }
        it = null;
        entry = null;
        int i52 = -1;
        length = this.zzcaq.length;
        Unsafe unsafe2 = zzcap;
        entry2 = entry;
        int i62 = 0;
        while (i < length) {
        }
        while (entry2 != null) {
        }
        zza(this.zzcbe, t2, zzxy2);
    }

    private final <K, V> void zza(zzxy zzxy, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzxy.zza(i, this.zzcbg.zzah(zzbr(i2)), this.zzcbg.zzad(obj));
        }
    }

    private static <UT, UB> void zza(zzxd<UT, UB> zzxd, T t, zzxy zzxy) throws IOException {
        zzxd.zza(zzxd.zzal(t), zzxy);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:166:?, code lost:
        r11.zza(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x05b5, code lost:
        if (r14 == null) goto L_0x05b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x05b7, code lost:
        r14 = r11.zzam(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x05c0, code lost:
        if (r11.zza(r14, r0) == false) goto L_0x05c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x05c2, code lost:
        r0 = r1.zzcba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x05c6, code lost:
        if (r0 < r1.zzcbb) goto L_0x05c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x05c8, code lost:
        r14 = zza((java.lang.Object) r2, r1.zzcaz[r0], (UB) r14, r11);
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x05d3, code lost:
        if (r14 != null) goto L_0x05d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x05d5, code lost:
        r11.zzg(r2, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x05d8, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:165:0x05b2 */
    public final void zza(T t, zzwk zzwk, zzub zzub) throws IOException {
        Object zza;
        Object obj;
        T t2 = t;
        zzwk zzwk2 = zzwk;
        zzub zzub2 = zzub;
        if (zzub2 != null) {
            zzxd<?, ?> zzxd = this.zzcbe;
            zzuc<?> zzuc = this.zzcbf;
            zzuf zzuf = null;
            Object obj2 = null;
            while (true) {
                try {
                    int zzvh = zzwk.zzvh();
                    int i = -1;
                    if (zzvh >= this.zzcas && zzvh <= this.zzcat) {
                        int i2 = 0;
                        int length = (this.zzcaq.length / 3) - 1;
                        while (true) {
                            if (i2 <= length) {
                                int i3 = (length + i2) >>> 1;
                                int i4 = i3 * 3;
                                int i5 = this.zzcaq[i4];
                                if (zzvh == i5) {
                                    i = i4;
                                } else if (zzvh < i5) {
                                    length = i3 - 1;
                                } else {
                                    i2 = i3 + 1;
                                }
                            }
                        }
                    }
                    if (i >= 0) {
                        int zzbt = zzbt(i);
                        switch ((267386880 & zzbt) >>> 20) {
                            case 0:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk.readDouble());
                                zzc(t2, i);
                                continue;
                            case 1:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk.readFloat());
                                zzc(t2, i);
                                continue;
                            case 2:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk.zzul());
                                zzc(t2, i);
                                continue;
                            case 3:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk.zzuk());
                                zzc(t2, i);
                                continue;
                            case 4:
                                zzxj.zzb((Object) t2, (long) (zzbt & 1048575), zzwk.zzum());
                                zzc(t2, i);
                                continue;
                            case 5:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk.zzun());
                                zzc(t2, i);
                                continue;
                            case 6:
                                zzxj.zzb((Object) t2, (long) (zzbt & 1048575), zzwk.zzuo());
                                zzc(t2, i);
                                continue;
                            case 7:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk.zzup());
                                zzc(t2, i);
                                continue;
                            case 8:
                                zza((Object) t2, zzbt, zzwk2);
                                zzc(t2, i);
                                continue;
                            case 9:
                                if (!zzb(t2, i)) {
                                    zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk2.zza(zzbq(i), zzub2));
                                    zzc(t2, i);
                                    break;
                                } else {
                                    long j = (long) (zzbt & 1048575);
                                    zzxj.zza((Object) t2, j, zzuq.zzb(zzxj.zzp(t2, j), zzwk2.zza(zzbq(i), zzub2)));
                                    continue;
                                }
                            case 10:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) zzwk.zzur());
                                zzc(t2, i);
                                continue;
                            case 11:
                                zzxj.zzb((Object) t2, (long) (zzbt & 1048575), zzwk.zzus());
                                zzc(t2, i);
                                continue;
                            case 12:
                                int zzut = zzwk.zzut();
                                zzut zzbs = zzbs(i);
                                if (zzbs != null) {
                                    if (!zzbs.zzb(zzut)) {
                                        zza = zzwn.zza(zzvh, zzut, obj2, zzxd);
                                    }
                                }
                                zzxj.zzb((Object) t2, (long) (zzbt & 1048575), zzut);
                                zzc(t2, i);
                                continue;
                            case 13:
                                zzxj.zzb((Object) t2, (long) (zzbt & 1048575), zzwk.zzuu());
                                zzc(t2, i);
                                continue;
                            case 14:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk.zzuv());
                                zzc(t2, i);
                                continue;
                            case 15:
                                zzxj.zzb((Object) t2, (long) (zzbt & 1048575), zzwk.zzuw());
                                zzc(t2, i);
                                continue;
                            case 16:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk.zzux());
                                zzc(t2, i);
                                continue;
                            case 17:
                                if (!zzb(t2, i)) {
                                    zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk2.zzb(zzbq(i), zzub2));
                                    zzc(t2, i);
                                    break;
                                } else {
                                    long j2 = (long) (zzbt & 1048575);
                                    zzxj.zza((Object) t2, j2, zzuq.zzb(zzxj.zzp(t2, j2), zzwk2.zzb(zzbq(i), zzub2)));
                                    continue;
                                }
                            case 18:
                                zzwk2.zzi(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 19:
                                zzwk2.zzj(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 20:
                                zzwk2.zzl(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 21:
                                zzwk2.zzk(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 22:
                                zzwk2.zzm(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 23:
                                zzwk2.zzn(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 24:
                                zzwk2.zzo(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 25:
                                zzwk2.zzp(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 26:
                                if (!zzbv(zzbt)) {
                                    zzwk2.readStringList(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                    break;
                                } else {
                                    zzwk2.zzq(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                    continue;
                                }
                            case 27:
                                zzwk2.zza(this.zzcbd.zza(t2, (long) (zzbt & 1048575)), zzbq(i), zzub2);
                                continue;
                            case 28:
                                zzwk2.zzr(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 29:
                                zzwk2.zzs(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 30:
                                List zza2 = this.zzcbd.zza(t2, (long) (zzbt & 1048575));
                                zzwk2.zzt(zza2);
                                zza = zzwn.zza(zzvh, zza2, zzbs(i), obj2, zzxd);
                                obj2 = zza;
                                break;
                            case 31:
                                zzwk2.zzu(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 32:
                                zzwk2.zzv(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 33:
                                zzwk2.zzw(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 34:
                                zzwk2.zzx(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 35:
                                zzwk2.zzi(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 36:
                                zzwk2.zzj(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 37:
                                zzwk2.zzl(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 38:
                                zzwk2.zzk(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 39:
                                zzwk2.zzm(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 40:
                                zzwk2.zzn(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 41:
                                zzwk2.zzo(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 42:
                                zzwk2.zzp(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 43:
                                zzwk2.zzs(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 44:
                                List zza3 = this.zzcbd.zza(t2, (long) (zzbt & 1048575));
                                zzwk2.zzt(zza3);
                                zza = zzwn.zza(zzvh, zza3, zzbs(i), obj2, zzxd);
                                obj2 = zza;
                                break;
                            case 45:
                                zzwk2.zzu(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 46:
                                zzwk2.zzv(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 47:
                                zzwk2.zzw(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 48:
                                zzwk2.zzx(this.zzcbd.zza(t2, (long) (zzbt & 1048575)));
                                continue;
                            case 49:
                                long j3 = (long) (zzbt & 1048575);
                                zzwk2.zzb(this.zzcbd.zza(t2, j3), zzbq(i), zzub2);
                                continue;
                            case 50:
                                Object zzbr = zzbr(i);
                                long zzbt2 = (long) (zzbt(i) & 1048575);
                                Object zzp = zzxj.zzp(t2, zzbt2);
                                if (zzp == null) {
                                    zzp = this.zzcbg.zzag(zzbr);
                                    zzxj.zza((Object) t2, zzbt2, zzp);
                                } else if (this.zzcbg.zzae(zzp)) {
                                    Object zzag = this.zzcbg.zzag(zzbr);
                                    this.zzcbg.zzc(zzag, zzp);
                                    zzxj.zza((Object) t2, zzbt2, zzag);
                                    zzp = zzag;
                                }
                                zzwk2.zza(this.zzcbg.zzac(zzp), this.zzcbg.zzah(zzbr), zzub2);
                                continue;
                            case 51:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Double.valueOf(zzwk.readDouble()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 52:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Float.valueOf(zzwk.readFloat()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 53:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Long.valueOf(zzwk.zzul()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 54:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Long.valueOf(zzwk.zzuk()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 55:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Integer.valueOf(zzwk.zzum()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 56:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Long.valueOf(zzwk.zzun()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 57:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Integer.valueOf(zzwk.zzuo()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 58:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Boolean.valueOf(zzwk.zzup()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 59:
                                zza((Object) t2, zzbt, zzwk2);
                                zzb(t2, zzvh, i);
                                continue;
                            case 60:
                                if (zza(t2, zzvh, i)) {
                                    long j4 = (long) (zzbt & 1048575);
                                    zzxj.zza((Object) t2, j4, zzuq.zzb(zzxj.zzp(t2, j4), zzwk2.zza(zzbq(i), zzub2)));
                                } else {
                                    zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk2.zza(zzbq(i), zzub2));
                                    zzc(t2, i);
                                }
                                zzb(t2, zzvh, i);
                                continue;
                            case 61:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) zzwk.zzur());
                                zzb(t2, zzvh, i);
                                continue;
                            case 62:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Integer.valueOf(zzwk.zzus()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 63:
                                int zzut2 = zzwk.zzut();
                                zzut zzbs2 = zzbs(i);
                                if (zzbs2 != null) {
                                    if (!zzbs2.zzb(zzut2)) {
                                        zza = zzwn.zza(zzvh, zzut2, obj2, zzxd);
                                    }
                                }
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Integer.valueOf(zzut2));
                                zzb(t2, zzvh, i);
                                continue;
                            case 64:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Integer.valueOf(zzwk.zzuu()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 65:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Long.valueOf(zzwk.zzuv()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 66:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Integer.valueOf(zzwk.zzuw()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 67:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), (Object) Long.valueOf(zzwk.zzux()));
                                zzb(t2, zzvh, i);
                                continue;
                            case 68:
                                zzxj.zza((Object) t2, (long) (zzbt & 1048575), zzwk2.zzb(zzbq(i), zzub2));
                                zzb(t2, zzvh, i);
                                continue;
                            default:
                                if (obj2 == null) {
                                    obj2 = zzxd.zzyk();
                                    break;
                                }
                                if (!zzxd.zza(obj2, zzwk2)) {
                                    for (int i6 = this.zzcba; i6 < this.zzcbb; i6++) {
                                        obj2 = zza((Object) t2, this.zzcaz[i6], (UB) obj2, zzxd);
                                    }
                                    if (obj2 != null) {
                                        zzxd.zzg(t2, obj2);
                                    }
                                    return;
                                }
                                continue;
                        }
                        obj2 = zza;
                    } else if (zzvh == Integer.MAX_VALUE) {
                        for (int i7 = this.zzcba; i7 < this.zzcbb; i7++) {
                            obj2 = zza((Object) t2, this.zzcaz[i7], (UB) obj2, zzxd);
                        }
                        if (obj2 != null) {
                            zzxd.zzg(t2, obj2);
                        }
                        return;
                    } else {
                        if (!this.zzcav) {
                            obj = null;
                        } else {
                            obj = zzuc.zza(zzub2, this.zzcau, zzvh);
                        }
                        if (obj != null) {
                            if (zzuf == null) {
                                zzuf = zzuc.zzx(t2);
                            }
                            zzuf zzuf2 = zzuf;
                            obj2 = zzuc.zza(zzwk, obj, zzub, zzuf2, obj2, zzxd);
                            zzuf = zzuf2;
                        } else {
                            zzxd.zza(zzwk2);
                            if (obj2 == null) {
                                obj2 = zzxd.zzam(t2);
                            }
                            if (!zzxd.zza(obj2, zzwk2)) {
                                if (obj2 != null) {
                                    zzxd.zzg(t2, obj2);
                                }
                                return;
                            }
                        }
                    }
                } finally {
                    for (int i8 = this.zzcba; i8 < this.zzcbb; i8++) {
                        obj2 = zza((Object) t2, this.zzcaz[i8], (UB) obj2, zzxd);
                    }
                    if (obj2 != null) {
                        zzxd.zzg(t2, obj2);
                    }
                }
            }
        } else {
            throw new NullPointerException();
        }
    }

    private final zzwl zzbq(int i) {
        int i2 = (i / 3) << 1;
        zzwl zzwl = (zzwl) this.zzcar[i2];
        if (zzwl != null) {
            return zzwl;
        }
        zzwl zzi = zzwh.zzxt().zzi((Class) this.zzcar[i2 + 1]);
        this.zzcar[i2] = zzi;
        return zzi;
    }

    private final Object zzbr(int i) {
        return this.zzcar[(i / 3) << 1];
    }

    private final zzut zzbs(int i) {
        return (zzut) this.zzcar[((i / 3) << 1) + 1];
    }

    public final void zzy(T t) {
        for (int i = this.zzcba; i < this.zzcbb; i++) {
            long zzbt = (long) (zzbt(this.zzcaz[i]) & 1048575);
            Object zzp = zzxj.zzp(t, zzbt);
            if (zzp != null) {
                zzxj.zza((Object) t, zzbt, this.zzcbg.zzaf(zzp));
            }
        }
        int length = this.zzcaz.length;
        for (int i2 = this.zzcbb; i2 < length; i2++) {
            this.zzcbd.zzb(t, (long) this.zzcaz[i2]);
        }
        this.zzcbe.zzy(t);
        if (this.zzcav) {
            this.zzcbf.zzy(t);
        }
    }

    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzxd<UT, UB> zzxd) {
        int i2 = this.zzcaq[i];
        Object zzp = zzxj.zzp(obj, (long) (zzbt(i) & 1048575));
        if (zzp == null) {
            return ub;
        }
        zzut zzbs = zzbs(i);
        if (zzbs == null) {
            return ub;
        }
        return zza(i, i2, this.zzcbg.zzac(zzp), zzbs, ub, zzxd);
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map<K, V> map, zzut zzut, UB ub, zzxd<UT, UB> zzxd) {
        zzvo zzah = this.zzcbg.zzah(zzbr(i));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (!zzut.zzb(((Integer) entry.getValue()).intValue())) {
                if (ub == null) {
                    ub = zzxd.zzyk();
                }
                zztm zzao = zzte.zzao(zzvn.zza(zzah, entry.getKey(), entry.getValue()));
                try {
                    zzvn.zza(zzao.zzui(), zzah, entry.getKey(), entry.getValue());
                    zzxd.zza(ub, i2, zzao.zzuh());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0104, code lost:
        continue;
     */
    public final boolean zzaj(T t) {
        int i;
        int i2 = 0;
        int i3 = -1;
        int i4 = 0;
        while (true) {
            boolean z = true;
            if (i2 >= this.zzcba) {
                return !this.zzcav || this.zzcbf.zzw(t).isInitialized();
            }
            int i5 = this.zzcaz[i2];
            int i6 = this.zzcaq[i5];
            int zzbt = zzbt(i5);
            if (!this.zzcax) {
                int i7 = this.zzcaq[i5 + 2];
                int i8 = i7 & 1048575;
                i = 1 << (i7 >>> 20);
                if (i8 != i3) {
                    i4 = zzcap.getInt(t, (long) i8);
                    i3 = i8;
                }
            } else {
                i = 0;
            }
            if (((268435456 & zzbt) != 0) && !zza(t, i5, i4, i)) {
                return false;
            }
            int i9 = (267386880 & zzbt) >>> 20;
            if (i9 != 9 && i9 != 17) {
                if (i9 != 27) {
                    if (i9 != 60 && i9 != 68) {
                        switch (i9) {
                            case 49:
                                break;
                            case 50:
                                Map zzad = this.zzcbg.zzad(zzxj.zzp(t, (long) (zzbt & 1048575)));
                                if (!zzad.isEmpty()) {
                                    if (this.zzcbg.zzah(zzbr(i5)).zzcak.zzyv() == zzxx.MESSAGE) {
                                        zzwl zzwl = null;
                                        Iterator it = zzad.values().iterator();
                                        while (true) {
                                            if (it.hasNext()) {
                                                Object next = it.next();
                                                if (zzwl == null) {
                                                    zzwl = zzwh.zzxt().zzi(next.getClass());
                                                }
                                                if (!zzwl.zzaj(next)) {
                                                    z = false;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!z) {
                                    return false;
                                }
                                continue;
                        }
                    } else if (zza(t, i6, i5) && !zza((Object) t, zzbt, zzbq(i5))) {
                        return false;
                    }
                }
                List list = (List) zzxj.zzp(t, (long) (zzbt & 1048575));
                if (!list.isEmpty()) {
                    zzwl zzbq = zzbq(i5);
                    int i10 = 0;
                    while (true) {
                        if (i10 < list.size()) {
                            if (!zzbq.zzaj(list.get(i10))) {
                                z = false;
                            } else {
                                i10++;
                            }
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza(t, i5, i4, i) && !zza((Object) t, zzbt, zzbq(i5))) {
                return false;
            }
            i2++;
        }
    }

    private static boolean zza(Object obj, int i, zzwl zzwl) {
        return zzwl.zzaj(zzxj.zzp(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzxy zzxy) throws IOException {
        if (obj instanceof String) {
            zzxy.zzb(i, (String) obj);
        } else {
            zzxy.zza(i, (zzte) obj);
        }
    }

    private final void zza(Object obj, int i, zzwk zzwk) throws IOException {
        if (zzbv(i)) {
            zzxj.zza(obj, (long) (i & 1048575), (Object) zzwk.zzuq());
        } else if (this.zzcaw) {
            zzxj.zza(obj, (long) (i & 1048575), (Object) zzwk.readString());
        } else {
            zzxj.zza(obj, (long) (i & 1048575), (Object) zzwk.zzur());
        }
    }

    private final int zzbt(int i) {
        return this.zzcaq[i + 1];
    }

    private final int zzbu(int i) {
        return this.zzcaq[i + 2];
    }

    private static <T> double zzf(T t, long j) {
        return ((Double) zzxj.zzp(t, j)).doubleValue();
    }

    private static <T> float zzg(T t, long j) {
        return ((Float) zzxj.zzp(t, j)).floatValue();
    }

    private static <T> int zzh(T t, long j) {
        return ((Integer) zzxj.zzp(t, j)).intValue();
    }

    private static <T> long zzi(T t, long j) {
        return ((Long) zzxj.zzp(t, j)).longValue();
    }

    private static <T> boolean zzj(T t, long j) {
        return ((Boolean) zzxj.zzp(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zzb(t, i) == zzb(t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3) {
        if (this.zzcax) {
            return zzb(t, i);
        }
        return (i2 & i3) != 0;
    }

    private final boolean zzb(T t, int i) {
        if (this.zzcax) {
            int zzbt = zzbt(i);
            long j = (long) (zzbt & 1048575);
            switch ((zzbt & 267386880) >>> 20) {
                case 0:
                    return zzxj.zzo(t, j) != Utils.DOUBLE_EPSILON;
                case 1:
                    return zzxj.zzn(t, j) != 0.0f;
                case 2:
                    return zzxj.zzl(t, j) != 0;
                case 3:
                    return zzxj.zzl(t, j) != 0;
                case 4:
                    return zzxj.zzk(t, j) != 0;
                case 5:
                    return zzxj.zzl(t, j) != 0;
                case 6:
                    return zzxj.zzk(t, j) != 0;
                case 7:
                    return zzxj.zzm(t, j);
                case 8:
                    Object zzp = zzxj.zzp(t, j);
                    if (zzp instanceof String) {
                        return !((String) zzp).isEmpty();
                    }
                    if (zzp instanceof zzte) {
                        return !zzte.zzbtq.equals(zzp);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzxj.zzp(t, j) != null;
                case 10:
                    return !zzte.zzbtq.equals(zzxj.zzp(t, j));
                case 11:
                    return zzxj.zzk(t, j) != 0;
                case 12:
                    return zzxj.zzk(t, j) != 0;
                case 13:
                    return zzxj.zzk(t, j) != 0;
                case 14:
                    return zzxj.zzl(t, j) != 0;
                case 15:
                    return zzxj.zzk(t, j) != 0;
                case 16:
                    return zzxj.zzl(t, j) != 0;
                case 17:
                    return zzxj.zzp(t, j) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int zzbu = zzbu(i);
            return (zzxj.zzk(t, (long) (zzbu & 1048575)) & (1 << (zzbu >>> 20))) != 0;
        }
    }

    private final void zzc(T t, int i) {
        if (!this.zzcax) {
            int zzbu = zzbu(i);
            long j = (long) (zzbu & 1048575);
            zzxj.zzb((Object) t, j, zzxj.zzk(t, j) | (1 << (zzbu >>> 20)));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzxj.zzk(t, (long) (zzbu(i2) & 1048575)) == i;
    }

    private final void zzb(T t, int i, int i2) {
        zzxj.zzb((Object) t, (long) (zzbu(i2) & 1048575), i);
    }
}
