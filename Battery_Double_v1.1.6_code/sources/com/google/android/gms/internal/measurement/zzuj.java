package com.google.android.gms.internal.measurement;

final /* synthetic */ class zzuj {
    static final /* synthetic */ int[] zzbxr = new int[zzuk.values().length];
    static final /* synthetic */ int[] zzbxs = new int[zzux.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(14:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
    static {
        try {
            zzbxs[zzux.BYTE_STRING.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zzbxs[zzux.MESSAGE.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            zzbxs[zzux.STRING.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        zzbxr[zzuk.MAP.ordinal()] = 1;
        zzbxr[zzuk.VECTOR.ordinal()] = 2;
        zzbxr[zzuk.SCALAR.ordinal()] = 3;
    }
}
