package com.google.android.gms.common;

public final class R {

    public static final class integer {
        public static final int google_play_services_version = 2131361799;

        private integer() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_unknown_issue = 2131624042;

        private string() {
        }
    }

    private R() {
    }
}
