package com.google.android.gms.common.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

@KeepForSdk
public class LibraryVersion {
    private static final GmsLogger zzel = new GmsLogger("LibraryVersion", "");
    private static LibraryVersion zzem = new LibraryVersion();
    private ConcurrentHashMap<String, String> zzen = new ConcurrentHashMap<>();

    @KeepForSdk
    public static LibraryVersion getInstance() {
        return zzem;
    }

    @VisibleForTesting
    protected LibraryVersion() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ae  */
    @KeepForSdk
    public String getVersion(@NonNull String str) {
        Preconditions.checkNotEmpty(str, "Please provide a valid libraryName");
        if (this.zzen.containsKey(str)) {
            return (String) this.zzen.get(str);
        }
        Properties properties = new Properties();
        String str2 = null;
        try {
            InputStream resourceAsStream = LibraryVersion.class.getResourceAsStream(String.format("/%s.properties", new Object[]{str}));
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
                String property = properties.getProperty("version", null);
                try {
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12 + String.valueOf(property).length());
                    sb.append(str);
                    sb.append(" version is ");
                    sb.append(property);
                    zzel.v("LibraryVersion", sb.toString());
                    str2 = property;
                } catch (IOException e) {
                    Throwable th = e;
                    str2 = property;
                    e = th;
                    GmsLogger gmsLogger = zzel;
                    String str3 = "LibraryVersion";
                    String str4 = "Failed to get app version for libraryName: ";
                    String valueOf = String.valueOf(str);
                    gmsLogger.e(str3, valueOf.length() == 0 ? str4.concat(valueOf) : new String(str4), e);
                    if (str2 == null) {
                    }
                    this.zzen.put(str, str2);
                    return str2;
                }
            } else {
                GmsLogger gmsLogger2 = zzel;
                String str5 = "LibraryVersion";
                String str6 = "Failed to get app version for libraryName: ";
                String valueOf2 = String.valueOf(str);
                gmsLogger2.e(str5, valueOf2.length() != 0 ? str6.concat(valueOf2) : new String(str6));
            }
        } catch (IOException e2) {
            e = e2;
            GmsLogger gmsLogger3 = zzel;
            String str32 = "LibraryVersion";
            String str42 = "Failed to get app version for libraryName: ";
            String valueOf3 = String.valueOf(str);
            gmsLogger3.e(str32, valueOf3.length() == 0 ? str42.concat(valueOf3) : new String(str42), e);
            if (str2 == null) {
            }
            this.zzen.put(str, str2);
            return str2;
        }
        if (str2 == null) {
            str2 = "UNKNOWN";
            zzel.d("LibraryVersion", ".properties file is dropped during release process. Failure to read app version isexpected druing Google internal testing where locally-built libraries are used");
        }
        this.zzen.put(str, str2);
        return str2;
    }
}
