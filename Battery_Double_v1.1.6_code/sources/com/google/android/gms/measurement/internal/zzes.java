package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks;
import com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzes implements ServiceConnection, BaseConnectionCallbacks, BaseOnConnectionFailedListener {
    final /* synthetic */ zzeb zzasi;
    /* access modifiers changed from: private */
    public volatile boolean zzaso;
    private volatile zzar zzasp;

    protected zzes(zzeb zzeb) {
        this.zzasi = zzeb;
    }

    @WorkerThread
    public final void zzb(Intent intent) {
        this.zzasi.zzaf();
        Context context = this.zzasi.getContext();
        ConnectionTracker instance = ConnectionTracker.getInstance();
        synchronized (this) {
            if (this.zzaso) {
                this.zzasi.zzgt().zzjo().zzby("Connection attempt already in progress");
                return;
            }
            this.zzasi.zzgt().zzjo().zzby("Using local app measurement service");
            this.zzaso = true;
            instance.bindService(context, intent, this.zzasi.zzasb, TsExtractor.TS_STREAM_TYPE_AC3);
        }
    }

    @WorkerThread
    public final void zzlk() {
        if (this.zzasp != null && (this.zzasp.isConnected() || this.zzasp.isConnecting())) {
            this.zzasp.disconnect();
        }
        this.zzasp = null;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r3.zzasi.zzgt().zzjg().zzby("Service connect failed to get IMeasurementService");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0063 */
    @MainThread
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        zzaj zzal;
        Preconditions.checkMainThread("MeasurementServiceConnection.onServiceConnected");
        synchronized (this) {
            if (iBinder == null) {
                this.zzaso = false;
                this.zzasi.zzgt().zzjg().zzby("Service connected with null binder");
                return;
            }
            zzaj zzaj = null;
            String interfaceDescriptor = iBinder.getInterfaceDescriptor();
            if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                if (iBinder != null) {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (queryLocalInterface instanceof zzaj) {
                        zzal = (zzaj) queryLocalInterface;
                    } else {
                        zzal = new zzal(iBinder);
                    }
                    zzaj = zzal;
                }
                this.zzasi.zzgt().zzjo().zzby("Bound to IMeasurementService interface");
            } else {
                this.zzasi.zzgt().zzjg().zzg("Got binder with a wrong descriptor", interfaceDescriptor);
            }
            if (zzaj == null) {
                this.zzaso = false;
                try {
                    ConnectionTracker.getInstance().unbindService(this.zzasi.getContext(), this.zzasi.zzasb);
                } catch (IllegalArgumentException unused) {
                }
            } else {
                this.zzasi.zzgs().zzc((Runnable) new zzet(this, zzaj));
            }
        }
    }

    @MainThread
    public final void onServiceDisconnected(ComponentName componentName) {
        Preconditions.checkMainThread("MeasurementServiceConnection.onServiceDisconnected");
        this.zzasi.zzgt().zzjn().zzby("Service disconnected");
        this.zzasi.zzgs().zzc((Runnable) new zzeu(this, componentName));
    }

    @WorkerThread
    public final void zzll() {
        this.zzasi.zzaf();
        Context context = this.zzasi.getContext();
        synchronized (this) {
            if (this.zzaso) {
                this.zzasi.zzgt().zzjo().zzby("Connection attempt already in progress");
            } else if (this.zzasp == null || (!this.zzasp.isConnecting() && !this.zzasp.isConnected())) {
                this.zzasp = new zzar(context, Looper.getMainLooper(), this, this);
                this.zzasi.zzgt().zzjo().zzby("Connecting to remote service");
                this.zzaso = true;
                this.zzasp.checkAvailabilityAndConnect();
            } else {
                this.zzasi.zzgt().zzjo().zzby("Already awaiting connection attempt");
            }
        }
    }

    @MainThread
    public final void onConnected(@Nullable Bundle bundle) {
        Preconditions.checkMainThread("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.zzasi.zzgs().zzc((Runnable) new zzev(this, (zzaj) this.zzasp.getService()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.zzasp = null;
                this.zzaso = false;
            }
        }
    }

    @MainThread
    public final void onConnectionSuspended(int i) {
        Preconditions.checkMainThread("MeasurementServiceConnection.onConnectionSuspended");
        this.zzasi.zzgt().zzjn().zzby("Service connection suspended");
        this.zzasi.zzgs().zzc((Runnable) new zzew(this));
    }

    @MainThread
    public final void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Preconditions.checkMainThread("MeasurementServiceConnection.onConnectionFailed");
        zzas zzkj = this.zzasi.zzada.zzkj();
        if (zzkj != null) {
            zzkj.zzjj().zzg("Service connection failed", connectionResult);
        }
        synchronized (this) {
            this.zzaso = false;
            this.zzasp = null;
        }
        this.zzasi.zzgs().zzc((Runnable) new zzex(this));
    }
}
