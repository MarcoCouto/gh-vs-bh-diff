package com.google.android.gms.measurement.internal;

import android.os.Process;
import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.BlockingQueue;

final class zzbv extends Thread {
    private final /* synthetic */ zzbr zzapb;
    private final Object zzape = new Object();
    private final BlockingQueue<zzbu<?>> zzapf;

    public zzbv(zzbr zzbr, String str, BlockingQueue<zzbu<?>> blockingQueue) {
        this.zzapb = zzbr;
        Preconditions.checkNotNull(str);
        Preconditions.checkNotNull(blockingQueue);
        this.zzapf = blockingQueue;
        setName(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0065, code lost:
        r1 = r6.zzapb.zzaow;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x006b, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r6.zzapb.zzaox.release();
        r6.zzapb.zzaow.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0084, code lost:
        if (r6 != r6.zzapb.zzaoq) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0086, code lost:
        r6.zzapb.zzaoq = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0092, code lost:
        if (r6 != r6.zzapb.zzaor) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0094, code lost:
        r6.zzapb.zzaor = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009a, code lost:
        r6.zzapb.zzgt().zzjg().zzby("Current scheduler thread is neither worker nor network");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00a9, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00aa, code lost:
        return;
     */
    public final void run() {
        boolean z = false;
        while (!z) {
            try {
                this.zzapb.zzaox.acquire();
                z = true;
            } catch (InterruptedException e) {
                zza(e);
            }
        }
        try {
            int threadPriority = Process.getThreadPriority(Process.myTid());
            while (true) {
                zzbu zzbu = (zzbu) this.zzapf.poll();
                if (zzbu != null) {
                    Process.setThreadPriority(zzbu.zzapd ? threadPriority : 10);
                    zzbu.run();
                } else {
                    synchronized (this.zzape) {
                        if (this.zzapf.peek() == null && !this.zzapb.zzaoy) {
                            try {
                                this.zzape.wait(30000);
                            } catch (InterruptedException e2) {
                                zza(e2);
                            }
                        }
                    }
                    synchronized (this.zzapb.zzaow) {
                        if (this.zzapf.peek() == null) {
                        }
                    }
                }
            }
        } catch (Throwable th) {
            synchronized (this.zzapb.zzaow) {
                this.zzapb.zzaox.release();
                this.zzapb.zzaow.notifyAll();
                if (this == this.zzapb.zzaoq) {
                    this.zzapb.zzaoq = null;
                } else if (this == this.zzapb.zzaor) {
                    this.zzapb.zzaor = null;
                } else {
                    this.zzapb.zzgt().zzjg().zzby("Current scheduler thread is neither worker nor network");
                }
                throw th;
            }
        }
    }

    public final void zzki() {
        synchronized (this.zzape) {
            this.zzape.notifyAll();
        }
    }

    private final void zza(InterruptedException interruptedException) {
        this.zzapb.zzgt().zzjj().zzg(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }
}
