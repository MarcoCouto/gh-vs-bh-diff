package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

public final class zzao extends zzf {
    private final zzap zzalm = new zzap(this, getContext(), "google_app_measurement_local.db");
    private boolean zzaln;

    zzao(zzbw zzbw) {
        super(zzbw);
    }

    /* access modifiers changed from: protected */
    public final boolean zzgy() {
        return false;
    }

    @WorkerThread
    public final void resetAnalyticsData() {
        zzgg();
        zzaf();
        try {
            int delete = getWritableDatabase().delete("messages", null, null) + 0;
            if (delete > 0) {
                zzgt().zzjo().zzg("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzgt().zzjg().zzg("Error resetting local analytics data. error", e);
        }
    }

    /* JADX WARNING: type inference failed for: r2v0 */
    /* JADX WARNING: type inference failed for: r2v1, types: [int, boolean] */
    /* JADX WARNING: type inference failed for: r7v0 */
    /* JADX WARNING: type inference failed for: r12v0, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v0, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r9v1 */
    /* JADX WARNING: type inference failed for: r7v1 */
    /* JADX WARNING: type inference failed for: r12v1 */
    /* JADX WARNING: type inference failed for: r2v4 */
    /* JADX WARNING: type inference failed for: r9v2, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r7v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v3 */
    /* JADX WARNING: type inference failed for: r9v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v5 */
    /* JADX WARNING: type inference failed for: r12v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r7v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r9v6 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r9v7 */
    /* JADX WARNING: type inference failed for: r12v4 */
    /* JADX WARNING: type inference failed for: r9v8, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARNING: type inference failed for: r12v5, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r7v5 */
    /* JADX WARNING: type inference failed for: r7v6 */
    /* JADX WARNING: type inference failed for: r12v6 */
    /* JADX WARNING: type inference failed for: r7v7 */
    /* JADX WARNING: type inference failed for: r12v7 */
    /* JADX WARNING: type inference failed for: r2v11 */
    /* JADX WARNING: type inference failed for: r7v8 */
    /* JADX WARNING: type inference failed for: r7v9 */
    /* JADX WARNING: type inference failed for: r7v10 */
    /* JADX WARNING: type inference failed for: r7v11 */
    /* JADX WARNING: type inference failed for: r7v12 */
    /* JADX WARNING: type inference failed for: r9v9 */
    /* JADX WARNING: type inference failed for: r2v12 */
    /* JADX WARNING: type inference failed for: r9v10 */
    /* JADX WARNING: type inference failed for: r9v11 */
    /* JADX WARNING: type inference failed for: r7v13 */
    /* JADX WARNING: type inference failed for: r7v14 */
    /* JADX WARNING: type inference failed for: r9v12 */
    /* JADX WARNING: type inference failed for: r9v13 */
    /* JADX WARNING: type inference failed for: r7v15 */
    /* JADX WARNING: type inference failed for: r7v16 */
    /* JADX WARNING: type inference failed for: r12v8 */
    /* JADX WARNING: type inference failed for: r9v14 */
    /* JADX WARNING: type inference failed for: r9v15 */
    /* JADX WARNING: type inference failed for: r9v16 */
    /* JADX WARNING: type inference failed for: r9v17 */
    /* JADX WARNING: type inference failed for: r9v18 */
    /* JADX WARNING: type inference failed for: r12v9 */
    /* JADX WARNING: type inference failed for: r12v10 */
    /* JADX WARNING: type inference failed for: r12v11 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r2v1, types: [int, boolean]
  assigns: []
  uses: [?[int, short, byte, char], int, boolean]
  mth insns count: 163
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00cb A[SYNTHETIC, Splitter:B:49:0x00cb] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x011c  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x011f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x011f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x011f A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 19 */
    @WorkerThread
    private final boolean zza(int i, byte[] bArr) {
        ? r12;
        ? r9;
        ? r92;
        ? r7;
        ? r93;
        ? r72;
        ? r94;
        ? r73;
        ? r122;
        ? r74;
        ? r75;
        ? r76;
        ? r123;
        zzgg();
        zzaf();
        ? r2 = 0;
        if (this.zzaln) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(i));
        contentValues.put("entry", bArr);
        int i2 = 5;
        int i3 = 0;
        int i4 = 5;
        ? r22 = r2;
        while (i3 < i2) {
            ? r77 = 0;
            try {
                ? writableDatabase = getWritableDatabase();
                if (writableDatabase == 0) {
                    try {
                        this.zzaln = true;
                        if (writableDatabase != 0) {
                            writableDatabase.close();
                        }
                        return r22;
                    } catch (SQLiteFullException e) {
                        e = e;
                        r76 = r77;
                        r93 = writableDatabase;
                    } catch (SQLiteDatabaseLockedException unused) {
                        r75 = r77;
                        r94 = writableDatabase;
                        try {
                            r92 = r94;
                            r7 = r73;
                            SystemClock.sleep((long) i4);
                            r92 = r94;
                            r7 = r73;
                            i4 += 20;
                            if (r73 != 0) {
                            }
                            if (r94 != 0) {
                            }
                            i3++;
                            i2 = 5;
                            r22 = 0;
                        } catch (Throwable th) {
                            th = th;
                            r12 = r7;
                            r9 = r92;
                            if (r12 != 0) {
                            }
                            if (r9 != 0) {
                            }
                            throw th;
                        }
                    } catch (SQLiteException e2) {
                        e = e2;
                        r123 = 0;
                        r74 = writableDatabase;
                        r122 = r123;
                        if (r74 != 0) {
                        }
                        zzgt().zzjg().zzg("Error writing entry to local database", e);
                        this.zzaln = true;
                        if (r122 != 0) {
                        }
                        if (r74 != 0) {
                        }
                        i3++;
                        i2 = 5;
                        r22 = 0;
                    }
                } else {
                    writableDatabase.beginTransaction();
                    long j = 0;
                    ? rawQuery = writableDatabase.rawQuery("select count(1) from messages", null);
                    if (rawQuery != 0) {
                        try {
                            if (rawQuery.moveToFirst()) {
                                j = rawQuery.getLong(r22);
                            }
                        } catch (SQLiteFullException e3) {
                            e = e3;
                            r76 = rawQuery;
                            r93 = writableDatabase;
                            r92 = r93;
                            r7 = r72;
                            zzgt().zzjg().zzg("Error writing entry to local database", e);
                            this.zzaln = true;
                            r92 = r93;
                            r7 = r72;
                            if (r72 != 0) {
                            }
                            if (r93 == 0) {
                            }
                            i3++;
                            i2 = 5;
                            r22 = 0;
                        } catch (SQLiteDatabaseLockedException unused2) {
                            r75 = rawQuery;
                            r94 = writableDatabase;
                            r92 = r94;
                            r7 = r73;
                            SystemClock.sleep((long) i4);
                            r92 = r94;
                            r7 = r73;
                            i4 += 20;
                            if (r73 != 0) {
                            }
                            if (r94 != 0) {
                            }
                            i3++;
                            i2 = 5;
                            r22 = 0;
                        } catch (SQLiteException e4) {
                            e = e4;
                            r123 = rawQuery;
                            r74 = writableDatabase;
                            r122 = r123;
                            if (r74 != 0) {
                            }
                            zzgt().zzjg().zzg("Error writing entry to local database", e);
                            this.zzaln = true;
                            if (r122 != 0) {
                            }
                            if (r74 != 0) {
                            }
                            i3++;
                            i2 = 5;
                            r22 = 0;
                        } catch (Throwable th2) {
                            th = th2;
                            r9 = writableDatabase;
                            r12 = rawQuery;
                            if (r12 != 0) {
                            }
                            if (r9 != 0) {
                            }
                            throw th;
                        }
                    }
                    if (j >= 100000) {
                        zzgt().zzjg().zzby("Data loss, local db full");
                        long j2 = (100000 - j) + 1;
                        String[] strArr = new String[1];
                        strArr[r22] = Long.toString(j2);
                        long delete = (long) writableDatabase.delete("messages", "rowid in (select rowid from messages order by rowid asc limit ?)", strArr);
                        if (delete != j2) {
                            zzgt().zzjg().zzd("Different delete count than expected in local db. expected, received, difference", Long.valueOf(j2), Long.valueOf(delete), Long.valueOf(j2 - delete));
                        }
                    }
                    writableDatabase.insertOrThrow("messages", null, contentValues);
                    writableDatabase.setTransactionSuccessful();
                    writableDatabase.endTransaction();
                    if (rawQuery != 0) {
                        rawQuery.close();
                    }
                    if (writableDatabase != 0) {
                        writableDatabase.close();
                    }
                    return true;
                }
            } catch (SQLiteFullException e5) {
                e = e5;
                r93 = 0;
                r76 = r77;
                r92 = r93;
                r7 = r72;
                zzgt().zzjg().zzg("Error writing entry to local database", e);
                this.zzaln = true;
                r92 = r93;
                r7 = r72;
                if (r72 != 0) {
                    r72.close();
                }
                if (r93 == 0) {
                    r93.close();
                }
                i3++;
                i2 = 5;
                r22 = 0;
            } catch (SQLiteDatabaseLockedException unused3) {
                r94 = 0;
                r75 = r77;
                r92 = r94;
                r7 = r73;
                SystemClock.sleep((long) i4);
                r92 = r94;
                r7 = r73;
                i4 += 20;
                if (r73 != 0) {
                    r73.close();
                }
                if (r94 != 0) {
                    r94.close();
                }
                i3++;
                i2 = 5;
                r22 = 0;
            } catch (SQLiteException e6) {
                e = e6;
                r122 = 0;
                r74 = r77;
                if (r74 != 0) {
                    try {
                        if (r74.inTransaction()) {
                            r74.endTransaction();
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        r9 = r74;
                        r12 = r122;
                        if (r12 != 0) {
                        }
                        if (r9 != 0) {
                        }
                        throw th;
                    }
                }
                zzgt().zzjg().zzg("Error writing entry to local database", e);
                this.zzaln = true;
                if (r122 != 0) {
                    r122.close();
                }
                if (r74 != 0) {
                    r74.close();
                }
                i3++;
                i2 = 5;
                r22 = 0;
            } catch (Throwable th4) {
                th = th4;
                r9 = 0;
                r12 = 0;
                if (r12 != 0) {
                    r12.close();
                }
                if (r9 != 0) {
                    r9.close();
                }
                throw th;
            }
        }
        zzgt().zzjj().zzby("Failed to write entry to local database");
        return false;
    }

    public final boolean zza(zzag zzag) {
        Parcel obtain = Parcel.obtain();
        zzag.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(0, marshall);
        }
        zzgt().zzjj().zzby("Event is too long for local database. Sending event directly to service");
        return false;
    }

    public final boolean zza(zzfv zzfv) {
        Parcel obtain = Parcel.obtain();
        zzfv.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(1, marshall);
        }
        zzgt().zzjj().zzby("User property too long for local database. Sending directly to service");
        return false;
    }

    public final boolean zzc(zzo zzo) {
        zzgr();
        byte[] zza = zzfy.zza((Parcelable) zzo);
        if (zza.length <= 131072) {
            return zza(2, zza);
        }
        zzgt().zzjj().zzby("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    /* JADX WARNING: type inference failed for: r2v0 */
    /* JADX WARNING: type inference failed for: r2v1, types: [java.util.List<com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable>] */
    /* JADX WARNING: type inference failed for: r9v0, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r2v5 */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v1 */
    /* JADX WARNING: type inference failed for: r2v8 */
    /* JADX WARNING: type inference failed for: r2v9, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v3 */
    /* JADX WARNING: type inference failed for: r2v11 */
    /* JADX WARNING: type inference failed for: r9v4, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r9v5 */
    /* JADX WARNING: type inference failed for: r9v6 */
    /* JADX WARNING: type inference failed for: r2v18 */
    /* JADX WARNING: type inference failed for: r2v19 */
    /* JADX WARNING: type inference failed for: r2v24 */
    /* JADX WARNING: type inference failed for: r2v25 */
    /* JADX WARNING: type inference failed for: r9v10 */
    /* JADX WARNING: type inference failed for: r2v28 */
    /* JADX WARNING: type inference failed for: r2v29 */
    /* JADX WARNING: type inference failed for: r2v30 */
    /* JADX WARNING: type inference failed for: r2v31 */
    /* JADX WARNING: type inference failed for: r2v32 */
    /* JADX WARNING: type inference failed for: r9v11 */
    /* JADX WARNING: type inference failed for: r9v12 */
    /* JADX WARNING: type inference failed for: r9v13 */
    /* JADX WARNING: type inference failed for: r2v33 */
    /* JADX WARNING: type inference failed for: r2v34 */
    /* JADX WARNING: type inference failed for: r9v14 */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:54|55|56|57) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:69|70|71|72) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:41|42|43|44|165) */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0199, code lost:
        if (r2.inTransaction() != false) goto L_0x019b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x019b, code lost:
        r2.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x019f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x01a0, code lost:
        r9 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x01b2, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x01b7, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x01c5, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x01ca, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        r9 = r2;
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003e, code lost:
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        r0 = e;
        r2 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        zzgt().zzjg().zzby("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r13.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        zzgt().zzjg().zzby("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        r13.recycle();
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        zzgt().zzjg().zzby("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r13.recycle();
        r0 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00a8 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:54:0x00d8 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:69:0x010e */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0195 A[SYNTHETIC, Splitter:B:112:0x0195] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x01b2  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x01ca  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x01eb  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x01f9  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x01ee A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x01ee A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x01ee A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:12:0x0031] */
    /* JADX WARNING: Unknown variable types count: 13 */
    public final List<AbstractSafeParcelable> zzr(int i) {
        ? r9;
        SQLiteDatabase sQLiteDatabase;
        SQLiteDatabase sQLiteDatabase2;
        ? r2;
        SQLiteDatabase sQLiteDatabase3;
        ? r22;
        ? r92;
        Parcel obtain;
        Parcel obtain2;
        Parcel obtain3;
        zzaf();
        zzgg();
        ? r23 = 0;
        if (this.zzaln) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!getContext().getDatabasePath("google_app_measurement_local.db").exists()) {
            return arrayList;
        }
        int i2 = 5;
        int i3 = 0;
        int i4 = 5;
        ? r24 = r23;
        while (i3 < i2) {
            try {
                sQLiteDatabase2 = getWritableDatabase();
                if (sQLiteDatabase2 == null) {
                    try {
                        this.zzaln = true;
                        if (sQLiteDatabase2 != null) {
                            sQLiteDatabase2.close();
                        }
                        return r24;
                    } catch (SQLiteFullException e) {
                        e = e;
                        SQLiteDatabase sQLiteDatabase4 = sQLiteDatabase2;
                        r2 = 0;
                        try {
                            zzgt().zzjg().zzg("Error reading entries from local database", e);
                            this.zzaln = true;
                            if (r2 != 0) {
                            }
                            if (sQLiteDatabase2 != null) {
                            }
                            i3++;
                            i2 = 5;
                            r24 = 0;
                        } catch (Throwable th) {
                            th = th;
                            r9 = r2;
                            sQLiteDatabase = sQLiteDatabase2;
                            if (r9 != 0) {
                                r9.close();
                            }
                            if (sQLiteDatabase != null) {
                                sQLiteDatabase.close();
                            }
                            throw th;
                        }
                    } catch (SQLiteDatabaseLockedException unused) {
                    } catch (SQLiteException e2) {
                        e = e2;
                        sQLiteDatabase = sQLiteDatabase2;
                        r92 = 0;
                        if (sQLiteDatabase != null) {
                        }
                        zzgt().zzjg().zzg("Error reading entries from local database", e);
                        this.zzaln = true;
                        if (r92 != 0) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        i3++;
                        i2 = 5;
                        r24 = 0;
                    } catch (Throwable th2) {
                        th = th2;
                        sQLiteDatabase = sQLiteDatabase2;
                        r9 = 0;
                        if (r9 != 0) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                } else {
                    sQLiteDatabase2.beginTransaction();
                    sQLiteDatabase = sQLiteDatabase2;
                    try {
                        Cursor query = sQLiteDatabase2.query("messages", new String[]{"rowid", "type", "entry"}, null, null, null, null, "rowid asc", Integer.toString(100));
                        long j = -1;
                        while (query.moveToNext()) {
                            try {
                                j = query.getLong(0);
                                int i5 = query.getInt(1);
                                byte[] blob = query.getBlob(2);
                                if (i5 == 0) {
                                    obtain3 = Parcel.obtain();
                                    obtain3.unmarshall(blob, 0, blob.length);
                                    obtain3.setDataPosition(0);
                                    zzag zzag = (zzag) zzag.CREATOR.createFromParcel(obtain3);
                                    obtain3.recycle();
                                    if (zzag != null) {
                                        arrayList.add(zzag);
                                    }
                                } else if (i5 == 1) {
                                    obtain2 = Parcel.obtain();
                                    obtain2.unmarshall(blob, 0, blob.length);
                                    obtain2.setDataPosition(0);
                                    zzfv zzfv = (zzfv) zzfv.CREATOR.createFromParcel(obtain2);
                                    obtain2.recycle();
                                    if (zzfv != null) {
                                        arrayList.add(zzfv);
                                    }
                                } else if (i5 == 2) {
                                    obtain = Parcel.obtain();
                                    obtain.unmarshall(blob, 0, blob.length);
                                    obtain.setDataPosition(0);
                                    zzo zzo = (zzo) zzo.CREATOR.createFromParcel(obtain);
                                    obtain.recycle();
                                    if (zzo != null) {
                                        arrayList.add(zzo);
                                    }
                                } else {
                                    zzgt().zzjg().zzby("Unknown record type in local database");
                                }
                            } catch (SQLiteFullException e3) {
                                e = e3;
                                sQLiteDatabase2 = sQLiteDatabase;
                                r2 = query;
                            } catch (SQLiteDatabaseLockedException unused2) {
                                sQLiteDatabase3 = sQLiteDatabase;
                                r22 = query;
                                try {
                                    SystemClock.sleep((long) i4);
                                    i4 += 20;
                                    if (r22 != 0) {
                                    }
                                    if (sQLiteDatabase3 == null) {
                                    }
                                    i3++;
                                    i2 = 5;
                                    r24 = 0;
                                } catch (Throwable th3) {
                                    th = th3;
                                    r9 = r22;
                                    sQLiteDatabase = sQLiteDatabase3;
                                    if (r9 != 0) {
                                    }
                                    if (sQLiteDatabase != null) {
                                    }
                                    throw th;
                                }
                            } catch (SQLiteException e4) {
                                e = e4;
                                r92 = query;
                                if (sQLiteDatabase != null) {
                                }
                                zzgt().zzjg().zzg("Error reading entries from local database", e);
                                this.zzaln = true;
                                if (r92 != 0) {
                                }
                                if (sQLiteDatabase != null) {
                                }
                                i3++;
                                i2 = 5;
                                r24 = 0;
                            } catch (Throwable th4) {
                                obtain3.recycle();
                                throw th4;
                            }
                        }
                        if (sQLiteDatabase.delete("messages", "rowid <= ?", new String[]{Long.toString(j)}) < arrayList.size()) {
                            zzgt().zzjg().zzby("Fewer entries removed from local database than expected");
                        }
                        sQLiteDatabase.setTransactionSuccessful();
                        sQLiteDatabase.endTransaction();
                        if (query != 0) {
                            query.close();
                        }
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        return arrayList;
                    } catch (SQLiteFullException e5) {
                        e = e5;
                        sQLiteDatabase2 = sQLiteDatabase;
                        r2 = 0;
                        zzgt().zzjg().zzg("Error reading entries from local database", e);
                        this.zzaln = true;
                        if (r2 != 0) {
                        }
                        if (sQLiteDatabase2 != null) {
                        }
                        i3++;
                        i2 = 5;
                        r24 = 0;
                    } catch (SQLiteDatabaseLockedException unused3) {
                        sQLiteDatabase3 = sQLiteDatabase;
                        r22 = 0;
                        SystemClock.sleep((long) i4);
                        i4 += 20;
                        if (r22 != 0) {
                        }
                        if (sQLiteDatabase3 == null) {
                        }
                        i3++;
                        i2 = 5;
                        r24 = 0;
                    } catch (SQLiteException e6) {
                        e = e6;
                        r92 = 0;
                        if (sQLiteDatabase != null) {
                        }
                        zzgt().zzjg().zzg("Error reading entries from local database", e);
                        this.zzaln = true;
                        if (r92 != 0) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        i3++;
                        i2 = 5;
                        r24 = 0;
                    } catch (Throwable th5) {
                        th = th5;
                        r9 = 0;
                        if (r9 != 0) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteFullException e7) {
                e = e7;
                sQLiteDatabase2 = null;
                r2 = 0;
                zzgt().zzjg().zzg("Error reading entries from local database", e);
                this.zzaln = true;
                if (r2 != 0) {
                    r2.close();
                }
                if (sQLiteDatabase2 != null) {
                    sQLiteDatabase2.close();
                }
                i3++;
                i2 = 5;
                r24 = 0;
            } catch (SQLiteDatabaseLockedException unused4) {
                sQLiteDatabase3 = null;
                r22 = 0;
                SystemClock.sleep((long) i4);
                i4 += 20;
                if (r22 != 0) {
                }
                if (sQLiteDatabase3 == null) {
                }
                i3++;
                i2 = 5;
                r24 = 0;
            } catch (SQLiteException e8) {
                e = e8;
                sQLiteDatabase = null;
                r92 = 0;
                if (sQLiteDatabase != null) {
                }
                zzgt().zzjg().zzg("Error reading entries from local database", e);
                this.zzaln = true;
                if (r92 != 0) {
                }
                if (sQLiteDatabase != null) {
                }
                i3++;
                i2 = 5;
                r24 = 0;
            } catch (Throwable th6) {
                th = th6;
                sQLiteDatabase = null;
                r9 = 0;
                if (r9 != 0) {
                }
                if (sQLiteDatabase != null) {
                }
                throw th;
            }
        }
        zzgt().zzjj().zzby("Failed to read events from database in reasonable time");
        return null;
    }

    @WorkerThread
    @VisibleForTesting
    private final SQLiteDatabase getWritableDatabase() throws SQLiteException {
        if (this.zzaln) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.zzalm.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.zzaln = true;
        return null;
    }

    public final /* bridge */ /* synthetic */ void zzgf() {
        super.zzgf();
    }

    public final /* bridge */ /* synthetic */ void zzgg() {
        super.zzgg();
    }

    public final /* bridge */ /* synthetic */ void zzgh() {
        super.zzgh();
    }

    public final /* bridge */ /* synthetic */ void zzaf() {
        super.zzaf();
    }

    public final /* bridge */ /* synthetic */ zza zzgi() {
        return super.zzgi();
    }

    public final /* bridge */ /* synthetic */ zzda zzgj() {
        return super.zzgj();
    }

    public final /* bridge */ /* synthetic */ zzam zzgk() {
        return super.zzgk();
    }

    public final /* bridge */ /* synthetic */ zzeb zzgl() {
        return super.zzgl();
    }

    public final /* bridge */ /* synthetic */ zzdy zzgm() {
        return super.zzgm();
    }

    public final /* bridge */ /* synthetic */ zzao zzgn() {
        return super.zzgn();
    }

    public final /* bridge */ /* synthetic */ zzfd zzgo() {
        return super.zzgo();
    }

    public final /* bridge */ /* synthetic */ zzaa zzgp() {
        return super.zzgp();
    }

    public final /* bridge */ /* synthetic */ Clock zzbx() {
        return super.zzbx();
    }

    public final /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public final /* bridge */ /* synthetic */ zzaq zzgq() {
        return super.zzgq();
    }

    public final /* bridge */ /* synthetic */ zzfy zzgr() {
        return super.zzgr();
    }

    public final /* bridge */ /* synthetic */ zzbr zzgs() {
        return super.zzgs();
    }

    public final /* bridge */ /* synthetic */ zzas zzgt() {
        return super.zzgt();
    }

    public final /* bridge */ /* synthetic */ zzbd zzgu() {
        return super.zzgu();
    }

    public final /* bridge */ /* synthetic */ zzq zzgv() {
        return super.zzgv();
    }

    public final /* bridge */ /* synthetic */ zzn zzgw() {
        return super.zzgw();
    }
}
