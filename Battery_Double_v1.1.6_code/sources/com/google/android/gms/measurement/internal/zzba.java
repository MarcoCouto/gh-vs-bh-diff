package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.Preconditions;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

@WorkerThread
final class zzba implements Runnable {
    private final String packageName;
    private final URL url;
    private final byte[] zzamr;
    private final zzay zzams;
    private final Map<String, String> zzamt;
    private final /* synthetic */ zzaw zzamu;

    public zzba(zzaw zzaw, String str, URL url2, byte[] bArr, Map<String, String> map, zzay zzay) {
        this.zzamu = zzaw;
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotNull(url2);
        Preconditions.checkNotNull(zzay);
        this.url = url2;
        this.zzamr = bArr;
        this.zzams = zzay;
        this.packageName = str;
        this.zzamt = map;
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c4 A[SYNTHETIC, Splitter:B:44:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0101 A[SYNTHETIC, Splitter:B:57:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x011d  */
    public final void run() {
        Map map;
        Throwable th;
        int i;
        HttpURLConnection httpURLConnection;
        Map map2;
        this.zzamu.zzgh();
        OutputStream outputStream = null;
        try {
            httpURLConnection = this.zzamu.zzb(this.url);
            try {
                if (this.zzamt != null) {
                    for (Entry entry : this.zzamt.entrySet()) {
                        httpURLConnection.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
                    }
                }
                if (this.zzamr != null) {
                    byte[] zzb = this.zzamu.zzjr().zzb(this.zzamr);
                    this.zzamu.zzgt().zzjo().zzg("Uploading data. size", Integer.valueOf(zzb.length));
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.addRequestProperty(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
                    httpURLConnection.setFixedLengthStreamingMode(zzb.length);
                    httpURLConnection.connect();
                    OutputStream outputStream2 = httpURLConnection.getOutputStream();
                    try {
                        outputStream2.write(zzb);
                        outputStream2.close();
                    } catch (IOException e) {
                        map2 = null;
                        th = e;
                        outputStream = outputStream2;
                    } catch (Throwable th2) {
                        th = th2;
                        map = null;
                        outputStream = outputStream2;
                        i = 0;
                        if (outputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        zzbr zzgs = this.zzamu.zzgs();
                        zzaz zzaz = new zzaz(this.packageName, this.zzams, i, null, null, map);
                        zzgs.zzc((Runnable) zzaz);
                        throw th;
                    }
                }
                i = httpURLConnection.getResponseCode();
            } catch (IOException e2) {
                e = e2;
                map2 = null;
                th = e;
                i = 0;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                zzbr zzgs2 = this.zzamu.zzgs();
                zzaz zzaz2 = new zzaz(this.packageName, this.zzams, i, th, null, map);
                zzgs2.zzc((Runnable) zzaz2);
            } catch (Throwable th3) {
                th = th3;
                map = null;
                i = 0;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                zzbr zzgs3 = this.zzamu.zzgs();
                zzaz zzaz3 = new zzaz(this.packageName, this.zzams, i, null, null, map);
                zzgs3.zzc((Runnable) zzaz3);
                throw th;
            }
            try {
                map = httpURLConnection.getHeaderFields();
                try {
                    byte[] zza = zzaw.zzb(httpURLConnection);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    zzbr zzgs4 = this.zzamu.zzgs();
                    zzaz zzaz4 = new zzaz(this.packageName, this.zzams, i, null, zza, map);
                    zzgs4.zzc((Runnable) zzaz4);
                } catch (IOException e3) {
                    e = e3;
                    th = e;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    zzbr zzgs22 = this.zzamu.zzgs();
                    zzaz zzaz22 = new zzaz(this.packageName, this.zzams, i, th, null, map);
                    zzgs22.zzc((Runnable) zzaz22);
                } catch (Throwable th4) {
                    th = th4;
                    if (outputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    zzbr zzgs32 = this.zzamu.zzgs();
                    zzaz zzaz32 = new zzaz(this.packageName, this.zzams, i, null, null, map);
                    zzgs32.zzc((Runnable) zzaz32);
                    throw th;
                }
            } catch (IOException e4) {
                e = e4;
                map = null;
                th = e;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                zzbr zzgs222 = this.zzamu.zzgs();
                zzaz zzaz222 = new zzaz(this.packageName, this.zzams, i, th, null, map);
                zzgs222.zzc((Runnable) zzaz222);
            } catch (Throwable th5) {
                th = th5;
                map = null;
                if (outputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                zzbr zzgs322 = this.zzamu.zzgs();
                zzaz zzaz322 = new zzaz(this.packageName, this.zzams, i, null, null, map);
                zzgs322.zzc((Runnable) zzaz322);
                throw th;
            }
        } catch (IOException e5) {
            e = e5;
            httpURLConnection = null;
            map2 = null;
            th = e;
            i = 0;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e6) {
                    this.zzamu.zzgt().zzjg().zze("Error closing HTTP compressed POST connection output stream. appId", zzas.zzbw(this.packageName), e6);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            zzbr zzgs2222 = this.zzamu.zzgs();
            zzaz zzaz2222 = new zzaz(this.packageName, this.zzams, i, th, null, map);
            zzgs2222.zzc((Runnable) zzaz2222);
        } catch (Throwable th6) {
            th = th6;
            httpURLConnection = null;
            map = null;
            i = 0;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e7) {
                    this.zzamu.zzgt().zzjg().zze("Error closing HTTP compressed POST connection output stream. appId", zzas.zzbw(this.packageName), e7);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            zzbr zzgs3222 = this.zzamu.zzgs();
            zzaz zzaz3222 = new zzaz(this.packageName, this.zzams, i, null, null, map);
            zzgs3222.zzc((Runnable) zzaz3222);
            throw th;
        }
    }
}
