package com.google.android.gms.measurement.internal;

import android.support.annotation.NonNull;

final class zzav {
    /* access modifiers changed from: private */
    public final String zzaml;

    public zzav(@NonNull String str) {
        this.zzaml = str;
    }
}
