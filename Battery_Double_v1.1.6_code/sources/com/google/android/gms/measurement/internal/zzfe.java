package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;

final class zzfe extends zzy {
    private final /* synthetic */ zzfd zzatd;

    zzfe(zzfd zzfd, zzct zzct) {
        this.zzatd = zzfd;
        super(zzct);
    }

    @WorkerThread
    public final void run() {
        this.zzatd.zzlo();
    }
}
