package com.google.zxing.pdf417.decoder;

import com.google.zxing.FormatException;
import com.google.zxing.common.CharacterSetECI;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.pdf417.PDF417ResultMetadata;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.Arrays;

final class DecodedBitStreamParser {
    private static final int AL = 28;
    private static final int AS = 27;
    private static final int BEGIN_MACRO_PDF417_CONTROL_BLOCK = 928;
    private static final int BEGIN_MACRO_PDF417_OPTIONAL_FIELD = 923;
    private static final int BYTE_COMPACTION_MODE_LATCH = 901;
    private static final int BYTE_COMPACTION_MODE_LATCH_6 = 924;
    private static final Charset DEFAULT_ENCODING = Charset.forName("ISO-8859-1");
    private static final int ECI_CHARSET = 927;
    private static final int ECI_GENERAL_PURPOSE = 926;
    private static final int ECI_USER_DEFINED = 925;
    private static final BigInteger[] EXP900;
    private static final int LL = 27;
    private static final int MACRO_PDF417_TERMINATOR = 922;
    private static final int MAX_NUMERIC_CODEWORDS = 15;
    private static final char[] MIXED_CHARS = "0123456789&\r\t,:#-.$/+%*=^".toCharArray();
    private static final int ML = 28;
    private static final int MODE_SHIFT_TO_BYTE_COMPACTION_MODE = 913;
    private static final int NUMBER_OF_SEQUENCE_CODEWORDS = 2;
    private static final int NUMERIC_COMPACTION_MODE_LATCH = 902;
    private static final int PAL = 29;
    private static final int PL = 25;
    private static final int PS = 29;
    private static final char[] PUNCT_CHARS = ";<>@[\\]_`~!\r\t,:\n-.$/\"|*()?{}'".toCharArray();
    private static final int TEXT_COMPACTION_MODE_LATCH = 900;

    private enum Mode {
        ALPHA,
        LOWER,
        MIXED,
        PUNCT,
        ALPHA_SHIFT,
        PUNCT_SHIFT
    }

    static {
        BigInteger[] bigIntegerArr = new BigInteger[16];
        EXP900 = bigIntegerArr;
        bigIntegerArr[0] = BigInteger.ONE;
        BigInteger valueOf = BigInteger.valueOf(900);
        EXP900[1] = valueOf;
        for (int i = 2; i < EXP900.length; i++) {
            EXP900[i] = EXP900[i - 1].multiply(valueOf);
        }
    }

    private DecodedBitStreamParser() {
    }

    static DecoderResult decode(int[] iArr, String str) throws FormatException {
        int i;
        StringBuilder sb = new StringBuilder(iArr.length << 1);
        Charset charset = DEFAULT_ENCODING;
        int i2 = iArr[1];
        PDF417ResultMetadata pDF417ResultMetadata = new PDF417ResultMetadata();
        int i3 = 2;
        while (i3 < iArr[0]) {
            if (i2 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                switch (i2) {
                    case 900:
                        i = textCompaction(iArr, i3, sb);
                        break;
                    case 901:
                        i = byteCompaction(i2, iArr, charset, i3, sb);
                        break;
                    case NUMERIC_COMPACTION_MODE_LATCH /*902*/:
                        i = numericCompaction(iArr, i3, sb);
                        break;
                    default:
                        switch (i2) {
                            case MACRO_PDF417_TERMINATOR /*922*/:
                            case BEGIN_MACRO_PDF417_OPTIONAL_FIELD /*923*/:
                                throw FormatException.getFormatInstance();
                            case BYTE_COMPACTION_MODE_LATCH_6 /*924*/:
                                break;
                            case ECI_USER_DEFINED /*925*/:
                                i = i3 + 1;
                                break;
                            case ECI_GENERAL_PURPOSE /*926*/:
                                i = i3 + 2;
                                break;
                            case ECI_CHARSET /*927*/:
                                i = i3 + 1;
                                charset = Charset.forName(CharacterSetECI.getCharacterSetECIByValue(iArr[i3]).name());
                                break;
                            case 928:
                                i = decodeMacroBlock(iArr, i3, pDF417ResultMetadata);
                                break;
                            default:
                                i = textCompaction(iArr, i3 - 1, sb);
                                break;
                        }
                        i = byteCompaction(i2, iArr, charset, i3, sb);
                        break;
                }
            } else {
                i = i3 + 1;
                sb.append((char) iArr[i3]);
            }
            if (i < iArr.length) {
                i3 = i + 1;
                i2 = iArr[i];
            } else {
                throw FormatException.getFormatInstance();
            }
        }
        if (sb.length() != 0) {
            DecoderResult decoderResult = new DecoderResult(null, sb.toString(), null, str);
            decoderResult.setOther(pDF417ResultMetadata);
            return decoderResult;
        }
        throw FormatException.getFormatInstance();
    }

    private static int decodeMacroBlock(int[] iArr, int i, PDF417ResultMetadata pDF417ResultMetadata) throws FormatException {
        if (i + 2 <= iArr[0]) {
            int[] iArr2 = new int[2];
            int i2 = i;
            int i3 = 0;
            while (i3 < 2) {
                iArr2[i3] = iArr[i2];
                i3++;
                i2++;
            }
            pDF417ResultMetadata.setSegmentIndex(Integer.parseInt(decodeBase900toBase10(iArr2, 2)));
            StringBuilder sb = new StringBuilder();
            int textCompaction = textCompaction(iArr, i2, sb);
            pDF417ResultMetadata.setFileId(sb.toString());
            if (iArr[textCompaction] == BEGIN_MACRO_PDF417_OPTIONAL_FIELD) {
                int i4 = textCompaction + 1;
                int[] iArr3 = new int[(iArr[0] - i4)];
                boolean z = false;
                int i5 = 0;
                while (i4 < iArr[0] && !z) {
                    int i6 = i4 + 1;
                    int i7 = iArr[i4];
                    if (i7 < 900) {
                        int i8 = i5 + 1;
                        iArr3[i5] = i7;
                        i4 = i6;
                        i5 = i8;
                    } else if (i7 == MACRO_PDF417_TERMINATOR) {
                        pDF417ResultMetadata.setLastSegment(true);
                        i4 = i6 + 1;
                        z = true;
                    } else {
                        throw FormatException.getFormatInstance();
                    }
                }
                pDF417ResultMetadata.setOptionalData(Arrays.copyOf(iArr3, i5));
                return i4;
            } else if (iArr[textCompaction] != MACRO_PDF417_TERMINATOR) {
                return textCompaction;
            } else {
                pDF417ResultMetadata.setLastSegment(true);
                return textCompaction + 1;
            }
        } else {
            throw FormatException.getFormatInstance();
        }
    }

    private static int textCompaction(int[] iArr, int i, StringBuilder sb) {
        int[] iArr2 = new int[((iArr[0] - i) << 1)];
        int[] iArr3 = new int[((iArr[0] - i) << 1)];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i4 < 900) {
                iArr2[i2] = i4 / 30;
                iArr2[i2 + 1] = i4 % 30;
                i2 += 2;
            } else if (i4 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                if (i4 != 928) {
                    switch (i4) {
                        case 900:
                            int i5 = i2 + 1;
                            iArr2[i2] = 900;
                            i2 = i5;
                            break;
                        case 901:
                        case NUMERIC_COMPACTION_MODE_LATCH /*902*/:
                            break;
                        default:
                            switch (i4) {
                                case MACRO_PDF417_TERMINATOR /*922*/:
                                case BEGIN_MACRO_PDF417_OPTIONAL_FIELD /*923*/:
                                case BYTE_COMPACTION_MODE_LATCH_6 /*924*/:
                                    break;
                            }
                    }
                }
                i = i3 - 1;
                z = true;
            } else {
                iArr2[i2] = MODE_SHIFT_TO_BYTE_COMPACTION_MODE;
                i = i3 + 1;
                iArr3[i2] = iArr[i3];
                i2++;
            }
            i = i3;
        }
        decodeTextCompaction(iArr2, iArr3, i2, sb);
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0049, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0056, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0083, code lost:
        r4 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00b4, code lost:
        r5 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00d6, code lost:
        r3 = ' ';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00f6, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00f7, code lost:
        if (r3 == 0) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x00f9, code lost:
        r0.append(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00fc, code lost:
        r2 = r2 + 1;
     */
    private static void decodeTextCompaction(int[] iArr, int[] iArr2, int i, StringBuilder sb) {
        char c;
        Mode mode;
        StringBuilder sb2 = sb;
        Mode mode2 = Mode.ALPHA;
        Mode mode3 = Mode.ALPHA;
        int i2 = 0;
        int i3 = i;
        while (i2 < i3) {
            int i4 = iArr[i2];
            switch (mode2) {
                case ALPHA:
                    if (i4 >= 26) {
                        if (i4 != 26) {
                            if (i4 != 27) {
                                if (i4 != 28) {
                                    if (i4 != 29) {
                                        if (i4 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                            if (i4 == 900) {
                                                mode = Mode.ALPHA;
                                                break;
                                            }
                                        } else {
                                            sb2.append((char) iArr2[i2]);
                                            break;
                                        }
                                    } else {
                                        mode = Mode.PUNCT_SHIFT;
                                        break;
                                    }
                                } else {
                                    mode = Mode.MIXED;
                                    break;
                                }
                            } else {
                                mode = Mode.LOWER;
                                break;
                            }
                        }
                    } else {
                        c = (char) (i4 + 65);
                        break;
                    }
                    break;
                case LOWER:
                    if (i4 >= 26) {
                        if (i4 != 26) {
                            if (i4 != 27) {
                                if (i4 != 28) {
                                    if (i4 != 29) {
                                        if (i4 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                            if (i4 == 900) {
                                                mode = Mode.ALPHA;
                                                break;
                                            }
                                        } else {
                                            sb2.append((char) iArr2[i2]);
                                            break;
                                        }
                                    } else {
                                        mode = Mode.PUNCT_SHIFT;
                                        break;
                                    }
                                } else {
                                    mode = Mode.MIXED;
                                    break;
                                }
                            } else {
                                mode = Mode.ALPHA_SHIFT;
                                break;
                            }
                        }
                    } else {
                        c = (char) (i4 + 97);
                        break;
                    }
                    break;
                case MIXED:
                    if (i4 >= 25) {
                        if (i4 != 25) {
                            if (i4 != 26) {
                                if (i4 != 27) {
                                    if (i4 != 28) {
                                        if (i4 != 29) {
                                            if (i4 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                                if (i4 == 900) {
                                                    mode = Mode.ALPHA;
                                                    break;
                                                }
                                            } else {
                                                sb2.append((char) iArr2[i2]);
                                                break;
                                            }
                                        } else {
                                            mode = Mode.PUNCT_SHIFT;
                                            break;
                                        }
                                    } else {
                                        mode = Mode.ALPHA;
                                        break;
                                    }
                                } else {
                                    mode = Mode.LOWER;
                                    break;
                                }
                            }
                        } else {
                            mode = Mode.PUNCT;
                            break;
                        }
                    } else {
                        c = MIXED_CHARS[i4];
                        break;
                    }
                    break;
                case PUNCT:
                    if (i4 >= 29) {
                        if (i4 != 29) {
                            if (i4 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                if (i4 == 900) {
                                    mode = Mode.ALPHA;
                                    break;
                                }
                            } else {
                                sb2.append((char) iArr2[i2]);
                                break;
                            }
                        } else {
                            mode = Mode.ALPHA;
                            break;
                        }
                    } else {
                        c = PUNCT_CHARS[i4];
                        break;
                    }
                    break;
                case ALPHA_SHIFT:
                    if (i4 >= 26) {
                        if (i4 != 26) {
                            if (i4 == 900) {
                                mode = Mode.ALPHA;
                                break;
                            }
                        } else {
                            mode2 = mode3;
                            break;
                        }
                    } else {
                        c = (char) (i4 + 65);
                        break;
                    }
                    break;
                case PUNCT_SHIFT:
                    if (i4 >= 29) {
                        if (i4 != 29) {
                            if (i4 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                                if (i4 == 900) {
                                    mode = Mode.ALPHA;
                                    break;
                                }
                            } else {
                                sb2.append((char) iArr2[i2]);
                                break;
                            }
                        } else {
                            mode = Mode.ALPHA;
                            break;
                        }
                    } else {
                        c = PUNCT_CHARS[i4];
                        break;
                    }
                    break;
            }
        }
    }

    private static int byteCompaction(int i, int[] iArr, Charset charset, int i2, StringBuilder sb) {
        int i3;
        long j;
        int i4;
        int i5;
        int i6 = i;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i7 = MACRO_PDF417_TERMINATOR;
        int i8 = BEGIN_MACRO_PDF417_OPTIONAL_FIELD;
        int i9 = 928;
        int i10 = NUMERIC_COMPACTION_MODE_LATCH;
        long j2 = 900;
        if (i6 == 901) {
            int[] iArr2 = new int[6];
            int i11 = i2 + 1;
            int i12 = iArr[i2];
            int i13 = i11;
            boolean z = false;
            loop0:
            while (true) {
                i4 = 0;
                long j3 = 0;
                while (i3 < iArr[0] && !z) {
                    int i14 = i4 + 1;
                    iArr2[i4] = i12;
                    j3 = (j3 * j) + ((long) i12);
                    int i15 = i3 + 1;
                    i12 = iArr[i3];
                    if (i12 == 900 || i12 == 901 || i12 == NUMERIC_COMPACTION_MODE_LATCH || i12 == BYTE_COMPACTION_MODE_LATCH_6 || i12 == 928 || i12 == i8 || i12 == i7) {
                        i3 = i15 - 1;
                        i4 = i14;
                        i7 = MACRO_PDF417_TERMINATOR;
                        i8 = BEGIN_MACRO_PDF417_OPTIONAL_FIELD;
                        j = 900;
                        z = true;
                    } else if (i14 % 5 != 0 || i14 <= 0) {
                        i3 = i15;
                        i4 = i14;
                        i7 = MACRO_PDF417_TERMINATOR;
                        i8 = BEGIN_MACRO_PDF417_OPTIONAL_FIELD;
                        j = 900;
                    } else {
                        int i16 = 0;
                        while (i16 < 6) {
                            byteArrayOutputStream.write((byte) ((int) (j3 >> ((5 - i16) * 8))));
                            i16++;
                            i7 = MACRO_PDF417_TERMINATOR;
                            i8 = BEGIN_MACRO_PDF417_OPTIONAL_FIELD;
                        }
                        i13 = i15;
                        j2 = 900;
                    }
                }
            }
            if (i3 != iArr[0] || i12 >= 900) {
                i5 = i4;
            } else {
                i5 = i4 + 1;
                iArr2[i4] = i12;
            }
            for (int i17 = 0; i17 < i5; i17++) {
                byteArrayOutputStream.write((byte) iArr2[i17]);
            }
        } else if (i6 == BYTE_COMPACTION_MODE_LATCH_6) {
            int i18 = i2;
            boolean z2 = false;
            int i19 = 0;
            long j4 = 0;
            while (i3 < iArr[0] && !z2) {
                int i20 = i3 + 1;
                int i21 = iArr[i3];
                if (i21 < 900) {
                    i19++;
                    j4 = (j4 * 900) + ((long) i21);
                    i18 = i20;
                } else {
                    if (i21 != 900 && i21 != 901 && i21 != i10 && i21 != BYTE_COMPACTION_MODE_LATCH_6 && i21 != i9) {
                        if (i21 != BEGIN_MACRO_PDF417_OPTIONAL_FIELD) {
                            if (i21 != MACRO_PDF417_TERMINATOR) {
                                i18 = i20;
                            }
                            i18 = i20 - 1;
                            z2 = true;
                        }
                    }
                    i18 = i20 - 1;
                    z2 = true;
                }
                if (i19 % 5 == 0 && i19 > 0) {
                    for (int i22 = 0; i22 < 6; i22++) {
                        byteArrayOutputStream.write((byte) ((int) (j4 >> ((5 - i22) * 8))));
                    }
                    i19 = 0;
                    j4 = 0;
                }
                i9 = 928;
                i10 = NUMERIC_COMPACTION_MODE_LATCH;
            }
        } else {
            i3 = i2;
        }
        sb.append(new String(byteArrayOutputStream.toByteArray(), charset));
        return i3;
    }

    private static int numericCompaction(int[] iArr, int i, StringBuilder sb) throws FormatException {
        int[] iArr2 = new int[15];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i3 == iArr[0]) {
                z = true;
            }
            if (i4 < 900) {
                iArr2[i2] = i4;
                i2++;
            } else if (i4 == 900 || i4 == 901 || i4 == BYTE_COMPACTION_MODE_LATCH_6 || i4 == 928 || i4 == BEGIN_MACRO_PDF417_OPTIONAL_FIELD || i4 == MACRO_PDF417_TERMINATOR) {
                i3--;
                z = true;
            }
            if ((i2 % 15 == 0 || i4 == NUMERIC_COMPACTION_MODE_LATCH || z) && i2 > 0) {
                sb.append(decodeBase900toBase10(iArr2, i2));
                i2 = 0;
            }
            i = i3;
        }
        return i;
    }

    private static String decodeBase900toBase10(int[] iArr, int i) throws FormatException {
        BigInteger bigInteger = BigInteger.ZERO;
        for (int i2 = 0; i2 < i; i2++) {
            bigInteger = bigInteger.add(EXP900[(i - i2) - 1].multiply(BigInteger.valueOf((long) iArr[i2])));
        }
        String bigInteger2 = bigInteger.toString();
        if (bigInteger2.charAt(0) == '1') {
            return bigInteger2.substring(1);
        }
        throw FormatException.getFormatInstance();
    }
}
