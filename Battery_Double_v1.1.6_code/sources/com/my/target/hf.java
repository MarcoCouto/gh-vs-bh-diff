package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.my.target.common.models.ImageData;

@SuppressLint({"ViewConstructor"})
/* compiled from: PromoStyle2PanelViewImpl */
public class hf extends ViewGroup implements OnClickListener, he {
    private final int bottomMargin;
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final TextView hN;
    @NonNull
    private final TextView ku;
    @NonNull
    private final TextView lI;
    private final int lL;
    @NonNull
    private final ge lO;
    @NonNull
    private final ge lP;
    @NonNull
    private final gd lQ;
    @NonNull
    private final TextView lR;
    @NonNull
    private final TextView lS;
    @NonNull
    private final com.my.target.he.a lT;
    @NonNull
    private final ge lU;
    private final int lV;
    private final int lW;
    private final int lX;
    private final int lY;
    private final int lZ;
    private final int ma;
    @NonNull
    private a mb = a.PORTRAIT;
    private boolean mc;
    private final int padding;

    /* renamed from: com.my.target.hf$1 reason: invalid class name */
    /* compiled from: PromoStyle2PanelViewImpl */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] md = new int[a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            md[a.SQUARE.ordinal()] = 1;
            md[a.PORTRAIT.ordinal()] = 2;
            try {
                md[a.LANDSCAPE.ordinal()] = 3;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    /* compiled from: PromoStyle2PanelViewImpl */
    enum a {
        PORTRAIT,
        LANDSCAPE,
        SQUARE
    }

    public View eo() {
        return this;
    }

    public hf(@NonNull Context context, @NonNull com.my.target.he.a aVar) {
        super(context);
        this.lT = aVar;
        ic P = ic.P(context);
        this.lL = P.M(42);
        this.lY = P.M(64);
        this.padding = P.M(12);
        this.lV = P.M(2);
        this.lW = P.M(100);
        this.lX = P.M(8);
        this.lZ = P.M(16);
        this.ma = P.M(34);
        this.bottomMargin = P.M(6);
        this.lU = new ge(context);
        this.lO = new ge(context);
        this.lP = new ge(context);
        this.lQ = new gd(context);
        this.hN = new TextView(context);
        this.hN.setMaxLines(2);
        this.hN.setEllipsize(TruncateAt.END);
        this.hN.setTypeface(this.hN.getTypeface(), 1);
        this.lI = new TextView(context);
        this.lI.setTextSize(2, 12.0f);
        this.lI.setMaxLines(2);
        this.lI.setEllipsize(TruncateAt.END);
        this.ku = new TextView(context);
        this.ku.setTextSize(2, 8.0f);
        this.ku.setEllipsize(TruncateAt.END);
        this.lR = new TextView(context);
        this.lR.setTextSize(2, 12.0f);
        this.lR.setMaxWidth(P.M(128));
        this.lR.setEllipsize(TruncateAt.END);
        this.lR.setLines(1);
        this.lS = new TextView(context);
        this.lS.setTextSize(2, 14.0f);
        this.ctaButton = new Button(context);
        this.ctaButton.setLines(1);
        this.ctaButton.setTextSize(2, 26.0f);
        this.ctaButton.setEllipsize(TruncateAt.END);
        int M = P.M(6);
        int i = M * 4;
        this.ctaButton.setPadding(i, M, i, M);
        ic.b(this.lO, "panel_icon");
        ic.b(this.lP, "panel_image");
        ic.b(this.hN, "panel_title");
        ic.b(this.lI, "panel_description");
        ic.b(this.ku, "panel_disclaimer");
        ic.b(this.lR, "panel_domain");
        ic.b(this.lS, "panel_rating");
        ic.b(this.ctaButton, "panel_cta");
        ic.b(this.lU, "panel_ads_logo");
        addView(this.lO);
        addView(this.lP);
        addView(this.lQ);
        addView(this.hN);
        addView(this.lI);
        addView(this.lR);
        addView(this.lS);
        addView(this.ctaButton);
        addView(this.ku);
        addView(this.lU);
    }

    public void setBanner(@NonNull cn cnVar) {
        ce promoStyleSettings = cnVar.getPromoStyleSettings();
        int textColor = promoStyleSettings.getTextColor();
        this.hN.setTextColor(promoStyleSettings.getTitleColor());
        this.lI.setTextColor(textColor);
        this.ku.setTextColor(textColor);
        this.lR.setTextColor(textColor);
        this.lS.setTextColor(textColor);
        this.lQ.setColor(textColor);
        this.mc = cnVar.getVideoBanner() != null;
        ImageData bq = promoStyleSettings.bq();
        if (!"store".equals(cnVar.getNavigationType()) || bq == null) {
            this.lP.setVisibility(8);
        } else {
            this.lP.setVisibility(0);
            this.lP.setImageData(bq);
        }
        this.lO.setImageData(cnVar.getIcon());
        this.hN.setText(cnVar.getTitle());
        this.lI.setText(cnVar.getDescription());
        String disclaimer = cnVar.getDisclaimer();
        if (!TextUtils.isEmpty(disclaimer)) {
            this.ku.setVisibility(0);
            this.ku.setText(disclaimer);
        } else {
            this.ku.setVisibility(8);
        }
        if (cnVar.getNavigationType().equals("store")) {
            this.lR.setText(cnVar.getPaidType());
            if (cnVar.getRating() > 0.0f) {
                String valueOf = String.valueOf(cnVar.getRating());
                if (valueOf.length() > 3) {
                    valueOf = valueOf.substring(0, 3);
                }
                this.lS.setText(valueOf);
            }
        } else {
            this.lR.setText(cnVar.getDomain());
            this.lR.setTextColor(promoStyleSettings.bx());
        }
        this.ctaButton.setText(cnVar.getCtaText());
        ic.a(this.ctaButton, promoStyleSettings.br(), promoStyleSettings.bs(), this.lX);
        this.ctaButton.setTextColor(promoStyleSettings.getTextColor());
        ImageData adIcon = cnVar.getAdIcon();
        if (!(adIcon == null || adIcon.getBitmap() == null)) {
            this.lU.setImageData(adIcon);
            this.lU.setOnClickListener(this);
        }
        setClickArea(cnVar.getClickArea());
    }

    public void onClick(View view) {
        if (view == this.lU) {
            this.lT.dx();
        }
        this.lT.du();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int i3 = size - (this.padding * 2);
        int size2 = MeasureSpec.getSize(i2) - (this.padding * 2);
        if (i3 == size2) {
            this.mb = a.SQUARE;
        } else if (i3 > size2) {
            this.mb = a.LANDSCAPE;
        } else {
            this.mb = a.PORTRAIT;
        }
        if (this.mb == a.SQUARE) {
            ic.b(this.lO, this.lY, this.lY, 1073741824);
        } else {
            ic.b(this.lO, this.lL, this.lL, 1073741824);
        }
        int i4 = 0;
        if (this.lS.getText() != null && !TextUtils.isEmpty(this.lS.getText())) {
            ic.b(this.lS, (i3 - this.lO.getMeasuredWidth()) - this.lV, size2, Integer.MIN_VALUE);
            i4 = this.lS.getMeasuredHeight();
            ic.b(this.lQ, i4, i4, 1073741824);
        }
        if (this.lR.getText() != null && this.lR.getText().length() > 0) {
            ic.b(this.lR, (((i3 - this.lO.getMeasuredWidth()) - (this.padding * 2)) - (this.lV * 2)) - this.lQ.getMeasuredWidth(), size2, Integer.MIN_VALUE);
        }
        if (this.mb == a.SQUARE) {
            j(size, i3);
        } else if (this.mb == a.LANDSCAPE) {
            d(size, i3, size2);
        } else {
            c(size, i3, i4);
        }
    }

    private void j(int i, int i2) {
        int i3 = this.lW / 4;
        this.hN.setGravity(1);
        this.lI.setGravity(1);
        this.ku.setGravity(1);
        this.lI.setVisibility(0);
        this.hN.setTextSize(2, 18.0f);
        this.lU.setVisibility(0);
        ic.b(this.lU, this.ma, this.lZ, Integer.MIN_VALUE);
        if (!TextUtils.isEmpty(this.ku.getText())) {
            this.ku.setMaxLines(2);
            this.ku.setVisibility(0);
        }
        this.hN.setMaxLines(3);
        this.lI.setMaxLines(3);
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec((i2 - (this.lU.getMeasuredWidth() * 2)) - this.padding, 1073741824), MeasureSpec.makeMeasureSpec(i2, Integer.MIN_VALUE));
        ic.b(this.lP, this.lW, i3, Integer.MIN_VALUE);
        ic.b(this.hN, i2, i2, Integer.MIN_VALUE);
        ic.b(this.lI, i2, i2, Integer.MIN_VALUE);
        ic.b(this.ku, i2, i2, Integer.MIN_VALUE);
        setMeasuredDimension(i, i);
    }

    private void c(int i, int i2, int i3) {
        this.hN.setGravity(GravityCompat.START);
        this.lI.setGravity(GravityCompat.START);
        this.lI.setVisibility(0);
        this.ku.setVisibility(8);
        this.lU.setVisibility(8);
        this.hN.setMaxLines(1);
        this.hN.setTextSize(2, 14.0f);
        this.lI.setMaxLines(2);
        ic.b(this.ctaButton, 0, 0, 1073741824);
        ic.b(this.lI, 0, 0, 1073741824);
        ic.b(this.hN, (i2 - this.lO.getMeasuredWidth()) - this.lV, this.lO.getMeasuredHeight() - (this.lV * 2), Integer.MIN_VALUE);
        int measuredWidth = (((((i2 - (this.padding * 2)) - this.lO.getMeasuredWidth()) - this.lS.getMeasuredWidth()) - i3) - this.lR.getMeasuredWidth()) - this.lV;
        if (measuredWidth > 0) {
            ic.b(this.lP, measuredWidth, Math.max(i3, this.lR.getMeasuredHeight()), Integer.MIN_VALUE);
        } else {
            ic.b(this.lP, 0, 0, 1073741824);
        }
        setMeasuredDimension(i, ic.a(this.lO.getMeasuredHeight() + (this.padding * 2), this.hN.getMeasuredHeight() + ic.a(i3, this.lP.getMeasuredHeight(), this.lR.getMeasuredHeight()) + this.padding));
    }

    private void d(int i, int i2, int i3) {
        this.hN.setGravity(GravityCompat.START);
        this.lI.setVisibility(8);
        this.lU.setVisibility(0);
        this.ku.setMaxLines(1);
        this.hN.setMaxLines(2);
        this.hN.setTextSize(2, 18.0f);
        ic.b(this.lU, this.ma, this.lZ, Integer.MIN_VALUE);
        if (!TextUtils.isEmpty(this.ku.getText())) {
            this.ku.setVisibility(0);
        }
        ic.b(this.ctaButton, i2 / 3, i3 - (this.padding * 2), Integer.MIN_VALUE);
        int measuredWidth = i2 - (((this.lO.getMeasuredWidth() + this.ctaButton.getMeasuredWidth()) + (this.padding * 2)) + this.lU.getMeasuredWidth());
        ic.b(this.hN, measuredWidth, i3, Integer.MIN_VALUE);
        ic.b(this.lR, measuredWidth, i3, Integer.MIN_VALUE);
        ic.b(this.lP, (((measuredWidth - this.lQ.getMeasuredWidth()) - this.lS.getMeasuredWidth()) - this.lR.getMeasuredWidth()) - (this.lV * 3), Math.max(this.lQ.getMeasuredHeight(), this.lR.getMeasuredHeight()), Integer.MIN_VALUE);
        ic.b(this.ku, (i2 - this.ctaButton.getMeasuredWidth()) - this.lU.getMeasuredWidth(), i3, Integer.MIN_VALUE);
        setMeasuredDimension(i, ic.a(this.lL, this.hN.getMeasuredHeight() + ic.a(this.lR.getMeasuredHeight(), this.lQ.getMeasuredHeight(), this.lP.getMeasuredHeight()) + this.lV, this.ctaButton.getMeasuredHeight()) + (this.padding / 2) + this.lV + this.ku.getMeasuredHeight());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredHeight = this.lR.getMeasuredHeight();
        int measuredHeight2 = this.lQ.getMeasuredHeight();
        int measuredHeight3 = this.lP.getMeasuredHeight();
        int i5 = AnonymousClass1.md[this.mb.ordinal()];
        if (i5 == 1) {
            d(i, i2, i3, i4);
        } else if (i5 != 3) {
            e(i2, measuredHeight, measuredHeight2, measuredHeight3);
        } else {
            a(i, i2, i3, i4, measuredHeight, measuredHeight2, measuredHeight3);
        }
    }

    private void a(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8;
        int i9 = i4 - i2;
        ic.d(this.ku, i9 - (this.padding / 2), this.padding / 2);
        if (this.ku.getVisibility() == 0) {
            i8 = (this.ku.getTop() - (this.lV / 2)) - this.lV;
        } else {
            i8 = (i9 - (this.padding / 2)) - this.lV;
        }
        ic.a((View) this.lO, this.padding, this.padding / 2, this.padding + this.lO.getMeasuredWidth(), i8);
        int i10 = i3 - i;
        ic.a((View) this.ctaButton, ((i10 - this.padding) - this.lU.getMeasuredWidth()) - this.ctaButton.getMeasuredWidth(), 0, (i10 - this.padding) - this.lU.getMeasuredWidth(), i9);
        int right = this.lO.getRight() + this.padding;
        int a2 = ic.a(this.lS.getMeasuredHeight(), i6, i5, i7);
        int a3 = ic.a(this.lO.getTop(), this.lV) + ((((this.lO.getMeasuredHeight() - this.hN.getMeasuredHeight()) - this.lV) - a2) / 2);
        this.hN.layout(right, a3, this.hN.getMeasuredWidth() + right, this.hN.getMeasuredHeight() + a3);
        ic.a(this.hN.getBottom() + this.lV, right, this.hN.getBottom() + this.lV + a2, this.padding / 2, this.lS, this.lQ, this.lR, this.lP);
        if (this.mc) {
            i9 -= this.bottomMargin;
        }
        ic.e(this.lU, i9, i10);
    }

    private void d(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int measuredHeight = this.lO.getMeasuredHeight();
        if (measuredHeight > 0) {
            i5 = measuredHeight + 0;
            i6 = 1;
        } else {
            i6 = 0;
            i5 = 0;
        }
        int measuredHeight2 = this.hN.getMeasuredHeight();
        if (measuredHeight2 > 0) {
            i6++;
            i5 += measuredHeight2;
        }
        int measuredHeight3 = this.lI.getMeasuredHeight();
        if (measuredHeight3 > 0) {
            i6++;
            i5 += measuredHeight3;
        }
        int measuredHeight4 = this.ku.getMeasuredHeight();
        if (measuredHeight4 > 0) {
            i6++;
            i5 += measuredHeight4;
        }
        int max = Math.max(this.lQ.getMeasuredHeight(), this.lR.getMeasuredHeight());
        if (max > 0) {
            i6++;
            i5 += max;
        }
        int measuredHeight5 = this.lP.getMeasuredHeight();
        if (measuredHeight5 > 0) {
            i6++;
            i5 += measuredHeight5;
        }
        int measuredHeight6 = this.ctaButton.getMeasuredHeight();
        if (measuredHeight6 > 0) {
            i6++;
            i5 += measuredHeight6;
        }
        int i7 = i6;
        int i8 = i4 - i2;
        int i9 = i8 - i5;
        int f = ic.f(this.lV, this.padding, i9 / i7);
        int i10 = (i9 - (i7 * f)) / 2;
        int i11 = i3 - i;
        ic.a((View) this.lO, 0, i10, i11, measuredHeight + i10);
        int a2 = ic.a(i10, this.lO.getBottom() + f);
        ic.a((View) this.hN, 0, a2, i11, measuredHeight2 + a2);
        int a3 = ic.a(a2, this.hN.getBottom() + f);
        ic.a((View) this.lI, 0, a3, i11, measuredHeight3 + a3);
        int a4 = ic.a(a3, this.lI.getBottom() + f);
        ic.a((View) this.ku, 0, a4, i11, measuredHeight4 + a4);
        int a5 = ic.a(a4, this.ku.getBottom() + f);
        ic.a(a5, ((((i11 - this.lS.getMeasuredWidth()) - this.lQ.getMeasuredWidth()) - this.lR.getMeasuredWidth()) - (this.lV * 2)) / 2, max + a5, this.lV, this.lS, this.lQ, this.lR);
        int a6 = ic.a(a5, this.lR.getBottom(), this.lQ.getBottom()) + f;
        ic.a((View) this.lP, 0, a6, i11, measuredHeight5 + a6);
        int a7 = ic.a(a6, this.lP.getBottom() + f);
        ic.a((View) this.ctaButton, 0, a7, i11, measuredHeight6 + a7);
        if (this.mc) {
            i8 -= this.bottomMargin;
        }
        ic.e(this.lU, i8, i11);
    }

    private void e(int i, int i2, int i3, int i4) {
        ic.b(this.lO, this.padding, this.padding);
        int right = this.lO.getRight() + this.padding;
        int a2 = ic.a(this.lS.getMeasuredHeight(), i3, i2, i4);
        int a3 = ic.a(i + this.padding, this.lO.getTop());
        if (this.lO.getMeasuredHeight() > 0) {
            a3 += (((this.lO.getMeasuredHeight() - this.hN.getMeasuredHeight()) - this.lV) - a2) / 2;
        }
        this.hN.layout(right, a3, this.hN.getMeasuredWidth() + right, this.hN.getMeasuredHeight() + a3);
        this.lI.layout(0, 0, 0, 0);
        ic.a(this.hN.getBottom() + this.lV, right, this.hN.getBottom() + this.lV + a2, this.padding / 2, this.lS, this.lQ, this.lR, this.lP);
    }

    private void setClickArea(@NonNull ca caVar) {
        if (caVar.f456do) {
            setOnClickListener(this);
            this.ctaButton.setOnClickListener(this);
            return;
        }
        if (caVar.di) {
            this.ctaButton.setOnClickListener(this);
        } else {
            this.ctaButton.setEnabled(false);
        }
        if (caVar.dn) {
            setOnClickListener(this);
        } else {
            setOnClickListener(null);
        }
        if (caVar.dc) {
            this.hN.setOnClickListener(this);
        } else {
            this.hN.setOnClickListener(null);
        }
        if (caVar.de) {
            this.lO.setOnClickListener(this);
        } else {
            this.lO.setOnClickListener(null);
        }
        if (caVar.dd) {
            this.lI.setOnClickListener(this);
        } else {
            this.lI.setOnClickListener(null);
        }
        if (caVar.dg) {
            this.lS.setOnClickListener(this);
            this.lQ.setOnClickListener(this);
        } else {
            this.lS.setOnClickListener(null);
            this.lQ.setOnClickListener(null);
        }
        if (caVar.dl) {
            this.lR.setOnClickListener(this);
        } else {
            this.lR.setOnClickListener(null);
        }
    }
}
