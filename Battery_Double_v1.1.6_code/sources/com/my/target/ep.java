package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.common.models.ImageData;

/* compiled from: InterstitialHtmlPresenter */
public class ep implements ez, com.my.target.ft.a {
    @Nullable
    private cl aU;
    @NonNull
    private final ft eQ;
    @NonNull
    private final fz eR;
    @NonNull
    private final FrameLayout eS;
    @Nullable
    private b eT;
    @Nullable
    private com.my.target.ez.a eU;
    private long eV;
    private long eW;

    /* compiled from: InterstitialHtmlPresenter */
    static class a implements OnClickListener {
        @NonNull
        private final ep eX;

        a(@NonNull ep epVar) {
            this.eX = epVar;
        }

        public void onClick(View view) {
            com.my.target.ez.a cS = this.eX.cS();
            if (cS != null) {
                cS.aj();
            }
        }
    }

    /* compiled from: InterstitialHtmlPresenter */
    static class b implements Runnable {
        @NonNull
        private final fz eR;

        b(@NonNull fz fzVar) {
            this.eR = fzVar;
        }

        public void run() {
            ah.a("banner became just closeable");
            this.eR.setVisibility(0);
        }
    }

    public void a(@NonNull bq bqVar) {
    }

    public void stop() {
    }

    @NonNull
    public static ep s(@NonNull Context context) {
        return new ep(context);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public com.my.target.ez.a cS() {
        return this.eU;
    }

    private ep(@NonNull Context context) {
        this.eQ = new ft(context);
        this.eR = new fz(context);
        this.eS = new FrameLayout(context);
        this.eR.setContentDescription("Close");
        ic.a((View) this.eR, "close_button");
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = GravityCompat.END;
        this.eR.setVisibility(8);
        this.eR.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-1, -1);
        layoutParams2.gravity = 1;
        this.eQ.setLayoutParams(layoutParams2);
        this.eS.addView(this.eQ);
        if (this.eR.getParent() == null) {
            this.eS.addView(this.eR);
        }
        Bitmap B = fl.B(ic.P(context).M(28));
        if (B != null) {
            this.eR.a(B, false);
        }
    }

    public void a(@NonNull cy cyVar, @NonNull cl clVar) {
        this.aU = clVar;
        this.eT = new b(this.eR);
        this.eQ.setBannerWebViewListener(this);
        String source = clVar.getSource();
        if (source != null) {
            this.eQ.f(null, source);
            ImageData closeIcon = clVar.getCloseIcon();
            if (closeIcon != null) {
                this.eR.a(closeIcon.getBitmap(), false);
            }
            this.eR.setOnClickListener(new a(this));
            if (clVar.getAllowCloseDelay() > 0.0f) {
                StringBuilder sb = new StringBuilder();
                sb.append("banner will be allowed to close in ");
                sb.append(clVar.getAllowCloseDelay());
                sb.append(" seconds");
                ah.a(sb.toString());
                a((long) (clVar.getAllowCloseDelay() * 1000.0f));
            } else {
                ah.a("banner is allowed to close");
                this.eR.setVisibility(0);
            }
            if (this.eU != null) {
                this.eU.a(clVar, cT().getContext());
            }
            return;
        }
        Y("failed to load, null source");
    }

    public void a(@Nullable com.my.target.ez.a aVar) {
        this.eU = aVar;
    }

    public void pause() {
        if (this.eV > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.eV;
            if (currentTimeMillis <= 0 || currentTimeMillis >= this.eW) {
                this.eW = 0;
            } else {
                this.eW -= currentTimeMillis;
            }
        }
    }

    public void resume() {
        if (this.eW > 0) {
            a(this.eW);
        }
    }

    public void destroy() {
        this.eS.removeView(this.eQ);
        this.eQ.destroy();
    }

    @NonNull
    public View cT() {
        return this.eS;
    }

    public void onError(@NonNull String str) {
        Y(str);
    }

    public void X(@NonNull String str) {
        if (this.eU != null) {
            this.eU.b(this.aU, str, cT().getContext());
        }
    }

    private void a(long j) {
        this.eQ.removeCallbacks(this.eT);
        this.eV = System.currentTimeMillis();
        this.eQ.postDelayed(this.eT, j);
    }

    private void Y(@NonNull String str) {
        if (this.eU != null) {
            this.eU.e(str);
        }
    }
}
