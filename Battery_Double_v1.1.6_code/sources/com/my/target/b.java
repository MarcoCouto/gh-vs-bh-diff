package com.my.target;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.cv;
import java.util.List;

/* compiled from: AdFactory */
public abstract class b<T extends cv> {
    @NonNull
    protected final a adConfig;
    @NonNull
    protected final a<T> b;
    @Nullable
    protected String c;
    /* access modifiers changed from: private */
    @Nullable
    public C0056b<T> d;

    /* compiled from: AdFactory */
    public interface a<T extends cv> {
        boolean a();

        @NonNull
        c<T> b();

        @Nullable
        d<T> c();

        @NonNull
        e d();
    }

    /* renamed from: com.my.target.b$b reason: collision with other inner class name */
    /* compiled from: AdFactory */
    public interface C0056b<T extends cv> {
        void onResult(@Nullable T t, @Nullable String str);
    }

    public b(@NonNull a<T> aVar, @NonNull a aVar2) {
        this.b = aVar;
        this.adConfig = aVar2;
    }

    @AnyThread
    @NonNull
    public final b<T> a(@NonNull C0056b<T> bVar) {
        this.d = bVar;
        return this;
    }

    @AnyThread
    @NonNull
    public b<T> a(@NonNull Context context) {
        final Context applicationContext = context.getApplicationContext();
        ai.a(new Runnable() {
            public void run() {
                b.this.a(b.this.b(applicationContext), b.this.c);
            }
        });
        return this;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public T b(@NonNull Context context) {
        hv.N(context);
        bz a2 = this.b.d().a(this.adConfig, context);
        dk cz = dk.cz();
        String a3 = a(a2, cz, context);
        if (a3 == null) {
            return null;
        }
        c b2 = this.b.b();
        cv a4 = b2.a(a3, a2, null, this.adConfig, context);
        if (this.b.a()) {
            a4 = a((List<bz>) a2.ba(), (T) a4, b2, cz, context);
        }
        return a((T) a4, context);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public T a(@Nullable T t, @NonNull Context context) {
        if (t == null) {
            return t;
        }
        d c2 = this.b.c();
        return c2 != null ? c2.a(t, this.adConfig, context) : t;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public String a(@NonNull bz bzVar, @NonNull dk dkVar, @NonNull Context context) {
        dkVar.f(bzVar.getUrl(), context);
        if (dkVar.cD()) {
            return (String) dkVar.cE();
        }
        this.c = dkVar.aH();
        return null;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public T a(@NonNull List<bz> list, @Nullable T t, @NonNull c<T> cVar, @NonNull dk dkVar, @NonNull Context context) {
        if (list.size() <= 0) {
            return t;
        }
        T t2 = t;
        for (bz a2 : list) {
            t2 = a(a2, t2, cVar, dkVar, context);
        }
        return t2;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public T a(@NonNull bz bzVar, @Nullable T t, @NonNull c<T> cVar, @NonNull dk dkVar, @NonNull Context context) {
        T t2;
        bz bzVar2 = bzVar;
        Context context2 = context;
        dkVar.f(bzVar.getUrl(), context2);
        if (!dkVar.cD()) {
            return t;
        }
        ib.a((List<dh>) bzVar.r("serviceRequested"), context2);
        int i = 0;
        int bannersCount = t != null ? t.getBannersCount() : 0;
        String str = (String) dkVar.cE();
        if (str != null) {
            Context context3 = context;
            t2 = a((List<bz>) bzVar.ba(), (T) cVar.a(str, bzVar, t, this.adConfig, context3), cVar, dkVar, context3);
        } else {
            t2 = t;
        }
        if (t2 != null) {
            i = t2.getBannersCount();
        }
        if (bannersCount == i) {
            ib.a((List<dh>) bzVar.r("serviceAnswerEmpty"), context2);
            bz aY = bzVar.aY();
            if (aY != null) {
                t2 = a(aY, t2, cVar, dkVar, context);
            }
        }
        return t2;
    }

    /* access modifiers changed from: protected */
    public void a(@Nullable final T t, @Nullable final String str) {
        if (this.d != null) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                this.d.onResult(t, str);
                this.d = null;
            } else {
                ai.c(new Runnable() {
                    public void run() {
                        if (b.this.d != null) {
                            b.this.d.onResult(t, str);
                            b.this.d = null;
                        }
                    }
                });
            }
        }
    }
}
