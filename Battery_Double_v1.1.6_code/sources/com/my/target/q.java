package com.my.target;

import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.WindowManager;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialAdResultProcessor */
public class q extends d<cy> {
    @NonNull
    public static q i() {
        return new q();
    }

    private q() {
    }

    @Nullable
    public cy a(@NonNull cy cyVar, @NonNull a aVar, @NonNull Context context) {
        cj bP = cyVar.bP();
        if (bP == null) {
            ct bI = cyVar.bI();
            if (bI == null || !bI.bE()) {
                return null;
            }
            return cyVar;
        } else if (a(context, bP)) {
            return cyVar;
        } else {
            return null;
        }
    }

    private boolean a(@NonNull Context context, cj cjVar) {
        if (cjVar instanceof cn) {
            return a((cn) cjVar, context);
        }
        if (cjVar instanceof cm) {
            return a((cm) cjVar, context);
        }
        if (!(cjVar instanceof cl)) {
            return false;
        }
        a((cl) cjVar, context);
        return true;
    }

    private boolean a(@NonNull cn cnVar, @NonNull Context context) {
        ArrayList arrayList = new ArrayList();
        co videoBanner = cnVar.getVideoBanner();
        if (videoBanner != null) {
            if (videoBanner.getPreview() != null) {
                arrayList.add(videoBanner.getPreview());
            }
            VideoData videoData = (VideoData) videoBanner.getMediaData();
            if (videoData != null && videoData.isCacheable()) {
                String str = (String) dp.cG().f(videoData.getUrl(), context);
                if (str != null) {
                    videoData.setData(str);
                } else if (cnVar.isVideoRequired()) {
                    return false;
                }
            }
        }
        if (cnVar.getImage() != null) {
            arrayList.add(cnVar.getImage());
        }
        if (cnVar.getIcon() != null) {
            arrayList.add(cnVar.getIcon());
        }
        if (cnVar.getCloseIcon() != null) {
            arrayList.add(cnVar.getCloseIcon());
        }
        if (cnVar.getAdIcon() != null) {
            arrayList.add(cnVar.getAdIcon());
        }
        if (cnVar.getAdChoices() != null) {
            arrayList.add(cnVar.getAdChoices().getIcon());
        }
        ImageData bq = cnVar.getPromoStyleSettings().bq();
        if (bq != null) {
            arrayList.add(bq);
        }
        List<ck> interstitialAdCards = cnVar.getInterstitialAdCards();
        if (!interstitialAdCards.isEmpty()) {
            for (ck image : interstitialAdCards) {
                ImageData image2 = image.getImage();
                if (image2 != null) {
                    arrayList.add(image2);
                }
            }
        }
        cj endCard = cnVar.getEndCard();
        if (endCard != null && !a(context, endCard)) {
            cnVar.setEndCard(null);
        }
        if (arrayList.size() > 0) {
            hu.e(arrayList).M(context);
        }
        return true;
    }

    private boolean a(@NonNull cm cmVar, @NonNull Context context) {
        ArrayList arrayList = new ArrayList();
        Point f = f(context);
        int i = f.x;
        int i2 = f.y;
        ImageData a = a(cmVar.getPortraitImages(), Math.min(i, i2), Math.max(i, i2));
        if (a != null) {
            arrayList.add(a);
            cmVar.setOptimalPortraitImage(a);
        }
        ImageData a2 = a(cmVar.getLandscapeImages(), Math.max(i, i2), Math.min(i, i2));
        if (a2 != null) {
            arrayList.add(a2);
            cmVar.setOptimalLandscapeImage(a2);
        }
        if (!(a == null && a2 == null)) {
            ImageData closeIcon = cmVar.getCloseIcon();
            if (closeIcon != null) {
                arrayList.add(closeIcon);
            }
        }
        if (arrayList.size() > 0) {
            hu.e(arrayList).M(context);
            if (a != null && a.getBitmap() != null) {
                return true;
            }
            if (!(a2 == null || a2.getBitmap() == null)) {
                return true;
            }
        }
        return false;
    }

    private void a(@NonNull cl clVar, @NonNull Context context) {
        if (clVar.getCloseIcon() != null) {
            hu.b(clVar.getCloseIcon()).M(context);
        }
    }

    @Nullable
    private ImageData a(@NonNull List<ImageData> list, int i, int i2) {
        float f;
        float f2;
        ImageData imageData = null;
        if (list.size() == 0) {
            return null;
        }
        if (i2 == 0 || i == 0) {
            ah.a("[InterstitialAdResultProcessor] display size is zero");
            return null;
        }
        float f3 = (float) i;
        float f4 = (float) i2;
        float f5 = f3 / f4;
        float f6 = 0.0f;
        for (ImageData imageData2 : list) {
            if (imageData2.getWidth() > 0 && imageData2.getHeight() > 0) {
                float width = ((float) imageData2.getWidth()) / ((float) imageData2.getHeight());
                if (f5 < width) {
                    float width2 = (float) imageData2.getWidth();
                    if (width2 > f3) {
                        width2 = f3;
                    }
                    float f7 = width2;
                    f = width2 / width;
                    f2 = f7;
                } else {
                    f = (float) imageData2.getHeight();
                    if (f > f4) {
                        f = f4;
                    }
                    f2 = width * f;
                }
                float f8 = f2 * f;
                if (f8 <= f6) {
                    break;
                }
                imageData = imageData2;
                f6 = f8;
            }
        }
        return imageData;
    }

    @NonNull
    private Point f(@NonNull Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        Display defaultDisplay = windowManager != null ? windowManager.getDefaultDisplay() : null;
        Point point = new Point();
        if (defaultDisplay != null) {
            if (VERSION.SDK_INT >= 17) {
                defaultDisplay.getRealSize(point);
            } else {
                defaultDisplay.getSize(point);
            }
        }
        return point;
    }
}
