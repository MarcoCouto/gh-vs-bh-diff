package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.my.target.nativeads.NativeAd;
import com.my.target.nativeads.NativeAd.NativeAdListener;
import com.my.target.nativeads.banners.NativePromoBanner;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeAdEngine */
public class bb implements ao {
    @NonNull
    private final NativeAd ad;
    @NonNull
    private final cp an;
    @NonNull
    private final a bB = new a();
    @NonNull
    private final am bC;
    @NonNull
    private final com.my.target.gk.a bD;
    @Nullable
    private final NativePromoBanner bE;
    private boolean bF;
    @NonNull
    private final ArrayList<cq> bf = new ArrayList<>();
    @NonNull
    private final hr clickHandler = hr.et();
    @NonNull
    private final ia k;

    /* compiled from: NativeAdEngine */
    class a implements Runnable {
        private a() {
        }

        public void run() {
            bb.this.at();
        }
    }

    /* compiled from: NativeAdEngine */
    public static class b implements com.my.target.am.a {
        @NonNull
        private final bb bH;

        b(@NonNull bb bbVar) {
            this.bH = bbVar;
        }

        public void onClick(@Nullable View view) {
            this.bH.f(view);
        }

        public void Q() {
            this.bH.aw();
        }

        public void R() {
            this.bH.av();
        }

        public void S() {
            this.bH.au();
        }

        public void b(@NonNull View view, int i) {
            this.bH.a(view, i);
        }

        public void b(@NonNull View view, @NonNull int[] iArr) {
            this.bH.a(view, iArr);
        }
    }

    /* compiled from: NativeAdEngine */
    static class c implements com.my.target.gk.a {
        @NonNull
        private final a bB;
        @NonNull
        private final am bC;
        @NonNull
        private final ia k;

        c(@NonNull a aVar, @NonNull ia iaVar, @NonNull am amVar) {
            this.bB = aVar;
            this.k = iaVar;
            this.bC = amVar;
        }

        public void c(boolean z) {
            if (z) {
                this.k.d(this.bB);
                return;
            }
            this.bC.b(false);
            this.k.e(this.bB);
        }
    }

    @Nullable
    public String aa() {
        return "myTarget";
    }

    @NonNull
    public static bb a(@NonNull NativeAd nativeAd, @NonNull cp cpVar) {
        return new bb(nativeAd, cpVar);
    }

    private bb(@NonNull NativeAd nativeAd, @NonNull cp cpVar) {
        this.ad = nativeAd;
        this.an = cpVar;
        this.bE = NativePromoBanner.newBanner(cpVar);
        this.bC = am.a(cpVar, (com.my.target.am.a) new b(this), nativeAd.isUseExoPlayer());
        float viewabilityRate = cpVar.getViewabilityRate();
        if (viewabilityRate == 1.0f) {
            this.k = ia.nN;
        } else {
            this.k = ia.K((int) (viewabilityRate * 1000.0f));
        }
        this.bD = new c(this.bB, this.k, this.bC);
    }

    @Nullable
    public NativePromoBanner ab() {
        return this.bE;
    }

    public void registerView(@NonNull View view, @Nullable List<View> list, int i) {
        unregisterView();
        this.bC.a(view, list, this.bD, i);
        if (this.bF && this.bC.U() != 1) {
            return;
        }
        if (this.bC.Z() || this.bC.T()) {
            this.k.d(this.bB);
        }
    }

    public void unregisterView() {
        this.bC.unregisterView();
        this.k.e(this.bB);
    }

    /* access modifiers changed from: 0000 */
    public void at() {
        int V = this.bC.V();
        Context context = this.bC.getContext();
        if (V == -1 || context == null) {
            this.k.e(this.bB);
            this.bC.W();
        } else if (!this.bF || this.bC.U() == 1) {
            if (V == 1) {
                if (!this.bF) {
                    this.bF = true;
                    o(context);
                }
                if (this.bC.U() == 1) {
                    this.bC.b(true);
                } else {
                    this.k.e(this.bB);
                    this.bC.X();
                }
            } else if (this.bC.U() == 1) {
                this.bC.b(false);
            }
        } else {
            this.k.e(this.bB);
            this.bC.X();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull View view, @NonNull int[] iArr) {
        for (int i : iArr) {
            cq cqVar = (cq) this.an.getNativeAdCards().get(i);
            if (this.bF && !this.bf.contains(cqVar)) {
                if (cqVar != null) {
                    di statHolder = cqVar.getStatHolder();
                    Context context = view.getContext();
                    if (context != null) {
                        ib.a((List<dh>) statHolder.N("playbackStarted"), context);
                    }
                }
                this.bf.add(cqVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull View view, int i) {
        ah.a("Click on native card received");
        List nativeAdCards = this.an.getNativeAdCards();
        if (i >= 0 && i < nativeAdCards.size()) {
            a((ch) (cq) nativeAdCards.get(i), view);
        }
        di statHolder = this.an.getStatHolder();
        Context context = view.getContext();
        if (context != null) {
            ib.a((List<dh>) statHolder.N(String.CLICK), context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void au() {
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onVideoComplete(this.ad);
        }
    }

    /* access modifiers changed from: 0000 */
    public void av() {
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onVideoPause(this.ad);
        }
    }

    /* access modifiers changed from: 0000 */
    public void aw() {
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onVideoPlay(this.ad);
        }
    }

    /* access modifiers changed from: 0000 */
    public void f(@Nullable View view) {
        ah.a("Click received by native ad");
        if (view != null) {
            a((ch) this.an, view);
        }
    }

    private void o(@NonNull Context context) {
        ib.a((List<dh>) this.an.getStatHolder().N("playbackStarted"), context);
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onShow(this.ad);
        }
        int U = this.bC.U();
        if (U == 2 || U == 3) {
            int[] Y = this.bC.Y();
            if (Y != null) {
                for (int i : Y) {
                    cq cqVar = (cq) this.an.getNativeAdCards().get(i);
                    if (this.bF && !this.bf.contains(cqVar) && cqVar != null) {
                        ib.a((List<dh>) cqVar.getStatHolder().N("playbackStarted"), context);
                        this.bf.add(cqVar);
                    }
                }
            }
        }
    }

    private void a(@Nullable ch chVar, @NonNull View view) {
        if (chVar != null) {
            Context context = view.getContext();
            if (context != null) {
                this.clickHandler.b(chVar, context);
            }
        }
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onClick(this.ad);
        }
    }
}
