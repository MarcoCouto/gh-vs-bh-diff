package com.my.target;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.MeasureSpec;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: SingleStar */
public class gd extends View {
    private static final Paint iJ = new Paint();
    private static final Path iK = new Path();

    static {
        iJ.setAntiAlias(true);
        iJ.setStyle(Style.FILL);
        iJ.setColor(-1);
    }

    public gd(Context context) {
        super(context);
    }

    public void setColor(int i) {
        iJ.setColor(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i);
        if (size != 0 && size2 != 0) {
            size = Math.min(size, size2);
        } else if (size == 0) {
            size = size2;
        }
        setMeasuredDimension(size, size);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.drawPath(l(((float) getMeasuredHeight()) / 2.0f), iJ);
    }

    @NonNull
    private Path l(float f) {
        iK.reset();
        iK.setFillType(FillType.EVEN_ODD);
        float f2 = 0.45f * f;
        Path path = iK;
        double d = (double) f;
        double sin = Math.sin(Utils.DOUBLE_EPSILON);
        Double.isNaN(d);
        double d2 = sin * d;
        Double.isNaN(d);
        float f3 = (float) (d2 + d);
        float f4 = f * 2.0f;
        double cos = Math.cos(Utils.DOUBLE_EPSILON);
        Double.isNaN(d);
        double d3 = cos * d;
        Double.isNaN(d);
        path.moveTo(f3, f4 - ((float) (d3 + d)));
        Path path2 = iK;
        double d4 = (double) f2;
        double sin2 = Math.sin(0.6283185307179586d);
        Double.isNaN(d4);
        double d5 = sin2 * d4;
        Double.isNaN(d);
        float f5 = (float) (d5 + d);
        double cos2 = Math.cos(0.6283185307179586d);
        Double.isNaN(d4);
        double d6 = cos2 * d4;
        Double.isNaN(d);
        path2.lineTo(f5, f4 - ((float) (d6 + d)));
        for (int i = 1; i < 5; i++) {
            Path path3 = iK;
            double d7 = (double) i;
            Double.isNaN(d7);
            double d8 = 1.2566370614359172d * d7;
            double sin3 = Math.sin(d8);
            Double.isNaN(d);
            double d9 = sin3 * d;
            Double.isNaN(d);
            float f6 = (float) (d9 + d);
            double cos3 = Math.cos(d8);
            Double.isNaN(d);
            double d10 = cos3 * d;
            Double.isNaN(d);
            path3.lineTo(f6, f4 - ((float) (d10 + d)));
            Path path4 = iK;
            double d11 = d8 + 0.6283185307179586d;
            double sin4 = Math.sin(d11);
            Double.isNaN(d4);
            double d12 = sin4 * d4;
            Double.isNaN(d);
            float f7 = (float) (d12 + d);
            double cos4 = Math.cos(d11);
            Double.isNaN(d4);
            double d13 = cos4 * d4;
            Double.isNaN(d);
            path4.lineTo(f7, f4 - ((float) (d13 + d)));
        }
        iK.close();
        return iK;
    }
}
