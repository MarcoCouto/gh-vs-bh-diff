package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import com.my.target.nativeads.views.MediaAdView;

/* compiled from: VideoDialogView */
public class gh extends ViewGroup {
    static final int MEDIA_ID = ic.eG();
    static final int RATING_ID = ic.eG();
    static final int iQ = ic.eG();
    static final int iW = ic.eG();
    static final int iX = ic.eG();
    static final int iY = ic.eG();
    static final int iZ = ic.eG();
    static final int ja = ic.eG();
    static final int jb = ic.eG();
    static final int jc = ic.eG();
    static final int jd = ic.eG();
    static final int je = ic.eG();
    static final int jf = ic.eG();
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final fs fx;
    @NonNull
    private final TextView hN;
    @NonNull
    private final Button jg;
    @NonNull
    private final LinearLayout jh;
    @NonNull
    private final TextView ji;
    @NonNull
    private final FrameLayout jj;
    @NonNull
    private final TextView jk;
    @NonNull
    private final gj jl;
    @NonNull
    private final fz jm;
    @NonNull
    private final fy jn;
    @NonNull
    private final fy jo;
    @NonNull
    private final fy jp;
    /* access modifiers changed from: private */
    @NonNull
    public final Runnable jq = new b();
    @NonNull
    private final c jr = new c();
    @NonNull
    private final OnClickListener js = new a();
    @Nullable
    private final Bitmap jt;
    @Nullable
    private final Bitmap ju;
    /* access modifiers changed from: private */
    public int jv;
    private final int jw;
    private boolean jx;
    /* access modifiers changed from: private */
    @Nullable
    public d jy;
    @NonNull
    private final MediaAdView mediaAdView;
    private final int padding;
    @NonNull
    private final gf starsRatingView;
    @NonNull
    private final ic uiUtils;

    /* compiled from: VideoDialogView */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (gh.this.jy != null) {
                int id = view.getId();
                if (id == gh.iX) {
                    gh.this.jy.a(view);
                } else if (id == gh.iY) {
                    gh.this.jy.F();
                } else if (id == gh.ja) {
                    gh.this.jy.G();
                } else if (id == gh.iZ) {
                    gh.this.jy.E();
                } else if (id == gh.iW) {
                    gh.this.jy.H();
                } else if (id == gh.je) {
                    gh.this.jy.I();
                }
            }
        }
    }

    /* compiled from: VideoDialogView */
    class b implements Runnable {
        private b() {
        }

        public void run() {
            if (gh.this.jv == 2) {
                gh.this.ec();
            }
        }
    }

    /* compiled from: VideoDialogView */
    class c implements OnClickListener {
        private c() {
        }

        public void onClick(View view) {
            gh.this.removeCallbacks(gh.this.jq);
            if (gh.this.jv == 2) {
                gh.this.ec();
                return;
            }
            if (gh.this.jv == 0) {
                gh.this.ed();
            }
            gh.this.postDelayed(gh.this.jq, 4000);
        }
    }

    /* compiled from: VideoDialogView */
    public interface d {
        void E();

        void F();

        void G();

        void H();

        void I();

        void a(View view);
    }

    public gh(@NonNull Context context) {
        super(context);
        this.jg = new Button(context);
        this.hN = new TextView(context);
        this.starsRatingView = new gf(context);
        this.ctaButton = new Button(context);
        this.ji = new TextView(context);
        this.jj = new FrameLayout(context);
        this.jn = new fy(context);
        this.jo = new fy(context);
        this.jp = new fy(context);
        this.jk = new TextView(context);
        this.mediaAdView = new MediaAdView(context);
        this.jl = new gj(context);
        this.jm = new fz(context);
        this.jh = new LinearLayout(context);
        this.uiUtils = ic.P(context);
        this.fx = new fs(context);
        this.jt = fn.C(this.uiUtils.M(28));
        this.ju = fn.D(this.uiUtils.M(28));
        ic.a((View) this.jg, "dismiss_button");
        ic.a((View) this.hN, "title_text");
        ic.a((View) this.starsRatingView, "stars_view");
        ic.a((View) this.ctaButton, "cta_button");
        ic.a((View) this.ji, "replay_text");
        ic.a((View) this.jj, "shadow");
        ic.a((View) this.jn, "pause_button");
        ic.a((View) this.jo, "play_button");
        ic.a((View) this.jp, "replay_button");
        ic.a((View) this.jk, "domain_text");
        ic.a((View) this.mediaAdView, "media_view");
        ic.a((View) this.jl, "video_progress_wheel");
        ic.a((View) this.jm, "sound_button");
        this.jw = this.uiUtils.M(28);
        this.padding = this.uiUtils.M(16);
        dc();
    }

    @NonNull
    public MediaAdView getMediaAdView() {
        return this.mediaAdView;
    }

    public void a(@NonNull cp cpVar, @NonNull VideoData videoData) {
        co videoBanner = cpVar.getVideoBanner();
        if (videoBanner != null) {
            this.jl.setMax(cpVar.getDuration());
            this.jx = videoBanner.isAllowReplay();
            this.ctaButton.setText(cpVar.getCtaText());
            this.hN.setText(cpVar.getTitle());
            if ("store".equals(cpVar.getNavigationType())) {
                this.jk.setVisibility(8);
                if (cpVar.getVotes() == 0 || cpVar.getRating() <= 0.0f) {
                    this.starsRatingView.setVisibility(8);
                } else {
                    this.starsRatingView.setVisibility(0);
                    this.starsRatingView.setRating(cpVar.getRating());
                }
            } else {
                this.starsRatingView.setVisibility(8);
                this.jk.setVisibility(0);
                this.jk.setText(cpVar.getDomain());
            }
            this.jg.setText(videoBanner.getCloseActionText());
            this.ji.setText(videoBanner.getReplayActionText());
            Bitmap F = fn.F(getContext());
            if (F != null) {
                this.jp.setImageBitmap(F);
            }
            this.mediaAdView.setPlaceHolderDimension(videoData.getWidth(), videoData.getHeight());
            ImageData image = cpVar.getImage();
            if (image != null) {
                this.mediaAdView.getImageView().setImageBitmap(image.getBitmap());
            }
        }
    }

    public void setVideoDialogViewListener(@Nullable d dVar) {
        this.jy = dVar;
    }

    public void a(float f, float f2) {
        if (this.jl.getVisibility() != 0) {
            this.jl.setVisibility(0);
        }
        this.jl.setProgress(f / f2);
        this.jl.setDigit((int) Math.ceil((double) (f2 - f)));
    }

    public void dX() {
        if (this.jv != 4) {
            this.jv = 4;
            this.mediaAdView.getImageView().setVisibility(0);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            if (this.jx) {
                this.jh.setVisibility(0);
                this.jj.setVisibility(0);
            }
            this.jo.setVisibility(8);
            this.jn.setVisibility(8);
            this.jl.setVisibility(8);
        }
    }

    public void dY() {
        if (this.jv != 3) {
            this.jv = 3;
            this.mediaAdView.getProgressBarView().setVisibility(0);
            this.jh.setVisibility(8);
            this.jo.setVisibility(8);
            this.jn.setVisibility(8);
            this.jj.setVisibility(8);
        }
    }

    public void dZ() {
        if (this.jv != 1) {
            this.jv = 1;
            this.mediaAdView.getImageView().setVisibility(0);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            this.jh.setVisibility(8);
            this.jo.setVisibility(0);
            this.jn.setVisibility(8);
            this.jj.setVisibility(0);
        }
    }

    public void ea() {
        if (this.jv != 0 && this.jv != 2) {
            this.jv = 0;
            this.mediaAdView.getImageView().setVisibility(8);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            this.jh.setVisibility(8);
            this.jo.setVisibility(8);
            if (this.jv != 2) {
                this.jn.setVisibility(8);
            }
        }
    }

    public void y(boolean z) {
        if (z) {
            this.jm.a(this.ju, false);
            this.jm.setContentDescription("sound off");
            return;
        }
        this.jm.a(this.jt, false);
        this.jm.setContentDescription("sound on");
    }

    public void eb() {
        this.mediaAdView.getImageView().setVisibility(0);
    }

    @NonNull
    public fs getAdVideoView() {
        return this.fx;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.jm.measure(MeasureSpec.makeMeasureSpec(this.jw, 1073741824), MeasureSpec.makeMeasureSpec(this.jw, 1073741824));
        this.jl.measure(MeasureSpec.makeMeasureSpec(this.jw, 1073741824), MeasureSpec.makeMeasureSpec(this.jw, 1073741824));
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.mediaAdView.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        int i3 = size - (this.padding << 1);
        int i4 = size2 - (this.padding << 1);
        this.jg.measure(MeasureSpec.makeMeasureSpec(i3 / 2, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jn.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jo.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jh.measure(MeasureSpec.makeMeasureSpec(i3 - (this.padding * 4), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.starsRatingView.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jj.measure(MeasureSpec.makeMeasureSpec(this.mediaAdView.getMeasuredWidth(), 1073741824), MeasureSpec.makeMeasureSpec(this.mediaAdView.getMeasuredHeight(), 1073741824));
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i3 - (this.padding * 4), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.hN.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jk.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        if (size > size2) {
            int measuredWidth = this.ctaButton.getMeasuredWidth();
            int measuredWidth2 = this.hN.getMeasuredWidth();
            if (this.jl.getMeasuredWidth() + measuredWidth2 + Math.max(this.starsRatingView.getMeasuredWidth(), this.jk.getMeasuredWidth()) + measuredWidth + (this.padding * 3) > i3) {
                int measuredWidth3 = (i3 - this.jl.getMeasuredWidth()) - (this.padding * 3);
                int i5 = measuredWidth3 / 3;
                this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.starsRatingView.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.jk.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.hN.measure(MeasureSpec.makeMeasureSpec(((measuredWidth3 - this.ctaButton.getMeasuredWidth()) - this.jk.getMeasuredWidth()) - this.starsRatingView.getMeasuredWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
            }
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int measuredWidth = this.mediaAdView.getMeasuredWidth();
        int measuredHeight = this.mediaAdView.getMeasuredHeight();
        int i7 = (i5 - measuredWidth) >> 1;
        int i8 = (i6 - measuredHeight) >> 1;
        this.mediaAdView.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
        this.jj.layout(this.mediaAdView.getLeft(), this.mediaAdView.getTop(), this.mediaAdView.getRight(), this.mediaAdView.getBottom());
        int measuredWidth2 = this.jo.getMeasuredWidth();
        int i9 = i3 >> 1;
        int i10 = measuredWidth2 >> 1;
        int i11 = i4 >> 1;
        int measuredHeight2 = this.jo.getMeasuredHeight() >> 1;
        this.jo.layout(i9 - i10, i11 - measuredHeight2, i10 + i9, measuredHeight2 + i11);
        int measuredWidth3 = this.jn.getMeasuredWidth();
        int i12 = measuredWidth3 >> 1;
        int measuredHeight3 = this.jn.getMeasuredHeight() >> 1;
        this.jn.layout(i9 - i12, i11 - measuredHeight3, i12 + i9, measuredHeight3 + i11);
        int measuredWidth4 = this.jh.getMeasuredWidth();
        int i13 = measuredWidth4 >> 1;
        int measuredHeight4 = this.jh.getMeasuredHeight() >> 1;
        this.jh.layout(i9 - i13, i11 - measuredHeight4, i9 + i13, i11 + measuredHeight4);
        this.jg.layout(this.padding, this.padding, this.padding + this.jg.getMeasuredWidth(), this.padding + this.jg.getMeasuredHeight());
        if (i5 > i6) {
            int max = Math.max(this.ctaButton.getMeasuredHeight(), Math.max(this.hN.getMeasuredHeight(), this.starsRatingView.getMeasuredHeight()));
            this.ctaButton.layout((i5 - this.padding) - this.ctaButton.getMeasuredWidth(), ((i6 - this.padding) - this.ctaButton.getMeasuredHeight()) - ((max - this.ctaButton.getMeasuredHeight()) >> 1), i5 - this.padding, (i6 - this.padding) - ((max - this.ctaButton.getMeasuredHeight()) >> 1));
            this.jm.layout((this.ctaButton.getRight() - this.jm.getMeasuredWidth()) + this.jm.getPadding(), (((this.mediaAdView.getBottom() - (this.padding << 1)) - this.jm.getMeasuredHeight()) - max) + this.jm.getPadding(), this.ctaButton.getRight() + this.jm.getPadding(), ((this.mediaAdView.getBottom() - (this.padding << 1)) - max) + this.jm.getPadding());
            this.starsRatingView.layout((this.ctaButton.getLeft() - this.padding) - this.starsRatingView.getMeasuredWidth(), ((i6 - this.padding) - this.starsRatingView.getMeasuredHeight()) - ((max - this.starsRatingView.getMeasuredHeight()) >> 1), this.ctaButton.getLeft() - this.padding, (i6 - this.padding) - ((max - this.starsRatingView.getMeasuredHeight()) >> 1));
            this.jk.layout((this.ctaButton.getLeft() - this.padding) - this.jk.getMeasuredWidth(), ((i6 - this.padding) - this.jk.getMeasuredHeight()) - ((max - this.jk.getMeasuredHeight()) >> 1), this.ctaButton.getLeft() - this.padding, (i6 - this.padding) - ((max - this.jk.getMeasuredHeight()) >> 1));
            int min = Math.min(this.starsRatingView.getLeft(), this.jk.getLeft());
            this.hN.layout((min - this.padding) - this.hN.getMeasuredWidth(), ((i6 - this.padding) - this.hN.getMeasuredHeight()) - ((max - this.hN.getMeasuredHeight()) >> 1), min - this.padding, (i6 - this.padding) - ((max - this.hN.getMeasuredHeight()) >> 1));
            this.jl.layout(this.padding, ((i6 - this.padding) - this.jl.getMeasuredHeight()) - ((max - this.jl.getMeasuredHeight()) >> 1), this.padding + this.jl.getMeasuredWidth(), (i6 - this.padding) - ((max - this.jl.getMeasuredHeight()) >> 1));
            return;
        }
        this.jm.layout(((this.mediaAdView.getRight() - this.padding) - this.jm.getMeasuredWidth()) + this.jm.getPadding(), ((this.mediaAdView.getBottom() - this.padding) - this.jm.getMeasuredHeight()) + this.jm.getPadding(), (this.mediaAdView.getRight() - this.padding) + this.jm.getPadding(), (this.mediaAdView.getBottom() - this.padding) + this.jm.getPadding());
        int i14 = i5 >> 1;
        this.hN.layout(i14 - (this.hN.getMeasuredWidth() >> 1), this.mediaAdView.getBottom() + this.padding, (this.hN.getMeasuredWidth() >> 1) + i14, this.mediaAdView.getBottom() + this.padding + this.hN.getMeasuredHeight());
        this.starsRatingView.layout(i14 - (this.starsRatingView.getMeasuredWidth() >> 1), this.hN.getBottom() + this.padding, (this.starsRatingView.getMeasuredWidth() >> 1) + i14, this.hN.getBottom() + this.padding + this.starsRatingView.getMeasuredHeight());
        this.jk.layout(i14 - (this.jk.getMeasuredWidth() >> 1), this.hN.getBottom() + this.padding, (this.jk.getMeasuredWidth() >> 1) + i14, this.hN.getBottom() + this.padding + this.jk.getMeasuredHeight());
        this.ctaButton.layout(i14 - (this.ctaButton.getMeasuredWidth() >> 1), this.starsRatingView.getBottom() + this.padding, i14 + (this.ctaButton.getMeasuredWidth() >> 1), this.starsRatingView.getBottom() + this.padding + this.ctaButton.getMeasuredHeight());
        this.jl.layout(this.padding, (this.mediaAdView.getBottom() - this.padding) - this.jl.getMeasuredHeight(), this.padding + this.jl.getMeasuredWidth(), this.mediaAdView.getBottom() - this.padding);
    }

    /* access modifiers changed from: private */
    public void ec() {
        if (this.jv != 0) {
            this.jv = 0;
            this.mediaAdView.getImageView().setVisibility(8);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            this.jh.setVisibility(8);
            this.jo.setVisibility(8);
            this.jn.setVisibility(8);
            this.jj.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void ed() {
        if (this.jv != 2) {
            this.jv = 2;
            this.mediaAdView.getImageView().setVisibility(8);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            this.jh.setVisibility(8);
            this.jo.setVisibility(8);
            this.jn.setVisibility(0);
            this.jj.setVisibility(8);
        }
    }

    private void dc() {
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        int i = this.padding;
        this.jm.setId(je);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13, -1);
        this.mediaAdView.setId(MEDIA_ID);
        this.mediaAdView.setLayoutParams(layoutParams);
        this.mediaAdView.setId(jd);
        this.mediaAdView.setOnClickListener(this.jr);
        this.mediaAdView.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.jj.setBackgroundColor(-1728053248);
        this.jj.setVisibility(8);
        this.jg.setId(iW);
        this.jg.setTextSize(2, 16.0f);
        this.jg.setTransformationMethod(null);
        this.jg.setEllipsize(TruncateAt.END);
        this.jg.setMaxLines(2);
        this.jg.setPadding(i, i, i, i);
        this.jg.setTextColor(-1);
        ic.a(this.jg, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        this.hN.setId(iQ);
        this.hN.setMaxLines(2);
        this.hN.setEllipsize(TruncateAt.END);
        this.hN.setTextSize(2, 18.0f);
        this.hN.setTextColor(-1);
        ic.a(this.ctaButton, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        this.ctaButton.setId(iX);
        this.ctaButton.setTextColor(-1);
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setGravity(1);
        this.ctaButton.setTextSize(2, 16.0f);
        this.ctaButton.setLines(1);
        this.ctaButton.setEllipsize(TruncateAt.END);
        this.ctaButton.setMinimumWidth(this.uiUtils.M(100));
        this.ctaButton.setPadding(i, i, i, i);
        this.hN.setShadowLayer((float) this.uiUtils.M(1), (float) this.uiUtils.M(1), (float) this.uiUtils.M(1), ViewCompat.MEASURED_STATE_MASK);
        this.jk.setId(jc);
        this.jk.setTextColor(-3355444);
        this.jk.setMaxEms(10);
        this.jk.setShadowLayer((float) this.uiUtils.M(1), (float) this.uiUtils.M(1), (float) this.uiUtils.M(1), ViewCompat.MEASURED_STATE_MASK);
        this.jh.setId(iY);
        this.jh.setOnClickListener(this.js);
        this.jh.setGravity(17);
        this.jh.setVisibility(8);
        this.jh.setPadding(this.uiUtils.M(8), 0, this.uiUtils.M(8), 0);
        this.ji.setSingleLine();
        this.ji.setEllipsize(TruncateAt.END);
        this.ji.setTypeface(this.ji.getTypeface(), 1);
        this.ji.setTextColor(-1);
        this.ji.setTextSize(2, 16.0f);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.leftMargin = this.uiUtils.M(4);
        this.jp.setPadding(this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16));
        this.jn.setId(ja);
        this.jn.setOnClickListener(this.js);
        this.jn.setVisibility(8);
        this.jn.setPadding(this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16));
        this.jo.setId(iZ);
        this.jo.setOnClickListener(this.js);
        this.jo.setVisibility(8);
        this.jo.setPadding(this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16));
        this.jj.setId(jf);
        Bitmap G = fn.G(getContext());
        if (G != null) {
            this.jo.setImageBitmap(G);
        }
        Bitmap H = fn.H(getContext());
        if (H != null) {
            this.jn.setImageBitmap(H);
        }
        ic.a(this.jn, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        ic.a(this.jo, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        ic.a(this.jp, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        this.starsRatingView.setId(RATING_ID);
        this.starsRatingView.setStarSize(this.uiUtils.M(12));
        this.jl.setId(jb);
        this.jl.setVisibility(8);
        this.mediaAdView.addView(this.fx, new ViewGroup.LayoutParams(-1, -1));
        addView(this.mediaAdView);
        addView(this.jj);
        addView(this.jm);
        addView(this.jg);
        addView(this.jl);
        addView(this.jh);
        addView(this.jn);
        addView(this.jo);
        addView(this.starsRatingView);
        addView(this.jk);
        addView(this.ctaButton);
        addView(this.hN);
        this.jh.addView(this.jp);
        this.jh.addView(this.ji, layoutParams2);
        this.ctaButton.setOnClickListener(this.js);
        this.jg.setOnClickListener(this.js);
        this.jm.setOnClickListener(this.js);
    }
}
