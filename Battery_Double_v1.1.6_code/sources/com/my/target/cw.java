package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: InstreamAdSection */
public class cw extends cv {
    @NonNull
    private final HashMap<String, da<VideoData>> dH = new HashMap<>();

    @NonNull
    public static cw bJ() {
        return new cw();
    }

    private cw() {
        this.dH.put(BreakId.PREROLL, da.z(BreakId.PREROLL));
        this.dH.put(BreakId.PAUSEROLL, da.z(BreakId.PAUSEROLL));
        this.dH.put(BreakId.MIDROLL, da.z(BreakId.MIDROLL));
        this.dH.put(BreakId.POSTROLL, da.z(BreakId.POSTROLL));
    }

    @Nullable
    public da<VideoData> x(@NonNull String str) {
        return (da) this.dH.get(str);
    }

    @NonNull
    public ArrayList<da<VideoData>> bK() {
        return new ArrayList<>(this.dH.values());
    }

    public int getBannersCount() {
        int i = 0;
        for (da bannersCount : this.dH.values()) {
            i += bannersCount.getBannersCount();
        }
        return i;
    }

    public boolean bL() {
        for (da daVar : this.dH.values()) {
            if (daVar.getBannersCount() <= 0) {
                if (daVar.bZ()) {
                }
            }
            return true;
        }
        return false;
    }
}
