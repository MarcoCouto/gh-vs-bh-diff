package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.github.mikephil.charting.utils.Utils;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: CommonVideoParser */
public class dx {
    @NonNull
    private final a adConfig;
    @Nullable
    private String ck;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;
    @NonNull
    private final dw eB;

    @NonNull
    public static dx c(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new dx(bzVar, aVar, context2);
    }

    private dx(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eA = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.eB = dw.b(bzVar, aVar, context2);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull co<VideoData> coVar) {
        this.eB.a(jSONObject, (ch) coVar);
        if ("statistics".equals(coVar.getType())) {
            c(jSONObject, coVar);
            return true;
        }
        this.ck = coVar.getId();
        float duration = coVar.getDuration();
        if (duration <= 0.0f) {
            StringBuilder sb = new StringBuilder();
            sb.append("wrong videoBanner duration ");
            sb.append(duration);
            f("Bad value", sb.toString());
            return false;
        }
        coVar.setCloseActionText(jSONObject.optString("closeActionText", "Close"));
        coVar.setReplayActionText(jSONObject.optString("replayActionText", coVar.getReplayActionText()));
        coVar.setCloseDelayActionText(jSONObject.optString("closeDelayActionText", coVar.getCloseDelayActionText()));
        coVar.setAllowReplay(jSONObject.optBoolean("allowReplay", coVar.isAllowReplay()));
        coVar.setAutoMute(jSONObject.optBoolean("automute", coVar.isAutoMute()));
        coVar.setAllowClose(jSONObject.optBoolean("allowClose", coVar.isAllowClose()));
        coVar.setAllowCloseDelay((float) jSONObject.optDouble("allowCloseDelay", Utils.DOUBLE_EPSILON));
        coVar.setShowPlayerControls(jSONObject.optBoolean("showPlayerControls", coVar.isShowPlayerControls()));
        coVar.setAutoPlay(jSONObject.optBoolean("autoplay", coVar.isAutoPlay()));
        coVar.setHasCtaButton(jSONObject.optBoolean("hasCtaButton", coVar.isHasCtaButton()));
        coVar.setAllowPause(jSONObject.optBoolean("hasPause", coVar.isAllowPause()));
        String optString = jSONObject.optString("previewLink");
        if (!TextUtils.isEmpty(optString)) {
            coVar.setPreview(ImageData.newImageData(optString, jSONObject.optInt("previewWidth"), jSONObject.optInt("previewHeight")));
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("mediafiles");
        if (optJSONArray == null || optJSONArray.length() == 0) {
            ah.a("mediafiles array is empty");
            f("Required field", "unable to find mediaFiles in MediaBanner");
            return false;
        }
        b(jSONObject, coVar);
        ArrayList arrayList = new ArrayList();
        int length = optJSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                VideoData f = f(optJSONObject);
                if (f != null) {
                    arrayList.add(f);
                }
            }
        }
        if (arrayList.size() > 0) {
            VideoData chooseBest = VideoData.chooseBest(arrayList, this.adConfig.getVideoQuality());
            if (chooseBest != null) {
                coVar.setMediaData(chooseBest);
                return true;
            }
        }
        return false;
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull co<VideoData> coVar) {
        c(jSONObject, coVar);
        Boolean be = this.eA.be();
        if (be != null) {
            coVar.setAllowClose(be.booleanValue());
        }
        Boolean bf = this.eA.bf();
        if (bf != null) {
            coVar.setAllowPause(bf.booleanValue());
        }
        float allowCloseDelay = this.eA.getAllowCloseDelay();
        if (allowCloseDelay >= 0.0f) {
            coVar.setAllowCloseDelay(allowCloseDelay);
        }
    }

    private void c(@NonNull JSONObject jSONObject, @NonNull co<VideoData> coVar) {
        double point = (double) this.eA.getPoint();
        if (point < Utils.DOUBLE_EPSILON) {
            point = jSONObject.optDouble("point");
        }
        if (Double.isNaN(point)) {
            point = -1.0d;
        } else if (point < Utils.DOUBLE_EPSILON) {
            StringBuilder sb = new StringBuilder();
            sb.append("Wrong value ");
            sb.append(point);
            sb.append(" for point");
            f("Bad value", sb.toString());
        }
        double pointP = (double) this.eA.getPointP();
        if (pointP < Utils.DOUBLE_EPSILON) {
            pointP = jSONObject.optDouble("pointP");
        }
        if (Double.isNaN(pointP)) {
            pointP = -1.0d;
        } else if (pointP < Utils.DOUBLE_EPSILON) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Wrong value ");
            sb2.append(pointP);
            sb2.append(" for pointP");
            f("Bad value", sb2.toString());
        }
        if (point < Utils.DOUBLE_EPSILON && pointP < Utils.DOUBLE_EPSILON) {
            pointP = 50.0d;
            point = -1.0d;
        }
        coVar.setPoint((float) point);
        coVar.setPointP((float) pointP);
    }

    @Nullable
    private VideoData f(@NonNull JSONObject jSONObject) {
        String optString = jSONObject.optString("src");
        int optInt = jSONObject.optInt("width");
        int optInt2 = jSONObject.optInt("height");
        if (TextUtils.isEmpty(optString) || optInt <= 0 || optInt2 <= 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("bad mediafile object, src = ");
            sb.append(optString);
            sb.append(", width = ");
            sb.append(optInt);
            sb.append(", height = ");
            sb.append(optInt2);
            f("Bad value", sb.toString());
            return null;
        }
        VideoData newVideoData = VideoData.newVideoData(optString, optInt, optInt2);
        newVideoData.setBitrate(jSONObject.optInt("bitrate"));
        if (!newVideoData.getUrl().endsWith(VideoData.M3U8) || hw.ey()) {
            return newVideoData;
        }
        ah.a("HLS Video does not supported, add com.google.android.exoplayer:exoplayer-hls dependency to play HLS video ");
        return null;
    }

    private void f(String str, String str2) {
        dq.P(str).Q(str2).x(this.adConfig.getSlotId()).S(this.ck).R(this.eA.getUrl()).q(this.context);
    }
}
