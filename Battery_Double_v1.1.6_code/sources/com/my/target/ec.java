package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import org.json.JSONObject;

/* compiled from: InterstitialSliderAdBannerParser */
public class ec {
    @NonNull
    private final cz be;
    @NonNull
    private final dw eB;
    @NonNull
    private final eb eC;

    @NonNull
    public static ec a(@NonNull cz czVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new ec(czVar, bzVar, aVar, context);
    }

    private ec(@NonNull cz czVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        this.be = czVar;
        this.eB = dw.b(bzVar, aVar, context);
        this.eC = eb.e(bzVar, aVar, context);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull cm cmVar) {
        b(jSONObject, cmVar);
        return this.eC.b(jSONObject, cmVar);
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull cj cjVar) {
        this.eB.a(jSONObject, (ch) cjVar);
        cjVar.setAllowClose(true);
        String optString = jSONObject.optString("close_icon_hd");
        if (!TextUtils.isEmpty(optString)) {
            cjVar.setCloseIcon(ImageData.newImageData(optString));
        } else {
            cjVar.setCloseIcon(this.be.getCloseIcon());
        }
    }
}
