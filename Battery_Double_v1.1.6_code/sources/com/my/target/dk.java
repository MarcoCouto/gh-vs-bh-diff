package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

/* compiled from: HttpAdRequest */
public final class dk extends dn<String> {
    @NonNull
    public static dk cz() {
        return new dk();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b9  */
    @Nullable
    /* renamed from: b */
    public String c(@NonNull String str, @NonNull Context context) {
        HttpURLConnection httpURLConnection;
        Throwable th;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("send ad request: ");
            sb.append(str);
            ah.a(sb.toString());
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection.setReadTimeout(10000);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestProperty("connection", "close");
                httpURLConnection.connect();
                this.responseCode = httpURLConnection.getResponseCode();
                if (this.responseCode == 200) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), Charset.forName("UTF-8")));
                    StringBuilder sb2 = new StringBuilder();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        sb2.append(readLine);
                    }
                    bufferedReader.close();
                    this.en = sb2.toString();
                } else if (this.responseCode != 204) {
                    this.em = false;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("ad request error: response code ");
                    sb3.append(this.responseCode);
                    this.c = sb3.toString();
                    ah.a(this.c);
                }
            } catch (Throwable th2) {
                th = th2;
                this.em = false;
                this.c = th.getMessage();
                StringBuilder sb4 = new StringBuilder();
                sb4.append("ad request error: ");
                sb4.append(this.c);
                ah.a(sb4.toString());
                if (httpURLConnection != null) {
                }
                return (String) this.en;
            }
        } catch (Throwable th3) {
            th = th3;
            httpURLConnection = null;
            this.em = false;
            this.c = th.getMessage();
            StringBuilder sb42 = new StringBuilder();
            sb42.append("ad request error: ");
            sb42.append(this.c);
            ah.a(sb42.toString());
            if (httpURLConnection != null) {
            }
            return (String) this.en;
        }
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        return (String) this.en;
    }
}
