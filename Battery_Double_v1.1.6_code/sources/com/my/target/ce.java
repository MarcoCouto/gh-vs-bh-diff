package com.my.target;

import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import com.my.target.common.models.ImageData;

/* compiled from: PromoStyleSettings */
public class ce {
    private int backgroundColor = ViewCompat.MEASURED_STATE_MASK;
    private int dq = -16733198;
    private int dr = -16746839;
    private int ds = -1;
    private int dt = -1;
    private int du = ViewCompat.MEASURED_STATE_MASK;
    private int dv = -11176784;
    private int dw = -1;
    private float dx = 0.5f;
    @Nullable
    private ImageData dy;
    private int textColor = -1;

    public static ce bp() {
        return new ce();
    }

    @Nullable
    public ImageData bq() {
        return this.dy;
    }

    public void a(@Nullable ImageData imageData) {
        this.dy = imageData;
    }

    public void i(int i) {
        this.dq = i;
    }

    public void j(int i) {
        this.dr = i;
    }

    public void k(int i) {
        this.ds = i;
    }

    public int br() {
        return this.dq;
    }

    public int bs() {
        return this.dr;
    }

    public int bt() {
        return this.ds;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(int i) {
        this.backgroundColor = i;
    }

    public int getTextColor() {
        return this.textColor;
    }

    public void setTextColor(int i) {
        this.textColor = i;
    }

    public int getTitleColor() {
        return this.dt;
    }

    public void setTitleColor(int i) {
        this.dt = i;
    }

    public int bu() {
        return this.du;
    }

    public void l(int i) {
        this.du = i;
    }

    public int bv() {
        return this.dv;
    }

    public void m(int i) {
        this.dv = i;
    }

    public float bw() {
        return this.dx;
    }

    public void f(float f) {
        this.dx = f;
    }

    public int bx() {
        return this.dw;
    }

    public void n(int i) {
        this.dw = i;
    }

    private ce() {
    }
}
