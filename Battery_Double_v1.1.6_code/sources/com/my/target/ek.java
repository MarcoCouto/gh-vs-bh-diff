package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.AppodealNetworks;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

/* compiled from: StandardAdBannerParser */
public class ek {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;
    @NonNull
    private final dw eB;

    @NonNull
    public static ek j(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new ek(bzVar, aVar, context2);
    }

    private ek(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eA = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.eB = dw.b(bzVar, aVar, context2);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull cs csVar, @Nullable String str) {
        String str2;
        this.eB.a(jSONObject, (ch) csVar);
        if (jSONObject.has("timeout")) {
            int optInt = jSONObject.optInt("timeout");
            if (optInt >= 5) {
                csVar.setTimeout(optInt);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("Wrong banner timeout: ");
                sb.append(optInt);
                b("Required field", sb.toString(), csVar.getId());
            }
        }
        if (!csVar.getType().equals(String.HTML)) {
            return true;
        }
        String optString = jSONObject.optString("source", null);
        if (optString == null) {
            dq.P("Required field").Q("Banner with type 'html' has no source field").S(csVar.getId()).R(this.eA.getUrl()).x(this.adConfig.getSlotId()).q(this.context);
            return false;
        }
        String decode = id.decode(optString);
        if (!TextUtils.isEmpty(str)) {
            csVar.setMraidJs(str);
            str2 = dw.g(str, decode);
            if (str2 != null) {
                csVar.setMraidSource(str2);
                csVar.setType(AppodealNetworks.MRAID);
                return this.eB.a(str2, jSONObject);
            }
        }
        str2 = decode;
        return this.eB.a(str2, jSONObject);
    }

    private void b(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        dq.P(str).Q(str2).x(this.adConfig.getSlotId()).S(str3).R(this.eA.getUrl()).q(this.context);
    }
}
