package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NotificationHandler */
public class hx {
    /* access modifiers changed from: private */
    @NonNull
    public final a adConfig;
    @NonNull
    private final dc section;

    @NonNull
    public static hx a(@NonNull dc dcVar, @NonNull a aVar) {
        return new hx(dcVar, aVar);
    }

    private hx(@NonNull dc dcVar, @NonNull a aVar) {
        this.section = dcVar;
        this.adConfig = aVar;
    }

    public void a(@NonNull final cr crVar, boolean z, @NonNull Context context) {
        if (crVar.isHasNotification() != z) {
            crVar.setHasNotification(z);
            final Context applicationContext = context.getApplicationContext();
            ai.a(new Runnable() {
                public void run() {
                    String a = hx.this.b(crVar);
                    if (a != null) {
                        hs K = hs.K(applicationContext);
                        if (K == null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("unable to open disk cache and save text data for slotId: ");
                            sb.append(hx.this.adConfig.getSlotId());
                            ah.a(sb.toString());
                            return;
                        }
                        K.a(hx.this.adConfig.getSlotId(), a, true);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    public String b(@NonNull cr crVar) {
        String id = crVar.getId();
        try {
            JSONObject cd = this.section.cd();
            if (cd == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to change cached notification for banner ");
                sb.append(id);
                sb.append(": no raw data in section");
                ah.a(sb.toString());
                return null;
            }
            JSONObject jSONObject = cd.getJSONObject(this.section.getName());
            if (jSONObject == null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to change cached notification for banner ");
                sb2.append(id);
                sb2.append(": no section object in raw data");
                ah.a(sb2.toString());
                return null;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("banners");
            if (jSONArray == null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("unable to change cached notification for banner ");
                sb3.append(id);
                sb3.append(": no banners array in section object");
                ah.a(sb3.toString());
                return null;
            }
            int length = jSONArray.length();
            int i = 0;
            while (i < length) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                String string = jSONObject2.getString("bannerID");
                if (string == null || !string.equals(id)) {
                    i++;
                } else {
                    jSONObject2.put("hasNotification", crVar.isHasNotification());
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("notification changed in raw data for banner ");
                    sb4.append(id);
                    ah.a(sb4.toString());
                    return cd.toString();
                }
            }
            return null;
        } catch (JSONException e) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("error updating cached notification for section ");
            sb5.append(this.section.getName());
            sb5.append(" and banner ");
            sb5.append(id);
            sb5.append(": ");
            sb5.append(e);
            ah.a(sb5.toString());
        }
    }
}
