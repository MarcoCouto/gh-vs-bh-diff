package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InterstitialAdImageBannerParser */
public class eb {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;

    @NonNull
    public static eb e(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new eb(bzVar, aVar, context2);
    }

    private eb(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eA = bzVar;
        this.adConfig = aVar;
        this.context = context2;
    }

    /* access modifiers changed from: 0000 */
    public boolean b(@NonNull JSONObject jSONObject, @NonNull cm cmVar) {
        JSONArray optJSONArray = jSONObject.optJSONArray("portrait");
        JSONArray optJSONArray2 = jSONObject.optJSONArray("landscape");
        boolean z = false;
        if ((optJSONArray == null || optJSONArray.length() <= 0) && (optJSONArray2 == null || optJSONArray2.length() <= 0)) {
            b("No images in InterstitialAdImageBanner", "Required field", cmVar.getId());
            return false;
        }
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    ImageData c = c(optJSONObject, cmVar.getId());
                    if (c != null) {
                        cmVar.addPortraitImage(c);
                    }
                }
            }
        }
        if (optJSONArray2 != null) {
            int length2 = optJSONArray2.length();
            for (int i2 = 0; i2 < length2; i2++) {
                JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                if (optJSONObject2 != null) {
                    ImageData c2 = c(optJSONObject2, cmVar.getId());
                    if (c2 != null) {
                        cmVar.addLandscapeImage(c2);
                    }
                }
            }
        }
        if (!cmVar.getLandscapeImages().isEmpty() || !cmVar.getPortraitImages().isEmpty()) {
            z = true;
        }
        return z;
    }

    @Nullable
    private ImageData c(@NonNull JSONObject jSONObject, @NonNull String str) {
        String optString = jSONObject.optString("imageLink");
        if (TextUtils.isEmpty(optString)) {
            b("InterstitialAdImageBanner no imageLink for image", "Required field", str);
            return null;
        }
        int optInt = jSONObject.optInt("width");
        int optInt2 = jSONObject.optInt("height");
        if (optInt > 0 && optInt2 > 0) {
            return ImageData.newImageData(optString, optInt, optInt2);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("InterstitialAdImageBanner  image has wrong dimensions, w = ");
        sb.append(optInt);
        sb.append(", h = ");
        sb.append(optInt2);
        b(sb.toString(), "Required field", str);
        return null;
    }

    private void b(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        dq.P(str2).Q(str).S(str3).x(this.adConfig.getSlotId()).R(this.eA.getUrl()).q(this.context);
    }
}
