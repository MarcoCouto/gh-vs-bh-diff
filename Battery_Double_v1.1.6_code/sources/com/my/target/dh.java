package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: Stat */
public class dh {
    @NonNull
    private final String type;
    @NonNull
    private final String url;

    @NonNull
    public static dh c(@NonNull String str, @NonNull String str2) {
        return new dh(str, str2);
    }

    protected dh(@NonNull String str, @NonNull String str2) {
        this.type = str;
        this.url = str2;
    }

    @NonNull
    public String getType() {
        return this.type;
    }

    @NonNull
    public String getUrl() {
        return this.url;
    }
}
