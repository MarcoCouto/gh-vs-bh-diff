package com.my.target;

import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: StatHolder */
public class di {
    private final Set<dh> ee = new HashSet();
    private final Set<dg> ef = new HashSet();
    private final ArrayList<df> eg = new ArrayList<>();
    private final ArrayList<de> eh = new ArrayList<>();

    @NonNull
    public static di cs() {
        return new di();
    }

    private di() {
    }

    public void b(@NonNull dh dhVar) {
        if (dhVar instanceof dg) {
            this.ef.add((dg) dhVar);
        } else if (dhVar instanceof df) {
            df dfVar = (df) dhVar;
            if (this.eg.isEmpty()) {
                this.eg.add(dfVar);
                return;
            }
            int size = this.eg.size();
            while (size > 0 && ((df) this.eg.get(size - 1)).cq() < dfVar.cq()) {
                size--;
            }
            this.eg.add(size, dfVar);
        } else if (dhVar instanceof de) {
            this.eh.add((de) dhVar);
        } else {
            this.ee.add(dhVar);
        }
    }

    @NonNull
    public ArrayList<df> ct() {
        return new ArrayList<>(this.eg);
    }

    @NonNull
    public ArrayList<de> cu() {
        return new ArrayList<>(this.eh);
    }

    @NonNull
    public Set<dg> cv() {
        return new HashSet(this.ef);
    }

    @NonNull
    public Set<dh> cw() {
        return new HashSet(this.ee);
    }

    @NonNull
    public ArrayList<dh> N(@NonNull String str) {
        ArrayList<dh> arrayList = new ArrayList<>();
        for (dh dhVar : this.ee) {
            if (str.equals(dhVar.getType())) {
                arrayList.add(dhVar);
            }
        }
        return arrayList;
    }

    public void a(@NonNull List<dg> list) {
        list.addAll(this.ef);
        Collections.sort(list, new Comparator<dg>() {
            /* renamed from: a */
            public int compare(dg dgVar, dg dgVar2) {
                return (int) (dgVar2.cq() - dgVar.cq());
            }
        });
    }

    public void a(@NonNull di diVar, float f) {
        this.ee.addAll(diVar.cw());
        this.eh.addAll(diVar.cu());
        if (f <= 0.0f) {
            this.ef.addAll(diVar.cv());
            this.eg.addAll(diVar.ct());
            return;
        }
        for (dg dgVar : diVar.cv()) {
            float cr = dgVar.cr();
            if (cr >= 0.0f) {
                dgVar.i((cr * f) / 100.0f);
                dgVar.j(-1.0f);
            }
            b(dgVar);
        }
        Iterator it = diVar.ct().iterator();
        while (it.hasNext()) {
            df dfVar = (df) it.next();
            float cr2 = dfVar.cr();
            if (cr2 >= 0.0f) {
                dfVar.i((cr2 * f) / 100.0f);
                dfVar.j(-1.0f);
            }
            b(dfVar);
        }
    }

    public void e(@NonNull ArrayList<dh> arrayList) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            b((dh) it.next());
        }
    }

    public void f(@NonNull ArrayList<dg> arrayList) {
        this.ef.addAll(arrayList);
    }

    public boolean cx() {
        return !this.ee.isEmpty() || !this.ef.isEmpty() || !this.eg.isEmpty() || !this.eh.isEmpty();
    }
}
