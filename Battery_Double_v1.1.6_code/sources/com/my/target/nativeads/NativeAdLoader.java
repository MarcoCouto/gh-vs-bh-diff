package com.my.target.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import com.my.target.ah;
import com.my.target.b;
import com.my.target.b.C0056b;
import com.my.target.common.BaseAd;
import com.my.target.cp;
import com.my.target.db;
import com.my.target.v;
import java.util.ArrayList;
import java.util.List;

public final class NativeAdLoader extends BaseAd {
    /* access modifiers changed from: private */
    @Nullable
    public b<db> adFactory;
    @NonNull
    private final Context appContext;
    @Nullable
    private OnLoad onLoad;

    public interface OnLoad {
        void onLoad(@NonNull List<NativeAd> list);
    }

    @NonNull
    public static NativeAdLoader newLoader(int i, int i2, @NonNull Context context) {
        return new NativeAdLoader(i, i2, context);
    }

    private NativeAdLoader(int i, int i2, @NonNull Context context) {
        super(i, "nativeads");
        int i3 = 1;
        if (i2 < 1) {
            ah.a("NativeAdLoader: invalid bannersCount < 1, bannersCount set to 1");
        } else {
            i3 = i2;
        }
        this.adConfig.setBannersCount(i3);
        this.adConfig.setMediationEnabled(false);
        this.appContext = context.getApplicationContext();
        ah.c("NativeAdLoader created. Version: 5.5.6");
    }

    @UiThread
    @NonNull
    public NativeAdLoader setOnLoad(@Nullable OnLoad onLoad2) {
        this.onLoad = onLoad2;
        return this;
    }

    public void setAutoLoadVideo(boolean z) {
        this.adConfig.setAutoLoadVideo(z);
    }

    public void setAutoLoadImages(boolean z) {
        this.adConfig.setAutoLoadImages(z);
    }

    public boolean isAutoLoadImages() {
        return this.adConfig.isAutoLoadImages();
    }

    public boolean isAutoLoadVideo() {
        return this.adConfig.isAutoLoadVideo();
    }

    @UiThread
    @NonNull
    public NativeAdLoader load() {
        final b<db> a = v.a(this.adConfig);
        this.adFactory = a;
        a.a((C0056b<T>) new v.b() {
            public void onResult(@Nullable db dbVar, @Nullable String str) {
                if (a == NativeAdLoader.this.adFactory) {
                    NativeAdLoader.this.adFactory = null;
                    NativeAdLoader.this.handleResult(dbVar, str);
                }
            }
        }).a(this.appContext);
        return this;
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable db dbVar, @Nullable String str) {
        List<cp> list;
        if (this.onLoad != null) {
            if (dbVar == null) {
                list = null;
            } else {
                list = dbVar.bT();
            }
            if (list == null || list.size() < 1) {
                this.onLoad.onLoad(new ArrayList());
                return;
            }
            ArrayList arrayList = new ArrayList();
            for (cp cpVar : list) {
                NativeAd nativeAd = new NativeAd(this.adConfig.getSlotId(), this.appContext);
                nativeAd.setAutoLoadImages(this.adConfig.isAutoLoadImages());
                nativeAd.setAutoLoadVideo(this.adConfig.isAutoLoadVideo());
                nativeAd.setTrackingLocationEnabled(this.adConfig.isTrackingLocationEnabled());
                nativeAd.setTrackingEnvironmentEnabled(this.adConfig.isTrackingEnvironmentEnabled());
                nativeAd.setBanner(cpVar);
                arrayList.add(nativeAd);
            }
            this.onLoad.onLoad(arrayList);
        }
    }
}
