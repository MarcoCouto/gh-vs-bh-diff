package com.my.target.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import com.my.target.ah;
import com.my.target.ao;
import com.my.target.az;
import com.my.target.b.C0056b;
import com.my.target.bb;
import com.my.target.common.BaseAd;
import com.my.target.common.models.ImageData;
import com.my.target.cp;
import com.my.target.ct;
import com.my.target.db;
import com.my.target.hu;
import com.my.target.nativeads.banners.NativePromoBanner;
import com.my.target.v;
import com.my.target.v.b;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map.Entry;
import java.util.WeakHashMap;

public final class NativeAd extends BaseAd {
    private int adChoicesPlacement = 0;
    @NonNull
    private final Context appContext;
    @Nullable
    private ao engine;
    @Nullable
    private NativeAdListener listener;
    private boolean useExoPlayer = true;

    public interface AdChoicesPlacement {
        public static final int BOTTOM_LEFT = 3;
        public static final int BOTTOM_RIGHT = 2;
        public static final int TOP_LEFT = 1;
        public static final int TOP_RIGHT = 0;
    }

    public interface NativeAdListener {
        void onClick(@NonNull NativeAd nativeAd);

        void onLoad(@NonNull NativePromoBanner nativePromoBanner, @NonNull NativeAd nativeAd);

        void onNoAd(@NonNull String str, @NonNull NativeAd nativeAd);

        void onShow(@NonNull NativeAd nativeAd);

        void onVideoComplete(@NonNull NativeAd nativeAd);

        void onVideoPause(@NonNull NativeAd nativeAd);

        void onVideoPlay(@NonNull NativeAd nativeAd);
    }

    static class NativeAdRegHelper {
        @NonNull
        private static final WeakHashMap<View, WeakReference<NativeAd>> nativeInstances = new WeakHashMap<>();

        private NativeAdRegHelper() {
        }

        public static void register(@NonNull View view, @NonNull NativeAd nativeAd) {
            unregister(nativeAd);
            WeakReference weakReference = (WeakReference) nativeInstances.get(view);
            if (weakReference != null) {
                NativeAd nativeAd2 = (NativeAd) weakReference.get();
                if (nativeAd2 != null) {
                    nativeAd2.unregisterView();
                }
            }
            nativeInstances.put(view, new WeakReference(nativeAd));
        }

        public static void unregister(@NonNull NativeAd nativeAd) {
            for (Entry entry : nativeInstances.entrySet()) {
                View view = (View) entry.getKey();
                WeakReference weakReference = (WeakReference) entry.getValue();
                if (weakReference != null) {
                    NativeAd nativeAd2 = (NativeAd) weakReference.get();
                    if (nativeAd2 == null || nativeAd2 == nativeAd) {
                        nativeInstances.remove(view);
                        return;
                    }
                }
            }
        }
    }

    public static void loadImageToView(@NonNull ImageData imageData, @NonNull ImageView imageView) {
        hu.a(imageData, imageView);
    }

    public NativeAd(int i, @NonNull Context context) {
        super(i, "nativeads");
        this.appContext = context.getApplicationContext();
        ah.c("NativeAd created. Version: 5.5.6");
    }

    public void setAutoLoadVideo(boolean z) {
        this.adConfig.setAutoLoadVideo(z);
    }

    public void setAutoLoadImages(boolean z) {
        this.adConfig.setAutoLoadImages(z);
    }

    public boolean isAutoLoadImages() {
        return this.adConfig.isAutoLoadImages();
    }

    public boolean isAutoLoadVideo() {
        return this.adConfig.isAutoLoadVideo();
    }

    public void setMediationEnabled(boolean z) {
        this.adConfig.setMediationEnabled(z);
    }

    public boolean isMediationEnabled() {
        return this.adConfig.isMediationEnabled();
    }

    @Nullable
    public NativeAdListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable NativeAdListener nativeAdListener) {
        this.listener = nativeAdListener;
    }

    @Nullable
    public NativePromoBanner getBanner() {
        if (this.engine == null) {
            return null;
        }
        return this.engine.ab();
    }

    public int getAdChoicesPlacement() {
        return this.adChoicesPlacement;
    }

    public void setAdChoicesPlacement(int i) {
        this.adChoicesPlacement = i;
    }

    public final void load() {
        v.a(this.adConfig).a((C0056b<T>) new b() {
            public void onResult(@Nullable db dbVar, @Nullable String str) {
                NativeAd.this.handleResult(dbVar, str);
            }
        }).a(this.appContext);
    }

    public void loadFromBid(@NonNull String str) {
        this.adConfig.setBidId(str);
        load();
    }

    public final void handleSection(@NonNull db dbVar) {
        v.a(dbVar, this.adConfig).a((C0056b<T>) new b() {
            public void onResult(@Nullable db dbVar, @Nullable String str) {
                NativeAd.this.handleResult(dbVar, str);
            }
        }).a(this.appContext);
    }

    public final void registerView(@NonNull View view, @Nullable List<View> list) {
        NativeAdRegHelper.register(view, this);
        if (this.engine != null) {
            this.engine.registerView(view, list, this.adChoicesPlacement);
        }
    }

    public final void registerView(@NonNull View view) {
        registerView(view, null);
    }

    public final void unregisterView() {
        NativeAdRegHelper.unregister(this);
        if (this.engine != null) {
            this.engine.unregisterView();
        }
    }

    public void useExoPlayer(boolean z) {
        this.useExoPlayer = z;
    }

    public boolean isUseExoPlayer() {
        return this.useExoPlayer;
    }

    @Nullable
    public String getAdSource() {
        if (this.engine != null) {
            return this.engine.aa();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void setBanner(@NonNull cp cpVar) {
        this.engine = bb.a(this, cpVar);
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable db dbVar, @Nullable String str) {
        ct ctVar;
        if (this.listener != null) {
            cp cpVar = null;
            if (dbVar != null) {
                cpVar = dbVar.cc();
                ctVar = dbVar.bI();
            } else {
                ctVar = null;
            }
            if (cpVar != null) {
                this.engine = bb.a(this, cpVar);
                if (this.engine.ab() != null) {
                    this.listener.onLoad(this.engine.ab(), this);
                }
            } else if (ctVar != null) {
                az a = az.a(this, ctVar, this.adConfig);
                this.engine = a;
                a.n(this.appContext);
            } else {
                NativeAdListener nativeAdListener = this.listener;
                if (str == null) {
                    str = "no ad";
                }
                nativeAdListener.onNoAd(str, this);
            }
        }
    }
}
