package com.my.target.nativeads.banners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import com.my.target.cp;
import com.my.target.cq;
import java.util.ArrayList;
import java.util.List;

public class NativePromoBanner {
    /* access modifiers changed from: private */
    @Nullable
    public String advertisingLabel;
    /* access modifiers changed from: private */
    @Nullable
    public String ageRestrictions;
    @Nullable
    private String category;
    /* access modifiers changed from: private */
    @Nullable
    public String ctaText;
    /* access modifiers changed from: private */
    @Nullable
    public String description;
    /* access modifiers changed from: private */
    @Nullable
    public String disclaimer;
    /* access modifiers changed from: private */
    @Nullable
    public String domain;
    /* access modifiers changed from: private */
    public boolean hasVideo;
    /* access modifiers changed from: private */
    @Nullable
    public ImageData icon;
    /* access modifiers changed from: private */
    @Nullable
    public ImageData image;
    @NonNull
    private ArrayList<NativePromoCard> nativePromoCards;
    /* access modifiers changed from: private */
    @NonNull
    public String navigationType;
    /* access modifiers changed from: private */
    public float rating;
    @Nullable
    private String subCategory;
    /* access modifiers changed from: private */
    @Nullable
    public String title;
    /* access modifiers changed from: private */
    public int votes;

    public static class Builder {
        private final NativePromoBanner nativePromoBanner = new NativePromoBanner();

        @NonNull
        public static Builder createBuilder() {
            return new Builder();
        }

        private Builder() {
        }

        @NonNull
        public Builder setHasVideo(boolean z) {
            this.nativePromoBanner.hasVideo = z;
            return this;
        }

        @NonNull
        public Builder setTitle(@Nullable String str) {
            this.nativePromoBanner.title = str;
            return this;
        }

        @NonNull
        public Builder setDescription(@Nullable String str) {
            this.nativePromoBanner.description = str;
            return this;
        }

        @NonNull
        public Builder setCtaText(@Nullable String str) {
            this.nativePromoBanner.ctaText = str;
            return this;
        }

        @NonNull
        public Builder setDomain(@Nullable String str) {
            this.nativePromoBanner.domain = str;
            return this;
        }

        @NonNull
        public Builder setAdvertisingLabel(@Nullable String str) {
            this.nativePromoBanner.advertisingLabel = str;
            return this;
        }

        @NonNull
        public Builder setIcon(@Nullable ImageData imageData) {
            this.nativePromoBanner.icon = imageData;
            return this;
        }

        @NonNull
        public Builder setImage(@Nullable ImageData imageData) {
            this.nativePromoBanner.image = imageData;
            return this;
        }

        @NonNull
        public Builder setRating(float f) {
            this.nativePromoBanner.rating = f;
            return this;
        }

        @NonNull
        public Builder setAgeRestrictions(@Nullable String str) {
            this.nativePromoBanner.ageRestrictions = str;
            return this;
        }

        @NonNull
        public Builder setDisclaimer(@Nullable String str) {
            this.nativePromoBanner.disclaimer = str;
            return this;
        }

        @NonNull
        public Builder setVotes(int i) {
            this.nativePromoBanner.votes = i;
            return this;
        }

        @NonNull
        public Builder setNavigationType(@NonNull String str) {
            if ("web".equals(str) || "store".equals(str)) {
                this.nativePromoBanner.navigationType = str;
            }
            return this;
        }

        @NonNull
        public NativePromoBanner build() {
            return this.nativePromoBanner;
        }
    }

    @NonNull
    public static NativePromoBanner newBanner(@NonNull cp cpVar) {
        return new NativePromoBanner(cpVar);
    }

    private NativePromoBanner(@NonNull cp cpVar) {
        this.navigationType = "web";
        this.nativePromoCards = new ArrayList<>();
        this.navigationType = cpVar.getNavigationType();
        this.rating = cpVar.getRating();
        this.votes = cpVar.getVotes();
        this.hasVideo = cpVar.getVideoBanner() != null;
        String title2 = cpVar.getTitle();
        if (TextUtils.isEmpty(title2)) {
            title2 = null;
        }
        this.title = title2;
        String description2 = cpVar.getDescription();
        if (TextUtils.isEmpty(description2)) {
            description2 = null;
        }
        this.description = description2;
        String ctaText2 = cpVar.getCtaText();
        if (TextUtils.isEmpty(ctaText2)) {
            ctaText2 = null;
        }
        this.ctaText = ctaText2;
        String disclaimer2 = cpVar.getDisclaimer();
        if (TextUtils.isEmpty(disclaimer2)) {
            disclaimer2 = null;
        }
        this.disclaimer = disclaimer2;
        String ageRestrictions2 = cpVar.getAgeRestrictions();
        if (TextUtils.isEmpty(ageRestrictions2)) {
            ageRestrictions2 = null;
        }
        this.ageRestrictions = ageRestrictions2;
        String category2 = cpVar.getCategory();
        if (TextUtils.isEmpty(category2)) {
            category2 = null;
        }
        this.category = category2;
        String subCategory2 = cpVar.getSubCategory();
        if (TextUtils.isEmpty(subCategory2)) {
            subCategory2 = null;
        }
        this.subCategory = subCategory2;
        String domain2 = cpVar.getDomain();
        if (TextUtils.isEmpty(domain2)) {
            domain2 = null;
        }
        this.domain = domain2;
        String advertisingLabel2 = cpVar.getAdvertisingLabel();
        if (TextUtils.isEmpty(advertisingLabel2)) {
            advertisingLabel2 = null;
        }
        this.advertisingLabel = advertisingLabel2;
        this.image = cpVar.getImage();
        this.icon = cpVar.getIcon();
        processCards(cpVar);
    }

    private NativePromoBanner() {
        this.navigationType = "web";
        this.nativePromoCards = new ArrayList<>();
    }

    private void processCards(@NonNull cp cpVar) {
        if (!this.hasVideo) {
            List<cq> nativeAdCards = cpVar.getNativeAdCards();
            if (!nativeAdCards.isEmpty()) {
                for (cq newCard : nativeAdCards) {
                    this.nativePromoCards.add(NativePromoCard.newCard(newCard));
                }
            }
        }
    }

    @Nullable
    public ImageData getIcon() {
        return this.icon;
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getCtaText() {
        return this.ctaText;
    }

    @Nullable
    public String getDisclaimer() {
        return this.disclaimer;
    }

    @Nullable
    public String getAgeRestrictions() {
        return this.ageRestrictions;
    }

    public float getRating() {
        return this.rating;
    }

    public int getVotes() {
        return this.votes;
    }

    @Nullable
    public String getCategory() {
        return this.category;
    }

    @Nullable
    public String getSubCategory() {
        return this.subCategory;
    }

    @Nullable
    public String getDomain() {
        return this.domain;
    }

    @NonNull
    public String getNavigationType() {
        return this.navigationType;
    }

    @Nullable
    public ImageData getImage() {
        return this.image;
    }

    @Nullable
    public String getAdvertisingLabel() {
        return this.advertisingLabel;
    }

    public boolean hasVideo() {
        return this.hasVideo;
    }

    @NonNull
    public ArrayList<NativePromoCard> getCards() {
        return this.nativePromoCards;
    }
}
