package com.my.target.nativeads.banners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import com.my.target.cq;

public class NativePromoCard {
    @Nullable
    private final String ctaText;
    @Nullable
    private final String description;
    @Nullable
    private final ImageData image;
    @Nullable
    private final String title;

    @NonNull
    static NativePromoCard newCard(@NonNull cq cqVar) {
        return new NativePromoCard(cqVar);
    }

    private NativePromoCard(@NonNull cq cqVar) {
        if (!TextUtils.isEmpty(cqVar.getTitle())) {
            this.title = cqVar.getTitle();
        } else {
            this.title = null;
        }
        if (!TextUtils.isEmpty(cqVar.getDescription())) {
            this.description = cqVar.getDescription();
        } else {
            this.description = null;
        }
        if (!TextUtils.isEmpty(cqVar.getCtaText())) {
            this.ctaText = cqVar.getCtaText();
        } else {
            this.ctaText = null;
        }
        this.image = cqVar.getImage();
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getCtaText() {
        return this.ctaText;
    }

    @Nullable
    public ImageData getImage() {
        return this.image;
    }
}
