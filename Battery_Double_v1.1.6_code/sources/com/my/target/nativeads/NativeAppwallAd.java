package com.my.target.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import com.my.target.ah;
import com.my.target.b.C0056b;
import com.my.target.bc;
import com.my.target.common.BaseAd;
import com.my.target.common.models.ImageData;
import com.my.target.cr;
import com.my.target.dc;
import com.my.target.dh;
import com.my.target.hr;
import com.my.target.hu;
import com.my.target.hx;
import com.my.target.ib;
import com.my.target.nativeads.banners.NativeAppwallBanner;
import com.my.target.nativeads.views.AppwallAdView;
import com.my.target.nativeads.views.AppwallAdView.AppwallAdViewListener;
import com.my.target.z;
import com.my.target.z.b;
import com.tapjoy.TJAdUnitConstants.String;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class NativeAppwallAd extends BaseAd {
    private static final String DEFAULT_TITLE = "Apps";
    private static final int DEFAULT_TITLE_BACKGROUND_COLOR = -12232093;
    private static final int DEFAULT_TITLE_SUPPLEMENTARY_COLOR = -13220531;
    private static final int DEFAULT_TITLE_TEXT_COLOR = -1;
    @NonNull
    private final ArrayList<NativeAppwallBanner> banners = new ArrayList<>();
    @NonNull
    private final HashMap<NativeAppwallBanner, cr> bannersMap = new HashMap<>();
    @NonNull
    private final hr clickHandler = hr.et();
    @NonNull
    private final Context context;
    @Nullable
    private bc engine;
    private boolean hideStatusBarInDialog = false;
    @Nullable
    private AppwallAdListener listener;
    @Nullable
    private dc section;
    @NonNull
    private String title = DEFAULT_TITLE;
    private int titleBackgroundColor = DEFAULT_TITLE_BACKGROUND_COLOR;
    private int titleSupplementaryColor = DEFAULT_TITLE_SUPPLEMENTARY_COLOR;
    private int titleTextColor = -1;
    /* access modifiers changed from: private */
    @Nullable
    public WeakReference<AppwallAdView> viewWeakReference;

    public interface AppwallAdListener {
        void onClick(@NonNull NativeAppwallBanner nativeAppwallBanner, @NonNull NativeAppwallAd nativeAppwallAd);

        void onDismiss(@NonNull NativeAppwallAd nativeAppwallAd);

        void onDisplay(@NonNull NativeAppwallAd nativeAppwallAd);

        void onLoad(@NonNull NativeAppwallAd nativeAppwallAd);

        void onNoAd(@NonNull String str, @NonNull NativeAppwallAd nativeAppwallAd);
    }

    public static void loadImageToView(@NonNull ImageData imageData, @NonNull ImageView imageView) {
        hu.a(imageData, imageView);
    }

    public NativeAppwallAd(int i, @NonNull Context context2) {
        super(i, "appwall");
        this.context = context2;
        this.adConfig.setAutoLoadImages(true);
        ah.c("NativeAppwallAd created. Version: 5.5.6");
    }

    @Nullable
    public AppwallAdListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable AppwallAdListener appwallAdListener) {
        this.listener = appwallAdListener;
    }

    public boolean isHideStatusBarInDialog() {
        return this.hideStatusBarInDialog;
    }

    public void setHideStatusBarInDialog(boolean z) {
        this.hideStatusBarInDialog = z;
    }

    public void setAutoLoadImages(boolean z) {
        this.adConfig.setAutoLoadImages(z);
    }

    public boolean isAutoLoadImages() {
        return this.adConfig.isAutoLoadImages();
    }

    public long getCachePeriod() {
        return this.adConfig.getCachePeriod();
    }

    public void setCachePeriod(long j) {
        this.adConfig.setCachePeriod(j);
    }

    @NonNull
    public ArrayList<NativeAppwallBanner> getBanners() {
        return this.banners;
    }

    @NonNull
    public String getTitle() {
        return this.title;
    }

    public void setTitle(@NonNull String str) {
        this.title = str;
    }

    public int getTitleBackgroundColor() {
        return this.titleBackgroundColor;
    }

    public void setTitleBackgroundColor(int i) {
        this.titleBackgroundColor = i;
    }

    public int getTitleSupplementaryColor() {
        return this.titleSupplementaryColor;
    }

    public void setTitleSupplementaryColor(int i) {
        this.titleSupplementaryColor = i;
    }

    public int getTitleTextColor() {
        return this.titleTextColor;
    }

    public void setTitleTextColor(int i) {
        this.titleTextColor = i;
    }

    public void load() {
        z.a(this.adConfig).a((C0056b<T>) new b() {
            public void onResult(@Nullable dc dcVar, @Nullable String str) {
                NativeAppwallAd.this.handleResult(dcVar, str);
            }
        }).a(this.context);
    }

    public boolean hasNotifications() {
        for (NativeAppwallBanner isHasNotification : this.bannersMap.keySet()) {
            if (isHasNotification.isHasNotification()) {
                return true;
            }
        }
        return false;
    }

    public void registerAppwallAdView(@NonNull AppwallAdView appwallAdView) {
        unregisterAppwallAdView();
        this.viewWeakReference = new WeakReference<>(appwallAdView);
        appwallAdView.setAppwallAdViewListener(new AppwallAdViewListener() {
            public void onBannersShow(@NonNull List<NativeAppwallBanner> list) {
                NativeAppwallAd.this.handleBannersShow(list);
            }

            public void onBannerClick(@NonNull NativeAppwallBanner nativeAppwallBanner) {
                NativeAppwallAd.this.handleBannerClick(nativeAppwallBanner);
                if (NativeAppwallAd.this.viewWeakReference != null) {
                    AppwallAdView appwallAdView = (AppwallAdView) NativeAppwallAd.this.viewWeakReference.get();
                    if (appwallAdView != null) {
                        appwallAdView.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    public void unregisterAppwallAdView() {
        if (this.viewWeakReference != null) {
            AppwallAdView appwallAdView = (AppwallAdView) this.viewWeakReference.get();
            if (appwallAdView != null) {
                appwallAdView.setAppwallAdViewListener(null);
            }
            this.viewWeakReference.clear();
            this.viewWeakReference = null;
        }
    }

    public void show() {
        if (this.section == null || this.banners.size() <= 0) {
            ah.c("NativeAppwallAd.show: No ad");
            return;
        }
        if (this.engine == null) {
            this.engine = bc.a(this);
        }
        this.engine.k(this.context);
    }

    public void showDialog() {
        if (this.section == null || this.banners.size() <= 0) {
            ah.c("NativeAppwallAd.show: No ad");
            return;
        }
        if (this.engine == null) {
            this.engine = bc.a(this);
        }
        this.engine.l(this.context);
    }

    public void dismiss() {
        if (this.engine != null) {
            this.engine.dismiss();
        }
    }

    public void destroy() {
        unregisterAppwallAdView();
        if (this.engine != null) {
            this.engine.destroy();
            this.engine = null;
        }
        this.listener = null;
    }

    public void handleBannerClick(NativeAppwallBanner nativeAppwallBanner) {
        cr crVar = (cr) this.bannersMap.get(nativeAppwallBanner);
        if (crVar != null) {
            this.clickHandler.b(crVar, this.context);
            if (this.section != null) {
                nativeAppwallBanner.setHasNotification(false);
                hx.a(this.section, this.adConfig).a(crVar, false, this.context);
            }
            if (this.listener != null) {
                this.listener.onClick(nativeAppwallBanner, this);
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("unable to handle banner click: no internal banner for id ");
        sb.append(nativeAppwallBanner.getId());
        ah.a(sb.toString());
    }

    @Nullable
    public String prepareBannerClickLink(NativeAppwallBanner nativeAppwallBanner) {
        cr crVar = (cr) this.bannersMap.get(nativeAppwallBanner);
        if (crVar != null) {
            ib.a((List<dh>) crVar.getStatHolder().N(String.CLICK), this.context);
            if (this.section != null) {
                hx.a(this.section, this.adConfig).a(crVar, false, this.context);
            }
            return crVar.getTrackingLink();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("unable to handle banner click: no internal banner for id ");
        sb.append(nativeAppwallBanner.getId());
        ah.a(sb.toString());
        return null;
    }

    public void handleBannersShow(@NonNull List<NativeAppwallBanner> list) {
        ArrayList arrayList = new ArrayList();
        for (NativeAppwallBanner nativeAppwallBanner : list) {
            cr crVar = (cr) this.bannersMap.get(nativeAppwallBanner);
            if (crVar != null) {
                arrayList.addAll(crVar.getStatHolder().N("playbackStarted"));
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to handle banner show: no internal banner for id ");
                sb.append(nativeAppwallBanner.getId());
                ah.a(sb.toString());
            }
        }
        if (arrayList.size() > 0) {
            ib.a((List<dh>) arrayList, this.context);
        }
    }

    public void handleBannerShow(@NonNull NativeAppwallBanner nativeAppwallBanner) {
        cr crVar = (cr) this.bannersMap.get(nativeAppwallBanner);
        if (crVar != null) {
            ib.a((List<dh>) crVar.getStatHolder().N("playbackStarted"), this.context);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("unable to handle banner show: no internal banner for id ");
        sb.append(nativeAppwallBanner.getId());
        ah.a(sb.toString());
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable dc dcVar, @Nullable String str) {
        if (this.listener == null) {
            return;
        }
        if (dcVar == null) {
            AppwallAdListener appwallAdListener = this.listener;
            if (str == null) {
                str = "no ad";
            }
            appwallAdListener.onNoAd(str, this);
            return;
        }
        this.section = dcVar;
        for (cr crVar : dcVar.bT()) {
            NativeAppwallBanner newBanner = NativeAppwallBanner.newBanner(crVar);
            this.banners.add(newBanner);
            this.bannersMap.put(newBanner, crVar);
        }
        this.listener.onLoad(this);
    }
}
