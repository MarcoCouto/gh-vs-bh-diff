package com.my.target.nativeads.views;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public interface PromoCardView {
    @NonNull
    Button getCtaButtonView();

    @NonNull
    TextView getDescriptionTextView();

    @NonNull
    MediaAdView getMediaAdView();

    @NonNull
    TextView getTitleTextView();

    @NonNull
    View getView();
}
