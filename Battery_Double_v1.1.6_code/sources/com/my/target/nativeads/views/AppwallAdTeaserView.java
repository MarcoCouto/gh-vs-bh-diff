package com.my.target.nativeads.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.my.target.common.models.ImageData;
import com.my.target.ge;
import com.my.target.gf;
import com.my.target.ic;
import com.my.target.nativeads.banners.NativeAppwallBanner;
import java.util.Locale;

public class AppwallAdTeaserView extends RelativeLayout {
    @Nullable
    private NativeAppwallBanner banner;
    @NonNull
    private final ge bannerIcon;
    @NonNull
    private final ShapeDrawable coinsBgShape;
    @NonNull
    private final TextView coinsCountView;
    @NonNull
    private final ge coinsIconView;
    @NonNull
    private final LinearLayout coinsLayout;
    @NonNull
    private final TextView descrView;
    @NonNull
    private final ge notificationView;
    @NonNull
    private final ge openImageView;
    @NonNull
    private final gf starsRatingView;
    @NonNull
    private final ge statusIconView;
    private final int textColor = Color.rgb(36, 36, 36);
    @NonNull
    private final TextView titleView;
    @NonNull
    private final ic uiUtils;
    private boolean viewed = false;
    @NonNull
    private final TextView votesCountView;

    public AppwallAdTeaserView(Context context) {
        super(context);
        this.bannerIcon = new ge(context);
        this.coinsLayout = new LinearLayout(context);
        this.coinsCountView = new TextView(context);
        this.coinsIconView = new ge(context);
        this.openImageView = new ge(context);
        this.statusIconView = new ge(context);
        this.titleView = new TextView(context);
        this.descrView = new TextView(context);
        this.starsRatingView = new gf(context);
        this.votesCountView = new TextView(context);
        this.notificationView = new ge(context);
        this.uiUtils = ic.P(context);
        float M = (float) this.uiUtils.M(6);
        this.coinsBgShape = new ShapeDrawable(new RoundRectShape(new float[]{M, M, M, M, M, M, M, M}, null, null));
        initView();
    }

    public void setNativeAppwallBanner(NativeAppwallBanner nativeAppwallBanner) {
        this.banner = nativeAppwallBanner;
        this.bannerIcon.setImageData(nativeAppwallBanner.getIcon());
        ImageData bubbleIcon = nativeAppwallBanner.getBubbleIcon();
        this.notificationView.setImageData(bubbleIcon);
        String description = nativeAppwallBanner.getDescription();
        this.titleView.setText(nativeAppwallBanner.getTitle());
        this.descrView.setText(description);
        if (nativeAppwallBanner.isHasNotification()) {
            this.notificationView.setVisibility(0);
            this.notificationView.setImageData(bubbleIcon);
        } else {
            this.notificationView.setVisibility(8);
        }
        if (nativeAppwallBanner.getCoins() > 0) {
            this.coinsLayout.setVisibility(0);
            this.coinsIconView.setImageData(nativeAppwallBanner.getCoinsIcon());
            this.coinsCountView.setText(String.format(Locale.getDefault(), "%d", new Object[]{Integer.valueOf(nativeAppwallBanner.getCoins())}));
            this.coinsCountView.setTextColor(nativeAppwallBanner.getCoinsIconTextColor());
            this.coinsBgShape.getPaint().setColor(nativeAppwallBanner.getCoinsIconBgColor());
            this.openImageView.setVisibility(8);
        } else if (nativeAppwallBanner.isAppInstalled()) {
            this.coinsLayout.setVisibility(8);
            this.openImageView.setVisibility(0);
            this.openImageView.setImageData(nativeAppwallBanner.getGotoAppIcon());
        } else {
            this.coinsLayout.setVisibility(8);
            this.openImageView.setVisibility(8);
        }
        ImageData statusIcon = nativeAppwallBanner.getStatusIcon();
        if (statusIcon != null) {
            this.statusIconView.setVisibility(0);
            this.statusIconView.setImageData(statusIcon);
        } else {
            this.statusIconView.setVisibility(8);
        }
        if (nativeAppwallBanner.getCoins() != 0 || nativeAppwallBanner.isAppInstalled()) {
            this.descrView.setPadding(0, 0, this.uiUtils.M(70), 0);
        } else if (statusIcon != null) {
            this.descrView.setPadding(0, 0, this.uiUtils.M(20), 0);
        }
        if (nativeAppwallBanner.getRating() > 0.0f) {
            this.starsRatingView.setRating(nativeAppwallBanner.getRating());
            this.starsRatingView.setVisibility(0);
            if (nativeAppwallBanner.getVotes() > 0) {
                this.votesCountView.setText(String.format(Locale.getDefault(), "%d", new Object[]{Integer.valueOf(nativeAppwallBanner.getVotes())}));
                this.votesCountView.setVisibility(0);
                return;
            }
            this.votesCountView.setVisibility(8);
            return;
        }
        this.starsRatingView.setVisibility(8);
        this.votesCountView.setVisibility(8);
        this.descrView.setPadding(this.descrView.getPaddingLeft(), this.descrView.getPaddingTop(), this.descrView.getPaddingRight(), this.uiUtils.M(20));
    }

    @Nullable
    public NativeAppwallBanner getBanner() {
        return this.banner;
    }

    public boolean isViewed() {
        return this.viewed;
    }

    public void setViewed(boolean z) {
        this.viewed = z;
    }

    @NonNull
    public ImageView getNotificationImageView() {
        return this.notificationView;
    }

    @NonNull
    public ImageView getOpenImageView() {
        return this.openImageView;
    }

    @NonNull
    public ImageView getBannerIconImageView() {
        return this.bannerIcon;
    }

    @NonNull
    public TextView getCoinsCountTextView() {
        return this.coinsCountView;
    }

    @NonNull
    public TextView getDescriptionTextView() {
        return this.descrView;
    }

    @NonNull
    public gf getStarsRatingView() {
        return this.starsRatingView;
    }

    @NonNull
    public TextView getVotesCountTextView() {
        return this.votesCountView;
    }

    @NonNull
    public ImageView getStatusIconImageView() {
        return this.statusIconView;
    }

    @NonNull
    public TextView getTitleTextView() {
        return this.titleView;
    }

    @NonNull
    public ImageView getCoinsIconImageView() {
        return this.coinsIconView;
    }

    /* access modifiers changed from: protected */
    public void removeNotification() {
        removeView(this.notificationView);
    }

    private void initView() {
        int M = this.uiUtils.M(18);
        int M2 = this.uiUtils.M(14);
        int M3 = this.uiUtils.M(53);
        int eG = ic.eG();
        int eG2 = ic.eG();
        int eG3 = ic.eG();
        setBackgroundColor(-1);
        LayoutParams layoutParams = new LayoutParams(M3 + M2 + M2, M3 + M + M);
        this.bannerIcon.setPadding(M2, M, M2, M);
        addView(this.bannerIcon, layoutParams);
        int M4 = this.uiUtils.M(20);
        LayoutParams layoutParams2 = new LayoutParams(M4, M4);
        layoutParams2.leftMargin = this.uiUtils.M(57);
        layoutParams2.topMargin = this.uiUtils.M(10);
        this.notificationView.setLayoutParams(layoutParams2);
        addView(this.notificationView);
        LayoutParams layoutParams3 = new LayoutParams(M3, M3);
        layoutParams3.addRule(11);
        layoutParams3.rightMargin = M2;
        layoutParams3.topMargin = M;
        this.coinsLayout.setBackgroundDrawable(this.coinsBgShape);
        this.coinsLayout.setOrientation(1);
        addView(this.coinsLayout, layoutParams3);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        this.coinsCountView.setTypeface(Typeface.SANS_SERIF);
        this.coinsCountView.setPadding(0, this.uiUtils.M(10), 0, this.uiUtils.M(2));
        this.coinsCountView.setTextSize(2, 13.0f);
        this.coinsCountView.setGravity(49);
        this.coinsLayout.addView(this.coinsCountView, layoutParams4);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(this.uiUtils.M(20), this.uiUtils.M(20));
        layoutParams5.gravity = 1;
        this.coinsLayout.addView(this.coinsIconView, layoutParams5);
        LayoutParams layoutParams6 = new LayoutParams(this.uiUtils.M(19), -2);
        layoutParams6.addRule(15);
        layoutParams6.addRule(11);
        layoutParams6.rightMargin = this.uiUtils.M(30);
        addView(this.openImageView, layoutParams6);
        LayoutParams layoutParams7 = new LayoutParams(M3, M3);
        layoutParams7.addRule(10);
        layoutParams7.addRule(11);
        addView(this.statusIconView, layoutParams7);
        this.titleView.setTypeface(Typeface.SANS_SERIF);
        this.titleView.setTextSize(2, 18.0f);
        this.titleView.setTextColor(this.textColor);
        this.titleView.setPadding(0, 0, this.uiUtils.M(67), 0);
        this.titleView.setId(eG3);
        LayoutParams layoutParams8 = new LayoutParams(-1, -2);
        layoutParams8.leftMargin = this.uiUtils.M(91);
        layoutParams8.rightMargin = this.uiUtils.M(15);
        layoutParams8.topMargin = this.uiUtils.M(13);
        this.titleView.setLayoutParams(layoutParams8);
        addView(this.titleView);
        this.descrView.setTypeface(Typeface.SANS_SERIF);
        this.descrView.setTextSize(2, 13.0f);
        this.descrView.setTextColor(this.textColor);
        LayoutParams layoutParams9 = new LayoutParams(-1, -2);
        layoutParams9.leftMargin = this.uiUtils.M(91);
        layoutParams9.addRule(3, eG3);
        this.descrView.setId(eG);
        this.descrView.setLayoutParams(layoutParams9);
        addView(this.descrView);
        LayoutParams layoutParams10 = new LayoutParams(-2, -2);
        layoutParams10.addRule(3, eG);
        layoutParams10.leftMargin = this.uiUtils.M(91);
        layoutParams10.topMargin = this.uiUtils.M(5);
        this.starsRatingView.setPadding(0, 0, 0, this.uiUtils.M(20));
        this.starsRatingView.setStarsPadding((float) this.uiUtils.M(2));
        this.starsRatingView.setStarSize(this.uiUtils.M(12));
        this.starsRatingView.setId(eG2);
        addView(this.starsRatingView, layoutParams10);
        LayoutParams layoutParams11 = new LayoutParams(-2, -2);
        layoutParams11.addRule(1, eG2);
        layoutParams11.addRule(3, eG);
        layoutParams11.leftMargin = this.uiUtils.M(9);
        this.votesCountView.setTypeface(Typeface.SANS_SERIF);
        this.votesCountView.setPadding(0, this.uiUtils.M(2), 0, 0);
        this.votesCountView.setTextSize(2, 13.0f);
        this.votesCountView.setTextColor(this.textColor);
        this.votesCountView.setGravity(16);
        addView(this.votesCountView, layoutParams11);
    }
}
