package com.my.target.nativeads.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.StateSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.my.target.R;
import com.my.target.ah;
import com.my.target.common.models.ImageData;
import com.my.target.fu;
import com.my.target.ge;
import com.my.target.gf;
import com.my.target.hu;
import com.my.target.ic;
import com.my.target.nativeads.banners.NativePromoBanner;

public class ChatListAdView extends RelativeLayout {
    private static final int COLOR_PLACEHOLDER_GRAY = -1118482;
    private static final int LABELS_ID = ic.eG();
    private static final int LETTERS_GREY = -6710887;
    private static final int RATING_ID = ic.eG();
    private static final int TITLE_2_ID = ic.eG();
    @NonNull
    private final TextView advertisingLabel;
    @NonNull
    private final fu ageRestrictionLabel;
    @Nullable
    private NativePromoBanner banner;
    @NonNull
    private final TextView descriptionLabel;
    @NonNull
    private final TextView disclaimerLabel;
    @NonNull
    private final ge iconImageView;
    @NonNull
    private final LinearLayout labelsLayout;
    @NonNull
    private final LinearLayout ratingLayout;
    @NonNull
    private final gf starsView;
    @NonNull
    private final TextView titleLabel;
    @NonNull
    private final ic uiUtils;
    @NonNull
    private final TextView urlLabel;
    @NonNull
    private final TextView votesLabel;

    public ChatListAdView(@NonNull Context context) {
        this(context, null);
    }

    public ChatListAdView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ChatListAdView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.ageRestrictionLabel = new fu(context);
        this.iconImageView = new ge(context);
        this.advertisingLabel = new TextView(context);
        this.labelsLayout = new LinearLayout(context);
        this.titleLabel = new TextView(context);
        this.urlLabel = new TextView(context);
        this.descriptionLabel = new TextView(context);
        this.ratingLayout = new LinearLayout(context);
        this.starsView = new gf(context);
        this.votesLabel = new TextView(context);
        this.disclaimerLabel = new TextView(context);
        this.uiUtils = ic.P(context);
        setId(R.id.nativeads_ad_view);
        this.ageRestrictionLabel.setId(R.id.nativeads_age_restrictions);
        this.advertisingLabel.setId(R.id.nativeads_advertising);
        this.iconImageView.setId(R.id.nativeads_icon);
        this.labelsLayout.setId(LABELS_ID);
        this.titleLabel.setId(R.id.nativeads_title);
        this.urlLabel.setId(R.id.nativeads_domain);
        this.descriptionLabel.setId(R.id.nativeads_description);
        this.ratingLayout.setId(RATING_ID);
        this.starsView.setId(R.id.nativeads_rating);
        this.votesLabel.setId(R.id.nativeads_votes);
        this.disclaimerLabel.setId(R.id.nativeads_disclaimer);
        initView();
    }

    @NonNull
    public TextView getAdvertisingTextView() {
        return this.advertisingLabel;
    }

    @NonNull
    public TextView getAgeRestrictionTextView() {
        return this.ageRestrictionLabel;
    }

    @NonNull
    public TextView getDescriptionTextView() {
        return this.descriptionLabel;
    }

    @NonNull
    public TextView getDisclaimerTextView() {
        return this.disclaimerLabel;
    }

    @NonNull
    public TextView getDomainOrCategoryTextView() {
        return this.urlLabel;
    }

    @NonNull
    public ImageView getIconImageView() {
        return this.iconImageView;
    }

    @NonNull
    public gf getStarsRatingView() {
        return this.starsView;
    }

    @NonNull
    public TextView getTitleTextView() {
        return this.titleLabel;
    }

    @NonNull
    public TextView getVotesTextView() {
        return this.votesLabel;
    }

    public void setupView(@Nullable NativePromoBanner nativePromoBanner) {
        if (nativePromoBanner != null) {
            this.banner = nativePromoBanner;
            ah.a("Setup banner");
            if ("web".equals(nativePromoBanner.getNavigationType())) {
                this.urlLabel.setVisibility(0);
                this.ratingLayout.setVisibility(8);
                this.urlLabel.setText(nativePromoBanner.getDomain());
            } else if ("store".equals(nativePromoBanner.getNavigationType())) {
                String category = nativePromoBanner.getCategory();
                String subCategory = nativePromoBanner.getSubCategory();
                String str = "";
                if (!TextUtils.isEmpty(category)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(category);
                    str = sb.toString();
                }
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(subCategory)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(", ");
                    str = sb2.toString();
                }
                if (!TextUtils.isEmpty(subCategory)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append(subCategory);
                    str = sb3.toString();
                }
                if (nativePromoBanner.getRating() <= 0.0f || nativePromoBanner.getRating() > 5.0f) {
                    this.urlLabel.setVisibility(0);
                    this.urlLabel.setText(str);
                    this.ratingLayout.setVisibility(8);
                    ic.a((View) this.urlLabel, "category_text");
                } else {
                    this.urlLabel.setVisibility(8);
                    ViewParent parent = this.ratingLayout.getParent();
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(this.ratingLayout);
                    }
                    this.labelsLayout.addView(this.ratingLayout, 1);
                    this.ratingLayout.setVisibility(0);
                    this.starsView.setRating(nativePromoBanner.getRating());
                    if (nativePromoBanner.getVotes() > 0) {
                        this.votesLabel.setText(String.valueOf(nativePromoBanner.getVotes()));
                        this.votesLabel.setVisibility(0);
                    } else {
                        this.votesLabel.setVisibility(8);
                    }
                    ic.a((View) this.votesLabel, "votes_text");
                }
            }
            ImageData icon = nativePromoBanner.getIcon();
            if (icon != null) {
                if (icon.getData() != null) {
                    this.iconImageView.setImageBitmap(icon.getData());
                } else {
                    this.iconImageView.setBackgroundColor(-1118482);
                    this.iconImageView.setPlaceholderWidth(icon.getWidth());
                    this.iconImageView.setPlaceholderHeight(icon.getHeight());
                }
            }
            this.titleLabel.setText(nativePromoBanner.getTitle());
            this.descriptionLabel.setText(nativePromoBanner.getDescription());
            this.advertisingLabel.setText(nativePromoBanner.getAdvertisingLabel());
            if (!TextUtils.isEmpty(nativePromoBanner.getAgeRestrictions())) {
                this.ageRestrictionLabel.setText(nativePromoBanner.getAgeRestrictions());
            } else {
                this.ageRestrictionLabel.setVisibility(8);
            }
            String disclaimer = nativePromoBanner.getDisclaimer();
            if (!TextUtils.isEmpty(disclaimer)) {
                this.disclaimerLabel.setText(disclaimer);
            } else {
                this.disclaimerLabel.setVisibility(8);
            }
        }
    }

    public void loadImages() {
        if (this.banner != null) {
            ImageData icon = this.banner.getIcon();
            if (icon != null) {
                if (icon.getData() == null) {
                    hu.a(icon, (ImageView) this.iconImageView);
                } else {
                    this.iconImageView.setImageBitmap(icon.getData());
                }
            }
        }
    }

    private void initView() {
        setPadding(this.uiUtils.M(12), this.uiUtils.M(12), this.uiUtils.M(12), this.uiUtils.M(12));
        this.ageRestrictionLabel.f(1, -7829368);
        this.ageRestrictionLabel.setPadding(this.uiUtils.M(2), 0, 0, 0);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.rightMargin = this.uiUtils.M(9);
        this.ageRestrictionLabel.setLayoutParams(layoutParams);
        this.ageRestrictionLabel.setTextColor(LETTERS_GREY);
        this.ageRestrictionLabel.f(1, LETTERS_GREY);
        this.ageRestrictionLabel.setBackgroundColor(0);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(1, R.id.nativeads_age_restrictions);
        this.advertisingLabel.setLayoutParams(layoutParams2);
        this.advertisingLabel.setTextSize(2, 14.0f);
        this.advertisingLabel.setTextColor(LETTERS_GREY);
        LayoutParams layoutParams3 = new LayoutParams(this.uiUtils.M(54), this.uiUtils.M(54));
        layoutParams3.addRule(3, R.id.nativeads_advertising);
        layoutParams3.topMargin = this.uiUtils.M(2);
        this.iconImageView.setLayoutParams(layoutParams3);
        this.labelsLayout.setOrientation(1);
        this.labelsLayout.setMinimumHeight(this.uiUtils.M(54));
        LayoutParams layoutParams4 = new LayoutParams(-1, -2);
        layoutParams4.addRule(3, R.id.nativeads_advertising);
        layoutParams4.addRule(1, R.id.nativeads_icon);
        layoutParams4.leftMargin = this.uiUtils.M(9);
        layoutParams4.topMargin = this.uiUtils.M(3);
        this.labelsLayout.setLayoutParams(layoutParams4);
        this.titleLabel.setLayoutParams(new LayoutParams(-2, -2));
        this.titleLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.titleLabel.setTextSize(2, 16.0f);
        this.titleLabel.setTypeface(null, 1);
        LayoutParams layoutParams5 = new LayoutParams(-2, -2);
        layoutParams5.topMargin = this.uiUtils.M(2);
        this.urlLabel.setLayoutParams(layoutParams5);
        this.urlLabel.setTextColor(LETTERS_GREY);
        this.urlLabel.setTextSize(2, 14.0f);
        LayoutParams layoutParams6 = new LayoutParams(-2, -2);
        layoutParams6.topMargin = this.uiUtils.M(2);
        this.descriptionLabel.setLayoutParams(layoutParams6);
        this.descriptionLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.descriptionLabel.setTextSize(2, 14.0f);
        this.ratingLayout.setOrientation(0);
        LayoutParams layoutParams7 = new LayoutParams(-2, -2);
        layoutParams7.addRule(3, TITLE_2_ID);
        this.ratingLayout.setLayoutParams(layoutParams7);
        LinearLayout.LayoutParams layoutParams8 = new LinearLayout.LayoutParams(this.uiUtils.M(73), this.uiUtils.M(12));
        layoutParams8.topMargin = this.uiUtils.M(4);
        layoutParams8.rightMargin = this.uiUtils.M(4);
        this.starsView.setLayoutParams(layoutParams8);
        this.votesLabel.setTextColor(LETTERS_GREY);
        this.votesLabel.setTextSize(2, 14.0f);
        LayoutParams layoutParams9 = new LayoutParams(-2, -2);
        layoutParams9.addRule(3, RATING_ID);
        this.disclaimerLabel.setLayoutParams(layoutParams9);
        this.disclaimerLabel.setTextColor(LETTERS_GREY);
        this.disclaimerLabel.setTextSize(2, 12.0f);
        ic.a(this, 0, -3806472);
        GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0, 0});
        gradientDrawable.setStroke(this.uiUtils.M(1), -3355444);
        gradientDrawable.setCornerRadius((float) this.uiUtils.M(1));
        GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-3806472, -3806472});
        gradientDrawable2.setStroke(this.uiUtils.M(1), -3355444);
        gradientDrawable2.setCornerRadius((float) this.uiUtils.M(1));
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
        stateListDrawable.addState(StateSet.WILD_CARD, gradientDrawable);
        setClickable(true);
        addView(this.ageRestrictionLabel);
        addView(this.advertisingLabel);
        addView(this.iconImageView);
        addView(this.labelsLayout);
        this.labelsLayout.addView(this.titleLabel);
        this.labelsLayout.addView(this.urlLabel);
        this.labelsLayout.addView(this.descriptionLabel);
        this.labelsLayout.addView(this.disclaimerLabel);
        addView(this.ratingLayout);
        this.ratingLayout.addView(this.starsView);
        this.ratingLayout.addView(this.votesLabel);
    }
}
