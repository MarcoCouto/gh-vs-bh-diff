package com.my.target.nativeads.views;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.my.target.fn;
import com.my.target.fz;
import com.my.target.ge;
import com.my.target.ic;

public class MediaAdView extends FrameLayout {
    private static final int BUTTON_ID = ic.eG();
    public static final int COLOR_PLACEHOLDER_GRAY = -1118482;
    private static final int IMAGE_ID = ic.eG();
    private static final int PROGRESS_ID = ic.eG();
    @NonNull
    private final ge imageView;
    private float mediaAspectRatio;
    private int placeholderHeight;
    private int placeholderWidth;
    @NonNull
    private final fz playButton;
    @NonNull
    private final ProgressBar progressBar;

    public MediaAdView(@NonNull Context context) {
        super(context);
        this.imageView = new ge(context);
        this.playButton = new fz(context);
        this.progressBar = new ProgressBar(context, null, 16842871);
        initViews(context);
    }

    public MediaAdView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        this.imageView = new ge(context);
        this.playButton = new fz(context);
        this.progressBar = new ProgressBar(context, null, 16842871);
        initViews(context);
    }

    public MediaAdView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.imageView = new ge(context);
        this.playButton = new fz(context);
        this.progressBar = new ProgressBar(context, null, 16842871);
        initViews(context);
    }

    @NonNull
    public ProgressBar getProgressBarView() {
        return this.progressBar;
    }

    @NonNull
    public ImageView getImageView() {
        return this.imageView;
    }

    @NonNull
    public View getPlayButtonView() {
        return this.playButton;
    }

    public void setPlaceHolderDimension(int i, int i2) {
        this.placeholderWidth = i;
        this.placeholderHeight = i2;
        this.imageView.setPlaceholderWidth(i);
        this.imageView.setPlaceholderHeight(i2);
        float f = (float) i2;
        float f2 = 0.0f;
        if (f != 0.0f) {
            f2 = ((float) i) / f;
        }
        this.mediaAspectRatio = f2;
    }

    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        super.setOnClickListener(onClickListener);
        this.playButton.setOnClickListener(onClickListener);
    }

    public float getMediaAspectRatio() {
        return this.mediaAspectRatio;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        if (this.placeholderWidth == 0 || this.placeholderHeight == 0) {
            if (mode != 1073741824) {
                size = 0;
            }
            if (mode2 != 1073741824) {
                size2 = 0;
            }
            setMeasuredDimension(size, size2);
            return;
        }
        float f = ((float) this.placeholderWidth) / ((float) this.placeholderHeight);
        if (mode == 0 && mode2 == 0) {
            super.onMeasure(i, i2);
            return;
        }
        if (mode == 0) {
            size = (int) (((float) size2) * f);
        } else if (mode2 == 0) {
            size2 = (int) (((float) size) / f);
        } else if (mode2 != 1073741824) {
            size2 = (int) (((float) size) / f);
        }
        int childCount = getChildCount();
        boolean z = false;
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = childAt.getLayoutParams();
                int i5 = Integer.MIN_VALUE;
                if (layoutParams != null) {
                    i3 = layoutParams.width == -1 ? 1073741824 : Integer.MIN_VALUE;
                    if (layoutParams.height == -1) {
                        i5 = 1073741824;
                    }
                } else {
                    i3 = Integer.MIN_VALUE;
                }
                childAt.measure(MeasureSpec.makeMeasureSpec(size, i3), MeasureSpec.makeMeasureSpec(size2, i5));
                if (childAt.getMeasuredHeight() > 0) {
                    z = true;
                }
            }
        }
        if (z) {
            setMeasuredDimension(size, size2);
        } else {
            setMeasuredDimension(0, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        for (int i5 = 0; i5 < getChildCount(); i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                if (!(measuredWidth == 0 || measuredHeight == 0)) {
                    int i6 = ((i3 - i) - measuredWidth) / 2;
                    int i7 = ((i4 - i2) - measuredHeight) / 2;
                    childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
                }
            }
        }
    }

    private void initViews(@NonNull Context context) {
        ic.a((View) this.imageView, "media_image");
        this.imageView.setId(IMAGE_ID);
        ic.a((View) this.progressBar, "progress_bar");
        this.progressBar.setId(PROGRESS_ID);
        ic.a((View) this.playButton, "play_button");
        this.playButton.setId(BUTTON_ID);
        setBackgroundColor(COLOR_PLACEHOLDER_GRAY);
        this.progressBar.setVisibility(8);
        this.progressBar.getIndeterminateDrawable().setColorFilter(-16733198, Mode.SRC_ATOP);
        this.playButton.a(fn.E(ic.P(context).M(64)), false);
        this.playButton.setVisibility(8);
        addView(this.imageView);
        addView(this.playButton, new FrameLayout.LayoutParams(-2, -2));
        addView(this.progressBar, new FrameLayout.LayoutParams(-2, -2));
    }
}
