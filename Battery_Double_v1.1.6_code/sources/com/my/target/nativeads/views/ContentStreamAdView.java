package com.my.target.nativeads.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.util.StateSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.my.target.R;
import com.my.target.ah;
import com.my.target.common.models.ImageData;
import com.my.target.fu;
import com.my.target.ge;
import com.my.target.gf;
import com.my.target.hu;
import com.my.target.ic;
import com.my.target.nativeads.banners.NativePromoBanner;
import com.my.target.nativeads.banners.NativePromoCard;
import com.my.target.nativeads.factories.NativeViewsFactory;
import com.my.target.nativeads.views.PromoCardRecyclerView.PromoCardAdapter;
import java.util.List;

public class ContentStreamAdView extends RelativeLayout {
    private static final int COLOR_PLACEHOLDER_GRAY = -1118482;
    private static final int LABELS_ID = ic.eG();
    private static final int RATING_ID = ic.eG();
    private static final int STANDARD_BLUE = -16748844;
    private static final int TITLE_2_ID = ic.eG();
    private static final int URL_2_ID = ic.eG();
    @NonNull
    private final TextView advertisingLabel;
    @NonNull
    private final fu ageRestrictionLabel;
    @Nullable
    private NativePromoBanner banner;
    private PromoCardAdapter cardAdapter;
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final TextView descriptionLabel;
    @NonNull
    private final TextView disclaimerLabel;
    @Nullable
    private LayoutParams disclaimerParams;
    @NonNull
    private final ge iconImageView;
    @NonNull
    private final LinearLayout labelsLayout;
    @NonNull
    private final LayoutParams labelsLayoutParams;
    @Nullable
    private MediaAdView mediaAdView;
    @Nullable
    private PromoCardRecyclerView promoCardRecyclerView;
    @NonNull
    private final LinearLayout ratingLayout;
    @NonNull
    private final gf starsView;
    @NonNull
    private final TextView title2Label;
    @NonNull
    private final TextView titleLabel;
    @NonNull
    private final ic uiUtils;
    @NonNull
    private final TextView url2Label;
    @NonNull
    private final TextView urlLabel;
    @NonNull
    private final TextView votesLabel;

    public ContentStreamAdView(@NonNull Context context) {
        this(context, null);
    }

    public ContentStreamAdView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ContentStreamAdView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.ageRestrictionLabel = new fu(context);
        this.advertisingLabel = new TextView(context);
        this.iconImageView = new ge(context);
        this.labelsLayout = new LinearLayout(context);
        this.titleLabel = new TextView(context);
        this.urlLabel = new TextView(context);
        this.descriptionLabel = new TextView(context);
        this.title2Label = new TextView(context);
        this.url2Label = new TextView(context);
        this.ratingLayout = new LinearLayout(context);
        this.starsView = new gf(context);
        this.votesLabel = new TextView(context);
        this.disclaimerLabel = new TextView(context);
        this.ctaButton = new Button(context);
        this.uiUtils = ic.P(context);
        setId(R.id.nativeads_ad_view);
        this.advertisingLabel.setId(R.id.nativeads_advertising);
        this.titleLabel.setId(R.id.nativeads_title);
        this.descriptionLabel.setId(R.id.nativeads_description);
        this.starsView.setId(R.id.nativeads_rating);
        this.urlLabel.setId(R.id.nativeads_domain);
        this.disclaimerLabel.setId(R.id.nativeads_disclaimer);
        this.ctaButton.setId(R.id.nativeads_call_to_action);
        this.iconImageView.setId(R.id.nativeads_icon);
        this.ageRestrictionLabel.setId(R.id.nativeads_age_restrictions);
        this.votesLabel.setId(R.id.nativeads_votes);
        this.labelsLayout.setId(LABELS_ID);
        this.title2Label.setId(TITLE_2_ID);
        this.url2Label.setId(URL_2_ID);
        this.starsView.setId(R.id.nativeads_rating);
        this.ratingLayout.setId(RATING_ID);
        ic.a((View) this.title2Label, "title_text_2");
        ic.a((View) this.votesLabel, "votes_text");
        this.labelsLayoutParams = new LayoutParams(-1, -2);
        initView();
    }

    @NonNull
    public TextView getAdvertisingTextView() {
        return this.advertisingLabel;
    }

    @NonNull
    public TextView getAgeRestrictionTextView() {
        return this.ageRestrictionLabel;
    }

    @NonNull
    public Button getCtaButtonView() {
        return this.ctaButton;
    }

    @NonNull
    public TextView getDescriptionTextView() {
        return this.descriptionLabel;
    }

    @NonNull
    public TextView getDisclaimerTextView() {
        return this.disclaimerLabel;
    }

    @NonNull
    public TextView getDomainOrCategoryTextView() {
        return this.urlLabel;
    }

    @NonNull
    public ImageView getIconImageView() {
        return this.iconImageView;
    }

    @Nullable
    public MediaAdView getMediaAdView() {
        return this.mediaAdView;
    }

    @Nullable
    public PromoCardRecyclerView getPromoCardRecyclerView() {
        return this.promoCardRecyclerView;
    }

    @NonNull
    public TextView getSecondDomainOrCategoryTextView() {
        return this.url2Label;
    }

    @NonNull
    public TextView getSecondTitleTextView() {
        return this.title2Label;
    }

    @NonNull
    public gf getStarsRatingView() {
        return this.starsView;
    }

    @NonNull
    public TextView getTitleTextView() {
        return this.titleLabel;
    }

    @NonNull
    public TextView getVotesTextView() {
        return this.votesLabel;
    }

    public void setupView(@Nullable NativePromoBanner nativePromoBanner) {
        if (nativePromoBanner != null) {
            this.banner = nativePromoBanner;
            ah.a("Setup banner");
            boolean z = !nativePromoBanner.getCards().isEmpty();
            if (z) {
                this.title2Label.setVisibility(8);
                this.url2Label.setVisibility(8);
                this.ctaButton.setVisibility(8);
                this.ratingLayout.setVisibility(8);
                this.disclaimerLabel.setVisibility(8);
                if (!(this.mediaAdView == null || this.mediaAdView.getParent() == null)) {
                    ((ViewGroup) this.mediaAdView.getParent()).removeView(this.mediaAdView);
                    this.mediaAdView = null;
                }
                if (this.promoCardRecyclerView == null) {
                    this.promoCardRecyclerView = NativeViewsFactory.getPromoCardRecyclerView(getContext());
                    this.promoCardRecyclerView.setId(R.id.nativeads_media_view);
                    LayoutParams layoutParams = new LayoutParams(-1, -1);
                    layoutParams.addRule(3, LABELS_ID);
                    layoutParams.topMargin = this.uiUtils.M(2);
                    this.promoCardRecyclerView.setLayoutParams(layoutParams);
                    addView(this.promoCardRecyclerView);
                }
                this.promoCardRecyclerView.setPromoCardAdapter(useAdapter(nativePromoBanner.getCards()));
            } else {
                this.title2Label.setVisibility(0);
                String ctaText = nativePromoBanner.getCtaText();
                if (!TextUtils.isEmpty(ctaText)) {
                    this.ctaButton.setVisibility(0);
                    this.ctaButton.setText(ctaText);
                } else {
                    this.ctaButton.setVisibility(8);
                }
                if (this.promoCardRecyclerView != null) {
                    ViewParent parent = this.promoCardRecyclerView.getParent();
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(this.promoCardRecyclerView);
                        this.promoCardRecyclerView = null;
                    }
                }
                if (this.mediaAdView == null) {
                    this.mediaAdView = NativeViewsFactory.getMediaAdView(getContext());
                    this.mediaAdView.setId(R.id.nativeads_media_view);
                    LayoutParams layoutParams2 = new LayoutParams(-1, -2);
                    layoutParams2.addRule(3, LABELS_ID);
                    layoutParams2.topMargin = this.uiUtils.M(2);
                    this.mediaAdView.setLayoutParams(layoutParams2);
                    addView(this.mediaAdView);
                }
            }
            if ("web".equals(nativePromoBanner.getNavigationType())) {
                if (!z) {
                    this.url2Label.setVisibility(0);
                    this.ratingLayout.setVisibility(8);
                    this.urlLabel.setText(nativePromoBanner.getDomain());
                    this.url2Label.setText(nativePromoBanner.getDomain());
                    this.disclaimerParams = new LayoutParams(-2, -2);
                    this.disclaimerParams.addRule(3, URL_2_ID);
                    this.disclaimerLabel.setLayoutParams(this.disclaimerParams);
                    ic.a((View) this.url2Label, "domain_text_2");
                }
            } else if ("store".equals(nativePromoBanner.getNavigationType())) {
                String category = nativePromoBanner.getCategory();
                String subCategory = nativePromoBanner.getSubCategory();
                String str = "";
                if (!TextUtils.isEmpty(category)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(category);
                    str = sb.toString();
                }
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(subCategory)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(", ");
                    str = sb2.toString();
                }
                if (!TextUtils.isEmpty(subCategory)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append(subCategory);
                    str = sb3.toString();
                }
                ic.a((View) this.urlLabel, "category_text");
                ic.a((View) this.url2Label, "category_text_2");
                if (!z) {
                    if (nativePromoBanner.getRating() <= 0.0f || nativePromoBanner.getRating() > 5.0f) {
                        this.url2Label.setVisibility(0);
                        this.ratingLayout.setVisibility(8);
                        this.url2Label.setText(str);
                        this.disclaimerParams = new LayoutParams(-2, -2);
                        this.disclaimerParams.addRule(3, URL_2_ID);
                        this.disclaimerParams.addRule(0, R.id.nativeads_call_to_action);
                        this.disclaimerParams.rightMargin = this.uiUtils.M(4);
                        this.disclaimerParams.addRule(9, -1);
                        this.disclaimerLabel.setLayoutParams(this.disclaimerParams);
                    } else {
                        this.starsView.setVisibility(0);
                        if (nativePromoBanner.getVotes() > 0) {
                            this.votesLabel.setVisibility(0);
                            this.votesLabel.setText(String.valueOf(nativePromoBanner.getVotes()));
                        } else {
                            this.votesLabel.setVisibility(8);
                        }
                        this.url2Label.setVisibility(8);
                        this.url2Label.setVisibility(8);
                        this.ratingLayout.setVisibility(0);
                        this.urlLabel.setText(str);
                        this.starsView.setRating(nativePromoBanner.getRating());
                        this.disclaimerParams = new LayoutParams(-2, -2);
                        this.disclaimerParams.addRule(3, RATING_ID);
                        this.disclaimerParams.addRule(0, R.id.nativeads_call_to_action);
                        this.disclaimerParams.rightMargin = this.uiUtils.M(4);
                        this.disclaimerParams.addRule(9, -1);
                        this.disclaimerLabel.setLayoutParams(this.disclaimerParams);
                    }
                }
                this.urlLabel.setText(str);
            }
            String disclaimer = nativePromoBanner.getDisclaimer();
            if (!TextUtils.isEmpty(disclaimer)) {
                this.disclaimerLabel.setVisibility(0);
                this.disclaimerLabel.setText(disclaimer);
            } else {
                this.disclaimerLabel.setVisibility(8);
            }
            ImageData icon = nativePromoBanner.getIcon();
            if (icon != null) {
                this.labelsLayoutParams.leftMargin = this.uiUtils.M(9);
                this.iconImageView.setImageData(icon);
                if (icon.getData() == null) {
                    this.iconImageView.setBackgroundColor(-1118482);
                }
            } else {
                this.iconImageView.setImageBitmap(null);
                this.iconImageView.setPlaceholderHeight(0);
                this.iconImageView.setPlaceholderWidth(0);
                this.labelsLayoutParams.leftMargin = 0;
            }
            this.titleLabel.setText(nativePromoBanner.getTitle());
            this.descriptionLabel.setText(nativePromoBanner.getDescription());
            this.advertisingLabel.setText(nativePromoBanner.getAdvertisingLabel());
            if (!TextUtils.isEmpty(nativePromoBanner.getAgeRestrictions())) {
                this.ageRestrictionLabel.setText(nativePromoBanner.getAgeRestrictions());
            } else {
                this.ageRestrictionLabel.setVisibility(8);
            }
            this.title2Label.setText(nativePromoBanner.getTitle());
        }
    }

    public void loadImages() {
        if (this.banner != null) {
            ImageData icon = this.banner.getIcon();
            if (icon != null) {
                if (icon.getData() == null) {
                    hu.a(icon, (ImageView) this.iconImageView);
                } else {
                    this.iconImageView.setImageBitmap(icon.getData());
                }
            }
        }
    }

    private void initView() {
        setPadding(this.uiUtils.M(12), this.uiUtils.M(12), this.uiUtils.M(12), this.uiUtils.M(12));
        this.ageRestrictionLabel.setId(R.id.nativeads_age_restrictions);
        this.ageRestrictionLabel.f(1, -7829368);
        this.ageRestrictionLabel.setPadding(this.uiUtils.M(2), 0, 0, 0);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.rightMargin = this.uiUtils.M(9);
        this.ageRestrictionLabel.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(1, R.id.nativeads_age_restrictions);
        this.advertisingLabel.setLayoutParams(layoutParams2);
        LayoutParams layoutParams3 = new LayoutParams(this.uiUtils.M(54), this.uiUtils.M(54));
        layoutParams3.addRule(3, R.id.nativeads_advertising);
        layoutParams3.topMargin = this.uiUtils.M(9);
        this.iconImageView.setLayoutParams(layoutParams3);
        this.labelsLayout.setOrientation(1);
        this.labelsLayout.setMinimumHeight(this.uiUtils.M(54));
        this.labelsLayoutParams.addRule(3, R.id.nativeads_advertising);
        this.labelsLayoutParams.addRule(1, R.id.nativeads_icon);
        this.labelsLayoutParams.topMargin = this.uiUtils.M(2);
        this.labelsLayout.setLayoutParams(this.labelsLayoutParams);
        this.titleLabel.setLayoutParams(new LayoutParams(-2, -2));
        LayoutParams layoutParams4 = new LayoutParams(-2, -2);
        layoutParams4.topMargin = this.uiUtils.M(2);
        this.urlLabel.setLayoutParams(layoutParams4);
        LayoutParams layoutParams5 = new LayoutParams(-2, -2);
        layoutParams5.topMargin = this.uiUtils.M(2);
        this.descriptionLabel.setLayoutParams(layoutParams5);
        LayoutParams layoutParams6 = new LayoutParams(-2, -2);
        layoutParams6.addRule(3, R.id.nativeads_media_view);
        layoutParams6.topMargin = this.uiUtils.M(2);
        this.title2Label.setLayoutParams(layoutParams6);
        LayoutParams layoutParams7 = new LayoutParams(-2, -2);
        layoutParams7.addRule(3, TITLE_2_ID);
        this.url2Label.setLayoutParams(layoutParams7);
        this.ratingLayout.setOrientation(0);
        LayoutParams layoutParams8 = new LayoutParams(-2, -2);
        layoutParams8.addRule(3, TITLE_2_ID);
        this.ratingLayout.setLayoutParams(layoutParams8);
        LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(this.uiUtils.M(73), this.uiUtils.M(12));
        layoutParams9.topMargin = this.uiUtils.M(4);
        layoutParams9.rightMargin = this.uiUtils.M(4);
        this.starsView.setLayoutParams(layoutParams9);
        this.disclaimerParams = new LayoutParams(-2, -2);
        this.disclaimerParams.addRule(3, RATING_ID);
        this.disclaimerLabel.setLayoutParams(this.disclaimerParams);
        this.ctaButton.setPadding(this.uiUtils.M(10), 0, this.uiUtils.M(10), 0);
        this.ctaButton.setMaxEms(8);
        this.ctaButton.setLines(1);
        this.ctaButton.setEllipsize(TruncateAt.END);
        LayoutParams layoutParams10 = new LayoutParams(-2, this.uiUtils.M(30));
        layoutParams10.addRule(3, TITLE_2_ID);
        layoutParams10.addRule(11);
        this.ctaButton.setLayoutParams(layoutParams10);
        this.ctaButton.setTransformationMethod(null);
        ic.a(this, 0, -3806472);
        GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0, 0});
        gradientDrawable.setStroke(this.uiUtils.M(1), STANDARD_BLUE);
        gradientDrawable.setCornerRadius((float) this.uiUtils.M(1));
        GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-3806472, -3806472});
        gradientDrawable2.setStroke(this.uiUtils.M(1), STANDARD_BLUE);
        gradientDrawable2.setCornerRadius((float) this.uiUtils.M(1));
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
        stateListDrawable.addState(StateSet.WILD_CARD, gradientDrawable);
        this.ctaButton.setBackgroundDrawable(stateListDrawable);
        setClickable(true);
        addView(this.ageRestrictionLabel);
        addView(this.advertisingLabel);
        addView(this.iconImageView);
        addView(this.labelsLayout);
        this.labelsLayout.addView(this.titleLabel);
        this.labelsLayout.addView(this.urlLabel);
        this.labelsLayout.addView(this.descriptionLabel);
        addView(this.title2Label);
        addView(this.url2Label);
        addView(this.ctaButton);
        addView(this.ratingLayout);
        addView(this.disclaimerLabel);
        this.ratingLayout.addView(this.starsView);
        this.ratingLayout.addView(this.votesLabel);
        updateDefaultParams();
    }

    @NonNull
    private PromoCardAdapter useAdapter(@NonNull List<NativePromoCard> list) {
        if (this.cardAdapter == null) {
            this.cardAdapter = new PromoCardAdapter() {
                @NonNull
                public PromoCardView getPromoCardView() {
                    return NativeViewsFactory.getContentStreamCardView(ContentStreamAdView.this.getContext());
                }
            };
        }
        this.cardAdapter.setCards(list);
        return this.cardAdapter;
    }

    private void updateDefaultParams() {
        this.ageRestrictionLabel.setTextColor(-6710887);
        this.ageRestrictionLabel.f(1, -6710887);
        this.ageRestrictionLabel.setBackgroundColor(0);
        this.advertisingLabel.setTextSize(2, 14.0f);
        this.advertisingLabel.setTextColor(-6710887);
        this.titleLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.titleLabel.setTextSize(2, 16.0f);
        this.titleLabel.setTypeface(null, 1);
        this.urlLabel.setTextColor(-6710887);
        this.urlLabel.setTextSize(2, 14.0f);
        this.descriptionLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.descriptionLabel.setTextSize(2, 14.0f);
        this.title2Label.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.title2Label.setTextSize(2, 16.0f);
        this.title2Label.setTypeface(null, 1);
        this.url2Label.setTextColor(-6710887);
        this.url2Label.setTextSize(2, 14.0f);
        this.votesLabel.setTextColor(-6710887);
        this.votesLabel.setTextSize(2, 14.0f);
        this.disclaimerLabel.setTextColor(-6710887);
        this.disclaimerLabel.setTextSize(2, 12.0f);
        this.ctaButton.setTextColor(STANDARD_BLUE);
    }
}
