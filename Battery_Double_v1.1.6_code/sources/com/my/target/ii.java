package com.my.target;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.my.target.common.models.VideoData;

/* compiled from: ExoVideoPlayer */
public class ii implements EventListener, ig {
    @NonNull
    private final ia A;
    @Nullable
    private fs fx;
    @Nullable
    private VideoData kP;
    @Nullable
    private com.my.target.ig.a oh;
    @NonNull
    private final SimpleExoPlayer on;
    @NonNull
    private final a oo;
    private boolean op;
    @Nullable
    private MediaSource source;
    private boolean started;

    /* compiled from: ExoVideoPlayer */
    public static class a implements Runnable {
        private int F;
        @Nullable
        private com.my.target.ig.a oh;
        private final int ol;
        @Nullable
        private SimpleExoPlayer oq;
        private float s;

        a(int i) {
            this.ol = i;
        }

        public void run() {
            if (this.oh != null && this.oq != null) {
                float currentPosition = ((float) this.oq.getCurrentPosition()) / 1000.0f;
                float duration = ((float) this.oq.getDuration()) / 1000.0f;
                if (this.s == currentPosition) {
                    this.F++;
                } else {
                    this.oh.a(currentPosition, duration);
                    this.s = currentPosition;
                    if (this.F > 0) {
                        this.F = 0;
                    }
                }
                if (this.F > this.ol) {
                    this.oh.d("timeout");
                    this.F = 0;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable com.my.target.ig.a aVar) {
            this.oh = aVar;
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable SimpleExoPlayer simpleExoPlayer) {
            this.oq = simpleExoPlayer;
        }
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public void onPositionDiscontinuity(int i) {
    }

    public void onRepeatModeChanged(int i) {
    }

    public void onSeekProcessed() {
    }

    public void onShuffleModeEnabledChanged(boolean z) {
    }

    public void onTimelineChanged(Timeline timeline, Object obj, int i) {
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    public static ii R(@NonNull Context context) {
        return new ii(context);
    }

    @VisibleForTesting
    ii(@NonNull SimpleExoPlayer simpleExoPlayer, @NonNull a aVar) {
        this.A = ia.K(200);
        this.on = simpleExoPlayer;
        this.oo = aVar;
        this.on.addListener(this);
        aVar.a(this.on);
    }

    private ii(@NonNull Context context) {
        this(ExoPlayerFactory.newSimpleInstance(context.getApplicationContext(), (TrackSelector) new DefaultTrackSelector()), new a(50));
    }

    @Nullable
    public VideoData eH() {
        return this.kP;
    }

    public boolean isMuted() {
        return this.on.getVolume() == 0.0f;
    }

    public long getPosition() {
        return this.on.getCurrentPosition();
    }

    public void seekTo(long j) {
        this.on.seekTo(j);
    }

    public boolean isStarted() {
        return this.started;
    }

    public void a(@Nullable com.my.target.ig.a aVar) {
        this.oh = aVar;
        this.oo.a(aVar);
    }

    public void destroy() {
        this.kP = null;
        this.started = false;
        this.op = false;
        this.on.setVideoTextureView(null);
        this.on.stop();
        this.on.release();
        this.on.removeListener(this);
        this.A.e(this.oo);
        this.fx = null;
    }

    public boolean isPaused() {
        return this.started && this.op;
    }

    public void a(@Nullable fs fsVar) {
        this.fx = fsVar;
        if (fsVar != null) {
            if (this.kP != null) {
                fsVar.e(this.kP.getWidth(), this.kP.getHeight());
            }
            this.on.setVideoTextureView(fsVar.getTextureView());
            return;
        }
        this.on.setVideoTextureView(null);
    }

    public void a(@NonNull VideoData videoData, @NonNull Context context) {
        ah.a("Play video in ExoPlayer");
        this.kP = videoData;
        this.op = false;
        if (this.fx != null) {
            this.fx.e(this.kP.getWidth(), this.kP.getHeight());
        }
        if (this.oh != null) {
            this.oh.B();
        }
        if (this.kP != videoData || !this.started) {
            this.source = ij.b(videoData, context);
            this.on.prepare(this.source);
        }
        this.on.setPlayWhenReady(true);
    }

    public void a(@NonNull Uri uri, @NonNull fs fsVar) {
        a(fsVar);
        a(uri, fsVar.getContext());
    }

    private void a(@NonNull Uri uri, @NonNull Context context) {
        ah.a("Play video in ExoPlayer");
        this.op = false;
        if (this.oh != null) {
            this.oh.B();
        }
        if (!this.started) {
            this.source = ij.b(uri, context);
            this.on.prepare(this.source);
        }
        this.on.setPlayWhenReady(true);
    }

    public void stop() {
        this.on.stop(true);
    }

    public void L() {
        this.on.setVolume(0.2f);
    }

    public void M() {
        this.on.setVolume(0.0f);
        if (this.oh != null) {
            this.oh.e(0.0f);
        }
    }

    public void setVolume(float f) {
        this.on.setVolume(f);
        if (this.oh != null) {
            this.oh.e(f);
        }
    }

    public void cU() {
        this.on.setVolume(1.0f);
        if (this.oh != null) {
            this.oh.e(1.0f);
        }
    }

    public void resume() {
        if (this.started) {
            this.on.setPlayWhenReady(true);
        } else if (this.source != null) {
            this.on.prepare(this.source, true, true);
        }
    }

    public void pause() {
        if (this.started && !this.op) {
            this.on.setPlayWhenReady(false);
        }
    }

    public float getDuration() {
        return ((float) this.on.getDuration()) / 1000.0f;
    }

    public boolean isPlaying() {
        return this.started && !this.op;
    }

    public void de() {
        if (this.on.getVolume() == 1.0f) {
            setVolume(0.0f);
        } else {
            setVolume(1.0f);
        }
    }

    public void onPlayerStateChanged(boolean z, int i) {
        switch (i) {
            case 1:
                if (this.started) {
                    this.started = false;
                    if (this.oh != null) {
                        this.oh.x();
                    }
                }
                this.A.e(this.oo);
                return;
            case 2:
                if (z && !this.started) {
                    this.A.d(this.oo);
                    return;
                }
                return;
            case 3:
                if (z) {
                    if (this.oh != null) {
                        this.oh.y();
                    }
                    if (!this.started) {
                        this.started = true;
                    } else if (this.op) {
                        this.op = false;
                        if (this.oh != null) {
                            this.oh.A();
                        }
                    }
                    this.A.d(this.oo);
                    return;
                }
                if (!this.op && this.oh != null) {
                    this.op = true;
                    this.oh.z();
                }
                this.A.e(this.oo);
                return;
            case 4:
                this.op = false;
                this.started = false;
                float duration = ((float) this.on.getDuration()) / 1000.0f;
                if (this.oh != null) {
                    this.oh.a(duration, duration);
                    this.oh.C();
                }
                this.A.e(this.oo);
                return;
            default:
                return;
        }
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        this.op = false;
        this.started = false;
        if (this.oh != null) {
            String message = exoPlaybackException.getMessage();
            if (message == null) {
                message = "Unknown video error";
            }
            this.oh.d(message);
        }
        this.on.release();
    }
}
