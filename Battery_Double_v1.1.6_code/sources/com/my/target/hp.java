package com.my.target;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.my.target.common.models.ImageData;
import java.lang.ref.WeakReference;

/* compiled from: AdChoicesHelper */
public class hp {
    @Nullable
    private final by adChoices;
    @Nullable
    private b nj;
    @Nullable
    private a nk;
    /* access modifiers changed from: private */
    @Nullable
    public WeakReference<fq> nl;
    /* access modifiers changed from: private */
    public int nm;

    /* compiled from: AdChoicesHelper */
    static class a implements OnClickListener {
        @NonNull
        private final String cH;

        a(@NonNull String str) {
            this.cH = str;
        }

        public void onClick(View view) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.cH));
            Context context = view.getContext();
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to open AdChoices link: ");
                sb.append(e.getMessage());
                ah.a(sb.toString());
            }
        }
    }

    /* compiled from: AdChoicesHelper */
    class b implements OnLayoutChangeListener {
        private b() {
        }

        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            int i9;
            int i10;
            int i11;
            int i12;
            fq fqVar = hp.this.nl != null ? (fq) hp.this.nl.get() : null;
            if (fqVar != null) {
                int measuredWidth = view.getMeasuredWidth();
                int measuredHeight = view.getMeasuredHeight();
                int measuredWidth2 = fqVar.getMeasuredWidth();
                int measuredHeight2 = fqVar.getMeasuredHeight();
                switch (hp.this.nm) {
                    case 1:
                        i11 = view.getPaddingLeft();
                        i9 = view.getPaddingTop();
                        int paddingLeft = view.getPaddingLeft() + measuredWidth2;
                        i12 = view.getPaddingTop() + measuredHeight2;
                        i10 = paddingLeft;
                        break;
                    case 2:
                        int paddingLeft2 = (measuredWidth - measuredWidth2) - view.getPaddingLeft();
                        i9 = (measuredHeight - view.getPaddingBottom()) - measuredHeight2;
                        i10 = measuredWidth - view.getPaddingRight();
                        i12 = measuredHeight - view.getPaddingBottom();
                        i11 = paddingLeft2;
                        break;
                    case 3:
                        i11 = view.getPaddingLeft();
                        i9 = (measuredHeight - view.getPaddingBottom()) - measuredHeight2;
                        i10 = view.getPaddingLeft() + measuredWidth2;
                        i12 = measuredHeight - view.getPaddingBottom();
                        break;
                    default:
                        int paddingLeft3 = (measuredWidth - measuredWidth2) - view.getPaddingLeft();
                        i9 = view.getPaddingTop();
                        int paddingRight = measuredWidth - view.getPaddingRight();
                        i12 = view.getPaddingTop() + measuredHeight2;
                        i10 = paddingRight;
                        i11 = paddingLeft3;
                        break;
                }
                fqVar.layout(i11, i9, i10, i12);
            }
        }
    }

    public static hp a(@Nullable by byVar) {
        return new hp(byVar);
    }

    private hp(@Nullable by byVar) {
        this.adChoices = byVar;
        if (byVar != null) {
            this.nk = new a(byVar.aU());
            this.nj = new b();
        }
    }

    public void a(@NonNull ViewGroup viewGroup, @Nullable fq fqVar, int i) {
        this.nm = i;
        if (this.adChoices != null) {
            if (fqVar == null) {
                Context context = viewGroup.getContext();
                fq fqVar2 = new fq(context);
                fqVar2.setId(ic.eG());
                ic.a((View) fqVar2, "ad_choices");
                fqVar2.setFixedHeight(ic.a(20, context));
                int a2 = ic.a(2, context);
                fqVar2.setPadding(a2, a2, a2, a2);
                fqVar = fqVar2;
            }
            this.nl = new WeakReference<>(fqVar);
            a(viewGroup, fqVar, this.adChoices);
        } else if (fqVar != null) {
            fqVar.setImageBitmap(null);
            fqVar.setVisibility(8);
        }
    }

    public void i(@NonNull View view) {
        if (this.nj != null) {
            view.removeOnLayoutChangeListener(this.nj);
        }
        fq fqVar = this.nl != null ? (fq) this.nl.get() : null;
        if (fqVar != null) {
            if (this.adChoices != null) {
                hu.b(this.adChoices.getIcon(), (ImageView) fqVar);
            }
            fqVar.setOnClickListener(null);
            fqVar.setImageBitmap(null);
            fqVar.setVisibility(8);
            this.nl.clear();
        }
        this.nl = null;
    }

    private void a(@NonNull ViewGroup viewGroup, @NonNull fq fqVar, @NonNull by byVar) {
        fqVar.setVisibility(0);
        fqVar.setOnClickListener(this.nk);
        viewGroup.addOnLayoutChangeListener(this.nj);
        if (fqVar.getParent() == null) {
            try {
                viewGroup.addView(fqVar);
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to add AdChoices View: ");
                sb.append(e.getMessage());
                ah.a(sb.toString());
            }
        }
        ImageData icon = byVar.getIcon();
        Bitmap bitmap = icon.getBitmap();
        if (icon.getBitmap() != null) {
            fqVar.setImageBitmap(bitmap);
        } else {
            hu.a(icon, (ImageView) fqVar);
        }
    }
}
