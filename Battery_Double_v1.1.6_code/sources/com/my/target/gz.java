package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils.TruncateAt;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import java.util.HashMap;

/* compiled from: CardItemView */
public class gz extends ViewGroup implements OnTouchListener {
    private static final int CTA_ID = ic.eG();
    private static final int MEDIA_ID = ic.eG();
    private static final int RATING_ID = ic.eG();
    private static final int iQ = ic.eG();
    private static final int jc = ic.eG();
    private static final int kq = ic.eG();
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final TextView descriptionView;
    @NonNull
    private final ge jZ;
    @NonNull
    private final TextView jk;
    private final boolean ki;
    @NonNull
    private final HashMap<View, Boolean> kj = new HashMap<>();
    @Nullable
    private OnClickListener kk;
    private int lp;
    private int lq;
    private int padding;
    @NonNull
    private final gf starsView;
    @NonNull
    private final TextView titleView;
    @NonNull
    private final ic uiUtils;

    public gz(boolean z, @NonNull Context context) {
        super(context);
        this.ki = z;
        this.uiUtils = ic.P(context);
        this.jZ = new ge(context);
        this.titleView = new TextView(context);
        this.descriptionView = new TextView(context);
        this.ctaButton = new Button(context);
        this.starsView = new gf(context);
        this.jk = new TextView(context);
        initView();
    }

    @NonNull
    public Button getCtaButtonView() {
        return this.ctaButton;
    }

    @NonNull
    public TextView getDescriptionTextView() {
        return this.descriptionView;
    }

    @NonNull
    public TextView getDomainTextView() {
        return this.jk;
    }

    @NonNull
    public ge getSmartImageView() {
        return this.jZ;
    }

    @NonNull
    public gf getRatingView() {
        return this.starsView;
    }

    @NonNull
    public TextView getTitleTextView() {
        return this.titleView;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void a(@Nullable OnClickListener onClickListener, @Nullable ca caVar) {
        this.kk = onClickListener;
        if (onClickListener == null || caVar == null) {
            super.setOnClickListener(null);
            this.ctaButton.setOnClickListener(null);
            return;
        }
        setOnTouchListener(this);
        this.jZ.setOnTouchListener(this);
        this.titleView.setOnTouchListener(this);
        this.descriptionView.setOnTouchListener(this);
        this.starsView.setOnTouchListener(this);
        this.jk.setOnTouchListener(this);
        this.ctaButton.setOnTouchListener(this);
        boolean z = false;
        this.kj.put(this.jZ, Boolean.valueOf(caVar.df || caVar.f456do));
        this.kj.put(this, Boolean.valueOf(caVar.dn || caVar.f456do));
        this.kj.put(this.titleView, Boolean.valueOf(caVar.dc || caVar.f456do));
        this.kj.put(this.descriptionView, Boolean.valueOf(caVar.dd || caVar.f456do));
        this.kj.put(this.starsView, Boolean.valueOf(caVar.dg || caVar.f456do));
        this.kj.put(this.jk, Boolean.valueOf(caVar.dl || caVar.f456do));
        HashMap<View, Boolean> hashMap = this.kj;
        Button button = this.ctaButton;
        if (caVar.di || caVar.f456do) {
            z = true;
        }
        hashMap.put(button, Boolean.valueOf(z));
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.kj.containsKey(view)) {
            return false;
        }
        boolean booleanValue = ((Boolean) this.kj.get(view)).booleanValue();
        view.setClickable(booleanValue);
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    if (booleanValue) {
                        if (view != this.ctaButton) {
                            setBackgroundColor(-3806472);
                            break;
                        } else {
                            this.ctaButton.setPressed(true);
                            break;
                        }
                    }
                    break;
                case 1:
                    if (this.kk != null) {
                        this.kk.onClick(view);
                    }
                    if (booleanValue) {
                        if (view != this.ctaButton) {
                            ic.a(this, 0, 0, -3355444, this.uiUtils.M(1), 0);
                            break;
                        } else {
                            this.ctaButton.setPressed(false);
                            break;
                        }
                    }
                    break;
            }
        } else if (booleanValue) {
            if (view == this.ctaButton) {
                this.ctaButton.setPressed(false);
            } else {
                ic.a(this, 0, 0, -3355444, this.uiUtils.M(1), 0);
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        int i4 = 0;
        boolean z = !this.ki && getResources().getConfiguration().orientation == 2;
        if (size != 0) {
            i4 = Integer.MIN_VALUE;
        }
        a(size, size2, z, i4);
        if (z) {
            i3 = (size2 - this.titleView.getMeasuredHeight()) - this.padding;
        } else {
            i3 = ((((size2 - this.ctaButton.getMeasuredHeight()) - (this.lp * 2)) - Math.max(this.starsView.getMeasuredHeight(), this.jk.getMeasuredHeight())) - this.descriptionView.getMeasuredHeight()) - this.titleView.getMeasuredHeight();
        }
        if (i3 <= size) {
            size = i3;
        }
        this.jZ.measure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size, 1073741824));
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = (i3 - i) - (this.padding * 2);
        boolean z2 = !this.ki && getResources().getConfiguration().orientation == 2;
        this.jZ.layout(0, 0, this.jZ.getMeasuredWidth(), this.jZ.getMeasuredHeight());
        if (z2) {
            this.titleView.setTypeface(null, 1);
            this.titleView.layout(0, this.jZ.getBottom(), i5, this.jZ.getBottom() + this.titleView.getMeasuredHeight());
            ic.a(this, 0, 0);
            this.descriptionView.layout(0, 0, 0, 0);
            this.ctaButton.layout(0, 0, 0, 0);
            this.starsView.layout(0, 0, 0, 0);
            this.jk.layout(0, 0, 0, 0);
            return;
        }
        this.titleView.setTypeface(null, 0);
        ic.a(this, 0, 0, -3355444, this.uiUtils.M(1), 0);
        this.titleView.layout(this.padding + this.lq, this.jZ.getBottom(), this.titleView.getMeasuredWidth() + this.padding + this.lq, this.jZ.getBottom() + this.titleView.getMeasuredHeight());
        this.descriptionView.layout(this.padding + this.lq, this.titleView.getBottom(), this.descriptionView.getMeasuredWidth() + this.padding + this.lq, this.titleView.getBottom() + this.descriptionView.getMeasuredHeight());
        int measuredWidth = (i5 - this.ctaButton.getMeasuredWidth()) / 2;
        this.ctaButton.layout(measuredWidth, (i4 - this.ctaButton.getMeasuredHeight()) - this.lq, this.ctaButton.getMeasuredWidth() + measuredWidth, i4 - this.lq);
        int measuredWidth2 = (i5 - this.starsView.getMeasuredWidth()) / 2;
        this.starsView.layout(measuredWidth2, (this.ctaButton.getTop() - this.lq) - this.starsView.getMeasuredHeight(), this.starsView.getMeasuredWidth() + measuredWidth2, this.ctaButton.getTop() - this.lq);
        int measuredWidth3 = (i5 - this.jk.getMeasuredWidth()) / 2;
        this.jk.layout(measuredWidth3, (this.ctaButton.getTop() - this.jk.getMeasuredHeight()) - this.lq, this.jk.getMeasuredWidth() + measuredWidth3, this.ctaButton.getTop() - this.lq);
    }

    private void initView() {
        ic.a(this, 0, 0, -3355444, this.uiUtils.M(1), 0);
        this.padding = this.uiUtils.M(2);
        this.lq = this.uiUtils.M(12);
        this.jZ.setId(MEDIA_ID);
        this.ctaButton.setId(CTA_ID);
        this.ctaButton.setPadding(this.uiUtils.M(15), this.uiUtils.M(10), this.uiUtils.M(15), this.uiUtils.M(10));
        this.ctaButton.setMinimumWidth(this.uiUtils.M(100));
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setSingleLine();
        if (this.ki) {
            this.ctaButton.setTextSize(20.0f);
        } else {
            this.ctaButton.setTextSize(18.0f);
        }
        this.ctaButton.setEllipsize(TruncateAt.END);
        if (VERSION.SDK_INT >= 21) {
            this.ctaButton.setElevation((float) this.uiUtils.M(2));
        }
        this.lp = this.uiUtils.M(12);
        ic.a(this.ctaButton, -16733198, -16746839, this.uiUtils.M(2));
        this.ctaButton.setTextColor(-1);
        this.titleView.setId(iQ);
        if (this.ki) {
            this.titleView.setTextSize(20.0f);
        } else {
            this.titleView.setTextSize(18.0f);
        }
        this.titleView.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.titleView.setTypeface(null, 1);
        this.titleView.setLines(1);
        this.titleView.setEllipsize(TruncateAt.END);
        this.descriptionView.setId(kq);
        this.descriptionView.setTextColor(-7829368);
        this.descriptionView.setLines(2);
        if (this.ki) {
            this.descriptionView.setTextSize(20.0f);
        } else {
            this.descriptionView.setTextSize(18.0f);
        }
        this.descriptionView.setEllipsize(TruncateAt.END);
        this.starsView.setId(RATING_ID);
        if (this.ki) {
            this.starsView.setStarSize(this.uiUtils.M(24));
        } else {
            this.starsView.setStarSize(this.uiUtils.M(18));
        }
        this.starsView.setStarsPadding((float) this.uiUtils.M(4));
        this.jk.setId(jc);
        ic.a((View) this, "card_view");
        ic.a((View) this.titleView, "card_title_text");
        ic.a((View) this.descriptionView, "card_description_text");
        ic.a((View) this.jk, "card_domain_text");
        ic.a((View) this.ctaButton, "card_cta_button");
        ic.a((View) this.starsView, "card_stars_view");
        ic.a((View) this.jZ, "card_image");
        addView(this.jZ);
        addView(this.descriptionView);
        addView(this.titleView);
        addView(this.ctaButton);
        addView(this.starsView);
        addView(this.jk);
    }

    private void a(int i, int i2, boolean z, int i3) {
        int i4 = i2 - (this.padding * 2);
        int i5 = i - (this.padding * 2);
        if (z) {
            this.titleView.measure(MeasureSpec.makeMeasureSpec(i, i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
            this.descriptionView.measure(0, 0);
            this.starsView.measure(0, 0);
            this.jk.measure(0, 0);
            this.ctaButton.measure(0, 0);
            return;
        }
        this.titleView.measure(MeasureSpec.makeMeasureSpec(i5 - (this.lq * 2), i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.descriptionView.measure(MeasureSpec.makeMeasureSpec(i5 - (this.lq * 2), i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.starsView.measure(MeasureSpec.makeMeasureSpec(i5, i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jk.measure(MeasureSpec.makeMeasureSpec(i5, i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i5 - (this.lq * 2), i3), MeasureSpec.makeMeasureSpec(i4 - (this.lq * 2), Integer.MIN_VALUE));
    }
}
