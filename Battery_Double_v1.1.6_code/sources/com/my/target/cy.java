package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: InterstitialAdSection */
public class cy extends cv {
    @Nullable
    private cj dJ;

    @NonNull
    public static cy bO() {
        return new cy();
    }

    private cy() {
    }

    @Nullable
    public cj bP() {
        return this.dJ;
    }

    public void a(@Nullable cj cjVar) {
        this.dJ = cjVar;
    }

    public int getBannersCount() {
        return this.dJ == null ? 0 : 1;
    }
}
