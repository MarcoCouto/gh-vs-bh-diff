package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ef.a;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StandardAdResponseParser */
public class af extends c<dd> implements a {
    @Nullable
    private String mraidJs;
    @Nullable
    private String v;

    @NonNull
    public static c<dd> f() {
        return new af();
    }

    private af() {
    }

    @Nullable
    public cv b(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        try {
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(jSONObject);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("banners", jSONArray);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(aVar.getFormat(), jSONObject2);
            dd ck = dd.ck();
            cs a = a(jSONObject, ck, jSONObject3, bzVar, aVar, context);
            if (a == null) {
                return null;
            }
            ck.a(a);
            return ck;
        } catch (JSONException unused) {
            return null;
        }
    }

    @Nullable
    public dd a(@NonNull String str, @NonNull bz bzVar, @Nullable dd ddVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a = a(str, context);
        if (a == null) {
            return null;
        }
        if (ddVar == null) {
            ddVar = dd.ck();
        }
        if (a.has("html_wrapper")) {
            this.v = a.optString("html_wrapper");
            a.remove("html_wrapper");
        }
        this.mraidJs = a.optString("mraid.js");
        JSONObject optJSONObject = a.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            if (aVar.isMediationEnabled()) {
                JSONObject optJSONObject2 = a.optJSONObject("mediation");
                if (optJSONObject2 != null) {
                    ct g = ef.a(this, bzVar, aVar, context).g(optJSONObject2);
                    if (g != null) {
                        ddVar.a(g);
                        return ddVar;
                    }
                }
            }
            return null;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        el.cN().a(optJSONObject, ddVar);
        if (optJSONArray.length() > 0) {
            JSONObject optJSONObject3 = optJSONArray.optJSONObject(0);
            if (optJSONObject3 != null) {
                cs a2 = a(optJSONObject3, ddVar, a, bzVar, aVar, context);
                if (a2 != null) {
                    ddVar.a(a2);
                    return ddVar;
                }
            }
        }
        return null;
    }

    private cs a(@NonNull JSONObject jSONObject, @NonNull dd ddVar, @NonNull JSONObject jSONObject2, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        ek j = ek.j(bzVar, aVar, context);
        cs newBanner = cs.newBanner();
        if (!j.a(jSONObject, newBanner, this.mraidJs)) {
            return null;
        }
        if (this.v != null) {
            ddVar.J(this.v);
            ddVar.d(jSONObject2);
            return newBanner;
        }
        dq.P("Required field").Q("Section has no HTML_WRAPPER field, required for viewType = html").x(aVar.getSlotId()).R(bzVar.getUrl()).q(context);
        return null;
    }
}
