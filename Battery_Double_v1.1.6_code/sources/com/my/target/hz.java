package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: PrefsCache */
public class hz {
    @Nullable
    private static volatile hz nL;
    @NonNull
    private final SharedPreferences nM;

    @NonNull
    public static hz O(@NonNull Context context) {
        hz hzVar = nL;
        if (hzVar == null) {
            synchronized (hz.class) {
                hzVar = nL;
                if (hzVar == null) {
                    hzVar = new hz(context.getSharedPreferences("mytarget_prefs", 0));
                    nL = hzVar;
                }
            }
        }
        return hzVar;
    }

    private hz(@NonNull SharedPreferences sharedPreferences) {
        this.nM = sharedPreferences;
    }

    @Nullable
    public String eC() {
        return getString("hoaid");
    }

    public void aj(@Nullable String str) {
        putString("hoaid", str);
    }

    @Nullable
    public String eD() {
        return getString("hlimit");
    }

    public void ak(@Nullable String str) {
        putString("hlimit", str);
    }

    @NonNull
    private String getString(@NonNull String str) {
        String str2 = "";
        try {
            String string = this.nM.getString(str, null);
            if (string == null) {
                string = "";
            }
            return string;
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("PrefsCache exception: ");
            sb.append(th);
            ah.c(sb.toString());
            return str2;
        }
    }

    @SuppressLint({"ApplySharedPref"})
    private void putString(@NonNull String str, @Nullable String str2) {
        try {
            Editor edit = this.nM.edit();
            edit.putString(str, str2);
            edit.commit();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("PrefsCache exception: ");
            sb.append(th);
            ah.c(sb.toString());
        }
    }
}
