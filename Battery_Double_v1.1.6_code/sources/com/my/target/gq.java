package com.my.target;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

@SuppressLint({"ViewConstructor"})
/* compiled from: FooterView */
public class gq extends RelativeLayout {
    private final boolean ki;
    @NonNull
    private final RelativeLayout kl;
    @NonNull
    private final ImageView km;
    @NonNull
    private final ImageView kn;
    @NonNull
    private final OnClickListener ko;
    @NonNull
    private final ic uiUtils;

    /* compiled from: FooterView */
    static class a implements OnClickListener {
        @NonNull
        private final Context context;

        private a(@NonNull Context context2) {
            this.context = context2;
        }

        public void onClick(View view) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://target.my.com/"));
                if (!(this.context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                this.context.startActivity(intent);
            } catch (Throwable th) {
                ah.a(th.getMessage());
            }
        }
    }

    public gq(@NonNull Context context, @NonNull ic icVar, boolean z) {
        super(context);
        this.kl = new RelativeLayout(context);
        this.km = new ImageView(context);
        ic.a((View) this.km, "logo_image");
        this.kn = new ImageView(context);
        ic.a((View) this.kn, "store_image");
        this.uiUtils = icVar;
        this.ki = z;
        this.ko = new a(context);
    }

    /* access modifiers changed from: 0000 */
    public void initView() {
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(12, -1);
        this.kl.setLayoutParams(layoutParams);
        this.km.setImageBitmap(fm.E(getContext()));
        this.kl.addView(this.km);
        this.kl.addView(this.kn);
        addView(this.kl);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i, boolean z) {
        int i2 = i / 3;
        if (this.ki) {
            i2 = i / 5;
        }
        LayoutParams layoutParams = new LayoutParams(-2, i2);
        if (z) {
            layoutParams.setMargins(this.uiUtils.M(24), this.uiUtils.M(4), this.uiUtils.M(24), this.uiUtils.M(8));
        } else {
            layoutParams.setMargins(this.uiUtils.M(24), this.uiUtils.M(16), this.uiUtils.M(24), this.uiUtils.M(16));
        }
        layoutParams.addRule(15, -1);
        if (VERSION.SDK_INT >= 17) {
            layoutParams.addRule(20);
        } else {
            layoutParams.addRule(9);
        }
        this.kn.setScaleType(ScaleType.FIT_START);
        this.kn.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, i2);
        if (z) {
            layoutParams2.setMargins(this.uiUtils.M(8), this.uiUtils.M(4), this.uiUtils.M(8), this.uiUtils.M(8));
        } else {
            layoutParams2.setMargins(this.uiUtils.M(24), this.uiUtils.M(16), this.uiUtils.M(24), this.uiUtils.M(16));
        }
        layoutParams2.addRule(15, -1);
        if (VERSION.SDK_INT >= 17) {
            layoutParams2.addRule(21);
        } else {
            layoutParams2.addRule(11);
        }
        this.km.setScaleType(ScaleType.FIT_CENTER);
        this.km.setLayoutParams(layoutParams2);
        this.km.setOnClickListener(this.ko);
    }
}
