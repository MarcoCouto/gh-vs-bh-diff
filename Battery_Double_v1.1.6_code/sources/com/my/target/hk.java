package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/* compiled from: PageDotsView */
public class hk extends LinearLayout {
    @Nullable
    private Bitmap mH;
    @Nullable
    private Bitmap mI;
    @Nullable
    private ImageView[] mJ;
    private int mK;
    private boolean mL;

    public hk(Context context) {
        super(context);
    }

    public void e(int i, int i2, int i3) {
        ic P = ic.P(getContext());
        this.mH = fm.d(P.M(12), i3);
        this.mI = fm.d(P.M(12), i2);
        this.mJ = new ImageView[i];
        for (int i4 = 0; i4 < i; i4++) {
            this.mJ[i4] = new ImageView(getContext());
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.setMargins(P.M(5), P.M(5), P.M(5), P.M(5));
            this.mJ[i4].setLayoutParams(layoutParams);
            this.mJ[i4].setImageBitmap(this.mI);
            addView(this.mJ[i4]);
        }
        this.mL = true;
    }

    public void J(int i) {
        if (this.mL && this.mJ != null && i >= 0 && i < this.mJ.length && this.mK < this.mJ.length) {
            this.mJ[this.mK].setImageBitmap(this.mI);
            this.mJ[i].setImageBitmap(this.mH);
            this.mK = i;
        }
    }
}
