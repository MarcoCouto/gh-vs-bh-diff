package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.AudioData;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: InstreamAudioAdSection */
public class cx extends cv {
    @NonNull
    private final HashMap<String, da<AudioData>> dI = new HashMap<>();

    @NonNull
    public static cx bM() {
        return new cx();
    }

    private cx() {
        this.dI.put(BreakId.PREROLL, da.A(BreakId.PREROLL));
        this.dI.put(BreakId.PAUSEROLL, da.A(BreakId.PAUSEROLL));
        this.dI.put(BreakId.MIDROLL, da.A(BreakId.MIDROLL));
        this.dI.put(BreakId.POSTROLL, da.A(BreakId.POSTROLL));
    }

    @Nullable
    public da<AudioData> y(@NonNull String str) {
        return (da) this.dI.get(str);
    }

    @NonNull
    public ArrayList<da<AudioData>> bN() {
        return new ArrayList<>(this.dI.values());
    }

    public int getBannersCount() {
        int i = 0;
        for (da bannersCount : this.dI.values()) {
            i += bannersCount.getBannersCount();
        }
        return i;
    }

    public boolean bL() {
        for (da daVar : this.dI.values()) {
            if (daVar.getBannersCount() <= 0) {
                if (daVar.bZ()) {
                }
            }
            return true;
        }
        return false;
    }
}
