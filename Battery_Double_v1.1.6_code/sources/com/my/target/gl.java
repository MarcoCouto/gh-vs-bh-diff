package com.my.target;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.net.URI;
import java.net.URISyntaxException;

/* compiled from: WebViewBrowser */
public class gl extends LinearLayout {
    public static final int jL = ic.eG();
    public static final int jM = ic.eG();
    /* access modifiers changed from: private */
    @NonNull
    public final ImageButton jN;
    @NonNull
    private final LinearLayout jO;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView jP;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView jQ;
    @NonNull
    private final FrameLayout jR;
    /* access modifiers changed from: private */
    @NonNull
    public final View jS;
    @NonNull
    private final FrameLayout jT;
    /* access modifiers changed from: private */
    @NonNull
    public final ImageButton jU;
    @NonNull
    private final RelativeLayout jV;
    @NonNull
    private final WebView jW;
    /* access modifiers changed from: private */
    @Nullable
    public b jX;
    /* access modifiers changed from: private */
    @NonNull
    public final ProgressBar progressBar;
    @NonNull
    private final ic uiUtils;

    /* compiled from: WebViewBrowser */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (view == gl.this.jN) {
                if (gl.this.jX != null) {
                    gl.this.jX.aj();
                }
            } else if (view == gl.this.jU) {
                gl.this.eh();
            }
        }
    }

    /* compiled from: WebViewBrowser */
    public interface b {
        void aj();
    }

    public void setListener(@Nullable b bVar) {
        this.jX = bVar;
    }

    public gl(@NonNull Context context) {
        super(context);
        this.jV = new RelativeLayout(context);
        this.jW = new WebView(context);
        this.jN = new ImageButton(context);
        this.jO = new LinearLayout(context);
        this.jP = new TextView(context);
        this.jQ = new TextView(context);
        this.jR = new FrameLayout(context);
        this.jT = new FrameLayout(context);
        this.jU = new ImageButton(context);
        this.progressBar = new ProgressBar(context, null, 16842872);
        this.jS = new View(context);
        this.uiUtils = ic.P(context);
    }

    public boolean canGoBack() {
        return this.jW.canGoBack();
    }

    public void goBack() {
        this.jW.goBack();
    }

    public void destroy() {
        this.jW.setWebChromeClient(null);
        this.jW.setWebViewClient(null);
        this.jW.destroy();
    }

    public void setUrl(@NonNull String str) {
        this.jW.loadUrl(str);
        this.jP.setText(ad(str));
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public void dc() {
        WebSettings settings = this.jW.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        if (VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        settings.setDomStorageEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setAppCachePath(getContext().getCacheDir().getAbsolutePath());
        this.jW.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                gl.this.jP.setText(gl.this.ad(str));
                return true;
            }
        });
        this.jW.setWebChromeClient(new WebChromeClient() {
            public void onReceivedTitle(WebView webView, String str) {
                super.onReceivedTitle(webView, str);
                gl.this.jQ.setText(webView.getTitle());
                gl.this.jQ.setVisibility(0);
            }

            public void onProgressChanged(WebView webView, int i) {
                if (i < 100 && gl.this.progressBar.getVisibility() == 8) {
                    gl.this.progressBar.setVisibility(0);
                    gl.this.jS.setVisibility(8);
                }
                gl.this.progressBar.setProgress(i);
                if (i >= 100) {
                    gl.this.progressBar.setVisibility(8);
                    gl.this.jS.setVisibility(0);
                }
            }
        });
        eg();
    }

    private void eg() {
        setOrientation(1);
        setGravity(16);
        a aVar = new a();
        this.jW.setLayoutParams(new LayoutParams(-1, -1));
        TypedValue typedValue = new TypedValue();
        int M = this.uiUtils.M(50);
        if (getContext().getTheme().resolveAttribute(16843499, typedValue, true)) {
            M = TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
        }
        this.jV.setLayoutParams(new LayoutParams(-1, M));
        this.jR.setLayoutParams(new LayoutParams(M, M));
        this.jR.setId(jL);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        this.jN.setLayoutParams(layoutParams);
        this.jN.setImageBitmap(fl.c(M / 4, this.uiUtils.M(2)));
        this.jN.setContentDescription("Close");
        this.jN.setOnClickListener(aVar);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(M, M);
        if (VERSION.SDK_INT >= 18) {
            layoutParams2.addRule(21);
        } else {
            layoutParams2.addRule(11);
        }
        this.jT.setLayoutParams(layoutParams2);
        this.jT.setId(jM);
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -1);
        layoutParams3.gravity = 17;
        this.jU.setLayoutParams(layoutParams3);
        this.jU.setImageBitmap(fl.D(getContext()));
        this.jU.setScaleType(ScaleType.CENTER_INSIDE);
        this.jU.setContentDescription("Open outside");
        this.jU.setOnClickListener(aVar);
        ic.a(this.jN, 0, -3355444);
        ic.a(this.jU, 0, -3355444);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(15, -1);
        layoutParams4.addRule(1, jL);
        layoutParams4.addRule(0, jM);
        this.jO.setLayoutParams(layoutParams4);
        this.jO.setOrientation(1);
        this.jO.setPadding(this.uiUtils.M(4), this.uiUtils.M(4), this.uiUtils.M(4), this.uiUtils.M(4));
        LayoutParams layoutParams5 = new LayoutParams(-2, -2);
        this.jQ.setVisibility(8);
        this.jQ.setLayoutParams(layoutParams5);
        this.jQ.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.jQ.setTextSize(2, 18.0f);
        this.jQ.setSingleLine();
        this.jQ.setEllipsize(TruncateAt.MIDDLE);
        this.jP.setLayoutParams(new LayoutParams(-2, -2));
        this.jP.setSingleLine();
        this.jP.setTextSize(2, 12.0f);
        this.jP.setEllipsize(TruncateAt.MIDDLE);
        ClipDrawable clipDrawable = new ClipDrawable(new ColorDrawable(-16537100), GravityCompat.START, 1);
        LayerDrawable layerDrawable = (LayerDrawable) this.progressBar.getProgressDrawable();
        layerDrawable.setDrawableByLayerId(16908288, new ColorDrawable(-1968642));
        layerDrawable.setDrawableByLayerId(16908301, clipDrawable);
        this.progressBar.setProgressDrawable(layerDrawable);
        this.progressBar.setLayoutParams(new LayoutParams(-1, this.uiUtils.M(2)));
        this.progressBar.setProgress(0);
        this.jO.addView(this.jQ);
        this.jO.addView(this.jP);
        this.jR.addView(this.jN);
        this.jT.addView(this.jU);
        this.jV.addView(this.jR);
        this.jV.addView(this.jO);
        this.jV.addView(this.jT);
        addView(this.jV);
        this.jS.setBackgroundColor(-5592406);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, 1);
        this.jS.setVisibility(8);
        this.jS.setLayoutParams(layoutParams6);
        addView(this.progressBar);
        addView(this.jS);
        addView(this.jW);
    }

    /* access modifiers changed from: private */
    public String ad(String str) {
        try {
            URI uri = new URI(str);
            StringBuilder sb = new StringBuilder();
            sb.append(uri.getScheme());
            sb.append("://");
            sb.append(uri.getHost());
            return sb.toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return str;
        }
    }

    /* access modifiers changed from: private */
    public void eh() {
        String url = this.jW.getUrl();
        if (!TextUtils.isEmpty(url)) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                if (!(getContext() instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                getContext().startActivity(intent);
            } catch (Exception unused) {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to open url ");
                sb.append(url);
                ah.a(sb.toString());
            }
        }
    }
}
