package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import com.my.target.e.a;
import java.util.Map;

/* compiled from: InstreamResearchServiceBuilder */
public class n extends a {
    private final int duration;

    @NonNull
    public static e b(int i) {
        return new n(i);
    }

    private n(int i) {
        this.duration = i;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Map<String, String> b(@NonNull a aVar, @NonNull Context context) {
        Map<String, String> b = super.b(aVar, context);
        b.put("duration", Integer.toString(this.duration));
        return b;
    }
}
