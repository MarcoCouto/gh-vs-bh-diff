package com.my.target;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.view.View;

/* compiled from: SliderLayoutManager */
public class hn extends LinearLayoutManager {
    private final int mZ;
    private int na;
    private int nb;

    hn(Context context) {
        super(context, 0, false);
        this.mZ = ic.P(context).M(16);
    }

    public void measureChildWithMargins(View view, int i, int i2) {
        int i3;
        int i4;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int width = getWidth();
        int height = getHeight();
        if (height != 0 && width != 0 && this.nb != 0 && this.na != 0) {
            float f = width < height ? 0.125f : 0.05f;
            int paddingRight = getPaddingRight();
            int paddingLeft = getPaddingLeft();
            float f2 = (float) width;
            if (f2 / ((float) height) > ((float) (this.na + (this.mZ * 2))) / ((float) this.nb)) {
                i4 = ((int) (((float) (height * this.na)) / ((float) this.nb))) - (this.mZ * 2);
                i3 = ((int) (f2 - (((float) ((width + i4) / 2)) + (f * f2)))) / 2;
            } else {
                i4 = (((int) ((1.0f - (f * 2.0f)) * f2)) - paddingRight) - paddingLeft;
                i3 = 0;
            }
            if (getItemViewType(view) == 1) {
                layoutParams.leftMargin = (int) ((((f2 - ((float) paddingLeft)) - ((float) paddingRight)) - ((float) i4)) / 2.0f);
                layoutParams.rightMargin = i3;
            } else if (getItemViewType(view) == 2) {
                layoutParams.rightMargin = (int) ((((f2 - ((float) paddingLeft)) - ((float) paddingRight)) - ((float) i4)) / 2.0f);
                layoutParams.leftMargin = i3;
            } else {
                layoutParams.leftMargin = i3;
                layoutParams.rightMargin = i3;
            }
            view.measure(getChildMeasureSpec(width, getWidthMode(), paddingLeft + paddingRight + layoutParams.leftMargin + layoutParams.rightMargin + i, i4, canScrollHorizontally()), getChildMeasureSpec(getHeight(), getHeightMode(), getPaddingTop() + getPaddingBottom() + layoutParams.topMargin + layoutParams.bottomMargin + i2, layoutParams.height, canScrollVertically()));
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean g(@Nullable View view) {
        return findViewByPosition(findFirstCompletelyVisibleItemPosition()) == view;
    }

    /* access modifiers changed from: 0000 */
    public int er() {
        return (((getWidth() + getPaddingRight()) + getPaddingLeft()) - findViewByPosition(findFirstVisibleItemPosition()).getWidth()) / 2;
    }

    /* access modifiers changed from: 0000 */
    public void n(int i, int i2) {
        this.na = i;
        this.nb = i2;
    }
}
