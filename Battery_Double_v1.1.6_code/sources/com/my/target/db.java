package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeAdSection */
public class db extends cv {
    @NonNull
    private static final LruCache<String, String> dR = new LruCache<>(10);
    @NonNull
    private final ArrayList<cp> banners = new ArrayList<>();

    @NonNull
    public static LruCache<String, String> ca() {
        return dR;
    }

    @NonNull
    public static db cb() {
        return new db();
    }

    private db() {
    }

    @Nullable
    public cp cc() {
        if (this.banners.size() > 0) {
            return (cp) this.banners.get(0);
        }
        return null;
    }

    public void a(@NonNull cp cpVar) {
        this.banners.add(cpVar);
        dR.put(cpVar.getId(), cpVar.getId());
    }

    @NonNull
    public List<cp> bT() {
        return new ArrayList(this.banners);
    }

    public int getBannersCount() {
        return this.banners.size();
    }
}
