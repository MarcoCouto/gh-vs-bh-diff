package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ClickArea */
public class ca {
    @NonNull
    public static final ca da = new ca(4096);
    @NonNull
    public static final ca db = new ca(64);
    public final boolean dc;
    public final boolean dd;
    public final boolean de;
    public final boolean df;
    public final boolean dg;
    public final boolean dh;
    public final boolean di;
    public final boolean dj;
    public final boolean dk;
    public final boolean dl;
    public final boolean dm;
    public final boolean dn;

    /* renamed from: do reason: not valid java name */
    public final boolean f456do;
    private final int dp;

    @NonNull
    public static ca h(int i) {
        return new ca(i);
    }

    private ca(int i) {
        this.dp = i;
        boolean z = false;
        this.dc = (i & 1) == 1;
        this.dd = (i & 2) == 2;
        this.de = (i & 4) == 4;
        this.df = (i & 8) == 8;
        this.dg = (i & 16) == 16;
        this.dh = (i & 32) == 32;
        this.di = (i & 64) == 64;
        this.dj = (i & 128) == 128;
        this.dk = (i & 256) == 256;
        this.dl = (i & 512) == 512;
        this.dm = (i & 1024) == 1024;
        this.dn = (i & 2048) == 2048;
        if ((i & 4096) == 4096) {
            z = true;
        }
        this.f456do = z;
    }

    public int bm() {
        return this.dp;
    }
}
