package com.my.target.mediation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;

public interface MediationAdConfig {
    int getAge();

    int getGender();

    @Nullable
    String getPayload();

    @NonNull
    String getPlacementId();

    @NonNull
    Map<String, String> getServerParams();

    boolean isTrackingEnvironmentEnabled();

    boolean isTrackingLocationEnabled();

    boolean isUserAgeRestricted();

    boolean isUserConsent();

    boolean isUserConsentSpecified();
}
