package com.my.target.mediation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.ah;
import com.my.target.common.CustomParams;
import com.my.target.cy;
import com.my.target.mediation.MediationInterstitialAdAdapter.MediationInterstitialAdListener;
import java.util.Map.Entry;

public final class MyTargetInterstitialAdAdapter implements MediationInterstitialAdAdapter {
    @Nullable
    private InterstitialAd ad;
    @Nullable
    private cy section;

    class AdListener implements InterstitialAdListener {
        @NonNull
        private final MediationInterstitialAdListener mediationListener;

        AdListener(MediationInterstitialAdListener mediationInterstitialAdListener) {
            this.mediationListener = mediationInterstitialAdListener;
        }

        public void onLoad(@NonNull InterstitialAd interstitialAd) {
            ah.a("MyTargetInterstitialAdAdapter: ad loaded");
            this.mediationListener.onLoad(MyTargetInterstitialAdAdapter.this);
        }

        public void onNoAd(@NonNull String str, @NonNull InterstitialAd interstitialAd) {
            StringBuilder sb = new StringBuilder();
            sb.append("MyTargetInterstitialAdAdapter: no ad (");
            sb.append(str);
            sb.append(")");
            ah.a(sb.toString());
            this.mediationListener.onNoAd(str, MyTargetInterstitialAdAdapter.this);
        }

        public void onClick(@NonNull InterstitialAd interstitialAd) {
            ah.a("MyTargetInterstitialAdAdapter: ad clicked");
            this.mediationListener.onClick(MyTargetInterstitialAdAdapter.this);
        }

        public void onDismiss(@NonNull InterstitialAd interstitialAd) {
            ah.a("MyTargetInterstitialAdAdapter: ad dismissed");
            this.mediationListener.onDismiss(MyTargetInterstitialAdAdapter.this);
        }

        public void onVideoCompleted(@NonNull InterstitialAd interstitialAd) {
            ah.a("MyTargetInterstitialAdAdapter: video completed");
            this.mediationListener.onVideoCompleted(MyTargetInterstitialAdAdapter.this);
        }

        public void onDisplay(@NonNull InterstitialAd interstitialAd) {
            ah.a("MyTargetInterstitialAdAdapter: ad displayed");
            this.mediationListener.onDisplay(MyTargetInterstitialAdAdapter.this);
        }
    }

    public void setSection(@Nullable cy cyVar) {
        this.section = cyVar;
    }

    public void load(@NonNull MediationAdConfig mediationAdConfig, @NonNull MediationInterstitialAdListener mediationInterstitialAdListener, @NonNull Context context) {
        String placementId = mediationAdConfig.getPlacementId();
        try {
            int parseInt = Integer.parseInt(placementId);
            this.ad = new InterstitialAd(parseInt, context);
            this.ad.setMediationEnabled(false);
            this.ad.setListener(new AdListener(mediationInterstitialAdListener));
            this.ad.setTrackingLocationEnabled(mediationAdConfig.isTrackingLocationEnabled());
            this.ad.setTrackingEnvironmentEnabled(mediationAdConfig.isTrackingEnvironmentEnabled());
            CustomParams customParams = this.ad.getCustomParams();
            customParams.setAge(mediationAdConfig.getAge());
            customParams.setGender(mediationAdConfig.getGender());
            for (Entry entry : mediationAdConfig.getServerParams().entrySet()) {
                customParams.setCustomParam((String) entry.getKey(), (String) entry.getValue());
            }
            String payload = mediationAdConfig.getPayload();
            if (this.section != null) {
                ah.a("MyTargetInterstitialAdAdapter: got banner from mediation response");
                this.ad.handleSection(this.section);
            } else if (TextUtils.isEmpty(payload)) {
                StringBuilder sb = new StringBuilder();
                sb.append("MyTargetInterstitialAdAdapter: load id ");
                sb.append(parseInt);
                ah.a(sb.toString());
                this.ad.load();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("MyTargetInterstitialAdAdapter: load id ");
                sb2.append(parseInt);
                sb2.append(" from BID ");
                sb2.append(payload);
                ah.a(sb2.toString());
                this.ad.loadFromBid(payload);
            }
        } catch (NumberFormatException unused) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("failed to request ad, unable to convert slotId ");
            sb3.append(placementId);
            sb3.append(" to int");
            String sb4 = sb3.toString();
            StringBuilder sb5 = new StringBuilder();
            sb5.append("MyTargetInterstitialAdAdapter error: ");
            sb5.append(sb4);
            ah.b(sb5.toString());
            mediationInterstitialAdListener.onNoAd(sb4, this);
        }
    }

    public void show(@NonNull Context context) {
        if (this.ad != null) {
            this.ad.show();
        }
    }

    public void dismiss() {
        if (this.ad != null) {
            this.ad.dismiss();
        }
    }

    public void destroy() {
        if (this.ad != null) {
            this.ad.setListener(null);
            this.ad.destroy();
            this.ad = null;
        }
    }
}
