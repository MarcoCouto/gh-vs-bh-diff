package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;

/* compiled from: NativeAppwallAdResultProcessor */
public class ab extends d<dc> {
    @NonNull
    public static ab m() {
        return new ab();
    }

    private ab() {
    }

    @Nullable
    public dc a(@NonNull dc dcVar, @NonNull a aVar, @NonNull Context context) {
        if (aVar.getCachePeriod() > 0 && !dcVar.aZ() && dcVar.cd() != null) {
            hs K = hs.K(context);
            int slotId = aVar.getSlotId();
            if (K != null) {
                K.a(slotId, dcVar.cd().toString(), false);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to open disk cache and save data for slotId ");
                sb.append(slotId);
                ah.a(sb.toString());
            }
        }
        if (aVar.isAutoLoadImages()) {
            ArrayList arrayList = new ArrayList();
            for (cr crVar : dcVar.bT()) {
                ImageData statusIcon = crVar.getStatusIcon();
                ImageData coinsIcon = crVar.getCoinsIcon();
                ImageData gotoAppIcon = crVar.getGotoAppIcon();
                ImageData icon = crVar.getIcon();
                ImageData labelIcon = crVar.getLabelIcon();
                ImageData bubbleIcon = crVar.getBubbleIcon();
                ImageData itemHighlightIcon = crVar.getItemHighlightIcon();
                ImageData crossNotifIcon = crVar.getCrossNotifIcon();
                if (statusIcon != null) {
                    arrayList.add(statusIcon);
                }
                if (coinsIcon != null) {
                    arrayList.add(coinsIcon);
                }
                if (gotoAppIcon != null) {
                    arrayList.add(gotoAppIcon);
                }
                if (icon != null) {
                    arrayList.add(icon);
                }
                if (labelIcon != null) {
                    arrayList.add(labelIcon);
                }
                if (bubbleIcon != null) {
                    arrayList.add(bubbleIcon);
                }
                if (itemHighlightIcon != null) {
                    arrayList.add(itemHighlightIcon);
                }
                if (crossNotifIcon != null) {
                    arrayList.add(crossNotifIcon);
                }
            }
            if (arrayList.size() > 0) {
                hu.e(arrayList).M(context);
            }
        }
        return dcVar;
    }
}
