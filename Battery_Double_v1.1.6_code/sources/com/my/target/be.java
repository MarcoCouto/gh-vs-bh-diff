package com.my.target;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ads.MyTargetView;
import com.my.target.ads.MyTargetView.MyTargetViewListener;
import com.my.target.b.C0056b;
import java.lang.ref.WeakReference;

/* compiled from: StandardAdMasterEngine */
public class be {
    @NonNull
    final a adConfig;
    @NonNull
    final MyTargetView bQ;
    @NonNull
    final b bR = new b();
    @NonNull
    final c bS;
    @Nullable
    private ap bT;
    private boolean bU = true;
    private boolean bV;
    private int bW = -1;
    private long bX;
    private long bY;

    /* compiled from: StandardAdMasterEngine */
    static class a implements com.my.target.ap.a {
        @NonNull
        private final be engine;

        public a(@NonNull be beVar) {
            this.engine = beVar;
        }

        public void ac() {
            this.engine.ac();
        }

        public void e(@NonNull String str) {
            this.engine.e(str);
        }

        public void ad() {
            this.engine.ad();
        }

        public void onClick() {
            this.engine.onClick();
        }

        public void ae() {
            this.engine.ae();
        }

        public void af() {
            this.engine.af();
        }
    }

    /* compiled from: StandardAdMasterEngine */
    static class b {
        private boolean ca;
        private boolean cb;
        private boolean cc;
        private boolean cd;
        private boolean ce;
        private boolean cf;
        private boolean cg;

        b() {
        }

        public void e(boolean z) {
            this.cb = z;
        }

        public void f(boolean z) {
            this.cd = z;
        }

        public void setFocused(boolean z) {
            this.ce = z;
        }

        public boolean aC() {
            return this.cd && this.cc && (this.cg || this.ce) && !this.ca;
        }

        public boolean aD() {
            return this.cc && this.ca && (this.cg || this.ce) && !this.cf && this.cb;
        }

        public void g(boolean z) {
            this.ca = z;
            this.cb = false;
        }

        public boolean canPause() {
            return !this.cb && this.ca && (this.cg || !this.ce);
        }

        public void aE() {
            this.cf = false;
            this.cc = false;
        }

        public void h(boolean z) {
            this.cc = z;
        }

        public void i(boolean z) {
            this.cf = z;
        }

        public void j(boolean z) {
            this.cg = z;
        }

        public boolean isPaused() {
            return this.cb;
        }

        public boolean aF() {
            return this.ca;
        }
    }

    /* compiled from: StandardAdMasterEngine */
    static class c implements Runnable {
        @NonNull
        private final WeakReference<be> ch;

        c(@NonNull be beVar) {
            this.ch = new WeakReference<>(beVar);
        }

        public void run() {
            be beVar = (be) this.ch.get();
            if (beVar != null) {
                beVar.az();
            }
        }
    }

    @NonNull
    public static be a(@NonNull MyTargetView myTargetView, @NonNull a aVar) {
        return new be(myTargetView, aVar);
    }

    private be(@NonNull MyTargetView myTargetView, @NonNull a aVar) {
        this.bQ = myTargetView;
        this.adConfig = aVar;
        this.bS = new c(this);
        if (!(myTargetView.getContext() instanceof Activity)) {
            ah.a("MyTargetView was created with non-activity focus, so system cannot automatically handle lifecycle");
            this.bR.j(true);
            return;
        }
        this.bR.j(false);
    }

    public void a(@NonNull dd ddVar) {
        if (this.bR.aF()) {
            stop();
        }
        aA();
        b(ddVar);
        if (this.bT != null) {
            this.bT.a(new a(this));
            this.bX = System.currentTimeMillis() + ((long) this.bW);
            this.bY = 0;
            if (this.bV && this.bR.isPaused()) {
                this.bY = (long) this.bW;
            }
            this.bT.prepare();
        }
    }

    public void d(boolean z) {
        this.bR.f(z);
        this.bR.setFocused(this.bQ.hasWindowFocus());
        if (this.bR.aC()) {
            start();
        } else if (!z && this.bR.aF()) {
            stop();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        this.bR.setFocused(z);
        if (this.bR.aC()) {
            start();
        } else if (this.bR.aD()) {
            resume();
        } else if (this.bR.canPause()) {
            pause();
        }
    }

    public void destroy() {
        if (this.bR.aF()) {
            stop();
        }
        this.bR.aE();
        aA();
    }

    @Nullable
    public String aa() {
        if (this.bT != null) {
            return this.bT.aa();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void az() {
        ah.a("load new standard ad");
        ae.a(this.adConfig).a((C0056b<T>) new com.my.target.ae.a() {
            public void onResult(@Nullable dd ddVar, @Nullable String str) {
                if (ddVar != null) {
                    be.this.a(ddVar);
                    return;
                }
                ah.a("No new ad");
                be.this.aB();
            }
        }).a(this.bQ.getContext());
    }

    private void b(@NonNull dd ddVar) {
        boolean z = true;
        this.bV = ddVar.cl() && this.adConfig.isRefreshAd() && !this.adConfig.getFormat().equals("standard_300x250");
        cs cn = ddVar.cn();
        if (cn == null) {
            ct bI = ddVar.bI();
            if (bI == null) {
                MyTargetViewListener listener = this.bQ.getListener();
                if (listener != null) {
                    listener.onNoAd("no ad", this.bQ);
                }
                return;
            }
            this.bT = ba.a(this.bQ, bI, this.adConfig);
            if (this.bV) {
                this.bW = bI.bC() * 1000;
                if (this.bW <= 0) {
                    z = false;
                }
                this.bV = z;
            }
        } else {
            this.bT = bd.a(this.bQ, cn, ddVar, this.adConfig);
            this.bW = cn.getTimeout() * 1000;
        }
    }

    /* access modifiers changed from: 0000 */
    public void aA() {
        if (this.bT != null) {
            this.bT.destroy();
            this.bT.a(null);
            this.bT = null;
        }
        this.bQ.removeAllViews();
    }

    /* access modifiers changed from: 0000 */
    public void start() {
        if (this.bW > 0 && this.bV) {
            this.bQ.postDelayed(this.bS, (long) this.bW);
        }
        if (this.bT != null) {
            this.bT.start();
        }
        this.bR.g(true);
    }

    /* access modifiers changed from: 0000 */
    public void resume() {
        if (this.bY > 0 && this.bV) {
            this.bX = System.currentTimeMillis() + this.bY;
            this.bQ.postDelayed(this.bS, this.bY);
            this.bY = 0;
        }
        if (this.bT != null) {
            this.bT.resume();
        }
        this.bR.e(false);
    }

    /* access modifiers changed from: 0000 */
    public void pause() {
        this.bQ.removeCallbacks(this.bS);
        if (this.bV) {
            this.bY = this.bX - System.currentTimeMillis();
        }
        if (this.bT != null) {
            this.bT.pause();
        }
        this.bR.e(true);
    }

    /* access modifiers changed from: 0000 */
    public void stop() {
        this.bR.g(false);
        this.bQ.removeCallbacks(this.bS);
        if (this.bT != null) {
            this.bT.stop();
        }
    }

    /* access modifiers changed from: 0000 */
    public void aB() {
        if (this.bV && this.bW > 0) {
            this.bQ.removeCallbacks(this.bS);
            this.bQ.postDelayed(this.bS, (long) this.bW);
        }
    }

    /* access modifiers changed from: 0000 */
    public void ac() {
        if (this.bU) {
            this.bR.h(true);
            MyTargetViewListener listener = this.bQ.getListener();
            if (listener != null) {
                listener.onLoad(this.bQ);
            }
            this.bU = false;
        }
        if (this.bR.aC()) {
            start();
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(@NonNull String str) {
        if (this.bU) {
            this.bR.h(false);
            MyTargetViewListener listener = this.bQ.getListener();
            if (listener != null) {
                listener.onNoAd(str, this.bQ);
            }
            this.bU = false;
            return;
        }
        aA();
        aB();
    }

    /* access modifiers changed from: 0000 */
    public void af() {
        this.bR.i(false);
        if (this.bR.aD()) {
            resume();
        }
    }

    /* access modifiers changed from: 0000 */
    public void ae() {
        if (this.bR.canPause()) {
            pause();
        }
        this.bR.i(true);
    }

    /* access modifiers changed from: private */
    public void onClick() {
        MyTargetViewListener listener = this.bQ.getListener();
        if (listener != null) {
            listener.onClick(this.bQ);
        }
    }

    /* access modifiers changed from: private */
    public void ad() {
        MyTargetViewListener listener = this.bQ.getListener();
        if (listener != null) {
            listener.onShow(this.bQ);
        }
    }
}
