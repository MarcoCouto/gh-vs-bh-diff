package com.my.target;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JsStartCall */
public class bk extends bg {
    public bk(@NonNull String str, @Nullable String[] strArr, int i) throws JSONException {
        JSONArray jSONArray;
        super("start");
        this.cj.put("format", str);
        this.cj.put("orientation", i);
        this.cj.put("rotation", false);
        if (strArr != null && strArr.length > 0) {
            if (VERSION.SDK_INT >= 19) {
                jSONArray = new JSONArray(strArr);
            } else {
                jSONArray = new JSONArray();
                for (String put : strArr) {
                    jSONArray.put(put);
                }
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("excludeBanners", jSONArray);
            this.cj.put("filter", jSONObject);
        }
    }
}
