package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import com.my.target.hi.a;
import com.my.target.nativeads.views.MediaAdView;
import com.tapjoy.TapjoyConstants;

@SuppressLint({"ViewConstructor"})
/* compiled from: PromoStyle2ViewImpl */
public class hj extends ViewGroup implements OnClickListener, hi {
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final TextView hN;
    @NonNull
    private final ge imageView;
    @Nullable
    private final Bitmap jt;
    @Nullable
    private final Bitmap ju;
    @NonNull
    private final gi kG;
    @NonNull
    private final ProgressBar kO;
    private int kR;
    private int kS;
    @NonNull
    private final TextView ku;
    @NonNull
    private final TextView lI;
    @NonNull
    private final ge lU;
    private int lX = this.uiUtils.M(8);
    private final int lZ = this.uiUtils.M(16);
    private final int mA = this.uiUtils.M(48);
    private final int mB = this.uiUtils.M(6);
    private final int mC = this.uiUtils.M(6);
    private final int mD = this.uiUtils.M(2);
    private final int mE = this.uiUtils.M(24);
    private final int mF = this.uiUtils.M(40);
    private int mG;
    private final int ma = (this.mF - (this.padding / 2));
    @NonNull
    private final ga mi;
    @NonNull
    private final ga mj;
    @NonNull
    private final fs mk;
    @NonNull
    private final View ml;
    @NonNull
    private final View mm;
    @NonNull
    private final a mn;
    @NonNull
    private final gr mo;
    @NonNull
    private final Button mp;
    @NonNull
    private final fq mq;
    @NonNull
    private final View mr;
    @NonNull
    private final View ms;
    @NonNull
    private final View mt;
    @NonNull
    private final fu mu;
    @Nullable
    private final Bitmap mv;
    @Nullable
    private final Bitmap mw;
    @Nullable
    private final Bitmap mx;
    private final int my = this.uiUtils.M(64);
    private final int mz = this.uiUtils.M(44);
    private final int padding = this.uiUtils.M(16);
    @NonNull
    private final ic uiUtils;

    @NonNull
    public View eo() {
        return this;
    }

    public hj(@NonNull fs fsVar, @NonNull View view, @NonNull View view2, @NonNull a aVar, @NonNull Context context) {
        super(context);
        this.mn = aVar;
        this.mk = fsVar;
        this.mm = view2;
        this.ml = view;
        this.uiUtils = ic.P(context);
        this.mj = new ga(context);
        this.mj.setVisibility(8);
        this.mj.setOnClickListener(this);
        this.mo = new gr(context);
        this.mo.setVisibility(8);
        this.mo.setOnClickListener(this);
        ic.a(this.mo, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        this.mp = new Button(context);
        this.mp.setTextColor(-1);
        this.mp.setLines(1);
        this.mp.setTextSize(2, 18.0f);
        this.mp.setMaxWidth(this.uiUtils.M(200));
        this.mp.setOnClickListener(this);
        this.mp.setBackgroundColor(0);
        this.mq = new fq(context);
        this.mq.setFixedHeight(this.uiUtils.M(20));
        this.mv = fm.G(context);
        this.mw = fm.F(context);
        this.mx = fm.H(context);
        this.jt = fm.C(this.uiUtils.M(28));
        this.ju = fm.D(this.uiUtils.M(28));
        this.imageView = new ge(context);
        this.kO = new ProgressBar(context, null, 16842874);
        this.kO.setVisibility(8);
        this.mr = new View(context);
        this.mr.setBackgroundColor(-1728053248);
        this.mr.setVisibility(8);
        this.mt = new View(context);
        this.ms = new View(context);
        this.hN = new TextView(context);
        this.hN.setTextSize(2, 22.0f);
        this.hN.setTypeface(this.hN.getTypeface(), 1);
        this.hN.setTextColor(-1);
        this.hN.setMaxLines(2);
        this.hN.setEllipsize(TruncateAt.END);
        this.hN.setGravity(17);
        this.lI = new TextView(context);
        this.lI.setTextSize(2, 16.0f);
        this.lI.setTextColor(-1);
        this.lI.setMaxLines(2);
        this.lI.setEllipsize(TruncateAt.END);
        this.lI.setGravity(17);
        this.ku = new TextView(context);
        this.ku.setTextSize(2, 10.0f);
        this.ku.setMaxLines(1);
        this.ku.setEllipsize(TruncateAt.END);
        this.ku.setGravity(17);
        this.ctaButton = new Button(context);
        this.ctaButton.setLines(1);
        this.ctaButton.setTextSize(2, 24.0f);
        this.ctaButton.setEllipsize(TruncateAt.END);
        int M = this.uiUtils.M(6);
        int i = M * 2;
        this.ctaButton.setPadding(i, M, i, M);
        this.mu = new fu(context);
        this.mu.setPadding(this.uiUtils.M(2), 0, 0, 0);
        this.mu.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
        this.mu.setMaxLines(1);
        this.mu.setTextSize(2, 12.0f);
        this.mu.a(1, 1711276032, this.uiUtils.M(3));
        this.mu.setBackgroundColor(1711276032);
        this.kG = new gi(context);
        int M2 = this.uiUtils.M(10);
        this.kG.setPadding(M2, M2, M2, M2);
        this.mi = new ga(context);
        this.mi.setPadding(0);
        this.lU = new ge(context);
        ic.b(this.hN, "title");
        ic.b(this.lI, "description");
        ic.b(this.ku, "disclaimer");
        ic.b(this.imageView, MessengerShareContentUtility.MEDIA_IMAGE);
        ic.b(this.ctaButton, "cta");
        ic.b(this.mj, TapjoyConstants.TJC_FULLSCREEN_AD_DISMISS_URL);
        ic.b(this.mo, "play");
        ic.b(this.lU, "ads_logo");
        ic.b(this.mr, "media_dim");
        ic.b(this.ms, "top_dim");
        ic.b(this.mt, "bot_dim");
        addView(fsVar);
        addView(this.imageView);
        addView(this.mt);
        addView(this.ms);
        addView(this.mr);
        addView(this.mi);
        addView(this.ml);
        addView(this.mj);
        addView(this.mo);
        addView(this.mp);
        addView(this.kO);
        addView(this.hN);
        addView(this.lI);
        addView(this.ctaButton);
        addView(this.ku);
        addView(this.mu);
        addView(this.kG);
        addView(view2);
        addView(this.lU);
        addView(this.mq);
    }

    public void setSoundState(boolean z) {
        if (z) {
            this.mi.a(this.jt, false);
            this.mi.setContentDescription("sound_on");
            return;
        }
        this.mi.a(this.ju, false);
        this.mi.setContentDescription("sound_off");
    }

    public void dC() {
        this.mj.setVisibility(0);
        this.kG.setVisibility(8);
    }

    public void a(int i, @Nullable String str) {
        this.mo.setVisibility(0);
        if (i == 1) {
            this.mo.setImageBitmap(this.mx);
            this.mG = 1;
        } else if (i == 2) {
            this.mo.setImageBitmap(this.mw);
            this.mG = 2;
        } else {
            this.mo.setImageBitmap(this.mv);
            this.mG = 0;
        }
        if (str != null) {
            this.mp.setVisibility(0);
            this.mp.setText(str);
            return;
        }
        this.mp.setVisibility(8);
    }

    public void ep() {
        this.mo.setVisibility(8);
        this.mp.setVisibility(8);
    }

    public void setBackgroundImage(ImageData imageData) {
        this.imageView.setImageData(imageData);
    }

    public void D(boolean z) {
        this.imageView.setVisibility(z ? 0 : 4);
    }

    public void E(boolean z) {
        this.kO.setVisibility(z ? 0 : 8);
    }

    public void F(boolean z) {
        this.mr.setVisibility(z ? 0 : 8);
    }

    public void setPanelColor(int i) {
        this.mt.setBackgroundColor(i);
        this.ms.setBackgroundColor(i);
    }

    public void a(int i, float f) {
        this.kG.setDigit(i);
        this.kG.setProgress(f);
    }

    public void eq() {
        this.kG.setVisibility(8);
    }

    public void setBanner(@NonNull cn cnVar) {
        ce promoStyleSettings = cnVar.getPromoStyleSettings();
        setBackgroundColor(promoStyleSettings.getBackgroundColor());
        int textColor = promoStyleSettings.getTextColor();
        this.hN.setTextColor(promoStyleSettings.getTitleColor());
        this.lI.setTextColor(textColor);
        this.ku.setTextColor(textColor);
        if (!TextUtils.isEmpty(cnVar.getAgeRestrictions()) || !TextUtils.isEmpty(cnVar.getAdvertisingLabel())) {
            String advertisingLabel = cnVar.getAdvertisingLabel();
            if (!TextUtils.isEmpty(cnVar.getAgeRestrictions()) && !TextUtils.isEmpty(cnVar.getAdvertisingLabel())) {
                StringBuilder sb = new StringBuilder();
                sb.append(advertisingLabel);
                sb.append(" ");
                advertisingLabel = sb.toString();
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(advertisingLabel);
            sb2.append(cnVar.getAgeRestrictions());
            String sb3 = sb2.toString();
            this.mu.setVisibility(0);
            this.mu.setText(sb3);
        } else {
            this.mu.setVisibility(8);
        }
        co videoBanner = cnVar.getVideoBanner();
        if (videoBanner == null) {
            this.mi.setVisibility(8);
            this.mm.setVisibility(8);
            this.mk.setVisibility(8);
            this.mr.setVisibility(8);
        } else {
            this.mm.setVisibility(0);
            this.mi.setVisibility(0);
            this.mi.setOnClickListener(this);
            this.mi.a(this.jt, false);
            this.mi.setContentDescription("sound_on");
            this.mo.setImageBitmap(this.mv);
            this.mG = 0;
            this.mk.e(videoBanner.getWidth(), videoBanner.getHeight());
            this.imageView.setOnClickListener(this);
            this.mk.setOnClickListener(this);
            this.mr.setOnClickListener(this);
            VideoData videoData = (VideoData) videoBanner.getMediaData();
            ImageData preview = videoBanner.getPreview();
            if (videoData != null) {
                this.kS = videoData.getWidth();
                this.kR = videoData.getHeight();
            }
            if ((this.kS <= 0 || this.kR <= 0) && preview != null) {
                this.kS = preview.getWidth();
                this.kR = preview.getHeight();
            }
            if (this.kS <= 0 || this.kR <= 0) {
                ImageData image = cnVar.getImage();
                if (image != null) {
                    this.kS = image.getWidth();
                    this.kR = image.getHeight();
                    if (this.kS <= 0 || this.kR <= 0) {
                        Bitmap bitmap = image.getBitmap();
                        if (bitmap != null) {
                            this.kS = bitmap.getWidth();
                            this.kR = bitmap.getHeight();
                        }
                    }
                }
            }
        }
        ImageData closeIcon = cnVar.getCloseIcon();
        if (closeIcon == null || closeIcon.getData() == null) {
            Bitmap B = fl.B(this.uiUtils.M(28));
            if (B != null) {
                this.mj.a(B, false);
            }
        } else {
            this.mj.a(closeIcon.getData(), true);
        }
        ic.a(this.ctaButton, promoStyleSettings.br(), promoStyleSettings.bs(), this.lX);
        this.ctaButton.setTextColor(promoStyleSettings.getTextColor());
        this.ctaButton.setText(cnVar.getCtaText());
        this.hN.setText(cnVar.getTitle());
        this.lI.setText(cnVar.getDescription());
        String disclaimer = cnVar.getDisclaimer();
        if (!TextUtils.isEmpty(disclaimer)) {
            this.ku.setText(disclaimer);
        } else {
            this.ku.setVisibility(8);
        }
        ImageData adIcon = cnVar.getAdIcon();
        if (!(adIcon == null || adIcon.getBitmap() == null)) {
            this.lU.setImageData(adIcon);
            this.lU.setOnClickListener(this);
        }
        by adChoices = cnVar.getAdChoices();
        if (adChoices != null) {
            this.mq.setImageBitmap(adChoices.getIcon().getBitmap());
            this.mq.setOnClickListener(this);
        } else {
            this.mq.setVisibility(8);
        }
        setClickArea(cnVar.getClickArea());
    }

    public void onClick(View view) {
        if (view == this.mj) {
            this.mn.dw();
        } else if (view == this.mi) {
            this.mn.cZ();
        } else if (view == this.mo || view == this.mp) {
            this.mn.A(this.mG);
        } else if (view == this.mk) {
            this.mn.dz();
        } else if (view == this.mr) {
            this.mn.dA();
        } else if (view == this.lU) {
            this.mn.dx();
        } else if (view == this.mq) {
            this.mn.dy();
        } else {
            this.mn.du();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007a  */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        if (this.kS <= 0 || this.kR <= 0) {
            i4 = size;
        } else {
            float f = ((float) this.kS) / ((float) this.kR);
            float f2 = (float) size;
            float f3 = f2 / ((float) this.kS);
            float f4 = (float) size2;
            if (Math.min(f3, f4 / ((float) this.kR)) != f3 || f <= 0.0f) {
                i4 = (int) (f4 * f);
            } else {
                i3 = (int) (f2 / f);
                i4 = size;
                ic.b(this.mk, i4, i3, Integer.MIN_VALUE);
                ic.b(this.imageView, i4, i3, Integer.MIN_VALUE);
                ic.b(this.mr, i4, i3, 1073741824);
                if (size >= size2) {
                    l(size, size2);
                } else {
                    k(size, size2);
                }
                ic.b(this.mi, this.mE, this.mE, 1073741824);
                ic.b(this.mj, this.my, this.my, 1073741824);
                ic.b(this.kG, this.mz, this.mz, 1073741824);
                if (this.mo.getVisibility() == 0) {
                    ic.b(this.mo, this.mA, this.mA, 1073741824);
                    if (this.mp.getVisibility() == 0) {
                        this.mp.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(this.mo.getMeasuredHeight(), 1073741824));
                    }
                }
                ic.b(this.kO, this.mA, this.mA, 1073741824);
                ic.b(this.mm, size, this.mB, 1073741824);
                ic.b(this.mu, size, size2, Integer.MIN_VALUE);
                ic.b(this.mq, size, size2, Integer.MIN_VALUE);
                setMeasuredDimension(size, size2);
            }
        }
        i3 = size2;
        ic.b(this.mk, i4, i3, Integer.MIN_VALUE);
        ic.b(this.imageView, i4, i3, Integer.MIN_VALUE);
        ic.b(this.mr, i4, i3, 1073741824);
        if (size >= size2) {
        }
        ic.b(this.mi, this.mE, this.mE, 1073741824);
        ic.b(this.mj, this.my, this.my, 1073741824);
        ic.b(this.kG, this.mz, this.mz, 1073741824);
        if (this.mo.getVisibility() == 0) {
        }
        ic.b(this.kO, this.mA, this.mA, 1073741824);
        ic.b(this.mm, size, this.mB, 1073741824);
        ic.b(this.mu, size, size2, Integer.MIN_VALUE);
        ic.b(this.mq, size, size2, Integer.MIN_VALUE);
        setMeasuredDimension(size, size2);
    }

    private boolean I(int i) {
        double a = (double) ic.a(this.mk.getMeasuredWidth(), this.imageView.getMeasuredWidth());
        Double.isNaN(a);
        if (a * 1.6d <= ((double) i)) {
            return true;
        }
        return false;
    }

    private void k(int i, int i2) {
        this.hN.setVisibility(8);
        this.lI.setVisibility(8);
        this.ku.setVisibility(8);
        this.ms.setVisibility(8);
        this.ml.setVisibility(0);
        this.lU.setVisibility(8);
        if (I(i)) {
            this.mt.setVisibility(8);
            int a = i - ic.a(this.mk.getMeasuredWidth(), this.imageView.getMeasuredWidth());
            ic.b(this.ml, a, a, Integer.MIN_VALUE);
        } else {
            this.mt.setVisibility(0);
            ic.b(this.ml, i, i2, Integer.MIN_VALUE);
        }
        ic.b(this.mt, this.ml.getMeasuredWidth(), this.ml.getMeasuredHeight(), 1073741824);
    }

    private void l(int i, int i2) {
        if (!TextUtils.isEmpty(this.ku.getText())) {
            this.ku.setVisibility(0);
        }
        this.ml.setVisibility(0);
        this.ms.setVisibility(0);
        this.mt.setVisibility(0);
        this.lU.setVisibility(0);
        ic.b(this.ml, i - this.my, i2, Integer.MIN_VALUE);
        ic.b(this.ms, i, this.ml.getMeasuredHeight(), 1073741824);
        double a = (double) ic.a(this.mk.getMeasuredHeight(), this.imageView.getMeasuredHeight());
        Double.isNaN(a);
        if (a * 1.6d > ((double) i2)) {
            this.hN.setVisibility(8);
            if (!TextUtils.isEmpty(this.lI.getText())) {
                this.lI.setVisibility(0);
            }
        } else {
            if (!TextUtils.isEmpty(this.hN.getText())) {
                this.hN.setVisibility(0);
            } else {
                this.hN.setVisibility(8);
            }
            if (!TextUtils.isEmpty(this.lI.getText())) {
                this.lI.setVisibility(0);
            } else {
                this.lI.setVisibility(8);
            }
        }
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec((i - (this.padding * 2)) - (this.mF * 2), 1073741824), MeasureSpec.makeMeasureSpec(i2 - (this.padding * 2), Integer.MIN_VALUE));
        ic.b(this.hN, i - (this.padding * 2), i2 - (this.padding * 2), Integer.MIN_VALUE);
        ic.b(this.lI, i - (this.padding * 2), i2 - (this.padding * 2), Integer.MIN_VALUE);
        ic.b(this.ku, i - (this.padding * 2), i2 - (this.padding * 2), Integer.MIN_VALUE);
        ic.b(this.mt, i, i2, 1073741824);
        ic.b(this.lU, this.ma, this.lZ, Integer.MIN_VALUE);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        if (i5 < i6) {
            e(i, i2, i3, i4);
        } else {
            f(i, i2, i3, i4);
        }
        if (this.kO.getVisibility() == 0) {
            ic.a((View) this.kO, this.mk.getLeft(), this.mk.getTop(), this.mk.getRight(), this.mk.getBottom());
        }
        if (this.mm.getVisibility() == 0) {
            ic.d(this.mm, i6, i);
        }
        if (this.mo.getVisibility() == 0) {
            if (this.mp.getVisibility() != 0) {
                ic.a((View) this.mo, this.mk.getLeft(), this.mk.getTop(), this.mk.getRight(), this.mk.getBottom());
            } else {
                int left = this.mk.getLeft() + (((this.mk.getRight() - this.mk.getLeft()) - ((this.mo.getMeasuredWidth() + this.padding) + this.mp.getMeasuredWidth())) / 2);
                int top = this.mk.getTop() + (((this.mk.getBottom() - this.mk.getTop()) - this.mo.getMeasuredHeight()) / 2);
                ic.b(this.mo, top, left);
                ic.b(this.mp, top, this.mo.getRight() + this.padding);
            }
        }
        if (this.mj.getVisibility() == 0) {
            ic.c(this.mj, this.mC + i2, i5 - this.mC);
        } else {
            ic.c(this.kG, this.mC + i2, i5 - this.mC);
        }
        ic.c(this.mq, ic.a(i2 + this.padding, this.mj.getBottom() + this.padding, this.kG.getBottom() + this.padding), i3 - this.padding);
    }

    private void e(int i, int i2, int i3, int i4) {
        int i5;
        boolean z = false;
        ic.a((View) this.ctaButton, 0, (i4 - this.padding) - this.ctaButton.getMeasuredHeight(), i3, i4 - this.padding);
        ic.a((View) this.mk, i, i2, i3, i4);
        ic.a((View) this.imageView, i, i2, i3, i4);
        this.mr.layout(ic.a(this.imageView.getLeft(), this.mk.getLeft()), ic.a(this.imageView.getTop(), this.mk.getTop()), ic.a(this.imageView.getRight(), this.mk.getRight()), ic.a(this.imageView.getBottom(), this.mk.getBottom()));
        int measuredHeight = this.lI.getMeasuredHeight() + (this.padding / 2);
        if (this.hN.getVisibility() == 0) {
            measuredHeight += this.padding + this.hN.getMeasuredHeight();
        }
        if (this.ku.getVisibility() == 0) {
            measuredHeight += this.padding + this.ku.getMeasuredHeight();
        }
        int i6 = i3 - i;
        int measuredWidth = this.padding + (((i6 - (this.padding * 2)) - this.hN.getMeasuredWidth()) / 2);
        int measuredWidth2 = this.padding + (((i6 - (this.padding * 2)) - this.lI.getMeasuredWidth()) / 2);
        int measuredWidth3 = this.padding + (((i6 - (this.padding * 2)) - this.ku.getMeasuredWidth()) / 2);
        int a = ic.a(this.imageView.getBottom(), this.mk.getBottom());
        if (measuredHeight < this.ctaButton.getTop() - a) {
            int top = a + (((this.ctaButton.getTop() - a) - measuredHeight) / 2);
            ic.b(this.hN, top, measuredWidth);
            ic.b(this.lI, ic.a(top, this.hN.getBottom() + this.padding), measuredWidth2);
            ic.b(this.ku, ic.a(top, this.hN.getBottom() + this.padding, this.lI.getBottom() + this.padding), measuredWidth3);
        } else {
            ic.d(this.ku, this.ctaButton.getTop() - this.padding, measuredWidth3);
            ic.d(this.lI, (this.ku.getVisibility() == 0 ? this.ku.getTop() : this.ctaButton.getTop()) - this.padding, measuredWidth2);
            this.hN.layout(0, 0, 0, 0);
        }
        ic.b(this.ml, i2, i);
        if (this.hN.getTop() > 0) {
            i5 = this.hN.getTop();
        } else if (this.lI.getTop() > 0) {
            i5 = this.lI.getTop();
        } else if (this.ku.getTop() > 0) {
            i5 = this.ku.getTop();
        } else {
            i5 = this.ctaButton.getTop();
        }
        ic.b(this.mt, i5 - this.padding, i);
        ic.b(this.ms, this.ml.getTop(), this.ml.getLeft());
        ic.d(this.mu, Math.min(this.mt.getTop(), Math.max(this.mk.getBottom(), this.imageView.getBottom())) - this.mD, this.mD);
        ic.e(this.mi, Math.min(this.mt.getTop(), Math.max(this.mk.getBottom(), this.imageView.getBottom())) - this.mD, i3 - this.padding);
        a aVar = this.mn;
        double bottom = (double) (this.imageView.getBottom() - this.mt.getTop());
        double measuredHeight2 = (double) this.imageView.getMeasuredHeight();
        Double.isNaN(measuredHeight2);
        if (bottom > measuredHeight2 * 0.1d) {
            z = true;
        }
        aVar.t(z);
        ic.e(this.lU, this.mm.getVisibility() == 0 ? this.mm.getTop() : i4 - i2, i3);
    }

    private void f(int i, int i2, int i3, int i4) {
        if (I(i3 - i)) {
            ic.a((View) this.imageView, i, i2, this.imageView.getMeasuredWidth() + i, i4);
            ic.a((View) this.mk, i, i2, this.mk.getMeasuredWidth() + i, i4);
            this.mr.layout(this.mk.getLeft(), this.mk.getTop(), this.mk.getRight(), this.mk.getBottom());
            this.ml.layout(ic.a(this.mk.getRight(), this.imageView.getRight()), i2, i3, i4);
            this.ms.layout(0, 0, 0, 0);
            int min = Math.min(this.imageView.getLeft(), this.mk.getLeft() + this.mD);
            int i5 = i4 - i2;
            int min2 = Math.min(i5 - this.mD, (i5 - this.mm.getMeasuredHeight()) - this.mD);
            ic.d(this.mu, min2, min);
            ic.e(this.mi, min2, Math.max(this.imageView.getRight(), this.mk.getRight()) - this.padding);
            this.mn.t(false);
            return;
        }
        ic.a((View) this.imageView, i, i2, i3, i4);
        ic.a((View) this.mk, i, i2, i3, i4);
        this.mr.layout(this.imageView.getLeft(), this.imageView.getTop(), this.imageView.getRight(), this.imageView.getBottom());
        ic.d(this.ml, i4 - i2, i);
        this.ms.layout(0, 0, 0, 0);
        ic.b(this.mt, this.ml.getTop(), this.ml.getLeft());
        int min3 = Math.min(this.mt.getTop(), Math.max(this.mk.getBottom(), this.imageView.getBottom())) - this.mD;
        ic.d(this.mu, min3, Math.min(this.imageView.getLeft(), this.mk.getLeft() + this.mD));
        int min4 = Math.min(this.mt.getTop(), Math.max(this.mk.getBottom(), this.imageView.getBottom())) - this.mD;
        ic.e(this.mi, min4, Math.max(this.imageView.getRight(), this.mk.getRight()) - this.padding);
        this.mn.t(true);
    }

    private void setClickArea(@NonNull ca caVar) {
        if (caVar.f456do) {
            setOnClickListener(this);
            this.ctaButton.setOnClickListener(this);
            return;
        }
        if (caVar.di) {
            this.ctaButton.setOnClickListener(this);
        } else {
            this.ctaButton.setEnabled(false);
        }
        if (caVar.dn) {
            setOnClickListener(this);
        } else {
            setOnClickListener(null);
        }
        if (caVar.dc) {
            this.hN.setOnClickListener(this);
        } else {
            this.hN.setOnClickListener(null);
        }
        if (caVar.dj) {
            this.mu.setOnClickListener(this);
        } else {
            this.mu.setOnClickListener(null);
        }
        if (caVar.dd) {
            this.lI.setOnClickListener(this);
        } else {
            this.lI.setOnClickListener(null);
        }
        if (caVar.df) {
            this.imageView.setOnClickListener(this);
        } else {
            this.imageView.setOnClickListener(null);
        }
    }
}
