package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0056b;

/* compiled from: InstreamResearchFactory */
public class l extends b<cc> {

    /* compiled from: InstreamResearchFactory */
    static class a implements com.my.target.b.a<cc> {
        private final int duration;

        public boolean a() {
            return false;
        }

        @Nullable
        public d<cc> c() {
            return null;
        }

        a(int i) {
            this.duration = i;
        }

        @NonNull
        public c<cc> b() {
            return m.a(this.duration);
        }

        @NonNull
        public e d() {
            return n.b(this.duration);
        }
    }

    public interface b extends C0056b {
    }

    @NonNull
    public static b<cc> a(@NonNull a aVar, int i) {
        return new l(aVar, i);
    }

    private l(a aVar, int i) {
        super(new a(i), aVar);
    }
}
