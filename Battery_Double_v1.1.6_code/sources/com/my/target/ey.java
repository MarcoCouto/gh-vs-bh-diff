package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import com.my.target.ho.c;

/* compiled from: InterstitialSliderPresenter */
public class ey {
    /* access modifiers changed from: private */
    @NonNull
    public final hl fZ;
    /* access modifiers changed from: private */
    @Nullable
    public a ga;

    /* compiled from: InterstitialSliderPresenter */
    public interface a {
        void a(@NonNull cm cmVar);

        void aj();

        void b(@NonNull cm cmVar);
    }

    @NonNull
    public static ey v(@NonNull Context context) {
        return new ey(context);
    }

    private ey(@NonNull Context context) {
        this.fZ = new hl(context);
        this.fZ.setFSSliderCardListener(new c() {
            public void f(@NonNull cm cmVar) {
                if (ey.this.ga != null) {
                    ey.this.ga.a(cmVar);
                }
            }

            public void a(int i, @NonNull cm cmVar) {
                if (ey.this.ga != null) {
                    ey.this.ga.b(cmVar);
                }
                ey.this.fZ.J(i);
            }
        });
        this.fZ.setCloseClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (ey.this.ga != null) {
                    ey.this.ga.aj();
                }
            }
        });
    }

    public void a(@NonNull cz czVar) {
        this.fZ.a(czVar, czVar.bT());
    }

    @NonNull
    public View getView() {
        return this.fZ;
    }

    public void a(@Nullable a aVar) {
        this.ga = aVar;
    }
}
