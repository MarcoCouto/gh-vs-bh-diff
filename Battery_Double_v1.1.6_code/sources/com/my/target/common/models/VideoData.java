package com.my.target.common.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ah;
import com.my.target.cd;
import java.util.List;

public final class VideoData extends cd<String> {
    public static final String M3U8 = ".m3u8";
    private int bitrate;
    private final boolean cacheable = (!this.url.endsWith(M3U8));

    @Nullable
    public static VideoData chooseBest(@NonNull List<VideoData> list, int i) {
        VideoData videoData = null;
        int i2 = 0;
        for (VideoData videoData2 : list) {
            int height = videoData2.getHeight();
            if (videoData == null || ((height <= i && i2 > i) || ((height <= i && height > i2) || (height > i && height < i2)))) {
                videoData = videoData2;
                i2 = height;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Accepted videoData quality = ");
        sb.append(i2);
        sb.append(TtmlNode.TAG_P);
        ah.a(sb.toString());
        return videoData;
    }

    @NonNull
    public static VideoData newVideoData(@NonNull String str, int i, int i2) {
        return new VideoData(str, i, i2);
    }

    private VideoData(@NonNull String str, int i, int i2) {
        super(str);
        this.width = i;
        this.height = i2;
    }

    public int getBitrate() {
        return this.bitrate;
    }

    public void setBitrate(int i) {
        this.bitrate = i;
    }

    public boolean isCacheable() {
        return this.cacheable;
    }
}
