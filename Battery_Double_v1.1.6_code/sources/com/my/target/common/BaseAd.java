package com.my.target.common;

import android.support.annotation.NonNull;
import com.my.target.a;
import com.my.target.ah;

public abstract class BaseAd {
    @NonNull
    protected final a adConfig;

    public static void setDebugMode(boolean z) {
        ah.enabled = z;
        if (z) {
            ah.a("Debug mode enabled");
        }
    }

    public void setTrackingEnvironmentEnabled(boolean z) {
        this.adConfig.setTrackingEnvironmentEnabled(z);
    }

    public boolean isTrackingEnvironmentEnabled() {
        return this.adConfig.isTrackingEnvironmentEnabled();
    }

    public void setTrackingLocationEnabled(boolean z) {
        this.adConfig.setTrackingLocationEnabled(z);
    }

    public boolean isTrackingLocationEnabled() {
        return this.adConfig.isTrackingLocationEnabled();
    }

    @NonNull
    public CustomParams getCustomParams() {
        return this.adConfig.getCustomParams();
    }

    protected BaseAd(int i, @NonNull String str) {
        this.adConfig = a.newConfig(i, str);
    }
}
