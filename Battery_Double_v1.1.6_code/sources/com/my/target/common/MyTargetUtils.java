package com.my.target.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.my.target.fg;
import com.my.target.ib;
import java.util.Map;

public class MyTargetUtils {
    public static void sendStat(@NonNull String str, @NonNull Context context) {
        ib.o(str, context);
    }

    @WorkerThread
    public static Map<String, String> collectInfo(@NonNull Context context) {
        fg.dM().collectData(context);
        return fg.dM().getData();
    }
}
