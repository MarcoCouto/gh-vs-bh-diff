package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: AdSection */
public abstract class cv {
    @Nullable
    private ct bi;
    @NonNull
    private final ArrayList<dh> cL = new ArrayList<>();

    public abstract int getBannersCount();

    public void d(@NonNull ArrayList<dh> arrayList) {
        this.cL.addAll(arrayList);
    }

    @NonNull
    public ArrayList<dh> bb() {
        return new ArrayList<>(this.cL);
    }

    @NonNull
    public ArrayList<dh> w(@NonNull String str) {
        ArrayList<dh> arrayList = new ArrayList<>();
        Iterator it = this.cL.iterator();
        while (it.hasNext()) {
            dh dhVar = (dh) it.next();
            if (str.equals(dhVar.getType())) {
                arrayList.add(dhVar);
            }
        }
        return arrayList;
    }

    @Nullable
    public ct bI() {
        return this.bi;
    }

    public void a(@Nullable ct ctVar) {
        this.bi = ctVar;
    }
}
