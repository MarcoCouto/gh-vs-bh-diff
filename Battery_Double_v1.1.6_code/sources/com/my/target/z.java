package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0056b;

/* compiled from: NativeAppwallAdFactory */
public final class z extends b<dc> {

    /* compiled from: NativeAppwallAdFactory */
    static class a implements com.my.target.b.a<dc> {
        public boolean a() {
            return false;
        }

        private a() {
        }

        @NonNull
        public c<dc> b() {
            return aa.f();
        }

        @Nullable
        public d<dc> c() {
            return ab.m();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    public interface b extends C0056b {
    }

    @NonNull
    public static b<dc> a(@NonNull a aVar) {
        return new z(aVar);
    }

    private z(@NonNull a aVar) {
        super(new a(), aVar);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public String a(@NonNull bz bzVar, @NonNull dk dkVar, @NonNull Context context) {
        if (this.adConfig.getCachePeriod() > 0) {
            ah.a("NativeAppwallAdFactory: check cached data");
            String str = null;
            hs K = hs.K(context);
            if (K != null) {
                str = K.a(this.adConfig.getSlotId(), this.adConfig.getCachePeriod());
            }
            if (str != null) {
                ah.a("NativeAppwallAdFactory: cached data loaded successfully");
                bzVar.n(true);
                return str;
            }
            ah.a("NativeAppwallAdFactory: no cached data");
        }
        return super.a(bzVar, dkVar, context);
    }
}
