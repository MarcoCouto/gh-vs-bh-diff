package com.my.target;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.my.target.common.models.ImageData;
import java.util.List;

/* compiled from: SliderRecyclerView */
public class ho extends RecyclerView {
    @NonNull
    private final OnClickListener cardClickListener = new a();
    /* access modifiers changed from: private */
    public int displayedCardNum = -1;
    /* access modifiers changed from: private */
    public boolean moving;
    /* access modifiers changed from: private */
    @NonNull
    public final hn nc = new hn(getContext());
    /* access modifiers changed from: private */
    @Nullable
    public List<cm> nd;
    /* access modifiers changed from: private */
    @Nullable
    public c ne;
    /* access modifiers changed from: private */
    @NonNull
    public final PagerSnapHelper snapHelper;

    /* compiled from: SliderRecyclerView */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (!ho.this.moving) {
                View findContainingItemView = ho.this.nc.findContainingItemView(view);
                if (findContainingItemView != null) {
                    if (!ho.this.nc.g(findContainingItemView)) {
                        int[] calculateDistanceToFinalSnap = ho.this.snapHelper.calculateDistanceToFinalSnap(ho.this.nc, findContainingItemView);
                        if (calculateDistanceToFinalSnap != null) {
                            ho.this.smoothScrollBy(calculateDistanceToFinalSnap[0], 0);
                        }
                    } else if (!(ho.this.ne == null || ho.this.nd == null)) {
                        ho.this.ne.f((cm) ho.this.nd.get(ho.this.nc.getPosition(findContainingItemView)));
                    }
                }
            }
        }
    }

    /* compiled from: SliderRecyclerView */
    static class b extends Adapter<d> {
        private final int backgroundColor;
        @Nullable
        private OnClickListener cardClickListener;
        @NonNull
        private final List<cm> ng;
        @NonNull
        private final Resources nh;

        b(@NonNull List<cm> list, int i, @NonNull Resources resources) {
            this.ng = list;
            this.backgroundColor = i;
            this.nh = resources;
        }

        public int getItemViewType(int i) {
            if (i == 0) {
                return 1;
            }
            return i == this.ng.size() - 1 ? 2 : 0;
        }

        @NonNull
        /* renamed from: c */
        public d onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            hm hmVar = new hm(viewGroup.getContext(), this.backgroundColor);
            hmVar.setLayoutParams(new LayoutParams(-1, -1));
            return new d(hmVar);
        }

        /* renamed from: a */
        public void onBindViewHolder(@NonNull d dVar, int i) {
            a((cm) this.ng.get(i), dVar.es());
            dVar.es().setOnClickListener(this.cardClickListener);
        }

        public int getItemCount() {
            return this.ng.size();
        }

        /* renamed from: a */
        public void onViewRecycled(@NonNull d dVar) {
            dVar.es().setOnClickListener(null);
            super.onViewRecycled(dVar);
        }

        /* access modifiers changed from: 0000 */
        public void setClickListener(@Nullable OnClickListener onClickListener) {
            this.cardClickListener = onClickListener;
        }

        private void a(@NonNull cm cmVar, @NonNull hm hmVar) {
            ImageData optimalLandscapeImage = cmVar.getOptimalLandscapeImage();
            ImageData optimalPortraitImage = cmVar.getOptimalPortraitImage();
            ImageData imageData = this.nh.getConfiguration().orientation == 2 ? optimalLandscapeImage : optimalPortraitImage;
            if (imageData != null) {
                optimalLandscapeImage = imageData;
            } else if (optimalLandscapeImage == null) {
                optimalLandscapeImage = optimalPortraitImage;
            }
            if (optimalLandscapeImage != null) {
                hmVar.setImage(optimalLandscapeImage);
            }
            if (!TextUtils.isEmpty(cmVar.getAgeRestrictions())) {
                hmVar.setAgeRestrictions(cmVar.getAgeRestrictions());
            }
        }
    }

    /* compiled from: SliderRecyclerView */
    public interface c {
        void a(int i, @NonNull cm cmVar);

        void f(@NonNull cm cmVar);
    }

    /* compiled from: SliderRecyclerView */
    static class d extends ViewHolder {
        @NonNull
        private final hm ni;

        d(@NonNull hm hmVar) {
            super(hmVar);
            this.ni = hmVar;
        }

        /* access modifiers changed from: 0000 */
        public hm es() {
            return this.ni;
        }
    }

    public ho(@NonNull Context context) {
        super(context);
        setHasFixedSize(true);
        this.snapHelper = new PagerSnapHelper();
        this.snapHelper.attachToRecyclerView(this);
    }

    public void a(@NonNull List<cm> list, int i) {
        WindowManager windowManager = (WindowManager) getContext().getSystemService("window");
        Display defaultDisplay = windowManager != null ? windowManager.getDefaultDisplay() : null;
        Point point = new Point();
        if (defaultDisplay != null) {
            defaultDisplay.getSize(point);
        }
        a(list, i, point.x, point.y);
        setLayoutManager(this.nc);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void setBanners(@NonNull List<cm> list) {
        this.nd = list;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull List<cm> list, int i, int i2, int i3) {
        this.nd = list;
        if (!list.isEmpty()) {
            cm cmVar = (cm) list.get(0);
            if (i2 > i3) {
                ImageData optimalLandscapeImage = cmVar.getOptimalLandscapeImage();
                if (optimalLandscapeImage != null) {
                    this.nc.n(optimalLandscapeImage.getWidth(), optimalLandscapeImage.getHeight());
                }
            } else {
                ImageData optimalPortraitImage = cmVar.getOptimalPortraitImage();
                if (optimalPortraitImage != null) {
                    this.nc.n(optimalPortraitImage.getWidth(), optimalPortraitImage.getHeight());
                }
            }
            b bVar = new b(list, i, getResources());
            bVar.setClickListener(this.cardClickListener);
            super.setAdapter(bVar);
            if (this.ne != null) {
                this.ne.a(0, (cm) list.get(0));
            }
        }
    }

    public void setSliderCardListener(@Nullable c cVar) {
        this.ne = cVar;
    }

    public void onScrollStateChanged(int i) {
        super.onScrollStateChanged(i);
        this.moving = i != 0;
        if (!this.moving) {
            checkCardChanged();
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (this.nd != null && !this.nd.isEmpty()) {
            cm cmVar = (cm) this.nd.get(0);
            if (configuration.orientation == 2) {
                ImageData optimalLandscapeImage = cmVar.getOptimalLandscapeImage();
                if (optimalLandscapeImage != null) {
                    this.nc.n(optimalLandscapeImage.getWidth(), optimalLandscapeImage.getHeight());
                }
            } else {
                ImageData optimalPortraitImage = cmVar.getOptimalPortraitImage();
                if (optimalPortraitImage != null) {
                    this.nc.n(optimalPortraitImage.getWidth(), optimalPortraitImage.getHeight());
                }
            }
        }
        super.onConfigurationChanged(configuration);
        Adapter adapter = getAdapter();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        postDelayed(new Runnable() {
            public void run() {
                ho.this.nc.scrollToPositionWithOffset(ho.this.displayedCardNum, ho.this.nc.er());
            }
        }, 100);
    }

    private void checkCardChanged() {
        int findFirstCompletelyVisibleItemPosition = this.nc.findFirstCompletelyVisibleItemPosition();
        if (findFirstCompletelyVisibleItemPosition >= 0 && this.displayedCardNum != findFirstCompletelyVisibleItemPosition) {
            this.displayedCardNum = findFirstCompletelyVisibleItemPosition;
            if (!(this.ne == null || this.nd == null)) {
                this.ne.a(this.displayedCardNum, (cm) this.nd.get(this.displayedCardNum));
            }
        }
    }
}
