package com.my.target;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.Point;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.facebook.places.model.PlaceFields;
import com.ironsource.sdk.constants.Constants;
import com.tapjoy.TapjoyConstants;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.TimeZone;

/* compiled from: DeviceParamsDataProvider */
public final class fd extends ff {
    private float density = 0.0f;
    private boolean gH = false;
    @NonNull
    private String gI = "";
    @NonNull
    private String gJ = "";
    @NonNull
    private String gK = "";
    @NonNull
    private String gL = "";
    @NonNull
    private String gM = "";
    @NonNull
    private String gN = "";
    @NonNull
    private String gO = "";
    @NonNull
    private String gP = "";
    @NonNull
    private String gQ = "";
    @NonNull
    private String gR = "";
    @NonNull
    private String gS = "";
    private int gT = 0;
    @NonNull
    private String gU = "";
    @NonNull
    private String gV = "";
    @NonNull
    private String gW = "";
    private int height = 0;
    @NonNull
    private String timezone = "";
    private int width = 0;

    /* JADX WARNING: Can't wrap try/catch for region: R(18:6|7|8|9|10|11|(1:13)(1:14)|15|16|(2:18|(1:20))|21|(4:23|(1:25)|26|(1:31)(1:30))|32|(2:35|33)|42|36|37|38) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x005c */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01d6 A[LOOP:0: B:33:0x01d0->B:35:0x01d6, LOOP_END] */
    @WorkerThread
    @SuppressLint({"HardwareIds"})
    public synchronized void collectData(@NonNull Context context) {
        if (!this.gH) {
            ah.a("collect application info...");
            ai.b(new Runnable() {
                public void run() {
                    fd.this.addParam("rooted", String.valueOf(fd.this.dL() ? 1 : 0));
                }
            });
            this.gI = Build.DEVICE;
            this.gO = Build.MANUFACTURER;
            this.gP = Build.MODEL;
            this.gK = VERSION.RELEASE;
            this.gL = context.getPackageName();
            this.gQ = Locale.getDefault().getLanguage();
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(this.gL, 0);
            this.gM = packageInfo.versionName;
            if (VERSION.SDK_INT >= 28) {
                this.gN = Long.toString(packageInfo.getLongVersionCode());
            } else {
                this.gN = Integer.toString(packageInfo.versionCode);
            }
            ContentResolver contentResolver = context.getContentResolver();
            if (contentResolver != null) {
                this.gJ = Secure.getString(contentResolver, TapjoyConstants.TJC_ANDROID_ID);
                if (this.gJ == null) {
                    this.gJ = "";
                }
            }
            this.gR = context.getResources().getConfiguration().locale.getLanguage();
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                this.gV = telephonyManager.getNetworkOperatorName();
                if (telephonyManager.getSimState() == 5) {
                    this.gW = telephonyManager.getSimOperator();
                }
                String networkOperator = telephonyManager.getNetworkOperator();
                if (TextUtils.isEmpty(networkOperator) || networkOperator.length() <= 3) {
                    this.gU = networkOperator;
                } else {
                    this.gU = networkOperator.substring(3);
                    this.gS = networkOperator.substring(0, 3);
                }
            }
            w(context);
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            this.gT = displayMetrics.densityDpi;
            this.density = displayMetrics.density;
            TimeZone timeZone = TimeZone.getDefault();
            StringBuilder sb = new StringBuilder();
            sb.append(timeZone.getDisplayName(false, 0));
            sb.append(" ");
            sb.append(timeZone.getID());
            this.timezone = sb.toString();
            addParam(TapjoyConstants.TJC_ANDROID_ID, this.gJ);
            addParam("device", this.gI);
            addParam("os", Constants.JAVASCRIPT_INTERFACE_NAME);
            addParam("manufacture", this.gO);
            addParam("osver", this.gK);
            addParam("app", this.gL);
            addParam("appver", this.gM);
            addParam("appbuild", this.gN);
            addParam("lang", this.gQ);
            addParam("app_lang", this.gR);
            addParam("sim_loc", this.gS);
            addParam("euname", this.gP);
            StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(this.width);
            addParam("w", sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("");
            sb3.append(this.height);
            addParam("h", sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append("");
            sb4.append(this.gT);
            addParam("dpi", sb4.toString());
            StringBuilder sb5 = new StringBuilder();
            sb5.append("");
            sb5.append(this.density);
            addParam("density", sb5.toString());
            addParam("operator_id", this.gU);
            addParam("operator_name", this.gV);
            addParam("sim_operator_id", this.gW);
            addParam(TapjoyConstants.TJC_DEVICE_TIMEZONE, this.timezone);
            for (Entry entry : getMap().entrySet()) {
                StringBuilder sb6 = new StringBuilder();
                sb6.append((String) entry.getKey());
                sb6.append(" = ");
                sb6.append((String) entry.getValue());
                ah.a(sb6.toString());
            }
            this.gH = true;
            ah.a("collected");
        }
    }

    private void w(@NonNull Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            Point point = new Point();
            if (VERSION.SDK_INT >= 17) {
                defaultDisplay.getRealSize(point);
            } else {
                defaultDisplay.getSize(point);
            }
            this.width = point.x;
            this.height = point.y;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00be, code lost:
        if (r8 != null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00bc */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00b9 A[SYNTHETIC, Splitter:B:41:0x00b9] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00c4 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:49:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00d0 A[SYNTHETIC, Splitter:B:59:0x00d0] */
    public boolean dL() {
        Process process;
        BufferedReader bufferedReader;
        Throwable th;
        String str = Build.TAGS;
        boolean z = str != null && str.contains("test-keys");
        if (!z) {
            String[] strArr = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (new File(strArr[i]).exists()) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
        }
        if (z) {
            return z;
        }
        Runtime runtime = Runtime.getRuntime();
        for (String exec : new String[]{"/system/xbin/which su", "/system/bin/which su", "which su"}) {
            try {
                process = runtime.exec(exec);
                try {
                    bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    try {
                        StringBuilder sb = new StringBuilder();
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                break;
                            }
                            sb.append(readLine);
                        }
                        process.destroy();
                        if (!TextUtils.isEmpty(sb.toString())) {
                            bufferedReader.close();
                            if (process != null) {
                                try {
                                    process.destroy();
                                } catch (Exception unused) {
                                }
                            }
                            return true;
                        }
                    } catch (Exception unused2) {
                    } catch (Throwable th2) {
                        th = th2;
                        if (bufferedReader != null) {
                        }
                        try {
                            throw th;
                        } catch (Exception unused3) {
                        } catch (Throwable th3) {
                        }
                    }
                } catch (Exception unused4) {
                    bufferedReader = null;
                } catch (Throwable th4) {
                    Throwable th5 = th4;
                    bufferedReader = null;
                    th = th5;
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                    throw th;
                }
                try {
                    bufferedReader.close();
                } catch (Exception unused5) {
                } catch (Throwable th32) {
                }
                if (process == null) {
                }
            } catch (Exception unused6) {
                process = null;
                if (process == null) {
                }
                process.destroy();
            } catch (Throwable th6) {
                th = th6;
                process = null;
                if (process != null) {
                    try {
                        process.destroy();
                    } catch (Exception unused7) {
                    }
                }
                throw th;
            }
            try {
                process.destroy();
            } catch (Exception unused8) {
            }
        }
        return z;
    }
}
