package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: MraidOrientation */
public class bw {
    private final int cx;
    @NonNull
    private final String cy;

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003d  */
    @Nullable
    public static bw p(@NonNull String str) {
        char c;
        int hashCode = str.hashCode();
        int i = 0;
        if (hashCode != 3387192) {
            if (hashCode != 729267099) {
                if (hashCode == 1430647483 && str.equals("landscape")) {
                    c = 2;
                    switch (c) {
                        case 0:
                            i = -1;
                            break;
                        case 1:
                            i = 1;
                            break;
                        case 2:
                            break;
                        default:
                            return null;
                    }
                    return new bw(str, i);
                }
            } else if (str.equals("portrait")) {
                c = 1;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
                return new bw(str, i);
            }
        } else if (str.equals("none")) {
            c = 0;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
            }
            return new bw(str, i);
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }
        return new bw(str, i);
    }

    public static bw aO() {
        return new bw();
    }

    private bw() {
        this.cx = -1;
        this.cy = "none";
    }

    private bw(@NonNull String str, int i) {
        this.cy = str;
        this.cx = i;
    }

    public String toString() {
        return this.cy;
    }

    public int aP() {
        return this.cx;
    }
}
