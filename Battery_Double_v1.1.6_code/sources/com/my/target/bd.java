package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.AppodealNetworks;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.my.target.fa.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: StandardAdEngine */
public class bd implements ap {
    @NonNull
    private final ArrayList<dg> aV = new ArrayList<>();
    @NonNull
    private final a adConfig;
    /* access modifiers changed from: private */
    @NonNull
    public final ViewGroup bK;
    @NonNull
    private final cs bL;
    @NonNull
    private final com.my.target.fb.a bM = new a();
    @Nullable
    private fb bN;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.ap.a bz;
    @NonNull
    private final Context context;
    @NonNull
    private final dd section;

    /* compiled from: StandardAdEngine */
    class a implements com.my.target.fb.a {
        a() {
        }

        public void a(@NonNull ch chVar, @Nullable String str) {
            if (bd.this.bz != null) {
                bd.this.bz.onClick();
            }
            hr et = hr.et();
            if (TextUtils.isEmpty(str)) {
                et.b(chVar, bd.this.bK.getContext());
            } else {
                et.c(chVar, str, bd.this.bK.getContext());
            }
        }

        public void a(@NonNull ch chVar) {
            ib.a((List<dh>) chVar.getStatHolder().N("playbackStarted"), bd.this.bK.getContext());
            if (bd.this.bz != null) {
                bd.this.bz.ad();
            }
        }
    }

    /* compiled from: StandardAdEngine */
    static class b implements c {
        private bd bP;

        public b(bd bdVar) {
            this.bP = bdVar;
        }

        public void ac() {
            this.bP.ac();
        }

        public void e(@NonNull String str) {
            this.bP.e(str);
        }

        public void ae() {
            this.bP.ae();
        }

        public void af() {
            this.bP.af();
        }

        public void a(@NonNull String str, @NonNull cs csVar, @NonNull Context context) {
            this.bP.a(str, csVar, context);
        }

        public void a(float f, float f2, @NonNull cs csVar, @NonNull Context context) {
            this.bP.a(f, f2, context);
        }
    }

    @Nullable
    public String aa() {
        return "myTarget";
    }

    @NonNull
    public static bd a(@NonNull ViewGroup viewGroup, @NonNull cs csVar, @NonNull dd ddVar, @NonNull a aVar) {
        return new bd(viewGroup, csVar, ddVar, aVar);
    }

    public void a(@Nullable com.my.target.ap.a aVar) {
        this.bz = aVar;
    }

    private bd(@NonNull ViewGroup viewGroup, @NonNull cs csVar, @NonNull dd ddVar, @NonNull a aVar) {
        this.bK = viewGroup;
        this.bL = csVar;
        this.section = ddVar;
        this.adConfig = aVar;
        this.context = viewGroup.getContext();
        this.aV.addAll(csVar.getStatHolder().cv());
    }

    public void prepare() {
        if (AppodealNetworks.MRAID.equals(this.bL.getType())) {
            ax();
        } else {
            ay();
        }
    }

    public void destroy() {
        if (this.bN != null) {
            this.bN.destroy();
            this.bN = null;
        }
    }

    public void start() {
        if (this.bN != null) {
            this.bN.start();
        }
    }

    public void resume() {
        if (this.bN != null) {
            this.bN.resume();
        }
    }

    public void pause() {
        if (this.bN != null) {
            this.bN.pause();
        }
    }

    public void stop() {
        if (this.bN != null) {
            this.bN.stop();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(float f, float f2, @NonNull Context context2) {
        if (!this.aV.isEmpty()) {
            float f3 = f2 - f;
            ArrayList arrayList = new ArrayList();
            Iterator it = this.aV.iterator();
            while (it.hasNext()) {
                dg dgVar = (dg) it.next();
                float cq = dgVar.cq();
                if (cq < 0.0f && dgVar.cr() >= 0.0f) {
                    cq = (f2 / 100.0f) * dgVar.cr();
                }
                if (cq >= 0.0f && cq <= f3) {
                    arrayList.add(dgVar);
                    it.remove();
                }
            }
            ib.a((List<dh>) arrayList, context2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, cs csVar, Context context2) {
        ib.a((List<dh>) csVar.getStatHolder().N(str), context2);
    }

    private void ax() {
        fa faVar;
        String format = this.adConfig.getFormat();
        if (this.bN instanceof fa) {
            faVar = (fa) this.bN;
        } else {
            if (this.bN != null) {
                this.bN.a(null);
                this.bN.destroy();
            }
            fa f = fa.f(this.bK);
            f.a(this.bM);
            this.bN = f;
            a(f.dF(), format);
            faVar = f;
        }
        faVar.a((c) new b(this));
        faVar.a(this.bL);
    }

    private void ay() {
        fc fcVar;
        String format = this.adConfig.getFormat();
        if (this.bN instanceof fc) {
            fcVar = (fc) this.bN;
        } else {
            if (this.bN != null) {
                this.bN.a(null);
                this.bN.destroy();
            }
            fc a2 = fc.a(format, this.section, this.context);
            a2.a(this.bM);
            this.bN = a2;
            a(a2.dF(), format);
            fcVar = a2;
        }
        fcVar.a((com.my.target.fc.a) new com.my.target.fc.a() {
            public void ac() {
                if (bd.this.bz != null) {
                    bd.this.bz.ac();
                }
            }

            public void e(@NonNull String str) {
                if (bd.this.bz != null) {
                    bd.this.bz.e(str);
                }
            }
        });
        fcVar.a(this.bL);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005e  */
    private void a(@NonNull fx fxVar, @NonNull String str) {
        char c;
        ic P = ic.P(this.bK.getContext());
        int hashCode = str.hashCode();
        if (hashCode != -1476994234) {
            if (hashCode == -1177968780 && str.equals("standard_728x90")) {
                c = 1;
                switch (c) {
                    case 0:
                        fxVar.g(P.M(300), P.M(250));
                        break;
                    case 1:
                        fxVar.g(P.M(728), P.M(90));
                        break;
                    default:
                        fxVar.g(P.M(ModuleDescriptor.MODULE_VERSION), P.M(50));
                        fxVar.setFlexibleWidth(true);
                        fxVar.setMaxWidth(P.M(640));
                        break;
                }
                LayoutParams layoutParams = new LayoutParams(-2, -2);
                layoutParams.addRule(13);
                fxVar.setLayoutParams(layoutParams);
                this.bK.removeAllViews();
                this.bK.addView(fxVar);
            }
        } else if (str.equals("standard_300x250")) {
            c = 0;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
            }
            LayoutParams layoutParams2 = new LayoutParams(-2, -2);
            layoutParams2.addRule(13);
            fxVar.setLayoutParams(layoutParams2);
            this.bK.removeAllViews();
            this.bK.addView(fxVar);
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
        }
        LayoutParams layoutParams22 = new LayoutParams(-2, -2);
        layoutParams22.addRule(13);
        fxVar.setLayoutParams(layoutParams22);
        this.bK.removeAllViews();
        this.bK.addView(fxVar);
    }

    /* access modifiers changed from: private */
    public void af() {
        if (this.bz != null) {
            this.bz.af();
        }
    }

    /* access modifiers changed from: private */
    public void ae() {
        if (this.bz != null) {
            this.bz.ae();
        }
    }

    /* access modifiers changed from: private */
    public void e(@NonNull String str) {
        if (this.bz != null) {
            this.bz.e(str);
        }
    }

    /* access modifiers changed from: private */
    public void ac() {
        if (this.bz != null) {
            this.bz.ac();
        }
    }
}
