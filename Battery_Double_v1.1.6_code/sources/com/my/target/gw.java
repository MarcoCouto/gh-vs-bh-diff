package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;

@SuppressLint({"ViewConstructor"})
/* compiled from: PromoMediaAdView */
public class gw extends ViewGroup {
    @Nullable
    private ig ag;
    private boolean allowReplay = true;
    @NonNull
    private final fs fx;
    @NonNull
    private final ge imageView;
    @NonNull
    private final b kM;
    @NonNull
    private final FrameLayout kN;
    @NonNull
    private final ProgressBar kO;
    @Nullable
    private VideoData kP;
    /* access modifiers changed from: private */
    @Nullable
    public a kQ;
    private int kR;
    private int kS;
    @Nullable
    private Bitmap kT;
    private final boolean ki;
    @NonNull
    private final fz playButton;
    @NonNull
    private final ic uiUtils;
    private final boolean useExoPlayer;

    /* compiled from: PromoMediaAdView */
    public interface a extends OnAudioFocusChangeListener, com.my.target.ig.a {
        void cY();

        void da();

        void db();
    }

    /* compiled from: PromoMediaAdView */
    class b implements OnClickListener {
        private b() {
        }

        public void onClick(View view) {
            if (gw.this.kQ != null) {
                if (!gw.this.isPlaying() && !gw.this.isPaused()) {
                    gw.this.kQ.cY();
                } else if (!gw.this.isPaused()) {
                    gw.this.kQ.da();
                } else {
                    gw.this.kQ.db();
                }
            }
        }
    }

    public gw(@NonNull Context context, @NonNull ic icVar, boolean z, boolean z2) {
        super(context);
        this.uiUtils = icVar;
        this.ki = z;
        this.useExoPlayer = z2;
        this.imageView = new ge(context);
        this.playButton = new fz(context);
        this.kO = new ProgressBar(context, null, 16842874);
        this.kN = new FrameLayout(context);
        ic.a(this.kN, 0, 868608760);
        this.fx = new fs(context);
        this.kM = new b();
    }

    @NonNull
    public FrameLayout getClickableLayout() {
        return this.kN;
    }

    @Nullable
    public ig getVideoPlayer() {
        return this.ag;
    }

    public void initView() {
        ic.a((View) this.playButton, "play_button");
        ic.a((View) this.imageView, "media_image");
        ic.a((View) this.fx, "video_texture");
        this.imageView.setScaleType(ScaleType.CENTER_INSIDE);
        this.imageView.setAdjustViewBounds(true);
        addView(this.fx);
        this.kO.setVisibility(8);
        addView(this.imageView);
        addView(this.kO);
        addView(this.playButton);
        addView(this.kN);
    }

    public void a(cn cnVar) {
        d(cnVar);
    }

    public void destroy() {
        if (this.ag != null) {
            this.ag.destroy();
        }
        this.ag = null;
    }

    public void setInterstitialPromoViewListener(@Nullable a aVar) {
        this.kQ = aVar;
        if (this.ag != null) {
            this.ag.a((com.my.target.ig.a) aVar);
        }
    }

    public void ek() {
        this.imageView.setOnClickListener(this.kM);
        this.playButton.setOnClickListener(this.kM);
        setOnClickListener(this.kM);
    }

    public boolean isPlaying() {
        return this.ag != null && this.ag.isPlaying();
    }

    public boolean isPaused() {
        return this.ag != null && this.ag.isPaused();
    }

    public void resume() {
        if (this.ag != null) {
            if (this.kP != null) {
                this.ag.resume();
                this.imageView.setVisibility(8);
            }
            this.playButton.setVisibility(8);
        }
    }

    public void pause() {
        if (this.ag != null) {
            this.ag.pause();
            this.imageView.setVisibility(0);
            Bitmap screenShot = this.fx.getScreenShot();
            if (screenShot != null && this.ag.isStarted()) {
                this.imageView.setImageBitmap(screenShot);
            }
            if (this.allowReplay) {
                this.playButton.setVisibility(0);
            }
        }
    }

    public void G(int i) {
        if (this.ag != null) {
            switch (i) {
                case 0:
                    this.ag.M();
                    return;
                case 1:
                    this.ag.L();
                    return;
                default:
                    this.ag.cU();
                    return;
            }
        }
    }

    public void ei() {
        this.imageView.setVisibility(8);
        this.kO.setVisibility(8);
    }

    public void a(@NonNull cn cnVar, int i) {
        if (cnVar.getVideoBanner() != null) {
            b(cnVar, i);
        } else {
            d(cnVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void B(boolean z) {
        if (this.ag != null) {
            this.ag.stop();
        }
        this.kO.setVisibility(8);
        this.imageView.setVisibility(0);
        this.imageView.setImageBitmap(this.kT);
        this.allowReplay = z;
        if (z) {
            this.playButton.setVisibility(0);
            return;
        }
        this.imageView.setOnClickListener(null);
        this.playButton.setOnClickListener(null);
        setOnClickListener(null);
    }

    /* access modifiers changed from: 0000 */
    public void dm() {
        this.playButton.setVisibility(8);
        this.kO.setVisibility(0);
        if (this.kP != null && this.ag != null) {
            this.ag.a((com.my.target.ig.a) this.kQ);
            this.ag.a(this.fx);
            this.ag.a(this.kP, this.fx.getContext());
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (this.kR == 0 || this.kS == 0) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(0, 1073741824), MeasureSpec.makeMeasureSpec(0, 1073741824));
            return;
        }
        if (mode2 == 0 && size2 == 0) {
            size2 = this.kR;
            size = this.kS;
            mode = Integer.MIN_VALUE;
            mode2 = Integer.MIN_VALUE;
        }
        if (size2 == 0 || mode2 == 0) {
            size2 = (int) ((((float) size) / ((float) this.kS)) * ((float) this.kR));
        }
        if (size == 0 || mode == 0) {
            size = (int) ((((float) size2) / ((float) this.kR)) * ((float) this.kS));
        }
        float f = ((float) this.kS) / ((float) this.kR);
        float f2 = ((float) size) / f;
        float f3 = (float) size2;
        if (f2 > f3) {
            size = (int) (f * f3);
        } else {
            size2 = (int) f2;
        }
        for (int i3 = 0; i3 < getChildCount(); i3++) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() != 8) {
                int i4 = (childAt == this.imageView || childAt == this.kN || childAt == this.fx) ? 1073741824 : Integer.MIN_VALUE;
                childAt.measure(MeasureSpec.makeMeasureSpec(size, i4), MeasureSpec.makeMeasureSpec(size2, i4));
            }
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        for (int i5 = 0; i5 < getChildCount(); i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i6 = ((i3 - i) - measuredWidth) / 2;
                int i7 = ((i4 - i2) - measuredHeight) / 2;
                childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
            }
        }
    }

    private void b(@NonNull cn cnVar, int i) {
        int i2;
        this.kN.setVisibility(8);
        co videoBanner = cnVar.getVideoBanner();
        if (videoBanner != null) {
            this.kP = (VideoData) videoBanner.getMediaData();
            if (this.kP != null) {
                if (!this.useExoPlayer || !hw.ex()) {
                    this.ag = ih.eI();
                } else {
                    this.ag = ii.R(getContext());
                }
                this.ag.a((com.my.target.ig.a) this.kQ);
                this.kS = this.kP.getWidth();
                this.kR = this.kP.getHeight();
                ImageData preview = videoBanner.getPreview();
                if (preview != null) {
                    this.kT = preview.getData();
                    if (this.kS <= 0 || this.kR <= 0) {
                        this.kS = preview.getWidth();
                        this.kR = preview.getHeight();
                    }
                    this.imageView.setImageBitmap(this.kT);
                } else {
                    ImageData image = cnVar.getImage();
                    if (image != null) {
                        if (this.kS <= 0 || this.kR <= 0) {
                            this.kS = image.getWidth();
                            this.kR = image.getHeight();
                        }
                        this.kT = image.getData();
                        this.imageView.setImageBitmap(this.kT);
                    }
                }
                if (i != 1) {
                    if (this.ki) {
                        i2 = this.uiUtils.M(IronSourceConstants.USING_CACHE_FOR_INIT_EVENT);
                    } else {
                        i2 = this.uiUtils.M(96);
                    }
                    this.playButton.a(fm.E(i2), false);
                }
            }
        }
    }

    private void d(@NonNull cn cnVar) {
        this.kN.setVisibility(0);
        setOnClickListener(null);
        this.playButton.setVisibility(8);
        ImageData image = cnVar.getImage();
        if (image != null && image.getData() != null) {
            this.kS = image.getWidth();
            this.kR = image.getHeight();
            if (this.kS == 0 || this.kR == 0) {
                this.kS = image.getData().getWidth();
                this.kR = image.getData().getHeight();
            }
            this.imageView.setImageBitmap(image.getData());
            this.imageView.setClickable(false);
        }
    }
}
