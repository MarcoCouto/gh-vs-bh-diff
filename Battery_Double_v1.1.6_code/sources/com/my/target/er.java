package com.my.target;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.exoplayer2.util.MimeTypes;
import com.my.target.ew.b;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: InterstitialMediaPresenter */
public class er {
    @NonNull
    private final ie N;
    /* access modifiers changed from: private */
    public boolean ae;
    /* access modifiers changed from: private */
    public boolean allowClose;
    /* access modifiers changed from: private */
    public float duration;
    /* access modifiers changed from: private */
    @Nullable
    public b eO;
    @NonNull
    private final a fc;
    /* access modifiers changed from: private */
    @NonNull
    public final gs fd;
    /* access modifiers changed from: private */
    public boolean fe = true;
    @Nullable
    private Set<dg> ff;
    /* access modifiers changed from: private */
    public boolean fg;
    /* access modifiers changed from: private */
    public boolean fh;
    /* access modifiers changed from: private */
    public boolean fi;
    /* access modifiers changed from: private */
    @NonNull
    public final co videoBanner;

    /* compiled from: InterstitialMediaPresenter */
    public class a implements com.my.target.gw.a {
        public void A() {
        }

        public void B() {
        }

        public void x() {
        }

        public a() {
        }

        public void cY() {
            if (!er.this.ae) {
                er.this.j(er.this.fd.getView().getContext());
            }
            er.this.fd.play();
        }

        public void cZ() {
            if (!er.this.ae) {
                er.this.M();
                ib.a((List<dh>) er.this.videoBanner.getStatHolder().N("volumeOff"), er.this.fd.getView().getContext());
                er.this.ae = true;
                return;
            }
            er.this.cU();
            ib.a((List<dh>) er.this.videoBanner.getStatHolder().N("volumeOn"), er.this.fd.getView().getContext());
            er.this.ae = false;
        }

        public void da() {
            er.this.i(er.this.fd.getView().getContext());
            ib.a((List<dh>) er.this.videoBanner.getStatHolder().N("playbackPaused"), er.this.fd.getView().getContext());
            er.this.fd.pause();
        }

        public void db() {
            ib.a((List<dh>) er.this.videoBanner.getStatHolder().N("playbackResumed"), er.this.fd.getView().getContext());
            er.this.fd.resume();
            if (er.this.ae) {
                er.this.M();
            } else {
                er.this.cU();
            }
        }

        public void e(float f) {
            er.this.fd.A(f <= 0.0f);
        }

        public void y() {
            if (er.this.allowClose && er.this.videoBanner.getAllowCloseDelay() == 0.0f) {
                er.this.fd.ej();
            }
            er.this.fd.ei();
        }

        public void z() {
            if (er.this.fg) {
                er.this.fd.pause();
            }
        }

        public void a(float f, float f2) {
            er.this.fd.setTimeChanged(f);
            er.this.fi = false;
            if (er.this.fe) {
                er.this.cX();
                ib.a((List<dh>) er.this.videoBanner.getStatHolder().N("playbackStarted"), er.this.fd.getView().getContext());
                er.this.k(0.0f);
                er.this.fe = false;
            }
            if (!er.this.fh) {
                er.this.fh = true;
            }
            if (er.this.allowClose && er.this.videoBanner.isAutoPlay() && er.this.videoBanner.getAllowCloseDelay() <= f) {
                er.this.fd.ej();
            }
            if (f <= er.this.duration) {
                if (f != 0.0f) {
                    er.this.k(f);
                }
                if (f == er.this.duration) {
                    C();
                    return;
                }
                return;
            }
            a(er.this.duration, er.this.duration);
        }

        public void d(String str) {
            StringBuilder sb = new StringBuilder();
            sb.append("Video playing error: ");
            sb.append(str);
            ah.a(sb.toString());
            er.this.cV();
            if (er.this.eO != null) {
                er.this.eO.al();
            }
        }

        public void C() {
            if (!er.this.fi) {
                er.this.fi = true;
                ah.a("Video playing complete:");
                er.this.cV();
                if (er.this.eO != null) {
                    er.this.eO.C();
                }
                er.this.fd.ej();
                er.this.fd.finish();
            }
        }

        public void onAudioFocusChange(final int i) {
            boolean z = VERSION.SDK_INT >= 23 ? Looper.getMainLooper().isCurrentThread() : Thread.currentThread() == Looper.getMainLooper().getThread();
            if (z) {
                er.this.y(i);
            } else {
                ai.c(new Runnable() {
                    public void run() {
                        er.this.y(i);
                    }
                });
            }
        }
    }

    @NonNull
    public static er a(@NonNull co coVar, @NonNull gs gsVar) {
        return new er(coVar, gsVar);
    }

    private er(@NonNull co coVar, @NonNull gs gsVar) {
        this.videoBanner = coVar;
        this.fc = new a();
        this.fd = gsVar;
        gsVar.setMediaListener(this.fc);
        this.N = ie.c(coVar.getStatHolder());
        this.N.setView(gsVar.getPromoMediaView());
    }

    public void a(@NonNull co coVar, @NonNull Context context) {
        this.allowClose = coVar.isAllowClose();
        if (this.allowClose && coVar.getAllowCloseDelay() == 0.0f && coVar.isAutoPlay()) {
            ah.a("banner is allowed to close");
            this.fd.ej();
        }
        this.duration = coVar.getDuration();
        this.ae = coVar.isAutoMute();
        if (this.ae) {
            this.fd.G(0);
            return;
        }
        if (coVar.isAutoPlay()) {
            j(context);
        }
        this.fd.G(2);
    }

    public void a(cn cnVar) {
        this.fd.ej();
        this.fd.a(cnVar);
    }

    /* access modifiers changed from: private */
    public void M() {
        i(this.fd.getView().getContext());
        this.fd.G(0);
    }

    /* access modifiers changed from: private */
    public void cU() {
        if (this.fd.isPlaying()) {
            j(this.fd.getView().getContext());
        }
        this.fd.G(2);
    }

    private void L() {
        this.fd.G(1);
    }

    /* access modifiers changed from: private */
    public void j(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.requestAudioFocus(this.fc, 3, 2);
        }
    }

    /* access modifiers changed from: private */
    public void i(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.abandonAudioFocus(this.fc);
        }
    }

    public void stop() {
        i(this.fd.getView().getContext());
    }

    public void destroy() {
        i(this.fd.getView().getContext());
        this.fd.destroy();
    }

    /* access modifiers changed from: private */
    public void cV() {
        this.fe = true;
        this.fd.ej();
        i(this.fd.getView().getContext());
        this.fd.stop(this.videoBanner.isAllowReplay());
    }

    public void pause() {
        this.fd.pause();
        i(this.fd.getView().getContext());
        if (this.fd.isPlaying() && !this.fd.isPaused()) {
            ib.a((List<dh>) this.videoBanner.getStatHolder().N("playbackPaused"), this.fd.getView().getContext());
        }
    }

    public void cW() {
        this.fd.stop(true);
        i(this.fd.getView().getContext());
        if (this.fh) {
            ib.a((List<dh>) this.videoBanner.getStatHolder().N("closedByUser"), this.fd.getView().getContext());
        }
    }

    public void r(boolean z) {
        this.fg = z;
    }

    public void a(@Nullable b bVar) {
        this.eO = bVar;
    }

    /* access modifiers changed from: private */
    public void cX() {
        if (this.ff != null) {
            this.ff.clear();
        }
        this.ff = this.videoBanner.getStatHolder().cv();
    }

    /* access modifiers changed from: private */
    public void k(float f) {
        this.N.m(f);
        if (this.ff != null && !this.ff.isEmpty()) {
            Iterator it = this.ff.iterator();
            while (it.hasNext()) {
                dg dgVar = (dg) it.next();
                if (dgVar.cq() <= f) {
                    ib.a((dh) dgVar, this.fd.getView().getContext());
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void y(int i) {
        switch (i) {
            case -3:
                ah.a("Audiofocus loss can duck, set volume to 0.3");
                if (!this.ae) {
                    L();
                    return;
                }
                return;
            case -2:
            case -1:
                pause();
                ah.a("Audiofocus loss, pausing");
                return;
            case 1:
            case 2:
            case 4:
                ah.a("Audiofocus gain, unmuting");
                if (!this.ae) {
                    cU();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
