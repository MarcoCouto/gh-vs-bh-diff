package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: OvvStat */
public class df extends dj {
    private boolean ec;

    @NonNull
    public static df L(@NonNull String str) {
        return new df("ovvStat", str);
    }

    private df(@NonNull String str, @NonNull String str2) {
        super(str, str2);
    }

    public boolean cp() {
        return this.ec;
    }

    public void q(boolean z) {
        this.ec = z;
    }
}
