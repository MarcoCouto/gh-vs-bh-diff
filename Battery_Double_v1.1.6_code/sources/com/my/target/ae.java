package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0056b;

/* compiled from: StandardAdFactory */
public final class ae extends b<dd> {
    @Nullable
    private final dd section;

    public interface a extends C0056b {
    }

    /* compiled from: StandardAdFactory */
    static class b implements com.my.target.b.a<dd> {
        public boolean a() {
            return false;
        }

        private b() {
        }

        @NonNull
        public c<dd> b() {
            return af.f();
        }

        @Nullable
        public d<dd> c() {
            return ag.n();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    @NonNull
    public static b<dd> a(@NonNull a aVar) {
        return new ae(aVar, null);
    }

    @NonNull
    public static b<dd> a(@NonNull dd ddVar, @NonNull a aVar) {
        return new ae(aVar, ddVar);
    }

    private ae(@NonNull a aVar, @Nullable dd ddVar) {
        super(new b(), aVar);
        this.section = ddVar;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: h */
    public dd b(@NonNull Context context) {
        if (this.section != null) {
            return (dd) a(this.section, context);
        }
        return (dd) super.b(context);
    }
}
