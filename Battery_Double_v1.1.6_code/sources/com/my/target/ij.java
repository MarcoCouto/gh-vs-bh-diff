package com.my.target;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.DefaultHlsDataSourceFactory;
import com.google.android.exoplayer2.source.hls.HlsDataSourceFactory;
import com.google.android.exoplayer2.source.hls.HlsMediaSource.Factory;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.my.target.common.models.VideoData;

/* compiled from: MyTargetMediaSourceFactory */
public class ij {
    @NonNull
    public static MediaSource b(@NonNull VideoData videoData, @NonNull Context context) {
        String str = (String) videoData.getData();
        if (str != null) {
            return b(Uri.parse(str), context);
        }
        return b(Uri.parse(videoData.getUrl()), context);
    }

    @NonNull
    public static MediaSource b(@NonNull Uri uri, @NonNull Context context) {
        DefaultDataSourceFactory defaultDataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "myTarget"));
        if (Util.inferContentType(uri) == 2) {
            return new Factory((HlsDataSourceFactory) new DefaultHlsDataSourceFactory(defaultDataSourceFactory)).createMediaSource(uri);
        }
        return new ExtractorMediaSource.Factory(defaultDataSourceFactory).createMediaSource(uri);
    }
}
