package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.HashMap;

@SuppressLint({"ViewConstructor"})
/* compiled from: BodyView */
public class gp extends FrameLayout implements OnTouchListener {
    @NonNull
    private final TextView descriptionLabel;
    @NonNull
    private final TextView disclaimerLabel;
    @NonNull
    private final TextView kg;
    @NonNull
    private final LinearLayout kh;
    private final boolean ki;
    @NonNull
    private final HashMap<View, Boolean> kj = new HashMap<>();
    @Nullable
    private OnClickListener kk;
    @Nullable
    private String navigationType;
    @NonNull
    private final LinearLayout ratingLayout;
    @NonNull
    private final gf starsView;
    @NonNull
    private final TextView titleLabel;
    @NonNull
    private final ic uiUtils;
    @NonNull
    private final TextView votesLabel;

    public gp(@NonNull Context context, @NonNull ic icVar, boolean z) {
        super(context);
        this.titleLabel = new TextView(context);
        this.kg = new TextView(context);
        this.descriptionLabel = new TextView(context);
        this.ratingLayout = new LinearLayout(context);
        this.disclaimerLabel = new TextView(context);
        this.starsView = new gf(context);
        this.votesLabel = new TextView(context);
        this.kh = new LinearLayout(context);
        ic.a((View) this.titleLabel, "title_text");
        ic.a((View) this.descriptionLabel, "description_text");
        ic.a((View) this.disclaimerLabel, "disclaimer_text");
        ic.a((View) this.starsView, "stars_view");
        ic.a((View) this.votesLabel, "votes_text");
        this.uiUtils = icVar;
        this.ki = z;
    }

    public void setBanner(@NonNull cn cnVar) {
        this.navigationType = cnVar.getNavigationType();
        this.titleLabel.setText(cnVar.getTitle());
        this.descriptionLabel.setText(cnVar.getDescription());
        this.starsView.setRating(cnVar.getRating());
        this.votesLabel.setText(String.valueOf(cnVar.getVotes()));
        if ("store".equals(cnVar.getNavigationType())) {
            ic.a((View) this.kg, "category_text");
            String category = cnVar.getCategory();
            String subCategory = cnVar.getSubCategory();
            String str = "";
            if (!TextUtils.isEmpty(category)) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(category);
                str = sb.toString();
            }
            if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(subCategory)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(", ");
                str = sb2.toString();
            }
            if (!TextUtils.isEmpty(subCategory)) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(subCategory);
                str = sb3.toString();
            }
            if (!TextUtils.isEmpty(str)) {
                this.kg.setText(str);
                this.kg.setVisibility(0);
            } else {
                this.kg.setVisibility(8);
            }
            this.ratingLayout.setVisibility(0);
            this.ratingLayout.setGravity(16);
            if (cnVar.getRating() > 0.0f) {
                this.starsView.setVisibility(0);
                if (cnVar.getVotes() > 0) {
                    this.votesLabel.setVisibility(0);
                } else {
                    this.votesLabel.setVisibility(8);
                }
            } else {
                this.starsView.setVisibility(8);
                this.votesLabel.setVisibility(8);
            }
            this.kg.setTextColor(-3355444);
        } else {
            ic.a((View) this.kg, "domain_text");
            this.ratingLayout.setVisibility(8);
            this.kg.setText(cnVar.getDomain());
            this.ratingLayout.setVisibility(8);
            this.kg.setTextColor(-16733198);
        }
        if (TextUtils.isEmpty(cnVar.getDisclaimer())) {
            this.disclaimerLabel.setVisibility(8);
        } else {
            this.disclaimerLabel.setVisibility(0);
            this.disclaimerLabel.setText(cnVar.getDisclaimer());
        }
        if (this.ki) {
            this.titleLabel.setTextSize(2, 32.0f);
            this.descriptionLabel.setTextSize(2, 24.0f);
            this.disclaimerLabel.setTextSize(2, 18.0f);
            this.kg.setTextSize(2, 18.0f);
            return;
        }
        this.titleLabel.setTextSize(2, 20.0f);
        this.descriptionLabel.setTextSize(2, 16.0f);
        this.disclaimerLabel.setTextSize(2, 14.0f);
        this.kg.setTextSize(2, 16.0f);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void a(@NonNull ca caVar, @NonNull OnClickListener onClickListener) {
        if (caVar.f456do) {
            setOnClickListener(onClickListener);
            ic.a(this, -1, -3806472);
            return;
        }
        this.kk = onClickListener;
        this.titleLabel.setOnTouchListener(this);
        this.kg.setOnTouchListener(this);
        this.descriptionLabel.setOnTouchListener(this);
        this.starsView.setOnTouchListener(this);
        this.votesLabel.setOnTouchListener(this);
        setOnTouchListener(this);
        this.kj.put(this.titleLabel, Boolean.valueOf(caVar.dc));
        if ("store".equals(this.navigationType)) {
            this.kj.put(this.kg, Boolean.valueOf(caVar.dm));
        } else {
            this.kj.put(this.kg, Boolean.valueOf(caVar.dl));
        }
        this.kj.put(this.descriptionLabel, Boolean.valueOf(caVar.dd));
        this.kj.put(this.starsView, Boolean.valueOf(caVar.dg));
        this.kj.put(this.votesLabel, Boolean.valueOf(caVar.dh));
        this.kj.put(this, Boolean.valueOf(caVar.dn));
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.kj.containsKey(view)) {
            return false;
        }
        if (!((Boolean) this.kj.get(view)).booleanValue()) {
            return true;
        }
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    setBackgroundColor(-3806472);
                    break;
                case 1:
                    setBackgroundColor(-1);
                    if (this.kk != null) {
                        this.kk.onClick(view);
                        break;
                    }
                    break;
            }
        } else {
            setBackgroundColor(-1);
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void z(boolean z) {
        this.kh.setOrientation(1);
        this.kh.setGravity(1);
        this.titleLabel.setGravity(1);
        this.titleLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 1;
        layoutParams.leftMargin = this.uiUtils.M(8);
        layoutParams.rightMargin = this.uiUtils.M(8);
        this.titleLabel.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.gravity = 1;
        this.kg.setLayoutParams(layoutParams2);
        this.kg.setLines(1);
        this.kg.setEllipsize(TruncateAt.MIDDLE);
        this.descriptionLabel.setGravity(1);
        this.descriptionLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        LayoutParams layoutParams3 = new LayoutParams(-2, -2);
        if (z) {
            this.descriptionLabel.setTextSize(2, 12.0f);
            this.descriptionLabel.setLines(2);
            this.descriptionLabel.setEllipsize(TruncateAt.END);
            layoutParams3.topMargin = 0;
            layoutParams3.leftMargin = this.uiUtils.M(4);
            layoutParams3.rightMargin = this.uiUtils.M(4);
        } else {
            this.descriptionLabel.setTextSize(2, 16.0f);
            layoutParams3.topMargin = this.uiUtils.M(8);
            layoutParams3.leftMargin = this.uiUtils.M(16);
            layoutParams3.rightMargin = this.uiUtils.M(16);
        }
        layoutParams3.gravity = 1;
        this.descriptionLabel.setLayoutParams(layoutParams3);
        this.ratingLayout.setOrientation(0);
        LayoutParams layoutParams4 = new LayoutParams(-2, -2);
        layoutParams4.gravity = 1;
        this.ratingLayout.setLayoutParams(layoutParams4);
        LayoutParams layoutParams5 = new LayoutParams(this.uiUtils.M(73), this.uiUtils.M(12));
        layoutParams5.topMargin = this.uiUtils.M(4);
        layoutParams5.rightMargin = this.uiUtils.M(4);
        this.starsView.setLayoutParams(layoutParams5);
        this.votesLabel.setTextColor(-6710887);
        this.votesLabel.setTextSize(2, 14.0f);
        this.disclaimerLabel.setTextColor(-6710887);
        this.disclaimerLabel.setGravity(1);
        LayoutParams layoutParams6 = new LayoutParams(-2, -2);
        layoutParams6.gravity = 1;
        if (z) {
            layoutParams6.leftMargin = this.uiUtils.M(4);
            layoutParams6.rightMargin = this.uiUtils.M(4);
        } else {
            layoutParams6.leftMargin = this.uiUtils.M(16);
            layoutParams6.rightMargin = this.uiUtils.M(16);
        }
        layoutParams6.gravity = 1;
        this.disclaimerLabel.setLayoutParams(layoutParams6);
        LayoutParams layoutParams7 = new LayoutParams(-2, -2);
        layoutParams7.gravity = 17;
        addView(this.kh, layoutParams7);
        this.kh.addView(this.titleLabel);
        this.kh.addView(this.kg);
        this.kh.addView(this.ratingLayout);
        this.kh.addView(this.descriptionLabel);
        this.kh.addView(this.disclaimerLabel);
        this.ratingLayout.addView(this.starsView);
        this.ratingLayout.addView(this.votesLabel);
    }
}
