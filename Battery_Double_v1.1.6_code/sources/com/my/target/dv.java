package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.github.mikephil.charting.utils.Utils;
import com.my.target.common.models.AudioData;
import com.my.target.common.models.ShareButtonData;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: AudioBannerParser */
public class dv {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;
    @NonNull
    private final dw eB;

    public static dv a(@NonNull da daVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new dv(daVar, bzVar, aVar, context2);
    }

    private dv(@NonNull da daVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eA = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.eB = dw.b(bzVar, aVar, context2);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull co<AudioData> coVar) {
        this.eB.a(jSONObject, (ch) coVar);
        if (coVar.getType().equals("statistics")) {
            c(jSONObject, coVar);
            return true;
        }
        float optDouble = (float) jSONObject.optDouble("duration", Utils.DOUBLE_EPSILON);
        if (optDouble <= 0.0f) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to set duration ");
            sb.append(optDouble);
            b("Required field", sb.toString(), coVar.getId());
            return false;
        }
        coVar.setAutoPlay(jSONObject.optBoolean("autoplay", coVar.isAutoPlay()));
        coVar.setHasCtaButton(jSONObject.optBoolean("hasCtaButton", coVar.isHasCtaButton()));
        coVar.setAdText(jSONObject.optString("adText", coVar.getAdText()));
        b(jSONObject, coVar);
        JSONArray optJSONArray = jSONObject.optJSONArray("companionAds");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    ci a = a(optJSONObject, coVar.getId());
                    if (a != null) {
                        coVar.addCompanion(a);
                    }
                }
            }
        }
        JSONArray optJSONArray2 = jSONObject.optJSONArray("shareButtons");
        if (optJSONArray2 != null) {
            int length2 = optJSONArray2.length();
            for (int i2 = 0; i2 < length2; i2++) {
                JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                if (optJSONObject2 != null) {
                    ShareButtonData newData = ShareButtonData.newData();
                    newData.setName(optJSONObject2.optString("name"));
                    newData.setUrl(optJSONObject2.optString("url"));
                    newData.setImageUrl(optJSONObject2.optString("imageUrl"));
                    coVar.addShareButtonData(newData);
                }
            }
        }
        return d(jSONObject, coVar);
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull co<AudioData> coVar) {
        c(jSONObject, coVar);
        Boolean be = this.eA.be();
        if (be != null) {
            coVar.setAllowClose(be.booleanValue());
        } else {
            coVar.setAllowClose(jSONObject.optBoolean("allowClose", coVar.isAllowClose()));
        }
        Boolean bg = this.eA.bg();
        if (bg != null) {
            coVar.setAllowSeek(bg.booleanValue());
        } else {
            coVar.setAllowSeek(jSONObject.optBoolean("allowSeek", coVar.isAllowSeek()));
        }
        Boolean bh = this.eA.bh();
        if (bh != null) {
            coVar.setAllowSkip(bh.booleanValue());
        } else {
            coVar.setAllowSkip(jSONObject.optBoolean("allowSkip", coVar.isAllowSkip()));
        }
        Boolean bi = this.eA.bi();
        if (bi != null) {
            coVar.setAllowTrackChange(bi.booleanValue());
        } else {
            coVar.setAllowTrackChange(jSONObject.optBoolean("allowTrackChange", coVar.isAllowTrackChange()));
        }
        Boolean bf = this.eA.bf();
        if (bf != null) {
            coVar.setAllowPause(bf.booleanValue());
        } else {
            coVar.setAllowPause(jSONObject.optBoolean("hasPause", coVar.isAllowPause()));
        }
        float allowCloseDelay = this.eA.getAllowCloseDelay();
        if (allowCloseDelay >= 0.0f) {
            coVar.setAllowCloseDelay(allowCloseDelay);
        } else {
            coVar.setAllowCloseDelay((float) jSONObject.optDouble("allowCloseDelay", (double) coVar.getAllowCloseDelay()));
        }
    }

    private void c(@NonNull JSONObject jSONObject, @NonNull co<AudioData> coVar) {
        double point = (double) this.eA.getPoint();
        if (point < Utils.DOUBLE_EPSILON) {
            point = jSONObject.optDouble("point");
        }
        if (Double.isNaN(point)) {
            point = -1.0d;
        } else if (point < Utils.DOUBLE_EPSILON) {
            StringBuilder sb = new StringBuilder();
            sb.append("Wrong value ");
            sb.append(point);
            sb.append(" for point");
            b("Bad value", sb.toString(), coVar.getId());
        }
        double pointP = (double) this.eA.getPointP();
        if (pointP < Utils.DOUBLE_EPSILON) {
            pointP = jSONObject.optDouble("pointP");
        }
        if (Double.isNaN(pointP)) {
            pointP = -1.0d;
        } else if (pointP < Utils.DOUBLE_EPSILON) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Wrong value ");
            sb2.append(pointP);
            sb2.append(" for pointP");
            b("Bad value", sb2.toString(), coVar.getId());
        }
        if (point < Utils.DOUBLE_EPSILON && pointP < Utils.DOUBLE_EPSILON) {
            pointP = 50.0d;
            point = -1.0d;
        }
        coVar.setPoint((float) point);
        coVar.setPointP((float) pointP);
    }

    @Nullable
    private ci a(@NonNull JSONObject jSONObject, @NonNull String str) {
        ci newBanner = ci.newBanner();
        this.eB.a(jSONObject, (ch) newBanner);
        if (newBanner.getWidth() == 0 || newBanner.getHeight() == 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to add companion banner with width ");
            sb.append(newBanner.getWidth());
            sb.append(" and height ");
            sb.append(newBanner.getHeight());
            b("Required field", sb.toString(), str);
            return null;
        }
        newBanner.setAssetWidth(jSONObject.optInt("assetWidth"));
        newBanner.setAssetHeight(jSONObject.optInt("assetHeight"));
        newBanner.setExpandedWidth(jSONObject.optInt("expandedWidth"));
        newBanner.setExpandedHeight(jSONObject.optInt("expandedHeight"));
        newBanner.setStaticResource(jSONObject.optString("staticResource"));
        newBanner.setIframeResource(jSONObject.optString("iframeResource"));
        newBanner.setHtmlResource(jSONObject.optString("htmlResource"));
        newBanner.setApiFramework(jSONObject.optString("apiFramework"));
        newBanner.setAdSlotID(jSONObject.optString("adSlotID"));
        String optString = jSONObject.optString("required");
        if (optString != null) {
            if ("all".equals(optString) || "any".equals(optString) || "none".equals(optString)) {
                newBanner.setRequired(optString);
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Wrong companion required attribute:");
                sb2.append(optString);
                b("Bad value", sb2.toString(), str);
            }
        }
        return newBanner;
    }

    private boolean d(@NonNull JSONObject jSONObject, @NonNull co<AudioData> coVar) {
        JSONArray optJSONArray = jSONObject.optJSONArray("mediafiles");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            ah.a("mediafiles array is empty");
            return false;
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                String optString = optJSONObject.optString("src");
                if (!TextUtils.isEmpty(optString)) {
                    AudioData newAudioData = AudioData.newAudioData(optString);
                    newAudioData.setBitrate(optJSONObject.optInt("bitrate"));
                    coVar.setMediaData(newAudioData);
                    return true;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("bad mediafile object, src = ");
                sb.append(optString);
                b("Bad value", sb.toString(), coVar.getId());
            }
        }
        return false;
    }

    private void b(@NonNull String str, @NonNull String str2, @Nullable String str3) {
        dq.P(str).Q(str2).x(this.adConfig.getSlotId()).S(str3).R(this.eA.getUrl()).q(this.context);
    }
}
