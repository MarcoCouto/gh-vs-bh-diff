package com.my.target;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Build.VERSION;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.exoplayer2.util.MimeTypes;
import com.my.target.common.models.VideoData;

/* compiled from: InterstitialPromoMediaPresenter2 */
public class ev implements OnAudioFocusChangeListener, es, com.my.target.ig.a {
    @NonNull
    private final ie N = ie.c(this.videoBanner.getStatHolder());
    @NonNull
    private final ig ag;
    @NonNull
    private final a fw;
    @NonNull
    private final fs fx;
    @NonNull
    private final hy fy;
    private final float fz;
    @NonNull
    private final co<VideoData> videoBanner;

    /* compiled from: InterstitialPromoMediaPresenter2 */
    public interface a {
        void A();

        void B();

        void C();

        void a(float f, float f2);

        void al();

        void dn();

        /* renamed from: do reason: not valid java name */
        void m406do();

        void onVolumeChanged(float f);

        void z();
    }

    public void x() {
    }

    @NonNull
    public static ev a(@NonNull co<VideoData> coVar, @NonNull fs fsVar, @NonNull a aVar, @NonNull ig igVar) {
        return new ev(coVar, fsVar, aVar, igVar);
    }

    private ev(@NonNull co<VideoData> coVar, @NonNull fs fsVar, @NonNull a aVar, @NonNull ig igVar) {
        this.fw = aVar;
        this.fx = fsVar;
        this.ag = igVar;
        this.videoBanner = coVar;
        this.fy = hy.b(this.videoBanner, fsVar.getContext());
        this.N.setView(fsVar);
        this.fz = this.videoBanner.getDuration();
        igVar.a((com.my.target.ig.a) this);
        if (this.videoBanner.isAutoMute()) {
            igVar.setVolume(0.0f);
        } else {
            igVar.setVolume(1.0f);
        }
    }

    public void dc() {
        if (this.videoBanner.isAutoPlay()) {
            this.fw.B();
            dm();
            return;
        }
        this.fw.dn();
    }

    public void dm() {
        VideoData videoData = (VideoData) this.videoBanner.getMediaData();
        this.fy.refresh();
        if (videoData != null) {
            if (!this.ag.isMuted()) {
                j(this.fx.getContext());
            }
            this.ag.a((com.my.target.ig.a) this);
            this.ag.a(this.fx);
            this.ag.a(videoData, this.fx.getContext());
        }
    }

    public void dd() {
        i(this.fx.getContext());
        this.ag.pause();
    }

    public void resume() {
        this.ag.resume();
        if (this.ag.isMuted()) {
            i(this.fx.getContext());
        } else if (this.ag.isPlaying()) {
            j(this.fx.getContext());
        }
    }

    public void dg() {
        this.fy.eA();
        destroy();
    }

    public void destroy() {
        dd();
        this.ag.destroy();
        this.N.destroy();
    }

    public void de() {
        this.ag.de();
        this.fy.H(!this.ag.isMuted());
    }

    public void df() {
        if (this.ag.isPlaying()) {
            dd();
            this.fy.ez();
        } else if (this.ag.getPosition() > 0) {
            resume();
            this.fy.trackResume();
        } else {
            dm();
        }
    }

    public void onAudioFocusChange(final int i) {
        boolean z = VERSION.SDK_INT >= 23 ? Looper.getMainLooper().isCurrentThread() : Thread.currentThread() == Looper.getMainLooper().getThread();
        if (z) {
            y(i);
        } else {
            ai.c(new Runnable() {
                public void run() {
                    ev.this.y(i);
                }
            });
        }
    }

    public void e(float f) {
        this.fw.onVolumeChanged(f);
    }

    public void y() {
        this.fw.m406do();
    }

    public void z() {
        this.fw.z();
    }

    public void A() {
        this.fw.A();
    }

    public void B() {
        this.fw.B();
    }

    public void a(float f, float f2) {
        if (f <= this.fz) {
            if (f != 0.0f) {
                this.fw.a(f, f2);
                this.fy.trackProgress(f);
                this.N.m(f);
            }
            if (f == f2) {
                this.ag.stop();
                C();
                return;
            }
            return;
        }
        a(f2, this.fz);
    }

    public void d(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Video playing error: ");
        sb.append(str);
        ah.a(sb.toString());
        this.fw.al();
        this.fy.eB();
        this.ag.stop();
    }

    public void C() {
        this.fw.C();
        this.ag.stop();
    }

    private void j(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.requestAudioFocus(this, 3, 2);
        }
    }

    private void i(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.abandonAudioFocus(this);
        }
    }

    /* access modifiers changed from: private */
    public void y(int i) {
        switch (i) {
            case -2:
            case -1:
                dd();
                ah.a("Audiofocus loss, pausing");
                return;
            default:
                return;
        }
    }
}
