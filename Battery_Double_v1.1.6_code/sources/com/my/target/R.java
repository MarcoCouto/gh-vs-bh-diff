package com.my.target;

public final class R {

    public static final class id {
        public static final int nativeads_ad_view = 2131296475;
        public static final int nativeads_advertising = 2131296476;
        public static final int nativeads_age_restrictions = 2131296477;
        public static final int nativeads_call_to_action = 2131296478;
        public static final int nativeads_description = 2131296479;
        public static final int nativeads_disclaimer = 2131296480;
        public static final int nativeads_domain = 2131296481;
        public static final int nativeads_icon = 2131296482;
        public static final int nativeads_media_view = 2131296483;
        public static final int nativeads_rating = 2131296484;
        public static final int nativeads_title = 2131296485;
        public static final int nativeads_votes = 2131296486;

        private id() {
        }
    }

    private R() {
    }
}
