package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;
import com.my.target.ef.a;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InterstitialAdResponseParser */
public class p extends c<cy> implements a {
    @Nullable
    private String mraidJs;

    @NonNull
    public static c<cy> f() {
        return new p();
    }

    public static void a(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        bz e = du.a(bzVar, aVar, context).e(jSONObject);
        if (e != null) {
            bzVar.b(e);
        }
    }

    private p() {
    }

    @Nullable
    public cy a(@NonNull String str, @NonNull bz bzVar, @Nullable cy cyVar, @NonNull a aVar, @NonNull Context context) {
        if (c.isVast(str)) {
            return b(str, bzVar, aVar, cyVar, context);
        }
        return a(str, bzVar, aVar, cyVar, context);
    }

    @Nullable
    public cv b(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        cj b = ea.d(bzVar, aVar, context).b(jSONObject, this.mraidJs);
        if (b == null) {
            return null;
        }
        cy bO = cy.bO();
        bO.a(b);
        return bO;
    }

    @Nullable
    private cy a(@NonNull String str, @NonNull bz bzVar, @NonNull a aVar, @Nullable cy cyVar, @NonNull Context context) {
        JSONObject a = a(str, context);
        if (a == null) {
            return cyVar;
        }
        if (cyVar == null) {
            cyVar = cy.bO();
        }
        this.mraidJs = a.optString("mraid.js");
        JSONObject optJSONObject = a.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            if (aVar.isMediationEnabled()) {
                JSONObject optJSONObject2 = a.optJSONObject("mediation");
                if (optJSONObject2 != null) {
                    ct g = ef.a(this, bzVar, aVar, context).g(optJSONObject2);
                    if (g != null) {
                        cyVar.a(g);
                    }
                }
            }
            return cyVar;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return cyVar;
        }
        JSONObject optJSONObject3 = optJSONArray.optJSONObject(0);
        if (optJSONObject3 != null) {
            if ("additionalData".equals(optJSONObject3.optString("type", ""))) {
                a(optJSONObject3, bzVar, aVar, context);
            } else {
                cj b = ea.d(bzVar, aVar, context).b(optJSONObject3, this.mraidJs);
                if (b != null) {
                    cyVar.a(b);
                }
            }
        }
        return cyVar;
    }

    @Nullable
    private cy b(@NonNull String str, @NonNull bz bzVar, @NonNull a aVar, @Nullable cy cyVar, @NonNull Context context) {
        en a = en.a(aVar, bzVar, context);
        a.V(str);
        return !a.cO().isEmpty() ? a(cyVar, a, bzVar) : cyVar;
    }

    @NonNull
    private cy a(@Nullable cy cyVar, @NonNull en<VideoData> enVar, @NonNull bz bzVar) {
        if (cyVar == null) {
            cyVar = cy.bO();
        }
        co coVar = (co) enVar.cO().get(0);
        cn newBanner = cn.newBanner();
        newBanner.setCtaText(coVar.getCtaText());
        newBanner.setVideoBanner(coVar);
        newBanner.setStyle(1);
        newBanner.setTrackingLink(coVar.getTrackingLink());
        Boolean bk = bzVar.bk();
        if (bk != null) {
            newBanner.setDirectLink(bk.booleanValue());
        }
        Boolean bl = bzVar.bl();
        if (bl != null) {
            newBanner.setOpenInBrowser(bl.booleanValue());
        }
        for (dh b : coVar.getStatHolder().N(String.CLICK)) {
            newBanner.getStatHolder().b(b);
        }
        cyVar.d(enVar.bb());
        cyVar.a(newBanner);
        Iterator it = coVar.getCompanionBanners().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ci ciVar = (ci) it.next();
            cj cjVar = null;
            if (ciVar.getHtmlResource() != null) {
                cjVar = cl.fromCompanion(ciVar);
                continue;
            } else if (ciVar.getStaticResource() != null) {
                cjVar = cm.fromCompanion(ciVar);
                continue;
            } else {
                continue;
            }
            if (cjVar != null) {
                newBanner.setEndCard(cjVar);
                break;
            }
        }
        return cyVar;
    }
}
