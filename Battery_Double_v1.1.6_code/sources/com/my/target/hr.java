package com.my.target;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.Window;
import android.widget.FrameLayout;
import com.my.target.common.MyTargetActivity;
import com.my.target.common.MyTargetActivity.ActivityEngine;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.List;
import java.util.WeakHashMap;

/* compiled from: ClickHandler */
public class hr {
    /* access modifiers changed from: private */
    @NonNull
    public static final WeakHashMap<ch, Boolean> no = new WeakHashMap<>();

    /* compiled from: ClickHandler */
    static abstract class a {
        @NonNull
        protected final ch nr;

        /* access modifiers changed from: protected */
        public abstract boolean J(@NonNull Context context);

        @NonNull
        static a b(@NonNull ch chVar) {
            return new b(chVar);
        }

        @NonNull
        static a a(@NonNull String str, @NonNull ch chVar) {
            if (id.am(str)) {
                return new c(str, chVar);
            }
            return new d(str, chVar);
        }

        protected a(@NonNull ch chVar) {
            this.nr = chVar;
        }
    }

    /* compiled from: ClickHandler */
    static class b extends a {
        private b(@NonNull ch chVar) {
            super(chVar);
        }

        /* access modifiers changed from: protected */
        public boolean J(@NonNull Context context) {
            if (!"store".equals(this.nr.getNavigationType())) {
                return false;
            }
            String bundleId = this.nr.getBundleId();
            if (bundleId == null) {
                return false;
            }
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(bundleId);
            if (launchIntentForPackage == null) {
                return false;
            }
            if (a(bundleId, this.nr.getDeeplink(), context)) {
                ib.a((List<dh>) this.nr.getStatHolder().N("deeplinkClick"), context);
                return true;
            } else if (!b(bundleId, this.nr.getUrlscheme(), context) && !a(launchIntentForPackage, context)) {
                return false;
            } else {
                ib.a((List<dh>) this.nr.getStatHolder().N(String.CLICK), context);
                String trackingLink = this.nr.getTrackingLink();
                if (trackingLink != null && !id.am(trackingLink)) {
                    id.ap(trackingLink).Q(context);
                }
                return true;
            }
        }

        private boolean a(@NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (str2 == null) {
                return false;
            }
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                intent.setPackage(str);
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        private boolean b(@NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (str2 == null) {
                return false;
            }
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                intent.setPackage(str);
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        private boolean a(@NonNull Intent intent, @NonNull Context context) {
            try {
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }
    }

    /* compiled from: ClickHandler */
    static class c extends d {
        private c(@NonNull String str, @NonNull ch chVar) {
            super(str, chVar);
        }

        /* access modifiers changed from: protected */
        public boolean J(@NonNull Context context) {
            if (id.an(this.url)) {
                if (j(this.url, context)) {
                    return true;
                }
            } else if (k(this.url, context)) {
                return true;
            }
            return super.J(context);
        }

        private boolean j(@NonNull String str, @NonNull Context context) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        private boolean k(@NonNull String str, @NonNull Context context) {
            try {
                if (this.nr.isUsePlayStoreAction()) {
                    Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage("com.android.vending");
                    if (launchIntentForPackage != null) {
                        launchIntentForPackage.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.activities.LaunchUrlHandlerActivity"));
                        if (!(context instanceof Activity)) {
                            launchIntentForPackage.addFlags(268435456);
                        }
                        launchIntentForPackage.setData(Uri.parse(str));
                        context.startActivity(launchIntentForPackage);
                    }
                } else {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    if (!(context instanceof Activity)) {
                        intent.addFlags(268435456);
                    }
                    context.startActivity(intent);
                }
                return true;
            } catch (Exception unused) {
                return false;
            }
        }
    }

    /* compiled from: ClickHandler */
    static class d extends a {
        @NonNull
        protected final String url;

        private d(@NonNull String str, @NonNull ch chVar) {
            super(chVar);
            this.url = str;
        }

        /* access modifiers changed from: protected */
        public boolean J(@NonNull Context context) {
            if (this.nr.isOpenInBrowser()) {
                return n(this.url, context);
            }
            if (VERSION.SDK_INT >= 18 && m(this.url, context)) {
                return true;
            }
            if ("store".equals(this.nr.getNavigationType()) || (VERSION.SDK_INT >= 28 && !id.ao(this.url))) {
                return n(this.url, context);
            }
            return l(this.url, context);
        }

        private boolean l(@NonNull String str, @NonNull Context context) {
            e.af(str).k(context);
            return true;
        }

        @TargetApi(18)
        private boolean m(@NonNull String str, @NonNull Context context) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                Bundle bundle = new Bundle();
                bundle.putBinder(CustomTabsIntent.EXTRA_SESSION, null);
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                intent.setPackage("com.android.chrome");
                intent.putExtras(bundle);
                context.startActivity(intent);
                return true;
            } catch (ActivityNotFoundException unused) {
                return false;
            }
        }

        private boolean n(@NonNull String str, @NonNull Context context) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }
    }

    /* compiled from: ClickHandler */
    static class e implements ActivityEngine {
        @NonNull
        private final String ns;
        @Nullable
        private gl nt;

        public boolean onActivityOptionsItemSelected(MenuItem menuItem) {
            return false;
        }

        public void onActivityPause() {
        }

        public void onActivityResume() {
        }

        public void onActivityStart() {
        }

        public void onActivityStop() {
        }

        @NonNull
        public static e af(@NonNull String str) {
            return new e(str);
        }

        private e(@NonNull String str) {
            this.ns = str;
        }

        public void k(@NonNull Context context) {
            MyTargetActivity.activityEngine = this;
            Intent intent = new Intent(context, MyTargetActivity.class);
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            context.startActivity(intent);
        }

        public void onActivityCreate(@NonNull final MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
            myTargetActivity.setTheme(16973837);
            if (VERSION.SDK_INT >= 21) {
                Window window = myTargetActivity.getWindow();
                window.addFlags(Integer.MIN_VALUE);
                window.setStatusBarColor(-12232092);
            }
            this.nt = new gl(myTargetActivity);
            frameLayout.addView(this.nt);
            this.nt.dc();
            this.nt.setUrl(this.ns);
            this.nt.setListener(new com.my.target.gl.b() {
                public void aj() {
                    myTargetActivity.finish();
                }
            });
        }

        public void onActivityDestroy() {
            if (this.nt != null) {
                this.nt.destroy();
                this.nt = null;
            }
        }

        public boolean onActivityBackPressed() {
            if (this.nt == null || !this.nt.canGoBack()) {
                return true;
            }
            this.nt.goBack();
            return false;
        }
    }

    @NonNull
    public static hr et() {
        return new hr();
    }

    private hr() {
    }

    public void b(@NonNull ch chVar, @NonNull Context context) {
        c(chVar, chVar.getTrackingLink(), context);
    }

    public void c(@NonNull ch chVar, @Nullable String str, @NonNull Context context) {
        if (!no.containsKey(chVar) && !a.b(chVar).J(context)) {
            if (str != null) {
                a(str, chVar, context);
            }
            ib.a((List<dh>) chVar.getStatHolder().N(String.CLICK), context);
        }
    }

    private void a(@NonNull String str, @NonNull final ch chVar, @NonNull final Context context) {
        if (chVar.isDirectLink() || id.am(str)) {
            b(str, chVar, context);
            return;
        }
        no.put(chVar, Boolean.valueOf(true));
        id.ap(str).a((com.my.target.id.a) new com.my.target.id.a() {
            public void ae(@Nullable String str) {
                if (!TextUtils.isEmpty(str)) {
                    hr.this.b(str, chVar, context);
                }
                hr.no.remove(chVar);
            }
        }).Q(context);
    }

    /* access modifiers changed from: private */
    public void b(@NonNull String str, @NonNull ch chVar, @NonNull Context context) {
        a.a(str, chVar).J(context);
    }
}
