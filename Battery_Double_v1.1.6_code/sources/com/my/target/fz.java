package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;

/* compiled from: IconButton */
public class fz extends View {
    private final float density;
    @NonNull
    private final Paint iu = new Paint();
    @NonNull
    private final ColorFilter iv;
    @Nullable
    private Bitmap iw;
    private int ix;
    private int iy;
    private final int padding;
    @NonNull
    private final Rect rect;

    public fz(@NonNull Context context) {
        super(context);
        this.iu.setFilterBitmap(true);
        this.density = context.getResources().getDisplayMetrics().density;
        this.padding = ic.a(10, context);
        this.rect = new Rect();
        this.iv = new LightingColorFilter(-3355444, 1);
    }

    public int getPadding() {
        return this.padding;
    }

    public void a(@Nullable Bitmap bitmap, boolean z) {
        this.iw = bitmap;
        if (this.iw == null) {
            this.iy = 0;
            this.ix = 0;
        } else if (z) {
            float f = 1.0f;
            if (this.density > 1.0f) {
                f = 2.0f;
            }
            this.iy = (int) ((((float) this.iw.getHeight()) / f) * this.density);
            this.ix = (int) ((((float) this.iw.getWidth()) / f) * this.density);
        } else {
            this.ix = this.iw.getWidth();
            this.iy = this.iw.getHeight();
        }
        setMeasuredDimension(this.ix + (this.padding * 2), this.iy + (this.padding * 2));
        requestLayout();
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    this.iu.setColorFilter(this.iv);
                    invalidate();
                    return true;
                case 1:
                    if (motionEvent.getX() >= 0.0f && motionEvent.getX() <= ((float) getMeasuredWidth()) && motionEvent.getY() >= 0.0f && motionEvent.getY() <= ((float) getMeasuredHeight())) {
                        performClick();
                        break;
                    }
                default:
                    return super.onTouchEvent(motionEvent);
            }
        }
        this.iu.setColorFilter(null);
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.iw != null) {
            this.rect.left = this.padding;
            this.rect.top = this.padding;
            this.rect.right = this.ix + this.padding;
            this.rect.bottom = this.iy + this.padding;
            canvas.drawBitmap(this.iw, null, this.rect, this.iu);
        }
    }
}
