package com.my.target;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.appodeal.ads.AppodealNetworks;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.common.MyTargetActivity;
import com.my.target.ew.b;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: InterstitialAdPromoEngine */
public class av extends as {
    @NonNull
    private final ArrayList<dg> aV = new ArrayList<>();
    @NonNull
    private cn ba;
    @Nullable
    private WeakReference<eu> bb;
    @NonNull
    private final cy section;

    /* compiled from: InterstitialAdPromoEngine */
    public static class a implements b, ex.b, com.my.target.ez.a {
        @NonNull
        private final av bc;

        public void e(@NonNull String str) {
        }

        a(@NonNull av avVar) {
            this.bc = avVar;
        }

        public void b(@Nullable ch chVar, @Nullable String str, @NonNull Context context) {
            if (chVar != null) {
                this.bc.b(chVar, str, context);
            }
        }

        public void aj() {
            this.bc.aj();
        }

        public void a(@NonNull ch chVar, @NonNull Context context) {
            this.bc.a(chVar, context);
        }

        public void a(@NonNull ch chVar, @NonNull String str, @NonNull Context context) {
            this.bc.a(chVar, str, context);
        }

        public void a(@NonNull ch chVar, float f, float f2, @NonNull Context context) {
            this.bc.a(f, f2, context);
        }

        public void al() {
            this.bc.al();
        }

        public void C() {
            this.bc.C();
        }
    }

    @NonNull
    public static av a(@NonNull InterstitialAd interstitialAd, @NonNull cn cnVar, @NonNull cy cyVar) {
        return new av(interstitialAd, cnVar, cyVar);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public eu ak() {
        if (this.bb != null) {
            return (eu) this.bb.get();
        }
        return null;
    }

    private av(@NonNull InterstitialAd interstitialAd, @NonNull cn cnVar, @NonNull cy cyVar) {
        super(interstitialAd);
        this.ba = cnVar;
        this.section = cyVar;
        this.aV.addAll(cnVar.getStatHolder().cv());
    }

    public void a(@NonNull fo foVar, @NonNull FrameLayout frameLayout) {
        super.a(foVar, frameLayout);
        a(this.ba, (ViewGroup) frameLayout);
    }

    public void a(boolean z) {
        super.a(z);
        eu ak = ak();
        if (ak == null) {
            return;
        }
        if (z) {
            ak.resume();
        } else {
            ak.pause();
        }
    }

    public void D() {
        super.D();
        eu ak = ak();
        if (ak != null) {
            ak.destroy();
        }
        if (this.bb != null) {
            this.bb.clear();
            this.bb = null;
        }
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        super.onActivityCreate(myTargetActivity, intent, frameLayout);
        a(this.ba, (ViewGroup) frameLayout);
    }

    public void onActivityPause() {
        super.onActivityPause();
        eu ak = ak();
        if (ak != null) {
            ak.pause();
        }
    }

    public void onActivityResume() {
        super.onActivityResume();
        eu ak = ak();
        if (ak != null) {
            ak.resume();
        }
    }

    public void onActivityStop() {
        super.onActivityStop();
        eu ak = ak();
        if (ak != null) {
            ak.stop();
        }
    }

    public void onActivityDestroy() {
        super.onActivityDestroy();
        if (this.bb != null) {
            eu euVar = (eu) this.bb.get();
            if (euVar != null) {
                View cT = euVar.cT();
                ViewParent parent = cT.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(cT);
                }
                euVar.destroy();
            }
            this.bb.clear();
            this.bb = null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean ai() {
        return this.ba.isAllowBackButton();
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull ch chVar, @Nullable String str, @NonNull Context context) {
        if (ak() != null) {
            hr et = hr.et();
            if (TextUtils.isEmpty(str)) {
                et.b(chVar, context);
            } else {
                et.c(chVar, str, context);
            }
            if (chVar instanceof ck) {
                ib.a((List<dh>) this.ba.getStatHolder().N(String.CLICK), context);
            }
            InterstitialAdListener listener = this.ad.getListener();
            if (listener != null) {
                listener.onClick(this.ad);
            }
            if ((chVar instanceof cn) && ((cn) chVar).isCloseOnClick()) {
                dismiss();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void C() {
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onVideoCompleted(this.ad);
        }
        cj endCard = this.ba.getEndCard();
        ViewParent viewParent = null;
        eu ak = ak();
        if (ak != null) {
            viewParent = ak.cT().getParent();
        }
        if (endCard != null && (viewParent instanceof ViewGroup)) {
            a(endCard, (ViewGroup) viewParent);
        }
    }

    /* access modifiers changed from: 0000 */
    public void aj() {
        dismiss();
    }

    /* access modifiers changed from: 0000 */
    public void al() {
        eu ak = ak();
        if (ak instanceof ew) {
            ((ew) ak).ds();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(float f, float f2, @NonNull Context context) {
        if (!this.aV.isEmpty()) {
            float f3 = f2 - f;
            ArrayList arrayList = new ArrayList();
            Iterator it = this.aV.iterator();
            while (it.hasNext()) {
                dg dgVar = (dg) it.next();
                float cq = dgVar.cq();
                if (cq < 0.0f && dgVar.cr() >= 0.0f) {
                    cq = (f2 / 100.0f) * dgVar.cr();
                }
                if (cq >= 0.0f && cq <= f3) {
                    arrayList.add(dgVar);
                    it.remove();
                }
            }
            ib.a((List<dh>) arrayList, context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(ch chVar, @NonNull String str, @NonNull Context context) {
        ib.a((List<dh>) chVar.getStatHolder().N(str), context);
    }

    /* access modifiers changed from: 0000 */
    public void a(ch chVar, @NonNull Context context) {
        ib.a((List<dh>) chVar.getStatHolder().N("playbackStarted"), context);
    }

    private void a(@NonNull cn cnVar, @NonNull ViewGroup viewGroup) {
        b(cnVar, viewGroup);
        ib.a((List<dh>) this.section.w(Events.AD_IMPRESSION), viewGroup.getContext());
    }

    private void a(@NonNull cj cjVar, @NonNull ViewGroup viewGroup) {
        eu ak = ak();
        if (ak != null) {
            ak.destroy();
        }
        if (cjVar instanceof cl) {
            viewGroup.removeAllViews();
            b(cjVar, viewGroup);
        } else if (cjVar instanceof cm) {
            viewGroup.removeAllViews();
            a((cm) cjVar, viewGroup);
        } else if (cjVar instanceof cn) {
            viewGroup.removeAllViews();
            b((cn) cjVar, viewGroup);
        }
    }

    private void b(@NonNull cj cjVar, @NonNull ViewGroup viewGroup) {
        ez ezVar;
        if (AppodealNetworks.MRAID.equals(cjVar.getType())) {
            ezVar = et.u(viewGroup.getContext());
        } else {
            ezVar = ep.s(viewGroup.getContext());
        }
        this.bb = new WeakReference<>(ezVar);
        ezVar.a(new a(this));
        ezVar.a(this.section, (cl) cjVar);
        viewGroup.addView(ezVar.cT(), new LayoutParams(-1, -1));
    }

    private void a(@NonNull cm cmVar, @NonNull ViewGroup viewGroup) {
        eq t = eq.t(viewGroup.getContext());
        this.bb = new WeakReference<>(t);
        t.a(new a(this));
        t.e(cmVar);
        viewGroup.addView(t.cT(), new LayoutParams(-1, -1));
    }

    private void b(@NonNull cn cnVar, @NonNull ViewGroup viewGroup) {
        eu euVar;
        if (cnVar.getStyle() != 2) {
            ew a2 = ew.a(cnVar, this.ad.isUseExoPlayer(), viewGroup.getContext());
            a2.r(ah());
            a2.b(new a(this));
            euVar = a2;
        } else {
            euVar = ex.a(hd.I(viewGroup.getContext()), cnVar, this.ad.isUseExoPlayer(), new a(this));
        }
        this.bb = new WeakReference<>(euVar);
        viewGroup.addView(euVar.cT(), new LayoutParams(-1, -1));
        ib.a((List<dh>) cnVar.getStatHolder().N("playbackStarted"), viewGroup.getContext());
        this.ba = cnVar;
    }
}
