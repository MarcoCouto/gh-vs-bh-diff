package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import com.my.target.al.b;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import com.my.target.nativeads.views.MediaAdView;
import com.my.target.nativeads.views.PromoCardRecyclerView;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* compiled from: NativeAdViewController */
public class am {
    @Nullable
    private WeakReference<MediaAdView> W;
    private final boolean ak;
    private final boolean al;
    @NonNull
    private final a am;
    @NonNull
    private final cp an;
    @NonNull
    private final hp ao;
    private int ap = 0;
    @Nullable
    private WeakReference<View> aq;
    @Nullable
    private WeakReference<go> ar;
    @Nullable
    private WeakReference<gk> as;
    @Nullable
    private HashSet<WeakReference<View>> at;
    @Nullable
    private al au;
    private boolean av;
    @Nullable
    private Parcelable aw;
    private boolean ax;
    private boolean ay;
    private final boolean useExoPlayer;

    /* compiled from: NativeAdViewController */
    public interface a extends OnClickListener, b, com.my.target.go.a {
    }

    public static am a(@NonNull cp cpVar, @NonNull a aVar, boolean z) {
        return new am(cpVar, aVar, z);
    }

    private am(@NonNull cp cpVar, @NonNull a aVar, boolean z) {
        boolean z2 = false;
        this.am = aVar;
        this.an = cpVar;
        this.ak = cpVar.getNativeAdCards().size() > 0;
        this.useExoPlayer = z && hw.ex() && hw.ey();
        co videoBanner = cpVar.getVideoBanner();
        if (!(videoBanner == null || videoBanner.getMediaData() == null)) {
            z2 = true;
        }
        this.al = z2;
        this.ao = hp.a(cpVar.getAdChoices());
    }

    @Nullable
    public Context getContext() {
        if (this.aq != null) {
            View view = (View) this.aq.get();
            if (view != null) {
                return view.getContext();
            }
        }
        return null;
    }

    public boolean T() {
        return this.ax;
    }

    public int U() {
        return this.ap;
    }

    public void b(boolean z) {
        if (this.au == null) {
            return;
        }
        if (z) {
            this.au.v();
        } else {
            this.au.w();
        }
    }

    public int V() {
        View view = this.aq != null ? (View) this.aq.get() : null;
        if (view == null) {
            return -1;
        }
        if (view.getVisibility() != 0 || view.getParent() == null || view.getAlpha() < 0.5f) {
            return 0;
        }
        Rect rect = new Rect();
        if (view.getGlobalVisibleRect(rect)) {
            double width = (double) (rect.width() * rect.height());
            double width2 = (double) (view.getWidth() * view.getHeight());
            double viewabilitySquare = (double) this.an.getViewabilitySquare();
            Double.isNaN(width2);
            Double.isNaN(viewabilitySquare);
            if (width >= width2 * viewabilitySquare) {
                return 1;
            }
        }
        return 0;
    }

    public void W() {
        if (this.au != null) {
            this.au.unregister();
        }
    }

    public void unregisterView() {
        View view = this.aq != null ? (View) this.aq.get() : null;
        W();
        if (this.ar != null) {
            go goVar = (go) this.ar.get();
            if (goVar != null) {
                goVar.setPromoCardSliderListener(null);
                this.aw = goVar.getState();
                goVar.dispose();
            }
            this.ar = null;
        }
        if (this.W != null) {
            MediaAdView mediaAdView = (MediaAdView) this.W.get();
            if (mediaAdView != null) {
                c(mediaAdView);
            }
            this.W = null;
        }
        X();
        if (this.at != null) {
            Iterator it = this.at.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                if (weakReference != null) {
                    View view2 = (View) weakReference.get();
                    if (view2 != null) {
                        view2.setOnClickListener(null);
                    }
                }
            }
            this.at = null;
        } else if (view != null) {
            d(view);
        }
        if (view != null) {
            this.ao.i(view);
        }
        if (this.aq != null) {
            this.aq.clear();
            this.aq = null;
        }
    }

    public void X() {
        if (this.as != null) {
            gk gkVar = (gk) this.as.get();
            if (gkVar != null) {
                gkVar.setViewabilityListener(null);
            }
            this.as.clear();
            this.as = null;
        }
    }

    @Nullable
    public int[] Y() {
        if (this.ap == 2) {
            if (this.ar != null) {
                go goVar = (go) this.ar.get();
                if (goVar != null) {
                    return goVar.getVisibleCardNumbers();
                }
            }
        } else if (this.ap == 3 && this.W != null) {
            MediaAdView mediaAdView = (MediaAdView) this.W.get();
            if (mediaAdView != null) {
                gn b = b(mediaAdView);
                if (b != null) {
                    return b.getVisibleCardNumbers();
                }
            }
        }
        return null;
    }

    public void a(@NonNull View view, @Nullable List<View> list, @Nullable com.my.target.gk.a aVar, int i) {
        if (list != null) {
            this.at = new HashSet<>();
            for (View view2 : list) {
                if (view2 != null) {
                    this.at.add(new WeakReference(view2));
                    if (view2 instanceof MediaAdView) {
                        this.av = true;
                    } else {
                        view2.setOnClickListener(this.am);
                    }
                }
            }
        }
        this.aq = new WeakReference<>(view);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            gk gkVar = null;
            fq fqVar = null;
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                if (childAt instanceof fq) {
                    fqVar = (fq) childAt;
                } else if (childAt instanceof gk) {
                    gkVar = (gk) childAt;
                }
            }
            a(aVar, gkVar, viewGroup);
            this.ao.a(viewGroup, fqVar, i);
        }
        c(view);
    }

    public boolean Z() {
        if (this.as != null) {
            gk gkVar = (gk) this.as.get();
            if (gkVar != null) {
                return gkVar.ef();
            }
        }
        return false;
    }

    private void a(@Nullable com.my.target.gk.a aVar, @Nullable gk gkVar, @NonNull ViewGroup viewGroup) {
        if (gkVar == null) {
            gkVar = new gk(viewGroup.getContext());
            gkVar.setId(ic.eG());
            ic.a((View) gkVar, "viewability_view");
            try {
                viewGroup.addView(gkVar);
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to add Viewability View: ");
                sb.append(e.getMessage());
                ah.a(sb.toString());
                this.ax = true;
                return;
            }
        }
        gkVar.setViewabilityListener(aVar);
        this.as = new WeakReference<>(gkVar);
    }

    private void c(@NonNull View view) {
        if (view instanceof ViewGroup) {
            a((ViewGroup) view);
        } else if (!(view instanceof fq) && this.at == null) {
            view.setOnClickListener(this.am);
        }
    }

    private void a(@NonNull ViewGroup viewGroup) {
        if (this.ak && (viewGroup instanceof PromoCardRecyclerView)) {
            a((go) (PromoCardRecyclerView) viewGroup);
        } else if (viewGroup instanceof MediaAdView) {
            a((MediaAdView) viewGroup);
        } else {
            if (this.at == null) {
                viewGroup.setOnClickListener(this.am);
            }
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt != null) {
                    c(childAt);
                }
            }
        }
    }

    private void a(@NonNull go goVar) {
        this.ap = 2;
        goVar.setPromoCardSliderListener(this.am);
        if (this.aw != null) {
            goVar.restoreState(this.aw);
        }
        this.ar = new WeakReference<>(goVar);
    }

    private void a(@NonNull MediaAdView mediaAdView) {
        this.W = new WeakReference<>(mediaAdView);
        ImageData image = this.an.getImage();
        if (this.ak) {
            a(mediaAdView, image);
            return;
        }
        d(mediaAdView, image);
        if (this.al) {
            a(mediaAdView, (b) this.am);
        } else {
            b(mediaAdView, image);
        }
    }

    private void a(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        c(mediaAdView, imageData);
        if (this.ap != 2) {
            this.ap = 3;
            Context context = mediaAdView.getContext();
            gn b = b(mediaAdView);
            if (b == null) {
                b = new gn(context);
                mediaAdView.addView(b, new LayoutParams(-1, -1));
            }
            if (this.aw != null) {
                b.restoreState(this.aw);
            }
            b.setClickable(this.at == null || this.av);
            b.setupCards(this.an.getNativeAdCards());
            b.setPromoCardSliderListener(this.am);
            mediaAdView.setBackgroundColor(0);
            b.setVisibility(0);
        }
    }

    private void b(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        c(mediaAdView, imageData);
        this.ap = 0;
        mediaAdView.getImageView().setVisibility(0);
        mediaAdView.getPlayButtonView().setVisibility(8);
        mediaAdView.getProgressBarView().setVisibility(8);
        if (this.at == null || this.av) {
            mediaAdView.setOnClickListener(this.am);
        }
    }

    private void c(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        if (imageData != null) {
            int width = imageData.getWidth();
            int height = imageData.getHeight();
            if (this.ay || width <= 0 || height <= 0) {
                mediaAdView.setPlaceHolderDimension(16, 9);
                this.ay = true;
                return;
            }
            mediaAdView.setPlaceHolderDimension(width, height);
            return;
        }
        mediaAdView.setPlaceHolderDimension(0, 0);
    }

    @Nullable
    private gn b(@NonNull MediaAdView mediaAdView) {
        for (int i = 0; i < mediaAdView.getChildCount(); i++) {
            View childAt = mediaAdView.getChildAt(i);
            if (childAt instanceof gn) {
                return (gn) childAt;
            }
        }
        return null;
    }

    private void d(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        ge geVar = (ge) mediaAdView.getImageView();
        if (imageData != null) {
            Bitmap bitmap = imageData.getBitmap();
            if (bitmap != null) {
                geVar.setImageBitmap(bitmap);
                return;
            }
            geVar.setImageBitmap(null);
            hu.a(imageData, (ImageView) geVar);
            return;
        }
        geVar.setImageBitmap(null);
    }

    private void a(@NonNull MediaAdView mediaAdView, @NonNull b bVar) {
        VideoData videoData;
        this.ap = 1;
        co videoBanner = this.an.getVideoBanner();
        if (videoBanner != null) {
            mediaAdView.setPlaceHolderDimension(videoBanner.getWidth(), videoBanner.getHeight());
            videoData = (VideoData) videoBanner.getMediaData();
        } else {
            videoData = null;
        }
        if (this.au == null && videoData != null) {
            this.ap = 1;
            this.au = new al(this.an, videoBanner, videoData, this.useExoPlayer);
        }
        if (this.au != null) {
            this.au.a(bVar);
            a(mediaAdView, this.au);
        }
    }

    private void a(@NonNull MediaAdView mediaAdView, @NonNull al alVar) {
        Context context;
        alVar.a((OnClickListener) this.am);
        if (this.aq != null) {
            View view = (View) this.aq.get();
            if (view != null) {
                context = view.getContext();
                alVar.a(mediaAdView, context);
            }
        }
        context = null;
        alVar.a(mediaAdView, context);
    }

    private void d(@NonNull View view) {
        if (!(view instanceof ViewGroup)) {
            view.setOnClickListener(null);
        } else if (!(view instanceof RecyclerView) && !(view instanceof MediaAdView)) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt != null) {
                    d(childAt);
                }
            }
            viewGroup.setOnClickListener(null);
        }
    }

    private void c(@NonNull MediaAdView mediaAdView) {
        ImageData image = this.an.getImage();
        ge geVar = (ge) mediaAdView.getImageView();
        if (image != null) {
            hu.b(image, (ImageView) geVar);
        }
        mediaAdView.getProgressBarView().setVisibility(8);
        mediaAdView.getPlayButtonView().setVisibility(8);
        geVar.setImageData(null);
        mediaAdView.setPlaceHolderDimension(0, 0);
        mediaAdView.setOnClickListener(null);
        mediaAdView.setBackgroundColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
        gn b = b(mediaAdView);
        if (b != null) {
            this.aw = b.getState();
            b.dispose();
            b.setVisibility(8);
        }
    }
}
