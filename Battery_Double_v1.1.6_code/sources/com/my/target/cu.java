package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

/* compiled from: MediationAdNetwork */
public final class cu {
    @NonNull
    private final String bn;
    @Nullable
    private String bo;
    @NonNull
    private final String dE;
    @NonNull
    private final HashMap<String, String> dF = new HashMap<>();
    @Nullable
    private cv dG;
    @NonNull
    private final String name;
    private int priority = 0;
    @NonNull
    private final di statHolder = di.cs();
    private int timeout = 10000;

    @NonNull
    public static cu a(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        return new cu(str, str2, str3);
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public String getPlacementId() {
        return this.bn;
    }

    @NonNull
    public String bF() {
        return this.dE;
    }

    @NonNull
    public di getStatHolder() {
        return this.statHolder;
    }

    @NonNull
    public Map<String, String> bG() {
        return new HashMap(this.dF);
    }

    @Nullable
    public String getPayload() {
        return this.bo;
    }

    public void v(@Nullable String str) {
        this.bo = str;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public void setTimeout(int i) {
        this.timeout = i;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int i) {
        this.priority = i;
    }

    @Nullable
    public cv bH() {
        return this.dG;
    }

    public void a(@Nullable cv cvVar) {
        this.dG = cvVar;
    }

    private cu(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        this.name = str;
        this.bn = str2;
        this.dE = str3;
    }

    public void b(@NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str)) {
            if (str2 == null) {
                this.dF.remove(str);
            } else {
                this.dF.put(str, str2);
            }
        }
    }
}
