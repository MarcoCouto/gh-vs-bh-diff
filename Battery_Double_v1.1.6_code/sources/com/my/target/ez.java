package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: InterstitialWebPresenter */
public interface ez extends eu {

    /* compiled from: InterstitialWebPresenter */
    public interface a extends com.my.target.eu.a {
        void a(@NonNull ch chVar, float f, float f2, @NonNull Context context);

        void a(@NonNull ch chVar, @NonNull String str, @NonNull Context context);

        void e(@NonNull String str);
    }

    void a(@NonNull cy cyVar, @NonNull cl clVar);

    void a(@Nullable a aVar);
}
