package com.my.target;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import java.util.HashSet;
import java.util.Iterator;
import java.util.WeakHashMap;

/* compiled from: Repeater */
public class ia {
    @NonNull
    private static final Handler handler = new Handler(Looper.getMainLooper());
    @NonNull
    public static final ia nN = new ia(1000);
    private final int nO;
    @NonNull
    private final WeakHashMap<Runnable, Boolean> nP = new WeakHashMap<>();
    @NonNull
    private final Runnable nQ = new Runnable() {
        public void run() {
            ia.this.eE();
        }
    };

    @NonNull
    public static final ia K(int i) {
        return new ia(i);
    }

    private ia(int i) {
        this.nO = i;
    }

    @AnyThread
    public void d(@NonNull Runnable runnable) {
        synchronized (this) {
            int size = this.nP.size();
            if (this.nP.put(runnable, Boolean.valueOf(true)) == null && size == 0) {
                eF();
            }
        }
    }

    @AnyThread
    public void e(@NonNull Runnable runnable) {
        synchronized (this) {
            this.nP.remove(runnable);
            if (this.nP.size() == 0) {
                handler.removeCallbacks(this.nQ);
            }
        }
    }

    /* access modifiers changed from: private */
    public void eE() {
        synchronized (this) {
            Iterator it = new HashSet(this.nP.keySet()).iterator();
            while (it.hasNext()) {
                ((Runnable) it.next()).run();
            }
            if (this.nP.keySet().size() > 0) {
                eF();
            }
        }
    }

    private void eF() {
        handler.postDelayed(this.nQ, (long) this.nO);
    }
}
