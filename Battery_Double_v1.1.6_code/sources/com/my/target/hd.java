package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import com.my.target.common.models.VideoData;
import com.my.target.hi.a;

/* compiled from: PromoStyle2Factory */
public class hd {
    @NonNull
    private final Context context;

    public static hd I(@NonNull Context context2) {
        return new hd(context2);
    }

    private hd(@NonNull Context context2) {
        this.context = context2;
    }

    @NonNull
    public hi a(@NonNull fs fsVar, @NonNull View view, @NonNull View view2, @NonNull a aVar) {
        hj hjVar = new hj(fsVar, view, view2, aVar, this.context);
        return hjVar;
    }

    @NonNull
    public fs em() {
        return new fs(this.context);
    }

    @NonNull
    public he a(@NonNull he.a aVar) {
        return new hf(this.context, aVar);
    }

    @NonNull
    public hg en() {
        return new hh(this.context);
    }

    @NonNull
    public es a(@NonNull co<VideoData> coVar, @NonNull fs fsVar, @NonNull ev.a aVar, boolean z) {
        ig igVar;
        if (!z || !hw.ex()) {
            igVar = ih.eI();
        } else {
            igVar = ii.R(fsVar.getContext());
        }
        return ev.a(coVar, fsVar, aVar, igVar);
    }
}
