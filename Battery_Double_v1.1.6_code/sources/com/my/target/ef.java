package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.ironsource.sdk.constants.Constants;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: MediationParser */
public class ef {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;
    @NonNull
    private final a eD;
    @NonNull
    private final em ez;

    /* compiled from: MediationParser */
    public interface a {
        @Nullable
        cv b(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context);
    }

    @NonNull
    public static ef a(@NonNull a aVar, @NonNull bz bzVar, @NonNull a aVar2, @NonNull Context context2) {
        return new ef(aVar, bzVar, aVar2, context2);
    }

    private ef(@NonNull a aVar, @NonNull bz bzVar, @NonNull a aVar2, @NonNull Context context2) {
        this.eD = aVar;
        this.eA = bzVar;
        this.adConfig = aVar2;
        this.context = context2;
        this.ez = em.k(bzVar, aVar2, context2);
    }

    @Nullable
    public ct g(@NonNull JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("networks");
        ct ctVar = null;
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        ct bB = ct.bB();
        int optInt = jSONObject.optInt("refreshTimeout", bB.bC());
        if (optInt >= 0) {
            bB.r(optInt);
        } else {
            f("Bad value", "refreshTimeout < 0");
        }
        int length = optJSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                cu h = h(optJSONObject);
                if (h != null) {
                    bB.b(h);
                }
            }
        }
        if (bB.bE()) {
            ctVar = bB;
        }
        return ctVar;
    }

    @Nullable
    private cu h(@NonNull JSONObject jSONObject) {
        String optString = jSONObject.optString("name");
        if (TextUtils.isEmpty(optString)) {
            f("Required field", "no name in mediationAdNetwork");
            return null;
        }
        String optString2 = jSONObject.optString(Constants.PLACEMENT_ID);
        if (TextUtils.isEmpty(optString2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("no placementId for ");
            sb.append(optString);
            sb.append(" mediationAdNetwork");
            f("Required field", sb.toString());
            return null;
        }
        String optString3 = jSONObject.optString("adapter");
        if (TextUtils.isEmpty(optString3)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("no adapter for ");
            sb2.append(optString);
            sb2.append(" mediationAdNetwork");
            f("Required field", sb2.toString());
            return null;
        }
        cu a2 = cu.a(optString, optString2, optString3);
        if ("myTarget".equals(optString)) {
            JSONObject optJSONObject = jSONObject.optJSONObject("banner");
            if (optJSONObject != null) {
                a2.a(this.eD.b(optJSONObject, this.eA, this.adConfig, this.context));
            }
        }
        String optString4 = jSONObject.optString(MessengerShareContentUtility.ATTACHMENT_PAYLOAD);
        if (!TextUtils.isEmpty(optString4)) {
            a2.v(optString4);
        }
        int optInt = jSONObject.optInt("timeout", a2.getTimeout());
        if (optInt > 0) {
            a2.setTimeout(optInt);
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("timeout <= 0 for ");
            sb3.append(optString);
            sb3.append(" mediationAdNetwork");
            f("Bad value", sb3.toString());
        }
        a2.setPriority(jSONObject.optInt("priority", a2.getPriority()));
        JSONObject optJSONObject2 = jSONObject.optJSONObject("params");
        if (optJSONObject2 != null) {
            Iterator keys = optJSONObject2.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                if (!TextUtils.isEmpty(str)) {
                    a2.b(str, optJSONObject2.optString(str));
                }
            }
        }
        this.ez.a(a2.getStatHolder(), jSONObject, optString, -1.0f);
        return a2;
    }

    private void f(@NonNull String str, @NonNull String str2) {
        dq.P(str).Q(str2).x(this.adConfig.getSlotId()).R(this.eA.getUrl()).q(this.context);
    }
}
