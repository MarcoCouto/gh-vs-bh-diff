package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: WebViewPresenter */
public class fc implements fb, com.my.target.ft.a {
    @Nullable
    private cs bL;
    @NonNull
    private final Context context;
    @NonNull
    private final ft eQ;
    @NonNull
    private final String format;
    @Nullable
    private a gG;
    @NonNull
    private final fx gc;
    @Nullable
    private com.my.target.fb.a gi;
    @NonNull
    private final dd section;

    /* compiled from: WebViewPresenter */
    public interface a {
        void ac();

        void e(@NonNull String str);
    }

    @NonNull
    public static fc a(@NonNull String str, @NonNull dd ddVar, @NonNull Context context2) {
        return new fc(str, ddVar, context2);
    }

    @VisibleForTesting
    fc(@NonNull ft ftVar, @NonNull fx fxVar, @NonNull String str, @NonNull dd ddVar, @NonNull Context context2) {
        this.eQ = ftVar;
        this.gc = fxVar;
        this.context = context2;
        this.format = str;
        this.section = ddVar;
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.gravity = 1;
        fxVar.addView(this.eQ);
        this.eQ.setLayoutParams(layoutParams);
        this.eQ.setBannerWebViewListener(this);
    }

    private fc(@NonNull String str, @NonNull dd ddVar, @NonNull Context context2) {
        this(new ft(context2), new fx(context2), str, ddVar, context2);
    }

    public void a(@NonNull cs csVar) {
        this.bL = csVar;
        JSONObject cd = this.section.cd();
        String cm = this.section.cm();
        if (cd == null) {
            Y("failed to load, null raw data");
        } else if (cm == null) {
            Y("failed to load, null html");
        } else {
            this.eQ.f(cd, cm);
        }
    }

    public void start() {
        try {
            this.eQ.a((bi) new bk(this.format, null, this.context.getResources().getConfiguration().orientation));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            this.eQ.a((bi) new bh("stop"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        try {
            this.eQ.a((bi) new bh("pause"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void resume() {
        try {
            this.eQ.a((bi) new bh("resume"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void destroy() {
        a((a) null);
        a((com.my.target.fb.a) null);
        if (this.eQ.getParent() != null) {
            ((ViewGroup) this.eQ.getParent()).removeView(this.eQ);
        }
        this.eQ.destroy();
    }

    @NonNull
    public fx dF() {
        return this.gc;
    }

    public void a(@NonNull bq bqVar) {
        char c;
        String type = bqVar.getType();
        switch (type.hashCode()) {
            case -2124458952:
                if (type.equals("onComplete")) {
                    c = 6;
                    break;
                }
            case -1349867671:
                if (type.equals("onError")) {
                    c = 3;
                    break;
                }
            case -1338265852:
                if (type.equals("onReady")) {
                    c = 0;
                    break;
                }
            case -1013111741:
                if (type.equals("onNoAd")) {
                    c = 7;
                    break;
                }
            case -1012956973:
                if (type.equals("onStat")) {
                    c = 14;
                    break;
                }
            case 157935686:
                if (type.equals("onAdClick")) {
                    c = 13;
                    break;
                }
            case 159970502:
                if (type.equals("onAdError")) {
                    c = 4;
                    break;
                }
            case 169625780:
                if (type.equals("onAdPause")) {
                    c = 10;
                    break;
                }
            case 172943136:
                if (type.equals("onAdStart")) {
                    c = 8;
                    break;
                }
            case 567029179:
                if (type.equals("onAdComplete")) {
                    c = 12;
                    break;
                }
            case 747469392:
                if (type.equals("onSizeChange")) {
                    c = 15;
                    break;
                }
            case 975410564:
                if (type.equals("onAdStop")) {
                    c = 9;
                    break;
                }
            case 1024326959:
                if (type.equals("onAdResume")) {
                    c = 11;
                    break;
                }
            case 1109243225:
                if (type.equals("onExpand")) {
                    c = 1;
                    break;
                }
            case 1252938159:
                if (type.equals("onCloseClick")) {
                    c = 5;
                    break;
                }
            case 2103168704:
                if (type.equals("onRequestNewAds")) {
                    c = 16;
                    break;
                }
            case 2137867948:
                if (type.equals("onCollapse")) {
                    c = 2;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                dK();
                return;
            case 3:
            case 4:
                bp bpVar = (bp) bqVar;
                String str = "JS error";
                if (bpVar.aH() != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(": ");
                    sb.append(bpVar.aH());
                    str = sb.toString();
                }
                dq.P("JS error").Q(str).R(this.eQ.getUrl()).S(this.bL != null ? this.bL.getId() : null).q(this.context);
                if (bqVar.getType().equals("onError")) {
                    Y("JS error");
                    return;
                }
                return;
            case 6:
                Y("Ad completed");
                return;
            case 7:
                Y("No ad");
                return;
            case 8:
                dJ();
                return;
            case 13:
                ac(((bn) bqVar).getUrl());
                return;
            case 14:
                c(((bt) bqVar).aI());
                return;
            default:
                return;
        }
    }

    public void a(@Nullable com.my.target.fb.a aVar) {
        this.gi = aVar;
    }

    public void a(@Nullable a aVar) {
        this.gG = aVar;
    }

    public void onError(@NonNull String str) {
        Y(str);
    }

    public void X(@NonNull String str) {
        if (this.bL != null) {
            ac(str);
        }
    }

    private void ac(@Nullable String str) {
        if (this.gi != null && this.bL != null) {
            this.gi.a(this.bL, str);
        }
    }

    private void c(@Nullable List<String> list) {
        ib.b(list, this.context);
    }

    private void dJ() {
        if (this.gi != null && this.bL != null) {
            this.gi.a(this.bL);
        }
    }

    private void Y(@NonNull String str) {
        if (this.gG != null) {
            this.gG.e(str);
        }
    }

    private void dK() {
        if (this.gG != null) {
            this.gG.ac();
        }
    }
}
