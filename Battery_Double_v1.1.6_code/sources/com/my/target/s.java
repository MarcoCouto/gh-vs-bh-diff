package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0056b;

/* compiled from: InterstitialSliderAdFactory */
public final class s extends b<cz> {

    /* compiled from: InterstitialSliderAdFactory */
    static class a implements com.my.target.b.a<cz> {
        public boolean a() {
            return false;
        }

        private a() {
        }

        @NonNull
        public c<cz> b() {
            return t.f();
        }

        @Nullable
        public d<cz> c() {
            return u.j();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    public interface b extends C0056b {
    }

    @NonNull
    public static b<cz> a(@NonNull a aVar) {
        return new s(aVar);
    }

    private s(@NonNull a aVar) {
        super(new a(), aVar);
    }
}
