package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import com.my.target.common.models.VideoData;

/* compiled from: DefaultVideoPlayer */
public class ih implements OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener, SurfaceTextureListener, ig {
    @NonNull
    private final ia A;
    @Nullable
    private fs fx;
    @Nullable
    private VideoData kP;
    @NonNull
    private final a of;
    @NonNull
    private final MediaPlayer og;
    @Nullable
    private com.my.target.ig.a oh;
    @Nullable
    private Surface oi;
    private int oj;
    private long ok;
    private int state;
    private float volume;

    @VisibleForTesting
    /* compiled from: DefaultVideoPlayer */
    static class a implements Runnable {
        private int F;
        @Nullable
        private com.my.target.ig.a oh;
        private final int ol;
        @Nullable
        private ih om;
        private float s;

        a(int i) {
            this.ol = i;
        }

        public void run() {
            if (this.oh != null && this.om != null) {
                float position = ((float) this.om.getPosition()) / 1000.0f;
                float duration = this.om.getDuration();
                if (this.s == position) {
                    this.F++;
                } else {
                    this.oh.a(position, duration);
                    this.s = position;
                    if (this.F > 0) {
                        this.F = 0;
                    }
                }
                if (this.F > this.ol) {
                    this.oh.d("timeout");
                    this.F = 0;
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable com.my.target.ig.a aVar) {
            this.oh = aVar;
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable ih ihVar) {
            this.om = ihVar;
        }
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    @NonNull
    public static ig eI() {
        return new ih();
    }

    @Nullable
    public VideoData eH() {
        return this.kP;
    }

    public boolean isMuted() {
        return this.volume == 0.0f;
    }

    public boolean isStarted() {
        return this.state >= 1 && this.state < 3;
    }

    public void a(@Nullable com.my.target.ig.a aVar) {
        this.oh = aVar;
        this.of.a(aVar);
    }

    public boolean isPaused() {
        return this.state == 2;
    }

    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
        if (i != 3) {
            return false;
        }
        if (this.oh != null) {
            this.oh.y();
        }
        return true;
    }

    private ih() {
        this(new MediaPlayer(), new a(50));
    }

    @VisibleForTesting
    ih(@NonNull MediaPlayer mediaPlayer, @NonNull a aVar) {
        this.A = ia.K(200);
        this.state = 0;
        this.volume = 1.0f;
        this.ok = 0;
        this.og = mediaPlayer;
        this.of = aVar;
        aVar.a(this);
    }

    public long getPosition() {
        if (!eK() || this.state == 3) {
            return 0;
        }
        return (long) this.og.getCurrentPosition();
    }

    public void seekTo(long j) {
        this.ok = j;
        if (eK()) {
            try {
                this.og.seekTo((int) j);
                this.ok = 0;
            } catch (IllegalStateException unused) {
                ah.a("seekTo called in wrong state");
            }
        }
    }

    public float getDuration() {
        if (eK()) {
            return ((float) this.og.getDuration()) / 1000.0f;
        }
        return 0.0f;
    }

    public void stop() {
        this.A.e(this.of);
        try {
            this.og.stop();
        } catch (IllegalStateException unused) {
            ah.a("stop called in wrong state");
        }
        if (this.oh != null) {
            this.oh.x();
        }
        this.state = 3;
    }

    public void a(@NonNull VideoData videoData, @NonNull Context context) {
        Uri uri;
        String str = (String) videoData.getData();
        if (this.fx != null) {
            this.fx.e(videoData.getWidth(), videoData.getHeight());
        }
        if (str != null) {
            uri = Uri.parse(str);
        } else {
            uri = Uri.parse(videoData.getUrl());
        }
        this.kP = videoData;
        a(uri, context);
    }

    @SuppressLint({"Recycle"})
    public void a(@Nullable fs fsVar) {
        eJ();
        this.fx = fsVar;
        Surface surface = null;
        if (fsVar != null) {
            if (this.kP != null) {
                fsVar.e(this.kP.getWidth(), this.kP.getHeight());
            }
            TextureView textureView = fsVar.getTextureView();
            if (textureView.getSurfaceTextureListener() != null) {
                Log.w("DefaultVideoPlayer", "Replacing existing SurfaceTextureListener.");
            }
            textureView.setSurfaceTextureListener(this);
            SurfaceTexture surfaceTexture = textureView.isAvailable() ? textureView.getSurfaceTexture() : null;
            if (surfaceTexture != null) {
                surface = new Surface(surfaceTexture);
            }
            a(surface);
            return;
        }
        a((Surface) null);
    }

    public void destroy() {
        this.state = 5;
        this.A.e(this.of);
        eJ();
        if (eK()) {
            try {
                this.og.stop();
            } catch (IllegalStateException unused) {
                ah.a("stop called in wrong state");
            }
        }
        this.og.release();
        this.fx = null;
    }

    public void L() {
        setVolume(0.2f);
    }

    public void M() {
        setVolume(0.0f);
    }

    public void setVolume(float f) {
        this.volume = f;
        if (eK()) {
            this.og.setVolume(f, f);
        }
        if (this.oh != null) {
            this.oh.e(f);
        }
    }

    public void cU() {
        setVolume(1.0f);
    }

    public void pause() {
        if (this.state == 1) {
            this.oj = this.og.getCurrentPosition();
            this.A.e(this.of);
            try {
                this.og.pause();
            } catch (IllegalStateException unused) {
                ah.a("pause called in wrong state");
            }
            this.state = 2;
            if (this.oh != null) {
                this.oh.z();
            }
        }
    }

    public void resume() {
        if (this.state == 2) {
            this.A.d(this.of);
            try {
                this.og.start();
            } catch (IllegalStateException unused) {
                ah.a("start called in wrong state");
            }
            if (this.oj > 0) {
                try {
                    this.og.seekTo(this.oj);
                } catch (IllegalStateException unused2) {
                    ah.a("seekTo called in wrong state");
                }
                this.oj = 0;
            }
            this.state = 1;
            if (this.oh != null) {
                this.oh.A();
            }
        }
    }

    public boolean isPlaying() {
        return this.state == 1;
    }

    public void de() {
        if (this.volume == 1.0f) {
            setVolume(0.0f);
        } else {
            setVolume(1.0f);
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.state = 4;
        if (this.oh != null) {
            this.oh.C();
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        this.A.e(this.of);
        eJ();
        a((Surface) null);
        if (this.oh != null) {
            String str = "Unknown error";
            String str2 = "Unknown";
            if (i == 100) {
                str = "Server died";
            }
            if (i2 == -1004) {
                str2 = "IO error";
            } else if (i2 == -1007) {
                str2 = "Malformed error";
            } else if (i2 == -1010) {
                str2 = "Unsupported error";
            } else if (i2 == -110) {
                str2 = "Timed out error";
            } else if (i2 == Integer.MIN_VALUE) {
                str2 = "Low-level system error";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" (reason: ");
            sb.append(str2);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("DefaultVideoPlayerVideo error: ");
            sb3.append(sb2);
            ah.a(sb3.toString());
            this.oh.d(sb2);
        }
        if (this.state > 0) {
            this.og.reset();
        }
        this.state = 0;
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.setVolume(this.volume, this.volume);
        this.state = 1;
        try {
            mediaPlayer.start();
            if (this.ok > 0) {
                seekTo(this.ok);
            }
        } catch (IllegalStateException unused) {
            ah.a("start called in wrong state");
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        a(new Surface(surfaceTexture));
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        a((Surface) null);
        return true;
    }

    @SuppressLint({"Recycle"})
    private void a(@NonNull Uri uri, @NonNull Context context) {
        ah.a("Play video in Android MediaPlayer");
        if (this.state != 0) {
            this.og.reset();
            this.state = 0;
        }
        this.og.setOnCompletionListener(this);
        this.og.setOnErrorListener(this);
        this.og.setOnPreparedListener(this);
        this.og.setOnInfoListener(this);
        try {
            this.og.setDataSource(context, uri);
        } catch (IllegalStateException unused) {
            ah.a("DefaultVideoPlayerIllegal state on setDataSource");
        } catch (Exception e) {
            if (this.oh != null) {
                this.oh.d(e.toString());
            }
            StringBuilder sb = new StringBuilder();
            sb.append("DefaultVideoPlayerUnable to parse video source ");
            sb.append(e.getMessage());
            ah.a(sb.toString());
            this.state = 5;
            e.printStackTrace();
            return;
        }
        if (this.oh != null) {
            this.oh.B();
        }
        try {
            this.og.prepareAsync();
        } catch (IllegalStateException unused2) {
            ah.a("prepareAsync called in wrong state");
        }
        this.A.d(this.of);
    }

    private void a(@Nullable Surface surface) {
        this.og.setSurface(surface);
        if (!(this.oi == null || this.oi == surface)) {
            this.oi.release();
        }
        this.oi = surface;
    }

    private void eJ() {
        TextureView textureView = this.fx != null ? this.fx.getTextureView() : null;
        if (textureView == null) {
            return;
        }
        if (textureView.getSurfaceTextureListener() != this) {
            Log.w("DefaultVideoPlayer", "SurfaceTextureListener already unset or replaced.");
        } else {
            textureView.setSurfaceTextureListener(null);
        }
    }

    private boolean eK() {
        return this.state >= 1 && this.state <= 4;
    }
}
