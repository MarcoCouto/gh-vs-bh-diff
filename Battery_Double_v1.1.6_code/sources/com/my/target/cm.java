package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialAdImageBanner */
public class cm extends cj {
    @NonNull
    private final List<ImageData> landscapeImages = new ArrayList();
    @Nullable
    private ImageData optimalLandscapeImage;
    @Nullable
    private ImageData optimalPortraitImage;
    @NonNull
    private final List<ImageData> portraitImages = new ArrayList();

    @NonNull
    public static cm newBanner() {
        return new cm();
    }

    @NonNull
    public static cm fromCompanion(@NonNull ci ciVar) {
        cm newBanner = newBanner();
        newBanner.setId(ciVar.getId());
        String staticResource = ciVar.getStaticResource();
        if (staticResource != null) {
            newBanner.addPortraitImage(ImageData.newImageData(staticResource, ciVar.getWidth(), ciVar.getHeight()));
            newBanner.getStatHolder().a(ciVar.getStatHolder(), 0.0f);
            newBanner.trackingLink = ciVar.trackingLink;
        }
        return newBanner;
    }

    private cm() {
    }

    @NonNull
    public List<ImageData> getPortraitImages() {
        return new ArrayList(this.portraitImages);
    }

    @NonNull
    public List<ImageData> getLandscapeImages() {
        return new ArrayList(this.landscapeImages);
    }

    @Nullable
    public ImageData getOptimalPortraitImage() {
        return this.optimalPortraitImage;
    }

    public void setOptimalPortraitImage(@Nullable ImageData imageData) {
        this.optimalPortraitImage = imageData;
    }

    @Nullable
    public ImageData getOptimalLandscapeImage() {
        return this.optimalLandscapeImage;
    }

    public void setOptimalLandscapeImage(@Nullable ImageData imageData) {
        this.optimalLandscapeImage = imageData;
    }

    public void addPortraitImage(@NonNull ImageData imageData) {
        this.portraitImages.add(imageData);
    }

    public void addLandscapeImage(@NonNull ImageData imageData) {
        this.landscapeImages.add(imageData);
    }
}
