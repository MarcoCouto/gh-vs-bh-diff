package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;

/* compiled from: CloseableLayout */
public class fw extends FrameLayout {
    private final int ic;
    @NonNull
    private final BitmapDrawable ie;

    /* renamed from: if reason: not valid java name */
    private final int f457if;
    private final int ig;
    private final int ih;
    @NonNull
    private final Rect ii = new Rect();
    @NonNull
    private final Rect ij = new Rect();
    @NonNull
    private final Rect ik = new Rect();
    @NonNull
    private final Rect il = new Rect();
    @Nullable
    private a im;
    private boolean in;

    /* renamed from: io reason: collision with root package name */
    private boolean f460io;
    private int ip = 8388661;
    @NonNull
    private final ic uiUtils;

    /* compiled from: CloseableLayout */
    public interface a {
        void onClose();
    }

    public fw(@NonNull Context context) {
        super(context);
        this.uiUtils = ic.P(context);
        this.ie = new BitmapDrawable(fl.B(this.uiUtils.M(30)));
        this.ie.setState(EMPTY_STATE_SET);
        this.ie.setCallback(this);
        this.ic = ViewConfiguration.get(context).getScaledTouchSlop();
        this.f457if = ic.a(50, context);
        this.ig = ic.a(30, context);
        this.ih = ic.a(8, context);
        setWillNotDraw(false);
    }

    public void a(int i, Rect rect, Rect rect2) {
        Gravity.apply(i, this.ig, this.ig, rect, rect2);
    }

    public void setOnCloseListener(@Nullable a aVar) {
        this.im = aVar;
    }

    public void setCloseVisible(boolean z) {
        if (this.ie.setVisible(z, false)) {
            invalidate(this.ij);
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!b((int) motionEvent.getX(), (int) motionEvent.getY(), this.ic)) {
            super.onTouchEvent(motionEvent);
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    this.f460io = true;
                    break;
                case 1:
                    if (this.f460io) {
                        dT();
                        this.f460io = false;
                        break;
                    }
                    break;
            }
        } else {
            this.f460io = false;
        }
        return true;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.in) {
            this.in = false;
            this.ii.set(0, 0, getWidth(), getHeight());
            b(this.f457if, this.ii, this.ij);
            this.il.set(this.ij);
            this.il.inset(this.ih, this.ih);
            b(this.ig, this.il, this.ik);
            this.ie.setBounds(this.ik);
        }
        if (this.ie.isVisible()) {
            this.ie.draw(canvas);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        return b((int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }

    public boolean dS() {
        return this.ie.isVisible();
    }

    public void setCloseGravity(int i) {
        this.ip = i;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void setCloseBounds(@NonNull Rect rect) {
        this.ij.set(rect);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean b(int i, int i2, int i3) {
        return i >= this.ij.left - i3 && i2 >= this.ij.top - i3 && i < this.ij.right + i3 && i2 < this.ij.bottom + i3;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.in = true;
    }

    private void b(int i, Rect rect, Rect rect2) {
        Gravity.apply(this.ip, i, i, rect, rect2);
    }

    private void dT() {
        playSoundEffect(0);
        if (this.im != null) {
            this.im.onClose();
        }
    }
}
