package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.mediation.MediationAdConfig;
import com.my.target.mediation.MediationAdapter;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

/* compiled from: MediationEngine */
public abstract class ax<T extends MediationAdapter> {
    @NonNull
    private final ct bi;
    @Nullable
    private WeakReference<Context> bj;
    @Nullable
    private b bk;
    @Nullable
    T bl;
    @Nullable
    private String bm;
    @Nullable
    private ia k;

    /* compiled from: MediationEngine */
    static class a implements MediationAdConfig {
        @NonNull
        private final String bn;
        @Nullable
        private final String bo;
        private final int bp;
        private final int bq;
        @NonNull
        private final Map<String, String> br;
        private final boolean bs;
        private final boolean bt;
        private final boolean trackingEnvironmentEnabled;
        private final boolean trackingLocationEnabled;
        private final boolean userAgeRestricted;

        @NonNull
        public static a a(@NonNull String str, @Nullable String str2, @NonNull Map<String, String> map, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
            a aVar = new a(str, str2, map, i, i2, z, z2, z3, z4, z5);
            return aVar;
        }

        @NonNull
        public String getPlacementId() {
            return this.bn;
        }

        @Nullable
        public String getPayload() {
            return this.bo;
        }

        @NonNull
        public Map<String, String> getServerParams() {
            return this.br;
        }

        public int getAge() {
            return this.bq;
        }

        public int getGender() {
            return this.bp;
        }

        public boolean isUserConsent() {
            return this.bs;
        }

        public boolean isUserConsentSpecified() {
            return this.bt;
        }

        public boolean isUserAgeRestricted() {
            return this.userAgeRestricted;
        }

        public boolean isTrackingLocationEnabled() {
            return this.trackingLocationEnabled;
        }

        public boolean isTrackingEnvironmentEnabled() {
            return this.trackingEnvironmentEnabled;
        }

        a(@NonNull String str, @Nullable String str2, @NonNull Map<String, String> map, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
            this.bn = str;
            this.bo = str2;
            this.br = map;
            this.bq = i;
            this.bp = i2;
            this.bt = z;
            this.bs = z2;
            this.userAgeRestricted = z3;
            this.trackingLocationEnabled = z4;
            this.trackingEnvironmentEnabled = z5;
        }
    }

    /* compiled from: MediationEngine */
    class b implements Runnable {
        @NonNull
        final cu bu;

        b(cu cuVar) {
            this.bu = cuVar;
        }

        public void run() {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationEngine: timeout for ");
            sb.append(this.bu.getName());
            sb.append(" ad network");
            ah.a(sb.toString());
            Context context = ax.this.getContext();
            if (context != null) {
                ib.a((List<dh>) this.bu.getStatHolder().N("networkTimeout"), context);
            }
            ax.this.a(this.bu, false);
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(@NonNull T t, @NonNull cu cuVar, @NonNull Context context);

    /* access modifiers changed from: 0000 */
    public abstract boolean a(@NonNull MediationAdapter mediationAdapter);

    /* access modifiers changed from: 0000 */
    @NonNull
    public abstract T an();

    /* access modifiers changed from: 0000 */
    public abstract void ao();

    ax(@NonNull ct ctVar) {
        this.bi = ctVar;
    }

    public void n(@NonNull Context context) {
        this.bj = new WeakReference<>(context);
        ap();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public Context getContext() {
        if (this.bj == null) {
            return null;
        }
        return (Context) this.bj.get();
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull cu cuVar, boolean z) {
        if (this.bk != null && this.bk.bu == cuVar) {
            if (this.k != null) {
                this.k.e(this.bk);
                this.k = null;
            }
            this.bk = null;
            if (z) {
                this.bm = cuVar.getName();
                Context context = getContext();
                if (context != null) {
                    ib.a((List<dh>) cuVar.getStatHolder().N("networkFilled"), context);
                }
            } else {
                ap();
            }
        }
    }

    @Nullable
    public String aa() {
        return this.bm;
    }

    private void ap() {
        if (this.bl != null) {
            try {
                this.bl.destroy();
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationEngine error: ");
                sb.append(th.toString());
                ah.b(sb.toString());
            }
            this.bl = null;
        }
        Context context = getContext();
        if (context == null) {
            ah.b("MediationEngine: can't configure next ad network, context is null");
            return;
        }
        cu bD = this.bi.bD();
        if (bD == null) {
            ah.a("MediationEngine: no ad networks available");
            ao();
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("MediationEngine: prepare adapter for ");
        sb2.append(bD.getName());
        sb2.append(" ad network");
        ah.a(sb2.toString());
        this.bl = a(bD);
        if (this.bl == null || !a((MediationAdapter) this.bl)) {
            ah.b("MediationEngine: can't create adapter, class not found or invalid");
            ap();
        } else {
            ah.a("MediationEngine: adapter created");
            this.bk = new b<>(bD);
            int timeout = bD.getTimeout();
            if (timeout > 0) {
                this.k = ia.K(timeout);
                this.k.d(this.bk);
            }
            ib.a((List<dh>) bD.getStatHolder().N("networkRequested"), context);
            a(this.bl, bD, context);
        }
    }

    @Nullable
    private T a(@NonNull cu cuVar) {
        if ("myTarget".equals(cuVar.getName())) {
            return an();
        }
        return f(cuVar.bF());
    }

    @Nullable
    private T f(@NonNull String str) {
        try {
            return (MediationAdapter) Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
            return null;
        }
    }
}
