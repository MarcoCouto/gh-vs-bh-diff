package com.my.target;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.ig.a;
import com.my.target.instreamads.InstreamAdPlayer;
import com.my.target.instreamads.InstreamAdPlayer.AdPlayerListener;

/* compiled from: InstreamAdVideoPlayer */
public class fp extends FrameLayout implements a, InstreamAdPlayer {
    @NonNull
    private final fs fx;
    @Nullable
    private AdPlayerListener hI;
    private boolean hJ;
    private boolean hK;
    @Nullable
    private ii hL;
    private int placeholderHeight;
    private int placeholderWidth;

    public void B() {
    }

    public void a(float f, float f2) {
    }

    @NonNull
    public View getView() {
        return this;
    }

    public fp(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, new fs(context));
    }

    @VisibleForTesting
    fp(@NonNull Context context, @Nullable AttributeSet attributeSet, int i, @NonNull fs fsVar) {
        super(context, attributeSet, i);
        this.fx = fsVar;
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.gravity = 17;
        addView(fsVar, layoutParams);
    }

    @Nullable
    public AdPlayerListener getAdPlayerListener() {
        return this.hI;
    }

    public fp(@NonNull Context context) {
        this(context, null);
    }

    public fp(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int getPlaceholderWidth() {
        return this.placeholderWidth;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int getPlaceholderHeight() {
        return this.placeholderHeight;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void setVideoPlayer(@Nullable ii iiVar) {
        this.hL = iiVar;
    }

    public float getAdVideoDuration() {
        if (this.hL != null) {
            return this.hL.getDuration();
        }
        return 0.0f;
    }

    public float getAdVideoPosition() {
        if (this.hL != null) {
            return ((float) this.hL.getPosition()) / 1000.0f;
        }
        return 0.0f;
    }

    public void setAdPlayerListener(@Nullable AdPlayerListener adPlayerListener) {
        this.hI = adPlayerListener;
    }

    public void playAdVideo(@NonNull Uri uri, int i, int i2) {
        this.placeholderWidth = i;
        this.placeholderHeight = i2;
        this.hJ = false;
        if (this.hL == null) {
            this.hL = ii.R(getContext());
            this.hL.a((a) this);
        }
        this.fx.e(i, i2);
        this.hL.a(uri, this.fx);
    }

    public void playAdVideo(@NonNull Uri uri, int i, int i2, float f) {
        playAdVideo(uri, i, i2);
        if (this.hL != null) {
            this.hL.seekTo((long) (f * 1000.0f));
        }
    }

    public void pauseAdVideo() {
        if (this.hL != null) {
            this.hL.pause();
        }
    }

    public void resumeAdVideo() {
        if (this.hL != null) {
            this.hL.resume();
        }
    }

    public void stopAdVideo() {
        if (this.hL != null) {
            this.hL.stop();
        }
    }

    public void destroy() {
        if (this.hL != null) {
            this.hL.destroy();
        }
    }

    public void setVolume(float f) {
        if (this.hL != null) {
            this.hL.setVolume(f);
        }
    }

    public void x() {
        if (this.hI != null) {
            this.hI.onAdVideoStopped();
        }
    }

    public void e(float f) {
        if (this.hI != null) {
            this.hI.onVolumeChanged(f);
        }
    }

    public void y() {
        if (!this.hJ && this.hI != null) {
            this.hI.onAdVideoStarted();
            this.hJ = true;
        }
    }

    public void A() {
        if (this.hK) {
            if (this.hI != null) {
                this.hI.onAdVideoResumed();
            }
            this.hK = false;
        }
    }

    public void z() {
        this.hK = true;
        if (this.hI != null) {
            this.hI.onAdVideoPaused();
        }
    }

    public void d(String str) {
        if (this.hI != null) {
            this.hI.onAdVideoError(str);
        }
    }

    public void C() {
        if (this.hI != null) {
            this.hI.onAdVideoCompleted();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int round;
        int round2;
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (mode == 0) {
            mode = Integer.MIN_VALUE;
        }
        if (mode2 == 0) {
            mode2 = Integer.MIN_VALUE;
        }
        if (this.placeholderHeight == 0 || this.placeholderWidth == 0) {
            super.onMeasure(i, i2);
            return;
        }
        float f = ((float) this.placeholderWidth) / ((float) this.placeholderHeight);
        float f2 = 0.0f;
        if (size2 != 0) {
            f2 = ((float) size) / ((float) size2);
        }
        if (!(mode == 1073741824 && mode2 == 1073741824)) {
            if (mode == Integer.MIN_VALUE && mode2 == Integer.MIN_VALUE) {
                if (f < f2) {
                    round = Math.round(((float) size2) * f);
                    if (size > 0 && round > size) {
                        round2 = Math.round(((float) size) / f);
                    }
                    size = round;
                } else {
                    i3 = Math.round(((float) size) / f);
                    if (size2 > 0 && i3 > size2) {
                        size = Math.round(((float) size2) * f);
                    }
                    size2 = i3;
                }
            } else if (mode == Integer.MIN_VALUE && mode2 == 1073741824) {
                round = Math.round(((float) size2) * f);
                if (size > 0 && round > size) {
                    round2 = Math.round(((float) size) / f);
                }
                size = round;
            } else if (mode == 1073741824 && mode2 == Integer.MIN_VALUE) {
                i3 = Math.round(((float) size) / f);
                if (size2 > 0 && i3 > size2) {
                    size = Math.round(((float) size2) * f);
                }
                size2 = i3;
            } else {
                size = 0;
                size2 = 0;
            }
            size2 = round2;
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2, 1073741824));
    }
}
