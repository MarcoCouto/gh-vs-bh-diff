package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

/* compiled from: AdTitle */
public class fr extends RelativeLayout {
    @NonNull
    private final TextView hN;
    @NonNull
    private final View hO;
    /* access modifiers changed from: private */
    @Nullable
    public a hP;
    @Nullable
    private String title;

    /* compiled from: AdTitle */
    public interface a {
        void aj();
    }

    public fr(@NonNull Context context) {
        super(context);
        this.hN = new TextView(context);
        this.hN.setTextColor(-1);
        this.hN.setTypeface(null, 1);
        this.hN.setTextSize(2, 20.0f);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.leftMargin = (int) TypedValue.applyDimension(1, 8.0f, getContext().getResources().getDisplayMetrics());
        layoutParams.addRule(15);
        layoutParams.addRule(1, 256);
        addView(this.hN, layoutParams);
        setBackgroundColor(-7829368);
        LayoutParams layoutParams2 = new LayoutParams(-1, (int) (context.getResources().getDisplayMetrics().density + 0.5f));
        layoutParams2.addRule(12);
        this.hO = new View(context);
        this.hO.setBackgroundColor(-10066330);
        addView(this.hO, layoutParams2);
        fz fzVar = new fz(context);
        fzVar.a(fk.C(context), false);
        fzVar.setId(256);
        fzVar.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (fr.this.hP != null) {
                    fr.this.hP.aj();
                }
            }
        });
        LayoutParams layoutParams3 = new LayoutParams(-2, -2);
        layoutParams3.leftMargin = (int) TypedValue.applyDimension(1, 5.0f, getContext().getResources().getDisplayMetrics());
        layoutParams3.addRule(15);
        layoutParams3.addRule(9);
        fzVar.setLayoutParams(layoutParams3);
        addView(fzVar);
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    public void setTitle(@Nullable String str) {
        this.title = str;
        this.hN.setText(str);
    }

    public void setStripeColor(int i) {
        this.hO.setBackgroundColor(i);
    }

    public void setMainColor(int i) {
        setBackgroundColor(i);
    }

    public void setTitleColor(int i) {
        this.hN.setTextColor(i);
    }

    public void setOnCloseClickListener(@Nullable a aVar) {
        this.hP = aVar;
    }
}
