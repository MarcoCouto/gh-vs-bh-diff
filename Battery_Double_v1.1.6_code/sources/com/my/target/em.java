package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: StatsParser */
public class em {
    @NonNull
    private final a adConfig;
    @Nullable
    private String ck;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;

    @NonNull
    public static em k(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new em(bzVar, aVar, context2);
    }

    protected em(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eA = bzVar;
        this.adConfig = aVar;
        this.context = context2;
    }

    public void a(@NonNull di diVar, @NonNull JSONObject jSONObject, @NonNull String str, float f) {
        diVar.a(this.eA.bj(), f);
        JSONArray optJSONArray = jSONObject.optJSONArray("statistics");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            if (length > 0) {
                this.ck = str;
                for (int i = 0; i < length; i++) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        dh a = a(optJSONObject, f);
                        if (a != null) {
                            diVar.b(a);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    @Nullable
    public dh a(@NonNull JSONObject jSONObject, float f) {
        String optString = jSONObject.optString("type");
        String optString2 = jSONObject.optString("url");
        if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2)) {
            f("Required field", "failed to parse stat: no type or url");
            return null;
        }
        char c = 65535;
        int hashCode = optString.hashCode();
        if (hashCode != 1669348544) {
            if (hashCode == 1788134515 && optString.equals("playheadReachedValue")) {
                c = 0;
            }
        } else if (optString.equals("playheadViewabilityValue")) {
            c = 1;
        }
        switch (c) {
            case 0:
                return a(jSONObject, optString2, f);
            case 1:
                return b(jSONObject, optString2, f);
            default:
                return dh.c(optString, optString2);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public dg a(@NonNull JSONObject jSONObject, @NonNull String str, float f) {
        dg M = dg.M(str);
        if (jSONObject.has("pvalue")) {
            float optDouble = (float) jSONObject.optDouble("pvalue", (double) M.cr());
            if (optDouble >= 0.0f && optDouble <= 100.0f) {
                if (f > 0.0f) {
                    M.i((optDouble * f) / 100.0f);
                } else {
                    M.j(optDouble);
                }
                return M;
            }
        }
        if (jSONObject.has("value")) {
            float optDouble2 = (float) jSONObject.optDouble("value", (double) M.cq());
            if (optDouble2 >= 0.0f) {
                M.i(optDouble2);
                return M;
            }
        }
        return null;
    }

    @Nullable
    private dh b(@NonNull JSONObject jSONObject, @NonNull String str, float f) {
        int optInt = jSONObject.optInt("viewablePercent", -1);
        if (optInt < 0 || optInt > 100) {
            f("Bad value", "failed to parse viewabilityStat: invalid viewable percent value");
            return null;
        }
        if (jSONObject.has("ovv")) {
            df L = df.L(str);
            L.w(optInt);
            L.q(jSONObject.optBoolean("ovv", false));
            if (jSONObject.has("pvalue")) {
                float optDouble = (float) jSONObject.optDouble("pvalue", (double) L.cr());
                if (optDouble >= 0.0f && optDouble <= 100.0f) {
                    if (f > 0.0f) {
                        L.i((optDouble * f) / 100.0f);
                    } else {
                        L.j(optDouble);
                    }
                    return L;
                }
            }
            if (jSONObject.has("value")) {
                float optDouble2 = (float) jSONObject.optDouble("value", (double) L.cq());
                if (optDouble2 >= 0.0f) {
                    L.i(optDouble2);
                    return L;
                }
            }
        } else if (jSONObject.has("duration")) {
            de K = de.K(str);
            K.w(optInt);
            float optDouble3 = (float) jSONObject.optDouble("duration", (double) K.getDuration());
            if (optDouble3 >= 0.0f) {
                K.setDuration(optDouble3);
                return K;
            }
        } else {
            f("Bad value", "failed to parse viewabilityStat: no ovv or duration");
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void f(@NonNull String str, @NonNull String str2) {
        dq.P(str).Q(str2).x(this.adConfig.getSlotId()).S(this.ck).R(this.eA.getUrl()).q(this.context);
    }
}
