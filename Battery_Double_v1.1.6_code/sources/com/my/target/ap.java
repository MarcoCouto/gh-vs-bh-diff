package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: IStandardAdEngine */
public interface ap {

    /* compiled from: IStandardAdEngine */
    public interface a {
        void ac();

        void ad();

        void ae();

        void af();

        void e(@NonNull String str);

        void onClick();
    }

    void a(@Nullable a aVar);

    @Nullable
    String aa();

    void destroy();

    void pause();

    void prepare();

    void resume();

    void start();

    void stop();
}
