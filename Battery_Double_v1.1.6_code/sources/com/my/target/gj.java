package com.my.target;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.MeasureSpec;

/* compiled from: VideoProgressWheelNative */
public class gj extends View {
    @NonNull
    private final Paint jA = new Paint();
    @NonNull
    private final Paint jB = new Paint();
    @NonNull
    private final Paint jC = new Paint();
    @NonNull
    private RectF jD = new RectF();
    private long jE = 0;
    private float jF = 0.0f;
    private float jG = 0.0f;
    private float jH = 230.0f;
    private boolean jI = false;
    private int jJ;
    @NonNull
    private final ic uiUtils;

    public gj(@NonNull Context context) {
        super(context);
        this.uiUtils = ic.P(context);
    }

    public void setDigit(int i) {
        this.jJ = i;
    }

    public void setProgress(float f) {
        if (this.jI) {
            this.jF = 0.0f;
            this.jI = false;
        }
        if (f > 1.0f) {
            f = 1.0f;
        } else if (f < 0.0f) {
            f = 0.0f;
        }
        if (f != this.jG) {
            if (this.jF == this.jG) {
                this.jE = SystemClock.uptimeMillis();
            }
            this.jG = Math.min(f * 360.0f, 360.0f);
            invalidate();
        }
    }

    public void setMax(float f) {
        if (f > 0.0f) {
            this.jH = 360.0f / f;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int M = this.uiUtils.M(28) + getPaddingLeft() + getPaddingRight();
        int M2 = this.uiUtils.M(28) + getPaddingTop() + getPaddingBottom();
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            M = size;
        } else if (mode == Integer.MIN_VALUE) {
            M = Math.min(M, size);
        }
        if (mode2 == 1073741824 || mode == 1073741824) {
            M2 = size2;
        } else if (mode2 == Integer.MIN_VALUE) {
            M2 = Math.min(M2, size2);
        }
        setMeasuredDimension(M, M2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        i(i, i2);
        ee();
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        boolean z;
        super.onDraw(canvas);
        canvas.drawOval(this.jD, this.jB);
        if (this.jF != this.jG) {
            this.jF = Math.min(this.jF + ((((float) (SystemClock.uptimeMillis() - this.jE)) / 1000.0f) * this.jH), this.jG);
            this.jE = SystemClock.uptimeMillis();
            z = true;
        } else {
            z = false;
        }
        canvas.drawArc(this.jD, -90.0f, isInEditMode() ? 360.0f : this.jF, false, this.jA);
        this.jC.setColor(-1);
        this.jC.setTextSize((float) this.uiUtils.M(12));
        this.jC.setTextAlign(Align.CENTER);
        this.jC.setAntiAlias(true);
        canvas.drawText(String.valueOf(this.jJ), (float) ((int) this.jD.centerX()), (float) ((int) (this.jD.centerY() - ((this.jC.descent() + this.jC.ascent()) / 2.0f))), this.jC);
        if (z) {
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(view, i);
        if (i == 0) {
            this.jE = SystemClock.uptimeMillis();
        }
    }

    private void ee() {
        this.jA.setColor(-1);
        this.jA.setAntiAlias(true);
        this.jA.setStyle(Style.STROKE);
        this.jA.setStrokeWidth((float) this.uiUtils.M(1));
        this.jB.setColor(-2013265920);
        this.jB.setAntiAlias(true);
        this.jB.setStyle(Style.FILL);
        this.jB.setStrokeWidth((float) this.uiUtils.M(4));
    }

    private void i(int i, int i2) {
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        this.jD = new RectF((float) (getPaddingLeft() + this.uiUtils.M(1)), (float) (paddingTop + this.uiUtils.M(1)), (float) ((i - getPaddingRight()) - this.uiUtils.M(1)), (float) ((i2 - paddingBottom) - this.uiUtils.M(1)));
    }
}
