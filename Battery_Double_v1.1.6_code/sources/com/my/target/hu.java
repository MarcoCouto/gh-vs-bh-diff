package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.widget.ImageView;
import com.my.target.common.models.ImageData;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

/* compiled from: ImageLoader */
public class hu {
    /* access modifiers changed from: private */
    @NonNull
    public static final WeakHashMap<ImageView, ImageData> nB = new WeakHashMap<>();
    private boolean ek;
    @NonNull
    private final List<ImageData> nC;
    /* access modifiers changed from: private */
    @Nullable
    public a nD;

    /* compiled from: ImageLoader */
    public interface a {
        void ac();
    }

    @NonNull
    public static hu b(@NonNull ImageData imageData) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(imageData);
        return new hu(arrayList);
    }

    @NonNull
    public static hu e(@NonNull List<ImageData> list) {
        return new hu(list);
    }

    @UiThread
    public static void a(@NonNull final ImageData imageData, @NonNull ImageView imageView) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            ah.b("[ImageLoader] method loadAndDisplay called from worker thread");
        } else if (nB.get(imageView) != imageData) {
            nB.remove(imageView);
            if (imageData.getBitmap() != null) {
                a(imageData.getBitmap(), imageView);
                return;
            }
            nB.put(imageView, imageData);
            final WeakReference weakReference = new WeakReference(imageView);
            b(imageData).a((a) new a() {
                public void ac() {
                    ImageView imageView = (ImageView) weakReference.get();
                    if (imageView != null && imageData == ((ImageData) hu.nB.get(imageView))) {
                        hu.nB.remove(imageView);
                        Bitmap bitmap = imageData.getBitmap();
                        if (bitmap != null) {
                            hu.a(bitmap, imageView);
                        }
                    }
                }
            }).L(imageView.getContext());
        }
    }

    @UiThread
    public static void b(@NonNull ImageData imageData, @NonNull ImageView imageView) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            ah.b("[ImageLoader] method cancel called from worker thread");
            return;
        }
        if (nB.get(imageView) == imageData) {
            nB.remove(imageView);
        }
    }

    /* access modifiers changed from: private */
    public static void a(@NonNull Bitmap bitmap, @NonNull ImageView imageView) {
        if (imageView instanceof ge) {
            ((ge) imageView).b(bitmap, true);
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    @NonNull
    public hu a(@Nullable a aVar) {
        this.nD = aVar;
        return this;
    }

    @NonNull
    public hu G(boolean z) {
        this.ek = z;
        return this;
    }

    private hu(@NonNull List<ImageData> list) {
        this.nC = list;
    }

    public void L(@NonNull Context context) {
        if (this.nC.isEmpty()) {
            finish();
            return;
        }
        final Context applicationContext = context.getApplicationContext();
        ai.a(new Runnable() {
            public void run() {
                hu.this.M(applicationContext);
                hu.this.finish();
            }
        });
    }

    @WorkerThread
    public void M(@NonNull Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            ah.b("[ImageLoader] method loadSync called from main thread");
            return;
        }
        Context applicationContext = context.getApplicationContext();
        dl cB = this.ek ? dl.cB() : dl.cA();
        for (ImageData imageData : this.nC) {
            if (imageData.getBitmap() == null) {
                Bitmap bitmap = (Bitmap) cB.f(imageData.getUrl(), applicationContext);
                if (bitmap != null) {
                    imageData.setData(bitmap);
                    if (imageData.getHeight() == 0 || imageData.getWidth() == 0) {
                        imageData.setHeight(bitmap.getHeight());
                        imageData.setWidth(bitmap.getWidth());
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void finish() {
        if (this.nD != null) {
            ai.c(new Runnable() {
                public void run() {
                    if (hu.this.nD != null) {
                        hu.this.nD.ac();
                        hu.this.nD = null;
                    }
                }
            });
        }
    }
}
