package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.common.MyTargetPrivacy;
import com.my.target.mediation.MediationAdapter;
import com.my.target.mediation.MediationInterstitialAdAdapter;
import com.my.target.mediation.MediationInterstitialAdAdapter.MediationInterstitialAdListener;
import com.my.target.mediation.MyTargetInterstitialAdAdapter;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.List;

/* compiled from: MediationInterstitialAdEngine */
public class ay extends ax<MediationInterstitialAdAdapter> implements an {
    @NonNull
    final InterstitialAd ad;
    @NonNull
    private final a adConfig;

    /* compiled from: MediationInterstitialAdEngine */
    class a implements MediationInterstitialAdListener {
        @NonNull
        private final cu bu;

        a(cu cuVar) {
            this.bu = cuVar;
        }

        public void onLoad(@NonNull MediationInterstitialAdAdapter mediationInterstitialAdAdapter) {
            if (ay.this.bl == mediationInterstitialAdAdapter) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationInterstitialAdEngine: data from ");
                sb.append(this.bu.getName());
                sb.append(" ad network loaded successfully");
                ah.a(sb.toString());
                ay.this.a(this.bu, true);
                InterstitialAdListener listener = ay.this.ad.getListener();
                if (listener != null) {
                    listener.onLoad(ay.this.ad);
                }
            }
        }

        public void onNoAd(@NonNull String str, @NonNull MediationInterstitialAdAdapter mediationInterstitialAdAdapter) {
            if (ay.this.bl == mediationInterstitialAdAdapter) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationInterstitialAdEngine: no data from ");
                sb.append(this.bu.getName());
                sb.append(" ad network");
                ah.a(sb.toString());
                ay.this.a(this.bu, false);
            }
        }

        public void onClick(@NonNull MediationInterstitialAdAdapter mediationInterstitialAdAdapter) {
            if (ay.this.bl == mediationInterstitialAdAdapter) {
                Context context = ay.this.getContext();
                if (context != null) {
                    ib.a((List<dh>) this.bu.getStatHolder().N(String.CLICK), context);
                }
                InterstitialAdListener listener = ay.this.ad.getListener();
                if (listener != null) {
                    listener.onClick(ay.this.ad);
                }
            }
        }

        public void onDismiss(@NonNull MediationInterstitialAdAdapter mediationInterstitialAdAdapter) {
            if (ay.this.bl == mediationInterstitialAdAdapter) {
                InterstitialAdListener listener = ay.this.ad.getListener();
                if (listener != null) {
                    listener.onDismiss(ay.this.ad);
                }
            }
        }

        public void onVideoCompleted(@NonNull MediationInterstitialAdAdapter mediationInterstitialAdAdapter) {
            if (ay.this.bl == mediationInterstitialAdAdapter) {
                InterstitialAdListener listener = ay.this.ad.getListener();
                if (listener != null) {
                    listener.onVideoCompleted(ay.this.ad);
                }
            }
        }

        public void onDisplay(@NonNull MediationInterstitialAdAdapter mediationInterstitialAdAdapter) {
            if (ay.this.bl == mediationInterstitialAdAdapter) {
                Context context = ay.this.getContext();
                if (context != null) {
                    ib.a((List<dh>) this.bu.getStatHolder().N("playbackStarted"), context);
                }
                InterstitialAdListener listener = ay.this.ad.getListener();
                if (listener != null) {
                    listener.onDisplay(ay.this.ad);
                }
            }
        }
    }

    @NonNull
    public static ay a(@NonNull InterstitialAd interstitialAd, @NonNull ct ctVar, @NonNull a aVar) {
        return new ay(interstitialAd, ctVar, aVar);
    }

    private ay(@NonNull InterstitialAd interstitialAd, @NonNull ct ctVar, @NonNull a aVar) {
        super(ctVar);
        this.ad = interstitialAd;
        this.adConfig = aVar;
    }

    public void k(@NonNull Context context) {
        if (this.bl == null) {
            ah.b("MediationInterstitialAdEngine error: can't show ad, adapter is not set");
            return;
        }
        try {
            ((MediationInterstitialAdAdapter) this.bl).show(context);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationInterstitialAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
    }

    public void l(@NonNull Context context) {
        ah.b("MediationInterstitialAdEngine error: show interstitial ad in dialog is not supported in mediation mode");
    }

    public void dismiss() {
        if (this.bl == null) {
            ah.b("MediationInterstitialAdEngine error: can't dismiss ad, adapter is not set");
            return;
        }
        try {
            ((MediationInterstitialAdAdapter) this.bl).dismiss();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationInterstitialAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
    }

    public void destroy() {
        if (this.bl == null) {
            ah.b("MediationInterstitialAdEngine error: can't destroy ad, adapter is not set");
            return;
        }
        try {
            ((MediationInterstitialAdAdapter) this.bl).destroy();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationInterstitialAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
        this.bl = null;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: aq */
    public MediationInterstitialAdAdapter an() {
        return new MyTargetInterstitialAdAdapter();
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull MediationInterstitialAdAdapter mediationInterstitialAdAdapter, @NonNull cu cuVar, @NonNull Context context) {
        a a2 = a.a(cuVar.getPlacementId(), cuVar.getPayload(), cuVar.bG(), this.adConfig.getCustomParams().getAge(), this.adConfig.getCustomParams().getGender(), MyTargetPrivacy.isConsentSpecified(), MyTargetPrivacy.isUserConsent(), MyTargetPrivacy.isUserAgeRestricted(), this.adConfig.isTrackingLocationEnabled(), this.adConfig.isTrackingEnvironmentEnabled());
        if (mediationInterstitialAdAdapter instanceof MyTargetInterstitialAdAdapter) {
            cv bH = cuVar.bH();
            if (bH instanceof cy) {
                ((MyTargetInterstitialAdAdapter) mediationInterstitialAdAdapter).setSection((cy) bH);
            }
        }
        try {
            mediationInterstitialAdAdapter.load(a2, new a(cuVar), context);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationInterstitialAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public void ao() {
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onNoAd("No data for available ad networks", this.ad);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull MediationAdapter mediationAdapter) {
        return mediationAdapter instanceof MediationInterstitialAdAdapter;
    }
}
