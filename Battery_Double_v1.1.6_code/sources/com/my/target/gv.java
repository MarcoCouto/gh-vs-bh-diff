package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;

@SuppressLint({"ViewConstructor"})
/* compiled from: PromoDefaultStyleView */
public class gv extends RelativeLayout implements gs {
    private static final int MEDIA_ID = ic.eG();
    private static final int kA = ic.eG();
    private static final int kB = ic.eG();
    private static final int kp = ic.eG();
    private static final int kz = ic.eG();
    @NonNull
    private final fz eR;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.er.a fc;
    private float fz;
    @NonNull
    private final ge iconImageView;
    private final int ix;
    /* access modifiers changed from: private */
    @NonNull
    public final fz jm;
    @Nullable
    private final Bitmap jt;
    @Nullable
    private final Bitmap ju;
    @NonNull
    private final a kC;
    @NonNull
    private final gy kD;
    @NonNull
    private final gw kE;
    /* access modifiers changed from: private */
    @NonNull
    public final gu kF;
    @NonNull
    private final gi kG;
    private final int kH;
    private final int kI;
    private final int kJ;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.gt.a kK;
    @NonNull
    private final ic uiUtils;

    /* compiled from: PromoDefaultStyleView */
    class a implements OnClickListener {
        a() {
        }

        public void onClick(View view) {
            if (view.isEnabled() && gv.this.kK != null) {
                gv.this.kK.dt();
            }
        }
    }

    public void finish() {
    }

    @NonNull
    public View getView() {
        return this;
    }

    public gv(@NonNull Context context, boolean z) {
        super(context);
        boolean z2 = (getContext().getResources().getConfiguration().screenLayout & 15) >= 3;
        this.uiUtils = ic.P(context);
        this.iconImageView = new ge(context);
        this.iconImageView.setId(kp);
        this.kD = new gy(context, this.uiUtils, z2);
        this.kD.setId(kz);
        this.kE = new gw(context, this.uiUtils, z2, z);
        this.kE.setId(MEDIA_ID);
        this.eR = new fz(context);
        this.eR.setId(kB);
        this.kG = new gi(context);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(3, MEDIA_ID);
        LayoutParams layoutParams2 = new LayoutParams(-1, -2);
        layoutParams2.addRule(14, -1);
        this.kF = new gu(context, this.uiUtils);
        LayoutParams layoutParams3 = new LayoutParams(-1, -2);
        layoutParams3.addRule(12, -1);
        this.kF.setLayoutParams(layoutParams3);
        this.kF.setId(kA);
        this.jm = new fz(context);
        this.jm.setId(je);
        this.jt = fm.C(this.uiUtils.M(28));
        this.ju = fm.D(this.uiUtils.M(28));
        this.kC = new a();
        this.ix = this.uiUtils.M(64);
        this.kH = this.uiUtils.M(20);
        ic.a((View) this.iconImageView, "icon_image");
        ic.a((View) this.jm, "sound_button");
        ic.a((View) this.kD, "vertical_view");
        ic.a((View) this.kE, "media_view");
        ic.a((View) this.kF, "panel_view");
        ic.a((View) this.eR, "close_button");
        ic.a((View) this.kG, "progress_wheel");
        addView(this.kF, 0);
        addView(this.iconImageView, 0);
        addView(this.kD, 0, layoutParams);
        addView(this.kE, 0, layoutParams2);
        addView(this.jm);
        addView(this.eR);
        addView(this.kG);
        this.kI = this.uiUtils.M(28);
        this.kJ = this.uiUtils.M(10);
    }

    public void setBanner(@NonNull cn cnVar) {
        int i;
        int i2;
        this.kG.setVisibility(8);
        LayoutParams layoutParams = new LayoutParams(this.kI, this.uiUtils.M(28));
        layoutParams.addRule(9);
        layoutParams.topMargin = this.uiUtils.M(10);
        layoutParams.leftMargin = this.uiUtils.M(10);
        this.kG.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(11);
        this.eR.setVisibility(8);
        co videoBanner = cnVar.getVideoBanner();
        if (videoBanner == null) {
            this.jm.setVisibility(8);
        }
        this.eR.setLayoutParams(layoutParams2);
        WindowManager windowManager = (WindowManager) getContext().getSystemService("window");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        boolean z = displayMetrics.widthPixels + displayMetrics.heightPixels < 1280 || c(cnVar);
        this.kF.initView();
        this.kF.setBanner(cnVar);
        this.kD.a(displayMetrics.widthPixels, displayMetrics.heightPixels, z);
        this.kD.setBanner(cnVar);
        this.kE.initView();
        this.kE.a(cnVar, 0);
        ImageData closeIcon = cnVar.getCloseIcon();
        if (closeIcon == null || closeIcon.getData() == null) {
            Bitmap B = fl.B(this.uiUtils.M(28));
            if (B != null) {
                this.eR.a(B, false);
            }
        } else {
            this.eR.a(closeIcon.getData(), true);
        }
        ImageData icon = cnVar.getIcon();
        if (icon != null) {
            i2 = icon.getWidth();
            i = icon.getHeight();
        } else {
            i2 = 0;
            i = 0;
        }
        LayoutParams layoutParams3 = new LayoutParams(-2, -2);
        layoutParams3.bottomMargin = this.uiUtils.M(4);
        if (!(i2 == 0 || i == 0)) {
            int M = (int) (((float) this.uiUtils.M(64)) * (((float) i) / ((float) i2)));
            layoutParams3.width = this.ix;
            layoutParams3.height = M;
            if (!z) {
                layoutParams3.bottomMargin = (-M) / 2;
            }
        }
        layoutParams3.addRule(8, MEDIA_ID);
        if (VERSION.SDK_INT >= 17) {
            layoutParams3.setMarginStart(this.uiUtils.M(20));
        } else {
            layoutParams3.leftMargin = this.uiUtils.M(20);
        }
        this.iconImageView.setLayoutParams(layoutParams3);
        if (icon != null) {
            this.iconImageView.setImageBitmap(icon.getData());
        }
        if (videoBanner != null && videoBanner.isAutoPlay()) {
            this.kE.dm();
            post(new Runnable() {
                public void run() {
                    gv.this.kF.a(gv.this.jm);
                }
            });
        }
        if (videoBanner != null) {
            this.fz = videoBanner.getDuration();
        }
        fz fzVar = this.jm;
        fzVar.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (gv.this.fc != null) {
                    gv.this.fc.cZ();
                }
            }
        });
        fzVar.a(this.jt, false);
        fzVar.setContentDescription("sound_on");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002b A[ADDED_TO_REGION] */
    private boolean c(@NonNull cn cnVar) {
        int i;
        int i2;
        co videoBanner = cnVar.getVideoBanner();
        boolean z = false;
        if (videoBanner != null) {
            VideoData videoData = (VideoData) videoBanner.getMediaData();
            if (videoData != null) {
                i2 = videoData.getHeight();
                i = videoData.getWidth();
                if (i2 > 0 || i <= 0) {
                    return false;
                }
                if (i2 > i || ((float) i) / ((float) i2) < 1.4f) {
                    z = true;
                }
                return z;
            }
        } else {
            ImageData image = cnVar.getImage();
            if (image != null) {
                i2 = image.getHeight();
                i = image.getWidth();
                if (i2 > 0) {
                }
                return false;
            }
        }
        i = 0;
        i2 = 0;
        if (i2 > 0) {
        }
        return false;
    }

    public void destroy() {
        this.kE.destroy();
    }

    public void a(@NonNull cn cnVar) {
        this.jm.setVisibility(8);
        this.eR.setVisibility(0);
        stop(false);
        this.kE.a(cnVar);
    }

    public boolean isPaused() {
        return this.kE.isPaused();
    }

    @NonNull
    public View getCloseButton() {
        return this.eR;
    }

    public void play() {
        this.kF.c(this.jm);
        this.kE.dm();
    }

    public void resume() {
        this.kF.c(this.jm);
        this.kE.resume();
    }

    @NonNull
    public gw getPromoMediaView() {
        return this.kE;
    }

    public void stop(boolean z) {
        this.kG.setVisibility(8);
        this.kF.d(this.jm);
        this.kE.B(z);
    }

    public void setClickArea(@NonNull ca caVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Apply click area ");
        sb.append(caVar.bm());
        sb.append(" to view");
        ah.a(sb.toString());
        if (caVar.de || caVar.f456do) {
            this.iconImageView.setOnClickListener(this.kC);
        } else {
            this.iconImageView.setOnClickListener(null);
        }
        this.kD.a(caVar, this.kC);
        this.kF.a(caVar, (OnClickListener) this.kC);
        if (caVar.df || caVar.f456do) {
            this.kE.getClickableLayout().setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (gv.this.kK != null) {
                        gv.this.kK.dt();
                    }
                }
            });
            return;
        }
        this.kE.getClickableLayout().setOnClickListener(null);
        this.kE.getClickableLayout().setEnabled(false);
    }

    public void setMediaListener(com.my.target.er.a aVar) {
        this.fc = aVar;
        this.kE.setInterstitialPromoViewListener(aVar);
        this.kE.ek();
    }

    public void ej() {
        this.eR.setVisibility(0);
    }

    public void setTimeChanged(float f) {
        this.kG.setVisibility(0);
        if (this.fz > 0.0f) {
            this.kG.setProgress(f / this.fz);
        }
        this.kG.setDigit((int) ((this.fz - f) + 1.0f));
    }

    public boolean isPlaying() {
        return this.kE.isPlaying();
    }

    public void pause() {
        this.kF.d(this.jm);
        this.kE.pause();
    }

    public void G(int i) {
        this.kE.G(i);
    }

    public void ei() {
        this.kE.ei();
    }

    public final void A(boolean z) {
        if (z) {
            this.jm.a(this.ju, false);
            this.jm.setContentDescription("sound_off");
            return;
        }
        this.jm.a(this.jt, false);
        this.jm.setContentDescription("sound_on");
    }

    public void setInterstitialPromoViewListener(@Nullable com.my.target.gt.a aVar) {
        this.kK = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.jm.measure(i, i2);
        this.eR.measure(i, i2);
        this.kG.measure(MeasureSpec.makeMeasureSpec(this.kI, 1073741824), MeasureSpec.makeMeasureSpec(this.kI, 1073741824));
        if (size2 > size) {
            this.kE.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.kD.measure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2 - this.kE.getMeasuredHeight(), Integer.MIN_VALUE));
            this.iconImageView.measure(MeasureSpec.makeMeasureSpec(this.ix, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        } else {
            this.kE.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.kF.measure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        }
        setMeasuredDimension(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.eR.layout(i3 - this.eR.getMeasuredWidth(), 0, i3, this.eR.getMeasuredHeight());
        this.kG.layout(this.kJ, this.kJ, this.kG.getMeasuredWidth() + this.kJ, this.kG.getMeasuredHeight() + this.kJ);
        if (i4 > i3) {
            if (this.jm.getTranslationY() > 0.0f) {
                this.jm.setTranslationY(0.0f);
            }
            setBackgroundColor(-1);
            int measuredWidth = (i3 - this.kE.getMeasuredWidth()) / 2;
            this.kE.layout(measuredWidth, 0, this.kE.getMeasuredWidth() + measuredWidth, this.kE.getMeasuredHeight());
            this.kD.layout(0, this.kE.getBottom(), i3, i4);
            int i5 = this.kH;
            if (this.kE.getMeasuredHeight() != 0) {
                i5 = this.kE.getBottom() - (this.iconImageView.getMeasuredHeight() / 2);
            }
            this.iconImageView.layout(this.kH, i5, this.kH + this.iconImageView.getMeasuredWidth(), this.iconImageView.getMeasuredHeight() + i5);
            this.kF.layout(0, 0, 0, 0);
            this.jm.layout(i3 - this.jm.getMeasuredWidth(), this.kE.getBottom() - this.jm.getMeasuredHeight(), i3, this.kE.getBottom());
            return;
        }
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        int measuredWidth2 = (i3 - this.kE.getMeasuredWidth()) / 2;
        int measuredHeight = (i4 - this.kE.getMeasuredHeight()) / 2;
        this.kE.layout(measuredWidth2, measuredHeight, this.kE.getMeasuredWidth() + measuredWidth2, this.kE.getMeasuredHeight() + measuredHeight);
        this.iconImageView.layout(0, 0, 0, 0);
        this.kD.layout(0, 0, 0, 0);
        this.kF.layout(0, i4 - this.kF.getMeasuredHeight(), i3, i4);
        this.jm.layout(i3 - this.jm.getMeasuredWidth(), this.kF.getTop() - this.jm.getMeasuredHeight(), i3, this.kF.getTop());
        if (this.kE.isPlaying()) {
            this.kF.a(this.jm);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        boolean z = this.fz <= 0.0f || isHardwareAccelerated();
        if (this.kK != null) {
            this.kK.s(z);
        }
    }
}
