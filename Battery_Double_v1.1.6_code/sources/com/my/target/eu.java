package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

/* compiled from: InterstitialPresenter */
public interface eu {

    /* compiled from: InterstitialPresenter */
    public interface a {
        void a(@NonNull ch chVar, @NonNull Context context);

        void aj();

        void b(@Nullable ch chVar, @Nullable String str, @NonNull Context context);
    }

    @NonNull
    View cT();

    void destroy();

    void pause();

    void resume();

    void stop();
}
