package com.my.target;

/* compiled from: JsExpandEvent */
public class br extends bl {
    private int height = 0;
    private int width = 0;

    public br() {
        super("onExpand");
    }

    public br(int i, int i2) {
        super("onExpand");
        this.width = i;
        this.height = i2;
    }
}
