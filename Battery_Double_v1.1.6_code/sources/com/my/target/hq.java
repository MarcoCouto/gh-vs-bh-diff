package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: ArrayUtils */
public class hq {
    @NonNull
    public static String a(@Nullable String[] strArr) {
        String str = null;
        if (strArr != null) {
            for (String str2 : strArr) {
                if (str == null) {
                    str = str2;
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(",");
                    sb.append(str2);
                    str = sb.toString();
                }
            }
        }
        return str == null ? "" : str;
    }
}
