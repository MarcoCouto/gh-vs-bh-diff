package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

/* compiled from: NativeAppwallAdSection */
public class dc extends cv {
    @NonNull
    private final ArrayList<cr> banners = new ArrayList<>();
    private boolean cQ;
    @NonNull
    private final ArrayList<Pair<String, String>> dS = new ArrayList<>();
    @Nullable
    private JSONObject dT;
    @Nullable
    private String dU;
    @Nullable
    private String dV;
    @Nullable
    private String dW;
    @Nullable
    private String dX;
    @Nullable
    private String dY;
    @NonNull
    private final String name;
    @Nullable
    private String title;

    @NonNull
    public static dc C(@NonNull String str) {
        return new dc(str);
    }

    private dc(@NonNull String str) {
        this.name = str;
    }

    public void d(@Nullable JSONObject jSONObject) {
        this.dT = jSONObject;
    }

    @Nullable
    public JSONObject cd() {
        return this.dT;
    }

    public boolean aZ() {
        return this.cQ;
    }

    public void n(boolean z) {
        this.cQ = z;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    public void a(@NonNull cr crVar) {
        this.banners.add(crVar);
    }

    @NonNull
    public List<cr> bT() {
        return new ArrayList(this.banners);
    }

    public int getBannersCount() {
        return this.banners.size();
    }

    public void setTitle(@Nullable String str) {
        this.title = str;
    }

    public void D(@Nullable String str) {
        this.dU = str;
    }

    public void E(@Nullable String str) {
        this.dV = str;
    }

    public void F(@Nullable String str) {
        this.dW = str;
    }

    public void G(@Nullable String str) {
        this.dX = str;
    }

    public void H(@Nullable String str) {
        this.dY = str;
    }

    @NonNull
    public ArrayList<Pair<String, String>> ce() {
        return this.dS;
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String cf() {
        return this.dU;
    }

    @Nullable
    public String cg() {
        return this.dV;
    }

    @Nullable
    public String ch() {
        return this.dW;
    }

    @Nullable
    public String ci() {
        return this.dX;
    }

    @Nullable
    public String cj() {
        return this.dY;
    }

    @Nullable
    public String I(@NonNull String str) {
        Iterator it = this.dS.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            if (str.equals(pair.first)) {
                return (String) pair.second;
            }
        }
        return null;
    }
}
