package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.cd;
import com.my.target.common.models.AudioData;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.ShareButtonData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;

/* compiled from: MediaBanner */
public final class co<T extends cd> extends ch {
    @Nullable
    private String adText;
    private boolean allowClose = true;
    private float allowCloseDelay = 0.0f;
    private boolean allowPause = true;
    private boolean allowReplay = true;
    private boolean allowSeek = false;
    private boolean allowSkip = false;
    private boolean allowTrackChange = false;
    private boolean autoMute = false;
    private boolean autoPlay = true;
    @NonNull
    private String closeActionText = "Close";
    @NonNull
    private String closeDelayActionText = "Ad can be skipped after %ds";
    @NonNull
    private final ArrayList<ci> companionBanners = new ArrayList<>();
    private boolean hasCtaButton = true;
    @Nullable
    private T mediaData;
    private float point;
    private float pointP;
    @Nullable
    private ImageData preview;
    @NonNull
    private String replayActionText = "Replay";
    @NonNull
    private final ArrayList<ShareButtonData> shareButtonDatas = new ArrayList<>();
    private boolean showPlayerControls = true;

    @NonNull
    public static co<VideoData> newVideoBanner() {
        return newBanner();
    }

    @NonNull
    public static co<AudioData> newAudioBanner() {
        return newBanner();
    }

    @NonNull
    public static <T extends cd> co<T> newBanner() {
        return new co<>();
    }

    private co() {
    }

    public void setAutoMute(boolean z) {
        this.autoMute = z;
    }

    public void setAutoPlay(boolean z) {
        this.autoPlay = z;
    }

    public void setCloseActionText(@NonNull String str) {
        this.closeActionText = str;
    }

    public void setHasCtaButton(boolean z) {
        this.hasCtaButton = z;
    }

    public void setPreview(@Nullable ImageData imageData) {
        this.preview = imageData;
    }

    public void setReplayActionText(@NonNull String str) {
        this.replayActionText = str;
    }

    public void setShowPlayerControls(boolean z) {
        this.showPlayerControls = z;
    }

    public void setMediaData(@Nullable T t) {
        this.mediaData = t;
    }

    @Nullable
    public T getMediaData() {
        return this.mediaData;
    }

    @NonNull
    public String getCloseActionText() {
        return this.closeActionText;
    }

    @NonNull
    public String getReplayActionText() {
        return this.replayActionText;
    }

    public boolean isAutoMute() {
        return this.autoMute;
    }

    public boolean isShowPlayerControls() {
        return this.showPlayerControls;
    }

    public boolean isAutoPlay() {
        return this.autoPlay;
    }

    public boolean isHasCtaButton() {
        return this.hasCtaButton;
    }

    @Nullable
    public ImageData getPreview() {
        return this.preview;
    }

    public boolean isAllowReplay() {
        return this.allowReplay;
    }

    public void setAllowReplay(boolean z) {
        this.allowReplay = z;
    }

    public void setAllowCloseDelay(float f) {
        this.allowCloseDelay = f;
    }

    public float getAllowCloseDelay() {
        return this.allowCloseDelay;
    }

    public void setAllowClose(boolean z) {
        this.allowClose = z;
    }

    public boolean isAllowClose() {
        return this.allowClose;
    }

    public float getPoint() {
        return this.point;
    }

    public float getPointP() {
        return this.pointP;
    }

    public void setPoint(float f) {
        this.point = f;
    }

    public void setPointP(float f) {
        this.pointP = f;
    }

    public int getWidth() {
        if (this.mediaData != null) {
            return this.mediaData.getWidth();
        }
        return 0;
    }

    public int getHeight() {
        if (this.mediaData != null) {
            return this.mediaData.getHeight();
        }
        return 0;
    }

    @Nullable
    public String getAdText() {
        return this.adText;
    }

    public void setAdText(@Nullable String str) {
        this.adText = str;
    }

    public boolean isAllowSeek() {
        return this.allowSeek;
    }

    public void setAllowSeek(boolean z) {
        this.allowSeek = z;
    }

    public boolean isAllowSkip() {
        return this.allowSkip;
    }

    public void setAllowSkip(boolean z) {
        this.allowSkip = z;
    }

    public boolean isAllowTrackChange() {
        return this.allowTrackChange;
    }

    public void setAllowTrackChange(boolean z) {
        this.allowTrackChange = z;
    }

    @NonNull
    public ArrayList<ci> getCompanionBanners() {
        return new ArrayList<>(this.companionBanners);
    }

    @NonNull
    public ArrayList<ShareButtonData> getShareButtonDatas() {
        return new ArrayList<>(this.shareButtonDatas);
    }

    public void addCompanion(@NonNull ci ciVar) {
        this.companionBanners.add(ciVar);
    }

    public boolean isAllowPause() {
        return this.allowPause;
    }

    public void setAllowPause(boolean z) {
        this.allowPause = z;
    }

    @NonNull
    public String getCloseDelayActionText() {
        return this.closeDelayActionText;
    }

    public void setCloseDelayActionText(@NonNull String str) {
        this.closeDelayActionText = str;
    }

    public void addShareButtonData(@NonNull ShareButtonData shareButtonData) {
        this.shareButtonDatas.add(shareButtonData);
    }
}
