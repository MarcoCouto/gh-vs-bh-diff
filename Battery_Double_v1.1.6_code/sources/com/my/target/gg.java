package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.TextView;

/* compiled from: TextViewWithAgeView */
public class gg extends ViewGroup {
    private static final int iQ = ic.eG();
    private static final int iR = ic.eG();
    @NonNull
    private final TextView iS;
    @NonNull
    private final fu iT;
    private final int iU;
    private final int iV;

    public gg(@NonNull Context context) {
        super(context);
        ic P = ic.P(context);
        this.iS = new TextView(context);
        this.iT = new fu(context);
        this.iS.setId(iQ);
        this.iT.setId(iR);
        this.iT.setLines(1);
        this.iS.setTextSize(2, 18.0f);
        this.iS.setEllipsize(TruncateAt.END);
        this.iS.setMaxLines(1);
        this.iS.setTextColor(-1);
        this.iU = P.M(4);
        this.iV = P.M(2);
        ic.a((View) this.iS, "title_text");
        ic.a((View) this.iT, "age_bordering");
        addView(this.iS);
        addView(this.iT);
    }

    @NonNull
    public TextView getLeftText() {
        return this.iS;
    }

    @NonNull
    public fu getRightBorderedView() {
        return this.iT;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.iT.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2 - (this.iV * 2), Integer.MIN_VALUE));
        int i3 = size / 2;
        if (this.iT.getMeasuredWidth() > i3) {
            this.iT.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2 - (this.iV * 2), Integer.MIN_VALUE));
        }
        this.iS.measure(MeasureSpec.makeMeasureSpec((size - this.iT.getMeasuredWidth()) - this.iU, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2 - (this.iV * 2), Integer.MIN_VALUE));
        setMeasuredDimension(this.iS.getMeasuredWidth() + this.iT.getMeasuredWidth() + this.iU, Math.max(this.iS.getMeasuredHeight(), this.iT.getMeasuredHeight()));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = this.iS.getMeasuredWidth();
        int measuredHeight = this.iS.getMeasuredHeight();
        int measuredWidth2 = this.iT.getMeasuredWidth();
        int measuredHeight2 = this.iT.getMeasuredHeight();
        int measuredHeight3 = getMeasuredHeight();
        int i5 = (measuredHeight3 - measuredHeight) / 2;
        int i6 = (measuredHeight3 - measuredHeight2) / 2;
        int i7 = this.iU + measuredWidth;
        this.iS.layout(0, i5, measuredWidth, measuredHeight + i5);
        this.iT.layout(i7, i6, measuredWidth2 + i7, measuredHeight2 + i6);
    }
}
