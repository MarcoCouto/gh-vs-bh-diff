package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InstreamAdResponseParser */
public class g extends c<cw> {
    @NonNull
    public static c<cw> f() {
        return new g();
    }

    private g() {
    }

    @Nullable
    public cw a(@NonNull String str, @NonNull bz bzVar, @Nullable cw cwVar, @NonNull a aVar, @NonNull Context context) {
        if (isVast(str)) {
            return b(str, bzVar, cwVar, aVar, context);
        }
        return c(str, bzVar, cwVar, aVar, context);
    }

    @NonNull
    private cw b(@NonNull String str, @NonNull bz bzVar, @Nullable cw cwVar, @NonNull a aVar, @NonNull Context context) {
        en a = en.a(aVar, bzVar, context);
        a.V(str);
        String bd = bzVar.bd();
        if (bd == null) {
            bd = BreakId.PREROLL;
        }
        if (cwVar == null) {
            cwVar = cw.bJ();
        }
        da x = cwVar.x(bd);
        if (x == null) {
            return cwVar;
        }
        if (!a.cO().isEmpty()) {
            a(a, x, bzVar);
        } else {
            bz cP = a.cP();
            if (cP != null) {
                cP.s(x.getName());
                int position = bzVar.getPosition();
                if (position >= 0) {
                    cP.setPosition(position);
                } else {
                    cP.setPosition(x.getBannersCount());
                }
                x.c(cP);
            }
        }
        return cwVar;
    }

    private void a(@NonNull en<VideoData> enVar, @NonNull da<VideoData> daVar, @NonNull bz bzVar) {
        daVar.d(enVar.bb());
        int position = bzVar.getPosition();
        Iterator it = enVar.cO().iterator();
        while (it.hasNext()) {
            co coVar = (co) it.next();
            Boolean be = bzVar.be();
            if (be != null) {
                coVar.setAllowClose(be.booleanValue());
            }
            float allowCloseDelay = bzVar.getAllowCloseDelay();
            if (allowCloseDelay > 0.0f) {
                coVar.setAllowCloseDelay(allowCloseDelay);
            }
            Boolean bf = bzVar.bf();
            if (bf != null) {
                coVar.setAllowPause(bf.booleanValue());
            }
            Boolean bk = bzVar.bk();
            if (bk != null) {
                coVar.setDirectLink(bk.booleanValue());
            }
            Boolean bl = bzVar.bl();
            if (bl != null) {
                coVar.setOpenInBrowser(bl.booleanValue());
            }
            coVar.setCloseActionText("Close");
            coVar.setPoint(bzVar.getPoint());
            coVar.setPointP(bzVar.getPointP());
            if (position >= 0) {
                int i = position + 1;
                daVar.a(coVar, position);
                position = i;
            } else {
                daVar.g(coVar);
            }
        }
    }

    @Nullable
    private cw c(@NonNull String str, @NonNull bz bzVar, @Nullable cw cwVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a = a(str, context);
        if (a == null) {
            return cwVar;
        }
        JSONObject optJSONObject = a.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            return cwVar;
        }
        if (cwVar == null) {
            cwVar = cw.bJ();
        }
        dy.cL().a(optJSONObject, cwVar);
        du a2 = du.a(bzVar, aVar, context);
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("sections");
        if (optJSONObject2 != null) {
            String bd = bzVar.bd();
            if (bd != null) {
                da x = cwVar.x(bd);
                if (x != null) {
                    a(optJSONObject2, a2, x, dx.c(bzVar, aVar, context), bzVar);
                }
            } else {
                Iterator it = cwVar.bK().iterator();
                while (it.hasNext()) {
                    a(optJSONObject2, a2, (da) it.next(), dx.c(bzVar, aVar, context), bzVar);
                }
            }
        }
        return cwVar;
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull du duVar, @NonNull da<VideoData> daVar, @NonNull dx dxVar, @NonNull bz bzVar) {
        da<VideoData> daVar2 = daVar;
        JSONObject jSONObject2 = jSONObject;
        JSONArray optJSONArray = jSONObject.optJSONArray(daVar.getName());
        if (optJSONArray != null) {
            int position = bzVar.getPosition();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            int i = position;
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i2);
                if (optJSONObject != null) {
                    if ("additionalData".equals(optJSONObject.optString("type"))) {
                        a(bzVar, duVar, optJSONObject, daVar, arrayList2, arrayList);
                    } else {
                        co newVideoBanner = co.newVideoBanner();
                        if (dxVar.a(optJSONObject, newVideoBanner)) {
                            float point = bzVar.getPoint();
                            if (point >= 0.0f) {
                                newVideoBanner.setPoint(point);
                            }
                            float pointP = bzVar.getPointP();
                            if (pointP >= 0.0f) {
                                newVideoBanner.setPointP(pointP);
                            }
                            if (i >= 0) {
                                int i3 = i + 1;
                                daVar2.a(newVideoBanner, i);
                                i = i3;
                            } else {
                                daVar2.g(newVideoBanner);
                            }
                        }
                    }
                }
                dx dxVar2 = dxVar;
            }
            a(arrayList2, arrayList);
        }
    }

    private void a(@NonNull bz bzVar, @NonNull du duVar, @NonNull JSONObject jSONObject, @NonNull da daVar, @NonNull ArrayList<bz> arrayList, @NonNull ArrayList<bz> arrayList2) {
        bz e = duVar.e(jSONObject);
        if (e != null) {
            e.s(daVar.getName());
            if (e.aW() != -1) {
                arrayList2.add(e);
                return;
            }
            arrayList.add(e);
            if (!e.aX() && !e.aV()) {
                bzVar.b(e);
                int position = bzVar.getPosition();
                if (position >= 0) {
                    e.setPosition(position);
                } else {
                    e.setPosition(daVar.getBannersCount());
                }
            }
            daVar.c(e);
        }
    }

    private void a(@NonNull ArrayList<bz> arrayList, @NonNull ArrayList<bz> arrayList2) {
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            bz bzVar = (bz) it.next();
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                bz bzVar2 = (bz) it2.next();
                if (bzVar.aW() == bzVar2.getId()) {
                    bzVar2.a(bzVar);
                    break;
                }
            }
        }
    }
}
