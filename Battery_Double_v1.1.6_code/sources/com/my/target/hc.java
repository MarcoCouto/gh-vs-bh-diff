package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.my.target.common.models.ImageData;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.List;

/* compiled from: CarouselView */
public class hc extends ViewGroup implements OnTouchListener, gt {
    private static final int CTA_ID = ic.eG();
    private static final int iQ = ic.eG();
    private static final int kp = ic.eG();
    private static final int kq = ic.eG();
    private static final int lE = ic.eG();
    private static final int lF = ic.eG();
    private static final int lG = ic.eG();
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final fz eR;
    @NonNull
    private final TextView hN;
    @NonNull
    private final ge iconImageView;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.gt.a kK;
    private final boolean ki;
    @NonNull
    private final HashMap<View, Boolean> kj;
    @NonNull
    private final TextView lH;
    @NonNull
    private final TextView lI;
    @NonNull
    private final hb lJ;
    private final int lK;
    private final int lL;
    private final double lM;
    private final int lp;
    @NonNull
    private final ic uiUtils;

    /* compiled from: CarouselView */
    public interface a {
        void a(@NonNull ck ckVar);

        void b(@NonNull List<ck> list);
    }

    @NonNull
    public View getView() {
        return this;
    }

    public hc(@NonNull Context context) {
        super(context);
        ic.a(this, -1, -3806472);
        this.ki = (context.getResources().getConfiguration().screenLayout & 15) >= 3;
        this.lM = this.ki ? 0.5d : 0.7d;
        this.eR = new fz(context);
        this.uiUtils = ic.P(context);
        this.hN = new TextView(context);
        this.lH = new TextView(context);
        this.lI = new TextView(context);
        this.iconImageView = new ge(context);
        this.ctaButton = new Button(context);
        this.lJ = new hb(context);
        this.eR.setId(lE);
        this.eR.setContentDescription("close");
        this.eR.setVisibility(4);
        this.iconImageView.setId(kp);
        this.iconImageView.setContentDescription(SettingsJsonConstants.APP_ICON_KEY);
        this.hN.setId(iQ);
        this.hN.setLines(1);
        this.hN.setEllipsize(TruncateAt.END);
        this.lH.setId(lG);
        this.lH.setLines(1);
        this.lH.setEllipsize(TruncateAt.END);
        this.lI.setId(kq);
        this.lI.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.ctaButton.setId(CTA_ID);
        this.ctaButton.setPadding(this.uiUtils.M(15), this.uiUtils.M(10), this.uiUtils.M(15), this.uiUtils.M(10));
        this.ctaButton.setMinimumWidth(this.uiUtils.M(100));
        this.ctaButton.setMaxEms(12);
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setSingleLine();
        this.ctaButton.setTextSize(18.0f);
        this.ctaButton.setEllipsize(TruncateAt.END);
        if (VERSION.SDK_INT >= 21) {
            this.ctaButton.setElevation((float) this.uiUtils.M(2));
        }
        ic.a(this.ctaButton, -16733198, -16746839, this.uiUtils.M(2));
        this.ctaButton.setTextColor(-1);
        this.lJ.setId(lF);
        this.lJ.setPadding(0, 0, 0, this.uiUtils.M(8));
        this.lJ.setSideSlidesMargins(this.uiUtils.M(10));
        if (this.ki) {
            this.lK = this.uiUtils.M(18);
            this.lp = this.lK;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowManager = (WindowManager) context.getSystemService("window");
            if (windowManager != null) {
                windowManager.getDefaultDisplay().getMetrics(displayMetrics);
            }
            this.hN.setTextSize((float) this.uiUtils.N(24));
            this.lI.setTextSize((float) this.uiUtils.N(20));
            this.lH.setTextSize((float) this.uiUtils.N(20));
            this.lL = this.uiUtils.M(96);
            this.hN.setTypeface(null, 1);
        } else {
            this.lp = this.uiUtils.M(12);
            this.lK = this.uiUtils.M(10);
            this.hN.setTextSize(22.0f);
            this.lI.setTextSize(18.0f);
            this.lH.setTextSize(18.0f);
            this.lL = this.uiUtils.M(64);
        }
        ic.a((View) this, "ad_view");
        ic.a((View) this.hN, "title_text");
        ic.a((View) this.lI, "description_text");
        ic.a((View) this.iconImageView, "icon_image");
        ic.a((View) this.eR, "close_button");
        ic.a((View) this.lH, "category_text");
        addView(this.lJ);
        addView(this.iconImageView);
        addView(this.hN);
        addView(this.lH);
        addView(this.lI);
        addView(this.eR);
        addView(this.ctaButton);
        this.kj = new HashMap<>();
    }

    public void setBanner(@NonNull cn cnVar) {
        ImageData closeIcon = cnVar.getCloseIcon();
        if (closeIcon == null || closeIcon.getData() == null) {
            Bitmap B = fl.B(this.uiUtils.M(28));
            if (B != null) {
                this.eR.a(B, false);
            }
        } else {
            this.eR.a(closeIcon.getData(), true);
        }
        this.ctaButton.setText(cnVar.getCtaText());
        ImageData icon = cnVar.getIcon();
        if (icon != null) {
            this.iconImageView.setPlaceholderHeight(icon.getHeight());
            this.iconImageView.setPlaceholderWidth(icon.getWidth());
            hu.a(icon, (ImageView) this.iconImageView);
        }
        this.hN.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.hN.setText(cnVar.getTitle());
        String category = cnVar.getCategory();
        String subCategory = cnVar.getSubCategory();
        String str = "";
        if (!TextUtils.isEmpty(category)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(category);
            str = sb.toString();
        }
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(subCategory)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(", ");
            str = sb2.toString();
        }
        if (!TextUtils.isEmpty(subCategory)) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(subCategory);
            str = sb3.toString();
        }
        if (!TextUtils.isEmpty(str)) {
            this.lH.setText(str);
            this.lH.setVisibility(0);
        } else {
            this.lH.setVisibility(8);
        }
        this.lI.setText(cnVar.getDescription());
        this.lJ.d(cnVar.getInterstitialAdCards());
    }

    public void setInterstitialPromoViewListener(@Nullable com.my.target.gt.a aVar) {
        this.kK = aVar;
    }

    public void setCarouselListener(@Nullable a aVar) {
        this.lJ.setCarouselListener(aVar);
    }

    public void ej() {
        this.eR.setVisibility(0);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void setClickArea(@NonNull ca caVar) {
        boolean z = true;
        if (caVar.f456do) {
            setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (hc.this.kK != null) {
                        hc.this.kK.dt();
                    }
                }
            });
            ic.a(this, -1, -3806472);
            setClickable(true);
            return;
        }
        this.hN.setOnTouchListener(this);
        this.lH.setOnTouchListener(this);
        this.iconImageView.setOnTouchListener(this);
        this.lI.setOnTouchListener(this);
        this.ctaButton.setOnTouchListener(this);
        setOnTouchListener(this);
        this.kj.put(this.hN, Boolean.valueOf(caVar.dc));
        this.kj.put(this.lH, Boolean.valueOf(caVar.dm));
        this.kj.put(this.iconImageView, Boolean.valueOf(caVar.de));
        this.kj.put(this.lI, Boolean.valueOf(caVar.dd));
        HashMap<View, Boolean> hashMap = this.kj;
        Button button = this.ctaButton;
        if (!caVar.dn && !caVar.di) {
            z = false;
        }
        hashMap.put(button, Boolean.valueOf(z));
        this.kj.put(this, Boolean.valueOf(caVar.dn));
    }

    @NonNull
    public View getCloseButton() {
        return this.eR;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.kj.containsKey(view)) {
            return false;
        }
        if (!((Boolean) this.kj.get(view)).booleanValue()) {
            return true;
        }
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    setBackgroundColor(-3806472);
                    break;
                case 1:
                    setBackgroundColor(-1);
                    if (this.kK != null) {
                        this.kK.dt();
                        break;
                    }
                    break;
            }
        } else {
            setBackgroundColor(-1);
        }
        return true;
    }

    @NonNull
    public int[] getNumbersOfCurrentShowingCards() {
        int findFirstVisibleItemPosition = this.lJ.getCardLayoutManager().findFirstVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = this.lJ.getCardLayoutManager().findLastCompletelyVisibleItemPosition();
        int i = 0;
        if (findFirstVisibleItemPosition == -1 || findLastCompletelyVisibleItemPosition == -1) {
            return new int[0];
        }
        int i2 = (findLastCompletelyVisibleItemPosition - findFirstVisibleItemPosition) + 1;
        int[] iArr = new int[i2];
        while (i < i2) {
            int i3 = findFirstVisibleItemPosition + 1;
            iArr[i] = findFirstVisibleItemPosition;
            i++;
            findFirstVisibleItemPosition = i3;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.eR.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        this.iconImageView.measure(MeasureSpec.makeMeasureSpec(this.lL, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(this.lL, Integer.MIN_VALUE));
        if (size2 > size || this.ki) {
            this.ctaButton.setVisibility(8);
            int measuredHeight = this.eR.getMeasuredHeight();
            if (this.ki) {
                measuredHeight = this.lK;
            }
            this.hN.measure(MeasureSpec.makeMeasureSpec((size - (this.lK * 2)) - this.iconImageView.getMeasuredWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.lH.measure(MeasureSpec.makeMeasureSpec((size - (this.lK * 2)) - this.iconImageView.getMeasuredWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.lI.measure(MeasureSpec.makeMeasureSpec(size - (this.lK * 2), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            int max = ((size2 - measuredHeight) - Math.max(this.hN.getMeasuredHeight() + this.lH.getMeasuredHeight(), this.iconImageView.getMeasuredHeight() - (this.lK * 2))) - this.lI.getMeasuredHeight();
            int i3 = size - this.lK;
            if (size2 > size && ((double) (((float) max) / ((float) size2))) > this.lM) {
                double d = (double) size2;
                double d2 = this.lM;
                Double.isNaN(d);
                max = (int) (d * d2);
            }
            if (this.ki) {
                this.lJ.measure(MeasureSpec.makeMeasureSpec(i3, 1073741824), MeasureSpec.makeMeasureSpec(max - (this.lK * 2), Integer.MIN_VALUE));
            } else {
                this.lJ.measure(MeasureSpec.makeMeasureSpec(i3, 1073741824), MeasureSpec.makeMeasureSpec(max - (this.lK * 2), 1073741824));
            }
        } else {
            this.ctaButton.setVisibility(0);
            this.ctaButton.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            int measuredWidth = this.ctaButton.getMeasuredWidth();
            int i4 = size / 2;
            if (measuredWidth > i4 - (this.lK * 2)) {
                this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i4 - (this.lK * 2), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            }
            this.hN.measure(MeasureSpec.makeMeasureSpec((((size - this.iconImageView.getMeasuredWidth()) - measuredWidth) - this.lp) - this.lK, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.lH.measure(MeasureSpec.makeMeasureSpec((((size - this.iconImageView.getMeasuredWidth()) - measuredWidth) - this.lp) - this.lK, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.lJ.measure(MeasureSpec.makeMeasureSpec(size - this.lK, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec((((size2 - Math.max(this.iconImageView.getMeasuredHeight(), Math.max(this.ctaButton.getMeasuredHeight(), this.hN.getMeasuredHeight() + this.lH.getMeasuredHeight()))) - (this.lK * 2)) - this.lJ.getPaddingBottom()) - this.lJ.getPaddingTop(), Integer.MIN_VALUE));
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        this.eR.layout(i3 - this.eR.getMeasuredWidth(), i2, i3, this.eR.getMeasuredHeight() + i2);
        if (i6 > i5 || this.ki) {
            int bottom = this.eR.getBottom();
            int measuredHeight = this.lJ.getMeasuredHeight() + Math.max(this.hN.getMeasuredHeight() + this.lH.getMeasuredHeight(), this.iconImageView.getMeasuredHeight()) + this.lI.getMeasuredHeight() + (this.lK * 2);
            if (measuredHeight < i6) {
                int i7 = (i6 - measuredHeight) / 2;
                if (i7 > bottom) {
                    bottom = i7;
                }
            }
            this.iconImageView.layout(this.lK + i, bottom, this.iconImageView.getMeasuredWidth() + i + this.lK, i2 + this.iconImageView.getMeasuredHeight() + bottom);
            this.hN.layout(this.iconImageView.getRight(), bottom, this.iconImageView.getRight() + this.hN.getMeasuredWidth(), this.hN.getMeasuredHeight() + bottom);
            this.lH.layout(this.iconImageView.getRight(), this.hN.getBottom(), this.iconImageView.getRight() + this.lH.getMeasuredWidth(), this.hN.getBottom() + this.lH.getMeasuredHeight());
            int max = Math.max(Math.max(this.iconImageView.getBottom(), this.lH.getBottom()), this.hN.getBottom());
            this.lI.layout(this.lK + i, max, this.lK + i + this.lI.getMeasuredWidth(), this.lI.getMeasuredHeight() + max);
            int max2 = Math.max(max, this.lI.getBottom()) + this.lK;
            this.lJ.layout(i + this.lK, max2, i3, this.lJ.getMeasuredHeight() + max2);
            this.lJ.C(!this.ki);
            return;
        }
        this.lJ.C(false);
        this.iconImageView.layout(this.lK, (i4 - this.lK) - this.iconImageView.getMeasuredHeight(), this.lK + this.iconImageView.getMeasuredWidth(), i4 - this.lK);
        int max3 = ((Math.max(this.iconImageView.getMeasuredHeight(), this.ctaButton.getMeasuredHeight()) - this.hN.getMeasuredHeight()) - this.lH.getMeasuredHeight()) / 2;
        if (max3 < 0) {
            max3 = 0;
        }
        this.lH.layout(this.iconImageView.getRight(), ((i4 - this.lK) - max3) - this.lH.getMeasuredHeight(), this.iconImageView.getRight() + this.lH.getMeasuredWidth(), (i4 - this.lK) - max3);
        this.hN.layout(this.iconImageView.getRight(), this.lH.getTop() - this.hN.getMeasuredHeight(), this.iconImageView.getRight() + this.hN.getMeasuredWidth(), this.lH.getTop());
        int max4 = (Math.max(this.iconImageView.getMeasuredHeight(), this.hN.getMeasuredHeight() + this.lH.getMeasuredHeight()) - this.ctaButton.getMeasuredHeight()) / 2;
        if (max4 < 0) {
            max4 = 0;
        }
        this.ctaButton.layout((i3 - this.lK) - this.ctaButton.getMeasuredWidth(), ((i4 - this.lK) - max4) - this.ctaButton.getMeasuredHeight(), i3 - this.lK, (i4 - this.lK) - max4);
        this.lJ.layout(this.lK, this.lK, i3, this.lK + this.lJ.getMeasuredHeight());
        this.lI.layout(0, 0, 0, 0);
    }
}
