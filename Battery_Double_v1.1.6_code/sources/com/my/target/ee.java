package com.my.target;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import org.json.JSONObject;

/* compiled from: JsonParser */
public class ee {
    public static int a(@NonNull JSONObject jSONObject, @NonNull String str, int i) {
        String optString = jSONObject.optString(str);
        if (!TextUtils.isEmpty(optString)) {
            try {
                return Color.parseColor(optString);
            } catch (Exception unused) {
                StringBuilder sb = new StringBuilder();
                sb.append("error parsing color ");
                sb.append(optString);
                ah.b(sb.toString());
            }
        }
        return i;
    }
}
