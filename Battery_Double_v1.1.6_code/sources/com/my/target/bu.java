package com.my.target;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.appodeal.ads.AppodealNetworks;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.ironsource.sdk.constants.Constants.ForceClosePosition;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MraidBridge */
public class bu {
    @NonNull
    private final String co;
    @NonNull
    private final WebViewClient cp = new c();
    /* access modifiers changed from: private */
    @Nullable
    public b cq;
    @Nullable
    private gc cr;
    private boolean cs;
    private boolean ct;

    @SuppressLint({"RtlHardcoded"})
    /* compiled from: MraidBridge */
    public static class a {
        public static int n(@Nullable String str) {
            if (str == null) {
                return 53;
            }
            char c = 65535;
            switch (str.hashCode()) {
                case -1364013995:
                    if (str.equals(TtmlNode.CENTER)) {
                        c = 2;
                        break;
                    }
                    break;
                case -1314880604:
                    if (str.equals(ForceClosePosition.TOP_RIGHT)) {
                        c = 1;
                        break;
                    }
                    break;
                case -1012429441:
                    if (str.equals(ForceClosePosition.TOP_LEFT)) {
                        c = 0;
                        break;
                    }
                    break;
                case -655373719:
                    if (str.equals(ForceClosePosition.BOTTOM_LEFT)) {
                        c = 3;
                        break;
                    }
                    break;
                case 1163912186:
                    if (str.equals(ForceClosePosition.BOTTOM_RIGHT)) {
                        c = 4;
                        break;
                    }
                    break;
                case 1288627767:
                    if (str.equals("bottom-center")) {
                        c = 6;
                        break;
                    }
                    break;
                case 1755462605:
                    if (str.equals("top-center")) {
                        c = 5;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    return 51;
                case 1:
                    return 53;
                case 2:
                    return 17;
                case 3:
                    return 83;
                case 4:
                    return 85;
                case 5:
                    return 49;
                case 6:
                    return 81;
                default:
                    return 53;
            }
        }
    }

    /* compiled from: MraidBridge */
    public interface b {
        boolean a(int i, int i2, int i3, int i4, boolean z, int i5);

        boolean a(@NonNull ConsoleMessage consoleMessage, @NonNull bu buVar);

        boolean a(@NonNull String str, @NonNull JsResult jsResult);

        boolean a(boolean z, bw bwVar);

        void aL();

        void aM();

        boolean aN();

        void b(@NonNull Uri uri);

        boolean b(float f, float f2);

        void c(@NonNull bu buVar);

        boolean c(@Nullable Uri uri);

        void l(boolean z);

        boolean o(@NonNull String str);

        void onClose();

        void onVisibilityChanged(boolean z);
    }

    /* compiled from: MraidBridge */
    class c extends WebViewClient {
        private c() {
        }

        @TargetApi(24)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            bu.this.a(webResourceRequest.getUrl());
            return true;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            bu.this.a(Uri.parse(str));
            return true;
        }

        public void onPageFinished(WebView webView, String str) {
            bu.this.aK();
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error: ");
            sb.append(str);
            ah.a(sb.toString());
            super.onReceivedError(webView, i, str, str2);
        }
    }

    /* compiled from: MraidBridge */
    class d extends WebChromeClient {
        private d() {
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            if (bu.this.cq != null) {
                return bu.this.cq.a(str2, jsResult);
            }
            return super.onJsAlert(webView, str, str2, jsResult);
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            if (bu.this.cq != null) {
                return bu.this.cq.a(consoleMessage, bu.this);
            }
            return super.onConsoleMessage(consoleMessage);
        }
    }

    /* compiled from: MraidBridge */
    class e implements com.my.target.gc.a {
        private e() {
        }

        public void onVisibilityChanged(boolean z) {
            if (bu.this.cq != null) {
                bu.this.cq.onVisibilityChanged(z);
            }
        }

        public void aL() {
            if (bu.this.cq != null) {
                bu.this.cq.aL();
            }
        }
    }

    public static bu h(@NonNull String str) {
        return new bu(str);
    }

    private bu(@NonNull String str) {
        this.co = str;
    }

    public void i(@NonNull String str) {
        if (this.cr == null) {
            ah.a("MRAID bridge called setContentHtml before WebView was attached");
            return;
        }
        this.cs = false;
        this.cr.loadDataWithBaseURL("https://ad.mail.ru/", str, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
    }

    public void a(@NonNull bx bxVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("mraidbridge.setScreenSize(");
        sb.append(b(bxVar.aT()));
        sb.append(");window.mraidbridge.setMaxSize(");
        sb.append(b(bxVar.aS()));
        sb.append(");window.mraidbridge.setCurrentPosition(");
        sb.append(a(bxVar.aQ()));
        sb.append(");window.mraidbridge.setDefaultPosition(");
        sb.append(a(bxVar.aR()));
        sb.append(")");
        m(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("mraidbridge.fireSizeChangeEvent(");
        sb2.append(b(bxVar.aQ()));
        sb2.append(")");
        m(sb2.toString());
    }

    public void a(@NonNull ArrayList<String> arrayList) {
        StringBuilder sb = new StringBuilder();
        sb.append("mraidbridge.setSupports(");
        sb.append(TextUtils.join(",", arrayList));
        sb.append(")");
        m(sb.toString());
    }

    public void a(@Nullable b bVar) {
        this.cq = bVar;
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public void a(@NonNull gc gcVar) {
        this.cr = gcVar;
        this.cr.getSettings().setJavaScriptEnabled(true);
        if (VERSION.SDK_INT >= 17 && VideoType.INTERSTITIAL.equals(this.co)) {
            gcVar.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        this.cr.setScrollContainer(false);
        this.cr.setVerticalScrollBarEnabled(false);
        this.cr.setHorizontalScrollBarEnabled(false);
        this.cr.setWebViewClient(this.cp);
        this.cr.setWebChromeClient(new d());
        this.cr.setVisibilityChangedListener(new e());
    }

    public void detach() {
        this.cr = null;
    }

    public void j(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("mraidbridge.setPlacementType(");
        sb.append(JSONObject.quote(str));
        sb.append(")");
        m(sb.toString());
    }

    public void k(boolean z) {
        if (z != this.ct) {
            StringBuilder sb = new StringBuilder();
            sb.append("mraidbridge.setIsViewable(");
            sb.append(z);
            sb.append(")");
            m(sb.toString());
        }
        this.ct = z;
    }

    public void k(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("mraidbridge.setState(");
        sb.append(JSONObject.quote(str));
        sb.append(")");
        m(sb.toString());
    }

    public void aJ() {
        m("mraidbridge.fireReadyEvent()");
    }

    public boolean isVisible() {
        return this.cr != null && this.cr.isVisible();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull Uri uri) {
        String scheme = uri.getScheme();
        String host = uri.getHost();
        if ("mytarget".equals(scheme)) {
            if ("onloadmraidjs".equals(host)) {
                ah.a("MraidBridge: JS call onLoad");
            }
            StringBuilder sb = new StringBuilder();
            sb.append("MraidBridge: got mytarget scheme ");
            sb.append(uri);
            ah.a(sb.toString());
        } else if (AppodealNetworks.MRAID.equals(scheme)) {
            if (host.contains(",")) {
                host = host.substring(0, host.indexOf(",")).trim();
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Got mraid command ");
            sb2.append(uri);
            ah.a(sb2.toString());
            String uri2 = uri.toString();
            JSONObject jSONObject = null;
            bv bvVar = new bv(host, this.co);
            l(bvVar.toString());
            int indexOf = uri2.indexOf("{");
            int lastIndexOf = uri2.lastIndexOf("}") + 1;
            if (indexOf >= 0 && lastIndexOf > 0 && indexOf < lastIndexOf) {
                try {
                    if (lastIndexOf <= uri2.length()) {
                        jSONObject = new JSONObject(uri2.substring(indexOf, lastIndexOf));
                    }
                } catch (JSONException e2) {
                    a(bvVar.toString(), e2.getMessage());
                }
            }
            a(bvVar, jSONObject);
        } else {
            try {
                new URI(uri.toString());
                if (!(this.cr == null || !this.cr.dV() || this.cq == null)) {
                    this.cq.b(uri);
                }
            } catch (URISyntaxException unused) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Invalid MRAID URL: ");
                sb3.append(uri);
                ah.a(sb3.toString());
                a("", "Mraid command sent an invalid URL");
            }
        }
    }

    public void a(@NonNull String str, @NonNull String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("mraidbridge.fireErrorEvent(");
        sb.append(JSONObject.quote(str2));
        sb.append(", ");
        sb.append(JSONObject.quote(str));
        sb.append(")");
        m(sb.toString());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(@NonNull bv bvVar, @Nullable JSONObject jSONObject) throws JSONException {
        String bvVar2 = bvVar.toString();
        if (bvVar.cv && this.cr != null && !this.cr.dV()) {
            a(bvVar2, "Cannot execute this command unless the user clicks");
            return false;
        } else if (this.cq == null) {
            a(bvVar2, "Invalid state to execute this command");
            return false;
        } else if (this.cr == null) {
            a(bvVar2, "The current WebView is being destroyed");
            return false;
        } else {
            char c2 = 65535;
            switch (bvVar2.hashCode()) {
                case -1910759310:
                    if (bvVar2.equals("vpaidInit")) {
                        c2 = 8;
                        break;
                    }
                    break;
                case -1886160473:
                    if (bvVar2.equals(MraidJsMethods.PLAY_VIDEO)) {
                        c2 = 11;
                        break;
                    }
                    break;
                case -1289167206:
                    if (bvVar2.equals("expand")) {
                        c2 = 3;
                        break;
                    }
                    break;
                case -934437708:
                    if (bvVar2.equals(MraidJsMethods.RESIZE)) {
                        c2 = 2;
                        break;
                    }
                    break;
                case -733616544:
                    if (bvVar2.equals("createCalendarEvent")) {
                        c2 = 13;
                        break;
                    }
                    break;
                case 0:
                    if (bvVar2.equals("")) {
                        c2 = 14;
                        break;
                    }
                    break;
                case 3417674:
                    if (bvVar2.equals(MraidJsMethods.OPEN)) {
                        c2 = 6;
                        break;
                    }
                    break;
                case 94756344:
                    if (bvVar2.equals("close")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 133423073:
                    if (bvVar2.equals("setOrientationProperties")) {
                        c2 = 7;
                        break;
                    }
                    break;
                case 459238621:
                    if (bvVar2.equals(MRAIDNativeFeature.STORE_PICTURE)) {
                        c2 = 12;
                        break;
                    }
                    break;
                case 624734601:
                    if (bvVar2.equals("setResizeProperties")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 892543864:
                    if (bvVar2.equals("vpaidEvent")) {
                        c2 = 9;
                        break;
                    }
                    break;
                case 1362316271:
                    if (bvVar2.equals("setExpandProperties")) {
                        c2 = 5;
                        break;
                    }
                    break;
                case 1614272768:
                    if (bvVar2.equals("useCustomClose")) {
                        c2 = 4;
                        break;
                    }
                    break;
                case 1797992422:
                    if (bvVar2.equals("playheadEvent")) {
                        c2 = 10;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    this.cq.onClose();
                    break;
                case 1:
                    if (jSONObject == null) {
                        a(bvVar2, "setResizeProperties params cannot be null");
                        return false;
                    }
                    return this.cq.a(jSONObject.getInt("width"), jSONObject.getInt("height"), jSONObject.getInt("offsetX"), jSONObject.getInt("offsetY"), jSONObject.optBoolean("allowOffscreen", false), a.n(jSONObject.optString("customClosePosition")));
                case 2:
                    return this.cq.aN();
                case 3:
                    Uri uri = null;
                    if (jSONObject != null) {
                        uri = Uri.parse(jSONObject.getString("url"));
                    }
                    return this.cq.c(uri);
                case 4:
                case 5:
                    if (jSONObject != null) {
                        this.cq.l(jSONObject.getBoolean("useCustomClose"));
                        break;
                    } else {
                        a(bvVar2, "useCustomClose params cannot be null");
                        return false;
                    }
                case 6:
                    if (jSONObject != null) {
                        this.cq.b(Uri.parse(jSONObject.getString("url")));
                        break;
                    } else {
                        a(bvVar2, "open params cannot be null");
                        return false;
                    }
                case 7:
                    if (jSONObject == null) {
                        a(bvVar2, "setOrientationProperties params cannot be null");
                        return false;
                    }
                    boolean z = jSONObject.getBoolean("allowOrientationChange");
                    String string = jSONObject.getString("forceOrientation");
                    bw p = bw.p(string);
                    if (p != null) {
                        return this.cq.a(z, p);
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("wrong orientation ");
                    sb.append(string);
                    a(bvVar2, sb.toString());
                    return false;
                case 8:
                    this.cq.aM();
                    break;
                case 9:
                    if (jSONObject == null) {
                        a(bvVar2, "vpaidEvent params cannot be null");
                        return false;
                    }
                    return this.cq.o(jSONObject.getString("event"));
                case 10:
                    if (jSONObject == null) {
                        a(bvVar2, "playheadEvent params cannot be null");
                        return false;
                    }
                    return this.cq.b((float) jSONObject.getDouble("remain"), (float) jSONObject.getDouble("duration"));
                case 11:
                    ah.a("playVideo is currently unsupported");
                    return false;
                case 12:
                    ah.a("storePicture is currently unsupported");
                    return false;
                case 13:
                    ah.a("createCalendarEvent is currently unsupported");
                    return false;
                case 14:
                    a(bvVar2, "Unspecified MRAID Javascript command");
                    return false;
            }
            return true;
        }
    }

    private void l(@NonNull String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("mraidbridge.nativeComplete(");
        sb.append(JSONObject.quote(str));
        sb.append(")");
        m(sb.toString());
    }

    private void m(@NonNull String str) {
        if (this.cr == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Attempted to inject Javascript into MRAID WebView while was not attached:\n\t");
            sb.append(str);
            ah.a(sb.toString());
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("javascript:window.");
        sb2.append(str);
        sb2.append(";");
        String sb3 = sb2.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("Injecting Javascript into MRAID WebView ");
        sb4.append(hashCode());
        sb4.append(": ");
        sb4.append(sb3);
        ah.a(sb4.toString());
        this.cr.loadUrl(sb3);
    }

    /* access modifiers changed from: private */
    public void aK() {
        if (!this.cs) {
            this.cs = true;
            if (this.cq != null) {
                this.cq.c(this);
            }
        }
    }

    @NonNull
    private String a(Rect rect) {
        StringBuilder sb = new StringBuilder();
        sb.append(rect.left);
        sb.append(",");
        sb.append(rect.top);
        sb.append(",");
        sb.append(rect.width());
        sb.append(",");
        sb.append(rect.height());
        return sb.toString();
    }

    @NonNull
    private String b(Rect rect) {
        StringBuilder sb = new StringBuilder();
        sb.append(rect.width());
        sb.append(",");
        sb.append(rect.height());
        return sb.toString();
    }
}
