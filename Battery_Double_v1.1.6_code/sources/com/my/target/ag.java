package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: StandardAdResultProcessor */
public class ag extends d<dd> {
    @NonNull
    public static ag n() {
        return new ag();
    }

    private ag() {
    }

    @Nullable
    public dd a(@NonNull dd ddVar, @NonNull a aVar, @NonNull Context context) {
        if (ddVar.cn() != null) {
            return ddVar;
        }
        ct bI = ddVar.bI();
        if (bI == null || !bI.bE()) {
            return null;
        }
        return ddVar;
    }
}
