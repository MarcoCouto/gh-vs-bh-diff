package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;

/* compiled from: InterstitialSliderAdResultProcessor */
public class u extends d<cz> {
    @NonNull
    public static u j() {
        return new u();
    }

    private u() {
    }

    @Nullable
    public cz a(@NonNull cz czVar, @NonNull a aVar, @NonNull Context context) {
        ArrayList arrayList = new ArrayList();
        for (cm cmVar : czVar.bT()) {
            if (cmVar.getPortraitImages().size() > 0) {
                ImageData imageData = (ImageData) cmVar.getPortraitImages().get(0);
                arrayList.add(imageData);
                cmVar.setOptimalPortraitImage(imageData);
            }
            if (cmVar.getLandscapeImages().size() > 0) {
                ImageData imageData2 = (ImageData) cmVar.getLandscapeImages().get(0);
                arrayList.add(imageData2);
                cmVar.setOptimalLandscapeImage(imageData2);
            }
        }
        if (arrayList.size() > 0) {
            if (czVar.getCloseIcon() != null) {
                arrayList.add(czVar.getCloseIcon());
            }
            hu.e(arrayList).G(true).M(context);
            hs ev = hs.ev();
            if (ev == null) {
                ah.a("Disk cache is not available");
                return null;
            }
            for (cm cmVar2 : czVar.bT()) {
                ImageData optimalLandscapeImage = cmVar2.getOptimalLandscapeImage();
                if (optimalLandscapeImage == null || ev.ah(optimalLandscapeImage.getUrl()) == null) {
                    ImageData optimalPortraitImage = cmVar2.getOptimalPortraitImage();
                    if (optimalPortraitImage == null || ev.ah(optimalPortraitImage.getUrl()) == null) {
                        czVar.d(cmVar2);
                    }
                }
            }
            if (czVar.getBannersCount() > 0) {
                return czVar;
            }
        }
        return null;
    }
}
