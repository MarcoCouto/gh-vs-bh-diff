package com.my.target;

import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.lang.reflect.Field;
import java.net.HttpCookie;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MyTargetJSONCookie */
public class dt {
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:0|1|2|(1:4)(2:5|6)|7|8|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x00a3 */
    @Nullable
    public String a(@NonNull HttpCookie httpCookie) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", httpCookie.getName());
            jSONObject.putOpt("value", httpCookie.getValue());
            jSONObject.putOpt("comment", httpCookie.getComment());
            jSONObject.putOpt("commentUrl", httpCookie.getCommentURL());
            jSONObject.putOpt(RequestParameters.DOMAIN, httpCookie.getDomain());
            jSONObject.putOpt("maxage", Long.valueOf(httpCookie.getMaxAge()));
            jSONObject.putOpt("path", httpCookie.getPath());
            jSONObject.putOpt("portlist", httpCookie.getPortlist());
            jSONObject.putOpt("version", Integer.valueOf(httpCookie.getVersion()));
            jSONObject.putOpt("secure", Boolean.valueOf(httpCookie.getSecure()));
            jSONObject.putOpt("discard", Boolean.valueOf(httpCookie.getDiscard()));
            if (VERSION.SDK_INT >= 24) {
                jSONObject.putOpt("httpOnly", Boolean.valueOf(httpCookie.isHttpOnly()));
            } else {
                Field declaredField = httpCookie.getClass().getDeclaredField("httpOnly");
                declaredField.setAccessible(true);
                jSONObject.putOpt("httpOnly", declaredField.get(httpCookie));
            }
            return jSONObject.toString();
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Exception encoding cookie");
            sb.append(e.getMessage());
            ah.a(sb.toString());
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public HttpCookie T(@NonNull String str) {
        HttpCookie httpCookie;
        JSONException e;
        try {
            JSONObject jSONObject = new JSONObject(str);
            httpCookie = new HttpCookie(jSONObject.getString("name"), jSONObject.optString("value"));
            try {
                httpCookie.setComment(jSONObject.optString("comment"));
                httpCookie.setCommentURL(jSONObject.optString("commentUrl"));
                httpCookie.setDomain(jSONObject.optString(RequestParameters.DOMAIN));
                httpCookie.setMaxAge((long) jSONObject.optInt("maxage"));
                httpCookie.setPath(jSONObject.optString("path"));
                httpCookie.setPortlist(jSONObject.optString("portlist"));
                httpCookie.setVersion(jSONObject.optInt("version"));
                httpCookie.setSecure(jSONObject.optBoolean("secure"));
                httpCookie.setDiscard(jSONObject.optBoolean("discard"));
                if (VERSION.SDK_INT >= 24) {
                    httpCookie.setHttpOnly(jSONObject.optBoolean("httpOnly"));
                } else {
                    try {
                        Field declaredField = httpCookie.getClass().getDeclaredField("httpOnly");
                        declaredField.setAccessible(true);
                        declaredField.set(httpCookie, Boolean.valueOf(jSONObject.optBoolean("httpOnly")));
                    } catch (IllegalAccessException | NoSuchFieldException unused) {
                    }
                }
            } catch (JSONException e2) {
                e = e2;
                StringBuilder sb = new StringBuilder();
                sb.append("Exception decoding cookie");
                sb.append(e.getMessage());
                ah.a(sb.toString());
                return httpCookie;
            }
        } catch (JSONException e3) {
            e = e3;
            httpCookie = null;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Exception decoding cookie");
            sb2.append(e.getMessage());
            ah.a(sb2.toString());
            return httpCookie;
        }
        return httpCookie;
    }
}
