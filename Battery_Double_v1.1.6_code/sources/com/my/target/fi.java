package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.lang.reflect.Method;

/* compiled from: HuaweiOAIdDataProvider */
public class fi extends ff {
    private boolean gH = false;

    fi() {
    }

    @WorkerThread
    public synchronized void collectData(@NonNull final Context context) {
        if (ai.isMainThread()) {
            ah.a("You must not call collectData method from main thread");
        } else if (!this.gH) {
            hz O = hz.O(context);
            try {
                Class.forName("com.huawei.hms.ads.identifier.AdvertisingIdClient");
                String eC = O.eC();
                String eD = O.eD();
                if (eC != null) {
                    if (eC.length() != 0) {
                        addParam("oaid", eC);
                        addParam("oaid_tracking_enabled", eD);
                        ai.b(new Runnable() {
                            public void run() {
                                fi.this.B(context);
                            }
                        });
                        this.gH = true;
                    }
                }
                B(context);
                this.gH = true;
            } catch (ClassNotFoundException unused) {
                O.aj(null);
                O.ak(null);
                this.gH = true;
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009d  */
    public void B(@NonNull Context context) {
        String str;
        String str2 = null;
        try {
            Class cls = Class.forName("com.huawei.hms.ads.identifier.AdvertisingIdClient");
            if (cls != null) {
                ah.a("send huawei AId");
                Method method = cls.getMethod("getAdvertisingIdInfo", new Class[]{Context.class});
                if (method != null) {
                    Object invoke = method.invoke(null, new Object[]{context});
                    if (invoke != null) {
                        Method method2 = invoke.getClass().getMethod("getId", new Class[0]);
                        if (method2 != null) {
                            str = (String) method2.invoke(invoke, new Object[0]);
                            try {
                                StringBuilder sb = new StringBuilder();
                                sb.append("huawei AId: ");
                                sb.append(str);
                                ah.a(sb.toString());
                            } catch (Throwable unused) {
                            }
                        } else {
                            str = null;
                        }
                        Method method3 = invoke.getClass().getMethod(RequestParameters.isLAT, new Class[0]);
                        if (method3 != null) {
                            Boolean bool = (Boolean) method3.invoke(invoke, new Object[0]);
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(bool.booleanValue() ^ true ? 1 : 0);
                            sb2.append("");
                            String sb3 = sb2.toString();
                            try {
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("huawei ad tracking enabled: ");
                                sb4.append(!bool.booleanValue());
                                ah.a(sb4.toString());
                            } catch (Throwable unused2) {
                            }
                            str2 = sb3;
                        }
                        synchronized (this) {
                            addParam("oaid", str);
                            addParam("oaid_tracking_enabled", str2);
                        }
                        hz.O(context).aj(str);
                        hz.O(context).ak(str2);
                    }
                }
            }
        } catch (Throwable unused3) {
        }
        str = null;
        synchronized (this) {
        }
        hz.O(context).aj(str);
        hz.O(context).ak(str2);
    }
}
