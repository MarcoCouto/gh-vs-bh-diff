package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;

/* compiled from: ContainerAdView */
public class fx extends FrameLayout {
    private int iq;
    private int ir;
    private boolean is;
    private int maxWidth;

    public fx(@NonNull Context context) {
        super(context);
    }

    public void setFlexibleWidth(boolean z) {
        this.is = z;
    }

    public void setMaxWidth(int i) {
        this.maxWidth = i;
    }

    public void g(int i, int i2) {
        this.iq = i;
        this.ir = i2;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.iq <= 0 || this.ir <= 0) {
            super.onMeasure(i, i2);
            return;
        }
        int size = MeasureSpec.getSize(i);
        if (!this.is || this.iq >= size) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(this.iq, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(this.ir, 1073741824));
            return;
        }
        if (this.maxWidth > 0) {
            size = Math.min(size, this.maxWidth);
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(this.ir, 1073741824));
    }
}
