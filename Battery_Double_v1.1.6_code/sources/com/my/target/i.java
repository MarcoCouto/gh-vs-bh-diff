package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0056b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InstreamAudioAdFactory */
public final class i extends b<cx> {
    @Nullable
    private final List<bz> j;
    /* access modifiers changed from: private */
    @NonNull
    public final ia k;
    /* access modifiers changed from: private */
    @Nullable
    public Runnable l;

    /* compiled from: InstreamAudioAdFactory */
    static class a implements com.my.target.b.a<cx> {
        public boolean a() {
            return true;
        }

        private a() {
        }

        @NonNull
        public c<cx> b() {
            return j.f();
        }

        @Nullable
        public d<cx> c() {
            return k.h();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    public interface b extends C0056b {
    }

    @NonNull
    public static b<cx> a(@NonNull a aVar, int i) {
        return new i(aVar, i);
    }

    @NonNull
    public static b<cx> a(@NonNull bz bzVar, @NonNull a aVar, int i) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(bzVar);
        return new i(arrayList, aVar, i);
    }

    @NonNull
    public static b<cx> a(@NonNull List<bz> list, @NonNull a aVar, int i) {
        return new i(list, aVar, i);
    }

    private i(@NonNull a aVar, int i) {
        this(null, aVar, i);
    }

    private i(@Nullable List<bz> list, @NonNull a aVar, int i) {
        super(new a(), aVar);
        this.j = list;
        this.k = ia.K(i * 1000);
    }

    @NonNull
    public b<cx> a(@NonNull Context context) {
        if (this.l == null) {
            this.l = new Runnable() {
                public void run() {
                    i.this.k.e(i.this.l);
                    i.this.a(null, "ad loading timeout");
                }
            };
        }
        this.k.d(this.l);
        return super.a(context);
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: d */
    public cx b(@NonNull Context context) {
        if (this.j == null) {
            return (cx) super.b(context);
        }
        return (cx) a((cx) a(this.j, null, this.b.b(), dk.cz(), context), context);
    }
}
