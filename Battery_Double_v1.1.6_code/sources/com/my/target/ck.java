package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: InterstitialAdCard */
public class ck extends ch {
    @NonNull
    public static ck newCard(@NonNull cj cjVar) {
        ck ckVar = new ck();
        ckVar.ctaText = cjVar.ctaText;
        ckVar.navigationType = cjVar.navigationType;
        ckVar.urlscheme = cjVar.urlscheme;
        ckVar.bundleId = cjVar.bundleId;
        ckVar.directLink = cjVar.directLink;
        ckVar.openInBrowser = cjVar.openInBrowser;
        ckVar.usePlayStoreAction = cjVar.usePlayStoreAction;
        ckVar.deeplink = cjVar.deeplink;
        ckVar.clickArea = cjVar.clickArea;
        ckVar.rating = cjVar.rating;
        ckVar.votes = cjVar.votes;
        ckVar.domain = cjVar.domain;
        ckVar.category = cjVar.category;
        ckVar.subCategory = cjVar.subCategory;
        return ckVar;
    }

    private ck() {
        this.clickArea = ca.db;
    }
}
