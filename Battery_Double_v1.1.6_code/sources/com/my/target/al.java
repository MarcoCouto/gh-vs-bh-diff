package com.my.target;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import com.google.android.exoplayer2.util.MimeTypes;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import com.my.target.gh.d;
import com.my.target.nativeads.views.MediaAdView;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* compiled from: NativeAdVideoController */
public class al implements com.my.target.fo.a, d, com.my.target.ig.a {
    @NonNull
    private final VideoData Q;
    @NonNull
    private final OnAudioFocusChangeListener R;
    @NonNull
    private final cp S;
    @NonNull
    private final HashSet<dg> T = new HashSet<>();
    @NonNull
    private final ie U;
    @Nullable
    private OnClickListener V;
    @Nullable
    private WeakReference<MediaAdView> W;
    @Nullable
    private WeakReference<fo> X;
    @Nullable
    private WeakReference<gh> Y;
    @Nullable
    private WeakReference<Context> Z;
    private boolean aa;
    /* access modifiers changed from: private */
    public boolean ab;
    private boolean ac;
    private boolean ae;
    @Nullable
    private b af;
    @Nullable
    private ig ag;
    private boolean ah;
    private long ai;
    private int state;
    private final boolean useExoPlayer;
    @NonNull
    private final co videoBanner;

    /* compiled from: NativeAdVideoController */
    class a implements OnAudioFocusChangeListener {
        private a() {
        }

        public void onAudioFocusChange(int i) {
            switch (i) {
                case -3:
                    al.this.L();
                    return;
                case -2:
                case -1:
                    al.this.N();
                    ah.a("Audiofocus loss, pausing");
                    return;
                case 1:
                case 2:
                case 4:
                    if (al.this.ab) {
                        ah.a("Audiofocus gain, unmuting");
                        al.this.O();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: NativeAdVideoController */
    public interface b {
        void Q();

        void R();

        void S();
    }

    public void A() {
    }

    public al(@NonNull cp cpVar, @NonNull co coVar, @NonNull VideoData videoData, boolean z) {
        this.videoBanner = coVar;
        this.S = cpVar;
        this.useExoPlayer = z;
        this.Q = videoData;
        this.aa = this.videoBanner.isAutoPlay();
        this.ae = this.videoBanner.isAutoMute();
        di statHolder = this.videoBanner.getStatHolder();
        this.U = ie.c(statHolder);
        this.T.addAll(statHolder.cv());
        this.R = new a();
    }

    public void a(@Nullable OnClickListener onClickListener) {
        this.V = onClickListener;
    }

    public void a(@NonNull MediaAdView mediaAdView, @Nullable Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append("register video ad with view ");
        sb.append(mediaAdView);
        ah.a(sb.toString());
        if (!this.ab) {
            unregister();
            this.W = new WeakReference<>(mediaAdView);
            this.Z = new WeakReference<>(context);
            fs fsVar = new fs(mediaAdView.getContext().getApplicationContext());
            mediaAdView.addView(fsVar, 0);
            this.U.setView(fsVar);
            if (this.aa) {
                B();
            } else {
                x();
            }
            mediaAdView.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    al.this.b(view);
                }
            });
        }
    }

    public void unregister() {
        w();
        this.U.setView(null);
        K();
        if (this.W != null) {
            MediaAdView mediaAdView = (MediaAdView) this.W.get();
            if (mediaAdView != null) {
                if (mediaAdView.getChildAt(0) instanceof fs) {
                    mediaAdView.removeViewAt(0);
                }
                mediaAdView.setOnClickListener(null);
            }
        }
    }

    public void v() {
        MediaAdView J = J();
        if (J == null) {
            ah.a("Trying to play video in unregistered view");
            K();
        } else if (J.getWindowVisibility() != 0) {
            if (this.state == 1) {
                if (this.ag != null) {
                    this.ai = this.ag.getPosition();
                }
                K();
                this.state = 4;
                this.ah = false;
                B();
            } else {
                K();
            }
        } else if (!this.ah) {
            fs fsVar = null;
            Context context = this.Z != null ? (Context) this.Z.get() : null;
            if (context != null) {
                a(J, context);
            }
            this.ah = true;
            if (J.getChildAt(0) instanceof fs) {
                fsVar = (fs) J.getChildAt(0);
            }
            if (fsVar == null) {
                K();
                return;
            }
            if (!(this.ag == null || this.Q == this.ag.eH())) {
                K();
            }
            if (!this.aa) {
                J.getImageView().setVisibility(0);
                J.getPlayButtonView().setVisibility(0);
                J.getProgressBarView().setVisibility(8);
            }
            if (this.aa && !this.ab) {
                if (this.ag == null || !this.ag.isPaused()) {
                    a(fsVar, true);
                } else {
                    this.ag.a(fsVar);
                    this.ag.a((com.my.target.ig.a) this);
                    this.ag.resume();
                }
                M();
            }
        }
    }

    public void w() {
        if (this.ah && !this.ab) {
            this.ah = false;
            if (this.state == 1 && this.ag != null) {
                this.ag.pause();
                this.state = 2;
            }
            if (this.ag != null) {
                this.ag.a((com.my.target.ig.a) null);
                this.ag.a((fs) null);
            }
        }
    }

    public void x() {
        Context context;
        this.ac = false;
        this.ai = 0;
        MediaAdView J = J();
        if (J != null) {
            ImageView imageView = J.getImageView();
            ImageData image = this.videoBanner.getImage();
            if (image != null) {
                imageView.setImageBitmap(image.getBitmap());
            }
            imageView.setVisibility(0);
            J.getPlayButtonView().setVisibility(0);
            J.getProgressBarView().setVisibility(8);
            context = J.getContext();
        } else {
            context = null;
        }
        if (this.ab && this.Y != null) {
            gh ghVar = (gh) this.Y.get();
            if (ghVar != null) {
                ghVar.dX();
                context = ghVar.getContext();
            }
        }
        if (context != null) {
            i(context);
        }
    }

    public void e(float f) {
        if (this.Y != null) {
            gh ghVar = (gh) this.Y.get();
            if (ghVar == null) {
                return;
            }
            if (f > 0.0f) {
                ghVar.y(false);
            } else {
                ghVar.y(true);
            }
        }
    }

    public void y() {
        if (this.state != 1) {
            this.state = 1;
            MediaAdView J = J();
            if (J != null) {
                J.getImageView().setVisibility(4);
                J.getProgressBarView().setVisibility(8);
                J.getPlayButtonView().setVisibility(8);
            }
            if (this.ab && this.Y != null) {
                gh ghVar = (gh) this.Y.get();
                if (ghVar != null) {
                    if (this.ag != null) {
                        this.ag.a(ghVar.getAdVideoView());
                    }
                    ghVar.ea();
                }
            }
        }
    }

    public void z() {
        Context context;
        MediaAdView J = J();
        if (J != null) {
            context = J.getContext();
            J.getPlayButtonView().setVisibility(0);
            J.getProgressBarView().setVisibility(8);
        } else {
            context = null;
        }
        N();
        if (J != null) {
            i(context);
        }
        if (this.af != null) {
            this.af.R();
        }
    }

    public void B() {
        this.state = 4;
        MediaAdView J = J();
        if (J != null) {
            J.getProgressBarView().setVisibility(0);
            J.getImageView().setVisibility(0);
            J.getPlayButtonView().setVisibility(8);
        }
        if (this.ab && this.Y != null) {
            gh ghVar = (gh) this.Y.get();
            if (ghVar != null) {
                ghVar.dY();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public void a(float f, float f2) {
        Context context;
        float duration;
        this.ai = 0;
        if (this.W != null) {
            View view = (View) this.W.get();
            if (view != null) {
                context = view.getContext();
                y();
                this.U.m(f);
                if (!this.ac) {
                    if (this.af != null) {
                        this.af.Q();
                    }
                    if (context != null) {
                        sendStat("playbackStarted", context);
                        this.T.clear();
                        this.T.addAll(this.videoBanner.getStatHolder().cv());
                        a(0.0f, context);
                    }
                    this.ac = true;
                }
                duration = this.videoBanner.getDuration();
                if (this.Y != null) {
                    gh ghVar = (gh) this.Y.get();
                    if (ghVar != null) {
                        ghVar.a(f, duration);
                    }
                }
                if (f > duration) {
                    if (f > 0.0f) {
                        if (context != null) {
                            a(f, context);
                        }
                        if (this.ag != null) {
                            this.ai = this.ag.getPosition();
                        }
                    }
                    if (f == duration) {
                        x();
                        this.state = 3;
                        this.aa = false;
                        if (this.ag != null) {
                            this.ag.stop();
                        }
                        if (this.af != null) {
                            this.af.S();
                            return;
                        }
                        return;
                    }
                    return;
                }
                a(duration, duration);
                return;
            }
        }
        context = null;
        y();
        this.U.m(f);
        if (!this.ac) {
        }
        duration = this.videoBanner.getDuration();
        if (this.Y != null) {
        }
        if (f > duration) {
        }
    }

    public void d(String str) {
        this.state = 3;
        x();
        if (this.ag != null) {
            this.ag.stop();
        }
    }

    public void C() {
        this.ai = 0;
    }

    public void a(@Nullable b bVar) {
        this.af = bVar;
    }

    public void a(@NonNull fo foVar, @NonNull FrameLayout frameLayout) {
        a(foVar, frameLayout, new gh(frameLayout.getContext()));
    }

    public void a(boolean z) {
        if (this.ag != null && !z) {
            this.ai = this.ag.getPosition();
            K();
            z();
        }
    }

    public void D() {
        ah.a("Dismiss dialog");
        this.X = null;
        this.ab = false;
        M();
        MediaAdView J = J();
        if (J != null) {
            i(J.getContext());
            switch (this.state) {
                case 1:
                    this.state = 4;
                    y();
                    if (this.videoBanner.isAutoPlay()) {
                        this.aa = true;
                    }
                    View childAt = J.getChildAt(0);
                    if (childAt instanceof fs) {
                        a((fs) childAt, true);
                        break;
                    }
                    break;
                case 2:
                case 3:
                    this.aa = false;
                    x();
                    break;
                case 4:
                    this.aa = true;
                    B();
                    View childAt2 = J.getChildAt(0);
                    if (childAt2 instanceof fs) {
                        a((fs) childAt2, true);
                        break;
                    }
                    break;
                default:
                    this.aa = false;
                    break;
            }
            sendStat("fullscreenOff", J.getContext());
            this.Y = null;
        }
    }

    public void E() {
        if (this.X != null) {
            fo foVar = (fo) this.X.get();
            if (foVar != null) {
                Context context = foVar.getContext();
                P();
                sendStat("playbackResumed", context);
            }
        }
        if (this.af != null) {
            this.af.Q();
        }
    }

    public void F() {
        P();
        if (this.Y != null) {
            gh ghVar = (gh) this.Y.get();
            if (ghVar != null) {
                ghVar.getMediaAdView().getImageView().setVisibility(8);
                ghVar.eb();
            }
        }
        if (this.af != null) {
            this.af.Q();
        }
    }

    public void a(View view) {
        if (this.state == 1) {
            if (this.ag != null) {
                this.ag.pause();
            }
            z();
        }
        if (this.V != null) {
            this.V.onClick(view);
        }
    }

    public void G() {
        if (this.state == 1) {
            N();
            this.state = 2;
            if (this.af != null) {
                this.af.R();
            }
            if (this.X != null) {
                fo foVar = (fo) this.X.get();
                if (foVar != null) {
                    sendStat("playbackPaused", foVar.getContext());
                }
            }
        }
    }

    public void H() {
        fo foVar = this.X == null ? null : (fo) this.X.get();
        if (foVar != null && foVar.isShowing()) {
            foVar.dismiss();
        }
    }

    public void I() {
        MediaAdView J = J();
        if (J == null || this.ag == null) {
            this.ae = !this.ae;
            return;
        }
        Context context = J.getContext();
        if (this.ag.isMuted()) {
            this.ag.cU();
            sendStat("volumeOn", context);
            this.ae = false;
            return;
        }
        this.ag.M();
        sendStat("volumeOff", context);
        this.ae = true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(fo foVar, FrameLayout frameLayout, gh ghVar) {
        this.state = 4;
        this.X = new WeakReference<>(foVar);
        ghVar.setLayoutParams(new LayoutParams(-1, -1));
        frameLayout.addView(ghVar);
        this.Y = new WeakReference<>(ghVar);
        ghVar.a(this.S, this.Q);
        ghVar.setVideoDialogViewListener(this);
        ghVar.y(this.ae);
        sendStat("fullscreenOn", frameLayout.getContext());
        a(ghVar.getAdVideoView(), this.ae);
    }

    private void a(@NonNull fs fsVar, boolean z) {
        if (this.ag == null) {
            if (this.useExoPlayer) {
                this.ag = ii.R(fsVar.getContext());
            } else {
                this.ag = ih.eI();
            }
            this.ag.a((com.my.target.ig.a) this);
        }
        if (z) {
            M();
        } else {
            O();
        }
        this.ag.a(fsVar);
        if (!this.ag.isPlaying()) {
            this.ag.a(this.Q, fsVar.getContext());
            if (this.ai > 0) {
                this.ag.seekTo(this.ai);
                return;
            }
            return;
        }
        y();
    }

    @Nullable
    private MediaAdView J() {
        if (this.W != null) {
            return (MediaAdView) this.W.get();
        }
        return null;
    }

    private void K() {
        if (this.ag != null) {
            this.ag.a((com.my.target.ig.a) null);
            this.ag.destroy();
            this.ag = null;
        }
    }

    /* access modifiers changed from: private */
    public void L() {
        if (this.ag != null && !this.ae) {
            this.ag.L();
        }
    }

    private void M() {
        if (this.ag != null) {
            this.ag.M();
        }
    }

    /* access modifiers changed from: private */
    public void b(@NonNull View view) {
        this.ab = true;
        Context context = this.Z != null ? (Context) this.Z.get() : null;
        if (context == null) {
            context = view.getContext();
        }
        j(context);
        if (this.state == 1) {
            this.state = 4;
        }
        try {
            fo.a(this, context).show();
        } catch (Exception e) {
            e.printStackTrace();
            ah.b("Unable to start video dialog! Check myTarget MediaAdView, maybe it was created with non-Activity context");
            D();
        }
    }

    /* access modifiers changed from: private */
    public void N() {
        if (this.ab && this.Y != null) {
            this.state = 2;
            gh ghVar = (gh) this.Y.get();
            if (ghVar != null) {
                if (this.ag != null) {
                    this.ag.pause();
                }
                ghVar.dZ();
            }
        }
    }

    private void i(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.abandonAudioFocus(this.R);
        }
    }

    private void a(float f, @NonNull Context context) {
        if (!this.T.isEmpty()) {
            Iterator it = this.T.iterator();
            while (it.hasNext()) {
                dg dgVar = (dg) it.next();
                if (dgVar.cq() <= f) {
                    ib.a((dh) dgVar, context);
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void O() {
        if (this.ag != null) {
            this.ag.cU();
        }
    }

    private void j(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.requestAudioFocus(this.R, 3, 2);
        }
    }

    private void sendStat(@NonNull String str, @NonNull Context context) {
        ib.a((List<dh>) this.videoBanner.getStatHolder().N(str), context);
    }

    private void P() {
        if (this.ag != null && this.ag.isPaused()) {
            MediaAdView J = J();
            if (J == null) {
                ah.a("Trying to play video in unregistered view");
                K();
                return;
            }
            fs fsVar = null;
            if (this.ab && this.Y != null) {
                fsVar = ((gh) this.Y.get()).getAdVideoView();
            } else if (J.getChildAt(0) instanceof fs) {
                fsVar = (fs) J.getChildAt(0);
            }
            if (fsVar == null) {
                K();
                return;
            } else {
                this.ag.a(fsVar);
                this.ag.resume();
            }
        } else if (this.ab && this.Y != null) {
            a(((gh) this.Y.get()).getAdVideoView(), true);
        }
        B();
    }
}
