package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.my.target.common.models.ImageData;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: CommonBannerParser */
public class dw {
    @NonNull
    private final a adConfig;
    @Nullable
    private String ck;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;
    @NonNull
    private final em ez;

    @NonNull
    public static dw b(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new dw(bzVar, aVar, context2);
    }

    @Nullable
    public static String g(@Nullable String str, @Nullable String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return null;
        }
        StringBuilder sb = new StringBuilder(str2);
        if (sb.length() > 0) {
            Matcher matcher = Pattern.compile("<script\\s+[^>]*\\bsrc\\s*=\\s*(\\\\?[\\\"\\'])mraid\\.js\\1[^>]*>\\s*<\\/script>\\n*", 2).matcher(str2);
            if (matcher.find()) {
                int start = matcher.start();
                sb.delete(start, matcher.end());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("<script src=\"");
                sb2.append(str);
                sb2.append("\"></script>");
                sb.insert(start, sb2.toString());
                return sb.toString();
            }
        }
        return null;
    }

    private dw(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eA = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.ez = em.k(bzVar, aVar, context2);
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull ch chVar) {
        this.ck = jSONObject.optString("id");
        if (TextUtils.isEmpty(this.ck)) {
            this.ck = jSONObject.optString("bannerID", chVar.getId());
        }
        chVar.setId(this.ck);
        String optString = jSONObject.optString("type");
        if (!TextUtils.isEmpty(optString)) {
            chVar.setType(optString);
        }
        chVar.setWidth(jSONObject.optInt("width", chVar.getWidth()));
        chVar.setHeight(jSONObject.optInt("height", chVar.getHeight()));
        String optString2 = jSONObject.optString("ageRestrictions");
        if (!TextUtils.isEmpty(optString2)) {
            chVar.setAgeRestrictions(optString2);
        }
        String optString3 = jSONObject.optString("deeplink");
        if (!TextUtils.isEmpty(optString3)) {
            chVar.setDeeplink(optString3);
        }
        String optString4 = jSONObject.optString("trackingLink");
        if (!TextUtils.isEmpty(optString4)) {
            chVar.setTrackingLink(optString4);
        }
        String optString5 = jSONObject.optString("bundle_id");
        if (!TextUtils.isEmpty(optString5)) {
            chVar.setBundleId(optString5);
        }
        String optString6 = jSONObject.optString("urlscheme");
        if (!TextUtils.isEmpty(optString6)) {
            chVar.setUrlscheme(optString6);
        }
        chVar.setOpenInBrowser(jSONObject.optBoolean("openInBrowser", chVar.isOpenInBrowser()));
        chVar.setUsePlayStoreAction(jSONObject.optBoolean("usePlayStoreAction", chVar.isUsePlayStoreAction()));
        chVar.setDirectLink(jSONObject.optBoolean("directLink", chVar.isDirectLink()));
        chVar.setPaidType(jSONObject.optString("paidType", chVar.getPaidType()));
        String optString7 = jSONObject.optString("navigationType");
        if (!TextUtils.isEmpty(optString7)) {
            if ("deeplink".equals(optString7)) {
                chVar.setNavigationType("store");
            } else {
                chVar.setNavigationType(optString7);
            }
        }
        String optString8 = jSONObject.optString("title");
        if (!TextUtils.isEmpty(optString8)) {
            chVar.setTitle(optString8);
        }
        String optString9 = jSONObject.optString("description");
        if (!TextUtils.isEmpty(optString9)) {
            chVar.setDescription(optString9);
        }
        String optString10 = jSONObject.optString("disclaimer");
        if (!TextUtils.isEmpty(optString10)) {
            chVar.setDisclaimer(optString10);
        }
        chVar.setVotes(jSONObject.optInt("votes", chVar.getVotes()));
        String optString11 = jSONObject.optString("category");
        if (!TextUtils.isEmpty(optString11)) {
            chVar.setCategory(optString11);
        }
        String optString12 = jSONObject.optString("subcategory");
        if (!TextUtils.isEmpty(optString12)) {
            chVar.setSubCategory(optString12);
        }
        String optString13 = jSONObject.optString(RequestParameters.DOMAIN);
        if (!TextUtils.isEmpty(optString13)) {
            chVar.setDomain(optString13);
        }
        chVar.setDuration((float) jSONObject.optDouble("duration", (double) chVar.getDuration()));
        if (jSONObject.has("rating")) {
            float optDouble = (float) jSONObject.optDouble("rating", -1.0d);
            double d = (double) optDouble;
            if (d > 5.0d || d < Utils.DOUBLE_EPSILON) {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to parse rating ");
                sb.append(optDouble);
                f("Bad value", sb.toString());
            } else {
                chVar.setRating(optDouble);
            }
        }
        chVar.setCtaText(jSONObject.optString("ctaText", chVar.getCtaText()));
        String optString14 = jSONObject.optString("iconLink");
        int optInt = jSONObject.optInt("iconWidth");
        int optInt2 = jSONObject.optInt("iconHeight");
        if (!TextUtils.isEmpty(optString14)) {
            chVar.setIcon(ImageData.newImageData(optString14, optInt, optInt2));
        }
        String optString15 = jSONObject.optString("imageLink");
        int optInt3 = jSONObject.optInt("imageWidth");
        int optInt4 = jSONObject.optInt("imageHeight");
        if (!TextUtils.isEmpty(optString15)) {
            chVar.setImage(ImageData.newImageData(optString15, optInt3, optInt4));
        }
        if (jSONObject.has("clickArea")) {
            int optInt5 = jSONObject.optInt("clickArea");
            if (optInt5 <= 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Bad ClickArea mask ");
                sb2.append(optInt5);
                f("Bad value", sb2.toString());
            } else {
                chVar.setClickArea(ca.h(optInt5));
            }
        } else if (jSONObject.has("extendedClickArea")) {
            if (jSONObject.optBoolean("extendedClickArea", true)) {
                chVar.setClickArea(ca.da);
            } else {
                chVar.setClickArea(ca.db);
            }
        }
        chVar.setAdvertisingLabel(jSONObject.optString("advertisingLabel", ""));
        JSONObject optJSONObject = jSONObject.optJSONObject("adChoices");
        if (optJSONObject != null) {
            String optString16 = optJSONObject.optString("iconLink");
            String optString17 = optJSONObject.optString("clickLink");
            if (!TextUtils.isEmpty(optString16) && !TextUtils.isEmpty(optString17)) {
                chVar.setAdChoices(by.a(ImageData.newImageData(optString16), optString17));
            }
        }
        this.ez.a(chVar.getStatHolder(), jSONObject, this.ck, chVar.getDuration());
    }

    public boolean a(@NonNull String str, @NonNull JSONObject jSONObject) {
        jSONObject.remove("source");
        try {
            jSONObject.put("source", str);
            return true;
        } catch (JSONException unused) {
            f("Json error", "Unable to re-encode source of html banner");
            return false;
        }
    }

    private void f(@NonNull String str, @NonNull String str2) {
        dq.P(str).Q(str2).x(this.adConfig.getSlotId()).S(this.ck).R(this.eA.getUrl()).q(this.context);
    }
}
