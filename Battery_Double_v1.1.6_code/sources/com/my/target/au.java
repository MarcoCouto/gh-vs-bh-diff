package com.my.target;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.common.MyTargetActivity;
import java.util.List;

/* compiled from: InterstitialAdImageEngine */
public class au extends as {
    @NonNull
    private final cm aY;

    /* compiled from: InterstitialAdImageEngine */
    static class a implements com.my.target.eu.a {
        @NonNull
        private final au aZ;

        a(@NonNull au auVar) {
            this.aZ = auVar;
        }

        public void b(@Nullable ch chVar, @Nullable String str, @NonNull Context context) {
            this.aZ.m(context);
        }

        public void aj() {
            this.aZ.aj();
        }

        public void a(@NonNull ch chVar, @NonNull Context context) {
            this.aZ.a(chVar, context);
        }
    }

    @NonNull
    static au a(@NonNull InterstitialAd interstitialAd, @NonNull cm cmVar) {
        return new au(interstitialAd, cmVar);
    }

    private au(InterstitialAd interstitialAd, @NonNull cm cmVar) {
        super(interstitialAd);
        this.aY = cmVar;
    }

    public void a(@NonNull fo foVar, @NonNull FrameLayout frameLayout) {
        super.a(foVar, frameLayout);
        b(frameLayout);
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        super.onActivityCreate(myTargetActivity, intent, frameLayout);
        b(frameLayout);
    }

    /* access modifiers changed from: protected */
    public boolean ai() {
        return this.aY.isAllowBackButton();
    }

    private void b(@NonNull ViewGroup viewGroup) {
        eq t = eq.t(viewGroup.getContext());
        t.a(new a(this));
        t.e(this.aY);
        viewGroup.addView(t.cT(), new LayoutParams(-1, -1));
    }

    /* access modifiers changed from: 0000 */
    public void aj() {
        dismiss();
    }

    /* access modifiers changed from: 0000 */
    public void m(@NonNull Context context) {
        hr.et().b(this.aY, context);
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onClick(this.ad);
        }
        dismiss();
    }

    /* access modifiers changed from: 0000 */
    public void a(ch chVar, Context context) {
        ib.a((List<dh>) chVar.getStatHolder().N("playbackStarted"), context);
    }
}
