package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeAdResultProcessor */
public class x extends d<db> {
    @NonNull
    public static x k() {
        return new x();
    }

    private x() {
    }

    @Nullable
    public db a(@NonNull db dbVar, @NonNull a aVar, @NonNull Context context) {
        List<cp> bT = dbVar.bT();
        if (bT.isEmpty()) {
            ct bI = dbVar.bI();
            if (bI == null || !bI.bE()) {
                return null;
            }
            return dbVar;
        }
        ArrayList arrayList = new ArrayList();
        for (cp cpVar : bT) {
            co videoBanner = cpVar.getVideoBanner();
            if (videoBanner != null) {
                VideoData videoData = (VideoData) videoBanner.getMediaData();
                if (videoData != null && aVar.isAutoLoadVideo() && videoData.isCacheable()) {
                    videoData.setData((String) dp.cG().f(videoData.getUrl(), context));
                }
            }
            ImageData image = cpVar.getImage();
            if (image != null) {
                image.useCache(true);
                arrayList.add(image);
            }
            ImageData icon = cpVar.getIcon();
            if (icon != null) {
                icon.useCache(true);
                arrayList.add(icon);
            }
            for (cq image2 : cpVar.getNativeAdCards()) {
                ImageData image3 = image2.getImage();
                if (image3 != null) {
                    image3.useCache(true);
                    arrayList.add(image3);
                }
            }
            by adChoices = cpVar.getAdChoices();
            if (adChoices != null) {
                ImageData icon2 = adChoices.getIcon();
                icon2.useCache(true);
                arrayList.add(icon2);
            }
        }
        if (aVar.isAutoLoadImages() && arrayList.size() > 0) {
            hu.e(arrayList).M(context);
        }
        return dbVar;
    }
}
