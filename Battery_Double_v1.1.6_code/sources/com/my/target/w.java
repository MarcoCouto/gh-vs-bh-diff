package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ef.a;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: NativeAdResponseParser */
public class w extends c<db> implements a {
    @NonNull
    public static c<db> f() {
        return new w();
    }

    private w() {
    }

    @Nullable
    public cv b(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        db cb = db.cb();
        eg g = eg.g(bzVar, aVar, context);
        cp newBanner = cp.newBanner();
        g.a(jSONObject, newBanner);
        cb.a(newBanner);
        return cb;
    }

    @Nullable
    public db a(@NonNull String str, @NonNull bz bzVar, @Nullable db dbVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a = a(str, context);
        if (a == null) {
            return null;
        }
        if (dbVar == null) {
            dbVar = db.cb();
        }
        JSONObject optJSONObject = a.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            if (aVar.isMediationEnabled()) {
                JSONObject optJSONObject2 = a.optJSONObject("mediation");
                if (optJSONObject2 != null) {
                    ct g = ef.a(this, bzVar, aVar, context).g(optJSONObject2);
                    if (g != null) {
                        dbVar.a(g);
                        return dbVar;
                    }
                }
            }
            return null;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        eg g2 = eg.g(bzVar, aVar, context);
        int bannersCount = aVar.getBannersCount();
        if (bannersCount > 0) {
            int length = optJSONArray.length();
            if (bannersCount > length) {
                bannersCount = length;
            }
        } else {
            bannersCount = 1;
        }
        for (int i = 0; i < bannersCount; i++) {
            JSONObject optJSONObject3 = optJSONArray.optJSONObject(i);
            if (optJSONObject3 != null) {
                cp newBanner = cp.newBanner();
                g2.a(optJSONObject3, newBanner);
                dbVar.a(newBanner);
            }
        }
        if (dbVar.getBannersCount() > 0) {
            return dbVar;
        }
        return null;
    }
}
