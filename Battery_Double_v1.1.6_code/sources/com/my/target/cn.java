package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialAdPromoBanner */
public class cn extends cj {
    @Nullable
    private ImageData adIcon;
    @Nullable
    private String adIconClickLink;
    private boolean closeOnClick = true;
    @Nullable
    private cj endCard;
    @NonNull
    private final List<ck> interstitialAdCards = new ArrayList();
    @NonNull
    private final ce promoStyleSettings = ce.bp();
    private int style;
    @Nullable
    private co<VideoData> videoBanner;
    private boolean videoRequired = false;

    @NonNull
    public static cn newBanner() {
        return new cn();
    }

    public boolean isCloseOnClick() {
        if (this.videoBanner != null) {
            return false;
        }
        return this.closeOnClick;
    }

    public void setCloseOnClick(boolean z) {
        this.closeOnClick = z;
    }

    @NonNull
    public ce getPromoStyleSettings() {
        return this.promoStyleSettings;
    }

    @Nullable
    public ImageData getAdIcon() {
        return this.adIcon;
    }

    public void setAdIcon(@Nullable ImageData imageData) {
        this.adIcon = imageData;
    }

    @Nullable
    public String getAdIconClickLink() {
        return this.adIconClickLink;
    }

    public void setAdIconClickLink(@Nullable String str) {
        this.adIconClickLink = str;
    }

    public void setVideoBanner(@Nullable co<VideoData> coVar) {
        this.videoBanner = coVar;
    }

    @Nullable
    public co<VideoData> getVideoBanner() {
        return this.videoBanner;
    }

    @NonNull
    public List<ck> getInterstitialAdCards() {
        return this.interstitialAdCards;
    }

    public void addInterstitialAdCard(@NonNull ck ckVar) {
        this.interstitialAdCards.add(ckVar);
    }

    public int getStyle() {
        return this.style;
    }

    public void setStyle(int i) {
        this.style = i;
    }

    public void setEndCard(@Nullable cj cjVar) {
        this.endCard = cjVar;
    }

    @Nullable
    public cj getEndCard() {
        return this.endCard;
    }

    public void setVideoRequired(boolean z) {
        this.videoRequired = z;
    }

    public boolean isVideoRequired() {
        return this.videoRequired;
    }

    private cn() {
    }
}
