package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: MediaData */
public abstract class cd<T> {
    protected int height;
    @Nullable
    private T t;
    @NonNull
    protected final String url;
    protected int width;

    @NonNull
    public String getUrl() {
        return this.url;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public void setData(@Nullable T t2) {
        this.t = t2;
    }

    @Nullable
    public T getData() {
        return this.t;
    }

    protected cd(@NonNull String str) {
        this.url = str;
    }
}
