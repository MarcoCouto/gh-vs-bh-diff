package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.ads.MyTargetView;
import com.my.target.common.MyTargetPrivacy;
import com.my.target.mediation.MediationAdapter;
import com.my.target.mediation.MediationStandardAdAdapter;
import com.my.target.mediation.MediationStandardAdAdapter.MediationStandardAdListener;
import com.my.target.mediation.MyTargetStandardAdAdapter;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.List;

/* compiled from: MediationStandardAdEngine */
public class ba extends ax<MediationStandardAdAdapter> implements ap {
    @NonNull
    private final a adConfig;
    @Nullable
    com.my.target.ap.a bz;
    @NonNull
    final MyTargetView myTargetView;

    /* compiled from: MediationStandardAdEngine */
    class a implements MediationStandardAdListener {
        @NonNull
        private final cu bu;

        a(cu cuVar) {
            this.bu = cuVar;
        }

        public void onLoad(@NonNull View view, @NonNull MediationStandardAdAdapter mediationStandardAdAdapter) {
            if (ba.this.bl == mediationStandardAdAdapter) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationStandardAdEngine: data from ");
                sb.append(this.bu.getName());
                sb.append(" ad network loaded successfully");
                ah.a(sb.toString());
                ba.this.a(this.bu, true);
                ba.this.e(view);
                if (ba.this.bz != null) {
                    ba.this.bz.ac();
                }
            }
        }

        public void onNoAd(@NonNull String str, @NonNull MediationStandardAdAdapter mediationStandardAdAdapter) {
            if (ba.this.bl == mediationStandardAdAdapter) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationStandardAdEngine: no data from ");
                sb.append(this.bu.getName());
                sb.append(" ad network");
                ah.a(sb.toString());
                ba.this.a(this.bu, false);
            }
        }

        public void onShow(@NonNull MediationStandardAdAdapter mediationStandardAdAdapter) {
            if (ba.this.bl == mediationStandardAdAdapter) {
                Context context = ba.this.getContext();
                if (context != null) {
                    ib.a((List<dh>) this.bu.getStatHolder().N("playbackStarted"), context);
                }
                if (ba.this.bz != null) {
                    ba.this.bz.ad();
                }
            }
        }

        public void onClick(@NonNull MediationStandardAdAdapter mediationStandardAdAdapter) {
            if (ba.this.bl == mediationStandardAdAdapter) {
                Context context = ba.this.getContext();
                if (context != null) {
                    ib.a((List<dh>) this.bu.getStatHolder().N(String.CLICK), context);
                }
                if (ba.this.bz != null) {
                    ba.this.bz.onClick();
                }
            }
        }
    }

    public void pause() {
    }

    public void resume() {
    }

    public void start() {
    }

    public void stop() {
    }

    @NonNull
    public static final ba a(@NonNull MyTargetView myTargetView2, @NonNull ct ctVar, @NonNull a aVar) {
        return new ba(myTargetView2, ctVar, aVar);
    }

    public void a(@Nullable com.my.target.ap.a aVar) {
        this.bz = aVar;
    }

    private ba(@NonNull MyTargetView myTargetView2, @NonNull ct ctVar, @NonNull a aVar) {
        super(ctVar);
        this.myTargetView = myTargetView2;
        this.adConfig = aVar;
    }

    public void prepare() {
        super.n(this.myTargetView.getContext());
    }

    public void destroy() {
        if (this.bl == null) {
            ah.b("MediationStandardAdEngine error: can't destroy ad, adapter is not set");
            return;
        }
        this.myTargetView.removeAllViews();
        try {
            ((MediationStandardAdAdapter) this.bl).destroy();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationStandardAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
        this.bl = null;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: as */
    public MediationStandardAdAdapter an() {
        return new MyTargetStandardAdAdapter();
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull MediationStandardAdAdapter mediationStandardAdAdapter, @NonNull cu cuVar, @NonNull Context context) {
        a a2 = a.a(cuVar.getPlacementId(), cuVar.getPayload(), cuVar.bG(), this.adConfig.getCustomParams().getAge(), this.adConfig.getCustomParams().getGender(), MyTargetPrivacy.isConsentSpecified(), MyTargetPrivacy.isUserConsent(), MyTargetPrivacy.isUserAgeRestricted(), this.adConfig.isTrackingLocationEnabled(), this.adConfig.isTrackingEnvironmentEnabled());
        if (mediationStandardAdAdapter instanceof MyTargetStandardAdAdapter) {
            cv bH = cuVar.bH();
            if (bH instanceof dd) {
                ((MyTargetStandardAdAdapter) mediationStandardAdAdapter).setSection((dd) bH);
            }
        }
        try {
            mediationStandardAdAdapter.load(a2, this.myTargetView.getAdSize(), new a(cuVar), context);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationStandardAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public void ao() {
        if (this.bz != null) {
            this.bz.e("No data for available ad networks");
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull MediationAdapter mediationAdapter) {
        return mediationAdapter instanceof MediationStandardAdAdapter;
    }

    /* access modifiers changed from: 0000 */
    public void e(@NonNull View view) {
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(13);
        view.setLayoutParams(layoutParams);
        this.myTargetView.removeAllViews();
        this.myTargetView.addView(view);
    }
}
