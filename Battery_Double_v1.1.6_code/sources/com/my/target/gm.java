package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PromoCardImageAdapter */
public class gm extends Adapter<b> {
    @Nullable
    private OnClickListener cardClickListener;
    @NonNull
    private final Context context;
    @NonNull
    private final List<cq> nativeAdCards = new ArrayList();

    /* compiled from: PromoCardImageAdapter */
    public static class a extends FrameLayout {
        public int height;
        public int width;

        public a(@NonNull Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            int size = MeasureSpec.getSize(i);
            int size2 = MeasureSpec.getSize(i2);
            int mode = MeasureSpec.getMode(i);
            int mode2 = MeasureSpec.getMode(i2);
            if (size == 0) {
                size = this.width;
            }
            if (size2 == 0) {
                size2 = this.height;
            }
            super.onMeasure(MeasureSpec.makeMeasureSpec(size, mode), MeasureSpec.makeMeasureSpec(size2, mode2));
        }
    }

    /* compiled from: PromoCardImageAdapter */
    static class b extends ViewHolder {
        /* access modifiers changed from: private */
        @NonNull
        public final ge jZ;
        /* access modifiers changed from: private */
        @NonNull
        public final FrameLayout ka;

        b(@NonNull FrameLayout frameLayout, @NonNull ge geVar, @NonNull FrameLayout frameLayout2) {
            super(frameLayout);
            this.jZ = geVar;
            this.ka = frameLayout2;
        }
    }

    gm(@NonNull Context context2) {
        this.context = context2;
    }

    public int getItemCount() {
        return this.nativeAdCards.size();
    }

    public int getItemViewType(int i) {
        if (i == 0) {
            return 1;
        }
        return i == this.nativeAdCards.size() - 1 ? 2 : 0;
    }

    @NonNull
    /* renamed from: a */
    public b onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        a aVar = new a(this.context);
        aVar.setLayoutParams(new LayoutParams(-2, -1));
        ge geVar = new ge(this.context);
        ic.a((View) geVar, "card_media_view");
        aVar.addView(geVar, new FrameLayout.LayoutParams(-1, -1));
        FrameLayout frameLayout = new FrameLayout(this.context);
        if (viewGroup.isClickable()) {
            ic.a(frameLayout, 0, 1153821432);
        }
        aVar.addView(frameLayout, new FrameLayout.LayoutParams(-1, -1));
        return new b(aVar, geVar, frameLayout);
    }

    /* renamed from: a */
    public void onBindViewHolder(@NonNull b bVar, int i) {
        ImageData imageData = null;
        cq cqVar = i < this.nativeAdCards.size() ? (cq) this.nativeAdCards.get(i) : null;
        if (cqVar != null) {
            imageData = cqVar.getImage();
        }
        if (imageData != null) {
            bVar.jZ.setPlaceholderWidth(imageData.getWidth());
            bVar.jZ.setPlaceholderHeight(imageData.getHeight());
            Bitmap bitmap = imageData.getBitmap();
            if (bitmap != null) {
                bVar.jZ.setImageBitmap(bitmap);
            } else {
                hu.a(imageData, (ImageView) bVar.jZ);
            }
        }
        ge b2 = bVar.jZ;
        StringBuilder sb = new StringBuilder();
        sb.append("card_");
        sb.append(i);
        b2.setContentDescription(sb.toString());
        bVar.ka.setOnClickListener(this.cardClickListener);
    }

    /* renamed from: a */
    public void onViewRecycled(@NonNull b bVar) {
        int adapterPosition = bVar.getAdapterPosition();
        cq cqVar = (adapterPosition <= 0 || adapterPosition >= this.nativeAdCards.size()) ? null : (cq) this.nativeAdCards.get(adapterPosition);
        bVar.jZ.setImageData(null);
        ImageData image = cqVar != null ? cqVar.getImage() : null;
        if (image != null) {
            hu.b(image, (ImageView) bVar.jZ);
        }
        bVar.ka.setOnClickListener(null);
    }

    public void setCards(@NonNull List<cq> list) {
        this.nativeAdCards.addAll(list);
    }

    public void dispose() {
        this.nativeAdCards.clear();
        notifyDataSetChanged();
        this.cardClickListener = null;
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable OnClickListener onClickListener) {
        this.cardClickListener = onClickListener;
    }
}
