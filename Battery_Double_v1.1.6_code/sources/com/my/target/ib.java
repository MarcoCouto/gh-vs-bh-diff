package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.URLUtil;
import java.util.List;

/* compiled from: StatResolver */
public class ib {
    private static final ib nS = new ib();

    public static void a(@Nullable dh dhVar, @NonNull Context context) {
        nS.b(dhVar, context);
    }

    public static void a(@Nullable List<dh> list, @NonNull Context context) {
        nS.c(list, context);
    }

    public static void o(@Nullable String str, @NonNull Context context) {
        nS.p(str, context);
    }

    public static void b(@Nullable List<String> list, @NonNull Context context) {
        nS.d(list, context);
    }

    ib() {
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable final dh dhVar, @NonNull Context context) {
        if (dhVar != null) {
            final Context applicationContext = context.getApplicationContext();
            ai.b(new Runnable() {
                public void run() {
                    ib.this.c(dhVar);
                    String a = ib.this.al(dhVar.getUrl());
                    if (a != null) {
                        Cdo.cF().f(a, applicationContext);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(@Nullable final List<dh> list, @NonNull Context context) {
        if (list != null && list.size() > 0) {
            final Context applicationContext = context.getApplicationContext();
            ai.b(new Runnable() {
                public void run() {
                    Cdo cF = Cdo.cF();
                    for (dh dhVar : list) {
                        ib.this.c(dhVar);
                        String a = ib.this.al(dhVar.getUrl());
                        if (a != null) {
                            cF.f(a, applicationContext);
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void p(@Nullable final String str, @NonNull Context context) {
        if (!TextUtils.isEmpty(str)) {
            final Context applicationContext = context.getApplicationContext();
            ai.b(new Runnable() {
                public void run() {
                    String a = ib.this.al(str);
                    if (a != null) {
                        Cdo.cF().f(a, applicationContext);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(@Nullable final List<String> list, @NonNull Context context) {
        if (list != null && list.size() > 0) {
            final Context applicationContext = context.getApplicationContext();
            ai.b(new Runnable() {
                public void run() {
                    Cdo cF = Cdo.cF();
                    for (String a : list) {
                        String a2 = ib.this.al(a);
                        if (a2 != null) {
                            cF.f(a2, applicationContext);
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    public String al(@NonNull String str) {
        String decode = id.decode(str);
        if (URLUtil.isNetworkUrl(decode)) {
            return decode;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("invalid stat url: ");
        sb.append(decode);
        ah.a(sb.toString());
        return null;
    }

    /* access modifiers changed from: private */
    public void c(@NonNull dh dhVar) {
        if (dhVar instanceof dg) {
            float cq = ((dg) dhVar).cq();
            StringBuilder sb = new StringBuilder();
            sb.append("tracking progress stat value:");
            sb.append(cq);
            sb.append(" url:");
            sb.append(dhVar.getUrl());
            ah.a(sb.toString());
        } else if (dhVar instanceof df) {
            df dfVar = (df) dhVar;
            int cy = dfVar.cy();
            float cq2 = dfVar.cq();
            boolean cp = dfVar.cp();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("tracking ovv stat percent:");
            sb2.append(cy);
            sb2.append(" value:");
            sb2.append(cq2);
            sb2.append(" ovv:");
            sb2.append(cp);
            sb2.append(" url:");
            sb2.append(dhVar.getUrl());
            ah.a(sb2.toString());
        } else if (dhVar instanceof de) {
            de deVar = (de) dhVar;
            int cy2 = deVar.cy();
            float cq3 = deVar.cq();
            float duration = deVar.getDuration();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("tracking mrc stat percent: value:");
            sb3.append(cq3);
            sb3.append(" percent ");
            sb3.append(cy2);
            sb3.append(" duration:");
            sb3.append(duration);
            sb3.append(" url:");
            sb3.append(dhVar.getUrl());
            ah.a(sb3.toString());
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("tracking stat type:");
            sb4.append(dhVar.getType());
            sb4.append(" url:");
            sb4.append(dhVar.getUrl());
            ah.a(sb4.toString());
        }
    }
}
