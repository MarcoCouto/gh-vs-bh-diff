package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: HttpRequest */
public abstract class dn<T> {
    @Nullable
    protected String c;
    protected boolean cQ;
    protected boolean em;
    @Nullable
    protected T en;
    protected int responseCode = -1;

    /* access modifiers changed from: protected */
    @Nullable
    public abstract T c(@NonNull String str, @NonNull Context context);

    public boolean cD() {
        return this.em;
    }

    @Nullable
    public T cE() {
        return this.en;
    }

    @Nullable
    public String aH() {
        return this.c;
    }

    @Nullable
    public final T f(@NonNull String str, @NonNull Context context) {
        this.em = true;
        this.cQ = false;
        this.responseCode = -1;
        this.en = null;
        this.c = null;
        return c(str, context);
    }
}
