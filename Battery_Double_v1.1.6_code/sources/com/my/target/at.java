package com.my.target;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.appodeal.ads.AppodealNetworks;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.common.MyTargetActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: InterstitialAdHtmlEngine */
public class at extends as {
    @NonNull
    private final cl aU;
    @NonNull
    private final ArrayList<dg> aV = new ArrayList<>();
    @Nullable
    private WeakReference<ez> aW;
    @NonNull
    private final cy section;

    /* compiled from: InterstitialAdHtmlEngine */
    public static class a implements com.my.target.ez.a {
        @NonNull
        private final cl aU;
        @NonNull
        private final at aX;
        @NonNull
        private final InterstitialAd ad;

        a(@NonNull at atVar, @NonNull InterstitialAd interstitialAd, @NonNull cl clVar) {
            this.aX = atVar;
            this.ad = interstitialAd;
            this.aU = clVar;
        }

        public void b(@Nullable ch chVar, @Nullable String str, @NonNull Context context) {
            hr et = hr.et();
            if (TextUtils.isEmpty(str)) {
                et.b(this.aU, context);
            } else {
                et.c(this.aU, str, context);
            }
            InterstitialAdListener listener = this.ad.getListener();
            if (listener != null) {
                listener.onClick(this.ad);
            }
        }

        public void e(@NonNull String str) {
            this.aX.dismiss();
        }

        public void aj() {
            this.aX.dismiss();
        }

        public void a(@NonNull ch chVar, @NonNull Context context) {
            this.aX.a(chVar, context);
        }

        public void a(@NonNull ch chVar, @NonNull String str, @NonNull Context context) {
            this.aX.a(chVar, str, context);
        }

        public void a(@NonNull ch chVar, float f, float f2, @NonNull Context context) {
            this.aX.a(f, f2, context);
        }
    }

    @NonNull
    static at a(@NonNull InterstitialAd interstitialAd, @NonNull cl clVar, @NonNull cy cyVar) {
        return new at(interstitialAd, clVar, cyVar);
    }

    private at(InterstitialAd interstitialAd, @NonNull cl clVar, @NonNull cy cyVar) {
        super(interstitialAd);
        this.aU = clVar;
        this.section = cyVar;
        this.aV.addAll(clVar.getStatHolder().cv());
    }

    public void a(@NonNull fo foVar, @NonNull FrameLayout frameLayout) {
        super.a(foVar, frameLayout);
        b(frameLayout);
    }

    public void a(boolean z) {
        super.a(z);
        if (this.aW != null) {
            ez ezVar = (ez) this.aW.get();
            if (ezVar == null) {
                return;
            }
            if (z) {
                ezVar.resume();
            } else {
                ezVar.pause();
            }
        }
    }

    public void D() {
        super.D();
        if (this.aW != null) {
            ez ezVar = (ez) this.aW.get();
            if (ezVar != null) {
                ezVar.destroy();
            }
        }
        this.aW = null;
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        super.onActivityCreate(myTargetActivity, intent, frameLayout);
        b(frameLayout);
    }

    public void onActivityPause() {
        super.onActivityPause();
        if (this.aW != null) {
            ez ezVar = (ez) this.aW.get();
            if (ezVar != null) {
                ezVar.pause();
            }
        }
    }

    public void onActivityResume() {
        super.onActivityResume();
        if (this.aW != null) {
            ez ezVar = (ez) this.aW.get();
            if (ezVar != null) {
                ezVar.resume();
            }
        }
    }

    public void onActivityDestroy() {
        super.onActivityDestroy();
        if (this.aW != null) {
            ez ezVar = (ez) this.aW.get();
            if (ezVar != null) {
                ezVar.destroy();
            }
        }
        this.aW = null;
    }

    /* access modifiers changed from: protected */
    public boolean ai() {
        return this.aU.isAllowBackButton();
    }

    /* access modifiers changed from: 0000 */
    public void a(float f, float f2, @NonNull Context context) {
        if (!this.aV.isEmpty()) {
            float f3 = f2 - f;
            ArrayList arrayList = new ArrayList();
            Iterator it = this.aV.iterator();
            while (it.hasNext()) {
                dg dgVar = (dg) it.next();
                float cq = dgVar.cq();
                if (cq < 0.0f && dgVar.cr() >= 0.0f) {
                    cq = (f2 / 100.0f) * dgVar.cr();
                }
                if (cq >= 0.0f && cq <= f3) {
                    arrayList.add(dgVar);
                    it.remove();
                }
            }
            ib.a((List<dh>) arrayList, context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull ch chVar, @NonNull String str, @NonNull Context context) {
        ib.a((List<dh>) chVar.getStatHolder().N(str), context);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull ch chVar, @NonNull Context context) {
        ib.a((List<dh>) chVar.getStatHolder().N("playbackStarted"), context);
    }

    private void b(@NonNull ViewGroup viewGroup) {
        ez ezVar;
        if (AppodealNetworks.MRAID.equals(this.aU.getType())) {
            ezVar = et.u(viewGroup.getContext());
        } else {
            ezVar = ep.s(viewGroup.getContext());
        }
        this.aW = new WeakReference<>(ezVar);
        ezVar.a(new a(this, this.ad, this.aU));
        ezVar.a(this.section, this.aU);
        viewGroup.addView(ezVar.cT(), new LayoutParams(-1, -1));
    }
}
