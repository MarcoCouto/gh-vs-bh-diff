package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

@SuppressLint({"ViewConstructor"})
/* compiled from: PromoStyle1View */
public class gx extends ViewGroup implements gs {
    private boolean allowClose;
    @Nullable
    private String closeActionText;
    @Nullable
    private String closeDelayActionText;
    @NonNull
    private final Button ctaButton;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.er.a fc;
    private float fz;
    @NonNull
    private final TextView hN;
    /* access modifiers changed from: private */
    @NonNull
    public final LinearLayout jh;
    @NonNull
    private final TextView ji;
    @NonNull
    private final FrameLayout jj;
    @NonNull
    private final TextView jk;
    @NonNull
    private final fz jm;
    /* access modifiers changed from: private */
    @NonNull
    public final Runnable jq = new c();
    @Nullable
    private final Bitmap jt;
    @Nullable
    private final Bitmap ju;
    /* access modifiers changed from: private */
    public int jv;
    private final int jw;
    private boolean jx;
    /* access modifiers changed from: private */
    @NonNull
    public final gw kE;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.gt.a kK;
    @NonNull
    private final b kV;
    @NonNull
    private final TextView kW;
    @NonNull
    private final gi kX;
    /* access modifiers changed from: private */
    @NonNull
    public final gr kY;
    /* access modifiers changed from: private */
    @NonNull
    public final gr kZ;
    @NonNull
    private final gr la;
    @NonNull
    private final d lb = new d();
    @NonNull
    private final a lc = new a();
    private final int ld;
    private float le;
    private boolean lf;
    private final int padding;
    @NonNull
    private final gf starsRatingView;
    @NonNull
    private final ic uiUtils;

    /* compiled from: PromoStyle1View */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (view == gx.this.jh) {
                if (gx.this.fc != null) {
                    gx.this.fc.cY();
                }
                gx.this.ec();
            } else if (view == gx.this.kY) {
                if (gx.this.kE.isPlaying() && gx.this.fc != null) {
                    gx.this.fc.da();
                }
            } else if (view == gx.this.kZ) {
                if (gx.this.fc != null) {
                    if (gx.this.isPaused()) {
                        gx.this.fc.db();
                    } else {
                        gx.this.fc.cY();
                    }
                }
                gx.this.ec();
            }
        }
    }

    /* compiled from: PromoStyle1View */
    class b implements OnClickListener {
        b() {
        }

        public void onClick(View view) {
            if (view.isEnabled() && gx.this.kK != null) {
                gx.this.kK.dt();
            }
        }
    }

    /* compiled from: PromoStyle1View */
    class c implements Runnable {
        private c() {
        }

        public void run() {
            if (gx.this.jv == 2 || gx.this.jv == 0) {
                gx.this.ec();
            }
        }
    }

    /* compiled from: PromoStyle1View */
    class d implements OnClickListener {
        private d() {
        }

        public void onClick(View view) {
            gx.this.removeCallbacks(gx.this.jq);
            if (gx.this.jv == 2) {
                gx.this.ec();
                return;
            }
            if (gx.this.jv == 0 || gx.this.jv == 3) {
                gx.this.ed();
            }
            gx.this.postDelayed(gx.this.jq, 4000);
        }
    }

    @NonNull
    public View getView() {
        return this;
    }

    public gx(@NonNull Context context, boolean z) {
        super(context);
        this.kW = new TextView(context);
        this.hN = new TextView(context);
        this.starsRatingView = new gf(context);
        this.ctaButton = new Button(context);
        this.ji = new TextView(context);
        this.jj = new FrameLayout(context);
        this.kY = new gr(context);
        this.kZ = new gr(context);
        this.la = new gr(context);
        this.jk = new TextView(context);
        this.kE = new gw(context, ic.P(context), false, z);
        this.kX = new gi(context);
        this.jm = new fz(context);
        this.jh = new LinearLayout(context);
        this.uiUtils = ic.P(context);
        ic.a((View) this.kW, "dismiss_button");
        ic.a((View) this.hN, "title_text");
        ic.a((View) this.starsRatingView, "stars_view");
        ic.a((View) this.ctaButton, "cta_button");
        ic.a((View) this.ji, "replay_text");
        ic.a((View) this.jj, "shadow");
        ic.a((View) this.kY, "pause_button");
        ic.a((View) this.kZ, "play_button");
        ic.a((View) this.la, "replay_button");
        ic.a((View) this.jk, "domain_text");
        ic.a((View) this.kE, "media_view");
        ic.a((View) this.kX, "video_progress_wheel");
        ic.a((View) this.jm, "sound_button");
        this.jw = this.uiUtils.M(28);
        this.padding = this.uiUtils.M(16);
        this.ld = this.uiUtils.M(4);
        this.jt = fm.C(this.uiUtils.M(28));
        this.ju = fm.D(this.uiUtils.M(28));
        this.kV = new b();
        dc();
    }

    public void setBanner(@NonNull cn cnVar) {
        this.kE.a(cnVar, 1);
        co videoBanner = cnVar.getVideoBanner();
        if (videoBanner != null) {
            this.kX.setMax(cnVar.getDuration());
            this.jx = videoBanner.isAllowReplay();
            this.allowClose = cnVar.isAllowClose();
            this.ctaButton.setText(cnVar.getCtaText());
            this.hN.setText(cnVar.getTitle());
            if ("store".equals(cnVar.getNavigationType())) {
                if (cnVar.getRating() > 0.0f) {
                    this.starsRatingView.setVisibility(0);
                    this.starsRatingView.setRating(cnVar.getRating());
                } else {
                    this.starsRatingView.setVisibility(8);
                }
                this.jk.setVisibility(8);
            } else {
                this.starsRatingView.setVisibility(8);
                this.jk.setVisibility(0);
                this.jk.setText(cnVar.getDomain());
            }
            this.closeActionText = videoBanner.getCloseActionText();
            this.closeDelayActionText = videoBanner.getCloseDelayActionText();
            this.kW.setText(this.closeActionText);
            if (videoBanner.isAllowClose() && videoBanner.isAutoPlay()) {
                if (videoBanner.getAllowCloseDelay() > 0.0f) {
                    this.le = videoBanner.getAllowCloseDelay();
                    this.kW.setEnabled(false);
                    this.kW.setTextColor(-3355444);
                    this.kW.setPadding(this.ld, this.ld, this.ld, this.ld);
                    ic.a(this.kW, -2013265920, -2013265920, -3355444, this.uiUtils.M(1), this.uiUtils.M(4));
                    this.kW.setTextSize(2, 12.0f);
                } else {
                    this.kW.setPadding(this.padding, this.padding, this.padding, this.padding);
                    this.kW.setVisibility(0);
                }
            }
            this.ji.setText(videoBanner.getReplayActionText());
            Bitmap F = fm.F(getContext());
            if (F != null) {
                this.la.setImageBitmap(F);
            }
            if (videoBanner.isAutoPlay()) {
                this.kE.dm();
                ec();
            } else {
                dZ();
            }
            this.fz = videoBanner.getDuration();
            fz fzVar = this.jm;
            fzVar.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (gx.this.fc != null) {
                        gx.this.fc.cZ();
                    }
                }
            });
            fzVar.a(this.jt, false);
            fzVar.setContentDescription("sound_on");
        }
    }

    public void ej() {
        this.kW.setText(this.closeActionText);
        this.kW.setTextSize(2, 16.0f);
        this.kW.setVisibility(0);
        this.kW.setTextColor(-1);
        this.kW.setEnabled(true);
        this.kW.setPadding(this.padding, this.padding, this.padding, this.padding);
        ic.a(this.kW, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        this.lf = true;
    }

    public void finish() {
        this.kX.setVisibility(8);
        dX();
    }

    @NonNull
    public gw getPromoMediaView() {
        return this.kE;
    }

    public void setTimeChanged(float f) {
        if (!this.lf && this.allowClose && this.le > 0.0f && this.le >= f) {
            if (this.kW.getVisibility() != 0) {
                this.kW.setVisibility(0);
            }
            if (this.closeDelayActionText != null) {
                int ceil = (int) Math.ceil((double) (this.le - f));
                String valueOf = String.valueOf(ceil);
                if (this.le > 9.0f && ceil <= 9) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("0");
                    sb.append(valueOf);
                    valueOf = sb.toString();
                }
                this.kW.setText(this.closeDelayActionText.replace("%d", valueOf));
            }
        }
        if (this.kX.getVisibility() != 0) {
            this.kX.setVisibility(0);
        }
        this.kX.setProgress(f / this.fz);
        this.kX.setDigit((int) Math.ceil((double) (this.fz - f)));
    }

    public void destroy() {
        this.kE.destroy();
    }

    public void a(@NonNull cn cnVar) {
        this.kE.setOnClickListener(null);
        this.jm.setVisibility(8);
        ej();
        ec();
    }

    public final void A(boolean z) {
        fz fzVar = this.jm;
        if (z) {
            fzVar.a(this.ju, false);
            fzVar.setContentDescription("sound_off");
            return;
        }
        fzVar.a(this.jt, false);
        fzVar.setContentDescription("sound_on");
    }

    public void setInterstitialPromoViewListener(@Nullable com.my.target.gt.a aVar) {
        this.kK = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.jm.measure(MeasureSpec.makeMeasureSpec(this.jw, 1073741824), MeasureSpec.makeMeasureSpec(this.jw, 1073741824));
        this.kX.measure(MeasureSpec.makeMeasureSpec(this.jw, 1073741824), MeasureSpec.makeMeasureSpec(this.jw, 1073741824));
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.kE.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        int i3 = size - (this.padding << 1);
        int i4 = size2 - (this.padding << 1);
        this.kW.measure(MeasureSpec.makeMeasureSpec(i3 / 2, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.kY.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.kZ.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jh.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.starsRatingView.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jj.measure(MeasureSpec.makeMeasureSpec(this.kE.getMeasuredWidth(), 1073741824), MeasureSpec.makeMeasureSpec(this.kE.getMeasuredHeight(), 1073741824));
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.hN.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.jk.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        if (size > size2) {
            int measuredWidth = this.ctaButton.getMeasuredWidth();
            int measuredWidth2 = this.hN.getMeasuredWidth();
            if (this.kX.getMeasuredWidth() + measuredWidth2 + Math.max(this.starsRatingView.getMeasuredWidth(), this.jk.getMeasuredWidth()) + measuredWidth + (this.padding * 3) > i3) {
                int measuredWidth3 = (i3 - this.kX.getMeasuredWidth()) - (this.padding * 3);
                int i5 = measuredWidth3 / 3;
                this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.starsRatingView.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.jk.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.hN.measure(MeasureSpec.makeMeasureSpec(((measuredWidth3 - this.ctaButton.getMeasuredWidth()) - this.jk.getMeasuredWidth()) - this.starsRatingView.getMeasuredWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
            }
        } else {
            if (this.hN.getMeasuredHeight() + this.starsRatingView.getMeasuredHeight() + this.jk.getMeasuredHeight() + this.ctaButton.getMeasuredHeight() + (this.padding * 3) > (size2 - this.kE.getMeasuredHeight()) / 2) {
                this.ctaButton.setPadding(this.padding, this.padding / 2, this.padding, this.padding / 2);
                this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
            }
        }
        setMeasuredDimension(size, size2);
    }

    public boolean isPlaying() {
        return this.kE.isPlaying();
    }

    public boolean isPaused() {
        return this.kE.isPaused();
    }

    @NonNull
    public View getCloseButton() {
        return this.kW;
    }

    public void play() {
        this.kE.dm();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int measuredWidth = this.kE.getMeasuredWidth();
        int measuredHeight = this.kE.getMeasuredHeight();
        int i7 = (i5 - measuredWidth) >> 1;
        int i8 = (i6 - measuredHeight) >> 1;
        this.kE.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
        this.jj.layout(this.kE.getLeft(), this.kE.getTop(), this.kE.getRight(), this.kE.getBottom());
        int measuredWidth2 = this.kZ.getMeasuredWidth();
        int i9 = i3 >> 1;
        int i10 = measuredWidth2 >> 1;
        int i11 = i4 >> 1;
        int measuredHeight2 = this.kZ.getMeasuredHeight() >> 1;
        this.kZ.layout(i9 - i10, i11 - measuredHeight2, i10 + i9, measuredHeight2 + i11);
        int measuredWidth3 = this.kY.getMeasuredWidth();
        int i12 = measuredWidth3 >> 1;
        int measuredHeight3 = this.kY.getMeasuredHeight() >> 1;
        this.kY.layout(i9 - i12, i11 - measuredHeight3, i12 + i9, measuredHeight3 + i11);
        int measuredWidth4 = this.jh.getMeasuredWidth();
        int i13 = measuredWidth4 >> 1;
        int measuredHeight4 = this.jh.getMeasuredHeight() >> 1;
        this.jh.layout(i9 - i13, i11 - measuredHeight4, i9 + i13, i11 + measuredHeight4);
        this.kW.layout(this.padding, this.padding, this.padding + this.kW.getMeasuredWidth(), this.padding + this.kW.getMeasuredHeight());
        if (i5 > i6) {
            int max = Math.max(this.ctaButton.getMeasuredHeight(), Math.max(this.hN.getMeasuredHeight(), this.starsRatingView.getMeasuredHeight()));
            this.ctaButton.layout((i5 - this.padding) - this.ctaButton.getMeasuredWidth(), ((i6 - this.padding) - this.ctaButton.getMeasuredHeight()) - ((max - this.ctaButton.getMeasuredHeight()) >> 1), i5 - this.padding, (i6 - this.padding) - ((max - this.ctaButton.getMeasuredHeight()) >> 1));
            this.jm.layout((this.ctaButton.getRight() - this.jm.getMeasuredWidth()) + this.jm.getPadding(), (((this.kE.getBottom() - (this.padding << 1)) - this.jm.getMeasuredHeight()) - max) + this.jm.getPadding(), this.ctaButton.getRight() + this.jm.getPadding(), ((this.kE.getBottom() - (this.padding << 1)) - max) + this.jm.getPadding());
            this.starsRatingView.layout((this.ctaButton.getLeft() - this.padding) - this.starsRatingView.getMeasuredWidth(), ((i6 - this.padding) - this.starsRatingView.getMeasuredHeight()) - ((max - this.starsRatingView.getMeasuredHeight()) >> 1), this.ctaButton.getLeft() - this.padding, (i6 - this.padding) - ((max - this.starsRatingView.getMeasuredHeight()) >> 1));
            this.jk.layout((this.ctaButton.getLeft() - this.padding) - this.jk.getMeasuredWidth(), ((i6 - this.padding) - this.jk.getMeasuredHeight()) - ((max - this.jk.getMeasuredHeight()) >> 1), this.ctaButton.getLeft() - this.padding, (i6 - this.padding) - ((max - this.jk.getMeasuredHeight()) >> 1));
            int min = Math.min(this.starsRatingView.getLeft(), this.jk.getLeft());
            this.hN.layout((min - this.padding) - this.hN.getMeasuredWidth(), ((i6 - this.padding) - this.hN.getMeasuredHeight()) - ((max - this.hN.getMeasuredHeight()) >> 1), min - this.padding, (i6 - this.padding) - ((max - this.hN.getMeasuredHeight()) >> 1));
            this.kX.layout(this.padding, ((i6 - this.padding) - this.kX.getMeasuredHeight()) - ((max - this.kX.getMeasuredHeight()) >> 1), this.padding + this.kX.getMeasuredWidth(), (i6 - this.padding) - ((max - this.kX.getMeasuredHeight()) >> 1));
            return;
        }
        this.jm.layout(((this.kE.getRight() - this.padding) - this.jm.getMeasuredWidth()) + this.jm.getPadding(), ((this.kE.getBottom() - this.padding) - this.jm.getMeasuredHeight()) + this.jm.getPadding(), (this.kE.getRight() - this.padding) + this.jm.getPadding(), (this.kE.getBottom() - this.padding) + this.jm.getPadding());
        int i14 = this.padding;
        int measuredHeight5 = this.hN.getMeasuredHeight() + this.starsRatingView.getMeasuredHeight() + this.jk.getMeasuredHeight() + this.ctaButton.getMeasuredHeight();
        int bottom = getBottom() - this.kE.getBottom();
        if ((i14 * 3) + measuredHeight5 > bottom) {
            i14 = (bottom - measuredHeight5) / 3;
        }
        int i15 = i5 >> 1;
        this.hN.layout(i15 - (this.hN.getMeasuredWidth() >> 1), this.kE.getBottom() + i14, (this.hN.getMeasuredWidth() >> 1) + i15, this.kE.getBottom() + i14 + this.hN.getMeasuredHeight());
        this.starsRatingView.layout(i15 - (this.starsRatingView.getMeasuredWidth() >> 1), this.hN.getBottom() + i14, (this.starsRatingView.getMeasuredWidth() >> 1) + i15, this.hN.getBottom() + i14 + this.starsRatingView.getMeasuredHeight());
        this.jk.layout(i15 - (this.jk.getMeasuredWidth() >> 1), this.hN.getBottom() + i14, (this.jk.getMeasuredWidth() >> 1) + i15, this.hN.getBottom() + i14 + this.jk.getMeasuredHeight());
        this.ctaButton.layout(i15 - (this.ctaButton.getMeasuredWidth() >> 1), this.starsRatingView.getBottom() + i14, i15 + (this.ctaButton.getMeasuredWidth() >> 1), this.starsRatingView.getBottom() + i14 + this.ctaButton.getMeasuredHeight());
        this.kX.layout(this.padding, (this.kE.getBottom() - this.padding) - this.kX.getMeasuredHeight(), this.padding + this.kX.getMeasuredWidth(), this.kE.getBottom() - this.padding);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        boolean z = this.fz <= 0.0f || isHardwareAccelerated();
        if (this.kK != null) {
            this.kK.s(z);
        }
    }

    private void dc() {
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        int i = this.padding;
        this.jm.setId(je);
        this.kE.setOnClickListener(this.lb);
        this.kE.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.kE.initView();
        this.jj.setBackgroundColor(-1728053248);
        this.jj.setVisibility(8);
        this.kW.setTextSize(2, 16.0f);
        this.kW.setTransformationMethod(null);
        this.kW.setEllipsize(TruncateAt.END);
        this.kW.setVisibility(8);
        if (VERSION.SDK_INT >= 17) {
            this.kW.setTextAlignment(4);
        }
        this.kW.setTextColor(-1);
        ic.a(this.kW, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        this.hN.setMaxLines(2);
        this.hN.setEllipsize(TruncateAt.END);
        this.hN.setTextSize(2, 18.0f);
        this.hN.setTextColor(-1);
        ic.a(this.ctaButton, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        this.ctaButton.setTextColor(-1);
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setGravity(1);
        this.ctaButton.setTextSize(2, 16.0f);
        this.ctaButton.setMinimumWidth(this.uiUtils.M(100));
        this.ctaButton.setPadding(i, i, i, i);
        this.hN.setShadowLayer((float) this.uiUtils.M(1), (float) this.uiUtils.M(1), (float) this.uiUtils.M(1), ViewCompat.MEASURED_STATE_MASK);
        this.jk.setTextColor(-3355444);
        this.jk.setMaxEms(10);
        this.jk.setShadowLayer((float) this.uiUtils.M(1), (float) this.uiUtils.M(1), (float) this.uiUtils.M(1), ViewCompat.MEASURED_STATE_MASK);
        this.jh.setOnClickListener(this.lc);
        this.jh.setGravity(17);
        this.jh.setVisibility(8);
        this.jh.setPadding(this.uiUtils.M(8), 0, this.uiUtils.M(8), 0);
        this.ji.setSingleLine();
        this.ji.setEllipsize(TruncateAt.END);
        this.ji.setTypeface(this.ji.getTypeface(), 1);
        this.ji.setTextColor(-1);
        this.ji.setTextSize(2, 16.0f);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.leftMargin = this.uiUtils.M(4);
        this.la.setPadding(this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16));
        this.kY.setOnClickListener(this.lc);
        this.kY.setVisibility(8);
        this.kY.setPadding(this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16));
        this.kZ.setOnClickListener(this.lc);
        this.kZ.setVisibility(8);
        this.kZ.setPadding(this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16));
        Bitmap G = fm.G(getContext());
        if (G != null) {
            this.kZ.setImageBitmap(G);
        }
        Bitmap H = fm.H(getContext());
        if (H != null) {
            this.kY.setImageBitmap(H);
        }
        ic.a(this.kY, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        ic.a(this.kZ, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        ic.a(this.la, -2013265920, -1, -1, this.uiUtils.M(1), this.uiUtils.M(4));
        this.starsRatingView.setStarSize(this.uiUtils.M(12));
        this.kX.setVisibility(8);
        addView(this.kE);
        addView(this.jj);
        addView(this.jm);
        addView(this.kW);
        addView(this.kX);
        addView(this.jh);
        addView(this.kY);
        addView(this.kZ);
        addView(this.starsRatingView);
        addView(this.jk);
        addView(this.ctaButton);
        addView(this.hN);
        this.jh.addView(this.la);
        this.jh.addView(this.ji, layoutParams);
    }

    public void setMediaListener(@Nullable com.my.target.er.a aVar) {
        this.fc = aVar;
        this.kE.setInterstitialPromoViewListener(aVar);
    }

    public void resume() {
        this.kE.resume();
    }

    public void setClickArea(@NonNull ca caVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Apply click area ");
        sb.append(caVar.bm());
        sb.append(" to view");
        ah.a(sb.toString());
        if (caVar.f456do) {
            setOnClickListener(this.kV);
        }
        if (caVar.di || caVar.f456do) {
            this.ctaButton.setOnClickListener(this.kV);
        } else {
            this.ctaButton.setOnClickListener(null);
            this.ctaButton.setEnabled(false);
        }
        if (caVar.dc || caVar.f456do) {
            this.hN.setOnClickListener(this.kV);
        } else {
            this.hN.setOnClickListener(null);
        }
        if (caVar.dg || caVar.f456do) {
            this.starsRatingView.setOnClickListener(this.kV);
        } else {
            this.starsRatingView.setOnClickListener(null);
        }
        if (caVar.dl || caVar.f456do) {
            this.jk.setOnClickListener(this.kV);
        } else {
            this.jk.setOnClickListener(null);
        }
        if (caVar.dn || caVar.f456do) {
            setOnClickListener(this.kV);
        }
    }

    public void stop(boolean z) {
        this.kE.B(true);
    }

    public void pause() {
        if (this.jv == 0 || this.jv == 2) {
            dZ();
            this.kE.pause();
        }
    }

    public void G(int i) {
        this.kE.G(i);
    }

    public void ei() {
        this.kE.ei();
        ea();
    }

    /* access modifiers changed from: private */
    public void ec() {
        this.jv = 0;
        this.jh.setVisibility(8);
        this.kZ.setVisibility(8);
        this.kY.setVisibility(8);
        this.jj.setVisibility(8);
    }

    private void dZ() {
        this.jv = 1;
        this.jh.setVisibility(8);
        this.kZ.setVisibility(0);
        this.kY.setVisibility(8);
        this.jj.setVisibility(0);
    }

    private void ea() {
        this.jh.setVisibility(8);
        this.kZ.setVisibility(8);
        if (this.jv != 2) {
            this.kY.setVisibility(8);
        }
    }

    private void dX() {
        this.jv = 4;
        if (this.jx) {
            this.jh.setVisibility(0);
            this.jj.setVisibility(0);
        }
        this.kZ.setVisibility(8);
        this.kY.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void ed() {
        this.jv = 2;
        this.jh.setVisibility(8);
        this.kZ.setVisibility(8);
        this.kY.setVisibility(0);
        this.jj.setVisibility(8);
    }
}
