package com.my.target;

import android.support.annotation.NonNull;
import android.view.View;

/* compiled from: PromoStyle2ProgressView */
public interface hg {
    @NonNull
    View eo();

    void setColor(int i);

    void setMaxTime(float f);

    void setTimeChanged(float f);

    void setVisible(boolean z);
}
