package com.my.target;

import android.support.annotation.Nullable;

/* compiled from: JsErrorEvent */
public class bp extends bl {
    @Nullable
    private String c;

    public bp(@Nullable String str) {
        super("onError");
        this.c = str;
    }

    public bp() {
        super("onError");
    }

    @Nullable
    public String aH() {
        return this.c;
    }
}
