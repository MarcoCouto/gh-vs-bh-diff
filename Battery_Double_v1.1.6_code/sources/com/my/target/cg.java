package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ResearchViewabilityStat */
public class cg extends de {
    private int dA;
    private int dz;

    public static cg u(@NonNull String str) {
        return new cg(str);
    }

    private cg(@NonNull String str) {
        super("playheadViewabilityValue", str);
    }

    public int by() {
        return this.dz;
    }

    public void o(int i) {
        this.dz = i;
    }

    public int bz() {
        return this.dA;
    }

    public void p(int i) {
        this.dA = i;
    }
}
