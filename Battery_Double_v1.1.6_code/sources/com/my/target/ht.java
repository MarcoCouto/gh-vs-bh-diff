package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Locale;

/* compiled from: EncryptionUtils */
public class ht {
    @Nullable
    public static String ai(@NonNull String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("md5");
            instance.update(str.getBytes(Charset.forName("UTF-8")));
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            for (byte valueOf : digest) {
                sb.append(String.format("%02X", new Object[]{Byte.valueOf(valueOf)}));
            }
            return sb.toString().toLowerCase(Locale.ROOT);
        } catch (Exception unused) {
            return null;
        }
    }
}
