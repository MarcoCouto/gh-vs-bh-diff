package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: PlaybackStatsTracker */
public class hy {
    @NonNull
    private final Context context;
    @NonNull
    private Set<dg> ff;
    private boolean nK;
    @NonNull
    private final di statHolder;

    public static hy b(@NonNull co coVar, @NonNull Context context2) {
        return new hy(coVar, context2.getApplicationContext());
    }

    private hy(@NonNull co coVar, @NonNull Context context2) {
        this.context = context2;
        this.statHolder = coVar.getStatHolder();
        this.ff = coVar.getStatHolder().cv();
    }

    public void trackProgress(float f) {
        if (!this.nK) {
            ib.a((List<dh>) this.statHolder.N("playbackStarted"), this.context);
            this.nK = true;
        }
        if (!this.ff.isEmpty()) {
            Iterator it = this.ff.iterator();
            while (it.hasNext()) {
                dg dgVar = (dg) it.next();
                if (dgVar.cq() <= f) {
                    ib.a((dh) dgVar, this.context);
                    it.remove();
                }
            }
        }
    }

    public void trackResume() {
        ib.a((List<dh>) this.statHolder.N("playbackResumed"), this.context);
    }

    public void ez() {
        ib.a((List<dh>) this.statHolder.N("playbackPaused"), this.context);
    }

    public void H(boolean z) {
        ib.a((List<dh>) this.statHolder.N(z ? "volumeOn" : "volumeOff"), this.context);
    }

    public void eA() {
        ib.a((List<dh>) this.statHolder.N("closedByUser"), this.context);
    }

    public void eB() {
        ib.a((List<dh>) this.statHolder.N("error"), this.context);
    }

    public void refresh() {
        this.ff = this.statHolder.cv();
        this.nK = false;
    }
}
