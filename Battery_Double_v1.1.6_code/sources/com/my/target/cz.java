package com.my.target;

import android.support.annotation.NonNull;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialSliderAdSection */
public class cz extends cv {
    private int backgroundColor = -16368537;
    @NonNull
    private final ArrayList<cm> banners = new ArrayList<>();
    private ImageData closeIcon;
    private int dK = -1;
    private int dL = -14696781;

    @NonNull
    public static cz bQ() {
        return new cz();
    }

    public int bR() {
        return this.dL;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public ImageData getCloseIcon() {
        return this.closeIcon;
    }

    public int bS() {
        return this.dK;
    }

    public void s(int i) {
        this.dL = i;
    }

    public void setBackgroundColor(int i) {
        this.backgroundColor = i;
    }

    public void setCloseIcon(ImageData imageData) {
        this.closeIcon = imageData;
    }

    public void t(int i) {
        this.dK = i;
    }

    private cz() {
    }

    public void c(@NonNull cm cmVar) {
        this.banners.add(cmVar);
    }

    public void d(@NonNull cm cmVar) {
        this.banners.remove(cmVar);
    }

    @NonNull
    public List<cm> bT() {
        return new ArrayList(this.banners);
    }

    public int getBannersCount() {
        return this.banners.size();
    }
}
