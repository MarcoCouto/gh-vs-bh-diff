package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;

/* compiled from: LoggingUtils */
public final class hv {
    public static boolean nH;

    public static void N(@NonNull Context context) {
        if (!ah.enabled && !nH) {
            try {
                Integer num = (Integer) context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("com.my.target.debugMode");
                if (num != null && num.intValue() == 1) {
                    ah.enabled = true;
                }
            } catch (Exception unused) {
            }
            nH = true;
        }
    }
}
