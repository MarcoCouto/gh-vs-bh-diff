package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;

/* compiled from: Mediation */
public class ct {
    @NonNull
    private final ArrayList<cu> dC = new ArrayList<>();
    private int dD = 60;

    @NonNull
    public static final ct bB() {
        return new ct();
    }

    public int bC() {
        return this.dD;
    }

    public void r(int i) {
        this.dD = i;
    }

    private ct() {
    }

    public void b(@NonNull cu cuVar) {
        int size = this.dC.size();
        for (int i = 0; i < size; i++) {
            if (cuVar.getPriority() > ((cu) this.dC.get(i)).getPriority()) {
                this.dC.add(i, cuVar);
                return;
            }
        }
        this.dC.add(cuVar);
    }

    @Nullable
    public cu bD() {
        if (this.dC.isEmpty()) {
            return null;
        }
        return (cu) this.dC.remove(0);
    }

    public boolean bE() {
        return !this.dC.isEmpty();
    }
}
