package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import com.my.target.nativeads.views.MediaAdView;

@SuppressLint({"ViewConstructor"})
/* compiled from: SliderImageView */
public class hm extends FrameLayout {
    @Nullable
    private fu ageRestrictionLabel;
    @NonNull
    private final ge imageView = new ge(getContext());
    private final int mW;
    private final int mX;
    @NonNull
    private final RelativeLayout mY;
    @NonNull
    private final ic uiUtils;

    public hm(@NonNull Context context, int i) {
        super(context);
        this.uiUtils = ic.P(context);
        this.mY = new RelativeLayout(context);
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(13, -1);
        this.imageView.setLayoutParams(layoutParams);
        this.imageView.setScaleType(ScaleType.FIT_XY);
        this.mW = this.uiUtils.M(8);
        this.mX = this.uiUtils.M(8);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 17;
        addView(this.mY, layoutParams2);
        this.mY.addView(this.imageView);
        this.mY.setBackgroundColor(i);
        setClipToPadding(false);
        if (VERSION.SDK_INT >= 21) {
            this.mY.setElevation((float) this.uiUtils.M(4));
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public ge getImageView() {
        return this.imageView;
    }

    public void setImage(@NonNull ImageData imageData) {
        this.imageView.setPlaceholderWidth(imageData.getWidth());
        this.imageView.setPlaceholderHeight(imageData.getHeight());
        hu.a(imageData, (ImageView) this.imageView);
        if (getResources().getConfiguration().orientation == 2) {
            setPadding(this.mW, this.mW, this.mW, this.mW);
        } else {
            setPadding(this.mX, this.mX, this.mX, this.mX);
        }
    }

    public void setAgeRestrictions(@NonNull String str) {
        if (this.ageRestrictionLabel == null) {
            this.ageRestrictionLabel = new fu(getContext());
            this.ageRestrictionLabel.f(1, -7829368);
            this.ageRestrictionLabel.setPadding(this.uiUtils.M(2), 0, 0, 0);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.setMargins(this.uiUtils.M(8), this.uiUtils.M(20), this.uiUtils.M(8), this.uiUtils.M(20));
            this.ageRestrictionLabel.setLayoutParams(layoutParams);
            this.ageRestrictionLabel.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
            this.ageRestrictionLabel.a(1, MediaAdView.COLOR_PLACEHOLDER_GRAY, this.uiUtils.M(3));
            this.ageRestrictionLabel.setBackgroundColor(1711276032);
            this.mY.addView(this.ageRestrictionLabel);
        }
        this.ageRestrictionLabel.setText(str);
    }
}
