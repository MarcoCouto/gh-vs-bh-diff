package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.View.OnClickListener;
import java.util.List;

/* compiled from: InterstitialPromoPresenter */
public class ew implements eu {
    @NonNull
    private final cn ba;
    private long eV;
    private long eW;
    @NonNull
    private final d fB;
    @NonNull
    private final gt fC;
    @Nullable
    private gs fD;
    @Nullable
    private hc fE;
    @Nullable
    private eo fF;
    @Nullable
    private er fG;
    @Nullable
    private b fH;

    /* compiled from: InterstitialPromoPresenter */
    static class a implements OnClickListener {
        private ew fI;

        a(ew ewVar) {
            this.fI = ewVar;
        }

        public void onClick(View view) {
            er dq = this.fI.dq();
            if (dq != null) {
                dq.cW();
            }
            b dp = this.fI.dp();
            if (dp != null) {
                dp.aj();
            }
        }
    }

    /* compiled from: InterstitialPromoPresenter */
    public interface b extends com.my.target.eu.a {
        void C();

        void al();
    }

    /* compiled from: InterstitialPromoPresenter */
    static class c implements com.my.target.gt.a {
        @NonNull
        private final ew fI;

        c(@NonNull ew ewVar) {
            this.fI = ewVar;
        }

        public void s(boolean z) {
            if (!z) {
                er dq = this.fI.dq();
                if (dq != null) {
                    dq.a(this.fI.dr());
                    dq.destroy();
                }
                this.fI.o(null);
            }
        }

        public void dt() {
            b dp = this.fI.dp();
            if (dp != null) {
                dp.b(this.fI.dr(), null, this.fI.cT().getContext());
            }
        }
    }

    /* compiled from: InterstitialPromoPresenter */
    static class d implements Runnable {
        @NonNull
        private final gt eM;

        d(@NonNull gt gtVar) {
            this.eM = gtVar;
        }

        public void run() {
            ah.a("banner became just closeable");
            this.eM.ej();
        }
    }

    @NonNull
    public static ew a(@NonNull cn cnVar, boolean z, @NonNull Context context) {
        return new ew(cnVar, z, context);
    }

    @Nullable
    public b dp() {
        return this.fH;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public er dq() {
        return this.fG;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void o(@Nullable er erVar) {
        this.fG = erVar;
    }

    @NonNull
    public cn dr() {
        return this.ba;
    }

    public void b(@Nullable b bVar) {
        this.fH = bVar;
        if (this.fG != null) {
            this.fG.a(bVar);
        }
        if (this.fF != null) {
            this.fF.a(bVar);
        }
    }

    public void r(boolean z) {
        if (this.fG != null) {
            this.fG.r(z);
        }
    }

    private ew(@NonNull cn cnVar, boolean z, @NonNull Context context) {
        this.ba = cnVar;
        c cVar = new c(this);
        co videoBanner = cnVar.getVideoBanner();
        if (!cnVar.getInterstitialAdCards().isEmpty()) {
            hc hcVar = new hc(context);
            this.fE = hcVar;
            this.fC = hcVar;
        } else if (videoBanner == null || cnVar.getStyle() != 1) {
            gv gvVar = new gv(context, z);
            this.fD = gvVar;
            this.fC = gvVar;
        } else {
            gx gxVar = new gx(context, z);
            this.fD = gxVar;
            this.fC = gxVar;
        }
        this.fB = new d(this.fC);
        this.fC.setInterstitialPromoViewListener(cVar);
        this.fC.setBanner(cnVar);
        this.fC.getCloseButton().setOnClickListener(new a(this));
        this.fC.setClickArea(cnVar.getClickArea());
        if (!(this.fD == null || videoBanner == null)) {
            this.fG = er.a(videoBanner, this.fD);
            this.fG.a(videoBanner, context);
            if (videoBanner.isAutoPlay()) {
                this.eV = 0;
            }
        }
        if (videoBanner == null || !videoBanner.isAutoPlay()) {
            this.eW = (long) (cnVar.getAllowCloseDelay() * 1000.0f);
            if (this.eW > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("banner will be allowed to close in ");
                sb.append(this.eW);
                sb.append(" millis");
                ah.a(sb.toString());
                a(this.eW);
            } else {
                ah.a("banner is allowed to close");
                this.fC.ej();
            }
        }
        List interstitialAdCards = cnVar.getInterstitialAdCards();
        if (!interstitialAdCards.isEmpty() && this.fE != null) {
            this.fF = eo.a(interstitialAdCards, this.fE);
        }
    }

    public void pause() {
        if (this.fG != null) {
            this.fG.pause();
        }
        this.fC.getView().removeCallbacks(this.fB);
        if (this.eV > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.eV;
            if (currentTimeMillis <= 0 || currentTimeMillis >= this.eW) {
                this.eW = 0;
            } else {
                this.eW -= currentTimeMillis;
            }
        }
    }

    public void resume() {
        if (this.fG == null && this.eW > 0) {
            a(this.eW);
        }
    }

    @NonNull
    public View cT() {
        return this.fC.getView();
    }

    public void stop() {
        if (this.fG != null) {
            this.fG.stop();
        }
    }

    public void destroy() {
        if (this.fG != null) {
            this.fG.destroy();
        }
    }

    public void ds() {
        if (this.fG != null) {
            this.fG.a(this.ba);
        }
    }

    private void a(long j) {
        if (this.fD != null) {
            this.fD.getView().removeCallbacks(this.fB);
            this.eV = System.currentTimeMillis();
            this.fD.getView().postDelayed(this.fB, j);
        }
    }
}
