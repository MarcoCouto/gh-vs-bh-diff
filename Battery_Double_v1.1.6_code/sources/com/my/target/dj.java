package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ViewabilityStat */
public abstract class dj extends dh {
    private float ed = -1.0f;
    private int ej;
    private float value = -1.0f;

    dj(@NonNull String str, @NonNull String str2) {
        super(str, str2);
    }

    public float cq() {
        return this.value;
    }

    public void i(float f) {
        this.value = f;
    }

    public float cr() {
        return this.ed;
    }

    public void j(float f) {
        this.ed = f;
    }

    public int cy() {
        return this.ej;
    }

    public void w(int i) {
        this.ej = i;
    }
}
