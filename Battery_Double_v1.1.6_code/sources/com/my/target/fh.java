package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import java.lang.reflect.Method;

/* compiled from: GoogleAIdDataProvider */
public class fh extends ff {
    private boolean gH = false;

    fh() {
    }

    @WorkerThread
    public synchronized void collectData(@NonNull Context context) {
        if (ai.isMainThread()) {
            ah.a("You must not call collectData method from main thread");
        } else if (!this.gH) {
            A(context);
            this.gH = true;
        }
    }

    private void A(@NonNull Context context) {
        ah.a("send google AId");
        try {
            Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            if (cls != null) {
                Method method = cls.getMethod("getAdvertisingIdInfo", new Class[]{Context.class});
                if (method != null) {
                    Object invoke = method.invoke(null, new Object[]{context});
                    if (invoke != null) {
                        Method method2 = invoke.getClass().getMethod("getId", new Class[0]);
                        if (method2 != null) {
                            String str = (String) method2.invoke(invoke, new Object[0]);
                            addParam(TapjoyConstants.TJC_ADVERTISING_ID, str);
                            StringBuilder sb = new StringBuilder();
                            sb.append("google AId: ");
                            sb.append(str);
                            ah.a(sb.toString());
                        }
                        Method method3 = invoke.getClass().getMethod(RequestParameters.isLAT, new Class[0]);
                        if (method3 != null) {
                            boolean booleanValue = ((Boolean) method3.invoke(invoke, new Object[0])).booleanValue();
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(booleanValue ^ true ? 1 : 0);
                            sb2.append("");
                            addParam("advertising_tracking_enabled", sb2.toString());
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("ad tracking enabled: ");
                            sb3.append(!booleanValue);
                            ah.a(sb3.toString());
                        }
                    }
                }
            }
        } catch (Throwable th) {
            ah.a(th.toString());
            ah.a("failed to send google AId");
        }
    }
}
