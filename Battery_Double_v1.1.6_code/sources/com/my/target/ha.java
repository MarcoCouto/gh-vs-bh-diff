package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import android.view.View.MeasureSpec;

/* compiled from: CarouselLayoutManager */
class ha extends LinearLayoutManager {
    private final int iV;
    private int lr;
    @Nullable
    private a ls;
    private int lt;
    private int lu;
    private int lv;
    private int lw;

    /* compiled from: CarouselLayoutManager */
    public interface a {
        void dR();
    }

    ha(Context context) {
        super(context, 0, false);
        this.iV = ic.P(context).M(4);
    }

    public void measureChildWithMargins(View view, int i, int i2) {
        int height = getHeight();
        int width = getWidth();
        if (height != this.lw || width != this.lv || this.lt <= 0 || this.lu <= 0) {
            view.measure(MeasureSpec.makeMeasureSpec(getWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(getHeight(), Integer.MIN_VALUE));
            float width2 = ((float) getWidth()) / ((float) view.getMeasuredWidth());
            if (width2 > 1.0f) {
                double d = (double) ((float) width);
                double floor = Math.floor((double) width2) + 0.5d;
                Double.isNaN(d);
                this.lt = (int) (d / floor);
            } else {
                this.lt = (int) (((float) width) / 1.5f);
            }
            this.lu = height;
            this.lv = width;
            this.lw = height;
        }
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (view != getChildAt(0)) {
            layoutParams.leftMargin = ic.a(this.lr / 2, view.getContext());
        }
        if (view != getChildAt(getChildCount())) {
            layoutParams.rightMargin = ic.a(this.lr / 2, view.getContext());
        }
        view.measure(getChildMeasureSpec(width, getWidthMode(), 0, this.lt, canScrollHorizontally()), getChildMeasureSpec(height, getHeightMode(), this.iV, height - (this.iV * 2), canScrollVertically()));
    }

    public void a(@Nullable a aVar) {
        this.ls = aVar;
    }

    public void H(int i) {
        this.lr = i;
    }

    public boolean g(@NonNull View view) {
        int findFirstCompletelyVisibleItemPosition = findFirstCompletelyVisibleItemPosition();
        int position = getPosition(view);
        return findFirstCompletelyVisibleItemPosition <= position && position <= findLastCompletelyVisibleItemPosition();
    }

    public void onLayoutCompleted(State state) {
        super.onLayoutCompleted(state);
        if (this.ls != null) {
            this.ls.dR();
        }
    }
}
