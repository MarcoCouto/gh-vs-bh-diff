package com.my.target.instreamads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ah;
import com.my.target.ar;
import com.my.target.b.C0056b;
import com.my.target.ci;
import com.my.target.co;
import com.my.target.common.BaseAd;
import com.my.target.common.models.AudioData;
import com.my.target.common.models.ShareButtonData;
import com.my.target.cx;
import com.my.target.da;
import com.my.target.hw;
import com.my.target.i;
import com.my.target.i.b;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public final class InstreamAudioAd extends BaseAd {
    private static final int DEFAULT_LOADING_TIMEOUT_SECONDS = 10;
    private static final int MIN_LOADING_TIMEOUT_SECONDS = 5;
    private float audioDuration;
    @NonNull
    private final Context context;
    @Nullable
    private ar engine;
    @Nullable
    private InstreamAudioAdListener listener;
    @NonNull
    private final AtomicBoolean loadOnce = new AtomicBoolean();
    private int loadingTimeoutSeconds = 10;
    @Nullable
    private float[] midpoints;
    @Nullable
    private InstreamAudioAdPlayer player;
    @Nullable
    private cx section;
    @Nullable
    private float[] userMidpoints;
    private float volume = 1.0f;

    public static final class InstreamAdCompanionBanner {
        @Nullable
        public final String adSlotID;
        @Nullable
        public final String apiFramework;
        public final int assetHeight;
        public final int assetWidth;
        public final int expandedHeight;
        public final int expandedWidth;
        public final int height;
        @Nullable
        public final String htmlResource;
        @Nullable
        public final String iframeResource;
        @Nullable
        public final String required;
        @Nullable
        public final String staticResource;
        public final int width;

        @NonNull
        public static InstreamAdCompanionBanner newBanner(@NonNull ci ciVar) {
            InstreamAdCompanionBanner instreamAdCompanionBanner = new InstreamAdCompanionBanner(ciVar.getWidth(), ciVar.getHeight(), ciVar.getAssetWidth(), ciVar.getAssetHeight(), ciVar.getExpandedWidth(), ciVar.getExpandedHeight(), ciVar.getStaticResource(), ciVar.getIframeResource(), ciVar.getHtmlResource(), ciVar.getApiFramework(), ciVar.getAdSlotID(), ciVar.getRequired());
            return instreamAdCompanionBanner;
        }

        private InstreamAdCompanionBanner(int i, int i2, int i3, int i4, int i5, int i6, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable String str6) {
            this.width = i;
            this.height = i2;
            this.assetWidth = i3;
            this.assetHeight = i4;
            this.expandedWidth = i5;
            this.expandedHeight = i6;
            this.staticResource = str;
            this.iframeResource = str2;
            this.htmlResource = str3;
            this.apiFramework = str4;
            this.adSlotID = str5;
            this.required = str6;
        }
    }

    public static final class InstreamAudioAdBanner {
        @Nullable
        public final String adText;
        public final boolean allowPause;
        public final boolean allowSeek;
        public final boolean allowSkip;
        public final boolean allowTrackChange;
        @NonNull
        public final List<InstreamAdCompanionBanner> companionBanners;
        public final float duration;
        @NonNull
        public final ArrayList<ShareButtonData> shareButtonDatas;

        @NonNull
        public static InstreamAudioAdBanner newBanner(@NonNull co<AudioData> coVar) {
            ArrayList arrayList = new ArrayList();
            Iterator it = coVar.getCompanionBanners().iterator();
            while (it.hasNext()) {
                arrayList.add(InstreamAdCompanionBanner.newBanner((ci) it.next()));
            }
            InstreamAudioAdBanner instreamAudioAdBanner = new InstreamAudioAdBanner(coVar.isAllowSeek(), coVar.isAllowSkip(), coVar.isAllowTrackChange(), coVar.getDuration(), coVar.getAdText(), coVar.isAllowPause(), coVar.getShareButtonDatas(), arrayList);
            return instreamAudioAdBanner;
        }

        private InstreamAudioAdBanner(boolean z, boolean z2, boolean z3, float f, @Nullable String str, boolean z4, @NonNull ArrayList<ShareButtonData> arrayList, @NonNull List<InstreamAdCompanionBanner> list) {
            this.allowSeek = z;
            this.allowSkip = z2;
            this.allowPause = z4;
            this.allowTrackChange = z3;
            this.duration = f;
            this.adText = str;
            this.shareButtonDatas = arrayList;
            this.companionBanners = list;
        }
    }

    public interface InstreamAudioAdListener {
        void onBannerComplete(@NonNull InstreamAudioAd instreamAudioAd, @NonNull InstreamAudioAdBanner instreamAudioAdBanner);

        void onBannerStart(@NonNull InstreamAudioAd instreamAudioAd, @NonNull InstreamAudioAdBanner instreamAudioAdBanner);

        void onBannerTimeLeftChange(float f, float f2, @NonNull InstreamAudioAd instreamAudioAd);

        void onComplete(@NonNull String str, @NonNull InstreamAudioAd instreamAudioAd);

        void onError(@NonNull String str, @NonNull InstreamAudioAd instreamAudioAd);

        void onLoad(@NonNull InstreamAudioAd instreamAudioAd);

        void onNoAd(@NonNull String str, @NonNull InstreamAudioAd instreamAudioAd);
    }

    public InstreamAudioAd(int i, @NonNull Context context2) {
        super(i, "instreamaudioads");
        this.context = context2;
        setTrackingEnvironmentEnabled(false);
        ah.c("InstreamAudioAd created. Version: 5.5.6");
    }

    @Nullable
    public InstreamAudioAdListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable InstreamAudioAdListener instreamAudioAdListener) {
        this.listener = instreamAudioAdListener;
    }

    public int getLoadingTimeout() {
        return this.loadingTimeoutSeconds;
    }

    public void setLoadingTimeout(int i) {
        if (i < 5) {
            ah.a("unable to set ad loading timeout < 5, set to 5 seconds");
            this.loadingTimeoutSeconds = 5;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("ad loading timeout set to ");
            sb.append(i);
            sb.append(" seconds");
            ah.a(sb.toString());
            this.loadingTimeoutSeconds = i;
        }
        if (this.engine != null) {
            this.engine.e(this.loadingTimeoutSeconds);
        }
    }

    @Nullable
    public InstreamAudioAdPlayer getPlayer() {
        return this.player;
    }

    public void setPlayer(@Nullable InstreamAudioAdPlayer instreamAudioAdPlayer) {
        this.player = instreamAudioAdPlayer;
        if (this.engine != null) {
            this.engine.setPlayer(instreamAudioAdPlayer);
        }
    }

    public float getVolume() {
        if (this.engine != null) {
            return this.engine.getVolume();
        }
        return this.volume;
    }

    public void setVolume(float f) {
        if (Float.compare(f, 0.0f) < 0 || Float.compare(f, 1.0f) > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to set volume");
            sb.append(f);
            sb.append(", volume must be in range [0..1]");
            ah.a(sb.toString());
            return;
        }
        this.volume = f;
        if (this.engine != null) {
            this.engine.setVolume(f);
        }
    }

    @NonNull
    public float[] getMidPoints() {
        if (this.midpoints == null) {
            return new float[0];
        }
        return (float[]) this.midpoints.clone();
    }

    public void load() {
        if (!this.loadOnce.compareAndSet(false, true)) {
            StringBuilder sb = new StringBuilder();
            sb.append(this);
            sb.append(" instance just loaded once, don't call load() more than one time per instance");
            ah.b(sb.toString());
            return;
        }
        i.a(this.adConfig, this.loadingTimeoutSeconds).a((C0056b<T>) new b() {
            public void onResult(@Nullable cx cxVar, @Nullable String str) {
                InstreamAudioAd.this.handleResult(cxVar, str);
            }
        }).a(this.context);
    }

    public void configureMidpoints(float f) {
        configureMidpoints(f, null);
    }

    public void configureMidpoints(float f, @Nullable float[] fArr) {
        if (f <= 0.0f) {
            ah.a("midpoints are not configured, duration is not set or <= zero");
        } else if (this.midpoints != null) {
            ah.a("midpoints already configured");
        } else {
            this.userMidpoints = fArr;
            this.audioDuration = f;
            if (this.section != null) {
                da y = this.section.y(BreakId.MIDROLL);
                if (y != null) {
                    this.midpoints = hw.a(y, this.userMidpoints, f);
                    if (this.engine != null) {
                        this.engine.a(this.midpoints);
                    }
                }
            }
        }
    }

    public void configureMidpointsPercents(float f, @Nullable float[] fArr) {
        if (fArr == null) {
            configureMidpoints(f);
        } else {
            configureMidpoints(f, hw.a(f, fArr));
        }
    }

    public void pause() {
        if (this.engine != null) {
            this.engine.pause();
        }
    }

    public void resume() {
        if (this.engine != null) {
            this.engine.resume();
        }
    }

    public void stop() {
        if (this.engine != null) {
            this.engine.stop();
        }
    }

    public void skip() {
        if (this.engine != null) {
            this.engine.skip();
        }
    }

    public void skipBanner() {
        if (this.engine != null) {
            this.engine.skipBanner();
        }
    }

    public void destroy() {
        this.listener = null;
        if (this.engine != null) {
            this.engine.destroy();
        }
    }

    public void startPreroll() {
        start(BreakId.PREROLL);
    }

    public void startPostroll() {
        start(BreakId.POSTROLL);
    }

    public void startPauseroll() {
        start(BreakId.PAUSEROLL);
    }

    public void startMidroll(float f) {
        if (this.engine == null) {
            ah.a("Unable to start ad: not loaded yet");
        } else if (this.engine.getPlayer() == null) {
            ah.a("Unable to start ad: player has not set");
        } else {
            this.engine.startMidroll(f);
        }
    }

    public void handleCompanionClick(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner) {
        if (this.engine != null) {
            this.engine.handleCompanionClick(instreamAdCompanionBanner);
        }
    }

    public void handleCompanionClick(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner, @NonNull Context context2) {
        if (this.engine != null) {
            this.engine.handleCompanionClick(instreamAdCompanionBanner, context2);
        }
    }

    public void handleCompanionShow(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner) {
        if (this.engine != null) {
            this.engine.handleCompanionShow(instreamAdCompanionBanner);
        }
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable cx cxVar, @Nullable String str) {
        if (this.listener == null) {
            return;
        }
        if (cxVar == null || !cxVar.bL()) {
            InstreamAudioAdListener instreamAudioAdListener = this.listener;
            if (str == null) {
                str = "no ad";
            }
            instreamAudioAdListener.onNoAd(str, this);
            return;
        }
        this.section = cxVar;
        this.engine = ar.a(this, this.section, this.adConfig);
        this.engine.e(this.loadingTimeoutSeconds);
        this.engine.setVolume(this.volume);
        if (this.player != null) {
            this.engine.setPlayer(this.player);
        }
        configureMidpoints(this.audioDuration, this.userMidpoints);
        this.listener.onLoad(this);
    }

    private void start(@NonNull String str) {
        if (this.engine == null) {
            ah.a("Unable to start ad: not loaded yet");
        } else if (this.engine.getPlayer() == null) {
            ah.a("Unable to start ad: player has not set");
        } else {
            this.engine.start(str);
        }
    }
}
