package com.my.target;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import com.my.target.common.models.ImageData;

/* compiled from: InterstitialPromoPresenter2 */
public class ex implements eu, com.my.target.ev.a, com.my.target.he.a, com.my.target.hi.a {
    @NonNull
    private final cn ba;
    @NonNull
    private final b fJ;
    @NonNull
    private final hi fK;
    @NonNull
    private final c fL;
    @NonNull
    private final hg fM;
    @Nullable
    private es fN;
    @NonNull
    private a fO = a.DISABLED;
    private long fP;
    private long fQ;
    private boolean fR;
    @NonNull
    private final Runnable fS = new Runnable() {
        public void run() {
            ex.this.dE();
        }
    };

    /* compiled from: InterstitialPromoPresenter2 */
    enum a {
        DISABLED,
        RULED_BY_POST,
        RULED_BY_VIDEO
    }

    /* compiled from: InterstitialPromoPresenter2 */
    public interface b extends com.my.target.eu.a {
        void C();

        void al();
    }

    /* compiled from: InterstitialPromoPresenter2 */
    static class c implements Runnable {
        @NonNull
        private final ex fY;

        c(@NonNull ex exVar) {
            this.fY = exVar;
        }

        public void run() {
            if (this.fY.dD()) {
                this.fY.dC();
            } else {
                this.fY.dB();
            }
        }
    }

    public static ex a(@NonNull hd hdVar, @NonNull cn cnVar, boolean z, @NonNull b bVar) {
        return new ex(hdVar, cnVar, z, bVar);
    }

    private ex(@NonNull hd hdVar, @NonNull cn cnVar, boolean z, @NonNull b bVar) {
        this.ba = cnVar;
        this.fJ = bVar;
        fs em = hdVar.em();
        this.fM = hdVar.en();
        this.fM.setColor(cnVar.getPromoStyleSettings().bv());
        he a2 = hdVar.a(this);
        a2.setBanner(cnVar);
        this.fK = hdVar.a(em, a2.eo(), this.fM.eo(), (com.my.target.hi.a) this);
        this.fK.setBanner(cnVar);
        this.fL = new c(this);
        b(cnVar);
        co videoBanner = cnVar.getVideoBanner();
        if (videoBanner == null) {
            this.fK.ep();
            this.fK.setBackgroundImage(cnVar.getImage());
            return;
        }
        this.fN = hdVar.a(videoBanner, em, (com.my.target.ev.a) this, z);
        this.fM.setMaxTime(videoBanner.getDuration());
        ImageData preview = videoBanner.getPreview();
        if (preview == null) {
            preview = cnVar.getImage();
        }
        this.fK.setBackgroundImage(preview);
        this.fN.dc();
    }

    public void du() {
        this.fJ.b(this.ba, null, this.fK.eo().getContext());
    }

    public void pause() {
        if (this.fN != null) {
            this.fN.dd();
        }
        this.fK.eo().removeCallbacks(this.fL);
        dv();
    }

    private void dv() {
        this.fR = false;
        this.fK.eo().removeCallbacks(this.fS);
    }

    public void resume() {
        if (this.fO != a.DISABLED && this.fP > 0) {
            dB();
        }
        dv();
    }

    public void destroy() {
        if (this.fN != null) {
            this.fN.destroy();
        }
        dv();
    }

    public void stop() {
        if (this.fN != null) {
            this.fN.dd();
        }
        dv();
    }

    @NonNull
    public View cT() {
        return this.fK.eo();
    }

    public void cZ() {
        if (this.fN != null) {
            this.fN.de();
        }
    }

    public void dw() {
        if (this.fN != null) {
            this.fN.dg();
        }
        dv();
        this.fJ.aj();
    }

    public void A(int i) {
        if (this.fN != null) {
            this.fN.df();
        }
        dv();
    }

    public void t(boolean z) {
        ce promoStyleSettings = this.ba.getPromoStyleSettings();
        int bu = promoStyleSettings.bu();
        int argb = Color.argb((int) (promoStyleSettings.bw() * 255.0f), Color.red(bu), Color.green(bu), Color.blue(bu));
        hi hiVar = this.fK;
        if (!z) {
            argb = bu;
        }
        hiVar.setPanelColor(argb);
    }

    public void dx() {
        dv();
        ab(this.ba.getAdIconClickLink());
    }

    public void dy() {
        dv();
        by adChoices = this.ba.getAdChoices();
        if (adChoices != null) {
            ab(adChoices.aU());
        }
    }

    public void dz() {
        this.fK.F(true);
        this.fK.a(1, (String) null);
        this.fK.E(false);
        dv();
        this.fK.eo().postDelayed(this.fS, 4000);
        this.fR = true;
    }

    public void dA() {
        if (this.fR) {
            dE();
        }
    }

    public void onVolumeChanged(float f) {
        this.fK.setSoundState(f != 0.0f);
    }

    public void a(float f, float f2) {
        if (this.fO == a.RULED_BY_VIDEO) {
            this.fP = (long) (((float) this.fQ) - (1000.0f * f));
        }
        this.fM.setTimeChanged(f);
    }

    public void dn() {
        this.fK.F(true);
        this.fK.a(0, (String) null);
        this.fK.E(false);
        this.fM.setVisible(false);
    }

    public void z() {
        this.fK.F(true);
        this.fK.a(0, (String) null);
        this.fK.E(false);
    }

    public void A() {
        this.fK.F(false);
        this.fK.D(false);
        this.fK.ep();
        this.fK.E(false);
    }

    /* renamed from: do reason: not valid java name */
    public void m407do() {
        this.fK.F(false);
        this.fK.D(false);
        this.fK.ep();
        this.fK.E(false);
        this.fM.setVisible(true);
    }

    public void B() {
        this.fK.F(true);
        this.fK.ep();
        this.fK.D(false);
        this.fK.E(true);
        this.fM.setVisible(true);
    }

    public void al() {
        this.fK.F(false);
        this.fK.D(true);
        this.fK.ep();
        this.fK.E(false);
        this.fM.setVisible(false);
        this.fJ.al();
        dC();
    }

    public void C() {
        co videoBanner = this.ba.getVideoBanner();
        if (videoBanner != null && videoBanner.isAllowReplay()) {
            this.fK.a(2, !TextUtils.isEmpty(videoBanner.getReplayActionText()) ? videoBanner.getReplayActionText() : null);
            this.fK.F(true);
        }
        this.fK.D(true);
        this.fM.setVisible(false);
        this.fM.setTimeChanged(0.0f);
        this.fJ.C();
        dC();
    }

    private void b(@NonNull cn cnVar) {
        co videoBanner = cnVar.getVideoBanner();
        if (videoBanner == null || !videoBanner.isAutoPlay()) {
            if (cnVar.isAllowClose()) {
                long allowCloseDelay = (long) (cnVar.getAllowCloseDelay() * 1000.0f);
                this.fQ = allowCloseDelay;
                this.fP = allowCloseDelay;
                if (this.fP > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("banner will be allowed to close in ");
                    sb.append(this.fP);
                    sb.append(" millis");
                    ah.a(sb.toString());
                    this.fO = a.RULED_BY_POST;
                    dB();
                    return;
                }
                ah.a("banner is allowed to close");
                dC();
                return;
            }
            this.fO = a.DISABLED;
            this.fK.eq();
        } else if (videoBanner.isAllowClose()) {
            long allowCloseDelay2 = (long) (videoBanner.getAllowCloseDelay() * 1000.0f);
            this.fQ = allowCloseDelay2;
            this.fP = allowCloseDelay2;
            if (this.fP > 0) {
                this.fO = a.RULED_BY_VIDEO;
                dB();
                return;
            }
            dC();
        } else {
            this.fK.eq();
        }
    }

    /* access modifiers changed from: private */
    public void dB() {
        this.fK.eo().removeCallbacks(this.fL);
        this.fK.eo().postDelayed(this.fL, 200);
        this.fK.a((int) ((this.fP / 1000) + 1), (((float) this.fQ) - ((float) this.fP)) / ((float) this.fQ));
    }

    /* access modifiers changed from: private */
    public void dC() {
        this.fK.dC();
        this.fK.eo().removeCallbacks(this.fL);
        this.fO = a.DISABLED;
    }

    /* access modifiers changed from: private */
    public boolean dD() {
        boolean z = true;
        if (this.fO == a.DISABLED) {
            return true;
        }
        if (this.fO == a.RULED_BY_POST) {
            this.fP -= 200;
        }
        if (this.fP > 0) {
            z = false;
        }
        return z;
    }

    private void ab(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                Context context = this.fK.eo().getContext();
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                context.startActivity(intent);
            } catch (Throwable th) {
                ah.a(th.getMessage());
            }
        }
    }

    /* access modifiers changed from: private */
    public void dE() {
        if (this.fR) {
            dv();
            this.fK.F(false);
            this.fK.ep();
            this.fR = false;
        }
    }
}
