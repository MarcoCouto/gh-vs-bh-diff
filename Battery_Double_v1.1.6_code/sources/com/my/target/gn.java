package com.my.target;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import java.util.List;

/* compiled from: PromoCardImageRecyclerView */
public class gn extends RecyclerView implements go {
    @NonNull
    private final OnClickListener cardClickListener;
    /* access modifiers changed from: private */
    @Nullable
    public List<cq> cards;
    /* access modifiers changed from: private */
    @NonNull
    public final b kb;
    @NonNull
    private final gm kc;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.go.a kd;
    /* access modifiers changed from: private */
    public boolean moving;

    /* compiled from: PromoCardImageRecyclerView */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (!gn.this.moving && gn.this.isClickable()) {
                View findContainingItemView = gn.this.kb.findContainingItemView(view);
                if (!(findContainingItemView == null || gn.this.kd == null || gn.this.cards == null)) {
                    gn.this.kd.b(findContainingItemView, gn.this.kb.getPosition(findContainingItemView));
                }
            }
        }
    }

    /* compiled from: PromoCardImageRecyclerView */
    static class b extends LinearLayoutManager {
        private int dividerPadding;
        @Nullable
        private com.my.target.fv.a kf;

        public b(Context context) {
            super(context, 0, false);
        }

        public void measureChildWithMargins(View view, int i, int i2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int width = getWidth();
            if (getHeight() > 0 && width > 0) {
                if (getItemViewType(view) == 1) {
                    layoutParams.rightMargin = this.dividerPadding;
                } else if (getItemViewType(view) == 2) {
                    layoutParams.leftMargin = this.dividerPadding;
                } else {
                    layoutParams.leftMargin = this.dividerPadding;
                    layoutParams.rightMargin = this.dividerPadding;
                }
                super.measureChildWithMargins(view, i, i2);
            }
        }

        public void a(@Nullable com.my.target.fv.a aVar) {
            this.kf = aVar;
        }

        public void onLayoutCompleted(State state) {
            super.onLayoutCompleted(state);
            if (this.kf != null) {
                this.kf.dR();
            }
        }

        public void setDividerPadding(int i) {
            this.dividerPadding = i;
        }
    }

    public gn(@NonNull Context context) {
        this(context, null);
    }

    public gn(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public gn(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.cardClickListener = new a();
        this.kb = new b(context);
        this.kb.setDividerPadding(ic.a(4, context));
        this.kc = new gm(getContext());
        setHasFixedSize(true);
    }

    public void setupCards(@NonNull List<cq> list) {
        this.cards = list;
        this.kc.setCards(list);
        if (isClickable()) {
            this.kc.b(this.cardClickListener);
        }
        setCardLayoutManager(this.kb);
        swapAdapter(this.kc, true);
    }

    public void onScrollStateChanged(int i) {
        super.onScrollStateChanged(i);
        this.moving = i != 0;
        if (!this.moving) {
            checkCardChanged();
        }
    }

    @NonNull
    public int[] getVisibleCardNumbers() {
        int findFirstCompletelyVisibleItemPosition = this.kb.findFirstCompletelyVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = this.kb.findLastCompletelyVisibleItemPosition();
        if (this.cards == null || findFirstCompletelyVisibleItemPosition > findLastCompletelyVisibleItemPosition || findFirstCompletelyVisibleItemPosition < 0 || findLastCompletelyVisibleItemPosition >= this.cards.size()) {
            return new int[0];
        }
        int[] iArr = new int[((findLastCompletelyVisibleItemPosition - findFirstCompletelyVisibleItemPosition) + 1)];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = findFirstCompletelyVisibleItemPosition;
            findFirstCompletelyVisibleItemPosition++;
        }
        return iArr;
    }

    public Parcelable getState() {
        return this.kb.onSaveInstanceState();
    }

    public void restoreState(@NonNull Parcelable parcelable) {
        this.kb.onRestoreInstanceState(parcelable);
    }

    public void setPromoCardSliderListener(@Nullable com.my.target.go.a aVar) {
        this.kd = aVar;
    }

    public void dispose() {
        this.kc.dispose();
    }

    /* access modifiers changed from: private */
    public void checkCardChanged() {
        if (this.kd != null) {
            this.kd.b((View) this, getVisibleCardNumbers());
        }
    }

    private void setCardLayoutManager(b bVar) {
        bVar.a(new com.my.target.fv.a() {
            public void dR() {
                gn.this.checkCardChanged();
            }
        });
        super.setLayoutManager(bVar);
    }
}
