package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.cd;
import com.my.target.common.models.AudioData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: MediaSection */
public class da<T extends cd> extends cv {
    @NonNull
    private final ArrayList<co<T>> banners = new ArrayList<>();
    @NonNull
    private final ArrayList<bz> dM = new ArrayList<>();
    @NonNull
    private final ArrayList<bz> dN = new ArrayList<>();
    @NonNull
    private final ArrayList<bz> dO = new ArrayList<>();
    private int dP = 10;
    private int dQ = -1;
    @NonNull
    private final String name;

    @NonNull
    public static da<VideoData> z(@NonNull String str) {
        return B(str);
    }

    @NonNull
    public static da<AudioData> A(@NonNull String str) {
        return B(str);
    }

    @NonNull
    private static <T extends cd> da<T> B(@NonNull String str) {
        return new da<>(str);
    }

    private da(@NonNull String str) {
        this.name = str;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    public void u(int i) {
        this.dP = i;
    }

    public int bU() {
        return this.dP;
    }

    public int bV() {
        return this.dQ;
    }

    public void v(int i) {
        this.dQ = i;
    }

    public void g(@NonNull co<T> coVar) {
        this.banners.add(coVar);
    }

    public void a(@NonNull co<T> coVar, int i) {
        int size = this.banners.size();
        if (i >= 0 && i <= size) {
            this.banners.add(i, coVar);
            Iterator it = this.dO.iterator();
            while (it.hasNext()) {
                bz bzVar = (bz) it.next();
                int position = bzVar.getPosition();
                if (position >= i) {
                    bzVar.setPosition(position + 1);
                }
            }
        }
    }

    @NonNull
    public List<co<T>> bT() {
        return new ArrayList(this.banners);
    }

    @NonNull
    public ArrayList<bz> bW() {
        return new ArrayList<>(this.dN);
    }

    public int getBannersCount() {
        return this.banners.size();
    }

    @Nullable
    public bz bX() {
        if (this.dM.size() > 0) {
            return (bz) this.dM.remove(0);
        }
        return null;
    }

    @NonNull
    public ArrayList<bz> g(float f) {
        ArrayList<bz> arrayList = new ArrayList<>();
        Iterator it = this.dN.iterator();
        while (it.hasNext()) {
            bz bzVar = (bz) it.next();
            if (bzVar.getPoint() == f) {
                arrayList.add(bzVar);
            }
        }
        if (arrayList.size() > 0) {
            this.dN.removeAll(arrayList);
        }
        return arrayList;
    }

    public void c(@NonNull bz bzVar) {
        if (bzVar.aX()) {
            this.dN.add(bzVar);
        } else if (bzVar.aV()) {
            this.dM.add(bzVar);
        } else {
            this.dO.add(bzVar);
        }
    }

    public void bY() {
        this.dO.clear();
    }

    public void b(@NonNull da<T> daVar) {
        this.banners.addAll(daVar.banners);
        this.dM.addAll(daVar.dM);
        this.dN.addAll(daVar.dN);
        d(daVar.bb());
    }

    public boolean bZ() {
        return !this.dN.isEmpty() || !this.dM.isEmpty();
    }
}
