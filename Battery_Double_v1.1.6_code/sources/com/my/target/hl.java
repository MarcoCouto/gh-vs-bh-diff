package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import com.my.target.ho.c;
import java.util.List;

/* compiled from: SliderAdView */
public class hl extends RelativeLayout {
    private static final int mM = ic.eG();
    private static final int mN = ic.eG();
    private static final int mO = ic.eG();
    @NonNull
    private final hk mP;
    @NonNull
    private final fz mQ;
    @NonNull
    private final ho mR;
    @NonNull
    private final FrameLayout mS;
    @NonNull
    private final LayoutParams mT;
    @NonNull
    private final LayoutParams mU = new LayoutParams(-2, -2);
    @NonNull
    private final LayoutParams mV;
    private int orientation;
    @NonNull
    private final ic uiUtils;

    public hl(Context context) {
        super(context);
        this.uiUtils = ic.P(context);
        this.mP = new hk(context);
        this.mQ = new fz(context);
        this.mS = new FrameLayout(context);
        this.mR = new ho(context);
        this.mR.setId(mM);
        this.mQ.setId(mO);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        this.mR.setLayoutParams(layoutParams);
        this.mT = new LayoutParams(-2, -2);
        this.mT.addRule(14, -1);
        this.mT.addRule(12, -1);
        this.mP.setId(mN);
        this.mV = new LayoutParams(-1, -1);
        this.mV.addRule(2, mN);
        this.mS.addView(this.mR);
        addView(this.mS);
        addView(this.mP);
        addView(this.mQ);
    }

    public void setCloseClickListener(@Nullable OnClickListener onClickListener) {
        this.mQ.setOnClickListener(onClickListener);
    }

    public void setFSSliderCardListener(@Nullable c cVar) {
        this.mR.setSliderCardListener(cVar);
    }

    public void a(@NonNull cz czVar, @NonNull List<cm> list) {
        ImageData closeIcon = czVar.getCloseIcon();
        if (closeIcon == null || closeIcon.getBitmap() == null) {
            this.mQ.a(fm.F(this.uiUtils.M(36)), false);
        } else {
            this.mQ.a(closeIcon.getBitmap(), true);
        }
        setBackgroundColor(czVar.getBackgroundColor());
        int size = list.size();
        if (size > 1) {
            this.mP.e(size, czVar.bS(), czVar.bR());
        } else {
            this.mP.setVisibility(8);
        }
        this.mR.a(list, czVar.getBackgroundColor());
    }

    public void J(int i) {
        this.mP.J(i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        m(MeasureSpec.getSize(i), MeasureSpec.getSize(i2));
        super.onMeasure(i, i2);
    }

    private void m(int i, int i2) {
        int i3 = ((float) i) / ((float) i2) > 1.0f ? 2 : 1;
        if (i3 != this.orientation) {
            setLayoutOrientation(i3);
        }
    }

    private void setLayoutOrientation(int i) {
        this.orientation = i;
        if (this.orientation == 1) {
            this.mT.setMargins(0, this.uiUtils.M(12), 0, this.uiUtils.M(16));
            this.mV.topMargin = this.uiUtils.M(56);
            this.mU.setMargins(0, 0, 0, 0);
        } else {
            this.mT.setMargins(0, this.uiUtils.M(6), 0, this.uiUtils.M(8));
            this.mV.topMargin = this.uiUtils.M(28);
            this.mU.setMargins(this.uiUtils.M(-4), this.uiUtils.M(-8), 0, 0);
        }
        this.mS.setLayoutParams(this.mV);
        this.mP.setLayoutParams(this.mT);
        this.mQ.setLayoutParams(this.mU);
    }
}
