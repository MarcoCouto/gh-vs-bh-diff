package com.my.target;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: NativeAppwallAdResponseParser */
public class aa extends c<dc> {
    @NonNull
    public static c<dc> f() {
        return new aa();
    }

    private aa() {
    }

    @Nullable
    public dc a(@NonNull String str, @NonNull bz bzVar, @Nullable dc dcVar, @NonNull a aVar, @NonNull Context context) {
        int i;
        Context context2 = context;
        JSONObject a = a(str, context2);
        if (a == null) {
            return null;
        }
        JSONArray names = a.names();
        ei h = ei.h(bzVar, aVar, context2);
        boolean z = false;
        dc dcVar2 = dcVar;
        int i2 = 0;
        while (true) {
            if (i2 >= names.length()) {
                break;
            }
            String optString = names.optString(i2);
            if ("appwall".equals(optString) || "showcaseApps".equals(optString) || "showcaseGames".equals(optString) || "showcase".equals(optString)) {
                i = i2;
                dcVar2 = a(optString, a, h, bzVar, aVar, context);
                if (dcVar2 != null && !dcVar2.bT().isEmpty()) {
                    z = true;
                    break;
                }
            } else {
                i = i2;
            }
            i2 = i + 1;
        }
        if (!z) {
            return null;
        }
        dcVar2.n(bzVar.aZ());
        dcVar2.d(a);
        return dcVar2;
    }

    @Nullable
    private dc a(@NonNull String str, @NonNull JSONObject jSONObject, @NonNull ei eiVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return null;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        dc C = dc.C(str);
        eiVar.a(optJSONObject, C);
        eh a = eh.a(C, bzVar, aVar, context);
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
            if (optJSONObject2 != null) {
                cr newBanner = cr.newBanner();
                a.a(optJSONObject2, newBanner);
                String bundleId = newBanner.getBundleId();
                if (!TextUtils.isEmpty(bundleId)) {
                    newBanner.setAppInstalled(a(context, bundleId));
                }
                C.a(newBanner);
            }
        }
        return C;
    }

    private boolean a(@NonNull Context context, @NonNull String str) {
        PackageManager packageManager = context.getPackageManager();
        Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
        if (launchIntentForPackage == null) {
            return false;
        }
        return !packageManager.queryIntentActivities(launchIntentForPackage, 65536).isEmpty();
    }
}
