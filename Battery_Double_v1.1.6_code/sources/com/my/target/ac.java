package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ResearchProgressTracker */
public class ac {
    @NonNull
    private final ArrayList<cf> o;
    @NonNull
    private final ArrayList<dg> p;
    private int q = -1;

    @NonNull
    public static ac a(@NonNull di diVar) {
        return new ac(diVar);
    }

    private ac(@NonNull di diVar) {
        ArrayList<cf> arrayList = new ArrayList<>();
        Iterator it = diVar.N("playheadTimerValue").iterator();
        while (it.hasNext()) {
            dh dhVar = (dh) it.next();
            if (dhVar instanceof cf) {
                arrayList.add((cf) dhVar);
            }
        }
        this.o = arrayList;
        this.p = new ArrayList<>();
        diVar.a(this.p);
    }

    public void a(int i, int i2, @NonNull Context context) {
        if (i2 >= 0 && i >= 0 && i != this.q) {
            this.q = i;
            if (!this.o.isEmpty() && i != 0) {
                Iterator it = this.o.iterator();
                while (it.hasNext()) {
                    a(i, (cf) it.next(), context);
                }
            }
            ArrayList arrayList = new ArrayList();
            while (!this.p.isEmpty() && ((dg) this.p.get(this.p.size() - 1)).cq() <= ((float) i)) {
                arrayList.add((dg) this.p.remove(this.p.size() - 1));
            }
            if (!arrayList.isEmpty()) {
                ib.a((List<dh>) arrayList, context);
            }
        }
    }

    private void a(int i, @NonNull cf cfVar, @NonNull Context context) {
        int by = cfVar.by();
        int bz = cfVar.bz();
        if ((by <= i && (bz == 0 || bz >= i)) && (i - by) % cfVar.bA() == 0) {
            String replace = cfVar.getUrl().replace("[CONTENTPLAYHEAD]", String.valueOf(i));
            if (!TextUtils.isEmpty(replace)) {
                ib.o(replace, context);
            }
        }
    }
}
