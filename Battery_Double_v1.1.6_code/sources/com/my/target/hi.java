package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.my.target.common.models.ImageData;

/* compiled from: PromoStyle2View */
public interface hi {

    /* compiled from: PromoStyle2View */
    public interface a {
        void A(int i);

        void cZ();

        void dA();

        void du();

        void dw();

        void dx();

        void dy();

        void dz();

        void t(boolean z);
    }

    void D(boolean z);

    void E(boolean z);

    void F(boolean z);

    void a(int i, float f);

    void a(int i, @Nullable String str);

    void dC();

    @NonNull
    View eo();

    void ep();

    void eq();

    void setBackgroundImage(ImageData imageData);

    void setBanner(@NonNull cn cnVar);

    void setPanelColor(int i);

    void setSoundState(boolean z);
}
