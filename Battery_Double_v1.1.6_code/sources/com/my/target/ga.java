package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;

/* compiled from: IconButton2 */
public class ga extends View {
    private final float density;
    @NonNull
    private final Paint iu = new Paint();
    @NonNull
    private final ColorFilter iv;
    @Nullable
    private Bitmap iw;
    private int ix;
    private int iy;
    private int padding;
    @NonNull
    private final Rect rect;

    public int getPadding() {
        return this.padding;
    }

    public void setPadding(int i) {
        this.padding = i;
    }

    public void a(@Nullable Bitmap bitmap, boolean z) {
        this.iw = bitmap;
        if (this.iw == null) {
            this.iy = 0;
            this.ix = 0;
        } else if (z) {
            float f = 1.0f;
            if (this.density > 1.0f) {
                f = 2.0f;
            }
            this.iy = (int) ((((float) this.iw.getHeight()) / f) * this.density);
            this.ix = (int) ((((float) this.iw.getWidth()) / f) * this.density);
        } else {
            this.ix = this.iw.getWidth();
            this.iy = this.iw.getHeight();
        }
        requestLayout();
    }

    public ga(@NonNull Context context) {
        super(context);
        this.iu.setFilterBitmap(true);
        this.density = context.getResources().getDisplayMetrics().density;
        this.padding = ic.a(10, context);
        this.rect = new Rect();
        this.iv = new LightingColorFilter(-3355444, 1);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    this.iu.setColorFilter(this.iv);
                    invalidate();
                    return true;
                case 1:
                    if (motionEvent.getX() >= 0.0f && motionEvent.getX() <= ((float) getMeasuredWidth()) && motionEvent.getY() >= 0.0f && motionEvent.getY() <= ((float) getMeasuredHeight())) {
                        performClick();
                        break;
                    }
                default:
                    return super.onTouchEvent(motionEvent);
            }
        }
        this.iu.setColorFilter(null);
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        int i3 = size - (this.padding * 2);
        int i4 = size2 - (this.padding * 2);
        if (this.iw == null || this.ix <= 0 || this.iy <= 0) {
            setMeasuredDimension(0, 0);
        } else {
            float f = ((float) this.ix) / ((float) this.iy);
            if (mode == 1073741824 && mode2 == 1073741824) {
                setMeasuredDimension(i3, i4);
                return;
            }
            if (mode == 0 && mode2 == 0) {
                i3 = this.ix;
                i4 = this.iy;
            } else if (mode == 0) {
                i3 = (int) (((float) i4) * f);
            } else if (mode2 == 0) {
                i4 = (int) (((float) i3) / f);
            } else {
                float f2 = (float) i3;
                float f3 = f2 / ((float) this.ix);
                float f4 = (float) i4;
                if (Math.min(f3, f4 / ((float) this.iy)) != f3 || f <= 0.0f) {
                    i3 = (int) (f4 * f);
                } else {
                    i4 = (int) (f2 / f);
                }
            }
            setMeasuredDimension(i3 + (this.padding * 2), i4 + (this.padding * 2));
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.iw != null) {
            this.rect.left = this.padding;
            this.rect.top = this.padding;
            this.rect.right = getMeasuredWidth() - this.padding;
            this.rect.bottom = getMeasuredHeight() - this.padding;
            canvas.drawBitmap(this.iw, null, this.rect, this.iu);
        }
    }
}
