package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ResearchTimerStat */
public class cf extends dh {
    private int dA;
    private int dB = 1;
    private int dz;

    public static cf t(@NonNull String str) {
        return new cf(str);
    }

    public int by() {
        return this.dz;
    }

    public void o(int i) {
        this.dz = i;
    }

    public int bz() {
        return this.dA;
    }

    public void p(int i) {
        this.dA = i;
    }

    public int bA() {
        return this.dB;
    }

    public void q(int i) {
        this.dB = i;
    }

    private cf(@NonNull String str) {
        super("playheadTimerValue", str);
    }
}
