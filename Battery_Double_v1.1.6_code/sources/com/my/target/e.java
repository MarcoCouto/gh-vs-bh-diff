package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.my.target.common.CustomParams;
import com.my.target.common.MyTargetPrivacy;
import com.my.target.common.MyTargetVersion;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/* compiled from: AdServiceBuilder */
public abstract class e {

    /* compiled from: AdServiceBuilder */
    public static class a extends e {
        @NonNull
        private static String i = "https://ad.mail.ru/mobile/";

        protected a() {
        }

        @NonNull
        public bz a(@NonNull a aVar, @NonNull Context context) {
            return bz.q(c(aVar, context));
        }

        /* access modifiers changed from: protected */
        @NonNull
        public Map<String, String> b(@NonNull a aVar, @NonNull Context context) {
            HashMap hashMap = new HashMap();
            hashMap.put("formats", aVar.getFormat());
            hashMap.put("adman_ver", MyTargetVersion.VERSION);
            if (MyTargetPrivacy.isConsentSpecified()) {
                hashMap.put("user_consent", MyTargetPrivacy.isUserConsent() ? "1" : "0");
            }
            if (MyTargetPrivacy.isUserAgeRestricted()) {
                hashMap.put("user_age_restricted", "1");
            }
            if (aVar.isAutoLoadVideo()) {
                hashMap.put("preloadvideo", "1");
            }
            int bannersCount = aVar.getBannersCount();
            if (bannersCount > 0) {
                hashMap.put("count", Integer.toString(bannersCount));
            }
            String bidId = aVar.getBidId();
            if (bidId != null) {
                hashMap.put("bid_id", bidId);
            }
            if (MyTargetPrivacy.isConsentSpecified() && !MyTargetPrivacy.isUserConsent()) {
                return hashMap;
            }
            CustomParams customParams = aVar.getCustomParams();
            customParams.putDataTo(hashMap);
            fg dM = fg.dM();
            try {
                fe dN = dM.dN();
                dN.v(aVar.isTrackingEnvironmentEnabled());
                dN.w(aVar.isTrackingLocationEnabled());
                dM.collectData(context);
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("Error collecting data: ");
                sb.append(th);
                ah.a(sb.toString());
            }
            dM.putDataTo(hashMap);
            String lang = customParams.getLang();
            if (lang != null) {
                hashMap.put("lang", lang);
            }
            String mrgsId = customParams.getMrgsId();
            if (mrgsId != null) {
                hashMap.put("mrgs_device_id", mrgsId);
            }
            return hashMap;
        }

        @NonNull
        private String c(@NonNull a aVar, @NonNull Context context) {
            Map b = b(aVar, context);
            StringBuilder sb = new StringBuilder();
            sb.append(i);
            sb.append(aVar.getSlotId());
            sb.append("/");
            StringBuilder sb2 = new StringBuilder(sb.toString());
            boolean z = true;
            for (Entry entry : b.entrySet()) {
                String str = (String) entry.getValue();
                if (str != null) {
                    String str2 = (String) entry.getKey();
                    try {
                        str = URLEncoder.encode(str, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        ah.a(e.getMessage());
                    }
                    if (z) {
                        sb2.append("?");
                        sb2.append(str2);
                        sb2.append(RequestParameters.EQUAL);
                        sb2.append(str);
                        z = false;
                    } else {
                        sb2.append(RequestParameters.AMPERSAND);
                        sb2.append(str2);
                        sb2.append(RequestParameters.EQUAL);
                        sb2.append(str);
                    }
                }
            }
            return sb2.toString();
        }
    }

    @NonNull
    public abstract bz a(@NonNull a aVar, @NonNull Context context);

    @NonNull
    public static e e() {
        return new a();
    }
}
