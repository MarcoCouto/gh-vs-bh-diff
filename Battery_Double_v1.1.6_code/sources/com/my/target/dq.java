package com.my.target;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.my.target.common.MyTargetVersion;
import java.nio.charset.Charset;
import org.json.JSONObject;

/* compiled from: LogMessage */
public class dq {
    /* access modifiers changed from: private */
    @NonNull
    public static String eo = "https://ad.mail.ru/sdk/log/";
    @VisibleForTesting
    public static boolean ep = true;
    @Nullable
    private String ck;
    @Nullable
    private String eq;
    @Nullable
    private String er;
    @Nullable
    private String es;
    @NonNull
    private final String name;
    private int slotId;
    @NonNull
    private final String type;

    @NonNull
    public static dq P(@NonNull String str) {
        return new dq(str, "error");
    }

    private dq(@NonNull String str, @NonNull String str2) {
        this.name = str;
        this.type = str2;
    }

    @NonNull
    public dq Q(@Nullable String str) {
        this.eq = str;
        return this;
    }

    @NonNull
    public dq x(int i) {
        this.slotId = i;
        return this;
    }

    @NonNull
    public dq R(@Nullable String str) {
        this.er = str;
        return this;
    }

    @NonNull
    public dq S(@Nullable String str) {
        this.ck = str;
        return this;
    }

    public void q(@NonNull final Context context) {
        ai.b(new Runnable() {
            public void run() {
                String cH = dq.this.cH();
                StringBuilder sb = new StringBuilder();
                sb.append("send message to log:\n ");
                sb.append(cH);
                ah.a(sb.toString());
                if (dq.ep) {
                    dm.cC().O(Base64.encodeToString(cH.getBytes(Charset.forName("UTF-8")), 0)).f(dq.eo, context);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public String cH() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("sdk", "myTarget");
            jSONObject.put("sdkver", MyTargetVersion.VERSION);
            jSONObject.put("os", Constants.JAVASCRIPT_INTERFACE_NAME);
            jSONObject.put("osver", VERSION.RELEASE);
            jSONObject.put("type", this.type);
            jSONObject.put("name", this.name);
            if (this.eq != null) {
                jSONObject.put("message", this.eq);
            }
            if (this.slotId > 0) {
                jSONObject.put("slot", this.slotId);
            }
            if (this.er != null) {
                jSONObject.put("url", this.er);
            }
            if (this.ck != null) {
                jSONObject.put(RequestParameters.BANNER_ID, this.ck);
            }
            if (this.es != null) {
                jSONObject.put("data", this.es);
            }
        } catch (Throwable unused) {
        }
        return jSONObject.toString();
    }
}
