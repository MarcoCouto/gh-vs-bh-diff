package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

/* compiled from: InterstitialPromoView */
public interface gt {
    public static final int je = ic.eG();

    /* compiled from: InterstitialPromoView */
    public interface a {
        void dt();

        void s(boolean z);
    }

    void ej();

    @NonNull
    View getCloseButton();

    @NonNull
    View getView();

    void setBanner(@NonNull cn cnVar);

    void setClickArea(@NonNull ca caVar);

    void setInterstitialPromoViewListener(@Nullable a aVar);
}
