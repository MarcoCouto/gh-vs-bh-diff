package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import org.json.JSONObject;

/* compiled from: NativeAppwallAdBannerParser */
public class eh {
    @NonNull
    private final dw eB;
    @NonNull
    private final dc section;

    public static eh a(@NonNull dc dcVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new eh(dcVar, bzVar, aVar, context);
    }

    private eh(@NonNull dc dcVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        this.section = dcVar;
        this.eB = dw.b(bzVar, aVar, context);
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull cr crVar) {
        this.eB.a(jSONObject, (ch) crVar);
        crVar.setHasNotification(jSONObject.optBoolean("hasNotification", crVar.isHasNotification()));
        crVar.setBanner(jSONObject.optBoolean("Banner", crVar.isBanner()));
        crVar.setRequireCategoryHighlight(jSONObject.optBoolean("RequireCategoryHighlight", crVar.isRequireCategoryHighlight()));
        crVar.setItemHighlight(jSONObject.optBoolean("ItemHighlight", crVar.isItemHighlight()));
        crVar.setMain(jSONObject.optBoolean("Main", crVar.isMain()));
        crVar.setRequireWifi(jSONObject.optBoolean("RequireWifi", crVar.isRequireWifi()));
        crVar.setSubItem(jSONObject.optBoolean("subitem", crVar.isSubItem()));
        crVar.setBubbleId(jSONObject.optString("bubble_id", crVar.getBubbleId()));
        crVar.setLabelType(jSONObject.optString("labelType", crVar.getLabelType()));
        crVar.setStatus(jSONObject.optString("status", crVar.getStatus()));
        crVar.setMrgsId(jSONObject.optInt("mrgs_id"));
        crVar.setCoins(jSONObject.optInt("coins"));
        crVar.setCoinsIconBgColor(ee.a(jSONObject, "coins_icon_bgcolor", crVar.getCoinsIconBgColor()));
        crVar.setCoinsIconTextColor(ee.a(jSONObject, "coins_icon_textcolor", crVar.getCoinsIconTextColor()));
        String optString = jSONObject.optString("icon_hd");
        if (!TextUtils.isEmpty(optString)) {
            crVar.setIcon(ImageData.newImageData(optString));
        }
        String optString2 = jSONObject.optString("coins_icon_hd");
        if (!TextUtils.isEmpty(optString2)) {
            crVar.setCoinsIcon(ImageData.newImageData(optString2));
        }
        String optString3 = jSONObject.optString("cross_notif_icon_hd");
        if (!TextUtils.isEmpty(optString3)) {
            crVar.setCrossNotifIcon(ImageData.newImageData(optString3));
        }
        String cg = this.section.cg();
        if (!TextUtils.isEmpty(cg)) {
            crVar.setBubbleIcon(ImageData.newImageData(cg));
        }
        String ci = this.section.ci();
        if (!TextUtils.isEmpty(ci)) {
            crVar.setGotoAppIcon(ImageData.newImageData(ci));
        }
        String ch = this.section.ch();
        if (!TextUtils.isEmpty(ch)) {
            crVar.setLabelIcon(ImageData.newImageData(ch));
        }
        String status = crVar.getStatus();
        if (status != null) {
            String I = this.section.I(status);
            if (!TextUtils.isEmpty(I)) {
                crVar.setStatusIcon(ImageData.newImageData(I));
            }
        }
        String cj = this.section.cj();
        if (crVar.isItemHighlight() && !TextUtils.isEmpty(cj)) {
            crVar.setItemHighlightIcon(ImageData.newImageData(cj));
        }
    }
}
