package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: NativeAdBannerParser */
public class eg {
    @NonNull
    private final a adConfig;
    @Nullable
    private String ck;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;
    @NonNull
    private final dw eB;

    @NonNull
    public static eg g(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new eg(bzVar, aVar, context2);
    }

    private eg(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eA = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.eB = dw.b(bzVar, aVar, context2);
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull cp cpVar) {
        this.eB.a(jSONObject, (ch) cpVar);
        this.ck = cpVar.getId();
        JSONObject optJSONObject = jSONObject.optJSONObject("viewability");
        if (optJSONObject != null) {
            if (optJSONObject.has("percent")) {
                int optInt = optJSONObject.optInt("percent");
                if (optInt < 5 || optInt > 100) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("invalid viewability percent ");
                    sb.append(optInt);
                    f("Bad value", sb.toString());
                } else {
                    cpVar.setViewabilitySquare(((float) optInt) / 100.0f);
                }
            }
            if (optJSONObject.has("rate")) {
                double optDouble = optJSONObject.optDouble("rate");
                if (optDouble >= 0.5d) {
                    cpVar.setViewabilityRate((float) optDouble);
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("invalid viewability rate ");
                    sb2.append(optDouble);
                    f("Bad value", sb2.toString());
                }
            }
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("cards");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                if (optJSONObject2 != null) {
                    cq b = b(optJSONObject2, cpVar);
                    if (b != null) {
                        cpVar.addNativeAdCard(b);
                    }
                }
            }
        }
        if (cpVar.getNativeAdCards().isEmpty()) {
            JSONObject optJSONObject3 = jSONObject.optJSONObject("video");
            if (optJSONObject3 != null) {
                co newVideoBanner = co.newVideoBanner();
                newVideoBanner.setId(cpVar.getId());
                if (dx.c(this.eA, this.adConfig, this.context).a(optJSONObject3, newVideoBanner)) {
                    cpVar.setVideoBanner(newVideoBanner);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public cq b(@NonNull JSONObject jSONObject, @NonNull cp cpVar) {
        cq newCard = cq.newCard(cpVar);
        this.eB.a(jSONObject, (ch) newCard);
        if (TextUtils.isEmpty(newCard.getTrackingLink())) {
            f("Required field", "no tracking link in nativeAdCard");
            return null;
        } else if (newCard.getImage() == null) {
            f("Required field", "no image in nativeAdCard");
            return null;
        } else {
            newCard.setId(jSONObject.optString("cardID", newCard.getId()));
            return newCard;
        }
    }

    private void f(String str, String str2) {
        dq.P(str).Q(str2).x(this.adConfig.getSlotId()).S(this.ck).R(this.eA.getUrl()).q(this.context);
    }
}
