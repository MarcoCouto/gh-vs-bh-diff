package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.CustomParams;

/* compiled from: AdConfig */
public final class a {
    private boolean autoLoadImages = false;
    private boolean autoLoadVideo = false;
    private int bannersCount;
    @Nullable
    private String bidId;
    private long cachePeriod = 86400000;
    @NonNull
    private final CustomParams customParams = new CustomParams();
    @NonNull
    private final String format;
    private boolean mediationEnabled = true;
    private boolean refreshAd = true;
    private final int slotId;
    private boolean trackingEnvironmentEnabled = true;
    private boolean trackingLocationEnabled = true;
    private int videoQuality = 360;

    @NonNull
    public static a newConfig(int i, @NonNull String str) {
        return new a(i, str);
    }

    public long getCachePeriod() {
        return this.cachePeriod;
    }

    public void setCachePeriod(long j) {
        if (j < 0) {
            this.cachePeriod = 0;
        } else {
            this.cachePeriod = j;
        }
    }

    @NonNull
    public CustomParams getCustomParams() {
        return this.customParams;
    }

    @NonNull
    public String getFormat() {
        return this.format;
    }

    public int getSlotId() {
        return this.slotId;
    }

    public boolean isAutoLoadImages() {
        return this.autoLoadImages;
    }

    public void setAutoLoadImages(boolean z) {
        this.autoLoadImages = z;
    }

    public boolean isAutoLoadVideo() {
        return this.autoLoadVideo;
    }

    public void setAutoLoadVideo(boolean z) {
        this.autoLoadVideo = z;
    }

    public boolean isTrackingEnvironmentEnabled() {
        return this.trackingEnvironmentEnabled;
    }

    public void setTrackingEnvironmentEnabled(boolean z) {
        this.trackingEnvironmentEnabled = z;
    }

    public boolean isTrackingLocationEnabled() {
        return this.trackingLocationEnabled;
    }

    public void setTrackingLocationEnabled(boolean z) {
        this.trackingLocationEnabled = z;
    }

    public int getVideoQuality() {
        return this.videoQuality;
    }

    public void setVideoQuality(int i) {
        this.videoQuality = i;
    }

    public boolean isRefreshAd() {
        return this.refreshAd;
    }

    public void setRefreshAd(boolean z) {
        this.refreshAd = z;
    }

    public int getBannersCount() {
        return this.bannersCount;
    }

    public void setBannersCount(int i) {
        this.bannersCount = i;
    }

    @Nullable
    public String getBidId() {
        return this.bidId;
    }

    public void setBidId(@Nullable String str) {
        this.bidId = str;
    }

    public boolean isMediationEnabled() {
        return this.mediationEnabled;
    }

    public void setMediationEnabled(boolean z) {
        this.mediationEnabled = z;
    }

    private a(int i, @NonNull String str) {
        this.slotId = i;
        this.format = str;
    }
}
