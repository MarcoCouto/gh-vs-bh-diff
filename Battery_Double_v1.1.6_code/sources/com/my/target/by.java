package com.my.target;

import android.support.annotation.NonNull;
import com.my.target.common.models.ImageData;

/* compiled from: AdChoices */
public class by {
    @NonNull
    private final String cH;
    @NonNull
    private final ImageData icon;

    @NonNull
    public static by a(@NonNull ImageData imageData, @NonNull String str) {
        return new by(imageData, str);
    }

    private by(@NonNull ImageData imageData, @NonNull String str) {
        this.icon = imageData;
        this.cH = str;
    }

    @NonNull
    public ImageData getIcon() {
        return this.icon;
    }

    @NonNull
    public String aU() {
        return this.cH;
    }
}
