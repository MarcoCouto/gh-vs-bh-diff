package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: AdService */
public class bz {
    private float allowCloseDelay = -1.0f;
    @NonNull
    private final ArrayList<bz> cI = new ArrayList<>();
    @NonNull
    private final ArrayList<dh> cJ = new ArrayList<>();
    @NonNull
    private final di cK = di.cs();
    @Nullable
    private ArrayList<dh> cL;
    @Nullable
    private bz cM;
    @Nullable
    private String cN;
    private int cO;
    private int cP = -1;
    private boolean cQ;
    private boolean cR;
    private boolean cS;
    @Nullable
    private Boolean cT;
    @Nullable
    private Boolean cU;
    @Nullable
    private Boolean cV;
    @Nullable
    private Boolean cW;
    @Nullable
    private Boolean cX;
    @Nullable
    private Boolean cY;
    @Nullable
    private Boolean cZ;
    @Nullable
    private ArrayList<ci> companionBanners;
    @Nullable
    private String ctaText;
    private int id = -1;
    private float point = -1.0f;
    private float pointP = -1.0f;
    private int position = -1;
    @NonNull
    private final String url;

    @NonNull
    public static bz q(@NonNull String str) {
        return new bz(str);
    }

    private bz(@NonNull String str) {
        this.url = str;
    }

    public boolean aV() {
        return this.cR;
    }

    public void m(boolean z) {
        this.cR = z;
    }

    public int aW() {
        return this.cP;
    }

    public boolean aX() {
        return this.cS;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public void a(@Nullable bz bzVar) {
        this.cM = bzVar;
        if (bzVar != null) {
            bzVar.setPosition(this.position);
        }
    }

    @Nullable
    public bz aY() {
        return this.cM;
    }

    public boolean aZ() {
        return this.cQ;
    }

    public void n(boolean z) {
        this.cQ = z;
    }

    @NonNull
    public String getUrl() {
        return this.url;
    }

    @NonNull
    public ArrayList<bz> ba() {
        return this.cI;
    }

    public void a(dh dhVar) {
        this.cJ.add(dhVar);
    }

    @NonNull
    public ArrayList<dh> r(@NonNull String str) {
        ArrayList<dh> arrayList = new ArrayList<>();
        Iterator it = this.cJ.iterator();
        while (it.hasNext()) {
            dh dhVar = (dh) it.next();
            if (str.equals(dhVar.getType())) {
                arrayList.add(dhVar);
            }
        }
        return arrayList;
    }

    @Nullable
    public ArrayList<dh> bb() {
        if (this.cL != null) {
            return new ArrayList<>(this.cL);
        }
        return null;
    }

    @Nullable
    public ArrayList<ci> getCompanionBanners() {
        return this.companionBanners;
    }

    public void b(@Nullable ArrayList<ci> arrayList) {
        this.companionBanners = arrayList;
    }

    public void c(@Nullable ArrayList<dh> arrayList) {
        this.cL = arrayList;
    }

    public int bc() {
        return this.cO;
    }

    public void f(int i) {
        this.cO = i;
    }

    public float getPoint() {
        return this.point;
    }

    public void setPoint(float f) {
        this.point = f;
    }

    public float getPointP() {
        return this.pointP;
    }

    public void setPointP(float f) {
        this.pointP = f;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int i) {
        this.position = i;
        if (this.cM != null) {
            this.cM.setPosition(i);
        }
    }

    @Nullable
    public String bd() {
        return this.cN;
    }

    public void s(@Nullable String str) {
        this.cN = str;
    }

    public void g(int i) {
        this.cP = i;
    }

    public void o(boolean z) {
        this.cS = z;
    }

    public void b(@NonNull bz bzVar) {
        this.cI.add(bzVar);
    }

    public void d(@Nullable ArrayList<dh> arrayList) {
        if (this.cL == null) {
            this.cL = arrayList;
        } else if (arrayList != null) {
            this.cL.addAll(arrayList);
        }
    }

    @Nullable
    public String getCtaText() {
        return this.ctaText;
    }

    public void setCtaText(@Nullable String str) {
        this.ctaText = str;
    }

    public float getAllowCloseDelay() {
        return this.allowCloseDelay;
    }

    public void setAllowCloseDelay(float f) {
        this.allowCloseDelay = f;
    }

    @Nullable
    public Boolean be() {
        return this.cT;
    }

    public void a(@Nullable Boolean bool) {
        this.cT = bool;
    }

    @Nullable
    public Boolean bf() {
        return this.cU;
    }

    public void b(@Nullable Boolean bool) {
        this.cU = bool;
    }

    @Nullable
    public Boolean bg() {
        return this.cV;
    }

    public void c(@Nullable Boolean bool) {
        this.cV = bool;
    }

    @Nullable
    public Boolean bh() {
        return this.cW;
    }

    public void d(@Nullable Boolean bool) {
        this.cW = bool;
    }

    @Nullable
    public Boolean bi() {
        return this.cX;
    }

    public void e(@Nullable Boolean bool) {
        this.cX = bool;
    }

    @NonNull
    public di bj() {
        return this.cK;
    }

    @Nullable
    public Boolean bk() {
        return this.cY;
    }

    public void f(@Nullable Boolean bool) {
        this.cY = bool;
    }

    @Nullable
    public Boolean bl() {
        return this.cZ;
    }

    public void g(@Nullable Boolean bool) {
        this.cZ = bool;
    }
}
