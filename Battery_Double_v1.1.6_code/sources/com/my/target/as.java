package com.my.target;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.FrameLayout;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.common.MyTargetActivity;
import com.my.target.common.MyTargetActivity.ActivityEngine;
import com.my.target.fo.a;
import java.lang.ref.WeakReference;

/* compiled from: InterstitialAdEngine */
public abstract class as implements an, ActivityEngine, a {
    @Nullable
    private WeakReference<fo> X;
    @Nullable
    private WeakReference<MyTargetActivity> aT;
    @NonNull
    final InterstitialAd ad;
    private boolean ah;

    public void a(boolean z) {
    }

    @Nullable
    public String aa() {
        return "myTarget";
    }

    /* access modifiers changed from: protected */
    public abstract boolean ai();

    public boolean onActivityOptionsItemSelected(MenuItem menuItem) {
        return false;
    }

    public void onActivityPause() {
    }

    public void onActivityResume() {
    }

    public void onActivityStart() {
    }

    public void onActivityStop() {
    }

    @Nullable
    public static as a(@NonNull InterstitialAd interstitialAd, @NonNull cj cjVar, @NonNull cy cyVar) {
        if (cjVar instanceof cn) {
            return av.a(interstitialAd, (cn) cjVar, cyVar);
        }
        if (cjVar instanceof cl) {
            return at.a(interstitialAd, (cl) cjVar, cyVar);
        }
        if (cjVar instanceof cm) {
            return au.a(interstitialAd, (cm) cjVar);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public boolean ah() {
        return (this.X == null || this.X.get() == null) ? false : true;
    }

    as(@NonNull InterstitialAd interstitialAd) {
        this.ad = interstitialAd;
    }

    public void k(@NonNull Context context) {
        if (this.ah) {
            ah.a("Unable to open Interstitial Ad twice, please dismiss currently showing ad first");
            return;
        }
        this.ah = true;
        MyTargetActivity.activityEngine = this;
        Intent intent = new Intent(context, MyTargetActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(268435456);
        }
        context.startActivity(intent);
    }

    public void l(@NonNull Context context) {
        if (this.ah) {
            ah.a("Unable to open Interstitial Ad twice, please dismiss currently showing ad first");
            return;
        }
        this.ah = true;
        fo foVar = this.X == null ? null : (fo) this.X.get();
        if (foVar == null || !foVar.isShowing()) {
            fo.a(this, context).show();
        } else {
            ah.c("InterstitialAdEngine.showDialog: dialog already showing");
        }
    }

    public void dismiss() {
        this.ah = false;
        fo foVar = null;
        MyTargetActivity myTargetActivity = this.aT == null ? null : (MyTargetActivity) this.aT.get();
        if (myTargetActivity != null) {
            myTargetActivity.finish();
            return;
        }
        if (this.X != null) {
            foVar = (fo) this.X.get();
        }
        if (foVar != null && foVar.isShowing()) {
            foVar.dismiss();
        }
    }

    public void destroy() {
        dismiss();
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        this.aT = new WeakReference<>(myTargetActivity);
        myTargetActivity.setTheme(16973830);
        myTargetActivity.getWindow().setFlags(1024, 1024);
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onDisplay(this.ad);
        }
    }

    public void onActivityDestroy() {
        this.ah = false;
        this.aT = null;
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onDismiss(this.ad);
        }
    }

    public final boolean onActivityBackPressed() {
        return ai();
    }

    public void a(@NonNull fo foVar, @NonNull FrameLayout frameLayout) {
        this.X = new WeakReference<>(foVar);
        if (this.ad.isHideStatusBarInDialog()) {
            foVar.dO();
        }
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onDisplay(this.ad);
        }
    }

    public void D() {
        this.ah = false;
        this.X = null;
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onDismiss(this.ad);
        }
    }
}
