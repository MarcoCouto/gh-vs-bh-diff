package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0056b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InstreamAdFactory */
public final class f extends b<cw> {
    @Nullable
    private final List<bz> j;
    /* access modifiers changed from: private */
    @NonNull
    public final ia k;
    /* access modifiers changed from: private */
    @Nullable
    public Runnable l;

    /* compiled from: InstreamAdFactory */
    static class a implements com.my.target.b.a<cw> {
        public boolean a() {
            return true;
        }

        private a() {
        }

        @NonNull
        public c<cw> b() {
            return g.f();
        }

        @Nullable
        public d<cw> c() {
            return h.g();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    public interface b extends C0056b {
    }

    @NonNull
    public static b<cw> a(@NonNull a aVar, int i) {
        return new f(aVar, i);
    }

    @NonNull
    public static b<cw> a(@NonNull bz bzVar, @NonNull a aVar, int i) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(bzVar);
        return new f(arrayList, aVar, i);
    }

    @NonNull
    public static b<cw> a(@NonNull List<bz> list, @NonNull a aVar, int i) {
        return new f(list, aVar, i);
    }

    private f(@NonNull a aVar, int i) {
        this(null, aVar, i);
    }

    private f(@Nullable List<bz> list, @NonNull a aVar, int i) {
        super(new a(), aVar);
        this.j = list;
        this.k = ia.K(i * 1000);
    }

    @NonNull
    public b<cw> a(@NonNull Context context) {
        if (this.l == null) {
            this.l = new Runnable() {
                public void run() {
                    f.this.k.e(f.this.l);
                    f.this.a(null, "ad loading timeout");
                }
            };
        }
        this.k.d(this.l);
        return super.a(context);
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: c */
    public cw b(@NonNull Context context) {
        if (this.j == null) {
            return (cw) super.b(context);
        }
        return (cw) a((cw) a(this.j, null, this.b.b(), dk.cz(), context), context);
    }
}
