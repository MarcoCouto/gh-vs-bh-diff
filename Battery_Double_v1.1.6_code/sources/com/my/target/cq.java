package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: NativeAdCard */
public class cq extends ch {
    @NonNull
    public static cq newCard(@NonNull cp cpVar) {
        cq cqVar = new cq();
        cqVar.ctaText = cpVar.ctaText;
        cqVar.navigationType = cpVar.navigationType;
        cqVar.urlscheme = cpVar.urlscheme;
        cqVar.bundleId = cpVar.bundleId;
        cqVar.directLink = cpVar.directLink;
        cqVar.openInBrowser = cpVar.openInBrowser;
        cqVar.usePlayStoreAction = cpVar.usePlayStoreAction;
        cqVar.deeplink = cpVar.deeplink;
        cqVar.clickArea = cpVar.clickArea;
        cqVar.rating = cpVar.rating;
        cqVar.votes = cpVar.votes;
        cqVar.domain = cpVar.domain;
        cqVar.category = cpVar.category;
        cqVar.subCategory = cpVar.subCategory;
        return cqVar;
    }

    private cq() {
    }
}
