package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ViewabilityTracker */
public class ie {
    @NonNull
    private final ArrayList<df> eg;
    @NonNull
    private final ArrayList<de> eh;
    private float s = -1.0f;
    @Nullable
    private WeakReference<View> viewWeakReference;

    public static ie c(@NonNull di diVar) {
        return new ie(diVar.ct(), diVar.cu());
    }

    public void setView(@Nullable View view) {
        if (view != null || this.viewWeakReference == null) {
            this.viewWeakReference = new WeakReference<>(view);
        } else {
            this.viewWeakReference.clear();
        }
    }

    protected ie(@NonNull ArrayList<df> arrayList, @NonNull ArrayList<de> arrayList2) {
        this.eh = arrayList2;
        this.eg = arrayList;
    }

    public void m(float f) {
        if (Math.abs(f - this.s) >= 1.0f) {
            Context context = null;
            double d = Utils.DOUBLE_EPSILON;
            if (this.viewWeakReference != null) {
                View view = (View) this.viewWeakReference.get();
                if (view != null) {
                    d = Cif.j(view);
                    context = view.getContext();
                }
            }
            a(d, f, context);
            this.s = f;
        }
    }

    /* access modifiers changed from: protected */
    public void a(double d, float f, @Nullable Context context) {
        if (this.eg.isEmpty() && this.eh.isEmpty()) {
            return;
        }
        if (context == null) {
            Iterator it = this.eh.iterator();
            while (it.hasNext()) {
                ((de) it.next()).h(-1.0f);
            }
            return;
        }
        ArrayList arrayList = new ArrayList();
        while (!this.eg.isEmpty() && ((df) this.eg.get(this.eg.size() - 1)).cq() <= f) {
            df dfVar = (df) this.eg.remove(this.eg.size() - 1);
            int cy = dfVar.cy();
            boolean cp = dfVar.cp();
            double d2 = (double) cy;
            if ((d2 <= d && cp) || (d2 > d && !cp)) {
                arrayList.add(dfVar);
            }
        }
        Iterator it2 = this.eh.iterator();
        while (it2.hasNext()) {
            de deVar = (de) it2.next();
            if (((double) deVar.cy()) > d) {
                deVar.h(-1.0f);
            } else if (deVar.co() < 0.0f || f <= deVar.co()) {
                deVar.h(f);
            } else if (f - deVar.co() >= deVar.getDuration()) {
                arrayList.add(deVar);
                it2.remove();
            }
        }
        if (!arrayList.isEmpty()) {
            ib.a((List<dh>) arrayList, context);
        }
    }

    public void destroy() {
        if (this.viewWeakReference != null) {
            this.viewWeakReference.clear();
        }
        this.eh.clear();
        this.eg.clear();
        this.viewWeakReference = null;
    }
}
