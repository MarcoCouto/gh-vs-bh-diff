package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: AbstractJsEvent */
public abstract class bl implements bq {
    @NonNull
    private final String type;

    public bl(@NonNull String str) {
        this.type = str;
    }

    @NonNull
    public String getType() {
        return this.type;
    }
}
