package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Xml;
import com.explorestack.iab.vast.tags.VastTagName;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.my.target.cd;
import com.my.target.common.models.AudioData;
import com.my.target.common.models.VideoData;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: VastParser */
public class en<T extends cd> {
    private static final String[] eE = {MimeTypes.VIDEO_MP4, "application/vnd.apple.mpegurl", "application/x-mpegurl"};
    private static final String[] eF = {"linkTxt"};
    @NonNull
    private final a adConfig;
    @NonNull
    private final ArrayList<dh> cL = new ArrayList<>();
    @NonNull
    private final Context context;
    @Nullable
    private String ctaText;
    @NonNull
    private final ArrayList<dg> eG = new ArrayList<>();
    @NonNull
    private final ArrayList<ci> eH = new ArrayList<>();
    @NonNull
    private final ArrayList<dh> eI = new ArrayList<>();
    @NonNull
    private final ArrayList<co<T>> eJ = new ArrayList<>();
    private boolean eK;
    @Nullable
    private bz eL;
    @NonNull
    private final bz ey;

    @NonNull
    public static <T extends cd> en<T> a(@NonNull a aVar, @NonNull bz bzVar, @NonNull Context context2) {
        return new en<>(aVar, bzVar, context2);
    }

    private static void a(@NonNull XmlPullParser xmlPullParser) {
        if (e(xmlPullParser) == 2) {
            int i = 1;
            while (i != 0) {
                switch (b(xmlPullParser)) {
                    case 2:
                        i++;
                        break;
                    case 3:
                        i--;
                        break;
                }
            }
        }
    }

    private static int b(@NonNull XmlPullParser xmlPullParser) {
        try {
            return xmlPullParser.next();
        } catch (XmlPullParserException e) {
            ah.a(e.getMessage());
            return Integer.MIN_VALUE;
        } catch (IOException e2) {
            ah.a(e2.getMessage());
            return Integer.MIN_VALUE;
        }
    }

    @NonNull
    private static String c(@NonNull XmlPullParser xmlPullParser) {
        String str = "";
        if (b(xmlPullParser) == 4) {
            str = xmlPullParser.getText();
            d(xmlPullParser);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("No text: ");
            sb.append(xmlPullParser.getName());
            ah.a(sb.toString());
        }
        return str.trim();
    }

    @Nullable
    private static String a(@NonNull String str, @NonNull XmlPullParser xmlPullParser) {
        return xmlPullParser.getAttributeValue(null, str);
    }

    @NonNull
    private static String U(@NonNull String str) {
        return str.replaceAll("&amp;", RequestParameters.AMPERSAND).replaceAll("&lt;", "<").replaceAll("&gt;", ">").trim();
    }

    private static int d(@NonNull XmlPullParser xmlPullParser) {
        try {
            return xmlPullParser.nextTag();
        } catch (XmlPullParserException e) {
            ah.a(e.getMessage());
            return Integer.MIN_VALUE;
        } catch (IOException e2) {
            ah.a(e2.getMessage());
            return Integer.MIN_VALUE;
        }
    }

    private static int e(@NonNull XmlPullParser xmlPullParser) {
        try {
            return xmlPullParser.getEventType();
        } catch (XmlPullParserException e) {
            ah.a(e.getMessage());
            return Integer.MIN_VALUE;
        }
    }

    private en(@NonNull a aVar, @NonNull bz bzVar, @NonNull Context context2) {
        this.adConfig = aVar;
        this.ey = bzVar;
        this.context = context2;
    }

    @NonNull
    public ArrayList<co<T>> cO() {
        return this.eJ;
    }

    @NonNull
    public ArrayList<dh> bb() {
        return this.cL;
    }

    public void V(@NonNull String str) {
        XmlPullParser newPullParser = Xml.newPullParser();
        try {
            newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
            newPullParser.setInput(new StringReader(str));
            cQ();
            int e = e(newPullParser);
            while (e != 1 && e != Integer.MIN_VALUE) {
                if (e == 2 && "VAST".equalsIgnoreCase(newPullParser.getName())) {
                    f(newPullParser);
                }
                e = b(newPullParser);
            }
        } catch (XmlPullParserException e2) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to parse VAST: ");
            sb.append(e2.getMessage());
            ah.a(sb.toString());
        }
    }

    @Nullable
    public bz cP() {
        return this.eL;
    }

    private void b(@Nullable String str, @NonNull String str2, @Nullable String str3) {
        dq.P(str2).Q(str3).x(this.adConfig.getSlotId()).S(str).R(this.ey.getUrl()).q(this.context);
    }

    private void cQ() {
        ArrayList bb = this.ey.bb();
        if (bb != null) {
            this.cL.addAll(bb);
        }
        ArrayList companionBanners = this.ey.getCompanionBanners();
        if (companionBanners != null) {
            this.eH.addAll(companionBanners);
        }
    }

    private void cR() {
        Iterator it = this.eJ.iterator();
        while (it.hasNext()) {
            co coVar = (co) it.next();
            di statHolder = coVar.getStatHolder();
            statHolder.a(this.ey.bj(), coVar.getDuration());
            if (!TextUtils.isEmpty(this.ctaText)) {
                coVar.setCtaText(this.ctaText);
            } else if (!TextUtils.isEmpty(this.ey.getCtaText())) {
                coVar.setCtaText(this.ey.getCtaText());
            }
            Iterator it2 = this.eG.iterator();
            while (it2.hasNext()) {
                dg dgVar = (dg) it2.next();
                a(dgVar.cr(), dgVar.getUrl(), (ch) coVar);
            }
            Iterator it3 = this.eI.iterator();
            while (it3.hasNext()) {
                statHolder.b((dh) it3.next());
            }
            Iterator it4 = this.eH.iterator();
            while (it4.hasNext()) {
                coVar.addCompanion((ci) it4.next());
            }
        }
    }

    private void f(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2 && "Ad".equals(xmlPullParser.getName())) {
                g(xmlPullParser);
            }
        }
    }

    private void g(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                if ("Wrapper".equals(name)) {
                    this.eK = true;
                    ah.a("VAST file contains wrapped ad information.");
                    int bc = this.ey.bc();
                    if (bc < 5) {
                        a(xmlPullParser, bc);
                    } else {
                        ah.a("got VAST wrapper, but max redirects limit exceeded");
                        a(xmlPullParser);
                    }
                } else if ("InLine".equals(name)) {
                    this.eK = false;
                    ah.a("VAST file contains inline ad information.");
                    h(xmlPullParser);
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void a(@NonNull XmlPullParser xmlPullParser, int i) {
        String str = null;
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                if ("Impression".equals(name)) {
                    j(xmlPullParser);
                } else if ("Creatives".equals(name)) {
                    k(xmlPullParser);
                } else if ("Extensions".equals(name)) {
                    i(xmlPullParser);
                } else if (VastTagName.VAST_AD_TAG_URI.equals(name)) {
                    str = c(xmlPullParser);
                } else {
                    a(xmlPullParser);
                }
            }
        }
        if (str != null) {
            this.eL = bz.q(str);
            this.eL.f(i + 1);
            this.eL.c(this.cL);
            this.eL.setCtaText(this.ctaText != null ? this.ctaText : this.ey.getCtaText());
            this.eL.b(this.eH);
            this.eL.a(this.ey.be());
            this.eL.b(this.ey.bf());
            this.eL.c(this.ey.bg());
            this.eL.d(this.ey.bh());
            this.eL.e(this.ey.bi());
            this.eL.f(this.ey.bk());
            this.eL.g(this.ey.bl());
            this.eL.setAllowCloseDelay(this.ey.getAllowCloseDelay());
            di bj = this.eL.bj();
            bj.e(this.eI);
            bj.f(this.eG);
            bj.a(this.ey.bj(), -1.0f);
            this.ey.b(this.eL);
            return;
        }
        ah.a("got VAST wrapper, but no vastAdTagUri");
    }

    private void h(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                if ("Impression".equals(name)) {
                    j(xmlPullParser);
                } else if (name != null && name.equals("Creatives")) {
                    k(xmlPullParser);
                } else if (name == null || !name.equals("Extensions")) {
                    a(xmlPullParser);
                } else {
                    i(xmlPullParser);
                }
            }
        }
        cR();
    }

    private void i(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if ("Extension".equals(xmlPullParser.getName())) {
                    String a = a("type", xmlPullParser);
                    for (String equals : eF) {
                        if (equals.equals(a)) {
                            a(xmlPullParser, a);
                        } else {
                            a(xmlPullParser);
                        }
                    }
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void a(@NonNull XmlPullParser xmlPullParser, String str) {
        if ("linkTxt".equals(str)) {
            String c = c(xmlPullParser);
            this.ctaText = id.decode(c);
            StringBuilder sb = new StringBuilder();
            sb.append("VAST linkTxt raw text: ");
            sb.append(c);
            ah.a(sb.toString());
        }
    }

    private void j(@NonNull XmlPullParser xmlPullParser) {
        String c = c(xmlPullParser);
        if (!TextUtils.isEmpty(c)) {
            this.cL.add(dh.c(Events.AD_IMPRESSION, c));
            StringBuilder sb = new StringBuilder();
            sb.append("Impression tracker url for wrapper: ");
            sb.append(c);
            ah.a(sb.toString());
        }
    }

    private void k(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if ("Creative".equals(xmlPullParser.getName())) {
                    b(xmlPullParser, a("id", xmlPullParser));
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void b(@NonNull XmlPullParser xmlPullParser, @Nullable String str) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                co coVar = null;
                if ("Linear".equals(name)) {
                    if (!this.eK) {
                        coVar = co.newBanner();
                        coVar.setId(str != null ? str : "");
                    }
                    a(xmlPullParser, coVar, a("skipoffset", xmlPullParser));
                    if (coVar != null) {
                        if (coVar.getDuration() <= 0.0f) {
                            b(coVar.getId(), "Required field", "VAST has no valid Duration");
                        } else if (coVar.getMediaData() != null) {
                            this.eJ.add(coVar);
                        } else {
                            b(coVar.getId(), "Required field", "VAST has no valid mediaData");
                        }
                    }
                } else if (name == null || !name.equals("CompanionAds")) {
                    a(xmlPullParser);
                } else {
                    String a = a("required", xmlPullParser);
                    if (a != null && !"all".equals(a) && !"any".equals(a) && !"none".equals(a)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Wrong companion required attribute:");
                        sb.append(a);
                        b(str, "Bad value", sb.toString());
                        a = null;
                    }
                    a(xmlPullParser, str, a);
                }
            }
        }
    }

    private void a(@NonNull XmlPullParser xmlPullParser, @Nullable String str, @Nullable String str2) {
        while (d(xmlPullParser) == 2) {
            b(xmlPullParser, str, str2);
        }
    }

    private void b(@NonNull XmlPullParser xmlPullParser, @Nullable String str, @Nullable String str2) {
        if (e(xmlPullParser) == 2) {
            String name = xmlPullParser.getName();
            if (name == null || !name.equals("Companion")) {
                a(xmlPullParser);
            } else {
                String a = a("width", xmlPullParser);
                String a2 = a("height", xmlPullParser);
                String a3 = a("id", xmlPullParser);
                ci newBanner = ci.newBanner();
                if (a3 == null) {
                    a3 = "";
                }
                newBanner.setId(a3);
                try {
                    newBanner.setWidth(Integer.parseInt(a));
                    newBanner.setHeight(Integer.parseInt(a2));
                } catch (NumberFormatException unused) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable  to convert required companion attributes, width = ");
                    sb.append(a);
                    sb.append(" height = ");
                    sb.append(a2);
                    b(str, "Bad value", sb.toString());
                }
                newBanner.setRequired(str2);
                String a4 = a("assetWidth", xmlPullParser);
                String a5 = a("assetHeight", xmlPullParser);
                try {
                    if (!TextUtils.isEmpty(a4)) {
                        newBanner.setAssetWidth(Integer.parseInt(a4));
                    }
                    if (!TextUtils.isEmpty(a5)) {
                        newBanner.setAssetHeight(Integer.parseInt(a5));
                    }
                } catch (NumberFormatException e) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("wrong VAST asset dimensions: ");
                    sb2.append(e.getMessage());
                    ah.a(sb2.toString());
                }
                String a6 = a("expandedWidth", xmlPullParser);
                String a7 = a("expandedHeight", xmlPullParser);
                try {
                    if (!TextUtils.isEmpty(a6)) {
                        newBanner.setExpandedWidth(Integer.parseInt(a6));
                    }
                    if (!TextUtils.isEmpty(a7)) {
                        newBanner.setExpandedHeight(Integer.parseInt(a7));
                    }
                } catch (NumberFormatException e2) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("wrong VAST expanded dimensions ");
                    sb3.append(e2.getMessage());
                    ah.a(sb3.toString());
                }
                newBanner.setAdSlotID(a("adSlotID", xmlPullParser));
                newBanner.setApiFramework(a("apiFramework", xmlPullParser));
                this.eH.add(newBanner);
                while (d(xmlPullParser) == 2) {
                    String name2 = xmlPullParser.getName();
                    if ("StaticResource".equals(name2)) {
                        newBanner.setStaticResource(id.decode(c(xmlPullParser)));
                    } else if ("HTMLResource".equals(name2)) {
                        newBanner.setHtmlResource(id.decode(c(xmlPullParser)));
                    } else if ("IFrameResource".equals(name2)) {
                        newBanner.setIframeResource(id.decode(c(xmlPullParser)));
                    } else if ("CompanionClickThrough".equals(name2)) {
                        String c = c(xmlPullParser);
                        if (!TextUtils.isEmpty(c)) {
                            newBanner.setTrackingLink(U(c));
                        }
                    } else if ("CompanionClickTracking".equals(name2)) {
                        String c2 = c(xmlPullParser);
                        if (!TextUtils.isEmpty(c2)) {
                            newBanner.getStatHolder().b(dh.c(String.CLICK, c2));
                        }
                    } else if ("TrackingEvents".equals(name2)) {
                        a(xmlPullParser, (ch) newBanner);
                    } else {
                        a(xmlPullParser);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public float W(@NonNull String str) {
        long j = 0;
        float f = -1.0f;
        try {
            if (str.contains(".")) {
                int indexOf = str.indexOf(".");
                long parseLong = Long.parseLong(str.substring(indexOf + 1));
                if (parseLong > 1000) {
                    return -1.0f;
                }
                str = str.substring(0, indexOf);
                j = parseLong;
            }
            String[] split = str.split(":", 3);
            long parseInt = (long) Integer.parseInt(split[0]);
            long parseInt2 = (long) Integer.parseInt(split[1]);
            long parseInt3 = (long) Integer.parseInt(split[2]);
            if (parseInt >= 24 || parseInt2 >= 60 || parseInt3 >= 60) {
                return -1.0f;
            }
            f = ((float) (((j + (parseInt3 * 1000)) + (parseInt2 * ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS)) + (parseInt * 3600000))) / 1000.0f;
            return f;
        } catch (Exception unused) {
        }
    }

    private boolean a(@NonNull XmlPullParser xmlPullParser, @NonNull co coVar) {
        float f;
        try {
            f = W(c(xmlPullParser));
        } catch (Exception unused) {
            f = 0.0f;
        }
        if (f <= 0.0f) {
            return false;
        }
        coVar.setDuration(f);
        return true;
    }

    private void b(@NonNull XmlPullParser xmlPullParser, @Nullable co coVar) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                if ("ClickThrough".equals(name)) {
                    if (coVar != null) {
                        String c = c(xmlPullParser);
                        if (!TextUtils.isEmpty(c)) {
                            coVar.setTrackingLink(U(c));
                        }
                    }
                } else if ("ClickTracking".equals(name)) {
                    String c2 = c(xmlPullParser);
                    if (!TextUtils.isEmpty(c2)) {
                        this.eI.add(dh.c(String.CLICK, c2));
                    }
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void c(@NonNull XmlPullParser xmlPullParser, @NonNull co coVar) {
        if ("instreamads".equals(this.adConfig.getFormat()) || Events.CREATIVE_FULLSCREEN.equals(this.adConfig.getFormat())) {
            e(xmlPullParser, coVar);
        } else if ("instreamaudioads".equals(this.adConfig.getFormat())) {
            d(xmlPullParser, coVar);
        }
    }

    private void d(@NonNull XmlPullParser xmlPullParser, @NonNull co<AudioData> coVar) {
        int i;
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if ("MediaFile".equals(xmlPullParser.getName())) {
                    String a = a("type", xmlPullParser);
                    String a2 = a("bitrate", xmlPullParser);
                    String U = U(c(xmlPullParser));
                    AudioData audioData = null;
                    if (!TextUtils.isEmpty(a) && !TextUtils.isEmpty(U) && a.toLowerCase(Locale.ROOT).trim().startsWith(MimeTypes.BASE_TYPE_AUDIO)) {
                        if (a2 != null) {
                            try {
                                i = Integer.parseInt(a2);
                            } catch (NumberFormatException unused) {
                            }
                            audioData = AudioData.newAudioData(U);
                            audioData.setBitrate(i);
                        }
                        i = 0;
                        audioData = AudioData.newAudioData(U);
                        audioData.setBitrate(i);
                    }
                    if (audioData == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Skipping unsupported VAST file (mimetype=");
                        sb.append(a);
                        sb.append(",url=");
                        sb.append(U);
                        ah.a(sb.toString());
                    } else {
                        coVar.setMediaData(audioData);
                    }
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    private void e(@NonNull XmlPullParser xmlPullParser, @NonNull co<VideoData> coVar) {
        int i;
        int i2;
        ArrayList arrayList = new ArrayList();
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if ("MediaFile".equals(xmlPullParser.getName())) {
                    String a = a("type", xmlPullParser);
                    String a2 = a("bitrate", xmlPullParser);
                    String a3 = a("width", xmlPullParser);
                    String a4 = a("height", xmlPullParser);
                    String U = U(c(xmlPullParser));
                    VideoData videoData = null;
                    if (!TextUtils.isEmpty(a) && !TextUtils.isEmpty(U)) {
                        String[] strArr = eE;
                        int length = strArr.length;
                        int i3 = 0;
                        int i4 = 0;
                        while (true) {
                            if (i4 >= length) {
                                break;
                            } else if (strArr[i4].equals(a)) {
                                if (a3 != null) {
                                    try {
                                        i2 = Integer.parseInt(a3);
                                    } catch (NumberFormatException unused) {
                                        i2 = 0;
                                        i = 0;
                                        videoData = VideoData.newVideoData(U, i2, i);
                                        videoData.setBitrate(i3);
                                        if (videoData == null) {
                                        }
                                    }
                                } else {
                                    i2 = 0;
                                }
                                if (a4 != null) {
                                    try {
                                        i = Integer.parseInt(a4);
                                    } catch (NumberFormatException unused2) {
                                        i = 0;
                                        videoData = VideoData.newVideoData(U, i2, i);
                                        videoData.setBitrate(i3);
                                        if (videoData == null) {
                                        }
                                    }
                                } else {
                                    i = 0;
                                }
                                if (a2 != null) {
                                    try {
                                        i3 = Integer.parseInt(a2);
                                    } catch (NumberFormatException unused3) {
                                    }
                                }
                                if (i2 > 0 && i > 0) {
                                    videoData = VideoData.newVideoData(U, i2, i);
                                    videoData.setBitrate(i3);
                                }
                            } else {
                                i4++;
                            }
                        }
                    }
                    if (videoData == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Skipping unsupported VAST file (mimeType=");
                        sb.append(a);
                        sb.append(",width=");
                        sb.append(a3);
                        sb.append(",height=");
                        sb.append(a4);
                        sb.append(",url=");
                        sb.append(U);
                        ah.a(sb.toString());
                    } else {
                        arrayList.add(videoData);
                    }
                } else {
                    a(xmlPullParser);
                }
            }
        }
        coVar.setMediaData(VideoData.chooseBest(arrayList, this.adConfig.getVideoQuality()));
    }

    private void a(@NonNull XmlPullParser xmlPullParser, @Nullable ch chVar) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if ("Tracking".equals(xmlPullParser.getName())) {
                    String a = a("event", xmlPullParser);
                    String a2 = a("offset", xmlPullParser);
                    if (a != null) {
                        if (!NotificationCompat.CATEGORY_PROGRESS.equals(a) || TextUtils.isEmpty(a2)) {
                            c(a, c(xmlPullParser), chVar);
                        } else if (a2.endsWith("%")) {
                            try {
                                a((float) Integer.parseInt(a2.replace("%", "")), c(xmlPullParser), chVar);
                            } catch (Exception unused) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Unable to parse progress stat with value ");
                                sb.append(a2);
                                ah.a(sb.toString());
                            }
                        } else {
                            b(a2, c(xmlPullParser), chVar);
                        }
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Added VAST tracking \"");
                    sb2.append(a);
                    sb2.append("\"");
                    ah.a(sb2.toString());
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void a(@NonNull String str, @NonNull String str2, @Nullable ch chVar) {
        if (chVar != null) {
            chVar.getStatHolder().b(dh.c(str, str2));
            return;
        }
        this.eI.add(dh.c(str, str2));
    }

    private void b(@NonNull String str, @NonNull String str2, @Nullable ch chVar) {
        float f;
        try {
            f = W(str);
        } catch (Exception unused) {
            f = -1.0f;
        }
        if (f >= 0.0f) {
            dg M = dg.M(str2);
            M.i(f);
            if (chVar != null) {
                chVar.getStatHolder().b(M);
            } else {
                this.eI.add(M);
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to parse progress stat with value ");
            sb.append(str);
            ah.a(sb.toString());
        }
    }

    private void a(float f, @NonNull String str, @Nullable ch chVar) {
        dg M = dg.M(str);
        if (chVar == null || chVar.getDuration() <= 0.0f) {
            M.j(f);
            this.eG.add(M);
            return;
        }
        M.i(chVar.getDuration() * (f / 100.0f));
        chVar.getStatHolder().b(M);
    }

    private void c(@NonNull String str, @NonNull String str2, @Nullable ch chVar) {
        if ("start".equalsIgnoreCase(str)) {
            a("playbackStarted", str2, chVar);
        } else if ("firstQuartile".equalsIgnoreCase(str)) {
            a(25.0f, str2, chVar);
        } else if ("midpoint".equalsIgnoreCase(str)) {
            a(50.0f, str2, chVar);
        } else if ("thirdQuartile".equalsIgnoreCase(str)) {
            a(75.0f, str2, chVar);
        } else if ("complete".equalsIgnoreCase(str)) {
            a(100.0f, str2, chVar);
        } else if ("creativeView".equalsIgnoreCase(str)) {
            a("playbackStarted", str2, chVar);
        } else if (Events.CREATIVE_MUTE.equalsIgnoreCase(str)) {
            a("volumeOff", str2, chVar);
        } else if (Events.CREATIVE_UNMUTE.equalsIgnoreCase(str)) {
            a("volumeOn", str2, chVar);
        } else if ("pause".equalsIgnoreCase(str)) {
            a("playbackPaused", str2, chVar);
        } else if ("resume".equalsIgnoreCase(str)) {
            a("playbackResumed", str2, chVar);
        } else if (Events.CREATIVE_FULLSCREEN.equalsIgnoreCase(str)) {
            a("fullscreenOn", str2, chVar);
        } else if ("exitFullscreen".equalsIgnoreCase(str)) {
            a("fullscreenOff", str2, chVar);
        } else if ("skip".equalsIgnoreCase(str)) {
            a("closedByUser", str2, chVar);
        } else if ("error".equalsIgnoreCase(str)) {
            a("error", str2, chVar);
        } else if ("ClickTracking".equalsIgnoreCase(str)) {
            a(String.CLICK, str2, chVar);
        } else if ("close".equalsIgnoreCase(str)) {
            a("closedByUser", str2, chVar);
        } else if ("closeLinear".equalsIgnoreCase(str)) {
            a("closedByUser", str2, chVar);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    @VisibleForTesting
    public void b(@NonNull co coVar, @Nullable String str) {
        float f;
        if (str != null) {
            if (str.contains("%")) {
                int parseInt = Integer.parseInt(str.substring(0, str.length() - 1));
                StringBuilder sb = new StringBuilder();
                sb.append("Linear skipoffset is ");
                sb.append(str);
                sb.append(" [%]");
                ah.a(sb.toString());
                f = (coVar.getDuration() / 100.0f) * ((float) parseInt);
            } else if (str.contains(":")) {
                try {
                    f = W(str);
                } catch (Exception unused) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to convert ISO time skipoffset string ");
                    sb2.append(str);
                    b(coVar.getId(), "Bad value", sb2.toString());
                }
            }
            if (f <= 0.0f) {
                coVar.setAllowCloseDelay(f);
                return;
            }
            return;
        }
        f = -1.0f;
        if (f <= 0.0f) {
        }
    }

    private void a(@NonNull XmlPullParser xmlPullParser, @Nullable co coVar, @Nullable String str) {
        while (d(xmlPullParser) == 2) {
            String name = xmlPullParser.getName();
            if (e(xmlPullParser) == 2) {
                if ("Duration".equals(name)) {
                    if (coVar == null) {
                        continue;
                    } else if (a(xmlPullParser, coVar)) {
                        b(coVar, str);
                    } else {
                        return;
                    }
                } else if ("TrackingEvents".equals(name)) {
                    a(xmlPullParser, (ch) coVar);
                } else if ("MediaFiles".equals(name)) {
                    if (coVar == null) {
                        continue;
                    } else {
                        c(xmlPullParser, coVar);
                        if (coVar.getMediaData() == null) {
                            ah.a("Unable to find valid mediafile!");
                            return;
                        }
                    }
                } else if ("VideoClicks".equals(name)) {
                    b(xmlPullParser, coVar);
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }
}
