package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONObject;

/* compiled from: StandardAdSection */
public class dd extends cv {
    @Nullable
    private cs bL;
    private JSONObject dT;
    private boolean dZ = true;
    @Nullable
    private String ea;

    @NonNull
    public static dd ck() {
        return new dd();
    }

    private dd() {
    }

    public void p(boolean z) {
        this.dZ = z;
    }

    public boolean cl() {
        return this.dZ;
    }

    public void J(@Nullable String str) {
        this.ea = str;
    }

    @Nullable
    public String cm() {
        return this.ea;
    }

    public void d(JSONObject jSONObject) {
        this.dT = jSONObject;
    }

    public JSONObject cd() {
        return this.dT;
    }

    @Nullable
    public cs cn() {
        return this.bL;
    }

    public void a(@Nullable cs csVar) {
        this.bL = csVar;
    }

    public int getBannersCount() {
        return this.bL == null ? 0 : 1;
    }
}
