package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0056b;

/* compiled from: NativeAdFactory */
public final class v extends b<db> {
    @Nullable
    private final db section;

    /* compiled from: NativeAdFactory */
    static class a implements com.my.target.b.a<db> {
        public boolean a() {
            return false;
        }

        private a() {
        }

        @NonNull
        public c<db> b() {
            return w.f();
        }

        @Nullable
        public d<db> c() {
            return x.k();
        }

        @NonNull
        public e d() {
            return y.l();
        }
    }

    public interface b extends C0056b {
    }

    @NonNull
    public static b<db> a(@NonNull a aVar) {
        return new v(aVar, null);
    }

    @NonNull
    public static b<db> a(@NonNull db dbVar, @NonNull a aVar) {
        return new v(aVar, dbVar);
    }

    private v(@NonNull a aVar, @Nullable db dbVar) {
        super(new a(), aVar);
        this.section = dbVar;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: g */
    public db b(@NonNull Context context) {
        if (this.section != null) {
            return (db) a(this.section, context);
        }
        return (db) super.b(context);
    }
}
