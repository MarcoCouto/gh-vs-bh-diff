package com.my.target;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.ads.InterstitialSliderAd;
import com.my.target.ads.InterstitialSliderAd.InterstitialSliderAdListener;
import com.my.target.common.MyTargetActivity;
import com.my.target.common.MyTargetActivity.ActivityEngine;
import com.my.target.common.models.ImageData;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialSliderAdEngine */
public class aw implements ActivityEngine, com.my.target.fo.a {
    @Nullable
    private WeakReference<fo> X;
    @Nullable
    private WeakReference<MyTargetActivity> aT;
    private boolean ah;
    /* access modifiers changed from: private */
    @NonNull
    public final InterstitialSliderAd bd;
    @NonNull
    private final cz be;
    /* access modifiers changed from: private */
    @NonNull
    public final ArrayList<Object> bf = new ArrayList<>();
    /* access modifiers changed from: private */
    @Nullable
    public WeakReference<ey> bg;

    /* compiled from: InterstitialSliderAdEngine */
    class a implements com.my.target.ey.a {
        private a() {
        }

        public void a(@NonNull cm cmVar) {
            ey eyVar = aw.this.bg != null ? (ey) aw.this.bg.get() : null;
            if (eyVar != null) {
                hr.et().b(cmVar, eyVar.getView().getContext());
                InterstitialSliderAdListener listener = aw.this.bd.getListener();
                if (listener != null) {
                    listener.onClick(aw.this.bd);
                }
            }
        }

        public void aj() {
            aw.this.dismiss();
        }

        public void b(@NonNull cm cmVar) {
            ey eyVar = aw.this.bg != null ? (ey) aw.this.bg.get() : null;
            if (eyVar != null) {
                Context context = eyVar.getView().getContext();
                if (!aw.this.bf.contains(cmVar)) {
                    aw.this.bf.add(cmVar);
                    ib.a((List<dh>) cmVar.getStatHolder().N("playbackStarted"), context);
                }
            }
        }
    }

    public void a(boolean z) {
    }

    public boolean onActivityBackPressed() {
        return true;
    }

    public boolean onActivityOptionsItemSelected(MenuItem menuItem) {
        return false;
    }

    public void onActivityPause() {
    }

    public void onActivityResume() {
    }

    public void onActivityStart() {
    }

    public void onActivityStop() {
    }

    @NonNull
    public static aw a(@NonNull InterstitialSliderAd interstitialSliderAd, @NonNull cz czVar) {
        return new aw(interstitialSliderAd, czVar);
    }

    private aw(@NonNull InterstitialSliderAd interstitialSliderAd, @NonNull cz czVar) {
        this.bd = interstitialSliderAd;
        this.be = czVar;
    }

    public void k(@NonNull Context context) {
        if (this.ah) {
            ah.a("Unable to open Interstitial Ad twice, please dismiss currently showing ad first");
            return;
        }
        this.ah = true;
        MyTargetActivity.activityEngine = this;
        Intent intent = new Intent(context, MyTargetActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(268435456);
        }
        context.startActivity(intent);
    }

    public void l(@NonNull Context context) {
        if (this.ah) {
            ah.a("Unable to open Interstitial Ad twice, please dismiss currently showing ad first");
            return;
        }
        this.ah = true;
        fo foVar = this.X == null ? null : (fo) this.X.get();
        if (foVar == null || !foVar.isShowing()) {
            fo.a(this, context).show();
        } else {
            ah.c("InterstitialSliderAdEngine.showDialog: dialog already showing");
        }
    }

    public void dismiss() {
        this.ah = false;
        fo foVar = null;
        MyTargetActivity myTargetActivity = this.aT == null ? null : (MyTargetActivity) this.aT.get();
        if (myTargetActivity != null) {
            myTargetActivity.finish();
            return;
        }
        if (this.X != null) {
            foVar = (fo) this.X.get();
        }
        if (foVar != null && foVar.isShowing()) {
            foVar.dismiss();
        }
    }

    public void destroy() {
        dismiss();
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        this.aT = new WeakReference<>(myTargetActivity);
        myTargetActivity.setTheme(16973830);
        myTargetActivity.getWindow().setFlags(1024, 1024);
        a(frameLayout);
        InterstitialSliderAdListener listener = this.bd.getListener();
        if (listener != null) {
            listener.onDisplay(this.bd);
        }
    }

    public void onActivityDestroy() {
        this.ah = false;
        am();
        this.bg = null;
        this.aT = null;
        InterstitialSliderAdListener listener = this.bd.getListener();
        if (listener != null) {
            listener.onDismiss(this.bd);
        }
    }

    public void a(@NonNull fo foVar, @NonNull FrameLayout frameLayout) {
        this.X = new WeakReference<>(foVar);
        if (this.bd.isHideStatusBarInDialog()) {
            foVar.dO();
        }
        a(frameLayout);
        InterstitialSliderAdListener listener = this.bd.getListener();
        if (listener != null) {
            listener.onDisplay(this.bd);
        }
    }

    public void D() {
        this.ah = false;
        am();
        this.bg = null;
        this.X = null;
        InterstitialSliderAdListener listener = this.bd.getListener();
        if (listener != null) {
            listener.onDismiss(this.bd);
        }
    }

    private void am() {
        for (cm cmVar : this.be.bT()) {
            for (ImageData bitmap : cmVar.getLandscapeImages()) {
                bitmap.setBitmap(null);
            }
            for (ImageData bitmap2 : cmVar.getPortraitImages()) {
                bitmap2.setBitmap(null);
            }
        }
    }

    private void a(@NonNull FrameLayout frameLayout) {
        ey v = ey.v(frameLayout.getContext());
        this.bg = new WeakReference<>(v);
        v.a((com.my.target.ey.a) new a());
        v.a(this.be);
        frameLayout.addView(v.getView(), new LayoutParams(-1, -1));
    }
}
