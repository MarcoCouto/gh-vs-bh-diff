package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: IInterstitialAdEngine */
public interface an {
    @Nullable
    String aa();

    void destroy();

    void dismiss();

    void k(@NonNull Context context);

    void l(@NonNull Context context);
}
