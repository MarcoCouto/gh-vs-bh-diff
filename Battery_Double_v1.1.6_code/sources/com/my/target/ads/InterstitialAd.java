package com.my.target.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ah;
import com.my.target.an;
import com.my.target.as;
import com.my.target.ay;
import com.my.target.b.C0056b;
import com.my.target.cj;
import com.my.target.common.BaseAd;
import com.my.target.ct;
import com.my.target.cy;
import com.my.target.o;
import com.my.target.o.b;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;

public final class InterstitialAd extends BaseAd {
    @NonNull
    private final Context context;
    @Nullable
    private an engine;
    private boolean hideStatusBarInDialog = false;
    @Nullable
    private InterstitialAdListener listener;
    private boolean useExoPlayer = true;

    public interface InterstitialAdListener {
        void onClick(@NonNull InterstitialAd interstitialAd);

        void onDismiss(@NonNull InterstitialAd interstitialAd);

        void onDisplay(@NonNull InterstitialAd interstitialAd);

        void onLoad(@NonNull InterstitialAd interstitialAd);

        void onNoAd(@NonNull String str, @NonNull InterstitialAd interstitialAd);

        void onVideoCompleted(@NonNull InterstitialAd interstitialAd);
    }

    public InterstitialAd(int i, @NonNull Context context2) {
        super(i, Events.CREATIVE_FULLSCREEN);
        this.context = context2;
        ah.c("InterstitialAd created. Version: 5.5.6");
    }

    public void useExoPlayer(boolean z) {
        this.useExoPlayer = z;
    }

    public boolean isUseExoPlayer() {
        return this.useExoPlayer;
    }

    public boolean isHideStatusBarInDialog() {
        return this.hideStatusBarInDialog;
    }

    public void setHideStatusBarInDialog(boolean z) {
        this.hideStatusBarInDialog = z;
    }

    public void setMediationEnabled(boolean z) {
        this.adConfig.setMediationEnabled(z);
    }

    public boolean isMediationEnabled() {
        return this.adConfig.isMediationEnabled();
    }

    @Nullable
    public InterstitialAdListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable InterstitialAdListener interstitialAdListener) {
        this.listener = interstitialAdListener;
    }

    public final void load() {
        o.a(this.adConfig).a((C0056b<T>) new b() {
            public void onResult(@Nullable cy cyVar, @Nullable String str) {
                InterstitialAd.this.handleResult(cyVar, str);
            }
        }).a(this.context);
    }

    public void loadFromBid(@NonNull String str) {
        this.adConfig.setBidId(str);
        load();
    }

    public final void handleSection(@NonNull cy cyVar) {
        o.a(cyVar, this.adConfig).a((C0056b<T>) new b() {
            public void onResult(@Nullable cy cyVar, @Nullable String str) {
                InterstitialAd.this.handleResult(cyVar, str);
            }
        }).a(this.context);
    }

    public void show() {
        if (this.engine == null) {
            ah.c("InterstitialAd.show: No ad");
        } else {
            this.engine.k(this.context);
        }
    }

    public void showDialog() {
        if (this.engine == null) {
            ah.c("InterstitialAd.showDialog: No ad");
        } else {
            this.engine.l(this.context);
        }
    }

    public void dismiss() {
        if (this.engine != null) {
            this.engine.dismiss();
        }
    }

    public void destroy() {
        if (this.engine != null) {
            this.engine.destroy();
            this.engine = null;
        }
        this.listener = null;
    }

    @Nullable
    public String getAdSource() {
        if (this.engine != null) {
            return this.engine.aa();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable cy cyVar, @Nullable String str) {
        ct ctVar;
        if (this.listener != null) {
            cj cjVar = null;
            if (cyVar != null) {
                cjVar = cyVar.bP();
                ctVar = cyVar.bI();
            } else {
                ctVar = null;
            }
            if (cjVar != null) {
                this.engine = as.a(this, cjVar, cyVar);
                if (this.engine != null) {
                    this.listener.onLoad(this);
                } else {
                    this.listener.onNoAd("no ad", this);
                }
            } else if (ctVar != null) {
                ay a = ay.a(this, ctVar, this.adConfig);
                this.engine = a;
                a.n(this.context);
            } else {
                InterstitialAdListener interstitialAdListener = this.listener;
                if (str == null) {
                    str = "no ad";
                }
                interstitialAdListener.onNoAd(str, this);
            }
        }
    }
}
