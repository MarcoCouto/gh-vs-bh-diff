package com.my.target.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.my.target.a;
import com.my.target.ae;
import com.my.target.ah;
import com.my.target.b.C0056b;
import com.my.target.be;
import com.my.target.common.CustomParams;
import com.my.target.dd;

public final class MyTargetView extends RelativeLayout {
    @Nullable
    private a adConfig;
    private int adSize = 0;
    private boolean attached;
    @Nullable
    private be engine;
    @Nullable
    private MyTargetViewListener listener;
    private boolean mediationEnabled = true;
    private boolean trackingEnvironmentEnabled = true;
    private boolean trackingLocationEnabled = true;

    public interface AdSize {
        public static final int BANNER_300x250 = 1;
        public static final int BANNER_320x50 = 0;
        public static final int BANNER_728x90 = 2;
    }

    public interface MyTargetViewListener {
        void onClick(@NonNull MyTargetView myTargetView);

        void onLoad(@NonNull MyTargetView myTargetView);

        void onNoAd(@NonNull String str, @NonNull MyTargetView myTargetView);

        void onShow(@NonNull MyTargetView myTargetView);
    }

    public static void setDebugMode(boolean z) {
        ah.enabled = z;
        if (z) {
            ah.a("Debug mode enabled");
        }
    }

    @Nullable
    public MyTargetViewListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable MyTargetViewListener myTargetViewListener) {
        this.listener = myTargetViewListener;
    }

    public boolean isTrackingEnvironmentEnabled() {
        return this.trackingEnvironmentEnabled;
    }

    public void setTrackingEnvironmentEnabled(boolean z) {
        this.trackingEnvironmentEnabled = z;
        if (this.adConfig != null) {
            this.adConfig.setTrackingEnvironmentEnabled(z);
        }
    }

    public void setTrackingLocationEnabled(boolean z) {
        this.trackingLocationEnabled = z;
        if (this.adConfig != null) {
            this.adConfig.setTrackingLocationEnabled(z);
        }
    }

    public boolean isTrackingLocationEnabled() {
        return this.trackingLocationEnabled;
    }

    public boolean isMediationEnabled() {
        return this.mediationEnabled;
    }

    public void setMediationEnabled(boolean z) {
        this.mediationEnabled = z;
        if (this.adConfig != null) {
            this.adConfig.setMediationEnabled(z);
        }
    }

    @Nullable
    public CustomParams getCustomParams() {
        if (this.adConfig != null) {
            return this.adConfig.getCustomParams();
        }
        return null;
    }

    public int getAdSize() {
        return this.adSize;
    }

    public MyTargetView(@NonNull Context context) {
        super(context);
        ah.c("MyTargetView created. Version: 5.5.6");
    }

    public MyTargetView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        ah.c("MyTargetView created. Version: 5.5.6");
    }

    public MyTargetView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ah.c("MyTargetView created. Version: 5.5.6");
    }

    public void init(int i) {
        init(i, true);
    }

    public void init(int i, boolean z) {
        init(i, 0, z);
    }

    public void init(int i, int i2) {
        init(i, i2, true);
    }

    public void init(int i, int i2, boolean z) {
        if (this.adConfig == null) {
            this.adSize = i2;
            String str = "standard_320x50";
            if (i2 == 1) {
                str = "standard_300x250";
            } else if (i2 == 2) {
                str = "standard_728x90";
            }
            this.adConfig = a.newConfig(i, str);
            this.adConfig.setTrackingEnvironmentEnabled(this.trackingEnvironmentEnabled);
            this.adConfig.setTrackingLocationEnabled(this.trackingLocationEnabled);
            this.adConfig.setMediationEnabled(this.mediationEnabled);
            this.adConfig.setRefreshAd(z);
            ah.a("MyTargetView initialized");
        }
    }

    public final void load() {
        if (this.adConfig == null) {
            ah.a("MyTargetView is not initialized");
        } else {
            ae.a(this.adConfig).a((C0056b<T>) new ae.a() {
                public void onResult(@Nullable dd ddVar, @Nullable String str) {
                    MyTargetView.this.handleResult(ddVar, str);
                }
            }).a(getContext());
        }
    }

    public void loadFromBid(@NonNull String str) {
        if (this.adConfig == null) {
            ah.a("MyTargetView is not initialized");
            return;
        }
        this.adConfig.setBidId(str);
        this.adConfig.setRefreshAd(false);
        load();
    }

    public final void handleSection(@NonNull dd ddVar) {
        if (this.adConfig == null) {
            ah.a("MyTargetView is not initialized");
        } else {
            ae.a(ddVar, this.adConfig).a((C0056b<T>) new ae.a() {
                public void onResult(@Nullable dd ddVar, @Nullable String str) {
                    MyTargetView.this.handleResult(ddVar, str);
                }
            }).a(getContext());
        }
    }

    @Nullable
    public String getAdSource() {
        if (this.engine != null) {
            return this.engine.aa();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable dd ddVar, @Nullable String str) {
        if (this.listener != null) {
            if (ddVar == null || this.adConfig == null) {
                MyTargetViewListener myTargetViewListener = this.listener;
                if (str == null) {
                    str = "no ad";
                }
                myTargetViewListener.onNoAd(str, this);
                return;
            }
            this.engine = be.a(this, this.adConfig);
            this.engine.d(this.attached);
            this.engine.a(ddVar);
            if (this.adConfig != null) {
                this.adConfig.setBidId(null);
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.engine != null) {
            this.engine.onWindowFocusChanged(z);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.attached = true;
        if (this.engine != null) {
            this.engine.d(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.attached = true;
        if (this.engine != null) {
            this.engine.d(false);
        }
    }

    public void destroy() {
        if (this.engine != null) {
            this.engine.destroy();
            this.engine = null;
        }
        this.listener = null;
    }
}
