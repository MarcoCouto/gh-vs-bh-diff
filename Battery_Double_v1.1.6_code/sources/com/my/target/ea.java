package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.appodeal.ads.AppodealNetworks;
import com.my.target.common.models.ImageData;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InterstitialAdBannerParser */
public class ea {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eA;
    @NonNull
    private final dw eB;

    @NonNull
    public static ea d(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new ea(bzVar, aVar, context2);
    }

    private ea(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eA = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.eB = dw.b(bzVar, aVar, context2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0064  */
    @Nullable
    public cj b(@NonNull JSONObject jSONObject, @Nullable String str) {
        char c;
        String optString = jSONObject.optString("type", "");
        int hashCode = optString.hashCode();
        if (hashCode != -1396342996) {
            if (hashCode != 3213227) {
                if (hashCode != 106940687) {
                    if (hashCode == 110066619 && optString.equals(Events.CREATIVE_FULLSCREEN)) {
                        c = 0;
                        switch (c) {
                            case 0:
                            case 1:
                                cm newBanner = cm.newBanner();
                                if (a(jSONObject, newBanner)) {
                                    return newBanner;
                                }
                                break;
                            case 2:
                                cn newBanner2 = cn.newBanner();
                                if (a(jSONObject, newBanner2, str)) {
                                    return newBanner2;
                                }
                                break;
                            case 3:
                                cl newBanner3 = cl.newBanner();
                                if (a(jSONObject, newBanner3, str)) {
                                    return newBanner3;
                                }
                                break;
                        }
                        return null;
                    }
                } else if (optString.equals(NotificationCompat.CATEGORY_PROMO)) {
                    c = 2;
                    switch (c) {
                        case 0:
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                    }
                    return null;
                }
            } else if (optString.equals(String.HTML)) {
                c = 3;
                switch (c) {
                    case 0:
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
                return null;
            }
        } else if (optString.equals("banner")) {
            c = 1;
            switch (c) {
                case 0:
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
            }
            return null;
        }
        c = 65535;
        switch (c) {
            case 0:
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(@NonNull JSONObject jSONObject, @NonNull cm cmVar) {
        b(jSONObject, (cj) cmVar);
        return eb.e(this.eA, this.adConfig, this.context).b(jSONObject, cmVar);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(@NonNull JSONObject jSONObject, @NonNull cn cnVar, @Nullable String str) {
        b(jSONObject, (cj) cnVar);
        JSONObject optJSONObject = jSONObject.optJSONObject("styleSettings");
        if (optJSONObject != null) {
            a(optJSONObject, cnVar.getPromoStyleSettings());
        }
        cnVar.setStyle(jSONObject.optInt("style", cnVar.getStyle()));
        cnVar.setCloseOnClick(jSONObject.optBoolean("closeOnClick", cnVar.isCloseOnClick()));
        cnVar.setVideoRequired(jSONObject.optBoolean("videoRequired", cnVar.isVideoRequired()));
        JSONArray optJSONArray = jSONObject.optJSONArray("cards");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                if (optJSONObject2 != null) {
                    ck a = a(optJSONObject2, (cj) cnVar);
                    if (a != null) {
                        cnVar.addInterstitialAdCard(a);
                    }
                }
            }
        }
        if (cnVar.getInterstitialAdCards().isEmpty()) {
            JSONObject optJSONObject3 = jSONObject.optJSONObject("video");
            if (optJSONObject3 != null) {
                co newVideoBanner = co.newVideoBanner();
                newVideoBanner.setId(cnVar.getId());
                if (dx.c(this.eA, this.adConfig, this.context).a(optJSONObject3, newVideoBanner)) {
                    cnVar.setVideoBanner(newVideoBanner);
                    if (newVideoBanner.isAutoPlay()) {
                        cnVar.setAllowClose(newVideoBanner.isAllowClose());
                        cnVar.setAllowCloseDelay(newVideoBanner.getAllowCloseDelay());
                    }
                }
                JSONObject optJSONObject4 = jSONObject.optJSONObject("endcard");
                if (optJSONObject4 != null) {
                    cnVar.setEndCard(b(optJSONObject4, str));
                }
            }
        }
        String optString = jSONObject.optString("adIconLink");
        if (!TextUtils.isEmpty(optString)) {
            cnVar.setAdIcon(ImageData.newImageData(optString));
            cnVar.setAdIconClickLink(jSONObject.optString("adIconClickLink"));
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(@NonNull JSONObject jSONObject, @NonNull cl clVar, @Nullable String str) {
        String str2;
        String optString = jSONObject.optString("source", null);
        if (optString == null) {
            b("Required field", "Banner with type 'html' has no source field", clVar.getId());
            return false;
        }
        String decode = id.decode(optString);
        b(jSONObject, (cj) clVar);
        if (!TextUtils.isEmpty(str)) {
            str2 = dw.g(str, decode);
            if (str2 != null) {
                clVar.setType(AppodealNetworks.MRAID);
                clVar.setSource(str2);
                return this.eB.a(str2, jSONObject);
            }
        }
        str2 = decode;
        clVar.setSource(str2);
        return this.eB.a(str2, jSONObject);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public ck a(@NonNull JSONObject jSONObject, @NonNull cj cjVar) {
        ck newCard = ck.newCard(cjVar);
        newCard.setClickArea(cjVar.getClickArea());
        this.eB.a(jSONObject, (ch) newCard);
        if (TextUtils.isEmpty(newCard.getTrackingLink())) {
            b("Required field", "no tracking link in interstitialAdCard", cjVar.getId());
            return null;
        } else if (newCard.getImage() == null) {
            b("Required field", "no image in interstitialAdCard", cjVar.getId());
            return null;
        } else {
            newCard.setId(jSONObject.optString("cardID", newCard.getId()));
            return newCard;
        }
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull ce ceVar) {
        ceVar.i(ee.a(jSONObject, "ctaButtonColor", ceVar.br()));
        ceVar.j(ee.a(jSONObject, "ctaButtonTouchColor", ceVar.bs()));
        ceVar.k(ee.a(jSONObject, "ctaButtonTextColor", ceVar.bt()));
        ceVar.setBackgroundColor(ee.a(jSONObject, "backgroundColor", ceVar.getBackgroundColor()));
        ceVar.setTextColor(ee.a(jSONObject, "textColor", ceVar.getTextColor()));
        ceVar.setTitleColor(ee.a(jSONObject, "titleTextColor", ceVar.getTextColor()));
        ceVar.n(ee.a(jSONObject, "domainTextColor", ceVar.bx()));
        ceVar.m(ee.a(jSONObject, "progressBarColor", ceVar.bv()));
        ceVar.l(ee.a(jSONObject, "barColor", ceVar.bu()));
        float optDouble = (float) jSONObject.optDouble("barOverlayAlpha", (double) ceVar.bw());
        if (0.0f <= optDouble && optDouble <= 1.0f) {
            ceVar.f(optDouble);
        }
        String optString = jSONObject.optString("storeIcon");
        if (!TextUtils.isEmpty(optString)) {
            ceVar.a(ImageData.newImageData(optString));
        }
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull cj cjVar) {
        this.eB.a(jSONObject, (ch) cjVar);
        cjVar.setAllowBackButton(jSONObject.optBoolean("allowBackButton", cjVar.isAllowBackButton()));
        cjVar.setAllowCloseDelay((float) jSONObject.optDouble("allowCloseDelay", (double) cjVar.getAllowCloseDelay()));
        String optString = jSONObject.optString("close_icon_hd");
        if (!TextUtils.isEmpty(optString)) {
            cjVar.setCloseIcon(ImageData.newImageData(optString));
        }
    }

    private void b(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        dq.P(str).Q(str2).x(this.adConfig.getSlotId()).S(str3).R(this.eA.getUrl()).q(this.context);
    }
}
