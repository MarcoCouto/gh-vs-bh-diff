package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InterstitialSliderAdResponseParser */
public class t extends c<cz> {
    @NonNull
    public static c<cz> f() {
        return new t();
    }

    private t() {
    }

    @Nullable
    public cz a(@NonNull String str, @NonNull bz bzVar, @Nullable cz czVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a = a(str, context);
        if (a == null) {
            return null;
        }
        JSONObject optJSONObject = a.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            return null;
        }
        if (czVar == null) {
            czVar = cz.bQ();
        }
        ed.f(bzVar, aVar, context).a(optJSONObject, czVar);
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        ec a2 = ec.a(czVar, bzVar, aVar, context);
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
            if (optJSONObject2 != null) {
                cm newBanner = cm.newBanner();
                if (a2.a(optJSONObject2, newBanner)) {
                    czVar.c(newBanner);
                }
            }
        }
        if (czVar.getBannersCount() > 0) {
            return czVar;
        }
        return null;
    }
}
