package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.AudioData;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InstreamAudioAdResponseParser */
public class j extends c<cx> {
    @NonNull
    public static c<cx> f() {
        return new j();
    }

    @Nullable
    public cx a(@NonNull String str, @NonNull bz bzVar, @Nullable cx cxVar, @NonNull a aVar, @NonNull Context context) {
        if (isVast(str)) {
            return b(str, bzVar, cxVar, aVar, context);
        }
        return c(str, bzVar, cxVar, aVar, context);
    }

    @NonNull
    private cx b(@NonNull String str, @NonNull bz bzVar, @Nullable cx cxVar, @NonNull a aVar, @NonNull Context context) {
        en a = en.a(aVar, bzVar, context);
        a.V(str);
        String bd = bzVar.bd();
        if (bd == null) {
            bd = BreakId.PREROLL;
        }
        if (cxVar == null) {
            cxVar = cx.bM();
        }
        da y = cxVar.y(bd);
        if (y == null) {
            return cxVar;
        }
        if (!a.cO().isEmpty()) {
            a(a, y, bzVar);
        } else {
            bz cP = a.cP();
            if (cP != null) {
                cP.s(y.getName());
                int position = bzVar.getPosition();
                if (position >= 0) {
                    cP.setPosition(position);
                } else {
                    cP.setPosition(y.getBannersCount());
                }
                y.c(cP);
            }
        }
        return cxVar;
    }

    private void a(@NonNull en<AudioData> enVar, @NonNull da<AudioData> daVar, @NonNull bz bzVar) {
        daVar.d(enVar.bb());
        int position = bzVar.getPosition();
        Iterator it = enVar.cO().iterator();
        while (it.hasNext()) {
            co coVar = (co) it.next();
            Boolean be = bzVar.be();
            if (be != null) {
                coVar.setAllowClose(be.booleanValue());
            }
            float allowCloseDelay = bzVar.getAllowCloseDelay();
            if (allowCloseDelay > 0.0f) {
                coVar.setAllowCloseDelay(allowCloseDelay);
            }
            Boolean bf = bzVar.bf();
            if (bf != null) {
                coVar.setAllowPause(bf.booleanValue());
            }
            Boolean bg = bzVar.bg();
            if (bg != null) {
                coVar.setAllowSeek(bg.booleanValue());
            }
            Boolean bh = bzVar.bh();
            if (bh != null) {
                coVar.setAllowSkip(bh.booleanValue());
            }
            Boolean bi = bzVar.bi();
            if (bi != null) {
                coVar.setAllowTrackChange(bi.booleanValue());
            }
            Boolean bk = bzVar.bk();
            if (bk != null) {
                coVar.setDirectLink(bk.booleanValue());
            }
            Boolean bl = bzVar.bl();
            if (bl != null) {
                coVar.setOpenInBrowser(bl.booleanValue());
            }
            coVar.setCloseActionText("Close");
            float point = bzVar.getPoint();
            if (point >= 0.0f) {
                coVar.setPoint(point);
            }
            float pointP = bzVar.getPointP();
            if (pointP >= 0.0f) {
                coVar.setPointP(pointP);
            }
            if (position >= 0) {
                int i = position + 1;
                daVar.a(coVar, position);
                position = i;
            } else {
                daVar.g(coVar);
            }
        }
    }

    @Nullable
    private cx c(@NonNull String str, @NonNull bz bzVar, @Nullable cx cxVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a = a(str, context);
        if (a == null) {
            return cxVar;
        }
        JSONObject optJSONObject = a.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            return cxVar;
        }
        if (cxVar == null) {
            cxVar = cx.bM();
        }
        dz.cM().a(optJSONObject, cxVar);
        du a2 = du.a(bzVar, aVar, context);
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("sections");
        if (optJSONObject2 != null) {
            String bd = bzVar.bd();
            if (bd != null) {
                da y = cxVar.y(bd);
                if (y != null) {
                    a(optJSONObject2, a2, y, dv.a(y, bzVar, aVar, context), bzVar);
                }
            } else {
                Iterator it = cxVar.bN().iterator();
                while (it.hasNext()) {
                    da daVar = (da) it.next();
                    a(optJSONObject2, a2, daVar, dv.a(daVar, bzVar, aVar, context), bzVar);
                }
            }
        }
        return cxVar;
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull du duVar, @NonNull da<AudioData> daVar, @NonNull dv dvVar, @NonNull bz bzVar) {
        da<AudioData> daVar2 = daVar;
        JSONObject jSONObject2 = jSONObject;
        JSONArray optJSONArray = jSONObject.optJSONArray(daVar.getName());
        if (optJSONArray != null) {
            int position = bzVar.getPosition();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            int i = position;
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i2);
                if (optJSONObject != null) {
                    if ("additionalData".equals(optJSONObject.optString("type"))) {
                        a(bzVar, duVar, optJSONObject, daVar, arrayList2, arrayList);
                    } else {
                        co newAudioBanner = co.newAudioBanner();
                        if (dvVar.a(optJSONObject, newAudioBanner)) {
                            if (bzVar.aX()) {
                                newAudioBanner.setPoint(bzVar.getPoint());
                                newAudioBanner.setPointP(bzVar.getPointP());
                            }
                            if (i >= 0) {
                                int i3 = i + 1;
                                daVar2.a(newAudioBanner, i);
                                i = i3;
                            } else {
                                daVar2.g(newAudioBanner);
                            }
                        }
                    }
                }
                dv dvVar2 = dvVar;
            }
            a(arrayList2, arrayList);
        }
    }

    private void a(@NonNull bz bzVar, @NonNull du duVar, @NonNull JSONObject jSONObject, @NonNull da daVar, @NonNull ArrayList<bz> arrayList, @NonNull ArrayList<bz> arrayList2) {
        bz e = duVar.e(jSONObject);
        if (e != null) {
            e.s(daVar.getName());
            if (e.aW() != -1) {
                arrayList2.add(e);
                return;
            }
            arrayList.add(e);
            if (!e.aX() && !e.aV()) {
                bzVar.b(e);
                int position = bzVar.getPosition();
                if (position >= 0) {
                    e.setPosition(position);
                } else {
                    e.setPosition(daVar.getBannersCount());
                }
            }
            daVar.c(e);
        }
    }

    private void a(@NonNull ArrayList<bz> arrayList, @NonNull ArrayList<bz> arrayList2) {
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            bz bzVar = (bz) it.next();
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                bz bzVar2 = (bz) it2.next();
                if (bzVar.aW() == bzVar2.getId()) {
                    bzVar2.a(bzVar);
                    break;
                }
            }
        }
    }
}
