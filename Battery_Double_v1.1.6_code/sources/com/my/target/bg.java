package com.my.target;

import android.support.annotation.NonNull;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AbstractJsCall */
public abstract class bg implements bi {
    @NonNull
    private final JSONObject ci = new JSONObject();
    @NonNull
    JSONObject cj = new JSONObject();
    @NonNull
    private final String type;

    public bg(@NonNull String str) throws JSONException {
        this.type = str;
        this.ci.put("method", str);
        this.ci.put("data", this.cj);
    }

    @NonNull
    public JSONObject aG() {
        return this.ci;
    }
}
