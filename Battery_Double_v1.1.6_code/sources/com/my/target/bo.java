package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: JsAdStartEvent */
public class bo extends bl {
    @NonNull
    private final String[] cl;
    @NonNull
    private final String format;

    public bo(@NonNull String[] strArr, @NonNull String str) {
        super("onAdStart");
        this.format = str;
        this.cl = strArr;
    }
}
