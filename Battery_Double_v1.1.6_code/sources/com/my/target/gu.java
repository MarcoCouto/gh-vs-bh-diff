package com.my.target;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.my.target.common.models.ImageData;
import com.my.target.nativeads.views.MediaAdView;
import java.util.ArrayList;

@SuppressLint({"ViewConstructor"})
/* compiled from: PanelView */
public class gu extends ViewGroup {
    private static final int CTA_ID = ic.eG();
    private static final int RATING_ID = ic.eG();
    private static final int iQ = ic.eG();
    private static final int jc = ic.eG();
    private static final int kp = ic.eG();
    private static final int kq = ic.eG();
    private static final int kr = ic.eG();
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final ge iconImageView;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView ks;
    @NonNull
    private final gg kt;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView ku;
    private final int kv;
    private final int kw;
    private final int kx;
    /* access modifiers changed from: private */
    @NonNull
    public final LinearLayout ratingLayout;
    @NonNull
    private final gf starsView;
    @NonNull
    private final ic uiUtils;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView urlLabel;
    @NonNull
    private final TextView votesLabel;

    public gu(@NonNull Context context, @NonNull ic icVar) {
        super(context);
        this.uiUtils = icVar;
        this.ctaButton = new Button(context);
        this.ctaButton.setId(CTA_ID);
        ic.a((View) this.ctaButton, "cta_button");
        this.iconImageView = new ge(context);
        this.iconImageView.setId(kp);
        ic.a((View) this.iconImageView, "icon_image");
        this.kt = new gg(context);
        this.kt.setId(iQ);
        this.ks = new TextView(context);
        this.ks.setId(kq);
        ic.a((View) this.ks, "description_text");
        this.ku = new TextView(context);
        ic.a((View) this.ku, "disclaimer_text");
        this.ratingLayout = new LinearLayout(context);
        this.starsView = new gf(context);
        this.starsView.setId(RATING_ID);
        ic.a((View) this.starsView, "stars_view");
        this.votesLabel = new TextView(context);
        this.votesLabel.setId(kr);
        ic.a((View) this.votesLabel, "votes_text");
        this.urlLabel = new TextView(context);
        ic.a((View) this.urlLabel, "domain_text");
        this.urlLabel.setId(jc);
        this.kv = icVar.M(16);
        this.kx = icVar.M(8);
        this.kw = icVar.M(64);
    }

    public void initView() {
        setBackgroundColor(1711276032);
        this.ks.setTextColor(-2236963);
        this.ks.setEllipsize(TruncateAt.END);
        this.urlLabel.setTextColor(-6710887);
        this.urlLabel.setVisibility(8);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(0);
        gradientDrawable.setStroke(1, -3355444);
        this.ku.setPadding(this.uiUtils.M(4), this.uiUtils.M(4), this.uiUtils.M(4), this.uiUtils.M(4));
        this.ku.setBackgroundDrawable(gradientDrawable);
        this.ku.setTextSize(2, 12.0f);
        this.ku.setTextColor(-3355444);
        this.ku.setVisibility(8);
        this.ratingLayout.setOrientation(0);
        this.ratingLayout.setGravity(16);
        this.ratingLayout.setVisibility(8);
        this.votesLabel.setTextColor(-6710887);
        this.votesLabel.setGravity(16);
        this.votesLabel.setTextSize(2, 14.0f);
        this.ctaButton.setPadding(this.uiUtils.M(15), 0, this.uiUtils.M(15), 0);
        this.ctaButton.setMinimumWidth(this.uiUtils.M(100));
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setTextSize(2, 22.0f);
        this.ctaButton.setMaxEms(10);
        this.ctaButton.setSingleLine();
        this.ctaButton.setEllipsize(TruncateAt.END);
        fu rightBorderedView = this.kt.getRightBorderedView();
        rightBorderedView.f(1, -7829368);
        rightBorderedView.setPadding(this.uiUtils.M(2), 0, 0, 0);
        rightBorderedView.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
        rightBorderedView.a(1, MediaAdView.COLOR_PLACEHOLDER_GRAY, this.uiUtils.M(3));
        rightBorderedView.setBackgroundColor(1711276032);
        this.starsView.setStarSize(this.uiUtils.M(12));
        this.ratingLayout.addView(this.starsView);
        this.ratingLayout.addView(this.votesLabel);
        this.ratingLayout.setVisibility(8);
        this.urlLabel.setVisibility(8);
        addView(this.kt);
        addView(this.ratingLayout);
        addView(this.urlLabel);
        addView(this.ks);
        addView(this.ku);
        addView(this.iconImageView);
        addView(this.ctaButton);
    }

    public void setBanner(@NonNull cn cnVar) {
        this.kt.getLeftText().setText(cnVar.getTitle());
        this.ks.setText(cnVar.getDescription());
        String disclaimer = cnVar.getDisclaimer();
        if (!TextUtils.isEmpty(disclaimer)) {
            this.ku.setVisibility(0);
            this.ku.setText(disclaimer);
        } else {
            this.ku.setVisibility(8);
        }
        ImageData icon = cnVar.getIcon();
        if (icon != null) {
            this.iconImageView.setVisibility(0);
            this.iconImageView.setImageData(icon);
        } else {
            this.iconImageView.setVisibility(8);
        }
        this.ctaButton.setText(cnVar.getCtaText());
        if (!"".equals(cnVar.getAgeRestrictions())) {
            this.kt.getRightBorderedView().setText(cnVar.getAgeRestrictions());
        } else {
            this.kt.getRightBorderedView().setVisibility(8);
        }
        ic.a(this.ctaButton, -16733198, -16746839, this.uiUtils.M(2));
        this.ctaButton.setTextColor(-1);
        if ("store".equals(cnVar.getNavigationType())) {
            if (cnVar.getVotes() == 0 || cnVar.getRating() <= 0.0f) {
                this.ratingLayout.setEnabled(false);
                this.ratingLayout.setVisibility(8);
            } else {
                this.ratingLayout.setEnabled(true);
                this.starsView.setRating(cnVar.getRating());
                this.votesLabel.setText(String.valueOf(cnVar.getVotes()));
            }
            this.urlLabel.setEnabled(false);
        } else {
            String domain = cnVar.getDomain();
            if (!TextUtils.isEmpty(domain)) {
                this.urlLabel.setEnabled(true);
                this.urlLabel.setText(domain);
            } else {
                this.urlLabel.setEnabled(false);
                this.urlLabel.setVisibility(8);
            }
            this.ratingLayout.setEnabled(false);
        }
        if (cnVar.getVideoBanner() == null || !cnVar.getVideoBanner().isAutoPlay()) {
            this.ratingLayout.setVisibility(8);
            this.urlLabel.setVisibility(8);
        }
    }

    public void a(View... viewArr) {
        if (getVisibility() == 0) {
            b(viewArr);
        }
    }

    public void a(@NonNull ca caVar, @NonNull OnClickListener onClickListener) {
        if (caVar.f456do) {
            setOnClickListener(onClickListener);
            this.ctaButton.setOnClickListener(onClickListener);
            return;
        }
        if (caVar.di) {
            this.ctaButton.setOnClickListener(onClickListener);
        } else {
            this.ctaButton.setEnabled(false);
        }
        if (caVar.dn) {
            setOnClickListener(onClickListener);
        } else {
            setOnClickListener(null);
        }
        if (caVar.dc) {
            this.kt.getLeftText().setOnClickListener(onClickListener);
        } else {
            this.kt.getLeftText().setOnClickListener(null);
        }
        if (caVar.dj) {
            this.kt.getRightBorderedView().setOnClickListener(onClickListener);
        } else {
            this.kt.getRightBorderedView().setOnClickListener(null);
        }
        if (caVar.de) {
            this.iconImageView.setOnClickListener(onClickListener);
        } else {
            this.iconImageView.setOnClickListener(null);
        }
        if (caVar.dd) {
            this.ks.setOnClickListener(onClickListener);
        } else {
            this.ks.setOnClickListener(null);
        }
        if (caVar.dg) {
            this.starsView.setOnClickListener(onClickListener);
        } else {
            this.starsView.setOnClickListener(null);
        }
        if (caVar.dh) {
            this.votesLabel.setOnClickListener(onClickListener);
        } else {
            this.votesLabel.setOnClickListener(null);
        }
        if (caVar.dl) {
            this.urlLabel.setOnClickListener(onClickListener);
        } else {
            this.urlLabel.setOnClickListener(null);
        }
    }

    private void b(View... viewArr) {
        a(0, viewArr);
    }

    /* access modifiers changed from: 0000 */
    public void c(View... viewArr) {
        if (getVisibility() == 0) {
            a(300, viewArr);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(View... viewArr) {
        e(viewArr);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int i3 = size - (this.kv * 2);
        int size2 = (MeasureSpec.getSize(i2) / 4) - (this.kx * 2);
        int min = Math.min(size2, this.kw);
        this.iconImageView.measure(MeasureSpec.makeMeasureSpec(min, 1073741824), MeasureSpec.makeMeasureSpec(min, 1073741824));
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(min - (this.kx * 2), 1073741824));
        int measuredWidth = ((i3 - this.iconImageView.getMeasuredWidth()) - this.ctaButton.getMeasuredWidth()) - (this.kv * 2);
        this.kt.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        this.ratingLayout.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        this.urlLabel.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        this.ks.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2 - this.kt.getMeasuredHeight(), Integer.MIN_VALUE));
        this.ku.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        int measuredHeight = this.kt.getMeasuredHeight() + Math.max(this.ks.getMeasuredHeight(), this.ratingLayout.getMeasuredHeight()) + (this.kx * 2);
        if (this.ku.getVisibility() == 0) {
            measuredHeight += this.ku.getMeasuredHeight();
        }
        setMeasuredDimension(size, Math.max(this.ctaButton.getMeasuredHeight(), Math.max(this.iconImageView.getMeasuredHeight(), measuredHeight)) + (this.kx * 2));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredHeight2 = this.iconImageView.getMeasuredHeight();
        int measuredWidth2 = this.iconImageView.getMeasuredWidth();
        int i5 = (measuredHeight - measuredHeight2) / 2;
        this.iconImageView.layout(this.kv, i5, this.kv + measuredWidth2, measuredHeight2 + i5);
        int measuredWidth3 = this.ctaButton.getMeasuredWidth();
        int measuredHeight3 = this.ctaButton.getMeasuredHeight();
        int i6 = (measuredHeight - measuredHeight3) / 2;
        this.ctaButton.layout((measuredWidth - measuredWidth3) - this.kv, i6, measuredWidth - this.kv, measuredHeight3 + i6);
        int i7 = this.kv + measuredWidth2 + this.kv;
        this.kt.layout(i7, this.kx, this.kt.getMeasuredWidth() + i7, this.kx + this.kt.getMeasuredHeight());
        this.ratingLayout.layout(i7, this.kt.getBottom(), this.ratingLayout.getMeasuredWidth() + i7, this.kt.getBottom() + this.ratingLayout.getMeasuredHeight());
        this.urlLabel.layout(i7, this.kt.getBottom(), this.urlLabel.getMeasuredWidth() + i7, this.kt.getBottom() + this.urlLabel.getMeasuredHeight());
        this.ks.layout(i7, this.kt.getBottom(), this.ks.getMeasuredWidth() + i7, this.kt.getBottom() + this.ks.getMeasuredHeight());
        this.ku.layout(i7, this.ks.getBottom(), this.ku.getMeasuredWidth() + i7, this.ks.getBottom() + this.ku.getMeasuredHeight());
    }

    private void a(int i, @NonNull View... viewArr) {
        int height = this.iconImageView.getHeight();
        int height2 = getHeight();
        int width = this.ctaButton.getWidth();
        int height3 = this.ctaButton.getHeight();
        int width2 = this.iconImageView.getWidth();
        this.iconImageView.setPivotX(0.0f);
        this.iconImageView.setPivotY(((float) height) / 2.0f);
        this.ctaButton.setPivotX((float) width);
        this.ctaButton.setPivotY(((float) height3) / 2.0f);
        float f = ((float) height2) * 0.3f;
        ArrayList arrayList = new ArrayList();
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.SCALE_X, new float[]{0.7f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.SCALE_Y, new float[]{0.7f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.SCALE_X, new float[]{0.7f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.SCALE_Y, new float[]{0.7f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ks, View.ALPHA, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ku, View.ALPHA, new float[]{0.0f}));
        if (this.ratingLayout.isEnabled()) {
            arrayList.add(ObjectAnimator.ofFloat(this.ratingLayout, View.ALPHA, new float[]{1.0f}));
        }
        arrayList.add(ObjectAnimator.ofFloat(this, View.ALPHA, new float[]{0.6f}));
        float f2 = -(((float) width2) * 0.3f);
        arrayList.add(ObjectAnimator.ofFloat(this.kt, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this.ratingLayout, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this.urlLabel, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this.ks, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this.ku, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this, View.TRANSLATION_Y, new float[]{f}));
        float f3 = (-f) / 2.0f;
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.TRANSLATION_Y, new float[]{f3}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.TRANSLATION_Y, new float[]{f3}));
        for (View ofFloat : viewArr) {
            arrayList.add(ObjectAnimator.ofFloat(ofFloat, View.TRANSLATION_Y, new float[]{f}));
        }
        if (this.ratingLayout.isEnabled()) {
            this.ratingLayout.setVisibility(0);
        }
        if (this.urlLabel.isEnabled()) {
            this.urlLabel.setVisibility(0);
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                gu.this.ku.setVisibility(8);
                gu.this.ks.setVisibility(8);
            }
        });
        animatorSet.playTogether(arrayList);
        animatorSet.setDuration((long) i);
        animatorSet.start();
    }

    private void e(@NonNull View... viewArr) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.SCALE_Y, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.SCALE_X, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.SCALE_Y, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.SCALE_X, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ks, View.ALPHA, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ku, View.ALPHA, new float[]{1.0f}));
        if (this.ratingLayout.isEnabled()) {
            arrayList.add(ObjectAnimator.ofFloat(this.ratingLayout, View.ALPHA, new float[]{0.0f}));
        }
        arrayList.add(ObjectAnimator.ofFloat(this, View.ALPHA, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.kt, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ratingLayout, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.urlLabel, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ks, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ku, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this, View.TRANSLATION_Y, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.TRANSLATION_Y, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.TRANSLATION_Y, new float[]{0.0f}));
        for (View ofFloat : viewArr) {
            arrayList.add(ObjectAnimator.ofFloat(ofFloat, View.TRANSLATION_Y, new float[]{0.0f}));
        }
        if (!TextUtils.isEmpty(this.ku.getText().toString())) {
            this.ku.setVisibility(0);
        }
        this.ks.setVisibility(0);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(arrayList);
        animatorSet.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                if (gu.this.ratingLayout.isEnabled()) {
                    gu.this.ratingLayout.setVisibility(8);
                }
                if (gu.this.urlLabel.isEnabled()) {
                    gu.this.urlLabel.setVisibility(8);
                }
            }
        });
        animatorSet.setDuration(300);
        animatorSet.start();
    }
}
