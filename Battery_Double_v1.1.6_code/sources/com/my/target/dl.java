package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: HttpImageRequest */
public final class dl extends dn<Bitmap> {
    private final boolean ek;

    @NonNull
    public static dl cA() {
        return new dl(false);
    }

    @NonNull
    public static dl cB() {
        return new dl(true);
    }

    private dl(boolean z) {
        this.ek = z;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: d */
    public Bitmap c(@NonNull String str, @NonNull Context context) {
        hs K = hs.K(context);
        if (K == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to open disk cache and get image ");
            sb.append(str);
            ah.a(sb.toString());
            if (this.ek) {
                this.em = false;
                this.c = "image request (caching only) error: can't cache image";
                ah.a(this.c);
                return null;
            }
        } else if (!this.ek) {
            this.en = K.getBitmap(str);
            if (this.en != null) {
                this.cQ = true;
                return (Bitmap) this.en;
            }
        } else if (K.ah(str) != null) {
            ah.a("image request (caching only): image already cached");
            this.cQ = true;
            return null;
        }
        a(K, str);
        return (Bitmap) this.en;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    private void a(@Nullable hs hsVar, @NonNull String str) {
        HttpURLConnection httpURLConnection;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("send image request: ");
            sb.append(str);
            ah.a(sb.toString());
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection.setReadTimeout(10000);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestProperty("connection", "close");
                httpURLConnection.connect();
                this.responseCode = httpURLConnection.getResponseCode();
                if (this.responseCode == 200) {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    if (hsVar != null) {
                        a(hsVar, inputStream, str);
                    } else {
                        a(inputStream);
                    }
                } else {
                    this.em = false;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("image request error: response code ");
                    sb2.append(this.responseCode);
                    this.c = sb2.toString();
                    ah.a(this.c);
                }
            } catch (Throwable th) {
                th = th;
                this.em = false;
                this.c = th.getMessage();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("image request error: ");
                sb3.append(this.c);
                ah.a(sb3.toString());
                if (httpURLConnection != null) {
                }
            }
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            this.em = false;
            this.c = th.getMessage();
            StringBuilder sb32 = new StringBuilder();
            sb32.append("image request error: ");
            sb32.append(this.c);
            ah.a(sb32.toString());
            if (httpURLConnection != null) {
            }
        }
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
    }

    private void a(@NonNull hs hsVar, @NonNull InputStream inputStream, @NonNull String str) {
        File a = hsVar.a(inputStream, str);
        if (a == null) {
            this.em = false;
            this.c = "image request error: can't save image to disk cache";
            ah.a(this.c);
        } else if (!this.ek) {
            this.en = BitmapFactory.decodeFile(a.getAbsolutePath());
        }
    }

    private void a(@NonNull InputStream inputStream) {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 8192);
        this.en = BitmapFactory.decodeStream(bufferedInputStream);
        try {
            bufferedInputStream.close();
        } catch (IOException e) {
            ah.a(e.getMessage());
        }
    }
}
