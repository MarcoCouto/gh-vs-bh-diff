package com.my.target;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

/* compiled from: PromoStyle2ProgressViewImpl */
public class hh extends ProgressBar implements hg {
    @NonNull
    public View eo() {
        return this;
    }

    public hh(Context context) {
        super(context, null, 16842872);
    }

    public void setTimeChanged(float f) {
        if (VERSION.SDK_INT >= 26) {
            setProgress((int) (f * 1000.0f), true);
            return;
        }
        ObjectAnimator ofInt = ObjectAnimator.ofInt(this, NotificationCompat.CATEGORY_PROGRESS, new int[]{getProgress(), (int) (f * 1000.0f)});
        ofInt.setDuration(200);
        ofInt.setInterpolator(new DecelerateInterpolator());
        ofInt.start();
    }

    public void setMaxTime(float f) {
        setMax((int) (f * 1000.0f));
    }

    public void setColor(int i) {
        ClipDrawable clipDrawable = new ClipDrawable(new ColorDrawable(i), GravityCompat.START, 1);
        LayerDrawable layerDrawable = (LayerDrawable) getProgressDrawable();
        layerDrawable.setDrawableByLayerId(16908288, new ColorDrawable(0));
        layerDrawable.setDrawableByLayerId(16908301, clipDrawable);
        setProgressDrawable(layerDrawable);
    }

    public void setVisible(boolean z) {
        setVisibility(z ? 0 : 8);
    }
}
