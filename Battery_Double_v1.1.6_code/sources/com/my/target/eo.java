package com.my.target;

import android.support.annotation.NonNull;
import com.my.target.ew.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialCarouselPresenter */
public class eo {
    /* access modifiers changed from: private */
    @NonNull
    public final gt eM;
    /* access modifiers changed from: private */
    @NonNull
    public final ArrayList<ck> eN = new ArrayList<>();
    /* access modifiers changed from: private */
    public b eO;

    /* compiled from: InterstitialCarouselPresenter */
    class a implements com.my.target.hc.a {
        private a() {
        }

        public void a(@NonNull ck ckVar) {
            if (eo.this.eO != null) {
                eo.this.eO.b(ckVar, null, eo.this.eM.getView().getContext());
            }
        }

        public void b(@NonNull List<ck> list) {
            for (ck ckVar : list) {
                if (!eo.this.eN.contains(ckVar)) {
                    eo.this.eN.add(ckVar);
                    ib.a((List<dh>) ckVar.getStatHolder().N("playbackStarted"), eo.this.eM.getView().getContext());
                }
            }
        }
    }

    public static eo a(@NonNull List<ck> list, @NonNull hc hcVar) {
        return new eo(list, hcVar);
    }

    private eo(@NonNull List<ck> list, @NonNull hc hcVar) {
        int[] numbersOfCurrentShowingCards;
        this.eM = hcVar;
        hcVar.setCarouselListener(new a());
        for (int i : hcVar.getNumbersOfCurrentShowingCards()) {
            if (i < list.size() && i >= 0) {
                ck ckVar = (ck) list.get(i);
                this.eN.add(ckVar);
                ib.a((List<dh>) ckVar.getStatHolder().N("playbackStarted"), hcVar.getView().getContext());
            }
        }
    }

    public void a(b bVar) {
        this.eO = bVar;
    }
}
