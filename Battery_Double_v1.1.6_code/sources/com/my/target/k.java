package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Iterator;

/* compiled from: InstreamAudioAdResultProcessor */
public class k extends d<cx> {
    @NonNull
    public static k h() {
        return new k();
    }

    private k() {
    }

    @Nullable
    public cx a(@NonNull cx cxVar, @NonNull a aVar, @NonNull Context context) {
        Iterator it = cxVar.bN().iterator();
        while (it.hasNext()) {
            ((da) it.next()).bY();
        }
        return cxVar;
    }
}
