package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: MrcStat */
public class de extends dj {
    private float duration = -1.0f;
    private float eb = 0.0f;

    @NonNull
    public static de K(@NonNull String str) {
        return new de("mrcStat", str);
    }

    public float getDuration() {
        return this.duration;
    }

    public void setDuration(float f) {
        this.duration = f;
    }

    public float co() {
        return this.eb;
    }

    public void h(float f) {
        this.eb = f;
    }

    protected de(@NonNull String str, @NonNull String str2) {
        super(str, str2);
    }
}
