package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

/* compiled from: HttpLogRequest */
public class dm extends dn<Void> {
    @Nullable
    private String el;

    @NonNull
    public static dm cC() {
        return new dm();
    }

    @NonNull
    public dm O(@Nullable String str) {
        this.el = str;
        return this;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0094, code lost:
        if (r6 != null) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0096, code lost:
        r6.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0099, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x006c, code lost:
        if (r6 != null) goto L_0x0096;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009d  */
    /* renamed from: e */
    public Void c(@NonNull String str, @NonNull Context context) {
        HttpURLConnection httpURLConnection;
        if (this.el == null) {
            ah.a("can't send log request: body is null");
            this.em = false;
            return null;
        }
        ah.a("send log request");
        try {
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.setReadTimeout(5000);
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                httpURLConnection.setRequestProperty("Content-Type", "text/html; charset=utf-8");
                httpURLConnection.setRequestProperty("connection", "close");
                httpURLConnection.connect();
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, Charset.forName("UTF-8")));
                bufferedWriter.write(this.el);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                httpURLConnection.getInputStream().close();
            } catch (Throwable th) {
                th = th;
                try {
                    this.em = false;
                    this.c = th.getMessage();
                    StringBuilder sb = new StringBuilder();
                    sb.append("log request error: ");
                    sb.append(this.c);
                    ah.a(sb.toString());
                } catch (Throwable th2) {
                    th = th2;
                    if (httpURLConnection != null) {
                    }
                    throw th;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            httpURLConnection = null;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }
}
