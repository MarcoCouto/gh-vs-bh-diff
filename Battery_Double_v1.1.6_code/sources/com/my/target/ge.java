package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.my.target.common.models.ImageData;

@SuppressLint({"AppCompatCustomView"})
/* compiled from: SmartImageView */
public class ge extends ImageView {
    @Nullable
    private Bitmap bitmap;
    private int maxHeight;
    private int maxWidth;
    private int placeholderHeight;
    private int placeholderWidth;

    public ge(Context context) {
        super(context);
        dc();
    }

    public void setImageData(@Nullable ImageData imageData) {
        if (imageData == null) {
            this.placeholderHeight = 0;
            this.placeholderWidth = 0;
            setImageBitmap(null);
            return;
        }
        this.placeholderHeight = imageData.getHeight();
        this.placeholderWidth = imageData.getWidth();
        setImageBitmap(imageData.getBitmap());
    }

    public void setImageBitmap(@Nullable Bitmap bitmap2) {
        this.bitmap = bitmap2;
        super.setImageBitmap(bitmap2);
    }

    public void b(@Nullable Bitmap bitmap2, boolean z) {
        if (z) {
            setAlpha(0.0f);
            setImageBitmap(bitmap2);
            animate().alpha(1.0f).setDuration(300);
            return;
        }
        setImageBitmap(bitmap2);
    }

    public void setPlaceholderHeight(int i) {
        this.placeholderHeight = i;
    }

    public void setPlaceholderWidth(int i) {
        this.placeholderWidth = i;
    }

    public void setMaxWidth(int i) {
        this.maxWidth = i;
    }

    public void setMaxHeight(int i) {
        this.maxHeight = i;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        if (this.placeholderHeight != 0 && this.placeholderWidth != 0) {
            i4 = this.placeholderWidth;
            i3 = this.placeholderHeight;
        } else if (this.bitmap != null) {
            i4 = this.bitmap.getWidth();
            i3 = this.bitmap.getHeight();
        } else {
            setMeasuredDimension(0, 0);
            return;
        }
        if (i4 <= 0 || i3 <= 0) {
            super.onMeasure(i, i2);
            return;
        }
        float f = (float) i4;
        float f2 = (float) i3;
        float f3 = f / f2;
        if (this.maxHeight > 0) {
            size2 = Math.min(this.maxHeight, size2);
        }
        if (this.maxWidth > 0) {
            size = Math.min(this.maxWidth, size);
        }
        if (mode == 1073741824 && mode2 == 1073741824) {
            setMeasuredDimension(size, size2);
            return;
        }
        if (!(mode == 0 && mode2 == 0)) {
            if (mode == 0) {
                i5 = (int) (((float) size2) * f3);
            } else {
                if (mode2 == 0) {
                    i6 = (int) (((float) size) / f3);
                } else {
                    float f4 = (float) size;
                    float f5 = f4 / f;
                    float f6 = (float) size2;
                    if (Math.min(f5, f6 / f2) != f5 || f3 <= 0.0f) {
                        i5 = (int) (f6 * f3);
                    } else {
                        i6 = (int) (f4 / f3);
                    }
                }
                i4 = size;
            }
            i3 = size2;
        }
        setMeasuredDimension(i4, i3);
    }

    private void dc() {
        setScaleType(ScaleType.CENTER_INSIDE);
        setAdjustViewBounds(true);
    }
}
