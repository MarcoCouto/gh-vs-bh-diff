package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.cv;

/* compiled from: AdResultProcessor */
public abstract class d<T extends cv> {
    @Nullable
    public abstract T a(@NonNull T t, @NonNull a aVar, @NonNull Context context);
}
