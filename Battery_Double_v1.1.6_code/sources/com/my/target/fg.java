package com.my.target;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import java.util.Map;

/* compiled from: FingerprintDataProvider */
public final class fg extends ff {
    @NonNull
    private static final fg hA = new fg();
    @NonNull
    private final fd hB = new fd();
    @NonNull
    private final fe hC = new fe();
    @NonNull
    private final fh hD = new fh();
    @NonNull
    private final fj hE = new fj();
    @NonNull
    private final fi hF = new fi();

    @NonNull
    public static fg dM() {
        return hA;
    }

    @NonNull
    public fe dN() {
        return this.hC;
    }

    private fg() {
    }

    @WorkerThread
    public synchronized void collectData(@NonNull Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            ah.a("FingerprintDataProvider: You must not call collectData method from main thread");
            return;
        }
        removeAll();
        this.hB.collectData(context);
        this.hC.collectData(context);
        this.hD.collectData(context);
        this.hE.collectData(context);
        this.hF.collectData(context);
        Map map = getMap();
        this.hB.putDataTo(map);
        this.hC.putDataTo(map);
        this.hD.putDataTo(map);
        this.hE.putDataTo(map);
        this.hF.putDataTo(map);
    }
}
