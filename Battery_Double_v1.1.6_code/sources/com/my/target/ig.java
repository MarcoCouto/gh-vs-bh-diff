package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;

/* compiled from: AdVideoPlayer */
public interface ig {

    /* compiled from: AdVideoPlayer */
    public interface a {
        void A();

        void B();

        void C();

        void a(float f, float f2);

        void d(String str);

        void e(float f);

        void x();

        void y();

        void z();
    }

    void L();

    void M();

    void a(@NonNull VideoData videoData, @NonNull Context context);

    void a(@Nullable fs fsVar);

    void a(@Nullable a aVar);

    void cU();

    void de();

    void destroy();

    @Nullable
    VideoData eH();

    long getPosition();

    boolean isMuted();

    boolean isPaused();

    boolean isPlaying();

    boolean isStarted();

    void pause();

    void resume();

    void seekTo(long j);

    void setVolume(float f);

    void stop();
}
