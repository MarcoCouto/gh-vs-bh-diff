package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.TextView;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CarouselRecyclerView */
public class hb extends RecyclerView {
    private final OnClickListener cardClickListener;
    /* access modifiers changed from: private */
    @Nullable
    public List<ck> cards;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.hc.a lA;
    /* access modifiers changed from: private */
    public boolean lB;
    @NonNull
    private final ha lx;
    @NonNull
    private final OnClickListener ly;
    @NonNull
    private final LinearSnapHelper lz;
    /* access modifiers changed from: private */
    public boolean moving;

    /* compiled from: CarouselRecyclerView */
    static class a extends Adapter<b> {
        @Nullable
        OnClickListener cardClickListener;
        @NonNull
        final Context context;
        @NonNull
        final List<ck> interstitialAdCards;
        private final boolean ki;
        OnClickListener ly;

        a(@NonNull List<ck> list, @NonNull Context context2) {
            this.interstitialAdCards = list;
            this.context = context2;
            this.ki = (context2.getResources().getConfiguration().screenLayout & 15) >= 3;
        }

        @NonNull
        /* renamed from: b */
        public b onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new b(new gz(this.ki, this.context));
        }

        /* renamed from: a */
        public void onBindViewHolder(@NonNull b bVar, int i) {
            gz el = bVar.el();
            ck ckVar = (ck) getInterstitialAdCards().get(i);
            a(ckVar, el);
            el.a(this.cardClickListener, ckVar.getClickArea());
            el.getCtaButtonView().setOnClickListener(this.ly);
        }

        /* renamed from: a */
        public void onViewRecycled(@NonNull b bVar) {
            gz el = bVar.el();
            el.a(null, null);
            el.getCtaButtonView().setOnClickListener(null);
        }

        public int getItemCount() {
            return getInterstitialAdCards().size();
        }

        public int getItemViewType(int i) {
            if (i == 0) {
                return 1;
            }
            return i == getItemCount() - 1 ? 2 : 0;
        }

        /* access modifiers changed from: 0000 */
        public void c(OnClickListener onClickListener) {
            this.ly = onClickListener;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public List<ck> getInterstitialAdCards() {
            return this.interstitialAdCards;
        }

        /* access modifiers changed from: 0000 */
        public void b(@Nullable OnClickListener onClickListener) {
            this.cardClickListener = onClickListener;
        }

        private void a(@NonNull ck ckVar, @NonNull gz gzVar) {
            ImageData image = ckVar.getImage();
            if (image != null) {
                ge smartImageView = gzVar.getSmartImageView();
                smartImageView.setPlaceholderWidth(image.getWidth());
                smartImageView.setPlaceholderHeight(image.getHeight());
                hu.a(image, (ImageView) smartImageView);
            }
            gzVar.getTitleTextView().setText(ckVar.getTitle());
            gzVar.getDescriptionTextView().setText(ckVar.getDescription());
            gzVar.getCtaButtonView().setText(ckVar.getCtaText());
            TextView domainTextView = gzVar.getDomainTextView();
            String domain = ckVar.getDomain();
            gf ratingView = gzVar.getRatingView();
            if ("web".equals(ckVar.getNavigationType())) {
                ratingView.setVisibility(8);
                domainTextView.setVisibility(0);
                domainTextView.setText(domain);
                return;
            }
            domainTextView.setVisibility(8);
            float rating = ckVar.getRating();
            if (rating > 0.0f) {
                ratingView.setVisibility(0);
                ratingView.setRating(rating);
                return;
            }
            ratingView.setVisibility(8);
        }
    }

    /* compiled from: CarouselRecyclerView */
    static class b extends ViewHolder {
        private final gz lD;

        b(gz gzVar) {
            super(gzVar);
            this.lD = gzVar;
        }

        /* access modifiers changed from: 0000 */
        public gz el() {
            return this.lD;
        }
    }

    public hb(Context context) {
        this(context, null);
    }

    public hb(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public hb(Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.cardClickListener = new OnClickListener() {
            public void onClick(View view) {
                if (!hb.this.moving) {
                    View findContainingItemView = hb.this.getCardLayoutManager().findContainingItemView(view);
                    if (findContainingItemView != null) {
                        if (!hb.this.getCardLayoutManager().g(findContainingItemView) && !hb.this.lB) {
                            hb.this.h(findContainingItemView);
                        } else if (!(!view.isClickable() || hb.this.lA == null || hb.this.cards == null)) {
                            hb.this.lA.a((ck) hb.this.cards.get(hb.this.getCardLayoutManager().getPosition(findContainingItemView)));
                        }
                    }
                }
            }
        };
        this.ly = new OnClickListener() {
            public void onClick(View view) {
                ViewParent parent = view.getParent();
                while (parent != null && !(parent instanceof gz)) {
                    parent = parent.getParent();
                }
                if (hb.this.lA != null && hb.this.cards != null && parent != null) {
                    hb.this.lA.a((ck) hb.this.cards.get(hb.this.getCardLayoutManager().getPosition((View) parent)));
                }
            }
        };
        setOverScrollMode(2);
        this.lx = new ha(context);
        this.lz = new LinearSnapHelper();
        this.lz.attachToRecyclerView(this);
    }

    @VisibleForTesting
    @NonNull
    public LinearSnapHelper getSnapHelper() {
        return this.lz;
    }

    public void d(List<ck> list) {
        a aVar = new a(list, getContext());
        this.cards = list;
        aVar.b(this.cardClickListener);
        aVar.c(this.ly);
        setCardLayoutManager(this.lx);
        setAdapter(aVar);
    }

    public void setCarouselListener(@Nullable com.my.target.hc.a aVar) {
        this.lA = aVar;
    }

    public void setSideSlidesMargins(int i) {
        getCardLayoutManager().H(i);
    }

    public void onScrollStateChanged(int i) {
        super.onScrollStateChanged(i);
        this.moving = i != 0;
        if (!this.moving) {
            checkCardChanged();
        }
    }

    public void C(boolean z) {
        if (z) {
            this.lz.attachToRecyclerView(this);
        } else {
            this.lz.attachToRecyclerView(null);
        }
    }

    @VisibleForTesting(otherwise = 3)
    public ha getCardLayoutManager() {
        return this.lx;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (i3 > i4) {
            this.lB = true;
        }
        super.onLayout(z, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public void h(@NonNull View view) {
        int[] calculateDistanceToFinalSnap = this.lz.calculateDistanceToFinalSnap(getCardLayoutManager(), view);
        if (calculateDistanceToFinalSnap != null) {
            smoothScrollBy(calculateDistanceToFinalSnap[0], 0);
        }
    }

    private void setCardLayoutManager(@NonNull ha haVar) {
        haVar.a(new com.my.target.ha.a() {
            public void dR() {
                hb.this.checkCardChanged();
            }
        });
        super.setLayoutManager(haVar);
    }

    @NonNull
    private List<ck> getVisibleCards() {
        ArrayList arrayList = new ArrayList();
        if (this.cards == null) {
            return arrayList;
        }
        int findFirstCompletelyVisibleItemPosition = getCardLayoutManager().findFirstCompletelyVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = getCardLayoutManager().findLastCompletelyVisibleItemPosition();
        if (findFirstCompletelyVisibleItemPosition > findLastCompletelyVisibleItemPosition || findFirstCompletelyVisibleItemPosition < 0 || findLastCompletelyVisibleItemPosition >= this.cards.size()) {
            return arrayList;
        }
        while (findFirstCompletelyVisibleItemPosition <= findLastCompletelyVisibleItemPosition) {
            arrayList.add(this.cards.get(findFirstCompletelyVisibleItemPosition));
            findFirstCompletelyVisibleItemPosition++;
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void checkCardChanged() {
        if (this.lA != null) {
            this.lA.b(getVisibleCards());
        }
    }
}
