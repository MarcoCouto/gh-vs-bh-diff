package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: InterstitialAdHtmlBanner */
public class cl extends cj {
    @Nullable
    private String source;

    @NonNull
    public static cl newBanner() {
        return new cl();
    }

    @NonNull
    public static cl fromCompanion(@NonNull ci ciVar) {
        cl newBanner = newBanner();
        newBanner.setId(ciVar.getId());
        newBanner.setSource(ciVar.getHtmlResource());
        newBanner.getStatHolder().a(ciVar.getStatHolder(), 0.0f);
        newBanner.trackingLink = ciVar.trackingLink;
        return newBanner;
    }

    public void setSource(@Nullable String str) {
        this.source = str;
    }

    @Nullable
    public String getSource() {
        return this.source;
    }

    private cl() {
    }
}
