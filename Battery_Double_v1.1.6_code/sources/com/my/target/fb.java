package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: StandardViewPresenter */
public interface fb {

    /* compiled from: StandardViewPresenter */
    public interface a {
        void a(@NonNull ch chVar);

        void a(@NonNull ch chVar, @Nullable String str);
    }

    void a(@Nullable a aVar);

    void destroy();

    void pause();

    void resume();

    void start();

    void stop();
}
