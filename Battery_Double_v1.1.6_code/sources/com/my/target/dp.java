package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: HttpVideoRequest */
public final class dp extends dn<String> {
    @NonNull
    public static dp cG() {
        return new dp();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b8  */
    @Nullable
    /* renamed from: b */
    public String c(@NonNull String str, @NonNull Context context) {
        HttpURLConnection httpURLConnection;
        hs K = hs.K(context);
        if (K != null) {
            this.en = K.ag(str);
            if (this.en != null) {
                this.cQ = true;
                return (String) this.en;
            }
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("send video request: ");
                sb.append(str);
                ah.a(sb.toString());
                httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                try {
                    httpURLConnection.setReadTimeout(10000);
                    httpURLConnection.setConnectTimeout(10000);
                    httpURLConnection.setInstanceFollowRedirects(true);
                    httpURLConnection.setRequestProperty("connection", "close");
                    httpURLConnection.connect();
                    this.responseCode = httpURLConnection.getResponseCode();
                    if (this.responseCode == 200) {
                        File b = K.b(httpURLConnection.getInputStream(), str);
                        if (b != null) {
                            this.en = b.getAbsolutePath();
                        } else {
                            this.em = false;
                            this.c = "video request error: can't save video to disk cache";
                            ah.a(this.c);
                        }
                    } else {
                        this.em = false;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("video request error: response code ");
                        sb2.append(this.responseCode);
                        this.c = sb2.toString();
                        ah.a(this.c);
                    }
                } catch (Throwable th) {
                    th = th;
                    this.em = false;
                    this.c = th.getMessage();
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("video request error: ");
                    sb3.append(this.c);
                    ah.a(sb3.toString());
                    if (httpURLConnection != null) {
                    }
                    return (String) this.en;
                }
            } catch (Throwable th2) {
                th = th2;
                httpURLConnection = null;
                this.em = false;
                this.c = th.getMessage();
                StringBuilder sb32 = new StringBuilder();
                sb32.append("video request error: ");
                sb32.append(this.c);
                ah.a(sb32.toString());
                if (httpURLConnection != null) {
                }
                return (String) this.en;
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return (String) this.en;
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append("unable to open disk cache and load/save video ");
        sb4.append(str);
        ah.a(sb4.toString());
        this.em = false;
        return null;
    }
}
