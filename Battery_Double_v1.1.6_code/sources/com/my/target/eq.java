package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.View.OnClickListener;
import com.my.target.eu.a;

/* compiled from: InterstitialImagePresenter */
public class eq implements eu {
    @Nullable
    a eY;
    @NonNull
    private final gb eZ;

    public void destroy() {
    }

    public void pause() {
    }

    public void resume() {
    }

    public void stop() {
    }

    public static eq t(Context context) {
        return new eq(new gb(context));
    }

    @VisibleForTesting
    eq(@NonNull gb gbVar) {
        this.eZ = gbVar;
    }

    @NonNull
    public View cT() {
        return this.eZ;
    }

    public void e(@NonNull final cm cmVar) {
        this.eZ.a(cmVar.getOptimalLandscapeImage(), cmVar.getOptimalPortraitImage(), cmVar.getCloseIcon());
        this.eZ.setAgeRestrictions(cmVar.getAgeRestrictions());
        this.eZ.getImageView().setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (eq.this.eY != null) {
                    eq.this.eY.b(cmVar, null, view.getContext());
                }
            }
        });
        this.eZ.getCloseButton().setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (eq.this.eY != null) {
                    eq.this.eY.aj();
                }
            }
        });
        if (this.eY != null) {
            this.eY.a(cmVar, this.eZ.getContext());
        }
    }

    public void a(@Nullable a aVar) {
        this.eY = aVar;
    }
}
