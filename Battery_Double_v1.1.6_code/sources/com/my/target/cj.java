package com.my.target;

import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;

/* compiled from: InterstitialAdBanner */
public abstract class cj extends ch {
    private boolean allowBackButton;
    private boolean allowClose;
    private float allowCloseDelay;
    @Nullable
    private ImageData closeIcon;

    cj() {
        this.allowClose = true;
        this.allowBackButton = true;
        this.clickArea = ca.db;
    }

    @Nullable
    public ImageData getCloseIcon() {
        return this.closeIcon;
    }

    public void setCloseIcon(@Nullable ImageData imageData) {
        this.closeIcon = imageData;
    }

    public float getAllowCloseDelay() {
        return this.allowCloseDelay;
    }

    public void setAllowCloseDelay(float f) {
        this.allowCloseDelay = f;
    }

    public void setAllowClose(boolean z) {
        this.allowClose = z;
    }

    public boolean isAllowClose() {
        return this.allowClose;
    }

    public boolean isAllowBackButton() {
        return this.allowBackButton;
    }

    public void setAllowBackButton(boolean z) {
        this.allowBackButton = z;
    }
}
