package com.my.target;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import com.my.target.nativeads.views.MediaAdView;

/* compiled from: InterstitialImageView */
public class gb extends RelativeLayout {
    private static final int iz = ic.eG();
    @NonNull
    private final fu ageRestrictionLabel;
    @NonNull
    private final fz eR;
    @NonNull
    private final LayoutParams iA = new LayoutParams(-2, -2);
    @Nullable
    private ImageData iB;
    @Nullable
    private ImageData iC;
    @NonNull
    private final ge imageView;
    @NonNull
    private final ic uiUtils;

    public gb(Context context) {
        super(context);
        setBackgroundColor(0);
        this.uiUtils = ic.P(context);
        this.imageView = new ge(context);
        this.imageView.setId(iz);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.imageView.setLayoutParams(layoutParams);
        addView(this.imageView);
        this.eR = new fz(context);
        this.eR.a(fl.B((int) TypedValue.applyDimension(1, 28.0f, context.getResources().getDisplayMetrics())), false);
        this.iA.addRule(7, iz);
        this.iA.addRule(6, iz);
        this.eR.setLayoutParams(this.iA);
        this.ageRestrictionLabel = new fu(context);
        addView(this.eR);
        addView(this.ageRestrictionLabel);
    }

    @NonNull
    public fz getCloseButton() {
        return this.eR;
    }

    @NonNull
    public ImageView getImageView() {
        return this.imageView;
    }

    public void a(@Nullable ImageData imageData, @Nullable ImageData imageData2, @Nullable ImageData imageData3) {
        this.iC = imageData;
        this.iB = imageData2;
        Bitmap bitmap = imageData3 != null ? imageData3.getBitmap() : null;
        if (bitmap != null) {
            this.eR.a(bitmap, true);
            LayoutParams layoutParams = this.iA;
            int i = -this.eR.getMeasuredWidth();
            this.iA.leftMargin = i;
            layoutParams.bottomMargin = i;
        }
        dU();
    }

    public void setAgeRestrictions(@NonNull String str) {
        if (!TextUtils.isEmpty(str)) {
            this.ageRestrictionLabel.f(1, -7829368);
            this.ageRestrictionLabel.setPadding(this.uiUtils.M(2), 0, 0, 0);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            int M = this.uiUtils.M(10);
            layoutParams.topMargin = M;
            layoutParams.leftMargin = M;
            layoutParams.addRule(5, iz);
            layoutParams.addRule(6, iz);
            this.ageRestrictionLabel.setLayoutParams(layoutParams);
            this.ageRestrictionLabel.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
            this.ageRestrictionLabel.a(1, MediaAdView.COLOR_PLACEHOLDER_GRAY, this.uiUtils.M(3));
            this.ageRestrictionLabel.setBackgroundColor(1711276032);
            this.ageRestrictionLabel.setText(str);
            return;
        }
        this.ageRestrictionLabel.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        dU();
    }

    private void dU() {
        WindowManager windowManager = (WindowManager) getContext().getApplicationContext().getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            ImageData imageData = ((float) defaultDisplay.getWidth()) / ((float) defaultDisplay.getHeight()) > 1.0f ? this.iC : this.iB;
            if (imageData == null) {
                imageData = this.iC != null ? this.iC : this.iB;
            }
            if (imageData != null) {
                this.imageView.setImageData(imageData);
            }
        }
    }
}
