package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0056b;

/* compiled from: InterstitialAdFactory */
public final class o extends b<cy> {
    @Nullable
    private final cy section;

    /* compiled from: InterstitialAdFactory */
    static class a implements com.my.target.b.a<cy> {
        public boolean a() {
            return true;
        }

        private a() {
        }

        @NonNull
        public c<cy> b() {
            return p.f();
        }

        @Nullable
        public d<cy> c() {
            return q.i();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    public interface b extends C0056b {
    }

    @NonNull
    public static b<cy> a(@NonNull a aVar) {
        return new o(aVar, null);
    }

    @NonNull
    public static b<cy> a(@NonNull cy cyVar, @NonNull a aVar) {
        return new o(aVar, cyVar);
    }

    private o(@NonNull a aVar, @Nullable cy cyVar) {
        super(new a(), aVar);
        this.section = cyVar;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: e */
    public cy b(@NonNull Context context) {
        if (this.section != null) {
            return (cy) a(this.section, context);
        }
        return (cy) super.b(context);
    }
}
