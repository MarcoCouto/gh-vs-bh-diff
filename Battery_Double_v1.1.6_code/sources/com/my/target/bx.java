package com.my.target;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

/* compiled from: MraidScreenMetrics */
public class bx {
    @NonNull
    private final Rect cA;
    @NonNull
    private final Rect cB;
    @NonNull
    private final Rect cC;
    @NonNull
    private final Rect cD;
    @NonNull
    private final Rect cE;
    @NonNull
    private final Rect cF;
    @NonNull
    private final Rect cG;
    @NonNull
    private final Rect cz;
    @NonNull
    private final ic uiUtils;

    public static bx p(@NonNull Context context) {
        return new bx(context);
    }

    @VisibleForTesting
    bx(@NonNull ic icVar) {
        this.uiUtils = icVar;
        this.cz = new Rect();
        this.cA = new Rect();
        this.cB = new Rect();
        this.cC = new Rect();
        this.cD = new Rect();
        this.cE = new Rect();
        this.cF = new Rect();
        this.cG = new Rect();
    }

    private bx(@NonNull Context context) {
        this(ic.P(context));
    }

    public void a(int i, int i2) {
        this.cz.set(0, 0, i, i2);
        a(this.cz, this.cA);
    }

    public void a(int i, int i2, int i3, int i4) {
        this.cD.set(i, i2, i3, i4);
        a(this.cD, this.cE);
    }

    public void b(int i, int i2, int i3, int i4) {
        this.cB.set(i, i2, i3, i4);
        a(this.cB, this.cC);
    }

    public void c(int i, int i2, int i3, int i4) {
        this.cF.set(i, i2, i3, i4);
        a(this.cF, this.cG);
    }

    @NonNull
    public Rect aQ() {
        return this.cC;
    }

    @NonNull
    public Rect aR() {
        return this.cE;
    }

    @NonNull
    public Rect aS() {
        return this.cG;
    }

    @NonNull
    public Rect aT() {
        return this.cA;
    }

    private void a(@NonNull Rect rect, @NonNull Rect rect2) {
        rect2.set(this.uiUtils.O(rect.left), this.uiUtils.O(rect.top), this.uiUtils.O(rect.right), this.uiUtils.O(rect.bottom));
    }
}
