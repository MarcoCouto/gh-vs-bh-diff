package com.my.target;

import android.support.annotation.NonNull;
import com.ironsource.sdk.precache.DownloadManager;
import java.util.Iterator;
import org.json.JSONObject;

/* compiled from: InstreamAudioAdSectionParser */
public class dz {
    @NonNull
    public static dz cM() {
        return new dz();
    }

    private dz() {
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull cx cxVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
        if (optJSONObject != null) {
            Iterator it = cxVar.bN().iterator();
            while (it.hasNext()) {
                da daVar = (da) it.next();
                JSONObject optJSONObject2 = optJSONObject.optJSONObject(daVar.getName());
                if (optJSONObject2 != null) {
                    a(optJSONObject2, daVar);
                }
            }
        }
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull da daVar) {
        daVar.u(jSONObject.optInt("connectionTimeout", daVar.bU()));
        int optInt = jSONObject.optInt("maxBannersShow", daVar.bV());
        if (optInt == 0) {
            optInt = -1;
        }
        daVar.v(optInt);
    }
}
