package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.aj.b;
import com.my.target.b.C0056b;
import com.my.target.common.models.AudioData;
import com.my.target.instreamads.InstreamAudioAd;
import com.my.target.instreamads.InstreamAudioAd.InstreamAdCompanionBanner;
import com.my.target.instreamads.InstreamAudioAd.InstreamAudioAdBanner;
import com.my.target.instreamads.InstreamAudioAd.InstreamAudioAdListener;
import com.my.target.instreamads.InstreamAudioAdPlayer;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InstreamAudioAdEngine */
public class ar {
    /* access modifiers changed from: private */
    @Nullable
    public da<AudioData> aC;
    /* access modifiers changed from: private */
    @Nullable
    public co<AudioData> aD;
    @Nullable
    private List<co<AudioData>> aF;
    private float aG;
    private int aH;
    private int aI;
    /* access modifiers changed from: private */
    public boolean aJ;
    /* access modifiers changed from: private */
    @NonNull
    public final InstreamAudioAd aN;
    @NonNull
    private final cx aO;
    /* access modifiers changed from: private */
    @NonNull
    public final aj aP;
    /* access modifiers changed from: private */
    @Nullable
    public InstreamAudioAdBanner aQ;
    @Nullable
    private List<InstreamAdCompanionBanner> aR;
    @NonNull
    private final a adConfig;
    @NonNull
    private final hr clickHandler;
    private int loadingTimeoutSeconds;
    @NonNull
    private float[] midpoints = new float[0];

    /* compiled from: InstreamAudioAdEngine */
    class a implements b {
        private a() {
        }

        public void b(@NonNull co coVar) {
            if (ar.this.aC != null && ar.this.aD == coVar && ar.this.aQ != null) {
                if (!ar.this.aJ) {
                    ar.this.aJ = true;
                    Context context = ar.this.aP.getContext();
                    if (context == null) {
                        ah.a("can't send stat: context is null");
                    } else {
                        ib.a((List<dh>) ar.this.aC.w(Events.AD_IMPRESSION), context);
                    }
                }
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onBannerStart(ar.this.aN, ar.this.aQ);
                }
            }
        }

        public void c(@NonNull co coVar) {
            if (ar.this.aC != null && ar.this.aD == coVar && ar.this.aQ != null) {
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onBannerComplete(ar.this.aN, ar.this.aQ);
                }
            }
        }

        public void d(@NonNull co coVar) {
            if (ar.this.aC != null && ar.this.aD == coVar && ar.this.aQ != null) {
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onBannerComplete(ar.this.aN, ar.this.aQ);
                }
                ar.this.ag();
            }
        }

        public void a(float f, float f2, @NonNull co coVar) {
            if (ar.this.aC != null && ar.this.aD == coVar && ar.this.aQ != null) {
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onBannerTimeLeftChange(f, f2, ar.this.aN);
                }
            }
        }

        public void a(@NonNull String str, @NonNull co coVar) {
            if (ar.this.aC != null && ar.this.aD == coVar) {
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onError(str, ar.this.aN);
                }
                ar.this.ag();
            }
        }
    }

    @NonNull
    public static ar a(@NonNull InstreamAudioAd instreamAudioAd, @NonNull cx cxVar, @NonNull a aVar) {
        return new ar(instreamAudioAd, cxVar, aVar);
    }

    private ar(@NonNull InstreamAudioAd instreamAudioAd, @NonNull cx cxVar, @NonNull a aVar) {
        this.aN = instreamAudioAd;
        this.aO = cxVar;
        this.adConfig = aVar;
        this.aP = aj.p();
        this.aP.a((b) new a());
        this.clickHandler = hr.et();
    }

    public void setVolume(float f) {
        this.aP.setVolume(f);
    }

    @Nullable
    public InstreamAudioAdPlayer getPlayer() {
        return this.aP.getPlayer();
    }

    public void setPlayer(@Nullable InstreamAudioAdPlayer instreamAudioAdPlayer) {
        this.aP.setPlayer(instreamAudioAdPlayer);
    }

    public void a(@NonNull float[] fArr) {
        this.midpoints = fArr;
    }

    public void e(int i) {
        this.loadingTimeoutSeconds = i;
    }

    public void start(@NonNull String str) {
        stop();
        this.aC = this.aO.y(str);
        if (this.aC != null) {
            this.aP.setConnectionTimeout(this.aC.bU());
            this.aJ = false;
            this.aI = this.aC.bV();
            this.aH = -1;
            this.aF = this.aC.bT();
            ag();
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("no section with name ");
        sb.append(str);
        ah.a(sb.toString());
    }

    public void startMidroll(float f) {
        boolean z;
        stop();
        float[] fArr = this.midpoints;
        int length = fArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (Float.compare(fArr[i], f) == 0) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            this.aC = this.aO.y(BreakId.MIDROLL);
            if (this.aC != null) {
                this.aP.setConnectionTimeout(this.aC.bU());
                this.aJ = false;
                this.aI = this.aC.bV();
                this.aH = -1;
                this.aG = f;
                a(this.aC, f);
                return;
            }
            return;
        }
        ah.a("attempt to start wrong midpoint, use one of InstreamAd.getMidPoints()");
    }

    public void pause() {
        if (this.aC != null) {
            this.aP.pause();
        }
    }

    public void resume() {
        if (this.aC != null) {
            this.aP.resume();
        }
    }

    public void stop() {
        if (this.aC != null) {
            this.aP.stop();
            a(this.aC);
        }
    }

    public void skip() {
        a((co) this.aD, "closedByUser");
        stop();
    }

    public void skipBanner() {
        a((co) this.aD, "closedByUser");
        this.aP.stop();
        ag();
    }

    public void handleCompanionClick(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner) {
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't handle click: context is null");
            return;
        }
        ci a2 = a(instreamAdCompanionBanner);
        if (a2 == null) {
            ah.a("can't handle click: companion banner not found");
        } else {
            this.clickHandler.b(a2, context);
        }
    }

    public void handleCompanionClick(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner, @NonNull Context context) {
        ci a2 = a(instreamAdCompanionBanner);
        if (a2 == null) {
            ah.a("can't handle click: companion banner not found");
        } else {
            this.clickHandler.b(a2, context);
        }
    }

    public void handleCompanionShow(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner) {
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't handle show: context is null");
            return;
        }
        ci a2 = a(instreamAdCompanionBanner);
        if (a2 == null) {
            ah.a("can't handle show: companion banner not found");
        } else {
            ib.a((List<dh>) a2.getStatHolder().N("playbackStarted"), context);
        }
    }

    public void destroy() {
        this.aP.destroy();
    }

    public float getVolume() {
        return this.aP.getVolume();
    }

    private void a(@NonNull da<AudioData> daVar, float f) {
        ArrayList arrayList = new ArrayList();
        for (co coVar : daVar.bT()) {
            if (coVar.getPoint() == f) {
                arrayList.add(coVar);
            }
        }
        int size = arrayList.size();
        if (size <= 0 || this.aH >= size - 1) {
            ArrayList g = daVar.g(f);
            if (g.size() > 0) {
                a(g, daVar, f);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("There is no one midpoint service for point: ");
            sb.append(f);
            ah.a(sb.toString());
            b(daVar, f);
            return;
        }
        this.aF = arrayList;
        ag();
    }

    /* access modifiers changed from: private */
    public void ag() {
        if (this.aC != null) {
            if (this.aI == 0 || this.aF == null) {
                b(this.aC, this.aG);
                return;
            }
            int i = this.aH + 1;
            if (i < this.aF.size()) {
                this.aH = i;
                co<AudioData> coVar = (co) this.aF.get(i);
                if ("statistics".equals(coVar.getType())) {
                    a((co) coVar, "playbackStarted");
                    ag();
                } else {
                    if (this.aI > 0) {
                        this.aI--;
                    }
                    this.aD = coVar;
                    this.aQ = InstreamAudioAdBanner.newBanner(coVar);
                    this.aR = new ArrayList(this.aQ.companionBanners);
                    this.aP.a(coVar);
                }
            } else {
                b(this.aC, this.aG);
            }
        }
    }

    private void b(@NonNull da<AudioData> daVar, float f) {
        bz bX = daVar.bX();
        if (bX == null) {
            a(daVar);
        } else if (BreakId.MIDROLL.equals(daVar.getName())) {
            bX.o(true);
            bX.setPoint(f);
            ArrayList arrayList = new ArrayList();
            arrayList.add(bX);
            StringBuilder sb = new StringBuilder();
            sb.append("using doAfter service for point: ");
            sb.append(f);
            ah.a(sb.toString());
            a(arrayList, daVar, f);
        } else {
            a(bX, daVar);
        }
    }

    private void a(@NonNull da<AudioData> daVar) {
        if (daVar == this.aC) {
            if (BreakId.MIDROLL.equals(daVar.getName())) {
                this.aC.v(this.aI);
            }
            this.aC = null;
            this.aJ = false;
            this.aD = null;
            this.aQ = null;
            this.aH = -1;
            InstreamAudioAdListener listener = this.aN.getListener();
            if (listener != null) {
                listener.onComplete(daVar.getName(), this.aN);
            }
        }
    }

    private void a(@NonNull bz bzVar, @NonNull final da<AudioData> daVar) {
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't load doAfter service: context is null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("loading doAfter service: ");
        sb.append(bzVar.getUrl());
        ah.a(sb.toString());
        i.a(bzVar, this.adConfig, this.loadingTimeoutSeconds).a((C0056b<T>) new i.b() {
            public void onResult(@Nullable cx cxVar, @Nullable String str) {
                ar.this.a(daVar, cxVar, str);
            }
        }).a(context);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull da<AudioData> daVar, @Nullable cx cxVar, @Nullable String str) {
        if (cxVar == null) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("loading doAfter service failed: ");
                sb.append(str);
                ah.a(sb.toString());
            }
            if (daVar == this.aC) {
                b(daVar, this.aG);
            }
            return;
        }
        da y = cxVar.y(daVar.getName());
        if (y != null) {
            daVar.b(y);
        }
        if (daVar == this.aC) {
            this.aF = daVar.bT();
            ag();
        }
    }

    private void a(@NonNull ArrayList<bz> arrayList, @NonNull final da<AudioData> daVar, final float f) {
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't load midpoint services: context is null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("loading midpoint services for point: ");
        sb.append(f);
        ah.a(sb.toString());
        i.a((List<bz>) arrayList, this.adConfig, this.loadingTimeoutSeconds).a((C0056b<T>) new i.b() {
            public void onResult(@Nullable cx cxVar, @Nullable String str) {
                ar.this.a(daVar, cxVar, str, f);
            }
        }).a(context);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull da<AudioData> daVar, @Nullable cx cxVar, @Nullable String str, float f) {
        if (cxVar == null) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("loading midpoint services failed: ");
                sb.append(str);
                ah.a(sb.toString());
            }
            if (daVar == this.aC && f == this.aG) {
                b(daVar, f);
            }
            return;
        }
        da y = cxVar.y(daVar.getName());
        if (y != null) {
            daVar.b(y);
        }
        if (daVar == this.aC && f == this.aG) {
            a(daVar, f);
        }
    }

    private void a(@Nullable co coVar, @NonNull String str) {
        if (coVar == null) {
            ah.a("can't send stat: banner is null");
            return;
        }
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't send stat: context is null");
        } else {
            ib.a((List<dh>) coVar.getStatHolder().N(str), context);
        }
    }

    @Nullable
    private ci a(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner) {
        if (this.aR == null || this.aQ == null || this.aD == null) {
            ah.a("can't find companion banner: no playing banner");
            return null;
        }
        ArrayList companionBanners = this.aD.getCompanionBanners();
        int indexOf = this.aR.indexOf(instreamAdCompanionBanner);
        if (indexOf >= 0 && indexOf < companionBanners.size()) {
            return (ci) companionBanners.get(indexOf);
        }
        ah.a("can't find companion banner: provided instreamAdCompanionBanner not found in current playing banner");
        return null;
    }
}
