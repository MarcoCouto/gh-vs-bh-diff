package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;

/* compiled from: ViewabilityView */
public class gk extends View {
    @Nullable
    private a bD;
    @NonNull
    private final b jK = new b();

    /* compiled from: ViewabilityView */
    public interface a {
        void c(boolean z);
    }

    /* compiled from: ViewabilityView */
    static class b {
        private boolean cd;
        private boolean ce;

        b() {
        }

        /* access modifiers changed from: 0000 */
        public void f(boolean z) {
            this.cd = z;
        }

        /* access modifiers changed from: 0000 */
        public void setFocused(boolean z) {
            this.ce = z;
        }

        /* access modifiers changed from: 0000 */
        public boolean ef() {
            return this.cd && this.ce;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public b getViewabilityState() {
        return this.jK;
    }

    public gk(Context context) {
        super(context);
    }

    public boolean ef() {
        return this.jK.ef();
    }

    public void setViewabilityListener(@Nullable a aVar) {
        this.bD = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(1, 1);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        this.jK.setFocused(z);
        if (this.jK.ef()) {
            if (this.bD != null) {
                this.bD.c(true);
            }
        } else if (!z && this.bD != null) {
            this.bD.c(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        d(true);
    }

    private void d(boolean z) {
        this.jK.f(z);
        this.jK.setFocused(hasWindowFocus());
        if (this.jK.ef()) {
            if (this.bD != null) {
                this.bD.c(true);
            }
        } else if (!z && this.bD != null) {
            this.bD.c(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        d(false);
    }
}
