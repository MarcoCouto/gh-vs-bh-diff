package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Pair;
import com.ironsource.sdk.precache.DownloadManager;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: NativeAppwallAdSectionParser */
public class ei {
    public static ei h(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new ei(bzVar, aVar, context);
    }

    private ei(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull dc dcVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
        if (optJSONObject != null) {
            b(optJSONObject, dcVar);
        }
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull dc dcVar) {
        dcVar.setTitle(jSONObject.optString("title", dcVar.getTitle()));
        dcVar.D(jSONObject.optString("icon_hd", dcVar.cf()));
        dcVar.E(jSONObject.optString("bubble_icon_hd", dcVar.cg()));
        dcVar.F(jSONObject.optString("label_icon_hd", dcVar.ch()));
        dcVar.G(jSONObject.optString("goto_app_icon_hd", dcVar.ci()));
        dcVar.H(jSONObject.optString("item_highlight_icon", dcVar.cj()));
        JSONArray optJSONArray = jSONObject.optJSONArray("icon_status");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    dcVar.ce().add(new Pair(optJSONObject.optString("value"), optJSONObject.optString("icon_hd")));
                }
            }
        }
    }
}
