package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.net.URLDecoder;

/* compiled from: UrlResolver */
public class id {
    private static final String[] oa = {"http://play.google.com", "https://play.google.com", "http://market.android.com", "https://market.android.com", "market://", "samsungapps://"};
    /* access modifiers changed from: private */
    @Nullable
    public a ob;
    /* access modifiers changed from: private */
    @NonNull
    public final String url;

    /* compiled from: UrlResolver */
    public interface a {
        void ae(@Nullable String str);
    }

    @NonNull
    public static String decode(@NonNull String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to decode url ");
            sb.append(th.getMessage());
            ah.a(sb.toString());
            return str;
        }
    }

    public static boolean am(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            for (String startsWith : oa) {
                if (str.startsWith(startsWith)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean an(@NonNull String str) {
        return str.startsWith("samsungapps://");
    }

    public static boolean ao(@Nullable String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("https");
    }

    @NonNull
    public static id ap(@NonNull String str) {
        return new id(str);
    }

    private id(@NonNull String str) {
        this.url = str;
    }

    @NonNull
    public id a(@Nullable a aVar) {
        this.ob = aVar;
        return this;
    }

    public void Q(@NonNull Context context) {
        final Context applicationContext = context.getApplicationContext();
        ai.b(new Runnable() {
            public void run() {
                final String str = (String) Cdo.cF().f(id.this.url, applicationContext);
                if (id.this.ob != null) {
                    ai.c(new Runnable() {
                        public void run() {
                            if (id.this.ob != null) {
                                id.this.ob.ae(str);
                                id.this.ob = null;
                            }
                        }
                    });
                }
            }
        });
    }
}
