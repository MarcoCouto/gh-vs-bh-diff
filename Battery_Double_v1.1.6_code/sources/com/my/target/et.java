package com.my.target;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.bu.b;
import com.my.target.ez.a;
import com.smaato.sdk.core.api.VideoType;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: InterstitialMraidPresenter */
public class et implements b, ez {
    @Nullable
    private cl aU;
    private boolean cb;
    @NonNull
    private final Context context;
    @Nullable
    private gc cr;
    @Nullable
    private a eU;
    @NonNull
    private final fw fm;
    @NonNull
    private final bx fn;
    @NonNull
    private final bu fo;
    @NonNull
    private final WeakReference<Activity> fp;
    @NonNull
    private String fq;
    @Nullable
    private Integer fr;
    private boolean fs;
    private bw ft;
    private boolean fu;

    private boolean b(int i, int i2) {
        return (i & i2) != 0;
    }

    @NonNull
    public static et u(@NonNull Context context2) {
        return new et(context2);
    }

    private et(@NonNull bu buVar, @NonNull Context context2) {
        this.fs = true;
        this.ft = bw.aO();
        this.fo = buVar;
        this.context = context2.getApplicationContext();
        if (context2 instanceof Activity) {
            this.fp = new WeakReference<>((Activity) context2);
        } else {
            this.fp = new WeakReference<>(null);
        }
        this.fq = "loading";
        this.fn = bx.p(context2);
        this.fm = new fw(context2);
        this.fm.setOnCloseListener(new fw.a() {
            public void onClose() {
                et.this.dj();
            }
        });
        buVar.a((b) this);
    }

    private et(@NonNull Context context2) {
        this(bu.h(VideoType.INTERSTITIAL), context2);
    }

    public void a(@NonNull cy cyVar, @NonNull cl clVar) {
        this.aU = clVar;
        String source = clVar.getSource();
        if (source != null) {
            Z(source);
        }
    }

    public void a(@Nullable a aVar) {
        this.eU = aVar;
    }

    public void stop() {
        this.cb = true;
        if (this.cr != null) {
            this.cr.x(false);
        }
    }

    public void pause() {
        this.cb = true;
        if (this.cr != null) {
            this.cr.x(false);
        }
    }

    public void resume() {
        this.cb = false;
        if (this.cr != null) {
            this.cr.onResume();
        }
    }

    public void destroy() {
        if (!this.cb) {
            this.cb = true;
            if (this.cr != null) {
                this.cr.x(true);
            }
        }
        ViewParent parent = this.fm.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.fm);
        }
        this.fo.detach();
        if (this.cr != null) {
            this.cr.destroy();
            this.cr = null;
        }
        this.fm.removeAllViews();
    }

    @NonNull
    public View cT() {
        return this.fm;
    }

    public boolean a(boolean z, bw bwVar) {
        if (!a(bwVar)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to force orientation to ");
            sb.append(bwVar);
            this.fo.a("setOrientationProperties", sb.toString());
            return false;
        }
        this.fs = z;
        this.ft = bwVar;
        return dh();
    }

    public void c(@NonNull bu buVar) {
        this.fq = "default";
        dk();
        ArrayList arrayList = new ArrayList();
        if (dl()) {
            arrayList.add("'inlineVideo'");
        }
        arrayList.add("'vpaid'");
        buVar.a(arrayList);
        buVar.j(VideoType.INTERSTITIAL);
        buVar.k(buVar.isVisible());
        aa("default");
        buVar.aJ();
        buVar.a(this.fn);
        if (this.eU != null && this.aU != null) {
            this.eU.a(this.aU, this.context);
        }
    }

    public void onVisibilityChanged(boolean z) {
        this.fo.k(z);
    }

    public boolean a(@NonNull String str, @NonNull JsResult jsResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("JS Alert: ");
        sb.append(str);
        ah.a(sb.toString());
        jsResult.confirm();
        return true;
    }

    public boolean a(@NonNull ConsoleMessage consoleMessage, @NonNull bu buVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Console message: ");
        sb.append(consoleMessage.message());
        ah.a(sb.toString());
        return true;
    }

    public void onClose() {
        dj();
    }

    public void l(boolean z) {
        if (z != (!this.fm.dS())) {
            this.fm.setCloseVisible(!z);
        }
    }

    public void b(@NonNull Uri uri) {
        if (this.eU != null) {
            this.eU.b(this.aU, uri.toString(), this.fm.getContext());
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void Z(@NonNull String str) {
        this.cr = new gc(this.context);
        this.fo.a(this.cr);
        this.fm.addView(this.cr, new LayoutParams(-1, -1));
        this.fo.i(str);
    }

    public void aL() {
        dk();
    }

    public void aM() {
        this.fu = true;
    }

    public boolean o(@NonNull String str) {
        boolean z = false;
        if (!this.fu) {
            this.fo.a("vpaidEvent", "Calling VPAID command before VPAID init");
            return false;
        }
        boolean z2 = this.eU != null;
        if (this.aU != null) {
            z = true;
        }
        if (z2 && z) {
            this.eU.a(this.aU, str, this.context);
        }
        return true;
    }

    public boolean b(float f, float f2) {
        if (!this.fu) {
            this.fo.a("playheadEvent", "Calling VPAID command before VPAID init");
            return false;
        }
        if (f >= 0.0f && f2 >= 0.0f && this.eU != null && this.aU != null) {
            this.eU.a(this.aU, f, f2, this.context);
        }
        return true;
    }

    public boolean c(@Nullable Uri uri) {
        ah.a("Expand method not used with interstitials");
        return false;
    }

    public boolean a(int i, int i2, int i3, int i4, boolean z, int i5) {
        ah.a("setResizeProperties method not used with interstitials");
        return false;
    }

    public boolean aN() {
        ah.a("resize method not used with interstitials");
        return false;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean z(int i) {
        Activity activity = (Activity) this.fp.get();
        if (activity == null || !a(this.ft)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Attempted to lock orientation to unsupported value: ");
            sb.append(this.ft.toString());
            this.fo.a("setOrientationProperties", sb.toString());
            return false;
        }
        if (this.fr == null) {
            this.fr = Integer.valueOf(activity.getRequestedOrientation());
        }
        activity.setRequestedOrientation(i);
        return true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean dh() {
        if (!"none".equals(this.ft.toString())) {
            return z(this.ft.aP());
        }
        if (this.fs) {
            di();
            return true;
        }
        Activity activity = (Activity) this.fp.get();
        if (activity != null) {
            return z(ic.a(activity));
        }
        this.fo.a("setOrientationProperties", "Unable to set MRAID expand orientation to 'none'; expected passed in Activity Context.");
        return false;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void di() {
        Activity activity = (Activity) this.fp.get();
        if (!(activity == null || this.fr == null)) {
            activity.setRequestedOrientation(this.fr.intValue());
        }
        this.fr = null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(bw bwVar) {
        boolean z = true;
        if ("none".equals(bwVar.toString())) {
            return true;
        }
        Activity activity = (Activity) this.fp.get();
        if (activity == null) {
            return false;
        }
        try {
            ActivityInfo activityInfo = activity.getPackageManager().getActivityInfo(new ComponentName(activity, activity.getClass()), 0);
            int i = activityInfo.screenOrientation;
            if (i != -1) {
                if (i != bwVar.aP()) {
                    z = false;
                }
                return z;
            }
            if (!b(activityInfo.configChanges, 128) || !b(activityInfo.configChanges, 1024)) {
                z = false;
            }
            return z;
        } catch (NameNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void dj() {
        if (this.cr != null && !"loading".equals(this.fq) && !"hidden".equals(this.fq)) {
            di();
            if ("default".equals(this.fq)) {
                this.fm.setVisibility(4);
                aa("hidden");
            }
        }
    }

    private void dk() {
        DisplayMetrics displayMetrics = this.context.getResources().getDisplayMetrics();
        this.fn.a(displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.fn.a(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.fn.b(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.fn.c(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    private boolean dl() {
        Activity activity = (Activity) this.fp.get();
        if (activity == null || this.cr == null) {
            return false;
        }
        return ic.a(activity, (View) this.cr);
    }

    private void aa(@NonNull String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("MRAID state set to ");
        sb.append(str);
        ah.a(sb.toString());
        this.fq = str;
        this.fo.k(str);
        if ("hidden".equals(str)) {
            ah.a("InterstitialMraidPresenter: Mraid on close");
            if (this.eU != null) {
                this.eU.aj();
            }
        }
    }
}
