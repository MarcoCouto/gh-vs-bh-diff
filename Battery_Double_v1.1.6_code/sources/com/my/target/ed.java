package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.ironsource.sdk.precache.DownloadManager;
import com.my.target.common.models.ImageData;
import org.json.JSONObject;

/* compiled from: InterstitialSliderAdSectionParser */
public class ed {
    @NonNull
    public static ed f(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new ed(bzVar, aVar, context);
    }

    private ed(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull cz czVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
        if (optJSONObject != null) {
            b(optJSONObject, czVar);
        }
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull cz czVar) {
        String optString = jSONObject.optString("close_icon_hd", "");
        if (!TextUtils.isEmpty(optString)) {
            czVar.setCloseIcon(ImageData.newImageData(optString));
        }
        czVar.setBackgroundColor(ee.a(jSONObject, "backgroundColor", czVar.getBackgroundColor()));
        czVar.t(ee.a(jSONObject, "markerColor", czVar.bS()));
        czVar.s(ee.a(jSONObject, "activeMarkerColor", czVar.bR()));
    }
}
