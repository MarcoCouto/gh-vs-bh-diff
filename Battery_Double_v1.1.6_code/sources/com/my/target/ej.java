package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import org.json.JSONObject;

/* compiled from: ResearchStatsParser */
public class ej extends em {
    public static ej i(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new ej(bzVar, aVar, context);
    }

    private ej(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        super(bzVar, aVar, context);
    }

    @Nullable
    public dh a(@NonNull JSONObject jSONObject, float f) {
        String optString = jSONObject.optString("type");
        String optString2 = jSONObject.optString("url");
        if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2)) {
            f("Required field", "failed to parse stat: no type or url");
            return null;
        }
        char c = 65535;
        int hashCode = optString.hashCode();
        if (hashCode != -1053159584) {
            if (hashCode != 1669348544) {
                if (hashCode == 1788134515 && optString.equals("playheadReachedValue")) {
                    c = 2;
                }
            } else if (optString.equals("playheadViewabilityValue")) {
                c = 0;
            }
        } else if (optString.equals("playheadTimerValue")) {
            c = 1;
        }
        switch (c) {
            case 0:
                return e(jSONObject, optString2);
            case 1:
                return d(jSONObject, optString2);
            case 2:
                dg a = super.a(jSONObject, optString2, f);
                if (a == null || a.cq() < 0.0f) {
                    return null;
                }
                return a;
            default:
                return super.a(jSONObject, f);
        }
    }

    @Nullable
    private dh d(@NonNull JSONObject jSONObject, @NonNull String str) {
        if (!str.contains("[CONTENTPLAYHEAD]")) {
            f("Required field", "failed to parse researchTimerStat: no [CONTENTPLAYHEAD] macros");
            return null;
        }
        int optInt = jSONObject.optInt("startTimer", 0);
        int optInt2 = jSONObject.optInt("endTimer", 0);
        if (optInt < 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("failed to parse researchTimerStat: wrong start timer ");
            sb.append(optInt);
            f("Bad value", sb.toString());
            return null;
        } else if (optInt2 < 0) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("failed to parse researchTimerStat: wrong end timer ");
            sb2.append(optInt);
            f("Bad value", sb2.toString());
            return null;
        } else if (optInt2 == 0 || optInt < optInt2) {
            cf t = cf.t(str);
            t.q(jSONObject.optInt("rate", 1));
            t.o(optInt);
            t.p(optInt2);
            return t;
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("failed to parse researchTimerStat: start timer (");
            sb3.append(optInt);
            sb3.append(") cannot be less than end timer (");
            sb3.append(optInt);
            sb3.append(")");
            f("Bad value", sb3.toString());
            return null;
        }
    }

    @Nullable
    private dh e(@NonNull JSONObject jSONObject, @NonNull String str) {
        int optInt = jSONObject.optInt("viewablePercent", -1);
        cg cgVar = null;
        if (optInt < 0 || optInt > 100) {
            f("Bad value", "failed to parse viewabilityStat: invalid viewable percent value");
            return null;
        }
        int optInt2 = jSONObject.optInt("duration", -1);
        if (optInt2 >= 0) {
            int optInt3 = jSONObject.optInt("startTimer", 0);
            int optInt4 = jSONObject.optInt("endTimer", 0);
            if (optInt3 < 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("failed to parse viewabilityStat: wrong start timer ");
                sb.append(optInt3);
                f("Bad value", sb.toString());
                return null;
            } else if (optInt4 < 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("failed to parse viewabilityStat: wrong end timer ");
                sb2.append(optInt3);
                f("Bad value", sb2.toString());
                return null;
            } else if (optInt4 == 0 || optInt3 < optInt4) {
                cgVar = cg.u(str);
                cgVar.w(optInt);
                cgVar.setDuration((float) optInt2);
                cgVar.o(optInt3);
                cgVar.p(optInt4);
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("failed to parse viewabilityStat: start timer (");
                sb3.append(optInt3);
                sb3.append(") cannot be less than end timer (");
                sb3.append(optInt4);
                sb3.append(")");
                f("Bad value", sb3.toString());
                return null;
            }
        }
        return cgVar;
    }
}
