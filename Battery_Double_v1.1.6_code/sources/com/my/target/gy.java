package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.nativeads.views.MediaAdView;

@SuppressLint({"ViewConstructor"})
/* compiled from: VerticalView */
public class gy extends RelativeLayout {
    private static final int CTA_ID = ic.eG();
    private static final int lh = ic.eG();
    private static final int li = ic.eG();
    private static final int lj = ic.eG();
    @NonNull
    private final fu ageRestrictionLabel;
    @NonNull
    private final Button ctaButton;
    private final boolean ki;
    /* access modifiers changed from: private */
    @NonNull
    public final gp lk;
    @NonNull
    private final gq ll;
    @NonNull
    private final ic uiUtils;

    public gy(@NonNull Context context, @NonNull ic icVar, boolean z) {
        super(context);
        this.uiUtils = icVar;
        this.ki = z;
        this.ll = new gq(context, icVar, z);
        ic.a((View) this.ll, "footer_layout");
        this.lk = new gp(context, icVar, z);
        ic.a((View) this.lk, "body_layout");
        this.ctaButton = new Button(context);
        ic.a((View) this.ctaButton, "cta_button");
        this.ageRestrictionLabel = new fu(context);
        ic.a((View) this.ageRestrictionLabel, "age_bordering");
    }

    public void a(int i, int i2, boolean z) {
        int i3;
        int max = Math.max(i2, i) / 8;
        this.lk.z(z);
        this.ll.initView();
        View view = new View(getContext());
        view.setBackgroundColor(-5592406);
        view.setLayoutParams(new LayoutParams(-1, 1));
        this.ll.setId(li);
        this.ll.a(max, z);
        this.ctaButton.setId(CTA_ID);
        this.ctaButton.setPadding(this.uiUtils.M(15), 0, this.uiUtils.M(15), 0);
        this.ctaButton.setMinimumWidth(this.uiUtils.M(100));
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setSingleLine();
        this.ctaButton.setEllipsize(TruncateAt.END);
        this.ageRestrictionLabel.setId(lh);
        this.ageRestrictionLabel.f(1, -7829368);
        this.ageRestrictionLabel.setPadding(this.uiUtils.M(2), 0, 0, 0);
        this.ageRestrictionLabel.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
        this.ageRestrictionLabel.setMaxEms(5);
        this.ageRestrictionLabel.a(1, MediaAdView.COLOR_PLACEHOLDER_GRAY, this.uiUtils.M(3));
        this.ageRestrictionLabel.setBackgroundColor(1711276032);
        this.lk.setId(lj);
        if (z) {
            this.lk.setPadding(this.uiUtils.M(4), this.uiUtils.M(4), this.uiUtils.M(4), this.uiUtils.M(4));
        } else {
            this.lk.setPadding(this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(16));
        }
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(2, li);
        this.lk.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.setMargins(this.uiUtils.M(16), z ? this.uiUtils.M(8) : this.uiUtils.M(16), this.uiUtils.M(16), this.uiUtils.M(4));
        if (VERSION.SDK_INT >= 17) {
            layoutParams2.addRule(21, -1);
        } else {
            layoutParams2.addRule(11, -1);
        }
        this.ageRestrictionLabel.setLayoutParams(layoutParams2);
        if (this.ki) {
            i3 = this.uiUtils.M(64);
        } else {
            i3 = this.uiUtils.M(52);
        }
        LayoutParams layoutParams3 = new LayoutParams(-2, i3);
        layoutParams3.addRule(14, -1);
        layoutParams3.addRule(8, lj);
        if (z) {
            double d = (double) (-this.uiUtils.M(52));
            Double.isNaN(d);
            layoutParams3.bottomMargin = (int) (d / 1.5d);
        } else {
            layoutParams3.bottomMargin = (-this.uiUtils.M(52)) / 2;
        }
        this.ctaButton.setLayoutParams(layoutParams3);
        LayoutParams layoutParams4 = new LayoutParams(-1, max);
        layoutParams4.addRule(12, -1);
        this.ll.setLayoutParams(layoutParams4);
        addView(this.lk);
        addView(view);
        addView(this.ageRestrictionLabel);
        addView(this.ll);
        addView(this.ctaButton);
        setClickable(true);
        if (this.ki) {
            this.ctaButton.setTextSize(2, 32.0f);
        } else {
            this.ctaButton.setTextSize(2, 22.0f);
        }
    }

    public void setBanner(@NonNull cn cnVar) {
        this.lk.setBanner(cnVar);
        this.ctaButton.setText(cnVar.getCtaText());
        this.ll.setBackgroundColor(-39322);
        if (TextUtils.isEmpty(cnVar.getAgeRestrictions())) {
            this.ageRestrictionLabel.setVisibility(8);
        } else {
            this.ageRestrictionLabel.setText(cnVar.getAgeRestrictions());
        }
        ic.a(this.ctaButton, -16733198, -16746839, this.uiUtils.M(2));
        this.ctaButton.setTextColor(-1);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void a(@NonNull final ca caVar, @NonNull final OnClickListener onClickListener) {
        this.lk.a(caVar, onClickListener);
        if (caVar.f456do) {
            this.ctaButton.setOnClickListener(onClickListener);
            return;
        }
        if (caVar.di) {
            this.ctaButton.setOnClickListener(onClickListener);
            this.ctaButton.setEnabled(true);
        } else {
            this.ctaButton.setOnClickListener(null);
            this.ctaButton.setEnabled(false);
        }
        this.ageRestrictionLabel.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!caVar.dj) {
                    return true;
                }
                int action = motionEvent.getAction();
                if (action != 3) {
                    switch (action) {
                        case 0:
                            gy.this.lk.setBackgroundColor(-3806472);
                            break;
                        case 1:
                            gy.this.lk.setBackgroundColor(-1);
                            onClickListener.onClick(view);
                            break;
                    }
                } else {
                    gy.this.setBackgroundColor(-1);
                }
                return true;
            }
        });
    }
}
