package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeAdBanner */
public class cp extends ch {
    @NonNull
    private final List<cq> nativeAdCards = new ArrayList();
    @Nullable
    private co<VideoData> videoBanner;
    private float viewabilityRate = 1.0f;
    private float viewabilitySquare = 0.5f;

    @NonNull
    public static cp newBanner() {
        return new cp();
    }

    public void setViewabilitySquare(float f) {
        this.viewabilitySquare = f;
    }

    public float getViewabilityRate() {
        return this.viewabilityRate;
    }

    public void setViewabilityRate(float f) {
        this.viewabilityRate = f;
    }

    private cp() {
    }

    public void setVideoBanner(@Nullable co<VideoData> coVar) {
        this.videoBanner = coVar;
    }

    @Nullable
    public co<VideoData> getVideoBanner() {
        return this.videoBanner;
    }

    public void addNativeAdCard(@NonNull cq cqVar) {
        this.nativeAdCards.add(cqVar);
    }

    @NonNull
    public List<cq> getNativeAdCards() {
        return new ArrayList(this.nativeAdCards);
    }

    public float getViewabilitySquare() {
        return this.viewabilitySquare;
    }
}
