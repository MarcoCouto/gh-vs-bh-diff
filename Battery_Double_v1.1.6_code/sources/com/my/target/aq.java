package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ak.c;
import com.my.target.b.C0056b;
import com.my.target.common.models.VideoData;
import com.my.target.f.b;
import com.my.target.instreamads.InstreamAd;
import com.my.target.instreamads.InstreamAd.InstreamAdBanner;
import com.my.target.instreamads.InstreamAd.InstreamAdListener;
import com.my.target.instreamads.InstreamAdPlayer;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InstreamAdEngine */
public class aq {
    @NonNull
    private final cw aA;
    /* access modifiers changed from: private */
    @NonNull
    public final ak aB;
    /* access modifiers changed from: private */
    @Nullable
    public da<VideoData> aC;
    /* access modifiers changed from: private */
    @Nullable
    public co<VideoData> aD;
    /* access modifiers changed from: private */
    @Nullable
    public InstreamAdBanner aE;
    @Nullable
    private List<co<VideoData>> aF;
    private float aG;
    private int aH;
    private int aI;
    /* access modifiers changed from: private */
    public boolean aJ;
    @NonNull
    private final a adConfig;
    /* access modifiers changed from: private */
    @NonNull
    public final InstreamAd az;
    @NonNull
    private final hr clickHandler;
    private int loadingTimeoutSeconds;
    @NonNull
    private float[] midpoints = new float[0];

    /* compiled from: InstreamAdEngine */
    class a implements c {
        private a() {
        }

        public void b(@NonNull co coVar) {
            if (aq.this.aC != null && aq.this.aD == coVar && aq.this.aE != null) {
                if (!aq.this.aJ) {
                    aq.this.aJ = true;
                    Context context = aq.this.aB.getContext();
                    if (context == null) {
                        ah.a("can't send stat: context is null");
                    } else {
                        ib.a((List<dh>) aq.this.aC.w(Events.AD_IMPRESSION), context);
                    }
                }
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerStart(aq.this.az, aq.this.aE);
                }
            }
        }

        public void e(@NonNull co coVar) {
            if (aq.this.aC != null && aq.this.aD == coVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerPause(aq.this.az, aq.this.aE);
                }
            }
        }

        public void f(@NonNull co coVar) {
            if (aq.this.aC != null && aq.this.aD == coVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerResume(aq.this.az, aq.this.aE);
                }
            }
        }

        public void c(@NonNull co coVar) {
            if (aq.this.aC != null && aq.this.aD == coVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerComplete(aq.this.az, aq.this.aE);
                }
            }
        }

        public void d(@NonNull co coVar) {
            if (aq.this.aC != null && aq.this.aD == coVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerComplete(aq.this.az, aq.this.aE);
                }
                aq.this.ag();
            }
        }

        public void a(float f, float f2, @NonNull co coVar) {
            if (aq.this.aC != null && aq.this.aD == coVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerTimeLeftChange(f, f2, aq.this.az);
                }
            }
        }

        public void a(@NonNull String str, @NonNull co coVar) {
            if (aq.this.aC != null && aq.this.aD == coVar) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onError(str, aq.this.az);
                }
                aq.this.ag();
            }
        }
    }

    @NonNull
    public static aq a(@NonNull InstreamAd instreamAd, @NonNull cw cwVar, @NonNull a aVar) {
        return new aq(instreamAd, cwVar, aVar);
    }

    private aq(@NonNull InstreamAd instreamAd, @NonNull cw cwVar, @NonNull a aVar) {
        this.az = instreamAd;
        this.aA = cwVar;
        this.adConfig = aVar;
        this.aB = ak.t();
        this.aB.a((c) new a());
        this.clickHandler = hr.et();
    }

    public void setVolume(float f) {
        this.aB.setVolume(f);
    }

    @Nullable
    public InstreamAdPlayer getPlayer() {
        return this.aB.getPlayer();
    }

    public void setPlayer(@Nullable InstreamAdPlayer instreamAdPlayer) {
        this.aB.setPlayer(instreamAdPlayer);
    }

    public void swapPlayer(@Nullable InstreamAdPlayer instreamAdPlayer) {
        this.aB.swapPlayer(instreamAdPlayer);
    }

    public void a(@NonNull float[] fArr) {
        this.midpoints = fArr;
    }

    public void e(int i) {
        this.loadingTimeoutSeconds = i;
    }

    public void setFullscreen(boolean z) {
        String str = "fullscreenOff";
        if (z) {
            str = "fullscreenOn";
        }
        a((co) this.aD, str);
    }

    public void start(@NonNull String str) {
        stop();
        this.aC = this.aA.x(str);
        if (this.aC != null) {
            this.aB.setConnectionTimeout(this.aC.bU());
            this.aJ = false;
            this.aI = this.aC.bV();
            this.aH = -1;
            this.aF = this.aC.bT();
            ag();
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("no section with name ");
        sb.append(str);
        ah.a(sb.toString());
    }

    public void startMidroll(float f) {
        boolean z;
        stop();
        float[] fArr = this.midpoints;
        int length = fArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (Float.compare(fArr[i], f) == 0) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            this.aC = this.aA.x(BreakId.MIDROLL);
            if (this.aC != null) {
                this.aB.setConnectionTimeout(this.aC.bU());
                this.aJ = false;
                this.aI = this.aC.bV();
                this.aH = -1;
                this.aG = f;
                a(this.aC, f);
                return;
            }
            return;
        }
        ah.a("attempt to start wrong midpoint, use one of InstreamAd.getMidPoints()");
    }

    public void pause() {
        if (this.aC != null) {
            this.aB.pause();
        }
    }

    public void resume() {
        if (this.aC != null) {
            this.aB.resume();
        }
    }

    public void stop() {
        if (this.aC != null) {
            this.aB.stop();
            a((da) this.aC);
        }
    }

    public void skip() {
        a((co) this.aD, "closedByUser");
        stop();
    }

    public void skipBanner() {
        a((co) this.aD, "closedByUser");
        this.aB.stop();
        ag();
    }

    public void handleClick() {
        if (this.aD == null) {
            ah.a("can't handle click: no playing banner");
            return;
        }
        Context context = this.aB.getContext();
        if (context == null) {
            ah.a("can't handle click: context is null");
        } else {
            this.clickHandler.b(this.aD, context);
        }
    }

    public void destroy() {
        this.aB.destroy();
    }

    public float getVolume() {
        return this.aB.getVolume();
    }

    private void a(@NonNull da<VideoData> daVar, float f) {
        ArrayList arrayList = new ArrayList();
        for (co coVar : daVar.bT()) {
            if (coVar.getPoint() == f) {
                arrayList.add(coVar);
            }
        }
        int size = arrayList.size();
        if (size <= 0 || this.aH >= size - 1) {
            ArrayList g = daVar.g(f);
            if (g.size() > 0) {
                a(g, daVar, f);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("There is no one midpoint service for point: ");
            sb.append(f);
            ah.a(sb.toString());
            b(daVar, f);
            return;
        }
        this.aF = arrayList;
        ag();
    }

    /* access modifiers changed from: private */
    public void ag() {
        if (this.aC != null) {
            if (this.aI == 0 || this.aF == null) {
                b(this.aC, this.aG);
                return;
            }
            int i = this.aH + 1;
            if (i < this.aF.size()) {
                this.aH = i;
                co<VideoData> coVar = (co) this.aF.get(i);
                if ("statistics".equals(coVar.getType())) {
                    a((co) coVar, "playbackStarted");
                    ag();
                } else {
                    if (this.aI > 0) {
                        this.aI--;
                    }
                    this.aD = coVar;
                    this.aE = InstreamAdBanner.newBanner(coVar);
                    this.aB.a(coVar);
                }
            } else {
                b(this.aC, this.aG);
            }
        }
    }

    private void b(@NonNull da<VideoData> daVar, float f) {
        bz bX = daVar.bX();
        if (bX == null) {
            a((da) daVar);
        } else if (BreakId.MIDROLL.equals(daVar.getName())) {
            bX.o(true);
            bX.setPoint(f);
            ArrayList arrayList = new ArrayList();
            arrayList.add(bX);
            StringBuilder sb = new StringBuilder();
            sb.append("using doAfter service for point: ");
            sb.append(f);
            ah.a(sb.toString());
            a(arrayList, daVar, f);
        } else {
            a(bX, daVar);
        }
    }

    private void a(@NonNull da daVar) {
        if (daVar == this.aC) {
            if (BreakId.MIDROLL.equals(daVar.getName())) {
                this.aC.v(this.aI);
            }
            this.aC = null;
            this.aJ = false;
            this.aD = null;
            this.aE = null;
            this.aH = -1;
            InstreamAdListener listener = this.az.getListener();
            if (listener != null) {
                listener.onComplete(daVar.getName(), this.az);
            }
        }
    }

    private void a(@NonNull bz bzVar, @NonNull final da<VideoData> daVar) {
        Context context = this.aB.getContext();
        if (context == null) {
            ah.a("can't load doAfter service: context is null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("loading doAfter service: ");
        sb.append(bzVar.getUrl());
        ah.a(sb.toString());
        f.a(bzVar, this.adConfig, this.loadingTimeoutSeconds).a((C0056b<T>) new b() {
            public void onResult(@Nullable cw cwVar, @Nullable String str) {
                aq.this.a(daVar, cwVar, str);
            }
        }).a(context);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull da<VideoData> daVar, @Nullable cw cwVar, @Nullable String str) {
        if (cwVar == null) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("loading doAfter service failed: ");
                sb.append(str);
                ah.a(sb.toString());
            }
            if (daVar == this.aC) {
                b(daVar, this.aG);
            }
            return;
        }
        da x = cwVar.x(daVar.getName());
        if (x != null) {
            daVar.b(x);
        }
        if (daVar == this.aC) {
            this.aF = daVar.bT();
            ag();
        }
    }

    private void a(@NonNull ArrayList<bz> arrayList, @NonNull final da<VideoData> daVar, final float f) {
        Context context = this.aB.getContext();
        if (context == null) {
            ah.a("can't load midpoint services: context is null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("loading midpoint services for point: ");
        sb.append(f);
        ah.a(sb.toString());
        f.a((List<bz>) arrayList, this.adConfig, this.loadingTimeoutSeconds).a((C0056b<T>) new b() {
            public void onResult(@Nullable cw cwVar, @Nullable String str) {
                aq.this.a(daVar, cwVar, str, f);
            }
        }).a(context);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull da<VideoData> daVar, @Nullable cw cwVar, @Nullable String str, float f) {
        if (cwVar == null) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("loading midpoint services failed: ");
                sb.append(str);
                ah.a(sb.toString());
            }
            if (daVar == this.aC && f == this.aG) {
                b(daVar, f);
            }
            return;
        }
        da x = cwVar.x(daVar.getName());
        if (x != null) {
            daVar.b(x);
        }
        if (daVar == this.aC && f == this.aG) {
            a(daVar, f);
        }
    }

    private void a(@Nullable co coVar, @NonNull String str) {
        if (coVar == null) {
            ah.a("can't send stat: banner is null");
            return;
        }
        Context context = this.aB.getContext();
        if (context == null) {
            ah.a("can't send stat: context is null");
        } else {
            ib.a((List<dh>) coVar.getStatHolder().N(str), context);
        }
    }
}
