package com.my.target;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.facebook.places.model.PlaceFields;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: EnvironmentParamsDataProvider */
public final class fe extends ff {
    private volatile boolean gY = true;
    private volatile boolean gZ = true;

    /* compiled from: EnvironmentParamsDataProvider */
    static class a {
        /* access modifiers changed from: private */
        @Nullable
        public ArrayList<b> hb = null;

        a(@NonNull Context context) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                try {
                    if ((VERSION.SDK_INT >= 17 && VERSION.SDK_INT < 29 && fe.h("android.permission.ACCESS_COARSE_LOCATION", context)) || fe.h("android.permission.ACCESS_FINE_LOCATION", context)) {
                        this.hb = b(telephonyManager);
                    }
                    if ((this.hb == null || this.hb.isEmpty()) && ((VERSION.SDK_INT < 29 && fe.h("android.permission.ACCESS_COARSE_LOCATION", context)) || fe.h("android.permission.ACCESS_FINE_LOCATION", context))) {
                        this.hb = a(telephonyManager);
                    }
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Environment provider exception ");
                    sb.append(e.getMessage());
                    ah.a(sb.toString());
                }
            }
        }

        @Nullable
        @SuppressLint({"MissingPermission"})
        private ArrayList<b> a(@NonNull TelephonyManager telephonyManager) {
            CellLocation cellLocation = telephonyManager.getCellLocation();
            if (!(cellLocation instanceof GsmCellLocation)) {
                return null;
            }
            ArrayList<b> arrayList = new ArrayList<>();
            GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
            b bVar = new b("gsm");
            arrayList.add(bVar);
            bVar.hc = gsmCellLocation.getCid();
            bVar.hd = gsmCellLocation.getLac();
            String networkOperator = telephonyManager.getNetworkOperator();
            if (!(networkOperator == null || networkOperator.length() == 0)) {
                try {
                    bVar.mcc = Integer.parseInt(networkOperator.substring(0, 3));
                    bVar.mnc = Integer.parseInt(networkOperator.substring(3));
                } catch (Exception unused) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("unable to substring network operator ");
                    sb.append(networkOperator);
                    ah.a(sb.toString());
                }
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("current cell: ");
            sb2.append(bVar.hc);
            sb2.append(",");
            sb2.append(bVar.hd);
            sb2.append(",");
            sb2.append(bVar.mcc);
            sb2.append(",");
            sb2.append(bVar.mnc);
            ah.a(sb2.toString());
            return arrayList;
        }

        @SuppressLint({"MissingPermission"})
        @Nullable
        @TargetApi(17)
        private ArrayList<b> b(@NonNull TelephonyManager telephonyManager) {
            b bVar;
            List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
            if (allCellInfo == null) {
                return null;
            }
            ArrayList<b> arrayList = new ArrayList<>();
            for (CellInfo cellInfo : allCellInfo) {
                if (cellInfo.isRegistered()) {
                    if (cellInfo instanceof CellInfoLte) {
                        bVar = new b("lte");
                        CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
                        CellIdentityLte cellIdentity = cellInfoLte.getCellIdentity();
                        CellSignalStrengthLte cellSignalStrength = cellInfoLte.getCellSignalStrength();
                        bVar.hc = cellIdentity.getCi();
                        bVar.hd = Integer.MAX_VALUE;
                        bVar.mcc = cellIdentity.getMcc();
                        bVar.mnc = cellIdentity.getMnc();
                        bVar.level = cellSignalStrength.getLevel();
                        bVar.he = cellSignalStrength.getDbm();
                        bVar.hf = cellSignalStrength.getAsuLevel();
                        bVar.hg = cellSignalStrength.getTimingAdvance();
                        if (VERSION.SDK_INT >= 24) {
                            bVar.hh = cellIdentity.getEarfcn();
                        }
                        bVar.hi = Integer.MAX_VALUE;
                        bVar.hj = Integer.MAX_VALUE;
                        bVar.hk = cellIdentity.getTac();
                    } else if (cellInfo instanceof CellInfoGsm) {
                        bVar = new b("gsm");
                        CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
                        CellIdentityGsm cellIdentity2 = cellInfoGsm.getCellIdentity();
                        CellSignalStrengthGsm cellSignalStrength2 = cellInfoGsm.getCellSignalStrength();
                        bVar.hc = cellIdentity2.getCid();
                        bVar.hd = cellIdentity2.getLac();
                        bVar.mcc = cellIdentity2.getMcc();
                        bVar.mnc = cellIdentity2.getMnc();
                        bVar.level = cellSignalStrength2.getLevel();
                        bVar.he = cellSignalStrength2.getDbm();
                        bVar.hf = cellSignalStrength2.getAsuLevel();
                        if (VERSION.SDK_INT >= 26) {
                            bVar.hg = cellSignalStrength2.getTimingAdvance();
                        } else {
                            bVar.hg = Integer.MAX_VALUE;
                        }
                        bVar.hh = Integer.MAX_VALUE;
                        if (VERSION.SDK_INT >= 24) {
                            bVar.hi = cellIdentity2.getBsic();
                        }
                        bVar.hj = cellIdentity2.getPsc();
                        bVar.hk = Integer.MAX_VALUE;
                    } else if (VERSION.SDK_INT >= 18 && (cellInfo instanceof CellInfoWcdma)) {
                        bVar = new b("wcdma");
                        CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
                        CellIdentityWcdma cellIdentity3 = cellInfoWcdma.getCellIdentity();
                        CellSignalStrengthWcdma cellSignalStrength3 = cellInfoWcdma.getCellSignalStrength();
                        bVar.hc = cellIdentity3.getCid();
                        bVar.hd = cellIdentity3.getLac();
                        bVar.mcc = cellIdentity3.getMcc();
                        bVar.mnc = cellIdentity3.getMnc();
                        bVar.level = cellSignalStrength3.getLevel();
                        bVar.he = cellSignalStrength3.getDbm();
                        bVar.hf = cellSignalStrength3.getAsuLevel();
                        bVar.hg = Integer.MAX_VALUE;
                        if (VERSION.SDK_INT >= 24) {
                            bVar.hh = cellIdentity3.getUarfcn();
                        }
                        bVar.hi = Integer.MAX_VALUE;
                        bVar.hj = cellIdentity3.getPsc();
                        bVar.hk = Integer.MAX_VALUE;
                    } else if (cellInfo instanceof CellInfoCdma) {
                        bVar = new b("cdma");
                        CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
                        CellIdentityCdma cellIdentity4 = cellInfoCdma.getCellIdentity();
                        CellSignalStrengthCdma cellSignalStrength4 = cellInfoCdma.getCellSignalStrength();
                        bVar.hl = cellIdentity4.getNetworkId();
                        bVar.hm = cellIdentity4.getSystemId();
                        bVar.hn = cellIdentity4.getBasestationId();
                        bVar.ho = cellIdentity4.getLatitude();
                        bVar.hp = cellIdentity4.getLongitude();
                        bVar.hq = cellSignalStrength4.getCdmaLevel();
                        bVar.level = cellSignalStrength4.getLevel();
                        bVar.hr = cellSignalStrength4.getEvdoLevel();
                        bVar.hf = cellSignalStrength4.getAsuLevel();
                        bVar.hs = cellSignalStrength4.getCdmaDbm();
                        bVar.he = cellSignalStrength4.getDbm();
                        bVar.ht = cellSignalStrength4.getEvdoDbm();
                        bVar.hu = cellSignalStrength4.getEvdoEcio();
                        bVar.hv = cellSignalStrength4.getCdmaEcio();
                        bVar.hw = cellSignalStrength4.getEvdoSnr();
                    }
                    arrayList.add(bVar);
                }
            }
            return arrayList;
        }
    }

    /* compiled from: EnvironmentParamsDataProvider */
    static class b {
        int hc = Integer.MAX_VALUE;
        int hd = Integer.MAX_VALUE;
        int he = Integer.MAX_VALUE;
        int hf = Integer.MAX_VALUE;
        int hg = Integer.MAX_VALUE;
        int hh = Integer.MAX_VALUE;
        int hi = Integer.MAX_VALUE;
        int hj = Integer.MAX_VALUE;
        int hk = Integer.MAX_VALUE;
        int hl = Integer.MAX_VALUE;
        int hm = Integer.MAX_VALUE;
        int hn = Integer.MAX_VALUE;
        int ho = Integer.MAX_VALUE;
        int hp = Integer.MAX_VALUE;
        int hq = Integer.MAX_VALUE;
        int hr = Integer.MAX_VALUE;
        int hs = Integer.MAX_VALUE;
        int ht = Integer.MAX_VALUE;
        int hu = Integer.MAX_VALUE;
        int hv = Integer.MAX_VALUE;
        int hw = Integer.MAX_VALUE;
        int level = Integer.MAX_VALUE;
        int mcc = Integer.MAX_VALUE;
        int mnc = Integer.MAX_VALUE;
        public final String type;

        b(String str) {
            this.type = str;
        }
    }

    /* compiled from: EnvironmentParamsDataProvider */
    static class c {
        WifiInfo hx;
        List<ScanResult> hy;

        @SuppressLint({"MissingPermission"})
        c(@NonNull Context context) {
            try {
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi");
                if (wifiManager != null && wifiManager.isWifiEnabled()) {
                    this.hx = wifiManager.getConnectionInfo();
                    if (VERSION.SDK_INT < 24 || fe.x(context)) {
                        this.hy = wifiManager.getScanResults();
                    }
                    if (this.hy != null) {
                        Collections.sort(this.hy, new Comparator<ScanResult>() {
                            /* renamed from: a */
                            public int compare(ScanResult scanResult, ScanResult scanResult2) {
                                if (scanResult.level < scanResult2.level) {
                                    return 1;
                                }
                                return scanResult.level > scanResult2.level ? -1 : 0;
                            }
                        });
                    }
                }
            } catch (SecurityException unused) {
                ah.a("No permissions for access to wifi state");
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean x(@NonNull Context context) {
        return h("android.permission.ACCESS_FINE_LOCATION", context) || h("android.permission.ACCESS_COARSE_LOCATION", context);
    }

    /* access modifiers changed from: private */
    public static boolean h(@NonNull String str, @NonNull Context context) {
        int i;
        try {
            i = context.checkCallingOrSelfPermission(str);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to check ");
            sb.append(str);
            sb.append(" permission! Unexpected throwable in Context.checkCallingOrSelfPermission() method: ");
            sb.append(th.getMessage());
            ah.a(sb.toString());
            i = -1;
        }
        return i == 0;
    }

    public void v(boolean z) {
        this.gZ = z;
    }

    public void w(boolean z) {
        this.gY = z;
    }

    public void collectData(@NonNull final Context context) {
        ai.b(new Runnable() {
            public void run() {
                fe.this.y(context);
            }
        });
    }

    /* access modifiers changed from: private */
    public void y(@NonNull Context context) {
        if (!this.gY) {
            removeAll();
            return;
        }
        HashMap hashMap = new HashMap();
        a(context, (Map<String, String>) hashMap);
        b(context, hashMap);
        c(context, hashMap);
        synchronized (this) {
            removeAll();
            addParams(hashMap);
        }
    }

    @SuppressLint({"MissingPermission"})
    private void a(@NonNull Context context, @NonNull Map<String, String> map) {
        if (h("android.permission.ACCESS_FINE_LOCATION", context) || h("android.permission.ACCESS_COARSE_LOCATION", context)) {
            long j = 0;
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            if (locationManager != null) {
                Location location = null;
                String str = null;
                float f = Float.MAX_VALUE;
                for (String str2 : locationManager.getAllProviders()) {
                    try {
                        Location lastKnownLocation = locationManager.getLastKnownLocation(str2);
                        if (lastKnownLocation != null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("locationProvider: ");
                            sb.append(str2);
                            ah.a(sb.toString());
                            float accuracy = lastKnownLocation.getAccuracy();
                            long time = lastKnownLocation.getTime();
                            if (location == null || (time > j && accuracy < f)) {
                                str = str2;
                                location = lastKnownLocation;
                                f = accuracy;
                                j = time;
                            }
                        }
                    } catch (SecurityException unused) {
                        ah.a("No permissions for get geo data");
                    }
                }
                if (location != null) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(location.getLatitude());
                    sb2.append(",");
                    sb2.append(location.getLongitude());
                    sb2.append(",");
                    sb2.append(location.getAccuracy());
                    sb2.append(",");
                    sb2.append(location.getSpeed());
                    sb2.append(",");
                    long j2 = j / 1000;
                    sb2.append(j2);
                    map.put("location", sb2.toString());
                    map.put("location_provider", str);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("location: ");
                    sb3.append(location.getLatitude());
                    sb3.append(", ");
                    sb3.append(location.getLongitude());
                    sb3.append(" accuracy = ");
                    sb3.append(location.getAccuracy());
                    sb3.append(" speed = ");
                    sb3.append(location.getSpeed());
                    sb3.append(" time = ");
                    sb3.append(j2);
                    sb3.append("  provider: ");
                    sb3.append(str);
                    ah.a(sb3.toString());
                }
            }
        }
    }

    @SuppressLint({"HardwareIds"})
    private void b(@NonNull Context context, @NonNull Map<String, String> map) {
        if (this.gZ && h("android.permission.ACCESS_WIFI_STATE", context)) {
            c cVar = new c(context);
            if (cVar.hx != null) {
                WifiInfo wifiInfo = cVar.hx;
                String bssid = wifiInfo.getBSSID();
                if (bssid == null) {
                    bssid = "";
                }
                int linkSpeed = wifiInfo.getLinkSpeed();
                int networkId = wifiInfo.getNetworkId();
                int rssi = wifiInfo.getRssi();
                String ssid = wifiInfo.getSSID();
                if (ssid == null) {
                    ssid = "";
                }
                StringBuilder sb = new StringBuilder();
                sb.append(bssid);
                sb.append(",");
                sb.append(ssid);
                sb.append(",");
                sb.append(rssi);
                sb.append(",");
                sb.append(networkId);
                sb.append(",");
                sb.append(linkSpeed);
                map.put("wifi", sb.toString());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("mac: ");
                sb2.append(wifiInfo.getMacAddress());
                ah.a(sb2.toString());
                StringBuilder sb3 = new StringBuilder();
                sb3.append("ip: ");
                sb3.append(wifiInfo.getIpAddress());
                ah.a(sb3.toString());
                StringBuilder sb4 = new StringBuilder();
                sb4.append("wifi: ");
                sb4.append(bssid);
                sb4.append(",");
                sb4.append(ssid);
                sb4.append(",");
                sb4.append(rssi);
                sb4.append(",");
                sb4.append(networkId);
                sb4.append(",");
                sb4.append(linkSpeed);
                ah.a(sb4.toString());
            }
            if (cVar.hy != null) {
                int i = 1;
                for (ScanResult scanResult : cVar.hy) {
                    if (i < 6) {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append(scanResult.level);
                        sb5.append("");
                        ah.a(sb5.toString());
                        String str = scanResult.BSSID;
                        if (str == null) {
                            str = "";
                        }
                        String str2 = scanResult.SSID;
                        if (str2 == null) {
                            str2 = "";
                        }
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append("wifi");
                        sb6.append(i);
                        String sb7 = sb6.toString();
                        StringBuilder sb8 = new StringBuilder();
                        sb8.append(str);
                        sb8.append(",");
                        sb8.append(str2);
                        sb8.append(",");
                        sb8.append(scanResult.level);
                        map.put(sb7, sb8.toString());
                        StringBuilder sb9 = new StringBuilder();
                        sb9.append("wifi");
                        sb9.append(i);
                        sb9.append(": ");
                        sb9.append(str);
                        sb9.append(",");
                        sb9.append(str2);
                        sb9.append(",");
                        sb9.append(scanResult.level);
                        ah.a(sb9.toString());
                        i++;
                    }
                }
            }
        }
    }

    private void c(@NonNull Context context, @NonNull Map<String, String> map) {
        if (this.gZ && h("android.permission.ACCESS_COARSE_LOCATION", context)) {
            a aVar = new a(context);
            String str = ",";
            if (aVar.hb != null) {
                int i = 0;
                while (i < aVar.hb.size()) {
                    StringBuilder sb = new StringBuilder();
                    b bVar = (b) aVar.hb.get(i);
                    if (!"cdma".equals(bVar.type)) {
                        sb.append(bVar.type);
                        sb.append(str);
                        sb.append(bVar.hc);
                        sb.append(str);
                        sb.append(bVar.hd);
                        sb.append(str);
                        sb.append(bVar.mcc);
                        sb.append(str);
                        sb.append(bVar.mnc);
                        sb.append(str);
                        sb.append(bVar.level);
                        sb.append(str);
                        sb.append(bVar.he);
                        sb.append(str);
                        sb.append(bVar.hf);
                        sb.append(str);
                        sb.append(bVar.hg);
                        sb.append(str);
                        sb.append(bVar.hh);
                        sb.append(str);
                        sb.append(bVar.hi);
                        sb.append(str);
                        sb.append(bVar.hj);
                        sb.append(str);
                        sb.append(bVar.hk);
                    } else {
                        sb.append(bVar.hl);
                        sb.append(str);
                        sb.append(bVar.hm);
                        sb.append(str);
                        sb.append(bVar.hn);
                        sb.append(str);
                        sb.append(bVar.ho);
                        sb.append(str);
                        sb.append(bVar.hp);
                        sb.append(str);
                        sb.append(bVar.hq);
                        sb.append(str);
                        sb.append(bVar.level);
                        sb.append(str);
                        sb.append(bVar.hr);
                        sb.append(str);
                        sb.append(bVar.hf);
                        sb.append(str);
                        sb.append(bVar.hs);
                        sb.append(str);
                        sb.append(bVar.he);
                        sb.append(str);
                        sb.append(bVar.ht);
                        sb.append(str);
                        sb.append(bVar.hu);
                        sb.append(str);
                        sb.append(bVar.hv);
                        sb.append(str);
                        sb.append(bVar.hw);
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("cell");
                    sb2.append(i != 0 ? Integer.valueOf(i) : "");
                    map.put(sb2.toString(), sb.toString());
                    i++;
                }
            }
        }
    }
}
