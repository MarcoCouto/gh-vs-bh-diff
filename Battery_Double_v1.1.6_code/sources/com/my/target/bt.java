package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

/* compiled from: JsStatEvent */
public class bt extends bl {
    @NonNull
    private final List<String> cm;
    @Nullable
    private String cn;

    public bt(@NonNull List<String> list, @Nullable String str) {
        super("onStat");
        this.cm = list;
        this.cn = str;
    }

    @NonNull
    public List<String> aI() {
        return this.cm;
    }
}
