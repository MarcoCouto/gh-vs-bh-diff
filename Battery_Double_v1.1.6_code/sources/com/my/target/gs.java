package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.er.a;

/* compiled from: InterstitialPromoVideoView */
public interface gs extends gt {
    void A(boolean z);

    void G(int i);

    void a(@NonNull cn cnVar);

    void destroy();

    void ei();

    void finish();

    @NonNull
    gw getPromoMediaView();

    boolean isPaused();

    boolean isPlaying();

    void pause();

    void play();

    void resume();

    void setMediaListener(@Nullable a aVar);

    void setTimeChanged(float f);

    void stop(boolean z);
}
