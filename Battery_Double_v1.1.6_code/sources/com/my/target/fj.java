package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.tapjoy.TapjoyConstants;

/* compiled from: NetworkInfoDataProvider */
public class fj extends ff {
    fj() {
    }

    @WorkerThread
    @SuppressLint({"MissingPermission"})
    public synchronized void collectData(@NonNull Context context) {
        removeAll();
        NetworkInfo networkInfo = null;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo != null) {
                addParam("connection", networkInfo.getTypeName());
                a(networkInfo);
            }
        } catch (SecurityException unused) {
            ah.a("No permissions for access to network state");
        }
        return;
    }

    private void a(@NonNull NetworkInfo networkInfo) {
        if (networkInfo.getType() == 0) {
            addParam(TapjoyConstants.TJC_CONNECTION_TYPE, networkInfo.getSubtypeName() != null ? networkInfo.getSubtypeName() : "");
        } else {
            addParam(TapjoyConstants.TJC_CONNECTION_TYPE, networkInfo.getTypeName() != null ? networkInfo.getTypeName() : "");
        }
    }
}
