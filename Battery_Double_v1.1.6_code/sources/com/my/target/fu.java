package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

@SuppressLint({"AppCompatCustomView"})
/* compiled from: BorderedTextView */
public class fu extends TextView {
    @NonNull
    private final GradientDrawable ia;
    private final int ib;

    public fu(@NonNull Context context) {
        this(context, null);
    }

    public fu(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public fu(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.ia = new GradientDrawable();
        this.ia.setStroke(0, -13421773);
        this.ia.setColor(0);
        this.ib = (int) TypedValue.applyDimension(1, 2.0f, context.getResources().getDisplayMetrics());
    }

    public void f(int i, int i2) {
        a(i, i2, 0);
    }

    public void a(int i, int i2, int i3) {
        this.ia.setStroke(i, i2);
        this.ia.setCornerRadius((float) i3);
        invalidate();
    }

    public void setBackgroundColor(int i) {
        this.ia.setColor(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.ia.setBounds(getPaddingLeft() - this.ib, getPaddingTop(), getWidth(), getHeight());
        this.ia.draw(canvas);
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth() + (this.ib * 2), getMeasuredHeight());
    }
}
