package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ProgressStat */
public class dg extends dh {
    private float ed = -1.0f;
    private float value = -1.0f;

    @NonNull
    public static dg M(@NonNull String str) {
        return new dg(str);
    }

    private dg(@NonNull String str) {
        super("playheadReachedValue", str);
    }

    public float cq() {
        return this.value;
    }

    public void i(float f) {
        this.value = f;
    }

    public float cr() {
        return this.ed;
    }

    public void j(float f) {
        this.ed = f;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ProgressStat{");
        sb.append("value=");
        sb.append(this.value);
        sb.append(", pvalue=");
        sb.append(this.ed);
        sb.append('}');
        return sb.toString();
    }
}
