package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ResearchViewabilityTracker */
public class ad {
    @NonNull
    private final ArrayList<cg> r = new ArrayList<>();
    private float s = -1.0f;
    private long u;
    @Nullable
    private WeakReference<View> viewWeakReference;

    @NonNull
    public static ad b(@NonNull di diVar) {
        return new ad(diVar);
    }

    public void setView(@Nullable View view) {
        if (view != null || this.viewWeakReference == null) {
            this.viewWeakReference = new WeakReference<>(view);
        } else {
            this.viewWeakReference.clear();
        }
    }

    private ad(@NonNull di diVar) {
        Iterator it = diVar.cu().iterator();
        while (it.hasNext()) {
            de deVar = (de) it.next();
            if (deVar instanceof cg) {
                this.r.add((cg) deVar);
            }
        }
    }

    public void c(int i) {
        float f = (float) i;
        if (f != this.s) {
            if (!d(i)) {
                rewind();
            }
            Context context = null;
            double d = Utils.DOUBLE_EPSILON;
            if (this.viewWeakReference != null) {
                View view = (View) this.viewWeakReference.get();
                if (view != null) {
                    d = Cif.j(view);
                    context = view.getContext();
                }
            }
            a(d, i, context);
            this.s = f;
            this.u = System.currentTimeMillis();
        }
    }

    private void rewind() {
        Iterator it = this.r.iterator();
        while (it.hasNext()) {
            ((cg) it.next()).h(-1.0f);
        }
    }

    private boolean d(int i) {
        float f = (float) i;
        boolean z = false;
        if (f < this.s) {
            return false;
        }
        if (this.u <= 0) {
            return true;
        }
        if ((((long) (f - this.s)) * 1000) - (System.currentTimeMillis() - this.u) <= 1000) {
            z = true;
        }
        return z;
    }

    private void a(double d, int i, @Nullable Context context) {
        if (!this.r.isEmpty()) {
            if (context == null) {
                Iterator it = this.r.iterator();
                while (it.hasNext()) {
                    ((de) it.next()).h(-1.0f);
                }
                return;
            }
            b(d, i, context);
        }
    }

    private void b(double d, int i, @NonNull Context context) {
        ArrayList arrayList = new ArrayList();
        Iterator it = this.r.iterator();
        while (it.hasNext()) {
            cg cgVar = (cg) it.next();
            int by = cgVar.by();
            int bz = cgVar.bz();
            if (!(by <= i && (bz == 0 || bz >= i))) {
                cgVar.h(-1.0f);
            } else if (((double) cgVar.cy()) > d) {
                cgVar.h(-1.0f);
            } else {
                if (cgVar.co() >= 0.0f) {
                    float f = (float) i;
                    if (f > cgVar.co()) {
                        if (f - cgVar.co() >= cgVar.getDuration()) {
                            arrayList.add(cgVar);
                            it.remove();
                        }
                    }
                }
                cgVar.h((float) i);
            }
        }
        if (!arrayList.isEmpty()) {
            ib.a((List<dh>) arrayList, context);
        }
    }
}
