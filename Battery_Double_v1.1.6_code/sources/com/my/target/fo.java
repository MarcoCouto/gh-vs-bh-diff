package com.my.target;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.FrameLayout;

/* compiled from: AdDialog */
public class fo extends Dialog {
    @NonNull
    private final a hH;

    /* compiled from: AdDialog */
    public interface a {
        void D();

        void a(@NonNull fo foVar, @NonNull FrameLayout frameLayout);

        void a(boolean z);
    }

    @NonNull
    public static fo a(@NonNull a aVar, @NonNull Context context) {
        return new fo(aVar, context);
    }

    private fo(@NonNull a aVar, @NonNull Context context) {
        super(context);
        this.hH = aVar;
    }

    public void dO() {
        Window window = getWindow();
        if (window != null) {
            window.setFlags(1024, 1024);
        }
    }

    public void dismiss() {
        super.dismiss();
        this.hH.D();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        FrameLayout frameLayout = new FrameLayout(getContext());
        setContentView(frameLayout);
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        this.hH.a(this, frameLayout);
        super.onCreate(bundle);
    }

    public void onWindowFocusChanged(boolean z) {
        this.hH.a(z);
        super.onWindowFocusChanged(z);
    }
}
