package com.my.target;

import android.support.annotation.NonNull;
import com.ironsource.sdk.precache.DownloadManager;
import org.json.JSONObject;

/* compiled from: StandardAdSectionParser */
public class el {
    @NonNull
    public static el cN() {
        return new el();
    }

    private el() {
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull dd ddVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
        if (optJSONObject != null) {
            b(optJSONObject, ddVar);
        }
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull dd ddVar) {
        ddVar.p(jSONObject.optBoolean("hasAdditionalAds", ddVar.cl()));
    }
}
