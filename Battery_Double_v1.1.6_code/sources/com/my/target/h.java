package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Iterator;

/* compiled from: InstreamAdResultProcessor */
public class h extends d<cw> {
    @NonNull
    public static h g() {
        return new h();
    }

    private h() {
    }

    @Nullable
    public cw a(@NonNull cw cwVar, @NonNull a aVar, @NonNull Context context) {
        Iterator it = cwVar.bK().iterator();
        while (it.hasNext()) {
            ((da) it.next()).bY();
        }
        return cwVar;
    }
}
