package com.my.target;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.tapjoy.TJAdUnitConstants.String;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: MraidPresenter */
public class fa implements fb, com.my.target.fo.a {
    @Nullable
    cs bL;
    @NonNull
    final Context context;
    @Nullable
    gc cr;
    @Nullable
    fw fm;
    @NonNull
    final bx fn;
    @NonNull
    private final bu fo;
    @NonNull
    private final WeakReference<Activity> fp;
    @NonNull
    String fq;
    boolean fu;
    @NonNull
    private final fx gc;
    @NonNull
    private final a gd;
    @NonNull
    private final com.my.target.bu.b ge;
    @NonNull
    final com.my.target.fw.a gf;
    @Nullable
    bu gg;
    @Nullable
    private gc gh;
    @Nullable
    com.my.target.fb.a gi;
    @Nullable
    c gj;
    boolean gk;
    @Nullable
    private Uri gl;
    @Nullable
    fo gm;
    @Nullable
    ViewGroup gn;
    @Nullable
    private e go;
    @Nullable
    f gp;

    /* compiled from: MraidPresenter */
    class a implements OnLayoutChangeListener {
        @NonNull
        private final bu fo;

        a(bu buVar) {
            this.fo = buVar;
        }

        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            fa.this.gp = null;
            fa.this.dk();
            this.fo.a(fa.this.fn);
        }
    }

    /* compiled from: MraidPresenter */
    class b implements com.my.target.fw.a {
        private b() {
        }

        public void onClose() {
            if (fa.this.gm != null) {
                fa.this.gm.dismiss();
            }
        }
    }

    /* compiled from: MraidPresenter */
    public interface c {
        void a(float f, float f2, @NonNull cs csVar, @NonNull Context context);

        void a(@NonNull String str, @NonNull cs csVar, @NonNull Context context);

        void ac();

        void ae();

        void af();

        void e(@NonNull String str);
    }

    @VisibleForTesting
    /* compiled from: MraidPresenter */
    static class d implements Runnable {
        @NonNull
        private cs bL;
        @NonNull
        private Context context;
        @NonNull
        bu gg;
        @NonNull
        private Uri gl;
        /* access modifiers changed from: private */
        @NonNull
        public fo gm;

        d(@NonNull cs csVar, @NonNull fo foVar, @NonNull Uri uri, @NonNull bu buVar, @NonNull Context context2) {
            this.bL = csVar;
            this.context = context2.getApplicationContext();
            this.gm = foVar;
            this.gl = uri;
            this.gg = buVar;
        }

        public void run() {
            dk cz = dk.cz();
            cz.f(this.gl.toString(), this.context);
            final String g = dw.g(this.bL.getMraidJs(), (String) cz.cE());
            ai.c(new Runnable() {
                public void run() {
                    if (!TextUtils.isEmpty(g)) {
                        d.this.gg.i(g);
                        return;
                    }
                    d.this.gg.a("expand", "Failed to handling mraid");
                    d.this.gm.dismiss();
                }
            });
        }
    }

    /* compiled from: MraidPresenter */
    class e implements com.my.target.bu.b {
        @NonNull
        private final bu gt;
        private final String gu;

        public void aL() {
        }

        e(bu buVar, @NonNull String str) {
            this.gt = buVar;
            this.gu = str;
        }

        public void c(@NonNull bu buVar) {
            StringBuilder sb = new StringBuilder();
            sb.append("onPageLoaded callback from ");
            sb.append(buVar == fa.this.gg ? " second " : " primary ");
            sb.append(ParametersKeys.WEB_VIEW);
            ah.a(sb.toString());
            ArrayList arrayList = new ArrayList();
            if (fa.this.dl()) {
                arrayList.add("'inlineVideo'");
            }
            arrayList.add("'vpaid'");
            buVar.a(arrayList);
            buVar.j(this.gu);
            buVar.k(buVar.isVisible());
            if (fa.this.gm == null || !fa.this.gm.isShowing()) {
                fa.this.aa("default");
            } else {
                fa.this.aa("expanded");
            }
            buVar.aJ();
            if (buVar != fa.this.gg && fa.this.gj != null) {
                fa.this.gj.ac();
            }
        }

        public void onVisibilityChanged(boolean z) {
            if (!z || fa.this.gm == null) {
                this.gt.k(z);
            }
        }

        public boolean a(@NonNull String str, @NonNull JsResult jsResult) {
            StringBuilder sb = new StringBuilder();
            sb.append("JS Alert: ");
            sb.append(str);
            ah.a(sb.toString());
            jsResult.confirm();
            return true;
        }

        public boolean a(@NonNull ConsoleMessage consoleMessage, @NonNull bu buVar) {
            StringBuilder sb = new StringBuilder();
            sb.append("Console message: from ");
            sb.append(buVar == fa.this.gg ? " second " : " primary ");
            sb.append("webview: ");
            sb.append(consoleMessage.message());
            ah.a(sb.toString());
            return true;
        }

        public void onClose() {
            if (fa.this.gm != null) {
                fa.this.gm.dismiss();
            }
        }

        public void l(boolean z) {
            fa.this.gk = z;
            if (fa.this.fq.equals("expanded") && fa.this.fm != null) {
                fa.this.fm.setCloseVisible(!z);
                if (!z) {
                    fa.this.fm.setOnCloseListener(fa.this.gf);
                }
            }
        }

        public boolean a(boolean z, bw bwVar) {
            ah.a("Orientation properties isn't supported in standard banners");
            return false;
        }

        public void b(@NonNull Uri uri) {
            if (fa.this.gi != null && fa.this.bL != null) {
                fa.this.gi.a(fa.this.bL, uri.toString());
            }
        }

        public void aM() {
            fa.this.fu = true;
        }

        public boolean o(@NonNull String str) {
            if (!fa.this.fu) {
                this.gt.a("vpaidEvent", "Calling VPAID command before VPAID init");
                return false;
            }
            if (!(fa.this.gj == null || fa.this.bL == null)) {
                fa.this.gj.a(str, fa.this.bL, fa.this.context);
            }
            return true;
        }

        public boolean b(float f, float f2) {
            if (!fa.this.fu) {
                this.gt.a("playheadEvent", "Calling VPAID command before VPAID init");
                return false;
            }
            if (f >= 0.0f && f2 >= 0.0f && fa.this.gj != null && fa.this.bL != null) {
                fa.this.gj.a(f, f2, fa.this.bL, fa.this.context);
            }
            return true;
        }

        public boolean c(@Nullable Uri uri) {
            return fa.this.d(uri);
        }

        public boolean a(int i, int i2, int i3, int i4, boolean z, int i5) {
            int i6 = i;
            int i7 = i2;
            boolean z2 = z;
            fa.this.gp = new f();
            if (fa.this.gn == null) {
                ah.a("Unable to set resize properties: container view for resize is not defined");
                this.gt.a("setResizeProperties", "container view for resize is not defined");
                fa.this.gp = null;
                return false;
            } else if (i6 < 50 || i7 < 50) {
                ah.a("Unable to set resize properties: properties cannot be less than closeable container");
                this.gt.a("setResizeProperties", "properties cannot be less than closeable container");
                fa.this.gp = null;
                return false;
            } else {
                ic P = ic.P(fa.this.context);
                fa.this.gp.u(z2);
                fa.this.gp.a(P.M(i), P.M(i7), P.M(i3), P.M(i4), i5);
                if (!z2) {
                    Rect rect = new Rect();
                    fa.this.gn.getGlobalVisibleRect(rect);
                    if (!fa.this.gp.c(rect)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unable to set resize properties: allowOffscreen is false, maxSize is (");
                        sb.append(rect.width());
                        sb.append(",");
                        sb.append(rect.height());
                        sb.append(") resize properties: (");
                        sb.append(fa.this.gp.dH());
                        sb.append(",");
                        sb.append(fa.this.gp.dI());
                        sb.append(")");
                        ah.a(sb.toString());
                        this.gt.a("setResizeProperties", "resize properties with allowOffscreen false out of viewport");
                        fa.this.gp = null;
                        return false;
                    }
                }
                return true;
            }
        }

        public boolean aN() {
            if (!fa.this.fq.equals("default")) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to resize: wrong state for resize: ");
                sb.append(fa.this.fq);
                ah.a(sb.toString());
                bu buVar = this.gt;
                String str = MraidJsMethods.RESIZE;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("wrong state for resize ");
                sb2.append(fa.this.fq);
                buVar.a(str, sb2.toString());
                return false;
            } else if (fa.this.gp == null) {
                ah.a("Unable to resize: resize properties not set");
                this.gt.a(MraidJsMethods.RESIZE, "resize properties not set");
                return false;
            } else if (fa.this.gn == null || fa.this.cr == null) {
                ah.a("Unable to resize: views not initialized");
                this.gt.a(MraidJsMethods.RESIZE, "views not initialized");
                return false;
            } else if (!fa.this.gp.a(fa.this.gn, fa.this.cr)) {
                ah.a("Unable to resize: views not visible");
                this.gt.a(MraidJsMethods.RESIZE, "views not visible");
                return false;
            } else {
                fa.this.fm = new fw(fa.this.context);
                fa.this.gp.a(fa.this.fm);
                if (!fa.this.gp.b(fa.this.fm)) {
                    ah.a("Unable to resize: close button is out of visible range");
                    this.gt.a(MraidJsMethods.RESIZE, "close button is out of visible range");
                    fa.this.fm = null;
                    return false;
                }
                ViewGroup viewGroup = (ViewGroup) fa.this.cr.getParent();
                if (viewGroup != null) {
                    viewGroup.removeView(fa.this.cr);
                }
                fa.this.fm.addView(fa.this.cr, new LayoutParams(-1, -1));
                fa.this.fm.setOnCloseListener(new com.my.target.fw.a() {
                    public void onClose() {
                        e.this.dG();
                    }
                });
                fa.this.gn.addView(fa.this.fm);
                fa.this.aa("resized");
                if (fa.this.gj != null) {
                    fa.this.gj.ae();
                }
                return true;
            }
        }

        /* access modifiers changed from: 0000 */
        public void dG() {
            if (fa.this.fm != null && fa.this.cr != null) {
                if (fa.this.fm.getParent() != null) {
                    ((ViewGroup) fa.this.fm.getParent()).removeView(fa.this.fm);
                    fa.this.fm.removeAllViews();
                    fa.this.b(fa.this.cr);
                    fa.this.aa("default");
                    fa.this.fm.setOnCloseListener(null);
                    fa.this.fm = null;
                }
                if (fa.this.gj != null) {
                    fa.this.gj.af();
                }
            }
        }
    }

    /* compiled from: MraidPresenter */
    public static class f {
        private int gA;
        private int gB;
        @Nullable
        private Rect gC;
        @Nullable
        private Rect gD;
        private int gE;
        private int gF;
        private boolean gw = true;
        private int gx;
        private int gy;
        private int gz;

        public int dH() {
            return this.gz;
        }

        /* access modifiers changed from: 0000 */
        public void u(boolean z) {
            this.gw = z;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, int i2, int i3, int i4, int i5) {
            this.gz = i;
            this.gA = i2;
            this.gx = i3;
            this.gy = i4;
            this.gB = i5;
        }

        /* access modifiers changed from: 0000 */
        public boolean c(@NonNull Rect rect) {
            return this.gz <= rect.width() && this.gA <= rect.height();
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull fw fwVar) {
            if (this.gD == null || this.gC == null) {
                ah.a("Setup views before resizing");
                return;
            }
            this.gE = (this.gD.top - this.gC.top) + this.gy;
            this.gF = (this.gD.left - this.gC.left) + this.gx;
            if (!this.gw) {
                if (this.gE + this.gA > this.gC.height()) {
                    ah.a("Try to reposition creative vertically because of resize allowOffscreen:false and out of max size properties");
                    this.gE = this.gC.height() - this.gA;
                }
                if (this.gF + this.gz > this.gC.width()) {
                    ah.a("Try to reposition creative horizontally because of resize allowOffscreen:false and out of max size properties");
                    this.gF = this.gC.width() - this.gz;
                }
            }
            LayoutParams layoutParams = new LayoutParams(this.gz, this.gA);
            layoutParams.topMargin = this.gE;
            layoutParams.leftMargin = this.gF;
            fwVar.setLayoutParams(layoutParams);
            fwVar.setCloseGravity(this.gB);
            fwVar.setCloseVisible(false);
        }

        public int dI() {
            return this.gA;
        }

        /* access modifiers changed from: 0000 */
        public boolean a(@NonNull ViewGroup viewGroup, @NonNull gc gcVar) {
            this.gC = new Rect();
            this.gD = new Rect();
            return viewGroup.getGlobalVisibleRect(this.gC) && gcVar.getGlobalVisibleRect(this.gD);
        }

        /* access modifiers changed from: 0000 */
        public boolean b(@NonNull fw fwVar) {
            if (this.gC == null) {
                return false;
            }
            Rect rect = new Rect(this.gF, this.gE, this.gC.right, this.gC.bottom);
            Rect rect2 = new Rect(this.gF, this.gE, this.gF + this.gz, this.gE + this.gA);
            Rect rect3 = new Rect();
            fwVar.a(this.gB, rect2, rect3);
            return rect.contains(rect3);
        }
    }

    @NonNull
    public static fa f(@NonNull ViewGroup viewGroup) {
        return new fa(viewGroup);
    }

    @VisibleForTesting
    fa(@NonNull bu buVar, @NonNull gc gcVar, @NonNull fx fxVar, @NonNull ViewGroup viewGroup) {
        this.gf = new b();
        this.fo = buVar;
        this.cr = gcVar;
        this.gc = fxVar;
        this.context = viewGroup.getContext();
        if (this.context instanceof Activity) {
            this.fp = new WeakReference<>((Activity) this.context);
            this.gn = (ViewGroup) ((Activity) this.context).getWindow().getDecorView().findViewById(16908290);
        } else {
            this.fp = new WeakReference<>(null);
            View rootView = viewGroup.getRootView();
            if (rootView != null) {
                this.gn = (ViewGroup) rootView.findViewById(16908290);
                if (this.gn == null) {
                    this.gn = (ViewGroup) rootView;
                }
            }
        }
        this.fq = "loading";
        this.fn = bx.p(this.context);
        b(gcVar);
        this.ge = new e(buVar, String.INLINE);
        buVar.a(this.ge);
        this.gd = new a(buVar);
        gcVar.addOnLayoutChangeListener(this.gd);
    }

    private fa(@NonNull ViewGroup viewGroup) {
        this(bu.h(String.INLINE), new gc(viewGroup.getContext()), new fx(viewGroup.getContext()), viewGroup);
    }

    public void a(@NonNull fo foVar, @NonNull FrameLayout frameLayout) {
        this.gm = foVar;
        this.fm = new fw(this.context);
        a(this.fm, frameLayout);
    }

    public void a(boolean z) {
        if (this.gg != null) {
            this.gg.k(z);
        } else {
            this.fo.k(z);
        }
        if (this.gh == null) {
            return;
        }
        if (z) {
            this.gh.onResume();
        } else {
            this.gh.x(false);
        }
    }

    public void D() {
        this.gc.setVisibility(0);
        if (this.gl != null) {
            this.gl = null;
            if (this.gg != null) {
                this.gg.k(false);
                this.gg.k("hidden");
                this.gg.detach();
                this.gg = null;
                this.fo.k(true);
            }
            if (this.gh != null) {
                this.gh.x(true);
                if (this.gh.getParent() != null) {
                    ((ViewGroup) this.gh.getParent()).removeView(this.gh);
                }
                this.gh.destroy();
                this.gh = null;
            }
        } else if (this.cr != null) {
            if (this.cr.getParent() != null) {
                ((ViewGroup) this.cr.getParent()).removeView(this.cr);
            }
            b(this.cr);
        }
        if (!(this.fm == null || this.fm.getParent() == null)) {
            ((ViewGroup) this.fm.getParent()).removeView(this.fm);
        }
        this.fm = null;
        aa("default");
        if (this.gj != null) {
            this.gj.af();
        }
        dk();
        this.fo.a(this.fn);
        this.cr.onResume();
    }

    public void a(@NonNull cs csVar) {
        this.bL = csVar;
        String mraidSource = csVar.getMraidSource();
        if (mraidSource == null || this.cr == null) {
            Y("failed to load, failed MRAID initialization");
            return;
        }
        this.fo.a(this.cr);
        this.fo.i(mraidSource);
    }

    public void start() {
        if (this.gi != null && this.bL != null) {
            this.gi.a(this.bL);
        }
    }

    public void stop() {
        if ((this.gm == null || this.gg != null) && this.cr != null) {
            this.cr.x(true);
        }
    }

    public void pause() {
        if ((this.gm == null || this.gg != null) && this.cr != null) {
            this.cr.x(false);
        }
    }

    public void resume() {
        if ((this.gm == null || this.gg != null) && this.cr != null) {
            this.cr.onResume();
        }
    }

    public void destroy() {
        aa("hidden");
        a((c) null);
        a((com.my.target.fb.a) null);
        this.fo.detach();
        if (this.fm != null) {
            this.fm.removeAllViews();
            this.fm.setOnCloseListener(null);
            ViewParent parent = this.fm.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(this.fm);
            }
            this.fm = null;
        }
        if (this.cr != null) {
            this.cr.x(true);
            if (this.cr.getParent() != null) {
                ((ViewGroup) this.cr.getParent()).removeView(this.cr);
            }
            this.cr.destroy();
            this.cr = null;
        }
        if (this.gg != null) {
            this.gg.detach();
            this.gg = null;
        }
        if (this.gh != null) {
            this.gh.x(true);
            if (this.gh.getParent() != null) {
                ((ViewGroup) this.gh.getParent()).removeView(this.gh);
            }
            this.gh.destroy();
            this.gh = null;
        }
    }

    @NonNull
    public fx dF() {
        return this.gc;
    }

    public void a(@Nullable com.my.target.fb.a aVar) {
        this.gi = aVar;
    }

    public void a(@Nullable c cVar) {
        this.gj = cVar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull fw fwVar, @NonNull FrameLayout frameLayout) {
        this.gc.setVisibility(8);
        frameLayout.addView(fwVar, new ViewGroup.LayoutParams(-1, -1));
        if (this.gl != null) {
            this.gg = bu.h(String.INLINE);
            this.gh = new gc(this.context);
            a(this.gg, this.gh, fwVar);
        } else if (!(this.cr == null || this.cr.getParent() == null)) {
            ((ViewGroup) this.cr.getParent()).removeView(this.cr);
            fwVar.addView(this.cr, new ViewGroup.LayoutParams(-1, -1));
            aa("expanded");
        }
        fwVar.setCloseVisible(!this.gk);
        fwVar.setOnCloseListener(this.gf);
        if (this.gj != null && this.gl == null) {
            this.gj.ae();
        }
        ah.a("MRAIDMRAID dialog create");
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull bu buVar, @NonNull gc gcVar, @NonNull fw fwVar) {
        this.go = new e(buVar, String.INLINE);
        buVar.a((com.my.target.bu.b) this.go);
        fwVar.addView(gcVar, new ViewGroup.LayoutParams(-1, -1));
        buVar.a(gcVar);
        if (this.gm == null) {
            return;
        }
        if (this.bL == null || this.gl == null) {
            this.gm.dismiss();
            return;
        }
        d dVar = new d(this.bL, this.gm, this.gl, buVar, this.context);
        ai.a(dVar);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void dk() {
        int[] iArr = new int[2];
        DisplayMetrics displayMetrics = this.context.getResources().getDisplayMetrics();
        this.fn.a(displayMetrics.widthPixels, displayMetrics.heightPixels);
        if (this.gn != null) {
            this.gn.getLocationOnScreen(iArr);
            this.fn.c(iArr[0], iArr[1], iArr[0] + this.gn.getMeasuredWidth(), iArr[1] + this.gn.getMeasuredHeight());
        }
        if (!this.fq.equals("expanded") && !this.fq.equals("resized")) {
            this.gc.getLocationOnScreen(iArr);
            this.fn.a(iArr[0], iArr[1], iArr[0] + this.gc.getMeasuredWidth(), iArr[1] + this.gc.getMeasuredHeight());
        }
        if (this.gh != null) {
            this.gh.getLocationOnScreen(iArr);
            this.fn.b(iArr[0], iArr[1], iArr[0] + this.gh.getMeasuredWidth(), iArr[1] + this.gh.getMeasuredHeight());
        } else if (this.cr != null) {
            this.cr.getLocationOnScreen(iArr);
            this.fn.b(iArr[0], iArr[1], iArr[0] + this.cr.getMeasuredWidth(), iArr[1] + this.cr.getMeasuredHeight());
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull gc gcVar) {
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.gravity = 1;
        this.gc.addView(gcVar);
        gcVar.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public boolean dl() {
        Activity activity = (Activity) this.fp.get();
        if (activity == null || this.cr == null) {
            return false;
        }
        return ic.a(activity, (View) this.cr);
    }

    /* access modifiers changed from: 0000 */
    public void aa(@NonNull String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("MRAID state set to ");
        sb.append(str);
        ah.a(sb.toString());
        this.fq = str;
        this.fo.k(str);
        if (this.gg != null) {
            this.gg.k(str);
        }
        if ("hidden".equals(str)) {
            ah.a("MraidPresenter: Mraid on close");
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean d(@Nullable Uri uri) {
        if (this.cr == null) {
            ah.a("Cannot expand: webview destroyed");
            return false;
        } else if (!this.fq.equals("default") && !this.fq.equals("resized")) {
            return false;
        } else {
            this.gl = uri;
            fo.a(this, this.context).show();
            return true;
        }
    }

    private void Y(@NonNull String str) {
        if (this.gj != null) {
            this.gj.e(str);
        }
    }
}
