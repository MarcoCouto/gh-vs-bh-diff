package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;

/* compiled from: FramedImageView */
public class gr extends FrameLayout {
    @NonNull
    private final ImageView it;

    public gr(@NonNull Context context) {
        super(context);
        this.it = new ImageView(context);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        addView(this.it, layoutParams);
    }

    public void setImageBitmap(@Nullable Bitmap bitmap) {
        this.it.setImageBitmap(bitmap);
    }
}
