package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public interface InterstitialAdExtendedListener extends InterstitialAdListener, RewardedAdListener {
    void onInterstitialActivityDestroyed();
}
