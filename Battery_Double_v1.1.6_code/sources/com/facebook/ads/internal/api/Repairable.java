package com.facebook.ads.internal.api;

import android.support.annotation.Keep;

@Keep
public interface Repairable {
    void repair(Throwable th);
}
