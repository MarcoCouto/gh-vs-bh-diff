package com.facebook.ads.internal.api;

import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import com.facebook.ads.Ad;
import com.facebook.ads.ExtraHints;
import com.facebook.ads.InstreamVideoAdListener;
import com.facebook.ads.InstreamVideoAdView.InstreamVideoLoadAdConfig;
import com.facebook.ads.InstreamVideoAdView.InstreamVideoLoadConfigBuilder;

@Keep
public interface InstreamVideoAdViewApi extends Ad {
    InstreamVideoLoadConfigBuilder buildLoadAdConfig();

    void destroy();

    String getPlacementId();

    @Nullable
    Bundle getSaveInstanceState();

    boolean isAdInvalidated();

    boolean isAdLoaded();

    void loadAd();

    void loadAd(InstreamVideoLoadAdConfig instreamVideoLoadAdConfig);

    @Deprecated
    void loadAdFromBid(String str);

    void setAdListener(@Nullable InstreamVideoAdListener instreamVideoAdListener);

    @Deprecated
    void setExtraHints(ExtraHints extraHints);

    void setIsAdLoaded(boolean z);

    boolean show();
}
