package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.view.View;
import com.facebook.ads.NativeAdBase.NativeComponentTag;

@Keep
public interface NativeComponentTagApi {
    void tagView(View view, NativeComponentTag nativeComponentTag);
}
