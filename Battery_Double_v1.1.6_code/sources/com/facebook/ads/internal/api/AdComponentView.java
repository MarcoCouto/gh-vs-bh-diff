package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.support.annotation.UiThread;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

@Keep
@UiThread
public interface AdComponentView {
    void addView(View view);

    void addView(View view, int i);

    void addView(View view, int i, int i2);

    void addView(View view, int i, LayoutParams layoutParams);

    void addView(View view, LayoutParams layoutParams);

    void onWindowFocusChanged(boolean z);

    void setLayoutParams(LayoutParams layoutParams);
}
