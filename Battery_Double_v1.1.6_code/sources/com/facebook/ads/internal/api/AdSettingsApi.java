package com.facebook.ads.internal.api;

import android.content.Context;
import android.support.annotation.Keep;

@Keep
public interface AdSettingsApi {
    boolean isTestMode(Context context);

    void turnOnDebugger();
}
