package com.facebook.ads.internal.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import com.facebook.ads.internal.dynamicloading.DynamicLoaderFactory;

@Keep
public abstract class AdNativeComponentView extends RelativeLayout implements AdComponentView {
    /* access modifiers changed from: protected */
    @Nullable
    public AdComponentViewApi mAdComponentViewApi;
    private final AdComponentViewParentApi mAdComponentViewParentApi = new AdComponentViewParentApi() {
        public void setLayoutParams(LayoutParams layoutParams) {
            AdNativeComponentView.super.setLayoutParams(layoutParams);
        }

        public void onMeasure(int i, int i2) {
            AdNativeComponentView.super.onMeasure(i, i2);
        }

        public void setMeasuredDimension(int i, int i2) {
            AdNativeComponentView.super.setMeasuredDimension(i, i2);
        }

        public void onAttachedToWindow() {
            AdNativeComponentView.super.onAttachedToWindow();
        }

        public void onDetachedFromWindow() {
            AdNativeComponentView.super.onDetachedFromWindow();
        }

        public void bringChildToFront(View view) {
            AdNativeComponentView.super.bringChildToFront(view);
        }

        public void onWindowFocusChanged(boolean z) {
            AdNativeComponentView.super.onWindowFocusChanged(z);
        }

        public void onVisibilityChanged(View view, int i) {
            AdNativeComponentView.super.onVisibilityChanged(view, i);
        }

        public void addView(View view, int i, LayoutParams layoutParams) {
            AdNativeComponentView.super.addView(view, i, layoutParams);
        }

        public void addView(View view) {
            AdNativeComponentView.super.addView(view);
        }

        public void addView(View view, int i) {
            AdNativeComponentView.super.addView(view, i);
        }

        public void addView(View view, LayoutParams layoutParams) {
            AdNativeComponentView.super.addView(view, layoutParams);
        }

        public void addView(View view, int i, int i2) {
            AdNativeComponentView.super.addView(view, i, i2);
        }
    };

    public abstract View getAdContentsView();

    public AdNativeComponentView(Context context) {
        super(context);
    }

    public AdNativeComponentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AdNativeComponentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @RequiresApi(api = 21)
    public AdNativeComponentView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: protected */
    public void attachAdComponentViewApi(AdComponentViewApiProvider adComponentViewApiProvider) {
        if (!DynamicLoaderFactory.isFallbackMode()) {
            if (this.mAdComponentViewApi == null) {
                adComponentViewApiProvider.getAdComponentViewApi().onAttachedToView(this, this.mAdComponentViewParentApi);
                this.mAdComponentViewApi = adComponentViewApiProvider.getAdComponentViewApi();
                return;
            }
            throw new IllegalStateException("AdComponentViewApi can't be attached more then once.");
        }
    }

    public void addView(View view) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.addView(view);
        } else {
            super.addView(view);
        }
    }

    public void addView(View view, int i) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.addView(view, i);
        } else {
            super.addView(view, i);
        }
    }

    public void addView(View view, LayoutParams layoutParams) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.addView(view, layoutParams);
        } else {
            super.addView(view, layoutParams);
        }
    }

    public void addView(View view, int i, int i2) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.addView(view, i, i2);
        } else {
            super.addView(view, i, i2);
        }
    }

    public void addView(View view, int i, LayoutParams layoutParams) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.addView(view, i, layoutParams);
        } else {
            super.addView(view, i, layoutParams);
        }
    }

    public void setLayoutParams(LayoutParams layoutParams) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.setLayoutParams(layoutParams);
        } else {
            super.setLayoutParams(layoutParams);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.onMeasure(i, i2);
        } else {
            super.onMeasure(i, i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.onVisibilityChanged(view, i);
        } else {
            super.onVisibilityChanged(view, i);
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.onWindowFocusChanged(z);
        } else {
            super.onWindowFocusChanged(z);
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"MissingSuperCall"})
    public void onAttachedToWindow() {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.onAttachedToWindow();
        } else {
            super.onAttachedToWindow();
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"MissingSuperCall"})
    public void onDetachedFromWindow() {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.onDetachedFromWindow();
        } else {
            super.onDetachedFromWindow();
        }
    }

    public void bringChildToFront(View view) {
        if (this.mAdComponentViewApi != null) {
            this.mAdComponentViewApi.bringChildToFront(view);
        } else {
            super.bringChildToFront(view);
        }
    }
}
