package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.support.annotation.UiThread;
import android.view.View;

@Keep
@UiThread
public interface AdComponentViewParentApi extends AdComponentView {
    void bringChildToFront(View view);

    void onAttachedToWindow();

    void onDetachedFromWindow();

    void onMeasure(int i, int i2);

    void onVisibilityChanged(View view, int i);

    void setMeasuredDimension(int i, int i2);
}
