package com.facebook.ads.internal.api;

import android.support.annotation.Keep;
import android.support.annotation.UiThread;

@Keep
@UiThread
public interface AdComponentViewApiProvider {
    AdComponentViewApi getAdComponentViewApi();
}
