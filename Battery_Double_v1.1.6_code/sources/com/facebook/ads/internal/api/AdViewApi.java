package com.facebook.ads.internal.api;

import android.content.res.Configuration;
import android.support.annotation.Keep;
import android.support.annotation.UiThread;
import com.facebook.ads.Ad;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdView.AdViewLoadConfig;
import com.facebook.ads.AdView.AdViewLoadConfigBuilder;
import com.facebook.ads.ExtraHints;

@Keep
@UiThread
public interface AdViewApi extends AdViewParentApi, Ad {
    AdViewLoadConfigBuilder buildLoadAdConfig();

    void loadAd(AdViewLoadConfig adViewLoadConfig);

    void onConfigurationChanged(Configuration configuration);

    void setAdListener(AdListener adListener);

    @Deprecated
    void setExtraHints(ExtraHints extraHints);
}
