package com.facebook.ads;

import android.support.annotation.Keep;

@Keep
public interface RewardedVideoAdListener extends AdListener {
    void onLoggingImpression(Ad ad);

    void onRewardedVideoClosed();

    void onRewardedVideoCompleted();
}
