package com.mansoon.BatteryDouble.network.services;

import com.mansoon.BatteryDouble.models.ServerStatus;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GreenHubStatusService {
    @GET("status.json")
    Call<ServerStatus> getStatus();
}
