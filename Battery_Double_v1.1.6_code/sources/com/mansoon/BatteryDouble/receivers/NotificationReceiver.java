package com.mansoon.BatteryDouble.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.ReceiverCallNotAllowedException;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.Notifier;

public class NotificationReceiver extends BroadcastReceiver {
    private static final String TAG = LogUtils.makeLogTag(NotificationReceiver.class);

    public void onReceive(Context context, Intent intent) {
        LogUtils.LOGI(TAG, "onReceive called!");
        try {
            Notifier.updateStatusBar(context);
        } catch (ReceiverCallNotAllowedException e) {
            e.printStackTrace();
        }
    }
}
