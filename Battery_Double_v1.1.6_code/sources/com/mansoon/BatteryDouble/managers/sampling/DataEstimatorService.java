package com.mansoon.BatteryDouble.managers.sampling;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import com.github.mikephil.charting.utils.Utils;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.StatusEvent;
import com.mansoon.BatteryDouble.managers.storage.GreenHubDb;
import com.mansoon.BatteryDouble.models.data.BatteryUsage;
import com.mansoon.BatteryDouble.models.data.Sample;
import com.mansoon.BatteryDouble.network.CommunicationManager;
import com.mansoon.BatteryDouble.tasks.CheckNewMessagesTask;
import com.mansoon.BatteryDouble.tasks.ServerStatusTask;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.Notifier;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import org.greenrobot.eventbus.EventBus;

public class DataEstimatorService extends IntentService {
    private static final String TAG = LogUtils.makeLogTag(DataEstimatorService.class);
    private double mDistance;

    public DataEstimatorService() {
        super(TAG);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        WakeLock newWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(1, TAG);
        newWakeLock.acquire();
        Context applicationContext = getApplicationContext();
        if ((intent != null ? intent.getStringExtra("OriginalAction") : null) != null) {
            takeSampleIfBatteryLevelChanged(intent, applicationContext);
        }
        newWakeLock.release();
        if (intent != null) {
            DataEstimator.completeWakefulIntent(intent);
        }
    }

    private void takeSampleIfBatteryLevelChanged(Intent intent, Context context) {
        this.mDistance = intent.getDoubleExtra("distance", Utils.DOUBLE_EPSILON);
        if (Inspector.getCurrentBatteryLevel() > Utils.DOUBLE_EPSILON) {
            GreenHubDb greenHubDb = new GreenHubDb();
            Sample lastSample = greenHubDb.lastSample();
            if (intent.getAction().equals("android.intent.action.SCREEN_ON") || intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                LogUtils.LOGI(TAG, "Getting new usage details");
                getBatteryUsage(context, intent, greenHubDb, true);
                greenHubDb.close();
                return;
            }
            if (lastSample != null) {
                Inspector.setLastBatteryLevel(lastSample.realmGet$batteryLevel());
            }
            boolean z = Inspector.getLastBatteryLevel() != Inspector.getCurrentBatteryLevel();
            if (z) {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("The battery percentage changed. About to take a new sample (currentBatteryLevel=");
                sb.append(Inspector.getCurrentBatteryLevel());
                sb.append(", lastBatteryLevel=");
                sb.append(Inspector.getLastBatteryLevel());
                sb.append(")");
                LogUtils.LOGI(str, sb.toString());
                EventBus.getDefault().post(new StatusEvent(getString(R.string.event_new_sample)));
                getSample(context, intent, lastSample, greenHubDb);
                getBatteryUsage(context, intent, greenHubDb, false);
                boolean z2 = intent.getIntExtra("plugged", 0) != 0;
                if (SettingsUtils.isBatteryAlertsOn(context) && SettingsUtils.isChargeAlertsOn(context)) {
                    if (Inspector.getCurrentBatteryLevel() == 1.0d && z2) {
                        Notifier.batteryFullAlert(context);
                    } else if (Inspector.getCurrentBatteryLevel() == 0.2d) {
                        Notifier.batteryLowAlert(context);
                    }
                }
                if (Inspector.getLastBatteryLevel() == Utils.DOUBLE_EPSILON) {
                    String str2 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Last Battery Level = 0. Updating to BatteryLevel => ");
                    sb2.append(Inspector.getCurrentBatteryLevel());
                    LogUtils.LOGI(str2, sb2.toString());
                    Inspector.setLastBatteryLevel(Inspector.getCurrentBatteryLevel());
                }
            } else {
                String str3 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("No battery percentage change. BatteryLevel => ");
                sb3.append(Inspector.getCurrentBatteryLevel());
                LogUtils.LOGI(str3, sb3.toString());
            }
            if (CommunicationManager.uploadAttempts >= 3 || !SettingsUtils.isAutomaticUploadingAllowed(context) || !SettingsUtils.isServerUrlPresent(context) || !SettingsUtils.isDeviceRegistered(context) || !z) {
                greenHubDb.close();
                return;
            }
            new ServerStatusTask().execute(new Context[]{context});
            new CheckNewMessagesTask().execute(new Context[]{context});
            if (greenHubDb.count(Sample.class) >= ((long) SettingsUtils.fetchUploadRate(context)) && !CommunicationManager.isUploading) {
                new CommunicationManager(context, true).sendSamples();
            }
            greenHubDb.close();
        } else {
            LogUtils.LOGI(TAG, "current battery level = 0");
        }
    }

    private void getSample(Context context, Intent intent, Sample sample, GreenHubDb greenHubDb) {
        Sample sample2 = Inspector.getSample(context, intent, sample != null ? sample.realmGet$batteryState() : "Unknown");
        if (sample2 != null) {
            sample2.realmSet$distanceTraveled(this.mDistance);
            this.mDistance = Utils.DOUBLE_EPSILON;
        }
        if (sample2 != null && !sample2.realmGet$batteryState().equals("Unknown") && sample2.realmGet$batteryLevel() >= Utils.DOUBLE_EPSILON) {
            greenHubDb.saveSample(sample2);
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Took sample ");
            sb.append(sample2.realmGet$id());
            sb.append(" for ");
            sb.append(intent.getAction());
            LogUtils.LOGI(str, sb.toString());
        }
        EventBus.getDefault().post(new StatusEvent(context.getString(R.string.event_idle)));
    }

    private void getBatteryUsage(Context context, Intent intent, GreenHubDb greenHubDb, boolean z) {
        if (z) {
            intent.putExtras(DataEstimator.getBatteryChangedIntent(context).getExtras());
        }
        BatteryUsage batteryUsage = Inspector.getBatteryUsage(context, intent);
        if (batteryUsage != null && !batteryUsage.realmGet$state().equals("Unknown") && batteryUsage.realmGet$level() >= 0.0f) {
            greenHubDb.saveUsage(batteryUsage);
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Took usage details ");
            sb.append(batteryUsage.realmGet$id());
            sb.append(" for ");
            sb.append(intent.getAction());
            LogUtils.LOGI(str, sb.toString());
        }
    }
}
