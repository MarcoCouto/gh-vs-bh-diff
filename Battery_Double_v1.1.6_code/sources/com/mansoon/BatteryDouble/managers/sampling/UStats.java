package com.mansoon.BatteryDouble.managers.sampling;

import android.annotation.TargetApi;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import java.util.Calendar;
import java.util.List;

public class UStats {
    @TargetApi(21)
    public static List<UsageStats> getUsageStatsList(Context context) {
        UsageStatsManager usageStatsManager = getUsageStatsManager(context);
        Calendar instance = Calendar.getInstance();
        instance.add(2, -1);
        return usageStatsManager.queryUsageStats(0, instance.getTimeInMillis(), System.currentTimeMillis());
    }

    private static UsageStatsManager getUsageStatsManager(Context context) {
        return (UsageStatsManager) context.getSystemService("usagestats");
    }
}
