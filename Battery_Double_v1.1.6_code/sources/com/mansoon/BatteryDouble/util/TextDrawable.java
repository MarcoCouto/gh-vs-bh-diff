package com.mansoon.BatteryDouble.util;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;

public class TextDrawable extends Drawable {
    private final Paint paint = new Paint();
    private final String text;

    public int getOpacity() {
        return -3;
    }

    public TextDrawable(String str) {
        this.text = str;
        this.paint.setColor(-1);
        this.paint.setTextSize(22.0f);
        this.paint.setAntiAlias(true);
        this.paint.setFakeBoldText(true);
        this.paint.setShadowLayer(6.0f, 0.0f, 0.0f, ViewCompat.MEASURED_STATE_MASK);
        this.paint.setStyle(Style.FILL);
        this.paint.setTextAlign(Align.LEFT);
    }

    public void draw(Canvas canvas) {
        canvas.drawText(this.text, 0.0f, 0.0f, this.paint);
    }

    public void setAlpha(int i) {
        this.paint.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.paint.setColorFilter(colorFilter);
    }
}
