package com.mansoon.BatteryDouble.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkWatcher {
    public static final int BACKGROUND_TASKS = 1;
    public static final int COMMUNICATION_MANAGER = 2;
    private static final String TAG = "NetworkWatcher";

    public static boolean hasInternet(Context context, int i) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        boolean z = false;
        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.getType() == 1) {
                return activeNetworkInfo.isConnected();
            }
            if (activeNetworkInfo.getType() == 0) {
                if (i == 1) {
                    return activeNetworkInfo.isConnected();
                }
                if (i == 2) {
                    if (SettingsUtils.isMobileDataAllowed(context) && activeNetworkInfo.isConnected()) {
                        z = true;
                    }
                    return z;
                }
            }
        }
        return false;
    }
}
