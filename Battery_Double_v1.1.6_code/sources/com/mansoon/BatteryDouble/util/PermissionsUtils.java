package com.mansoon.BatteryDouble.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;

public class PermissionsUtils {
    private static final String TAG = LogUtils.makeLogTag(PermissionsUtils.class);

    public static boolean checkPermission(Context context, String str) {
        return ContextCompat.checkSelfPermission(context, str) == 0;
    }
}
