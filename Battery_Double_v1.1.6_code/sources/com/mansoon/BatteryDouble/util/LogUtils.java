package com.mansoon.BatteryDouble.util;

import android.util.Log;

public class LogUtils {
    public static boolean LOGGING_ENABLED = false;
    private static final String LOG_PREFIX = "greenhub_";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;

    public static String makeLogTag(String str) {
        if (str.length() > 23 - LOG_PREFIX_LENGTH) {
            StringBuilder sb = new StringBuilder();
            sb.append(LOG_PREFIX);
            sb.append(str.substring(0, (23 - LOG_PREFIX_LENGTH) - 1));
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(LOG_PREFIX);
        sb2.append(str);
        return sb2.toString();
    }

    public static String makeLogTag(Class cls) {
        return makeLogTag(cls.getSimpleName());
    }

    public static void LOGD(String str, String str2) {
        if (LOGGING_ENABLED && Log.isLoggable(str, 3)) {
            Log.d(str, str2);
        }
    }

    public static void LOGD(String str, String str2, Throwable th) {
        if (LOGGING_ENABLED && Log.isLoggable(str, 3)) {
            Log.d(str, str2, th);
        }
    }

    public static void LOGV(String str, String str2) {
        if (LOGGING_ENABLED && Log.isLoggable(str, 2)) {
            Log.v(str, str2);
        }
    }

    public static void LOGV(String str, String str2, Throwable th) {
        if (LOGGING_ENABLED && Log.isLoggable(str, 2)) {
            Log.v(str, str2, th);
        }
    }

    public static void LOGI(String str, String str2) {
        if (LOGGING_ENABLED) {
            Log.i(str, str2);
        }
    }

    public static void LOGI(String str, String str2, Throwable th) {
        if (LOGGING_ENABLED) {
            Log.i(str, str2, th);
        }
    }

    public static void LOGW(String str, String str2) {
        if (LOGGING_ENABLED) {
            Log.w(str, str2);
        }
    }

    public static void LOGW(String str, String str2, Throwable th) {
        if (LOGGING_ENABLED) {
            Log.w(str, str2, th);
        }
    }

    public static void LOGE(String str, String str2) {
        if (LOGGING_ENABLED) {
            Log.e(str, str2);
        }
    }

    public static void LOGE(String str, String str2, Throwable th) {
        if (LOGGING_ENABLED) {
            Log.e(str, str2, th);
        }
    }

    private LogUtils() {
    }
}
