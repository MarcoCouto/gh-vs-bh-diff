package com.mansoon.BatteryDouble.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.BatteryLevelEvent;
import com.mansoon.BatteryDouble.events.PowerSourceEvent;
import com.mansoon.BatteryDouble.events.StatusEvent;
import com.mansoon.BatteryDouble.managers.sampling.DataEstimator;
import com.mansoon.BatteryDouble.managers.sampling.Inspector;
import com.mansoon.BatteryDouble.models.Battery;
import com.mansoon.BatteryDouble.models.ui.BatteryCard;
import com.mansoon.BatteryDouble.ui.MainActivity;
import com.mansoon.BatteryDouble.ui.adapters.BatteryRVAdapter;
import com.mansoon.BatteryDouble.util.LogUtils;
import java.util.ArrayList;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class HomeFragment extends Fragment {
    private static final String TAG = LogUtils.makeLogTag("HomeFragment");
    String globaleLevel = "0";
    private ImageView imageCharging;
    /* access modifiers changed from: private */
    public RelativeLayout levelContainer;
    /* access modifiers changed from: private */
    public RelativeLayout levelContainer1;
    /* access modifiers changed from: private */
    public String mActivePower;
    private MainActivity mActivity;
    private BatteryRVAdapter mAdapter;
    /* access modifiers changed from: private */
    public ArrayList<BatteryCard> mBatteryCards;
    private ProgressBar mBatteryCircleBar;
    /* access modifiers changed from: private */
    public TextView mBatteryCurrentMax;
    /* access modifiers changed from: private */
    public TextView mBatteryCurrentMin;
    /* access modifiers changed from: private */
    public TextView mBatteryCurrentNow;
    private TextView mBatteryPercentage;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private Thread mLocalThread;
    /* access modifiers changed from: private */
    public int mMax;
    /* access modifiers changed from: private */
    public int mMin;
    private ImageView mPowerAc;
    private ImageView mPowerDischarging;
    private ImageView mPowerUsb;
    private ImageView mPowerWireless;
    private RecyclerView mRecyclerView;
    private TextView mStatus;
    int m_nLevel1 = 0;
    int m_nLevel2 = 0;
    private Runnable runnable = new Runnable() {
        public void run() {
            int batteryCurrentNow = Battery.getBatteryCurrentNow(HomeFragment.this.mContext);
            double currentBatteryLevel = Inspector.getCurrentBatteryLevel();
            if (HomeFragment.this.mActivePower.equals("unplugged") || currentBatteryLevel != 1.0d) {
                if (Math.abs(batteryCurrentNow) < Math.abs(HomeFragment.this.mMin)) {
                    HomeFragment.this.mMin = batteryCurrentNow;
                    StringBuilder sb = new StringBuilder();
                    sb.append("min: ");
                    sb.append(HomeFragment.this.mMin);
                    sb.append(" mA");
                    HomeFragment.this.mBatteryCurrentMin.setText(sb.toString());
                }
                if (Math.abs(batteryCurrentNow) > Math.abs(HomeFragment.this.mMax)) {
                    HomeFragment.this.mMax = batteryCurrentNow;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("max: ");
                    sb2.append(HomeFragment.this.mMax);
                    sb2.append(" mA");
                    HomeFragment.this.mBatteryCurrentMax.setText(sb2.toString());
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append(batteryCurrentNow);
                sb3.append(" mA");
                HomeFragment.this.mBatteryCurrentNow.setText(sb3.toString());
                HomeFragment.this.mHandler.postDelayed(this, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
                return;
            }
            HomeFragment.this.mBatteryCurrentMin.setText("min: --");
            HomeFragment.this.mBatteryCurrentMax.setText("max: --");
            HomeFragment.this.mBatteryCurrentNow.setText(HomeFragment.this.mContext.getString(R.string.battery_full));
            HomeFragment.this.mHandler.postDelayed(this, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        }
    };
    private TextView textBatterLevelPercentage;
    private TextView textBatterLevelPercentage1;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_home, viewGroup, false);
        this.mContext = inflate.getContext();
        this.mActivity = (MainActivity) getActivity();
        this.mRecyclerView = (RecyclerView) inflate.findViewById(R.id.rv);
        this.mAdapter = null;
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this.mContext) {
            public boolean canScrollVertically() {
                return false;
            }
        });
        this.mRecyclerView.setHasFixedSize(true);
        this.mBatteryPercentage = (TextView) inflate.findViewById(R.id.batteryCurrentValue);
        this.mBatteryCircleBar = (ProgressBar) inflate.findViewById(R.id.batteryProgressbar);
        this.mStatus = (TextView) inflate.findViewById(R.id.status);
        this.mBatteryCurrentNow = (TextView) inflate.findViewById(R.id.batteryCurrentNow);
        this.mBatteryCurrentMin = (TextView) inflate.findViewById(R.id.batteryCurrentMin);
        this.mBatteryCurrentMax = (TextView) inflate.findViewById(R.id.batteryCurrentMax);
        this.mPowerDischarging = (ImageView) inflate.findViewById(R.id.imgPowerDischarging);
        this.mPowerAc = (ImageView) inflate.findViewById(R.id.imgPowerAc);
        this.mPowerUsb = (ImageView) inflate.findViewById(R.id.imgPowerUsb);
        this.mPowerWireless = (ImageView) inflate.findViewById(R.id.imgPowerWireless);
        this.mActivePower = "";
        this.levelContainer = (RelativeLayout) inflate.findViewById(R.id.levelContainer);
        this.textBatterLevelPercentage = (TextView) inflate.findViewById(R.id.batteryPercentage);
        this.levelContainer1 = (RelativeLayout) inflate.findViewById(R.id.levelContainer1);
        this.textBatterLevelPercentage1 = (TextView) inflate.findViewById(R.id.batteryPercentage1);
        this.imageCharging = (ImageView) inflate.findViewById(R.id.charging);
        this.levelContainer.setVisibility(4);
        this.textBatterLevelPercentage.setVisibility(4);
        this.levelContainer1.setVisibility(4);
        this.textBatterLevelPercentage1.setVisibility(4);
        this.mMin = Integer.MAX_VALUE;
        this.mMax = 0;
        this.mHandler = new Handler();
        this.mHandler.postDelayed(this.runnable, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        return inflate;
    }

    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onResume() {
        super.onResume();
        if (this.mActivity.getEstimator() != null) {
            String num = Integer.toString(this.mActivity.getEstimator().getLevel());
            this.mBatteryPercentage.setText(num);
            setupBatteryLevele(num);
            this.mBatteryCircleBar.setProgress(this.mActivity.getEstimator().getLevel());
            loadData(this.mActivity.getEstimator());
            loadPluggedState("home");
        }
    }

    private void setupBatteryLevele(String str) {
        this.globaleLevel = str;
        int parseInt = Integer.parseInt(this.globaleLevel);
        this.m_nLevel1 = Math.max((parseInt - 50) * 2, 0);
        this.m_nLevel2 = Math.min(parseInt * 2, 100);
        this.levelContainer.setVisibility(0);
        this.textBatterLevelPercentage.setVisibility(0);
        this.levelContainer1.setVisibility(0);
        this.textBatterLevelPercentage1.setVisibility(0);
        TextView textView = this.textBatterLevelPercentage;
        StringBuilder sb = new StringBuilder();
        sb.append(this.m_nLevel2);
        sb.append("%");
        textView.setText(sb.toString());
        TextView textView2 = this.textBatterLevelPercentage1;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(this.m_nLevel1);
        sb2.append("%");
        textView2.setText(sb2.toString());
        if (this.m_nLevel1 == 0) {
            this.levelContainer1.setVisibility(4);
        }
        if (this.m_nLevel2 == 0) {
            this.levelContainer.setVisibility(4);
        }
        this.levelContainer.post(new Runnable() {
            public void run() {
                LayoutParams layoutParams = (LayoutParams) HomeFragment.this.levelContainer.getLayoutParams();
                int i = HomeFragment.this.m_nLevel2 * 4;
                if (i >= 23) {
                    i = (HomeFragment.this.m_nLevel2 * 4) - 23;
                }
                layoutParams.height = i;
                HomeFragment.this.levelContainer.setLayoutParams(layoutParams);
            }
        });
        this.levelContainer1.post(new Runnable() {
            public void run() {
                LayoutParams layoutParams = (LayoutParams) HomeFragment.this.levelContainer1.getLayoutParams();
                layoutParams.height = (HomeFragment.this.m_nLevel1 * 4) - 23;
                HomeFragment.this.levelContainer1.setLayoutParams(layoutParams);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateBatteryLevelUI(BatteryLevelEvent batteryLevelEvent) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(batteryLevelEvent.level);
        String sb2 = sb.toString();
        this.mBatteryPercentage.setText(sb2);
        this.mBatteryCircleBar.setProgress(batteryLevelEvent.level);
        setupBatteryLevele(sb2);
        loadData(this.mActivity.getEstimator());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateStatus(StatusEvent statusEvent) {
        this.mStatus.setText(statusEvent.status);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updatePowerSource(PowerSourceEvent powerSourceEvent) {
        loadPluggedState(powerSourceEvent.status);
        if (this.mActivePower.equals("unplugged")) {
            resetBatteryCurrent();
        }
    }

    private void loadData(final DataEstimator dataEstimator) {
        this.mLocalThread = new Thread(new Runnable() {
            public void run() {
                String str;
                HomeFragment.this.mBatteryCards = new ArrayList();
                float temperature = dataEstimator.getTemperature();
                StringBuilder sb = new StringBuilder();
                sb.append(temperature);
                sb.append(" ºC");
                String valueOf = String.valueOf(sb.toString());
                int i = (temperature > 45.0f ? 1 : (temperature == 45.0f ? 0 : -1));
                int i2 = SupportMenu.CATEGORY_MASK;
                int i3 = i > 0 ? SupportMenu.CATEGORY_MASK : (temperature > 45.0f || temperature <= 35.0f) ? -16711936 : InputDeviceCompat.SOURCE_ANY;
                HomeFragment.this.mBatteryCards.add(new BatteryCard(R.drawable.ic_thermometer_black_18dp, HomeFragment.this.getString(R.string.battery_summary_temperature), valueOf, i3));
                StringBuilder sb2 = new StringBuilder();
                sb2.append(dataEstimator.getVoltage());
                sb2.append(" V");
                HomeFragment.this.mBatteryCards.add(new BatteryCard(R.drawable.ic_flash_black_18dp, HomeFragment.this.getString(R.string.battery_summary_voltage), String.valueOf(sb2.toString())));
                String healthStatus = dataEstimator.getHealthStatus(HomeFragment.this.mContext);
                if (healthStatus.equals(HomeFragment.this.mContext.getString(R.string.battery_health_good))) {
                    i2 = -16711936;
                }
                HomeFragment.this.mBatteryCards.add(new BatteryCard(R.drawable.ic_heart_black_18dp, HomeFragment.this.getString(R.string.battery_summary_health), healthStatus, i2));
                int i4 = -7829368;
                if (dataEstimator.getTechnology() == null) {
                    str = HomeFragment.this.getString(R.string.not_available);
                } else {
                    if (!dataEstimator.getTechnology().equals("Li-ion")) {
                        i4 = -16711936;
                    }
                    str = dataEstimator.getTechnology();
                }
                HomeFragment.this.mBatteryCards.add(new BatteryCard(R.drawable.ic_wrench_black_18dp, HomeFragment.this.getString(R.string.battery_summary_technology), str, i4));
            }
        });
        this.mLocalThread.start();
        setAdapter();
    }

    private void setAdapter() {
        try {
            this.mLocalThread.join();
            if (this.mAdapter == null) {
                this.mAdapter = new BatteryRVAdapter(this.mBatteryCards);
                this.mRecyclerView.setAdapter(this.mAdapter);
            } else {
                this.mAdapter.swap(this.mBatteryCards);
            }
            this.mRecyclerView.invalidate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void loadPluggedState(String str) {
        this.mMin = Integer.MAX_VALUE;
        this.mMax = Integer.MIN_VALUE;
        String str2 = "unplugged";
        if (str.equals("home")) {
            if (this.mActivity.getEstimator() != null) {
                switch (this.mActivity.getEstimator().getPlugged()) {
                    case 1:
                        str = "ac";
                        break;
                    case 2:
                        str = "usb";
                        break;
                    default:
                        str = str2;
                        break;
                }
            } else {
                return;
            }
        }
        this.imageCharging.setVisibility(4);
        String str3 = this.mActivePower;
        char c = 65535;
        int hashCode = str3.hashCode();
        if (hashCode != -1000044642) {
            if (hashCode != 3106) {
                if (hashCode != 116100) {
                    if (hashCode == 1236169279 && str3.equals("unplugged")) {
                        c = 0;
                    }
                } else if (str3.equals("usb")) {
                    c = 2;
                }
            } else if (str3.equals("ac")) {
                c = 1;
            }
        } else if (str3.equals("wireless")) {
            c = 3;
        }
        switch (c) {
            case 0:
                this.mPowerDischarging.setImageResource(R.drawable.ic_battery_50_grey600_24dp);
                break;
            case 1:
                this.mPowerAc.setImageResource(R.drawable.ic_power_plug_grey600_24dp);
                break;
            case 2:
                this.mPowerUsb.setImageResource(R.drawable.ic_usb_grey600_24dp);
                break;
            case 3:
                this.mPowerWireless.setImageResource(R.drawable.ic_access_point_grey600_24dp);
                break;
        }
        if (str.equals("unplugged")) {
            this.mPowerDischarging.setImageResource(R.drawable.ic_battery_50_white_24dp);
        } else if (str.equals("ac")) {
            this.imageCharging.setVisibility(0);
            this.mPowerAc.setImageResource(R.drawable.ic_power_plug_white_24dp);
        } else if (str.equals("usb")) {
            this.imageCharging.setVisibility(0);
            this.mPowerUsb.setImageResource(R.drawable.ic_usb_white_24dp);
        } else if (str.equals("wireless")) {
            this.mPowerWireless.setImageResource(R.drawable.ic_access_point_white_24dp);
        }
        this.mActivePower = str;
    }

    private void resetBatteryCurrent() {
        this.mMin = Integer.MAX_VALUE;
        this.mMax = 0;
        this.mBatteryCurrentMin.setText("min: --");
        this.mBatteryCurrentMax.setText("max: --");
        this.mBatteryCurrentNow.setText(getString(R.string.battery_measure));
    }
}
