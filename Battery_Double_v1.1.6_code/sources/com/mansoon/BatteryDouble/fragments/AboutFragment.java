package com.mansoon.BatteryDouble.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mansoon.BatteryDouble.R;

public class AboutFragment extends Fragment {
    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_about, viewGroup, false);
    }
}
