package com.mansoon.BatteryDouble.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.mansoon.BatteryDouble.ui.MainActivity;
import com.mansoon.BatteryDouble.util.LogUtils;

public abstract class WelcomeFragment extends Fragment {
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(WelcomeFragment.class);
    protected Activity mActivity;

    interface WelcomeFragmentClickAction {
        void doAction(Context context);
    }

    public interface WelcomeFragmentContainer {
        Button getNegativeButton();

        Button getPositiveButton();

        void setNegativeButtonEnabled(Boolean bool);

        void setPositiveButtonEnabled(Boolean bool);
    }

    protected abstract class WelcomeFragmentOnClickListener implements OnClickListener {
        Activity mActivity;

        WelcomeFragmentOnClickListener(Activity activity) {
            this.mActivity = activity;
        }

        /* access modifiers changed from: 0000 */
        public void doNext() {
            LogUtils.LOGD(WelcomeFragment.TAG, "Proceeding to next activity");
            WelcomeFragment.this.startActivity(new Intent(this.mActivity, MainActivity.class));
            this.mActivity.finish();
        }

        /* access modifiers changed from: 0000 */
        public void doFinish() {
            LogUtils.LOGD(WelcomeFragment.TAG, "Closing app");
            this.mActivity.finish();
        }
    }

    /* access modifiers changed from: protected */
    public abstract OnClickListener getNegativeListener();

    /* access modifiers changed from: protected */
    public abstract String getNegativeText();

    /* access modifiers changed from: protected */
    public abstract OnClickListener getPositiveListener();

    /* access modifiers changed from: protected */
    public abstract String getPositiveText();

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        LogUtils.LOGD(TAG, "Attaching to activity");
        this.mActivity = activity;
    }

    public void onDetach() {
        super.onDetach();
        this.mActivity = null;
    }

    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        LogUtils.LOGD(TAG, "Creating View");
        if (this.mActivity instanceof WelcomeFragmentContainer) {
            WelcomeFragmentContainer welcomeFragmentContainer = (WelcomeFragmentContainer) this.mActivity;
            attachToPositiveButton(welcomeFragmentContainer.getPositiveButton());
            attachToNegativeButton(welcomeFragmentContainer.getNegativeButton());
        }
        return onCreateView;
    }

    /* access modifiers changed from: protected */
    public void attachToPositiveButton(Button button) {
        button.setText(getPositiveText());
        button.setOnClickListener(getPositiveListener());
    }

    /* access modifiers changed from: protected */
    public void attachToNegativeButton(Button button) {
        button.setText(getNegativeText());
        button.setOnClickListener(getNegativeListener());
    }

    /* access modifiers changed from: protected */
    public String getResourceString(int i) {
        if (this.mActivity != null) {
            return this.mActivity.getResources().getString(i);
        }
        return null;
    }
}
