package com.mansoon.BatteryDouble.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.OpenMessageEvent;
import com.mansoon.BatteryDouble.models.data.Message;
import com.mansoon.BatteryDouble.util.StringHelper;
import java.util.ArrayList;
import org.greenrobot.eventbus.EventBus;

public class MessageAdapter extends Adapter<DashboardViewHolder> {
    private ArrayList<Message> mMessages;

    static class DashboardViewHolder extends ViewHolder {
        public TextView body;
        public TextView date;
        public TextView title;

        DashboardViewHolder(View view) {
            super(view);
            this.title = (TextView) view.findViewById(R.id.message_title);
            this.body = (TextView) view.findViewById(R.id.message_body);
            this.date = (TextView) view.findViewById(R.id.message_date);
        }
    }

    public MessageAdapter(ArrayList<Message> arrayList) {
        this.mMessages = arrayList;
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public DashboardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new DashboardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_item_view, viewGroup, false));
    }

    public void onBindViewHolder(final DashboardViewHolder dashboardViewHolder, int i) {
        dashboardViewHolder.title.setText(StringHelper.truncate(((Message) this.mMessages.get(i)).realmGet$title(), 25));
        dashboardViewHolder.body.setText(StringHelper.truncate(((Message) this.mMessages.get(i)).realmGet$body(), 30));
        dashboardViewHolder.date.setText(((Message) this.mMessages.get(i)).realmGet$date().substring(0, 10));
        if (!((Message) this.mMessages.get(i)).realmGet$read()) {
            dashboardViewHolder.title.setTypeface(null, 1);
        }
        dashboardViewHolder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                EventBus.getDefault().post(new OpenMessageEvent(dashboardViewHolder.getAdapterPosition()));
            }
        });
    }

    public int getItemCount() {
        return this.mMessages.size();
    }

    public void swap(ArrayList<Message> arrayList) {
        if (this.mMessages != null) {
            this.mMessages.clear();
            this.mMessages.addAll(arrayList);
        } else {
            this.mMessages = arrayList;
        }
        notifyDataSetChanged();
    }
}
