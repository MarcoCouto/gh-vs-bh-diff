package com.mansoon.BatteryDouble.ui.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Parcelable;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;
import com.mansoon.BatteryDouble.fragments.DeviceFragment;
import com.mansoon.BatteryDouble.fragments.HomeFragment;
import com.mansoon.BatteryDouble.fragments.StatisticsFragment;

public class TabAdapter extends FragmentStatePagerAdapter {
    public static final int NUM_TABS = 3;
    public static final int TAB_CHARTS = 2;
    public static final int TAB_HOME = 0;
    public static final int TAB_MY_DEVICE = 1;
    private final SparseArray<Fragment> mFragments = new SparseArray<>(3);

    private boolean isValidPosition(int i) {
        return i >= 0 && i < 3;
    }

    public int getCount() {
        return 3;
    }

    public String getTabName(int i) {
        switch (i) {
            case 0:
                return "Home";
            case 1:
                return "My Device";
            case 2:
                return "Statistics";
            default:
                return "default";
        }
    }

    public TabAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
        try {
            super.restoreState(parcelable, classLoader);
        } catch (IllegalStateException unused) {
        }
    }

    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return HomeFragment.newInstance();
            case 1:
                return DeviceFragment.newInstance();
            case 2:
                return StatisticsFragment.newInstance();
            default:
                return null;
        }
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Object instantiateItem = super.instantiateItem(viewGroup, i);
        if (instantiateItem instanceof Fragment) {
            this.mFragments.put(i, (Fragment) instantiateItem);
        }
        return instantiateItem;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        this.mFragments.remove(i);
        super.destroyItem(viewGroup, i, obj);
    }

    public Fragment getFragment(int i) {
        if (isValidPosition(i)) {
            return (Fragment) this.mFragments.get(i);
        }
        return null;
    }
}
