package com.mansoon.BatteryDouble.ui.layouts;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import com.mansoon.BatteryDouble.R;

public class MainTabLayout extends TabLayout {
    public MainTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public MainTabLayout(Context context) {
        super(context);
    }

    public MainTabLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void createTabs() {
        addTab(R.drawable.ic_home_white_24dp, R.string.title_fragment_home);
        addTab(R.drawable.ic_cellphone_android_white_24dp, R.string.title_fragment_device);
        addTab(R.drawable.ic_chart_areaspline_white_24dp, R.string.title_fragment_stats);
    }

    private void addTab(@DrawableRes int i, @StringRes int i2) {
        addTab(newTab().setIcon(i).setContentDescription(i2));
    }
}
