package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.mansoon.BatteryDouble.models.data.LocationProvider;
import com.mansoon.BatteryDouble.util.LogUtils;
import java.util.LinkedList;
import java.util.List;

public class LocationInfo {
    private static final String TAG = LogUtils.makeLogTag(LocationInfo.class);
    private static Location lastKnownLocation = null;

    public static double getDistance(Context context) {
        Location lastKnownLocation2 = getLastKnownLocation(context);
        double distanceTo = (lastKnownLocation == null || lastKnownLocation2 == null) ? Utils.DOUBLE_EPSILON : (double) lastKnownLocation.distanceTo(lastKnownLocation2);
        lastKnownLocation = lastKnownLocation2;
        return distanceTo;
    }

    private static Location getLastKnownLocation(Context context) {
        String bestProvider = getBestProvider(context);
        if (bestProvider == null || bestProvider.equals("gps")) {
            return null;
        }
        return getLastKnownLocation(context, bestProvider);
    }

    private static Location getLastKnownLocation(Context context, String str) {
        try {
            return ((LocationManager) context.getSystemService("location")).getLastKnownLocation(str);
        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static double getDistance(double d, double d2, double d3, double d4) {
        float[] fArr = new float[1];
        Location.distanceBetween(d, d2, d3, d4, fArr);
        return (double) fArr[0];
    }

    public static String getCoarseLocation(Context context) {
        String bestProvider = getBestProvider(context);
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        if (bestProvider != null && !bestProvider.equals("gps")) {
            try {
                Location lastKnownLocation2 = locationManager.getLastKnownLocation(bestProvider);
                double latitude = lastKnownLocation2.getLatitude();
                double longitude = lastKnownLocation2.getLongitude();
                StringBuilder sb = new StringBuilder();
                sb.append(String.valueOf(latitude));
                sb.append(",");
                sb.append(String.valueOf(longitude));
                return sb.toString();
            } catch (SecurityException e) {
                LogUtils.LOGD("SamplingLibrary", "Failed getting coarse location!");
                e.printStackTrace();
            }
        }
        return "Unknown";
    }

    public static List<LocationProvider> getEnabledLocationProviders(Context context) {
        List<String> providers = ((LocationManager) context.getSystemService("location")).getProviders(true);
        LinkedList linkedList = new LinkedList();
        for (String locationProvider : providers) {
            linkedList.add(new LocationProvider(locationProvider));
        }
        return linkedList;
    }

    public static String getBestProvider(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        Criteria criteria = new Criteria();
        criteria.setAccuracy(2);
        criteria.setPowerRequirement(1);
        return locationManager.getBestProvider(criteria, true);
    }

    @Deprecated
    public static int getMaxNumSatellite(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        return 0;
    }

    public static CellLocation getDeviceLocation(Context context) {
        return ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getCellLocation();
    }

    public static String getCountryCode(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        if (telephonyManager.getPhoneType() != 2) {
            String networkCountryIso = telephonyManager.getNetworkCountryIso();
            if (networkCountryIso != null && networkCountryIso.length() == 2) {
                return networkCountryIso;
            }
            String countryCodeFromProperty = getCountryCodeFromProperty(context, "gsm.operator.numeric");
            if (countryCodeFromProperty != null && countryCodeFromProperty.length() == 2) {
                return countryCodeFromProperty;
            }
            String simCountryIso = telephonyManager.getSimCountryIso();
            if (simCountryIso != null && simCountryIso.length() == 2) {
                return simCountryIso;
            }
        } else {
            String countryCodeFromProperty2 = getCountryCodeFromProperty(context, "ro.cdma.home.operator.numeric");
            if (countryCodeFromProperty2 != null && countryCodeFromProperty2.length() == 2) {
                return countryCodeFromProperty2;
            }
        }
        return "Unknown";
    }

    private static String getCountryCodeFromProperty(Context context, String str) {
        try {
            String systemProperty = Specifications.getSystemProperty(context, str);
            if (systemProperty != null && systemProperty.length() >= 5) {
                return Phone.getCountryCodeForMcc(context, Integer.parseInt(systemProperty.substring(0, 3)));
            }
        } catch (Exception e) {
            if (e.getLocalizedMessage() != null) {
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Failed getting network location: ");
                sb.append(e.getLocalizedMessage());
                LogUtils.LOGD(str2, sb.toString());
            }
        }
        return null;
    }
}
