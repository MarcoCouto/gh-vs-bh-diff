package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.location.LocationManager;

public class Gps {
    private static final String TAG = "Gps";

    public static boolean isEnabled(Context context) {
        return ((LocationManager) context.getSystemService("location")).isProviderEnabled("gps");
    }
}
