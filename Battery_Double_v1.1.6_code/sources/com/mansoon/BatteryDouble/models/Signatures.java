package com.mansoon.BatteryDouble.models;

import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import com.mansoon.BatteryDouble.models.data.AppSignature;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.StringHelper;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.LinkedList;
import java.util.List;

public class Signatures {
    private static final String TAG = "Signatures";

    public static List<AppSignature> getSignatureList(PackageInfo packageInfo) {
        Signature[] signatureArr;
        LinkedList linkedList = new LinkedList();
        String[] strArr = packageInfo.requestedPermissions;
        if (strArr != null) {
            linkedList.add(new AppSignature(StringHelper.convertToHex(Permissions.getPermissionBytes(strArr))));
        }
        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest instance = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
                instance.update(signature.toByteArray());
                linkedList.add(new AppSignature(StringHelper.convertToHex(instance.digest())));
                CertificateFactory instance2 = CertificateFactory.getInstance("X.509");
                if (instance2 != null) {
                    X509Certificate x509Certificate = (X509Certificate) instance2.generateCertificate(new ByteArrayInputStream(signature.toByteArray()));
                    if (x509Certificate != null) {
                        PublicKey publicKey = x509Certificate.getPublicKey();
                        if (publicKey != null) {
                            String algorithm = publicKey.getAlgorithm();
                            char c = 65535;
                            int hashCode = algorithm.hashCode();
                            if (hashCode != 67986) {
                                if (hashCode == 81440) {
                                    if (algorithm.equals("RSA")) {
                                        c = 0;
                                    }
                                }
                            } else if (algorithm.equals("DSA")) {
                                c = 1;
                            }
                            switch (c) {
                                case 0:
                                    MessageDigest instance3 = MessageDigest.getInstance(CommonUtils.SHA256_INSTANCE);
                                    byte[] byteArray = ((RSAPublicKey) publicKey).getModulus().toByteArray();
                                    if (byteArray[0] == 0) {
                                        byte[] bArr = new byte[(byteArray.length - 1)];
                                        System.arraycopy(byteArray, 1, bArr, 0, byteArray.length - 1);
                                        instance3.update(bArr);
                                    } else {
                                        instance3.update(byteArray);
                                    }
                                    linkedList.add(new AppSignature(StringHelper.convertToHex(instance3.digest())));
                                    break;
                                case 1:
                                    DSAPublicKey dSAPublicKey = (DSAPublicKey) publicKey;
                                    MessageDigest instance4 = MessageDigest.getInstance(CommonUtils.SHA256_INSTANCE);
                                    byte[] byteArray2 = dSAPublicKey.getY().toByteArray();
                                    if (byteArray2[0] == 0) {
                                        byte[] bArr2 = new byte[(byteArray2.length - 1)];
                                        System.arraycopy(byteArray2, 1, bArr2, 0, byteArray2.length - 1);
                                        instance4.update(bArr2);
                                    } else {
                                        instance4.update(byteArray2);
                                    }
                                    linkedList.add(new AppSignature(StringHelper.convertToHex(instance4.digest())));
                                    break;
                                default:
                                    String str = TAG;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Weird algorithm: ");
                                    sb.append(algorithm);
                                    sb.append(" for ");
                                    sb.append(packageInfo.packageName);
                                    LogUtils.LOGE(str, sb.toString());
                                    break;
                            }
                        }
                    }
                }
            } catch (NoSuchAlgorithmException | CertificateException e) {
                e.printStackTrace();
            }
        }
        return linkedList;
    }
}
