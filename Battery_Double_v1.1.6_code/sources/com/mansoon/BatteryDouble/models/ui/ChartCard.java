package com.mansoon.BatteryDouble.models.ui;

import com.github.mikephil.charting.data.Entry;
import java.util.ArrayList;
import java.util.List;

public class ChartCard {
    private static final String TAG = "ChartCard";
    public int color;
    public List<Entry> entries = new ArrayList();
    public double[] extras = null;
    public String label;
    public int type;

    public ChartCard(int i, String str, int i2) {
        this.type = i;
        this.label = str;
        this.color = i2;
    }
}
