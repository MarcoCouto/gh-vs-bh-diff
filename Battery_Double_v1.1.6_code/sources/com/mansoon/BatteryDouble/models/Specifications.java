package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import com.mansoon.BatteryDouble.models.data.Feature;
import com.mansoon.BatteryDouble.util.LogUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.UUID;

public class Specifications {
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    private static final String TAG = LogUtils.makeLogTag(Specifications.class);
    private static final String TYPE_UNKNOWN = "unknown";
    private static final int UUID_LENGTH = 16;

    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static synchronized String getAndroidId(Context context) {
        String string;
        synchronized (Specifications.class) {
            string = getSharedPreferences(context).getString(PREF_UNIQUE_ID, null);
            if (string == null) {
                string = UUID.randomUUID().toString();
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("No Android ID Key on this device. Generating random one: ");
                sb.append(string);
                LogUtils.LOGD(str, sb.toString());
                setAndroidId(context, string);
            }
        }
        return string;
    }

    private static void setAndroidId(Context context, String str) {
        getSharedPreferences(context).edit().putString(PREF_UNIQUE_ID, str).apply();
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Android ID Key of device set to: ");
        sb.append(str);
        LogUtils.LOGD(str2, sb.toString());
    }

    public static String getBrand() {
        return Build.BRAND;
    }

    public static String getBuildSerial() {
        return Build.SERIAL;
    }

    public static String getKernelVersion() {
        return System.getProperty("os.version", "unknown");
    }

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getModel() {
        return Build.MODEL;
    }

    public static String getOsVersion() {
        return VERSION.RELEASE;
    }

    public static String getProductName() {
        return Build.PRODUCT;
    }

    public static Feature getVmVersion() {
        Feature feature = new Feature();
        String property = System.getProperty("java.vm.version");
        if (property == null) {
            property = "";
        }
        feature.realmSet$key("vm");
        feature.realmSet$value(property);
        return feature;
    }

    public static String getSystemProperty(Context context, String str) throws Exception {
        Method method = Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class});
        method.setAccessible(true);
        return (String) method.invoke(context, new Object[]{str});
    }

    public static String getStringFromSystemProperty(Context context, String str) {
        try {
            String systemProperty = getSystemProperty(context, str);
            if (systemProperty != null && systemProperty.length() > 0) {
                return systemProperty;
            }
        } catch (Exception e) {
            if (e.getLocalizedMessage() != null) {
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Failed getting service provider: ");
                sb.append(e.getLocalizedMessage());
                LogUtils.LOGD(str2, sb.toString());
            }
        }
        return null;
    }

    public static boolean isRooted() {
        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3();
    }

    private static boolean checkRootMethod1() {
        String str = Build.TAGS;
        return str != null && str.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        for (String file : new String[]{"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su"}) {
            if (new File(file).exists()) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003f  */
    private static boolean checkRootMethod3() {
        boolean z = false;
        Process process = null;
        try {
            Process exec = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            try {
                if (new BufferedReader(new InputStreamReader(exec.getInputStream())).readLine() != null) {
                    z = true;
                }
                if (exec != null) {
                    exec.destroy();
                }
                return z;
            } catch (Throwable th) {
                th = th;
                process = exec;
                if (process != null) {
                    process.destroy();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            if (process != null) {
            }
            throw th;
        }
    }
}
