package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;

public class Screen {
    private static final String TAG = "Screen";

    public static int getBrightness(Context context) {
        try {
            return System.getInt(context.getContentResolver(), "screen_brightness");
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static boolean isAutoBrightness(Context context) {
        try {
            if (System.getInt(context.getContentResolver(), "screen_brightness_mode") == 1) {
                return true;
            }
            return false;
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int isOn(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return 0;
        }
        return VERSION.SDK_INT >= 20 ? powerManager.isInteractive() ? 1 : 0 : powerManager.isScreenOn() ? 1 : 0;
    }
}
