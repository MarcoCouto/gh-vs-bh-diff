package com.mansoon.BatteryDouble.models;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.StringHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Application {
    private static final String TAG = LogUtils.makeLogTag(Application.class);

    public static ArrayList<ProcessInfo> getRunningAppInfo(Context context) {
        List<RunningAppProcessInfo> runningProcessInfo = Process.getRunningProcessInfo(context);
        List<RunningServiceInfo> runningServiceInfo = Service.getRunningServiceInfo(context);
        HashSet hashSet = new HashSet();
        ArrayList<ProcessInfo> arrayList = new ArrayList<>();
        if (runningProcessInfo != null) {
            for (RunningAppProcessInfo runningAppProcessInfo : runningProcessInfo) {
                if (runningAppProcessInfo != null && !hashSet.contains(runningAppProcessInfo.processName)) {
                    hashSet.add(runningAppProcessInfo.processName);
                    ProcessInfo processInfo = new ProcessInfo();
                    processInfo.realmSet$importance(StringHelper.importanceString(runningAppProcessInfo.importance));
                    processInfo.realmSet$processId(runningAppProcessInfo.pid);
                    processInfo.realmSet$name(runningAppProcessInfo.processName);
                    arrayList.add(processInfo);
                }
            }
        }
        if (runningServiceInfo != null) {
            for (RunningServiceInfo runningServiceInfo2 : runningServiceInfo) {
                if (runningServiceInfo2 != null && !hashSet.contains(runningServiceInfo2.process)) {
                    hashSet.add(runningServiceInfo2.process);
                    ProcessInfo processInfo2 = new ProcessInfo();
                    processInfo2.realmSet$importance(runningServiceInfo2.foreground ? "Foreground app" : "Service");
                    processInfo2.realmSet$processId(runningServiceInfo2.pid);
                    processInfo2.realmSet$applicationLabel(runningServiceInfo2.service.flattenToString());
                    processInfo2.realmSet$name(runningServiceInfo2.process);
                    arrayList.add(processInfo2);
                }
            }
        }
        return arrayList;
    }

    public static boolean isRunning(Context context, String str) {
        List<RunningAppProcessInfo> runningProcessInfo = Process.getRunningProcessInfo(context);
        List<RunningServiceInfo> runningServiceInfo = Service.getRunningServiceInfo(context);
        for (RunningAppProcessInfo runningAppProcessInfo : runningProcessInfo) {
            if (runningAppProcessInfo.processName.equals(str) && runningAppProcessInfo.importance != 500) {
                return true;
            }
        }
        for (RunningServiceInfo runningServiceInfo2 : runningServiceInfo) {
            if (StringHelper.formatProcessName(runningServiceInfo2.process).equals(str)) {
                return true;
            }
        }
        return false;
    }

    public static String getAppPriority(Context context, String str) {
        List<RunningAppProcessInfo> runningProcessInfo = Process.getRunningProcessInfo(context);
        int i = Integer.MAX_VALUE;
        for (RunningServiceInfo runningServiceInfo : Service.getRunningServiceInfo(context)) {
            if (runningServiceInfo.service.getPackageName().equals(str)) {
                i = 300;
            }
        }
        for (RunningAppProcessInfo runningAppProcessInfo : runningProcessInfo) {
            if (Arrays.asList(runningAppProcessInfo.pkgList).contains(str) && runningAppProcessInfo.importance < i) {
                i = runningAppProcessInfo.importance;
            }
        }
        return StringHelper.translatedPriority(context, StringHelper.importanceString(i));
    }
}
