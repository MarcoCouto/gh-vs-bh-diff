package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.mansoon.BatteryDouble.Config;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import com.mansoon.BatteryDouble.util.LogUtils;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Package {
    private static final String TAG = LogUtils.makeLogTag(Package.class);
    private static WeakReference<Map<String, PackageInfo>> packages = null;

    private static Map<String, PackageInfo> getPackages(Context context, boolean z) {
        List<PackageInfo> list;
        Map<String, PackageInfo> map = null;
        try {
            if (packages == null || packages.get() == null || ((Map) packages.get()).size() == 0) {
                HashMap hashMap = new HashMap();
                PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    return null;
                }
                if (z) {
                    try {
                        list = packageManager.getInstalledPackages(4160);
                    } catch (Throwable unused) {
                        list = null;
                    }
                } else {
                    list = packageManager.getInstalledPackages(0);
                }
                if (list == null) {
                    return null;
                }
                for (PackageInfo packageInfo : list) {
                    if (!(packageInfo == null || packageInfo.applicationInfo == null)) {
                        if (packageInfo.applicationInfo.processName != null) {
                            hashMap.put(packageInfo.applicationInfo.processName, packageInfo);
                        }
                    }
                }
                packages = new WeakReference<>(hashMap);
                if (hashMap.size() != 0) {
                    map = hashMap;
                }
                return map;
            } else if (packages == null) {
                return null;
            } else {
                Map<String, PackageInfo> map2 = (Map) packages.get();
                if (map2 == null || map2.size() == 0) {
                    map2 = null;
                }
                return map2;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PackageInfo getPackageInfo(Context context, String str) {
        Map packages2 = getPackages(context, true);
        if (packages2 == null || !packages2.containsKey(str)) {
            return null;
        }
        return (PackageInfo) packages2.get(str);
    }

    public static Map<String, ProcessInfo> getInstalledPackages(Context context, boolean z) {
        Map packages2 = getPackages(context, true);
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (Entry entry : packages2.entrySet()) {
            try {
                String str = (String) entry.getKey();
                PackageInfo packageInfo = (PackageInfo) entry.getValue();
                if (packageInfo != null) {
                    int i = packageInfo.versionCode;
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    String charSequence = packageManager.getApplicationLabel(applicationInfo).toString();
                    int i2 = applicationInfo.uid;
                    int i3 = packageInfo.applicationInfo.flags;
                    boolean z2 = false;
                    if (((i3 & 1) > 0) || (i3 & 128) > 0) {
                        z2 = true;
                    }
                    if (!z || !z2) {
                        if (packageInfo.signatures.length > 0) {
                            ProcessInfo processInfo = new ProcessInfo();
                            processInfo.realmSet$name(str);
                            processInfo.realmSet$applicationLabel(charSequence);
                            processInfo.realmSet$versionCode(i);
                            processInfo.realmSet$processId(-1);
                            processInfo.realmSet$isSystemApp(z2);
                            processInfo.realmGet$appSignatures().addAll(Signatures.getSignatureList(packageInfo));
                            processInfo.realmSet$importance(Config.IMPORTANCE_NOT_RUNNING);
                            processInfo.realmSet$installationPkg(packageManager.getInstallerPackageName(str));
                            processInfo.realmSet$versionName(packageInfo.versionName);
                            hashMap.put(str, processInfo);
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
        return hashMap;
    }

    public static ProcessInfo getInstalledPackage(Context context, String str) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return null;
        }
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(str, 4160);
            if (packageInfo == null) {
                return null;
            }
            ProcessInfo processInfo = new ProcessInfo();
            int i = packageInfo.versionCode;
            String charSequence = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
            int i2 = packageInfo.applicationInfo.flags;
            boolean z = false;
            if (((i2 & 1) > 0) || (i2 & 128) > 0) {
                z = true;
            }
            if (packageInfo.signatures.length > 0) {
                processInfo.realmSet$name(str);
                processInfo.realmSet$applicationLabel(charSequence);
                processInfo.realmSet$versionCode(i);
                processInfo.realmSet$processId(-1);
                processInfo.realmSet$isSystemApp(z);
                processInfo.realmGet$appSignatures().addAll(Signatures.getSignatureList(packageInfo));
                processInfo.realmSet$importance(Config.IMPORTANCE_NOT_RUNNING);
                processInfo.realmSet$installationPkg(packageManager.getInstallerPackageName(str));
                processInfo.realmSet$versionName(packageInfo.versionName);
            }
            return processInfo;
        } catch (NameNotFoundException unused) {
            return null;
        }
    }
}
