package com.mansoon.BatteryDouble.models.data;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.SampleRealmProxyInterface;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.internal.RealmObjectProxy;

public class Sample extends RealmObject implements SampleRealmProxyInterface {
    public BatteryDetails batteryDetails;
    public double batteryLevel;
    public String batteryState;
    public CallInfo callInfo;
    public String countryCode;
    public CpuStatus cpuStatus;
    public int database;
    public double distanceTraveled;
    public RealmList<Feature> features;
    @PrimaryKey
    public int id;
    public RealmList<LocationProvider> locationProviders;
    public int memoryActive;
    public int memoryFree;
    public int memoryInactive;
    public int memoryUser;
    public int memoryWired;
    public NetworkDetails networkDetails;
    public String networkStatus;
    public RealmList<ProcessInfo> processInfos;
    public int screenBrightness;
    public int screenOn;
    public Settings settings;
    public StorageDetails storageDetails;
    public String timeZone;
    @Index
    public long timestamp;
    public String triggeredBy;
    public String uuId;
    public int version;

    public BatteryDetails realmGet$batteryDetails() {
        return this.batteryDetails;
    }

    public double realmGet$batteryLevel() {
        return this.batteryLevel;
    }

    public String realmGet$batteryState() {
        return this.batteryState;
    }

    public CallInfo realmGet$callInfo() {
        return this.callInfo;
    }

    public String realmGet$countryCode() {
        return this.countryCode;
    }

    public CpuStatus realmGet$cpuStatus() {
        return this.cpuStatus;
    }

    public int realmGet$database() {
        return this.database;
    }

    public double realmGet$distanceTraveled() {
        return this.distanceTraveled;
    }

    public RealmList realmGet$features() {
        return this.features;
    }

    public int realmGet$id() {
        return this.id;
    }

    public RealmList realmGet$locationProviders() {
        return this.locationProviders;
    }

    public int realmGet$memoryActive() {
        return this.memoryActive;
    }

    public int realmGet$memoryFree() {
        return this.memoryFree;
    }

    public int realmGet$memoryInactive() {
        return this.memoryInactive;
    }

    public int realmGet$memoryUser() {
        return this.memoryUser;
    }

    public int realmGet$memoryWired() {
        return this.memoryWired;
    }

    public NetworkDetails realmGet$networkDetails() {
        return this.networkDetails;
    }

    public String realmGet$networkStatus() {
        return this.networkStatus;
    }

    public RealmList realmGet$processInfos() {
        return this.processInfos;
    }

    public int realmGet$screenBrightness() {
        return this.screenBrightness;
    }

    public int realmGet$screenOn() {
        return this.screenOn;
    }

    public Settings realmGet$settings() {
        return this.settings;
    }

    public StorageDetails realmGet$storageDetails() {
        return this.storageDetails;
    }

    public String realmGet$timeZone() {
        return this.timeZone;
    }

    public long realmGet$timestamp() {
        return this.timestamp;
    }

    public String realmGet$triggeredBy() {
        return this.triggeredBy;
    }

    public String realmGet$uuId() {
        return this.uuId;
    }

    public int realmGet$version() {
        return this.version;
    }

    public void realmSet$batteryDetails(BatteryDetails batteryDetails2) {
        this.batteryDetails = batteryDetails2;
    }

    public void realmSet$batteryLevel(double d) {
        this.batteryLevel = d;
    }

    public void realmSet$batteryState(String str) {
        this.batteryState = str;
    }

    public void realmSet$callInfo(CallInfo callInfo2) {
        this.callInfo = callInfo2;
    }

    public void realmSet$countryCode(String str) {
        this.countryCode = str;
    }

    public void realmSet$cpuStatus(CpuStatus cpuStatus2) {
        this.cpuStatus = cpuStatus2;
    }

    public void realmSet$database(int i) {
        this.database = i;
    }

    public void realmSet$distanceTraveled(double d) {
        this.distanceTraveled = d;
    }

    public void realmSet$features(RealmList realmList) {
        this.features = realmList;
    }

    public void realmSet$id(int i) {
        this.id = i;
    }

    public void realmSet$locationProviders(RealmList realmList) {
        this.locationProviders = realmList;
    }

    public void realmSet$memoryActive(int i) {
        this.memoryActive = i;
    }

    public void realmSet$memoryFree(int i) {
        this.memoryFree = i;
    }

    public void realmSet$memoryInactive(int i) {
        this.memoryInactive = i;
    }

    public void realmSet$memoryUser(int i) {
        this.memoryUser = i;
    }

    public void realmSet$memoryWired(int i) {
        this.memoryWired = i;
    }

    public void realmSet$networkDetails(NetworkDetails networkDetails2) {
        this.networkDetails = networkDetails2;
    }

    public void realmSet$networkStatus(String str) {
        this.networkStatus = str;
    }

    public void realmSet$processInfos(RealmList realmList) {
        this.processInfos = realmList;
    }

    public void realmSet$screenBrightness(int i) {
        this.screenBrightness = i;
    }

    public void realmSet$screenOn(int i) {
        this.screenOn = i;
    }

    public void realmSet$settings(Settings settings2) {
        this.settings = settings2;
    }

    public void realmSet$storageDetails(StorageDetails storageDetails2) {
        this.storageDetails = storageDetails2;
    }

    public void realmSet$timeZone(String str) {
        this.timeZone = str;
    }

    public void realmSet$timestamp(long j) {
        this.timestamp = j;
    }

    public void realmSet$triggeredBy(String str) {
        this.triggeredBy = str;
    }

    public void realmSet$uuId(String str) {
        this.uuId = str;
    }

    public void realmSet$version(int i) {
        this.version = i;
    }

    public Sample() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
