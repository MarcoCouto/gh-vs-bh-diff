package com.mansoon.BatteryDouble.models.data;

import com.tapjoy.TJAdUnitConstants.String;
import io.realm.MessageRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.internal.RealmObjectProxy;

public class Message extends RealmObject implements MessageRealmProxyInterface {
    public String body;
    public String date;
    @PrimaryKey
    public int id;
    public boolean read;
    public String title;
    public String type;

    public String realmGet$body() {
        return this.body;
    }

    public String realmGet$date() {
        return this.date;
    }

    public int realmGet$id() {
        return this.id;
    }

    public boolean realmGet$read() {
        return this.read;
    }

    public String realmGet$title() {
        return this.title;
    }

    public String realmGet$type() {
        return this.type;
    }

    public void realmSet$body(String str) {
        this.body = str;
    }

    public void realmSet$date(String str) {
        this.date = str;
    }

    public void realmSet$id(int i) {
        this.id = i;
    }

    public void realmSet$read(boolean z) {
        this.read = z;
    }

    public void realmSet$title(String str) {
        this.title = str;
    }

    public void realmSet$type(String str) {
        this.type = str;
    }

    public Message() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
        realmSet$type(String.VIDEO_INFO);
        realmSet$read(false);
    }

    public Message(int i, String str, String str2, String str3) {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
        realmSet$id(i);
        realmSet$type(String.VIDEO_INFO);
        realmSet$title(str);
        realmSet$body(str2);
        realmSet$date(str3);
        realmSet$read(false);
    }

    public Message(int i, String str, String str2, String str3, boolean z) {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
        realmSet$id(i);
        realmSet$type(String.VIDEO_INFO);
        realmSet$title(str);
        realmSet$body(str2);
        realmSet$date(str3);
        realmSet$read(z);
    }
}
