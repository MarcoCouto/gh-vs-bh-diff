package com.mansoon.BatteryDouble.models.data;

import io.realm.AppPermissionRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class AppPermission extends RealmObject implements AppPermissionRealmProxyInterface {
    public String permission;

    public String realmGet$permission() {
        return this.permission;
    }

    public void realmSet$permission(String str) {
        this.permission = str;
    }

    public AppPermission() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }

    public AppPermission(String str) {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
        realmSet$permission(str);
    }
}
