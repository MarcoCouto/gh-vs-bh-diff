package com.mansoon.BatteryDouble.models.data;

import io.realm.NetworkDetailsRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class NetworkDetails extends RealmObject implements NetworkDetailsRealmProxyInterface {
    public String mcc;
    public String mnc;
    public String mobileDataActivity;
    public String mobileDataStatus;
    public String mobileNetworkType;
    public String networkOperator;
    public NetworkStatistics networkStatistics;
    public String networkType;
    public int roamingEnabled;
    public String simOperator;
    public String wifiApStatus;
    public int wifiLinkSpeed;
    public int wifiSignalStrength;
    public String wifiStatus;

    public String realmGet$mcc() {
        return this.mcc;
    }

    public String realmGet$mnc() {
        return this.mnc;
    }

    public String realmGet$mobileDataActivity() {
        return this.mobileDataActivity;
    }

    public String realmGet$mobileDataStatus() {
        return this.mobileDataStatus;
    }

    public String realmGet$mobileNetworkType() {
        return this.mobileNetworkType;
    }

    public String realmGet$networkOperator() {
        return this.networkOperator;
    }

    public NetworkStatistics realmGet$networkStatistics() {
        return this.networkStatistics;
    }

    public String realmGet$networkType() {
        return this.networkType;
    }

    public int realmGet$roamingEnabled() {
        return this.roamingEnabled;
    }

    public String realmGet$simOperator() {
        return this.simOperator;
    }

    public String realmGet$wifiApStatus() {
        return this.wifiApStatus;
    }

    public int realmGet$wifiLinkSpeed() {
        return this.wifiLinkSpeed;
    }

    public int realmGet$wifiSignalStrength() {
        return this.wifiSignalStrength;
    }

    public String realmGet$wifiStatus() {
        return this.wifiStatus;
    }

    public void realmSet$mcc(String str) {
        this.mcc = str;
    }

    public void realmSet$mnc(String str) {
        this.mnc = str;
    }

    public void realmSet$mobileDataActivity(String str) {
        this.mobileDataActivity = str;
    }

    public void realmSet$mobileDataStatus(String str) {
        this.mobileDataStatus = str;
    }

    public void realmSet$mobileNetworkType(String str) {
        this.mobileNetworkType = str;
    }

    public void realmSet$networkOperator(String str) {
        this.networkOperator = str;
    }

    public void realmSet$networkStatistics(NetworkStatistics networkStatistics2) {
        this.networkStatistics = networkStatistics2;
    }

    public void realmSet$networkType(String str) {
        this.networkType = str;
    }

    public void realmSet$roamingEnabled(int i) {
        this.roamingEnabled = i;
    }

    public void realmSet$simOperator(String str) {
        this.simOperator = str;
    }

    public void realmSet$wifiApStatus(String str) {
        this.wifiApStatus = str;
    }

    public void realmSet$wifiLinkSpeed(int i) {
        this.wifiLinkSpeed = i;
    }

    public void realmSet$wifiSignalStrength(int i) {
        this.wifiSignalStrength = i;
    }

    public void realmSet$wifiStatus(String str) {
        this.wifiStatus = str;
    }

    public NetworkDetails() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
