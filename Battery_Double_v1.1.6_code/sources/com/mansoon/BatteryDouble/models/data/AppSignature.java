package com.mansoon.BatteryDouble.models.data;

import io.realm.AppSignatureRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class AppSignature extends RealmObject implements AppSignatureRealmProxyInterface {
    public String signature;

    public String realmGet$signature() {
        return this.signature;
    }

    public void realmSet$signature(String str) {
        this.signature = str;
    }

    public AppSignature() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }

    public AppSignature(String str) {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
        realmSet$signature(str);
    }
}
