package com.mansoon.BatteryDouble.models.data;

import io.realm.FeatureRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class Feature extends RealmObject implements FeatureRealmProxyInterface {
    public String key;
    public String value;

    public String realmGet$key() {
        return this.key;
    }

    public String realmGet$value() {
        return this.value;
    }

    public void realmSet$key(String str) {
        this.key = str;
    }

    public void realmSet$value(String str) {
        this.value = str;
    }

    public Feature() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
