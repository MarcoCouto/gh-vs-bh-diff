package com.mansoon.BatteryDouble.models.data;

import io.realm.BatterySessionRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.internal.RealmObjectProxy;

public class BatterySession extends RealmObject implements BatterySessionRealmProxyInterface {
    @PrimaryKey
    public int id;
    public float level;
    public int screenOn;
    @Index
    public long timestamp;
    public String triggeredBy;

    public int realmGet$id() {
        return this.id;
    }

    public float realmGet$level() {
        return this.level;
    }

    public int realmGet$screenOn() {
        return this.screenOn;
    }

    public long realmGet$timestamp() {
        return this.timestamp;
    }

    public String realmGet$triggeredBy() {
        return this.triggeredBy;
    }

    public void realmSet$id(int i) {
        this.id = i;
    }

    public void realmSet$level(float f) {
        this.level = f;
    }

    public void realmSet$screenOn(int i) {
        this.screenOn = i;
    }

    public void realmSet$timestamp(long j) {
        this.timestamp = j;
    }

    public void realmSet$triggeredBy(String str) {
        this.triggeredBy = str;
    }

    public BatterySession() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
