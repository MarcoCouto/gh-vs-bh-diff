package com.mansoon.BatteryDouble.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.mansoon.BatteryDouble.network.handlers.RegisterDeviceHandler;

public class RegisterDeviceTask extends AsyncTask<Context, Void, Void> {
    private static final String TAG = "RegisterDeviceTask";

    /* access modifiers changed from: protected */
    public Void doInBackground(Context... contextArr) {
        new RegisterDeviceHandler(contextArr[0]).registerClient();
        return null;
    }
}
