package com.mansoon.BatteryDouble.tasks;

import android.os.AsyncTask;
import com.mansoon.BatteryDouble.models.data.BatteryUsage;
import com.mansoon.BatteryDouble.util.DateUtils;
import io.realm.Realm;
import io.realm.Realm.Transaction;
import io.realm.RealmResults;

public class DeleteUsagesTask extends AsyncTask<Integer, Void, Boolean> {
    private static final String TAG = "DeleteUsagesTask";
    /* access modifiers changed from: private */
    public boolean mResponse;

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public Boolean doInBackground(Integer... numArr) {
        this.mResponse = false;
        Realm defaultInstance = Realm.getDefaultInstance();
        try {
            final int intValue = numArr[0].intValue();
            defaultInstance.executeTransaction(new Transaction() {
                public void execute(Realm realm) {
                    RealmResults findAll = realm.where(BatteryUsage.class).lessThan("timestamp", DateUtils.getMilliSecondsInterval(intValue)).findAll();
                    if (findAll != null && !findAll.isEmpty()) {
                        DeleteUsagesTask.this.mResponse = findAll.deleteAllFromRealm();
                    }
                }
            });
            defaultInstance.close();
            return Boolean.valueOf(this.mResponse);
        } catch (Throwable th) {
            defaultInstance.close();
            throw th;
        }
    }
}
