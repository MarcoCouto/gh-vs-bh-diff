package com.mansoon.BatteryDouble.events;

public class OpenMessageEvent {
    public final int index;

    public OpenMessageEvent(int i) {
        this.index = i;
    }
}
