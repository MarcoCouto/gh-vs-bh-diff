package com.appodealx.sdk.utils;

public class RequestInfoKeys {
    public static final String APPODEAL_ECPM = "ecpm";
    public static final String APPODEAL_ID = "id";
    public static final String APPODEAL_INFO = "appodeal";
    public static final String DISPLAY_MANAGER = "displaymanager";
    public static final String DISPLAY_MANAGER_VERSION = "displaymanager_ver";
    public static final String EXT = "ext";
    public static final String EXTRA_PARALLEL_BIDDING_INFO = "parallel_bidding_ext";
}
