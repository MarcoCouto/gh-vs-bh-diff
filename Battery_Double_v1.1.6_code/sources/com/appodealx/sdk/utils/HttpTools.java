package com.appodealx.sdk.utils;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.appodealx.sdk.AppodealX;
import com.appodealx.sdk.AppodealXExecutors;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpTools {

    public interface TrackingListener {
        void onComplete(boolean z);
    }

    private static class a extends AsyncTask<String, Void, Boolean> {
        @Nullable
        private final TrackingListener a;

        a(@Nullable TrackingListener trackingListener) {
            this.a = trackingListener;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x008c A[SYNTHETIC, Splitter:B:32:0x008c] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x0097 A[SYNTHETIC, Splitter:B:39:0x0097] */
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            HttpURLConnection httpURLConnection;
            Throwable th;
            String str = strArr[0];
            try {
                if (AppodealX.isLoggingEnabled()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("connection to URL:");
                    sb.append(str);
                    Log.d("AppodealX", sb.toString());
                }
                URL url = new URL(str);
                boolean z = true;
                HttpURLConnection.setFollowRedirects(true);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                try {
                    httpURLConnection.setConnectTimeout(5000);
                    httpURLConnection.setRequestProperty("Connection", "close");
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                    int responseCode = httpURLConnection.getResponseCode();
                    if (AppodealX.isLoggingEnabled()) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("response code:");
                        sb2.append(responseCode);
                        sb2.append(", for URL:");
                        sb2.append(str);
                        Log.d("AppodealX", sb2.toString());
                    }
                    if (responseCode != 200) {
                        if (responseCode != 204) {
                            z = false;
                        }
                    }
                    Boolean valueOf = Boolean.valueOf(z);
                    if (httpURLConnection != null) {
                        try {
                            httpURLConnection.disconnect();
                        } catch (Throwable unused) {
                        }
                    }
                    return valueOf;
                } catch (Throwable th2) {
                    th = th2;
                    try {
                        Log.d("AppodealX", str, th);
                        if (httpURLConnection != null) {
                        }
                        return Boolean.valueOf(false);
                    } catch (Throwable th3) {
                        th = th3;
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                }
            } catch (Throwable th4) {
                th = th4;
                httpURLConnection = null;
                if (httpURLConnection != null) {
                    try {
                        httpURLConnection.disconnect();
                    } catch (Throwable unused2) {
                    }
                }
                throw th;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (this.a != null) {
                this.a.onComplete(bool.booleanValue());
            }
        }
    }

    public static void fireUrl(@Nullable String str, @Nullable TrackingListener trackingListener) {
        if (!TextUtils.isEmpty(str)) {
            a aVar = new a(trackingListener);
            try {
                aVar.executeOnExecutor(AppodealXExecutors.networkExecutor, new String[]{str});
            } catch (Throwable th) {
                Log.d("AppodealX", "", th);
            }
        } else {
            Log.d("AppodealX", "Url is null or empty");
        }
    }
}
