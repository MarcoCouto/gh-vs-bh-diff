package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

class b extends a {
    private final BannerListener b;
    private final BannerView c;

    b(@NonNull Activity activity, @NonNull BannerView bannerView, long j, @NonNull List<JSONObject> list, @NonNull BannerListener bannerListener) {
        super(activity, j, list);
        this.c = bannerView;
        this.b = bannerListener;
    }

    /* access modifiers changed from: 0000 */
    public JSONArray a(@NonNull Activity activity, @NonNull InternalAdapterInterface internalAdapterInterface, @NonNull JSONObject jSONObject) {
        return internalAdapterInterface.getBannerRequestInfo(activity, jSONObject);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull Activity activity, @NonNull j jVar) {
        InternalAdapterInterface internalAdapterInterface = (InternalAdapterInterface) AppodealX.a().get(jVar.b());
        c cVar = new c(jVar, this.a);
        if (internalAdapterInterface != null) {
            this.c.setEventTracker(cVar);
            internalAdapterInterface.setLogging(AppodealX.isLoggingEnabled());
            internalAdapterInterface.loadBanner(activity, this.c, jVar.k(), new e(this.b, cVar));
            return;
        }
        a(AdError.InternalError);
        cVar.a("1008");
    }

    /* access modifiers changed from: 0000 */
    public void a(AdError adError) {
        this.b.onBannerFailedToLoad(adError);
    }
}
