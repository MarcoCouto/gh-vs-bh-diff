package com.appodealx.sdk;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodealx.sdk.utils.HttpTools;
import com.appodealx.sdk.utils.HttpTools.TrackingListener;
import com.google.android.gms.ads.formats.NativeContentAd;

class c {
    private final j a;
    private long b;
    private int c = -1;
    private boolean d = true;

    c(j jVar, long j) {
        this.a = jVar;
        this.b = j;
    }

    @Nullable
    private String a(@Nullable String str, long j, int i, @NonNull String str2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return str.replace("%%SEGMENT%%", String.valueOf(j)).replace("%25%25SEGMENT%25%25", String.valueOf(j)).replace("%%PLACEMENT%%", String.valueOf(i)).replace("%25%25PLACEMENT%25%25", String.valueOf(i)).replace("%%ERRORCODE%%", str2).replace("%25%25ERRORCODE%25%25", str2);
    }

    private void a(String str, long j, int i, @NonNull String str2, @Nullable TrackingListener trackingListener) {
        HttpTools.fireUrl(a(str, j, i, str2), trackingListener);
    }

    private void a(String str, TrackingListener trackingListener) {
        a(str, this.b, this.c, "", trackingListener);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.d) {
            a(this.a.g(), new TrackingListener() {
                public void onComplete(boolean z) {
                    if (!z) {
                        c.this.a(NativeContentAd.ASSET_ADVERTISER);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        this.c = i;
        a(this.a.f(), new TrackingListener() {
            public void onComplete(boolean z) {
                if (!z) {
                    c.this.a(NativeContentAd.ASSET_HEADLINE);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        if (!TextUtils.isEmpty(this.a.i()) && !TextUtils.isEmpty(str)) {
            if (NativeContentAd.ASSET_IMAGE.equals(str)) {
                this.d = false;
            }
            a(this.a.i(), this.b, this.c, str, null);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a(this.a.e(), new TrackingListener() {
            public void onComplete(boolean z) {
                if (!z) {
                    c.this.a(NativeContentAd.ASSET_BODY);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        a(this.a.h(), new TrackingListener() {
            public void onComplete(boolean z) {
                if (!z) {
                    c.this.a(NativeContentAd.ASSET_CALL_TO_ACTION);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return this.a.j();
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return this.a.b();
    }

    /* access modifiers changed from: 0000 */
    public String f() {
        return this.a.c();
    }

    /* access modifiers changed from: 0000 */
    public double g() {
        return this.a.d();
    }
}
