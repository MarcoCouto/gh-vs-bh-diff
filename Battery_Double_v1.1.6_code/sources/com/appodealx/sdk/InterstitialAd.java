package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import java.util.List;
import org.json.JSONObject;

public class InterstitialAd extends FullScreenAd {
    public void loadAd(@NonNull Activity activity, @NonNull String str, @NonNull List<JSONObject> list, long j, @NonNull FullScreenAdListener fullScreenAdListener) {
        g gVar = new g(activity, j, list, this, fullScreenAdListener);
        gVar.a(str);
    }
}
