package com.appodealx.sdk;

public abstract class Ad implements AdListener {
    private String a;
    private String b;
    private String c;
    private double d;

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.a = str;
    }

    public String getAdId() {
        return this.a;
    }

    public String getDemandSource() {
        return this.c;
    }

    public double getEcpm() {
        return this.d;
    }

    public String getNetworkName() {
        return this.b;
    }

    public void setDemandSource(String str) {
        this.c = str;
    }

    public void setEcpm(double d2) {
        this.d = d2;
    }

    public void setNetworkName(String str) {
        this.b = str;
    }
}
