package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

class g extends a {
    private final FullScreenAd b;
    private final FullScreenAdListener c;

    g(@NonNull Activity activity, long j, @NonNull List<JSONObject> list, @NonNull FullScreenAd fullScreenAd, @NonNull FullScreenAdListener fullScreenAdListener) {
        super(activity, j, list);
        this.b = fullScreenAd;
        this.c = fullScreenAdListener;
    }

    /* access modifiers changed from: 0000 */
    public JSONArray a(@NonNull Activity activity, @NonNull InternalAdapterInterface internalAdapterInterface, @NonNull JSONObject jSONObject) {
        return internalAdapterInterface.getInterstitialRequestInfo(activity, jSONObject);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull Activity activity, @NonNull j jVar) {
        InternalAdapterInterface internalAdapterInterface = (InternalAdapterInterface) AppodealX.a().get(jVar.b());
        c cVar = new c(jVar, this.a);
        if (internalAdapterInterface != null) {
            this.b.a(cVar);
            internalAdapterInterface.setLogging(AppodealX.isLoggingEnabled());
            internalAdapterInterface.loadInterstitial(activity, jVar.k(), this.b, new InternalFullScreenAdListener(this.c, cVar));
            return;
        }
        a(AdError.InternalError);
        cVar.a("1008");
    }

    /* access modifiers changed from: 0000 */
    public void a(AdError adError) {
        this.c.onFullScreenAdFailedToLoad(adError);
    }
}
