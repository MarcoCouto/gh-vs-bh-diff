package com.appodealx.sdk;

import android.util.Log;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AppodealXExecutors {
    private static final TimeUnit a = TimeUnit.SECONDS;
    private static final int b = Runtime.getRuntime().availableProcessors();
    private static final int c = Math.max(2, Math.min(b - 1, 4));
    private static final int d = (c + 1);
    private static final ThreadFactory e = new ThreadFactory() {
        private final AtomicInteger a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            StringBuilder sb = new StringBuilder();
            sb.append("AppodealXTask #");
            sb.append(this.a.getAndIncrement());
            Thread thread = new Thread(runnable, sb.toString());
            thread.setPriority(10);
            thread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable th) {
                    String str;
                    String str2;
                    if (th != null) {
                        str = "AppodealXExecutors";
                        str2 = th.toString();
                    } else {
                        str = "AppodealXExecutors";
                        str2 = "Unknown error";
                    }
                    Log.e(str, str2);
                }
            });
            return thread;
        }
    };
    private static BlockingQueue<Runnable> f = new LinkedBlockingQueue();
    public static final Executor networkExecutor;

    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(c, d, 1, a, f, e);
        networkExecutor = threadPoolExecutor;
    }
}
