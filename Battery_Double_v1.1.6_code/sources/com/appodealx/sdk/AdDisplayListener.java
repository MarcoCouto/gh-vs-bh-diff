package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;

public interface AdDisplayListener {
    void show(@NonNull Activity activity);
}
