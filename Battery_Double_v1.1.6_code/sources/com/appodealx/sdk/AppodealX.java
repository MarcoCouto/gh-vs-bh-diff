package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.appodeal.ads.AppodealNetworks;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class AppodealX {
    @VisibleForTesting
    static final Map<String, String> a = new HashMap<String, String>() {
        {
            put(AppodealNetworks.ADCOLONY, "com.appodealx.adcolony.AdColonyAdapter");
            put(AppodealNetworks.APPLOVIN, "com.appodealx.applovin.Applovin");
            put(AppodealNetworks.FACEBOOK, "com.appodealx.facebook.Facebook");
            put(AppodealNetworks.TAPJOY, "com.appodealx.tapjoy.Tapjoy");
            put(AppodealNetworks.VAST, "com.appodealx.vast.Vast");
            put(AppodealNetworks.MY_TARGET, "com.appodealx.mytarget.MyTarget");
            put(AppodealNetworks.MRAID, "com.appodealx.mraid.Mraid");
            put(AppodealNetworks.NAST, "com.appodealx.nast.Nast");
            put(AppodealNetworks.INNER_ACTIVE, "com.appodealx.s2s.Adapter");
            put(AppodealNetworks.SMAATO, "com.appodealx.s2s.Adapter");
            put("openx", "com.appodealx.s2s.Adapter");
            put("pubnative", "com.appodealx.s2s.Adapter");
        }
    };
    private static boolean b;
    private static Map<String, InternalAdapterInterface> c = new HashMap();

    private static InternalAdapterInterface a(String str) throws Exception {
        String str2 = (String) a.get(str);
        if (str2 != null) {
            Class cls = Class.forName(str2);
            if (InternalAdapterInterface.class.isAssignableFrom(cls)) {
                return (InternalAdapterInterface) cls.newInstance();
            }
        }
        return null;
    }

    static Map<String, InternalAdapterInterface> a() {
        return c;
    }

    private static void a(Activity activity, String str, JSONObject jSONObject) throws Throwable {
        String str2;
        String str3;
        Object[] objArr;
        if (!c.containsKey(str)) {
            InternalAdapterInterface a2 = a(str);
            if (a2 != null) {
                a2.initialize(activity, jSONObject);
                c.put(str, a2);
                str2 = "AppodealX";
                str3 = "Register adapter: %s";
                objArr = new Object[]{str};
            } else {
                str2 = "AppodealX";
                str3 = "AppodealX adapter %s not found";
                objArr = new Object[]{str};
            }
            Log.d(str2, String.format(str3, objArr));
        }
    }

    @NonNull
    public static Set<String> getSupportedAdaptersNames() {
        return a.keySet();
    }

    public static String getVersion() {
        return "1.0.0";
    }

    public static void initialize(@NonNull Activity activity, @NonNull List<JSONObject> list) {
        for (JSONObject jSONObject : list) {
            try {
                a(activity, jSONObject.getString("status"), jSONObject);
            } catch (Throwable unused) {
                Log.e("AppodealX", String.format("AppodealX adapter %s not found", new Object[]{jSONObject.toString()}));
            }
        }
    }

    public static boolean isLoggingEnabled() {
        return b;
    }

    public static void setLogging(boolean z) {
        b = z;
    }

    public static void updateConsent(Activity activity, boolean z, boolean z2) {
        for (InternalAdapterInterface updateConsent : c.values()) {
            updateConsent.updateConsent(activity, z, z2);
        }
    }

    public static void updateCoppa(boolean z) {
        for (InternalAdapterInterface updateCoppa : c.values()) {
            updateCoppa.updateCoppa(z);
        }
    }
}
