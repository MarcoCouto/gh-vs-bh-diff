package com.appodealx.sdk;

import android.support.annotation.NonNull;
import com.google.android.gms.ads.formats.NativeContentAd;

public class InternalFullScreenAdListener implements FullScreenAdListener {
    private final FullScreenAdListener a;
    private final c b;

    InternalFullScreenAdListener(@NonNull FullScreenAdListener fullScreenAdListener, @NonNull c cVar) {
        this.a = fullScreenAdListener;
        this.b = cVar;
    }

    public int getPlacementId() {
        return this.a.getPlacementId();
    }

    public void onFullScreenAdClicked() {
        this.b.b();
        this.a.onFullScreenAdClicked();
    }

    public void onFullScreenAdClosed(boolean z) {
        this.a.onFullScreenAdClosed(z);
    }

    public void onFullScreenAdCompleted() {
        this.b.c();
        this.a.onFullScreenAdCompleted();
    }

    public void onFullScreenAdExpired() {
        this.a.onFullScreenAdExpired();
    }

    public void onFullScreenAdFailedToLoad(@NonNull AdError adError) {
        this.b.a("1010");
        this.a.onFullScreenAdFailedToLoad(adError);
    }

    public void onFullScreenAdFailedToShow(@NonNull AdError adError) {
        this.a.onFullScreenAdFailedToShow(adError);
    }

    public void onFullScreenAdLoaded(FullScreenAdObject fullScreenAdObject) {
        this.b.a();
        fullScreenAdObject.a(this.b.d());
        fullScreenAdObject.setNetworkName(this.b.e());
        fullScreenAdObject.setDemandSource(this.b.f());
        fullScreenAdObject.setEcpm(this.b.g());
        this.a.onFullScreenAdLoaded(fullScreenAdObject);
    }

    public void onFullScreenAdShown() {
        this.b.a(getPlacementId());
        this.a.onFullScreenAdShown();
    }

    public void trackCloseTimeError() {
        this.b.a(NativeContentAd.ASSET_MEDIA_VIDEO);
    }
}
