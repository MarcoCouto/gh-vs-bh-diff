package com.appodealx.sdk;

import android.support.annotation.NonNull;

class e implements BannerListener {
    private final BannerListener a;
    private final c b;

    e(@NonNull BannerListener bannerListener, @NonNull c cVar) {
        this.a = bannerListener;
        this.b = cVar;
    }

    public void onBannerClicked() {
        this.b.b();
        this.a.onBannerClicked();
    }

    public void onBannerExpired() {
        this.a.onBannerExpired();
    }

    public void onBannerFailedToLoad(@NonNull AdError adError) {
        this.b.a("1010");
        this.a.onBannerFailedToLoad(adError);
    }

    public void onBannerLoaded(BannerView bannerView) {
        this.b.a();
        bannerView.setAdId(this.b.d());
        bannerView.setNetworkName(this.b.e());
        bannerView.setDemandSource(this.b.f());
        bannerView.setEcpm(this.b.g());
        this.a.onBannerLoaded(bannerView);
    }
}
