package com.appodealx.sdk;

import android.support.annotation.NonNull;

class f implements NativeListener {
    private final NativeListener a;
    private final c b;

    f(@NonNull NativeListener nativeListener, @NonNull c cVar) {
        this.a = nativeListener;
        this.b = cVar;
    }

    public void onNativeClicked() {
        this.a.onNativeClicked();
        this.b.b();
    }

    public void onNativeExpired() {
        this.a.onNativeExpired();
    }

    public void onNativeFailedToLoad(@NonNull AdError adError) {
        this.b.a("1010");
        this.a.onNativeFailedToLoad(adError);
    }

    public void onNativeLoaded(NativeAdObject nativeAdObject) {
        this.b.a();
        nativeAdObject.setEventTracker(this.b);
        nativeAdObject.a(this.b.d());
        nativeAdObject.setNetworkName(this.b.e());
        nativeAdObject.setDemandSource(this.b.f());
        nativeAdObject.setEcpm(this.b.g());
        this.a.onNativeLoaded(nativeAdObject);
    }
}
