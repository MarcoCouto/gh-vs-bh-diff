package com.appodealx.sdk;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;

class i extends AsyncTask<a, Void, j> {
    private final String a;
    private final b b;

    interface a {
        @NonNull
        JSONArray a();
    }

    interface b {
        void a(@Nullable j jVar);
    }

    i(@Nullable String str, @NonNull b bVar) {
        this.a = str;
        this.b = bVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058 A[SYNTHETIC, Splitter:B:25:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0068 A[SYNTHETIC, Splitter:B:32:0x0068] */
    private JSONObject a(InputStream inputStream) {
        BufferedReader bufferedReader;
        try {
            StringBuilder sb = new StringBuilder(inputStream.available());
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    sb.append(10);
                } catch (Throwable th) {
                    th = th;
                    try {
                        Log.e("AppodealX-Request", "", th);
                        if (bufferedReader != null) {
                        }
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        if (bufferedReader != null) {
                        }
                        throw th;
                    }
                }
            }
            if (sb.length() > 0) {
                sb.setLength(sb.length() - 1);
            }
            JSONObject jSONObject = new JSONObject(sb.toString());
            try {
                bufferedReader.close();
            } catch (Throwable th3) {
                Log.e("AppodealX-Request", "", th3);
            }
            return jSONObject;
        } catch (Throwable th4) {
            th = th4;
            bufferedReader = null;
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (Throwable th5) {
                    Log.e("AppodealX-Request", "", th5);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008d A[SYNTHETIC, Splitter:B:34:0x008d] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0092 A[Catch:{ Throwable -> 0x0098 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x009d A[SYNTHETIC, Splitter:B:43:0x009d] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00a2 A[Catch:{ Throwable -> 0x00a8 }] */
    /* renamed from: a */
    public j doInBackground(a... aVarArr) {
        HttpURLConnection httpURLConnection;
        BufferedOutputStream bufferedOutputStream;
        BufferedOutputStream bufferedOutputStream2 = null;
        try {
            byte[] bytes = aVarArr[0].a().toString().getBytes("UTF-8");
            httpURLConnection = (HttpURLConnection) new URL(this.a).openConnection();
            try {
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.setReadTimeout(5000);
                httpURLConnection.setDoOutput(true);
                bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
            } catch (Throwable th) {
                th = th;
                if (httpURLConnection != null) {
                }
                if (bufferedOutputStream2 != null) {
                }
                throw th;
            }
            try {
                bufferedOutputStream.write(bytes);
                bufferedOutputStream.flush();
                if (httpURLConnection.getResponseCode() == 200) {
                    j jVar = new j(httpURLConnection.getHeaderFields(), a(httpURLConnection.getInputStream()));
                    if (httpURLConnection != null) {
                        try {
                            httpURLConnection.disconnect();
                        } catch (Throwable unused) {
                        }
                    }
                    bufferedOutputStream.flush();
                    bufferedOutputStream.close();
                    return jVar;
                }
                if (httpURLConnection != null) {
                    try {
                        httpURLConnection.disconnect();
                    } catch (Throwable unused2) {
                    }
                }
                bufferedOutputStream.flush();
                bufferedOutputStream.close();
                return null;
            } catch (Throwable th2) {
                th = th2;
                Log.e("AppodealX", "", th);
                if (httpURLConnection != null) {
                }
                if (bufferedOutputStream != null) {
                }
                return null;
            }
        } catch (Throwable th3) {
            th = th3;
            httpURLConnection = null;
            if (httpURLConnection != null) {
                try {
                    httpURLConnection.disconnect();
                } catch (Throwable unused3) {
                    throw th;
                }
            }
            if (bufferedOutputStream2 != null) {
                bufferedOutputStream2.flush();
                bufferedOutputStream2.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(j jVar) {
        super.onPostExecute(jVar);
        this.b.a(jVar);
    }
}
