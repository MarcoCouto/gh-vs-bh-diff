package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class NativeAd implements AdListener {
    private NativeAdObject a;
    private c b;

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        this.b = cVar;
    }

    public void destroy() {
        if (this.a != null) {
            this.a.destroy();
        }
    }

    public void loadAd(@NonNull Activity activity, @NonNull String str, @NonNull List<JSONObject> list, @NonNull Map<String, String> map, long j, @NonNull NativeListener nativeListener) {
        h hVar = new h(activity, j, list, map, this, nativeListener);
        String str2 = str;
        hVar.a(str);
    }

    @CallSuper
    public void onAdClick() {
        this.b.b();
    }

    @CallSuper
    public void onImpression(int i) {
        this.b.a(i);
    }

    public void setAd(NativeAdObject nativeAdObject) {
        this.a = nativeAdObject;
    }

    public void trackError(int i) {
        if (this.b != null) {
            this.b.a(String.valueOf(i));
        }
    }
}
