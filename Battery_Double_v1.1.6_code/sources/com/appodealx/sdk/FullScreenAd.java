package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import java.util.List;
import org.json.JSONObject;

public abstract class FullScreenAd implements AdDisplayListener, AdListener {
    private FullScreenAdObject a;
    private c b;

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        this.b = cVar;
    }

    public void destroy() {
        if (this.a != null) {
            this.a.destroy();
        }
    }

    public abstract void loadAd(@NonNull Activity activity, @NonNull String str, @NonNull List<JSONObject> list, long j, @NonNull FullScreenAdListener fullScreenAdListener);

    public void setAd(FullScreenAdObject fullScreenAdObject) {
        this.a = fullScreenAdObject;
    }

    public void show(@NonNull Activity activity) {
        if (this.a != null) {
            this.a.show(activity);
        }
    }

    public void trackError(int i) {
        if (this.b != null) {
            this.b.a(String.valueOf(i));
        }
    }
}
