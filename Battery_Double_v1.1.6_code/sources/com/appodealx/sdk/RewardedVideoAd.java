package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import java.util.List;
import org.json.JSONObject;

public class RewardedVideoAd extends FullScreenAd {
    public void loadAd(@NonNull Activity activity, @NonNull String str, @NonNull List<JSONObject> list, long j, @NonNull FullScreenAdListener fullScreenAdListener) {
        k kVar = new k(activity, j, list, this, fullScreenAdListener);
        kVar.a(str);
    }
}
