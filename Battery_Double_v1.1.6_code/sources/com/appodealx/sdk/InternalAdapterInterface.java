package com.appodealx.sdk;

import android.app.Activity;
import android.support.annotation.NonNull;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class InternalAdapterInterface {
    @NonNull
    public JSONArray getBannerRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return new JSONArray();
    }

    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return new JSONArray();
    }

    @NonNull
    public JSONArray getNativeRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return new JSONArray();
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return new JSONArray();
    }

    public void initialize(@NonNull Activity activity, @NonNull JSONObject jSONObject) throws Throwable {
    }

    public void loadBanner(@NonNull Activity activity, @NonNull BannerView bannerView, JSONObject jSONObject, BannerListener bannerListener) {
        bannerListener.onBannerFailedToLoad(AdError.InternalError);
    }

    public void loadInterstitial(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
    }

    public void loadNative(@NonNull Activity activity, JSONObject jSONObject, @NonNull Map<String, String> map, NativeAd nativeAd, NativeListener nativeListener) {
        nativeListener.onNativeFailedToLoad(AdError.InternalError);
    }

    public void loadRewardedVideo(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
    }

    public void setLogging(boolean z) {
    }

    public void updateConsent(Activity activity, boolean z, boolean z2) {
    }

    public void updateCoppa(boolean z) {
    }
}
