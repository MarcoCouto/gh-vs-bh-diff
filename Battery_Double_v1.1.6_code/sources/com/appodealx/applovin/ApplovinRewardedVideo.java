package com.appodealx.applovin;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.applovin.adview.AppLovinIncentivizedInterstitial;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;

public class ApplovinRewardedVideo extends ApplovinFullScreenAd {
    public /* bridge */ /* synthetic */ void destroy() {
        super.destroy();
    }

    public /* bridge */ /* synthetic */ void load() {
        super.load();
    }

    ApplovinRewardedVideo(@NonNull String str, @NonNull AppLovinSdk appLovinSdk, @NonNull FullScreenAdListener fullScreenAdListener) {
        super(str, appLovinSdk, fullScreenAdListener);
    }

    public void show(@NonNull Activity activity) {
        if (this.appLovinAd != null) {
            AppLovinIncentivizedInterstitial.create(this.appLovinSdk).show(this.appLovinAd, (Context) activity, (AppLovinAdRewardListener) this.applovinFullScreenAdListener, (AppLovinAdVideoPlaybackListener) this.applovinFullScreenAdListener, (AppLovinAdDisplayListener) this.applovinFullScreenAdListener, (AppLovinAdClickListener) this.applovinFullScreenAdListener);
            return;
        }
        this.listener.onFullScreenAdFailedToShow(AdError.RendererNotReady);
    }
}
