package com.appodealx.applovin;

import android.app.Activity;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdk.SdkInitializationListener;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.appodeal.ads.AppodealNetworks;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Applovin extends InternalAdapterInterface {
    private static boolean hasConsent;

    private static class ApplovinSdkState implements SdkInitializationListener {
        private static ConcurrentHashMap<String, ApplovinSdkState> states = new ConcurrentHashMap<>();
        private AppLovinSdk appLovinSdk;
        private final String applovinKey;
        private ConcurrentLinkedQueue<ApplovinSdkStateListener> externalListeners;
        private boolean isInitialized;

        interface ApplovinSdkStateListener {
            void onInitialized(AppLovinSdk appLovinSdk);
        }

        @NonNull
        static ApplovinSdkState get(String str) {
            ApplovinSdkState applovinSdkState = (ApplovinSdkState) states.get(str);
            if (applovinSdkState == null) {
                synchronized (ApplovinSdkState.class) {
                    applovinSdkState = (ApplovinSdkState) states.get(str);
                    if (applovinSdkState == null) {
                        applovinSdkState = new ApplovinSdkState(str);
                        states.put(str, applovinSdkState);
                    }
                }
            }
            return applovinSdkState;
        }

        private ApplovinSdkState(String str) {
            this.applovinKey = str;
        }

        /* access modifiers changed from: 0000 */
        public void initialize(@NonNull Activity activity, @Nullable ApplovinSdkStateListener applovinSdkStateListener) {
            if (!this.isInitialized) {
                if (applovinSdkStateListener != null) {
                    if (this.externalListeners == null) {
                        this.externalListeners = new ConcurrentLinkedQueue<>();
                    }
                    this.externalListeners.add(applovinSdkStateListener);
                }
                this.appLovinSdk = AppLovinSdk.getInstance(this.applovinKey, new AppLovinSdkSettings(activity), activity);
                this.appLovinSdk.initializeSdk((SdkInitializationListener) this);
            } else if (applovinSdkStateListener != null) {
                applovinSdkStateListener.onInitialized(this.appLovinSdk);
            }
        }

        public void onSdkInitialized(AppLovinSdkConfiguration appLovinSdkConfiguration) {
            this.isInitialized = true;
            if (this.externalListeners != null) {
                Iterator it = this.externalListeners.iterator();
                while (it.hasNext()) {
                    ((ApplovinSdkStateListener) it.next()).onInitialized(this.appLovinSdk);
                }
                this.externalListeners.clear();
            }
        }

        /* access modifiers changed from: 0000 */
        @Nullable
        public AppLovinSdk getAppLovinSdk() {
            return this.appLovinSdk;
        }
    }

    public void updateConsent(Activity activity, boolean z, boolean z2) {
        hasConsent = z;
    }

    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(activity, jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    public void loadInterstitial(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        try {
            String string = jSONObject.getString("ad_token");
            AppLovinSdk instance = AppLovinSdk.getInstance(jSONObject.getString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY), new AppLovinSdkSettings(), activity);
            if (instance == null || TextUtils.isEmpty(string)) {
                fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
                return;
            }
            instance.initializeSdk();
            AppLovinPrivacySettings.setHasUserConsent(hasConsent, activity);
            ApplovinInterstitial applovinInterstitial = new ApplovinInterstitial(string, instance, fullScreenAdListener);
            applovinInterstitial.load();
            fullScreenAd.setAd(applovinInterstitial);
        } catch (JSONException unused) {
            fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
        }
    }

    public void loadRewardedVideo(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        try {
            String string = jSONObject.getString("ad_token");
            AppLovinSdk instance = AppLovinSdk.getInstance(jSONObject.getString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY), new AppLovinSdkSettings(), activity);
            if (instance == null || TextUtils.isEmpty(string)) {
                fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
                return;
            }
            instance.initializeSdk();
            AppLovinPrivacySettings.setHasUserConsent(hasConsent, activity);
            ApplovinRewardedVideo applovinRewardedVideo = new ApplovinRewardedVideo(string, instance, fullScreenAdListener);
            applovinRewardedVideo.load();
            fullScreenAd.setAd(applovinRewardedVideo);
        } catch (JSONException unused) {
            fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
        }
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(activity, jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    public void loadBanner(@NonNull Activity activity, @NonNull BannerView bannerView, JSONObject jSONObject, BannerListener bannerListener) {
        try {
            String string = jSONObject.getString("ad_token");
            AppLovinSdk instance = AppLovinSdk.getInstance(jSONObject.getString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY), new AppLovinSdkSettings(), activity);
            if (instance == null || TextUtils.isEmpty(string)) {
                bannerListener.onBannerFailedToLoad(AdError.InternalError);
                return;
            }
            instance.initializeSdk();
            AppLovinPrivacySettings.setHasUserConsent(hasConsent, activity);
            new ApplovinBanner(bannerView, string, instance, bannerListener).load();
        } catch (JSONException unused) {
            bannerListener.onBannerFailedToLoad(AdError.InternalError);
        }
    }

    @NonNull
    public JSONArray getBannerRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(activity, jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    @Nullable
    private JSONObject getRequestInfoFromSettings(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        try {
            if (VERSION.SDK_INT < 16) {
                return null;
            }
            String string = jSONObject.getString("applovin_key");
            if (TextUtils.isEmpty(string)) {
                return null;
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, AppodealNetworks.APPLOVIN);
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, AppLovinSdk.VERSION);
            AppLovinPrivacySettings.setHasUserConsent(hasConsent, activity);
            ApplovinSdkState applovinSdkState = ApplovinSdkState.get(string);
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            applovinSdkState.initialize(activity, new ApplovinSdkStateListener() {
                public void onInitialized(AppLovinSdk appLovinSdk) {
                    countDownLatch.countDown();
                }
            });
            try {
                countDownLatch.await(5, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            AppLovinSdk appLovinSdk = applovinSdkState.getAppLovinSdk();
            if (appLovinSdk == null) {
                return null;
            }
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, string);
            jSONObject3.put("zone_id", jSONObject.getString("zone_id"));
            String bidToken = appLovinSdk.getAdService().getBidToken();
            if (!TextUtils.isEmpty(bidToken)) {
                jSONObject3.put("ad_token", bidToken);
            }
            JSONObject optJSONObject = jSONObject.optJSONObject(RequestInfoKeys.EXTRA_PARALLEL_BIDDING_INFO);
            if (optJSONObject != null) {
                Iterator keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    jSONObject3.put(str, optJSONObject.get(str));
                }
            }
            jSONObject2.put(RequestInfoKeys.EXT, jSONObject3);
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", jSONObject.getString("id"));
            jSONObject4.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM));
            jSONObject2.put("appodeal", jSONObject4);
            return jSONObject2;
        } catch (JSONException e2) {
            Log.e("Appodealx-Applovin", e2.getMessage());
            return null;
        }
    }
}
