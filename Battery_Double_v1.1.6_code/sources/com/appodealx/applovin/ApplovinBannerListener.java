package com.appodealx.applovin;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;

public class ApplovinBannerListener implements AppLovinAdClickListener, AppLovinAdLoadListener {
    private ApplovinBanner applovinBanner;
    private BannerListener listener;

    ApplovinBannerListener(ApplovinBanner applovinBanner2, BannerListener bannerListener) {
        this.applovinBanner = applovinBanner2;
        this.listener = bannerListener;
    }

    public void adClicked(AppLovinAd appLovinAd) {
        this.listener.onBannerClicked();
    }

    public void adReceived(AppLovinAd appLovinAd) {
        this.applovinBanner.fillContainer(appLovinAd, this);
    }

    public void failedToReceiveAd(int i) {
        this.listener.onBannerFailedToLoad(AdError.NoFill);
    }
}
