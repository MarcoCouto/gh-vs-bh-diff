package com.appodealx.applovin;

import android.support.annotation.NonNull;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinSdk;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;

abstract class ApplovinFullScreenAd extends FullScreenAdObject {
    private String adToken;
    AppLovinAd appLovinAd;
    AppLovinSdk appLovinSdk;
    ApplovinFullScreenAdListener applovinFullScreenAdListener;
    protected FullScreenAdListener listener;

    public void destroy() {
    }

    ApplovinFullScreenAd(@NonNull String str, @NonNull AppLovinSdk appLovinSdk2, @NonNull FullScreenAdListener fullScreenAdListener) {
        this.adToken = str;
        this.appLovinSdk = appLovinSdk2;
        this.listener = fullScreenAdListener;
    }

    public void load() {
        AppLovinAdService adService = this.appLovinSdk.getAdService();
        this.applovinFullScreenAdListener = new ApplovinFullScreenAdListener(this, this.listener);
        adService.loadNextAdForAdToken(this.adToken, this.applovinFullScreenAdListener);
    }
}
