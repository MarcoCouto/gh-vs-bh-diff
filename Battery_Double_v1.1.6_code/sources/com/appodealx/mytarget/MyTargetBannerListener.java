package com.appodealx.mytarget;

import android.support.annotation.NonNull;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;
import com.my.target.ads.MyTargetView;
import com.my.target.ads.MyTargetView.MyTargetViewListener;

public class MyTargetBannerListener implements MyTargetViewListener {
    private BannerListener bannerListener;
    private MyTargetBanner myTargetBanner;

    public void onShow(@NonNull MyTargetView myTargetView) {
    }

    MyTargetBannerListener(@NonNull MyTargetBanner myTargetBanner2, BannerListener bannerListener2) {
        this.myTargetBanner = myTargetBanner2;
        this.bannerListener = bannerListener2;
    }

    public void onLoad(@NonNull MyTargetView myTargetView) {
        this.myTargetBanner.fillContainer();
    }

    public void onNoAd(@NonNull String str, @NonNull MyTargetView myTargetView) {
        this.bannerListener.onBannerFailedToLoad(AdError.NoFill);
    }

    public void onClick(@NonNull MyTargetView myTargetView) {
        this.bannerListener.onBannerClicked();
    }
}
