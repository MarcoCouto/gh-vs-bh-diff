package com.appodealx.mytarget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import com.appodealx.sdk.NativeAdObject;
import com.appodealx.sdk.NativeListener;
import com.my.target.nativeads.NativeAd;
import com.my.target.nativeads.banners.NativePromoBanner;
import com.my.target.nativeads.factories.NativeViewsFactory;
import com.my.target.nativeads.views.ContentStreamAdView;
import com.my.target.nativeads.views.MediaAdView;
import java.util.List;
import java.util.Map;

public class MyTargetNative extends NativeAdObject {
    private NativeAd nativeAd;
    private String nativeAdType;
    private NativeListener nativeListener;

    MyTargetNative(NativeListener nativeListener2) {
        this.nativeListener = nativeListener2;
    }

    public void load(Context context, @NonNull Map<String, String> map, int i, String str) {
        this.nativeAdType = (String) map.get("native_ad_type");
        NativeAd nativeAd2 = new NativeAd(i, context);
        nativeAd2.setListener(new MyTargetNativeListener(this, this.nativeListener));
        nativeAd2.getCustomParams().setCustomParam("bid_id", str);
        nativeAd2.load();
    }

    /* access modifiers changed from: 0000 */
    public NativeAdObject createNativeAd(NativeAd nativeAd2) {
        this.nativeAd = nativeAd2;
        NativePromoBanner banner = nativeAd2.getBanner();
        if (banner != null) {
            if (banner.getIcon() != null) {
                setIcon(banner.getIcon().getUrl());
            }
            if (banner.getImage() != null) {
                setImage(banner.getImage().getUrl());
            }
            if (banner.getRating() != 0.0f) {
                setRating((double) banner.getRating());
            }
            setTitle(banner.getTitle());
            setDescription(banner.getDescription());
            setCta(banner.getCtaText());
        }
        return this;
    }

    public View getMediaView(Context context) {
        if (this.nativeAd == null || "NoVideo".equals(this.nativeAdType) || !hasVideo()) {
            return super.getMediaView(context);
        }
        ContentStreamAdView contentStreamView = NativeViewsFactory.getContentStreamView(this.nativeAd, context);
        MediaAdView mediaAdView = contentStreamView.getMediaAdView();
        contentStreamView.removeView(mediaAdView);
        return mediaAdView;
    }

    public String getAgeRestrictions() {
        if (this.nativeAd == null || this.nativeAd.getBanner() == null) {
            return super.getAgeRestrictions();
        }
        return this.nativeAd.getBanner().getAgeRestrictions();
    }

    public void registerViewForInteraction(View view, List<View> list) {
        if (this.nativeAd != null) {
            this.nativeAd.registerView(view);
        }
    }

    public void unregisterViewForInteraction() {
        if (this.nativeAd != null) {
            this.nativeAd.unregisterView();
        }
    }

    public boolean hasVideo() {
        return (this.nativeAd == null || this.nativeAd.getBanner() == null || !this.nativeAd.getBanner().hasVideo()) ? false : true;
    }

    public void destroy() {
        if (this.nativeAd != null) {
            this.nativeAd.setListener(null);
        }
    }
}
