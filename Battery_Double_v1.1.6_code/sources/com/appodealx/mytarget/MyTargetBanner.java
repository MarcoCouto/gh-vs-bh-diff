package com.appodealx.mytarget;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.view.ViewGroup.LayoutParams;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;
import com.my.target.ads.MyTargetView;

public class MyTargetBanner {
    /* access modifiers changed from: private */
    public BannerListener bannerListener;
    /* access modifiers changed from: private */
    public BannerView bannerView;
    private Runnable destroyRunnable = new Runnable() {
        public void run() {
            if (MyTargetBanner.this.myTargetView != null) {
                MyTargetBanner.this.myTargetView.destroy();
                MyTargetBanner.this.myTargetView = null;
            }
            MyTargetBanner.this.bannerView = null;
        }
    };
    /* access modifiers changed from: private */
    public MyTargetView myTargetView;

    MyTargetBanner(@NonNull BannerView bannerView2, BannerListener bannerListener2) {
        this.bannerView = bannerView2;
        this.bannerListener = bannerListener2;
    }

    public void load(Context context, int i, String str) {
        this.bannerView.setDestroyRunnable(this.destroyRunnable);
        this.myTargetView = new MyTargetView(context.getApplicationContext());
        this.myTargetView.init(i, false);
        this.myTargetView.setLayoutParams(new LayoutParams(-1, -2));
        this.myTargetView.setListener(new MyTargetBannerListener(this, this.bannerListener));
        if (this.myTargetView.getCustomParams() != null) {
            this.myTargetView.getCustomParams().setCustomParam("bid_id", str);
        }
        this.myTargetView.load();
    }

    /* access modifiers changed from: 0000 */
    public void fillContainer() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (MyTargetBanner.this.bannerView != null) {
                    MyTargetBanner.this.bannerView.setBannerHeight(50);
                    MyTargetBanner.this.bannerView.addView(MyTargetBanner.this.myTargetView);
                    MyTargetBanner.this.bannerListener.onBannerLoaded(MyTargetBanner.this.bannerView);
                }
            }
        });
    }
}
