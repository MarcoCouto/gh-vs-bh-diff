package com.appodealx.mraid;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;
import com.explorestack.iab.mraid.MRAIDNativeFeatureListener;
import com.explorestack.iab.mraid.MRAIDView;
import com.explorestack.iab.mraid.MRAIDViewListener;
import com.explorestack.iab.utils.Utils;

public class MraidBannerListener implements MRAIDViewListener, MRAIDNativeFeatureListener {
    private BannerListener listener;
    /* access modifiers changed from: private */
    public MraidBanner mraidBanner;

    public void mraidNativeFeatureCallTel(String str) {
    }

    public void mraidNativeFeatureCreateCalendarEvent(String str) {
    }

    public void mraidNativeFeaturePlayVideo(String str) {
    }

    public void mraidNativeFeatureSendSms(String str) {
    }

    public void mraidNativeFeatureStorePicture(String str) {
    }

    public void mraidViewClose(MRAIDView mRAIDView) {
    }

    public void mraidViewExpand(MRAIDView mRAIDView) {
    }

    public boolean mraidViewResize(MRAIDView mRAIDView, int i, int i2, int i3, int i4) {
        return false;
    }

    MraidBannerListener(@NonNull MraidBanner mraidBanner2, @NonNull BannerListener bannerListener) {
        this.mraidBanner = mraidBanner2;
        this.listener = bannerListener;
    }

    public void mraidViewLoaded(MRAIDView mRAIDView) {
        this.mraidBanner.fillContainer();
    }

    public void mraidViewNoFill(MRAIDView mRAIDView) {
        this.listener.onBannerFailedToLoad(AdError.NoFill);
    }

    public void mraidNativeFeatureOpenBrowser(String str, WebView webView) {
        this.listener.onBannerClicked();
        if (str != null && this.mraidBanner.getMraidView() != null) {
            Utils.addBannerSpinnerView(this.mraidBanner.getMraidView());
            Utils.openBrowser(this.mraidBanner.getMraidView().getContext(), str, new Runnable() {
                public void run() {
                    Utils.hideBannerSpinnerView(MraidBannerListener.this.mraidBanner.getMraidView());
                }
            });
        }
    }
}
