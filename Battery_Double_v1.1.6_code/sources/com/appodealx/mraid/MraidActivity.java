package com.appodealx.mraid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.InternalFullScreenAdListener;
import com.explorestack.iab.utils.Assets;
import com.explorestack.iab.utils.Utils;
import com.explorestack.iab.vast.VideoType;
import com.explorestack.iab.vast.view.CircleCountdownView;

public class MraidActivity extends Activity {
    private static final int CLOSE_REGION_SIZE = 50;
    /* access modifiers changed from: private */
    public static MraidInterstitial mraidInterstitial;
    private boolean canSkip;
    /* access modifiers changed from: private */
    public CircleCountdownView circleCountdownView;
    private Handler handler;
    private ProgressBar progressBar;
    private Runnable showCloseTime;

    public static void show(Context context, MraidInterstitial mraidInterstitial2, VideoType videoType) {
        mraidInterstitial = mraidInterstitial2;
        try {
            Intent intent = new Intent(context, MraidActivity.class);
            intent.addFlags(268435456);
            intent.addFlags(8388608);
            intent.putExtra("type", videoType);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            if (!(mraidInterstitial2 == null || mraidInterstitial2.getListener() == null)) {
                mraidInterstitial2.getListener().onFullScreenAdFailedToLoad(AdError.InternalError);
            }
            mraidInterstitial = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        try {
            if (!getIntent().hasExtra("type")) {
                finishActivity();
                return;
            }
            VideoType videoType = (VideoType) getIntent().getSerializableExtra("type");
            setRequestedOrientation(Utils.getScreenOrientation(this));
            if (videoType == VideoType.NonRewarded) {
                this.canSkip = true;
            }
            hideTitleStatusBars();
            addBackgroundView();
            if (mraidInterstitial != null) {
                if (mraidInterstitial.getCloseTime() == 0) {
                    showMraidInterstitial();
                } else {
                    showProgressBarWithCloseTime(mraidInterstitial.getCloseTime());
                    mraidInterstitial.prepare(this, new Runnable() {
                        public void run() {
                            MraidActivity.this.showMraidInterstitial();
                        }
                    });
                }
                mraidInterstitial.setShowingActivity(this);
            }
        } catch (Exception unused) {
            if (!(mraidInterstitial == null || mraidInterstitial.getListener() == null)) {
                mraidInterstitial.getListener().onFullScreenAdFailedToLoad(AdError.InternalError);
            }
            finishActivity();
        }
    }

    public void onBackPressed() {
        if (this.canSkip) {
            finishActivity();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (mraidInterstitial != null) {
            mraidInterstitial.setShowingActivity(null);
            verifyClosedDispatched(mraidInterstitial);
            if (mraidInterstitial.getMraidInterstitial() != null) {
                mraidInterstitial.getMraidInterstitial().destroy();
            }
            mraidInterstitial = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void showMraidInterstitial() {
        hideProgressBarWithCloseTime();
        if (mraidInterstitial != null && mraidInterstitial.getMraidInterstitial() != null) {
            mraidInterstitial.getMraidInterstitial().show(this, true);
        }
    }

    public void showProgressBar() {
        this.progressBar = new ProgressBar(this);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        this.progressBar.setLayoutParams(layoutParams);
        this.progressBar.setBackgroundColor(0);
        addContentView(this.progressBar, layoutParams);
    }

    public void showProgressBarWithCloseTime(long j) {
        showProgressBar();
        int applyDimension = (int) TypedValue.applyDimension(1, 50.0f, getResources().getDisplayMetrics());
        this.circleCountdownView = new CircleCountdownView(this);
        this.circleCountdownView.setBackgroundColor(0);
        this.circleCountdownView.setVisibility(8);
        this.circleCountdownView.setImage(Assets.getBitmapFromBase64(Assets.close));
        this.circleCountdownView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (!(MraidActivity.mraidInterstitial == null || MraidActivity.mraidInterstitial.getListener() == null)) {
                    MraidActivity.mraidInterstitial.getListener().onFullScreenAdFailedToShow(AdError.TimeoutError);
                    if (MraidActivity.mraidInterstitial.getListener() instanceof InternalFullScreenAdListener) {
                        ((InternalFullScreenAdListener) MraidActivity.mraidInterstitial.getListener()).trackCloseTimeError();
                    }
                }
                MraidActivity.this.finishActivity();
            }
        });
        LayoutParams layoutParams = new LayoutParams(applyDimension, applyDimension);
        layoutParams.gravity = 8388661;
        addContentView(this.circleCountdownView, layoutParams);
        this.showCloseTime = new Runnable() {
            public void run() {
                if (MraidActivity.this.circleCountdownView != null) {
                    MraidActivity.this.circleCountdownView.setVisibility(0);
                    MraidActivity.this.circleCountdownView.setClickable(true);
                }
            }
        };
        this.handler = new Handler(Looper.getMainLooper());
        this.handler.postDelayed(this.showCloseTime, j);
    }

    public void hideProgressBar() {
        if (this.progressBar != null) {
            this.progressBar.setVisibility(8);
        }
    }

    public void hideProgressBarWithCloseTime() {
        hideProgressBar();
        if (!(this.handler == null || this.showCloseTime == null)) {
            this.handler.removeCallbacks(this.showCloseTime);
        }
        if (this.circleCountdownView != null) {
            this.circleCountdownView.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void finishActivity() {
        finish();
        overridePendingTransition(0, 0);
        if (mraidInterstitial != null) {
            mraidInterstitial.setShowingActivity(null);
            verifyClosedDispatched(mraidInterstitial);
            mraidInterstitial = null;
        }
    }

    private void hideTitleStatusBars() {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    private void addBackgroundView() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        relativeLayout.setBackgroundColor(Color.parseColor("#7F000000"));
        setContentView(relativeLayout);
    }

    private synchronized void verifyClosedDispatched(@Nullable MraidInterstitial mraidInterstitial2) {
        if (mraidInterstitial2 != null) {
            if (mraidInterstitial2.getMraidInterstitial() != null && !mraidInterstitial2.getMraidInterstitial().isClosed()) {
                mraidInterstitial2.getMraidInterstitial().dispatchClose();
            }
        }
    }
}
