package com.appodealx.mraid;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.FullScreenAdObject;
import com.explorestack.iab.mraid.MRAIDInterstitial;
import com.explorestack.iab.vast.VideoType;

public class MraidInterstitial extends FullScreenAdObject {
    private long closeTime;
    private String creative;
    private int height;
    private FullScreenAdListener listener;
    private MRAIDInterstitial mraidInterstitial;
    private MraidActivity showingActivity;
    private VideoType type;
    private int width;

    MraidInterstitial(String str, int i, int i2, long j, FullScreenAdListener fullScreenAdListener, VideoType videoType) {
        this.creative = str;
        this.width = i;
        this.height = i2;
        this.closeTime = j;
        this.listener = fullScreenAdListener;
        this.type = videoType;
    }

    /* access modifiers changed from: 0000 */
    public void prepare(@NonNull Activity activity) {
        prepare(activity, null);
    }

    /* access modifiers changed from: 0000 */
    public void prepare(@NonNull Activity activity, Runnable runnable) {
        MraidInterstitialListener mraidInterstitialListener = new MraidInterstitialListener(this, this.listener, runnable);
        this.mraidInterstitial = MRAIDInterstitial.newBuilder(activity, this.creative, this.width, this.height).setListener(mraidInterstitialListener).setNativeFeatureListener(mraidInterstitialListener).setPreload(true).build();
        this.mraidInterstitial.load();
    }

    public void show(@NonNull Activity activity) {
        MraidActivity.show(activity, this, this.type);
    }

    public void destroy() {
        if (this.mraidInterstitial != null) {
            this.mraidInterstitial = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public long getCloseTime() {
        return this.closeTime;
    }

    /* access modifiers changed from: 0000 */
    public FullScreenAdListener getListener() {
        return this.listener;
    }

    /* access modifiers changed from: 0000 */
    public MRAIDInterstitial getMraidInterstitial() {
        return this.mraidInterstitial;
    }

    /* access modifiers changed from: 0000 */
    public MraidActivity getShowingActivity() {
        return this.showingActivity;
    }

    /* access modifiers changed from: 0000 */
    public void setShowingActivity(MraidActivity mraidActivity) {
        this.showingActivity = mraidActivity;
    }
}
