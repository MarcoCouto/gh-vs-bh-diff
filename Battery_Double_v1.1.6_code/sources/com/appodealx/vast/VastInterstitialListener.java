package com.appodealx.vast;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAdListener;
import com.explorestack.iab.utils.Utils;
import com.explorestack.iab.vast.VastActivityListener;
import com.explorestack.iab.vast.VastClickCallback;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.VastRequestListener;
import com.explorestack.iab.vast.activity.VastActivity;

public class VastInterstitialListener implements VastRequestListener, VastActivityListener {
    private FullScreenAdListener listener;
    private VastInterstitial vastInterstitial;

    VastInterstitialListener(@NonNull VastInterstitial vastInterstitial2, @NonNull FullScreenAdListener fullScreenAdListener) {
        this.vastInterstitial = vastInterstitial2;
        this.listener = fullScreenAdListener;
    }

    public void onVastLoaded(@NonNull VastRequest vastRequest) {
        this.listener.onFullScreenAdLoaded(this.vastInterstitial);
    }

    public void onVastError(@NonNull Context context, @Nullable VastRequest vastRequest, int i) {
        this.listener.onFullScreenAdFailedToLoad(AdError.NoFill);
    }

    public void onVastShown(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest) {
        this.listener.onFullScreenAdShown();
    }

    public void onVastClick(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest, @NonNull final VastClickCallback vastClickCallback, @Nullable String str) {
        this.listener.onFullScreenAdClicked();
        if (str != null) {
            Utils.openBrowser(vastActivity, str, new Runnable() {
                public void run() {
                    vastClickCallback.clickHandled();
                }
            });
        }
    }

    public void onVastComplete(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest) {
        this.listener.onFullScreenAdCompleted();
    }

    public void onVastDismiss(@NonNull VastActivity vastActivity, @Nullable VastRequest vastRequest, boolean z) {
        this.listener.onFullScreenAdClosed(z);
    }
}
