package com.appodealx.adcolony;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;
import com.appodeal.ads.AppodealNetworks;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.utils.RequestInfoKeys;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdColonyAdapter extends InternalAdapterInterface {
    private static final String KEY_APP_ID = "app_id";
    private static final String KEY_REWARDED_ZONES = "rewarded";
    private static final String KEY_SKIPPABLE_ZONES = "skippable";
    private static final String KEY_STORE_ID = "store";
    private static final String KEY_ZONES = "zones";
    private static final String KEY_ZONE_ID = "zone_id";
    private static String cachedSdkVersion;
    private static Boolean hasConsent;
    private static boolean isAdapterInitialized = false;
    private static Boolean isGDPRScope;
    private static final Map<String, Stack<AdColonyInterstitialWrapper>> loadedInterstitialsCache = new HashMap();
    private static Set<String> zonesCache = new HashSet();

    public void initialize(@NonNull Activity activity, @NonNull JSONObject jSONObject) throws Throwable {
        super.initialize(activity, jSONObject);
        configure(activity, jSONObject);
        for (String orRequestInterstitial : zonesCache) {
            getOrRequestInterstitial(orRequestInterstitial, null);
        }
    }

    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfo(activity, jSONObject);
    }

    public void loadInterstitial(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        AdColonyFullscreenAd adColonyFullscreenAd = new AdColonyFullscreenAd(fullScreenAdListener, false);
        fullScreenAd.setAd(adColonyFullscreenAd);
        adColonyFullscreenAd.load(jSONObject);
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfo(activity, jSONObject);
    }

    public void loadRewardedVideo(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        AdColonyFullscreenAd adColonyFullscreenAd = new AdColonyFullscreenAd(fullScreenAdListener, true);
        fullScreenAd.setAd(adColonyFullscreenAd);
        adColonyFullscreenAd.load(jSONObject);
    }

    private boolean extractZones(@NonNull JSONObject jSONObject) throws JSONException {
        boolean stringCollection;
        JSONObject jSONObject2 = jSONObject.getJSONObject(KEY_ZONES);
        synchronized (AdColonyAdapter.class) {
            stringCollection = toStringCollection(jSONObject2.getJSONArray("rewarded"), zonesCache) | toStringCollection(jSONObject2.getJSONArray(KEY_SKIPPABLE_ZONES), zonesCache);
        }
        return stringCollection;
    }

    private boolean toStringCollection(@Nullable JSONArray jSONArray, @NonNull Collection<String> collection) throws JSONException {
        if (jSONArray == null) {
            return false;
        }
        boolean z = false;
        for (int i = 0; i < jSONArray.length(); i++) {
            Object obj = jSONArray.get(i);
            if (obj != null) {
                z |= collection.add(obj.toString());
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003d, code lost:
        return;
     */
    private void configure(@NonNull Context context, @NonNull JSONObject jSONObject) throws JSONException {
        String string = jSONObject.getString("app_id");
        String string2 = jSONObject.getString("store");
        boolean extractZones = extractZones(jSONObject);
        synchronized (AdColonyAdapter.class) {
            if (!isAdapterInitialized || extractZones) {
                AdColony.configure((Application) context.getApplicationContext(), createAppOptions(context, string2), string, (String[]) zonesCache.toArray(new String[0]));
                if (isAdColonyConfigured()) {
                    isAdapterInitialized = true;
                }
            }
        }
    }

    public void updateConsent(Activity activity, boolean z, boolean z2) {
        super.updateConsent(activity, z, z2);
        hasConsent = Boolean.valueOf(z);
        isGDPRScope = Boolean.valueOf(z2);
        AdColonyAppOptions appOptions = AdColony.getAppOptions();
        if (appOptions != null) {
            configureConsent(appOptions);
            AdColony.setAppOptions(appOptions);
        }
    }

    private static AdColonyAppOptions createAppOptions(@NonNull Context context, @NonNull String str) {
        AdColonyAppOptions appOptions = AdColony.getAppOptions();
        if (appOptions == null) {
            appOptions = new AdColonyAppOptions();
        }
        appOptions.setOriginStore(str);
        try {
            appOptions.setAppVersion(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        configureConsent(appOptions);
        return appOptions;
    }

    private static void configureConsent(@NonNull AdColonyAppOptions adColonyAppOptions) {
        if (isGDPRScope != null && isGDPRScope.booleanValue()) {
            adColonyAppOptions.setGDPRConsentString((hasConsent == null || !hasConsent.booleanValue()) ? "0" : "1");
            adColonyAppOptions.setGDPRRequired(true);
        }
    }

    private boolean isAdColonyConfigured() {
        return !TextUtils.isEmpty(AdColony.getSDKVersion());
    }

    static void getOrRequestInterstitial(@NonNull String str, @Nullable final AdColonyInterstitialListener adColonyInterstitialListener) {
        AdColonyInterstitialWrapper pop = pop(str);
        if (pop != null) {
            pop.setExternalListener(adColonyInterstitialListener);
            if (adColonyInterstitialListener != null) {
                adColonyInterstitialListener.onRequestFilled(pop.getAdColonyInterstitial());
                return;
            }
            return;
        }
        AdColony.requestInterstitial(str, new AdColonyInterstitialListener() {
            public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
                AdColonyAdapter.push(adColonyInterstitial, adColonyInterstitialListener);
                if (adColonyInterstitialListener != null) {
                    adColonyInterstitialListener.onRequestFilled(adColonyInterstitial);
                }
            }

            public void onRequestNotFilled(AdColonyZone adColonyZone) {
                super.onRequestNotFilled(adColonyZone);
                if (adColonyInterstitialListener != null) {
                    adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
                }
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        return null;
     */
    @Nullable
    private static AdColonyInterstitialWrapper pop(@NonNull String str) {
        synchronized (loadedInterstitialsCache) {
            Stack stack = (Stack) loadedInterstitialsCache.get(str);
            if (stack != null) {
                if (!stack.isEmpty()) {
                    AdColonyInterstitialWrapper adColonyInterstitialWrapper = (AdColonyInterstitialWrapper) stack.pop();
                    if (adColonyInterstitialWrapper.isExpired()) {
                        adColonyInterstitialWrapper = null;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    @NonNull
    public static AdColonyInterstitialWrapper push(@NonNull AdColonyInterstitial adColonyInterstitial, @Nullable AdColonyInterstitialListener adColonyInterstitialListener) {
        AdColonyInterstitialWrapper adColonyInterstitialWrapper = new AdColonyInterstitialWrapper(adColonyInterstitial);
        adColonyInterstitialWrapper.setExternalListener(adColonyInterstitialListener);
        push(adColonyInterstitialWrapper);
        return adColonyInterstitialWrapper;
    }

    private static void push(@NonNull AdColonyInterstitialWrapper adColonyInterstitialWrapper) {
        synchronized (loadedInterstitialsCache) {
            String zoneId = adColonyInterstitialWrapper.getZoneId();
            Stack stack = (Stack) loadedInterstitialsCache.get(zoneId);
            if (stack == null) {
                stack = new Stack();
                loadedInterstitialsCache.put(zoneId, stack);
            }
            stack.push(adColonyInterstitialWrapper);
        }
    }

    static boolean remove(@NonNull AdColonyInterstitialWrapper adColonyInterstitialWrapper) {
        synchronized (loadedInterstitialsCache) {
            Stack stack = (Stack) loadedInterstitialsCache.get(adColonyInterstitialWrapper.getZoneId());
            if (stack == null) {
                return false;
            }
            boolean remove = stack.remove(adColonyInterstitialWrapper);
            return remove;
        }
    }

    private static boolean shouldCacheInterstitial(@NonNull String str) {
        boolean z;
        synchronized (loadedInterstitialsCache) {
            Stack stack = (Stack) loadedInterstitialsCache.get(str);
            if (stack != null) {
                if (!stack.isEmpty()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @NonNull
    private JSONArray getRequestInfo(@NonNull Context context, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        try {
            String string = jSONObject.getString(KEY_ZONE_ID);
            if (TextUtils.isEmpty(string)) {
                return jSONArray;
            }
            configure(context, jSONObject);
            if (shouldCacheInterstitial(string)) {
                final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
                final CountDownLatch countDownLatch = new CountDownLatch(1);
                AdColony.requestInterstitial(string, new AdColonyInterstitialListener() {
                    public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
                        AdColonyAdapter.push(adColonyInterstitial, null);
                        countDownLatch.countDown();
                        atomicBoolean.set(true);
                    }

                    public void onRequestNotFilled(AdColonyZone adColonyZone) {
                        super.onRequestNotFilled(adColonyZone);
                        countDownLatch.countDown();
                    }
                });
                countDownLatch.await(5, TimeUnit.SECONDS);
                if (!atomicBoolean.get()) {
                    return jSONArray;
                }
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, AppodealNetworks.ADCOLONY);
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, obtainAdColonyVersion());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", jSONObject.getString("id"));
            jSONObject3.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getString(RequestInfoKeys.APPODEAL_ECPM));
            jSONObject2.put("appodeal", jSONObject3);
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put(KEY_ZONE_ID, jSONObject.getString(KEY_ZONE_ID));
            jSONObject4.put("app_id", jSONObject.getString("app_id"));
            JSONObject optJSONObject = jSONObject.optJSONObject(RequestInfoKeys.EXTRA_PARALLEL_BIDDING_INFO);
            if (optJSONObject != null) {
                Iterator keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    jSONObject4.put(str, optJSONObject.get(str));
                }
            }
            jSONObject2.put(RequestInfoKeys.EXT, jSONObject4);
            jSONArray.put(jSONObject2);
            return jSONArray;
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private static String obtainAdColonyVersion() {
        String sDKVersion = AdColony.getSDKVersion();
        if (TextUtils.isEmpty(sDKVersion)) {
            try {
                if (cachedSdkVersion != null) {
                    return cachedSdkVersion;
                }
                Class cls = Class.forName("com.adcolony.sdk.j");
                Constructor constructor = cls.getDeclaredConstructors()[0];
                constructor.setAccessible(true);
                Object newInstance = constructor.newInstance(new Object[0]);
                Method declaredMethod = cls.getDeclaredMethod("E", new Class[0]);
                declaredMethod.setAccessible(true);
                cachedSdkVersion = (String) declaredMethod.invoke(newInstance, new Object[0]);
                return cachedSdkVersion;
            } catch (Throwable unused) {
            }
        }
        return sDKVersion;
    }
}
