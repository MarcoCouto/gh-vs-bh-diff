package com.appodealx.facebook;

import com.appodealx.sdk.FullScreenAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.InterstitialAdListener;

public class FacebookInterstitialListener implements InterstitialAdListener, AdListener {
    private FacebookInterstitial facebookInterstitial;
    private FullScreenAdListener fullScreenAdListener;

    public void onInterstitialDisplayed(Ad ad) {
    }

    FacebookInterstitialListener(FacebookInterstitial facebookInterstitial2, FullScreenAdListener fullScreenAdListener2) {
        this.facebookInterstitial = facebookInterstitial2;
        this.fullScreenAdListener = fullScreenAdListener2;
    }

    public void onAdLoaded(Ad ad) {
        this.fullScreenAdListener.onFullScreenAdLoaded(this.facebookInterstitial);
    }

    public void onError(Ad ad, AdError adError) {
        if (ad != null) {
            ad.destroy();
        }
        this.fullScreenAdListener.onFullScreenAdFailedToLoad(com.appodealx.sdk.AdError.NoFill);
    }

    public void onAdClicked(Ad ad) {
        this.fullScreenAdListener.onFullScreenAdClicked();
    }

    public void onInterstitialDismissed(Ad ad) {
        if (ad != null) {
            ad.destroy();
        }
        this.fullScreenAdListener.onFullScreenAdClosed(false);
    }

    public void onLoggingImpression(Ad ad) {
        this.fullScreenAdListener.onFullScreenAdShown();
    }
}
