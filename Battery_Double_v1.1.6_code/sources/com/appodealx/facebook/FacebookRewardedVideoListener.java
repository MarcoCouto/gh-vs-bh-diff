package com.appodealx.facebook;

import com.appodealx.sdk.FullScreenAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.RewardedVideoAdListener;

public class FacebookRewardedVideoListener implements RewardedVideoAdListener {
    private FacebookRewardedVideo facebookRewardedVideo;
    private boolean finished = false;
    private FullScreenAdListener fullScreenAdListener;

    FacebookRewardedVideoListener(FacebookRewardedVideo facebookRewardedVideo2, FullScreenAdListener fullScreenAdListener2) {
        this.facebookRewardedVideo = facebookRewardedVideo2;
        this.fullScreenAdListener = fullScreenAdListener2;
    }

    public void onAdLoaded(Ad ad) {
        this.fullScreenAdListener.onFullScreenAdLoaded(this.facebookRewardedVideo);
    }

    public void onError(Ad ad, AdError adError) {
        if (ad != null) {
            ad.destroy();
        }
        this.fullScreenAdListener.onFullScreenAdFailedToLoad(com.appodealx.sdk.AdError.NoFill);
    }

    public void onLoggingImpression(Ad ad) {
        this.fullScreenAdListener.onFullScreenAdShown();
    }

    public void onRewardedVideoCompleted() {
        this.finished = true;
        this.fullScreenAdListener.onFullScreenAdCompleted();
    }

    public void onRewardedVideoClosed() {
        this.fullScreenAdListener.onFullScreenAdClosed(this.finished);
    }

    public void onAdClicked(Ad ad) {
        this.fullScreenAdListener.onFullScreenAdClicked();
    }
}
