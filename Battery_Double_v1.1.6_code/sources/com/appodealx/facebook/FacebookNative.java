package com.appodealx.facebook;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.NativeAdObject;
import com.appodealx.sdk.NativeListener;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdIconView;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAd.AdCreativeType;
import com.facebook.ads.NativeAdBase;
import com.facebook.ads.NativeAdBase.Image;
import com.facebook.ads.NativeAdBase.Rating;
import com.facebook.ads.NativeBannerAd;
import com.facebook.ads.internal.api.NativeAdImageApi;
import com.github.mikephil.charting.utils.Utils;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class FacebookNative {
    private NativeListener nativeListener;

    private static class NativeAdapter extends NativeBaseAdapter<NativeAd> {
        private MediaView facebookMediaView;

        NativeAdapter(NativeAd nativeAd) {
            super(nativeAd);
        }

        public View getMediaView(Context context) {
            if (this.facebookMediaView == null) {
                this.facebookMediaView = new MediaView(context);
            }
            return this.facebookMediaView;
        }

        public boolean hasVideo() {
            return ((NativeAd) this.facebookNativeAd).getAdCreativeType() == AdCreativeType.VIDEO;
        }

        public boolean containsVideo() {
            return hasVideo();
        }

        public void registerViewForInteraction(View view, List<View> list) {
            ((NativeAd) this.facebookNativeAd).registerViewForInteraction(view, this.facebookMediaView, this.facebookAdIconView, list);
        }

        public void destroy() {
            if (this.facebookMediaView != null) {
                this.facebookMediaView.destroy();
                this.facebookMediaView = null;
            }
            super.destroy();
        }
    }

    private static class NativeBannerAdapter extends NativeBaseAdapter<NativeBannerAd> {
        NativeBannerAdapter(NativeBannerAd nativeBannerAd) {
            super(nativeBannerAd);
        }

        public void registerViewForInteraction(View view, List<View> list) {
            if (this.facebookNativeAd != null) {
                ((NativeBannerAd) this.facebookNativeAd).registerViewForInteraction(view, this.facebookAdIconView, list);
            }
        }
    }

    private static class NativeBaseAdapter<T extends NativeAdBase> extends NativeAdObject {
        MediaView facebookAdIconView;
        final T facebookNativeAd;

        NativeBaseAdapter(T t) {
            this.facebookNativeAd = t;
        }

        public double getRating() {
            Rating adStarRating = this.facebookNativeAd.getAdStarRating();
            if (adStarRating == null || adStarRating.getValue() == Utils.DOUBLE_EPSILON) {
                return super.getRating();
            }
            return (double) ((float) adStarRating.getValue());
        }

        public View getProviderView(Context context) {
            if (this.facebookNativeAd != null) {
                return new AdChoicesView(context, (NativeAdBase) this.facebookNativeAd, true);
            }
            return super.getProviderView(context);
        }

        public View getIconView(Context context) {
            if (this.facebookAdIconView == null) {
                this.facebookAdIconView = new AdIconView(context);
            }
            return this.facebookAdIconView;
        }

        public void unregisterViewForInteraction() {
            if (this.facebookNativeAd != null) {
                this.facebookNativeAd.unregisterView();
            }
        }

        public int getHash() {
            return this.facebookNativeAd != null ? this.facebookNativeAd.hashCode() : super.getHash();
        }

        public void destroy() {
            if (this.facebookAdIconView != null) {
                this.facebookAdIconView.destroy();
                this.facebookAdIconView = null;
            }
            if (this.facebookNativeAd != null) {
                this.facebookNativeAd.destroy();
            }
        }
    }

    FacebookNative(NativeListener nativeListener2) {
        this.nativeListener = nativeListener2;
    }

    public void load(Context context, JSONObject jSONObject, String str, @NonNull Map<String, String> map, com.appodealx.sdk.NativeAd nativeAd) throws Exception {
        NativeAdBase nativeAdBase;
        if (VERSION.SDK_INT < 15) {
            this.nativeListener.onNativeFailedToLoad(AdError.InternalError);
            return;
        }
        String string = jSONObject.getString("facebook_key");
        String str2 = (String) map.get("media_asset_type");
        if (str2 == null || !str2.equals("ICON")) {
            nativeAdBase = createNativeAd(context, string);
        } else {
            nativeAdBase = createNativeBannerAd(context, string);
        }
        nativeAdBase.setAdListener(new FacebookNativeListener(this, nativeAd, this.nativeListener));
        nativeAdBase.loadAdFromBid(str);
    }

    private NativeBannerAd createNativeBannerAd(Context context, String str) {
        return new NativeBannerAd(context, str);
    }

    private NativeAd createNativeAd(Context context, String str) {
        return new NativeAd(context, str);
    }

    /* access modifiers changed from: 0000 */
    public NativeAdObject createNativeAdObject(NativeAdBase nativeAdBase) {
        NativeAdObject nativeAdObject;
        if (nativeAdBase instanceof NativeBannerAd) {
            nativeAdObject = new NativeBannerAdapter((NativeBannerAd) nativeAdBase);
        } else {
            nativeAdObject = new NativeAdapter((NativeAd) nativeAdBase);
        }
        nativeAdObject.setIcon(getImageUrl(nativeAdBase.getAdIcon()));
        nativeAdObject.setImage(getImageUrl(nativeAdBase.getAdCoverImage()));
        nativeAdObject.setTitle(nativeAdBase.getAdHeadline());
        nativeAdObject.setDescription(nativeAdBase.getAdBodyText());
        nativeAdObject.setCta(nativeAdBase.getAdCallToAction());
        return nativeAdObject;
    }

    private String getImageUrl(@Nullable Image image) {
        if (image == null) {
            return null;
        }
        try {
            Field declaredField = image.getClass().getDeclaredField("mNativeAdImageApi");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(image);
            if (obj instanceof NativeAdImageApi) {
                return ((NativeAdImageApi) obj).getUrl();
            }
        } catch (Exception e) {
            Log.e("Appodealx-Facebook", "", e);
        }
        return null;
    }
}
