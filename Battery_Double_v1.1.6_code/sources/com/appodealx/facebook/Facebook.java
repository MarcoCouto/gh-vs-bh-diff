package com.appodealx.facebook;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.appodeal.ads.AppodealNetworks;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.BannerListener;
import com.appodealx.sdk.BannerView;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.NativeAd;
import com.appodealx.sdk.NativeListener;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.BidderTokenProvider;
import com.facebook.ads.BuildConfig;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Facebook extends InternalAdapterInterface {
    private FacebookTokenFetcher facebookToken = new FacebookTokenFetcher();

    private static class FacebookTokenFetcher {
        /* access modifiers changed from: private */
        public boolean inProcess;
        /* access modifiers changed from: private */
        public String token;

        private FacebookTokenFetcher() {
        }

        /* access modifiers changed from: private */
        public String getToken() {
            return this.token;
        }

        /* access modifiers changed from: private */
        public void prepare(final Context context) {
            if (TextUtils.isEmpty(this.token) && !this.inProcess) {
                this.inProcess = true;
                try {
                    Executors.newSingleThreadExecutor().execute(new Runnable() {
                        public void run() {
                            FacebookTokenFetcher.this.token = BidderTokenProvider.getBidderToken(context);
                            FacebookTokenFetcher.this.inProcess = false;
                        }
                    });
                } catch (Exception e) {
                    Log.e("Appodealx-Facebook", "", e);
                }
            }
        }
    }

    public void initialize(@NonNull Activity activity, @NonNull JSONObject jSONObject) throws Throwable {
        AudienceNetworkAds.initialize(activity);
        this.facebookToken.prepare(activity.getApplicationContext());
    }

    @NonNull
    public JSONArray getBannerRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfoFromSettings(jSONObject);
    }

    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfoFromSettings(jSONObject);
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfoFromSettings(jSONObject);
    }

    @NonNull
    public JSONArray getNativeRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        return getRequestInfoFromSettings(jSONObject);
    }

    public void loadBanner(@NonNull Activity activity, @NonNull BannerView bannerView, JSONObject jSONObject, BannerListener bannerListener) {
        try {
            String string = jSONObject.getString("facebook_key");
            String string2 = jSONObject.getString("bid_payload");
            int optInt = jSONObject.optInt("height", 50);
            if (!TextUtils.isEmpty(string2)) {
                new FacebookBanner(bannerView, bannerListener, optInt).load(activity, string, string2);
            } else {
                bannerListener.onBannerFailedToLoad(AdError.InternalError);
            }
        } catch (Exception unused) {
            bannerListener.onBannerFailedToLoad(AdError.InternalError);
        }
    }

    public void loadInterstitial(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        try {
            String string = jSONObject.getString("facebook_key");
            String string2 = jSONObject.getString("bid_payload");
            if (!TextUtils.isEmpty(string2)) {
                FacebookInterstitial facebookInterstitial = new FacebookInterstitial(fullScreenAdListener);
                facebookInterstitial.load(activity, string, string2);
                fullScreenAd.setAd(facebookInterstitial);
                return;
            }
            fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
        } catch (Exception unused) {
            fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
        }
    }

    public void loadRewardedVideo(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        try {
            String string = jSONObject.getString("facebook_key");
            String string2 = jSONObject.getString("bid_payload");
            if (!TextUtils.isEmpty(string2)) {
                FacebookRewardedVideo facebookRewardedVideo = new FacebookRewardedVideo(fullScreenAdListener);
                facebookRewardedVideo.load(activity, string, string2);
                fullScreenAd.setAd(facebookRewardedVideo);
                return;
            }
            fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
        } catch (Exception unused) {
            fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.InternalError);
        }
    }

    public void loadNative(@NonNull Activity activity, JSONObject jSONObject, @NonNull Map<String, String> map, NativeAd nativeAd, NativeListener nativeListener) {
        try {
            String string = jSONObject.getString("bid_payload");
            if (!TextUtils.isEmpty(string)) {
                new FacebookNative(nativeListener).load(activity, jSONObject, string, map, nativeAd);
            } else {
                nativeListener.onNativeFailedToLoad(AdError.InternalError);
            }
        } catch (Exception unused) {
            nativeListener.onNativeFailedToLoad(AdError.InternalError);
        }
    }

    @NonNull
    private JSONArray getRequestInfoFromSettings(JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        try {
            if (!TextUtils.isEmpty(this.facebookToken.getToken())) {
                JSONObject jSONObject2 = new JSONObject();
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("id", jSONObject.getString("id"));
                jSONObject3.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM));
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put("facebook_key", jSONObject.getString("facebook_key"));
                jSONObject4.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, this.facebookToken.getToken());
                if (jSONObject.has("app_id")) {
                    jSONObject4.put("app_id", jSONObject.getString("app_id"));
                }
                JSONObject optJSONObject = jSONObject.optJSONObject(RequestInfoKeys.EXTRA_PARALLEL_BIDDING_INFO);
                if (optJSONObject != null) {
                    Iterator keys = optJSONObject.keys();
                    while (keys.hasNext()) {
                        String str = (String) keys.next();
                        jSONObject4.put(str, optJSONObject.get(str));
                    }
                }
                jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, AppodealNetworks.FACEBOOK);
                jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, BuildConfig.VERSION_NAME);
                jSONObject2.put("appodeal", jSONObject3);
                jSONObject2.put(RequestInfoKeys.EXT, jSONObject4);
                jSONArray.put(jSONObject2);
            }
        } catch (JSONException e) {
            Log.e("Appodealx-Facebook", "", e);
        }
        return jSONArray;
    }
}
