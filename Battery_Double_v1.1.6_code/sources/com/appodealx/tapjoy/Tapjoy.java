package com.appodealx.tapjoy;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.appodeal.ads.AppodealNetworks;
import com.appodealx.sdk.AdError;
import com.appodealx.sdk.FullScreenAd;
import com.appodealx.sdk.FullScreenAdListener;
import com.appodealx.sdk.InternalAdapterInterface;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TJConnectListener;
import com.tapjoy.TapjoyLog;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Tapjoy extends InternalAdapterInterface {
    /* access modifiers changed from: private */
    public String token;

    public void initialize(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        try {
            String string = jSONObject.getString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY);
            if (!com.tapjoy.Tapjoy.isLimitedConnected()) {
                com.tapjoy.Tapjoy.limitedConnect(activity.getApplicationContext(), string, new TJConnectListener() {
                    public void onConnectFailure() {
                    }

                    public void onConnectSuccess() {
                        Tapjoy.this.token = com.tapjoy.Tapjoy.getUserToken();
                    }
                });
            } else {
                this.token = com.tapjoy.Tapjoy.getUserToken();
            }
        } catch (JSONException e) {
            Log.e("Appodealx-Tapjoy", e.getMessage());
        }
    }

    public void setLogging(boolean z) {
        TapjoyLog.setDebugEnabled(z);
    }

    public void updateConsent(Activity activity, boolean z, boolean z2) {
        if (z2) {
            com.tapjoy.Tapjoy.setUserConsent(z ? "1" : "0");
        }
        com.tapjoy.Tapjoy.subjectToGDPR(z2);
    }

    public void updateCoppa(boolean z) {
        com.tapjoy.Tapjoy.belowConsentAge(z);
    }

    public void loadInterstitial(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        loadFullscreenAd(activity, jSONObject, fullScreenAd, fullScreenAdListener);
    }

    @NonNull
    public JSONArray getInterstitialRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    public void loadRewardedVideo(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        loadFullscreenAd(activity, jSONObject, fullScreenAd, fullScreenAdListener);
    }

    @NonNull
    public JSONArray getRewardedVideoRequestInfo(@NonNull Activity activity, @NonNull JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        JSONObject requestInfoFromSettings = getRequestInfoFromSettings(jSONObject);
        if (requestInfoFromSettings != null) {
            jSONArray.put(requestInfoFromSettings);
        }
        return jSONArray;
    }

    @Nullable
    private JSONObject getRequestInfoFromSettings(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER, AppodealNetworks.TAPJOY);
            jSONObject2.put(RequestInfoKeys.DISPLAY_MANAGER_VERSION, com.tapjoy.Tapjoy.getVersion());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, jSONObject.getString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY));
            jSONObject3.put("placement_name", jSONObject.getString(IronSourceConstants.EVENTS_PLACEMENT_NAME));
            if (!TextUtils.isEmpty(this.token)) {
                jSONObject3.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, this.token);
            }
            JSONObject optJSONObject = jSONObject.optJSONObject(RequestInfoKeys.EXTRA_PARALLEL_BIDDING_INFO);
            if (optJSONObject != null) {
                Iterator keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    jSONObject3.put(str, optJSONObject.get(str));
                }
            }
            jSONObject2.put(RequestInfoKeys.EXT, jSONObject3);
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", jSONObject.getString("id"));
            jSONObject4.put(RequestInfoKeys.APPODEAL_ECPM, jSONObject.getDouble(RequestInfoKeys.APPODEAL_ECPM));
            jSONObject2.put("appodeal", jSONObject4);
            return jSONObject2;
        } catch (JSONException e) {
            Log.e("Appodealx-Tapjoy", e.getMessage());
            return null;
        }
    }

    private void loadFullscreenAd(@NonNull Activity activity, JSONObject jSONObject, FullScreenAd fullScreenAd, FullScreenAdListener fullScreenAdListener) {
        if (com.tapjoy.Tapjoy.isLimitedConnected()) {
            TapjoyFullScreenAd tapjoyFullScreenAd = new TapjoyFullScreenAd(jSONObject, fullScreenAdListener);
            tapjoyFullScreenAd.load(activity);
            fullScreenAd.setAd(tapjoyFullScreenAd);
            return;
        }
        fullScreenAdListener.onFullScreenAdFailedToLoad(AdError.SDKNotInitialized);
    }
}
