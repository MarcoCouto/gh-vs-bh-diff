package com.applovin.communicator;

import android.content.Context;
import com.applovin.impl.communicator.MessagingServiceImpl;
import com.applovin.impl.communicator.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import java.util.Collections;
import java.util.List;

public final class AppLovinCommunicator {
    private static AppLovinCommunicator a;
    private static final Object b = new Object();
    private j c;
    private final a d;
    private final MessagingServiceImpl e;

    private AppLovinCommunicator(Context context) {
        this.d = new a(context);
        this.e = new MessagingServiceImpl(context);
    }

    public static AppLovinCommunicator getInstance(Context context) {
        synchronized (b) {
            if (a == null) {
                a = new AppLovinCommunicator(context.getApplicationContext());
            }
        }
        return a;
    }

    public void a(j jVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Attaching SDK instance: ");
        sb.append(jVar);
        sb.append("...");
        p.g("AppLovinCommunicator", sb.toString());
        this.c = jVar;
    }

    public AppLovinCommunicatorMessagingService getMessagingService() {
        return this.e;
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        subscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String str : list) {
            StringBuilder sb = new StringBuilder();
            sb.append("Subscribing ");
            sb.append(appLovinCommunicatorSubscriber);
            sb.append(" to topic: ");
            sb.append(str);
            p.g("AppLovinCommunicator", sb.toString());
            if (this.d.a(appLovinCommunicatorSubscriber, str)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Subscribed ");
                sb2.append(appLovinCommunicatorSubscriber);
                sb2.append(" to topic: ");
                sb2.append(str);
                p.g("AppLovinCommunicator", sb2.toString());
                this.e.maybeFlushStickyMessages(str);
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to subscribe ");
                sb3.append(appLovinCommunicatorSubscriber);
                sb3.append(" to topic: ");
                sb3.append(str);
                p.g("AppLovinCommunicator", sb3.toString());
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinCommunicator{sdk=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        unsubscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String str : list) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unsubscribing ");
            sb.append(appLovinCommunicatorSubscriber);
            sb.append(" from topic: ");
            sb.append(str);
            p.g("AppLovinCommunicator", sb.toString());
            this.d.b(appLovinCommunicatorSubscriber, str);
        }
    }
}
