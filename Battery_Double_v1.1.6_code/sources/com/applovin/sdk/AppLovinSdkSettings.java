package com.applovin.sdk;

import android.content.Context;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.q;
import java.util.HashMap;
import java.util.Map;

public class AppLovinSdkSettings {
    private boolean a;
    private long b;
    private String c;
    private String d;
    private boolean e;
    private final Map<String, Object> localSettings;

    public AppLovinSdkSettings() {
        this(null);
    }

    public AppLovinSdkSettings(Context context) {
        this.localSettings = new HashMap();
        this.a = q.a(context);
        this.b = -1;
        StringBuilder sb = new StringBuilder();
        sb.append(AppLovinAdSize.INTERSTITIAL.getLabel());
        sb.append(",");
        sb.append(AppLovinAdSize.BANNER.getLabel());
        sb.append(",");
        sb.append(AppLovinAdSize.MREC.getLabel());
        this.c = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(AppLovinAdType.INCENTIVIZED.getLabel());
        sb2.append(",");
        sb2.append(AppLovinAdType.REGULAR.getLabel());
        sb2.append(",");
        sb2.append(AppLovinAdType.NATIVE.getLabel());
        this.d = sb2.toString();
    }

    @Deprecated
    public String getAutoPreloadSizes() {
        return this.c;
    }

    @Deprecated
    public String getAutoPreloadTypes() {
        return this.d;
    }

    @Deprecated
    public long getBannerAdRefreshSeconds() {
        return this.b;
    }

    public boolean isMuted() {
        return this.e;
    }

    public boolean isVerboseLoggingEnabled() {
        return this.a;
    }

    @Deprecated
    public void setAutoPreloadSizes(String str) {
        this.c = str;
    }

    @Deprecated
    public void setAutoPreloadTypes(String str) {
        this.d = str;
    }

    @Deprecated
    public void setBannerAdRefreshSeconds(long j) {
        this.b = j;
    }

    public void setMuted(boolean z) {
        this.e = z;
    }

    public void setVerboseLogging(boolean z) {
        if (q.a()) {
            p.j("AppLovinSdkSettings", "Ignoring setting of verbose logging - it is configured from Android manifest already or AppLovinSdkSettings was initialized without a context.");
        } else {
            this.a = z;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinSdkSettings{isVerboseLoggingEnabled=");
        sb.append(this.a);
        sb.append(", muted=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
