package com.applovin.impl.sdk;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import com.applovin.impl.mediation.b.b;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.utils.q;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.WeakReference;

public class w {
    private final j a;
    private final Object b = new Object();
    private final Rect c = new Rect();
    private final Handler d;
    private final Runnable e;
    private final OnPreDrawListener f;
    private WeakReference<ViewTreeObserver> g = new WeakReference<>(null);
    /* access modifiers changed from: private */
    public WeakReference<View> h = new WeakReference<>(null);
    private int i;
    private long j;
    private long k = Long.MIN_VALUE;

    public interface a {
        void onLogVisibilityImpression();
    }

    public w(MaxAdView maxAdView, j jVar, a aVar) {
        this.a = jVar;
        this.d = new Handler(Looper.getMainLooper());
        final WeakReference weakReference = new WeakReference(maxAdView);
        final WeakReference weakReference2 = new WeakReference(aVar);
        this.e = new Runnable() {
            public void run() {
                MaxAdView maxAdView = (MaxAdView) weakReference.get();
                View view = (View) w.this.h.get();
                if (maxAdView != null && view != null) {
                    if (w.this.b(maxAdView, view)) {
                        w.this.a();
                        a aVar = (a) weakReference2.get();
                        if (aVar != null) {
                            aVar.onLogVisibilityImpression();
                            return;
                        }
                        return;
                    }
                    w.this.b();
                }
            }
        };
        this.f = new OnPreDrawListener() {
            public boolean onPreDraw() {
                w.this.b();
                w.this.c();
                return true;
            }
        };
    }

    private void a(Context context, View view) {
        View a2 = q.a(context, view);
        if (a2 == null) {
            this.a.v().b("VisibilityTracker", "Unable to set view tree observer due to no root view.");
            return;
        }
        ViewTreeObserver viewTreeObserver = a2.getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            this.a.v().d("VisibilityTracker", "Unable to set view tree observer since the view tree observer is not alive.");
            return;
        }
        this.g = new WeakReference<>(viewTreeObserver);
        viewTreeObserver.addOnPreDrawListener(this.f);
    }

    private boolean a(View view, View view2) {
        boolean z = false;
        if (view2 != null && view2.getVisibility() == 0 && view.getParent() != null && view2.getWidth() > 0 && view2.getHeight() > 0) {
            if (!view2.getGlobalVisibleRect(this.c)) {
                return false;
            }
            if (((long) (AppLovinSdkUtils.pxToDp(view2.getContext(), this.c.width()) * AppLovinSdkUtils.pxToDp(view2.getContext(), this.c.height()))) >= ((long) this.i)) {
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public void b() {
        this.d.postDelayed(this.e, ((Long) this.a.a(d.cm)).longValue());
    }

    /* access modifiers changed from: private */
    public boolean b(View view, View view2) {
        if (!a(view, view2)) {
            return false;
        }
        if (this.k == Long.MIN_VALUE) {
            this.k = SystemClock.uptimeMillis();
        }
        return SystemClock.uptimeMillis() - this.k >= this.j;
    }

    /* access modifiers changed from: private */
    public void c() {
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.g.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnPreDrawListener(this.f);
        }
        this.g.clear();
    }

    public void a() {
        synchronized (this.b) {
            this.d.removeMessages(0);
            c();
            this.k = Long.MIN_VALUE;
            this.h.clear();
        }
    }

    public void a(Context context, b bVar) {
        synchronized (this.b) {
            a();
            this.h = new WeakReference<>(bVar.l());
            this.i = bVar.q();
            this.j = bVar.s();
            a(context, (View) this.h.get());
        }
    }
}
