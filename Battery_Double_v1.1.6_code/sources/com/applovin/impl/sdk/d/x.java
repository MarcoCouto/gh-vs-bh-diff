package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.r.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a.C0012a;
import com.applovin.impl.sdk.network.a.c;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinErrorCodes;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.concurrent.TimeUnit;

public abstract class x<T> extends a implements c<T> {
    /* access modifiers changed from: private */
    public final b<T> a;
    private final c<T> c;
    protected C0012a d;
    /* access modifiers changed from: private */
    public a e;
    /* access modifiers changed from: private */
    public d<String> f;
    /* access modifiers changed from: private */
    public d<String> g;

    public x(b<T> bVar, j jVar) {
        this(bVar, jVar, false);
    }

    public x(b<T> bVar, final j jVar, boolean z) {
        super("TaskRepeatRequest", jVar, z);
        this.e = a.BACKGROUND;
        this.f = null;
        this.g = null;
        if (bVar != null) {
            this.a = bVar;
            this.d = new C0012a();
            this.c = new c<T>() {
                public void a(int i) {
                    d dVar;
                    x xVar;
                    boolean z = false;
                    boolean z2 = i < 200 || i >= 500;
                    boolean z3 = i == 429;
                    if (i != -103) {
                        z = true;
                    }
                    if (z && (z2 || z3)) {
                        String f = x.this.a.f();
                        if (x.this.a.j() > 0) {
                            x xVar2 = x.this;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Unable to send request due to server failure (code ");
                            sb.append(i);
                            sb.append("). ");
                            sb.append(x.this.a.j());
                            sb.append(" attempts left, retrying in ");
                            sb.append(TimeUnit.MILLISECONDS.toSeconds((long) x.this.a.l()));
                            sb.append(" seconds...");
                            xVar2.c(sb.toString());
                            int j = x.this.a.j() - 1;
                            x.this.a.a(j);
                            if (j == 0) {
                                x.this.c(x.this.f);
                                if (n.b(f) && f.length() >= 4) {
                                    x.this.a.a(f);
                                    x xVar3 = x.this;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Switching to backup endpoint ");
                                    sb2.append(f);
                                    xVar3.b(sb2.toString());
                                }
                            }
                            jVar.K().a(x.this, x.this.e, (long) x.this.a.l());
                            return;
                        }
                        if (f == null || !f.equals(x.this.a.a())) {
                            xVar = x.this;
                            dVar = x.this.f;
                        } else {
                            xVar = x.this;
                            dVar = x.this.g;
                        }
                        xVar.c(dVar);
                    }
                    x.this.a(i);
                }

                public void a(T t, int i) {
                    x.this.a.a(0);
                    x.this.a(t, i);
                }
            };
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    /* access modifiers changed from: private */
    public <ST> void c(d<ST> dVar) {
        if (dVar != null) {
            e C = e().C();
            C.a(dVar, dVar.b());
            C.b();
        }
    }

    public i a() {
        return i.e;
    }

    public abstract void a(int i);

    public void a(d<String> dVar) {
        this.f = dVar;
    }

    public void a(a aVar) {
        this.e = aVar;
    }

    public abstract void a(T t, int i);

    public void b(d<String> dVar) {
        this.g = dVar;
    }

    public void run() {
        int i;
        com.applovin.impl.sdk.network.a J = e().J();
        if (!e().c() && !e().d()) {
            d("AppLovin SDK is disabled: please check your connection");
            p.j("AppLovinSdk", "AppLovin SDK is disabled: please check your connection");
            i = -22;
        } else if (!n.b(this.a.a()) || this.a.a().length() < 4) {
            d("Task has an invalid or null request endpoint.");
            i = AppLovinErrorCodes.INVALID_URL;
        } else {
            if (TextUtils.isEmpty(this.a.b())) {
                this.a.b(this.a.e() != null ? HttpRequest.METHOD_POST : HttpRequest.METHOD_GET);
            }
            J.a(this.a, this.d, this.c);
            return;
        }
        a(i);
    }
}
