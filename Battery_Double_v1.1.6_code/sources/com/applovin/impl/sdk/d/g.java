package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.n;
import com.applovin.impl.sdk.utils.h;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.List;

public class g extends f {
    public g(List<NativeAdImpl> list, j jVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super("TaskCacheNativeAdVideos", list, jVar, appLovinNativeAdLoadListener);
    }

    public g(List<NativeAdImpl> list, j jVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        super("TaskCacheNativeAdVideos", list, jVar, appLovinNativeAdPrecacheListener);
    }

    private boolean b(NativeAdImpl nativeAdImpl) {
        StringBuilder sb = new StringBuilder();
        sb.append("Unable to cache video resource ");
        sb.append(nativeAdImpl.getSourceVideoUrl());
        c(sb.toString());
        a(nativeAdImpl, !h.a(g()) ? AppLovinErrorCodes.NO_NETWORK : AppLovinErrorCodes.UNABLE_TO_PRECACHE_VIDEO_RESOURCES);
        return false;
    }

    public i a() {
        return i.k;
    }

    /* access modifiers changed from: protected */
    public void a(NativeAdImpl nativeAdImpl) {
        if (this.a != null) {
            this.a.onNativeAdVideoPreceached(nativeAdImpl);
        }
    }

    /* access modifiers changed from: protected */
    public void a(NativeAdImpl nativeAdImpl, int i) {
        if (this.a != null) {
            this.a.onNativeAdVideoPrecachingFailed(nativeAdImpl, i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(NativeAdImpl nativeAdImpl, n nVar) {
        if (!com.applovin.impl.sdk.utils.n.b(nativeAdImpl.getSourceVideoUrl())) {
            return true;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Beginning native ad video caching");
        sb.append(nativeAdImpl.getAdId());
        a(sb.toString());
        if (((Boolean) this.b.a(d.bK)).booleanValue()) {
            String a = a(nativeAdImpl.getSourceVideoUrl(), nVar, nativeAdImpl.getResourcePrefixes());
            if (a == null) {
                return b(nativeAdImpl);
            }
            nativeAdImpl.setVideoUrl(a);
        } else {
            a("Resource caching is disabled, skipping...");
        }
        return true;
    }

    public /* bridge */ /* synthetic */ void run() {
        super.run();
    }
}
