package com.applovin.impl.sdk.d;

import android.os.Build.VERSION;
import com.applovin.impl.mediation.d.b;
import com.applovin.impl.mediation.d.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.g;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class k extends a {
    private static int a;
    /* access modifiers changed from: private */
    public final AtomicBoolean c = new AtomicBoolean();

    private class a extends a {
        public a(j jVar) {
            super("TaskTimeoutFetchBasicSettings", jVar, true);
        }

        public i a() {
            return i.g;
        }

        public void run() {
            if (!k.this.c.get()) {
                d("Timing out fetch basic settings...");
                k.this.a(new JSONObject());
            }
        }
    }

    public k(j jVar) {
        super("TaskFetchBasicSettings", jVar, true);
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        boolean z = true;
        if (this.c.compareAndSet(false, true)) {
            h.d(jSONObject, this.b);
            h.c(jSONObject, this.b);
            if (jSONObject.length() <= 0) {
                z = false;
            }
            h.a(jSONObject, z, this.b);
            b.a(jSONObject, this.b);
            b.b(jSONObject, this.b);
            b("Executing initialize SDK...");
            this.b.z().a(com.applovin.impl.sdk.utils.i.a(jSONObject, "smd", Boolean.valueOf(false), this.b).booleanValue());
            h.f(jSONObject, this.b);
            this.b.K().a((a) new q(this.b));
            h.e(jSONObject, this.b);
            b("Finished executing initialize SDK");
        }
    }

    private String d() {
        return h.a((String) this.b.a(d.aJ), "5.0/i", e());
    }

    private String i() {
        return h.a((String) this.b.a(d.aK), "5.0/i", e());
    }

    public i a() {
        return i.d;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap();
        hashMap.put("rid", UUID.randomUUID().toString());
        if (!((Boolean) this.b.a(d.eR)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.t());
        }
        Boolean a2 = g.a(g());
        if (a2 != null) {
            hashMap.put("huc", a2.toString());
        }
        Boolean b = g.b(g());
        if (b != null) {
            hashMap.put("aru", b.toString());
        }
        Boolean c2 = g.c(g());
        if (c2 != null) {
            hashMap.put("dns", c2.toString());
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("sdk_version", AppLovinSdk.VERSION);
            jSONObject.put("build", String.valueOf(131));
            int i = a + 1;
            a = i;
            jSONObject.put("init_count", String.valueOf(i));
            jSONObject.put("server_installed_at", n.e((String) this.b.a(d.ac)));
            if (this.b.H()) {
                jSONObject.put("first_install", true);
            }
            if (!this.b.I()) {
                jSONObject.put("first_install_v2", true);
            }
            String str = (String) this.b.a(d.ea);
            if (n.b(str)) {
                jSONObject.put("plugin_version", n.e(str));
            }
            String n = this.b.n();
            if (n.b(n)) {
                jSONObject.put("mediation_provider", n.e(n));
            }
            com.applovin.impl.mediation.d.c.a a2 = c.a(this.b);
            jSONObject.put("installed_mediation_adapters", a2.a());
            jSONObject.put("uninstalled_mediation_adapter_classnames", a2.b());
            com.applovin.impl.sdk.k.b c2 = this.b.O().c();
            jSONObject.put("package_name", n.e(c2.c));
            jSONObject.put(TapjoyConstants.TJC_APP_VERSION_NAME, n.e(c2.b));
            jSONObject.put("debug", n.e(c2.g));
            jSONObject.put(TapjoyConstants.TJC_PLATFORM, "android");
            jSONObject.put("os", n.e(VERSION.RELEASE));
            jSONObject.put("tg", q.a(f.g, this.b));
            jSONObject.put("ltg", q.a(f.h, this.b));
            if (((Boolean) this.b.a(d.dV)).booleanValue()) {
                jSONObject.put("compass_random_token", this.b.j());
            }
            if (((Boolean) this.b.a(d.dX)).booleanValue()) {
                jSONObject.put("applovin_random_token", this.b.k());
            }
        } catch (JSONException e) {
            a("Failed to construct JSON body", e);
        }
        return jSONObject;
    }

    public void run() {
        Map b = b();
        com.applovin.impl.sdk.network.b a2 = com.applovin.impl.sdk.network.b.a(this.b).a(d()).c(i()).a(b).a(c()).b(HttpRequest.METHOD_POST).a(new JSONObject()).a(((Integer) this.b.a(d.dF)).intValue()).c(((Integer) this.b.a(d.dI)).intValue()).b(((Integer) this.b.a(d.dE)).intValue()).b(true).a();
        this.b.K().a(new a(this.b), com.applovin.impl.sdk.d.r.a.TIMEOUT, ((long) ((Integer) this.b.a(d.dE)).intValue()) + 250);
        AnonymousClass1 r1 = new x<JSONObject>(a2, this.b, h()) {
            public void a(int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to fetch basic SDK settings: server returned ");
                sb.append(i);
                d(sb.toString());
                k.this.a(new JSONObject());
            }

            public void a(JSONObject jSONObject, int i) {
                k.this.a(jSONObject);
            }
        };
        r1.a(d.aL);
        r1.b(d.aM);
        this.b.K().a((a) r1);
    }
}
