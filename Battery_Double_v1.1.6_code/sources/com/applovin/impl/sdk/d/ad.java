package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.Collections;
import java.util.Map;
import org.json.JSONObject;

public class ad extends ae {
    private final f a;
    private final AppLovinAdRewardListener c;

    public ad(f fVar, AppLovinAdRewardListener appLovinAdRewardListener, j jVar) {
        super("TaskValidateAppLovinReward", jVar);
        this.a = fVar;
        this.c = appLovinAdRewardListener;
    }

    public i a() {
        return i.z;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        super.a(i);
        String str = "network_timeout";
        if (i < 400 || i >= 500) {
            this.c.validationRequestFailed(this.a, i);
        } else {
            this.c.userRewardRejected(this.a, Collections.emptyMap());
            str = "rejected";
        }
        this.a.a(c.a(str));
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        this.a.a(cVar);
        String b = cVar.b();
        Map a2 = cVar.a();
        if (b.equals("accepted")) {
            this.c.userRewardVerified(this.a, a2);
        } else if (b.equals("quota_exceeded")) {
            this.c.userOverQuota(this.a, a2);
        } else if (b.equals("rejected")) {
            this.c.userRewardRejected(this.a, a2);
        } else {
            this.c.validationRequestFailed(this.a, AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR);
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.a.getAdZone().a(), this.b);
        String clCode = this.a.getClCode();
        String str = "clcode";
        if (!n.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str, clCode, this.b);
    }

    public String b() {
        return "2.0/vr";
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.a.aH();
    }
}
