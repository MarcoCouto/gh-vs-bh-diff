package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a.c;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import io.fabric.sdk.android.services.network.HttpRequest;
import org.json.JSONObject;

public abstract class ab extends a {
    protected ab(String str, j jVar) {
        super(str, jVar);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        h.a(i, this.b);
    }

    /* access modifiers changed from: protected */
    public abstract void a(JSONObject jSONObject);

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject, final c<JSONObject> cVar) {
        AnonymousClass1 r0 = new x<JSONObject>(b.a(this.b).a(h.a(b(), this.b)).c(h.b(b(), this.b)).a(h.e(this.b)).b(HttpRequest.METHOD_POST).a(jSONObject).a(new JSONObject()).a(((Integer) this.b.a(d.bP)).intValue()).a(), this.b) {
            public void a(int i) {
                cVar.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                cVar.a(jSONObject, i);
            }
        };
        r0.a(d.aN);
        r0.b(d.aO);
        this.b.K().a((a) r0);
    }

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        String i = this.b.i();
        if (((Boolean) this.b.a(d.dU)).booleanValue() && n.b(i)) {
            i.a(jSONObject, "cuid", i, this.b);
        }
        if (((Boolean) this.b.a(d.dW)).booleanValue()) {
            i.a(jSONObject, "compass_random_token", this.b.j(), this.b);
        }
        if (((Boolean) this.b.a(d.dY)).booleanValue()) {
            i.a(jSONObject, "applovin_random_token", this.b.k(), this.b);
        }
        a(jSONObject);
        return jSONObject;
    }
}
