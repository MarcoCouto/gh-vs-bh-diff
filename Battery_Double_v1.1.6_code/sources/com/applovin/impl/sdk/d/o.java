package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.c;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.HashMap;
import java.util.Map;

public class o extends m {
    private final c a;

    public o(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        super(d.a("adtoken_zone", jVar), appLovinAdLoadListener, "TaskFetchTokenAd", jVar);
        this.a = cVar;
    }

    public i a() {
        return i.p;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap(2);
        hashMap.put("adtoken", n.e(this.a.a()));
        hashMap.put("adtoken_prefix", n.e(this.a.c()));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b c() {
        return b.REGULAR_AD_TOKEN;
    }
}
