package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAdLoadListener;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class m extends a {
    private final d a;
    private final AppLovinAdLoadListener c;
    private boolean d;

    public m(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        this(dVar, appLovinAdLoadListener, "TaskFetchNextAd", jVar);
    }

    m(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, String str, j jVar) {
        super(str, jVar);
        this.d = false;
        this.a = dVar;
        this.c = appLovinAdLoadListener;
    }

    private void a(h hVar) {
        long b = hVar.b(g.c);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - b > TimeUnit.MINUTES.toMillis((long) ((Integer) this.b.a(com.applovin.impl.sdk.b.d.dM)).intValue())) {
            hVar.b(g.c, currentTimeMillis);
            hVar.c(g.d);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        boolean z = i != 204;
        p v = e().v();
        String f = f();
        Boolean valueOf = Boolean.valueOf(z);
        StringBuilder sb = new StringBuilder();
        sb.append("Unable to fetch ");
        sb.append(this.a);
        sb.append(" ad: server returned ");
        sb.append(i);
        v.a(f, valueOf, sb.toString());
        if (i == -800) {
            this.b.L().a(g.h);
        }
        try {
            a(i);
        } catch (Throwable th) {
            p.c(f(), "Unable process a failure to recieve an ad", th);
        }
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.h.d(jSONObject, this.b);
        com.applovin.impl.sdk.utils.h.c(jSONObject, this.b);
        com.applovin.impl.sdk.utils.h.e(jSONObject, this.b);
        this.b.K().a(a(jSONObject));
    }

    public i a() {
        return i.n;
    }

    /* access modifiers changed from: protected */
    public a a(JSONObject jSONObject) {
        s sVar = new s(jSONObject, this.a, c(), this.c, this.b);
        return sVar;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (this.c == null) {
            return;
        }
        if (this.c instanceof com.applovin.impl.sdk.m) {
            ((com.applovin.impl.sdk.m) this.c).a(this.a, i);
        } else {
            this.c.failedToReceiveAd(i);
        }
    }

    public void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap(4);
        hashMap.put("zone_id", n.e(this.a.a()));
        if (this.a.b() != null) {
            hashMap.put("size", this.a.b().getLabel());
        }
        if (this.a.c() != null) {
            hashMap.put("require", this.a.c().getLabel());
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.ad)).booleanValue()) {
            hashMap.put("n", String.valueOf(this.b.ab().a(this.a.a())));
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b c() {
        return this.a.i() ? b.APPLOVIN_PRIMARY_ZONE : b.APPLOVIN_CUSTOM_ZONE;
    }

    /* access modifiers changed from: protected */
    public String d() {
        return com.applovin.impl.sdk.utils.h.g(this.b);
    }

    /* access modifiers changed from: protected */
    public String i() {
        return com.applovin.impl.sdk.utils.h.h(this.b);
    }

    public void run() {
        StringBuilder sb;
        String str;
        if (this.d) {
            sb = new StringBuilder();
            str = "Preloading next ad of zone: ";
        } else {
            sb = new StringBuilder();
            str = "Fetching next ad of zone: ";
        }
        sb.append(str);
        sb.append(this.a);
        a(sb.toString());
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.eh)).booleanValue() && q.d()) {
            a("User is connected to a VPN");
        }
        h L = this.b.L();
        L.a(g.a);
        if (L.b(g.c) == 0) {
            L.b(g.c, System.currentTimeMillis());
        }
        try {
            Map a2 = this.b.O().a(b(), this.d, false);
            a(L);
            AnonymousClass1 r2 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(d()).a(a2).c(i()).b(HttpRequest.METHOD_GET).a(new JSONObject()).a(((Integer) this.b.a(com.applovin.impl.sdk.b.d.dB)).intValue()).b(((Integer) this.b.a(com.applovin.impl.sdk.b.d.dA)).intValue()).b(true).a(), this.b) {
                public void a(int i) {
                    m.this.b(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    if (i == 200) {
                        com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_latency_millis", this.d.a(), this.b);
                        com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_response_size", this.d.b(), this.b);
                        m.this.b(jSONObject);
                        return;
                    }
                    m.this.b(i);
                }
            };
            r2.a(com.applovin.impl.sdk.b.d.aL);
            r2.b(com.applovin.impl.sdk.b.d.aM);
            this.b.K().a((a) r2);
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to fetch ad ");
            sb2.append(this.a);
            a(sb2.toString(), th);
            b(0);
            this.b.M().a(a());
        }
    }
}
