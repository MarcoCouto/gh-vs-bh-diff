package com.applovin.impl.sdk.d;

import com.applovin.impl.a.a;
import com.applovin.impl.a.b;
import com.applovin.impl.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.a.f;
import com.applovin.impl.a.g;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdType;
import com.smaato.sdk.video.vast.model.InLine;
import com.smaato.sdk.video.vast.model.ViewableImpression;
import java.util.HashSet;
import java.util.Set;

class w extends a {
    private c a;
    private final AppLovinAdLoadListener c;

    w(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        super("TaskRenderVastAd", jVar);
        this.c = appLovinAdLoadListener;
        this.a = cVar;
    }

    public i a() {
        return i.w;
    }

    public void run() {
        a("Rendering VAST ad...");
        String str = "";
        String str2 = "";
        int size = this.a.b().size();
        HashSet hashSet = new HashSet(size);
        HashSet hashSet2 = new HashSet(size);
        f fVar = null;
        com.applovin.impl.a.j jVar = null;
        b bVar = null;
        for (s sVar : this.a.b()) {
            s c2 = sVar.c(com.applovin.impl.a.i.a(sVar) ? "Wrapper" : "InLine");
            if (c2 != null) {
                s c3 = c2.c("AdSystem");
                if (c3 != null) {
                    fVar = f.a(c3, fVar, this.b);
                }
                str = com.applovin.impl.a.i.a(c2, InLine.AD_TITLE, str);
                str2 = com.applovin.impl.a.i.a(c2, InLine.DESCRIPTION, str2);
                com.applovin.impl.a.i.a(c2.a("Impression"), (Set<g>) hashSet, this.a, this.b);
                s b = c2.b("ViewableImpression");
                if (b != null) {
                    com.applovin.impl.a.i.a(b.a(ViewableImpression.VIEWABLE), (Set<g>) hashSet, this.a, this.b);
                }
                com.applovin.impl.a.i.a(c2.a("Error"), (Set<g>) hashSet2, this.a, this.b);
                s b2 = c2.b("Creatives");
                if (b2 != null) {
                    for (s sVar2 : b2.d()) {
                        s b3 = sVar2.b("Linear");
                        if (b3 != null) {
                            jVar = com.applovin.impl.a.j.a(b3, jVar, this.a, this.b);
                        } else {
                            s c4 = sVar2.c("CompanionAds");
                            if (c4 != null) {
                                s c5 = c4.c("Companion");
                                if (c5 != null) {
                                    bVar = b.a(c5, bVar, this.a, this.b);
                                }
                            } else {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Received and will skip rendering for an unidentified creative: ");
                                sb.append(sVar2);
                                d(sb.toString());
                            }
                        }
                    }
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Did not find wrapper or inline response for node: ");
                sb2.append(sVar);
                d(sb2.toString());
            }
        }
        a a2 = a.aO().a(this.b).a(this.a.c()).b(this.a.d()).a(this.a.e()).a(this.a.f()).a(str).b(str2).a(fVar).a(jVar).a(bVar).a((Set<g>) hashSet).b((Set<g>) hashSet2).a();
        d a3 = com.applovin.impl.a.i.a(a2);
        if (a3 == null) {
            h hVar = new h(a2, this.b, this.c);
            r.a aVar = r.a.CACHING_OTHER;
            if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.bm)).booleanValue()) {
                if (a2.getType() == AppLovinAdType.REGULAR) {
                    aVar = r.a.CACHING_INTERSTITIAL;
                } else if (a2.getType() == AppLovinAdType.INCENTIVIZED) {
                    aVar = r.a.CACHING_INCENTIVIZED;
                }
            }
            this.b.K().a((a) hVar, aVar);
            return;
        }
        com.applovin.impl.a.i.a(this.a, this.c, a3, -6, this.b);
    }
}
