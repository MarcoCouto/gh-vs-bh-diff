package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.appodeal.ads.AppodealNetworks;
import org.json.JSONArray;
import org.json.JSONObject;

public class s extends a implements AppLovinAdLoadListener {
    private final JSONObject a;
    private final d c;
    private final b d;
    private final AppLovinAdLoadListener e;

    public s(JSONObject jSONObject, d dVar, b bVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        super("TaskProcessAdResponse", jVar);
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (dVar != null) {
            this.a = jSONObject;
            this.c = dVar;
            this.d = bVar;
            this.e = appLovinAdLoadListener;
        } else {
            throw new IllegalArgumentException("No zone specified");
        }
    }

    private void a(int i) {
        q.a(this.e, this.c, i, this.b);
    }

    private void a(AppLovinAd appLovinAd) {
        try {
            if (this.e != null) {
                this.e.adReceived(appLovinAd);
            }
        } catch (Throwable th) {
            a("Unable process a ad received notification", th);
        }
    }

    private void a(JSONObject jSONObject) {
        String b = i.b(jSONObject, "type", "undefined", this.b);
        if (AppodealNetworks.APPLOVIN.equalsIgnoreCase(b)) {
            a("Starting task for AppLovin ad...");
            r K = this.b.K();
            u uVar = new u(jSONObject, this.a, this.d, this, this.b);
            K.a((a) uVar);
        } else if (AppodealNetworks.VAST.equalsIgnoreCase(b)) {
            a("Starting task for VAST ad...");
            this.b.K().a((a) t.a(jSONObject, this.a, this.d, this, this.b));
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to process ad of unknown type: ");
            sb.append(b);
            c(sb.toString());
            failedToReceiveAd(-800);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.r;
    }

    public void adReceived(AppLovinAd appLovinAd) {
        a(appLovinAd);
    }

    public void failedToReceiveAd(int i) {
        a(i);
    }

    public void run() {
        JSONArray b = i.b(this.a, "ads", new JSONArray(), this.b);
        if (b.length() > 0) {
            a("Processing ad...");
            a(i.a(b, 0, new JSONObject(), this.b));
            return;
        }
        c("No ads were returned from the server");
        q.a(this.c.a(), this.a, this.b);
        a(204);
    }
}
