package com.applovin.impl.sdk.d;

import android.net.Uri;
import com.applovin.impl.mediation.k.a;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.e;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.n;
import com.applovin.impl.sdk.network.a.C0012a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

abstract class c extends a implements a {
    protected final f a;
    private AppLovinAdLoadListener c;
    private final n d;
    private final Collection<Character> e;
    private final e f;
    private boolean g;

    c(String str, f fVar, j jVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super(str, jVar);
        if (fVar != null) {
            this.a = fVar;
            this.c = appLovinAdLoadListener;
            this.d = jVar.V();
            this.e = j();
            this.f = new e();
            return;
        }
        throw new IllegalArgumentException("No ad specified.");
    }

    private Uri a(Uri uri, String str) {
        String str2;
        StringBuilder sb;
        if (uri != null) {
            String uri2 = uri.toString();
            if (com.applovin.impl.sdk.utils.n.b(uri2)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Caching ");
                sb2.append(str);
                sb2.append(" image...");
                a(sb2.toString());
                return g(uri2);
            }
            sb = new StringBuilder();
            sb.append("Failed to cache ");
            sb.append(str);
            str2 = " image";
        } else {
            sb = new StringBuilder();
            sb.append("No ");
            sb.append(str);
            str2 = " image to cache";
        }
        sb.append(str2);
        a(sb.toString());
        return null;
    }

    private String a(String str, String str2) {
        StringBuilder sb;
        String replace = str2.replace("/", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        String G = this.a.G();
        if (com.applovin.impl.sdk.utils.n.b(G)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(G);
            sb2.append(replace);
            replace = sb2.toString();
        }
        File a2 = this.d.a(replace, this.b.D());
        if (a2 == null) {
            return null;
        }
        if (a2.exists()) {
            this.f.b(a2.length());
            sb = new StringBuilder();
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(str2);
            if (!this.d.a(a2, sb3.toString(), Arrays.asList(new String[]{str}), this.f)) {
                return null;
            }
            sb = new StringBuilder();
        }
        sb.append("file://");
        sb.append(a2.getAbsolutePath());
        return sb.toString();
    }

    private Uri g(String str) {
        return b(str, this.a.F(), true);
    }

    private Collection<Character> j() {
        HashSet hashSet = new HashSet();
        for (char valueOf : ((String) this.b.a(d.bI)).toCharArray()) {
            hashSet.add(Character.valueOf(valueOf));
        }
        hashSet.add(Character.valueOf('\"'));
        return hashSet;
    }

    /* access modifiers changed from: 0000 */
    public Uri a(String str, List<String> list, boolean z) {
        String str2;
        try {
            if (com.applovin.impl.sdk.utils.n.b(str)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Caching video ");
                sb.append(str);
                sb.append("...");
                a(sb.toString());
                String a2 = this.d.a(g(), str, this.a.G(), list, z, this.f);
                if (com.applovin.impl.sdk.utils.n.b(a2)) {
                    File a3 = this.d.a(a2, g());
                    if (a3 != null) {
                        Uri fromFile = Uri.fromFile(a3);
                        if (fromFile != null) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Finish caching video for ad #");
                            sb2.append(this.a.getAdIdNumber());
                            sb2.append(". Updating ad with cachedVideoFilename = ");
                            sb2.append(a2);
                            a(sb2.toString());
                            return fromFile;
                        }
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Unable to create URI from cached video file = ");
                        sb3.append(a3);
                        str2 = sb3.toString();
                    } else {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Unable to cache video = ");
                        sb4.append(str);
                        sb4.append("Video file was missing or null");
                        str2 = sb4.toString();
                    }
                } else if (((Boolean) this.b.a(d.bL)).booleanValue()) {
                    d("Failed to cache video");
                    q.a(this.c, this.a.getAdZone(), AppLovinErrorCodes.UNABLE_TO_PRECACHE_VIDEO_RESOURCES, this.b);
                    this.c = null;
                } else {
                    str2 = "Failed to cache video, but not failing ad load";
                }
                d(str2);
            }
        } catch (Exception e2) {
            a("Encountered exception while attempting to cache video.", e2);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public String a(String str, List<String> list) {
        if (com.applovin.impl.sdk.utils.n.b(str)) {
            Uri parse = Uri.parse(str);
            if (parse == null) {
                a("Nothing to cache, skipping...");
                return null;
            }
            String lastPathSegment = parse.getLastPathSegment();
            if (com.applovin.impl.sdk.utils.n.b(this.a.G())) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.a.G());
                sb.append(lastPathSegment);
                lastPathSegment = sb.toString();
            }
            File a2 = this.d.a(lastPathSegment, g());
            ByteArrayOutputStream a3 = (a2 == null || !a2.exists()) ? null : this.d.a(a2);
            if (a3 == null) {
                a3 = this.d.a(str, list, true);
                if (a3 != null) {
                    this.d.a(a3, a2);
                    this.f.a((long) a3.size());
                }
            } else {
                this.f.b((long) a3.size());
            }
            try {
                return a3.toString("UTF-8");
            } catch (UnsupportedEncodingException e2) {
                a("UTF-8 encoding not supported.", e2);
            } catch (Throwable th) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("String resource at ");
                sb2.append(str);
                sb2.append(" failed to load.");
                a(sb2.toString(), th);
                return null;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public String a(String str, List<String> list, f fVar) {
        if (!com.applovin.impl.sdk.utils.n.b(str)) {
            return str;
        }
        if (!((Boolean) this.b.a(d.bK)).booleanValue()) {
            a("Resource caching is disabled, skipping cache...");
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        boolean shouldCancelHtmlCachingIfShown = fVar.shouldCancelHtmlCachingIfShown();
        for (String str2 : list) {
            int i = 0;
            int i2 = 0;
            while (i < sb.length()) {
                if (c()) {
                    return str;
                }
                i = sb.indexOf(str2, i2);
                if (i == -1) {
                    continue;
                    break;
                }
                int length = sb.length();
                int i3 = i;
                while (!this.e.contains(Character.valueOf(sb.charAt(i3))) && i3 < length) {
                    i3++;
                }
                if (i3 <= i || i3 == length) {
                    d("Unable to cache resource; ad HTML is invalid.");
                    return str;
                }
                String substring = sb.substring(str2.length() + i, i3);
                if (!com.applovin.impl.sdk.utils.n.b(substring)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Skip caching of non-resource ");
                    sb2.append(substring);
                    a(sb2.toString());
                } else if (!shouldCancelHtmlCachingIfShown || !fVar.hasShown()) {
                    String a2 = a(str2, substring);
                    if (a2 != null) {
                        sb.replace(i, i3, a2);
                        this.f.e();
                    } else {
                        this.f.f();
                    }
                } else {
                    a("Cancelling HTML caching due to ad being shown already");
                    this.f.a();
                    return str;
                }
                i2 = i3;
            }
        }
        return sb.toString();
    }

    public void a(com.applovin.impl.mediation.b.a aVar) {
        if (aVar.b().equalsIgnoreCase(this.a.K())) {
            d("Updating flag for timeout...");
            this.g = true;
        }
        this.b.A().b(this);
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinAdBase appLovinAdBase) {
        com.applovin.impl.sdk.c.d.a(this.f, appLovinAdBase, this.b);
    }

    /* access modifiers changed from: 0000 */
    public Uri b(String str, List<String> list, boolean z) {
        String sb;
        try {
            String a2 = this.d.a(g(), str, this.a.G(), list, z, this.f);
            if (com.applovin.impl.sdk.utils.n.b(a2)) {
                File a3 = this.d.a(a2, g());
                if (a3 != null) {
                    Uri fromFile = Uri.fromFile(a3);
                    if (fromFile != null) {
                        return fromFile;
                    }
                    sb = "Unable to extract Uri from image file";
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unable to retrieve File from cached image filename = ");
                    sb2.append(a2);
                    sb = sb2.toString();
                }
                d(sb);
            }
        } catch (Throwable th) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to cache image at url = ");
            sb3.append(str);
            a(sb3.toString(), th);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.b.A().b(this);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        a("Caching mute images...");
        Uri a2 = a(this.a.aF(), Events.CREATIVE_MUTE);
        if (a2 != null) {
            this.a.b(a2);
        }
        Uri a3 = a(this.a.aG(), Events.CREATIVE_UNMUTE);
        if (a3 != null) {
            this.a.c(a3);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Ad updated with muteImageFilename = ");
        sb.append(this.a.aF());
        sb.append(", unmuteImageFilename = ");
        sb.append(this.a.aG());
        a(sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public Uri e(String str) {
        return a(str, this.a.F(), true);
    }

    /* access modifiers changed from: 0000 */
    public String f(final String str) {
        if (!com.applovin.impl.sdk.utils.n.b(str)) {
            return null;
        }
        b a2 = b.a(this.b).a(str).b(HttpRequest.METHOD_GET).a("").a(0).a();
        final AtomicReference atomicReference = new AtomicReference(null);
        this.b.J().a(a2, new C0012a(), new com.applovin.impl.sdk.network.a.c<String>() {
            public void a(int i) {
                c cVar = c.this;
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to load resource from '");
                sb.append(str);
                sb.append("'");
                cVar.d(sb.toString());
            }

            public void a(String str, int i) {
                atomicReference.set(str);
            }
        });
        String str2 = (String) atomicReference.get();
        if (str2 != null) {
            this.f.a((long) str2.length());
        }
        return str2;
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        if (this.c != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Rendered new ad:");
            sb.append(this.a);
            a(sb.toString());
            this.c.adReceived(this.a);
            this.c = null;
        }
    }

    public void run() {
        if (this.a.J()) {
            a("Subscribing to timeout events...");
            this.b.A().a((a) this);
        }
    }
}
