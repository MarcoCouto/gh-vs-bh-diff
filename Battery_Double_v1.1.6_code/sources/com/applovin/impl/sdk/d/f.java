package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.n;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.q;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import java.util.List;

abstract class f extends a {
    protected final AppLovinNativeAdPrecacheListener a;
    private final List<NativeAdImpl> c;
    private final AppLovinNativeAdLoadListener d;
    private int e;

    f(String str, List<NativeAdImpl> list, j jVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super(str, jVar);
        this.c = list;
        this.d = appLovinNativeAdLoadListener;
        this.a = null;
    }

    f(String str, List<NativeAdImpl> list, j jVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        super(str, jVar);
        if (list != null) {
            this.c = list;
            this.d = null;
            this.a = appLovinNativeAdPrecacheListener;
            return;
        }
        throw new IllegalArgumentException("Native ads cannot be null");
    }

    private void a(int i) {
        if (this.d != null) {
            this.d.onNativeAdsFailedToLoad(i);
        }
    }

    private void a(List<AppLovinNativeAd> list) {
        if (this.d != null) {
            this.d.onNativeAdsLoaded(list);
        }
    }

    /* access modifiers changed from: protected */
    public String a(String str, n nVar, List<String> list) {
        if (!com.applovin.impl.sdk.utils.n.b(str)) {
            a("Asked to cache file with null/empty URL, nothing to do.");
            return null;
        } else if (!q.a(str, list)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Domain is not whitelisted, skipping precache for URL ");
            sb.append(str);
            a(sb.toString());
            return null;
        } else {
            try {
                String a2 = nVar.a(g(), str, null, list, true, true, null);
                if (a2 != null) {
                    return a2;
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to cache icon resource ");
                sb2.append(str);
                c(sb2.toString());
                return null;
            } catch (Exception e2) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to cache icon resource ");
                sb3.append(str);
                a(sb3.toString(), e2);
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(NativeAdImpl nativeAdImpl);

    /* access modifiers changed from: protected */
    public abstract void a(NativeAdImpl nativeAdImpl, int i);

    /* access modifiers changed from: protected */
    public abstract boolean a(NativeAdImpl nativeAdImpl, n nVar);

    public void run() {
        List list;
        for (NativeAdImpl nativeAdImpl : this.c) {
            a("Beginning resource caching phase...");
            if (a(nativeAdImpl, this.b.V())) {
                this.e++;
                a(nativeAdImpl);
            } else {
                d("Unable to cache resources");
            }
        }
        try {
            if (this.e == this.c.size()) {
                list = this.c;
            } else if (((Boolean) this.b.a(d.dq)).booleanValue()) {
                d("Mismatch between successful populations and requested size");
                a(-6);
                return;
            } else {
                list = this.c;
            }
            a(list);
        } catch (Throwable th) {
            p.c(f(), "Encountered exception while notifying publisher code", th);
        }
    }
}
