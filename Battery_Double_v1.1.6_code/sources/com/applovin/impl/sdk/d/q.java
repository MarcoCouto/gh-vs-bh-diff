package com.applovin.impl.sdk.d;

import android.app.Activity;
import com.applovin.impl.sdk.EventServiceImpl;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.r.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.e;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.TimeUnit;

public class q extends a {
    /* access modifiers changed from: private */
    public final j a;

    public q(j jVar) {
        super("TaskInitializeSdk", jVar);
        this.a = jVar;
    }

    private void a(d<Boolean> dVar) {
        if (((Boolean) this.a.a(dVar)).booleanValue()) {
            this.a.T().f(com.applovin.impl.sdk.ad.d.a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, this.a));
        }
    }

    private void b() {
        if (!this.a.x().a()) {
            Activity ag = this.a.ag();
            if (ag != null) {
                this.a.x().a(ag);
            } else {
                this.a.K().a(new ac(this.a, true, new Runnable() {
                    public void run() {
                        q.this.a.x().a(q.this.a.aa().a());
                    }
                }), a.MAIN, TimeUnit.SECONDS.toMillis(1));
            }
        }
    }

    private void c() {
        this.a.K().a((a) new b(this.a), a.MAIN);
    }

    private void d() {
        this.a.T().a();
        this.a.U().a();
    }

    private void i() {
        j();
        k();
        l();
    }

    private void j() {
        LinkedHashSet a2 = this.a.W().a();
        if (!a2.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Scheduling preload(s) for ");
            sb.append(a2.size());
            sb.append(" zone(s)");
            a(sb.toString());
            Iterator it = a2.iterator();
            while (it.hasNext()) {
                com.applovin.impl.sdk.ad.d dVar = (com.applovin.impl.sdk.ad.d) it.next();
                if (dVar.d()) {
                    this.a.p().preloadAds(dVar);
                } else {
                    this.a.o().preloadAds(dVar);
                }
            }
        }
    }

    private void k() {
        d<Boolean> dVar = d.be;
        String str = (String) this.a.a(d.bd);
        boolean z = false;
        if (str.length() > 0) {
            for (String fromString : e.a(str)) {
                AppLovinAdSize fromString2 = AppLovinAdSize.fromString(fromString);
                if (fromString2 != null) {
                    this.a.T().f(com.applovin.impl.sdk.ad.d.a(fromString2, AppLovinAdType.REGULAR, this.a));
                    if (AppLovinAdSize.INTERSTITIAL.getLabel().equals(fromString2.getLabel())) {
                        a(dVar);
                        z = true;
                    }
                }
            }
        }
        if (!z) {
            a(dVar);
        }
    }

    private void l() {
        if (((Boolean) this.a.a(d.bf)).booleanValue()) {
            this.a.U().f(com.applovin.impl.sdk.ad.d.h(this.a));
        }
    }

    public i a() {
        return i.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0145, code lost:
        if (r6.a.d() == false) goto L_0x014a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0147, code lost:
        r3 = com.facebook.internal.AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x014a, code lost:
        r3 = com.ironsource.sdk.constants.Constants.ParametersKeys.FAILED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x014c, code lost:
        r2.append(r3);
        r2.append(" in ");
        r2.append(java.lang.System.currentTimeMillis() - r0);
        r2.append("ms");
        a(r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0168, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00e9, code lost:
        if (r6.a.d() != false) goto L_0x0147;
     */
    public void run() {
        StringBuilder sb;
        long currentTimeMillis = System.currentTimeMillis();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Initializing AppLovin SDK ");
        sb2.append(AppLovinSdk.VERSION);
        sb2.append("...");
        a(sb2.toString());
        try {
            this.a.L().d();
            this.a.L().c(g.b);
            this.a.V().a(g());
            this.a.V().b(g());
            d();
            i();
            b();
            this.a.X().a();
            c();
            this.a.O().e();
            this.a.ad().a();
            this.a.a(true);
            this.a.N().a();
            ((EventServiceImpl) this.a.q()).maybeTrackAppOpenEvent();
            this.a.B().a();
            if (this.a.z().a()) {
                this.a.z().b();
            }
            if (((Boolean) this.a.a(d.au)).booleanValue()) {
                this.a.a(((Long) this.a.a(d.av)).longValue());
            }
            sb = new StringBuilder();
            sb.append("AppLovin SDK ");
            sb.append(AppLovinSdk.VERSION);
            sb.append(" initialization ");
        } catch (Throwable th) {
            if (((Boolean) this.a.a(d.au)).booleanValue()) {
                this.a.a(((Long) this.a.a(d.av)).longValue());
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append("AppLovin SDK ");
            sb3.append(AppLovinSdk.VERSION);
            sb3.append(" initialization ");
            sb3.append(this.a.d() ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : ParametersKeys.FAILED);
            sb3.append(" in ");
            sb3.append(System.currentTimeMillis() - currentTimeMillis);
            sb3.append("ms");
            a(sb3.toString());
            throw th;
        }
    }
}
