package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.ad.NativeAdImpl.a;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

class v extends a {
    private final AppLovinNativeAdLoadListener a;
    private final JSONObject c;

    v(JSONObject jSONObject, j jVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super("TaskRenderNativeAd", jVar);
        this.a = appLovinNativeAdLoadListener;
        this.c = jSONObject;
    }

    private String a(String str, JSONObject jSONObject, String str2) {
        String b = i.b(jSONObject, str, (String) null, this.b);
        if (b != null) {
            return b.replace("{CLCODE}", str2);
        }
        return null;
    }

    private String a(JSONObject jSONObject, String str, String str2) {
        String b = i.b(jSONObject, TapjoyConstants.TJC_CLICK_URL, (String) null, this.b);
        if (str2 == null) {
            str2 = "";
        }
        return b.replace("{CLCODE}", str).replace("{EVENT_ID}", str2);
    }

    private void a(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject;
        JSONArray b = i.b(jSONObject2, "native_ads", new JSONArray(), this.b);
        JSONObject b2 = i.b(jSONObject2, "native_settings", new JSONObject(), this.b);
        if (b.length() > 0) {
            ArrayList arrayList = new ArrayList(b.length());
            int i = 0;
            while (i < b.length()) {
                JSONObject a2 = i.a(b, i, (JSONObject) null, this.b);
                String b3 = i.b(a2, "clcode", (String) null, this.b);
                String b4 = i.b(jSONObject2, "zone_id", (String) null, this.b);
                d b5 = d.b(b4, this.b);
                String b6 = i.b(a2, "event_id", (String) null, this.b);
                String a3 = a("simp_url", b2, b3);
                String a4 = a(b2, b3, b6);
                List a5 = q.a("simp_urls", b2, b3, a3, this.b);
                JSONArray jSONArray = b;
                String str = a4;
                int i2 = i;
                String str2 = a3;
                d dVar = b5;
                List a6 = q.a("click_tracking_urls", b2, b3, b6, i.a(b2, "should_post_click_url", Boolean.valueOf(true), this.b).booleanValue() ? a4 : null, this.b);
                if (a5.size() == 0) {
                    throw new IllegalArgumentException("No impression URL available");
                } else if (a6.size() != 0) {
                    String b7 = i.b(a2, "resource_cache_prefix", (String) null, this.b);
                    NativeAdImpl a7 = new a().a(dVar).e(b4).f(i.b(a2, "title", (String) null, this.b)).g(i.b(a2, "description", (String) null, this.b)).h(i.b(a2, ShareConstants.FEED_CAPTION_PARAM, (String) null, this.b)).q(i.b(a2, "cta", (String) null, this.b)).a(i.b(a2, "icon_url", (String) null, this.b)).b(i.b(a2, MessengerShareContentUtility.IMAGE_URL, (String) null, this.b)).d(i.b(a2, TapjoyConstants.TJC_VIDEO_URL, (String) null, this.b)).c(i.b(a2, "star_rating_url", (String) null, this.b)).i(i.b(a2, "icon_url", (String) null, this.b)).j(i.b(a2, MessengerShareContentUtility.IMAGE_URL, (String) null, this.b)).k(i.b(a2, TapjoyConstants.TJC_VIDEO_URL, (String) null, this.b)).a(i.a(a2, "star_rating", 5.0f, this.b)).p(b3).l(str).m(str2).n(a("video_start_url", b2, b3)).o(a("video_end_url", b2, b3)).a(a5).b(a6).a(i.a(a2, "ad_id", 0, this.b)).c(n.b(b7) ? e.a(b7) : this.b.b((com.applovin.impl.sdk.b.d) com.applovin.impl.sdk.b.d.bM)).a(this.b).a();
                    arrayList.add(a7);
                    StringBuilder sb = new StringBuilder();
                    sb.append("Prepared native ad: ");
                    sb.append(a7.getAdId());
                    a(sb.toString());
                    i = i2 + 1;
                    b = jSONArray;
                    jSONObject2 = jSONObject;
                } else {
                    throw new IllegalArgumentException("No click tracking URL available");
                }
            }
            if (this.a != null) {
                this.a.onNativeAdsLoaded(arrayList);
                return;
            }
            return;
        }
        c("No ads were returned from the server");
        this.a.onNativeAdsFailedToLoad(204);
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.v;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        try {
            if (this.a != null) {
                this.a.onNativeAdsFailedToLoad(i);
            }
        } catch (Exception e) {
            a("Unable to notify listener about failure.", e);
        }
    }

    public void run() {
        if (this.c == null || this.c.length() <= 0) {
            d("Attempting to run task with empty or null ad response");
            a(204);
            return;
        }
        a(this.c);
    }
}
