package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.k.b;
import com.applovin.impl.sdk.k.c;
import com.applovin.impl.sdk.k.d;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class p extends a {
    /* access modifiers changed from: private */
    public final a a;

    public interface a {
        void a();
    }

    public p(j jVar, a aVar) {
        super("TaskFetchVariables", jVar);
        this.a = aVar;
    }

    private void a(Map<String, String> map) {
        try {
            com.applovin.impl.sdk.k.a d = this.b.O().d();
            String str = d.b;
            if (n.b(str)) {
                map.put("idfa", str);
            }
            map.put("dnt", Boolean.toString(d.a));
        } catch (Throwable th) {
            a("Failed to populate advertising info", th);
        }
    }

    public i a() {
        return i.q;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> b() {
        k O = this.b.O();
        d b = O.b();
        b c = O.c();
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_PLATFORM, n.e(b.c));
        hashMap.put("model", n.e(b.a));
        hashMap.put("package_name", n.e(c.c));
        hashMap.put("installer_name", n.e(c.d));
        hashMap.put("ia", Long.toString(c.h));
        hashMap.put("api_did", this.b.a(com.applovin.impl.sdk.b.d.X));
        hashMap.put("brand", n.e(b.d));
        hashMap.put("brand_name", n.e(b.e));
        hashMap.put("hardware", n.e(b.f));
        hashMap.put("revision", n.e(b.g));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("os", n.e(b.b));
        hashMap.put("orientation_lock", b.l);
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, n.e(c.b));
        hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, n.e(b.i));
        hashMap.put("carrier", n.e(b.j));
        hashMap.put("tz_offset", String.valueOf(b.r));
        hashMap.put("aida", String.valueOf(b.N));
        hashMap.put("adr", b.t ? "1" : "0");
        hashMap.put(AvidVideoPlaybackListenerImpl.VOLUME, String.valueOf(b.x));
        hashMap.put("sb", String.valueOf(b.y));
        hashMap.put("sim", b.A ? "1" : "0");
        hashMap.put("gy", String.valueOf(b.B));
        hashMap.put("is_tablet", String.valueOf(b.C));
        hashMap.put("tv", String.valueOf(b.D));
        hashMap.put("vs", String.valueOf(b.E));
        hashMap.put("lpm", String.valueOf(b.F));
        hashMap.put("tg", c.e);
        hashMap.put("ltg", c.f);
        hashMap.put("fs", String.valueOf(b.H));
        hashMap.put("tds", String.valueOf(b.I));
        hashMap.put("fm", String.valueOf(b.J.b));
        hashMap.put("tm", String.valueOf(b.J.a));
        hashMap.put("lmt", String.valueOf(b.J.c));
        hashMap.put("lm", String.valueOf(b.J.d));
        hashMap.put("adns", String.valueOf(b.m));
        hashMap.put("adnsd", String.valueOf(b.n));
        hashMap.put("xdpi", String.valueOf(b.o));
        hashMap.put("ydpi", String.valueOf(b.p));
        hashMap.put("screen_size_in", String.valueOf(b.q));
        hashMap.put("debug", Boolean.toString(q.b(this.b)));
        hashMap.put("af", String.valueOf(b.v));
        hashMap.put("font", String.valueOf(b.w));
        hashMap.put("bt_ms", String.valueOf(b.Q));
        if (!((Boolean) this.b.a(com.applovin.impl.sdk.b.d.eR)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.t());
        }
        a((Map<String, String>) hashMap);
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.dT)).booleanValue()) {
            q.a("cuid", this.b.i(), (Map<String, String>) hashMap);
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.dW)).booleanValue()) {
            hashMap.put("compass_random_token", this.b.j());
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.dY)).booleanValue()) {
            hashMap.put("applovin_random_token", this.b.k());
        }
        Boolean bool = b.K;
        if (bool != null) {
            hashMap.put("huc", bool.toString());
        }
        Boolean bool2 = b.L;
        if (bool2 != null) {
            hashMap.put("aru", bool2.toString());
        }
        Boolean bool3 = b.M;
        if (bool3 != null) {
            hashMap.put("dns", bool3.toString());
        }
        c cVar = b.u;
        if (cVar != null) {
            hashMap.put("act", String.valueOf(cVar.a));
            hashMap.put("acm", String.valueOf(cVar.b));
        }
        String str = b.z;
        if (n.b(str)) {
            hashMap.put("ua", n.e(str));
        }
        String str2 = b.G;
        if (!TextUtils.isEmpty(str2)) {
            hashMap.put("so", n.e(str2));
        }
        if (b.O > 0.0f) {
            hashMap.put("da", String.valueOf(b.O));
        }
        if (b.P > 0.0f) {
            hashMap.put("dm", String.valueOf(b.P));
        }
        hashMap.put("sc", n.e((String) this.b.a(com.applovin.impl.sdk.b.d.aa)));
        hashMap.put("sc2", n.e((String) this.b.a(com.applovin.impl.sdk.b.d.ab)));
        hashMap.put("server_installed_at", n.e((String) this.b.a(com.applovin.impl.sdk.b.d.ac)));
        q.a("persisted_data", n.e((String) this.b.a(f.z)), (Map<String, String>) hashMap);
        return hashMap;
    }

    public void run() {
        AnonymousClass1 r1 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(h.i(this.b)).c(h.j(this.b)).a(b()).b(HttpRequest.METHOD_GET).a(new JSONObject()).b(((Integer) this.b.a(com.applovin.impl.sdk.b.d.dJ)).intValue()).a(), this.b) {
            public void a(int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to fetch variables: server returned ");
                sb.append(i);
                d(sb.toString());
                com.applovin.impl.sdk.p.j("AppLovinVariableService", "Failed to load variables.");
                p.this.a.a();
            }

            public void a(JSONObject jSONObject, int i) {
                h.d(jSONObject, this.b);
                h.c(jSONObject, this.b);
                h.f(jSONObject, this.b);
                p.this.a.a();
            }
        };
        r1.a(com.applovin.impl.sdk.b.d.aR);
        r1.b(com.applovin.impl.sdk.b.d.aS);
        this.b.K().a((a) r1);
    }
}
