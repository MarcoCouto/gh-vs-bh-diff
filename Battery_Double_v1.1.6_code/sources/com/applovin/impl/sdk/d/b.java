package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.k.a;
import com.applovin.impl.sdk.k.c;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.internal.NativeProtocol;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b extends a {
    b(j jVar) {
        super("TaskApiSubmitData", jVar);
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        try {
            this.b.P().c();
            JSONObject a = h.a(jSONObject);
            this.b.C().a(d.X, (Object) a.getString("device_id"));
            this.b.C().a(d.Y, (Object) a.getString("device_token"));
            this.b.C().b();
            h.d(a, this.b);
            h.e(a, this.b);
            String b = i.b(a, "latest_version", "", this.b);
            if (!TextUtils.isEmpty(b) && !AppLovinSdk.VERSION.equals(b)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Current SDK version (");
                sb.append(AppLovinSdk.VERSION);
                sb.append(") is outdated. Please integrate the latest version of the AppLovin SDK (");
                sb.append(b);
                sb.append("). Doing so will improve your CPMs and ensure you have access to the latest revenue earning features.");
                String sb2 = sb.toString();
                if (i.a(a, "sdk_update_message")) {
                    sb2 = i.b(a, "sdk_update_message", sb2, this.b);
                }
                p.i("AppLovinSdk", sb2);
            }
            this.b.L().b();
            this.b.M().b();
        } catch (Throwable th) {
            a("Unable to parse API response", th);
        }
    }

    private void b(JSONObject jSONObject) throws JSONException {
        k O = this.b.O();
        com.applovin.impl.sdk.k.b c = O.c();
        k.d b = O.b();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("model", b.a);
        jSONObject2.put("os", b.b);
        jSONObject2.put("brand", b.d);
        jSONObject2.put("brand_name", b.e);
        jSONObject2.put("hardware", b.f);
        jSONObject2.put("sdk_version", b.h);
        jSONObject2.put("revision", b.g);
        jSONObject2.put("adns", (double) b.m);
        jSONObject2.put("adnsd", b.n);
        jSONObject2.put("xdpi", String.valueOf(b.o));
        jSONObject2.put("ydpi", String.valueOf(b.p));
        jSONObject2.put("screen_size_in", String.valueOf(b.q));
        jSONObject2.put("gy", n.a(b.B));
        jSONObject2.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, b.i);
        jSONObject2.put("carrier", b.j);
        jSONObject2.put("orientation_lock", b.l);
        jSONObject2.put("tz_offset", b.r);
        jSONObject2.put("aida", String.valueOf(b.N));
        jSONObject2.put("adr", n.a(b.t));
        jSONObject2.put("wvvc", b.s);
        jSONObject2.put(AvidVideoPlaybackListenerImpl.VOLUME, b.x);
        jSONObject2.put("sb", b.y);
        jSONObject2.put("type", "android");
        jSONObject2.put("sim", n.a(b.A));
        jSONObject2.put("is_tablet", n.a(b.C));
        jSONObject2.put("lpm", b.F);
        jSONObject2.put("tv", n.a(b.D));
        jSONObject2.put("vs", n.a(b.E));
        jSONObject2.put("fs", b.H);
        jSONObject2.put("tds", b.I);
        jSONObject2.put("fm", String.valueOf(b.J.b));
        jSONObject2.put("tm", String.valueOf(b.J.a));
        jSONObject2.put("lmt", String.valueOf(b.J.c));
        jSONObject2.put("lm", String.valueOf(b.J.d));
        jSONObject2.put("af", String.valueOf(b.v));
        jSONObject2.put("font", String.valueOf(b.w));
        jSONObject2.put("bt_ms", String.valueOf(b.Q));
        g(jSONObject2);
        Boolean bool = b.K;
        if (bool != null) {
            jSONObject2.put("huc", bool.toString());
        }
        Boolean bool2 = b.L;
        if (bool2 != null) {
            jSONObject2.put("aru", bool2.toString());
        }
        Boolean bool3 = b.M;
        if (bool3 != null) {
            jSONObject2.put("dns", bool3.toString());
        }
        c cVar = b.u;
        if (cVar != null) {
            jSONObject2.put("act", cVar.a);
            jSONObject2.put("acm", cVar.b);
        }
        String str = b.z;
        if (n.b(str)) {
            jSONObject2.put("ua", n.e(str));
        }
        String str2 = b.G;
        if (!TextUtils.isEmpty(str2)) {
            jSONObject2.put("so", n.e(str2));
        }
        Locale locale = b.k;
        if (locale != null) {
            jSONObject2.put("locale", n.e(locale.toString()));
        }
        if (b.O > 0.0f) {
            jSONObject2.put("da", (double) b.O);
        }
        if (b.P > 0.0f) {
            jSONObject2.put("dm", (double) b.P);
        }
        jSONObject.put(DeviceRequestsHelper.DEVICE_INFO_PARAM, jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("package_name", c.c);
        jSONObject3.put("installer_name", c.d);
        jSONObject3.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, c.a);
        jSONObject3.put(TapjoyConstants.TJC_APP_VERSION_NAME, c.b);
        jSONObject3.put("installed_at", c.h);
        jSONObject3.put("tg", c.e);
        jSONObject3.put("ltg", c.f);
        jSONObject3.put("applovin_sdk_version", AppLovinSdk.VERSION);
        jSONObject3.put("first_install", String.valueOf(this.b.H()));
        jSONObject3.put("first_install_v2", String.valueOf(!this.b.I()));
        jSONObject3.put("debug", Boolean.toString(q.b(this.b)));
        String str3 = (String) this.b.a(d.ea);
        if (n.b(str3)) {
            jSONObject3.put("plugin_version", str3);
        }
        if (((Boolean) this.b.a(d.dT)).booleanValue() && n.b(this.b.i())) {
            jSONObject3.put("cuid", this.b.i());
        }
        if (((Boolean) this.b.a(d.dW)).booleanValue()) {
            jSONObject3.put("compass_random_token", this.b.j());
        }
        if (((Boolean) this.b.a(d.dY)).booleanValue()) {
            jSONObject3.put("applovin_random_token", this.b.k());
        }
        jSONObject.put("app_info", jSONObject3);
    }

    private void c(JSONObject jSONObject) throws JSONException {
        if (((Boolean) this.b.a(d.ex)).booleanValue()) {
            jSONObject.put("stats", this.b.L().c());
        }
        if (((Boolean) this.b.a(d.ag)).booleanValue()) {
            JSONObject b = com.applovin.impl.sdk.network.d.b(g());
            if (b.length() > 0) {
                jSONObject.put("network_response_codes", b);
            }
            if (((Boolean) this.b.a(d.ah)).booleanValue()) {
                com.applovin.impl.sdk.network.d.a(g());
            }
        }
    }

    private void d(JSONObject jSONObject) throws JSONException {
        if (((Boolean) this.b.a(d.eE)).booleanValue()) {
            JSONArray a = this.b.P().a();
            if (a != null && a.length() > 0) {
                jSONObject.put("errors", a);
            }
        }
    }

    private void e(JSONObject jSONObject) throws JSONException {
        if (((Boolean) this.b.a(d.eD)).booleanValue()) {
            JSONArray a = this.b.M().a();
            if (a != null && a.length() > 0) {
                jSONObject.put("tasks", a);
            }
        }
    }

    private void f(JSONObject jSONObject) {
        AnonymousClass1 r0 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(h.a("2.0/device", this.b)).c(h.b("2.0/device", this.b)).a(h.e(this.b)).b(HttpRequest.METHOD_POST).a(jSONObject).a(new JSONObject()).a(((Integer) this.b.a(d.dC)).intValue()).a(), this.b) {
            public void a(int i) {
                h.a(i, this.b);
            }

            public void a(JSONObject jSONObject, int i) {
                b.this.a(jSONObject);
            }
        };
        r0.a(d.aN);
        r0.b(d.aO);
        this.b.K().a((a) r0);
    }

    private void g(JSONObject jSONObject) {
        try {
            a d = this.b.O().d();
            String str = d.b;
            if (n.b(str)) {
                jSONObject.put("idfa", str);
            }
            jSONObject.put("dnt", Boolean.toString(d.a));
        } catch (Throwable th) {
            a("Failed to populate advertising info", th);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.h;
    }

    public void run() {
        try {
            b("Submitting user data...");
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            c(jSONObject);
            d(jSONObject);
            e(jSONObject);
            f(jSONObject);
        } catch (JSONException e) {
            a("Unable to build JSON message with collected data", e);
            this.b.M().a(a());
        }
    }
}
