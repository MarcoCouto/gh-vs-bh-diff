package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.utils.i;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Map;
import org.json.JSONObject;

public abstract class z extends ab {
    protected z(String str, j jVar) {
        super(str, jVar);
    }

    private JSONObject a(c cVar) {
        JSONObject i = i();
        i.a(i, IronSourceConstants.EVENTS_RESULT, cVar.b(), this.b);
        Map a = cVar.a();
        if (a != null) {
            i.a(i, "params", new JSONObject(a), this.b);
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public abstract void b(JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public abstract c c();

    /* access modifiers changed from: protected */
    public abstract void d();

    public void run() {
        c c = c();
        if (c != null) {
            a(a(c), new a.c<JSONObject>() {
                public void a(int i) {
                    z.this.a(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    z.this.b(jSONObject);
                }
            });
        } else {
            d();
        }
    }
}
