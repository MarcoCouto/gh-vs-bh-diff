package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.q;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class r {
    private final String a = "TaskManager";
    /* access modifiers changed from: private */
    public final j b;
    /* access modifiers changed from: private */
    public final p c;
    private final ScheduledThreadPoolExecutor d;
    private final ScheduledThreadPoolExecutor e;
    private final ScheduledThreadPoolExecutor f;
    private final ScheduledThreadPoolExecutor g;
    private final ScheduledThreadPoolExecutor h;
    private final ScheduledThreadPoolExecutor i;
    private final ScheduledThreadPoolExecutor j;
    private final ScheduledThreadPoolExecutor k;
    private final ScheduledThreadPoolExecutor l;
    private final ScheduledThreadPoolExecutor m;
    private final ScheduledThreadPoolExecutor n;
    private final ScheduledThreadPoolExecutor o;
    private final ScheduledThreadPoolExecutor p;
    private final ScheduledThreadPoolExecutor q;
    private final ScheduledThreadPoolExecutor r;
    private final ScheduledThreadPoolExecutor s;
    private final ScheduledThreadPoolExecutor t;
    private final ScheduledThreadPoolExecutor u;
    private final ScheduledThreadPoolExecutor v;
    private final ScheduledThreadPoolExecutor w;
    private final List<c> x = new ArrayList(5);
    private final Object y = new Object();
    private boolean z;

    public enum a {
        MAIN,
        TIMEOUT,
        BACKGROUND,
        ADVERTISING_INFO_COLLECTION,
        POSTBACKS,
        CACHING_INTERSTITIAL,
        CACHING_INCENTIVIZED,
        CACHING_OTHER,
        REWARD,
        MEDIATION_MAIN,
        MEDIATION_TIMEOUT,
        MEDIATION_BACKGROUND,
        MEDIATION_POSTBACKS,
        MEDIATION_BANNER,
        MEDIATION_INTERSTITIAL,
        MEDIATION_INCENTIVIZED,
        MEDIATION_REWARD
    }

    private class b implements ThreadFactory {
        private final String b;

        b(String str) {
            this.b = str;
        }

        public Thread newThread(Runnable runnable) {
            StringBuilder sb = new StringBuilder();
            sb.append("AppLovinSdk:");
            sb.append(this.b);
            sb.append(":");
            sb.append(q.a(r.this.b.t()));
            Thread thread = new Thread(runnable, sb.toString());
            thread.setDaemon(true);
            thread.setPriority(10);
            thread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable th) {
                    r.this.c.b("TaskManager", "Caught unhandled exception", th);
                }
            });
            return thread;
        }
    }

    private class c implements Runnable {
        private final String b;
        /* access modifiers changed from: private */
        public final a c;
        /* access modifiers changed from: private */
        public final a d;

        c(a aVar, a aVar2) {
            this.b = aVar.f();
            this.c = aVar;
            this.d = aVar2;
        }

        public void run() {
            long a2;
            p b2;
            String str;
            StringBuilder sb;
            long currentTimeMillis = System.currentTimeMillis();
            try {
                g.a();
                if (r.this.b.c()) {
                    if (!this.c.h()) {
                        r.this.c.c(this.b, "Task re-scheduled...");
                        r.this.a(this.c, this.d, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
                        a2 = r.this.a(this.d) - 1;
                        b2 = r.this.c;
                        str = "TaskManager";
                        sb = new StringBuilder();
                        sb.append(this.d);
                        sb.append(" queue finished task ");
                        sb.append(this.c.f());
                        sb.append(" with queue size ");
                        sb.append(a2);
                        b2.c(str, sb.toString());
                    }
                }
                r.this.c.c(this.b, "Task started execution...");
                this.c.run();
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                r.this.b.M().a(this.c.a(), currentTimeMillis2);
                p b3 = r.this.c;
                String str2 = this.b;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Task executed successfully in ");
                sb2.append(currentTimeMillis2);
                sb2.append("ms.");
                b3.c(str2, sb2.toString());
                a2 = r.this.a(this.d) - 1;
                b2 = r.this.c;
                str = "TaskManager";
                sb = new StringBuilder();
            } catch (Throwable th) {
                long a3 = r.this.a(this.d) - 1;
                p b4 = r.this.c;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.d);
                sb3.append(" queue finished task ");
                sb3.append(this.c.f());
                sb3.append(" with queue size ");
                sb3.append(a3);
                b4.c("TaskManager", sb3.toString());
                throw th;
            }
            sb.append(this.d);
            sb.append(" queue finished task ");
            sb.append(this.c.f());
            sb.append(" with queue size ");
            sb.append(a2);
            b2.c(str, sb.toString());
        }
    }

    public r(j jVar) {
        this.b = jVar;
        this.c = jVar.v();
        this.d = a(ParametersKeys.MAIN);
        this.e = a("timeout");
        this.f = a("back");
        this.g = a("advertising_info_collection");
        this.h = a("postbacks");
        this.i = a("caching_interstitial");
        this.j = a("caching_incentivized");
        this.k = a("caching_other");
        this.l = a("reward");
        this.m = a("mediation_main");
        this.n = a("mediation_timeout");
        this.o = a("mediation_background");
        this.p = a("mediation_postbacks");
        this.q = a("mediation_banner");
        this.r = a("mediation_interstitial");
        this.s = a("mediation_incentivized");
        this.t = a("mediation_reward");
        this.u = a("auxiliary_operations", ((Integer) jVar.a(d.cx)).intValue());
        this.v = a("caching_operations", ((Integer) jVar.a(d.cy)).intValue());
        this.w = a("shared_thread_pool", ((Integer) jVar.a(d.al)).intValue());
    }

    /* access modifiers changed from: private */
    public long a(a aVar) {
        long taskCount;
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
        if (aVar == a.MAIN) {
            taskCount = this.d.getTaskCount();
            scheduledThreadPoolExecutor = this.d;
        } else if (aVar == a.TIMEOUT) {
            taskCount = this.e.getTaskCount();
            scheduledThreadPoolExecutor = this.e;
        } else if (aVar == a.BACKGROUND) {
            taskCount = this.f.getTaskCount();
            scheduledThreadPoolExecutor = this.f;
        } else if (aVar == a.ADVERTISING_INFO_COLLECTION) {
            taskCount = this.g.getTaskCount();
            scheduledThreadPoolExecutor = this.g;
        } else if (aVar == a.POSTBACKS) {
            taskCount = this.h.getTaskCount();
            scheduledThreadPoolExecutor = this.h;
        } else if (aVar == a.CACHING_INTERSTITIAL) {
            taskCount = this.i.getTaskCount();
            scheduledThreadPoolExecutor = this.i;
        } else if (aVar == a.CACHING_INCENTIVIZED) {
            taskCount = this.j.getTaskCount();
            scheduledThreadPoolExecutor = this.j;
        } else if (aVar == a.CACHING_OTHER) {
            taskCount = this.k.getTaskCount();
            scheduledThreadPoolExecutor = this.k;
        } else if (aVar == a.REWARD) {
            taskCount = this.l.getTaskCount();
            scheduledThreadPoolExecutor = this.l;
        } else if (aVar == a.MEDIATION_MAIN) {
            taskCount = this.m.getTaskCount();
            scheduledThreadPoolExecutor = this.m;
        } else if (aVar == a.MEDIATION_TIMEOUT) {
            taskCount = this.n.getTaskCount();
            scheduledThreadPoolExecutor = this.n;
        } else if (aVar == a.MEDIATION_BACKGROUND) {
            taskCount = this.o.getTaskCount();
            scheduledThreadPoolExecutor = this.o;
        } else if (aVar == a.MEDIATION_POSTBACKS) {
            taskCount = this.p.getTaskCount();
            scheduledThreadPoolExecutor = this.p;
        } else if (aVar == a.MEDIATION_BANNER) {
            taskCount = this.q.getTaskCount();
            scheduledThreadPoolExecutor = this.q;
        } else if (aVar == a.MEDIATION_INTERSTITIAL) {
            taskCount = this.r.getTaskCount();
            scheduledThreadPoolExecutor = this.r;
        } else if (aVar == a.MEDIATION_INCENTIVIZED) {
            taskCount = this.s.getTaskCount();
            scheduledThreadPoolExecutor = this.s;
        } else if (aVar != a.MEDIATION_REWARD) {
            return 0;
        } else {
            taskCount = this.t.getTaskCount();
            scheduledThreadPoolExecutor = this.t;
        }
        return taskCount - scheduledThreadPoolExecutor.getCompletedTaskCount();
    }

    private ScheduledThreadPoolExecutor a(String str) {
        return a(str, 1);
    }

    private ScheduledThreadPoolExecutor a(String str, int i2) {
        return new ScheduledThreadPoolExecutor(i2, new b(str));
    }

    private void a(final Runnable runnable, long j2, final ScheduledExecutorService scheduledExecutorService, boolean z2) {
        if (j2 <= 0) {
            scheduledExecutorService.submit(runnable);
        } else if (z2) {
            com.applovin.impl.sdk.utils.d.a(j2, this.b, new Runnable() {
                public void run() {
                    scheduledExecutorService.execute(runnable);
                }
            });
        } else {
            scheduledExecutorService.schedule(runnable, j2, TimeUnit.MILLISECONDS);
        }
    }

    private boolean a(c cVar) {
        if (cVar.c.h()) {
            return false;
        }
        synchronized (this.y) {
            if (this.z) {
                return false;
            }
            this.x.add(cVar);
            return true;
        }
    }

    public void a(a aVar) {
        if (aVar != null) {
            long currentTimeMillis = System.currentTimeMillis();
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("Executing ");
                sb.append(aVar.f());
                sb.append(" immediately...");
                this.c.c("TaskManager", sb.toString());
                aVar.run();
                this.b.M().a(aVar.a(), System.currentTimeMillis() - currentTimeMillis);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(aVar.f());
                sb2.append(" finished executing...");
                this.c.c("TaskManager", sb2.toString());
            } catch (Throwable th) {
                this.c.b(aVar.f(), "Task failed execution", th);
                this.b.M().a(aVar.a(), true, System.currentTimeMillis() - currentTimeMillis);
            }
        } else {
            this.c.e("TaskManager", "Attempted to execute null task immediately");
        }
    }

    public void a(a aVar, a aVar2) {
        a(aVar, aVar2, 0);
    }

    public void a(a aVar, a aVar2, long j2) {
        a(aVar, aVar2, j2, false);
    }

    public void a(a aVar, a aVar2, long j2, boolean z2) {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
        r rVar;
        Runnable runnable;
        long j3;
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor2;
        a aVar3 = aVar2;
        long j4 = j2;
        if (aVar == 0) {
            throw new IllegalArgumentException("No task specified");
        } else if (j4 >= 0) {
            c cVar = new c(aVar, aVar2);
            if (!a(cVar)) {
                if (((Boolean) this.b.a(d.am)).booleanValue()) {
                    scheduledThreadPoolExecutor2 = this.w;
                    rVar = this;
                    runnable = aVar;
                    j3 = j2;
                } else {
                    long a2 = a(aVar2) + 1;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Scheduling ");
                    sb.append(aVar.f());
                    sb.append(" on ");
                    sb.append(aVar2);
                    sb.append(" queue in ");
                    sb.append(j4);
                    sb.append("ms with new queue size ");
                    sb.append(a2);
                    this.c.b("TaskManager", sb.toString());
                    if (aVar3 == a.MAIN) {
                        scheduledThreadPoolExecutor = this.d;
                    } else if (aVar3 == a.TIMEOUT) {
                        scheduledThreadPoolExecutor = this.e;
                    } else if (aVar3 == a.BACKGROUND) {
                        scheduledThreadPoolExecutor = this.f;
                    } else if (aVar3 == a.ADVERTISING_INFO_COLLECTION) {
                        scheduledThreadPoolExecutor = this.g;
                    } else if (aVar3 == a.POSTBACKS) {
                        scheduledThreadPoolExecutor = this.h;
                    } else if (aVar3 == a.CACHING_INTERSTITIAL) {
                        scheduledThreadPoolExecutor = this.i;
                    } else if (aVar3 == a.CACHING_INCENTIVIZED) {
                        scheduledThreadPoolExecutor = this.j;
                    } else if (aVar3 == a.CACHING_OTHER) {
                        scheduledThreadPoolExecutor = this.k;
                    } else if (aVar3 == a.REWARD) {
                        scheduledThreadPoolExecutor = this.l;
                    } else if (aVar3 == a.MEDIATION_MAIN) {
                        scheduledThreadPoolExecutor = this.m;
                    } else if (aVar3 == a.MEDIATION_TIMEOUT) {
                        scheduledThreadPoolExecutor = this.n;
                    } else if (aVar3 == a.MEDIATION_BACKGROUND) {
                        scheduledThreadPoolExecutor = this.o;
                    } else if (aVar3 == a.MEDIATION_POSTBACKS) {
                        scheduledThreadPoolExecutor = this.p;
                    } else if (aVar3 == a.MEDIATION_BANNER) {
                        scheduledThreadPoolExecutor = this.q;
                    } else if (aVar3 == a.MEDIATION_INTERSTITIAL) {
                        scheduledThreadPoolExecutor = this.r;
                    } else if (aVar3 == a.MEDIATION_INCENTIVIZED) {
                        scheduledThreadPoolExecutor = this.s;
                    } else if (aVar3 == a.MEDIATION_REWARD) {
                        scheduledThreadPoolExecutor = this.t;
                    } else {
                        return;
                    }
                    rVar = this;
                    runnable = cVar;
                    j3 = j2;
                    scheduledThreadPoolExecutor2 = scheduledThreadPoolExecutor;
                }
                rVar.a(runnable, j3, (ScheduledExecutorService) scheduledThreadPoolExecutor2, z2);
                return;
            }
            p pVar = this.c;
            String f2 = aVar.f();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Task ");
            sb2.append(aVar.f());
            sb2.append(" execution delayed until after init");
            pVar.c(f2, sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Invalid delay specified: ");
            sb3.append(j4);
            throw new IllegalArgumentException(sb3.toString());
        }
    }

    public boolean a() {
        return this.z;
    }

    public ScheduledExecutorService b() {
        return this.u;
    }

    public ScheduledExecutorService c() {
        return this.v;
    }

    public void d() {
        synchronized (this.y) {
            this.z = false;
        }
    }

    public void e() {
        synchronized (this.y) {
            this.z = true;
            for (c cVar : this.x) {
                a(cVar.c, cVar.d);
            }
            this.x.clear();
        }
    }
}
