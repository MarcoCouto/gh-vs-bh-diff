package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.a;
import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import org.json.JSONObject;

class u extends a {
    private final JSONObject a;
    private final JSONObject c;
    private final AppLovinAdLoadListener d;
    private final b e;

    u(JSONObject jSONObject, JSONObject jSONObject2, b bVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        super("TaskRenderAppLovinAd", jVar);
        this.a = jSONObject;
        this.c = jSONObject2;
        this.e = bVar;
        this.d = appLovinAdLoadListener;
    }

    public i a() {
        return i.u;
    }

    public void run() {
        a("Rendering ad...");
        a aVar = new a(this.a, this.c, this.e, this.b);
        boolean booleanValue = com.applovin.impl.sdk.utils.i.a(this.a, "gs_load_immediately", Boolean.valueOf(false), this.b).booleanValue();
        boolean booleanValue2 = com.applovin.impl.sdk.utils.i.a(this.a, "vs_load_immediately", Boolean.valueOf(true), this.b).booleanValue();
        d dVar = new d(aVar, this.b, this.d);
        dVar.a(booleanValue2);
        dVar.b(booleanValue);
        r.a aVar2 = r.a.CACHING_OTHER;
        if (((Boolean) this.b.a(d.bm)).booleanValue()) {
            if (aVar.getSize() == AppLovinAdSize.INTERSTITIAL && aVar.getType() == AppLovinAdType.REGULAR) {
                aVar2 = r.a.CACHING_INTERSTITIAL;
            } else if (aVar.getSize() == AppLovinAdSize.INTERSTITIAL && aVar.getType() == AppLovinAdType.INCENTIVIZED) {
                aVar2 = r.a.CACHING_INCENTIVIZED;
            }
        }
        this.b.K().a((a) dVar, aVar2);
    }
}
