package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.r.a;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class j extends a {
    /* access modifiers changed from: private */
    public final g a;
    /* access modifiers changed from: private */
    public final AppLovinPostbackListener c;
    private final a d;

    public j(g gVar, a aVar, com.applovin.impl.sdk.j jVar, AppLovinPostbackListener appLovinPostbackListener) {
        super("TaskDispatchPostback", jVar);
        if (gVar != null) {
            this.a = gVar;
            this.c = appLovinPostbackListener;
            this.d = aVar;
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    public i a() {
        return i.c;
    }

    public void run() {
        final String a2 = this.a.a();
        if (!n.b(a2)) {
            b("Requested URL is not valid; nothing to do...");
            if (this.c != null) {
                this.c.onPostbackFailure(a2, AppLovinErrorCodes.INVALID_URL);
            }
            return;
        }
        AnonymousClass1 r1 = new x<Object>(this.a, e()) {
            public void a(int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to dispatch postback. Error code: ");
                sb.append(i);
                sb.append(" URL: ");
                sb.append(a2);
                d(sb.toString());
                if (j.this.c != null) {
                    j.this.c.onPostbackFailure(a2, i);
                }
                if (j.this.a.o()) {
                    this.b.ac().a(j.this.a.p(), j.this.a.a(), i, null);
                }
            }

            public void a(Object obj, int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Successfully dispatched postback to URL: ");
                sb.append(a2);
                a(sb.toString());
                if (((Boolean) this.b.a(d.fb)).booleanValue()) {
                    if (obj instanceof JSONObject) {
                        JSONObject jSONObject = (JSONObject) obj;
                        Iterator it = this.b.b((d) d.aI).iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            if (j.this.a.a().startsWith((String) it.next())) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("Updating settings from: ");
                                sb2.append(j.this.a.a());
                                a(sb2.toString());
                                h.d(jSONObject, this.b);
                                h.c(jSONObject, this.b);
                                break;
                            }
                        }
                    }
                } else if (obj instanceof String) {
                    for (String startsWith : this.b.b((d) d.aI)) {
                        if (j.this.a.a().startsWith(startsWith)) {
                            String str = (String) obj;
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Updating settings from: ");
                                    sb3.append(j.this.a.a());
                                    a(sb3.toString());
                                    JSONObject jSONObject2 = new JSONObject(str);
                                    h.d(jSONObject2, this.b);
                                    h.c(jSONObject2, this.b);
                                    break;
                                } catch (JSONException unused) {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
                if (j.this.c != null) {
                    j.this.c.onPostbackSuccess(a2);
                }
                if (j.this.a.o()) {
                    this.b.ac().a(j.this.a.p(), j.this.a.a(), i, obj);
                }
            }
        };
        r1.a(this.d);
        e().K().a((a) r1);
    }
}
