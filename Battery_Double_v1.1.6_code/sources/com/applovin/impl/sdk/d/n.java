package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import java.util.Map;
import org.json.JSONObject;

public class n extends m {
    private final int a;
    private final AppLovinNativeAdLoadListener c;

    public n(String str, int i, j jVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super(d.b(str, jVar), null, "TaskFetchNextNativeAd", jVar);
        this.a = i;
        this.c = appLovinNativeAdLoadListener;
    }

    public i a() {
        return i.o;
    }

    /* access modifiers changed from: protected */
    public a a(JSONObject jSONObject) {
        return new v(jSONObject, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (this.c != null) {
            this.c.onNativeAdsFailedToLoad(i);
        }
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> b() {
        Map<String, String> b = super.b();
        b.put("slot_count", Integer.toString(this.a));
        return b;
    }

    /* access modifiers changed from: protected */
    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) this.b.a(com.applovin.impl.sdk.b.d.aL));
        sb.append("4.0/nad");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String i() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) this.b.a(com.applovin.impl.sdk.b.d.aM));
        sb.append("4.0/nad");
        return sb.toString();
    }
}
