package com.applovin.impl.sdk.d;

import android.content.Context;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;

public abstract class a implements Runnable {
    private final String a;
    /* access modifiers changed from: protected */
    public final j b;
    private final p c;
    private final Context d;
    private final boolean e;

    public a(String str, j jVar) {
        this(str, jVar, false);
    }

    public a(String str, j jVar, boolean z) {
        this.a = str;
        this.b = jVar;
        this.c = jVar.v();
        this.d = jVar.D();
        this.e = z;
    }

    public abstract i a();

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.c.b(this.a, str);
    }

    /* access modifiers changed from: protected */
    public void a(String str, Throwable th) {
        this.c.b(this.a, str, th);
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.c.c(this.a, str);
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        this.c.d(this.a, str);
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        this.c.e(this.a, str);
    }

    /* access modifiers changed from: protected */
    public j e() {
        return this.b;
    }

    public String f() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public Context g() {
        return this.d;
    }

    public boolean h() {
        return this.e;
    }
}
