package com.applovin.impl.sdk.d;

import com.applovin.impl.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.a.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinAdLoadListener;
import io.fabric.sdk.android.services.network.HttpRequest;

class aa extends a {
    /* access modifiers changed from: private */
    public c a;
    /* access modifiers changed from: private */
    public final AppLovinAdLoadListener c;

    aa(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        super("TaskResolveVastWrapper", jVar);
        this.c = appLovinAdLoadListener;
        this.a = cVar;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to resolve VAST wrapper due to error code ");
        sb.append(i);
        d(sb.toString());
        if (i == -103) {
            q.a(this.c, this.a.g(), i, this.b);
        } else {
            i.a(this.a, this.c, i == -102 ? d.TIMED_OUT : d.GENERAL_WRAPPER_ERROR, i, this.b);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.y;
    }

    public void run() {
        String a2 = i.a(this.a);
        if (n.b(a2)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Resolving VAST ad with depth ");
            sb.append(this.a.a());
            sb.append(" at ");
            sb.append(a2);
            a(sb.toString());
            try {
                this.b.K().a((a) new x<s>(b.a(this.b).a(a2).b(HttpRequest.METHOD_GET).a(s.a).a(((Integer) this.b.a(com.applovin.impl.sdk.b.d.eP)).intValue()).b(((Integer) this.b.a(com.applovin.impl.sdk.b.d.eQ)).intValue()).a(false).a(), this.b) {
                    public void a(int i) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unable to resolve VAST wrapper. Server returned ");
                        sb.append(i);
                        d(sb.toString());
                        aa.this.a(i);
                    }

                    public void a(s sVar, int i) {
                        this.b.K().a((a) t.a(sVar, aa.this.a, aa.this.c, aa.this.b));
                    }
                });
            } catch (Throwable th) {
                a("Unable to resolve VAST wrapper", th);
                a(-1);
                this.b.M().a(a());
            }
        } else {
            d("Resolving VAST failed. Could not find resolution URL");
            a(-1);
        }
    }
}
