package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class ae extends ab {
    protected ae(String str, j jVar) {
        super(str, jVar);
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        c c = c(jSONObject);
        if (c != null) {
            a(c);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r3 = java.util.Collections.emptyMap();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001b */
    private c c(JSONObject jSONObject) {
        String str;
        try {
            JSONObject a = h.a(jSONObject);
            h.d(a, this.b);
            h.c(jSONObject, this.b);
            Map map = i.a((JSONObject) a.get("params"));
            try {
                str = a.getString(IronSourceConstants.EVENTS_RESULT);
            } catch (Throwable unused) {
                str = "network_timeout";
            }
            return c.a(str, map);
        } catch (JSONException e) {
            a("Unable to parse API response", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(c cVar);

    /* access modifiers changed from: protected */
    public abstract boolean c();

    public void run() {
        a(i(), new a.c<JSONObject>() {
            public void a(int i) {
                if (!ae.this.c()) {
                    ae.this.a(i);
                }
            }

            public void a(JSONObject jSONObject, int i) {
                if (!ae.this.c()) {
                    ae.this.b(jSONObject);
                }
            }
        });
    }
}
