package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import org.json.JSONObject;

public class y extends z {
    private final f a;

    public y(f fVar, j jVar) {
        super("TaskReportAppLovinReward", jVar);
        this.a = fVar;
    }

    public i a() {
        return i.x;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        super.a(i);
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to report reward for ad: ");
        sb.append(this.a);
        sb.append(" - error code: ");
        sb.append(i);
        d(sb.toString());
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.a.getAdZone().a(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, "fire_percent", this.a.ai(), this.b);
        String clCode = this.a.getClCode();
        String str = "clcode";
        if (!n.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str, clCode, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/cr";
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append("Reported reward successfully for ad: ");
        sb.append(this.a);
        a(sb.toString());
    }

    /* access modifiers changed from: protected */
    public c c() {
        return this.a.aJ();
    }

    /* access modifiers changed from: protected */
    public void d() {
        StringBuilder sb = new StringBuilder();
        sb.append("No reward result was found for ad: ");
        sb.append(this.a);
        d(sb.toString());
    }
}
