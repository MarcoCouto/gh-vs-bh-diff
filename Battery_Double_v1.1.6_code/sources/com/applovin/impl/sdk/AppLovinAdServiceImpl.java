package com.applovin.impl.sdk;

import android.graphics.PointF;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.text.TextUtils;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.adview.AdViewControllerImpl;
import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.ad.c;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.d.l;
import com.applovin.impl.sdk.d.m;
import com.applovin.impl.sdk.d.o;
import com.applovin.impl.sdk.d.s;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinAdUpdateListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class AppLovinAdServiceImpl implements AppLovinAdService {
    public static String URI_LOAD_URL = "/adservice/load_url";
    public static String URI_NO_OP = "/adservice/no_op";
    public static String URI_TRACK_CLICK_IMMEDIATELY = "/adservice/track_click_now";
    /* access modifiers changed from: private */
    public final j a;
    private final p b;
    private final Handler c = new Handler(Looper.getMainLooper());
    private final Map<d, b> d;
    private final Object e = new Object();

    private class a implements AppLovinAdLoadListener {
        private final b b;

        private a(b bVar) {
            this.b = bVar;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            HashSet<AppLovinAdLoadListener> hashSet;
            d adZone = ((AppLovinAdBase) appLovinAd).getAdZone();
            if (!(appLovinAd instanceof g)) {
                AppLovinAdServiceImpl.this.a.T().adReceived(appLovinAd);
                appLovinAd = new g(adZone, AppLovinAdServiceImpl.this.a);
            }
            synchronized (this.b.a) {
                hashSet = new HashSet<>(this.b.c);
                this.b.c.clear();
                this.b.b = false;
            }
            for (AppLovinAdLoadListener a2 : hashSet) {
                AppLovinAdServiceImpl.this.a(appLovinAd, a2);
            }
        }

        public void failedToReceiveAd(int i) {
            HashSet<AppLovinAdLoadListener> hashSet;
            synchronized (this.b.a) {
                hashSet = new HashSet<>(this.b.c);
                this.b.c.clear();
                this.b.b = false;
            }
            for (AppLovinAdLoadListener a2 : hashSet) {
                AppLovinAdServiceImpl.this.a(i, a2);
            }
        }
    }

    private static class b {
        final Object a;
        boolean b;
        final Collection<AppLovinAdLoadListener> c;

        private b() {
            this.a = new Object();
            this.c = new HashSet();
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("AdLoadState{, isWaitingForAd=");
            sb.append(this.b);
            sb.append(", pendingAdListeners=");
            sb.append(this.c);
            sb.append('}');
            return sb.toString();
        }
    }

    AppLovinAdServiceImpl(j jVar) {
        this.a = jVar;
        this.b = jVar.v();
        this.d = new HashMap(5);
        this.d.put(d.c(jVar), new b());
        this.d.put(d.d(jVar), new b());
        this.d.put(d.e(jVar), new b());
        this.d.put(d.f(jVar), new b());
        this.d.put(d.g(jVar), new b());
    }

    private b a(d dVar) {
        b bVar;
        synchronized (this.e) {
            bVar = (b) this.d.get(dVar);
            if (bVar == null) {
                bVar = new b();
                this.d.put(dVar, bVar);
            }
        }
        return bVar;
    }

    private String a(String str, long j, int i, String str2, boolean z) {
        try {
            if (!n.b(str)) {
                return null;
            }
            if (i < 0 || i > 100) {
                i = 0;
            }
            return Uri.parse(str).buildUpon().appendQueryParameter("et_s", Long.toString(j)).appendQueryParameter(NativeAdImpl.QUERY_PARAM_VIDEO_PERCENT_VIEWED, Integer.toString(i)).appendQueryParameter("vid_ts", str2).appendQueryParameter("uvs", Boolean.toString(z)).build().toString();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unknown error parsing the video end url: ");
            sb.append(str);
            this.b.b("AppLovinAdService", sb.toString(), th);
            return null;
        }
    }

    private String a(String str, long j, long j2, boolean z, int i) {
        if (!n.b(str)) {
            return null;
        }
        Builder appendQueryParameter = Uri.parse(str).buildUpon().appendQueryParameter("et_ms", Long.toString(j)).appendQueryParameter("vs_ms", Long.toString(j2));
        if (i != e.a) {
            appendQueryParameter.appendQueryParameter("musw_ch", Boolean.toString(z));
            appendQueryParameter.appendQueryParameter("musw_st", Boolean.toString(e.a(i)));
        }
        return appendQueryParameter.build().toString();
    }

    /* access modifiers changed from: private */
    public void a(final int i, final AppLovinAdLoadListener appLovinAdLoadListener) {
        this.c.post(new Runnable() {
            public void run() {
                try {
                    appLovinAdLoadListener.failedToReceiveAd(i);
                } catch (Throwable th) {
                    p.c("AppLovinAdService", "Unable to notify listener about ad load failure", th);
                }
            }
        });
    }

    private void a(Uri uri, f fVar, AppLovinAdView appLovinAdView, AdViewControllerImpl adViewControllerImpl) {
        if (appLovinAdView != null) {
            if (q.a(appLovinAdView.getContext(), uri, this.a)) {
                j.c(adViewControllerImpl.getAdViewEventListener(), (AppLovinAd) fVar, appLovinAdView);
            }
            adViewControllerImpl.dismissInterstitialIfRequired();
            return;
        }
        this.b.e("AppLovinAdService", "Unable to launch click - adView has been prematurely destroyed");
    }

    private void a(d dVar, a aVar) {
        AppLovinAd appLovinAd = (AppLovinAd) this.a.T().e(dVar);
        if (appLovinAd != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Using pre-loaded ad: ");
            sb.append(appLovinAd);
            sb.append(" for ");
            sb.append(dVar);
            this.b.b("AppLovinAdService", sb.toString());
            aVar.adReceived(appLovinAd);
            if (dVar.i() || dVar.g() > 0) {
                this.a.T().i(dVar);
                return;
            }
            return;
        }
        a((com.applovin.impl.sdk.d.a) new m(dVar, aVar, this.a), (AppLovinAdLoadListener) aVar);
    }

    private void a(d dVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        p pVar;
        String str;
        String str2;
        if (dVar == null) {
            throw new IllegalArgumentException("No zone specified");
        } else if (appLovinAdLoadListener != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Loading next ad of zone {");
            sb.append(dVar);
            sb.append("}...");
            this.a.v().b("AppLovinAdService", sb.toString());
            b a2 = a(dVar);
            synchronized (a2.a) {
                a2.c.add(appLovinAdLoadListener);
                if (!a2.b) {
                    this.b.b("AppLovinAdService", "Loading next ad...");
                    a2.b = true;
                    a aVar = new a(a2);
                    if (!dVar.h()) {
                        this.b.b("AppLovinAdService", "Task merge not necessary.");
                    } else if (this.a.T().a(dVar, (AppLovinAdLoadListener) aVar)) {
                        pVar = this.b;
                        str = "AppLovinAdService";
                        str2 = "Attaching load listener to initial preload task...";
                    } else {
                        this.b.b("AppLovinAdService", "Skipped attach of initial preload callback.");
                    }
                    a(dVar, aVar);
                } else {
                    pVar = this.b;
                    str = "AppLovinAdService";
                    str2 = "Already waiting on an ad load...";
                }
                pVar.b(str, str2);
            }
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    private void a(com.applovin.impl.sdk.c.a aVar) {
        if (n.b(aVar.a())) {
            this.a.N().a(com.applovin.impl.sdk.network.f.l().a(q.b(aVar.a())).b(n.b(aVar.b()) ? q.b(aVar.b()) : null).a(false).a());
            return;
        }
        this.b.d("AppLovinAdService", "Requested a postback dispatch for a null URL; nothing to do...");
    }

    private void a(com.applovin.impl.sdk.d.a aVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (!this.a.d()) {
            p.i("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
        }
        this.a.a();
        this.a.K().a(aVar, com.applovin.impl.sdk.d.r.a.MAIN);
    }

    /* access modifiers changed from: private */
    public void a(final AppLovinAd appLovinAd, final AppLovinAdLoadListener appLovinAdLoadListener) {
        this.c.post(new Runnable() {
            public void run() {
                try {
                    appLovinAdLoadListener.adReceived(appLovinAd);
                } catch (Throwable th) {
                    p.c("AppLovinAdService", "Unable to notify listener about a newly loaded ad", th);
                }
            }
        });
    }

    private void a(List<com.applovin.impl.sdk.c.a> list) {
        if (list != null && !list.isEmpty()) {
            for (com.applovin.impl.sdk.c.a a2 : list) {
                a(a2);
            }
        }
    }

    public void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener) {
    }

    public void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize) {
    }

    public AppLovinAd dequeueAd(d dVar) {
        AppLovinAd appLovinAd = (AppLovinAd) this.a.T().d(dVar);
        StringBuilder sb = new StringBuilder();
        sb.append("Dequeued ad: ");
        sb.append(appLovinAd);
        sb.append(" for zone: ");
        sb.append(dVar);
        sb.append("...");
        this.b.b("AppLovinAdService", sb.toString());
        return appLovinAd;
    }

    public String getBidToken() {
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        String a2 = this.a.O().a();
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        return a2;
    }

    public boolean hasPreloadedAd(AppLovinAdSize appLovinAdSize) {
        return this.a.T().g(d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.a));
    }

    public boolean hasPreloadedAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            p.j("AppLovinAdService", "Unable to check if ad is preloaded - invalid zone id");
            return false;
        }
        return this.a.T().g(d.a(str, this.a));
    }

    public void loadNextAd(AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        a(d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.a), appLovinAdLoadListener);
    }

    public void loadNextAd(String str, AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("Loading next ad of zone {");
        sb.append(str);
        sb.append("} with size ");
        sb.append(appLovinAdSize);
        this.b.b("AppLovinAdService", sb.toString());
        a(d.a(appLovinAdSize, AppLovinAdType.REGULAR, str, this.a), appLovinAdLoadListener);
    }

    /* JADX WARNING: type inference failed for: r10v17, types: [com.applovin.impl.sdk.d.a] */
    /* JADX WARNING: type inference failed for: r10v19, types: [com.applovin.impl.sdk.d.o] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public void loadNextAdForAdToken(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        s sVar;
        String trim = str != null ? str.trim() : null;
        if (TextUtils.isEmpty(trim)) {
            p.j("AppLovinAdService", "Invalid ad token specified");
            a(-8, appLovinAdLoadListener);
            return;
        }
        c cVar = new c(trim, this.a);
        if (cVar.b() == com.applovin.impl.sdk.ad.c.a.REGULAR) {
            StringBuilder sb = new StringBuilder();
            sb.append("Loading next ad for token: ");
            sb.append(cVar);
            this.b.b("AppLovinAdService", sb.toString());
            sVar = new o(cVar, appLovinAdLoadListener, this.a);
        } else {
            if (cVar.b() == com.applovin.impl.sdk.ad.c.a.AD_RESPONSE_JSON) {
                JSONObject d2 = cVar.d();
                if (d2 != null) {
                    h.f(d2, this.a);
                    h.d(d2, this.a);
                    h.c(d2, this.a);
                    if (i.b(d2, "ads", new JSONArray(), this.a).length() > 0) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Rendering ad for token: ");
                        sb2.append(cVar);
                        this.b.b("AppLovinAdService", sb2.toString());
                        s sVar2 = new s(d2, q.a(d2, this.a), com.applovin.impl.sdk.ad.b.DECODED_AD_TOKEN_JSON, appLovinAdLoadListener, this.a);
                        sVar = sVar2;
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("No ad returned from the server for token: ");
                        sb3.append(cVar);
                        this.b.e("AppLovinAdService", sb3.toString());
                        appLovinAdLoadListener.failedToReceiveAd(204);
                    }
                } else {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("Unable to retrieve ad response JSON from token: ");
                    sb4.append(cVar);
                    this.b.e("AppLovinAdService", sb4.toString());
                }
            } else {
                StringBuilder sb5 = new StringBuilder();
                sb5.append("Invalid ad token specified: ");
                sb5.append(cVar);
                p.j("AppLovinAdService", sb5.toString());
            }
            appLovinAdLoadListener.failedToReceiveAd(-8);
        }
        a((com.applovin.impl.sdk.d.a) sVar, appLovinAdLoadListener);
    }

    public void loadNextAdForZoneId(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (!TextUtils.isEmpty(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Loading next ad of zone {");
            sb.append(str);
            sb.append("}");
            this.b.b("AppLovinAdService", sb.toString());
            a(d.a(str, this.a), appLovinAdLoadListener);
            return;
        }
        throw new IllegalArgumentException("No zone id specified");
    }

    public void loadNextAdForZoneIds(List<String> list, AppLovinAdLoadListener appLovinAdLoadListener) {
        List a2 = e.a(list);
        if (a2 == null || a2.isEmpty()) {
            p.j("AppLovinAdService", "No zones were provided");
            a(-7, appLovinAdLoadListener);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Loading next ad for zones: ");
        sb.append(a2);
        this.b.b("AppLovinAdService", sb.toString());
        a((com.applovin.impl.sdk.d.a) new l(a2, appLovinAdLoadListener, this.a), appLovinAdLoadListener);
    }

    public void loadNextIncentivizedAd(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("Loading next incentivized ad of zone {");
        sb.append(str);
        sb.append("}");
        this.b.b("AppLovinAdService", sb.toString());
        a(d.c(str, this.a), appLovinAdLoadListener);
    }

    public void preloadAd(AppLovinAdSize appLovinAdSize) {
        this.a.a();
        this.a.T().i(d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.a));
    }

    public void preloadAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            p.j("AppLovinAdService", "Unable to preload ad for invalid zone identifier");
            return;
        }
        d a2 = d.a(str, this.a);
        this.a.T().h(a2);
        this.a.T().i(a2);
    }

    public void preloadAds(d dVar) {
        this.a.T().h(dVar);
        int g = dVar.g();
        if (g == 0 && this.a.T().b(dVar)) {
            g = 1;
        }
        this.a.T().b(dVar, g);
    }

    public void removeAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize) {
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinAdService{adLoadStates=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }

    public void trackAndLaunchClick(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, AdViewControllerImpl adViewControllerImpl, Uri uri, PointF pointF) {
        if (appLovinAd == null) {
            this.b.e("AppLovinAdService", "Unable to track ad view click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking click on an ad...");
        f fVar = (f) appLovinAd;
        a(fVar.a(pointF));
        a(uri, fVar, appLovinAdView, adViewControllerImpl);
    }

    public void trackAndLaunchVideoClick(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, Uri uri, PointF pointF) {
        if (appLovinAd == null) {
            this.b.e("AppLovinAdService", "Unable to track video click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking VIDEO click on an ad...");
        a(((f) appLovinAd).b(pointF));
        q.a(appLovinAdView.getContext(), uri, this.a);
    }

    public void trackAppKilled(f fVar) {
        if (fVar == null) {
            this.b.e("AppLovinAdService", "Unable to track app killed. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking app killed during ad...");
        List<com.applovin.impl.sdk.c.a> at = fVar.at();
        if (at == null || at.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to track app killed during AD #");
            sb.append(fVar.getAdIdNumber());
            sb.append(". Missing app killed tracking URL.");
            this.b.d("AppLovinAdService", sb.toString());
        } else {
            for (com.applovin.impl.sdk.c.a aVar : at) {
                a(new com.applovin.impl.sdk.c.a(aVar.a(), aVar.b()));
            }
        }
    }

    public void trackFullScreenAdClosed(f fVar, long j, long j2, boolean z, int i) {
        if (fVar == null) {
            this.b.e("AppLovinAdService", "Unable to track ad closed. No ad specified.");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking ad closed...");
        List<com.applovin.impl.sdk.c.a> as = fVar.as();
        if (as == null || as.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to track ad closed for AD #");
            sb.append(fVar.getAdIdNumber());
            sb.append(". Missing ad close tracking URL.");
            sb.append(fVar.getAdIdNumber());
            this.b.d("AppLovinAdService", sb.toString());
        } else {
            for (com.applovin.impl.sdk.c.a aVar : as) {
                long j3 = j;
                long j4 = j2;
                boolean z2 = z;
                int i2 = i;
                String a2 = a(aVar.a(), j3, j4, z2, i2);
                String a3 = a(aVar.b(), j3, j4, z2, i2);
                if (n.b(a2)) {
                    a(new com.applovin.impl.sdk.c.a(a2, a3));
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to parse url: ");
                    sb2.append(aVar.a());
                    this.b.e("AppLovinAdService", sb2.toString());
                }
            }
        }
    }

    public void trackImpression(f fVar) {
        if (fVar == null) {
            this.b.e("AppLovinAdService", "Unable to track impression click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking impression on ad...");
        a(fVar.au());
    }

    public void trackVideoEnd(f fVar, long j, int i, boolean z) {
        if (fVar == null) {
            this.b.e("AppLovinAdService", "Unable to track video end. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking video end on ad...");
        List<com.applovin.impl.sdk.c.a> ar = fVar.ar();
        if (ar == null || ar.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to submit persistent postback for AD #");
            sb.append(fVar.getAdIdNumber());
            sb.append(". Missing video end tracking URL.");
            this.b.d("AppLovinAdService", sb.toString());
        } else {
            String l = Long.toString(System.currentTimeMillis());
            for (com.applovin.impl.sdk.c.a aVar : ar) {
                if (n.b(aVar.a())) {
                    long j2 = j;
                    int i2 = i;
                    String str = l;
                    boolean z2 = z;
                    String a2 = a(aVar.a(), j2, i2, str, z2);
                    String a3 = a(aVar.b(), j2, i2, str, z2);
                    if (a2 != null) {
                        a(new com.applovin.impl.sdk.c.a(a2, a3));
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Failed to parse url: ");
                        sb2.append(aVar.a());
                        this.b.e("AppLovinAdService", sb2.toString());
                    }
                } else {
                    this.b.d("AppLovinAdService", "Requested a postback dispatch for an empty video end URL; nothing to do...");
                }
            }
        }
    }
}
