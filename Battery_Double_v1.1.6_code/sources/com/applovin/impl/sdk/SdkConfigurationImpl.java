package com.applovin.impl.sdk;

import com.applovin.impl.sdk.b.d;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkConfiguration.ConsentDialogState;

public class SdkConfigurationImpl implements AppLovinSdkConfiguration {
    private final j a;

    public SdkConfigurationImpl(j jVar) {
        this.a = jVar;
    }

    public ConsentDialogState getConsentDialogState() {
        String str = (String) this.a.a(d.fh);
        return "applies".equalsIgnoreCase(str) ? ConsentDialogState.APPLIES : "does_not_apply".equalsIgnoreCase(str) ? ConsentDialogState.DOES_NOT_APPLY : ConsentDialogState.UNKNOWN;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinSdkConfiguration{consentDialogState=");
        sb.append(getConsentDialogState());
        sb.append('}');
        return sb.toString();
    }
}
