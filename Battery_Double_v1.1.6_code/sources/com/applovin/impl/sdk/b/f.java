package com.applovin.impl.sdk.b;

import java.util.HashSet;

public class f<T> {
    public static final f<String> A = new f<>("com.applovin.sdk.mediation_provider", String.class);
    public static final f<String> B = new f<>("com.applovin.sdk.mediation.test_mode_network", String.class);
    public static final f<String> a = new f<>("com.applovin.sdk.impl.isFirstRun", String.class);
    public static final f<Boolean> b = new f<>("com.applovin.sdk.launched_before", Boolean.class);
    public static final f<String> c = new f<>("com.applovin.sdk.user_id", String.class);
    public static final f<String> d = new f<>("com.applovin.sdk.compass_id", String.class);
    public static final f<String> e = new f<>("com.applovin.sdk.compass_random_token", String.class);
    public static final f<String> f = new f<>("com.applovin.sdk.applovin_random_token", String.class);
    public static final f<String> g = new f<>("com.applovin.sdk.device_test_group", String.class);
    public static final f<String> h = new f<>("com.applovin.sdk.local_test_group", String.class);
    public static final f<String> i = new f<>("com.applovin.sdk.variables", String.class);
    public static final f<Boolean> j = new f<>("com.applovin.sdk.compliance.has_user_consent", Boolean.class);
    public static final f<Boolean> k = new f<>("com.applovin.sdk.compliance.is_age_restricted_user", Boolean.class);
    public static final f<Boolean> l = new f<>("com.applovin.sdk.compliance.is_do_not_sell", Boolean.class);
    public static final f<HashSet> m = new f<>("com.applovin.sdk.impl.postbackQueue.key", HashSet.class);
    public static final f<String> n = new f<>("com.applovin.sdk.stats", String.class);
    public static final f<String> o = new f<>("com.applovin.sdk.errors", String.class);
    public static final f<HashSet> p = new f<>("com.applovin.sdk.task.stats", HashSet.class);
    public static final f<String> q = new f<>("com.applovin.sdk.network_response_code_mapping", String.class);
    public static final f<String> r = new f<>("com.applovin.sdk.event_tracking.super_properties", String.class);
    public static final f<String> s = new f<>("com.applovin.sdk.request_tracker.counter", String.class);
    public static final f<String> t = new f<>("com.applovin.sdk.zones", String.class);
    public static final f<HashSet> u = new f<>("com.applovin.sdk.ad.stats", HashSet.class);
    public static final f<Integer> v = new f<>("com.applovin.sdk.last_video_position", Integer.class);
    public static final f<Boolean> w = new f<>("com.applovin.sdk.should_resume_video", Boolean.class);
    public static final f<String> x = new f<>("com.applovin.sdk.mediation.signal_providers", String.class);
    public static final f<String> y = new f<>("com.applovin.sdk.mediation.auto_init_adapters", String.class);
    public static final f<String> z = new f<>("com.applovin.sdk.persisted_data", String.class);
    private final String C;
    private final Class<T> D;

    public f(String str, Class<T> cls) {
        this.C = str;
        this.D = cls;
    }

    public String a() {
        return this.C;
    }

    public Class<T> b() {
        return this.D;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Key{name='");
        sb.append(this.C);
        sb.append('\'');
        sb.append(", type=");
        sb.append(this.D);
        sb.append('}');
        return sb.toString();
    }
}
