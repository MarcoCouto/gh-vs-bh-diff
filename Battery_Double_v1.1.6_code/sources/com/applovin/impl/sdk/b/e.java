package com.applovin.impl.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkSettings;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
    protected final j a;
    protected final p b;
    protected final Context c;
    protected final SharedPreferences d;
    private final Map<String, Object> e = new HashMap();
    private final Object f = new Object();
    private Map<String, Object> g;
    private final b h;

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x003c */
    public e(j jVar) {
        this.a = jVar;
        this.b = jVar.v();
        this.c = jVar.D();
        this.d = this.c.getSharedPreferences("com.applovin.sdk.1", 0);
        Class.forName(d.class.getName());
        Class.forName(c.class.getName());
        try {
            Field a2 = q.a(jVar.l().getClass(), "localSettings");
            a2.setAccessible(true);
            this.g = (HashMap) a2.get(jVar.l());
        } catch (Throwable unused) {
        }
        this.h = new b(this, jVar);
    }

    private static Object a(String str, JSONObject jSONObject, Object obj) throws JSONException {
        if (obj instanceof Boolean) {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        }
        if (obj instanceof Float) {
            return Float.valueOf((float) jSONObject.getDouble(str));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        if (obj instanceof Long) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        if (obj instanceof String) {
            return jSONObject.getString(str);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SDK Error: unknown value type: ");
        sb.append(obj.getClass());
        throw new RuntimeException(sb.toString());
    }

    private <T> T c(d<T> dVar) {
        try {
            return dVar.a(this.g.get(dVar.a()));
        } catch (Throwable unused) {
            return null;
        }
    }

    private String f() {
        StringBuilder sb = new StringBuilder();
        sb.append("com.applovin.sdk.");
        sb.append(q.a(this.a.t()));
        sb.append(".");
        return sb.toString();
    }

    public <T> d<T> a(String str, d<T> dVar) {
        synchronized (this.f) {
            for (d<T> dVar2 : d.c()) {
                if (dVar2.a().equals(str)) {
                    return dVar2;
                }
            }
            return dVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        return r4;
     */
    public <T> T a(d<T> dVar) {
        if (dVar != null) {
            synchronized (this.f) {
                T c2 = c(dVar);
                if (c2 != null) {
                    return c2;
                }
                Object obj = this.e.get(dVar.a());
                if (obj == null) {
                    Object a2 = this.h.a(dVar);
                    T a3 = a2 != null ? dVar.a(a2) : dVar.b();
                } else {
                    T a4 = dVar.a(obj);
                    return a4;
                }
            }
        } else {
            throw new IllegalArgumentException("No setting type specified");
        }
    }

    public void a() {
        this.h.a();
    }

    public <T> void a(d<?> dVar, Object obj) {
        if (dVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        } else if (obj != null) {
            synchronized (this.f) {
                this.e.put(dVar.a(), obj);
            }
        } else {
            throw new IllegalArgumentException("No new value specified");
        }
    }

    public void a(AppLovinSdkSettings appLovinSdkSettings) {
        boolean z;
        boolean z2;
        if (appLovinSdkSettings != null) {
            synchronized (this.f) {
                if (((Boolean) this.a.a(d.Z)).booleanValue()) {
                    this.e.put(d.Z.a(), Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled()));
                }
                if (((Boolean) this.a.a(d.bD)).booleanValue()) {
                    String autoPreloadSizes = appLovinSdkSettings.getAutoPreloadSizes();
                    if (!n.b(autoPreloadSizes)) {
                        autoPreloadSizes = "NONE";
                    }
                    if (autoPreloadSizes.equals("NONE")) {
                        this.e.put(d.bd.a(), "");
                    } else {
                        this.e.put(d.bd.a(), autoPreloadSizes);
                    }
                }
                if (((Boolean) this.a.a(d.bE)).booleanValue()) {
                    String autoPreloadTypes = appLovinSdkSettings.getAutoPreloadTypes();
                    if (!n.b(autoPreloadTypes)) {
                        autoPreloadTypes = "NONE";
                    }
                    boolean z3 = false;
                    if (!"NONE".equals(autoPreloadTypes)) {
                        z2 = false;
                        z = false;
                        for (String str : com.applovin.impl.sdk.utils.e.a(autoPreloadTypes)) {
                            if (str.equals(AppLovinAdType.REGULAR.getLabel())) {
                                z3 = true;
                            } else {
                                if (!str.equals(AppLovinAdType.INCENTIVIZED.getLabel()) && !str.contains("INCENT")) {
                                    if (!str.contains("REWARD")) {
                                        if (str.equals(AppLovinAdType.NATIVE.getLabel())) {
                                            z = true;
                                        }
                                    }
                                }
                                z2 = true;
                            }
                        }
                    } else {
                        z2 = false;
                        z = false;
                    }
                    if (!z3) {
                        this.e.put(d.bd.a(), "");
                    }
                    this.e.put(d.be.a(), Boolean.valueOf(z2));
                    this.e.put(d.bf.a(), Boolean.valueOf(z));
                }
            }
        }
    }

    public void a(JSONObject jSONObject) {
        p pVar;
        String str;
        String str2;
        synchronized (this.f) {
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                if (str3 != null && str3.length() > 0) {
                    try {
                        d<Long> a2 = a(str3, null);
                        if (a2 != null) {
                            this.e.put(a2.a(), a(str3, jSONObject, a2.b()));
                            if (a2 == d.eT) {
                                this.e.put(d.eU.a(), Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    } catch (JSONException e2) {
                        th = e2;
                        pVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to parse JSON settingsValues array";
                        pVar.b(str, str2, th);
                    } catch (Throwable th) {
                        th = th;
                        pVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to convert setting object ";
                        pVar.b(str, str2, th);
                    }
                }
            }
        }
    }

    public List<String> b(d<String> dVar) {
        return com.applovin.impl.sdk.utils.e.a((String) a(dVar));
    }

    public void b() {
        if (this.c != null) {
            String f2 = f();
            synchronized (this.f) {
                Editor edit = this.d.edit();
                for (d dVar : d.c()) {
                    Object obj = this.e.get(dVar.a());
                    if (obj != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(f2);
                        sb.append(dVar.a());
                        this.a.a(sb.toString(), obj, edit);
                    }
                }
                edit.apply();
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public void c() {
        if (this.c != null) {
            String f2 = f();
            synchronized (this.f) {
                for (d dVar : d.c()) {
                    try {
                        StringBuilder sb = new StringBuilder();
                        sb.append(f2);
                        sb.append(dVar.a());
                        Object a2 = this.a.a(sb.toString(), null, dVar.b().getClass(), this.d);
                        if (a2 != null) {
                            this.e.put(dVar.a(), a2);
                        }
                    } catch (Exception e2) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unable to load \"");
                        sb2.append(dVar.a());
                        sb2.append("\"");
                        this.b.b("SettingsManager", sb2.toString(), e2);
                    }
                }
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public void d() {
        synchronized (this.f) {
            this.e.clear();
        }
        this.a.a(this.d);
    }

    public boolean e() {
        return this.a.l().isVerboseLoggingEnabled() || ((Boolean) a(d.Z)).booleanValue();
    }
}
