package com.applovin.impl.sdk.b;

import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.q;
import java.util.ArrayList;
import java.util.List;

public class b {
    private final j a;
    private final e b;
    private final List<d> c = new ArrayList(25);
    private final List<Object> d = new ArrayList(25);
    private final Object e = new Object();

    public b(e eVar, j jVar) {
        this.a = jVar;
        this.b = eVar;
        q.a(f.h, 25, jVar);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
        a(d.U, null);
    }

    private void a(d dVar, Object obj) {
        this.c.add(dVar);
        this.d.add(obj);
    }

    public Object a(d dVar) {
        synchronized (this.e) {
            int indexOf = this.c.indexOf(dVar);
            if (indexOf == -1) {
                return null;
            }
            Object obj = this.d.get(indexOf);
            return obj;
        }
    }

    public void a() {
        List b2 = this.b.b(d.V);
        int intValue = Integer.valueOf(q.a(f.h, this.a)).intValue();
        int i = 0;
        while (i < 25) {
            d dVar = (d) this.c.get(i);
            int i2 = i + 1;
            if (i2 != intValue || b2.contains(dVar.a())) {
                StringBuilder sb = new StringBuilder();
                sb.append("Disabling local setting: ");
                sb.append(dVar.a());
                this.a.v().b("LocalSettingsProvider", sb.toString());
                this.c.set(i, d.U);
            }
            i = i2;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Enabled local settings: ");
        sb2.append(this.c);
        this.a.v().b("LocalSettingsProvider", sb2.toString());
    }
}
