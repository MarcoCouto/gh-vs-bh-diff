package com.applovin.impl.sdk.b;

import android.net.Uri;
import com.applovin.impl.a.j.a;
import com.applovin.impl.adview.AppLovinTouchToClickListener.ClickRecognitionState;
import com.applovin.sdk.AppLovinAdSize;
import com.appodeal.ads.utils.LogConstants;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.tapjoy.TJAdUnitConstants;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class d<T> implements Comparable {
    public static final d<Boolean> U = a("empty", Boolean.valueOf(true));
    public static final d<String> V = a("dls", "");
    public static final d<Boolean> W = a("is_disabled", Boolean.valueOf(false));
    public static final d<String> X = a("device_id", "");
    public static final d<String> Y = a("device_token", "");
    public static final d<Boolean> Z = a("is_verbose_logging", Boolean.valueOf(false));
    private static final List<?> a = Arrays.asList(new Class[]{Boolean.class, Float.class, Integer.class, Long.class, String.class});
    public static final d<Long> aA = a("alert_consent_reschedule_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final d<String> aB = a("text_alert_consent_title", "Make this App Better and Stay Free!");
    public static final d<String> aC = a("text_alert_consent_body", "If you don't give us consent to use your data, you will be making our ability to support this app harder, which may negatively affect the user experience.");
    public static final d<String> aD = a("text_alert_consent_yes_option", "I Agree");
    public static final d<String> aE = a("text_alert_consent_no_option", LogConstants.EVENT_CANCEL);
    public static final d<Long> aF = a("ttc_max_click_duration_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final d<Integer> aG = a("ttc_max_click_distance_dp", Integer.valueOf(10));
    public static final d<Integer> aH = a("ttc_acrs", Integer.valueOf(ClickRecognitionState.DISABLED.ordinal()));
    public static final d<String> aI = a("whitelisted_postback_endpoints", "https://prod-a.applovin.com,https://rt.applovin.com/4.0/pix, https://rt.applvn.com/4.0/pix,https://ms.applovin.com/,https://ms.applvn.com/");
    public static final d<String> aJ = a("fetch_settings_endpoint", "https://ms.applovin.com/");
    public static final d<String> aK = a("fetch_settings_backup_endpoint", "https://ms.applvn.com/");
    public static final d<String> aL = a("adserver_endpoint", "https://a.applovin.com/");
    public static final d<String> aM = a("adserver_backup_endpoint", "https://a.applvn.com/");
    public static final d<String> aN = a("api_endpoint", "https://d.applovin.com/");
    public static final d<String> aO = a("api_backup_endpoint", "https://d.applvn.com/");
    public static final d<String> aP = a("event_tracking_endpoint_v2", "https://rt.applovin.com/");
    public static final d<String> aQ = a("event_tracking_backup_endpoint_v2", "https://rt.applvn.com/");
    public static final d<String> aR = a("fetch_variables_endpoint", "https://ms.applovin.com/");
    public static final d<String> aS = a("fetch_variables_backup_endpoint", "https://ms.applvn.com/");
    public static final d<String> aT = a("token_type_prefixes_r", "4!");
    public static final d<String> aU = a("token_type_prefixes_arj", "json_v3!");
    public static final d<String> aV = a("top_level_events", "landing,paused,resumed,checkout,iap");
    public static final d<Boolean> aW = a("events_enabled", Boolean.valueOf(true));
    public static final d<String> aX;
    public static final d<Boolean> aY = a("persist_super_properties", Boolean.valueOf(true));
    public static final d<Integer> aZ = a("super_property_string_max_length", Integer.valueOf(1024));
    public static final d<String> aa = a("sc", "");
    public static final d<String> ab = a("sc2", "");
    public static final d<String> ac = a("server_installed_at", "");
    public static final d<Boolean> ad = a("trn", Boolean.valueOf(false));
    public static final d<Boolean> ae = a("honor_publisher_settings", Boolean.valueOf(true));
    public static final d<Boolean> af = a("track_network_response_codes", Boolean.valueOf(false));
    public static final d<Boolean> ag = a("submit_network_response_codes", Boolean.valueOf(false));
    public static final d<Boolean> ah = a("clear_network_response_codes_on_request", Boolean.valueOf(true));
    public static final d<Boolean> ai = a("clear_completion_callback_on_failure", Boolean.valueOf(false));
    public static final d<Long> aj = a("sicd_ms", Long.valueOf(0));
    public static final d<Integer> ak = a("logcat_max_line_size", Integer.valueOf(1000));
    public static final d<Integer> al = a("stps", Integer.valueOf(32));
    public static final d<Boolean> am = a("ustp", Boolean.valueOf(false));
    public static final d<Boolean> an = a("publisher_can_show_consent_dialog", Boolean.valueOf(true));
    public static final d<String> ao = a("consent_dialog_url", "https://assets.applovin.com/gdpr/flow_v1/gdpr-flow-1.html");
    public static final d<Boolean> ap = a("consent_dialog_immersive_mode_on", Boolean.valueOf(false));
    public static final d<Long> aq = a("consent_dialog_show_from_alert_delay_ms", Long.valueOf(450));
    public static final d<Boolean> ar = a("alert_consent_for_dialog_rejected", Boolean.valueOf(false));
    public static final d<Boolean> as = a("alert_consent_for_dialog_closed", Boolean.valueOf(false));
    public static final d<Boolean> at = a("alert_consent_for_dialog_closed_with_back_button", Boolean.valueOf(false));
    public static final d<Boolean> au = a("alert_consent_after_init", Boolean.valueOf(false));
    public static final d<Long> av = a("alert_consent_after_init_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final d<Long> aw = a("alert_consent_after_dialog_rejection_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(30)));
    public static final d<Long> ax = a("alert_consent_after_dialog_close_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final d<Long> ay = a("alert_consent_after_dialog_close_with_back_button_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final d<Long> az = a("alert_consent_after_cancel_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(10)));
    private static final Map<String, d<?>> b = new HashMap(512);
    public static final d<Boolean> bA = a("preload_merge_init_tasks_leader_regular", Boolean.valueOf(false));
    public static final d<Boolean> bB = a("preload_merge_init_tasks_zones", Boolean.valueOf(false));
    public static final d<Boolean> bC = a("honor_publisher_settings_verbose_logging", Boolean.valueOf(true));
    public static final d<Boolean> bD = a("honor_publisher_settings_auto_preload_ad_sizes", Boolean.valueOf(true));
    public static final d<Boolean> bE = a("honor_publisher_settings_auto_preload_ad_types", Boolean.valueOf(true));
    public static final d<Boolean> bF = a("cache_cleanup_enabled", Boolean.valueOf(false));
    public static final d<Long> bG = a("cache_file_ttl_seconds", Long.valueOf(TimeUnit.DAYS.toSeconds(1)));
    public static final d<Integer> bH = a("cache_max_size_mb", Integer.valueOf(-1));
    public static final d<String> bI = a("precache_delimiters", ")]',");
    public static final d<Boolean> bJ = a("native_auto_cache_preload_resources", Boolean.valueOf(true));
    public static final d<Boolean> bK = a("ad_resource_caching_enabled", Boolean.valueOf(true));
    public static final d<Boolean> bL = a("fail_ad_load_on_failed_video_cache", Boolean.valueOf(true));
    public static final d<String> bM = a("resource_cache_prefix", "https://vid.applovin.com/,https://pdn.applovin.com/,https://img.applovin.com/,https://d.applovin.com/,https://assets.applovin.com/,https://cdnjs.cloudflare.com/,http://vid.applovin.com/,http://pdn.applovin.com/,http://img.applovin.com/,http://d.applovin.com/,http://assets.applovin.com/,http://cdnjs.cloudflare.com/");
    public static final d<String> bN = a("preserved_cached_assets", "sound_off.png,sound_on.png,closeOptOut.png,1381250003_28x28.png,zepto-1.1.3.min.js,jquery-2.1.1.min.js,jquery-1.9.1.min.js,jquery.knob.js");
    public static final d<Integer> bO = a("vr_retry_count_v1", Integer.valueOf(1));
    public static final d<Integer> bP = a("cr_retry_count_v1", Integer.valueOf(1));
    public static final d<Boolean> bQ = a("incent_warning_enabled", Boolean.valueOf(false));
    public static final d<String> bR = a("text_incent_warning_title", "Attention!");
    public static final d<String> bS = a("text_incent_warning_body", "You won’t get your reward if the video hasn’t finished.");
    public static final d<String> bT = a("text_incent_warning_close_option", "Close");
    public static final d<String> bU = a("text_incent_warning_continue_option", "Keep Watching");
    public static final d<Boolean> bV = a("incent_nonvideo_warning_enabled", Boolean.valueOf(false));
    public static final d<String> bW = a("text_incent_nonvideo_warning_title", "Attention!");
    public static final d<String> bX = a("text_incent_nonvideo_warning_body", "You won’t get your reward if the game hasn’t finished.");
    public static final d<String> bY = a("text_incent_nonvideo_warning_close_option", "Close");
    public static final d<String> bZ = a("text_incent_nonvideo_warning_continue_option", "Keep Playing");
    public static final d<Integer> ba = a("super_property_url_max_length", Integer.valueOf(1024));
    public static final d<Integer> bb = a("preload_callback_timeout_seconds", Integer.valueOf(-1));
    public static final d<Boolean> bc = a("ad_preload_enabled", Boolean.valueOf(true));
    public static final d<String> bd = a("ad_auto_preload_sizes", "");
    public static final d<Boolean> be = a("ad_auto_preload_incent", Boolean.valueOf(true));
    public static final d<Boolean> bf = a("ad_auto_preload_native", Boolean.valueOf(false));
    public static final d<Boolean> bg = a("preload_native_ad_on_dequeue", Boolean.valueOf(false));
    public static final d<Integer> bh = a("preload_capacity_banner_regular", Integer.valueOf(0));
    public static final d<Integer> bi = a("preload_capacity_mrec_regular", Integer.valueOf(0));
    public static final d<Integer> bj = a("preload_capacity_leader_regular", Integer.valueOf(0));
    public static final d<Integer> bk = a("preload_capacity_inter_regular", Integer.valueOf(0));
    public static final d<Integer> bl = a("preload_capacity_inter_videoa", Integer.valueOf(0));
    public static final d<Boolean> bm = a("use_per_format_cache_queues", Boolean.valueOf(true));
    public static final d<Integer> bn = a("extended_preload_capacity_banner_regular", Integer.valueOf(15));
    public static final d<Integer> bo = a("extended_preload_capacity_mrec_regular", Integer.valueOf(15));
    public static final d<Integer> bp = a("extended_preload_capacity_leader_regular", Integer.valueOf(15));
    public static final d<Integer> bq = a("extended_preload_capacity_inter_regular", Integer.valueOf(15));
    public static final d<Integer> br = a("extended_preload_capacity_inter_videoa", Integer.valueOf(15));
    public static final d<Integer> bs = a("preload_capacity_zone", Integer.valueOf(1));
    public static final d<Integer> bt = a("preload_capacity_zone_native", Integer.valueOf(1));
    public static final d<Integer> bu = a("extended_preload_capacity_zone", Integer.valueOf(15));
    public static final d<Integer> bv = a("preload_capacity_native_native", Integer.valueOf(0));
    public static final d<Boolean> bw = a("preload_merge_init_tasks_inter_regular", Boolean.valueOf(false));
    public static final d<Boolean> bx = a("preload_merge_init_tasks_inter_videoa", Boolean.valueOf(false));
    public static final d<Boolean> by = a("preload_merge_init_tasks_banner_regular", Boolean.valueOf(false));
    public static final d<Boolean> bz = a("preload_merge_init_tasks_mrec_regular", Boolean.valueOf(false));
    public static final d<Long> cA = a("fullscreen_ad_showing_state_timeout_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(2)));
    public static final d<Boolean> cB = a("lhs_close_button_video", Boolean.valueOf(false));
    public static final d<Integer> cC = a("close_button_right_margin_video", Integer.valueOf(4));
    public static final d<Integer> cD = a("close_button_size_video", Integer.valueOf(30));
    public static final d<Integer> cE = a("close_button_top_margin_video", Integer.valueOf(8));
    public static final d<Integer> cF = a("close_fade_in_time", Integer.valueOf(ErrorCode.GENERAL_LINEAR_ERROR));
    public static final d<Boolean> cG = a("show_close_on_exit", Boolean.valueOf(true));
    public static final d<Integer> cH = a("video_countdown_clock_margin", Integer.valueOf(10));
    public static final d<Integer> cI = a("video_countdown_clock_gravity", Integer.valueOf(83));
    public static final d<Integer> cJ = a("countdown_clock_size", Integer.valueOf(32));
    public static final d<Integer> cK = a("countdown_clock_stroke_size", Integer.valueOf(4));
    public static final d<Integer> cL = a("countdown_clock_text_size", Integer.valueOf(28));
    public static final d<Boolean> cM = a("draw_countdown_clock", Boolean.valueOf(true));
    public static final d<Boolean> cN = a("force_back_button_enabled_always", Boolean.valueOf(false));
    public static final d<Boolean> cO = a("force_back_button_enabled_close_button", Boolean.valueOf(false));
    public static final d<Boolean> cP = a("force_back_button_enabled_poststitial", Boolean.valueOf(false));
    public static final d<Long> cQ = a("force_hide_status_bar_delay_ms", Long.valueOf(0));
    public static final d<Boolean> cR = a("handle_window_actions", Boolean.valueOf(false));
    public static final d<Long> cS = a("inter_display_delay", Long.valueOf(200));
    public static final d<Boolean> cT = a("lock_specific_orientation", Boolean.valueOf(false));
    public static final d<Boolean> cU = a("lhs_skip_button", Boolean.valueOf(true));
    public static final d<String> cV = a("soft_buttons_resource_id", "config_showNavigationBar");
    public static final d<Boolean> cW = a("countdown_toggleable", Boolean.valueOf(false));
    public static final d<Boolean> cX = a("track_app_killed", Boolean.valueOf(false));
    public static final d<Boolean> cY = a("should_use_animated_mute_icon", Boolean.valueOf(false));
    public static final d<Boolean> cZ = a("mute_controls_enabled", Boolean.valueOf(false));
    public static final d<Boolean> ca = a("video_callbacks_for_incent_nonvideo_ads_enabled", Boolean.valueOf(true));
    public static final d<Boolean> cb = a("check_webview_has_gesture", Boolean.valueOf(false));
    public static final d<Integer> cc = a("close_button_touch_area", Integer.valueOf(0));
    public static final d<Long> cd = a("viewability_adview_imp_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final d<Integer> ce = a("viewability_adview_banner_min_width", Integer.valueOf(ModuleDescriptor.MODULE_VERSION));
    public static final d<Integer> cf = a("viewability_adview_banner_min_height", Integer.valueOf(AppLovinAdSize.BANNER.getHeight()));
    public static final d<Integer> cg = a("viewability_adview_mrec_min_width", Integer.valueOf(AppLovinAdSize.MREC.getWidth()));
    public static final d<Integer> ch = a("viewability_adview_mrec_min_height", Integer.valueOf(AppLovinAdSize.MREC.getWidth()));
    public static final d<Integer> ci = a("viewability_adview_leader_min_width", Integer.valueOf(728));
    public static final d<Integer> cj = a("viewability_adview_leader_min_height", Integer.valueOf(AppLovinAdSize.LEADER.getWidth()));
    public static final d<Float> ck = a("viewability_adview_min_alpha", Float.valueOf(10.0f));
    public static final d<Long> cl = a("viewability_timer_min_visible_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final d<Long> cm = a("viewability_timer_interval_ms", Long.valueOf(100));
    public static final d<Boolean> cn = a("dismiss_expanded_adview_on_refresh", Boolean.valueOf(false));
    public static final d<Boolean> co = a("dismiss_expanded_adview_on_detach", Boolean.valueOf(false));
    public static final d<Boolean> cp = a("contract_expanded_ad_on_close", Boolean.valueOf(true));
    public static final d<Long> cq = a("expandable_close_button_animation_duration_ms", Long.valueOf(300));
    public static final d<Integer> cr = a("expandable_close_button_size", Integer.valueOf(27));
    public static final d<Integer> cs = a("expandable_h_close_button_margin", Integer.valueOf(10));
    public static final d<Integer> ct = a("expandable_t_close_button_margin", Integer.valueOf(10));
    public static final d<Boolean> cu = a("expandable_lhs_close_button", Boolean.valueOf(false));
    public static final d<Integer> cv = a("expandable_close_button_touch_area", Integer.valueOf(0));
    public static final d<Boolean> cw = a("click_failed_expand", Boolean.valueOf(false));
    public static final d<Integer> cx = a("auxiliary_operations_threads", Integer.valueOf(3));
    public static final d<Integer> cy = a("caching_operations_threads", Integer.valueOf(8));
    public static final d<Long> cz = a("fullscreen_ad_pending_display_state_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(10)));
    public static final d<Integer> dA = a("fetch_ad_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final d<Integer> dB = a("fetch_ad_retry_count_v1", Integer.valueOf(1));
    public static final d<Integer> dC = a("submit_data_retry_count_v1", Integer.valueOf(1));
    public static final d<Integer> dD = a("response_buffer_size", Integer.valueOf(16000));
    public static final d<Integer> dE = a("fetch_basic_settings_connection_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final d<Integer> dF = a("fetch_basic_settings_retry_count", Integer.valueOf(3));
    public static final d<Boolean> dG = a("fetch_basic_settings_on_reconnect", Boolean.valueOf(false));
    public static final d<Boolean> dH = a("skip_fetch_basic_settings_if_not_connected", Boolean.valueOf(false));
    public static final d<Integer> dI = a("fetch_basic_settings_retry_delay_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(2)));
    public static final d<Integer> dJ = a("fetch_variables_connection_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(5)));
    public static final d<Boolean> dK = a("preload_persisted_zones", Boolean.valueOf(true));
    public static final d<Boolean> dL = a("persist_zones", Boolean.valueOf(false));
    public static final d<Integer> dM = a("ad_session_minutes", Integer.valueOf(60));
    public static final d<Boolean> dN = a("session_tracking_cooldown_on_event_fire", Boolean.valueOf(true));
    public static final d<Long> dO = a("session_tracking_resumed_cooldown_minutes", Long.valueOf(90));
    public static final d<Long> dP = a("session_tracking_paused_cooldown_minutes", Long.valueOf(90));
    public static final d<Boolean> dQ = a("track_app_paused", Boolean.valueOf(false));
    public static final d<Boolean> dR = a("qq", Boolean.valueOf(false));
    public static final d<Boolean> dS = a("qq1", Boolean.valueOf(true));
    public static final d<Boolean> dT = a("qq3", Boolean.valueOf(true));
    public static final d<Boolean> dU = a("qq4", Boolean.valueOf(true));
    public static final d<Boolean> dV = a("qq5", Boolean.valueOf(true));
    public static final d<Boolean> dW = a("qq6", Boolean.valueOf(true));
    public static final d<Boolean> dX = a("qq7", Boolean.valueOf(true));
    public static final d<Boolean> dY = a("qq8", Boolean.valueOf(true));
    public static final d<Boolean> dZ = a("pui", Boolean.valueOf(true));
    public static final d<Boolean> da = a("allow_user_muting", Boolean.valueOf(true));
    public static final d<Boolean> db = a("mute_videos", Boolean.valueOf(false));
    public static final d<Boolean> dc = a("show_mute_by_default", Boolean.valueOf(false));
    public static final d<Boolean> dd = a("mute_with_user_settings", Boolean.valueOf(true));
    public static final d<Integer> de = a("mute_button_size", Integer.valueOf(32));
    public static final d<Integer> df = a("mute_button_margin", Integer.valueOf(10));
    public static final d<Integer> dg = a("mute_button_gravity", Integer.valueOf(85));
    public static final d<Boolean> dh = a("video_immersive_mode_enabled", Boolean.valueOf(false));
    public static final d<Long> di = a("progress_bar_step", Long.valueOf(25));
    public static final d<Integer> dj = a("progress_bar_scale", Integer.valueOf(10000));
    public static final d<Integer> dk = a("progress_bar_vertical_padding", Integer.valueOf(-8));
    public static final d<Long> dl = a("video_resume_delay", Long.valueOf(250));
    public static final d<Boolean> dm = a("is_video_skippable", Boolean.valueOf(false));
    public static final d<Integer> dn = a("vs_buffer_indicator_size", Integer.valueOf(50));

    /* renamed from: do reason: not valid java name */
    public static final d<Boolean> f0do = a("video_zero_length_as_computed", Boolean.valueOf(false));
    public static final d<Long> dp = a("set_poststitial_muted_initial_delay_ms", Long.valueOf(500));
    public static final d<Boolean> dq = a("widget_fail_on_slot_count_diff", Boolean.valueOf(true));
    public static final d<Integer> dr = a("native_batch_precache_count", Integer.valueOf(1));
    public static final d<Integer> ds = a("submit_postback_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final d<Integer> dt = a("submit_postback_retries", Integer.valueOf(4));
    public static final d<Integer> du = a("max_postback_attempts", Integer.valueOf(3));
    public static final d<Integer> dv = a("get_retry_delay_v1", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final d<Integer> dw = a("http_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final d<Integer> dx = a("http_socket_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(20)));
    public static final d<Boolean> dy = a("force_ssl", Boolean.valueOf(false));
    public static final d<Boolean> dz = a("network_available_if_none_detected", Boolean.valueOf(true));
    public static final d<Integer> eA = a("submit_ad_stats_retry_count", Integer.valueOf(1));
    public static final d<Integer> eB = a("submit_ad_stats_max_count", Integer.valueOf(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL));
    public static final d<Boolean> eC = a("asdm", Boolean.valueOf(false));
    public static final d<Boolean> eD = a("task_stats_enabled", Boolean.valueOf(false));
    public static final d<Boolean> eE = a("error_reporting_enabled", Boolean.valueOf(false));
    public static final d<Integer> eF = a("error_reporting_log_limit", Integer.valueOf(100));
    public static final d<String> eG = a("vast_image_html", "<html><head><style>html,body{height:100%;width:100%}body{background-image:url({SOURCE});background-repeat:no-repeat;background-size:contain;background-position:center;}a{position:absolute;top:0;bottom:0;left:0;right:0}</style></head><body><a href=\"applovin://com.applovin.sdk/adservice/track_click_now\"></a></body></html>");
    public static final d<String> eH = a("vast_link_html", "<html><head><style>html,body,iframe{height:100%;width:100%;}body{margin:0}iframe{border:0;overflow:hidden;position:absolute}</style></head><body><iframe src={SOURCE} frameborder=0></iframe></body></html>");
    public static final d<Integer> eI = a("vast_max_response_length", Integer.valueOf(640000));
    public static final d<Integer> eJ = a("vast_max_wrapper_depth", Integer.valueOf(5));
    public static final d<Long> eK = a("vast_progress_tracking_countdown_step", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final d<String> eL = a("vast_unsupported_video_extensions", "ogv,flv");
    public static final d<String> eM = a("vast_unsupported_video_types", "video/ogg,video/x-flv");
    public static final d<Boolean> eN = a("vast_validate_with_extension_if_no_video_type", Boolean.valueOf(true));
    public static final d<Integer> eO = a("vast_video_selection_policy", Integer.valueOf(a.MEDIUM.ordinal()));
    public static final d<Integer> eP = a("vast_wrapper_resolution_retry_count_v1", Integer.valueOf(1));
    public static final d<Integer> eQ = a("vast_wrapper_resolution_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final d<Boolean> eR = a("ree", Boolean.valueOf(true));
    public static final d<Boolean> eS = a("btee", Boolean.valueOf(true));
    public static final d<Long> eT = a("server_timestamp_ms", Long.valueOf(0));
    public static final d<Long> eU = a("device_timestamp_ms", Long.valueOf(0));
    public static final d<Boolean> eV = a("cleanup_webview", Boolean.valueOf(false));
    public static final d<Boolean> eW = a("sanitize_webview", Boolean.valueOf(false));
    public static final d<Boolean> eX = a("force_rerender", Boolean.valueOf(false));
    public static final d<Boolean> eY = a("ignore_is_showing", Boolean.valueOf(false));
    public static final d<Boolean> eZ = a("render_empty_adview", Boolean.valueOf(true));
    public static final d<String> ea = a("plugin_version", "");
    public static final d<Boolean> eb = a("hgn", Boolean.valueOf(false));
    public static final d<Boolean> ec = a("citab", Boolean.valueOf(false));
    public static final d<Boolean> ed = a("cit", Boolean.valueOf(false));
    public static final d<Boolean> ee = a("cso", Boolean.valueOf(false));
    public static final d<Boolean> ef = a("cfs", Boolean.valueOf(false));
    public static final d<Boolean> eg = a("cmi", Boolean.valueOf(false));
    public static final d<Boolean> eh = a("cvs", Boolean.valueOf(false));
    public static final d<Boolean> ei = a("caf", Boolean.valueOf(false));
    public static final d<Boolean> ej = a("cf", Boolean.valueOf(false));
    public static final d<String> ek = a("emulator_hardware_list", "ranchu,goldfish,vbox");
    public static final d<String> el = a("emulator_device_list", "generic,vbox");
    public static final d<String> em = a("emulator_manufacturer_list", "Genymotion");
    public static final d<String> en = a("emulator_model_list", "Android SDK built for x86");
    public static final d<Boolean> eo = a("adr", Boolean.valueOf(false));
    public static final d<Float> ep = a("volume_normalization_factor", Float.valueOf(6.6666665f));
    public static final d<Boolean> eq = a("user_agent_collection_enabled", Boolean.valueOf(false));
    public static final d<Long> er = a("user_agent_collection_timeout_ms", Long.valueOf(600));
    public static final d<String> es = a("webview_package_name", "com.google.android.webview");
    public static final d<Boolean> et = a("collect_device_angle", Boolean.valueOf(false));
    public static final d<Boolean> eu = a("collect_device_movement", Boolean.valueOf(false));
    public static final d<Float> ev = a("movement_degradation", Float.valueOf(0.75f));
    public static final d<Integer> ew = a("device_sensor_period_ms", Integer.valueOf(250));
    public static final d<Boolean> ex = a("is_track_ad_info", Boolean.valueOf(true));
    public static final d<Boolean> ey = a("submit_ad_stats_enabled", Boolean.valueOf(false));
    public static final d<Integer> ez = a("submit_ad_stats_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final d<Boolean> fa = a("daostr", Boolean.valueOf(false));
    public static final d<Boolean> fb = a("urrr", Boolean.valueOf(false));
    public static final d<Boolean> fc = a("tctpmw", Boolean.valueOf(false));
    public static final d<Boolean> fd = a("tctlaa", Boolean.valueOf(false));
    public static final d<Boolean> fe = a("swvb", Boolean.valueOf(false));
    public static final d<Boolean> ff = a("rwvdv", Boolean.valueOf(false));
    public static final d<Boolean> fg = a("sfawv", Boolean.valueOf(false));
    public static final d<String> fh = a("config_consent_dialog_state", "unknown");
    private final String c;
    private final T d;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(String.class.getName());
        sb.append(",");
        sb.append(Integer.class.getName());
        sb.append(",");
        sb.append(Long.class.getName());
        sb.append(",");
        sb.append(Double.class.getName());
        sb.append(",");
        sb.append(Float.class.getName());
        sb.append(",");
        sb.append(Date.class.getName());
        sb.append(",");
        sb.append(Uri.class.getName());
        sb.append(",");
        sb.append(List.class.getName());
        sb.append(",");
        sb.append(Map.class.getName());
        aX = a("valid_super_property_types", sb.toString());
    }

    public d(String str, T t) {
        if (str == null) {
            throw new IllegalArgumentException("No name specified");
        } else if (t != null) {
            this.c = str;
            this.d = t;
        } else {
            throw new IllegalArgumentException("No default value specified");
        }
    }

    protected static <T> d<T> a(String str, T t) {
        if (t == null) {
            throw new IllegalArgumentException("No default value specified");
        } else if (a.contains(t.getClass())) {
            d<T> dVar = new d<>(str, t);
            if (!b.containsKey(str)) {
                b.put(str, dVar);
                return dVar;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Setting has already been used: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Unsupported value type: ");
            sb2.append(t.getClass());
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    public static Collection<d<?>> c() {
        return Collections.synchronizedCollection(b.values());
    }

    /* access modifiers changed from: 0000 */
    public T a(Object obj) {
        return this.d.getClass().cast(obj);
    }

    public String a() {
        return this.c;
    }

    public T b() {
        return this.d;
    }

    public int compareTo(Object obj) {
        if (!(obj instanceof d)) {
            return 0;
        }
        return this.c.compareTo(((d) obj).a());
    }
}
