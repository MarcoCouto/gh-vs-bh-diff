package com.applovin.impl.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import java.util.Set;

public final class g {
    private static SharedPreferences a;
    private final SharedPreferences b;

    public g(j jVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("com.applovin.sdk.preferences.");
        sb.append(jVar.t());
        this.b = jVar.D().getSharedPreferences(sb.toString(), 0);
    }

    private static SharedPreferences a(Context context) {
        if (a == null) {
            a = context.getSharedPreferences("com.applovin.sdk.shared", 0);
        }
        return a;
    }

    public static <T> T a(String str, T t, Class cls, SharedPreferences sharedPreferences) {
        T t2;
        long j;
        int i;
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            if (sharedPreferences.contains(str)) {
                if (Boolean.class.equals(cls)) {
                    t2 = Boolean.valueOf(t != null ? sharedPreferences.getBoolean(str, ((Boolean) t).booleanValue()) : sharedPreferences.getBoolean(str, false));
                } else if (Float.class.equals(cls)) {
                    t2 = Float.valueOf(t != null ? sharedPreferences.getFloat(str, ((Float) t).floatValue()) : sharedPreferences.getFloat(str, 0.0f));
                } else if (Integer.class.equals(cls)) {
                    if (t != null) {
                        i = sharedPreferences.getInt(str, t.getClass().equals(Long.class) ? ((Long) t).intValue() : ((Integer) t).intValue());
                    } else {
                        i = sharedPreferences.getInt(str, 0);
                    }
                    t2 = Integer.valueOf(i);
                } else if (Long.class.equals(cls)) {
                    if (t != null) {
                        j = sharedPreferences.getLong(str, t.getClass().equals(Integer.class) ? ((Integer) t).longValue() : ((Long) t).longValue());
                    } else {
                        j = sharedPreferences.getLong(str, 0);
                    }
                    t2 = Long.valueOf(j);
                } else {
                    t2 = String.class.equals(cls) ? sharedPreferences.getString(str, (String) t) : Set.class.isAssignableFrom(cls) ? sharedPreferences.getStringSet(str, (Set) t) : t;
                }
                if (t2 != null) {
                    return cls.cast(t2);
                }
                StrictMode.setThreadPolicy(allowThreadDiskReads);
                return t;
            }
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return t;
        } catch (Throwable th) {
            String str2 = "SharedPreferencesManager";
            StringBuilder sb = new StringBuilder();
            sb.append("Error getting value for key: ");
            sb.append(str);
            p.c(str2, sb.toString(), th);
            return t;
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    public static <T> void a(f<T> fVar, Context context) {
        a(context).edit().remove(fVar.a()).apply();
    }

    public static <T> void a(f<T> fVar, T t, Context context) {
        a(fVar.a(), t, a(context), (Editor) null);
    }

    private static <T> void a(String str, T t, SharedPreferences sharedPreferences, Editor editor) {
        boolean z = false;
        boolean z2 = editor != null;
        if (!z2) {
            editor = sharedPreferences.edit();
        }
        if (t instanceof Boolean) {
            editor.putBoolean(str, ((Boolean) t).booleanValue());
        } else if (t instanceof Float) {
            editor.putFloat(str, ((Float) t).floatValue());
        } else if (t instanceof Integer) {
            editor.putInt(str, ((Integer) t).intValue());
        } else if (t instanceof Long) {
            editor.putLong(str, ((Long) t).longValue());
        } else if (t instanceof String) {
            editor.putString(str, (String) t);
        } else if (t instanceof Set) {
            editor.putStringSet(str, (Set) t);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to put default value of invalid type: ");
            sb.append(t);
            p.j("SharedPreferencesManager", sb.toString());
            if (z && !z2) {
                editor.apply();
                return;
            }
        }
        z = true;
        if (z) {
        }
    }

    public static <T> T b(f<T> fVar, T t, Context context) {
        return a(fVar.a(), t, fVar.b(), a(context));
    }

    public void a(SharedPreferences sharedPreferences) {
        sharedPreferences.edit().clear().apply();
    }

    public <T> void a(f<T> fVar) {
        this.b.edit().remove(fVar.a()).apply();
    }

    public <T> void a(f<T> fVar, T t) {
        a(fVar, t, this.b);
    }

    public <T> void a(f<T> fVar, T t, SharedPreferences sharedPreferences) {
        a(fVar.a(), t, sharedPreferences);
    }

    public <T> void a(String str, T t, Editor editor) {
        a(str, t, (SharedPreferences) null, editor);
    }

    public <T> void a(String str, T t, SharedPreferences sharedPreferences) {
        a(str, t, sharedPreferences, (Editor) null);
    }

    public <T> T b(f<T> fVar, T t) {
        return b(fVar, t, this.b);
    }

    public <T> T b(f<T> fVar, T t, SharedPreferences sharedPreferences) {
        return a(fVar.a(), t, fVar.b(), sharedPreferences);
    }
}
