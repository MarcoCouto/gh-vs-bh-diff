package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.mediation.g;
import com.applovin.impl.mediation.h;
import com.applovin.impl.mediation.k;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.c;
import com.applovin.impl.sdk.c.f;
import com.applovin.impl.sdk.d.ac;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.network.PostbackServiceImpl;
import com.applovin.impl.sdk.network.e;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdk.SdkInitializationListener;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserService;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class j {
    protected static Context a;
    private c A;
    private u B;
    private a C;
    private o D;
    private t E;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.network.c F;
    private f G;
    private m H;
    private e I;
    private PostbackServiceImpl J;
    private e K;
    private h L;
    private g M;
    private MediationServiceImpl N;
    private k O;
    private a P;
    private com.applovin.impl.mediation.j Q;
    /* access modifiers changed from: private */
    public final Object R = new Object();
    private final AtomicBoolean S = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public boolean T = false;
    private boolean U = false;
    private boolean V = false;
    private boolean W = false;
    private boolean X = false;
    private String Y = "";
    private SdkInitializationListener Z;
    private SdkInitializationListener aa;
    /* access modifiers changed from: private */
    public AppLovinSdkConfiguration ab;
    protected com.applovin.impl.sdk.b.e b;
    private String c;
    private WeakReference<Activity> d;
    private long e;
    private AppLovinSdkSettings f;
    private AppLovinAdServiceImpl g;
    private NativeAdServiceImpl h;
    private EventServiceImpl i;
    private UserServiceImpl j;
    private VariableServiceImpl k;
    private AppLovinSdk l;
    /* access modifiers changed from: private */
    public p m;
    /* access modifiers changed from: private */
    public r n;
    private com.applovin.impl.sdk.network.a o;
    private com.applovin.impl.sdk.c.h p;
    private com.applovin.impl.sdk.c.j q;
    private k r;
    private com.applovin.impl.sdk.b.g s;
    private f t;
    private i u;
    private p v;
    private c w;
    private q x;
    private n y;
    private com.applovin.impl.sdk.ad.e z;

    public static Context E() {
        return a;
    }

    private void ah() {
        this.F.a((com.applovin.impl.sdk.network.c.a) new com.applovin.impl.sdk.network.c.a() {
            public void a() {
                j.this.m.c("AppLovinSdk", "Connected to internet - re-initializing SDK");
                synchronized (j.this.R) {
                    if (!j.this.T) {
                        j.this.b();
                    }
                }
                j.this.F.b(this);
            }

            public void b() {
            }
        });
    }

    public k A() {
        return this.O;
    }

    public com.applovin.impl.mediation.j B() {
        return this.Q;
    }

    public com.applovin.impl.sdk.b.e C() {
        return this.b;
    }

    public Context D() {
        return a;
    }

    public Activity F() {
        if (this.d != null) {
            return (Activity) this.d.get();
        }
        return null;
    }

    public long G() {
        return this.e;
    }

    public boolean H() {
        return this.W;
    }

    public boolean I() {
        return this.X;
    }

    public com.applovin.impl.sdk.network.a J() {
        return this.o;
    }

    public r K() {
        return this.n;
    }

    public com.applovin.impl.sdk.c.h L() {
        return this.p;
    }

    public com.applovin.impl.sdk.c.j M() {
        return this.q;
    }

    public e N() {
        return this.K;
    }

    public k O() {
        return this.r;
    }

    public f P() {
        return this.t;
    }

    public i Q() {
        return this.u;
    }

    public PostbackServiceImpl R() {
        return this.J;
    }

    public AppLovinSdk S() {
        return this.l;
    }

    public c T() {
        return this.w;
    }

    public q U() {
        return this.x;
    }

    public n V() {
        return this.y;
    }

    public com.applovin.impl.sdk.ad.e W() {
        return this.z;
    }

    public c X() {
        return this.A;
    }

    public u Y() {
        return this.B;
    }

    public o Z() {
        return this.D;
    }

    public <ST> d<ST> a(String str, d<ST> dVar) {
        return this.b.a(str, dVar);
    }

    public <T> T a(d<T> dVar) {
        return this.b.a(dVar);
    }

    public <T> T a(com.applovin.impl.sdk.b.f<T> fVar) {
        return b(fVar, null);
    }

    public <T> T a(String str, T t2, Class cls, SharedPreferences sharedPreferences) {
        com.applovin.impl.sdk.b.g gVar = this.s;
        return com.applovin.impl.sdk.b.g.a(str, t2, cls, sharedPreferences);
    }

    public void a() {
        synchronized (this.R) {
            if (!this.T && !this.U) {
                b();
            }
        }
    }

    public void a(long j2) {
        this.u.a(j2);
    }

    public void a(SharedPreferences sharedPreferences) {
        this.s.a(sharedPreferences);
    }

    public void a(com.applovin.impl.mediation.b.e eVar) {
        if (!this.n.a()) {
            List b2 = b((d) com.applovin.impl.sdk.b.c.a);
            if (b2.size() > 0 && this.M.b().containsAll(b2)) {
                this.m.b("AppLovinSdk", "All required adapters initialized");
                this.n.e();
                f();
            }
        }
    }

    public <T> void a(com.applovin.impl.sdk.b.f<T> fVar, T t2) {
        this.s.a(fVar, t2);
    }

    public <T> void a(com.applovin.impl.sdk.b.f<T> fVar, T t2, SharedPreferences sharedPreferences) {
        this.s.a(fVar, t2, sharedPreferences);
    }

    public void a(SdkInitializationListener sdkInitializationListener) {
        if (!d()) {
            this.Z = sdkInitializationListener;
        } else if (sdkInitializationListener != null) {
            sdkInitializationListener.onSdkInitialized(this.ab);
        }
    }

    public void a(AppLovinSdk appLovinSdk) {
        this.l = appLovinSdk;
    }

    public void a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Setting plugin version: ");
        sb.append(str);
        p.g("AppLovinSdk", sb.toString());
        this.b.a(d.ea, (Object) str);
        this.b.b();
    }

    public void a(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        com.applovin.impl.sdk.b.g gVar;
        com.applovin.impl.sdk.b.f<String> fVar;
        String bool;
        this.c = str;
        this.e = System.currentTimeMillis();
        this.f = appLovinSdkSettings;
        this.ab = new SdkConfigurationImpl(this);
        a = context.getApplicationContext();
        if (context instanceof Activity) {
            this.d = new WeakReference<>((Activity) context);
        }
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            this.m = new p(this);
            this.s = new com.applovin.impl.sdk.b.g(this);
            this.b = new com.applovin.impl.sdk.b.e(this);
            this.b.c();
            this.b.a();
            this.t = new f(this);
            this.t.b();
            this.y = new n(this);
            this.w = new c(this);
            this.x = new q(this);
            this.z = new com.applovin.impl.sdk.ad.e(this);
            this.i = new EventServiceImpl(this);
            this.j = new UserServiceImpl(this);
            this.k = new VariableServiceImpl(this);
            this.A = new c(this);
            this.n = new r(this);
            this.o = new com.applovin.impl.sdk.network.a(this);
            this.p = new com.applovin.impl.sdk.c.h(this);
            this.q = new com.applovin.impl.sdk.c.j(this);
            this.r = new k(this);
            this.C = new a(context);
            this.g = new AppLovinAdServiceImpl(this);
            this.h = new NativeAdServiceImpl(this);
            this.B = new u(this);
            this.D = new o(this);
            this.J = new PostbackServiceImpl(this);
            this.K = new e(this);
            this.L = new h(this);
            this.M = new g(this);
            this.N = new MediationServiceImpl(this);
            this.P = new a(this);
            this.O = new k();
            this.Q = new com.applovin.impl.mediation.j(this);
            this.u = new i(this);
            this.v = new p(this);
            this.E = new t(this);
            this.H = new m(this);
            this.I = new e(this);
            this.G = new f(this);
            if (((Boolean) this.b.a(d.dG)).booleanValue()) {
                this.F = new com.applovin.impl.sdk.network.c(context);
            }
            if (TextUtils.isEmpty(str)) {
                this.V = true;
                p.j("AppLovinSdk", "Unable to find AppLovin SDK key. Please add  meta-data android:name=\"applovin.sdk.key\" android:value=\"YOUR_SDK_KEY_HERE\" into AndroidManifest.xml.");
                StringWriter stringWriter = new StringWriter();
                new Throwable("").printStackTrace(new PrintWriter(stringWriter));
                String stringWriter2 = stringWriter.toString();
                StringBuilder sb = new StringBuilder();
                sb.append("Called with an invalid SDK key from: ");
                sb.append(stringWriter2);
                p.j("AppLovinSdk", sb.toString());
            }
            if (!u()) {
                if (((Boolean) this.b.a(d.ae)).booleanValue()) {
                    appLovinSdkSettings.setVerboseLogging(q.a(context));
                    C().a(appLovinSdkSettings);
                    C().b();
                }
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                if (TextUtils.isEmpty((String) this.s.b(com.applovin.impl.sdk.b.f.a, null, defaultSharedPreferences))) {
                    this.W = true;
                    gVar = this.s;
                    fVar = com.applovin.impl.sdk.b.f.a;
                    bool = Boolean.toString(true);
                } else {
                    gVar = this.s;
                    fVar = com.applovin.impl.sdk.b.f.a;
                    bool = Boolean.toString(false);
                }
                gVar.a(fVar, bool, defaultSharedPreferences);
                if (((Boolean) this.s.b(com.applovin.impl.sdk.b.f.b, Boolean.valueOf(false))).booleanValue()) {
                    this.m.b("AppLovinSdk", "Initializing SDK for non-maiden launch");
                    this.X = true;
                } else {
                    this.m.b("AppLovinSdk", "Initializing SDK for maiden launch");
                    this.s.a(com.applovin.impl.sdk.b.f.b, Boolean.valueOf(true));
                }
                q.a(com.applovin.impl.sdk.b.f.g, 100, this);
                boolean a2 = com.applovin.impl.sdk.utils.h.a(D());
                if (!((Boolean) this.b.a(d.dH)).booleanValue() || a2) {
                    b();
                }
                if (((Boolean) this.b.a(d.dG)).booleanValue() && !a2) {
                    this.m.c("AppLovinSdk", "SDK initialized with no internet connection - listening for connection");
                    ah();
                }
            } else {
                a(false);
            }
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    public <T> void a(String str, T t2, Editor editor) {
        this.s.a(str, t2, editor);
    }

    public void a(boolean z2) {
        synchronized (this.R) {
            this.T = false;
            this.U = z2;
        }
        if (this.b != null && this.n != null) {
            List b2 = b((d) com.applovin.impl.sdk.b.c.a);
            if (b2.isEmpty()) {
                this.n.e();
                f();
                return;
            }
            long longValue = ((Long) a(com.applovin.impl.sdk.b.c.b)).longValue();
            ac acVar = new ac(this, true, new Runnable() {
                public void run() {
                    if (!j.this.n.a()) {
                        j.this.m.b("AppLovinSdk", "Timing out adapters init...");
                        j.this.n.e();
                        j.this.f();
                    }
                }
            });
            StringBuilder sb = new StringBuilder();
            sb.append("Waiting for required adapters to init: ");
            sb.append(b2);
            sb.append(" - timing out in ");
            sb.append(longValue);
            sb.append("ms...");
            this.m.b("AppLovinSdk", sb.toString());
            this.n.a((com.applovin.impl.sdk.d.a) acVar, r.a.MEDIATION_TIMEOUT, longValue, true);
        }
    }

    public a aa() {
        return this.C;
    }

    public t ab() {
        return this.E;
    }

    public f ac() {
        return this.G;
    }

    public m ad() {
        return this.H;
    }

    public e ae() {
        return this.I;
    }

    public AppLovinBroadcastManager af() {
        return AppLovinBroadcastManager.getInstance(a);
    }

    public Activity ag() {
        Activity F2 = F();
        if (F2 != null) {
            return F2;
        }
        Activity a2 = aa().a();
        if (a2 != null) {
            return a2;
        }
        return null;
    }

    public <T> T b(com.applovin.impl.sdk.b.f<T> fVar, T t2) {
        return this.s.b(fVar, t2);
    }

    public <T> T b(com.applovin.impl.sdk.b.f<T> fVar, T t2, SharedPreferences sharedPreferences) {
        return this.s.b(fVar, t2, sharedPreferences);
    }

    public List<String> b(d dVar) {
        return this.b.b(dVar);
    }

    public void b() {
        synchronized (this.R) {
            this.T = true;
            K().d();
            K().a((com.applovin.impl.sdk.d.a) new com.applovin.impl.sdk.d.k(this), r.a.MAIN);
        }
    }

    public <T> void b(com.applovin.impl.sdk.b.f<T> fVar) {
        this.s.a(fVar);
    }

    public void b(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Setting user id: ");
        sb.append(str);
        p.g("AppLovinSdk", sb.toString());
        this.v.a(str);
    }

    public void c(String str) {
        a(com.applovin.impl.sdk.b.f.A, (T) str);
    }

    public boolean c() {
        boolean z2;
        synchronized (this.R) {
            z2 = this.T;
        }
        return z2;
    }

    public boolean d() {
        boolean z2;
        synchronized (this.R) {
            z2 = this.U;
        }
        return z2;
    }

    public boolean e() {
        return "HSrCHRtOan6wp2kwOIGJC1RDtuSrF2mWVbio2aBcMHX9KF3iTJ1lLSzCKP1ZSo5yNolPNw1kCTtWpxELFF4ah1".equalsIgnoreCase(t());
    }

    public void f() {
        if (this.Z != null) {
            final SdkInitializationListener sdkInitializationListener = this.Z;
            if (d()) {
                this.Z = null;
                this.aa = null;
            } else if (this.aa != sdkInitializationListener) {
                if (((Boolean) a(d.ai)).booleanValue()) {
                    this.Z = null;
                } else {
                    this.aa = sdkInitializationListener;
                }
            } else {
                return;
            }
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    j.this.m.b("AppLovinSdk", "Calling back publisher's initialization completion handler...");
                    sdkInitializationListener.onSdkInitialized(j.this.ab);
                }
            }, Math.max(0, ((Long) a(d.aj)).longValue()));
        }
    }

    public void g() {
        long b2 = this.p.b(com.applovin.impl.sdk.c.g.g);
        this.b.d();
        this.b.b();
        this.p.a();
        this.A.b();
        this.q.b();
        this.p.b(com.applovin.impl.sdk.c.g.g, b2 + 1);
        if (this.S.compareAndSet(true, false)) {
            b();
        } else {
            this.S.set(true);
        }
    }

    public void h() {
        this.P.b();
    }

    public String i() {
        return this.v.a();
    }

    public String j() {
        return this.v.b();
    }

    public String k() {
        return this.v.c();
    }

    public AppLovinSdkSettings l() {
        return this.f;
    }

    public AppLovinSdkConfiguration m() {
        return this.ab;
    }

    public String n() {
        return (String) a(com.applovin.impl.sdk.b.f.A);
    }

    public AppLovinAdServiceImpl o() {
        return this.g;
    }

    public NativeAdServiceImpl p() {
        return this.h;
    }

    public AppLovinEventService q() {
        return this.i;
    }

    public AppLovinUserService r() {
        return this.j;
    }

    public VariableServiceImpl s() {
        return this.k;
    }

    public String t() {
        return this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CoreSdk{sdkKey='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", enabled=");
        sb.append(this.U);
        sb.append(", isFirstSession=");
        sb.append(this.W);
        sb.append('}');
        return sb.toString();
    }

    public boolean u() {
        return this.V;
    }

    public p v() {
        return this.m;
    }

    public h w() {
        return this.L;
    }

    public g x() {
        return this.M;
    }

    public MediationServiceImpl y() {
        return this.N;
    }

    public a z() {
        return this.P;
    }
}
