package com.applovin.impl.sdk;

import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.p;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinVariableService;
import com.applovin.sdk.AppLovinVariableService.OnVariablesUpdateListener;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class VariableServiceImpl implements AppLovinVariableService {
    private final j a;
    /* access modifiers changed from: private */
    public final AtomicBoolean b = new AtomicBoolean();
    private final AtomicBoolean c = new AtomicBoolean();
    /* access modifiers changed from: private */
    public OnVariablesUpdateListener d;
    private Bundle e;
    private final Object f = new Object();

    VariableServiceImpl(j jVar) {
        this.a = jVar;
        String str = (String) jVar.a(f.i);
        if (n.b(str)) {
            updateVariables(i.a(str, jVar));
        }
    }

    private Object a(String str, Object obj, Class<?> cls) {
        if (TextUtils.isEmpty(str)) {
            p.j("AppLovinVariableService", "Unable to retrieve variable value for empty name");
            return obj;
        }
        if (!this.a.d()) {
            p.i("AppLovinSdk", "Attempted to retrieve variable before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
        }
        synchronized (this.f) {
            if (this.e == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to retrieve variable value for name \"");
                sb.append(str);
                sb.append("\", none retrieved from server yet. Please set a listener to be notified when values are retrieved from the server.");
                p.j("AppLovinVariableService", sb.toString());
                return obj;
            } else if (cls.equals(String.class)) {
                String string = this.e.getString(str, (String) obj);
                return string;
            } else if (cls.equals(Boolean.class)) {
                Boolean valueOf = Boolean.valueOf(this.e.getBoolean(str, ((Boolean) obj).booleanValue()));
                return valueOf;
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to retrieve variable value for ");
                sb2.append(str);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        return;
     */
    private void a() {
        synchronized (this.f) {
            if (this.d != null) {
                if (this.e != null) {
                    final Bundle bundle = (Bundle) this.e.clone();
                    AppLovinSdkUtils.runOnUiThread(true, new Runnable() {
                        public void run() {
                            VariableServiceImpl.this.d.onVariablesUpdate(bundle);
                        }
                    });
                }
            }
        }
    }

    public boolean getBoolean(String str) {
        return getBoolean(str, false);
    }

    public boolean getBoolean(String str, boolean z) {
        return ((Boolean) a(str, Boolean.valueOf(z), Boolean.class)).booleanValue();
    }

    public String getString(String str) {
        return getString(str, null);
    }

    public String getString(String str, String str2) {
        return (String) a(str, str2, String.class);
    }

    public void loadVariables() {
        String str;
        String str2;
        if (!this.a.d()) {
            str2 = "AppLovinVariableService";
            str = "The AppLovin SDK is waiting for the initial variables to be returned upon completing initialization.";
        } else if (this.b.compareAndSet(false, true)) {
            this.a.K().a((a) new p(this.a, new p.a() {
                public void a() {
                    VariableServiceImpl.this.b.set(false);
                }
            }), r.a.BACKGROUND);
            return;
        } else {
            str2 = "AppLovinVariableService";
            str = "Ignored explicit variables load. Service is already in the process of retrieving the latest set of variables.";
        }
        p.j(str2, str);
    }

    public void setOnVariablesUpdateListener(OnVariablesUpdateListener onVariablesUpdateListener) {
        this.d = onVariablesUpdateListener;
        synchronized (this.f) {
            if (onVariablesUpdateListener != null) {
                try {
                    if (this.e != null && this.c.compareAndSet(false, true)) {
                        this.a.v().b("AppLovinVariableService", "Setting initial listener");
                        a();
                    }
                } finally {
                }
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VariableService{variables=");
        sb.append(this.e);
        sb.append(", listener=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }

    public void updateVariables(JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append("Updating variables: ");
        sb.append(jSONObject);
        sb.append("...");
        this.a.v().b("AppLovinVariableService", sb.toString());
        synchronized (this.f) {
            this.e = i.c(jSONObject);
            a();
            this.a.a(f.i, jSONObject.toString());
        }
    }
}
