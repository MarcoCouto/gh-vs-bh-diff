package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.HashSet;
import java.util.Set;

public class e extends BroadcastReceiver {
    public static int a = -1;
    private final AudioManager b;
    private final Context c;
    private final j d;
    private final Set<a> e = new HashSet();
    private final Object f = new Object();
    private boolean g;
    private int h;

    public interface a {
        void onRingerModeChanged(int i);
    }

    e(j jVar) {
        this.d = jVar;
        this.c = jVar.D();
        this.b = (AudioManager) this.c.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
    }

    private void a() {
        this.d.v().b("AudioSessionManager", "Observing ringer mode...");
        this.h = a;
        Context context = this.c;
        AudioManager audioManager = this.b;
        context.registerReceiver(this, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
        this.d.af().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        this.d.af().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public static boolean a(int i) {
        return i == 0 || i == 1;
    }

    private void b() {
        this.d.v().b("AudioSessionManager", "Stopping observation of mute switch state...");
        this.c.unregisterReceiver(this);
        this.d.af().unregisterReceiver(this);
    }

    private void b(final int i) {
        if (!this.g) {
            StringBuilder sb = new StringBuilder();
            sb.append("Ringer mode is ");
            sb.append(i);
            this.d.v().b("AudioSessionManager", sb.toString());
            synchronized (this.f) {
                for (final a aVar : this.e) {
                    AppLovinSdkUtils.runOnUiThread(new Runnable() {
                        public void run() {
                            aVar.onRingerModeChanged(i);
                        }
                    });
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        return;
     */
    public void a(a aVar) {
        synchronized (this.f) {
            if (!this.e.contains(aVar)) {
                this.e.add(aVar);
                if (this.e.size() == 1) {
                    a();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        return;
     */
    public void b(a aVar) {
        synchronized (this.f) {
            if (this.e.contains(aVar)) {
                this.e.remove(aVar);
                if (this.e.isEmpty()) {
                    b();
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        AudioManager audioManager = this.b;
        if (!"android.media.RINGER_MODE_CHANGED".equals(action)) {
            if ("com.applovin.application_paused".equals(action)) {
                this.g = true;
                this.h = this.b.getRingerMode();
                return;
            } else if ("com.applovin.application_resumed".equals(action)) {
                this.g = false;
                if (this.h != this.b.getRingerMode()) {
                    this.h = a;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
        b(this.b.getRingerMode());
    }
}
