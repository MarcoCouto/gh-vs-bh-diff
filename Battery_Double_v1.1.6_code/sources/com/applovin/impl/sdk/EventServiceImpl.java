package com.applovin.impl.sdk;

import android.content.Intent;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.k.a;
import com.applovin.impl.sdk.k.b;
import com.applovin.impl.sdk.k.c;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinEventParameters;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinEventTypes;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class EventServiceImpl implements AppLovinEventService {
    /* access modifiers changed from: private */
    public final j a;
    /* access modifiers changed from: private */
    public final Map<String, Object> b;
    private final AtomicBoolean c = new AtomicBoolean();

    public EventServiceImpl(j jVar) {
        this.a = jVar;
        if (((Boolean) jVar.a(d.aY)).booleanValue()) {
            this.b = i.a((String) this.a.b(f.r, "{}"), (Map<String, Object>) new HashMap<String,Object>(), this.a);
            return;
        }
        this.b = new HashMap();
        jVar.a(f.r, "{}");
    }

    /* access modifiers changed from: private */
    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) this.a.a(d.aP));
        sb.append("4.0/pix");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> a(l lVar, a aVar) {
        k O = this.a.O();
        k.d b2 = O.b();
        b c2 = O.c();
        boolean contains = this.a.b((d) d.aV).contains(lVar.a());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("event", contains ? n.e(lVar.a()) : "postinstall");
        hashMap.put("ts", Long.toString(lVar.c()));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, n.e(b2.c));
        hashMap.put("model", n.e(b2.a));
        hashMap.put("package_name", n.e(c2.c));
        hashMap.put("installer_name", n.e(c2.d));
        hashMap.put("ia", Long.toString(c2.h));
        hashMap.put("api_did", this.a.a(d.X));
        hashMap.put("brand", n.e(b2.d));
        hashMap.put("brand_name", n.e(b2.e));
        hashMap.put("hardware", n.e(b2.f));
        hashMap.put("revision", n.e(b2.g));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("os", n.e(b2.b));
        hashMap.put("orientation_lock", b2.l);
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, n.e(c2.b));
        hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, n.e(b2.i));
        hashMap.put("carrier", n.e(b2.j));
        hashMap.put("tz_offset", String.valueOf(b2.r));
        hashMap.put("aida", String.valueOf(b2.N));
        hashMap.put("adr", b2.t ? "1" : "0");
        hashMap.put(AvidVideoPlaybackListenerImpl.VOLUME, String.valueOf(b2.x));
        hashMap.put("sb", String.valueOf(b2.y));
        hashMap.put("sim", b2.A ? "1" : "0");
        hashMap.put("gy", String.valueOf(b2.B));
        hashMap.put("is_tablet", String.valueOf(b2.C));
        hashMap.put("tv", String.valueOf(b2.D));
        hashMap.put("vs", String.valueOf(b2.E));
        hashMap.put("lpm", String.valueOf(b2.F));
        hashMap.put("tg", c2.e);
        hashMap.put("ltg", c2.f);
        hashMap.put("fs", String.valueOf(b2.H));
        hashMap.put("tds", String.valueOf(b2.I));
        hashMap.put("fm", String.valueOf(b2.J.b));
        hashMap.put("tm", String.valueOf(b2.J.a));
        hashMap.put("lmt", String.valueOf(b2.J.c));
        hashMap.put("lm", String.valueOf(b2.J.d));
        hashMap.put("adns", String.valueOf(b2.m));
        hashMap.put("adnsd", String.valueOf(b2.n));
        hashMap.put("xdpi", String.valueOf(b2.o));
        hashMap.put("ydpi", String.valueOf(b2.p));
        hashMap.put("screen_size_in", String.valueOf(b2.q));
        hashMap.put("debug", Boolean.toString(q.b(this.a)));
        hashMap.put("af", String.valueOf(b2.v));
        hashMap.put("font", String.valueOf(b2.w));
        hashMap.put("bt_ms", String.valueOf(b2.Q));
        if (!((Boolean) this.a.a(d.eR)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.a.t());
        }
        a(aVar, (Map<String, String>) hashMap);
        if (((Boolean) this.a.a(d.dT)).booleanValue()) {
            q.a("cuid", this.a.i(), (Map<String, String>) hashMap);
        }
        if (((Boolean) this.a.a(d.dW)).booleanValue()) {
            hashMap.put("compass_random_token", this.a.j());
        }
        if (((Boolean) this.a.a(d.dY)).booleanValue()) {
            hashMap.put("applovin_random_token", this.a.k());
        }
        Boolean bool = b2.K;
        if (bool != null) {
            hashMap.put("huc", bool.toString());
        }
        Boolean bool2 = b2.L;
        if (bool2 != null) {
            hashMap.put("aru", bool2.toString());
        }
        Boolean bool3 = b2.M;
        if (bool3 != null) {
            hashMap.put("dns", bool3.toString());
        }
        c cVar = b2.u;
        if (cVar != null) {
            hashMap.put("act", String.valueOf(cVar.a));
            hashMap.put("acm", String.valueOf(cVar.b));
        }
        String str = b2.z;
        if (n.b(str)) {
            hashMap.put("ua", n.e(str));
        }
        String str2 = b2.G;
        if (!TextUtils.isEmpty(str2)) {
            hashMap.put("so", n.e(str2));
        }
        if (!contains) {
            hashMap.put("sub_event", n.e(lVar.a()));
        }
        if (b2.O > 0.0f) {
            hashMap.put("da", String.valueOf(b2.O));
        }
        if (b2.P > 0.0f) {
            hashMap.put("dm", String.valueOf(b2.P));
        }
        hashMap.put("sc", n.e((String) this.a.a(d.aa)));
        hashMap.put("sc2", n.e((String) this.a.a(d.ab)));
        hashMap.put("server_installed_at", n.e((String) this.a.a(d.ac)));
        q.a("persisted_data", n.e((String) this.a.a(f.z)), (Map<String, String>) hashMap);
        q.a("plugin_version", n.e((String) this.a.a(d.ea)), (Map<String, String>) hashMap);
        q.a("mediation_provider", n.e(this.a.n()), (Map<String, String>) hashMap);
        return hashMap;
    }

    private void a(com.applovin.impl.sdk.d.i.a aVar) {
        this.a.K().a((com.applovin.impl.sdk.d.a) new com.applovin.impl.sdk.d.i(this.a, aVar), r.a.ADVERTISING_INFO_COLLECTION);
    }

    private void a(a aVar, Map<String, String> map) {
        String str = aVar.b;
        if (n.b(str)) {
            map.put("idfa", str);
        }
        map.put("dnt", Boolean.toString(aVar.a));
    }

    /* access modifiers changed from: private */
    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append((String) this.a.a(d.aQ));
        sb.append("4.0/pix");
        return sb.toString();
    }

    private void c() {
        if (((Boolean) this.a.a(d.aY)).booleanValue()) {
            this.a.a(f.r, i.a(this.b, "{}", this.a));
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, boolean z) {
        trackEvent(str, new HashMap(), null, z);
    }

    public Map<String, Object> getSuperProperties() {
        return new HashMap(this.b);
    }

    public void maybeTrackAppOpenEvent() {
        if (this.c.compareAndSet(false, true)) {
            this.a.q().trackEvent("landing");
        }
    }

    public void setSuperProperty(Object obj, String str) {
        if (TextUtils.isEmpty(str)) {
            p.j("AppLovinEventService", "Super property key cannot be null or empty");
        } else if (obj == null) {
            this.b.remove(str);
            c();
        } else {
            List b2 = this.a.b((d) d.aX);
            if (!q.a(obj, b2, this.a)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to set super property '");
                sb.append(obj);
                sb.append("' for key '");
                sb.append(str);
                sb.append("' - valid super property types include: ");
                sb.append(b2);
                p.j("AppLovinEventService", sb.toString());
                return;
            }
            this.b.put(str, q.a(obj, this.a));
            c();
        }
    }

    public String toString() {
        return "EventService{}";
    }

    public void trackCheckout(String str, Map<String, String> map) {
        HashMap hashMap = map != null ? new HashMap(map) : new HashMap(1);
        hashMap.put("transaction_id", str);
        trackEvent(AppLovinEventTypes.USER_COMPLETED_CHECKOUT, hashMap);
    }

    public void trackEvent(String str) {
        trackEvent(str, new HashMap());
    }

    public void trackEvent(String str, Map<String, String> map) {
        trackEvent(str, map, null, true);
    }

    public void trackEvent(String str, Map<String, String> map, Map<String, String> map2, boolean z) {
        if (((Boolean) this.a.a(d.aW)).booleanValue()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Tracking event: \"");
            sb.append(str);
            sb.append("\" with parameters: ");
            sb.append(map);
            this.a.v().b("AppLovinEventService", sb.toString());
            final String str2 = str;
            final Map<String, String> map3 = map;
            final boolean z2 = z;
            final Map<String, String> map4 = map2;
            AnonymousClass1 r3 = new com.applovin.impl.sdk.d.i.a() {
                public void a(a aVar) {
                    l lVar = new l(str2, map3, EventServiceImpl.this.b);
                    try {
                        if (z2) {
                            EventServiceImpl.this.a.N().a(com.applovin.impl.sdk.network.f.l().a(EventServiceImpl.this.a()).b(EventServiceImpl.this.b()).a((Map<String, String>) EventServiceImpl.this.a(lVar, aVar)).b(map4).c(lVar.b()).a(((Boolean) EventServiceImpl.this.a.a(d.eR)).booleanValue()).a());
                            return;
                        }
                        EventServiceImpl.this.a.R().dispatchPostbackRequest(g.b(EventServiceImpl.this.a).a(EventServiceImpl.this.a()).c(EventServiceImpl.this.b()).a((Map<String, String>) EventServiceImpl.this.a(lVar, aVar)).c(map4).a(i.a(lVar.b())).a(((Boolean) EventServiceImpl.this.a.a(d.eR)).booleanValue()).a(), null);
                    } catch (Throwable th) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unable to track event: ");
                        sb.append(lVar);
                        EventServiceImpl.this.a.v().b("AppLovinEventService", sb.toString(), th);
                    }
                }
            };
            a((com.applovin.impl.sdk.d.i.a) r3);
        }
    }

    public void trackInAppPurchase(Intent intent, Map<String, String> map) {
        HashMap hashMap = map != null ? new HashMap(map) : new HashMap();
        try {
            hashMap.put(AppLovinEventParameters.IN_APP_PURCHASE_DATA, intent.getStringExtra("INAPP_PURCHASE_DATA"));
            hashMap.put(AppLovinEventParameters.IN_APP_DATA_SIGNATURE, intent.getStringExtra("INAPP_DATA_SIGNATURE"));
        } catch (Throwable th) {
            p.c("AppLovinEventService", "Unable to track in app purchase - invalid purchase intent", th);
        }
        trackEvent("iap", hashMap);
    }
}
