package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Intent;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.h.a;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserService.OnConsentDialogDismissListener;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.applovin.sdk.AppLovinWebViewActivity.EventListener;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public class i implements a, EventListener {
    /* access modifiers changed from: private */
    public static final AtomicBoolean a = new AtomicBoolean();
    /* access modifiers changed from: private */
    public static WeakReference<AppLovinWebViewActivity> b;
    /* access modifiers changed from: private */
    public final j c;
    /* access modifiers changed from: private */
    public final p d;
    /* access modifiers changed from: private */
    public OnConsentDialogDismissListener e;
    /* access modifiers changed from: private */
    public h f;
    /* access modifiers changed from: private */
    public WeakReference<Activity> g = new WeakReference<>(null);
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.utils.a h;

    i(j jVar) {
        this.c = jVar;
        this.d = jVar.v();
        if (jVar.F() != null) {
            this.g = new WeakReference<>(jVar.F());
        }
        jVar.aa().a(new com.applovin.impl.sdk.utils.a() {
            public void onActivityStarted(Activity activity) {
                i.this.g = new WeakReference(activity);
            }
        });
        this.f = new h(this, jVar);
    }

    private void a(boolean z, long j) {
        f();
        if (z) {
            a(j);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(j jVar) {
        if (c()) {
            p.j("AppLovinSdk", "Consent dialog already showing");
            return false;
        } else if (!h.a(jVar.D())) {
            p.j("AppLovinSdk", "No internet available, skip showing of consent dialog");
            return false;
        } else if (!((Boolean) jVar.a(d.an)).booleanValue()) {
            this.d.e("ConsentDialogManager", "Blocked publisher from showing consent dialog");
            return false;
        } else if (n.b((String) jVar.a(d.ao))) {
            return true;
        } else {
            this.d.e("ConsentDialogManager", "AdServer returned empty consent dialog URL");
            return false;
        }
    }

    private void f() {
        this.c.aa().b(this.h);
        if (c()) {
            AppLovinWebViewActivity appLovinWebViewActivity = (AppLovinWebViewActivity) b.get();
            b = null;
            if (appLovinWebViewActivity != null) {
                appLovinWebViewActivity.finish();
                if (this.e != null) {
                    this.e.onDismiss();
                    this.e = null;
                }
            }
        }
    }

    public void a() {
        if (this.g.get() != null) {
            final Activity activity = (Activity) this.g.get();
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    i.this.a(activity, (OnConsentDialogDismissListener) null);
                }
            }, ((Long) this.c.a(d.aq)).longValue());
        }
    }

    public void a(final long j) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                i.this.d.b("ConsentDialogManager", "Scheduling repeating consent alert");
                i.this.f.a(j, i.this.c, i.this);
            }
        });
    }

    public void a(final Activity activity, final OnConsentDialogDismissListener onConsentDialogDismissListener) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (!i.this.a(i.this.c) || i.a.getAndSet(true)) {
                    if (onConsentDialogDismissListener != null) {
                        onConsentDialogDismissListener.onDismiss();
                    }
                    return;
                }
                i.this.g = new WeakReference(activity);
                i.this.e = onConsentDialogDismissListener;
                i.this.h = new com.applovin.impl.sdk.utils.a() {
                    public void onActivityStarted(Activity activity) {
                        if (activity instanceof AppLovinWebViewActivity) {
                            if (!i.this.c() || i.b.get() != activity) {
                                AppLovinWebViewActivity appLovinWebViewActivity = (AppLovinWebViewActivity) activity;
                                i.b = new WeakReference(appLovinWebViewActivity);
                                appLovinWebViewActivity.loadUrl((String) i.this.c.a(d.ao), i.this);
                            }
                            i.a.set(false);
                        }
                    }
                };
                i.this.c.aa().a(i.this.h);
                Intent intent = new Intent(activity, AppLovinWebViewActivity.class);
                intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, i.this.c.t());
                intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON, (Serializable) i.this.c.a(d.ap));
                activity.startActivity(intent);
            }
        });
    }

    public void b() {
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return (b == null || b.get() == null) ? false : true;
    }

    public void onReceivedEvent(String str) {
        boolean booleanValue;
        j jVar;
        d<Long> dVar;
        if ("accepted".equalsIgnoreCase(str)) {
            AppLovinPrivacySettings.setHasUserConsent(true, this.c.D());
            f();
            return;
        }
        if ("rejected".equalsIgnoreCase(str)) {
            AppLovinPrivacySettings.setHasUserConsent(false, this.c.D());
            booleanValue = ((Boolean) this.c.a(d.ar)).booleanValue();
            jVar = this.c;
            dVar = d.aw;
        } else if ("closed".equalsIgnoreCase(str)) {
            booleanValue = ((Boolean) this.c.a(d.as)).booleanValue();
            jVar = this.c;
            dVar = d.ax;
        } else if (AppLovinWebViewActivity.EVENT_DISMISSED_VIA_BACK_BUTTON.equalsIgnoreCase(str)) {
            booleanValue = ((Boolean) this.c.a(d.at)).booleanValue();
            jVar = this.c;
            dVar = d.ay;
        } else {
            return;
        }
        a(booleanValue, ((Long) jVar.a(dVar)).longValue());
    }
}
