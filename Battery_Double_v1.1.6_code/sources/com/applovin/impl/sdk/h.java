package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicBoolean;

class h extends BroadcastReceiver {
    /* access modifiers changed from: private */
    public static AlertDialog b;
    /* access modifiers changed from: private */
    public static final AtomicBoolean c = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final i a;
    private o d;

    public interface a {
        void a();

        void b();
    }

    h(i iVar, j jVar) {
        this.a = iVar;
        jVar.af().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        jVar.af().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public void a(long j, final j jVar, final a aVar) {
        if (j > 0) {
            if (b == null || !b.isShowing()) {
                if (c.getAndSet(true)) {
                    if (j < this.d.a()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Scheduling consent alert earlier (");
                        sb.append(j);
                        sb.append("ms) than remaining scheduled time (");
                        sb.append(this.d.a());
                        sb.append("ms)");
                        jVar.v().b("ConsentAlertManager", sb.toString());
                        this.d.d();
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Skip scheduling consent alert - one scheduled already with remaining time of ");
                        sb2.append(this.d.a());
                        sb2.append(" milliseconds");
                        jVar.v().d("ConsentAlertManager", sb2.toString());
                        return;
                    }
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Scheduling consent alert for ");
                sb3.append(j);
                sb3.append(" milliseconds");
                jVar.v().b("ConsentAlertManager", sb3.toString());
                this.d = o.a(j, jVar, new Runnable() {
                    public void run() {
                        p v;
                        String str;
                        String str2;
                        if (h.this.a.c()) {
                            jVar.v().e("ConsentAlertManager", "Consent dialog already showing, skip showing of consent alert");
                            return;
                        }
                        Activity a2 = jVar.aa().a();
                        if (a2 == null || !com.applovin.impl.sdk.utils.h.a(jVar.D())) {
                            if (a2 == null) {
                                v = jVar.v();
                                str = "ConsentAlertManager";
                                str2 = "No parent Activity found - rescheduling consent alert...";
                            } else {
                                v = jVar.v();
                                str = "ConsentAlertManager";
                                str2 = "No internet available - rescheduling consent alert...";
                            }
                            v.e(str, str2);
                            h.c.set(false);
                            h.this.a(((Long) jVar.a(d.aA)).longValue(), jVar, aVar);
                            return;
                        }
                        AppLovinSdkUtils.runOnUiThread(new Runnable() {
                            public void run() {
                                h.b = new Builder(jVar.aa().a()).setTitle((CharSequence) jVar.a(d.aB)).setMessage((CharSequence) jVar.a(d.aC)).setCancelable(false).setPositiveButton((CharSequence) jVar.a(d.aD), new OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.a();
                                        dialogInterface.dismiss();
                                        h.c.set(false);
                                    }
                                }).setNegativeButton((CharSequence) jVar.a(d.aE), new OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.b();
                                        dialogInterface.dismiss();
                                        h.c.set(false);
                                        h.this.a(((Long) jVar.a(d.az)).longValue(), jVar, aVar);
                                    }
                                }).create();
                                h.b.show();
                            }
                        });
                    }
                });
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (this.d != null) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                this.d.b();
            } else if ("com.applovin.application_resumed".equals(action)) {
                this.d.c();
            }
        }
    }
}
