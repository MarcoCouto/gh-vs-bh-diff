package com.applovin.impl.sdk;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.SensorManager;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.d.i;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.l;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.places.model.PlaceFields;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class k {
    private static String e;
    private static String f;
    private static int g;
    private final j a;
    /* access modifiers changed from: private */
    public final p b;
    /* access modifiers changed from: private */
    public final Context c;
    private final Map<Class, Object> d;
    /* access modifiers changed from: private */
    public final AtomicReference<a> h = new AtomicReference<>();

    public static class a {
        public boolean a;
        public String b = "";
    }

    public static class b {
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public long h;
    }

    public static class c {
        public int a = -1;
        public int b = -1;
    }

    public static class d {
        public boolean A;
        public boolean B;
        public boolean C;
        public boolean D;
        public boolean E;
        public int F = -1;
        public String G;
        public long H;
        public long I;
        public e J = new e();
        public Boolean K;
        public Boolean L;
        public Boolean M;
        public boolean N;
        public float O;
        public float P;
        public long Q;
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public int h;
        public String i;
        public String j;
        public Locale k;
        public String l;
        public float m;
        public int n;
        public float o;
        public float p;
        public double q;
        public double r;
        public int s;
        public boolean t;
        public c u;
        public long v;
        public float w;
        public int x;
        public int y;
        public String z;
    }

    public static class e {
        public long a = -1;
        public long b = -1;
        public long c = -1;
        public boolean d = false;
    }

    protected k(j jVar) {
        if (jVar != null) {
            this.a = jVar;
            this.b = jVar.v();
            this.c = jVar.D();
            this.d = Collections.synchronizedMap(new HashMap());
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private d a(d dVar) {
        if (dVar == null) {
            dVar = new d();
        }
        dVar.K = g.a(this.c);
        dVar.L = g.b(this.c);
        dVar.M = g.c(this.c);
        dVar.u = ((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eb)).booleanValue() ? j() : null;
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eo)).booleanValue()) {
            dVar.t = p();
        }
        try {
            AudioManager audioManager = (AudioManager) this.c.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            if (audioManager != null) {
                dVar.x = (int) (((float) audioManager.getStreamVolume(3)) * ((Float) this.a.a(com.applovin.impl.sdk.b.d.ep)).floatValue());
            }
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect volume", th);
        }
        try {
            dVar.y = (int) ((((float) System.getInt(this.c.getContentResolver(), "screen_brightness")) / 255.0f) * 100.0f);
        } catch (SettingNotFoundException e2) {
            this.b.b("DataCollector", "Unable to collect screen brightness", e2);
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eq)).booleanValue()) {
            if (e == null) {
                String t = t();
                if (!n.b(t)) {
                    t = "";
                }
                e = t;
            }
            if (n.b(e)) {
                dVar.z = e;
            }
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ef)).booleanValue()) {
            try {
                dVar.H = Environment.getDataDirectory().getFreeSpace();
                dVar.I = Environment.getDataDirectory().getTotalSpace();
            } catch (Throwable th2) {
                dVar.H = -1;
                dVar.I = -1;
                this.b.b("DataCollector", "Unable to collect total & free space.", th2);
            }
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eg)).booleanValue()) {
            try {
                ActivityManager activityManager = (ActivityManager) this.c.getSystemService("activity");
                MemoryInfo memoryInfo = new MemoryInfo();
                activityManager.getMemoryInfo(memoryInfo);
                dVar.J.b = memoryInfo.availMem;
                dVar.J.d = memoryInfo.lowMemory;
                dVar.J.c = memoryInfo.threshold;
                dVar.J.a = memoryInfo.totalMem;
            } catch (Throwable th3) {
                this.b.b("DataCollector", "Unable to collect memory info.", th3);
            }
        }
        String str = (String) this.a.C().a(com.applovin.impl.sdk.b.d.es);
        if (!str.equalsIgnoreCase(f)) {
            try {
                f = str;
                PackageInfo packageInfo = this.c.getPackageManager().getPackageInfo(str, 0);
                dVar.s = packageInfo.versionCode;
                g = packageInfo.versionCode;
            } catch (Throwable unused) {
                g = 0;
            }
        } else {
            dVar.s = g;
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ec)).booleanValue()) {
            dVar.C = AppLovinSdkUtils.isTablet(this.c);
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ed)).booleanValue()) {
            dVar.D = o();
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ee)).booleanValue()) {
            String m = m();
            if (!TextUtils.isEmpty(m)) {
                dVar.G = m;
            }
        }
        dVar.l = g();
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eh)).booleanValue()) {
            dVar.E = q.d();
        }
        if (g.d()) {
            try {
                dVar.F = ((PowerManager) this.c.getSystemService("power")).isPowerSaveMode() ? 1 : 0;
            } catch (Throwable th4) {
                this.b.b("DataCollector", "Unable to collect power saving mode", th4);
            }
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.et)).booleanValue() && this.a.ad() != null) {
            dVar.O = this.a.ad().c();
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eu)).booleanValue() && this.a.ad() != null) {
            dVar.P = this.a.ad().b();
        }
        return dVar;
    }

    private String a(int i) {
        if (i == 1) {
            return "receiver";
        }
        if (i == 2) {
            return "speaker";
        }
        if (i == 4 || i == 3) {
            return "headphones";
        }
        if (i == 8) {
            return "bluetootha2dpoutput";
        }
        if (i == 13 || i == 19 || i == 5 || i == 6 || i == 12 || i == 11) {
            return "lineout";
        }
        if (i == 9 || i == 10) {
            return "hdmioutput";
        }
        return null;
    }

    private boolean a(String str) {
        try {
            return Secure.getInt(this.c.getContentResolver(), str) == 1;
        } catch (SettingNotFoundException unused) {
            return false;
        }
    }

    private boolean a(String str, com.applovin.impl.sdk.b.d<String> dVar) {
        for (String startsWith : com.applovin.impl.sdk.utils.e.a((String) this.a.a(dVar))) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private boolean b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("No permission name specified");
        } else if (this.c != null) {
            return com.applovin.impl.sdk.utils.k.a(str, this.c.getPackageName(), this.c.getPackageManager()) == 0;
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    private String c(String str) {
        int length = str.length();
        int[] iArr = {11, 12, 10, 3, 2, 1, 15, 10, 15, 14};
        int length2 = iArr.length;
        char[] cArr = new char[length];
        for (int i = 0; i < length; i++) {
            cArr[i] = str.charAt(i);
            for (int i2 = length2 - 1; i2 >= 0; i2--) {
                cArr[i] = (char) (cArr[i] ^ iArr[i2]);
            }
        }
        return new String(cArr);
    }

    private Map<String, String> f() {
        return a(null, false, true);
    }

    private String g() {
        String str;
        String str2 = "none";
        try {
            int b2 = q.b(this.c);
            if (b2 == 1) {
                str = "portrait";
            } else if (b2 != 2) {
                return str2;
            } else {
                str = "landscape";
            }
            return str;
        } catch (Throwable th) {
            this.a.v().b("DataCollector", "Encountered error while attempting to collect application orientation", th);
            return str2;
        }
    }

    private a h() {
        if (i()) {
            try {
                a aVar = new a();
                Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.c);
                aVar.a = advertisingIdInfo.isLimitAdTrackingEnabled();
                aVar.b = advertisingIdInfo.getId();
                return aVar;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }", th);
            }
        } else {
            p.j("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }");
            return new a();
        }
    }

    private boolean i() {
        return q.e("com.google.android.gms.ads.identifier.AdvertisingIdClient");
    }

    private c j() {
        try {
            c cVar = new c();
            Intent registerReceiver = this.c.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = -1;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : -1;
            int intExtra2 = registerReceiver != null ? registerReceiver.getIntExtra("scale", -1) : -1;
            if (intExtra <= 0 || intExtra2 <= 0) {
                cVar.b = -1;
            } else {
                cVar.b = (int) ((((float) intExtra) / ((float) intExtra2)) * 100.0f);
            }
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("status", -1);
            }
            cVar.a = i;
            return cVar;
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect battery info", th);
            return null;
        }
    }

    private long k() {
        List asList = Arrays.asList(n.c(Secure.getString(this.c.getContentResolver(), "enabled_accessibility_services")).split(":"));
        long j = asList.contains("AccessibilityMenuService") ? 256 : 0;
        if (asList.contains("SelectToSpeakService")) {
            j |= 512;
        }
        if (asList.contains("SoundAmplifierService")) {
            j |= 2;
        }
        if (asList.contains("SpeechToTextAccessibilityService")) {
            j |= 128;
        }
        if (asList.contains("SwitchAccessService")) {
            j |= 4;
        }
        if ((this.c.getResources().getConfiguration().uiMode & 48) == 32) {
            j |= PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
        }
        if (a("accessibility_enabled")) {
            j |= 8;
        }
        if (a("touch_exploration_enabled")) {
            j |= 16;
        }
        if (!g.d()) {
            return j;
        }
        if (a("accessibility_display_inversion_enabled")) {
            j |= 32;
        }
        return a("skip_first_use_hints") ? j | 64 : j;
    }

    private float l() {
        try {
            return System.getFloat(this.c.getContentResolver(), "font_scale");
        } catch (SettingNotFoundException e2) {
            this.b.b("DataCollector", "Error collecting font scale", e2);
            return -1.0f;
        }
    }

    private String m() {
        try {
            AudioManager audioManager = (AudioManager) this.c.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            StringBuilder sb = new StringBuilder();
            if (g.e()) {
                for (AudioDeviceInfo type : audioManager.getDevices(2)) {
                    String a2 = a(type.getType());
                    if (!TextUtils.isEmpty(a2)) {
                        sb.append(a2);
                        sb.append(",");
                    }
                }
            } else {
                if (audioManager.isWiredHeadsetOn()) {
                    sb.append("headphones");
                    sb.append(",");
                }
                if (audioManager.isBluetoothA2dpOn()) {
                    sb.append("bluetootha2dpoutput");
                }
            }
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == ',') {
                sb.deleteCharAt(sb.length() - 1);
            }
            String sb2 = sb.toString();
            if (TextUtils.isEmpty(sb2)) {
                this.b.b("DataCollector", "No sound outputs detected");
            }
            return sb2;
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect sound outputs", th);
            return null;
        }
    }

    private double n() {
        double offset = (double) TimeZone.getDefault().getOffset(new Date().getTime());
        Double.isNaN(offset);
        double round = (double) Math.round((offset * 10.0d) / 3600000.0d);
        Double.isNaN(round);
        return round / 10.0d;
    }

    private boolean o() {
        try {
            PackageManager packageManager = this.c.getPackageManager();
            return g.d() ? packageManager.hasSystemFeature("android.software.leanback") : packageManager.hasSystemFeature("android.hardware.type.television");
        } catch (Throwable th) {
            this.b.b("DataCollector", "Failed to determine if device is TV.", th);
            return false;
        }
    }

    private boolean p() {
        try {
            return q() || r();
        } catch (Throwable unused) {
            return false;
        }
    }

    private boolean q() {
        String str = Build.TAGS;
        return str != null && str.contains(c("lz}$blpz"));
    }

    private boolean r() {
        for (String c2 : new String[]{"&zpz}ld&hyy&Z|yl{|zl{'hyb", "&zk`g&z|", "&zpz}ld&k`g&z|", "&zpz}ld&qk`g&z|", "&mh}h&efjhe&qk`g&z|", "&mh}h&efjhe&k`g&z|", "&zpz}ld&zm&qk`g&z|", "&zpz}ld&k`g&oh`ezhol&z|", "&mh}h&efjhe&z|"}) {
            if (new File(c(c2)).exists()) {
                return true;
            }
        }
        return false;
    }

    private boolean s() {
        return a(Build.DEVICE, com.applovin.impl.sdk.b.d.el) || a(Build.HARDWARE, com.applovin.impl.sdk.b.d.ek) || a(Build.MANUFACTURER, com.applovin.impl.sdk.b.d.em) || a(Build.MODEL, com.applovin.impl.sdk.b.d.en);
    }

    private String t() {
        final AtomicReference atomicReference = new AtomicReference();
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        new Handler(this.c.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    atomicReference.set(new WebView(k.this.c).getSettings().getUserAgentString());
                } catch (Throwable th) {
                    countDownLatch.countDown();
                    throw th;
                }
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await(((Long) this.a.a(com.applovin.impl.sdk.b.d.er)).longValue(), TimeUnit.MILLISECONDS);
        } catch (Throwable unused) {
        }
        return (String) atomicReference.get();
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        String encodeToString = Base64.encodeToString(new JSONObject(f()).toString().getBytes(Charset.defaultCharset()), 2);
        if (!((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eS)).booleanValue()) {
            return encodeToString;
        }
        return l.a(encodeToString, this.a.t(), q.a(this.a));
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0435  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0455  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0470  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x048b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0498  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x04b4  */
    public Map<String, String> a(Map<String, String> map, boolean z, boolean z2) {
        a aVar;
        String str;
        com.applovin.impl.sdk.network.a.b a2;
        HashMap hashMap = new HashMap();
        d b2 = b();
        hashMap.put("brand", n.e(b2.d));
        hashMap.put("brand_name", n.e(b2.e));
        hashMap.put("hardware", n.e(b2.f));
        hashMap.put("api_level", String.valueOf(b2.h));
        hashMap.put("carrier", n.e(b2.j));
        hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, n.e(b2.i));
        hashMap.put("locale", n.e(b2.k.toString()));
        hashMap.put("model", n.e(b2.a));
        hashMap.put("os", n.e(b2.b));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, n.e(b2.c));
        hashMap.put("revision", n.e(b2.g));
        hashMap.put("orientation_lock", b2.l);
        hashMap.put("tz_offset", String.valueOf(b2.r));
        hashMap.put("aida", String.valueOf(b2.N));
        hashMap.put("wvvc", String.valueOf(b2.s));
        hashMap.put("adns", String.valueOf(b2.m));
        hashMap.put("adnsd", String.valueOf(b2.n));
        hashMap.put("xdpi", String.valueOf(b2.o));
        hashMap.put("ydpi", String.valueOf(b2.p));
        hashMap.put("screen_size_in", String.valueOf(b2.q));
        hashMap.put("sim", n.a(b2.A));
        hashMap.put("gy", n.a(b2.B));
        hashMap.put("is_tablet", n.a(b2.C));
        hashMap.put("tv", n.a(b2.D));
        hashMap.put("vs", n.a(b2.E));
        hashMap.put("lpm", String.valueOf(b2.F));
        hashMap.put("fs", String.valueOf(b2.H));
        hashMap.put("tds", String.valueOf(b2.I));
        hashMap.put("fm", String.valueOf(b2.J.b));
        hashMap.put("tm", String.valueOf(b2.J.a));
        hashMap.put("lmt", String.valueOf(b2.J.c));
        hashMap.put("lm", String.valueOf(b2.J.d));
        hashMap.put("adr", n.a(b2.t));
        hashMap.put(AvidVideoPlaybackListenerImpl.VOLUME, String.valueOf(b2.x));
        hashMap.put("sb", String.valueOf(b2.y));
        hashMap.put("af", String.valueOf(b2.v));
        hashMap.put("font", String.valueOf(b2.w));
        q.a("ua", n.e(b2.z), (Map<String, String>) hashMap);
        q.a("so", n.e(b2.G), (Map<String, String>) hashMap);
        hashMap.put("bt_ms", String.valueOf(b2.Q));
        if (b2.O > 0.0f) {
            hashMap.put("da", String.valueOf(b2.O));
        }
        if (b2.P > 0.0f) {
            hashMap.put("dm", String.valueOf(b2.P));
        }
        c cVar = b2.u;
        if (cVar != null) {
            hashMap.put("act", String.valueOf(cVar.a));
            hashMap.put("acm", String.valueOf(cVar.b));
        }
        Boolean bool = b2.K;
        if (bool != null) {
            hashMap.put("huc", bool.toString());
        }
        Boolean bool2 = b2.L;
        if (bool2 != null) {
            hashMap.put("aru", bool2.toString());
        }
        Boolean bool3 = b2.M;
        if (bool3 != null) {
            hashMap.put("dns", bool3.toString());
        }
        Point a3 = g.a(this.c);
        hashMap.put("dx", Integer.toString(a3.x));
        hashMap.put("dy", Integer.toString(a3.y));
        hashMap.put("accept", "custom_size,launch_app,video");
        hashMap.put("api_did", this.a.a(com.applovin.impl.sdk.b.d.X));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", Integer.toString(131));
        hashMap.put("format", "json");
        b c2 = c();
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, n.e(c2.b));
        hashMap.put("ia", Long.toString(c2.h));
        hashMap.put("tg", String.valueOf(c2.e));
        hashMap.put("ltg", String.valueOf(c2.f));
        hashMap.put("installer_name", c2.d);
        hashMap.put("debug", c2.g);
        q.a("mediation_provider", n.e(this.a.n()), (Map<String, String>) hashMap);
        hashMap.put("network", h.f(this.a));
        q.a("plugin_version", (String) this.a.a(com.applovin.impl.sdk.b.d.ea), (Map<String, String>) hashMap);
        hashMap.put("preloading", String.valueOf(z));
        hashMap.put("first_install", String.valueOf(this.a.H()));
        hashMap.put("first_install_v2", String.valueOf(!this.a.I()));
        if (!((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eR)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.a.t());
        }
        hashMap.put("sc", this.a.a(com.applovin.impl.sdk.b.d.aa));
        hashMap.put("sc2", this.a.a(com.applovin.impl.sdk.b.d.ab));
        hashMap.put("server_installed_at", n.e((String) this.a.a(com.applovin.impl.sdk.b.d.ac)));
        q.a("persisted_data", n.e((String) this.a.a(f.z)), (Map<String, String>) hashMap);
        hashMap.put("v1", Boolean.toString(g.a("android.permission.WRITE_EXTERNAL_STORAGE", this.c)));
        hashMap.put("v2", "true");
        hashMap.put("v3", "true");
        hashMap.put("v4", "true");
        hashMap.put("v5", "true");
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ex)).booleanValue()) {
            com.applovin.impl.sdk.c.h L = this.a.L();
            hashMap.put("li", String.valueOf(L.b(com.applovin.impl.sdk.c.g.b)));
            hashMap.put("si", String.valueOf(L.b(com.applovin.impl.sdk.c.g.d)));
            hashMap.put("pf", String.valueOf(L.b(com.applovin.impl.sdk.c.g.h)));
            hashMap.put("mpf", String.valueOf(L.b(com.applovin.impl.sdk.c.g.n)));
            hashMap.put("gpf", String.valueOf(L.b(com.applovin.impl.sdk.c.g.i)));
        }
        hashMap.put("vz", n.f(this.c.getPackageName()));
        if (z2) {
            aVar = (a) this.h.get();
            if (aVar != null) {
                e();
            } else if (q.b()) {
                aVar = new a();
                hashMap.put("inc", Boolean.toString(true));
            }
            str = aVar.b;
            if (n.b(str)) {
                hashMap.put("idfa", str);
            }
            hashMap.put("dnt", Boolean.toString(aVar.a));
            if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dT)).booleanValue()) {
                q.a("cuid", this.a.i(), (Map<String, String>) hashMap);
            }
            if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dW)).booleanValue()) {
                hashMap.put("compass_random_token", this.a.j());
            }
            if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dY)).booleanValue()) {
                hashMap.put("applovin_random_token", this.a.k());
            }
            if (map != null) {
                hashMap.putAll(map);
            }
            hashMap.put("rid", UUID.randomUUID().toString());
            a2 = this.a.J().a();
            if (a2 != null) {
                hashMap.put("lrm_ts_ms", String.valueOf(a2.a()));
                hashMap.put("lrm_url", a2.b());
                hashMap.put("lrm_ct_ms", String.valueOf(a2.d()));
                hashMap.put("lrm_rs", String.valueOf(a2.c()));
            }
            return hashMap;
        }
        aVar = this.a.O().d();
        str = aVar.b;
        if (n.b(str)) {
        }
        hashMap.put("dnt", Boolean.toString(aVar.a));
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dT)).booleanValue()) {
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dW)).booleanValue()) {
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dY)).booleanValue()) {
        }
        if (map != null) {
        }
        hashMap.put("rid", UUID.randomUUID().toString());
        a2 = this.a.J().a();
        if (a2 != null) {
        }
        return hashMap;
    }

    public d b() {
        d dVar;
        Object obj = this.d.get(d.class);
        if (obj != null) {
            dVar = (d) obj;
        } else {
            dVar = new d();
            dVar.k = Locale.getDefault();
            dVar.a = Build.MODEL;
            dVar.b = VERSION.RELEASE;
            dVar.c = "android";
            dVar.d = Build.MANUFACTURER;
            dVar.e = Build.BRAND;
            dVar.f = Build.HARDWARE;
            dVar.h = VERSION.SDK_INT;
            dVar.g = Build.DEVICE;
            dVar.r = n();
            dVar.A = s();
            dVar.N = i();
            try {
                dVar.B = ((SensorManager) this.c.getSystemService("sensor")).getDefaultSensor(4) != null;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Unable to retrieve gyroscope availability", th);
            }
            if (b("android.permission.READ_PHONE_STATE")) {
                TelephonyManager telephonyManager = (TelephonyManager) this.c.getSystemService(PlaceFields.PHONE);
                if (telephonyManager != null) {
                    dVar.i = telephonyManager.getSimCountryIso().toUpperCase(Locale.ENGLISH);
                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                    try {
                        dVar.j = URLEncoder.encode(networkOperatorName, "UTF-8");
                    } catch (UnsupportedEncodingException unused) {
                        dVar.j = networkOperatorName;
                    }
                }
            }
            try {
                DisplayMetrics displayMetrics = this.c.getResources().getDisplayMetrics();
                dVar.m = displayMetrics.density;
                dVar.n = displayMetrics.densityDpi;
                dVar.o = displayMetrics.xdpi;
                dVar.p = displayMetrics.ydpi;
                Point a2 = g.a(this.c);
                double sqrt = Math.sqrt(Math.pow((double) a2.x, 2.0d) + Math.pow((double) a2.y, 2.0d));
                double d2 = (double) dVar.o;
                Double.isNaN(d2);
                dVar.q = sqrt / d2;
            } catch (Throwable unused2) {
            }
            if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ei)).booleanValue()) {
                dVar.v = k();
            }
            if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ej)).booleanValue()) {
                dVar.w = l();
            }
            dVar.Q = System.currentTimeMillis() - SystemClock.elapsedRealtime();
            this.d.put(d.class, dVar);
        }
        return a(dVar);
    }

    public b c() {
        PackageInfo packageInfo;
        Object obj = this.d.get(b.class);
        if (obj != null) {
            return (b) obj;
        }
        ApplicationInfo applicationInfo = this.c.getApplicationInfo();
        long lastModified = new File(applicationInfo.sourceDir).lastModified();
        PackageManager packageManager = this.c.getPackageManager();
        String str = null;
        try {
            packageInfo = packageManager.getPackageInfo(this.c.getPackageName(), 0);
            try {
                str = packageManager.getInstallerPackageName(applicationInfo.packageName);
            } catch (Throwable unused) {
            }
        } catch (Throwable unused2) {
            packageInfo = null;
        }
        b bVar = new b();
        bVar.c = applicationInfo.packageName;
        if (str == null) {
            str = "";
        }
        bVar.d = str;
        bVar.h = lastModified;
        bVar.a = String.valueOf(packageManager.getApplicationLabel(applicationInfo));
        bVar.b = packageInfo != null ? packageInfo.versionName : "";
        bVar.e = q.a(f.g, this.a);
        bVar.f = q.a(f.h, this.a);
        bVar.g = Boolean.toString(q.b(this.a));
        this.d.put(b.class, bVar);
        return bVar;
    }

    public a d() {
        a h2 = h();
        if (!((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dS)).booleanValue()) {
            return new a();
        }
        if (!h2.a || ((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dR)).booleanValue()) {
            return h2;
        }
        h2.b = "";
        return h2;
    }

    public void e() {
        this.a.K().a((com.applovin.impl.sdk.d.a) new i(this.a, new com.applovin.impl.sdk.d.i.a() {
            public void a(a aVar) {
                k.this.h.set(aVar);
            }
        }), com.applovin.impl.sdk.d.r.a.ADVERTISING_INFO_COLLECTION);
    }
}
