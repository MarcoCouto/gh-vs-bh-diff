package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AppLovinBroadcastManager {
    private static AppLovinBroadcastManager f;
    private static final Object g = new Object();
    private final Context a;
    private final HashMap<BroadcastReceiver, ArrayList<b>> b = new HashMap<>();
    private final HashMap<String, ArrayList<b>> c = new HashMap<>();
    private final ArrayList<a> d = new ArrayList<>();
    private final Handler e = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message message) {
            if (message.what == 1) {
                AppLovinBroadcastManager.this.a();
            } else {
                super.handleMessage(message);
            }
        }
    };

    private static class a {
        final Intent a;
        final List<b> b;

        a(Intent intent, List<b> list) {
            this.a = intent;
            this.b = list;
        }
    }

    private static class b {
        final IntentFilter a;
        final BroadcastReceiver b;
        boolean c;
        boolean d;

        b(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
            this.a = intentFilter;
            this.b = broadcastReceiver;
        }
    }

    private AppLovinBroadcastManager(Context context) {
        this.a = context;
    }

    private List<b> a(Intent intent) {
        synchronized (this.b) {
            String action = intent.getAction();
            String resolveTypeIfNeeded = intent.resolveTypeIfNeeded(this.a.getContentResolver());
            Uri data = intent.getData();
            String scheme = intent.getScheme();
            Set categories = intent.getCategories();
            List<b> list = (List) this.c.get(action);
            if (list == null) {
                return null;
            }
            List<b> list2 = null;
            for (b bVar : list) {
                if (!bVar.c) {
                    b bVar2 = bVar;
                    if (bVar.a.match(action, resolveTypeIfNeeded, scheme, data, categories, "AppLovinBroadcastManager") >= 0) {
                        List<b> arrayList = list2 == null ? new ArrayList<>() : list2;
                        arrayList.add(bVar2);
                        bVar2.c = true;
                        list2 = arrayList;
                    }
                }
            }
            if (list2 == null) {
                return null;
            }
            for (b bVar3 : list2) {
                bVar3.c = false;
            }
            return list2;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        if (r2 >= r0) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        r3 = r1[r2];
        r4 = r3.b.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r4.hasNext() == false) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        r5 = (com.applovin.impl.sdk.AppLovinBroadcastManager.b) r4.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        if (r5.d != false) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
        r5.b.onReceive(r8.a, r3.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        r0 = r1.length;
        r2 = 0;
     */
    public void a() {
        while (true) {
            synchronized (this.b) {
                int size = this.d.size();
                if (size > 0) {
                    a[] aVarArr = new a[size];
                    this.d.toArray(aVarArr);
                    this.d.clear();
                } else {
                    return;
                }
            }
        }
        while (true) {
        }
    }

    public static AppLovinBroadcastManager getInstance(Context context) {
        AppLovinBroadcastManager appLovinBroadcastManager;
        synchronized (g) {
            if (f == null) {
                f = new AppLovinBroadcastManager(context.getApplicationContext());
            }
            appLovinBroadcastManager = f;
        }
        return appLovinBroadcastManager;
    }

    public void registerReceiver(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        synchronized (this.b) {
            b bVar = new b(intentFilter, broadcastReceiver);
            ArrayList arrayList = (ArrayList) this.b.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList(1);
                this.b.put(broadcastReceiver, arrayList);
            }
            arrayList.add(bVar);
            Iterator actionsIterator = intentFilter.actionsIterator();
            while (actionsIterator.hasNext()) {
                String str = (String) actionsIterator.next();
                ArrayList arrayList2 = (ArrayList) this.c.get(str);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(1);
                    this.c.put(str, arrayList2);
                }
                arrayList2.add(bVar);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        return true;
     */
    public boolean sendBroadcast(Intent intent) {
        synchronized (this.b) {
            List a2 = a(intent);
            if (a2 == null) {
                return false;
            }
            this.d.add(new a(intent, a2));
            if (!this.e.hasMessages(1)) {
                this.e.sendEmptyMessage(1);
            }
        }
    }

    public void sendBroadcastSync(Intent intent) {
        List<b> a2 = a(intent);
        if (a2 != null) {
            for (b bVar : a2) {
                if (!bVar.d) {
                    bVar.b.onReceive(this.a, intent);
                }
            }
        }
    }

    public void sendBroadcastSyncWithPendingBroadcasts(Intent intent) {
        if (sendBroadcast(intent)) {
            a();
        }
    }

    public void unregisterReceiver(BroadcastReceiver broadcastReceiver) {
        synchronized (this.b) {
            List<b> list = (List) this.b.remove(broadcastReceiver);
            if (list != null) {
                for (b bVar : list) {
                    bVar.d = true;
                    Iterator actionsIterator = bVar.a.actionsIterator();
                    while (actionsIterator.hasNext()) {
                        String str = (String) actionsIterator.next();
                        List list2 = (List) this.c.get(str);
                        if (list2 != null) {
                            Iterator it = list2.iterator();
                            while (it.hasNext()) {
                                if (((b) it.next()).b == broadcastReceiver) {
                                    bVar.d = true;
                                    it.remove();
                                }
                            }
                            if (list2.size() <= 0) {
                                this.c.remove(str);
                            }
                        }
                    }
                }
            }
        }
    }
}
