package com.applovin.impl.sdk;

import android.os.Bundle;
import com.applovin.communicator.AppLovinCommunicator;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorPublisher;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.communicator.AppLovinSdkTopic;
import com.applovin.impl.communicator.CommunicatorMessageImpl;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.b.a;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.utils.BundleUtils;
import com.applovin.impl.sdk.utils.i;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.places.model.PlaceFields;
import com.ironsource.sdk.precache.DownloadManager;
import com.tapjoy.TapjoyConstants;
import java.util.Map;
import org.json.JSONObject;

public class f implements AppLovinCommunicatorPublisher, AppLovinCommunicatorSubscriber {
    private final j a;
    private final AppLovinCommunicator b;

    f(j jVar) {
        this.a = jVar;
        this.b = AppLovinCommunicator.getInstance(jVar.D());
        if (!jVar.e()) {
            this.b.a(jVar);
            this.b.subscribe((AppLovinCommunicatorSubscriber) this, AppLovinSdkTopic.ALL_TOPICS);
        }
    }

    private void a(Bundle bundle, String str) {
        if (!this.a.e()) {
            if (!"log".equals(str)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Sending message ");
                sb.append(bundle);
                sb.append(" for topic: ");
                sb.append(str);
                sb.append("...");
                this.a.v().b("CommunicatorService", sb.toString());
            }
            this.b.getMessagingService().publish(CommunicatorMessageImpl.create(bundle, str, this, this.a.b((d) a.a).contains(str)));
        }
    }

    public void a(com.applovin.impl.mediation.b.a aVar, String str) {
        boolean j = aVar instanceof c ? ((c) aVar).j() : false;
        Bundle bundle = new Bundle();
        bundle.putString("type", str);
        bundle.putString("id", aVar.b());
        bundle.putString("network_name", aVar.D());
        bundle.putString("max_ad_unit_id", aVar.getAdUnitId());
        bundle.putString("third_party_ad_placement_id", aVar.e());
        bundle.putString("ad_format", aVar.getFormat().getLabel());
        bundle.putString("is_fallback_ad", String.valueOf(j));
        a(bundle, "max_ad_events");
    }

    public void a(String str, String str2, int i, Object obj) {
        Bundle bundle = new Bundle();
        bundle.putString("id", str);
        bundle.putString("url", str2);
        bundle.putInt("code", i);
        bundle.putBundle(TtmlNode.TAG_BODY, i.a(obj));
        a(bundle, AppLovinSdkTopic.RECEIVE_HTTP_RESPONSE);
    }

    public void a(JSONObject jSONObject, boolean z) {
        Bundle c = i.c(i.b(i.b(jSONObject, "communicator_settings", new JSONObject(), this.a), "safedk_settings", new JSONObject(), this.a));
        Bundle bundle = new Bundle();
        bundle.putString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.a.t());
        bundle.putString("applovin_random_token", this.a.k());
        bundle.putString(TapjoyConstants.TJC_DEVICE_TYPE_NAME, AppLovinSdkUtils.isTablet(this.a.D()) ? "tablet" : PlaceFields.PHONE);
        bundle.putString("init_success", String.valueOf(z));
        bundle.putBundle(DownloadManager.SETTINGS, c);
        bundle.putBoolean("debug_mode", ((Boolean) this.a.a(d.eR)).booleanValue());
        a(bundle, "safedk_init");
    }

    public String getCommunicatorId() {
        return "applovin_sdk";
    }

    public void onMessageReceived(AppLovinCommunicatorMessage appLovinCommunicatorMessage) {
        if (AppLovinSdkTopic.HTTP_REQUEST.equalsIgnoreCase(appLovinCommunicatorMessage.getTopic())) {
            Bundle messageData = appLovinCommunicatorMessage.getMessageData();
            Map a2 = i.a(messageData.getBundle("query_params"));
            Map map = BundleUtils.toMap(messageData.getBundle("post_body"));
            Map a3 = i.a(messageData.getBundle("headers"));
            String string = messageData.getString("id", "");
            if (!map.containsKey(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY)) {
                map.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.a.t());
            }
            this.a.N().a(new com.applovin.impl.sdk.network.f.a().a(messageData.getString("url")).b(messageData.getString("backup_url")).a(a2).c(map).b(a3).a(((Boolean) this.a.a(d.eR)).booleanValue()).c(string).a());
        }
    }
}
