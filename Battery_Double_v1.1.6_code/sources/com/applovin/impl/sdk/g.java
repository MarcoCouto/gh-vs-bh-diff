package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.impl.sdk.b.f;

public class g {
    public static Boolean a(Context context) {
        return a(f.j, context);
    }

    private static Boolean a(f<Boolean> fVar, Context context) {
        return (Boolean) com.applovin.impl.sdk.b.g.b(fVar, null, context);
    }

    private static boolean a(f<Boolean> fVar, Boolean bool, Context context) {
        Boolean a = a(fVar, context);
        com.applovin.impl.sdk.b.g.a(fVar, bool, context);
        return a == null || a != bool;
    }

    public static boolean a(boolean z, Context context) {
        return a(f.j, Boolean.valueOf(z), context);
    }

    public static Boolean b(Context context) {
        return a(f.k, context);
    }

    public static boolean b(boolean z, Context context) {
        return a(f.k, Boolean.valueOf(z), context);
    }

    public static Boolean c(Context context) {
        return a(f.l, context);
    }

    public static boolean c(boolean z, Context context) {
        return a(f.l, Boolean.valueOf(z), context);
    }
}
