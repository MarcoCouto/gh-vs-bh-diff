package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.d.a;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

abstract class r implements m, AppLovinNativeAdLoadListener {
    protected final j a;
    protected final p b;
    /* access modifiers changed from: private */
    public final Object c = new Object();
    private final Map<d, s> d = new HashMap();
    private final Map<d, s> e = new HashMap();
    /* access modifiers changed from: private */
    public final Map<d, Object> f = new HashMap();
    private final Set<d> g = new HashSet();

    r(j jVar) {
        this.a = jVar;
        this.b = jVar.v();
    }

    private void b(final d dVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        synchronized (this.c) {
            if (this.f.containsKey(dVar)) {
                this.b.d("PreloadManager", "Possibly missing prior registered preload callback.");
            }
            this.f.put(dVar, appLovinAdLoadListener);
        }
        final int intValue = ((Integer) this.a.a(com.applovin.impl.sdk.b.d.bb)).intValue();
        if (intValue > 0) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    synchronized (r.this.c) {
                        Object obj = r.this.f.get(dVar);
                        if (obj != null) {
                            r.this.f.remove(dVar);
                            StringBuilder sb = new StringBuilder();
                            sb.append("Load callback for zone ");
                            sb.append(dVar);
                            sb.append(" timed out after ");
                            sb.append(intValue);
                            sb.append(" seconds");
                            r.this.b.e("PreloadManager", sb.toString());
                            r.this.a(obj, dVar, AppLovinErrorCodes.FETCH_AD_TIMEOUT);
                        }
                    }
                }
            }, TimeUnit.SECONDS.toMillis((long) intValue));
        }
    }

    private s j(d dVar) {
        return (s) this.d.get(dVar);
    }

    private s k(d dVar) {
        return (s) this.e.get(dVar);
    }

    private boolean l(d dVar) {
        boolean z;
        synchronized (this.c) {
            s j = j(dVar);
            z = j != null && j.c();
        }
        return z;
    }

    private s m(d dVar) {
        synchronized (this.c) {
            s k = k(dVar);
            if (k != null && k.a() > 0) {
                return k;
            }
            s j = j(dVar);
            return j;
        }
    }

    private boolean n(d dVar) {
        boolean contains;
        synchronized (this.c) {
            contains = this.g.contains(dVar);
        }
        return contains;
    }

    /* access modifiers changed from: 0000 */
    public abstract d a(j jVar);

    /* access modifiers changed from: 0000 */
    public abstract a a(d dVar);

    /* access modifiers changed from: 0000 */
    public abstract void a(Object obj, d dVar, int i);

    /* access modifiers changed from: 0000 */
    public abstract void a(Object obj, j jVar);

    public void a(LinkedHashSet<d> linkedHashSet) {
        if (this.f != null && !this.f.isEmpty()) {
            synchronized (this.c) {
                Iterator it = this.f.keySet().iterator();
                while (it.hasNext()) {
                    d dVar = (d) it.next();
                    if (!dVar.i() && !linkedHashSet.contains(dVar)) {
                        Object obj = this.f.get(dVar);
                        it.remove();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to load ad for zone (");
                        sb.append(dVar.a());
                        sb.append("). Please check that the zone has been added to your AppLovin account and given at least 30 minutes to fully propagate.");
                        p.j("AppLovinAdService", sb.toString());
                        a(obj, dVar, -7);
                    }
                }
            }
        }
    }

    public boolean a(d dVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        boolean z;
        synchronized (this.c) {
            if (!n(dVar)) {
                b(dVar, appLovinAdLoadListener);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public void b(d dVar, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            i(dVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(j jVar) {
        Object obj;
        d a2 = a(jVar);
        synchronized (this.c) {
            obj = this.f.get(a2);
            this.f.remove(a2);
            this.g.add(a2);
            j(a2).a(jVar);
            StringBuilder sb = new StringBuilder();
            sb.append("Ad enqueued: ");
            sb.append(jVar);
            this.b.b("PreloadManager", sb.toString());
        }
        if (obj != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Called additional callback regarding ");
            sb2.append(jVar);
            this.b.b("PreloadManager", sb2.toString());
            a(obj, (j) new g(a2, this.a));
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Pulled ad from network and saved to preload cache: ");
        sb3.append(jVar);
        this.b.b("PreloadManager", sb3.toString());
    }

    public boolean b(d dVar) {
        return this.f.containsKey(dVar);
    }

    public j c(d dVar) {
        j f2;
        synchronized (this.c) {
            s m = m(dVar);
            f2 = m != null ? m.f() : null;
        }
        return f2;
    }

    /* access modifiers changed from: 0000 */
    public void c(d dVar, int i) {
        Object remove;
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to pre-load an ad of zone ");
        sb.append(dVar);
        sb.append(", error code ");
        sb.append(i);
        this.b.b("PreloadManager", sb.toString());
        synchronized (this.c) {
            remove = this.f.remove(dVar);
            this.g.add(dVar);
        }
        if (remove != null) {
            try {
                a(remove, dVar, i);
            } catch (Throwable th) {
                p.c("PreloadManager", "Encountered exception while invoking user callback", th);
            }
        }
    }

    public j d(d dVar) {
        j e2;
        synchronized (this.c) {
            s m = m(dVar);
            e2 = m != null ? m.e() : null;
        }
        return e2;
    }

    public j e(d dVar) {
        j jVar;
        p pVar;
        String str;
        StringBuilder sb;
        String str2;
        g gVar;
        synchronized (this.c) {
            s j = j(dVar);
            jVar = null;
            if (j != null) {
                s k = k(dVar);
                if (k.c()) {
                    gVar = new g(dVar, this.a);
                } else if (j.a() > 0) {
                    k.a(j.e());
                    gVar = new g(dVar, this.a);
                }
                jVar = gVar;
            }
        }
        if (jVar != null) {
            pVar = this.b;
            str = "PreloadManager";
            sb = new StringBuilder();
            str2 = "Retrieved ad of zone ";
        } else {
            pVar = this.b;
            str = "PreloadManager";
            sb = new StringBuilder();
            str2 = "Unable to retrieve ad of zone ";
        }
        sb.append(str2);
        sb.append(dVar);
        sb.append("...");
        pVar.b(str, sb.toString());
        return jVar;
    }

    public void f(d dVar) {
        if (dVar != null) {
            int i = 0;
            synchronized (this.c) {
                s j = j(dVar);
                if (j != null) {
                    i = j.b() - j.a();
                }
            }
            b(dVar, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0021, code lost:
        return r2;
     */
    public boolean g(d dVar) {
        synchronized (this.c) {
            s k = k(dVar);
            boolean z = true;
            if (k != null && k.a() > 0) {
                return true;
            }
            s j = j(dVar);
            if (j == null || j.d()) {
                z = false;
            }
        }
    }

    public void h(d dVar) {
        synchronized (this.c) {
            s j = j(dVar);
            if (j != null) {
                j.a(dVar.e());
            } else {
                this.d.put(dVar, new s(dVar.e()));
            }
            s k = k(dVar);
            if (k != null) {
                k.a(dVar.f());
            } else {
                this.e.put(dVar, new s(dVar.f()));
            }
        }
    }

    public void i(d dVar) {
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.bc)).booleanValue() && !l(dVar)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Preloading ad for zone ");
            sb.append(dVar);
            sb.append("...");
            this.b.b("PreloadManager", sb.toString());
            this.a.K().a(a(dVar), com.applovin.impl.sdk.d.r.a.MAIN, 500);
        }
    }
}
