package com.applovin.impl.sdk.ad;

import android.text.TextUtils;
import android.util.Base64;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    private final j a;
    private final String b;

    public enum a {
        UNSPECIFIED("UNSPECIFIED"),
        REGULAR("REGULAR"),
        AD_RESPONSE_JSON("AD_RESPONSE_JSON");
        
        private final String d;

        private a(String str) {
            this.d = str;
        }

        public String toString() {
            return this.d;
        }
    }

    public c(String str, j jVar) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Identifier is empty");
        } else if (jVar != null) {
            this.b = str;
            this.a = jVar;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    private String a(d<String> dVar) {
        for (String str : this.a.b((d) dVar)) {
            if (this.b.startsWith(str)) {
                return str;
            }
        }
        return null;
    }

    public String a() {
        return this.b;
    }

    public a b() {
        return a(d.aT) != null ? a.REGULAR : a(d.aU) != null ? a.AD_RESPONSE_JSON : a.UNSPECIFIED;
    }

    public String c() {
        String a2 = a(d.aT);
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        String a3 = a(d.aU);
        if (!TextUtils.isEmpty(a3)) {
            return a3;
        }
        return null;
    }

    public JSONObject d() {
        if (b() == a.AD_RESPONSE_JSON) {
            try {
                JSONObject jSONObject = new JSONObject(new String(Base64.decode(this.b.substring(c().length()), 0), "UTF-8"));
                StringBuilder sb = new StringBuilder();
                sb.append("Decoded token into ad response: ");
                sb.append(jSONObject);
                this.a.v().b("AdToken", sb.toString());
                return jSONObject;
            } catch (JSONException e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to decode token '");
                sb2.append(this.b);
                sb2.append("' into JSON");
                this.a.v().b("AdToken", sb2.toString(), e);
            } catch (Throwable th) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to process ad response from token '");
                sb3.append(this.b);
                sb3.append("'");
                this.a.v().b("AdToken", sb3.toString(), th);
            }
        }
        return null;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (this.b != null) {
            z = this.b.equals(cVar.b);
        } else if (cVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        if (this.b != null) {
            return this.b.hashCode();
        }
        return 0;
    }

    public String toString() {
        String a2 = n.a(32, this.b);
        StringBuilder sb = new StringBuilder();
        sb.append("AdToken{id=");
        sb.append(a2);
        sb.append(", type=");
        sb.append(b());
        sb.append('}');
        return sb.toString();
    }
}
