package com.applovin.impl.sdk.ad;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.j;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import org.json.JSONObject;

public final class g extends AppLovinAdBase {
    private AppLovinAd a;
    private final d b;

    public g(d dVar, j jVar) {
        super(new JSONObject(), new JSONObject(), b.UNKNOWN, jVar);
        this.b = dVar;
    }

    private AppLovinAd c() {
        return (AppLovinAd) this.sdk.T().c(this.b);
    }

    private String d() {
        d adZone = getAdZone();
        if (adZone == null || adZone.i()) {
            return null;
        }
        return adZone.a();
    }

    public AppLovinAd a() {
        return this.a;
    }

    public void a(AppLovinAd appLovinAd) {
        this.a = appLovinAd;
    }

    public AppLovinAd b() {
        return this.a != null ? this.a : c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AppLovinAd b2 = b();
        return b2 != null ? b2.equals(obj) : super.equals(obj);
    }

    public long getAdIdNumber() {
        AppLovinAd b2 = b();
        if (b2 != null) {
            return b2.getAdIdNumber();
        }
        return 0;
    }

    public d getAdZone() {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) b();
        return appLovinAdBase != null ? appLovinAdBase.getAdZone() : this.b;
    }

    public AppLovinAdSize getSize() {
        return getAdZone().b();
    }

    public b getSource() {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) b();
        return appLovinAdBase != null ? appLovinAdBase.getSource() : b.UNKNOWN;
    }

    public AppLovinAdType getType() {
        return getAdZone().c();
    }

    public String getZoneId() {
        if (this.b.i()) {
            return null;
        }
        return this.b.a();
    }

    public int hashCode() {
        AppLovinAd b2 = b();
        return b2 != null ? b2.hashCode() : super.hashCode();
    }

    public boolean isVideoAd() {
        AppLovinAd b2 = b();
        return b2 != null && b2.isVideoAd();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppLovinAd{ #");
        sb.append(getAdIdNumber());
        sb.append(", adType=");
        sb.append(getType());
        sb.append(", adSize=");
        sb.append(getSize());
        sb.append(", zoneId='");
        sb.append(d());
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
