package com.applovin.impl.sdk.ad;

import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import java.util.Iterator;
import java.util.LinkedHashSet;
import org.json.JSONArray;
import org.json.JSONObject;

public class e {
    private final j a;
    private final p b;
    private LinkedHashSet<d> c;
    private final Object d = new Object();
    private volatile boolean e;

    public e(j jVar) {
        this.a = jVar;
        this.b = jVar.v();
        this.c = b();
    }

    private LinkedHashSet<d> b() {
        LinkedHashSet<d> linkedHashSet = new LinkedHashSet<>();
        try {
            String str = (String) this.a.a(f.t);
            if (n.b(str)) {
                JSONArray jSONArray = new JSONArray(str);
                if (jSONArray.length() > 0) {
                    linkedHashSet = b(jSONArray);
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable to inflate json string: ");
                    sb.append(str);
                    this.b.b("AdZoneManager", sb.toString());
                }
            }
            if (!linkedHashSet.isEmpty()) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Retrieved persisted zones: ");
                sb2.append(linkedHashSet);
                this.b.b("AdZoneManager", sb2.toString());
                Iterator it = linkedHashSet.iterator();
                while (it.hasNext()) {
                    ((d) it.next()).a(this.a);
                }
            }
        } catch (Throwable th) {
            if (!linkedHashSet.isEmpty()) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Retrieved persisted zones: ");
                sb3.append(linkedHashSet);
                this.b.b("AdZoneManager", sb3.toString());
                Iterator it2 = linkedHashSet.iterator();
                while (it2.hasNext()) {
                    ((d) it2.next()).a(this.a);
                }
            }
            throw th;
        }
        return linkedHashSet;
    }

    private LinkedHashSet<d> b(JSONArray jSONArray) {
        LinkedHashSet<d> linkedHashSet = new LinkedHashSet<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject a2 = i.a(jSONArray, i, (JSONObject) null, this.a);
            StringBuilder sb = new StringBuilder();
            sb.append("Loading zone: ");
            sb.append(i.d(a2));
            sb.append("...");
            this.b.b("AdZoneManager", sb.toString());
            linkedHashSet.add(d.a(i.b(a2, "id", (String) null, this.a), a2, this.a));
        }
        return linkedHashSet;
    }

    private void c(JSONArray jSONArray) {
        if (((Boolean) this.a.a(d.dL)).booleanValue()) {
            this.b.b("AdZoneManager", "Persisting zones...");
            this.a.a(f.t, jSONArray.toString());
        }
    }

    public LinkedHashSet<d> a() {
        LinkedHashSet<d> linkedHashSet;
        synchronized (this.d) {
            linkedHashSet = this.c;
        }
        return linkedHashSet;
    }

    public LinkedHashSet<d> a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return new LinkedHashSet<>();
        }
        LinkedHashSet<d> linkedHashSet = new LinkedHashSet<>(jSONArray.length());
        LinkedHashSet<d> linkedHashSet2 = null;
        synchronized (this.d) {
            if (!this.e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Found ");
                sb.append(jSONArray.length());
                sb.append(" zone(s)...");
                this.b.b("AdZoneManager", sb.toString());
                linkedHashSet2 = b(jSONArray);
                linkedHashSet = new LinkedHashSet<>(linkedHashSet2);
                linkedHashSet.removeAll(this.c);
                this.c = linkedHashSet2;
                this.e = true;
            }
        }
        if (linkedHashSet2 != null) {
            c(jSONArray);
            this.b.b("AdZoneManager", "Finished loading zones");
        }
        return linkedHashSet;
    }

    public boolean a(d dVar) {
        boolean contains;
        synchronized (this.d) {
            contains = this.c.contains(dVar);
        }
        return contains;
    }
}
