package com.applovin.impl.sdk.ad;

import android.text.TextUtils;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.i;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.facebook.appevents.AppEventsConstants;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

public final class d {
    private static final Map<String, d> a = new HashMap();
    private static final Object b = new Object();
    private j c;
    private p d;
    private JSONObject e;
    private final String f;
    private String g;
    private AppLovinAdSize h;
    private AppLovinAdType i;

    private d(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str, j jVar) {
        if (!TextUtils.isEmpty(str) || !(appLovinAdType == null || appLovinAdSize == null)) {
            this.c = jVar;
            this.d = jVar != null ? jVar.v() : null;
            this.h = appLovinAdSize;
            this.i = appLovinAdType;
            if (!TextUtils.isEmpty(str)) {
                this.f = str.toLowerCase(Locale.ENGLISH);
                this.g = str.toLowerCase(Locale.ENGLISH);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(appLovinAdSize.getLabel());
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(appLovinAdType.getLabel());
            this.f = sb.toString().toLowerCase(Locale.ENGLISH);
            return;
        }
        throw new IllegalArgumentException("No zone identifier or type or size specified");
    }

    public static d a(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, j jVar) {
        return a(appLovinAdSize, appLovinAdType, null, jVar);
    }

    public static d a(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str, j jVar) {
        d dVar = new d(appLovinAdSize, appLovinAdType, str, jVar);
        synchronized (b) {
            String str2 = dVar.f;
            if (a.containsKey(str2)) {
                dVar = (d) a.get(str2);
            } else {
                a.put(str2, dVar);
            }
        }
        return dVar;
    }

    public static d a(String str, j jVar) {
        return a(null, null, str, jVar);
    }

    public static d a(String str, JSONObject jSONObject, j jVar) {
        d a2 = a(str, jVar);
        a2.e = jSONObject;
        return a2;
    }

    private <ST> com.applovin.impl.sdk.b.d<ST> a(String str, com.applovin.impl.sdk.b.d<ST> dVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.f);
        return this.c.a(sb.toString(), dVar);
    }

    private boolean a(com.applovin.impl.sdk.b.d<String> dVar, AppLovinAdSize appLovinAdSize) {
        return ((String) this.c.a(dVar)).toUpperCase(Locale.ENGLISH).contains(appLovinAdSize.getLabel());
    }

    public static d b(String str, j jVar) {
        return a(AppLovinAdSize.NATIVE, AppLovinAdType.NATIVE, str, jVar);
    }

    public static Collection<d> b(j jVar) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(8);
        Collections.addAll(linkedHashSet, new d[]{c(jVar), d(jVar), e(jVar), f(jVar), g(jVar), h(jVar)});
        return Collections.unmodifiableSet(linkedHashSet);
    }

    public static d c(j jVar) {
        return a(AppLovinAdSize.BANNER, AppLovinAdType.REGULAR, jVar);
    }

    public static d c(String str, j jVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, str, jVar);
    }

    public static d d(j jVar) {
        return a(AppLovinAdSize.MREC, AppLovinAdType.REGULAR, jVar);
    }

    public static d e(j jVar) {
        return a(AppLovinAdSize.LEADER, AppLovinAdType.REGULAR, jVar);
    }

    public static d f(j jVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.REGULAR, jVar);
    }

    public static d g(j jVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, jVar);
    }

    public static d h(j jVar) {
        return a(AppLovinAdSize.NATIVE, AppLovinAdType.NATIVE, jVar);
    }

    private boolean j() {
        try {
            if (!TextUtils.isEmpty(this.g)) {
                return true;
            }
            return AppLovinAdType.INCENTIVIZED.equals(c()) ? ((Boolean) this.c.a(com.applovin.impl.sdk.b.d.be)).booleanValue() : a(com.applovin.impl.sdk.b.d.bd, b());
        } catch (Throwable th) {
            this.d.b("AdZone", "Unable to safely test preload merge capability", th);
            return false;
        }
    }

    public String a() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public void a(j jVar) {
        this.c = jVar;
        this.d = jVar.v();
    }

    public AppLovinAdSize b() {
        if (this.h == null && i.a(this.e, "ad_size")) {
            this.h = AppLovinAdSize.fromString(i.b(this.e, "ad_size", (String) null, this.c));
        }
        return this.h;
    }

    public AppLovinAdType c() {
        if (this.i == null && i.a(this.e, AppEventsConstants.EVENT_PARAM_AD_TYPE)) {
            this.i = AppLovinAdType.fromString(i.b(this.e, AppEventsConstants.EVENT_PARAM_AD_TYPE, (String) null, this.c));
        }
        return this.i;
    }

    public boolean d() {
        return AppLovinAdSize.NATIVE.equals(b()) && AppLovinAdType.NATIVE.equals(c());
    }

    public int e() {
        if (i.a(this.e, "capacity")) {
            return i.b(this.e, "capacity", 0, this.c);
        }
        if (!TextUtils.isEmpty(this.g)) {
            return d() ? ((Integer) this.c.a(com.applovin.impl.sdk.b.d.bt)).intValue() : ((Integer) this.c.a(com.applovin.impl.sdk.b.d.bs)).intValue();
        }
        return ((Integer) this.c.a(a("preload_capacity_", com.applovin.impl.sdk.b.d.bh))).intValue();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.f.equalsIgnoreCase(((d) obj).f);
    }

    public int f() {
        if (i.a(this.e, "extended_capacity")) {
            return i.b(this.e, "extended_capacity", 0, this.c);
        }
        if (TextUtils.isEmpty(this.g)) {
            return ((Integer) this.c.a(a("extended_preload_capacity_", com.applovin.impl.sdk.b.d.bn))).intValue();
        } else if (d()) {
            return 0;
        } else {
            return ((Integer) this.c.a(com.applovin.impl.sdk.b.d.bu)).intValue();
        }
    }

    public int g() {
        return i.b(this.e, "preload_count", 0, this.c);
    }

    public boolean h() {
        boolean z = false;
        if (!((Boolean) this.c.a(com.applovin.impl.sdk.b.d.bc)).booleanValue() || !j()) {
            return false;
        }
        if (TextUtils.isEmpty(this.g)) {
            com.applovin.impl.sdk.b.d a2 = a("preload_merge_init_tasks_", null);
            if (a2 != null && ((Boolean) this.c.a(a2)).booleanValue() && e() > 0) {
                z = true;
            }
            return z;
        } else if (this.e != null && g() == 0) {
            return false;
        } else {
            String upperCase = ((String) this.c.a(com.applovin.impl.sdk.b.d.bd)).toUpperCase(Locale.ENGLISH);
            return (upperCase.contains(AppLovinAdSize.INTERSTITIAL.getLabel()) || upperCase.contains(AppLovinAdSize.BANNER.getLabel()) || upperCase.contains(AppLovinAdSize.MREC.getLabel()) || upperCase.contains(AppLovinAdSize.LEADER.getLabel())) ? ((Boolean) this.c.a(com.applovin.impl.sdk.b.d.bB)).booleanValue() : this.c.W().a(this) && g() > 0 && ((Boolean) this.c.a(com.applovin.impl.sdk.b.d.dK)).booleanValue();
        }
    }

    public int hashCode() {
        return this.f.hashCode();
    }

    public boolean i() {
        return b(this.c).contains(this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdZone{identifier=");
        sb.append(this.f);
        sb.append(", zoneObject=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
