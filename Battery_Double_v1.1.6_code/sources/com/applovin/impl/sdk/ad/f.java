package com.applovin.impl.sdk.ad;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.net.Uri;
import android.support.v4.view.ViewCompat;
import com.applovin.impl.adview.s;
import com.applovin.impl.adview.w;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public abstract class f extends AppLovinAdBase {
    private final AtomicBoolean a = new AtomicBoolean();
    private final AtomicBoolean b = new AtomicBoolean();
    private final AtomicReference<c> c = new AtomicReference<>();
    private List<com.applovin.impl.sdk.c.a> d;
    private List<com.applovin.impl.sdk.c.a> e;
    private List<com.applovin.impl.sdk.c.a> f;
    private List<com.applovin.impl.sdk.c.a> g;

    public enum a {
        UNSPECIFIED,
        DISMISS,
        DO_NOT_DISMISS
    }

    public enum b {
        DEFAULT,
        ACTIVITY_PORTRAIT,
        ACTIVITY_LANDSCAPE
    }

    public f(JSONObject jSONObject, JSONObject jSONObject2, b bVar, j jVar) {
        super(jSONObject, jSONObject2, bVar, jVar);
    }

    private String a() {
        String stringFromAdObject = getStringFromAdObject("video_end_url", null);
        if (stringFromAdObject != null) {
            return stringFromAdObject.replace("{CLCODE}", getClCode());
        }
        return null;
    }

    private List<com.applovin.impl.sdk.c.a> a(PointF pointF, boolean z) {
        List<com.applovin.impl.sdk.c.a> a2;
        synchronized (this.adObjectLock) {
            a2 = q.a("click_tracking_urls", this.adObject, c(pointF, z), b(pointF, z), this.sdk);
        }
        return a2;
    }

    private com.applovin.impl.adview.h.a b(boolean z) {
        return z ? com.applovin.impl.adview.h.a.WhiteXOnTransparentGrey : com.applovin.impl.adview.h.a.WhiteXOnOpaqueBlack;
    }

    private String b(PointF pointF, boolean z) {
        String stringFromAdObject = getStringFromAdObject("click_tracking_url", null);
        Map c2 = c(pointF, z);
        if (stringFromAdObject != null) {
            return n.a(stringFromAdObject, c2);
        }
        return null;
    }

    private Map<String, String> c(PointF pointF, boolean z) {
        Point a2 = g.a(this.sdk.D());
        HashMap hashMap = new HashMap(5);
        hashMap.put("{CLCODE}", getClCode());
        hashMap.put("{CLICK_X}", String.valueOf(pointF.x));
        hashMap.put("{CLICK_Y}", String.valueOf(pointF.y));
        hashMap.put("{SCREEN_WIDTH}", String.valueOf(a2.x));
        hashMap.put("{SCREEN_HEIGHT}", String.valueOf(a2.y));
        hashMap.put("{IS_VIDEO_CLICK}", String.valueOf(z));
        return hashMap;
    }

    public int A() {
        return getIntFromAdObject("countdown_length", 0);
    }

    public int B() {
        int parseColor = Color.parseColor("#C8FFFFFF");
        String stringFromAdObject = getStringFromAdObject("countdown_color", null);
        if (!n.b(stringFromAdObject)) {
            return parseColor;
        }
        try {
            return Color.parseColor(stringFromAdObject);
        } catch (Throwable th) {
            this.sdk.v().b("DirectAd", "Unable to parse countdown color", th);
            return parseColor;
        }
    }

    public int C() {
        String stringFromAdObject = getStringFromAdObject("video_background_color", null);
        if (n.b(stringFromAdObject)) {
            try {
                return Color.parseColor(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return ViewCompat.MEASURED_STATE_MASK;
    }

    public int D() {
        int i = hasVideoUrl() ? ViewCompat.MEASURED_STATE_MASK : -1157627904;
        String stringFromAdObject = getStringFromAdObject("graphic_background_color", null);
        if (!n.b(stringFromAdObject)) {
            return i;
        }
        try {
            return Color.parseColor(stringFromAdObject);
        } catch (Throwable unused) {
            return i;
        }
    }

    public a E() {
        String stringFromAdObject = getStringFromAdObject("poststitial_dismiss_type", null);
        if (n.b(stringFromAdObject)) {
            if (TapjoyConstants.TJC_FULLSCREEN_AD_DISMISS_URL.equalsIgnoreCase(stringFromAdObject)) {
                return a.DISMISS;
            }
            if ("no_dismiss".equalsIgnoreCase(stringFromAdObject)) {
                return a.DO_NOT_DISMISS;
            }
        }
        return a.UNSPECIFIED;
    }

    public List<String> F() {
        String stringFromAdObject = getStringFromAdObject("resource_cache_prefix", null);
        return stringFromAdObject != null ? e.a(stringFromAdObject) : this.sdk.b((d) d.bM);
    }

    public String G() {
        return getStringFromAdObject("cache_prefix", null);
    }

    public boolean H() {
        return getBooleanFromAdObject("daome", Boolean.valueOf(true));
    }

    public boolean I() {
        return getBooleanFromAdObject("utpfc", Boolean.valueOf(false));
    }

    public boolean J() {
        return getBooleanFromAdObject("sscomt", Boolean.valueOf(false));
    }

    public String K() {
        return getStringFromFullResponse("event_id", null);
    }

    public boolean L() {
        return getBooleanFromAdObject("progress_bar_enabled", Boolean.valueOf(false));
    }

    public int M() {
        String stringFromAdObject = getStringFromAdObject("progress_bar_color", "#C8FFFFFF");
        if (n.b(stringFromAdObject)) {
            try {
                return Color.parseColor(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return 0;
    }

    public boolean N() {
        return getBooleanFromAdObject("vs_buffer_indicator_enabled", Boolean.valueOf(false));
    }

    public boolean O() {
        return getBooleanFromAdObject("vs_buffer_indicator_initial_load_enabled", Boolean.valueOf(false));
    }

    public int P() {
        return getIntFromAdObject("vs_buffer_indicator_style", 16842874);
    }

    public int Q() {
        String stringFromAdObject = getStringFromAdObject("vs_buffer_indicator_color", null);
        if (n.b(stringFromAdObject)) {
            try {
                return Color.parseColor(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return -1;
    }

    public int R() {
        int parseColor = Color.parseColor("#66000000");
        String stringFromAdObject = getStringFromAdObject("vs_buffer_indicator_bg_color", null);
        if (!n.b(stringFromAdObject)) {
            return parseColor;
        }
        try {
            return Color.parseColor(stringFromAdObject);
        } catch (Throwable unused) {
            return parseColor;
        }
    }

    public boolean S() {
        return getBooleanFromAdObject("clear_dismissible", Boolean.valueOf(false));
    }

    public int T() {
        int a2;
        synchronized (this.adObjectLock) {
            a2 = q.a(this.adObject);
        }
        return a2;
    }

    public int U() {
        return getIntFromAdObject("poststitial_shown_forward_delay_millis", -1);
    }

    public int V() {
        return getIntFromAdObject("poststitial_dismiss_forward_delay_millis", -1);
    }

    public boolean W() {
        return getBooleanFromAdObject("should_apply_mute_setting_to_poststitial", Boolean.valueOf(false));
    }

    public boolean X() {
        return getBooleanFromAdObject("should_forward_close_button_tapped_to_poststitial", Boolean.valueOf(false));
    }

    public boolean Y() {
        return getBooleanFromAdObject("vkuv", Boolean.valueOf(false));
    }

    public boolean Z() {
        return getBooleanFromAdObject("forward_lifecycle_events_to_webview", Boolean.valueOf(false));
    }

    /* access modifiers changed from: protected */
    public com.applovin.impl.adview.h.a a(int i) {
        return i == 1 ? com.applovin.impl.adview.h.a.WhiteXOnTransparentGrey : i == 2 ? com.applovin.impl.adview.h.a.Invisible : com.applovin.impl.adview.h.a.WhiteXOnOpaqueBlack;
    }

    public List<com.applovin.impl.sdk.c.a> a(PointF pointF) {
        return a(pointF, false);
    }

    public void a(c cVar) {
        this.c.set(cVar);
    }

    public void a(boolean z) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("html_resources_cached", z);
            }
        } catch (Throwable unused) {
        }
    }

    public List<String> aA() {
        return e.a(getStringFromAdObject("wls", ""));
    }

    public List<String> aB() {
        return e.a(getStringFromAdObject("wlh", null));
    }

    public boolean aC() {
        return getBooleanFromAdObject("tvv", Boolean.valueOf(false));
    }

    public boolean aD() {
        return getBooleanFromAdObject("ibbdfs", Boolean.valueOf(false));
    }

    public boolean aE() {
        return getBooleanFromAdObject("ibbdfc", Boolean.valueOf(false));
    }

    public Uri aF() {
        String stringFromAdObject = getStringFromAdObject("mute_image", null);
        if (n.b(stringFromAdObject)) {
            try {
                return Uri.parse(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    public Uri aG() {
        String stringFromAdObject = getStringFromAdObject("unmute_image", "");
        if (n.b(stringFromAdObject)) {
            try {
                return Uri.parse(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    public boolean aH() {
        return this.b.get();
    }

    public void aI() {
        this.b.set(true);
    }

    public c aJ() {
        return (c) this.c.getAndSet(null);
    }

    public int aa() {
        return getIntFromAdObject("close_button_size", ((Integer) this.sdk.a(d.cD)).intValue());
    }

    public int ab() {
        return getIntFromAdObject("close_button_top_margin", ((Integer) this.sdk.a(d.cE)).intValue());
    }

    public int ac() {
        return getIntFromAdObject("close_button_horizontal_margin", ((Integer) this.sdk.a(d.cC)).intValue());
    }

    public boolean ad() {
        return getBooleanFromAdObject("lhs_close_button", (Boolean) this.sdk.a(d.cB));
    }

    public boolean ae() {
        return getBooleanFromAdObject("lhs_skip_button", (Boolean) this.sdk.a(d.cU));
    }

    public boolean af() {
        return getBooleanFromAdObject("stop_video_player_after_poststitial_render", Boolean.valueOf(false));
    }

    public boolean ag() {
        return getBooleanFromAdObject("unhide_adview_on_render", Boolean.valueOf(false));
    }

    public long ah() {
        long longFromAdObject = getLongFromAdObject("report_reward_duration", -1);
        if (longFromAdObject >= 0) {
            return TimeUnit.SECONDS.toMillis(longFromAdObject);
        }
        return -1;
    }

    public int ai() {
        return getIntFromAdObject("report_reward_percent", -1);
    }

    public boolean aj() {
        return getBooleanFromAdObject("report_reward_percent_include_close_delay", Boolean.valueOf(true));
    }

    public AtomicBoolean ak() {
        return this.a;
    }

    public boolean al() {
        return getBooleanFromAdObject("show_skip_button_on_click", Boolean.valueOf(false));
    }

    public boolean am() {
        return getBooleanFromAdObject("show_nia", Boolean.valueOf(false));
    }

    public String an() {
        return getStringFromAdObject("nia_title", "");
    }

    public String ao() {
        return getStringFromAdObject("nia_message", "");
    }

    public String ap() {
        return getStringFromAdObject("nia_button_title", "");
    }

    public boolean aq() {
        return getBooleanFromAdObject("avoms", Boolean.valueOf(false));
    }

    public List<com.applovin.impl.sdk.c.a> ar() {
        if (this.d != null) {
            return this.d;
        }
        synchronized (this.adObjectLock) {
            this.d = q.a("video_end_urls", this.adObject, getClCode(), a(), this.sdk);
        }
        return this.d;
    }

    public List<com.applovin.impl.sdk.c.a> as() {
        if (this.e != null) {
            return this.e;
        }
        synchronized (this.adObjectLock) {
            this.e = q.a("ad_closed_urls", this.adObject, getClCode(), (String) null, this.sdk);
        }
        return this.e;
    }

    public List<com.applovin.impl.sdk.c.a> at() {
        if (this.f != null) {
            return this.f;
        }
        synchronized (this.adObjectLock) {
            this.f = q.a("app_killed_urls", this.adObject, getClCode(), (String) null, this.sdk);
        }
        return this.f;
    }

    public List<com.applovin.impl.sdk.c.a> au() {
        if (this.g != null) {
            return this.g;
        }
        synchronized (this.adObjectLock) {
            this.g = q.a("imp_urls", this.adObject, getClCode(), (String) null, this.sdk);
        }
        return this.g;
    }

    public boolean av() {
        return getBooleanFromAdObject("playback_requires_user_action", Boolean.valueOf(true));
    }

    public boolean aw() {
        return getBooleanFromAdObject("sanitize_webview", Boolean.valueOf(false));
    }

    public String ax() {
        String stringFromAdObject = getStringFromAdObject("base_url", "/");
        if ("null".equalsIgnoreCase(stringFromAdObject)) {
            return null;
        }
        return stringFromAdObject;
    }

    public boolean ay() {
        return getBooleanFromAdObject("web_contents_debugging_enabled", Boolean.valueOf(false));
    }

    public w az() {
        JSONObject jsonObjectFromAdObject = getJsonObjectFromAdObject("web_view_settings", null);
        if (jsonObjectFromAdObject != null) {
            return new w(jsonObjectFromAdObject, this.sdk);
        }
        return null;
    }

    public List<com.applovin.impl.sdk.c.a> b(PointF pointF) {
        List<com.applovin.impl.sdk.c.a> a2;
        synchronized (this.adObjectLock) {
            a2 = q.a("video_click_tracking_urls", this.adObject, c(pointF, true), (String) null, this.sdk);
        }
        return a2.isEmpty() ? a(pointF, true) : a2;
    }

    public void b(Uri uri) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("mute_image", uri);
            }
        } catch (Throwable unused) {
        }
    }

    public boolean b() {
        this.sdk.v().e("DirectAd", "Attempting to invoke isVideoStream() from base ad class");
        return false;
    }

    public void c(Uri uri) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("unmute_image", uri);
            }
        } catch (Throwable unused) {
        }
    }

    public Uri d() {
        this.sdk.v().e("DirectAd", "Attempting to invoke getVideoUri() from base ad class");
        return null;
    }

    public Uri f() {
        this.sdk.v().e("DirectAd", "Attempting to invoke getClickDestinationUri() from base ad class");
        return null;
    }

    public Uri g() {
        this.sdk.v().e("DirectAd", "Attempting to invoke getVideoClickDestinationUri() from base ad class");
        return null;
    }

    public b l() {
        String upperCase = getStringFromAdObject("ad_target", b.DEFAULT.toString()).toUpperCase(Locale.ENGLISH);
        return "ACTIVITY_PORTRAIT".equalsIgnoreCase(upperCase) ? b.ACTIVITY_PORTRAIT : "ACTIVITY_LANDSCAPE".equalsIgnoreCase(upperCase) ? b.ACTIVITY_LANDSCAPE : b.DEFAULT;
    }

    public float m() {
        return getFloatFromAdObject("close_delay", 0.0f);
    }

    public float n() {
        return getFloatFromAdObject("close_delay_graphic", 0.0f);
    }

    public com.applovin.impl.adview.h.a o() {
        int intFromAdObject = getIntFromAdObject("close_style", -1);
        return intFromAdObject == -1 ? b(hasVideoUrl()) : a(intFromAdObject);
    }

    public com.applovin.impl.adview.h.a p() {
        int intFromAdObject = getIntFromAdObject("skip_style", -1);
        return intFromAdObject == -1 ? o() : a(intFromAdObject);
    }

    public boolean q() {
        return getBooleanFromAdObject("dismiss_on_skip", Boolean.valueOf(false));
    }

    public boolean r() {
        return getBooleanFromAdObject("html_resources_cached", Boolean.valueOf(false));
    }

    public String s() {
        JSONObject jsonObjectFromAdObject = getJsonObjectFromAdObject("video_button_properties", null);
        return jsonObjectFromAdObject != null ? i.b(jsonObjectFromAdObject, "video_button_html", "", this.sdk) : "";
    }

    public s t() {
        return new s(getJsonObjectFromAdObject("video_button_properties", null), this.sdk);
    }

    public boolean u() {
        return getBooleanFromAdObject("video_clickable", Boolean.valueOf(false));
    }

    public boolean v() {
        return getBooleanFromAdObject("accelerate_hardware", Boolean.valueOf(false));
    }

    public boolean w() {
        return getBooleanFromAdObject("keep_screen_on", Boolean.valueOf(false));
    }

    public boolean x() {
        return getBooleanFromAdObject("hide_close_on_exit_graphic", Boolean.valueOf(false));
    }

    public boolean y() {
        return getBooleanFromAdObject("hide_close_on_exit", Boolean.valueOf(false));
    }

    public boolean z() {
        return getBooleanFromAdObject("lock_current_orientation", Boolean.valueOf(false));
    }
}
