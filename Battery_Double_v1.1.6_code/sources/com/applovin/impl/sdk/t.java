package com.applovin.impl.sdk;

import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.utils.i;
import org.json.JSONObject;

public class t {
    private final j a;
    private final JSONObject b;
    private final Object c = new Object();

    public t(j jVar) {
        this.a = jVar;
        this.b = i.a((String) jVar.b(f.s, "{}"), new JSONObject(), jVar);
    }

    public Integer a(String str) {
        Integer valueOf;
        synchronized (this.c) {
            if (this.b.has(str)) {
                i.a(this.b, str, i.b(this.b, str, 0, this.a) + 1, this.a);
            } else {
                i.a(this.b, str, 1, this.a);
            }
            this.a.a(f.s, this.b.toString());
            valueOf = Integer.valueOf(i.b(this.b, str, 0, this.a));
        }
        return valueOf;
    }
}
