package com.applovin.impl.sdk.a;

import android.content.Context;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.AppLovinAdServiceImpl;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.i;
import com.applovin.impl.sdk.d.ad;
import com.applovin.impl.sdk.d.y;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.SoftReference;
import java.util.Map;

public class a {
    protected final j a;
    protected final AppLovinAdServiceImpl b;
    /* access modifiers changed from: private */
    public AppLovinAd c;
    private String d;
    private SoftReference<AppLovinAdLoadListener> e;
    private final Object f = new Object();
    private volatile String g;
    /* access modifiers changed from: private */
    public volatile boolean h = false;

    /* renamed from: com.applovin.impl.sdk.a.a$a reason: collision with other inner class name */
    private class C0010a implements AppLovinAdLoadListener {
        /* access modifiers changed from: private */
        public final AppLovinAdLoadListener b;

        C0010a(AppLovinAdLoadListener appLovinAdLoadListener) {
            this.b = appLovinAdLoadListener;
        }

        public void adReceived(final AppLovinAd appLovinAd) {
            a.this.c = appLovinAd;
            if (this.b != null) {
                AppLovinSdkUtils.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            C0010a.this.b.adReceived(appLovinAd);
                        } catch (Throwable th) {
                            p.c("AppLovinIncentivizedInterstitial", "Unable to notify ad listener about a newly loaded ad", th);
                        }
                    }
                });
            }
        }

        public void failedToReceiveAd(final int i) {
            if (this.b != null) {
                AppLovinSdkUtils.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            C0010a.this.b.failedToReceiveAd(i);
                        } catch (Throwable th) {
                            p.c("AppLovinIncentivizedInterstitial", "Unable to notify listener about ad load failure", th);
                        }
                    }
                });
            }
        }
    }

    private class b implements i, AppLovinAdClickListener, AppLovinAdRewardListener, AppLovinAdVideoPlaybackListener {
        private final AppLovinAdDisplayListener b;
        private final AppLovinAdClickListener c;
        private final AppLovinAdVideoPlaybackListener d;
        private final AppLovinAdRewardListener e;

        private b(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
            this.b = appLovinAdDisplayListener;
            this.c = appLovinAdClickListener;
            this.d = appLovinAdVideoPlaybackListener;
            this.e = appLovinAdRewardListener;
        }

        private void a(f fVar) {
            int i;
            String str;
            if (!n.b(a.this.e()) || !a.this.h) {
                fVar.aI();
                if (a.this.h) {
                    str = "network_timeout";
                    i = AppLovinErrorCodes.INCENTIVIZED_SERVER_TIMEOUT;
                } else {
                    str = "user_closed_video";
                    i = AppLovinErrorCodes.INCENTIVIZED_USER_CLOSED_VIDEO;
                }
                fVar.a(c.a(str));
                com.applovin.impl.sdk.utils.j.a(this.e, (AppLovinAd) fVar, i);
            }
            a.this.a((AppLovinAd) fVar);
            com.applovin.impl.sdk.utils.j.b(this.b, (AppLovinAd) fVar);
            if (!fVar.ak().getAndSet(true)) {
                a.this.a.K().a((com.applovin.impl.sdk.d.a) new y(fVar, a.this.a), com.applovin.impl.sdk.d.r.a.REWARD);
            }
        }

        public void adClicked(AppLovinAd appLovinAd) {
            com.applovin.impl.sdk.utils.j.a(this.c, appLovinAd);
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            com.applovin.impl.sdk.utils.j.a(this.b, appLovinAd);
        }

        public void adHidden(AppLovinAd appLovinAd) {
            if (appLovinAd instanceof g) {
                appLovinAd = ((g) appLovinAd).a();
            }
            if (appLovinAd instanceof f) {
                a((f) appLovinAd);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Something is terribly wrong. Received `adHidden` callback for invalid ad of type: ");
            sb.append(appLovinAd);
            a.this.a.v().e("IncentivizedAdController", sb.toString());
        }

        public void onAdDisplayFailed(String str) {
            com.applovin.impl.sdk.utils.j.a(this.b, str);
        }

        public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
        }

        public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("quota_exceeded");
            com.applovin.impl.sdk.utils.j.b(this.e, appLovinAd, map);
        }

        public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("rejected");
            com.applovin.impl.sdk.utils.j.c(this.e, appLovinAd, map);
        }

        public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("accepted");
            com.applovin.impl.sdk.utils.j.a(this.e, appLovinAd, map);
        }

        public void validationRequestFailed(AppLovinAd appLovinAd, int i) {
            a.this.a("network_timeout");
            com.applovin.impl.sdk.utils.j.a(this.e, appLovinAd, i);
        }

        public void videoPlaybackBegan(AppLovinAd appLovinAd) {
            com.applovin.impl.sdk.utils.j.a(this.d, appLovinAd);
        }

        public void videoPlaybackEnded(AppLovinAd appLovinAd, double d2, boolean z) {
            com.applovin.impl.sdk.utils.j.a(this.d, appLovinAd, d2, z);
            a.this.h = z;
        }
    }

    public a(String str, AppLovinSdk appLovinSdk) {
        this.a = q.a(appLovinSdk);
        this.b = (AppLovinAdServiceImpl) appLovinSdk.getAdService();
        this.d = str;
    }

    private void a(AppLovinAdBase appLovinAdBase, Context context, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (!appLovinAdBase.getType().equals(AppLovinAdType.INCENTIVIZED)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to render an ad of type ");
            sb.append(appLovinAdBase.getType());
            sb.append(" in an Incentivized Ad interstitial.");
            this.a.v().e("IncentivizedAdController", sb.toString());
            a(appLovinAdBase, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener);
            return;
        }
        AppLovinAd a2 = q.a((AppLovinAd) appLovinAdBase, this.a);
        if (a2 != null) {
            AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.a.S(), context);
            b bVar = new b(appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
            create.setAdDisplayListener(bVar);
            create.setAdVideoPlaybackListener(bVar);
            create.setAdClickListener(bVar);
            create.showAndRender(a2);
            if (a2 instanceof f) {
                a((f) a2, (AppLovinAdRewardListener) bVar);
            }
        } else {
            a(appLovinAdBase, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener);
        }
    }

    private void a(f fVar, AppLovinAdRewardListener appLovinAdRewardListener) {
        this.a.K().a((com.applovin.impl.sdk.d.a) new ad(fVar, appLovinAdRewardListener, this.a), com.applovin.impl.sdk.d.r.a.REWARD);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        if (this.c != null) {
            if (this.c instanceof g) {
                if (appLovinAd != ((g) this.c).a()) {
                    return;
                }
            } else if (appLovinAd != this.c) {
                return;
            }
            this.c = null;
        }
    }

    private void a(AppLovinAd appLovinAd, Context context, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (appLovinAd == null) {
            appLovinAd = this.c;
        }
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) appLovinAd;
        if (appLovinAdBase != null) {
            a(appLovinAdBase, context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
            return;
        }
        p.j("IncentivizedAdController", "Skipping incentivized video playback: user attempted to play an incentivized video before one was preloaded.");
        d();
    }

    private void a(AppLovinAd appLovinAd, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.a.L().a(com.applovin.impl.sdk.c.g.j);
        com.applovin.impl.sdk.utils.j.a(appLovinAdVideoPlaybackListener, appLovinAd, Utils.DOUBLE_EPSILON, false);
        com.applovin.impl.sdk.utils.j.b(appLovinAdDisplayListener, appLovinAd);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        synchronized (this.f) {
            this.g = str;
        }
    }

    private void b(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.b.loadNextIncentivizedAd(this.d, appLovinAdLoadListener);
    }

    private void d() {
        if (this.e != null) {
            AppLovinAdLoadListener appLovinAdLoadListener = (AppLovinAdLoadListener) this.e.get();
            if (appLovinAdLoadListener != null) {
                appLovinAdLoadListener.failedToReceiveAd(AppLovinErrorCodes.INCENTIVIZED_NO_AD_PRELOADED);
            }
        }
    }

    /* access modifiers changed from: private */
    public String e() {
        String str;
        synchronized (this.f) {
            str = this.g;
        }
        return str;
    }

    private AppLovinAdRewardListener f() {
        return new AppLovinAdRewardListener() {
            public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
                a.this.a.v().b("IncentivizedAdController", "User declined to view");
            }

            public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
                StringBuilder sb = new StringBuilder();
                sb.append("User over quota: ");
                sb.append(map);
                a.this.a.v().b("IncentivizedAdController", sb.toString());
            }

            public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
                StringBuilder sb = new StringBuilder();
                sb.append("Reward rejected: ");
                sb.append(map);
                a.this.a.v().b("IncentivizedAdController", sb.toString());
            }

            public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
                StringBuilder sb = new StringBuilder();
                sb.append("Reward validated: ");
                sb.append(map);
                a.this.a.v().b("IncentivizedAdController", sb.toString());
            }

            public void validationRequestFailed(AppLovinAd appLovinAd, int i) {
                StringBuilder sb = new StringBuilder();
                sb.append("Reward validation failed: ");
                sb.append(i);
                a.this.a.v().b("IncentivizedAdController", sb.toString());
            }
        };
    }

    public void a(AppLovinAd appLovinAd, Context context, String str, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (appLovinAdRewardListener == null) {
            appLovinAdRewardListener = f();
        }
        a(appLovinAd, context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
    }

    public void a(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.a.v().b("IncentivizedAdController", "User requested preload of incentivized ad...");
        this.e = new SoftReference<>(appLovinAdLoadListener);
        if (a()) {
            p.j("IncentivizedAdController", "Attempted to call preloadAndNotify: while an ad was already loaded or currently being played. Do not call preloadAndNotify: again until the last ad has been closed (adHidden).");
            if (appLovinAdLoadListener != null) {
                appLovinAdLoadListener.adReceived(this.c);
            }
            return;
        }
        b((AppLovinAdLoadListener) new C0010a(appLovinAdLoadListener));
    }

    public boolean a() {
        return this.c != null;
    }

    public String b() {
        return this.d;
    }

    public void c() {
    }
}
