package com.applovin.impl.sdk.c;

import android.annotation.TargetApi;
import android.app.Activity;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.c.c.b;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.h;

public class d {
    private final j a;
    private final h b;
    private final b c;
    private final Object d = new Object();
    private final long e;
    private long f;
    private long g;
    private long h;
    private long i;
    private boolean j;

    public d(AppLovinAdBase appLovinAdBase, j jVar) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (jVar != null) {
            this.a = jVar;
            this.b = jVar.L();
            this.c = jVar.X().a(appLovinAdBase);
            this.c.a(b.a, (long) appLovinAdBase.getSource().ordinal()).a();
            this.e = appLovinAdBase.getCreatedAtMillis();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static void a(long j2, AppLovinAdBase appLovinAdBase, j jVar) {
        if (appLovinAdBase != null && jVar != null) {
            jVar.X().a(appLovinAdBase).a(b.b, j2).a();
        }
    }

    public static void a(AppLovinAdBase appLovinAdBase, j jVar) {
        if (appLovinAdBase != null && jVar != null) {
            jVar.X().a(appLovinAdBase).a(b.c, appLovinAdBase.getFetchLatencyMillis()).a(b.d, appLovinAdBase.getFetchResponseSize()).a();
        }
    }

    private void a(b bVar) {
        synchronized (this.d) {
            if (this.f > 0) {
                this.c.a(bVar, System.currentTimeMillis() - this.f).a();
            }
        }
    }

    public static void a(e eVar, AppLovinAdBase appLovinAdBase, j jVar) {
        if (appLovinAdBase != null && jVar != null && eVar != null) {
            jVar.X().a(appLovinAdBase).a(b.e, eVar.c()).a(b.f, eVar.d()).a(b.v, eVar.g()).a(b.w, eVar.h()).a(b.z, eVar.b() ? 1 : 0).a();
        }
    }

    @TargetApi(24)
    public void a() {
        this.c.a(b.j, this.b.a(g.b)).a(b.i, this.b.a(g.d));
        synchronized (this.d) {
            long j2 = 0;
            if (this.e > 0) {
                this.f = System.currentTimeMillis();
                long G = this.f - this.a.G();
                long j3 = this.f - this.e;
                long j4 = h.a(this.a.D()) ? 1 : 0;
                Activity a2 = this.a.aa().a();
                if (g.f() && a2 != null && a2.isInMultiWindowMode()) {
                    j2 = 1;
                }
                this.c.a(b.h, G).a(b.g, j3).a(b.p, j4).a(b.A, j2);
            }
        }
        this.c.a();
    }

    public void a(long j2) {
        this.c.a(b.r, j2).a();
    }

    public void b() {
        synchronized (this.d) {
            if (this.g < 1) {
                this.g = System.currentTimeMillis();
                if (this.f > 0) {
                    this.c.a(b.m, this.g - this.f).a();
                }
            }
        }
    }

    public void b(long j2) {
        this.c.a(b.q, j2).a();
    }

    public void c() {
        a(b.k);
    }

    public void c(long j2) {
        this.c.a(b.s, j2).a();
    }

    public void d() {
        a(b.n);
    }

    public void d(long j2) {
        synchronized (this.d) {
            if (this.h < 1) {
                this.h = j2;
                this.c.a(b.t, j2).a();
            }
        }
    }

    public void e() {
        a(b.o);
    }

    public void e(long j2) {
        synchronized (this.d) {
            if (!this.j) {
                this.j = true;
                this.c.a(b.x, j2).a();
            }
        }
    }

    public void f() {
        a(b.l);
    }

    public void g() {
        this.c.a(b.u, 1).a();
    }

    public void h() {
        this.c.a(b.B).a();
    }

    public void i() {
        synchronized (this.d) {
            if (this.i < 1) {
                this.i = System.currentTimeMillis();
                if (this.f > 0) {
                    this.c.a(b.y, this.i - this.f).a();
                }
            }
        }
    }
}
