package com.applovin.impl.sdk.c;

import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.j;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

public class h {
    private final j a;
    private final Map<String, Long> b = new HashMap();

    public h(j jVar) {
        if (jVar != null) {
            this.a = jVar;
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private void e() {
        try {
            this.a.a(f.n, c().toString());
        } catch (Throwable th) {
            this.a.v().b("GlobalStatsManager", "Unable to save stats", th);
        }
    }

    public long a(g gVar) {
        return a(gVar, 1);
    }

    /* access modifiers changed from: 0000 */
    public long a(g gVar, long j) {
        long longValue;
        synchronized (this.b) {
            Long l = (Long) this.b.get(gVar.a());
            if (l == null) {
                l = Long.valueOf(0);
            }
            longValue = l.longValue() + j;
            this.b.put(gVar.a(), Long.valueOf(longValue));
        }
        e();
        return longValue;
    }

    public void a() {
        synchronized (this.b) {
            this.b.clear();
        }
        e();
    }

    public long b(g gVar) {
        long longValue;
        synchronized (this.b) {
            Long l = (Long) this.b.get(gVar.a());
            if (l == null) {
                l = Long.valueOf(0);
            }
            longValue = l.longValue();
        }
        return longValue;
    }

    public void b() {
        synchronized (this.b) {
            for (g a2 : g.b()) {
                this.b.remove(a2.a());
            }
            e();
        }
    }

    public void b(g gVar, long j) {
        synchronized (this.b) {
            this.b.put(gVar.a(), Long.valueOf(j));
        }
        e();
    }

    public JSONObject c() throws JSONException {
        JSONObject jSONObject;
        synchronized (this.b) {
            jSONObject = new JSONObject();
            for (Entry entry : this.b.entrySet()) {
                jSONObject.put((String) entry.getKey(), entry.getValue());
            }
        }
        return jSONObject;
    }

    public void c(g gVar) {
        synchronized (this.b) {
            this.b.remove(gVar.a());
        }
        e();
    }

    public void d() {
        try {
            JSONObject jSONObject = new JSONObject((String) this.a.b(f.n, "{}"));
            synchronized (this.b) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    try {
                        String str = (String) keys.next();
                        this.b.put(str, Long.valueOf(jSONObject.getLong(str)));
                    } catch (JSONException unused) {
                    }
                }
            }
        } catch (Throwable th) {
            this.a.v().b("GlobalStatsManager", "Unable to load stats", th);
        }
    }
}
