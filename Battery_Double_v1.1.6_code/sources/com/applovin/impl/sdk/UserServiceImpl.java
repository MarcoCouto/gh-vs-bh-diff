package com.applovin.impl.sdk;

import android.app.Activity;
import com.applovin.sdk.AppLovinUserService;
import com.applovin.sdk.AppLovinUserService.OnConsentDialogDismissListener;

public class UserServiceImpl implements AppLovinUserService {
    private final j a;

    UserServiceImpl(j jVar) {
        this.a = jVar;
    }

    public void showConsentDialog(Activity activity, OnConsentDialogDismissListener onConsentDialogDismissListener) {
        this.a.Q().a(activity, onConsentDialogDismissListener);
    }

    public String toString() {
        return "UserService{}";
    }
}
