package com.applovin.impl.sdk;

import android.content.Context;
import android.net.Uri;
import android.support.v4.media.session.PlaybackStateCompat;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.e;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.utils.q;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class n {
    private final String a = "FileManager";
    private final j b;
    private final p c;
    private final Object d = new Object();

    n(j jVar) {
        this.b = jVar;
        this.c = jVar.v();
    }

    private long a() {
        long longValue = ((Long) this.b.a(d.bG)).longValue();
        if (longValue < 0 || !b()) {
            return -1;
        }
        return longValue;
    }

    private long a(long j) {
        return j / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    private void a(long j, Context context) {
        p pVar;
        String str;
        String str2;
        if (b()) {
            long intValue = (long) ((Integer) this.b.a(d.bH)).intValue();
            if (intValue == -1) {
                pVar = this.c;
                str = "FileManager";
                str2 = "Cache has no maximum size set; skipping drop...";
            } else if (a(j) > intValue) {
                this.c.b("FileManager", "Cache has exceeded maximum size; dropping...");
                for (File b2 : d(context)) {
                    b(b2);
                }
                this.b.L().a(g.f);
            } else {
                pVar = this.c;
                str = "FileManager";
                str2 = "Cache is present but under size limit; not dropping...";
            }
            pVar.b(str, str2);
        }
    }

    private boolean a(File file, String str, List<String> list, boolean z, e eVar) {
        if (file == null || !file.exists() || file.isDirectory()) {
            ByteArrayOutputStream a2 = a(str, list, z);
            if (!(eVar == null || a2 == null)) {
                eVar.a((long) a2.size());
            }
            return a(a2, file);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("File exists for ");
        sb.append(str);
        this.c.b("FileManager", sb.toString());
        if (eVar != null) {
            eVar.b(file.length());
        }
        return true;
    }

    private boolean b() {
        return ((Boolean) this.b.a(d.bF)).booleanValue();
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x003b=Splitter:B:18:0x003b, B:24:0x004b=Splitter:B:24:0x004b} */
    private boolean b(ByteArrayOutputStream byteArrayOutputStream, File file) {
        boolean z;
        j jVar;
        StringBuilder sb = new StringBuilder();
        sb.append("Writing resource to filesystem: ");
        sb.append(file.getName());
        this.c.b("FileManager", sb.toString());
        synchronized (this.d) {
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                try {
                    byteArrayOutputStream.writeTo(fileOutputStream2);
                    z = true;
                    q.a((Closeable) fileOutputStream2, this.b);
                } catch (IOException e) {
                    e = e;
                    fileOutputStream = fileOutputStream2;
                    this.c.b("FileManager", "Unable to write data to file.", e);
                    jVar = this.b;
                    q.a((Closeable) fileOutputStream, jVar);
                    z = false;
                    return z;
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = fileOutputStream2;
                    q.a((Closeable) fileOutputStream, this.b);
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                this.c.b("FileManager", "Unable to write data to file.", e);
                jVar = this.b;
                q.a((Closeable) fileOutputStream, jVar);
                z = false;
                return z;
            } catch (Throwable th2) {
                th = th2;
                this.c.b("FileManager", "Unknown failure to write file.", th);
                jVar = this.b;
                q.a((Closeable) fileOutputStream, jVar);
                z = false;
                return z;
            }
        }
        return z;
    }

    private boolean b(File file) {
        boolean delete;
        StringBuilder sb = new StringBuilder();
        sb.append("Removing file ");
        sb.append(file.getName());
        sb.append(" from filesystem...");
        this.c.b("FileManager", sb.toString());
        synchronized (this.d) {
            try {
                delete = file.delete();
            } catch (Exception e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to remove file ");
                sb2.append(file.getName());
                sb2.append(" from filesystem!");
                this.c.b("FileManager", sb2.toString(), e);
                return false;
            } catch (Throwable th) {
                throw th;
            }
        }
        return delete;
    }

    private long c(Context context) {
        long j;
        boolean z;
        long a2 = a();
        boolean z2 = a2 != -1;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        synchronized (this.d) {
            List b2 = this.b.b((d) d.bN);
            j = 0;
            for (File file : d(context)) {
                if (!z2 || b2.contains(file.getName()) || seconds - TimeUnit.MILLISECONDS.toSeconds(file.lastModified()) <= a2) {
                    z = false;
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("File ");
                    sb.append(file.getName());
                    sb.append(" has expired, removing...");
                    this.c.b("FileManager", sb.toString());
                    z = b(file);
                }
                if (z) {
                    this.b.L().a(g.e);
                } else {
                    j += file.length();
                }
            }
        }
        return j;
    }

    private List<File> d(Context context) {
        List<File> asList;
        File e = e(context);
        if (!e.isDirectory()) {
            return Collections.emptyList();
        }
        synchronized (this.d) {
            asList = Arrays.asList(e.listFiles());
        }
        return asList;
    }

    private File e(Context context) {
        return new File(context.getFilesDir(), "al");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r3, r8.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r2, r8.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003d */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:45:0x0095=Splitter:B:45:0x0095, B:29:0x005b=Splitter:B:29:0x005b, B:37:0x006d=Splitter:B:37:0x006d} */
    public ByteArrayOutputStream a(File file) {
        FileInputStream fileInputStream;
        if (file == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Reading resource from filesystem: ");
        sb.append(file.getName());
        this.c.b("FileManager", sb.toString());
        synchronized (this.d) {
            try {
                fileInputStream = new FileInputStream(file);
                try {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr, 0, bArr.length);
                        if (read >= 0) {
                            byteArrayOutputStream.write(bArr, 0, read);
                            break;
                        }
                        q.a((Closeable) fileInputStream, this.b);
                        return byteArrayOutputStream;
                    }
                } catch (FileNotFoundException e) {
                    e = e;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("File not found. ");
                    sb2.append(e);
                    this.c.c("FileManager", sb2.toString());
                    q.a((Closeable) fileInputStream, this.b);
                    return null;
                } catch (IOException e2) {
                    e = e2;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Failed to read file: ");
                    sb3.append(file.getName());
                    sb3.append(e);
                    this.c.b("FileManager", sb3.toString());
                    q.a((Closeable) fileInputStream, this.b);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    try {
                        this.c.b("FileManager", "Unknown failure to read file.", th);
                        q.a((Closeable) fileInputStream, this.b);
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        q.a((Closeable) fileInputStream, this.b);
                        throw th;
                    }
                }
            } catch (FileNotFoundException e3) {
                e = e3;
                fileInputStream = null;
                StringBuilder sb22 = new StringBuilder();
                sb22.append("File not found. ");
                sb22.append(e);
                this.c.c("FileManager", sb22.toString());
                q.a((Closeable) fileInputStream, this.b);
                return null;
            } catch (IOException e4) {
                e = e4;
                fileInputStream = null;
                StringBuilder sb32 = new StringBuilder();
                sb32.append("Failed to read file: ");
                sb32.append(file.getName());
                sb32.append(e);
                this.c.b("FileManager", sb32.toString());
                q.a((Closeable) fileInputStream, this.b);
                return null;
            } catch (Throwable th3) {
                th = th3;
                fileInputStream = null;
                q.a((Closeable) fileInputStream, this.b);
                throw th;
            }
        }
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r10v3, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r0v1, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r10v4, types: [java.net.HttpURLConnection] */
    /* JADX WARNING: type inference failed for: r1v3, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r0v3 */
    /* JADX WARNING: type inference failed for: r10v5 */
    /* JADX WARNING: type inference failed for: r1v4 */
    /* JADX WARNING: type inference failed for: r10v7 */
    /* JADX WARNING: type inference failed for: r10v9 */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r0v6 */
    /* JADX WARNING: type inference failed for: r10v16 */
    /* JADX WARNING: type inference failed for: r10v17 */
    /* JADX WARNING: type inference failed for: r10v18 */
    /* JADX WARNING: type inference failed for: r10v19 */
    /* JADX WARNING: type inference failed for: r10v20 */
    /* JADX WARNING: type inference failed for: r1v19 */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:28|29|30) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r9, r7.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00cd, code lost:
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r1, r7.b);
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r9, r7.b);
        com.applovin.impl.sdk.utils.q.a(r10, r7.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00dc, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x00c8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 6 */
    public ByteArrayOutputStream a(String str, List<String> list, boolean z) {
        ? r10;
        ByteArrayOutputStream byteArrayOutputStream;
        ? r0;
        ? r102;
        ? r1;
        ? r103;
        HttpURLConnection httpURLConnection;
        HttpURLConnection httpURLConnection2;
        ? r02 = 0;
        if (!z || q.a(str, list)) {
            if (((Boolean) this.b.a(d.dy)).booleanValue() && !str.contains("https://")) {
                this.c.d("FileManager", "Plaintext HTTP operation requested; upgrading to HTTPS due to universal SSL setting...");
                str = str.replace("http://", "https://");
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Loading ");
            sb.append(str);
            sb.append("...");
            this.c.b("FileManager", sb.toString());
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    httpURLConnection2 = (HttpURLConnection) new URL(str).openConnection();
                } catch (IOException e) {
                    e = e;
                    httpURLConnection = 0;
                    r1 = r103;
                    r102 = r103;
                    try {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Error loading ");
                        sb2.append(str);
                        this.c.b("FileManager", sb2.toString(), e);
                        q.a((Closeable) r1, this.b);
                        q.a((Closeable) byteArrayOutputStream, this.b);
                        q.a((HttpURLConnection) r102, this.b);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        r0 = r1;
                        r10 = r102;
                        q.a((Closeable) r0, this.b);
                        q.a((Closeable) byteArrayOutputStream, this.b);
                        q.a((HttpURLConnection) r10, this.b);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    r10 = 0;
                    r0 = r02;
                    q.a((Closeable) r0, this.b);
                    q.a((Closeable) byteArrayOutputStream, this.b);
                    q.a((HttpURLConnection) r10, this.b);
                    throw th;
                }
                try {
                    httpURLConnection2.setConnectTimeout(((Integer) this.b.a(d.dw)).intValue());
                    httpURLConnection2.setReadTimeout(((Integer) this.b.a(d.dx)).intValue());
                    httpURLConnection2.setDefaultUseCaches(true);
                    httpURLConnection2.setUseCaches(true);
                    httpURLConnection2.setAllowUserInteraction(false);
                    httpURLConnection2.setInstanceFollowRedirects(true);
                    int responseCode = httpURLConnection2.getResponseCode();
                    if (responseCode >= 200) {
                        if (responseCode < 300) {
                            InputStream inputStream = httpURLConnection2.getInputStream();
                            try {
                                byte[] bArr = new byte[1024];
                                while (true) {
                                    int read = inputStream.read(bArr, 0, bArr.length);
                                    if (read >= 0) {
                                        byteArrayOutputStream.write(bArr, 0, read);
                                        break;
                                    }
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Loaded resource at ");
                                    sb3.append(str);
                                    this.c.b("FileManager", sb3.toString());
                                    q.a((Closeable) inputStream, this.b);
                                    q.a((Closeable) byteArrayOutputStream, this.b);
                                    q.a(httpURLConnection2, this.b);
                                    return byteArrayOutputStream;
                                }
                            } catch (IOException e2) {
                                e = e2;
                                r102 = httpURLConnection2;
                                r1 = inputStream;
                                StringBuilder sb22 = new StringBuilder();
                                sb22.append("Error loading ");
                                sb22.append(str);
                                this.c.b("FileManager", sb22.toString(), e);
                                q.a((Closeable) r1, this.b);
                                q.a((Closeable) byteArrayOutputStream, this.b);
                                q.a((HttpURLConnection) r102, this.b);
                                return null;
                            }
                        }
                    }
                    q.a((Closeable) null, this.b);
                    q.a((Closeable) byteArrayOutputStream, this.b);
                    q.a(httpURLConnection2, this.b);
                    return null;
                } catch (IOException e3) {
                    e = e3;
                    r1 = 0;
                    r102 = httpURLConnection2;
                    StringBuilder sb222 = new StringBuilder();
                    sb222.append("Error loading ");
                    sb222.append(str);
                    this.c.b("FileManager", sb222.toString(), e);
                    q.a((Closeable) r1, this.b);
                    q.a((Closeable) byteArrayOutputStream, this.b);
                    q.a((HttpURLConnection) r102, this.b);
                    return null;
                } catch (Throwable th3) {
                    th = th3;
                    r0 = r02;
                    r10 = httpURLConnection2;
                    q.a((Closeable) r0, this.b);
                    q.a((Closeable) byteArrayOutputStream, this.b);
                    q.a((HttpURLConnection) r10, this.b);
                    throw th;
                }
            } catch (IOException e4) {
                e = e4;
                byteArrayOutputStream = null;
                httpURLConnection = 0;
                r1 = r103;
                r102 = r103;
                StringBuilder sb2222 = new StringBuilder();
                sb2222.append("Error loading ");
                sb2222.append(str);
                this.c.b("FileManager", sb2222.toString(), e);
                q.a((Closeable) r1, this.b);
                q.a((Closeable) byteArrayOutputStream, this.b);
                q.a((HttpURLConnection) r102, this.b);
                return null;
            } catch (Throwable th4) {
                th = th4;
                byteArrayOutputStream = null;
                r10 = 0;
                r0 = r02;
                q.a((Closeable) r0, this.b);
                q.a((Closeable) byteArrayOutputStream, this.b);
                q.a((HttpURLConnection) r10, this.b);
                throw th;
            }
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Domain is not whitelisted, skipping precache for url: ");
            sb4.append(str);
            this.c.b("FileManager", sb4.toString());
            return null;
        }
    }

    public File a(String str, Context context) {
        File file;
        if (!com.applovin.impl.sdk.utils.n.b(str)) {
            this.c.b("FileManager", "Nothing to look up, skipping...");
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Looking up cached resource: ");
        sb.append(str);
        this.c.b("FileManager", sb.toString());
        if (str.contains(SettingsJsonConstants.APP_ICON_KEY)) {
            str = str.replace("/", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR).replace(".", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        }
        synchronized (this.d) {
            File e = e(context);
            file = new File(e, str);
            try {
                e.mkdirs();
            } catch (Throwable th) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to make cache directory at ");
                sb2.append(e);
                this.c.b("FileManager", sb2.toString(), th);
                return null;
            }
        }
        return file;
    }

    public String a(Context context, String str, String str2, List<String> list, boolean z, e eVar) {
        return a(context, str, str2, list, z, false, eVar);
    }

    public String a(Context context, String str, String str2, List<String> list, boolean z, boolean z2, e eVar) {
        if (!com.applovin.impl.sdk.utils.n.b(str)) {
            this.c.b("FileManager", "Nothing to cache, skipping...");
            return null;
        }
        String lastPathSegment = Uri.parse(str).getLastPathSegment();
        if (com.applovin.impl.sdk.utils.n.b(lastPathSegment) && com.applovin.impl.sdk.utils.n.b(str2)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(lastPathSegment);
            lastPathSegment = sb.toString();
        }
        File a2 = a(lastPathSegment, context);
        if (!a(a2, str, list, z, eVar)) {
            return null;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Caching succeeded for file ");
        sb2.append(lastPathSegment);
        this.c.b("FileManager", sb2.toString());
        if (z2) {
            lastPathSegment = Uri.fromFile(a2).toString();
        }
        return lastPathSegment;
    }

    public void a(Context context) {
        if (b() && this.b.c()) {
            this.c.b("FileManager", "Compacting cache...");
            synchronized (this.d) {
                a(c(context), context);
            }
        }
    }

    public boolean a(ByteArrayOutputStream byteArrayOutputStream, File file) {
        if (file == null) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Caching ");
        sb.append(file.getAbsolutePath());
        sb.append("...");
        this.c.b("FileManager", sb.toString());
        if (byteArrayOutputStream == null || byteArrayOutputStream.size() <= 0) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("No data for ");
            sb2.append(file.getAbsolutePath());
            this.c.d("FileManager", sb2.toString());
            return false;
        } else if (!b(byteArrayOutputStream, file)) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to cache ");
            sb3.append(file.getAbsolutePath());
            this.c.e("FileManager", sb3.toString());
            return false;
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Caching completed for ");
            sb4.append(file);
            this.c.b("FileManager", sb4.toString());
            return true;
        }
    }

    public boolean a(File file, String str, List<String> list, e eVar) {
        return a(file, str, list, true, eVar);
    }

    public void b(Context context) {
        try {
            a(".nomedia", context);
            File file = new File(e(context), ".nomedia");
            if (!file.exists()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Creating .nomedia file at ");
                sb.append(file.getAbsolutePath());
                this.c.b("FileManager", sb.toString());
                if (!file.createNewFile()) {
                    this.c.e("FileManager", "Failed to create .nomedia file");
                }
            }
        } catch (IOException e) {
            this.c.b("FileManager", "Failed to create .nomedia file", e);
        }
    }
}
