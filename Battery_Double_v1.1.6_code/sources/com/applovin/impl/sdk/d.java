package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.utils.o;
import java.lang.ref.WeakReference;

public class d extends BroadcastReceiver implements a {
    private o a;
    private final Object b = new Object();
    private final j c;
    /* access modifiers changed from: private */
    public final WeakReference<a> d;
    private long e;

    public interface a {
        void onAdRefresh();
    }

    public d(j jVar, a aVar) {
        this.d = new WeakReference<>(aVar);
        this.c = jVar;
    }

    /* access modifiers changed from: private */
    public void j() {
        synchronized (this.b) {
            this.a = null;
            if (!((Boolean) this.c.a(c.x)).booleanValue()) {
                this.c.af().unregisterReceiver(this);
                this.c.Z().b((a) this);
            }
        }
    }

    public void a(long j) {
        synchronized (this.b) {
            c();
            this.e = j;
            this.a = o.a(j, this.c, new Runnable() {
                public void run() {
                    d.this.j();
                    a aVar = (a) d.this.d.get();
                    if (aVar != null) {
                        aVar.onAdRefresh();
                    }
                }
            });
            if (!((Boolean) this.c.a(c.x)).booleanValue()) {
                this.c.af().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
                this.c.af().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
                this.c.Z().a((a) this);
            }
            if (((Boolean) this.c.a(c.w)).booleanValue() && (this.c.Z().b() || this.c.Y().a())) {
                this.a.b();
            }
        }
    }

    public boolean a() {
        boolean z;
        synchronized (this.b) {
            z = this.a != null;
        }
        return z;
    }

    public long b() {
        long a2;
        synchronized (this.b) {
            a2 = this.a != null ? this.a.a() : -1;
        }
        return a2;
    }

    public void c() {
        synchronized (this.b) {
            if (this.a != null) {
                this.a.d();
                j();
            }
        }
    }

    public void d() {
        synchronized (this.b) {
            if (this.a != null) {
                this.a.b();
            }
        }
    }

    public void e() {
        synchronized (this.b) {
            if (this.a != null) {
                this.a.c();
            }
        }
    }

    public void f() {
        if (((Boolean) this.c.a(c.v)).booleanValue()) {
            d();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005f, code lost:
        if (r2 == false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0061, code lost:
        r0 = (com.applovin.impl.sdk.d.a) r9.d.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0069, code lost:
        if (r0 == null) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006b, code lost:
        r0.onAdRefresh();
     */
    public void g() {
        if (((Boolean) this.c.a(c.v)).booleanValue()) {
            synchronized (this.b) {
                if (this.c.Z().b()) {
                    this.c.v().b("AdRefreshManager", "Waiting for the full screen ad to be dismissed to resume the timer.");
                    return;
                }
                boolean z = false;
                if (this.a != null) {
                    long b2 = this.e - b();
                    long longValue = ((Long) this.c.a(c.u)).longValue();
                    if (longValue < 0 || b2 <= longValue) {
                        this.a.c();
                    } else {
                        c();
                        z = true;
                    }
                }
            }
        }
    }

    public void h() {
        if (((Boolean) this.c.a(c.w)).booleanValue()) {
            d();
        }
    }

    public void i() {
        if (((Boolean) this.c.a(c.w)).booleanValue()) {
            synchronized (this.b) {
                if (this.c.Y().a()) {
                    this.c.v().b("AdRefreshManager", "Waiting for the application to enter foreground to resume the timer.");
                } else if (this.a != null) {
                    this.a.c();
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            f();
        } else if ("com.applovin.application_resumed".equals(action)) {
            g();
        }
    }
}
