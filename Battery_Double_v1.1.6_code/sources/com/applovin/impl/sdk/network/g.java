package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import java.util.Map;
import org.json.JSONObject;

public class g<T> extends b {
    private String a;

    public static class a<T> extends com.applovin.impl.sdk.network.b.a<T> {
        /* access modifiers changed from: private */
        public String n;

        public a(j jVar) {
            super(jVar);
            this.h = false;
            this.i = ((Integer) jVar.a(d.dt)).intValue();
            this.j = ((Integer) jVar.a(d.ds)).intValue();
            this.k = ((Integer) jVar.a(d.dv)).intValue();
        }

        /* renamed from: b */
        public a a(T t) {
            this.g = t;
            return this;
        }

        /* renamed from: b */
        public a a(Map<String, String> map) {
            this.d = map;
            return this;
        }

        /* renamed from: b */
        public a a(JSONObject jSONObject) {
            this.f = jSONObject;
            return this;
        }

        /* renamed from: b */
        public g<T> a() {
            return new g<>(this);
        }

        public a c(Map<String, String> map) {
            this.e = map;
            return this;
        }

        /* renamed from: c */
        public a a(boolean z) {
            this.l = z;
            return this;
        }

        /* renamed from: d */
        public a a(int i) {
            this.i = i;
            return this;
        }

        /* renamed from: d */
        public a a(String str) {
            this.b = str;
            return this;
        }

        /* renamed from: e */
        public a b(int i) {
            this.j = i;
            return this;
        }

        /* renamed from: e */
        public a c(String str) {
            this.c = str;
            return this;
        }

        /* renamed from: f */
        public a c(int i) {
            this.k = i;
            return this;
        }

        /* renamed from: f */
        public a b(String str) {
            this.a = str;
            return this;
        }

        public a g(String str) {
            this.n = str;
            return this;
        }
    }

    protected g(a aVar) {
        super(aVar);
        this.a = aVar.n;
    }

    public static a b(j jVar) {
        return new a(jVar);
    }

    public boolean o() {
        return this.a != null;
    }

    public String p() {
        return this.a;
    }
}
