package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class f {
    private String a;
    private String b;
    private String c;
    private Map<String, String> d;
    private Map<String, String> e;
    private Map<String, Object> f;
    private boolean g;
    private String h;
    private int i;

    public static class a {
        /* access modifiers changed from: private */
        public String a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public Map<String, String> c;
        /* access modifiers changed from: private */
        public Map<String, String> d;
        /* access modifiers changed from: private */
        public Map<String, Object> e;
        /* access modifiers changed from: private */
        public boolean f;
        /* access modifiers changed from: private */
        public String g;

        public a a(String str) {
            this.a = str;
            return this;
        }

        public a a(Map<String, String> map) {
            this.c = map;
            return this;
        }

        public a a(boolean z) {
            this.f = z;
            return this;
        }

        public f a() {
            return new f(this);
        }

        public a b(String str) {
            this.b = str;
            return this;
        }

        public a b(Map<String, String> map) {
            this.d = map;
            return this;
        }

        public a c(String str) {
            this.g = str;
            return this;
        }

        public a c(Map<String, Object> map) {
            this.e = map;
            return this;
        }
    }

    private f(a aVar) {
        this.a = UUID.randomUUID().toString();
        this.b = aVar.a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.e = aVar.d;
        this.f = aVar.e;
        this.g = aVar.f;
        this.h = aVar.g;
        this.i = 0;
    }

    f(JSONObject jSONObject, j jVar) throws Exception {
        String b2 = i.b(jSONObject, "uniqueId", UUID.randomUUID().toString(), jVar);
        String string = jSONObject.getString("targetUrl");
        String b3 = i.b(jSONObject, "backupUrl", "", jVar);
        int i2 = jSONObject.getInt("attemptNumber");
        Map<String, String> synchronizedMap = i.a(jSONObject, "parameters") ? Collections.synchronizedMap(i.a(jSONObject.getJSONObject("parameters"))) : Collections.emptyMap();
        Map<String, String> synchronizedMap2 = i.a(jSONObject, "httpHeaders") ? Collections.synchronizedMap(i.a(jSONObject.getJSONObject("httpHeaders"))) : Collections.emptyMap();
        Map<String, Object> synchronizedMap3 = i.a(jSONObject, "requestBody") ? Collections.synchronizedMap(i.b(jSONObject.getJSONObject("requestBody"))) : Collections.emptyMap();
        this.a = b2;
        this.b = string;
        this.c = b3;
        this.d = synchronizedMap;
        this.e = synchronizedMap2;
        this.f = synchronizedMap3;
        this.g = jSONObject.optBoolean("isEncodingEnabled", false);
        this.i = i2;
    }

    public static a l() {
        return new a();
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> c() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, String> d() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, Object> e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.a.equals(((f) obj).a);
    }

    /* access modifiers changed from: 0000 */
    public boolean f() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public String g() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public int h() {
        return this.i;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        this.i++;
    }

    /* access modifiers changed from: 0000 */
    public void j() {
        HashMap hashMap = new HashMap();
        if (this.d != null) {
            hashMap.putAll(this.d);
        }
        hashMap.put("postback_ts", String.valueOf(System.currentTimeMillis()));
        this.d = hashMap;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject k() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("uniqueId", this.a);
        jSONObject.put("targetUrl", this.b);
        jSONObject.put("backupUrl", this.c);
        jSONObject.put("isEncodingEnabled", this.g);
        jSONObject.put("attemptNumber", this.i);
        if (this.d != null) {
            jSONObject.put("parameters", new JSONObject(this.d));
        }
        if (this.e != null) {
            jSONObject.put("httpHeaders", new JSONObject(this.e));
        }
        if (this.f != null) {
            jSONObject.put("requestBody", new JSONObject(this.f));
        }
        return jSONObject;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PostbackRequest{uniqueId='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", communicatorRequestId='");
        sb.append(this.h);
        sb.append('\'');
        sb.append(", targetUrl='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", backupUrl='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", attemptNumber=");
        sb.append(this.i);
        sb.append(", isEncodingEnabled=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }
}
