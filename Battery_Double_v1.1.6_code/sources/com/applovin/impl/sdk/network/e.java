package com.applovin.impl.sdk.network;

import android.content.SharedPreferences;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONObject;

public class e {
    private final j a;
    /* access modifiers changed from: private */
    public final p b;
    private final SharedPreferences c;
    private final Object d = new Object();
    private final ArrayList<f> e;
    private final ArrayList<f> f = new ArrayList<>();
    private final Set<f> g = new HashSet();

    public e(j jVar) {
        if (jVar != null) {
            this.a = jVar;
            this.b = jVar.v();
            this.c = jVar.D().getSharedPreferences("com.applovin.sdk.impl.postbackQueue.domain", 0);
            this.e = b();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    /* JADX INFO: used method not loaded: com.applovin.impl.sdk.network.g.a.d(java.lang.String):null, types can be incorrect */
    /* JADX INFO: used method not loaded: com.applovin.impl.sdk.network.g.a.e(java.lang.String):null, types can be incorrect */
    /* JADX INFO: used method not loaded: com.applovin.impl.sdk.network.g.a.b(java.util.Map):null, types can be incorrect */
    /* JADX INFO: used method not loaded: com.applovin.impl.sdk.network.g.a.c(java.util.Map):null, types can be incorrect */
    /* JADX INFO: used method not loaded: com.applovin.impl.sdk.network.g.a.b(org.json.JSONObject):null, types can be incorrect */
    /* JADX INFO: used method not loaded: com.applovin.impl.sdk.network.g.a.c(boolean):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005a, code lost:
        r0 = ((java.lang.Integer) r4.a.a(com.applovin.impl.sdk.b.d.du)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006c, code lost:
        if (r5.h() <= r0) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
        r2 = new java.lang.StringBuilder();
        r2.append("Exceeded maximum persisted attempt count of ");
        r2.append(r0);
        r2.append(". Dequeuing postback: ");
        r2.append(r5);
        r4.b.d("PersistentPostbackManager", r2.toString());
        d(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0092, code lost:
        r1 = r4.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0094, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.g.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x009a, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x009f, code lost:
        if (r5.e() == null) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a1, code lost:
        r0 = new org.json.JSONObject(r5.e());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ab, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ac, code lost:
        r4.a.R().dispatchPostbackRequest(com.applovin.impl.sdk.network.g.b(r4.a).d(r5.a()).e(r5.b()).b(r5.c()).c(r5.d()).b(r0).c(r5.f()).g(r5.g()).b(), new com.applovin.impl.sdk.network.e.AnonymousClass1(r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00f8, code lost:
        return;
     */
    private void a(final f fVar, final AppLovinPostbackListener appLovinPostbackListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("Preparing to submit postback...");
        sb.append(fVar);
        this.b.b("PersistentPostbackManager", sb.toString());
        if (this.a.c()) {
            this.b.b("PersistentPostbackManager", "Skipping postback dispatch because SDK is still initializing - postback will be dispatched afterwards");
            return;
        }
        synchronized (this.d) {
            if (this.g.contains(fVar)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Skip pending postback: ");
                sb2.append(fVar.a());
                this.b.b("PersistentPostbackManager", sb2.toString());
                return;
            }
            fVar.i();
            c();
        }
    }

    private ArrayList<f> b() {
        Set<String> set = (Set) this.a.b(f.m, new LinkedHashSet(0), this.c);
        ArrayList<f> arrayList = new ArrayList<>(Math.max(1, set.size()));
        int intValue = ((Integer) this.a.a(d.du)).intValue();
        StringBuilder sb = new StringBuilder();
        sb.append("Deserializing ");
        sb.append(set.size());
        sb.append(" postback(s).");
        this.b.b("PersistentPostbackManager", sb.toString());
        for (String str : set) {
            try {
                f fVar = new f(new JSONObject(str), this.a);
                if (fVar.h() < intValue) {
                    arrayList.add(fVar);
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Skipping deserialization because maximum attempt count exceeded for postback: ");
                    sb2.append(fVar);
                    this.b.b("PersistentPostbackManager", sb2.toString());
                }
            } catch (Throwable th) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to deserialize postback request from json: ");
                sb3.append(str);
                this.b.b("PersistentPostbackManager", sb3.toString(), th);
            }
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append("Successfully loaded postback queue with ");
        sb4.append(arrayList.size());
        sb4.append(" postback(s).");
        this.b.b("PersistentPostbackManager", sb4.toString());
        return arrayList;
    }

    private void b(f fVar) {
        synchronized (this.d) {
            this.e.add(fVar);
            c();
            StringBuilder sb = new StringBuilder();
            sb.append("Enqueued postback: ");
            sb.append(fVar);
            this.b.b("PersistentPostbackManager", sb.toString());
        }
    }

    private void c() {
        LinkedHashSet linkedHashSet = new LinkedHashSet(this.e.size());
        Iterator it = this.e.iterator();
        while (it.hasNext()) {
            try {
                linkedHashSet.add(((f) it.next()).k().toString());
            } catch (Throwable th) {
                this.b.b("PersistentPostbackManager", "Unable to serialize postback request to JSON.", th);
            }
        }
        this.a.a(f.m, linkedHashSet, this.c);
        this.b.b("PersistentPostbackManager", "Wrote updated postback queue to disk.");
    }

    private void c(f fVar) {
        a(fVar, (AppLovinPostbackListener) null);
    }

    /* access modifiers changed from: private */
    public void d() {
        synchronized (this.d) {
            Iterator it = this.f.iterator();
            while (it.hasNext()) {
                c((f) it.next());
            }
            this.f.clear();
        }
    }

    /* access modifiers changed from: private */
    public void d(f fVar) {
        synchronized (this.d) {
            this.g.remove(fVar);
            this.e.remove(fVar);
            c();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Dequeued successfully transmitted postback: ");
        sb.append(fVar);
        this.b.b("PersistentPostbackManager", sb.toString());
    }

    /* access modifiers changed from: private */
    public void e(f fVar) {
        synchronized (this.d) {
            this.g.remove(fVar);
            this.f.add(fVar);
        }
    }

    public void a() {
        synchronized (this.d) {
            if (this.e != null) {
                for (f c2 : new ArrayList(this.e)) {
                    c(c2);
                }
            }
        }
    }

    public void a(f fVar) {
        a(fVar, true);
    }

    public void a(f fVar, boolean z) {
        a(fVar, z, null);
    }

    public void a(f fVar, boolean z, AppLovinPostbackListener appLovinPostbackListener) {
        if (n.b(fVar.a())) {
            if (z) {
                fVar.j();
            }
            synchronized (this.d) {
                b(fVar);
                a(fVar, appLovinPostbackListener);
            }
        }
    }
}
