package com.applovin.impl.sdk.network;

import android.content.Context;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.b.g;
import com.applovin.impl.sdk.utils.q;
import org.json.JSONException;
import org.json.JSONObject;

public class d {
    private static final Object a = new Object();

    private static JSONObject a(String str, Context context) {
        JSONObject b = b(context);
        if (b == null) {
            b = new JSONObject();
        }
        if (!b.has(str)) {
            try {
                b.put(str, new JSONObject());
            } catch (JSONException unused) {
            }
        }
        return b;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:5|6|7|8|9|10|11|12|13|14) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0023 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0026 */
    static void a(int i, String str, Context context) {
        if (!TextUtils.isEmpty(str)) {
            synchronized (a) {
                String d = q.d(str);
                JSONObject a2 = a(d, context);
                String num = Integer.toString(i);
                JSONObject optJSONObject = a2.optJSONObject(d);
                optJSONObject.put(num, optJSONObject.optInt(num) + 1);
                a2.put(d, optJSONObject);
                a(a2, context);
            }
        }
    }

    public static void a(Context context) {
        synchronized (a) {
            g.a(f.q, context);
        }
    }

    private static void a(JSONObject jSONObject, Context context) {
        g.a(f.q, jSONObject.toString(), context);
    }

    public static JSONObject b(Context context) {
        JSONObject jSONObject;
        synchronized (a) {
            try {
                jSONObject = new JSONObject((String) g.b(f.q, "{}", context));
            } catch (JSONException unused) {
                return new JSONObject();
            } catch (Throwable th) {
                throw th;
            }
        }
        return jSONObject;
    }
}
