package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.d.r.a;
import com.applovin.impl.sdk.j;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinPostbackService;

public class PostbackServiceImpl implements AppLovinPostbackService {
    private final j a;

    public PostbackServiceImpl(j jVar) {
        this.a = jVar;
    }

    public void dispatchPostbackAsync(String str, AppLovinPostbackListener appLovinPostbackListener) {
        dispatchPostbackRequest(g.b(this.a).a(str).a(false).a(), appLovinPostbackListener);
    }

    public void dispatchPostbackRequest(g gVar, a aVar, AppLovinPostbackListener appLovinPostbackListener) {
        this.a.K().a((com.applovin.impl.sdk.d.a) new com.applovin.impl.sdk.d.j(gVar, aVar, this.a, appLovinPostbackListener), aVar);
    }

    public void dispatchPostbackRequest(g gVar, AppLovinPostbackListener appLovinPostbackListener) {
        dispatchPostbackRequest(gVar, a.POSTBACKS, appLovinPostbackListener);
    }

    public String toString() {
        return "PostbackService{}";
    }
}
