package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import com.applovin.impl.sdk.p;

public class c {
    private static c a;
    private static final Object b = new Object();
    private final Bundle c;
    private final int d;

    private c(Context context) {
        int i;
        Bundle bundle = null;
        try {
            bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (NameNotFoundException e) {
            p.c("AndroidManifest", "Failed to get meta data.", e);
        } finally {
            this.c = bundle;
        }
        int i2 = 0;
        try {
            XmlResourceParser openXmlResourceParser = context.getAssets().openXmlResourceParser("AndroidManifest.xml");
            int eventType = openXmlResourceParser.getEventType();
            i = 0;
            do {
                if (2 == eventType) {
                    try {
                        if (openXmlResourceParser.getName().equals("application")) {
                            int i3 = 0;
                            while (true) {
                                if (i3 >= openXmlResourceParser.getAttributeCount()) {
                                    break;
                                }
                                String attributeName = openXmlResourceParser.getAttributeName(i3);
                                String attributeValue = openXmlResourceParser.getAttributeValue(i3);
                                if (attributeName.equals("networkSecurityConfig")) {
                                    i = Integer.valueOf(attributeValue.substring(1)).intValue();
                                    break;
                                }
                                i3++;
                            }
                        }
                    } catch (Throwable th) {
                        th = th;
                        this.d = i;
                        throw th;
                    }
                }
                eventType = openXmlResourceParser.next();
            } while (eventType != 1);
            this.d = i;
        } catch (Throwable th2) {
            th = th2;
            i = 0;
            this.d = i;
            throw th;
        }
    }

    public static c a(Context context) {
        c cVar;
        synchronized (b) {
            if (a == null) {
                a = new c(context);
            }
            cVar = a;
        }
        return cVar;
    }

    public String a(String str, String str2) {
        return this.c.getString(str, str2);
    }

    public boolean a() {
        return this.d != 0;
    }

    public boolean a(String str) {
        return this.c.containsKey(str);
    }

    public boolean a(String str, boolean z) {
        return this.c.getBoolean(str, z);
    }
}
