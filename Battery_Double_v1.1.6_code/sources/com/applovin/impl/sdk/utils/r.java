package com.applovin.impl.sdk.utils;

import android.support.v4.view.GravityCompat;

public class r {
    public static boolean a(int i) {
        return i == 0;
    }

    public static boolean a(int i, int i2) {
        return a(i) != a(i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        if ((r6 & 16) == 16) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0078, code lost:
        if ((r6 & 1) == 1) goto L_0x0044;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0047  */
    public static int[] a(int i, int i2, int i3) {
        if (i == 0) {
            return new int[]{i2, i3};
        } else if ((i & 119) == 119) {
            return new int[]{13};
        } else {
            if ((i & 112) != 112) {
                if ((i & 48) == 48) {
                    i2 = 10;
                } else if ((i & 80) == 80) {
                    i2 = 12;
                }
                if ((i & 7) != 7) {
                    if (!g.b() || (i & GravityCompat.START) != 8388611) {
                        if ((i & 3) == 3) {
                            i3 = 9;
                        } else if (g.b() && (i & GravityCompat.END) == 8388613) {
                            i3 = 21;
                        } else if ((i & 5) == 5) {
                            i3 = 11;
                        }
                        return new int[]{i3, i2};
                    }
                    i3 = 20;
                    return new int[]{i3, i2};
                }
                i3 = 14;
                return new int[]{i3, i2};
            }
            i2 = 15;
            if ((i & 7) != 7) {
            }
            i3 = 14;
            return new int[]{i3, i2};
        }
    }
}
