package com.applovin.impl.sdk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import java.util.concurrent.TimeUnit;

public class m extends BroadcastReceiver implements SensorEventListener {
    private final int a;
    private final float b;
    private final SensorManager c;
    private final Sensor d = this.c.getDefaultSensor(9);
    private final Sensor e = this.c.getDefaultSensor(4);
    private final j f;
    private float[] g;
    private float h;

    public m(j jVar) {
        this.f = jVar;
        this.c = (SensorManager) jVar.D().getSystemService("sensor");
        this.a = ((Integer) jVar.a(d.ew)).intValue();
        this.b = ((Float) jVar.a(d.ev)).floatValue();
        jVar.af().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        jVar.af().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public void a() {
        this.c.unregisterListener(this);
        if (((Boolean) this.f.C().a(d.et)).booleanValue()) {
            this.c.registerListener(this, this.d, (int) TimeUnit.MILLISECONDS.toMicros((long) this.a));
        }
        if (((Boolean) this.f.C().a(d.eu)).booleanValue()) {
            this.c.registerListener(this, this.e, (int) TimeUnit.MILLISECONDS.toMicros((long) this.a));
        }
    }

    public float b() {
        return this.h;
    }

    public float c() {
        if (this.g == null) {
            return 0.0f;
        }
        return (float) Math.toDegrees(Math.acos((double) (this.g[2] / 9.81f)));
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            this.c.unregisterListener(this);
        } else if ("com.applovin.application_resumed".equals(action)) {
            a();
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == 9) {
            this.g = sensorEvent.values;
        } else if (sensorEvent.sensor.getType() == 4) {
            this.h *= this.b;
            this.h += Math.abs(sensorEvent.values[0]) + Math.abs(sensorEvent.values[1]) + Math.abs(sensorEvent.values[2]);
        }
    }
}
