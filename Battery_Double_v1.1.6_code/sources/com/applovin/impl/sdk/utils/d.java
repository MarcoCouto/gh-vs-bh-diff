package com.applovin.impl.sdk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.j;
import java.util.HashSet;
import java.util.Set;

public class d extends BroadcastReceiver {
    private static final Set<d> a = new HashSet();
    private final o b;

    private d(long j, final j jVar, final Runnable runnable) {
        this.b = o.a(j, jVar, new Runnable() {
            public void run() {
                jVar.af().unregisterReceiver(d.this);
                d.this.a();
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        a.add(this);
        jVar.af().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        jVar.af().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public static d a(long j, j jVar, Runnable runnable) {
        return new d(j, jVar, runnable);
    }

    public void a() {
        this.b.d();
        a.remove(this);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            this.b.b();
        } else if ("com.applovin.application_resumed".equals(action)) {
            this.b.c();
        }
    }
}
