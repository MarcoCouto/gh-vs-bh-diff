package com.applovin.impl.sdk.utils;

import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.applovin.adview.AppLovinAdView;
import com.applovin.sdk.AppLovinAdSize;

public class b {
    public static AppLovinAdSize a(AttributeSet attributeSet) {
        AppLovinAdSize appLovinAdSize = null;
        if (attributeSet == null) {
            return null;
        }
        String attributeValue = attributeSet.getAttributeValue(AppLovinAdView.NAMESPACE, "size");
        if (n.b(attributeValue)) {
            appLovinAdSize = AppLovinAdSize.fromString(attributeValue);
        }
        return appLovinAdSize;
    }

    public static void a(ViewGroup viewGroup, View view) {
        if (viewGroup != null) {
            int indexOfChild = viewGroup.indexOfChild(view);
            if (indexOfChild == -1) {
                viewGroup.removeAllViews();
            } else {
                viewGroup.removeViews(0, indexOfChild);
                viewGroup.removeViews(1, viewGroup.getChildCount() - 1);
            }
        }
    }

    public static boolean b(AttributeSet attributeSet) {
        return attributeSet != null && attributeSet.getAttributeBooleanValue(AppLovinAdView.NAMESPACE, "loadAdOnCreate", false);
    }
}
