package com.applovin.impl.sdk.utils;

import android.content.pm.PackageManager;

public class k {
    public static int a(String str, String str2, PackageManager packageManager) {
        try {
            return packageManager.checkPermission(str, str2);
        } catch (Throwable unused) {
            return -1;
        }
    }
}
