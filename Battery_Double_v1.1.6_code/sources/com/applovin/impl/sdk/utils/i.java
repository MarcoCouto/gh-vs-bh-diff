package com.applovin.impl.sdk.utils;

import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.impl.sdk.j;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class i {
    public static float a(JSONObject jSONObject, String str, float f, j jVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return f;
        }
        try {
            double d = jSONObject.getDouble(str);
            return (-3.4028234663852886E38d >= d || d >= 3.4028234663852886E38d) ? f : (float) d;
        } catch (JSONException e) {
            if (jVar == null) {
                return f;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to retrieve float property for key = ");
            sb.append(str);
            jVar.v().b("JsonUtils", sb.toString(), e);
            return f;
        }
    }

    public static long a(JSONObject jSONObject, String str, long j, j jVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return j;
        }
        try {
            return jSONObject.getLong(str);
        } catch (JSONException e) {
            if (jVar == null) {
                return j;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to retrieve int property for key = ");
            sb.append(str);
            jVar.v().b("JsonUtils", sb.toString(), e);
            return j;
        }
    }

    public static Bundle a(Object obj) {
        JSONObject jSONObject;
        if (obj instanceof JSONObject) {
            jSONObject = (JSONObject) obj;
        } else {
            if (obj instanceof String) {
                try {
                    jSONObject = new JSONObject((String) obj);
                } catch (JSONException unused) {
                }
            }
            jSONObject = null;
        }
        return c(jSONObject);
    }

    public static Boolean a(JSONObject jSONObject, String str, Boolean bool, j jVar) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                bool = Boolean.valueOf(jSONObject.getBoolean(str));
            } catch (JSONException unused) {
                boolean z = true;
                if (b(jSONObject, str, (bool == null || !bool.booleanValue()) ? 0 : 1, jVar) <= 0) {
                    z = false;
                }
                return Boolean.valueOf(z);
            }
        }
        return bool;
    }

    public static Object a(JSONArray jSONArray, int i, Object obj, j jVar) {
        if (jSONArray != null && jSONArray.length() > i) {
            try {
                return jSONArray.get(i);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to retrieve object at index ");
                    sb.append(i);
                    sb.append(" for JSON array");
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
        return obj;
    }

    public static Object a(JSONObject jSONObject, String str, Object obj, j jVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return obj;
        }
        try {
            Object obj2 = jSONObject.get(str);
            return obj2 != null ? obj2 : obj;
        } catch (JSONException e) {
            if (jVar == null) {
                return obj;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to retrieve Object for key = ");
            sb.append(str);
            jVar.v().b("JsonUtils", sb.toString(), e);
            return obj;
        }
    }

    public static String a(Map<String, Object> map, String str, j jVar) {
        try {
            return a(map).toString();
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to convert map '");
            sb.append(map);
            sb.append("' to JSON string.");
            jVar.v().b("JsonUtils", sb.toString(), e);
            return str;
        }
    }

    public static ArrayList<Bundle> a(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return new ArrayList<>();
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(c(jSONArray.optJSONObject(i)));
        }
        return arrayList;
    }

    public static List a(JSONObject jSONObject, String str, List list, j jVar) {
        try {
            JSONArray b = b(jSONObject, str, (JSONArray) null, jVar);
            return b != null ? b(b) : list;
        } catch (JSONException unused) {
            return list;
        }
    }

    public static Map<String, String> a(Bundle bundle) {
        if (bundle == null) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap(bundle.size());
        for (String str : bundle.keySet()) {
            hashMap.put(str, String.valueOf(bundle.get(str)));
        }
        return hashMap;
    }

    public static Map<String, Object> a(String str, Map<String, Object> map, j jVar) {
        try {
            return b(new JSONObject(str));
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to convert json string '");
            sb.append(str);
            sb.append("' to map");
            jVar.v().b("JsonUtils", sb.toString(), e);
            return map;
        }
    }

    public static Map<String, String> a(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            hashMap.put(str, b(jSONObject.get(str)).toString());
        }
        return hashMap;
    }

    public static JSONObject a(String str, j jVar) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return new JSONObject(str);
        } catch (Throwable unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to deserialize into JSON: ");
            sb.append(str);
            jVar.v().e("JsonUtils", sb.toString());
            return null;
        }
    }

    public static JSONObject a(String str, JSONObject jSONObject, j jVar) {
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            if (jVar != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to convert JSON string '");
                sb.append(str);
                sb.append("' to JSONObject");
                jVar.v().b("JsonUtils", sb.toString(), e);
            }
            return jSONObject;
        }
    }

    public static JSONObject a(Map<String, ?> map) throws JSONException {
        if (map == null) {
            return new JSONObject();
        }
        JSONObject jSONObject = new JSONObject();
        for (Entry entry : map.entrySet()) {
            jSONObject.put((String) entry.getKey(), entry.getValue());
        }
        return jSONObject;
    }

    public static JSONObject a(JSONArray jSONArray, int i, JSONObject jSONObject, j jVar) {
        if (jSONArray != null && i < jSONArray.length()) {
            try {
                return jSONArray.getJSONObject(i);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to retrieve JSON object from array for index = ");
                    sb.append(i);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
        return jSONObject;
    }

    public static void a(JSONObject jSONObject, String str, int i, j jVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, i);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to put int property for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, String str2, j jVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, str2);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to put String property for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, JSONArray jSONArray, j jVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, jSONArray);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to put JSONArray property for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, JSONObject jSONObject2, j jVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, jSONObject2);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to put JSON property for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
    }

    public static boolean a(String str, JSONArray jSONArray) {
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                Object obj = jSONArray.get(i);
                if ((obj instanceof String) && ((String) obj).equalsIgnoreCase(str)) {
                    return true;
                }
                i++;
            } catch (JSONException unused) {
            }
        }
        return false;
    }

    public static boolean a(JSONObject jSONObject, String str) {
        return jSONObject != null && jSONObject.has(str);
    }

    public static int b(JSONObject jSONObject, String str, int i, j jVar) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return jSONObject.getInt(str);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to retrieve int property for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
        return i;
    }

    private static Object b(Object obj) throws JSONException {
        if (obj == JSONObject.NULL) {
            return null;
        }
        if (obj instanceof JSONObject) {
            return b((JSONObject) obj);
        }
        if (obj instanceof JSONArray) {
            obj = b((JSONArray) obj);
        }
        return obj;
    }

    public static String b(JSONObject jSONObject, String str, String str2, j jVar) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return jSONObject.getString(str);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to retrieve string property for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
        return str2;
    }

    public static List b(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(b(jSONArray.get(i)));
        }
        return arrayList;
    }

    public static Map<String, Object> b(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            hashMap.put(str, b(jSONObject.get(str)));
        }
        return hashMap;
    }

    public static JSONArray b(JSONObject jSONObject, String str, JSONArray jSONArray, j jVar) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return jSONObject.getJSONArray(str);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to retrieve JSON array for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
        return jSONArray;
    }

    public static JSONObject b(JSONObject jSONObject, String str, JSONObject jSONObject2, j jVar) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return jSONObject.getJSONObject(str);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to retrieve JSON property for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
        return jSONObject2;
    }

    public static void b(JSONObject jSONObject, String str, long j, j jVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, j);
            } catch (JSONException e) {
                if (jVar != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to put long property for key = ");
                    sb.append(str);
                    jVar.v().b("JsonUtils", sb.toString(), e);
                }
            }
        }
    }

    public static Bundle c(JSONObject jSONObject) {
        if (jSONObject == null || jSONObject.length() == 0) {
            return new Bundle();
        }
        Bundle bundle = new Bundle();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            if (jSONObject.isNull(str)) {
                bundle.putString(str, null);
            } else {
                Object opt = jSONObject.opt(str);
                if (opt instanceof JSONObject) {
                    bundle.putBundle(str, c((JSONObject) opt));
                } else if (opt instanceof JSONArray) {
                    JSONArray jSONArray = (JSONArray) opt;
                    if (jSONArray.length() == 0) {
                        bundle.putStringArrayList(str, new ArrayList(0));
                    } else if (a(jSONArray, 0, (Object) null, (j) null) instanceof String) {
                        ArrayList arrayList = new ArrayList(jSONArray.length());
                        for (int i = 0; i < jSONArray.length(); i++) {
                            arrayList.add((String) a(jSONArray, i, (Object) null, (j) null));
                        }
                        bundle.putStringArrayList(str, arrayList);
                    } else {
                        bundle.putParcelableArrayList(str, a(jSONArray));
                    }
                } else if (opt instanceof Boolean) {
                    bundle.putBoolean(str, ((Boolean) opt).booleanValue());
                } else if (opt instanceof String) {
                    bundle.putString(str, (String) opt);
                } else if (opt instanceof Integer) {
                    bundle.putInt(str, ((Integer) opt).intValue());
                } else if (opt instanceof Long) {
                    bundle.putLong(str, ((Long) opt).longValue());
                } else if (opt instanceof Double) {
                    bundle.putDouble(str, ((Double) opt).doubleValue());
                }
            }
        }
        return bundle;
    }

    public static String d(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        try {
            return jSONObject.toString(4);
        } catch (JSONException unused) {
            return jSONObject.toString();
        }
    }
}
