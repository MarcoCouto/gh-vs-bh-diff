package com.applovin.impl.sdk.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class s {
    public static final s a = new s();
    protected String b;
    protected final List<s> c;
    private final s d;
    private final String e;
    private final Map<String, String> f;

    private s() {
        this.d = null;
        this.e = "";
        this.f = Collections.emptyMap();
        this.b = "";
        this.c = Collections.emptyList();
    }

    public s(String str, Map<String, String> map, s sVar) {
        this.d = sVar;
        this.e = str;
        this.f = Collections.unmodifiableMap(map);
        this.c = new ArrayList();
    }

    public String a() {
        return this.e;
    }

    public List<s> a(String str) {
        if (str != null) {
            ArrayList arrayList = new ArrayList(this.c.size());
            for (s sVar : this.c) {
                if (str.equalsIgnoreCase(sVar.a())) {
                    arrayList.add(sVar);
                }
            }
            return arrayList;
        }
        throw new IllegalArgumentException("No name specified.");
    }

    public s b(String str) {
        if (str != null) {
            for (s sVar : this.c) {
                if (str.equalsIgnoreCase(sVar.a())) {
                    return sVar;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("No name specified.");
    }

    public Map<String, String> b() {
        return this.f;
    }

    public s c(String str) {
        if (str != null) {
            if (this.c.size() > 0) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(this);
                while (!arrayList.isEmpty()) {
                    s sVar = (s) arrayList.get(0);
                    arrayList.remove(0);
                    if (str.equalsIgnoreCase(sVar.a())) {
                        return sVar;
                    }
                    arrayList.addAll(sVar.d());
                }
            }
            return null;
        }
        throw new IllegalArgumentException("No name specified.");
    }

    public String c() {
        return this.b;
    }

    public List<s> d() {
        return Collections.unmodifiableList(this.c);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("XmlNode{elementName='");
        sb.append(this.e);
        sb.append('\'');
        sb.append(", text='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", attributes=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
}
