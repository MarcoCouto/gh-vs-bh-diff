package com.applovin.impl.sdk.utils;

import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Map;
import java.util.Map.Entry;

public class n {
    private static final char[] a = "0123456789abcdef".toCharArray();

    public static int a(String str) {
        return a(str, 0);
    }

    public static int a(String str, int i) {
        return d(str) ? Integer.parseInt(str) : i;
    }

    public static String a(int i, String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        if (i > str.length()) {
            i = str.length();
        }
        return str.substring(0, i);
    }

    private static String a(String str, Integer num) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
            instance.update(str.getBytes("UTF-8"));
            String a2 = a(instance.digest());
            return num.intValue() > 0 ? a2.substring(0, Math.min(num.intValue(), a2.length())) : a2;
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("SHA-1 for \"");
            sb.append(str);
            sb.append("\" failed.");
            throw new RuntimeException(sb.toString(), th);
        }
    }

    public static String a(String str, String str2, String str3) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return str;
        }
        Builder buildUpon = Uri.parse(str).buildUpon();
        buildUpon.appendQueryParameter(str2, str3);
        return buildUpon.build().toString();
    }

    public static String a(String str, Map<String, String> map) {
        if (!(str == null || map == null)) {
            for (Entry entry : map.entrySet()) {
                str = str.replace((CharSequence) entry.getKey(), (CharSequence) entry.getValue());
            }
        }
        return str;
    }

    public static String a(boolean z) {
        return z ? "1" : "0";
    }

    public static String a(byte[] bArr) {
        if (bArr != null) {
            char[] cArr = new char[(bArr.length * 2)];
            for (int i = 0; i < bArr.length; i++) {
                int i2 = i * 2;
                cArr[i2] = a[(bArr[i] & 240) >>> 4];
                cArr[i2 + 1] = a[bArr[i] & 15];
            }
            return new String(cArr);
        }
        throw new IllegalArgumentException("No data specified");
    }

    public static boolean a(String str, String str2) {
        return b(str) && b(str2) && str.toLowerCase().contains(str2.toLowerCase());
    }

    public static String b(String str, Map<String, String> map) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        if (map != null && !map.isEmpty()) {
            Builder buildUpon = Uri.parse(str).buildUpon();
            for (Entry entry : map.entrySet()) {
                buildUpon.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
            }
            str = buildUpon.build().toString();
        }
        return str;
    }

    public static boolean b(String str) {
        return !TextUtils.isEmpty(str);
    }

    public static String c(String str) {
        return str == null ? "" : str;
    }

    public static boolean d(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        char charAt = str.charAt(0);
        int i = (charAt == '-' || charAt == '+') ? 1 : 0;
        int length = str.length();
        if (i == 1 && length == 1) {
            return false;
        }
        while (i < length) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
            i++;
        }
        return true;
    }

    public static String e(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public static String f(String str) {
        return a(str, Integer.valueOf(16));
    }
}
