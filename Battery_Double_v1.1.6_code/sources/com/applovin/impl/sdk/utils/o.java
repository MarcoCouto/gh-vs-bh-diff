package com.applovin.impl.sdk.utils;

import com.applovin.impl.sdk.j;
import java.util.Timer;
import java.util.TimerTask;

public class o {
    /* access modifiers changed from: private */
    public final j a;
    /* access modifiers changed from: private */
    public Timer b;
    private long c;
    private long d;
    /* access modifiers changed from: private */
    public final Runnable e;
    private long f;
    /* access modifiers changed from: private */
    public final Object g = new Object();

    private o(j jVar, Runnable runnable) {
        this.a = jVar;
        this.e = runnable;
    }

    public static o a(long j, j jVar, Runnable runnable) {
        if (j < 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot create a scheduled timer. Invalid fire time passed in: ");
            sb.append(j);
            sb.append(".");
            throw new IllegalArgumentException(sb.toString());
        } else if (runnable != null) {
            o oVar = new o(jVar, runnable);
            oVar.c = System.currentTimeMillis();
            oVar.d = j;
            try {
                oVar.b = new Timer();
                oVar.b.schedule(oVar.e(), j);
            } catch (OutOfMemoryError e2) {
                jVar.v().b("Timer", "Failed to create timer due to OOM error", e2);
            }
            return oVar;
        } else {
            throw new IllegalArgumentException("Cannot create a scheduled timer. Runnable is null.");
        }
    }

    private TimerTask e() {
        return new TimerTask() {
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            public void run() {
                try {
                    o.this.e.run();
                    synchronized (o.this.g) {
                        o.this.b = null;
                    }
                } catch (Throwable th) {
                    try {
                        if (o.this.a != null) {
                            o.this.a.v().b("Timer", "Encountered error while executing timed task", th);
                        }
                        synchronized (o.this.g) {
                            o.this.b = null;
                        }
                    } catch (Throwable th2) {
                        synchronized (o.this.g) {
                            o.this.b = null;
                            throw th2;
                        }
                    }
                }
            }
        };
    }

    public long a() {
        if (this.b == null) {
            return this.d - this.f;
        }
        return this.d - (System.currentTimeMillis() - this.c);
    }

    public void b() {
        synchronized (this.g) {
            if (this.b != null) {
                try {
                    this.b.cancel();
                    this.f = System.currentTimeMillis() - this.c;
                } catch (Throwable th) {
                    try {
                        if (this.a != null) {
                            this.a.v().b("Timer", "Encountered error while pausing timer", th);
                        }
                    } catch (Throwable th2) {
                        this.b = null;
                        throw th2;
                    }
                }
                this.b = null;
            }
        }
    }

    public void c() {
        synchronized (this.g) {
            if (this.f > 0) {
                try {
                    this.d -= this.f;
                    if (this.d < 0) {
                        this.d = 0;
                    }
                    this.b = new Timer();
                    this.b.schedule(e(), this.d);
                    this.c = System.currentTimeMillis();
                } catch (Throwable th) {
                    try {
                        if (this.a != null) {
                            this.a.v().b("Timer", "Encountered error while resuming timer", th);
                        }
                    } catch (Throwable th2) {
                        this.f = 0;
                        throw th2;
                    }
                }
                this.f = 0;
            }
        }
    }

    public void d() {
        synchronized (this.g) {
            if (this.b != null) {
                try {
                    this.b.cancel();
                    this.b = null;
                } catch (Throwable th) {
                    try {
                        if (this.a != null) {
                            this.a.v().b("Timer", "Encountered error while cancelling timer", th);
                        }
                        this.b = null;
                    } catch (Throwable th2) {
                        this.b = null;
                        this.f = 0;
                        throw th2;
                    }
                }
                this.f = 0;
            }
        }
    }
}
