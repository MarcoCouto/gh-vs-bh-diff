package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.utils.n;
import com.ironsource.sdk.constants.Constants.RequestParameters;

public class p {
    private final j a;

    p(j jVar) {
        this.a = jVar;
    }

    private void a(String str, String str2, boolean z) {
        if (n.b(str2)) {
            int intValue = ((Integer) this.a.a(d.ak)).intValue();
            if (intValue > 0) {
                int length = str2.length();
                int i = ((length + intValue) - 1) / intValue;
                for (int i2 = 0; i2 < i; i2++) {
                    int i3 = i2 * intValue;
                    int min = Math.min(length, i3 + intValue);
                    if (z) {
                        Log.d(str, str2.substring(i3, min));
                    } else {
                        b(str, str2.substring(i3, min));
                    }
                }
            }
        }
    }

    private boolean a() {
        return this.a.C().e();
    }

    public static void c(String str, String str2, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.e("AppLovinSdk", sb.toString(), th);
    }

    public static void g(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.d("AppLovinSdk", sb.toString());
    }

    public static void h(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.i("AppLovinSdk", sb.toString());
    }

    public static void i(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.w("AppLovinSdk", sb.toString());
    }

    public static void j(String str, String str2) {
        c(str, str2, null);
    }

    private void k(String str, String str2) {
    }

    public void a(String str, Boolean bool, String str2) {
        a(str, bool, str2, null);
    }

    public void a(String str, Boolean bool, String str2, Throwable th) {
        if (a()) {
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            String sb2 = sb.toString();
            Log.e("AppLovinSdk", sb2, th);
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb2);
            sb3.append(" : ");
            sb3.append(th);
            k("ERROR", sb3.toString());
        }
        if (bool.booleanValue() && ((Boolean) this.a.a(d.eE)).booleanValue() && this.a.P() != null) {
            this.a.P().a(str2, th);
        }
    }

    public void a(String str, String str2) {
        if (a()) {
            a(str, str2, false);
        }
    }

    public void a(String str, String str2, Throwable th) {
        if (a()) {
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            String sb2 = sb.toString();
            Log.w("AppLovinSdk", sb2, th);
            k("WARN", sb2);
        }
    }

    public void b(String str, String str2) {
        if (a()) {
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            String sb2 = sb.toString();
            Log.d("AppLovinSdk", sb2);
            k("DEBUG", sb2);
        }
    }

    public void b(String str, String str2, Throwable th) {
        a(str, Boolean.valueOf(true), str2, th);
    }

    public void c(String str, String str2) {
        if (a()) {
            StringBuilder sb = new StringBuilder();
            sb.append(RequestParameters.LEFT_BRACKETS);
            sb.append(str);
            sb.append("] ");
            sb.append(str2);
            String sb2 = sb.toString();
            Log.i("AppLovinSdk", sb2);
            k("INFO", sb2);
        }
    }

    public void d(String str, String str2) {
        a(str, str2, (Throwable) null);
    }

    public void e(String str, String str2) {
        b(str, str2, null);
    }

    public void f(String str, String str2) {
        a(str, str2, true);
    }
}
