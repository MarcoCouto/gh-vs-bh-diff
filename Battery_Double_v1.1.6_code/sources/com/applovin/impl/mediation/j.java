package com.applovin.impl.mediation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.o;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class j {
    /* access modifiers changed from: private */
    public final List<b> a = Collections.synchronizedList(new ArrayList());
    private final a b;
    private final a c;

    private static class a extends BroadcastReceiver implements MaxAdListener, MaxRewardedAdListener {
        private final com.applovin.impl.sdk.j a;
        private final j b;
        private final MaxAdFormat c;
        private final d<String> d;
        /* access modifiers changed from: private */
        public MaxAdListener e;
        /* access modifiers changed from: private */
        public c f;
        private final Object g;
        private o h;
        private long i;
        private final AtomicBoolean j;
        private volatile boolean k;

        private a(d<String> dVar, MaxAdFormat maxAdFormat, j jVar, com.applovin.impl.sdk.j jVar2) {
            this.g = new Object();
            this.j = new AtomicBoolean();
            this.b = jVar;
            this.a = jVar2;
            this.d = dVar;
            this.c = maxAdFormat;
            jVar2.af().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
            jVar2.af().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
        }

        private void a(long j2) {
            if (j2 > 0) {
                this.i = System.currentTimeMillis() + j2;
                this.h = o.a(j2, this.a, new Runnable() {
                    public void run() {
                        a.this.b(true);
                    }
                });
            }
        }

        private void a(boolean z) {
            if (this.a.Y().a()) {
                this.k = z;
                this.j.set(true);
                return;
            }
            String str = (String) this.a.a(this.d);
            if (n.b(str)) {
                this.a.y().loadAd(str, this.c, new com.applovin.impl.mediation.f.a().a("fa", String.valueOf(true)).a("faie", String.valueOf(z)).a(), true, this.a.ag(), this);
            }
        }

        /* access modifiers changed from: private */
        public void b() {
            a(false);
        }

        /* access modifiers changed from: private */
        public void b(boolean z) {
            synchronized (this.g) {
                this.i = 0;
                c();
                this.f = null;
            }
            a(z);
        }

        private void c() {
            synchronized (this.g) {
                if (this.h != null) {
                    this.h.d();
                    this.h = null;
                }
            }
        }

        public void a() {
            if (this.j.compareAndSet(true, false)) {
                a(this.k);
            } else if (this.i != 0) {
                long currentTimeMillis = this.i - System.currentTimeMillis();
                if (currentTimeMillis <= 0) {
                    b(true);
                } else {
                    a(currentTimeMillis);
                }
            }
        }

        public void onAdClicked(MaxAd maxAd) {
            this.e.onAdClicked(maxAd);
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i2) {
            this.e.onAdDisplayFailed(maxAd, i2);
        }

        public void onAdDisplayed(MaxAd maxAd) {
            this.e.onAdDisplayed(maxAd);
            b(false);
        }

        public void onAdHidden(MaxAd maxAd) {
            this.e.onAdHidden(maxAd);
            this.e = null;
        }

        public void onAdLoadFailed(String str, int i2) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    a.this.b();
                }
            }, TimeUnit.SECONDS.toMillis(((Long) this.a.a(com.applovin.impl.sdk.b.c.R)).longValue()));
        }

        public void onAdLoaded(MaxAd maxAd) {
            this.f = (c) maxAd;
            a(this.f.l());
            Iterator it = new ArrayList(this.b.a).iterator();
            while (it.hasNext()) {
                ((b) it.next()).a(this.f);
            }
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                c();
            } else if ("com.applovin.application_resumed".equals(action)) {
                a();
            }
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            if (this.e instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) this.e).onRewardedVideoCompleted(maxAd);
            }
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            if (this.e instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) this.e).onRewardedVideoStarted(maxAd);
            }
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            if (this.e instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) this.e).onUserRewarded(maxAd, maxReward);
            }
        }
    }

    public interface b {
        void a(c cVar);
    }

    public j(com.applovin.impl.sdk.j jVar) {
        a aVar = new a(com.applovin.impl.sdk.b.c.O, MaxAdFormat.INTERSTITIAL, this, jVar);
        this.b = aVar;
        a aVar2 = new a(com.applovin.impl.sdk.b.c.P, MaxAdFormat.REWARDED, this, jVar);
        this.c = aVar2;
    }

    private a b(MaxAdFormat maxAdFormat) {
        if (MaxAdFormat.INTERSTITIAL == maxAdFormat) {
            return this.b;
        }
        if (MaxAdFormat.REWARDED == maxAdFormat) {
            return this.c;
        }
        return null;
    }

    public c a(MaxAdFormat maxAdFormat) {
        a b2 = b(maxAdFormat);
        if (b2 != null) {
            return b2.f;
        }
        return null;
    }

    public void a() {
        this.b.b();
        this.c.b();
    }

    public void a(b bVar) {
        this.a.add(bVar);
    }

    public void a(MaxAdListener maxAdListener, MaxAdFormat maxAdFormat) {
        a b2 = b(maxAdFormat);
        if (b2 != null) {
            b2.e = maxAdListener;
        }
    }

    public void b(b bVar) {
        this.a.remove(bVar);
    }
}
