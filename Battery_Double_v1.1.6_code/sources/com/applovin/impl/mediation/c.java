package com.applovin.impl.mediation;

import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.d;

public class c {
    private final j a;
    /* access modifiers changed from: private */
    public final p b;
    /* access modifiers changed from: private */
    public final a c;
    private d d;

    public interface a {
        void c(com.applovin.impl.mediation.b.c cVar);
    }

    c(j jVar, a aVar) {
        this.a = jVar;
        this.b = jVar.v();
        this.c = aVar;
    }

    public void a() {
        this.b.b("AdHiddenCallbackTimeoutManager", "Cancelling timeout");
        if (this.d != null) {
            this.d.a();
            this.d = null;
        }
    }

    public void a(final com.applovin.impl.mediation.b.c cVar, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("Scheduling in ");
        sb.append(j);
        sb.append("ms...");
        this.b.b("AdHiddenCallbackTimeoutManager", sb.toString());
        this.d = d.a(j, this.a, new Runnable() {
            public void run() {
                c.this.b.b("AdHiddenCallbackTimeoutManager", "Timing out...");
                c.this.c.c(cVar);
            }
        });
    }
}
