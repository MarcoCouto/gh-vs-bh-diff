package com.applovin.impl.mediation.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.applovin.impl.mediation.a.a.d;
import com.applovin.impl.mediation.a.c.a.b;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a.c;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.i;
import com.applovin.mediation.MaxDebuggerActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class a implements c<JSONObject> {
    /* access modifiers changed from: private */
    public static WeakReference<MaxDebuggerActivity> a;
    /* access modifiers changed from: private */
    public static final AtomicBoolean b = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final j c;
    private final p d;
    /* access modifiers changed from: private */
    public final b e;
    private final AtomicBoolean f = new AtomicBoolean();
    private boolean g;

    public a(j jVar) {
        this.c = jVar;
        this.d = jVar.v();
        this.e = new b(jVar.D());
    }

    private List<d> a(JSONObject jSONObject, j jVar) {
        JSONArray b2 = i.b(jSONObject, "networks", new JSONArray(), jVar);
        ArrayList arrayList = new ArrayList(b2.length());
        for (int i = 0; i < b2.length(); i++) {
            JSONObject a2 = i.a(b2, i, (JSONObject) null, jVar);
            if (a2 != null) {
                arrayList.add(new d(a2, jVar));
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    private void e() {
        if (this.f.compareAndSet(false, true)) {
            this.c.K().a((com.applovin.impl.sdk.d.a) new com.applovin.impl.mediation.a.b.a(this, this.c), com.applovin.impl.sdk.d.r.a.MEDIATION_MAIN);
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        return (a == null || a.get() == null) ? false : true;
    }

    public void a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("Unable to fetch mediation debugger info: server returned ");
        sb.append(i);
        this.d.e("MediationDebuggerService", sb.toString());
        p.j("AppLovinSdk", "Unable to show mediation debugger.");
        this.e.a(null, this.c);
        this.f.set(false);
    }

    public void a(JSONObject jSONObject, int i) {
        List<d> a2 = a(jSONObject, this.c);
        this.e.a(a2, this.c);
        StringBuilder sb = new StringBuilder();
        for (d l : a2) {
            sb.append(l.l());
        }
        sb.append("\n------------------ END ------------------");
        this.d.f("MediationDebuggerService", sb.toString());
    }

    public void a(boolean z) {
        this.g = z;
    }

    public boolean a() {
        return this.g;
    }

    public void b() {
        e();
        if (f() || !b.compareAndSet(false, true)) {
            p.j("AppLovinSdk", "Mediation Debugger is already showing.");
            return;
        }
        this.c.aa().a(new com.applovin.impl.sdk.utils.a() {
            public void onActivityDestroyed(Activity activity) {
                if (activity instanceof MaxDebuggerActivity) {
                    a.this.c.aa().b(this);
                    a.a = null;
                }
            }

            public void onActivityStarted(Activity activity) {
                if (activity instanceof MaxDebuggerActivity) {
                    if (!a.this.f() || a.a.get() != activity) {
                        MaxDebuggerActivity maxDebuggerActivity = (MaxDebuggerActivity) activity;
                        a.a = new WeakReference(maxDebuggerActivity);
                        maxDebuggerActivity.setListAdapter(a.this.e, a.this.c.aa());
                    }
                    a.b.set(false);
                }
            }
        });
        Context D = this.c.D();
        Intent intent = new Intent(D, MaxDebuggerActivity.class);
        intent.setFlags(268435456);
        D.startActivity(intent);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediationDebuggerService{, listAdapter=");
        sb.append(this.e);
        sb.append("}");
        return sb.toString();
    }
}
