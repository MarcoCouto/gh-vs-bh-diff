package com.applovin.impl.mediation.a.a;

import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.c;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public class e {
    private final boolean a;
    private final boolean b;
    private final boolean c;
    private final String d;

    public e(JSONObject jSONObject, j jVar) {
        this.a = c.a(jVar.D()).a();
        JSONObject b2 = i.b(jSONObject, "cleartext_traffic", (JSONObject) null, jVar);
        boolean z = false;
        if (b2 != null) {
            this.b = true;
            this.d = i.b(b2, "description", "", jVar);
            if (h.a()) {
                this.c = true;
                return;
            }
            List a2 = i.a(b2, "domains", (List) new ArrayList(), jVar);
            if (a2.size() > 0) {
                Iterator it = a2.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (!h.a((String) it.next())) {
                            break;
                        }
                    } else {
                        z = true;
                        break;
                    }
                }
            }
            this.c = z;
            return;
        }
        this.b = false;
        this.d = "";
        this.c = h.a();
    }

    public boolean a() {
        return this.b;
    }

    public boolean b() {
        return this.c;
    }

    public String c() {
        return this.a ? this.d : "You must include an entry in your AndroidManifest.xml to point to your network_security_config.xml.\n\nFor more information, visit: https://developer.android.com/training/articles/security-config";
    }
}
