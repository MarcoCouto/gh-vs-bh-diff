package com.applovin.impl.mediation.a.a;

import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.q;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public class a {
    private final String a;
    private final String b;
    private final boolean c;

    a(JSONObject jSONObject, j jVar) {
        boolean z;
        this.a = i.b(jSONObject, "name", "", jVar);
        this.b = i.b(jSONObject, "description", "", jVar);
        List a2 = i.a(jSONObject, "existence_classes", (List) null, jVar);
        if (a2 != null) {
            z = false;
            Iterator it = a2.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (q.e((String) it.next())) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
        } else {
            z = q.e(i.b(jSONObject, "existence_class", "", jVar));
        }
        this.c = z;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }
}
