package com.applovin.impl.mediation.a.a;

import android.text.TextUtils;
import com.applovin.impl.mediation.d.c;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.q;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class d implements Comparable<d> {
    private final a a;
    private final boolean b;
    private final boolean c;
    private final boolean d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;
    private final String i;
    private final List<MaxAdFormat> j;
    private final List<f> k;
    private final List<a> l;
    private final e m;

    public enum a {
        MISSING("MISSING"),
        INCOMPLETE_INTEGRATION("INCOMPLETE INTEGRATION"),
        INVALID_INTEGRATION("INVALID INTEGRATION"),
        COMPLETE("COMPLETE");
        
        private final String e;

        private a(String str) {
            this.e = str;
        }

        /* access modifiers changed from: private */
        public String a() {
            return this.e;
        }
    }

    public d(JSONObject jSONObject, j jVar) {
        String sdkVersion;
        this.e = i.b(jSONObject, "display_name", "", jVar);
        this.h = i.b(jSONObject, "name", "", jVar);
        this.i = i.b(jSONObject, "latest_adapter_version", "", jVar);
        JSONObject b2 = i.b(jSONObject, "configuration", new JSONObject(), jVar);
        this.k = a(b2, jVar);
        this.l = b(b2, jVar);
        this.m = new e(b2, jVar);
        this.b = q.e(i.b(jSONObject, "existence_class", "", jVar));
        String str = "";
        String str2 = "";
        List<MaxAdFormat> emptyList = Collections.emptyList();
        MaxAdapter a2 = c.a(i.b(jSONObject, "adapter_class", "", jVar), jVar);
        if (a2 != null) {
            this.c = true;
            try {
                String adapterVersion = a2.getAdapterVersion();
                try {
                    sdkVersion = a2.getSdkVersion();
                } catch (Throwable th) {
                    th = th;
                    str = adapterVersion;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Failed to load adapter for network ");
                    sb.append(this.e);
                    sb.append(". Please check that you have a compatible network SDK integrated. Error: ");
                    sb.append(th);
                    p.j("MediatedNetwork", sb.toString());
                    this.g = str;
                    this.f = str2;
                    this.j = emptyList;
                    this.a = m();
                    this.d = !str.equals(this.i);
                }
                try {
                    emptyList = a(a2);
                    str2 = sdkVersion;
                    str = adapterVersion;
                } catch (Throwable th2) {
                    th = th2;
                    str2 = sdkVersion;
                    str = adapterVersion;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to load adapter for network ");
                    sb2.append(this.e);
                    sb2.append(". Please check that you have a compatible network SDK integrated. Error: ");
                    sb2.append(th);
                    p.j("MediatedNetwork", sb2.toString());
                    this.g = str;
                    this.f = str2;
                    this.j = emptyList;
                    this.a = m();
                    this.d = !str.equals(this.i);
                }
            } catch (Throwable th3) {
                th = th3;
                StringBuilder sb22 = new StringBuilder();
                sb22.append("Failed to load adapter for network ");
                sb22.append(this.e);
                sb22.append(". Please check that you have a compatible network SDK integrated. Error: ");
                sb22.append(th);
                p.j("MediatedNetwork", sb22.toString());
                this.g = str;
                this.f = str2;
                this.j = emptyList;
                this.a = m();
                this.d = !str.equals(this.i);
            }
        } else {
            this.c = false;
        }
        this.g = str;
        this.f = str2;
        this.j = emptyList;
        this.a = m();
        this.d = !str.equals(this.i);
    }

    private List<MaxAdFormat> a(MaxAdapter maxAdapter) {
        ArrayList arrayList = new ArrayList(5);
        if (maxAdapter instanceof MaxInterstitialAdapter) {
            arrayList.add(MaxAdFormat.INTERSTITIAL);
        }
        if (maxAdapter instanceof MaxRewardedAdapter) {
            arrayList.add(MaxAdFormat.REWARDED);
        }
        if (maxAdapter instanceof MaxAdViewAdapter) {
            arrayList.add(MaxAdFormat.BANNER);
            arrayList.add(MaxAdFormat.LEADER);
            arrayList.add(MaxAdFormat.MREC);
        }
        return arrayList;
    }

    private List<f> a(JSONObject jSONObject, j jVar) {
        ArrayList arrayList = new ArrayList();
        JSONObject b2 = i.b(jSONObject, NativeProtocol.RESULT_ARGS_PERMISSIONS, new JSONObject(), jVar);
        Iterator keys = b2.keys();
        while (keys.hasNext()) {
            try {
                String str = (String) keys.next();
                arrayList.add(new f(str, b2.getString(str), jVar.D()));
            } catch (JSONException unused) {
            }
        }
        return arrayList;
    }

    private List<a> b(JSONObject jSONObject, j jVar) {
        ArrayList arrayList = new ArrayList();
        JSONArray b2 = i.b(jSONObject, "dependencies", new JSONArray(), jVar);
        for (int i2 = 0; i2 < b2.length(); i2++) {
            JSONObject a2 = i.a(b2, i2, (JSONObject) null, jVar);
            if (a2 != null) {
                arrayList.add(new a(a2, jVar));
            }
        }
        return arrayList;
    }

    private a m() {
        if (!this.b && !this.c) {
            return a.MISSING;
        }
        for (f c2 : this.k) {
            if (!c2.c()) {
                return a.INVALID_INTEGRATION;
            }
        }
        for (a c3 : this.l) {
            if (!c3.c()) {
                return a.INVALID_INTEGRATION;
            }
        }
        return (!this.m.a() || this.m.b()) ? (!this.b || !this.c) ? a.INCOMPLETE_INTEGRATION : a.COMPLETE : a.INVALID_INTEGRATION;
    }

    /* renamed from: a */
    public int compareTo(d dVar) {
        return this.e.compareToIgnoreCase(dVar.e);
    }

    public a a() {
        return this.a;
    }

    public boolean b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }

    public boolean d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public String f() {
        return this.f;
    }

    public String g() {
        return this.g;
    }

    public String h() {
        return this.i;
    }

    public List<f> i() {
        return this.k;
    }

    public List<a> j() {
        return this.l;
    }

    public final e k() {
        return this.m;
    }

    public final String l() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n------------------ ");
        sb.append(this.e);
        sb.append(" ------------------");
        sb.append("\nStatus  - ");
        sb.append(this.a.a());
        sb.append("\nAdapter - ");
        sb.append((!this.c || TextUtils.isEmpty(this.g)) ? "UNAVAILABLE" : this.g);
        sb.append("\nSDK     - ");
        sb.append((!this.b || TextUtils.isEmpty(this.f)) ? "UNAVAILABLE" : this.f);
        if (this.m.a() && !this.m.b()) {
            sb.append("\n* ");
            sb.append(this.m.c());
        }
        for (f fVar : i()) {
            if (!fVar.c()) {
                sb.append("\n* MISSING ");
                sb.append(fVar.a());
                sb.append(": ");
                sb.append(fVar.b());
            }
        }
        for (a aVar : j()) {
            if (!aVar.c()) {
                sb.append("\n* MISSING ");
                sb.append(aVar.a());
                sb.append(": ");
                sb.append(aVar.b());
            }
        }
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedNetwork{name=");
        sb.append(this.e);
        sb.append(", sdkAvailable=");
        sb.append(this.b);
        sb.append(", sdkVersion=");
        sb.append(this.f);
        sb.append(", adapterAvailable=");
        sb.append(this.c);
        sb.append(", adapterVersion=");
        sb.append(this.g);
        sb.append("}");
        return sb.toString();
    }
}
