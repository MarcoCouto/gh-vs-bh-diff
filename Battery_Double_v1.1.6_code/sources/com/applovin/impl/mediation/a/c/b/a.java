package com.applovin.impl.mediation.a.c.b;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.os.Bundle;
import android.widget.ListView;
import com.applovin.impl.mediation.a.a.d;
import com.applovin.sdk.R;

public class a extends Activity {
    private ListView a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.mediation_debugger_detail_activity);
        this.a = (ListView) findViewById(R.id.listView);
    }

    public void setNetwork(d dVar) {
        setTitle(dVar.e());
        b bVar = new b(dVar, this);
        bVar.a((com.applovin.impl.mediation.a.c.b.b.a) new com.applovin.impl.mediation.a.c.b.b.a() {
            public void a(String str) {
                new Builder(a.this, 16974130).setTitle(R.string.applovin_instructions_dialog_title).setMessage(str).setNegativeButton(17039370, null).create().show();
            }
        });
        this.a.setAdapter(bVar);
    }
}
