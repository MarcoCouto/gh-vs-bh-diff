package com.applovin.impl.mediation.a.c.b;

import android.content.Context;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.impl.mediation.a.a.d;
import com.applovin.impl.mediation.a.a.e;
import com.applovin.impl.mediation.a.a.g;
import com.applovin.impl.mediation.a.c.b.a.a.C0009a;
import com.applovin.impl.sdk.utils.f;
import com.applovin.sdk.R;
import com.appodeal.ads.utils.LogConstants;
import java.util.ArrayList;
import java.util.List;

public class b extends com.applovin.impl.mediation.a.c.a {
    private final c c = new g("INTEGRATIONS");
    private final c d = new g("PERMISSIONS");
    private final c e = new g("CONFIGURATION");
    private final c f = new g("DEPENDENCIES");
    private final c g = new g("");
    private SpannedString h;
    private a i;

    public interface a {
        void a(String str);
    }

    b(d dVar, Context context) {
        super(context);
        if (dVar.a() == com.applovin.impl.mediation.a.a.d.a.INVALID_INTEGRATION) {
            SpannableString spannableString = new SpannableString("Tap for more information");
            spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, spannableString.length(), 33);
            this.h = new SpannedString(spannableString);
        } else {
            this.h = new SpannedString("");
        }
        this.b.add(this.c);
        this.b.add(a(dVar));
        this.b.add(b(dVar));
        this.b.addAll(a(dVar.i()));
        this.b.addAll(a(dVar.k()));
        this.b.addAll(b(dVar.j()));
        this.b.add(this.g);
    }

    private int a(boolean z) {
        return z ? R.drawable.applovin_ic_check_mark : R.drawable.applovin_ic_x_mark;
    }

    private int b(boolean z) {
        return f.a(z ? R.color.applovin_sdk_checkmarkColor : R.color.applovin_sdk_xmarkColor, this.a);
    }

    public c a(d dVar) {
        C0009a a2 = com.applovin.impl.mediation.a.c.b.a.a.j().a(LogConstants.KEY_SDK).b(dVar.f()).a(TextUtils.isEmpty(dVar.f()) ? com.applovin.impl.mediation.a.a.c.a.DETAIL : com.applovin.impl.mediation.a.a.c.a.RIGHT_DETAIL);
        if (TextUtils.isEmpty(dVar.f())) {
            a2.a(a(dVar.b())).b(b(dVar.b()));
        }
        return a2.a();
    }

    public List<c> a(e eVar) {
        ArrayList arrayList = new ArrayList(2);
        if (eVar.a()) {
            boolean b = eVar.b();
            arrayList.add(this.e);
            arrayList.add(com.applovin.impl.mediation.a.c.b.a.a.j().a("Cleartext Traffic").a(b ? null : this.h).c(eVar.c()).a(a(b)).b(b(b)).a(!b).a());
        }
        return arrayList;
    }

    public List<c> a(List<com.applovin.impl.mediation.a.a.f> list) {
        ArrayList arrayList = new ArrayList(list.size() + 1);
        if (list.size() > 0) {
            arrayList.add(this.d);
            for (com.applovin.impl.mediation.a.a.f fVar : list) {
                boolean c2 = fVar.c();
                arrayList.add(com.applovin.impl.mediation.a.c.b.a.a.j().a(fVar.a()).a(c2 ? null : this.h).c(fVar.b()).a(a(c2)).b(b(c2)).a(!c2).a());
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        if (this.i != null && (cVar instanceof com.applovin.impl.mediation.a.c.b.a.a)) {
            String i2 = ((com.applovin.impl.mediation.a.c.b.a.a) cVar).i();
            if (!TextUtils.isEmpty(i2)) {
                this.i.a(i2);
            }
        }
    }

    public void a(a aVar) {
        this.i = aVar;
    }

    public c b(d dVar) {
        C0009a a2 = com.applovin.impl.mediation.a.c.b.a.a.j().a("Adapter").b(dVar.g()).a(TextUtils.isEmpty(dVar.g()) ? com.applovin.impl.mediation.a.a.c.a.DETAIL : com.applovin.impl.mediation.a.a.c.a.RIGHT_DETAIL);
        if (TextUtils.isEmpty(dVar.g())) {
            a2.a(a(dVar.c())).b(b(dVar.c()));
        }
        return a2.a();
    }

    public List<c> b(List<com.applovin.impl.mediation.a.a.a> list) {
        ArrayList arrayList = new ArrayList(list.size() + 1);
        if (list.size() > 0) {
            arrayList.add(this.f);
            for (com.applovin.impl.mediation.a.a.a aVar : list) {
                boolean c2 = aVar.c();
                arrayList.add(com.applovin.impl.mediation.a.c.b.a.a.j().a(aVar.a()).a(c2 ? null : this.h).c(aVar.b()).a(a(c2)).b(b(c2)).a(!c2).a());
            }
        }
        return arrayList;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedNetworkListAdapter{listItems=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}
