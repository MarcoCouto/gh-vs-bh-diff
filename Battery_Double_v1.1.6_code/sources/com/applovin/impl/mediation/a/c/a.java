package com.applovin.impl.mediation.a.c;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.applovin.impl.mediation.a.a.b;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.sdk.R;
import java.util.ArrayList;
import java.util.List;

public abstract class a extends BaseAdapter implements OnClickListener {
    protected final Context a;
    protected final List<c> b = new ArrayList();
    private final LayoutInflater c;

    protected a(Context context) {
        this.a = context;
        this.c = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* renamed from: a */
    public c getItem(int i) {
        return (c) this.b.get(i);
    }

    /* access modifiers changed from: protected */
    public abstract void a(c cVar);

    public boolean areAllItemsEnabled() {
        return false;
    }

    public int getCount() {
        return this.b.size();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public int getItemViewType(int i) {
        return getItem(i).e();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        b bVar;
        c a2 = getItem(i);
        if (view == null) {
            view = this.c.inflate(a2.f(), viewGroup, false);
            bVar = new b();
            bVar.a = (TextView) view.findViewById(16908308);
            bVar.b = (TextView) view.findViewById(16908309);
            bVar.c = (ImageView) view.findViewById(R.id.imageView);
            view.setTag(bVar);
            view.setOnClickListener(this);
        } else {
            bVar = (b) view.getTag();
        }
        bVar.a(a2);
        view.setEnabled(a2.b());
        return view;
    }

    public int getViewTypeCount() {
        return c.a();
    }

    public boolean isEnabled(int i) {
        return getItem(i).b();
    }

    public void onClick(View view) {
        a(((b) view.getTag()).a());
    }
}
