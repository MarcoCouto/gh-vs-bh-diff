package com.applovin.impl.mediation.a.c.a.a;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.impl.mediation.a.a.d;
import com.applovin.impl.sdk.utils.f;
import com.applovin.sdk.R;

public class a extends c {
    private final d d;
    private final Context e;

    public a(d dVar, Context context) {
        super(dVar.a() == com.applovin.impl.mediation.a.a.d.a.MISSING ? com.applovin.impl.mediation.a.a.c.a.SIMPLE : com.applovin.impl.mediation.a.a.c.a.DETAIL);
        this.d = dVar;
        this.e = context;
    }

    private SpannedString a(String str, int i) {
        return a(str, i, 16);
    }

    private SpannedString a(String str, int i, int i2) {
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new ForegroundColorSpan(i), 0, spannableString.length(), 33);
        spannableString.setSpan(new AbsoluteSizeSpan(i2, true), 0, spannableString.length(), 33);
        return new SpannedString(spannableString);
    }

    private SpannedString j() {
        int i;
        String str;
        if (this.d.b()) {
            if (!TextUtils.isEmpty(this.d.f())) {
                StringBuilder sb = new StringBuilder();
                sb.append("SDK ");
                sb.append(this.d.f());
                str = sb.toString();
            } else {
                str = this.d.c() ? "Retrieving SDK Version..." : "SDK Found";
            }
            i = -7829368;
        } else {
            str = "SDK Missing";
            i = SupportMenu.CATEGORY_MASK;
        }
        return a(str, i);
    }

    private SpannedString k() {
        int i;
        String str;
        if (this.d.c()) {
            if (!TextUtils.isEmpty(this.d.g())) {
                StringBuilder sb = new StringBuilder();
                sb.append("Adapter ");
                sb.append(this.d.g());
                str = sb.toString();
            } else {
                str = "Adapter Found";
            }
            i = -7829368;
        } else {
            str = "Adapter Missing";
            i = SupportMenu.CATEGORY_MASK;
        }
        return a(str, i);
    }

    private SpannedString l() {
        return a("Invalid Integration", SupportMenu.CATEGORY_MASK);
    }

    private SpannedString m() {
        StringBuilder sb = new StringBuilder();
        sb.append("Latest Version: Adapter ");
        sb.append(this.d.h());
        return a(sb.toString(), Color.rgb(255, 127, 0));
    }

    public boolean b() {
        return this.d.a() != com.applovin.impl.mediation.a.a.d.a.MISSING;
    }

    public SpannedString c() {
        if (this.b != null) {
            return this.b;
        }
        this.b = a(this.d.e(), this.d.a() == com.applovin.impl.mediation.a.a.d.a.MISSING ? -7829368 : ViewCompat.MEASURED_STATE_MASK, 18);
        return this.b;
    }

    public SpannedString d() {
        if (this.c != null) {
            return this.c;
        }
        if (this.d.a() != com.applovin.impl.mediation.a.a.d.a.MISSING) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            spannableStringBuilder.append(j());
            spannableStringBuilder.append(a(", ", -7829368));
            spannableStringBuilder.append(k());
            if (this.d.d()) {
                spannableStringBuilder.append(new SpannableString("\n"));
                spannableStringBuilder.append(m());
            }
            if (this.d.a() == com.applovin.impl.mediation.a.a.d.a.INVALID_INTEGRATION) {
                spannableStringBuilder.append(new SpannableString("\n"));
                spannableStringBuilder.append(l());
            }
            this.c = new SpannedString(spannableStringBuilder);
        } else {
            this.c = new SpannedString("");
        }
        return this.c;
    }

    public int g() {
        return b() ? R.drawable.applovin_ic_disclosure_arrow : super.g();
    }

    public int h() {
        return f.a(R.color.applovin_sdk_disclosureButtonColor, this.e);
    }

    public d i() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedNetworkListItemViewModel{text=");
        sb.append(this.b);
        sb.append(", detailText=");
        sb.append(this.c);
        sb.append(", network=");
        sb.append(this.d);
        sb.append("}");
        return sb.toString();
    }
}
