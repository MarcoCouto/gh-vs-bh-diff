package com.applovin.impl.mediation.a.c.a;

import android.content.Context;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.impl.mediation.a.a.d;
import com.applovin.impl.mediation.a.a.g;
import com.applovin.impl.sdk.j;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class b extends com.applovin.impl.mediation.a.c.a {
    private final AtomicBoolean c = new AtomicBoolean();
    private final c d = new g("INCOMPLETE INTEGRATIONS");
    private final c e = new g("COMPLETED INTEGRATIONS");
    private final c f = new g("MISSING INTEGRATIONS");
    private final c g = new g("");
    private a h;

    public interface a {
        void a(d dVar);
    }

    public b(Context context) {
        super(context);
    }

    private List<c> b(List<d> list, j jVar) {
        jVar.v().b("MediationDebuggerListAdapter", "Updating networks...");
        ArrayList arrayList = new ArrayList(list.size());
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        for (d dVar : list) {
            com.applovin.impl.mediation.a.c.a.a.a aVar = new com.applovin.impl.mediation.a.c.a.a.a(dVar, this.a);
            if (dVar.a() == com.applovin.impl.mediation.a.a.d.a.INCOMPLETE_INTEGRATION || dVar.a() == com.applovin.impl.mediation.a.a.d.a.INVALID_INTEGRATION) {
                arrayList2.add(aVar);
            } else if (dVar.a() == com.applovin.impl.mediation.a.a.d.a.COMPLETE) {
                arrayList3.add(aVar);
            } else if (dVar.a() == com.applovin.impl.mediation.a.a.d.a.MISSING) {
                arrayList4.add(aVar);
            }
        }
        if (arrayList2.size() > 0) {
            arrayList.add(this.d);
            arrayList.addAll(arrayList2);
        }
        if (arrayList3.size() > 0) {
            arrayList.add(this.e);
            arrayList.addAll(arrayList3);
        }
        if (arrayList4.size() > 0) {
            arrayList.add(this.f);
            arrayList.addAll(arrayList4);
        }
        arrayList.add(this.g);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        if (this.h != null && (cVar instanceof com.applovin.impl.mediation.a.c.a.a.a)) {
            this.h.a(((com.applovin.impl.mediation.a.c.a.a.a) cVar).i());
        }
    }

    public void a(a aVar) {
        this.h = aVar;
    }

    public void a(List<d> list, j jVar) {
        if (list != null && this.c.compareAndSet(false, true)) {
            this.b.addAll(b(list, jVar));
        }
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                b.this.notifyDataSetChanged();
            }
        });
    }

    public boolean a() {
        return this.c.get();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediationDebuggerListAdapter{isInitialized=");
        sb.append(this.c.get());
        sb.append(", listItems=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}
