package com.applovin.impl.mediation.a.b;

import android.os.Build.VERSION;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.x;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.k.b;
import com.applovin.impl.sdk.network.a.c;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class a extends com.applovin.impl.sdk.d.a {
    /* access modifiers changed from: private */
    public final c<JSONObject> a;

    public a(c<JSONObject> cVar, j jVar) {
        super("TaskFetchMediationDebuggerInfo", jVar, true);
        this.a = cVar;
    }

    public i a() {
        return i.J;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap();
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", String.valueOf(131));
        if (!((Boolean) this.b.a(d.eR)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.t());
        }
        b c = this.b.O().c();
        hashMap.put("package_name", n.e(c.c));
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, n.e(c.b));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, "android");
        hashMap.put("os", n.e(VERSION.RELEASE));
        return hashMap;
    }

    public void run() {
        AnonymousClass1 r1 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(com.applovin.impl.mediation.d.b.c(this.b)).c(com.applovin.impl.mediation.d.b.d(this.b)).a(b()).b(HttpRequest.METHOD_GET).a(new JSONObject()).b(((Long) this.b.a(com.applovin.impl.sdk.b.c.g)).intValue()).a(), this.b, h()) {
            public void a(int i) {
                a.this.a.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                a.this.a.a(jSONObject, i);
            }
        };
        r1.a(com.applovin.impl.sdk.b.c.c);
        r1.b(com.applovin.impl.sdk.b.c.d);
        this.b.K().a((com.applovin.impl.sdk.d.a) r1);
    }
}
