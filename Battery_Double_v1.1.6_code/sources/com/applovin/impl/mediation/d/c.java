package com.applovin.impl.mediation.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.sdk.AppLovinSdk;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {
    private static final List<String> a = new ArrayList();
    private static a b;

    public static class a {
        private final JSONArray a;
        private final JSONArray b;

        private a(JSONArray jSONArray, JSONArray jSONArray2) {
            this.a = jSONArray;
            this.b = jSONArray2;
        }

        public JSONArray a() {
            return this.a;
        }

        public JSONArray b() {
            return this.b;
        }
    }

    static {
        a.add("com.applovin.mediation.adapters.AdColonyMediationAdapter");
        a.add("com.applovin.mediation.adapters.AmazonMediationAdapter");
        a.add("com.applovin.mediation.adapters.AmazonBiddingMediationAdapter");
        a.add("com.applovin.mediation.adapters.AppLovinMediationAdapter");
        a.add("com.applovin.mediation.adapters.ChartboostMediationAdapter");
        a.add("com.applovin.mediation.adapters.FacebookMediationAdapter");
        a.add("com.applovin.mediation.adapters.GoogleMediationAdapter");
        a.add("com.applovin.mediation.adapters.GoogleAdManagerMediationAdapter");
        a.add("com.applovin.mediation.adapters.HyperMXMediationAdapter");
        a.add("com.applovin.mediation.adapters.IMobileMediationAdapter");
        a.add("com.applovin.mediation.adapters.InMobiMediationAdapter");
        a.add("com.applovin.mediation.adapters.InneractiveMediationAdapter");
        a.add("com.applovin.mediation.adapters.IronSourceMediationAdapter");
        a.add("com.applovin.mediation.adapters.LeadboltMediationAdapter");
        a.add("com.applovin.mediation.adapters.MadvertiseMediationAdapter");
        a.add("com.applovin.mediation.adapters.MaioMediationAdapter");
        a.add("com.applovin.mediation.adapters.MintegralMediationAdapter");
        a.add("com.applovin.mediation.adapters.MoPubMediationAdapter");
        a.add("com.applovin.mediation.adapters.MyTargetMediationAdapter");
        a.add("com.applovin.mediation.adapters.NendMediationAdapter");
        a.add("com.applovin.mediation.adapters.OguryMediationAdapter");
        a.add("com.applovin.mediation.adapters.OguryPresageMediationAdapter");
        a.add("com.applovin.mediation.adapters.SmaatoMediationAdapter");
        a.add("com.applovin.mediation.adapters.TapjoyMediationAdapter");
        a.add("com.applovin.mediation.adapters.TencentMediationAdapter");
        a.add("com.applovin.mediation.adapters.UnityAdsMediationAdapter");
        a.add("com.applovin.mediation.adapters.VerizonAdsMediationAdapter");
        a.add("com.applovin.mediation.adapters.VoodooAdsMediationAdapter");
        a.add("com.applovin.mediation.adapters.VungleMediationAdapter");
        a.add("com.applovin.mediation.adapters.YandexMediationAdapter");
    }

    public static a a(j jVar) {
        if (!((Boolean) jVar.a(com.applovin.impl.sdk.b.c.T)).booleanValue() && b != null) {
            return b;
        }
        if (b != null) {
            b(jVar);
        } else {
            JSONArray jSONArray = new JSONArray();
            JSONArray jSONArray2 = new JSONArray();
            for (String str : a) {
                MaxAdapter a2 = a(str, jVar);
                if (a2 != null) {
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put("class", str);
                        jSONObject.put("sdk_version", a2.getSdkVersion());
                        jSONObject.put("version", a2.getAdapterVersion());
                    } catch (Throwable unused) {
                    }
                    jSONArray.put(jSONObject);
                } else {
                    jSONArray2.put(str);
                }
            }
            b = new a(jSONArray, jSONArray2);
        }
        return b;
    }

    public static com.applovin.impl.sdk.d.r.a a(MaxAdFormat maxAdFormat) {
        return maxAdFormat == MaxAdFormat.INTERSTITIAL ? com.applovin.impl.sdk.d.r.a.MEDIATION_INTERSTITIAL : maxAdFormat == MaxAdFormat.REWARDED ? com.applovin.impl.sdk.d.r.a.MEDIATION_INCENTIVIZED : com.applovin.impl.sdk.d.r.a.MEDIATION_BANNER;
    }

    public static MaxAdapter a(String str, j jVar) {
        if (TextUtils.isEmpty(str)) {
            jVar.v().e("AppLovinSdk", "Failed to create adapter instance. No class name provided");
            return null;
        }
        try {
            Class cls = Class.forName(str);
            if (MaxAdapter.class.isAssignableFrom(cls)) {
                return (MaxAdapter) cls.getConstructor(new Class[]{AppLovinSdk.class}).newInstance(new Object[]{jVar.S()});
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" error: not an instance of '");
            sb.append(MaxAdapter.class.getName());
            sb.append("'.");
            jVar.v().e("AppLovinSdk", sb.toString());
            return null;
        } catch (ClassNotFoundException unused) {
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to load: ");
            sb2.append(str);
            jVar.v().b("AppLovinSdk", sb2.toString(), th);
        }
    }

    public static String b(MaxAdFormat maxAdFormat) {
        return maxAdFormat.getLabel();
    }

    private static void b(j jVar) {
        JSONArray a2 = b.a();
        for (int i = 0; i < a2.length(); i++) {
            JSONObject a3 = i.a(a2, i, (JSONObject) null, jVar);
            String b2 = i.b(a3, "class", "", jVar);
            if (!n.b(i.b(a3, "sdk_version", "", jVar))) {
                MaxAdapter a4 = a(b2, jVar);
                if (a4 != null) {
                    i.a(a3, "sdk_version", a4.getSdkVersion(), jVar);
                }
            }
        }
    }
}
