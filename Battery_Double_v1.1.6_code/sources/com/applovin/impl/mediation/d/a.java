package com.applovin.impl.mediation.d;

import com.applovin.impl.sdk.j;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;

public class a implements MaxAdListener, MaxAdViewAdListener, MaxRewardedAdListener {
    private final j a;
    private final MaxAdListener b;

    public a(MaxAdListener maxAdListener, j jVar) {
        this.a = jVar;
        this.b = maxAdListener;
    }

    public void onAdClicked(MaxAd maxAd) {
        com.applovin.impl.sdk.utils.j.d(this.b, maxAd);
    }

    public void onAdCollapsed(MaxAd maxAd) {
        com.applovin.impl.sdk.utils.j.h(this.b, maxAd);
    }

    public void onAdDisplayFailed(MaxAd maxAd, int i) {
        com.applovin.impl.sdk.utils.j.a(this.b, maxAd, i);
    }

    public void onAdDisplayed(MaxAd maxAd) {
        com.applovin.impl.sdk.utils.j.b(this.b, maxAd);
    }

    public void onAdExpanded(MaxAd maxAd) {
        com.applovin.impl.sdk.utils.j.g(this.b, maxAd);
    }

    public void onAdHidden(MaxAd maxAd) {
        com.applovin.impl.sdk.utils.j.c(this.b, maxAd);
    }

    public void onAdLoadFailed(String str, int i) {
        com.applovin.impl.sdk.utils.j.a(this.b, str, i);
    }

    public void onAdLoaded(MaxAd maxAd) {
        com.applovin.impl.sdk.utils.j.a(this.b, maxAd);
    }

    public void onRewardedVideoCompleted(MaxAd maxAd) {
        com.applovin.impl.sdk.utils.j.f(this.b, maxAd);
    }

    public void onRewardedVideoStarted(MaxAd maxAd) {
        com.applovin.impl.sdk.utils.j.e(this.b, maxAd);
    }

    public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
        com.applovin.impl.sdk.utils.j.a(this.b, maxAd, maxReward);
    }
}
