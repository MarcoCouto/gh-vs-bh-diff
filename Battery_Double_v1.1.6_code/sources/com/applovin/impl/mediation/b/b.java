package com.applovin.impl.mediation.b;

import android.view.View;
import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.q;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import org.json.JSONObject;

public class b extends a {
    private static final int c = AppLovinAdSize.BANNER.getHeight();
    private static final int d = AppLovinAdSize.LEADER.getHeight();

    private b(b bVar, i iVar) {
        super(bVar.B(), bVar.A(), iVar, bVar.b);
    }

    public b(JSONObject jSONObject, JSONObject jSONObject2, j jVar) {
        super(jSONObject, jSONObject2, null, jVar);
    }

    public a a(i iVar) {
        return new b(this, iVar);
    }

    public int j() {
        int b = b("ad_view_width", ((Integer) this.b.a(c.p)).intValue());
        if (b != -2) {
            return b;
        }
        if (AppLovinSdkUtils.isTablet(this.b.D())) {
            return 728;
        }
        return ModuleDescriptor.MODULE_VERSION;
    }

    public int k() {
        int b = b("ad_view_height", ((Integer) this.b.a(c.q)).intValue());
        return b == -2 ? AppLovinSdkUtils.isTablet(this.b.D()) ? d : c : b;
    }

    public View l() {
        if (!a() || this.a == null) {
            return null;
        }
        View a = this.a.a();
        if (a != null) {
            return a;
        }
        throw new IllegalStateException("Ad-view based ad is missing an ad view");
    }

    public long m() {
        return b("viewability_imp_delay_ms", ((Long) this.b.a(d.cd)).longValue());
    }

    public int n() {
        d<Integer> dVar = getFormat() == MaxAdFormat.BANNER ? d.ce : getFormat() == MaxAdFormat.MREC ? d.cg : d.ci;
        return b("viewability_min_width", ((Integer) this.b.a(dVar)).intValue());
    }

    public int o() {
        d<Integer> dVar = getFormat() == MaxAdFormat.BANNER ? d.cf : getFormat() == MaxAdFormat.MREC ? d.ch : d.cj;
        return b("viewability_min_height", ((Integer) this.b.a(dVar)).intValue());
    }

    public float p() {
        return a("viewability_min_alpha", ((Float) this.b.a(c.ck)).floatValue() / 100.0f);
    }

    public int q() {
        return b("viewability_min_pixels", -1);
    }

    public boolean r() {
        return q() >= 0;
    }

    public long s() {
        return b("viewability_timer_min_visible_ms", ((Long) this.b.a(c.cl)).longValue());
    }

    public boolean t() {
        return b("proe", (Boolean) this.b.a(c.N));
    }

    public long u() {
        return q.f(b("bg_color", (String) null));
    }
}
