package com.applovin.impl.mediation.b;

import com.applovin.impl.sdk.j;
import org.json.JSONObject;

public class g extends e {
    public g(JSONObject jSONObject, JSONObject jSONObject2, j jVar) {
        super(jSONObject, jSONObject2, jVar);
    }

    /* access modifiers changed from: 0000 */
    public int a() {
        return b("max_signal_length", 2048);
    }

    public boolean b() {
        return b("only_collect_signal_when_initialized", Boolean.valueOf(false));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SignalProviderSpec{specObject=");
        sb.append(B());
        sb.append('}');
        return sb.toString();
    }
}
