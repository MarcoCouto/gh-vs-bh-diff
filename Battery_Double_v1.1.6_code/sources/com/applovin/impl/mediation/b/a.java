package com.applovin.impl.mediation.b;

import android.os.SystemClock;
import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.q;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public abstract class a extends e implements MaxAd {
    protected i a;
    private final AtomicBoolean c = new AtomicBoolean();

    protected a(JSONObject jSONObject, JSONObject jSONObject2, i iVar, j jVar) {
        super(jSONObject, jSONObject2, jVar);
        this.a = iVar;
    }

    private long j() {
        return b("load_started_time_ms", 0);
    }

    public abstract a a(i iVar);

    public boolean a() {
        return this.a != null && this.a.c() && this.a.d();
    }

    public String b() {
        return a("event_id", "");
    }

    public i c() {
        return this.a;
    }

    public String d() {
        return b("bid_response", (String) null);
    }

    public String e() {
        return b("third_party_ad_placement_id", (String) null);
    }

    public long f() {
        if (j() > 0) {
            return SystemClock.elapsedRealtime() - j();
        }
        return -1;
    }

    public void g() {
        c("load_started_time_ms", SystemClock.elapsedRealtime());
    }

    public String getAdUnitId() {
        return a("ad_unit_id", "");
    }

    public MaxAdFormat getFormat() {
        return q.c(a("ad_format", (String) null));
    }

    public String getNetworkName() {
        return b("network_name", "");
    }

    public AtomicBoolean h() {
        return this.c;
    }

    public void i() {
        this.a = null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append("{thirdPartyAdPlacementId=");
        sb.append(e());
        sb.append(", adUnitId=");
        sb.append(getAdUnitId());
        sb.append(", format=");
        sb.append(getFormat().getLabel());
        sb.append(", networkName='");
        sb.append(getNetworkName());
        sb.append('}');
        return sb.toString();
    }
}
