package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.j;
import org.json.JSONObject;

public class d extends a {
    private d(d dVar, i iVar) {
        super(dVar.B(), dVar.A(), iVar, dVar.b);
    }

    public d(JSONObject jSONObject, JSONObject jSONObject2, j jVar) {
        super(jSONObject, jSONObject2, null, jVar);
    }

    public a a(i iVar) {
        return new d(this, iVar);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedNativeAd{format=");
        sb.append(getFormat());
        sb.append(", adUnitId=");
        sb.append(getAdUnitId());
        sb.append(", isReady=");
        sb.append(a());
        sb.append(", adapterClass='");
        sb.append(C());
        sb.append("', adapterName='");
        sb.append(D());
        sb.append("', isTesting=");
        sb.append(E());
        sb.append(", isRefreshEnabled=");
        sb.append(I());
        sb.append(", getAdRefreshMillis=");
        sb.append(J());
        sb.append('}');
        return sb.toString();
    }
}
