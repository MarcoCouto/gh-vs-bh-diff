package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.j;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class c extends a {
    private String c;
    private final AtomicReference<com.applovin.impl.sdk.a.c> d;
    private final AtomicBoolean e;

    private c(c cVar, i iVar) {
        super(cVar.B(), cVar.A(), iVar, cVar.b);
        this.d = cVar.d;
        this.e = cVar.e;
    }

    public c(JSONObject jSONObject, JSONObject jSONObject2, j jVar) {
        super(jSONObject, jSONObject2, null, jVar);
        this.d = new AtomicReference<>();
        this.e = new AtomicBoolean();
    }

    public a a(i iVar) {
        return new c(this, iVar);
    }

    public void a(com.applovin.impl.sdk.a.c cVar) {
        this.d.set(cVar);
    }

    public void a(String str) {
        this.c = str;
    }

    public boolean j() {
        return b("fa", Boolean.valueOf(false));
    }

    public long k() {
        return b("ifacd_ms", -1);
    }

    public long l() {
        return b("fard_ms", TimeUnit.HOURS.toMillis(1));
    }

    public String m() {
        return this.c;
    }

    public long n() {
        long b = b("ad_expiration_ms", -1);
        return b >= 0 ? b : a("ad_expiration_ms", ((Long) this.b.a(com.applovin.impl.sdk.b.c.H)).longValue());
    }

    public long o() {
        long b = b("ad_hidden_timeout_ms", -1);
        return b >= 0 ? b : a("ad_hidden_timeout_ms", ((Long) this.b.a(com.applovin.impl.sdk.b.c.K)).longValue());
    }

    public boolean p() {
        if (b("schedule_ad_hidden_on_ad_dismiss", Boolean.valueOf(false))) {
            return true;
        }
        return a("schedule_ad_hidden_on_ad_dismiss", (Boolean) this.b.a(com.applovin.impl.sdk.b.c.L));
    }

    public long q() {
        long b = b("ad_hidden_on_ad_dismiss_callback_delay_ms", -1);
        return b >= 0 ? b : a("ad_hidden_on_ad_dismiss_callback_delay_ms", ((Long) this.b.a(com.applovin.impl.sdk.b.c.M)).longValue());
    }

    public String r() {
        return b("bcode", "");
    }

    public String s() {
        return a("mcode", "");
    }

    public boolean t() {
        return this.e.get();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MediatedFullscreenAd{format=");
        sb.append(getFormat());
        sb.append(", adUnitId=");
        sb.append(getAdUnitId());
        sb.append(", isReady=");
        sb.append(a());
        sb.append(", adapterClass='");
        sb.append(C());
        sb.append("', adapterName='");
        sb.append(D());
        sb.append("', isTesting=");
        sb.append(E());
        sb.append(", isRefreshEnabled=");
        sb.append(I());
        sb.append(", getAdRefreshMillis=");
        sb.append(J());
        sb.append('}');
        return sb.toString();
    }

    public void u() {
        this.e.set(true);
    }

    public com.applovin.impl.sdk.a.c v() {
        return (com.applovin.impl.sdk.a.c) this.d.getAndSet(null);
    }

    public boolean w() {
        return b("show_nia", Boolean.valueOf(a("show_nia", Boolean.valueOf(false))));
    }

    public String x() {
        return b("nia_title", a("nia_title", ""));
    }

    public String y() {
        return b("nia_message", a("nia_message", ""));
    }

    public String z() {
        return b("nia_button_title", a("nia_button_title", ""));
    }
}
