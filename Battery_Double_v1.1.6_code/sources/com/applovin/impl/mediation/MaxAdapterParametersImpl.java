package com.applovin.impl.mediation;

import android.content.Context;
import android.os.Bundle;
import com.applovin.impl.mediation.b.a;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.mediation.b.g;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;

public class MaxAdapterParametersImpl implements MaxAdapterInitializationParameters, MaxAdapterResponseParameters, MaxAdapterSignalCollectionParameters {
    private Bundle a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private String f;
    private String g;
    private MaxAdFormat h;

    private MaxAdapterParametersImpl() {
    }

    static MaxAdapterParametersImpl a(a aVar, Context context) {
        MaxAdapterParametersImpl a2 = a((e) aVar, context);
        a2.f = aVar.e();
        a2.g = aVar.d();
        return a2;
    }

    static MaxAdapterParametersImpl a(e eVar, Context context) {
        MaxAdapterParametersImpl maxAdapterParametersImpl = new MaxAdapterParametersImpl();
        maxAdapterParametersImpl.b = eVar.a(context);
        maxAdapterParametersImpl.c = eVar.b(context);
        maxAdapterParametersImpl.d = eVar.c(context);
        maxAdapterParametersImpl.a = eVar.G();
        maxAdapterParametersImpl.e = eVar.E();
        return maxAdapterParametersImpl;
    }

    static MaxAdapterParametersImpl a(g gVar, MaxAdFormat maxAdFormat, Context context) {
        MaxAdapterParametersImpl a2 = a((e) gVar, context);
        a2.h = maxAdFormat;
        return a2;
    }

    public MaxAdFormat getAdFormat() {
        return this.h;
    }

    public String getBidResponse() {
        return this.g;
    }

    public Bundle getServerParameters() {
        return this.a;
    }

    public String getThirdPartyAdPlacementId() {
        return this.f;
    }

    public boolean hasUserConsent() {
        return this.b;
    }

    public boolean isAgeRestrictedUser() {
        return this.c;
    }

    public boolean isDoNotSell() {
        return this.d;
    }

    public boolean isTesting() {
        return this.e;
    }
}
