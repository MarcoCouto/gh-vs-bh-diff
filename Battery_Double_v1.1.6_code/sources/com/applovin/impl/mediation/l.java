package com.applovin.impl.mediation;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;

public class l implements MaxAd {
    private final String a;
    private final MaxAdFormat b;
    private final String c;

    public l(String str, MaxAdFormat maxAdFormat, String str2) {
        this.a = str;
        this.b = maxAdFormat;
        this.c = str2;
    }

    public String getAdUnitId() {
        return this.a;
    }

    public MaxAdFormat getFormat() {
        return this.b;
    }

    public String getNetworkName() {
        return this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("MaxAd{adUnitId=");
        sb.append(this.a);
        sb.append(", format=");
        sb.append(this.b);
        sb.append(", network=");
        sb.append(this.c);
        sb.append("}");
        return sb.toString();
    }
}
