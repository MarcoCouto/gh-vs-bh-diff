package com.applovin.impl.mediation;

import android.app.Activity;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.i;
import com.applovin.mediation.adapter.MaxAdapter.InitializationStatus;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.facebook.internal.AnalyticsEvents;
import java.util.LinkedHashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class g {
    private final j a;
    private final p b;
    private final AtomicBoolean c = new AtomicBoolean();
    private final JSONArray d = new JSONArray();
    private final LinkedHashSet<String> e = new LinkedHashSet<>();
    private final Object f = new Object();

    public g(j jVar) {
        this.a = jVar;
        this.b = jVar.v();
    }

    public void a(Activity activity) {
        if (this.c.compareAndSet(false, true)) {
            this.a.K().a((a) new com.applovin.impl.mediation.c.a(activity, this.a));
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(e eVar, long j, InitializationStatus initializationStatus, String str) {
        boolean z;
        if (initializationStatus != null && initializationStatus != InitializationStatus.INITIALIZING) {
            synchronized (this.f) {
                z = !a(eVar);
                if (z) {
                    this.e.add(eVar.C());
                    JSONObject jSONObject = new JSONObject();
                    i.a(jSONObject, "class", eVar.C(), this.a);
                    i.a(jSONObject, "init_status", String.valueOf(initializationStatus.getCode()), this.a);
                    i.a(jSONObject, AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE, JSONObject.quote(str), this.a);
                    this.d.put(jSONObject);
                }
            }
            if (z) {
                this.a.a(eVar);
                this.a.y().maybeScheduleAdapterInitializationPostback(eVar, j, initializationStatus, str);
            }
        }
    }

    public void a(e eVar, Activity activity) {
        i a2 = this.a.w().a(eVar);
        if (a2 != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Initializing adapter ");
            sb.append(eVar);
            this.b.c("MediationAdapterInitializationManager", sb.toString());
            a2.a((MaxAdapterInitializationParameters) MaxAdapterParametersImpl.a(eVar, activity.getApplicationContext()), activity);
        }
    }

    public boolean a() {
        return this.c.get();
    }

    /* access modifiers changed from: 0000 */
    public boolean a(e eVar) {
        boolean contains;
        synchronized (this.f) {
            contains = this.e.contains(eVar.C());
        }
        return contains;
    }

    public LinkedHashSet<String> b() {
        LinkedHashSet<String> linkedHashSet;
        synchronized (this.f) {
            linkedHashSet = this.e;
        }
        return linkedHashSet;
    }

    public JSONArray c() {
        JSONArray jSONArray;
        synchronized (this.f) {
            jSONArray = this.d;
        }
        return jSONArray;
    }
}
