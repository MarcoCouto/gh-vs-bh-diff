package com.applovin.impl.mediation.ads;

import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;

public abstract class a {
    protected final MaxAdFormat adFormat;
    protected MaxAdListener adListener = null;
    protected final String adUnitId;
    protected final com.applovin.impl.mediation.f.a loadRequestBuilder;
    protected final p logger;
    protected final j sdk;
    protected final String tag;

    protected a(String str, MaxAdFormat maxAdFormat, String str2, j jVar) {
        this.adUnitId = str;
        this.adFormat = maxAdFormat;
        this.sdk = jVar;
        this.tag = str2;
        this.logger = jVar.v();
        this.loadRequestBuilder = new com.applovin.impl.mediation.f.a();
    }

    public String getAdUnitId() {
        return this.adUnitId;
    }

    public void setExtraParameter(String str, String str2) {
        if (str != null) {
            this.loadRequestBuilder.a(str, str2);
            return;
        }
        throw new IllegalArgumentException("No key specified");
    }

    public void setListener(MaxAdListener maxAdListener) {
        p pVar = this.logger;
        String str = this.tag;
        StringBuilder sb = new StringBuilder();
        sb.append("Setting listener: ");
        sb.append(maxAdListener);
        pVar.b(str, sb.toString());
        this.adListener = maxAdListener;
    }
}
