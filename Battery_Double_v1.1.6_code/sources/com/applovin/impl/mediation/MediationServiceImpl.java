package com.applovin.impl.mediation;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.mediation.b.f;
import com.applovin.impl.mediation.c.b;
import com.applovin.impl.mediation.c.d;
import com.applovin.impl.mediation.c.g;
import com.applovin.impl.mediation.c.h;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.adapter.MaxAdapter.InitializationStatus;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

public class MediationServiceImpl {
    /* access modifiers changed from: private */
    public final j a;
    /* access modifiers changed from: private */
    public final p b;

    private class a implements d, MaxAdViewAdListener, MaxRewardedAdListener {
        private final com.applovin.impl.mediation.b.a b;
        /* access modifiers changed from: private */
        public final MaxAdListener c;

        private a(com.applovin.impl.mediation.b.a aVar, MaxAdListener maxAdListener) {
            this.b = aVar;
            this.c = maxAdListener;
        }

        public void a(MaxAd maxAd, e eVar) {
            MediationServiceImpl.this.b(this.b, eVar, this.c);
            if (maxAd.getFormat() == MaxAdFormat.REWARDED && (maxAd instanceof c)) {
                ((c) maxAd).u();
            }
        }

        public void a(String str, e eVar) {
            MediationServiceImpl.this.a(this.b, eVar, this.c);
        }

        public void onAdClicked(MaxAd maxAd) {
            MediationServiceImpl.this.a.ac().a((com.applovin.impl.mediation.b.a) maxAd, "DID_CLICKED");
            MediationServiceImpl.this.c(this.b);
            com.applovin.impl.sdk.utils.j.d(this.c, maxAd);
        }

        public void onAdCollapsed(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.h(this.c, maxAd);
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i) {
            MediationServiceImpl.this.b(this.b, new e(i), this.c);
        }

        public void onAdDisplayed(MaxAd maxAd) {
            MediationServiceImpl.this.b.b("MediationService", "Scheduling impression for ad via callback...");
            MediationServiceImpl.this.maybeScheduleCallbackAdImpressionPostback(this.b);
            if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
                MediationServiceImpl.this.a.Z().c();
            }
            com.applovin.impl.sdk.utils.j.b(this.c, maxAd);
        }

        public void onAdExpanded(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.g(this.c, maxAd);
        }

        public void onAdHidden(final MaxAd maxAd) {
            MediationServiceImpl.this.a.ac().a((com.applovin.impl.mediation.b.a) maxAd, "DID_HIDE");
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
                        MediationServiceImpl.this.a.Z().d();
                    }
                    com.applovin.impl.sdk.utils.j.c(a.this.c, maxAd);
                }
            }, maxAd instanceof e ? ((e) maxAd).M() : 0);
        }

        public void onAdLoadFailed(String str, int i) {
            MediationServiceImpl.this.a(this.b, new e(i), this.c);
        }

        public void onAdLoaded(MaxAd maxAd) {
            MediationServiceImpl.this.b(this.b);
            com.applovin.impl.sdk.utils.j.a(this.c, maxAd);
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.f(this.c, maxAd);
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.e(this.c, maxAd);
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            com.applovin.impl.sdk.utils.j.a(this.c, maxAd, maxReward);
            MediationServiceImpl.this.a.K().a((com.applovin.impl.sdk.d.a) new g((c) maxAd, MediationServiceImpl.this.a), com.applovin.impl.sdk.d.r.a.MEDIATION_REWARD);
        }
    }

    public MediationServiceImpl(j jVar) {
        this.a = jVar;
        this.b = jVar.v();
    }

    private void a(com.applovin.impl.mediation.b.a aVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Firing ad preload postback for ");
        sb.append(aVar.D());
        this.b.b("MediationService", sb.toString());
        a("mpreload", (e) aVar);
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.a aVar, e eVar, MaxAdListener maxAdListener) {
        a(eVar, aVar);
        destroyAd(aVar);
        com.applovin.impl.sdk.utils.j.a(maxAdListener, aVar.getAdUnitId(), eVar.getErrorCode());
    }

    private void a(e eVar, com.applovin.impl.mediation.b.a aVar) {
        long f = aVar.f();
        StringBuilder sb = new StringBuilder();
        sb.append("Firing ad load failure postback with load time: ");
        sb.append(f);
        this.b.b("MediationService", sb.toString());
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(f));
        a("mlerr", (Map<String, String>) hashMap, eVar, (e) aVar);
    }

    private void a(String str, e eVar) {
        a(str, Collections.EMPTY_MAP, (e) null, eVar);
    }

    /* access modifiers changed from: private */
    public void a(String str, com.applovin.impl.mediation.b.g gVar) {
        a("serr", Collections.EMPTY_MAP, new e(str), (e) gVar);
    }

    private void a(String str, Map<String, String> map, e eVar) {
        a(str, map, (e) null, eVar);
    }

    private void a(String str, Map<String, String> map, e eVar, e eVar2) {
        HashMap hashMap = new HashMap(map);
        hashMap.put("{PLACEMENT}", eVar2.N() != null ? eVar2.N() : "");
        if (eVar2 instanceof c) {
            c cVar = (c) eVar2;
            hashMap.put("{PUBLISHER_AD_UNIT_ID}", cVar.m() != null ? cVar.m() : "");
        }
        d dVar = new d(str, hashMap, eVar, eVar2, this.a);
        this.a.K().a((com.applovin.impl.sdk.d.a) dVar, com.applovin.impl.sdk.d.r.a.MEDIATION_POSTBACKS);
    }

    /* access modifiers changed from: private */
    public void b(com.applovin.impl.mediation.b.a aVar) {
        long f = aVar.f();
        StringBuilder sb = new StringBuilder();
        sb.append("Firing ad load success postback with load time: ");
        sb.append(f);
        this.b.b("MediationService", sb.toString());
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(f));
        a("load", (Map<String, String>) hashMap, (e) aVar);
    }

    /* access modifiers changed from: private */
    public void b(com.applovin.impl.mediation.b.a aVar, e eVar, MaxAdListener maxAdListener) {
        this.a.ac().a(aVar, "DID_FAIL_DISPLAY");
        maybeScheduleAdDisplayErrorPostback(eVar, aVar);
        if (aVar.h().compareAndSet(false, true)) {
            com.applovin.impl.sdk.utils.j.a(maxAdListener, (MaxAd) aVar, eVar.getErrorCode());
        }
    }

    /* access modifiers changed from: private */
    public void c(com.applovin.impl.mediation.b.a aVar) {
        a("mclick", (e) aVar);
    }

    public void collectSignal(MaxAdFormat maxAdFormat, final com.applovin.impl.mediation.b.g gVar, Activity activity, final com.applovin.impl.mediation.b.f.a aVar) {
        String str;
        p pVar;
        String str2;
        StringBuilder sb;
        String str3;
        if (gVar == null) {
            throw new IllegalArgumentException("No spec specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (aVar != null) {
            final i a2 = this.a.w().a((e) gVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(gVar, maxAdFormat, activity.getApplicationContext());
                a2.a((MaxAdapterInitializationParameters) a3, activity);
                AnonymousClass4 r1 = new MaxSignalCollectionListener() {
                    public void onSignalCollected(String str) {
                        aVar.a(f.a(gVar, a2, str));
                    }

                    public void onSignalCollectionFailed(String str) {
                        MediationServiceImpl.this.a(str, gVar);
                        aVar.a(f.b(gVar, a2, str));
                    }
                };
                if (!gVar.b()) {
                    pVar = this.b;
                    str3 = "MediationService";
                    sb = new StringBuilder();
                    str2 = "Collecting signal for adapter: ";
                } else if (this.a.x().a((e) gVar)) {
                    pVar = this.b;
                    str3 = "MediationService";
                    sb = new StringBuilder();
                    str2 = "Collecting signal for now-initialized adapter: ";
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Skip collecting signal for not-initialized adapter: ");
                    sb2.append(a2.b());
                    this.b.e("MediationService", sb2.toString());
                    str = "Adapter not initialized yet";
                }
                sb.append(str2);
                sb.append(a2.b());
                pVar.b(str3, sb.toString());
                a2.a(a3, gVar, activity, r1);
                return;
            }
            str = "Could not load adapter";
            aVar.a(f.a(gVar, str));
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    public void destroyAd(MaxAd maxAd) {
        if (maxAd instanceof com.applovin.impl.mediation.b.a) {
            StringBuilder sb = new StringBuilder();
            sb.append("Destroying ");
            sb.append(maxAd);
            this.b.c("MediationService", sb.toString());
            com.applovin.impl.mediation.b.a aVar = (com.applovin.impl.mediation.b.a) maxAd;
            i c = aVar.c();
            if (c != null) {
                c.g();
                aVar.i();
            }
        }
    }

    public void loadAd(String str, MaxAdFormat maxAdFormat, f fVar, boolean z, Activity activity, MaxAdListener maxAdListener) {
        final MaxAdListener maxAdListener2 = maxAdListener;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAdListener2 != null) {
            if (!this.a.d()) {
                p.i("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
            }
            this.a.a();
            MaxAdFormat maxAdFormat2 = maxAdFormat;
            final c a2 = this.a.B().a(maxAdFormat);
            if (a2 != null) {
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        maxAdListener2.onAdLoaded(a2);
                    }
                }, a2.k());
            }
            j jVar = this.a;
            final f fVar2 = fVar;
            final String str2 = str;
            final MaxAdFormat maxAdFormat3 = maxAdFormat;
            final Activity activity2 = activity;
            final MaxAdListener maxAdListener3 = maxAdListener;
            AnonymousClass2 r0 = new com.applovin.impl.mediation.c.b.a() {
                public void a(JSONArray jSONArray) {
                    com.applovin.impl.mediation.c.c cVar = new com.applovin.impl.mediation.c.c(str2, maxAdFormat3, fVar2 != null ? fVar2 : new com.applovin.impl.mediation.f.a().a(), jSONArray, activity2, MediationServiceImpl.this.a, maxAdListener3);
                    MediationServiceImpl.this.a.K().a((com.applovin.impl.sdk.d.a) cVar);
                }
            };
            b bVar = new b(maxAdFormat, z, activity, jVar, r0);
            this.a.K().a((com.applovin.impl.sdk.d.a) bVar, com.applovin.impl.mediation.d.c.a(maxAdFormat));
        } else {
            throw new IllegalArgumentException("No listener specified");
        }
    }

    public void loadThirdPartyMediatedAd(String str, com.applovin.impl.mediation.b.a aVar, Activity activity, MaxAdListener maxAdListener) {
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (activity != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Loading ");
            sb.append(aVar);
            sb.append("...");
            this.b.b("MediationService", sb.toString());
            this.a.ac().a(aVar, "WILL_LOAD");
            a(aVar);
            i a2 = this.a.w().a((e) aVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(aVar, activity.getApplicationContext());
                a2.a((MaxAdapterInitializationParameters) a3, activity);
                com.applovin.impl.mediation.b.a a4 = aVar.a(a2);
                a2.a(str, a4);
                a4.g();
                a2.a(str, a3, a4, activity, new a(a4, maxAdListener));
                return;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to load ");
            sb2.append(aVar);
            sb2.append(": adapter not loaded");
            this.b.d("MediationService", sb2.toString());
            a(aVar, new e((int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED), maxAdListener);
        } else {
            throw new IllegalArgumentException("A valid Activity is required");
        }
    }

    public void maybeScheduleAdDisplayErrorPostback(e eVar, com.applovin.impl.mediation.b.a aVar) {
        a("mierr", Collections.EMPTY_MAP, eVar, (e) aVar);
    }

    public void maybeScheduleAdapterInitializationPostback(e eVar, long j, InitializationStatus initializationStatus, String str) {
        HashMap hashMap = new HashMap(3);
        hashMap.put("{INIT_STATUS}", String.valueOf(initializationStatus.getCode()));
        hashMap.put("{INIT_TIME_MS}", String.valueOf(j));
        a("minit", (Map<String, String>) hashMap, new e(str), eVar);
    }

    public void maybeScheduleCallbackAdImpressionPostback(com.applovin.impl.mediation.b.a aVar) {
        a("mcimp", (e) aVar);
    }

    public void maybeScheduleRawAdImpressionPostback(com.applovin.impl.mediation.b.a aVar) {
        this.a.ac().a(aVar, "WILL_DISPLAY");
        a("mimp", (e) aVar);
    }

    public void maybeScheduleViewabilityAdImpressionPostback(com.applovin.impl.mediation.b.b bVar, long j) {
        HashMap hashMap = new HashMap(1);
        hashMap.put("{VIEWABILITY_FLAGS}", String.valueOf(j));
        hashMap.put("{USED_VIEWABILITY_TIMER}", String.valueOf(bVar.r()));
        a("mvimp", (Map<String, String>) hashMap, (e) bVar);
    }

    public void showFullscreenAd(MaxAd maxAd, String str, final Activity activity) {
        if (maxAd == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAd instanceof c) {
            this.a.Z().a(true);
            final c cVar = (c) maxAd;
            final i c = cVar.c();
            if (c != null) {
                cVar.d(str);
                long K = cVar.K();
                StringBuilder sb = new StringBuilder();
                sb.append("Showing ad ");
                sb.append(maxAd.getAdUnitId());
                sb.append(" with delay of ");
                sb.append(K);
                sb.append("ms...");
                this.b.c("MediationService", sb.toString());
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        if (cVar.getFormat() == MaxAdFormat.REWARDED) {
                            MediationServiceImpl.this.a.K().a((com.applovin.impl.sdk.d.a) new h(cVar, MediationServiceImpl.this.a), com.applovin.impl.sdk.d.r.a.MEDIATION_REWARD);
                        }
                        c.a((com.applovin.impl.mediation.b.a) cVar, activity);
                        MediationServiceImpl.this.a.Z().a(false);
                        MediationServiceImpl.this.b.b("MediationService", "Scheduling impression for ad manually...");
                        MediationServiceImpl.this.maybeScheduleRawAdImpressionPostback(cVar);
                    }
                }, K);
                return;
            }
            this.a.Z().a(false);
            p pVar = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to show ");
            sb2.append(maxAd);
            sb2.append(": adapter not found");
            pVar.d("MediationService", sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("There may be an integration problem with the adapter for ad unit id '");
            sb3.append(cVar.getAdUnitId());
            sb3.append("'. Please check if you have a supported version of that SDK integrated into your project.");
            p.j("MediationService", sb3.toString());
            throw new IllegalStateException("Could not find adapter for provided ad");
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Unable to show ad for '");
            sb4.append(maxAd.getAdUnitId());
            sb4.append("': only REWARDED or INTERSTITIAL ads are eligible for showFullscreenAd(). ");
            sb4.append(maxAd.getFormat());
            sb4.append(" ad was provided.");
            p.j("MediationService", sb4.toString());
            throw new IllegalArgumentException("Provided ad is not a MediatedFullscreenAd");
        }
    }
}
