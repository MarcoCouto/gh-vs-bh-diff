package com.applovin.impl.mediation.c;

import android.app.Activity;
import com.applovin.impl.mediation.d.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.q;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import com.ironsource.sdk.precache.DownloadManager;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class f extends com.applovin.impl.sdk.d.a {
    /* access modifiers changed from: private */
    public final String a;
    /* access modifiers changed from: private */
    public final MaxAdFormat c;
    /* access modifiers changed from: private */
    public final JSONObject d;
    /* access modifiers changed from: private */
    public final MaxAdListener e;
    /* access modifiers changed from: private */
    public final WeakReference<Activity> f;

    private class a extends com.applovin.impl.sdk.d.a {
        private final JSONArray c;
        private final int d;

        a(int i, JSONArray jSONArray) {
            super("TaskProcessNextWaterfallAd", f.this.b);
            if (i < 0 || i >= jSONArray.length()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Invalid ad index specified: ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
            }
            this.c = jSONArray;
            this.d = i;
        }

        private String a(int i) {
            return (i < 0 || i >= this.c.length()) ? "undefined" : i.b(i.a(this.c, i, new JSONObject(), this.b), "type", "undefined", this.b);
        }

        private void b() {
            JSONObject a2 = i.a(this.c, this.d, (JSONObject) null, this.b);
            String a3 = a(this.d);
            if ("adapter".equalsIgnoreCase(a3)) {
                a("Starting task for adapter ad...");
                e("started to load ad");
                r K = this.b.K();
                e eVar = new e(f.this.a, a2, f.this.d, this.b, (Activity) f.this.f.get(), new com.applovin.impl.mediation.d.a(f.this.e, this.b) {
                    public void onAdLoadFailed(String str, int i) {
                        a aVar = a.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("failed to load ad: ");
                        sb.append(i);
                        aVar.e(sb.toString());
                        a.this.c();
                    }

                    public void onAdLoaded(MaxAd maxAd) {
                        a.this.e("loaded ad");
                        f.this.a(maxAd);
                    }
                });
                K.a((com.applovin.impl.sdk.d.a) eVar);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to process ad of unknown type: ");
            sb.append(a3);
            d(sb.toString());
            f.this.a(-800);
        }

        /* access modifiers changed from: private */
        public void c() {
            if (this.d < this.c.length() - 1) {
                StringBuilder sb = new StringBuilder();
                sb.append("Attempting to load next ad (");
                sb.append(this.d);
                sb.append(") after failure...");
                b(sb.toString());
                this.b.K().a((com.applovin.impl.sdk.d.a) new a(this.d + 1, this.c), c.a(f.this.c));
                return;
            }
            f.this.b();
        }

        /* access modifiers changed from: private */
        public void e(String str) {
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.F;
        }

        public void run() {
            if (((Boolean) this.b.a(d.fc)).booleanValue()) {
                try {
                    b();
                } catch (Throwable th) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Encountered error while processing ad number ");
                    sb.append(this.d);
                    a(sb.toString(), th);
                    this.b.M().a(a());
                    f.this.b();
                }
            } else {
                b();
            }
        }
    }

    f(String str, MaxAdFormat maxAdFormat, JSONObject jSONObject, Activity activity, j jVar, MaxAdListener maxAdListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("TaskProcessMediationWaterfall ");
        sb.append(str);
        super(sb.toString(), jVar);
        this.a = str;
        this.c = maxAdFormat;
        this.d = jSONObject;
        this.e = maxAdListener;
        this.f = new WeakReference<>(activity);
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        h L;
        g gVar;
        if (i == 204) {
            L = this.b.L();
            gVar = g.p;
        } else if (i == -5001) {
            L = this.b.L();
            gVar = g.q;
        } else {
            L = this.b.L();
            gVar = g.r;
        }
        L.a(gVar);
        StringBuilder sb = new StringBuilder();
        sb.append("Notifying parent of ad load failure for ad unit ");
        sb.append(this.a);
        sb.append(": ");
        sb.append(i);
        b(sb.toString());
        com.applovin.impl.sdk.utils.j.a(this.e, this.a, i);
    }

    /* access modifiers changed from: private */
    public void a(MaxAd maxAd) {
        StringBuilder sb = new StringBuilder();
        sb.append("Notifying parent of ad load success for ad unit ");
        sb.append(this.a);
        b(sb.toString());
        com.applovin.impl.sdk.utils.j.a(this.e, maxAd);
    }

    /* access modifiers changed from: private */
    public void b() {
        a((int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED);
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.E;
    }

    public void run() {
        JSONArray optJSONArray = this.d.optJSONArray("ads");
        int length = optJSONArray != null ? optJSONArray.length() : 0;
        if (length > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Loading the first out of ");
            sb.append(length);
            sb.append(" ads...");
            a(sb.toString());
            this.b.K().a((com.applovin.impl.sdk.d.a) new a(0, optJSONArray));
            return;
        }
        c("No ads were returned from the server");
        q.a(this.a, this.d, this.b);
        JSONObject b = i.b(this.d, DownloadManager.SETTINGS, new JSONObject(), this.b);
        long a2 = i.a(b, "alfdcs", 0, this.b);
        if (a2 > 0) {
            long millis = TimeUnit.SECONDS.toMillis(a2);
            AnonymousClass1 r4 = new Runnable() {
                public void run() {
                    f.this.a(204);
                }
            };
            if (i.a(b, "alfdcs_iba", Boolean.valueOf(false), this.b).booleanValue()) {
                com.applovin.impl.sdk.utils.d.a(millis, this.b, r4);
            } else {
                AppLovinSdkUtils.runOnUiThreadDelayed(r4, millis);
            }
        } else {
            a(204);
        }
    }
}
