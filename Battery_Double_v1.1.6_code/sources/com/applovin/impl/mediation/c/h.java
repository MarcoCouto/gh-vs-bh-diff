package com.applovin.impl.mediation.c;

import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.ae;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import org.json.JSONObject;

public class h extends ae {
    private final c a;

    public h(c cVar, j jVar) {
        super("TaskValidateMaxReward", jVar);
        this.a = cVar;
    }

    public i a() {
        return i.K;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        super.a(i);
        this.a.a(com.applovin.impl.sdk.a.c.a((i < 400 || i >= 500) ? "network_timeout" : "rejected"));
    }

    /* access modifiers changed from: protected */
    public void a(com.applovin.impl.sdk.a.c cVar) {
        this.a.a(cVar);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "ad_unit_id", this.a.getAdUnitId(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, IronSourceConstants.EVENTS_PLACEMENT_NAME, this.a.N(), this.b);
        String s = this.a.s();
        String str = "mcode";
        if (!n.b(s)) {
            s = "NO_MCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str, s, this.b);
        String r = this.a.r();
        String str2 = "bcode";
        if (!n.b(r)) {
            r = "NO_BCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str2, r, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/mvr";
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.a.t();
    }
}
