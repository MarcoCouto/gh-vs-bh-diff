package com.applovin.impl.mediation.c;

import android.app.Activity;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinMediationProvider;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a extends com.applovin.impl.sdk.d.a {
    /* access modifiers changed from: private */
    public final Activity a;

    public a(Activity activity, j jVar) {
        super("TaskAutoInitAdapters", jVar, true);
        this.a = activity;
    }

    private List<e> a(JSONArray jSONArray, JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(new e(i.a(jSONArray, i, (JSONObject) null, this.b), jSONObject, this.b));
        }
        return arrayList;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.A;
    }

    public void run() {
        String str;
        String str2 = (String) this.b.a(f.y);
        if (n.b(str2)) {
            if (this.a == null) {
                p.j("AppLovinSdk", "Failed to initialize 3rd-party SDKs. Please contact us at devsupport@applovin.com for more information.");
                this.b.L().b(g.o, 1);
                return;
            }
            try {
                JSONObject jSONObject = new JSONObject(str2);
                boolean a2 = i.a(this.b.O().d().b, i.b(jSONObject, "test_mode_idfas", new JSONArray(), this.b));
                List<e> a3 = a(i.b(jSONObject, a2 ? "test_mode_auto_init_adapters" : "auto_init_adapters", new JSONArray(), this.b), jSONObject);
                if (a3.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Auto-initing ");
                    sb.append(a3.size());
                    sb.append(" adapters");
                    sb.append(a2 ? " in test mode" : "");
                    sb.append("...");
                    a(sb.toString());
                    this.b.c(AppLovinMediationProvider.MAX);
                    for (final e eVar : a3) {
                        this.b.K().b().execute(new Runnable() {
                            public void run() {
                                a aVar = a.this;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Auto-initing adapter: ");
                                sb.append(eVar);
                                aVar.a(sb.toString());
                                a.this.b.x().a(eVar, a.this.a);
                            }
                        });
                    }
                } else {
                    d("No auto-init adapters found");
                }
            } catch (JSONException e) {
                th = e;
                str = "Failed to parse auto-init adapters JSON";
                a(str, th);
            } catch (Throwable th) {
                th = th;
                str = "Failed to auto-init adapters";
                a(str, th);
            }
        }
    }
}
