package com.applovin.impl.mediation.c;

import android.app.Activity;
import com.applovin.impl.mediation.b.b;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.mediation.b.d;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.q;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

public class e extends a {
    private final String a;
    private final JSONObject c;
    private final JSONObject d;
    private final MaxAdListener e;
    private final WeakReference<Activity> f;

    e(String str, JSONObject jSONObject, JSONObject jSONObject2, j jVar, Activity activity, MaxAdListener maxAdListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("TaskLoadAdapterAd ");
        sb.append(str);
        super(sb.toString(), jVar);
        this.a = str;
        this.c = jSONObject;
        this.d = jSONObject2;
        this.f = new WeakReference<>(activity);
        this.e = maxAdListener;
    }

    private void b() {
        this.b.y().loadThirdPartyMediatedAd(this.a, d(), c(), this.e);
    }

    private Activity c() {
        Activity activity = (Activity) this.f.get();
        return activity != null ? activity : this.b.ag();
    }

    private com.applovin.impl.mediation.b.a d() {
        String b = i.b(this.d, "ad_format", (String) null, this.b);
        MaxAdFormat c2 = q.c(b);
        if (c2 == MaxAdFormat.BANNER || c2 == MaxAdFormat.MREC || c2 == MaxAdFormat.LEADER) {
            return new b(this.c, this.d, this.b);
        }
        if (c2 == MaxAdFormat.NATIVE) {
            return new d(this.c, this.d, this.b);
        }
        if (c2 == MaxAdFormat.INTERSTITIAL || c2 == MaxAdFormat.REWARDED) {
            return new c(this.c, this.d, this.b);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Unsupported ad format: ");
        sb.append(b);
        throw new IllegalArgumentException(sb.toString());
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.D;
    }

    public void run() {
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.fd)).booleanValue()) {
            try {
                b();
            } catch (Throwable th) {
                a("Unable to process adapter ad", th);
                this.b.M().a(a());
                com.applovin.impl.sdk.utils.j.a(this.e, this.a, (int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED);
            }
        } else {
            b();
        }
    }
}
