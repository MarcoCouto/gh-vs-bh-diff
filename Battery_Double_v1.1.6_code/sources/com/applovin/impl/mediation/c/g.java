package com.applovin.impl.mediation.c;

import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.z;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import org.json.JSONObject;

public class g extends z {
    private final c a;

    public g(c cVar, j jVar) {
        super("TaskReportMaxReward", jVar);
        this.a = cVar;
    }

    public i a() {
        return i.L;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        super.a(i);
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to report reward for mediated ad: ");
        sb.append(this.a);
        sb.append(" - error code: ");
        sb.append(i);
        a(sb.toString());
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "ad_unit_id", this.a.getAdUnitId(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, IronSourceConstants.EVENTS_PLACEMENT_NAME, this.a.N(), this.b);
        String s = this.a.s();
        String str = "mcode";
        if (!n.b(s)) {
            s = "NO_MCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str, s, this.b);
        String r = this.a.r();
        String str2 = "bcode";
        if (!n.b(r)) {
            r = "NO_BCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, str2, r, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/mcr";
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append("Reported reward successfully for mediated ad: ");
        sb.append(this.a);
        a(sb.toString());
    }

    /* access modifiers changed from: protected */
    public com.applovin.impl.sdk.a.c c() {
        return this.a.v();
    }

    /* access modifiers changed from: protected */
    public void d() {
        StringBuilder sb = new StringBuilder();
        sb.append("No reward result was found for mediated ad: ");
        sb.append(this.a);
        d(sb.toString());
    }
}
