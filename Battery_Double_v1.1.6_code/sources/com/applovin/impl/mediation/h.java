package com.applovin.impl.mediation;

import android.text.TextUtils;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapters.MediationAdapterBase;
import com.applovin.sdk.AppLovinSdk;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class h {
    private final j a;
    private final p b;
    private final Object c = new Object();
    private final Map<String, Class<? extends MaxAdapter>> d = new HashMap();
    private final Set<String> e = new HashSet();

    public h(j jVar) {
        if (jVar != null) {
            this.a = jVar;
            this.b = jVar.v();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private i a(e eVar, Class<? extends MaxAdapter> cls) {
        try {
            i iVar = new i(eVar, (MediationAdapterBase) cls.getConstructor(new Class[]{AppLovinSdk.class}).newInstance(new Object[]{this.a.S()}), this.a);
            if (iVar.c()) {
                return iVar;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Adapter is disabled after initialization: ");
            sb.append(eVar);
            p.j("MediationAdapterManager", sb.toString());
            return null;
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to load adapter: ");
            sb2.append(eVar);
            p.c("MediationAdapterManager", sb2.toString(), th);
        }
    }

    private Class<? extends MaxAdapter> a(String str) {
        try {
            Class cls = Class.forName(str);
            if (MaxAdapter.class.isAssignableFrom(cls)) {
                return cls.asSubclass(MaxAdapter.class);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" error: not an instance of '");
            sb.append(MaxAdapter.class.getName());
            sb.append("'.");
            p.j("MediationAdapterManager", sb.toString());
            return null;
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to load: ");
            sb2.append(str);
            p.c("MediationAdapterManager", sb2.toString(), th);
        }
    }

    /* access modifiers changed from: 0000 */
    public i a(e eVar) {
        Class cls;
        if (eVar != null) {
            String D = eVar.D();
            String C = eVar.C();
            if (TextUtils.isEmpty(D)) {
                StringBuilder sb = new StringBuilder();
                sb.append("No adapter name provided for ");
                sb.append(C);
                sb.append(", not loading the adapter ");
                this.b.e("MediationAdapterManager", sb.toString());
                return null;
            } else if (TextUtils.isEmpty(C)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to find default classname for '");
                sb2.append(D);
                sb2.append("'");
                this.b.e("MediationAdapterManager", sb2.toString());
                return null;
            } else {
                synchronized (this.c) {
                    if (!this.e.contains(C)) {
                        if (this.d.containsKey(C)) {
                            cls = (Class) this.d.get(C);
                        } else {
                            cls = a(C);
                            if (cls == null) {
                                this.e.add(C);
                                return null;
                            }
                        }
                        i a2 = a(eVar, cls);
                        if (a2 != null) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Loaded ");
                            sb3.append(D);
                            this.b.b("MediationAdapterManager", sb3.toString());
                            this.d.put(C, cls);
                            return a2;
                        }
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Failed to load ");
                        sb4.append(D);
                        this.b.e("MediationAdapterManager", sb4.toString());
                        this.e.add(C);
                        return null;
                    }
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("Not attempting to load ");
                    sb5.append(D);
                    sb5.append(" due to prior errors");
                    this.b.b("MediationAdapterManager", sb5.toString());
                    return null;
                }
            }
        } else {
            throw new IllegalArgumentException("No adapter spec specified");
        }
    }

    public Collection<String> a() {
        Set unmodifiableSet;
        synchronized (this.c) {
            HashSet hashSet = new HashSet(this.d.size());
            for (Class name : this.d.values()) {
                hashSet.add(name.getName());
            }
            unmodifiableSet = Collections.unmodifiableSet(hashSet);
        }
        return unmodifiableSet;
    }

    public Collection<String> b() {
        Set unmodifiableSet;
        synchronized (this.c) {
            unmodifiableSet = Collections.unmodifiableSet(this.e);
        }
        return unmodifiableSet;
    }
}
