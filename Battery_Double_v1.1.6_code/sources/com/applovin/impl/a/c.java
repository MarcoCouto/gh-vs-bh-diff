package com.applovin.impl.a;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.q;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;

public class c {
    private static final List<String> c = Arrays.asList(new String[]{MimeTypes.VIDEO_MP4, MimeTypes.VIDEO_WEBM, MimeTypes.VIDEO_H263, "video/x-matroska"});
    protected List<s> a = new ArrayList();
    private final j b;
    private final JSONObject d;
    private final JSONObject e;
    private final b f;
    private final long g = System.currentTimeMillis();

    public c(JSONObject jSONObject, JSONObject jSONObject2, b bVar, j jVar) {
        this.b = jVar;
        this.d = jSONObject;
        this.e = jSONObject2;
        this.f = bVar;
    }

    public int a() {
        return this.a.size();
    }

    public List<s> b() {
        return this.a;
    }

    public JSONObject c() {
        return this.d;
    }

    public JSONObject d() {
        return this.e;
    }

    public b e() {
        return this.f;
    }

    public long f() {
        return this.g;
    }

    public d g() {
        String b2 = i.b(this.e, "zone_id", (String) null, this.b);
        return d.a(AppLovinAdSize.fromString(i.b(this.e, "ad_size", (String) null, this.b)), AppLovinAdType.fromString(i.b(this.e, AppEventsConstants.EVENT_PARAM_AD_TYPE, (String) null, this.b)), b2, this.b);
    }

    public List<String> h() {
        List<String> a2 = e.a(i.b(this.d, "vast_preferred_video_types", (String) null, (j) null));
        return !a2.isEmpty() ? a2 : c;
    }

    public int i() {
        return q.a(this.d);
    }
}
