package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.explorestack.iab.vast.tags.VastTagName;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class i {
    private static DateFormat a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
    private static Random b = new Random(System.currentTimeMillis());

    public static Uri a(String str, long j, Uri uri, d dVar, j jVar) {
        if (URLUtil.isValidUrl(str)) {
            try {
                String replace = str.replace("[ERRORCODE]", Integer.toString(dVar.a()));
                if (j >= 0) {
                    replace = replace.replace("[CONTENTPLAYHEAD]", a(j));
                }
                if (uri != null) {
                    replace = replace.replace("[ASSETURI]", uri.toString());
                }
                return Uri.parse(replace.replace("[CACHEBUSTING]", a()).replace("[TIMESTAMP]", b()));
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to replace macros in URL string ");
                sb.append(str);
                jVar.v().b("VastUtils", sb.toString(), th);
                return null;
            }
        } else {
            jVar.v().e("VastUtils", "Unable to replace macros in invalid URL string.");
            return null;
        }
    }

    public static d a(a aVar) {
        if (b(aVar) || c(aVar)) {
            return null;
        }
        return d.GENERAL_WRAPPER_ERROR;
    }

    private static String a() {
        return Integer.toString(b.nextInt(89999999) + 10000000);
    }

    private static String a(long j) {
        if (j <= 0) {
            return "00:00:00.000";
        }
        return String.format(Locale.US, "%02d:%02d:%02d.000", new Object[]{Long.valueOf(TimeUnit.SECONDS.toHours(j)), Long.valueOf(TimeUnit.SECONDS.toMinutes(j) % TimeUnit.MINUTES.toSeconds(1)), Long.valueOf(j % TimeUnit.MINUTES.toSeconds(1))});
    }

    public static String a(c cVar) {
        if (cVar != null) {
            List b2 = cVar.b();
            int size = cVar.b().size();
            if (size > 0) {
                s c = ((s) b2.get(size - 1)).c(VastTagName.VAST_AD_TAG_URI);
                if (c != null) {
                    return c.c();
                }
            }
            return null;
        }
        throw new IllegalArgumentException("Unable to get resolution uri string for fetching the next wrapper or inline response in the chain");
    }

    public static String a(s sVar, String str, String str2) {
        s b2 = sVar.b(str);
        if (b2 != null) {
            String c = b2.c();
            if (n.b(c)) {
                return c;
            }
        }
        return str2;
    }

    private static Set<g> a(c cVar, j jVar) {
        if (cVar == null) {
            return null;
        }
        List<s> b2 = cVar.b();
        Set<g> hashSet = new HashSet<>(b2.size());
        for (s sVar : b2) {
            s c = sVar.c("Wrapper");
            if (c == null) {
                c = sVar.c("InLine");
            }
            hashSet = a(hashSet, c != null ? c.a("Error") : sVar.a("Error"), cVar, jVar);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Retrieved ");
        sb.append(hashSet.size());
        sb.append(" top level error trackers: ");
        sb.append(hashSet);
        jVar.v().b("VastUtils", sb.toString());
        return hashSet;
    }

    private static Set<g> a(Set<g> set, List<s> list, c cVar, j jVar) {
        if (list != null) {
            for (s a2 : list) {
                g a3 = g.a(a2, cVar, jVar);
                if (a3 != null) {
                    set.add(a3);
                }
            }
        }
        return set;
    }

    public static void a(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, d dVar, int i, j jVar) {
        if (jVar != null) {
            q.a(appLovinAdLoadListener, cVar.g(), i, jVar);
            a(a(cVar, jVar), dVar, jVar);
            return;
        }
        throw new IllegalArgumentException("Unable to handle failure. No sdk specified.");
    }

    public static void a(s sVar, Map<String, Set<g>> map, c cVar, j jVar) {
        p v;
        String str;
        String str2;
        if (jVar != null) {
            if (sVar == null) {
                v = jVar.v();
                str = "VastUtils";
                str2 = "Unable to render event trackers; null node provided";
            } else if (map == null) {
                v = jVar.v();
                str = "VastUtils";
                str2 = "Unable to render event trackers; null event trackers provided";
            } else {
                s b2 = sVar.b("TrackingEvents");
                if (b2 != null) {
                    List<s> a2 = b2.a("Tracking");
                    if (a2 != null) {
                        for (s sVar2 : a2) {
                            String str3 = (String) sVar2.b().get("event");
                            if (n.b(str3)) {
                                g a3 = g.a(sVar2, cVar, jVar);
                                if (a3 != null) {
                                    Set set = (Set) map.get(str3);
                                    if (set != null) {
                                        set.add(a3);
                                    } else {
                                        HashSet hashSet = new HashSet();
                                        hashSet.add(a3);
                                        map.put(str3, hashSet);
                                    }
                                }
                            } else {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Could not find event for tracking node = ");
                                sb.append(sVar2);
                                jVar.v().e("VastUtils", sb.toString());
                            }
                        }
                    }
                }
                return;
            }
            v.e(str, str2);
            return;
        }
        throw new IllegalArgumentException("Unable to render event trackers. No sdk specified.");
    }

    public static void a(List<s> list, Set<g> set, c cVar, j jVar) {
        p v;
        String str;
        String str2;
        if (jVar != null) {
            if (list == null) {
                v = jVar.v();
                str = "VastUtils";
                str2 = "Unable to render trackers; null nodes provided";
            } else if (set == null) {
                v = jVar.v();
                str = "VastUtils";
                str2 = "Unable to render trackers; null trackers provided";
            } else {
                for (s a2 : list) {
                    g a3 = g.a(a2, cVar, jVar);
                    if (a3 != null) {
                        set.add(a3);
                    }
                }
                return;
            }
            v.e(str, str2);
            return;
        }
        throw new IllegalArgumentException("Unable to render trackers. No sdk specified.");
    }

    public static void a(Set<g> set, long j, Uri uri, d dVar, j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("Unable to fire trackers. No sdk specified.");
        } else if (set != null && !set.isEmpty()) {
            for (g b2 : set) {
                Uri a2 = a(b2.b(), j, uri, dVar, jVar);
                if (a2 != null) {
                    jVar.N().a(f.l().a(a2.toString()).a(false).a(), false);
                }
            }
        }
    }

    public static void a(Set<g> set, d dVar, j jVar) {
        a(set, -1, (Uri) null, dVar, jVar);
    }

    public static void a(Set<g> set, j jVar) {
        a(set, -1, (Uri) null, d.UNSPECIFIED, jVar);
    }

    public static boolean a(s sVar) {
        if (sVar != null) {
            return sVar.c("Wrapper") != null;
        }
        throw new IllegalArgumentException("Unable to check if a given XmlNode contains a wrapper response");
    }

    private static String b() {
        a.setTimeZone(TimeZone.getDefault());
        return a.format(new Date());
    }

    public static boolean b(a aVar) {
        boolean z = false;
        if (aVar == null) {
            return false;
        }
        j h = aVar.h();
        if (h != null) {
            List a2 = h.a();
            if (a2 != null && !a2.isEmpty()) {
                z = true;
            }
        }
        return z;
    }

    public static boolean b(s sVar) {
        if (sVar != null) {
            return sVar.c("InLine") != null;
        }
        throw new IllegalArgumentException("Unable to check if a given XmlNode contains an inline response");
    }

    public static boolean c(a aVar) {
        boolean z = false;
        if (aVar == null) {
            return false;
        }
        b j = aVar.j();
        if (j != null) {
            e b2 = j.b();
            if (b2 != null && (b2.b() != null || n.b(b2.c()))) {
                z = true;
            }
        }
        return z;
    }
}
