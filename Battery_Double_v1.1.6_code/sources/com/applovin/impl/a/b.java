package com.applovin.impl.a;

import android.net.Uri;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.s;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class b {
    private int a;
    private int b;
    private Uri c;
    private e d;
    private Set<g> e = new HashSet();
    private Map<String, Set<g>> f = new HashMap();

    private b() {
    }

    public static b a(s sVar, b bVar, c cVar, j jVar) {
        if (sVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (jVar != null) {
            if (bVar == null) {
                try {
                    bVar = new b();
                } catch (Throwable th) {
                    jVar.v().b("VastCompanionAd", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (bVar.a == 0 && bVar.b == 0) {
                int a2 = n.a((String) sVar.b().get("width"));
                int a3 = n.a((String) sVar.b().get("height"));
                if (a2 > 0 && a3 > 0) {
                    bVar.a = a2;
                    bVar.b = a3;
                }
            }
            bVar.d = e.a(sVar, bVar.d, jVar);
            if (bVar.c == null) {
                s b2 = sVar.b("CompanionClickThrough");
                if (b2 != null) {
                    String c2 = b2.c();
                    if (n.b(c2)) {
                        bVar.c = Uri.parse(c2);
                    }
                }
            }
            i.a(sVar.a("CompanionClickTracking"), bVar.e, cVar, jVar);
            i.a(sVar, bVar.f, cVar, jVar);
            return bVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public Uri a() {
        return this.c;
    }

    public e b() {
        return this.d;
    }

    public Set<g> c() {
        return this.e;
    }

    public Map<String, Set<g>> d() {
        return this.f;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (this.a != bVar.a || this.b != bVar.b) {
            return false;
        }
        if (this.c == null ? bVar.c != null : !this.c.equals(bVar.c)) {
            return false;
        }
        if (this.d == null ? bVar.d != null : !this.d.equals(bVar.d)) {
            return false;
        }
        if (this.e == null ? bVar.e != null : !this.e.equals(bVar.e)) {
            return false;
        }
        if (this.f != null) {
            z = this.f.equals(bVar.f);
        } else if (bVar.f != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.a * 31) + this.b) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31;
        if (this.f != null) {
            i = this.f.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastCompanionAd{width=");
        sb.append(this.a);
        sb.append(", height=");
        sb.append(this.b);
        sb.append(", destinationUri=");
        sb.append(this.c);
        sb.append(", nonVideoResource=");
        sb.append(this.d);
        sb.append(", clickTrackers=");
        sb.append(this.e);
        sb.append(", eventTrackers=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
}
