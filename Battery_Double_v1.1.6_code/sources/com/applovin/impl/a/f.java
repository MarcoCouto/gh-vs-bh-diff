package com.applovin.impl.a;

import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.s;

public class f {
    private String a;
    private String b;

    private f() {
    }

    public static f a(s sVar, f fVar, j jVar) {
        if (sVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (jVar != null) {
            if (fVar == null) {
                try {
                    fVar = new f();
                } catch (Throwable th) {
                    jVar.v().b("VastSystemInfo", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (!n.b(fVar.a)) {
                String c = sVar.c();
                if (n.b(c)) {
                    fVar.a = c;
                }
            }
            if (!n.b(fVar.b)) {
                String str = (String) sVar.b().get("version");
                if (n.b(str)) {
                    fVar.b = str;
                }
            }
            return fVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (this.a == null ? fVar.a != null : !this.a.equals(fVar.a)) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(fVar.b);
        } else if (fVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.a != null ? this.a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastSystemInfo{name='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", version='");
        sb.append(this.b);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
