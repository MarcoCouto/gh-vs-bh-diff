package com.applovin.impl.a;

import android.annotation.TargetApi;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.s;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class j {
    private List<k> a = Collections.EMPTY_LIST;
    private List<String> b = Collections.EMPTY_LIST;
    private int c;
    private Uri d;
    private final Set<g> e = new HashSet();
    private final Map<String, Set<g>> f = new HashMap();

    public enum a {
        UNSPECIFIED,
        LOW,
        MEDIUM,
        HIGH
    }

    private j() {
    }

    private j(c cVar) {
        this.b = cVar.h();
    }

    private static int a(String str, com.applovin.impl.sdk.j jVar) {
        try {
            List a2 = e.a(str, ":");
            if (a2.size() == 3) {
                return (int) (TimeUnit.HOURS.toSeconds((long) n.a((String) a2.get(0))) + TimeUnit.MINUTES.toSeconds((long) n.a((String) a2.get(1))) + ((long) n.a((String) a2.get(2))));
            }
        } catch (Throwable unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to parse duration from \"");
            sb.append(str);
            sb.append("\"");
            jVar.v().e("VastVideoCreative", sb.toString());
        }
        return 0;
    }

    public static j a(s sVar, j jVar, c cVar, com.applovin.impl.sdk.j jVar2) {
        if (sVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (cVar == null) {
            throw new IllegalArgumentException("No context specified.");
        } else if (jVar2 != null) {
            if (jVar == null) {
                try {
                    jVar = new j(cVar);
                } catch (Throwable th) {
                    jVar2.v().b("VastVideoCreative", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (jVar.c == 0) {
                s b2 = sVar.b("Duration");
                if (b2 != null) {
                    int a2 = a(b2.c(), jVar2);
                    if (a2 > 0) {
                        jVar.c = a2;
                    }
                }
            }
            s b3 = sVar.b("MediaFiles");
            if (b3 != null) {
                List<k> a3 = a(b3, jVar2);
                if (a3 != null && a3.size() > 0) {
                    if (jVar.a != null) {
                        a3.addAll(jVar.a);
                    }
                    jVar.a = a3;
                }
            }
            s b4 = sVar.b("VideoClicks");
            if (b4 != null) {
                if (jVar.d == null) {
                    s b5 = b4.b("ClickThrough");
                    if (b5 != null) {
                        String c2 = b5.c();
                        if (n.b(c2)) {
                            jVar.d = Uri.parse(c2);
                        }
                    }
                }
                i.a(b4.a("ClickTracking"), jVar.e, cVar, jVar2);
            }
            i.a(sVar, jVar.f, cVar, jVar2);
            return jVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    private static List<k> a(s sVar, com.applovin.impl.sdk.j jVar) {
        List<s> a2 = sVar.a("MediaFile");
        ArrayList arrayList = new ArrayList(a2.size());
        List a3 = e.a((String) jVar.a(d.eM));
        List a4 = e.a((String) jVar.a(d.eL));
        for (s a5 : a2) {
            k a6 = k.a(a5, jVar);
            if (a6 != null) {
                try {
                    String d2 = a6.d();
                    if (!n.b(d2) || a3.contains(d2)) {
                        if (((Boolean) jVar.a(d.eN)).booleanValue()) {
                            String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(a6.b().toString());
                            if (n.b(fileExtensionFromUrl) && !a4.contains(fileExtensionFromUrl)) {
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append("Video file not supported: ");
                        sb.append(a6);
                        jVar.v().d("VastVideoCreative", sb.toString());
                    }
                    arrayList.add(a6);
                } catch (Throwable th) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to validate vidoe file: ");
                    sb2.append(a6);
                    jVar.v().b("VastVideoCreative", sb2.toString(), th);
                }
            }
        }
        return arrayList;
    }

    public k a(a aVar) {
        if (this.a == null || this.a.size() == 0) {
            return null;
        }
        List arrayList = new ArrayList(3);
        for (String str : this.b) {
            for (k kVar : this.a) {
                String d2 = kVar.d();
                if (n.b(d2) && str.equalsIgnoreCase(d2)) {
                    arrayList.add(kVar);
                }
            }
            if (!arrayList.isEmpty()) {
                break;
            }
        }
        if (arrayList.isEmpty()) {
            arrayList = this.a;
        }
        if (g.c()) {
            Collections.sort(arrayList, new Comparator<k>() {
                @TargetApi(19)
                /* renamed from: a */
                public int compare(k kVar, k kVar2) {
                    return Integer.compare(kVar.e(), kVar2.e());
                }
            });
        }
        int size = aVar == a.LOW ? 0 : aVar == a.MEDIUM ? arrayList.size() / 2 : arrayList.size() - 1;
        return (k) arrayList.get(size);
    }

    public List<k> a() {
        return this.a;
    }

    public int b() {
        return this.c;
    }

    public Uri c() {
        return this.d;
    }

    public Set<g> d() {
        return this.e;
    }

    public Map<String, Set<g>> e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        if (this.c != jVar.c) {
            return false;
        }
        if (this.a == null ? jVar.a != null : !this.a.equals(jVar.a)) {
            return false;
        }
        if (this.d == null ? jVar.d != null : !this.d.equals(jVar.d)) {
            return false;
        }
        if (this.e == null ? jVar.e != null : !this.e.equals(jVar.e)) {
            return false;
        }
        if (this.f != null) {
            z = this.f.equals(jVar.f);
        } else if (jVar.f != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.a != null ? this.a.hashCode() : 0) * 31) + this.c) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31;
        if (this.f != null) {
            i = this.f.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VastVideoCreative{videoFiles=");
        sb.append(this.a);
        sb.append(", durationSeconds=");
        sb.append(this.c);
        sb.append(", destinationUri=");
        sb.append(this.d);
        sb.append(", clickTrackers=");
        sb.append(this.e);
        sb.append(", eventTrackers=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
}
