package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import com.applovin.impl.a.b;
import com.applovin.impl.a.e;
import com.applovin.impl.sdk.ad.a;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.h;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;

class c extends g {
    private static c g;
    /* access modifiers changed from: private */
    public final p a;
    private final j b;
    private d c;
    private AppLovinAd d;
    private boolean e;
    private final boolean f;

    c(d dVar, j jVar, Context context) {
        this(dVar, jVar, context, false);
    }

    c(d dVar, j jVar, Context context, boolean z) {
        super(context);
        this.d = null;
        this.e = false;
        if (jVar != null) {
            this.b = jVar;
            this.a = jVar.v();
            this.f = z;
            setBackgroundColor(0);
            WebSettings settings = getSettings();
            settings.setSupportMultipleWindows(false);
            settings.setJavaScriptEnabled(true);
            setWebViewClient(dVar);
            setWebChromeClient(new b(jVar));
            setVerticalScrollBarEnabled(false);
            setHorizontalScrollBarEnabled(false);
            setScrollBarStyle(33554432);
            if (g.g()) {
                setWebViewRenderProcessClient(new e(jVar).a());
            }
            setOnTouchListener(new OnTouchListener() {
                @SuppressLint({"ClickableViewAccessibility"})
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (!view.hasFocus()) {
                        view.requestFocus();
                    }
                    return false;
                }
            });
            setOnLongClickListener(new OnLongClickListener() {
                public boolean onLongClick(View view) {
                    c.this.a.b("AdWebView", "Received a LongClick event.");
                    return true;
                }
            });
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    public static c a(AppLovinAdSize appLovinAdSize, d dVar, j jVar, Context context) {
        if (!((Boolean) jVar.a(com.applovin.impl.sdk.b.d.fg)).booleanValue() || appLovinAdSize != AppLovinAdSize.INTERSTITIAL) {
            return new c(dVar, jVar, context);
        }
        if (g == null) {
            g = new c(dVar, jVar, context.getApplicationContext(), true);
        } else {
            g.setWebViewClient(dVar);
        }
        return g;
    }

    private String a(String str, String str2) {
        if (n.b(str)) {
            return q.b(str).replace("{SOURCE}", str2);
        }
        return null;
    }

    private void a(final f fVar) {
        try {
            if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.eW)).booleanValue() || fVar.aw()) {
                a((Runnable) new Runnable() {
                    public void run() {
                        c.this.loadUrl("about:blank");
                    }
                });
            }
            if (g.b()) {
                a((Runnable) new Runnable() {
                    @TargetApi(17)
                    public void run() {
                        c.this.getSettings().setMediaPlaybackRequiresUserGesture(fVar.av());
                    }
                });
            }
            if (g.c() && fVar.ay()) {
                a((Runnable) new Runnable() {
                    @TargetApi(19)
                    public void run() {
                        WebView.setWebContentsDebuggingEnabled(true);
                    }
                });
            }
            w az = fVar.az();
            if (az != null) {
                final WebSettings settings = getSettings();
                final PluginState b2 = az.b();
                if (b2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setPluginState(b2);
                        }
                    });
                }
                final Boolean c2 = az.c();
                if (c2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setAllowFileAccess(c2.booleanValue());
                        }
                    });
                }
                final Boolean d2 = az.d();
                if (d2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setLoadWithOverviewMode(d2.booleanValue());
                        }
                    });
                }
                final Boolean e2 = az.e();
                if (e2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setUseWideViewPort(e2.booleanValue());
                        }
                    });
                }
                final Boolean f2 = az.f();
                if (f2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setAllowContentAccess(f2.booleanValue());
                        }
                    });
                }
                final Boolean g2 = az.g();
                if (g2 != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setBuiltInZoomControls(g2.booleanValue());
                        }
                    });
                }
                final Boolean h = az.h();
                if (h != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setDisplayZoomControls(h.booleanValue());
                        }
                    });
                }
                final Boolean i = az.i();
                if (i != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setSaveFormData(i.booleanValue());
                        }
                    });
                }
                final Boolean j = az.j();
                if (j != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setGeolocationEnabled(j.booleanValue());
                        }
                    });
                }
                final Boolean k = az.k();
                if (k != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setNeedInitialFocus(k.booleanValue());
                        }
                    });
                }
                final Boolean l = az.l();
                if (l != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setAllowFileAccessFromFileURLs(l.booleanValue());
                        }
                    });
                }
                final Boolean m = az.m();
                if (m != null) {
                    a((Runnable) new Runnable() {
                        public void run() {
                            settings.setAllowUniversalAccessFromFileURLs(m.booleanValue());
                        }
                    });
                }
                if (g.d()) {
                    final Integer a2 = az.a();
                    if (a2 != null) {
                        a((Runnable) new Runnable() {
                            @TargetApi(21)
                            public void run() {
                                settings.setMixedContentMode(a2.intValue());
                            }
                        });
                    }
                }
                if (g.e()) {
                    final Boolean n = az.n();
                    if (n != null) {
                        a((Runnable) new Runnable() {
                            @TargetApi(23)
                            public void run() {
                                settings.setOffscreenPreRaster(n.booleanValue());
                            }
                        });
                    }
                }
            }
        } catch (Throwable th) {
            this.a.b("AdWebView", "Unable to apply WebView settings", th);
        }
    }

    private void a(Runnable runnable) {
        try {
            runnable.run();
        } catch (Throwable th) {
            this.a.b("AdWebView", "Unable to apply WebView setting", th);
        }
    }

    private void a(String str, String str2, String str3, j jVar) {
        p pVar;
        String str4;
        StringBuilder sb;
        String a2 = a(str3, str);
        if (n.b(a2)) {
            pVar = this.a;
            str4 = "AdWebView";
            sb = new StringBuilder();
        } else {
            a2 = a((String) jVar.a(com.applovin.impl.sdk.b.d.eH), str);
            if (n.b(a2)) {
                pVar = this.a;
                str4 = "AdWebView";
                sb = new StringBuilder();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Rendering webview for VAST ad with resourceURL : ");
                sb2.append(str);
                this.a.b("AdWebView", sb2.toString());
                loadUrl(str);
                return;
            }
        }
        sb.append("Rendering webview for VAST ad with resourceContents : ");
        sb.append(a2);
        pVar.b(str4, sb.toString());
        loadDataWithBaseURL(str2, a2, WebRequest.CONTENT_TYPE_HTML, null, "");
    }

    public void a(d dVar) {
        this.c = dVar;
    }

    public void a(AppLovinAd appLovinAd) {
        p pVar;
        String str;
        String str2;
        p pVar2;
        String str3;
        String str4;
        String str5;
        String ax;
        String str6;
        String str7;
        String str8;
        String ax2;
        j jVar;
        if (!this.e) {
            this.d = appLovinAd;
            try {
                if (appLovinAd instanceof h) {
                    loadDataWithBaseURL("/", ((h) appLovinAd).a(), WebRequest.CONTENT_TYPE_HTML, null, "");
                    pVar = this.a;
                    str = "AdWebView";
                    str2 = "Empty ad rendered";
                } else {
                    f fVar = (f) appLovinAd;
                    a(fVar);
                    if (fVar.ag()) {
                        setVisibility(0);
                    }
                    if (appLovinAd instanceof a) {
                        loadDataWithBaseURL(fVar.ax(), q.b(((a) appLovinAd).a()), WebRequest.CONTENT_TYPE_HTML, null, "");
                        pVar = this.a;
                        str = "AdWebView";
                        str2 = "AppLovinAd rendered";
                    } else if (appLovinAd instanceof com.applovin.impl.a.a) {
                        com.applovin.impl.a.a aVar = (com.applovin.impl.a.a) appLovinAd;
                        b j = aVar.j();
                        if (j != null) {
                            e b2 = j.b();
                            Uri b3 = b2.b();
                            String uri = b3 != null ? b3.toString() : "";
                            String c2 = b2.c();
                            String aK = aVar.aK();
                            if (!n.b(uri)) {
                                if (!n.b(c2)) {
                                    pVar2 = this.a;
                                    str3 = "AdWebView";
                                    str4 = "Unable to load companion ad. No resources provided.";
                                    pVar2.e(str3, str4);
                                    return;
                                }
                            }
                            if (b2.a() == e.a.STATIC) {
                                this.a.b("AdWebView", "Rendering WebView for static VAST ad");
                                loadDataWithBaseURL(fVar.ax(), a((String) this.b.a(com.applovin.impl.sdk.b.d.eG), uri), WebRequest.CONTENT_TYPE_HTML, null, "");
                                return;
                            }
                            if (b2.a() == e.a.HTML) {
                                if (n.b(c2)) {
                                    String a2 = a(aK, c2);
                                    str5 = n.b(a2) ? a2 : c2;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Rendering WebView for HTML VAST ad with resourceContents: ");
                                    sb.append(str5);
                                    this.a.b("AdWebView", sb.toString());
                                    ax = fVar.ax();
                                    str6 = WebRequest.CONTENT_TYPE_HTML;
                                    str7 = null;
                                    str8 = "";
                                } else if (n.b(uri)) {
                                    this.a.b("AdWebView", "Preparing to load HTML VAST ad resourceUri");
                                    ax2 = fVar.ax();
                                    jVar = this.b;
                                    a(uri, ax2, aK, jVar);
                                    return;
                                } else {
                                    return;
                                }
                            } else if (b2.a() != e.a.IFRAME) {
                                pVar2 = this.a;
                                str3 = "AdWebView";
                                str4 = "Failed to render VAST companion ad of invalid type";
                                pVar2.e(str3, str4);
                                return;
                            } else if (n.b(uri)) {
                                this.a.b("AdWebView", "Preparing to load iFrame VAST ad resourceUri");
                                ax2 = fVar.ax();
                                jVar = this.b;
                                a(uri, ax2, aK, jVar);
                                return;
                            } else if (n.b(c2)) {
                                String a3 = a(aK, c2);
                                str5 = n.b(a3) ? a3 : c2;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("Rendering WebView for iFrame VAST ad with resourceContents: ");
                                sb2.append(str5);
                                this.a.b("AdWebView", sb2.toString());
                                ax = fVar.ax();
                                str6 = WebRequest.CONTENT_TYPE_HTML;
                                str7 = null;
                                str8 = "";
                            } else {
                                return;
                            }
                            loadDataWithBaseURL(ax, str5, str6, str7, str8);
                            return;
                        }
                        pVar = this.a;
                        str = "AdWebView";
                        str2 = "No companion ad provided.";
                    } else {
                        return;
                    }
                }
                pVar.b(str, str2);
            } catch (Throwable th) {
                this.a.b("AdWebView", "Unable to render AppLovinAd", th);
            }
        } else {
            p.j("AdWebView", "Ad can not be loaded in a destroyed webview");
        }
    }

    public void a(String str) {
        a(str, (Runnable) null);
    }

    public void a(String str, Runnable runnable) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("Forwarding \"");
            sb.append(str);
            sb.append("\" to ad template");
            this.a.b("AdWebView", sb.toString());
            loadUrl(str);
        } catch (Throwable th) {
            this.a.b("AdWebView", "Unable to forward to template", th);
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    public boolean a() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public AppLovinAd b() {
        return this.d;
    }

    public d c() {
        return this.c;
    }

    public void computeScroll() {
    }

    public void destroy() {
        this.e = true;
        try {
            super.destroy();
            this.a.b("AdWebView", "Web view destroyed");
        } catch (Throwable th) {
            if (this.a != null) {
                this.a.b("AdWebView", "destroy() threw exception", th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        try {
            super.onFocusChanged(z, i, rect);
        } catch (Exception e2) {
            this.a.b("AdWebView", "onFocusChanged() threw exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
    }

    public void onWindowFocusChanged(boolean z) {
        try {
            super.onWindowFocusChanged(z);
        } catch (Exception e2) {
            this.a.b("AdWebView", "onWindowFocusChanged() threw exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        try {
            super.onWindowVisibilityChanged(i);
        } catch (Exception e2) {
            this.a.b("AdWebView", "onWindowVisibilityChanged() threw exception", e2);
        }
    }

    public boolean requestFocus(int i, Rect rect) {
        try {
            return super.requestFocus(i, rect);
        } catch (Exception e2) {
            this.a.b("AdWebView", "requestFocus() threw exception", e2);
            return false;
        }
    }

    public void scrollTo(int i, int i2) {
    }
}
