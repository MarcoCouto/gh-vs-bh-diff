package com.applovin.impl.adview;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.text.TextUtils;
import com.applovin.adview.AppLovinInterstitialActivity;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.f.b;
import com.applovin.impl.sdk.ad.i;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class n implements AppLovinInterstitialAdDialog {
    public static volatile boolean b = false;
    public static volatile boolean c = false;
    private static final Map<String, n> d = Collections.synchronizedMap(new HashMap());
    private static volatile boolean n;
    protected final j a;
    private final String e;
    private final WeakReference<Context> f;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener g;
    private volatile AppLovinAdDisplayListener h;
    private volatile AppLovinAdVideoPlaybackListener i;
    private volatile AppLovinAdClickListener j;
    private volatile f k;
    private volatile b l;
    /* access modifiers changed from: private */
    public volatile j m;

    n(AppLovinSdk appLovinSdk, Context context) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (context != null) {
            this.a = q.a(appLovinSdk);
            this.e = UUID.randomUUID().toString();
            this.f = new WeakReference<>(context);
            b = true;
            c = false;
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    public static n a(String str) {
        return (n) d.get(str);
    }

    /* access modifiers changed from: private */
    public void a(final int i2) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (n.this.g != null) {
                    n.this.g.failedToReceiveAd(i2);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        Intent intent = new Intent(context, AppLovinInterstitialActivity.class);
        intent.putExtra(m.KEY_WRAPPER_ID, this.e);
        m.lastKnownWrapper = this;
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        if (context instanceof Activity) {
            try {
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(0, 0);
            } catch (Throwable th) {
                this.a.v().b("InterstitialAdDialogWrapper", "Unable to remove pending transition animations", th);
            }
        } else {
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        a(true);
    }

    private void a(f fVar, final Context context) {
        d.put(this.e, this);
        this.k = fVar;
        this.l = this.k.l();
        final long max = Math.max(0, ((Long) this.a.a(d.cS)).longValue());
        StringBuilder sb = new StringBuilder();
        sb.append("Presenting ad with delay of ");
        sb.append(max);
        this.a.v().b("InterstitialAdDialogWrapper", sb.toString());
        a(fVar, context, new Runnable() {
            public void run() {
                new Handler(context.getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        n.this.a(context);
                    }
                }, max);
            }
        });
    }

    private void a(f fVar, Context context, final Runnable runnable) {
        if (!TextUtils.isEmpty(fVar.K()) || !fVar.am() || h.a(context) || !(context instanceof Activity)) {
            runnable.run();
            return;
        }
        AlertDialog create = new Builder(context).setTitle(fVar.an()).setMessage(fVar.ao()).setPositiveButton(fVar.ap(), null).create();
        create.setOnDismissListener(new OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                runnable.run();
            }
        });
        create.show();
    }

    private void a(AppLovinAd appLovinAd) {
        if (this.h != null) {
            this.h.adHidden(appLovinAd);
        }
        n = false;
    }

    /* access modifiers changed from: private */
    public void b(final AppLovinAd appLovinAd) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (n.this.g != null) {
                    n.this.g.adReceived(appLovinAd);
                }
            }
        });
    }

    private Context h() {
        if (this.f != null) {
            return (Context) this.f.get();
        }
        return null;
    }

    public j a() {
        return this.a;
    }

    public void a(j jVar) {
        this.m = jVar;
    }

    /* access modifiers changed from: protected */
    public void a(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.a.o().loadNextAd(AppLovinAdSize.INTERSTITIAL, appLovinAdLoadListener);
    }

    public void a(boolean z) {
        n = z;
    }

    public AppLovinAd b() {
        return this.k;
    }

    public AppLovinAdVideoPlaybackListener c() {
        return this.i;
    }

    public AppLovinAdDisplayListener d() {
        return this.h;
    }

    public void dismiss() {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (n.this.m != null) {
                    n.this.m.dismiss();
                }
            }
        });
    }

    public AppLovinAdClickListener e() {
        return this.j;
    }

    public b f() {
        return this.l;
    }

    public void g() {
        b = false;
        c = true;
        d.remove(this.e);
        if (this.k != null && this.k.S()) {
            this.m = null;
        }
    }

    public boolean isAdReadyToDisplay() {
        return this.a.o().hasPreloadedAd(AppLovinAdSize.INTERSTITIAL);
    }

    public boolean isShowing() {
        return n;
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.j = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.h = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.g = appLovinAdLoadListener;
    }

    public void setAdVideoPlaybackListener(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        this.i = appLovinAdVideoPlaybackListener;
    }

    public void show() {
        show(null);
    }

    public void show(final String str) {
        a((AppLovinAdLoadListener) new AppLovinAdLoadListener() {
            public void adReceived(AppLovinAd appLovinAd) {
                n.this.b(appLovinAd);
                n.this.showAndRender(appLovinAd, str);
            }

            public void failedToReceiveAd(int i) {
                n.this.a(i);
            }
        });
    }

    public void showAndRender(AppLovinAd appLovinAd) {
        showAndRender(appLovinAd, null);
    }

    public void showAndRender(AppLovinAd appLovinAd, String str) {
        p pVar;
        String str2;
        String str3;
        if (!isShowing() || ((Boolean) this.a.a(d.eY)).booleanValue()) {
            Context h2 = h();
            if (h2 != null) {
                AppLovinAd a2 = q.a(appLovinAd, this.a);
                if (a2 != null) {
                    if (a2 instanceof f) {
                        a((f) a2, h2);
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to show interstitial: unknown ad type provided: '");
                        sb.append(a2);
                        sb.append("'");
                        this.a.v().e("InterstitialAdDialogWrapper", sb.toString());
                        a(a2);
                    }
                    return;
                }
                pVar = this.a.v();
                str3 = "InterstitialAdDialogWrapper";
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to show ad: ");
                sb2.append(appLovinAd);
                str2 = sb2.toString();
            } else {
                pVar = this.a.v();
                str3 = "InterstitialAdDialogWrapper";
                str2 = "Failed to show interstitial: stale activity reference provided";
            }
            pVar.e(str3, str2);
            a(appLovinAd);
            return;
        }
        String str4 = "Attempted to show an interstitial while one is already displayed; ignoring.";
        p.j("AppLovinInterstitialAdDialog", str4);
        if (this.h instanceof i) {
            ((i) this.h).onAdDisplayFailed(str4);
        }
    }

    public String toString() {
        return "AppLovinInterstitialAdDialog{}";
    }
}
