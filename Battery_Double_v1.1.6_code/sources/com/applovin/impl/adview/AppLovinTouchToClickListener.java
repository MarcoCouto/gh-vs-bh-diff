package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PointF;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;

public class AppLovinTouchToClickListener implements OnTouchListener {
    private final long a;
    private final int b;
    private final ClickRecognitionState c;
    private long d;
    private PointF e;
    private boolean f;
    private final Context g;
    private final OnClickListener h;

    public enum ClickRecognitionState {
        DISABLED,
        ACTION_DOWN,
        ACTION_POINTER_UP,
        ACTION_UP
    }

    public interface OnClickListener {
        void onClick(View view, PointF pointF);
    }

    public AppLovinTouchToClickListener(j jVar, Context context, OnClickListener onClickListener) {
        this.a = ((Long) jVar.a(d.aF)).longValue();
        this.b = ((Integer) jVar.a(d.aG)).intValue();
        this.c = ClickRecognitionState.values()[((Integer) jVar.a(d.aH)).intValue()];
        this.g = context;
        this.h = onClickListener;
    }

    private float a(float f2) {
        return f2 / this.g.getResources().getDisplayMetrics().density;
    }

    private float a(PointF pointF, PointF pointF2) {
        float f2 = pointF.x - pointF2.x;
        float f3 = pointF.y - pointF2.y;
        return a((float) Math.sqrt((double) ((f2 * f2) + (f3 * f3))));
    }

    private void a(View view, MotionEvent motionEvent) {
        this.h.onClick(view, new PointF(motionEvent.getRawX(), motionEvent.getRawY()));
        this.f = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0047, code lost:
        if (r0 < r8.a) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        if (r2 < ((float) r8.b)) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0080, code lost:
        if (r8.c == com.applovin.impl.adview.AppLovinTouchToClickListener.ClickRecognitionState.ACTION_POINTER_UP) goto L_0x005b;
     */
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 6) {
            switch (actionMasked) {
                case 0:
                    if (this.c != ClickRecognitionState.ACTION_DOWN) {
                        this.d = SystemClock.elapsedRealtime();
                        this.e = new PointF(motionEvent.getX(), motionEvent.getY());
                        this.f = false;
                        break;
                    }
                case 1:
                    if (this.f || this.c != ClickRecognitionState.ACTION_UP) {
                        if (this.c == ClickRecognitionState.DISABLED) {
                            long elapsedRealtime = SystemClock.elapsedRealtime() - this.d;
                            float a2 = a(this.e, new PointF(motionEvent.getX(), motionEvent.getY()));
                            if (!this.f) {
                                if (this.a >= 0) {
                                    break;
                                }
                                if (this.b >= 0) {
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }
        } else {
            if (!this.f) {
            }
            return true;
        }
        a(view, motionEvent);
        return true;
    }
}
