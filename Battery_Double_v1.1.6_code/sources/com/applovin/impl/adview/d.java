package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.net.Uri;
import android.net.http.SslError;
import android.view.ViewParent;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.a.a;
import com.applovin.impl.a.b;
import com.applovin.impl.a.i;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.AppLovinAdServiceImpl;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdService;
import com.appodeal.ads.AppodealNetworks;
import com.tapjoy.TapjoyConstants;
import java.util.List;

class d extends WebViewClient {
    private final j a;
    private final p b;
    private final AdViewControllerImpl c;

    public d(AdViewControllerImpl adViewControllerImpl, j jVar) {
        this.a = jVar;
        this.b = jVar.v();
        this.c = adViewControllerImpl;
    }

    private void a() {
        this.c.a();
    }

    private void a(PointF pointF) {
        this.c.expandAd(pointF);
    }

    private void a(Uri uri, c cVar) {
        p pVar;
        String str;
        String str2;
        try {
            String queryParameter = uri.getQueryParameter("n");
            if (n.b(queryParameter)) {
                String queryParameter2 = uri.getQueryParameter("load_type");
                if ("external".equalsIgnoreCase(queryParameter2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Loading new page externally: ");
                    sb.append(queryParameter);
                    this.b.b("AdWebView", sb.toString());
                    q.a(cVar.getContext(), Uri.parse(queryParameter), this.a);
                    com.applovin.impl.sdk.utils.j.c(this.c.getAdViewEventListener(), this.c.getCurrentAd(), this.c.getParentView());
                    return;
                } else if (TapjoyConstants.LOG_LEVEL_INTERNAL.equalsIgnoreCase(queryParameter2)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Loading new page in WebView: ");
                    sb2.append(queryParameter);
                    this.b.b("AdWebView", sb2.toString());
                    cVar.loadUrl(queryParameter);
                    String queryParameter3 = uri.getQueryParameter("bg_color");
                    if (n.b(queryParameter3)) {
                        cVar.setBackgroundColor(Color.parseColor(queryParameter3));
                        return;
                    }
                    return;
                } else {
                    pVar = this.b;
                    str = "AdWebView";
                    str2 = "Could not find load type in original uri";
                }
            } else {
                pVar = this.b;
                str = "AdWebView";
                str2 = "Could not find url to load from query in original uri";
            }
            pVar.e(str, str2);
        } catch (Throwable unused) {
            this.b.e("AdWebView", "Failed to load new page from query in original uri");
        }
    }

    private void a(a aVar, c cVar) {
        b j = aVar.j();
        if (j != null) {
            i.a(j.c(), this.c.getSdk());
            a(cVar, j.a());
        }
    }

    private void a(c cVar) {
        ViewParent parent = cVar.getParent();
        if (parent instanceof AppLovinAdView) {
            ((AppLovinAdView) parent).loadNextAd();
        }
    }

    private void a(c cVar, Uri uri) {
        AppLovinAd b2 = cVar.b();
        AppLovinAdView parentView = this.c.getParentView();
        if (parentView == null || b2 == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Attempting to track click that is null or not an ApplovinAdView instance for clickedUri = ");
            sb.append(uri);
            this.b.e("AdWebView", sb.toString());
            return;
        }
        com.applovin.impl.sdk.c.d c2 = cVar.c();
        if (c2 != null) {
            c2.b();
        }
        this.c.a(b2, parentView, uri, cVar.getAndClearLastClickLocation());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0176, code lost:
        if (r6.k() != false) goto L_0x00a9;
     */
    private boolean a(WebView webView, String str, boolean z) {
        String str2;
        String str3;
        p pVar;
        a aVar;
        StringBuilder sb = new StringBuilder();
        sb.append("Processing click on ad URL \"");
        sb.append(str);
        sb.append("\"");
        this.b.c("AdWebView", sb.toString());
        if (str != null && (webView instanceof c)) {
            Uri parse = Uri.parse(str);
            c cVar = (c) webView;
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            AppLovinAd currentAd = this.c.getCurrentAd();
            if (!AppodealNetworks.APPLOVIN.equals(scheme) || !"com.applovin.sdk".equals(host)) {
                if (!z) {
                    return false;
                }
                if (currentAd instanceof f) {
                    f fVar = (f) currentAd;
                    List aA = fVar.aA();
                    List aB = fVar.aB();
                    if ((aA.isEmpty() || aA.contains(scheme)) && (aB.isEmpty() || aB.contains(host))) {
                        if (currentAd instanceof a) {
                            aVar = (a) currentAd;
                        }
                        a(cVar, parse);
                    } else {
                        pVar = this.b;
                        str3 = "AdWebView";
                        str2 = "URL is not whitelisted - bypassing click";
                    }
                } else {
                    pVar = this.b;
                    str3 = "AdWebView";
                    str2 = "Bypassing click for ad of invalid type";
                }
                pVar.e(str3, str2);
            } else if (AppLovinAdService.URI_NEXT_AD.equals(path)) {
                a(cVar);
            } else if (AppLovinAdService.URI_CLOSE_AD.equals(path)) {
                a();
            } else if (AppLovinAdService.URI_EXPAND_AD.equals(path)) {
                a(cVar.getAndClearLastClickLocation());
            } else if (AppLovinAdService.URI_CONTRACT_AD.equals(path)) {
                b();
            } else if (AppLovinAdServiceImpl.URI_NO_OP.equals(path)) {
                return true;
            } else {
                if (AppLovinAdServiceImpl.URI_LOAD_URL.equals(path)) {
                    a(parse, cVar);
                } else if (AppLovinAdServiceImpl.URI_TRACK_CLICK_IMMEDIATELY.equals(path)) {
                    if (currentAd instanceof a) {
                        aVar = (a) currentAd;
                    } else {
                        a(cVar, Uri.parse(AppLovinAdServiceImpl.URI_TRACK_CLICK_IMMEDIATELY));
                    }
                } else if (path == null || !path.startsWith("/launch/")) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unknown URL: ");
                    sb2.append(str);
                    this.b.d("AdWebView", sb2.toString());
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Path: ");
                    sb3.append(path);
                    this.b.d("AdWebView", sb3.toString());
                } else {
                    List pathSegments = parse.getPathSegments();
                    if (pathSegments != null && pathSegments.size() > 1) {
                        String str4 = (String) pathSegments.get(pathSegments.size() - 1);
                        try {
                            Context context = webView.getContext();
                            context.startActivity(context.getPackageManager().getLaunchIntentForPackage(str4));
                            a(cVar, (Uri) null);
                        } catch (Throwable th) {
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("Threw Exception Trying to Launch App for Package: ");
                            sb4.append(str4);
                            this.b.b("AdWebView", sb4.toString(), th);
                        }
                    }
                }
            }
            a(aVar, cVar);
        }
        return true;
    }

    private void b() {
        this.c.contractAd();
    }

    public void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
        StringBuilder sb = new StringBuilder();
        sb.append("Loaded resource: ");
        sb.append(str);
        this.b.c("AdWebView", sb.toString());
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.c.onAdHtmlLoaded(webView);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        AppLovinAd currentAd = this.c.getCurrentAd();
        StringBuilder sb = new StringBuilder();
        sb.append("Received error with error code: ");
        sb.append(i);
        sb.append(" with description \\'");
        sb.append(str);
        sb.append("\\' for URL: ");
        sb.append(str2);
        String sb2 = sb.toString();
        if (currentAd instanceof AppLovinAdBase) {
            this.a.X().a((AppLovinAdBase) currentAd).a(com.applovin.impl.sdk.c.b.C, sb2).a();
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(sb2);
        sb3.append(" for ad: ");
        sb3.append(currentAd);
        this.b.e("AdWebView", sb3.toString());
    }

    @TargetApi(23)
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        onReceivedError(webView, webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
    }

    @TargetApi(21)
    public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
        super.onReceivedHttpError(webView, webResourceRequest, webResourceResponse);
        AppLovinAd currentAd = this.c.getCurrentAd();
        if (currentAd instanceof AppLovinAdBase) {
            this.a.X().a((AppLovinAdBase) currentAd).a(com.applovin.impl.sdk.c.b.D).a();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Received HTTP error: ");
        sb.append(webResourceResponse);
        sb.append("for url: ");
        sb.append(webResourceRequest.getUrl());
        sb.append(" and ad: ");
        sb.append(currentAd);
        this.b.e("AdWebView", sb.toString());
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        AppLovinAd currentAd = this.c.getCurrentAd();
        StringBuilder sb = new StringBuilder();
        sb.append("Received SSL error: ");
        sb.append(sslError);
        String sb2 = sb.toString();
        if (currentAd instanceof AppLovinAdBase) {
            this.a.X().a((AppLovinAdBase) currentAd).a(com.applovin.impl.sdk.c.b.E, sb2).a();
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(sb2);
        sb3.append(" for ad: ");
        sb3.append(currentAd);
        this.b.e("AdWebView", sb3.toString());
    }

    @TargetApi(24)
    public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        boolean hasGesture = ((Boolean) this.a.a(com.applovin.impl.sdk.b.d.cb)).booleanValue() ? webResourceRequest.hasGesture() : true;
        Uri url = webResourceRequest.getUrl();
        if (url != null) {
            return a(webView, url.toString(), hasGesture);
        }
        this.b.e("AdWebView", "No url found for request");
        return false;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return a(webView, str, true);
    }
}
