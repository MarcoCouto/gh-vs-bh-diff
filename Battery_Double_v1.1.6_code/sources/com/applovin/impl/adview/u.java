package com.applovin.impl.adview;

import android.content.Context;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;

public class u extends g {
    private static u a;

    public u(v vVar, Context context) {
        super(context);
        setBackgroundColor(0);
        WebSettings settings = getSettings();
        settings.setSupportMultipleWindows(false);
        settings.setJavaScriptEnabled(true);
        setWebViewClient(vVar);
        setWebChromeClient(new WebChromeClient());
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setScrollBarStyle(33554432);
    }

    public static u a(j jVar, v vVar, Context context) {
        if (!((Boolean) jVar.a(d.fe)).booleanValue()) {
            return new u(vVar, context);
        }
        if (a == null) {
            a = new u(vVar, context);
        } else {
            a.loadUrl("about:blank");
            a.clearHistory();
            a.setWebViewClient(vVar);
        }
        return a;
    }

    public void a(String str) {
        loadDataWithBaseURL("/", str, WebRequest.CONTENT_TYPE_HTML, null, "");
    }
}
