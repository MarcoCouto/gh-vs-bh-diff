package com.applovin.impl.adview;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;

class b extends WebChromeClient {
    private final p a;

    public b(j jVar) {
        this.a = jVar.v();
    }

    public void onConsoleMessage(String str, int i, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("console.log[");
        sb.append(i);
        sb.append("] :");
        sb.append(str);
        this.a.d("AdWebView", sb.toString());
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        StringBuilder sb = new StringBuilder();
        sb.append(consoleMessage.sourceId());
        sb.append(": ");
        sb.append(consoleMessage.lineNumber());
        sb.append(": ");
        sb.append(consoleMessage.message());
        this.a.b("AdWebView", sb.toString());
        return true;
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("Alert attempted: ");
        sb.append(str2);
        this.a.d("AdWebView", sb.toString());
        return true;
    }

    public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("JS onBeforeUnload attempted: ");
        sb.append(str2);
        this.a.d("AdWebView", sb.toString());
        return true;
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("JS confirm attempted: ");
        sb.append(str2);
        this.a.d("AdWebView", sb.toString());
        return true;
    }
}
