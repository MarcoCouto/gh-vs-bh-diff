package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.webkit.WebSettings.PluginState;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public final class w {
    private j a;
    private JSONObject b;

    public w(JSONObject jSONObject, j jVar) {
        this.a = jVar;
        this.b = jSONObject;
    }

    /* access modifiers changed from: 0000 */
    @TargetApi(21)
    public Integer a() {
        int i;
        String b2 = i.b(this.b, "mixed_content_mode", (String) null, this.a);
        if (n.b(b2)) {
            if ("always_allow".equalsIgnoreCase(b2)) {
                i = 0;
            } else if ("never_allow".equalsIgnoreCase(b2)) {
                i = 1;
            } else if ("compatibility_mode".equalsIgnoreCase(b2)) {
                i = 2;
            }
            return Integer.valueOf(i);
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public PluginState b() {
        String b2 = i.b(this.b, "plugin_state", (String) null, this.a);
        if (n.b(b2)) {
            if (String.SPLIT_VIEW_TRIGGER_ON.equalsIgnoreCase(b2)) {
                return PluginState.ON;
            }
            if ("on_demand".equalsIgnoreCase(b2)) {
                return PluginState.ON_DEMAND;
            }
            if ("off".equalsIgnoreCase(b2)) {
                return PluginState.OFF;
            }
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public Boolean c() {
        return i.a(this.b, "allow_file_access", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean d() {
        return i.a(this.b, "load_with_overview_mode", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean e() {
        return i.a(this.b, "use_wide_view_port", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean f() {
        return i.a(this.b, "allow_content_access", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean g() {
        return i.a(this.b, "use_built_in_zoom_controls", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean h() {
        return i.a(this.b, "display_zoom_controls", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean i() {
        return i.a(this.b, "save_form_data", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean j() {
        return i.a(this.b, "geolocation_enabled", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean k() {
        return i.a(this.b, "need_initial_focus", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean l() {
        return i.a(this.b, "allow_file_access_from_file_urls", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean m() {
        return i.a(this.b, "allow_universal_access_from_file_urls", (Boolean) null, this.a);
    }

    /* access modifiers changed from: 0000 */
    public Boolean n() {
        return i.a(this.b, "offscreen_pre_raster", (Boolean) null, this.a);
    }
}
