package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import com.applovin.impl.adview.AppLovinTouchToClickListener.OnClickListener;
import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.h;
import com.applovin.impl.sdk.ad.i;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.d.ac;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.d.y;
import com.applovin.impl.sdk.e;
import com.applovin.impl.sdk.e.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.AppKilledService;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.R;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class m extends Activity implements j, a {
    public static final String KEY_WRAPPER_ID = "com.applovin.interstitial.wrapper_id";
    public static volatile n lastKnownWrapper;
    private boolean A;
    private final Handler B = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final Handler C = new Handler(Looper.getMainLooper());
    private FrameLayout D;
    /* access modifiers changed from: private */
    public h E;
    /* access modifiers changed from: private */
    public View F;
    /* access modifiers changed from: private */
    public h G;
    /* access modifiers changed from: private */
    public View H;
    /* access modifiers changed from: private */
    public f I;
    private ImageView J;
    /* access modifiers changed from: private */
    public WeakReference<MediaPlayer> K = new WeakReference<>(null);
    private b L;
    /* access modifiers changed from: private */
    public u M;
    /* access modifiers changed from: private */
    public ProgressBar N;
    private v.a O;
    private a P;
    private o Q;
    private com.applovin.impl.sdk.utils.a R;
    private BroadcastReceiver S;
    /* access modifiers changed from: private */
    public l a;
    /* access modifiers changed from: private */
    public n b;
    private volatile boolean c = false;
    protected int computedLengthSeconds = 0;
    protected i countdownManager;
    public volatile f currentAd;
    /* access modifiers changed from: private */
    public d d;
    private volatile boolean e = false;
    /* access modifiers changed from: private */
    public volatile boolean f = false;
    /* access modifiers changed from: private */
    public volatile boolean g = false;
    private volatile boolean h = false;
    private volatile boolean i = false;
    /* access modifiers changed from: private */
    public volatile boolean j = false;
    /* access modifiers changed from: private */
    public volatile boolean k = false;
    private boolean l = false;
    public p logger;
    private volatile boolean m = false;
    private boolean n = true;
    /* access modifiers changed from: private */
    public boolean o = false;
    private long p = 0;
    protected volatile boolean postitialWasDisplayed = false;
    /* access modifiers changed from: private */
    public long q = 0;
    /* access modifiers changed from: private */
    public long r = 0;
    /* access modifiers changed from: private */
    public long s = 0;
    public j sdk;
    /* access modifiers changed from: private */
    public long t = -2;
    private int u = 0;
    private int v = Integer.MIN_VALUE;
    protected volatile boolean videoMuted = false;
    public t videoView;
    private AtomicBoolean w = new AtomicBoolean(false);
    private AtomicBoolean x = new AtomicBoolean(false);
    private AtomicBoolean y = new AtomicBoolean(false);
    private int z = e.a;

    /* access modifiers changed from: private */
    public void A() {
        if (C()) {
            M();
            pauseReportRewardTask();
            this.logger.b("InterActivity", "Prompting incentivized ad close warning");
            this.L.b();
            return;
        }
        skipVideo();
    }

    /* access modifiers changed from: private */
    public void B() {
        if (this.currentAd.X()) {
            c adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView();
            if (adWebView != null) {
                adWebView.a("javascript:al_onCloseButtonTapped();");
            }
        }
        if (D()) {
            this.logger.b("InterActivity", "Prompting incentivized non-video ad close warning");
            this.L.c();
            return;
        }
        dismiss();
    }

    private boolean C() {
        return G() && !isFullyWatched() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.bQ)).booleanValue() && this.L != null;
    }

    private boolean D() {
        return H() && !F() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.bV)).booleanValue() && this.L != null;
    }

    private int E() {
        if (!(this.currentAd instanceof com.applovin.impl.sdk.ad.a)) {
            return 0;
        }
        float h2 = ((com.applovin.impl.sdk.ad.a) this.currentAd).h();
        if (h2 <= 0.0f) {
            h2 = this.currentAd.n();
        }
        double a2 = q.a(System.currentTimeMillis() - this.p);
        double d2 = (double) h2;
        Double.isNaN(d2);
        return (int) Math.min((a2 / d2) * 100.0d, 100.0d);
    }

    private boolean F() {
        return E() >= this.currentAd.T();
    }

    private boolean G() {
        return AppLovinAdType.INCENTIVIZED.equals(this.currentAd.getType());
    }

    private boolean H() {
        return !this.currentAd.hasVideoUrl() && G();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0066, code lost:
        if (r0 > 0) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0098, code lost:
        if (r0 > 0) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009a, code lost:
        r0 = java.util.concurrent.TimeUnit.SECONDS.toMillis((long) r0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0061  */
    public void I() {
        long j2;
        int i2;
        long millis;
        long j3;
        if (this.currentAd != null) {
            long j4 = 0;
            if ((this.currentAd.ah() >= 0 || this.currentAd.ai() >= 0) && this.Q == null) {
                if (this.currentAd.ah() >= 0) {
                    j2 = this.currentAd.ah();
                } else {
                    if (isVastAd()) {
                        com.applovin.impl.a.a aVar = (com.applovin.impl.a.a) this.currentAd;
                        com.applovin.impl.a.j h2 = aVar.h();
                        if (h2 == null || h2.b() <= 0) {
                            int duration = this.videoView.getDuration();
                            if (duration > 0) {
                                j3 = (long) duration;
                            }
                            if (aVar.aj()) {
                                i2 = (int) aVar.n();
                            }
                            double d2 = (double) j4;
                            double ai = (double) this.currentAd.ai();
                            Double.isNaN(ai);
                            double d3 = ai / 100.0d;
                            Double.isNaN(d2);
                            j2 = (long) (d2 * d3);
                        } else {
                            j3 = TimeUnit.SECONDS.toMillis((long) h2.b());
                        }
                        j4 = 0 + j3;
                        if (aVar.aj()) {
                        }
                        double d22 = (double) j4;
                        double ai2 = (double) this.currentAd.ai();
                        Double.isNaN(ai2);
                        double d32 = ai2 / 100.0d;
                        Double.isNaN(d22);
                        j2 = (long) (d22 * d32);
                    } else {
                        if (this.currentAd instanceof com.applovin.impl.sdk.ad.a) {
                            com.applovin.impl.sdk.ad.a aVar2 = (com.applovin.impl.sdk.ad.a) this.currentAd;
                            int duration2 = this.videoView.getDuration();
                            if (duration2 > 0) {
                                j4 = 0 + ((long) duration2);
                            }
                            if (aVar2.aj()) {
                                int h3 = (int) aVar2.h();
                                if (h3 > 0) {
                                    millis = TimeUnit.SECONDS.toMillis((long) h3);
                                } else {
                                    i2 = (int) aVar2.n();
                                }
                            }
                        }
                        double d222 = (double) j4;
                        double ai22 = (double) this.currentAd.ai();
                        Double.isNaN(ai22);
                        double d322 = ai22 / 100.0d;
                        Double.isNaN(d222);
                        j2 = (long) (d222 * d322);
                    }
                    j4 += millis;
                    double d2222 = (double) j4;
                    double ai222 = (double) this.currentAd.ai();
                    Double.isNaN(ai222);
                    double d3222 = ai222 / 100.0d;
                    Double.isNaN(d2222);
                    j2 = (long) (d2222 * d3222);
                }
                StringBuilder sb = new StringBuilder();
                sb.append("Scheduling report reward in ");
                sb.append(TimeUnit.MILLISECONDS.toSeconds(j2));
                sb.append(" seconds...");
                this.logger.b("InterActivity", sb.toString());
                this.Q = o.a(j2, this.sdk, new Runnable() {
                    public void run() {
                        if (m.this.currentAd != null && !m.this.currentAd.ak().getAndSet(true)) {
                            m.this.sdk.K().a((com.applovin.impl.sdk.d.a) new y(m.this.currentAd, m.this.sdk), r.a.REWARD);
                        }
                    }
                });
            }
        }
    }

    private void J() {
        p pVar;
        String str;
        StringBuilder sb;
        String str2;
        if (this.a != null) {
            this.a.setAdDisplayListener(new AppLovinAdDisplayListener() {
                public void adDisplayed(AppLovinAd appLovinAd) {
                    if (!m.this.f) {
                        m.this.a(appLovinAd);
                    }
                }

                public void adHidden(AppLovinAd appLovinAd) {
                    m.this.b(appLovinAd);
                }
            });
            this.a.setAdClickListener(new AppLovinAdClickListener() {
                public void adClicked(AppLovinAd appLovinAd) {
                    com.applovin.impl.sdk.utils.j.a(m.this.b.e(), appLovinAd);
                }
            });
            this.currentAd = (f) this.b.b();
            if (this.x.compareAndSet(false, true)) {
                this.sdk.o().trackImpression(this.currentAd);
                this.currentAd.setHasShown(true);
            }
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            this.D = new FrameLayout(this);
            this.D.setLayoutParams(layoutParams);
            this.D.setBackgroundColor(this.currentAd.C());
            this.countdownManager = new i(this.B, this.sdk);
            j();
            if (this.currentAd.isVideoAd()) {
                this.m = this.currentAd.b();
                if (this.m) {
                    pVar = this.logger;
                    str = "InterActivity";
                    sb = new StringBuilder();
                    str2 = "Preparing stream for ";
                } else {
                    pVar = this.logger;
                    str = "InterActivity";
                    sb = new StringBuilder();
                    str2 = "Preparing cached video playback for ";
                }
                sb.append(str2);
                sb.append(this.currentAd.d());
                pVar.b(str, sb.toString());
                if (this.d != null) {
                    this.d.b(this.m ? 1 : 0);
                }
            }
            this.videoMuted = i();
            Uri d2 = this.currentAd.d();
            a(d2);
            if (d2 == null) {
                I();
            }
            this.E.bringToFront();
            if (n() && this.F != null) {
                this.F.bringToFront();
            }
            if (this.G != null) {
                this.G.bringToFront();
            }
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ff)).booleanValue()) {
                this.D.addView(this.a);
                this.a.setVisibility(4);
            }
            this.a.renderAd(this.currentAd);
            this.b.a(true);
            if (!this.currentAd.hasVideoUrl()) {
                if (H() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ca)).booleanValue()) {
                    d((AppLovinAd) this.currentAd);
                }
                showPostitial();
                return;
            }
            return;
        }
        exitWithError("AdView was null");
    }

    private void K() {
        if (this.videoView != null) {
            this.u = getVideoPercentViewed();
            this.videoView.stopPlayback();
        }
    }

    private boolean L() {
        return this.videoMuted;
    }

    private void M() {
        this.sdk.a(com.applovin.impl.sdk.b.f.v, Integer.valueOf(this.videoView != null ? this.videoView.getCurrentPosition() : 0));
        this.sdk.a(com.applovin.impl.sdk.b.f.w, Boolean.valueOf(true));
        try {
            this.countdownManager.c();
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to pause countdown timers", th);
        }
        if (this.videoView != null) {
            this.videoView.pause();
        }
    }

    private void N() {
        long max = Math.max(0, ((Long) this.sdk.a(com.applovin.impl.sdk.b.d.dl)).longValue());
        if (max > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Resuming video with delay of ");
            sb.append(max);
            this.sdk.v().b("InterActivity", sb.toString());
            this.C.postDelayed(new Runnable() {
                public void run() {
                    m.this.O();
                }
            }, max);
            return;
        }
        this.sdk.v().b("InterActivity", "Resuming video immediately");
        O();
    }

    /* access modifiers changed from: private */
    public void O() {
        if (!this.postitialWasDisplayed && this.videoView != null && !this.videoView.isPlaying()) {
            this.videoView.seekTo(((Integer) this.sdk.b(com.applovin.impl.sdk.b.f.v, Integer.valueOf(this.videoView.getDuration()))).intValue());
            this.videoView.start();
            this.countdownManager.a();
        }
    }

    private void P() {
        if (!this.i) {
            try {
                int videoPercentViewed = getVideoPercentViewed();
                if (this.currentAd.hasVideoUrl()) {
                    a((AppLovinAd) this.currentAd, (double) videoPercentViewed, isFullyWatched());
                    if (this.d != null) {
                        this.d.c((long) videoPercentViewed);
                    }
                } else if ((this.currentAd instanceof com.applovin.impl.sdk.ad.a) && H() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ca)).booleanValue()) {
                    int E2 = E();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Rewarded playable engaged at ");
                    sb.append(E2);
                    sb.append(" percent");
                    this.logger.b("InterActivity", sb.toString());
                    a((AppLovinAd) this.currentAd, (double) E2, E2 >= this.currentAd.T());
                }
                this.sdk.o().trackVideoEnd(this.currentAd, TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - this.p), videoPercentViewed, this.m);
                this.sdk.o().trackFullScreenAdClosed(this.currentAd, SystemClock.elapsedRealtime() - this.r, this.t, this.A, this.z);
            } catch (Throwable th) {
                if (this.logger != null) {
                    this.logger.b("InterActivity", "Failed to notify end listener.", th);
                }
            }
        }
    }

    private int a(int i2) {
        return AppLovinSdkUtils.dpToPx(this, i2);
    }

    private int a(int i2, boolean z2) {
        if (z2) {
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 1) {
                return 9;
            }
            if (i2 == 2) {
                return 8;
            }
            if (i2 == 3) {
                return 1;
            }
        } else if (i2 == 0) {
            return 1;
        } else {
            if (i2 == 1) {
                return 0;
            }
            if (i2 == 2) {
                return 9;
            }
            if (i2 == 3) {
                return 8;
            }
        }
        return -1;
    }

    private void a(long j2, final h hVar) {
        this.C.postDelayed(new Runnable() {
            public void run() {
                if (hVar.equals(m.this.E)) {
                    m.this.m();
                } else if (hVar.equals(m.this.G)) {
                    m.this.o();
                }
            }
        }, j2);
    }

    /* access modifiers changed from: private */
    public void a(PointF pointF) {
        if (!this.currentAd.u() || this.currentAd.g() == null) {
            e();
            f();
            return;
        }
        this.sdk.v().b("InterActivity", "Clicking through video...");
        clickThroughFromVideo(pointF);
    }

    private void a(Uri uri) {
        this.videoView = this.currentAd.aC() ? new p(this.sdk, this, new a() {
            public void a(String str) {
                m.this.handleMediaError(str);
            }
        }) : new AppLovinVideoView(this, this.sdk);
        if (uri != null) {
            this.videoView.setOnPreparedListener(new OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {
                    m.this.K = new WeakReference(mediaPlayer);
                    boolean c = m.this.i();
                    float f = c ^ true ? 1.0f : 0.0f;
                    mediaPlayer.setVolume(f, f);
                    if (m.this.d != null) {
                        m.this.d.e(c ? 1 : 0);
                    }
                    int videoWidth = mediaPlayer.getVideoWidth();
                    int videoHeight = mediaPlayer.getVideoHeight();
                    m.this.computedLengthSeconds = (int) TimeUnit.MILLISECONDS.toSeconds((long) mediaPlayer.getDuration());
                    m.this.videoView.setVideoSize(videoWidth, videoHeight);
                    if (m.this.videoView instanceof AppLovinVideoView) {
                        SurfaceHolder holder = ((AppLovinVideoView) m.this.videoView).getHolder();
                        if (holder.getSurface() != null) {
                            mediaPlayer.setDisplay(holder);
                        }
                    }
                    mediaPlayer.setOnErrorListener(new OnErrorListener() {
                        public boolean onError(MediaPlayer mediaPlayer, final int i, final int i2) {
                            m.this.C.post(new Runnable() {
                                public void run() {
                                    m mVar = m.this;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Media player error (");
                                    sb.append(i);
                                    sb.append(",");
                                    sb.append(i2);
                                    sb.append(")");
                                    mVar.handleMediaError(sb.toString());
                                }
                            });
                            return true;
                        }
                    });
                    mediaPlayer.setOnInfoListener(new OnInfoListener() {
                        /* JADX WARNING: Code restructure failed: missing block: B:9:0x002e, code lost:
                            if (r2 == 702) goto L_0x0003;
                         */
                        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                            if (i != 3) {
                                if (i == 701) {
                                    m.this.y();
                                    if (m.this.d != null) {
                                        m.this.d.h();
                                    }
                                }
                                return false;
                            }
                            m.this.z();
                            return false;
                        }
                    });
                    if (m.this.q == 0) {
                        m.this.q();
                        m.this.k();
                        m.this.v();
                        m.this.u();
                        m.this.playVideo();
                        m.this.I();
                    }
                }
            });
            this.videoView.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    m.this.h();
                }
            });
            this.videoView.setOnErrorListener(new OnErrorListener() {
                public boolean onError(MediaPlayer mediaPlayer, final int i, final int i2) {
                    m.this.C.post(new Runnable() {
                        public void run() {
                            m mVar = m.this;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Video view error (");
                            sb.append(i);
                            sb.append(",");
                            sb.append(i2);
                            mVar.handleMediaError(sb.toString());
                        }
                    });
                    return true;
                }
            });
            ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            this.videoView.setVideoURI(uri);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
        this.videoView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1, 17));
        this.videoView.setOnTouchListener(new AppLovinTouchToClickListener(this.sdk, this, new OnClickListener() {
            public void onClick(View view, PointF pointF) {
                m.this.a(pointF);
            }
        }));
        this.D.addView((View) this.videoView);
        setContentView(this.D);
        p();
        x();
    }

    /* access modifiers changed from: private */
    public void a(final View view, final boolean z2, long j2) {
        float f2 = 1.0f;
        float f3 = z2 ? 0.0f : 1.0f;
        if (!z2) {
            f2 = 0.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f2);
        alphaAnimation.setDuration(j2);
        alphaAnimation.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (!z2) {
                    view.setVisibility(4);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                view.setVisibility(0);
            }
        });
        view.startAnimation(alphaAnimation);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        com.applovin.impl.sdk.utils.j.a(this.b.d(), appLovinAd);
        this.f = true;
        this.sdk.Z().c();
        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
            public void run() {
                m.this.b(m.this.videoMuted);
            }
        }, ((Long) this.sdk.a(com.applovin.impl.sdk.b.d.dp)).longValue());
    }

    private void a(AppLovinAd appLovinAd, double d2, boolean z2) {
        this.i = true;
        com.applovin.impl.sdk.utils.j.a(this.b.c(), appLovinAd, d2, z2);
    }

    private void a(final String str) {
        if (this.b != null) {
            final AppLovinAdDisplayListener d2 = this.b.d();
            if ((d2 instanceof i) && this.y.compareAndSet(false, true)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        ((i) d2).onAdDisplayFailed(str);
                    }
                });
            }
        }
    }

    private void a(final String str, long j2) {
        if (j2 >= 0) {
            this.C.postDelayed(new Runnable() {
                public void run() {
                    c adWebView = ((AdViewControllerImpl) m.this.a.getAdViewController()).getAdWebView();
                    if (adWebView != null && n.b(str)) {
                        adWebView.a(str);
                    }
                }
            }, j2);
        }
    }

    private void a(boolean z2) {
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cY)).booleanValue() && g.d()) {
            AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) getDrawable(z2 ? R.drawable.unmute_to_mute : R.drawable.mute_to_unmute);
            if (animatedVectorDrawable != null) {
                this.J.setScaleType(ScaleType.FIT_XY);
                this.J.setImageDrawable(animatedVectorDrawable);
                animatedVectorDrawable.start();
                return;
            }
        }
        Uri aF = z2 ? this.currentAd.aF() : this.currentAd.aG();
        int a2 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.de)).intValue());
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        AppLovinSdkUtils.safePopulateImageView(this.J, aF, a2);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    private boolean a() {
        int identifier = getResources().getIdentifier((String) this.sdk.a(com.applovin.impl.sdk.b.d.cV), "bool", "android");
        return identifier > 0 && getResources().getBoolean(identifier);
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    public void b() {
        getWindow().getDecorView().setSystemUiVisibility(5894);
    }

    private void b(int i2) {
        try {
            setRequestedOrientation(i2);
        } catch (Throwable th) {
            this.sdk.v().b("InterActivity", "Failed to set requested orientation", th);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0057, code lost:
        if (r7 == 2) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005a, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0063, code lost:
        if (r7 == 1) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        if (r7 == 1) goto L_0x002d;
     */
    private void b(int i2, boolean z2) {
        int i3;
        boolean booleanValue = ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cT)).booleanValue();
        if (this.b.f() == f.b.ACTIVITY_PORTRAIT) {
            i3 = 9;
            if (z2) {
                if (i2 == 1 || i2 == 3) {
                    if (!booleanValue) {
                        return;
                    }
                }
            } else if (i2 == 0 || i2 == 2) {
                if (!booleanValue) {
                    return;
                }
                if (i2 == 0) {
                    i3 = 1;
                }
            }
            this.c = true;
            b(1);
            return;
        } else if (this.b.f() == f.b.ACTIVITY_LANDSCAPE) {
            i3 = 8;
            if (z2) {
                if (i2 == 0 || i2 == 2) {
                    if (!booleanValue) {
                        return;
                    }
                }
            } else if (i2 == 1 || i2 == 3) {
                if (!booleanValue) {
                    return;
                }
            }
            this.c = true;
            b(0);
            return;
        } else {
            return;
        }
        b(i3);
    }

    /* access modifiers changed from: private */
    public void b(AppLovinAd appLovinAd) {
        dismiss();
        c(appLovinAd);
    }

    private void b(String str) {
        f fVar = this.currentAd;
        if (fVar != null && fVar.Z()) {
            a(str, 0);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (this.currentAd.W()) {
            c adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView();
            if (adWebView != null) {
                try {
                    adWebView.a(z2 ? "javascript:al_mute();" : "javascript:al_unmute();");
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Unable to forward mute setting to template.", th);
                }
            }
        }
    }

    private void c(AppLovinAd appLovinAd) {
        if (!this.g) {
            this.g = true;
            if (this.b != null) {
                com.applovin.impl.sdk.utils.j.b(this.b.d(), appLovinAd);
            }
            this.sdk.Z().d();
        }
    }

    private void c(boolean z2) {
        this.videoMuted = z2;
        MediaPlayer mediaPlayer = (MediaPlayer) this.K.get();
        if (mediaPlayer != null) {
            float f2 = z2 ^ true ? 1.0f : 0.0f;
            try {
                mediaPlayer.setVolume(f2, f2);
            } catch (IllegalStateException e2) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to set MediaPlayer muted: ");
                sb.append(z2);
                this.logger.b("InterActivity", sb.toString(), e2);
            }
        }
    }

    private boolean c() {
        boolean z2 = true;
        if (!(this.b == null || this.sdk == null)) {
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cN)).booleanValue()) {
                return true;
            }
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cO)).booleanValue() && this.j) {
                return true;
            }
            if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cP)).booleanValue() || !this.postitialWasDisplayed) {
                z2 = false;
            }
        }
        return z2;
    }

    @SuppressLint({"WrongConstant"})
    private void d() {
        if (this.sdk == null || !isFinishing()) {
            if (!(this.currentAd == null || this.v == Integer.MIN_VALUE)) {
                b(this.v);
            }
            finish();
        }
    }

    private void d(AppLovinAd appLovinAd) {
        if (!this.h) {
            this.h = true;
            com.applovin.impl.sdk.utils.j.a(this.b.c(), appLovinAd);
        }
    }

    private void e() {
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cW)).booleanValue() && this.I != null && this.I.getVisibility() != 8) {
            a((View) this.I, this.I.getVisibility() == 4, 750);
        }
    }

    private void f() {
        s t2 = this.currentAd.t();
        if (t2 != null && t2.e() && !this.postitialWasDisplayed && this.M != null) {
            a((View) this.M, this.M.getVisibility() == 4, t2.f());
        }
    }

    private void g() {
        if (this.sdk != null) {
            this.sdk.a(com.applovin.impl.sdk.b.f.w, Boolean.valueOf(false));
            this.sdk.a(com.applovin.impl.sdk.b.f.v, Integer.valueOf(0));
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        this.e = true;
        showPostitial();
    }

    /* access modifiers changed from: private */
    public boolean i() {
        return ((Integer) this.sdk.b(com.applovin.impl.sdk.b.f.v, Integer.valueOf(0))).intValue() > 0 ? this.videoMuted : ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.dd)).booleanValue() ? this.sdk.l().isMuted() : ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.db)).booleanValue();
    }

    private void j() {
        this.E = h.a(this.sdk, this, this.currentAd.o());
        this.E.setVisibility(8);
        this.E.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                m.this.B();
            }
        });
        int a2 = a(this.currentAd.aa());
        int i2 = 3;
        int i3 = (this.currentAd.ad() ? 3 : 5) | 48;
        if (!this.currentAd.ae()) {
            i2 = 5;
        }
        int i4 = i2 | 48;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, i3 | 48);
        this.E.a(a2);
        int a3 = a(this.currentAd.ab());
        int a4 = a(this.currentAd.ac());
        layoutParams.setMargins(a4, a3, a4, a3);
        this.D.addView(this.E, layoutParams);
        this.G = h.a(this.sdk, this, this.currentAd.p());
        this.G.setVisibility(8);
        this.G.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                m.this.A();
            }
        });
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(a2, a2, i4);
        layoutParams2.setMargins(a4, a3, a4, a3);
        this.G.a(a2);
        this.D.addView(this.G, layoutParams2);
        this.G.bringToFront();
        if (n()) {
            int a5 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cc)).intValue());
            this.F = new View(this);
            this.F.setBackgroundColor(0);
            this.F.setVisibility(8);
            this.H = new View(this);
            this.H.setBackgroundColor(0);
            this.H.setVisibility(8);
            int i5 = a2 + a5;
            int a6 = a3 - a(5);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(i5, i5, i3);
            layoutParams3.setMargins(a6, a6, a6, a6);
            FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(i5, i5, i4);
            layoutParams4.setMargins(a6, a6, a6, a6);
            this.F.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    m.this.E.performClick();
                }
            });
            this.H.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    m.this.G.performClick();
                }
            });
            this.D.addView(this.F, layoutParams3);
            this.F.bringToFront();
            this.D.addView(this.H, layoutParams4);
            this.H.bringToFront();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.J == null) {
            try {
                this.videoMuted = i();
                this.J = new ImageView(this);
                if (!l()) {
                    int a2 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.de)).intValue());
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.dg)).intValue());
                    this.J.setScaleType(ScaleType.FIT_CENTER);
                    int a3 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.df)).intValue());
                    layoutParams.setMargins(a3, a3, a3, a3);
                    if ((this.videoMuted ? this.currentAd.aF() : this.currentAd.aG()) != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Added mute button with params: ");
                        sb.append(layoutParams);
                        this.sdk.v().b("InterActivity", sb.toString());
                        a(this.videoMuted);
                        this.J.setClickable(true);
                        this.J.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                m.this.toggleMute();
                            }
                        });
                        this.D.addView(this.J, layoutParams);
                        this.J.bringToFront();
                        return;
                    }
                    this.sdk.v().e("InterActivity", "Attempting to add mute button but could not find uri");
                    return;
                }
                this.sdk.v().b("InterActivity", "Mute button should be hidden");
            } catch (Exception e2) {
                this.sdk.v().a("InterActivity", "Failed to attach mute button", (Throwable) e2);
            }
        }
    }

    private boolean l() {
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cZ)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.da)).booleanValue() || i()) {
            return false;
        }
        return !((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.dc)).booleanValue();
    }

    /* access modifiers changed from: private */
    public void m() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (m.this.j) {
                        m.this.E.setVisibility(0);
                        return;
                    }
                    m.this.r = SystemClock.elapsedRealtime();
                    m.this.j = true;
                    if (m.this.n() && m.this.F != null) {
                        m.this.F.setVisibility(0);
                        m.this.F.bringToFront();
                    }
                    m.this.E.setVisibility(0);
                    m.this.E.bringToFront();
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setDuration((long) ((Integer) m.this.sdk.a(com.applovin.impl.sdk.b.d.cF)).intValue());
                    alphaAnimation.setRepeatCount(0);
                    m.this.E.startAnimation(alphaAnimation);
                } catch (Throwable unused) {
                    m.this.dismiss();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean n() {
        return ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cc)).intValue() > 0;
    }

    /* access modifiers changed from: private */
    public void o() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (!m.this.k && m.this.G != null) {
                        m.this.t = -1;
                        m.this.s = SystemClock.elapsedRealtime();
                        m.this.k = true;
                        m.this.G.setVisibility(0);
                        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                        alphaAnimation.setDuration((long) ((Integer) m.this.sdk.a(com.applovin.impl.sdk.b.d.cF)).intValue());
                        alphaAnimation.setRepeatCount(0);
                        m.this.G.startAnimation(alphaAnimation);
                        if (m.this.n() && m.this.H != null) {
                            m.this.H.setVisibility(0);
                            m.this.H.bringToFront();
                        }
                    }
                } catch (Throwable th) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable to show skip button: ");
                    sb.append(th);
                    m.this.logger.d("InterActivity", sb.toString());
                }
            }
        });
    }

    private void p() {
        if (this.currentAd.m() >= 0.0f) {
            a(q.b(this.currentAd.m()), (!this.l || this.G == null) ? this.E : this.G);
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        boolean z2 = ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cM)).booleanValue() && t() > 0;
        if (this.I == null && z2) {
            this.I = new f(this);
            int B2 = this.currentAd.B();
            this.I.setTextColor(B2);
            this.I.setTextSize((float) ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cL)).intValue());
            this.I.setFinishedStrokeColor(B2);
            this.I.setFinishedStrokeWidth((float) ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cK)).intValue());
            this.I.setMax(t());
            this.I.setProgress(t());
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cJ)).intValue()), a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cJ)).intValue()), ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cI)).intValue());
            int a2 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cH)).intValue());
            layoutParams.setMargins(a2, a2, a2, a2);
            this.D.addView(this.I, layoutParams);
            this.I.bringToFront();
            this.I.setVisibility(0);
            final long s2 = s();
            this.countdownManager.a("COUNTDOWN_CLOCK", 1000, (a) new a() {
                public void a() {
                    if (m.this.I != null) {
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(s2 - ((long) m.this.videoView.getCurrentPosition()));
                        if (seconds <= 0) {
                            m.this.I.setVisibility(8);
                            m.this.o = true;
                        } else if (m.this.r()) {
                            m.this.I.setProgress((int) seconds);
                        }
                    }
                }

                public boolean b() {
                    return m.this.r();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public boolean r() {
        return !this.o && !this.postitialWasDisplayed && this.videoView.isPlaying();
    }

    private long s() {
        return TimeUnit.SECONDS.toMillis((long) t());
    }

    private int t() {
        int A2 = this.currentAd.A();
        if (A2 > 0) {
            return A2;
        }
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.f0do)).booleanValue()) {
            A2 = this.computedLengthSeconds + 1;
        }
        return A2;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public void u() {
        if (this.N == null && this.currentAd.L()) {
            this.logger.c("InterActivity", "Attaching video progress bar...");
            this.N = new ProgressBar(this, null, 16842872);
            this.N.setMax(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.dj)).intValue());
            this.N.setPadding(0, 0, 0, 0);
            if (g.d()) {
                try {
                    this.N.setProgressTintList(ColorStateList.valueOf(this.currentAd.M()));
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Unable to update progress bar color.", th);
                }
            }
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.videoView.getWidth(), 20, 80);
            layoutParams.setMargins(0, 0, 0, ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.dk)).intValue());
            this.D.addView(this.N, layoutParams);
            this.N.bringToFront();
            this.countdownManager.a("PROGRESS_BAR", ((Long) this.sdk.a(com.applovin.impl.sdk.b.d.di)).longValue(), (a) new a() {
                public void a() {
                    if (m.this.N == null) {
                        return;
                    }
                    if (m.this.shouldContinueFullLengthVideoCountdown()) {
                        m.this.N.setProgress((int) ((((float) m.this.videoView.getCurrentPosition()) / ((float) m.this.videoView.getDuration())) * ((float) ((Integer) m.this.sdk.a(com.applovin.impl.sdk.b.d.dj)).intValue())));
                        return;
                    }
                    m.this.N.setVisibility(8);
                }

                public boolean b() {
                    return m.this.shouldContinueFullLengthVideoCountdown();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        final s t2 = this.currentAd.t();
        if (n.b(this.currentAd.s()) && t2 != null && this.M == null) {
            this.logger.c("InterActivity", "Attaching video button...");
            this.M = w();
            double a2 = (double) t2.a();
            Double.isNaN(a2);
            double d2 = a2 / 100.0d;
            double b2 = (double) t2.b();
            Double.isNaN(b2);
            double d3 = b2 / 100.0d;
            int width = this.videoView.getWidth();
            int height = this.videoView.getHeight();
            double d4 = (double) width;
            Double.isNaN(d4);
            int i2 = (int) (d2 * d4);
            double d5 = (double) height;
            Double.isNaN(d5);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i2, (int) (d3 * d5), t2.d());
            int a3 = a(t2.c());
            layoutParams.setMargins(a3, a3, a3, a3);
            this.D.addView(this.M, layoutParams);
            this.M.bringToFront();
            if (t2.i() > 0.0f) {
                this.M.setVisibility(4);
                this.C.postDelayed(new Runnable() {
                    public void run() {
                        m.this.a((View) m.this.M, true, t2.g());
                        m.this.M.bringToFront();
                    }
                }, q.b(t2.i()));
            }
            if (t2.j() > 0.0f) {
                this.C.postDelayed(new Runnable() {
                    public void run() {
                        m.this.a((View) m.this.M, false, t2.h());
                    }
                }, q.b(t2.j()));
            }
        }
    }

    private u w() {
        StringBuilder sb = new StringBuilder();
        sb.append("Create video button with HTML = ");
        sb.append(this.currentAd.s());
        this.logger.b("InterActivity", sb.toString());
        v vVar = new v(this.sdk);
        this.O = new v.a() {
            public void a(u uVar) {
                m.this.logger.b("InterActivity", "Clicking through from video button...");
                m.this.clickThroughFromVideo(uVar.getAndClearLastClickLocation());
            }

            public void b(u uVar) {
                m.this.logger.b("InterActivity", "Closing ad from video button...");
                m.this.dismiss();
            }

            public void c(u uVar) {
                m.this.logger.b("InterActivity", "Skipping video from video button...");
                m.this.skipVideo();
            }
        };
        vVar.a(new WeakReference(this.O));
        u a2 = u.a(this.sdk, vVar, getApplicationContext());
        a2.a(this.currentAd.s());
        return a2;
    }

    private void x() {
        if (this.m && this.currentAd.N()) {
            this.P = new a(this, ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.dn)).intValue(), this.currentAd.P());
            this.P.setColor(this.currentAd.Q());
            this.P.setBackgroundColor(this.currentAd.R());
            this.P.setVisibility(8);
            this.D.addView(this.P, new FrameLayout.LayoutParams(-1, -1, 17));
            this.D.bringChildToFront(this.P);
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        if (this.P != null) {
            this.P.a();
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        if (this.P != null) {
            this.P.b();
        }
    }

    public void clickThroughFromVideo(PointF pointF) {
        try {
            if (this.currentAd.al() && this.l) {
                o();
            }
            this.sdk.o().trackAndLaunchVideoClick(this.currentAd, this.a, this.currentAd.g(), pointF);
            com.applovin.impl.sdk.utils.j.a(this.b.e(), (AppLovinAd) this.currentAd);
            if (this.d != null) {
                this.d.b();
            }
        } catch (Throwable th) {
            this.sdk.v().b("InterActivity", "Encountered error while clicking through video.", th);
        }
    }

    public void continueVideo() {
        O();
    }

    public void dismiss() {
        long currentTimeMillis = System.currentTimeMillis() - this.p;
        StringBuilder sb = new StringBuilder();
        sb.append("Dismissing ad after ");
        sb.append(currentTimeMillis);
        sb.append(" milliseconds elapsed");
        p.g("InterActivity", sb.toString());
        if (this.sdk != null) {
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cX)).booleanValue()) {
                stopService(new Intent(getBaseContext(), AppKilledService.class));
                this.sdk.af().unregisterReceiver(this.S);
            }
            this.sdk.ae().b((a) this);
        }
        g();
        P();
        if (this.b != null) {
            if (this.currentAd != null) {
                c((AppLovinAd) this.currentAd);
                if (this.d != null) {
                    this.d.c();
                    this.d = null;
                }
                a("javascript:al_onPoststitialDismiss();", (long) this.currentAd.V());
            }
            this.b.a(false);
            this.b.g();
        }
        lastKnownWrapper = null;
        d();
    }

    public void exitWithError(String str) {
        a(str);
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("Initialized = ");
            sb.append(n.b);
            sb.append("; CleanedUp = ");
            sb.append(n.c);
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to properly render an Interstitial Activity, due to error: ");
            sb3.append(str);
            p.c("InterActivity", sb3.toString(), new Throwable(sb2));
            c((AppLovinAd) new h());
        } catch (Exception e2) {
            p.c("InterActivity", "Failed to show a video ad due to error:", e2);
        }
        dismiss();
    }

    public boolean getPostitialWasDisplayed() {
        return this.postitialWasDisplayed;
    }

    public int getVideoPercentViewed() {
        if (this.e) {
            return 100;
        }
        if (this.videoView != null) {
            int duration = this.videoView.getDuration();
            if (duration <= 0) {
                return this.u;
            }
            double currentPosition = (double) this.videoView.getCurrentPosition();
            double d2 = (double) duration;
            Double.isNaN(currentPosition);
            Double.isNaN(d2);
            return (int) ((currentPosition / d2) * 100.0d);
        }
        this.logger.e("InterActivity", "No video view detected on video end");
        return 0;
    }

    public void handleMediaError(String str) {
        this.logger.e("InterActivity", str);
        if (this.w.compareAndSet(false, true) && this.currentAd.H()) {
            a(str);
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isFullyWatched() {
        return getVideoPercentViewed() >= this.currentAd.T();
    }

    /* access modifiers changed from: protected */
    public boolean isVastAd() {
        return this.currentAd instanceof com.applovin.impl.a.a;
    }

    public void onBackPressed() {
        h hVar;
        if (this.currentAd != null) {
            if (this.currentAd.aD() && !this.postitialWasDisplayed) {
                return;
            }
            if (this.currentAd.aE() && this.postitialWasDisplayed) {
                return;
            }
        }
        if (c()) {
            this.logger.b("InterActivity", "Back button was pressed; forwarding to Android for handling...");
        } else {
            try {
                if (!this.postitialWasDisplayed && this.l && this.G != null && this.G.getVisibility() == 0 && this.G.getAlpha() > 0.0f) {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to skip button.");
                    hVar = this.G;
                } else if (this.E == null || this.E.getVisibility() != 0 || this.E.getAlpha() <= 0.0f) {
                    this.logger.b("InterActivity", "Back button was pressed, but was not eligible for dismissal.");
                    b("javascript:al_onBackPressed();");
                } else {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to close button.");
                    hVar = this.E;
                }
                hVar.performClick();
                b("javascript:al_onBackPressed();");
            } catch (Exception unused) {
            }
        }
        super.onBackPressed();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation != 0 && (this.videoView instanceof p) && this.K.get() != null) {
            MediaPlayer mediaPlayer = (MediaPlayer) this.K.get();
            this.videoView.setVideoSize(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01cb  */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        if (bundle != null) {
            this.x.set(bundle.getBoolean("instance_impression_tracked"));
        }
        requestWindowFeature(1);
        ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            String stringExtra = getIntent().getStringExtra(KEY_WRAPPER_ID);
            if (stringExtra == null || stringExtra.isEmpty()) {
                str = "Wrapper ID is null";
            } else {
                this.b = n.a(stringExtra);
                if (this.b == null && lastKnownWrapper != null) {
                    this.b = lastKnownWrapper;
                }
                if (this.b != null) {
                    final AppLovinAd b2 = this.b.b();
                    this.sdk = this.b.a();
                    this.logger = this.b.a().v();
                    if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cX)).booleanValue()) {
                        this.S = new BroadcastReceiver() {
                            public void onReceive(Context context, Intent intent) {
                                m.this.sdk.o().trackAppKilled((f) b2);
                                m.this.stopService(new Intent(m.this.getBaseContext(), AppKilledService.class));
                                m.this.sdk.af().unregisterReceiver(this);
                            }
                        };
                        this.sdk.af().registerReceiver(this.S, new IntentFilter(AppKilledService.ACTION_APP_KILLED));
                        startService(new Intent(getBaseContext(), AppKilledService.class));
                    }
                    if (b2 != null) {
                        f fVar = (f) b2;
                        this.d = new d(fVar, this.sdk);
                        if (fVar.aq()) {
                            this.sdk.ae().a((a) this);
                        }
                        View findViewById = findViewById(16908290);
                        if (findViewById != null) {
                            findViewById.setBackgroundColor(fVar.hasVideoUrl() ? fVar.C() : fVar.D());
                        }
                        this.p = System.currentTimeMillis();
                        if (fVar.v()) {
                            getWindow().setFlags(16777216, 16777216);
                        }
                        if (fVar.w()) {
                            getWindow().addFlags(128);
                        }
                        int e2 = q.e((Context) this);
                        boolean isTablet = AppLovinSdkUtils.isTablet(this);
                        int a2 = a(e2, isTablet);
                        if (bundle == null) {
                            this.v = a2;
                        } else {
                            this.v = bundle.getInt("original_orientation", a2);
                        }
                        if (!fVar.z()) {
                            this.logger.b("InterActivity", "Locking activity orientation to targeted orientation...");
                        } else if (a2 != -1) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Locking activity orientation to current orientation: ");
                            sb.append(a2);
                            this.logger.b("InterActivity", sb.toString());
                            b(a2);
                            this.a = new l(this.sdk.S(), AppLovinAdSize.INTERSTITIAL, this);
                            ((AdViewControllerImpl) this.a.getAdViewController()).setStatsManagerHelper(this.d);
                            this.b.a((j) this);
                            this.l = ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.dm)).booleanValue();
                            this.L = new b(this, this.sdk);
                            this.R = new com.applovin.impl.sdk.utils.a() {
                                public void onActivityCreated(Activity activity, Bundle bundle) {
                                    if (m.this.sdk != null && ((Boolean) m.this.sdk.a(com.applovin.impl.sdk.b.d.fa)).booleanValue() && !m.this.g) {
                                        if (activity.getClass().getName().equals(q.d(m.this.getApplicationContext()))) {
                                            m.this.sdk.K().a((com.applovin.impl.sdk.d.a) new ac(m.this.sdk, new Runnable() {
                                                public void run() {
                                                    p.j("AppLovinInterstitialActivity", "Dismissing on-screen ad due to app relaunched via launcher.");
                                                    m.this.dismiss();
                                                }
                                            }), r.a.MAIN);
                                        }
                                    }
                                }
                            };
                            this.sdk.aa().a(this.R);
                            StrictMode.setThreadPolicy(allowThreadDiskReads);
                            g();
                            if (this.d != null) {
                                this.d.a();
                            }
                            J();
                        } else {
                            this.logger.e("InterActivity", "Unable to detect current orientation. Locking to targeted orientation...");
                        }
                        b(e2, isTablet);
                        this.a = new l(this.sdk.S(), AppLovinAdSize.INTERSTITIAL, this);
                        ((AdViewControllerImpl) this.a.getAdViewController()).setStatsManagerHelper(this.d);
                        this.b.a((j) this);
                        this.l = ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.dm)).booleanValue();
                        this.L = new b(this, this.sdk);
                        this.R = new com.applovin.impl.sdk.utils.a() {
                            public void onActivityCreated(Activity activity, Bundle bundle) {
                                if (m.this.sdk != null && ((Boolean) m.this.sdk.a(com.applovin.impl.sdk.b.d.fa)).booleanValue() && !m.this.g) {
                                    if (activity.getClass().getName().equals(q.d(m.this.getApplicationContext()))) {
                                        m.this.sdk.K().a((com.applovin.impl.sdk.d.a) new ac(m.this.sdk, new Runnable() {
                                            public void run() {
                                                p.j("AppLovinInterstitialActivity", "Dismissing on-screen ad due to app relaunched via launcher.");
                                                m.this.dismiss();
                                            }
                                        }), r.a.MAIN);
                                    }
                                }
                            }
                        };
                        this.sdk.aa().a(this.R);
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        g();
                        if (this.d != null) {
                        }
                        J();
                    }
                    str = "No current ad found.";
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Wrapper is null; initialized state: ");
                    sb2.append(n.b);
                    str = sb2.toString();
                }
            }
            exitWithError(str);
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        g();
        if (this.d != null) {
        }
        J();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r4.currentAd != null) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0079, code lost:
        if (r4.currentAd == null) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007b, code lost:
        P();
        c((com.applovin.sdk.AppLovinAd) r4.currentAd);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0083, code lost:
        super.onDestroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0086, code lost:
        return;
     */
    public void onDestroy() {
        try {
            if (this.a != null) {
                ViewParent parent = this.a.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(this.a);
                }
                this.a.destroy();
                this.a = null;
            }
            if (this.videoView != null) {
                this.videoView.pause();
                this.videoView.stopPlayback();
            }
            if (this.sdk != null) {
                MediaPlayer mediaPlayer = (MediaPlayer) this.K.get();
                if (mediaPlayer != null) {
                    mediaPlayer.release();
                }
                this.sdk.aa().b(this.R);
            }
            if (this.countdownManager != null) {
                this.countdownManager.b();
            }
            if (this.C != null) {
                this.C.removeCallbacksAndMessages(null);
            }
            if (this.B != null) {
                this.B.removeCallbacksAndMessages(null);
            }
        } catch (Throwable th) {
            if (this.currentAd != null) {
                P();
                c((AppLovinAd) this.currentAd);
            }
            throw th;
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if ((i2 == 25 || i2 == 24) && this.currentAd.Y() && L()) {
            toggleMute();
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.logger.b("InterActivity", "App paused...");
        this.q = System.currentTimeMillis();
        if (this.postitialWasDisplayed) {
            M();
        }
        this.b.a(false);
        this.L.a();
        pauseReportRewardTask();
        b("javascript:al_onAppPaused();");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        h hVar;
        super.onResume();
        this.logger.b("InterActivity", "App resumed...");
        boolean z2 = true;
        this.b.a(true);
        if (!this.n) {
            if (this.d != null) {
                this.d.d(System.currentTimeMillis() - this.q);
            }
            if (!((Boolean) this.sdk.b(com.applovin.impl.sdk.b.f.w, Boolean.valueOf(false))).booleanValue() || this.L.d() || this.postitialWasDisplayed) {
                if (!(this.currentAd instanceof com.applovin.impl.sdk.ad.a) || !((com.applovin.impl.sdk.ad.a) this.currentAd).i()) {
                    z2 = false;
                }
                if (this.currentAd != null && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cG)).booleanValue() && !this.currentAd.x() && this.postitialWasDisplayed && this.E != null && !z2) {
                    hVar = this.E;
                }
                resumeReportRewardTask();
            } else {
                N();
                y();
                if (this.currentAd != null && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cG)).booleanValue() && !this.currentAd.y() && !this.postitialWasDisplayed && this.l && this.G != null) {
                    hVar = this.G;
                }
                resumeReportRewardTask();
            }
            a(0, hVar);
            resumeReportRewardTask();
        } else if (!this.L.d() && !this.postitialWasDisplayed && this.currentAd != null && this.currentAd.O()) {
            y();
        }
        b("javascript:al_onAppResumed();");
    }

    public void onRingerModeChanged(int i2) {
        String str;
        if (this.z != e.a) {
            this.A = true;
        }
        c adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView();
        if (adWebView != null) {
            if (e.a(i2) && !e.a(this.z)) {
                str = "javascript:al_muteSwitchOn();";
            } else if (i2 == 2) {
                str = "javascript:al_muteSwitchOff();";
            }
            adWebView.a(str);
        }
        this.z = i2;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("instance_impression_tracked", this.x.get());
        bundle.putInt("original_orientation", this.v);
    }

    public void onWindowFocusChanged(boolean z2) {
        String str;
        String str2;
        super.onWindowFocusChanged(z2);
        if (z2) {
            if (this.sdk != null) {
                this.logger.b("InterActivity", "Window gained focus");
                try {
                    if (!g.c() || !((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.dh)).booleanValue() || !a()) {
                        getWindow().setFlags(1024, 1024);
                    } else {
                        b();
                        if (((Long) this.sdk.a(com.applovin.impl.sdk.b.d.cQ)).longValue() > 0) {
                            this.C.postDelayed(new Runnable() {
                                public void run() {
                                    m.this.b();
                                }
                            }, ((Long) this.sdk.a(com.applovin.impl.sdk.b.d.cQ)).longValue());
                        }
                    }
                    if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cR)).booleanValue() && !this.postitialWasDisplayed) {
                        N();
                        resumeReportRewardTask();
                    }
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Setting window flags failed.", th);
                }
                this.n = false;
                StringBuilder sb = new StringBuilder();
                sb.append("javascript:al_onWindowFocusChanged( ");
                sb.append(z2);
                sb.append(" );");
                b(sb.toString());
            }
            str = "InterActivity";
            str2 = "Window gained focus. SDK is null.";
        } else if (this.sdk != null) {
            this.logger.b("InterActivity", "Window lost focus");
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cR)).booleanValue() && !this.postitialWasDisplayed) {
                M();
                pauseReportRewardTask();
            }
            this.n = false;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("javascript:al_onWindowFocusChanged( ");
            sb2.append(z2);
            sb2.append(" );");
            b(sb2.toString());
        } else {
            str = "InterActivity";
            str2 = "Window lost focus. SDK is null.";
        }
        p.g(str, str2);
        this.n = false;
        StringBuilder sb22 = new StringBuilder();
        sb22.append("javascript:al_onWindowFocusChanged( ");
        sb22.append(z2);
        sb22.append(" );");
        b(sb22.toString());
    }

    public void pauseReportRewardTask() {
        if (this.Q != null) {
            this.Q.b();
        }
    }

    /* access modifiers changed from: protected */
    public void playVideo() {
        d((AppLovinAd) this.currentAd);
        this.videoView.start();
        this.countdownManager.a();
    }

    public void resumeReportRewardTask() {
        if (this.Q != null) {
            this.Q.c();
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldContinueFullLengthVideoCountdown() {
        return !this.e && !this.postitialWasDisplayed;
    }

    public void showPostitial() {
        long j2;
        h hVar;
        try {
            if (this.d != null) {
                this.d.g();
            }
            if (!this.currentAd.af()) {
                K();
            }
            if (this.a != null) {
                ViewParent parent = this.a.getParent();
                if ((parent instanceof ViewGroup) && (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ff)).booleanValue() || parent != this.D)) {
                    ((ViewGroup) parent).removeView(this.a);
                }
                FrameLayout frameLayout = ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ff)).booleanValue() ? this.D : new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
                frameLayout.setBackgroundColor(this.currentAd.D());
                if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ff)).booleanValue()) {
                    this.a.setVisibility(0);
                } else {
                    frameLayout.addView(this.a);
                }
                if (this.currentAd.af()) {
                    K();
                }
                if (this.D != null) {
                    if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ff)).booleanValue()) {
                        com.applovin.impl.sdk.utils.b.a(this.D, this.a);
                    } else {
                        this.D.removeAllViewsInLayout();
                    }
                }
                if (n() && this.F != null) {
                    if (this.F.getParent() instanceof ViewGroup) {
                        ((ViewGroup) this.F.getParent()).removeView(this.F);
                    }
                    frameLayout.addView(this.F);
                    this.F.bringToFront();
                }
                if (this.E != null) {
                    ViewParent parent2 = this.E.getParent();
                    if (parent2 instanceof ViewGroup) {
                        ((ViewGroup) parent2).removeView(this.E);
                    }
                    frameLayout.addView(this.E);
                    this.E.bringToFront();
                }
                if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ff)).booleanValue()) {
                    setContentView(frameLayout);
                }
                if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.eX)).booleanValue()) {
                    this.a.setVisibility(4);
                    this.a.setVisibility(0);
                }
                a("javascript:al_onPoststitialShow();", (long) this.currentAd.U());
            }
            if (!((this.currentAd instanceof com.applovin.impl.sdk.ad.a) && ((com.applovin.impl.sdk.ad.a) this.currentAd).i())) {
                if (this.currentAd.n() >= 0.0f) {
                    j2 = q.b(this.currentAd.n());
                    hVar = this.E;
                } else if (this.currentAd.n() == -2.0f) {
                    this.E.setVisibility(0);
                } else {
                    j2 = 0;
                    hVar = this.E;
                }
                a(j2, hVar);
            } else {
                this.logger.b("InterActivity", "Skip showing of close button");
            }
            this.postitialWasDisplayed = true;
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Encountered error while showing postitial. Dismissing...", th);
            dismiss();
        }
    }

    public void skipVideo() {
        this.t = SystemClock.elapsedRealtime() - this.s;
        if (this.d != null) {
            this.d.f();
        }
        if (this.currentAd.q()) {
            dismiss();
        } else {
            showPostitial();
        }
    }

    public void toggleMute() {
        boolean z2 = !L();
        if (this.d != null) {
            this.d.i();
        }
        try {
            c(z2);
            a(z2);
            b(z2);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to set volume to ");
            sb.append(z2);
            this.logger.b("InterActivity", sb.toString(), th);
        }
    }
}
