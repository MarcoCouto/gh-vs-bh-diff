package com.applovin.impl.adview;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.appodeal.ads.AppodealNetworks;
import java.lang.ref.WeakReference;

public class v extends WebViewClient {
    private final p a;
    private WeakReference<a> b;

    public interface a {
        void a(u uVar);

        void b(u uVar);

        void c(u uVar);
    }

    public v(j jVar) {
        this.a = jVar.v();
    }

    private void a(WebView webView, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("Processing click on ad URL \"");
        sb.append(str);
        sb.append("\"");
        this.a.c("WebViewButtonClient", sb.toString());
        if (str != null && (webView instanceof u)) {
            u uVar = (u) webView;
            Uri parse = Uri.parse(str);
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            a aVar = (a) this.b.get();
            if (AppodealNetworks.APPLOVIN.equalsIgnoreCase(scheme) && "com.applovin.sdk".equalsIgnoreCase(host) && aVar != null) {
                if ("/track_click".equals(path)) {
                    aVar.a(uVar);
                } else if ("/close_ad".equals(path)) {
                    aVar.b(uVar);
                } else if ("/skip_ad".equals(path)) {
                    aVar.c(uVar);
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unknown URL: ");
                    sb2.append(str);
                    this.a.d("WebViewButtonClient", sb2.toString());
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Path: ");
                    sb3.append(path);
                    this.a.d("WebViewButtonClient", sb3.toString());
                }
            }
        }
    }

    public void a(WeakReference<a> weakReference) {
        this.b = weakReference;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a(webView, str);
        return true;
    }
}
