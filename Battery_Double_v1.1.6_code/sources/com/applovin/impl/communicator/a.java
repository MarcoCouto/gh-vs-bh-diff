package com.applovin.impl.communicator;

import android.content.Context;
import android.content.IntentFilter;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import java.util.HashSet;
import java.util.Set;

public class a {
    private final Context a;
    private final Set<b> b = new HashSet(32);
    private final Object c = new Object();

    public a(Context context) {
        this.a = context;
    }

    private b a(String str, AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber) {
        for (b bVar : this.b) {
            if (str.equals(bVar.a()) && appLovinCommunicatorSubscriber.equals(bVar.b())) {
                return bVar;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x004e, code lost:
        return true;
     */
    public boolean a(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        if (appLovinCommunicatorSubscriber == null || !n.b(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to subscribe - invalid subscriber (");
            sb.append(appLovinCommunicatorSubscriber);
            sb.append(") or topic (");
            sb.append(str);
            sb.append(")");
            p.j("AppLovinCommunicator", sb.toString());
            return false;
        }
        synchronized (this.c) {
            b a2 = a(str, appLovinCommunicatorSubscriber);
            if (a2 != null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Attempting to re-subscribe subscriber (");
                sb2.append(appLovinCommunicatorSubscriber);
                sb2.append(") to topic (");
                sb2.append(str);
                sb2.append(")");
                p.g("AppLovinCommunicator", sb2.toString());
                if (!a2.c()) {
                    a2.a(true);
                    AppLovinBroadcastManager.getInstance(this.a).registerReceiver(a2, new IntentFilter(str));
                }
            } else {
                b bVar = new b(str, appLovinCommunicatorSubscriber);
                this.b.add(bVar);
                AppLovinBroadcastManager.getInstance(this.a).registerReceiver(bVar, new IntentFilter(str));
                return true;
            }
        }
    }

    public void b(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        b a2;
        if (n.b(str)) {
            synchronized (this.c) {
                a2 = a(str, appLovinCommunicatorSubscriber);
            }
            if (a2 != null) {
                a2.a(false);
                AppLovinBroadcastManager.getInstance(this.a).unregisterReceiver(a2);
            }
        }
    }
}
