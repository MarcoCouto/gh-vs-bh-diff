package com.moat.analytics.mobile.inm;

import android.support.annotation.CallSuper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class h extends c {
    int l = Integer.MIN_VALUE;
    private a m = a.UNINITIALIZED;
    private int n = Integer.MIN_VALUE;
    private double o = Double.NaN;
    private int p = Integer.MIN_VALUE;
    private int q = 0;

    enum a {
        UNINITIALIZED,
        PAUSED,
        PLAYING,
        STOPPED,
        COMPLETED
    }

    h(String str) {
        super(str);
    }

    private void t() {
        this.i.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (!h.this.n() || h.this.m()) {
                        h.this.l();
                    } else if (Boolean.valueOf(h.this.s()).booleanValue()) {
                        h.this.i.postDelayed(this, 200);
                    } else {
                        h.this.l();
                    }
                } catch (Exception e) {
                    h.this.l();
                    m.a(e);
                }
            }
        }, 200);
    }

    /* access modifiers changed from: 0000 */
    public JSONObject a(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.b.equals(MoatAdEvent.a)) {
            num = moatAdEvent.b;
        } else {
            try {
                num = o();
            } catch (Exception unused) {
                num = Integer.valueOf(this.n);
            }
            moatAdEvent.b = num;
        }
        if (moatAdEvent.b.intValue() < 0 || (moatAdEvent.b.intValue() == 0 && moatAdEvent.d == MoatAdEventType.AD_EVT_COMPLETE && this.n > 0)) {
            num = Integer.valueOf(this.n);
            moatAdEvent.b = num;
        }
        if (moatAdEvent.d == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.l == Integer.MIN_VALUE || !a(num, Integer.valueOf(this.l))) {
                this.m = a.STOPPED;
                moatAdEvent.d = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.m = a.COMPLETED;
            }
        }
        return super.a(moatAdEvent);
    }

    public boolean a(Map<String, String> map, View view) {
        try {
            boolean a2 = super.a(map, view);
            if (!a2 || !p()) {
                return a2;
            }
            t();
            return a2;
        } catch (Exception e) {
            p.a(3, "IntervalVideoTracker", (Object) this, "Problem with video loop");
            a("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean n();

    /* access modifiers changed from: 0000 */
    public abstract Integer o();

    /* access modifiers changed from: protected */
    public boolean p() {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public abstract boolean q();

    /* access modifiers changed from: 0000 */
    public abstract Integer r();

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x009a A[Catch:{ Exception -> 0x00d2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009c A[Catch:{ Exception -> 0x00d2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00bd A[Catch:{ Exception -> 0x00d2 }] */
    @CallSuper
    public boolean s() {
        boolean z;
        a aVar;
        if (n() && !m()) {
            try {
                int intValue = o().intValue();
                if (this.n >= 0 && intValue < 0) {
                    return false;
                }
                this.n = intValue;
                if (intValue == 0) {
                    return true;
                }
                int intValue2 = r().intValue();
                boolean q2 = q();
                double d = (double) intValue2;
                Double.isNaN(d);
                double d2 = d / 4.0d;
                double doubleValue = j().doubleValue();
                MoatAdEventType moatAdEventType = null;
                if (intValue > this.p) {
                    this.p = intValue;
                }
                if (this.l == Integer.MIN_VALUE) {
                    this.l = intValue2;
                }
                if (q2) {
                    if (this.m == a.UNINITIALIZED) {
                        moatAdEventType = MoatAdEventType.AD_EVT_START;
                    } else if (this.m == a.PAUSED) {
                        moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                    } else {
                        double d3 = (double) intValue;
                        Double.isNaN(d3);
                        int floor = ((int) Math.floor(d3 / d2)) - 1;
                        if (floor >= 0 && floor < 3) {
                            MoatAdEventType moatAdEventType2 = g[floor];
                            if (!this.h.containsKey(moatAdEventType2)) {
                                this.h.put(moatAdEventType2, Integer.valueOf(1));
                                moatAdEventType = moatAdEventType2;
                            }
                        }
                        z = moatAdEventType != null;
                        if (!z && !Double.isNaN(this.o) && Math.abs(this.o - doubleValue) > 0.05d) {
                            moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                            z = true;
                        }
                        if (z) {
                            dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), k()));
                        }
                        this.o = doubleValue;
                        this.q = 0;
                        return true;
                    }
                    aVar = a.PLAYING;
                } else {
                    if (this.m != a.PAUSED) {
                        moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                        aVar = a.PAUSED;
                    }
                    if (moatAdEventType != null) {
                    }
                    moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                    z = true;
                    if (z) {
                    }
                    this.o = doubleValue;
                    this.q = 0;
                    return true;
                }
                this.m = aVar;
                if (moatAdEventType != null) {
                }
                moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                z = true;
                if (z) {
                }
                this.o = doubleValue;
                this.q = 0;
                return true;
            } catch (Exception unused) {
                int i = this.q;
                this.q = i + 1;
                if (i < 5) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.o = j().doubleValue();
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            m.a(e);
        }
    }
}
