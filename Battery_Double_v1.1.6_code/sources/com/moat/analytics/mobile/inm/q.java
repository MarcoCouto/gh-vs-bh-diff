package com.moat.analytics.mobile.inm;

import com.moat.analytics.mobile.inm.a.b.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class q {
    q() {
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:17|16|19|20|(2:22|23)|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004c, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004e */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0054 A[SYNTHETIC, Splitter:B:22:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a A[SYNTHETIC, Splitter:B:27:0x005a] */
    static a<String> a(String str) {
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
        httpURLConnection.setDoInput(true);
        httpURLConnection.connect();
        if (httpURLConnection.getResponseCode() >= 400) {
            return a.a();
        }
        InputStream inputStream2 = httpURLConnection.getInputStream();
        try {
            a<String> a = a.a(a(inputStream2));
            if (inputStream2 != null) {
                try {
                    inputStream2.close();
                } catch (IOException unused) {
                }
            }
            return a;
        } catch (IOException unused2) {
            inputStream = inputStream2;
            a<String> a2 = a.a();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused3) {
                }
            }
            return a2;
        } catch (Throwable th) {
            Throwable th2 = th;
            inputStream = inputStream2;
            th = th2;
            if (inputStream != null) {
            }
            throw th;
        }
    }

    private static String a(InputStream inputStream) {
        char[] cArr = new char[256];
        StringBuilder sb = new StringBuilder();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        int i = 0;
        do {
            int read = inputStreamReader.read(cArr, 0, 256);
            if (read <= 0) {
                break;
            }
            i += read;
            sb.append(cArr, 0, read);
        } while (i < 1024);
        return sb.toString();
    }

    static void b(final String str) {
        new Thread() {
            public final void run() {
                try {
                    q.a(str);
                } catch (Exception unused) {
                }
            }
        }.start();
    }
}
