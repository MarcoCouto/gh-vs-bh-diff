package com.moat.analytics.mobile.ogury;

import android.os.Handler;
import android.support.annotation.CallSuper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

abstract class e extends c {

    /* renamed from: ʽ reason: contains not printable characters */
    static final MoatAdEventType[] f271 = {MoatAdEventType.AD_EVT_FIRST_QUARTILE, MoatAdEventType.AD_EVT_MID_POINT, MoatAdEventType.AD_EVT_THIRD_QUARTILE};

    /* renamed from: ʻ reason: contains not printable characters */
    final Map<MoatAdEventType, Integer> f272;

    /* renamed from: ʼ reason: contains not printable characters */
    WeakReference<View> f273;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private Map<String, String> f274;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private final Set<MoatAdEventType> f275;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ reason: contains not printable characters */
    public VideoTrackerListener f276;

    /* renamed from: ͺ reason: contains not printable characters */
    private Double f277;

    /* renamed from: ॱˊ reason: contains not printable characters */
    private boolean f278;

    /* renamed from: ॱˎ reason: contains not printable characters */
    private final String f279;

    /* renamed from: ॱॱ reason: contains not printable characters */
    final Handler f280;
    /* access modifiers changed from: private */

    /* renamed from: ॱᐝ reason: contains not printable characters */
    public final d f281 = new d(a.m210(), b.f270);

    /* renamed from: com.moat.analytics.mobile.ogury.e$1 reason: invalid class name */
    class AnonymousClass1 implements Runnable {
        AnonymousClass1() {
        }

        public final void run() {
            try {
                m243(3, "BaseVideoTracker", this, "Shutting down.");
                d r0 = e.this.f281;
                m243(3, "GlobalWebView", r0, "Cleaning up");
                r0.f262.m269();
                r0.f262 = null;
                r0.f263.destroy();
                r0.f263 = null;
                e.this.f276 = null;
            } catch (Exception e) {
                l.m310(e);
            }
        }

        AnonymousClass1() {
        }

        /* renamed from: ˋ reason: contains not printable characters */
        static void m243(int i, String str, Object obj, String str2) {
            if (q.m343().f394) {
                if (obj == null) {
                    Log.println(i, "Moat".concat(String.valueOf(str)), String.format("message = %s", new Object[]{str2}));
                    return;
                }
                Log.println(i, "Moat".concat(String.valueOf(str)), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}));
            }
        }

        /* renamed from: ˎ reason: contains not printable characters */
        static void m244(String str, Object obj, String str2) {
            Object obj2;
            if (q.m343().f393) {
                String concat = "Moat".concat(String.valueOf(str));
                String str3 = "id = %s, message = %s";
                Object[] objArr = new Object[2];
                if (obj == null) {
                    obj2 = "null";
                } else {
                    obj2 = Integer.valueOf(obj.hashCode());
                }
                objArr[0] = obj2;
                objArr[1] = str2;
                Log.println(2, concat, String.format(str3, objArr));
            }
        }

        /* renamed from: ˏ reason: contains not printable characters */
        static void m246(String str, Object obj, String str2, Throwable th) {
            if (q.m343().f394) {
                Log.e("Moat".concat(String.valueOf(str)), String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}), th);
            }
        }

        /* renamed from: ॱ reason: contains not printable characters */
        static void m247(String str, String str2) {
            if (!q.m343().f394 && ((i) MoatAnalytics.getInstance()).f337) {
                int i = 2;
                if (str.equals("[ERROR] ")) {
                    i = 6;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(str2);
                Log.println(i, "MoatAnalytics", sb.toString());
            }
        }

        /* renamed from: ˏ reason: contains not printable characters */
        static String m245(View view) {
            if (view == null) {
                return "null";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(view.getClass().getSimpleName());
            sb.append("@");
            sb.append(view.hashCode());
            return sb.toString();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public abstract Map<String, Object> m242() throws l;

    e(String str) {
        super(null, false, true);
        AnonymousClass1.m243(3, "BaseVideoTracker", this, "Initializing.");
        this.f279 = str;
        try {
            super.m221(this.f281.f263);
        } catch (l e) {
            this.f254 = e;
        }
        this.f272 = new HashMap();
        this.f275 = new HashSet();
        this.f280 = new Handler();
        this.f278 = false;
        this.f277 = Double.valueOf(1.0d);
    }

    public void setVideoListener(VideoTrackerListener videoTrackerListener) {
        this.f276 = videoTrackerListener;
    }

    public void removeVideoListener() {
        this.f276 = null;
    }

    @CallSuper
    /* renamed from: ˋ reason: contains not printable characters */
    public boolean m238(Map<String, String> map, View view) {
        try {
            m225();
            m220();
            if (view == null) {
                AnonymousClass1.m243(3, "BaseVideoTracker", this, "trackVideoAd received null video view instance");
            }
            this.f274 = map;
            this.f273 = new WeakReference<>(view);
            m239();
            String format = String.format("trackVideoAd tracking ids: %s | view: %s", new Object[]{new JSONObject(map).toString(), AnonymousClass1.m245(view)});
            AnonymousClass1.m243(3, "BaseVideoTracker", this, format);
            StringBuilder sb = new StringBuilder();
            sb.append(m224());
            sb.append(" ");
            sb.append(format);
            AnonymousClass1.m247("[SUCCESS] ", sb.toString());
            if (this.f255 != null) {
                this.f255.onTrackingStarted(m216());
            }
            return true;
        } catch (Exception e) {
            m222("trackVideoAd", e);
            return false;
        }
    }

    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(AnonymousClass1.m245(view));
        AnonymousClass1.m243(3, "BaseVideoTracker", this, sb.toString());
        this.f273 = new WeakReference<>(view);
        try {
            super.changeTargetView(view);
        } catch (Exception e) {
            l.m310(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public void m236(List<String> list) throws l {
        if (this.f274 == null) {
            list.add("Null adIds object");
        }
        if (list.isEmpty()) {
            super.m218(list);
            return;
        }
        throw new l(TextUtils.join(" and ", list));
    }

    public void stopTracking() {
        try {
            super.stopTracking();
            m240();
            if (this.f276 != null) {
                this.f276 = null;
            }
        } catch (Exception e) {
            l.m310(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊॱ reason: contains not printable characters */
    public final Double m237() {
        return this.f277;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m239() throws l {
        super.changeTargetView((View) this.f273.get());
        super.m223();
        Map r0 = m242();
        Integer num = (Integer) r0.get("width");
        Integer num2 = (Integer) r0.get("height");
        Integer num3 = (Integer) r0.get("duration");
        AnonymousClass1.m243(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "Player metadata: height = %d, width = %d, duration = %d", new Object[]{num2, num, num3}));
        this.f281.m230(this.f279, this.f274, num, num2, num3);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public JSONObject m235(MoatAdEvent moatAdEvent) {
        if (Double.isNaN(moatAdEvent.f229.doubleValue())) {
            moatAdEvent.f229 = this.f277;
        }
        return new JSONObject(moatAdEvent.m204());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏॱ reason: contains not printable characters */
    public final void m240() {
        if (!this.f278) {
            this.f278 = true;
            this.f280.postDelayed(new AnonymousClass1(), 500);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˊ reason: contains not printable characters */
    public final boolean m241() {
        return this.f272.containsKey(MoatAdEventType.AD_EVT_COMPLETE) || this.f272.containsKey(MoatAdEventType.AD_EVT_STOPPED) || this.f272.containsKey(MoatAdEventType.AD_EVT_SKIPPED);
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static boolean m233(Integer num, Integer num2) {
        int abs = Math.abs(num2.intValue() - num.intValue());
        double intValue = (double) num2.intValue();
        Double.isNaN(intValue);
        return ((double) abs) <= Math.min(750.0d, intValue * 0.05d);
    }

    public void setPlayerVolume(Double d) {
        Double valueOf = Double.valueOf(this.f277.doubleValue() * s.m373());
        if (!d.equals(this.f277)) {
            AnonymousClass1.m243(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "player volume changed to %f ", new Object[]{d}));
            this.f277 = d;
            if (!valueOf.equals(Double.valueOf(this.f277.doubleValue() * s.m373()))) {
                dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_VOLUME_CHANGE, MoatAdEvent.f227, this.f277));
            }
        }
    }

    public void dispatchEvent(MoatAdEvent moatAdEvent) {
        try {
            JSONObject r0 = m235(moatAdEvent);
            boolean z = false;
            AnonymousClass1.m243(3, "BaseVideoTracker", this, String.format("Received event: %s", new Object[]{r0.toString()}));
            StringBuilder sb = new StringBuilder();
            sb.append(m224());
            sb.append(String.format(" Received event: %s", new Object[]{r0.toString()}));
            AnonymousClass1.m247("[SUCCESS] ", sb.toString());
            if (m219() && this.f252 != null) {
                this.f252.m275(this.f281.f265, r0);
                if (!this.f275.contains(moatAdEvent.f230)) {
                    this.f275.add(moatAdEvent.f230);
                    if (this.f276 != null) {
                        this.f276.onVideoEventReported(moatAdEvent.f230);
                    }
                }
            }
            MoatAdEventType moatAdEventType = moatAdEvent.f230;
            if (moatAdEventType == MoatAdEventType.AD_EVT_COMPLETE || moatAdEventType == MoatAdEventType.AD_EVT_STOPPED || moatAdEventType == MoatAdEventType.AD_EVT_SKIPPED) {
                z = true;
            }
            if (z) {
                this.f272.put(moatAdEventType, Integer.valueOf(1));
                if (this.f252 != null) {
                    this.f252.m273((c) this);
                }
                m240();
            }
        } catch (Exception e) {
            l.m310(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʼ reason: contains not printable characters */
    public final Double m234() {
        return Double.valueOf(this.f277.doubleValue() * s.m373());
    }
}
