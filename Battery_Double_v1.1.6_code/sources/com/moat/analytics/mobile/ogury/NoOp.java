package com.moat.analytics.mobile.ogury;

import android.app.Activity;
import android.app.Application;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.ogury.NativeDisplayTracker.MoatUserInteractionType;
import java.util.Map;

abstract class NoOp {

    public static class MoatAnalytics extends MoatAnalytics {
        public void prepareNativeDisplayTracking(String str) {
        }

        public void start(Application application) {
        }

        public void start(MoatOptions moatOptions, Application application) {
        }
    }

    public static class MoatFactory extends MoatFactory {
        public WebAdTracker createWebAdTracker(@NonNull WebView webView) {
            return new b();
        }

        public WebAdTracker createWebAdTracker(@NonNull ViewGroup viewGroup) {
            return new b();
        }

        public NativeDisplayTracker createNativeDisplayTracker(@NonNull View view, @NonNull Map<String, String> map) {
            return new d();
        }

        public NativeVideoTracker createNativeVideoTracker(@NonNull String str) {
            return new a();
        }

        public <T> T createCustomTracker(n<T> nVar) {
            return nVar.createNoOp();
        }
    }

    static class a implements NativeVideoTracker {
        public final void changeTargetView(View view) {
        }

        public final void dispatchEvent(MoatAdEvent moatAdEvent) {
        }

        public final void removeListener() {
        }

        public final void removeVideoListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void setPlayerVolume(Double d) {
        }

        public final void setVideoListener(VideoTrackerListener videoTrackerListener) {
        }

        public final void stopTracking() {
        }

        public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
            return false;
        }

        a() {
        }
    }

    static class b implements WebAdTracker {
        public final void removeListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void startTracking() {
        }

        public final void stopTracking() {
        }

        b() {
        }
    }

    static class d implements NativeDisplayTracker {
        public final void removeListener() {
        }

        public final void reportUserInteractionEvent(MoatUserInteractionType moatUserInteractionType) {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void startTracking() {
        }

        public final void stopTracking() {
        }

        d() {
        }
    }

    NoOp() {
    }
}
