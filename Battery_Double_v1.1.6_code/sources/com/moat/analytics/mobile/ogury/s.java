package com.moat.analytics.mobile.ogury;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.provider.Settings.Global;
import android.support.annotation.FloatRange;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.presage.CamembertauCalvados;
import java.lang.ref.WeakReference;

final class s {

    /* renamed from: ˊ reason: contains not printable characters */
    private static long f416 = 5996206772453036003L;

    /* renamed from: ˋ reason: contains not printable characters */
    private static String f417 = null;

    /* renamed from: ˎ reason: contains not printable characters */
    private static b f418 = null;

    /* renamed from: ˏ reason: contains not printable characters */
    private static a f419 = null;

    /* renamed from: ॱ reason: contains not printable characters */
    private static int f420 = 0;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private static int f421 = 1;

    static class a {

        /* renamed from: ˋ reason: contains not printable characters */
        private String f423;

        /* renamed from: ˎ reason: contains not printable characters */
        private String f424;

        /* renamed from: ˏ reason: contains not printable characters */
        private String f425;
        /* access modifiers changed from: private */

        /* renamed from: ॱ reason: contains not printable characters */
        public boolean f426;

        /* synthetic */ a(byte b) {
            this();
        }

        private a() {
            this.f426 = false;
            this.f424 = "_unknown_";
            this.f423 = "_unknown_";
            this.f425 = "_unknown_";
            try {
                Context r0 = s.m372();
                if (r0 != null) {
                    this.f426 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f423 = r0.getPackageName();
                    this.f424 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f425 = packageManager.getInstallerPackageName(this.f423);
                    return;
                }
                AnonymousClass1.m243(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                l.m310(e);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ॱ reason: contains not printable characters */
        public final String m384() {
            return this.f424;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˏ reason: contains not printable characters */
        public final String m383() {
            return this.f423;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˋ reason: contains not printable characters */
        public final String m382() {
            return this.f425 != null ? this.f425 : "_unknown_";
        }
    }

    static class b {

        /* renamed from: ʽ reason: contains not printable characters */
        boolean f427;

        /* renamed from: ˊ reason: contains not printable characters */
        String f428;

        /* renamed from: ˋ reason: contains not printable characters */
        Integer f429;

        /* renamed from: ˎ reason: contains not printable characters */
        String f430;

        /* renamed from: ˏ reason: contains not printable characters */
        boolean f431;

        /* renamed from: ॱ reason: contains not printable characters */
        boolean f432;

        /* synthetic */ b(byte b) {
            this();
        }

        private b() {
            this.f430 = "_unknown_";
            this.f428 = "_unknown_";
            this.f429 = Integer.valueOf(-1);
            this.f431 = false;
            this.f432 = false;
            this.f427 = false;
            try {
                Context r0 = s.m372();
                if (r0 != null) {
                    this.f427 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService(PlaceFields.PHONE);
                    this.f430 = telephonyManager.getSimOperatorName();
                    this.f428 = telephonyManager.getNetworkOperatorName();
                    this.f429 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f431 = s.m370();
                    this.f432 = s.m380(r0);
                }
            } catch (Exception e) {
                l.m310(e);
            }
        }
    }

    s() {
    }

    /* renamed from: ʻ reason: contains not printable characters */
    static /* synthetic */ String m369() {
        int i = f420 + 27;
        f421 = i % 128;
        int i2 = i % 2;
        String str = f417;
        int i3 = f421 + 115;
        f420 = i3 % 128;
        if ((i3 % 2 != 0 ? '*' : 'M') != '*') {
        }
        return str;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static /* synthetic */ String m374(String str) {
        int i = f421 + 125;
        f420 = i % 128;
        if (!(i % 2 != 0)) {
            f417 = str;
        } else {
            f417 = str;
            Object obj = null;
            super.hashCode();
        }
        int i2 = f420 + 43;
        f421 = i2 % 128;
        if (i2 % 2 == 0) {
        }
        return str;
    }

    static {
        int i = f420 + 121;
        f421 = i % 128;
        if (i % 2 == 0) {
            Object obj = null;
            super.hashCode();
        }
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    /* renamed from: ˋ reason: contains not printable characters */
    static double m373() {
        int i = f420 + 81;
        f421 = i % 128;
        if ((i % 2 == 0 ? '0' : '!') != '0') {
            try {
                double r1 = (double) m371();
                double streamMaxVolume = (double) ((AudioManager) a.m210().getSystemService(m376("꙼㞉ꘝꭠ㓂鵕馟").intern())).getStreamMaxVolume(3);
                Double.isNaN(r1);
                Double.isNaN(streamMaxVolume);
                return r1 / streamMaxVolume;
            } catch (Exception e) {
                l.m310(e);
                return Utils.DOUBLE_EPSILON;
            }
        } else {
            double r12 = (double) m371();
            double streamMaxVolume2 = (double) ((AudioManager) a.m210().getSystemService(m376("꙼㞉ꘝꭠ㓂鵕馟").intern())).getStreamMaxVolume(2);
            Double.isNaN(r12);
            Double.isNaN(streamMaxVolume2);
            return r12 * streamMaxVolume2;
        }
    }

    /* renamed from: ʽ reason: contains not printable characters */
    private static int m371() {
        int i = f421 + 39;
        f420 = i % 128;
        int i2 = i % 2;
        try {
            int streamVolume = ((AudioManager) a.m210().getSystemService(m376("꙼㞉ꘝꭠ㓂鵕馟").intern())).getStreamVolume(3);
            int i3 = f421 + 57;
            f420 = i3 % 128;
            if (i3 % 2 != 0) {
            }
            return streamVolume;
        } catch (Exception e) {
            l.m310(e);
            return 0;
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static void m377(final Context context) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
                        Class cls2 = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
                        Object invoke = cls.getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{context});
                        if (!((Boolean) cls2.getMethod(RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0])).booleanValue()) {
                            s.m374((String) cls2.getMethod("getId", new Class[0]).invoke(invoke, new Object[0]));
                            StringBuilder sb = new StringBuilder("Retrieved Advertising ID = ");
                            sb.append(s.m369());
                            AnonymousClass1.m243(3, "Util", this, sb.toString());
                            return;
                        }
                        AnonymousClass1.m243(3, "Util", this, "User has limited ad tracking");
                    } catch (ClassNotFoundException e) {
                        AnonymousClass1.m246("Util", this, "ClassNotFoundException while retrieving Advertising ID", e);
                    } catch (NoSuchMethodException e2) {
                        AnonymousClass1.m246("Util", this, "NoSuchMethodException while retrieving Advertising ID", e2);
                    } catch (Exception e3) {
                        l.m310(e3);
                    }
                }
            });
            int i = f421 + 103;
            f420 = i % 128;
            if (i % 2 != 0) {
            }
        } catch (Exception e) {
            l.m310(e);
        }
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static String m379() {
        int i = f420 + 117;
        f421 = i % 128;
        int i2 = i % 2;
        String str = f417;
        int i3 = f420 + 1;
        f421 = i3 % 128;
        if (!(i3 % 2 == 0)) {
        }
        return str;
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static Context m372() {
        int i = f421 + 39;
        f420 = i % 128;
        int i2 = i % 2;
        WeakReference<Context> weakReference = ((i) MoatAnalytics.getInstance()).f338;
        if ((weakReference != null ? 'c' : 1) != 'c') {
            return null;
        }
        int i3 = f421 + 103;
        f420 = i3 % 128;
        if ((i3 % 2 != 0 ? '7' : '*') != '7') {
            return (Context) weakReference.get();
        }
        return (Context) weakReference.get();
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static a m375() {
        int i = f420 + 103;
        f421 = i % 128;
        int i2 = i % 2;
        if (!(f419 != null) || !f419.f426) {
            f419 = new a(0);
            int i3 = f420 + 65;
            f421 = i3 % 128;
            int i4 = i3 % 2;
        }
        return f419;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (f418.f427 == false) goto L_0x001a;
     */
    /* renamed from: ˏ reason: contains not printable characters */
    static b m378() {
        if (f418 != null) {
            int i = f421 + 31;
            f420 = i % 128;
            int i2 = i % 2;
        }
        f418 = new b(0);
        int i3 = f421 + 45;
        f420 = i3 % 128;
        int i4 = i3 % 2;
        return f418;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0016, code lost:
        if ((r3.getApplicationInfo().flags & 4) != 0) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
        if (((r3.getApplicationInfo().flags & 2) == 0) != true) goto L_0x0028;
     */
    /* renamed from: ॱ reason: contains not printable characters */
    static boolean m380(Context context) {
        int i = f420 + 83;
        f421 = i % 128;
        if (i % 2 == 0) {
        }
        int i2 = f420 + 97;
        f421 = i2 % 128;
        if ((i2 % 2 == 0 ? '\'' : 'D') != 'D') {
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004f, code lost:
        if (android.os.Build.VERSION.SDK_INT >= 108) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005f, code lost:
        if ((android.os.Build.VERSION.SDK_INT >= 17 ? '7' : 'c') != 'c') goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0074, code lost:
        r0 = android.provider.Settings.Secure.getInt(r1.getContentResolver(), m376("낑ᗰ냰?⮲됖䤘⛮轸᫱欥쿥孮ꪬ").intern(), 0);
     */
    /* renamed from: ʼ reason: contains not printable characters */
    static /* synthetic */ boolean m370() {
        int i;
        int i2 = f421 + 107;
        f420 = i2 % 128;
        int i3 = i2 % 2;
        WeakReference<Context> weakReference = ((i) MoatAnalytics.getInstance()).f338;
        Context context = null;
        if (weakReference != null) {
            int i4 = f421 + 3;
            f420 = i4 % 128;
            if (i4 % 2 != 0) {
                Context context2 = (Context) weakReference.get();
                super.hashCode();
                context = context2;
            } else {
                context = (Context) weakReference.get();
            }
        }
        if ((context != null ? 'P' : 'A') != 'A') {
            int i5 = f420 + 9;
            f421 = i5 % 128;
            if (i5 % 2 == 0) {
            }
            i = Global.getInt(context.getContentResolver(), m376("낑ᗰ냰?⮲됖䤘⛮轸᫱欥쿥孮ꪬ").intern(), 0);
        } else {
            i = 0;
        }
        if (i == 1) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: type inference failed for: r10v1 */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000c, code lost:
        if (r10 != 0) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0014, code lost:
        if ((r10 == 0) != false) goto L_0x0033;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: ˎ reason: contains not printable characters */
    private static String m376(String str) {
        int i = f421 + 31;
        f420 = i % 128;
        if (i % 2 != 0) {
        }
        int i2 = f420 + 121;
        f421 = i2 % 128;
        str = (i2 % 2 == 0 ? ':' : 2) != ':' ? str.toCharArray() : str.toCharArray();
        char[] a2 = CamembertauCalvados.a(f416, (char[]) str);
        int i3 = 4;
        while (i3 < a2.length) {
            a2[i3] = (char) ((int) (((long) (a2[i3] ^ a2[i3 % 4])) ^ (((long) (i3 - 4)) * f416)));
            i3++;
            int i4 = f421 + 107;
            f420 = i4 % 128;
            if (i4 % 2 != 0) {
            }
        }
        return new String(a2, 4, a2.length - 4);
    }
}
