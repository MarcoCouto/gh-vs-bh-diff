package com.moat.analytics.mobile.ogury;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import java.lang.ref.WeakReference;

final class i extends MoatAnalytics implements b {

    /* renamed from: ʻ reason: contains not printable characters */
    private MoatOptions f333;

    /* renamed from: ʼ reason: contains not printable characters */
    private String f334;
    @Nullable

    /* renamed from: ˊ reason: contains not printable characters */
    d f335;

    /* renamed from: ˋ reason: contains not printable characters */
    boolean f336 = false;

    /* renamed from: ˎ reason: contains not printable characters */
    boolean f337 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    WeakReference<Context> f338;

    /* renamed from: ॱ reason: contains not printable characters */
    boolean f339 = false;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private boolean f340 = false;

    i() {
    }

    public final void start(Application application) {
        start(new MoatOptions(), application);
    }

    @UiThread
    public final void prepareNativeDisplayTracking(String str) {
        this.f334 = str;
        if (q.m343().f396 != e.f412) {
            try {
                m292();
            } catch (Exception e) {
                l.m310(e);
            }
        }
    }

    @UiThread
    /* renamed from: ॱ reason: contains not printable characters */
    private void m292() {
        if (this.f335 == null) {
            this.f335 = new d(a.m210(), b.f269);
            this.f335.m229(this.f334);
            StringBuilder sb = new StringBuilder("Preparing native display tracking with partner code ");
            sb.append(this.f334);
            AnonymousClass1.m243(3, "Analytics", this, sb.toString());
            StringBuilder sb2 = new StringBuilder("Prepared for native display tracking with partner code ");
            sb2.append(this.f334);
            AnonymousClass1.m247("[SUCCESS] ", sb2.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final boolean m295() {
        return this.f340;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final boolean m294() {
        return this.f333 != null && this.f333.disableLocationServices;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    public final void m293() throws l {
        l.m309();
        m.m318();
        if (this.f334 != null) {
            try {
                m292();
            } catch (Exception e) {
                l.m310(e);
            }
        }
    }

    public final void start(MoatOptions moatOptions, Application application) {
        try {
            if (this.f340) {
                AnonymousClass1.m243(3, "Analytics", this, "Moat SDK has already been started.");
                return;
            }
            this.f333 = moatOptions;
            q.m343().m356();
            this.f336 = moatOptions.disableLocationServices;
            if (application != null) {
                if (moatOptions.loggingEnabled && s.m380(application.getApplicationContext())) {
                    this.f337 = true;
                }
                this.f338 = new WeakReference<>(application.getApplicationContext());
                this.f340 = true;
                this.f339 = moatOptions.autoTrackGMAInterstitials;
                a.m207(application);
                q.m343().m355((b) this);
                if (!moatOptions.disableAdIdCollection) {
                    s.m377((Context) application);
                }
                AnonymousClass1.m247("[SUCCESS] ", "Moat Analytics SDK Version 2.4.3 started");
                return;
            }
            throw new l("Moat Analytics SDK didn't start, application was null");
        } catch (Exception e) {
            l.m310(e);
        }
    }
}
