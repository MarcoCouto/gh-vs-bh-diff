package com.moat.analytics.mobile.ogury;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class c {

    /* renamed from: ʻ reason: contains not printable characters */
    private final v f248;

    /* renamed from: ʼ reason: contains not printable characters */
    private boolean f249;

    /* renamed from: ʽ reason: contains not printable characters */
    private WeakReference<View> f250;

    /* renamed from: ˊ reason: contains not printable characters */
    WeakReference<WebView> f251;

    /* renamed from: ˋ reason: contains not printable characters */
    f f252 = this.f281.f262;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private boolean f253;

    /* renamed from: ˎ reason: contains not printable characters */
    l f254 = null;

    /* renamed from: ˏ reason: contains not printable characters */
    TrackerListener f255;

    /* renamed from: ॱ reason: contains not printable characters */
    final String f256;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private final boolean f257;

    /* renamed from: ᐝ reason: contains not printable characters */
    final boolean f258;

    @Deprecated
    public void setActivity(Activity activity) {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public abstract String m224();

    c(@Nullable View view, boolean z, boolean z2) {
        String str;
        AnonymousClass1.m243(3, "BaseTracker", this, "Initializing.");
        if (z) {
            StringBuilder sb = new StringBuilder("m");
            sb.append(hashCode());
            str = sb.toString();
        } else {
            str = "";
        }
        this.f256 = str;
        this.f250 = new WeakReference<>(view);
        this.f257 = z;
        this.f258 = z2;
        this.f249 = false;
        this.f253 = false;
        this.f248 = new v();
    }

    public void setListener(TrackerListener trackerListener) {
        this.f255 = trackerListener;
    }

    public void removeListener() {
        this.f255 = null;
    }

    public void startTracking() {
        try {
            AnonymousClass1.m243(3, "BaseTracker", this, "In startTracking method.");
            m223();
            if (this.f255 != null) {
                TrackerListener trackerListener = this.f255;
                StringBuilder sb = new StringBuilder("Tracking started on ");
                sb.append(AnonymousClass1.m245((View) this.f250.get()));
                trackerListener.onTrackingStarted(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder("startTracking succeeded for ");
            sb2.append(AnonymousClass1.m245((View) this.f250.get()));
            String obj = sb2.toString();
            AnonymousClass1.m243(3, "BaseTracker", this, obj);
            StringBuilder sb3 = new StringBuilder();
            sb3.append(m224());
            sb3.append(" ");
            sb3.append(obj);
            AnonymousClass1.m247("[SUCCESS] ", sb3.toString());
        } catch (Exception e) {
            m222("startTracking", e);
        }
    }

    @CallSuper
    public void stopTracking() {
        boolean z = false;
        try {
            AnonymousClass1.m243(3, "BaseTracker", this, "In stopTracking method.");
            this.f253 = true;
            if (this.f252 != null) {
                this.f252.m273(this);
                z = true;
            }
        } catch (Exception e) {
            l.m310(e);
        }
        String str = "BaseTracker";
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        AnonymousClass1.m243(3, str, this, sb.toString());
        String str2 = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m224());
        sb2.append(" stopTracking ");
        sb2.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : ParametersKeys.FAILED);
        sb2.append(" for ");
        sb2.append(AnonymousClass1.m245((View) this.f250.get()));
        AnonymousClass1.m247(str2, sb2.toString());
        if (this.f255 != null) {
            this.f255.onTrackingStopped("");
            this.f255 = null;
        }
    }

    @CallSuper
    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(AnonymousClass1.m245(view));
        AnonymousClass1.m243(3, "BaseTracker", this, sb.toString());
        this.f250 = new WeakReference<>(view);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˎ reason: contains not printable characters */
    public void m223() throws l {
        AnonymousClass1.m243(3, "BaseTracker", this, "Attempting to start impression.");
        m225();
        if (this.f249) {
            throw new l("Tracker already started");
        } else if (!this.f253) {
            m218(new ArrayList());
            if (this.f252 != null) {
                this.f252.m270(this);
                this.f249 = true;
                AnonymousClass1.m243(3, "BaseTracker", this, "Impression started.");
                return;
            }
            AnonymousClass1.m243(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new l("Bridge is null");
        } else {
            throw new l("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m221(WebView webView) throws l {
        if (webView != null) {
            this.f251 = new WeakReference<>(webView);
            if (this.f252 == null) {
                if (!(this.f257 || this.f258)) {
                    AnonymousClass1.m243(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f251.get() != null) {
                        this.f252 = new f((WebView) this.f251.get(), a.f306);
                        AnonymousClass1.m243(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f252 = null;
                        AnonymousClass1.m243(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            if (this.f252 != null) {
                this.f252.m272(this);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m225() throws l {
        if (this.f254 != null) {
            StringBuilder sb = new StringBuilder("Tracker initialization failed: ");
            sb.append(this.f254.getMessage());
            throw new l(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final boolean m219() {
        return this.f249 && !this.f253;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱॱ reason: contains not printable characters */
    public final View m226() {
        return (View) this.f250.get();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʽ reason: contains not printable characters */
    public final String m217() {
        this.f248.m400(this.f256, (View) this.f250.get());
        return this.f248.f442;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m222(String str, Exception exc) {
        try {
            l.m310(exc);
            String r3 = l.m308(str, exc);
            if (this.f255 != null) {
                this.f255.onTrackingFailedToStart(r3);
            }
            AnonymousClass1.m243(3, "BaseTracker", this, r3);
            StringBuilder sb = new StringBuilder();
            sb.append(m224());
            sb.append(" ");
            sb.append(r3);
            AnonymousClass1.m247("[ERROR] ", sb.toString());
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m220() throws l {
        if (this.f249) {
            throw new l("Tracker already started");
        } else if (this.f253) {
            throw new l("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˊ reason: contains not printable characters */
    public void m218(List<String> list) throws l {
        if (((View) this.f250.get()) == null && !this.f258) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new l(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻ reason: contains not printable characters */
    public final String m216() {
        return AnonymousClass1.m245((View) this.f250.get());
    }
}
