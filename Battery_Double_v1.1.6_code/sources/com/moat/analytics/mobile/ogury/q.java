package com.moat.analytics.mobile.ogury;

import android.os.Handler;
import android.os.Looper;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class q {

    /* renamed from: ʻ reason: contains not printable characters */
    private static q f386;
    /* access modifiers changed from: private */

    /* renamed from: ʼ reason: contains not printable characters */
    public static final Queue<a> f387 = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */

    /* renamed from: ʽ reason: contains not printable characters */
    public Handler f388;

    /* renamed from: ˊ reason: contains not printable characters */
    volatile int f389 = 10;
    /* access modifiers changed from: private */

    /* renamed from: ˊॱ reason: contains not printable characters */
    public final AtomicInteger f390 = new AtomicInteger(0);

    /* renamed from: ˋ reason: contains not printable characters */
    volatile int f391 = 200;
    /* access modifiers changed from: private */

    /* renamed from: ˋॱ reason: contains not printable characters */
    public volatile long f392 = 0;

    /* renamed from: ˎ reason: contains not printable characters */
    volatile boolean f393 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    volatile boolean f394 = false;
    /* access modifiers changed from: private */

    /* renamed from: ͺ reason: contains not printable characters */
    public final AtomicBoolean f395 = new AtomicBoolean(false);

    /* renamed from: ॱ reason: contains not printable characters */
    volatile int f396 = e.f412;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ reason: contains not printable characters */
    public final AtomicBoolean f397 = new AtomicBoolean(false);
    /* access modifiers changed from: private */

    /* renamed from: ॱॱ reason: contains not printable characters */
    public long f398 = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;

    /* renamed from: ᐝ reason: contains not printable characters */
    private long f399 = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;

    class a {

        /* renamed from: ˋ reason: contains not printable characters */
        final Long f403;

        /* renamed from: ॱ reason: contains not printable characters */
        final b f405;

        a(Long l, b bVar) {
            this.f403 = l;
            this.f405 = bVar;
        }
    }

    interface b {
        /* renamed from: ˋ reason: contains not printable characters */
        void m358() throws l;
    }

    interface c {
        /* renamed from: ॱ reason: contains not printable characters */
        void m359(j jVar) throws l;
    }

    class d implements Runnable {

        /* renamed from: ˋ reason: contains not printable characters */
        private final String f407;
        /* access modifiers changed from: private */

        /* renamed from: ˎ reason: contains not printable characters */
        public final c f408;

        /* renamed from: ॱ reason: contains not printable characters */
        private final Handler f409;

        /* synthetic */ d(q qVar, String str, Handler handler, c cVar, byte b) {
            this(str, handler, cVar);
        }

        private d(String str, Handler handler, c cVar) {
            this.f408 = cVar;
            this.f409 = handler;
            StringBuilder sb = new StringBuilder("https://z.moatads.com/");
            sb.append(str);
            sb.append("/android/");
            sb.append(BuildConfig.REVISION.substring(0, 7));
            sb.append("/status.json");
            this.f407 = sb.toString();
        }

        /* renamed from: ˊ reason: contains not printable characters */
        private String m360() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f407);
            sb.append("?ts=");
            sb.append(System.currentTimeMillis());
            sb.append("&v=2.4.3");
            try {
                return (String) k.m305(sb.toString()).get();
            } catch (Exception unused) {
                return null;
            }
        }

        public final void run() {
            try {
                String r0 = m360();
                final j jVar = new j(r0);
                q.this.f394 = jVar.m301();
                q.this.f393 = jVar.m303();
                q.this.f391 = jVar.m300();
                q.this.f389 = jVar.m302();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            d.this.f408.m359(jVar);
                        } catch (Exception e) {
                            l.m310(e);
                        }
                    }
                });
                q.this.f392 = System.currentTimeMillis();
                q.this.f395.compareAndSet(true, false);
                if (r0 != null) {
                    q.this.f390.set(0);
                } else if (q.this.f390.incrementAndGet() < 10) {
                    q.this.m354(q.this.f398);
                }
            } catch (Exception e) {
                l.m310(e);
            }
            this.f409.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    enum e {
        ;
        

        /* renamed from: ˊ reason: contains not printable characters */
        public static final int f412 = 1;

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f413 = 2;

        static {
            f414 = new int[]{1, 2};
        }

        public static int[] values$160b2863() {
            return (int[]) f414.clone();
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static synchronized q m343() {
        q qVar;
        synchronized (q.class) {
            if (f386 == null) {
                f386 = new q();
            }
            qVar = f386;
        }
        return qVar;
    }

    private q() {
        try {
            this.f388 = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            l.m310(e2);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m356() {
        if (System.currentTimeMillis() - this.f392 > this.f399) {
            m354(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ reason: contains not printable characters */
    public void m354(final long j) {
        if (this.f395.compareAndSet(false, true)) {
            AnonymousClass1.m243(3, "OnOff", this, "Performing status check.");
            new c() {
                public final void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    d dVar = new d(q.this, BuildConfig.NAMESPACE, handler, this, 0);
                    handler.postDelayed(dVar, j);
                    Looper.loop();
                }

                /* renamed from: ॱ reason: contains not printable characters */
                public final void m357(j jVar) throws l {
                    synchronized (q.f387) {
                        boolean z = ((i) MoatAnalytics.getInstance()).f337;
                        if (q.this.f396 != jVar.m304() || (q.this.f396 == e.f412 && z)) {
                            q.this.f396 = jVar.m304();
                            if (q.this.f396 == e.f412 && z) {
                                q.this.f396 = e.f413;
                            }
                            if (q.this.f396 == e.f413) {
                                AnonymousClass1.m243(3, "OnOff", this, "Moat enabled - Version 2.4.3");
                            }
                            for (a aVar : q.f387) {
                                if (q.this.f396 == e.f413) {
                                    aVar.f405.m358();
                                }
                            }
                        }
                        while (!q.f387.isEmpty()) {
                            q.f387.remove();
                        }
                    }
                }
            }.start();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m355(b bVar) throws l {
        if (this.f396 == e.f413) {
            bVar.m358();
            return;
        }
        m350();
        f387.add(new a(Long.valueOf(System.currentTimeMillis()), bVar));
        if (this.f397.compareAndSet(false, true)) {
            this.f388.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (q.f387.size() > 0) {
                            q.m350();
                            q.this.f388.postDelayed(this, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                            return;
                        }
                        q.this.f397.compareAndSet(true, false);
                        q.this.f388.removeCallbacks(this);
                    } catch (Exception e) {
                        l.m310(e);
                    }
                }
            }, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ reason: contains not printable characters */
    public static void m350() {
        synchronized (f387) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator it = f387.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - ((a) it.next()).f403.longValue() >= ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS) {
                    it.remove();
                }
            }
            if (f387.size() >= 15) {
                for (int i = 0; i < 5; i++) {
                    f387.remove();
                }
            }
        }
    }
}
