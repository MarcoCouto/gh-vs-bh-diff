package com.moat.analytics.mobile.ogury;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.ogury.base.functional.Optional;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;

final class u {

    /* renamed from: ॱ reason: contains not printable characters */
    private static final LinkedHashSet<String> f435 = new LinkedHashSet<>();

    u() {
    }

    @NonNull
    /* renamed from: ˎ reason: contains not printable characters */
    static Optional<WebView> m388(ViewGroup viewGroup, boolean z) {
        if (viewGroup == null) {
            try {
                return Optional.empty();
            } catch (Exception unused) {
                return Optional.empty();
            }
        } else if (viewGroup instanceof WebView) {
            return Optional.of((WebView) viewGroup);
        } else {
            LinkedList linkedList = new LinkedList();
            linkedList.add(viewGroup);
            Object obj = null;
            int i = 0;
            while (!linkedList.isEmpty() && i < 100) {
                i++;
                ViewGroup viewGroup2 = (ViewGroup) linkedList.poll();
                int childCount = viewGroup2.getChildCount();
                Object obj2 = obj;
                int i2 = 0;
                while (true) {
                    if (i2 >= childCount) {
                        obj = obj2;
                        break;
                    }
                    View childAt = viewGroup2.getChildAt(i2);
                    if (childAt instanceof WebView) {
                        AnonymousClass1.m243(3, "WebViewHound", childAt, "Found WebView");
                        if (z || m389(String.valueOf(childAt.hashCode()))) {
                            if (obj2 != null) {
                                AnonymousClass1.m243(3, "WebViewHound", childAt, "Ambiguous ad container: multiple WebViews reside within it.");
                                AnonymousClass1.m247("[ERROR] ", "WebAdTracker not created, ambiguous ad container: multiple WebViews reside within it");
                                obj = null;
                                break;
                            }
                            obj2 = (WebView) childAt;
                        }
                    }
                    if (childAt instanceof ViewGroup) {
                        linkedList.add((ViewGroup) childAt);
                    }
                    i2++;
                }
            }
            return Optional.ofNullable(obj);
        }
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static boolean m389(String str) {
        try {
            boolean add = f435.add(str);
            if (f435.size() > 50) {
                Iterator it = f435.iterator();
                for (int i = 0; i < 25 && it.hasNext(); i++) {
                    it.next();
                    it.remove();
                }
            }
            AnonymousClass1.m243(3, "WebViewHound", null, add ? "Newly Found WebView" : "Already Found WebView");
            return add;
        } catch (Exception e) {
            l.m310(e);
            return false;
        }
    }
}
