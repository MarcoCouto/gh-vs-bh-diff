package com.moat.analytics.mobile.ogury;

import android.os.Build.VERSION;
import android.util.Base64;
import android.util.Log;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import java.net.URLEncoder;
import java.util.Locale;

final class l extends Exception {

    /* renamed from: ˋ reason: contains not printable characters */
    private static final Long f353 = Long.valueOf(ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);

    /* renamed from: ˎ reason: contains not printable characters */
    private static Exception f354 = null;

    /* renamed from: ॱ reason: contains not printable characters */
    private static Long f355;

    l(String str) {
        super(str);
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static String m308(String str, Exception exc) {
        if (exc instanceof l) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" failed: ");
            sb.append(exc.getMessage());
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(" failed unexpectedly");
        return sb2.toString();
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static void m310(Exception exc) {
        if (q.m343().f394) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            m307(exc);
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static void m307(Exception exc) {
        String str;
        String str2;
        String str3;
        Long valueOf;
        try {
            if (q.m343().f396 == e.f413) {
                int i = q.m343().f389;
                if (i != 0) {
                    if (i < 100) {
                        double d = (double) i;
                        Double.isNaN(d);
                        if (d / 100.0d < Math.random()) {
                            return;
                        }
                    }
                    String str4 = "";
                    String str5 = "";
                    String str6 = "";
                    String str7 = "";
                    StringBuilder sb = new StringBuilder("https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1");
                    StringBuilder sb2 = new StringBuilder("&zt=");
                    sb2.append(exc instanceof l ? 1 : 0);
                    sb.append(sb2.toString());
                    sb.append("&zr=".concat(String.valueOf(i)));
                    try {
                        StringBuilder sb3 = new StringBuilder("&zm=");
                        sb3.append(exc.getMessage() == null ? "null" : URLEncoder.encode(Base64.encodeToString(exc.getMessage().getBytes("UTF-8"), 0), "UTF-8"));
                        sb.append(sb3.toString());
                        StringBuilder sb4 = new StringBuilder("&k=");
                        sb4.append(URLEncoder.encode(Base64.encodeToString(Log.getStackTraceString(exc).getBytes("UTF-8"), 0), "UTF-8"));
                        sb.append(sb4.toString());
                    } catch (Exception unused) {
                    }
                    String str8 = BuildConfig.NAMESPACE;
                    try {
                        sb.append("&zMoatMMAKv=5e29fb7ac3f5a02776850780700bd118383621e1");
                        str3 = BuildConfig.JMMAK_VERSION;
                        try {
                            a r1 = s.m375();
                            StringBuilder sb5 = new StringBuilder("&zMoatMMAKan=");
                            sb5.append(r1.m384());
                            sb.append(sb5.toString());
                            str2 = r1.m383();
                            try {
                                str = Integer.toString(VERSION.SDK_INT);
                            } catch (Exception unused2) {
                                str = str7;
                                StringBuilder sb6 = new StringBuilder("&d=Android:");
                                sb6.append(str8);
                                sb6.append(":");
                                sb6.append(str2);
                                sb6.append(":-");
                                sb.append(sb6.toString());
                                sb.append("&bo=".concat(String.valueOf(str3)));
                                sb.append("&bd=".concat(String.valueOf(str)));
                                valueOf = Long.valueOf(System.currentTimeMillis());
                                sb.append("&t=".concat(String.valueOf(valueOf)));
                                StringBuilder sb7 = new StringBuilder("&de=");
                                sb7.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                                sb.append(sb7.toString());
                                sb.append("&cs=0");
                                new Thread(sb.toString()) {

                                    /* renamed from: ˏ reason: contains not printable characters */
                                    private /* synthetic */ String f352;

                                    {
                                        this.f352 = r1;
                                    }

                                    public final void run() {
                                        try {
                                            k.m305(this.f352);
                                        } catch (Exception unused) {
                                        }
                                    }
                                }.start();
                                f355 = valueOf;
                                return;
                            }
                        } catch (Exception unused3) {
                            str2 = str5;
                            str = str7;
                            StringBuilder sb62 = new StringBuilder("&d=Android:");
                            sb62.append(str8);
                            sb62.append(":");
                            sb62.append(str2);
                            sb62.append(":-");
                            sb.append(sb62.toString());
                            sb.append("&bo=".concat(String.valueOf(str3)));
                            sb.append("&bd=".concat(String.valueOf(str)));
                            valueOf = Long.valueOf(System.currentTimeMillis());
                            sb.append("&t=".concat(String.valueOf(valueOf)));
                            StringBuilder sb72 = new StringBuilder("&de=");
                            sb72.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                            sb.append(sb72.toString());
                            sb.append("&cs=0");
                            new Thread(sb.toString()) {

                                /* renamed from: ˏ reason: contains not printable characters */
                                private /* synthetic */ String f352;

                                {
                                    this.f352 = r1;
                                }

                                public final void run() {
                                    try {
                                        k.m305(this.f352);
                                    } catch (Exception unused) {
                                    }
                                }
                            }.start();
                            f355 = valueOf;
                            return;
                        }
                    } catch (Exception unused4) {
                        str2 = str5;
                        str3 = str6;
                        str = str7;
                        StringBuilder sb622 = new StringBuilder("&d=Android:");
                        sb622.append(str8);
                        sb622.append(":");
                        sb622.append(str2);
                        sb622.append(":-");
                        sb.append(sb622.toString());
                        sb.append("&bo=".concat(String.valueOf(str3)));
                        sb.append("&bd=".concat(String.valueOf(str)));
                        valueOf = Long.valueOf(System.currentTimeMillis());
                        sb.append("&t=".concat(String.valueOf(valueOf)));
                        StringBuilder sb722 = new StringBuilder("&de=");
                        sb722.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                        sb.append(sb722.toString());
                        sb.append("&cs=0");
                        new Thread(sb.toString()) {

                            /* renamed from: ˏ reason: contains not printable characters */
                            private /* synthetic */ String f352;

                            {
                                this.f352 = r1;
                            }

                            public final void run() {
                                try {
                                    k.m305(this.f352);
                                } catch (Exception unused) {
                                }
                            }
                        }.start();
                        f355 = valueOf;
                        return;
                    }
                    StringBuilder sb6222 = new StringBuilder("&d=Android:");
                    sb6222.append(str8);
                    sb6222.append(":");
                    sb6222.append(str2);
                    sb6222.append(":-");
                    sb.append(sb6222.toString());
                    sb.append("&bo=".concat(String.valueOf(str3)));
                    sb.append("&bd=".concat(String.valueOf(str)));
                    valueOf = Long.valueOf(System.currentTimeMillis());
                    sb.append("&t=".concat(String.valueOf(valueOf)));
                    StringBuilder sb7222 = new StringBuilder("&de=");
                    sb7222.append(String.format(Locale.ROOT, "%.0f", new Object[]{Double.valueOf(Math.floor(Math.random() * Math.pow(10.0d, 12.0d)))}));
                    sb.append(sb7222.toString());
                    sb.append("&cs=0");
                    if (f355 == null || valueOf.longValue() - f355.longValue() > f353.longValue()) {
                        new Thread(sb.toString()) {

                            /* renamed from: ˏ reason: contains not printable characters */
                            private /* synthetic */ String f352;

                            {
                                this.f352 = r1;
                            }

                            public final void run() {
                                try {
                                    k.m305(this.f352);
                                } catch (Exception unused) {
                                }
                            }
                        }.start();
                        f355 = valueOf;
                    }
                    return;
                }
                return;
            }
            f354 = exc;
        } catch (Exception unused5) {
        }
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static void m309() {
        if (f354 != null) {
            m307(f354);
            f354 = null;
        }
    }
}
