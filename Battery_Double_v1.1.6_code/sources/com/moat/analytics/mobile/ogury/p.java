package com.moat.analytics.mobile.ogury;

import android.support.annotation.VisibleForTesting;
import com.moat.analytics.mobile.ogury.base.asserts.Asserts;
import com.moat.analytics.mobile.ogury.base.functional.Optional;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class p<T> implements InvocationHandler {
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public static final Object[] f375 = new Object[0];

    /* renamed from: ʼ reason: contains not printable characters */
    private T f376;

    /* renamed from: ˊ reason: contains not printable characters */
    private final b<T> f377;

    /* renamed from: ˎ reason: contains not printable characters */
    private final Class<T> f378;

    /* renamed from: ˏ reason: contains not printable characters */
    private boolean f379;

    /* renamed from: ॱ reason: contains not printable characters */
    private final LinkedList<d> f380 = new LinkedList<>();

    interface b<T> {
        /* renamed from: ˏ reason: contains not printable characters */
        Optional<T> m340() throws l;
    }

    class d {

        /* renamed from: ˊ reason: contains not printable characters */
        private final LinkedList<Object> f382;
        /* access modifiers changed from: private */

        /* renamed from: ˎ reason: contains not printable characters */
        public final WeakReference[] f383;
        /* access modifiers changed from: private */

        /* renamed from: ˏ reason: contains not printable characters */
        public final Method f384;

        /* synthetic */ d(p pVar, Method method, Object[] objArr, byte b) {
            this(method, objArr);
        }

        private d(Method method, Object... objArr) {
            this.f382 = new LinkedList<>();
            if (objArr == null) {
                objArr = p.f375;
            }
            WeakReference[] weakReferenceArr = new WeakReference[objArr.length];
            int length = objArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Object obj = objArr[i];
                if ((obj instanceof Map) || (obj instanceof Integer) || (obj instanceof Double)) {
                    this.f382.add(obj);
                }
                int i3 = i2 + 1;
                weakReferenceArr[i2] = new WeakReference(obj);
                i++;
                i2 = i3;
            }
            this.f383 = weakReferenceArr;
            this.f384 = method;
        }
    }

    @VisibleForTesting
    private p(b<T> bVar, Class<T> cls) throws l {
        Asserts.checkNotNull(bVar);
        Asserts.checkNotNull(cls);
        this.f377 = bVar;
        this.f378 = cls;
        q.m343().m355((b) new b() {
            /* renamed from: ˋ reason: contains not printable characters */
            public final void m339() throws l {
                p.this.m334();
            }
        });
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static <T> T m335(b<T> bVar, Class<T> cls) throws l {
        ClassLoader classLoader = cls.getClassLoader();
        p pVar = new p(bVar, cls);
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, pVar);
    }

    /* renamed from: ˏ reason: contains not printable characters */
    private static Object m336(Method method) {
        try {
            if (Boolean.TYPE.equals(method.getReturnType())) {
                return Boolean.TRUE;
            }
        } catch (Exception e) {
            l.m310(e);
        }
        return null;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            Class declaringClass = method.getDeclaringClass();
            q r0 = q.m343();
            if (Object.class.equals(declaringClass)) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    return this.f378;
                }
                if (!"toString".equals(name)) {
                    return method.invoke(this, objArr);
                }
                Object invoke = method.invoke(this, objArr);
                return String.valueOf(invoke).replace(p.class.getName(), this.f378.getName());
            } else if (!this.f379 || this.f376 != null) {
                if (r0.f396 == e.f413) {
                    m334();
                    if (this.f376 != null) {
                        return method.invoke(this.f376, objArr);
                    }
                }
                if (r0.f396 == e.f412 && (!this.f379 || this.f376 != null)) {
                    if (this.f380.size() >= 15) {
                        this.f380.remove(5);
                    }
                    this.f380.add(new d(this, method, objArr, 0));
                }
                return m336(method);
            } else {
                this.f380.clear();
                return m336(method);
            }
        } catch (Exception e) {
            l.m310(e);
            return m336(method);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ reason: contains not printable characters */
    public void m334() throws l {
        if (!this.f379) {
            try {
                this.f376 = this.f377.m340().orElse(null);
            } catch (Exception e) {
                AnonymousClass1.m246("OnOffTrackerProxy", this, "Could not create instance", e);
                l.m310(e);
            }
            this.f379 = true;
        }
        if (this.f376 != null) {
            Iterator it = this.f380.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                try {
                    Object[] objArr = new Object[dVar.f383.length];
                    WeakReference[] r3 = dVar.f383;
                    int length = r3.length;
                    int i = 0;
                    int i2 = 0;
                    while (i < length) {
                        int i3 = i2 + 1;
                        objArr[i2] = r3[i].get();
                        i++;
                        i2 = i3;
                    }
                    dVar.f384.invoke(this.f376, objArr);
                } catch (Exception e2) {
                    l.m310(e2);
                }
            }
            this.f380.clear();
        }
    }
}
