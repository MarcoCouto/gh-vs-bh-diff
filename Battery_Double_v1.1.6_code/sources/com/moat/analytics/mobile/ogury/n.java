package com.moat.analytics.mobile.ogury;

interface n<T> {
    T create() throws l;

    T createNoOp();
}
