package com.moat.analytics.mobile.ogury;

import android.os.Build.VERSION;
import org.json.JSONArray;
import org.json.JSONObject;

final class j {

    /* renamed from: ʻ reason: contains not printable characters */
    private static int f341 = 0;

    /* renamed from: ʼ reason: contains not printable characters */
    private static byte[] f342 = {-37, -77, 1, 1, 105, 52, 49, 56, 48, 58, 48, 60, 53, 2, 55, 103, 53, 46, 60, 45, 54, 60, 48, 50, 55, 52, 53, 58, 55, 4, 97, 4, 104, 5, 55, 95, 10, 49, 98, 60, 2, 101, 1, 119, 122, 113, 126, 113, 122, 67, -88, 120, 114, 69, -84, 109, 122, 123, 64, -92, 73, 122, 117, 115, -90, 74, -90, 110, 126, 113, 115, 123, 116, 72, -88, 71, -97, 126, 66, -87, 65, -92, -37, 54, -37, -115, -37, 93, -37, -105, -37, 44, -37, -26};

    /* renamed from: ʽ reason: contains not printable characters */
    private static int f343 = 39;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private static int f344 = 1;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private static int f345 = -1675497915;

    /* renamed from: ᐝ reason: contains not printable characters */
    private static int f346 = 814200731;

    /* renamed from: ˊ reason: contains not printable characters */
    private boolean f347 = false;

    /* renamed from: ˋ reason: contains not printable characters */
    private boolean f348 = false;

    /* renamed from: ˎ reason: contains not printable characters */
    private int f349 = 10;

    /* renamed from: ˏ reason: contains not printable characters */
    private boolean f350 = false;

    /* renamed from: ॱ reason: contains not printable characters */
    private int f351 = 200;

    j(String str) {
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                String string = jSONObject.getString(m298(-40, 0, -814200731, 1675498030, 59).intern());
                boolean equals = string.equals(m298(-40, 0, -814200729, 1675497968, -53).intern());
                boolean equals2 = string.equals(m298(-40, 0, -814200689, 1675497971, -118).intern());
                if ((string.equals(m298(-40, 0, -814200649, 1675498026, -55).intern()) || equals || equals2) && !m296(jSONObject) && !m299(jSONObject)) {
                    this.f350 = true;
                    this.f348 = equals;
                    this.f347 = equals2;
                    if (this.f347) {
                        this.f348 = true;
                    }
                }
                if (jSONObject.has(m298(-40, 0, -814200647, 1675498020, 120).intern())) {
                    int i = jSONObject.getInt(m298(-40, 0, -814200647, 1675498020, 120).intern());
                    if (i >= 100 && i <= 1000) {
                        this.f351 = i;
                    }
                }
                if (jSONObject.has(m298(-40, 0, -814200645, 1675498016, -79).intern())) {
                    this.f349 = jSONObject.getInt(m298(-40, 0, -814200645, 1675498016, -79).intern());
                }
                if (m297(jSONObject)) {
                    ((i) MoatAnalytics.getInstance()).f336 = true;
                }
            } catch (Exception e) {
                this.f350 = false;
                this.f348 = false;
                this.f351 = 200;
                l.m310(e);
            }
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static boolean m296(JSONObject jSONObject) {
        try {
            if (16 > VERSION.SDK_INT) {
                int i = f341 + 111;
                f344 = i % 128;
                int i2 = i % 2;
                int i3 = f344 + 13;
                f341 = i3 % 128;
                if ((i3 % 2 != 0 ? 19 : 'D') != 19) {
                    return true;
                }
                Object obj = null;
                super.hashCode();
                return true;
            }
            if (jSONObject.has(m298(-40, 0, -814200643, 1675498026, 92).intern())) {
                int i4 = f341 + 109;
                f344 = i4 % 128;
                int i5 = i4 % 2;
                JSONArray jSONArray = jSONObject.getJSONArray(m298(-40, 0, -814200643, 1675498026, 92).intern());
                int length = jSONArray.length();
                int i6 = 0;
                while (i6 < length) {
                    int i7 = f341 + 55;
                    f344 = i7 % 128;
                    if ((i7 % 2 == 0 ? 'S' : '*') != 'S') {
                        if ((jSONArray.getInt(i6) == VERSION.SDK_INT ? 'S' : 'T') != 'S') {
                            i6++;
                        }
                    } else if (jSONArray.getInt(i6) != VERSION.SDK_INT) {
                        i6++;
                    }
                    return true;
                }
            }
            return false;
        } catch (Exception unused) {
            return true;
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    private static boolean m299(JSONObject jSONObject) {
        try {
            if ((jSONObject.has(m298(-40, 0, -814200641, 1675498012, -29).intern()) ? '[' : '5') == '[') {
                int i = f341 + 63;
                f344 = i % 128;
                int i2 = i % 2;
                String r6 = s.m375().m383();
                JSONArray jSONArray = jSONObject.getJSONArray(m298(-40, 0, -814200641, 1675498012, -29).intern());
                int length = jSONArray.length();
                for (int i3 = 0; i3 < length; i3++) {
                    if (jSONArray.getString(i3).contentEquals(r6)) {
                        int i4 = f344 + 97;
                        f341 = i4 % 128;
                        if (i4 % 2 != 0) {
                        }
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            l.m310(e);
        }
        int i5 = f341 + 97;
        f344 = i5 % 128;
        if (!(i5 % 2 != 0)) {
            Object obj = null;
            super.hashCode();
        }
        return false;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    private static boolean m297(JSONObject jSONObject) {
        String r5;
        JSONArray jSONArray;
        int length;
        try {
            if (jSONObject.has(m298(-40, 0, -814200639, 1675498012, 37).intern())) {
                int i = f344 + 77;
                f341 = i % 128;
                if (i % 2 != 0) {
                    String r0 = s.m375().m383();
                    jSONArray = jSONObject.getJSONArray(m298(-40, 0, -814200639, 1675498012, 116).intern());
                    length = jSONArray.length();
                    r5 = r0;
                } else {
                    r5 = s.m375().m383();
                    jSONArray = jSONObject.getJSONArray(m298(-40, 0, -814200639, 1675498012, 37).intern());
                    length = jSONArray.length();
                }
                JSONArray jSONArray2 = jSONArray;
                for (int i2 = 0; i2 < length; i2++) {
                    int i3 = f344 + 115;
                    f341 = i3 % 128;
                    if ((i3 % 2 != 0 ? 'R' : 'P') != 'R') {
                        if (!(!jSONArray2.getString(i2).contentEquals(r5))) {
                            return true;
                        }
                    } else {
                        boolean contentEquals = jSONArray2.getString(i2).contentEquals(r5);
                        Object obj = null;
                        super.hashCode();
                        if (!(!contentEquals)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            l.m310(e);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final boolean m301() {
        int i = f341 + 3;
        f344 = i % 128;
        int i2 = i % 2;
        boolean z = this.f348;
        int i3 = f344 + 47;
        f341 = i3 % 128;
        if (i3 % 2 != 0) {
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final boolean m303() {
        int i = f341 + 65;
        f344 = i % 128;
        if (i % 2 == 0) {
        }
        boolean z = this.f347;
        int i2 = f344 + 43;
        f341 = i2 % 128;
        if (i2 % 2 == 0) {
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final int m300() {
        int i = f344 + 25;
        f341 = i % 128;
        if (i % 2 != 0) {
        }
        int i2 = this.f351;
        int i3 = f341 + 91;
        f344 = i3 % 128;
        if ((i3 % 2 == 0 ? 'P' : 5) != 5) {
        }
        return i2;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final int m302() {
        boolean z = true;
        int i = f344 + 1;
        f341 = i % 128;
        int i2 = i % 2;
        int i3 = this.f349;
        int i4 = f344 + 89;
        f341 = i4 % 128;
        if (i4 % 2 == 0) {
            z = false;
        }
        if (z) {
        }
        return i3;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        if (r3.f350 != false) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        return com.moat.analytics.mobile.ogury.q.e.f412;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if ((r3.f350) != false) goto L_0x0023;
     */
    /* renamed from: ॱ reason: contains not printable characters */
    public final int m304() {
        int i = f341 + 51;
        f344 = i % 128;
        if ((i % 2 == 0 ? 24 : '%') != '%') {
        }
        int i2 = e.f413;
        int i3 = f341 + 83;
        f344 = i3 % 128;
        if (i3 % 2 == 0) {
        }
        return i2;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    private static String m298(int i, byte b, int i2, int i3, short s) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        int i4 = i + f343;
        int i5 = 0;
        if (i4 == -1) {
            int i6 = f344 + 99;
            f341 = i6 % 128;
            int i7 = i6 % 2;
            z = true;
        } else {
            z = false;
        }
        short[] sArr = null;
        if (z) {
            if (f342 != null) {
                i4 = (byte) (f342[f346 + i2] + f343);
            } else {
                i4 = (short) (sArr[f346 + i2] + f343);
                int i8 = f341 + 27;
                f344 = i8 % 128;
                if (i8 % 2 == 0) {
                    super.hashCode();
                }
            }
        }
        if (!(i4 <= 0)) {
            int i9 = ((i2 + i4) - 2) + f346;
            if (z) {
                int i10 = f341 + 113;
                f344 = i10 % 128;
                if (!(i10 % 2 == 0)) {
                    i5 = 1;
                }
            }
            int i11 = i9 + i5;
            char c = (char) (i3 + f345);
            sb.append(c);
            for (int i12 = 1; i12 < i4; i12++) {
                if ((f342 != null ? 'S' : 17) != 17) {
                    c = (char) (c + (((byte) (f342[i11] + s)) ^ b));
                    i11--;
                } else {
                    c = (char) (c + (((short) (sArr[i11] + s)) ^ b));
                    i11--;
                }
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
