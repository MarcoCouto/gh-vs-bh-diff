package com.moat.analytics.mobile.ogury;

import android.media.MediaPlayer;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class r extends h implements NativeVideoTracker {

    /* renamed from: ͺ reason: contains not printable characters */
    private WeakReference<MediaPlayer> f415;

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final String m365() {
        return "NativeVideoTracker";
    }

    r(String str) {
        super(str);
        AnonymousClass1.m243(3, "NativeVideoTracker", this, "In initialization method.");
        if (str == null || str.isEmpty()) {
            StringBuilder sb = new StringBuilder("PartnerCode is ");
            sb.append(str == null ? "null" : "empty");
            String obj = sb.toString();
            String concat = "NativeDisplayTracker creation problem, ".concat(String.valueOf(obj));
            AnonymousClass1.m243(3, "NativeVideoTracker", this, concat);
            AnonymousClass1.m247("[ERROR] ", concat);
            this.f254 = new l(obj);
        }
        AnonymousClass1.m247("[SUCCESS] ", "NativeVideoTracker created");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋॱ reason: contains not printable characters */
    public final boolean m364() {
        return (this.f415 == null || this.f415.get() == null) ? false : true;
    }

    public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
        try {
            m225();
            m220();
            if (mediaPlayer != null) {
                mediaPlayer.getCurrentPosition();
                this.f415 = new WeakReference<>(mediaPlayer);
                return super.m287(map, view);
            }
            throw new l("Null player instance");
        } catch (Exception unused) {
            throw new l("Playback has already completed");
        } catch (Exception e) {
            l.m310(e);
            String r2 = l.m308("trackVideoAd", e);
            if (this.f255 != null) {
                this.f255.onTrackingFailedToStart(r2);
            }
            AnonymousClass1.m243(3, "NativeVideoTracker", this, r2);
            AnonymousClass1.m247("[ERROR] ", "NativeVideoTracker ".concat(String.valueOf(r2)));
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ͺ reason: contains not printable characters */
    public final Integer m366() {
        return Integer.valueOf(((MediaPlayer) this.f415.get()).getCurrentPosition());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝॱ reason: contains not printable characters */
    public final boolean m368() {
        return ((MediaPlayer) this.f415.get()).isPlaying();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻॱ reason: contains not printable characters */
    public final Integer m362() {
        return Integer.valueOf(((MediaPlayer) this.f415.get()).getDuration());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public final Map<String, Object> m367() throws l {
        MediaPlayer mediaPlayer = (MediaPlayer) this.f415.get();
        HashMap hashMap = new HashMap();
        hashMap.put("width", Integer.valueOf(mediaPlayer.getVideoWidth()));
        hashMap.put("height", Integer.valueOf(mediaPlayer.getVideoHeight()));
        hashMap.put("duration", Integer.valueOf(mediaPlayer.getDuration()));
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m363(List<String> list) throws l {
        if (!((this.f415 == null || this.f415.get() == null) ? false : true)) {
            list.add("Player is null");
        }
        super.m236(list);
    }
}
