package com.moat.analytics.mobile.ogury;

import android.support.annotation.CallSuper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class h extends e {

    /* renamed from: ˊॱ reason: contains not printable characters */
    private int f320 = Integer.MIN_VALUE;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private int f321 = Integer.MIN_VALUE;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private double f322 = Double.NaN;

    /* renamed from: ͺ reason: contains not printable characters */
    private int f323 = d.f329;

    /* renamed from: ॱˊ reason: contains not printable characters */
    private int f324 = Integer.MIN_VALUE;

    /* renamed from: ॱˋ reason: contains not printable characters */
    private int f325 = 0;

    enum d {
        ;
        

        /* renamed from: ˊ reason: contains not printable characters */
        public static final int f328 = 2;

        /* renamed from: ˋ reason: contains not printable characters */
        public static final int f329 = 1;

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f330 = 3;

        /* renamed from: ˏ reason: contains not printable characters */
        public static final int f331 = 5;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f332 = 4;

        static {
            f327 = new int[]{1, 2, 3, 4, 5};
        }

        public static int[] values$48d63df8() {
            return (int[]) f327.clone();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻॱ reason: contains not printable characters */
    public abstract Integer m285();

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋॱ reason: contains not printable characters */
    public abstract boolean m288();

    /* access modifiers changed from: 0000 */
    /* renamed from: ͺ reason: contains not printable characters */
    public abstract Integer m289();

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝॱ reason: contains not printable characters */
    public abstract boolean m291();

    h(String str) {
        super(str);
    }

    /* renamed from: ˋ reason: contains not printable characters */
    public final boolean m287(Map<String, String> map, View view) {
        try {
            boolean r4 = super.m238(map, view);
            if (!r4) {
                return r4;
            }
            this.f280.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (!h.this.m288() || h.this.m241()) {
                            h.this.m240();
                        } else if (h.this.m290()) {
                            h.this.f280.postDelayed(this, 200);
                        } else {
                            h.this.m240();
                        }
                    } catch (Exception e) {
                        h.this.m240();
                        l.m310(e);
                    }
                }
            }, 200);
            return r4;
        } catch (Exception e) {
            AnonymousClass1.m243(3, "IntervalVideoTracker", this, "Problem with video loop");
            m222("trackVideoAd", e);
            return false;
        }
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            l.m310(e);
        }
    }

    public void setPlayerVolume(Double d2) {
        super.setPlayerVolume(d2);
        this.f322 = m234().doubleValue();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final JSONObject m286(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.f231.equals(MoatAdEvent.f227)) {
            num = moatAdEvent.f231;
        } else {
            try {
                num = m289();
            } catch (Exception unused) {
                num = Integer.valueOf(this.f324);
            }
            moatAdEvent.f231 = num;
        }
        if (moatAdEvent.f231.intValue() < 0 || (moatAdEvent.f231.intValue() == 0 && moatAdEvent.f230 == MoatAdEventType.AD_EVT_COMPLETE && this.f324 > 0)) {
            num = Integer.valueOf(this.f324);
            moatAdEvent.f231 = num;
        }
        if (moatAdEvent.f230 == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.f320 == Integer.MIN_VALUE || !m233(num, Integer.valueOf(this.f320))) {
                this.f323 = d.f332;
                moatAdEvent.f230 = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.f323 = d.f331;
            }
        }
        return super.m235(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ॱᐝ reason: contains not printable characters */
    public final boolean m290() throws l {
        if (!m288() || m241()) {
            return false;
        }
        try {
            int intValue = m289().intValue();
            if (this.f324 >= 0 && intValue < 0) {
                return false;
            }
            this.f324 = intValue;
            if (intValue == 0) {
                return true;
            }
            int intValue2 = m285().intValue();
            boolean r4 = m291();
            double d2 = (double) intValue2;
            Double.isNaN(d2);
            double d3 = d2 / 4.0d;
            double doubleValue = m234().doubleValue();
            MoatAdEventType moatAdEventType = null;
            if (intValue > this.f321) {
                this.f321 = intValue;
            }
            if (this.f320 == Integer.MIN_VALUE) {
                this.f320 = intValue2;
            }
            if (r4) {
                if (this.f323 == d.f329) {
                    moatAdEventType = MoatAdEventType.AD_EVT_START;
                    this.f323 = d.f330;
                } else if (this.f323 == d.f328) {
                    moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                    this.f323 = d.f330;
                } else {
                    double d4 = (double) intValue;
                    Double.isNaN(d4);
                    int floor = ((int) Math.floor(d4 / d3)) - 1;
                    if (floor >= 0 && floor < 3) {
                        MoatAdEventType moatAdEventType2 = f271[floor];
                        if (!this.f272.containsKey(moatAdEventType2)) {
                            this.f272.put(moatAdEventType2, Integer.valueOf(1));
                            moatAdEventType = moatAdEventType2;
                        }
                    }
                }
            } else if (this.f323 != d.f328) {
                moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                this.f323 = d.f328;
            }
            boolean z = moatAdEventType != null;
            if (!z && !Double.isNaN(this.f322) && Math.abs(this.f322 - doubleValue) > 0.05d) {
                moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                z = true;
            }
            if (z) {
                dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), m237()));
            }
            this.f322 = doubleValue;
            this.f325 = 0;
            return true;
        } catch (Exception unused) {
            int i = this.f325;
            this.f325 = i + 1;
            return i < 5;
        }
    }
}
