package com.moat.analytics.mobile.ogury;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.ogury.base.functional.Optional;
import java.lang.ref.WeakReference;
import java.util.Map;

final class o extends MoatFactory {
    o() throws l {
        if (!((i) i.getInstance()).m295()) {
            String str = "Failed to initialize MoatFactory";
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(", SDK was not started");
            String obj = sb.toString();
            AnonymousClass1.m243(3, "Factory", this, obj);
            AnonymousClass1.m247("[ERROR] ", obj);
            throw new l(str);
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull WebView webView) {
        try {
            final WeakReference weakReference = new WeakReference(webView);
            return (WebAdTracker) p.m335(new b<WebAdTracker>() {
                /* renamed from: ˏ reason: contains not printable characters */
                public final Optional<WebAdTracker> m333() {
                    WebView webView = (WebView) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create WebAdTracker for ");
                    sb.append(AnonymousClass1.m245(webView));
                    String obj = sb.toString();
                    AnonymousClass1.m243(3, "Factory", this, obj);
                    AnonymousClass1.m247("[INFO] ", obj);
                    return Optional.of(new w(webView));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            l.m310(e);
            return new b();
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull ViewGroup viewGroup) {
        try {
            final WeakReference weakReference = new WeakReference(viewGroup);
            return (WebAdTracker) p.m335(new b<WebAdTracker>() {
                /* renamed from: ˏ reason: contains not printable characters */
                public final Optional<WebAdTracker> m332() throws l {
                    ViewGroup viewGroup = (ViewGroup) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create WebAdTracker for adContainer ");
                    sb.append(AnonymousClass1.m245(viewGroup));
                    String obj = sb.toString();
                    AnonymousClass1.m243(3, "Factory", this, obj);
                    AnonymousClass1.m247("[INFO] ", obj);
                    return Optional.of(new w(viewGroup));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            l.m310(e);
            return new b();
        }
    }

    public final NativeDisplayTracker createNativeDisplayTracker(@NonNull View view, @NonNull final Map<String, String> map) {
        try {
            final WeakReference weakReference = new WeakReference(view);
            return (NativeDisplayTracker) p.m335(new b<NativeDisplayTracker>() {
                /* renamed from: ˏ reason: contains not printable characters */
                public final Optional<NativeDisplayTracker> m331() {
                    View view = (View) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create NativeDisplayTracker for ");
                    sb.append(AnonymousClass1.m245(view));
                    String obj = sb.toString();
                    AnonymousClass1.m243(3, "Factory", this, obj);
                    AnonymousClass1.m247("[INFO] ", obj);
                    return Optional.of(new t(view, map));
                }
            }, NativeDisplayTracker.class);
        } catch (Exception e) {
            l.m310(e);
            return new d();
        }
    }

    public final NativeVideoTracker createNativeVideoTracker(final String str) {
        try {
            return (NativeVideoTracker) p.m335(new b<NativeVideoTracker>() {
                /* renamed from: ˏ reason: contains not printable characters */
                public final Optional<NativeVideoTracker> m330() {
                    String str = "Attempting to create NativeVideoTracker";
                    AnonymousClass1.m243(3, "Factory", this, str);
                    AnonymousClass1.m247("[INFO] ", str);
                    return Optional.of(new r(str));
                }
            }, NativeVideoTracker.class);
        } catch (Exception e) {
            l.m310(e);
            return new a();
        }
    }

    public final <T> T createCustomTracker(n<T> nVar) {
        try {
            return nVar.create();
        } catch (Exception e) {
            l.m310(e);
            return nVar.createNoOp();
        }
    }
}
