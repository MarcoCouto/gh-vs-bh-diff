package com.moat.analytics.mobile.ogury;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.github.mikephil.charting.utils.Utils;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class m implements LocationListener {

    /* renamed from: ˊ reason: contains not printable characters */
    private static m f356;

    /* renamed from: ʻ reason: contains not printable characters */
    private boolean f357;

    /* renamed from: ʽ reason: contains not printable characters */
    private Location f358;

    /* renamed from: ˋ reason: contains not printable characters */
    private ScheduledFuture<?> f359;

    /* renamed from: ˎ reason: contains not printable characters */
    private ScheduledFuture<?> f360;

    /* renamed from: ˏ reason: contains not printable characters */
    private ScheduledExecutorService f361;

    /* renamed from: ॱ reason: contains not printable characters */
    private LocationManager f362;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private boolean f363;

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static m m318() {
        if (f356 == null) {
            f356 = new m();
        }
        return f356;
    }

    private m() {
        try {
            this.f357 = ((i) MoatAnalytics.getInstance()).f336;
            if (this.f357) {
                AnonymousClass1.m243(3, "LocationManager", this, "Moat location services disabled");
                return;
            }
            this.f361 = Executors.newScheduledThreadPool(1);
            this.f362 = (LocationManager) a.m210().getSystemService("location");
            if (this.f362.getAllProviders().size() == 0) {
                AnonymousClass1.m243(3, "LocationManager", this, "Device has no location providers");
            } else {
                m322();
            }
        } catch (Exception e) {
            l.m310(e);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: ˏ reason: contains not printable characters */
    public final Location m329() {
        if (this.f357 || this.f362 == null) {
            return null;
        }
        return this.f358;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m327() {
        m322();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m328() {
        m323(false);
    }

    public final void onLocationChanged(Location location) {
        String str = "LocationManager";
        try {
            StringBuilder sb = new StringBuilder("Received an updated location = ");
            sb.append(location.toString());
            AnonymousClass1.m243(3, str, this, sb.toString());
            float currentTimeMillis = (float) ((System.currentTimeMillis() - location.getTime()) / 1000);
            if (location.hasAccuracy() && location.getAccuracy() <= 100.0f && currentTimeMillis < 600.0f) {
                this.f358 = m317(this.f358, location);
                AnonymousClass1.m243(3, "LocationManager", this, "fetchCompleted");
                m323(true);
            }
        } catch (Exception e) {
            l.m310(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ reason: contains not printable characters */
    public void m322() {
        try {
            if (!this.f357) {
                if (this.f362 != null) {
                    if (this.f363) {
                        AnonymousClass1.m243(3, "LocationManager", this, "already updating location");
                    }
                    AnonymousClass1.m243(3, "LocationManager", this, "starting location fetch");
                    this.f358 = m317(this.f358, m325());
                    if (this.f358 != null) {
                        StringBuilder sb = new StringBuilder("Have a valid location, won't fetch = ");
                        sb.append(this.f358.toString());
                        AnonymousClass1.m243(3, "LocationManager", this, sb.toString());
                        m314();
                        return;
                    }
                    m313();
                }
            }
        } catch (Exception e) {
            l.m310(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ reason: contains not printable characters */
    public void m323(boolean z) {
        try {
            AnonymousClass1.m243(3, "LocationManager", this, "stopping location fetch");
            m326();
            m312();
            if (z) {
                m314();
            } else {
                m311();
            }
        } catch (Exception e) {
            l.m310(e);
        }
    }

    /* renamed from: ॱॱ reason: contains not printable characters */
    private Location m325() {
        Location location;
        try {
            boolean r1 = m316();
            boolean r2 = m321();
            if (r1 && r2) {
                location = m317(this.f362.getLastKnownLocation("gps"), this.f362.getLastKnownLocation("network"));
            } else if (r1) {
                location = this.f362.getLastKnownLocation("gps");
            } else if (!r2) {
                return null;
            } else {
                location = this.f362.getLastKnownLocation("network");
            }
            return location;
        } catch (SecurityException e) {
            l.m310(e);
            return null;
        }
    }

    /* renamed from: ʽ reason: contains not printable characters */
    private void m313() {
        try {
            if (!this.f363) {
                AnonymousClass1.m243(3, "LocationManager", this, "Attempting to start update");
                if (m316()) {
                    AnonymousClass1.m243(3, "LocationManager", this, "start updating gps location");
                    this.f362.requestLocationUpdates("gps", 0, 0.0f, this, Looper.getMainLooper());
                    this.f363 = true;
                }
                if (m321()) {
                    AnonymousClass1.m243(3, "LocationManager", this, "start updating network location");
                    this.f362.requestLocationUpdates("network", 0, 0.0f, this, Looper.getMainLooper());
                    this.f363 = true;
                }
                if (this.f363) {
                    m312();
                    this.f360 = this.f361.schedule(new Runnable() {
                        public final void run() {
                            try {
                                AnonymousClass1.m243(3, "LocationManager", this, "fetchTimedOut");
                                m.this.m323(true);
                            } catch (Exception e) {
                                l.m310(e);
                            }
                        }
                    }, 60, TimeUnit.SECONDS);
                }
            }
        } catch (SecurityException e) {
            l.m310(e);
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private void m326() {
        try {
            AnonymousClass1.m243(3, "LocationManager", this, "Stopping to update location");
            boolean z = true;
            if (!(ContextCompat.checkSelfPermission(a.m210().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
                if (!(ContextCompat.checkSelfPermission(a.m210().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                    z = false;
                }
            }
            if (z && this.f362 != null) {
                this.f362.removeUpdates(this);
                this.f363 = false;
            }
        } catch (SecurityException e) {
            l.m310(e);
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private void m312() {
        if (this.f360 != null && !this.f360.isCancelled()) {
            this.f360.cancel(true);
            this.f360 = null;
        }
    }

    /* renamed from: ʻ reason: contains not printable characters */
    private void m311() {
        if (this.f359 != null && !this.f359.isCancelled()) {
            this.f359.cancel(true);
            this.f359 = null;
        }
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    private void m314() {
        AnonymousClass1.m243(3, "LocationManager", this, "Resetting fetch timer");
        m311();
        float f = 600.0f;
        if (this.f358 != null) {
            f = Math.max(600.0f - ((float) ((System.currentTimeMillis() - this.f358.getTime()) / 1000)), 0.0f);
        }
        this.f359 = this.f361.schedule(new Runnable() {
            public final void run() {
                try {
                    AnonymousClass1.m243(3, "LocationManager", this, "fetchTimerCompleted");
                    m.this.m322();
                } catch (Exception e) {
                    l.m310(e);
                }
            }
        }, (long) f, TimeUnit.SECONDS);
    }

    /* renamed from: ˎ reason: contains not printable characters */
    private static Location m317(Location location, Location location2) {
        boolean r0 = m315(location);
        boolean r1 = m315(location2);
        if (r0) {
            return (r1 && location.getAccuracy() >= location.getAccuracy()) ? location2 : location;
        }
        if (!r1) {
            return null;
        }
        return location2;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    private static boolean m315(Location location) {
        if (location == null) {
            return false;
        }
        if ((location.getLatitude() != Utils.DOUBLE_EPSILON || location.getLongitude() != Utils.DOUBLE_EPSILON) && location.getAccuracy() >= 0.0f && ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)) < 600.0f) {
            return true;
        }
        return false;
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static boolean m324(Location location, Location location2) {
        if (location == location2) {
            return true;
        }
        return (location == null || location2 == null || location.getTime() != location2.getTime()) ? false : true;
    }

    /* renamed from: ˋॱ reason: contains not printable characters */
    private boolean m316() {
        return (ContextCompat.checkSelfPermission(a.m210().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) && this.f362.getProvider("gps") != null && this.f362.isProviderEnabled("gps");
    }

    /* renamed from: ˏॱ reason: contains not printable characters */
    private boolean m321() {
        boolean z;
        if (!(ContextCompat.checkSelfPermission(a.m210().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
            if (!(ContextCompat.checkSelfPermission(a.m210().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                z = false;
                return !z && this.f362.getProvider("network") != null && this.f362.isProviderEnabled("network");
            }
        }
        z = true;
        if (!z) {
        }
    }
}
