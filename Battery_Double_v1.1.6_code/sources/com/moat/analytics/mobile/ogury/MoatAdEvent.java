package com.moat.analytics.mobile.ogury;

import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf(Utils.DOUBLE_EPSILON);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: ˋ reason: contains not printable characters */
    private static final Double f226 = Double.valueOf(Double.NaN);

    /* renamed from: ˏ reason: contains not printable characters */
    static final Integer f227 = Integer.valueOf(Integer.MIN_VALUE);

    /* renamed from: ʻ reason: contains not printable characters */
    private final Double f228;

    /* renamed from: ˊ reason: contains not printable characters */
    Double f229;

    /* renamed from: ˎ reason: contains not printable characters */
    MoatAdEventType f230;

    /* renamed from: ॱ reason: contains not printable characters */
    Integer f231;

    /* renamed from: ᐝ reason: contains not printable characters */
    private final Long f232;

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d) {
        this.f232 = Long.valueOf(System.currentTimeMillis());
        this.f230 = moatAdEventType;
        this.f229 = d;
        this.f231 = num;
        this.f228 = Double.valueOf(s.m373());
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, f226);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f227, f226);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final Map<String, Object> m204() {
        HashMap hashMap = new HashMap();
        hashMap.put("adVolume", this.f229);
        hashMap.put("playhead", this.f231);
        hashMap.put("aTimeStamp", this.f232);
        hashMap.put("type", this.f230.toString());
        hashMap.put(RequestParameters.DEVICE_VOLUME, this.f228);
        return hashMap;
    }
}
