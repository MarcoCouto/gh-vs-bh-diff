package com.moat.analytics.mobile.ogury;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.ads.AdActivity;
import com.moat.analytics.mobile.ogury.base.functional.Optional;
import java.lang.ref.WeakReference;

final class b {

    /* renamed from: ˊ reason: contains not printable characters */
    private static WeakReference<Activity> f244 = new WeakReference<>(null);
    @Nullable

    /* renamed from: ˏ reason: contains not printable characters */
    private static WebAdTracker f245;

    b() {
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static void m215(Activity activity) {
        try {
            if (q.m343().f396 != e.f412) {
                String name = activity.getClass().getName();
                AnonymousClass1.m243(3, "GMAInterstitialHelper", activity, "Activity name: ".concat(String.valueOf(name)));
                if (!name.contains(AdActivity.CLASS_NAME)) {
                    if (f245 != null) {
                        AnonymousClass1.m243(3, "GMAInterstitialHelper", f244.get(), "Stopping to track GMA interstitial");
                        f245.stopTracking();
                        f245 = null;
                    }
                    f244 = new WeakReference<>(null);
                } else if (f244.get() == null || f244.get() != activity) {
                    View decorView = activity.getWindow().getDecorView();
                    if (decorView instanceof ViewGroup) {
                        Optional r0 = u.m388((ViewGroup) decorView, true);
                        if (r0.isPresent()) {
                            f244 = new WeakReference<>(activity);
                            WebView webView = (WebView) r0.get();
                            AnonymousClass1.m243(3, "GMAInterstitialHelper", f244.get(), "Starting to track GMA interstitial");
                            WebAdTracker createWebAdTracker = MoatFactory.create().createWebAdTracker(webView);
                            f245 = createWebAdTracker;
                            createWebAdTracker.startTracking();
                            return;
                        }
                        AnonymousClass1.m243(3, "GMAInterstitialHelper", activity, "Sorry, no WebView in this activity");
                    }
                }
            }
        } catch (Exception e) {
            l.m310(e);
        }
    }
}
