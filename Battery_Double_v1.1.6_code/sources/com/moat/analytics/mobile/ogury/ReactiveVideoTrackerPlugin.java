package com.moat.analytics.mobile.ogury;

import android.app.Activity;
import android.view.View;
import com.moat.analytics.mobile.ogury.base.functional.Optional;
import java.util.Map;

public class ReactiveVideoTrackerPlugin implements n<ReactiveVideoTracker> {
    /* access modifiers changed from: private */

    /* renamed from: ˎ reason: contains not printable characters */
    public final String f237;

    static class c implements ReactiveVideoTracker {
        public final void changeTargetView(View view) {
        }

        public final void dispatchEvent(MoatAdEvent moatAdEvent) {
        }

        public final void removeListener() {
        }

        public final void removeVideoListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void setPlayerVolume(Double d) {
        }

        public final void setVideoListener(VideoTrackerListener videoTrackerListener) {
        }

        public final void stopTracking() {
        }

        public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
            return false;
        }

        c() {
        }
    }

    public ReactiveVideoTrackerPlugin(String str) {
        this.f237 = str;
    }

    public ReactiveVideoTracker create() throws l {
        return (ReactiveVideoTracker) p.m335(new b<ReactiveVideoTracker>() {
            /* renamed from: ˏ reason: contains not printable characters */
            public final Optional<ReactiveVideoTracker> m206() {
                AnonymousClass1.m247("[INFO] ", "Attempting to create ReactiveVideoTracker");
                return Optional.of(new y(ReactiveVideoTrackerPlugin.this.f237));
            }
        }, ReactiveVideoTracker.class);
    }

    public ReactiveVideoTracker createNoOp() {
        return new c();
    }
}
