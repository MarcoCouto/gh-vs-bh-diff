package com.moat.analytics.mobile.ogury;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class a {

    /* renamed from: ˊ reason: contains not printable characters */
    private static Application f239 = null;

    /* renamed from: ˋ reason: contains not printable characters */
    private static boolean f240 = false;

    /* renamed from: ˎ reason: contains not printable characters */
    static WeakReference<Activity> f241 = null;
    /* access modifiers changed from: private */

    /* renamed from: ˏ reason: contains not printable characters */
    public static int f242 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public static boolean f243 = false;

    /* renamed from: com.moat.analytics.mobile.ogury.a$a reason: collision with other inner class name */
    static class C0055a implements ActivityLifecycleCallbacks {
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        C0055a() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            a.f242 = 1;
        }

        public final void onActivityStarted(Activity activity) {
            try {
                a.f241 = new WeakReference<>(activity);
                a.f242 = 2;
                if (!a.f243) {
                    m214(true);
                }
                a.f243 = true;
                StringBuilder sb = new StringBuilder("Activity started: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m243(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                l.m310(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                a.f241 = new WeakReference<>(activity);
                a.f242 = 3;
                q.m343().m356();
                StringBuilder sb = new StringBuilder("Activity resumed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m243(3, "ActivityState", this, sb.toString());
                if (((i) MoatAnalytics.getInstance()).f339) {
                    b.m215(activity);
                }
            } catch (Exception e) {
                l.m310(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                a.f242 = 4;
                if (a.m213(activity)) {
                    a.f241 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity paused: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m243(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                l.m310(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (a.f242 != 3) {
                    a.f243 = false;
                    m214(false);
                }
                a.f242 = 5;
                if (a.m213(activity)) {
                    a.f241 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity stopped: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m243(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                l.m310(e);
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(a.f242 == 3 || a.f242 == 5)) {
                    if (a.f243) {
                        m214(false);
                    }
                    a.f243 = false;
                }
                a.f242 = 6;
                StringBuilder sb = new StringBuilder("Activity destroyed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                AnonymousClass1.m243(3, "ActivityState", this, sb.toString());
                if (a.m213(activity)) {
                    a.f241 = new WeakReference<>(null);
                }
            } catch (Exception e) {
                l.m310(e);
            }
        }

        /* renamed from: ˎ reason: contains not printable characters */
        private static void m214(boolean z) {
            if (z) {
                AnonymousClass1.m243(3, "ActivityState", null, "App became visible");
                if (q.m343().f396 == e.f413 && !((i) MoatAnalytics.getInstance()).f336) {
                    m.m318().m327();
                }
            } else {
                AnonymousClass1.m243(3, "ActivityState", null, "App became invisible");
                if (q.m343().f396 == e.f413 && !((i) MoatAnalytics.getInstance()).f336) {
                    m.m318().m328();
                }
            }
        }
    }

    a() {
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static void m207(Application application) {
        f239 = application;
        if (!f240) {
            f240 = true;
            f239.registerActivityLifecycleCallbacks(new C0055a());
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static Application m210() {
        return f239;
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static /* synthetic */ boolean m213(Activity activity) {
        return f241 != null && f241.get() == activity;
    }
}
