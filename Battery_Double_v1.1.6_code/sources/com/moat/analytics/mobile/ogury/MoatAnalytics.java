package com.moat.analytics.mobile.ogury;

import android.app.Application;
import android.support.annotation.UiThread;

public abstract class MoatAnalytics {

    /* renamed from: ॱ reason: contains not printable characters */
    private static MoatAnalytics f235;

    @UiThread
    public abstract void prepareNativeDisplayTracking(String str);

    public abstract void start(Application application);

    public abstract void start(MoatOptions moatOptions, Application application);

    public static synchronized MoatAnalytics getInstance() {
        MoatAnalytics moatAnalytics;
        synchronized (MoatAnalytics.class) {
            if (f235 == null) {
                try {
                    f235 = new i();
                } catch (Exception e) {
                    l.m310(e);
                    f235 = new com.moat.analytics.mobile.ogury.NoOp.MoatAnalytics();
                }
            }
            moatAnalytics = f235;
        }
        return moatAnalytics;
    }
}
