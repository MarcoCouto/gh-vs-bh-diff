package com.moat.analytics.mobile.ogury;

import android.app.Activity;
import android.graphics.Rect;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.utils.Utils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

final class v {

    /* renamed from: ʼ reason: contains not printable characters */
    private static int f436 = 0;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private static int f437 = 1;

    /* renamed from: ʻ reason: contains not printable characters */
    private Map<String, Object> f438 = new HashMap();

    /* renamed from: ʽ reason: contains not printable characters */
    private Location f439;

    /* renamed from: ˊ reason: contains not printable characters */
    private d f440 = new d();

    /* renamed from: ˋ reason: contains not printable characters */
    private Rect f441;

    /* renamed from: ˎ reason: contains not printable characters */
    String f442 = "{}";

    /* renamed from: ˏ reason: contains not printable characters */
    private JSONObject f443;

    /* renamed from: ॱ reason: contains not printable characters */
    private Rect f444;

    /* renamed from: ॱॱ reason: contains not printable characters */
    private JSONObject f445;

    /* renamed from: ᐝ reason: contains not printable characters */
    private JSONObject f446;

    static class c {

        /* renamed from: ˊ reason: contains not printable characters */
        int f447 = 0;

        /* renamed from: ˋ reason: contains not printable characters */
        final Set<Rect> f448 = new HashSet();

        /* renamed from: ॱ reason: contains not printable characters */
        boolean f449 = false;

        c() {
        }
    }

    static class d {

        /* renamed from: ˋ reason: contains not printable characters */
        double f450 = Utils.DOUBLE_EPSILON;

        /* renamed from: ˏ reason: contains not printable characters */
        Rect f451 = new Rect(0, 0, 0, 0);

        /* renamed from: ॱ reason: contains not printable characters */
        double f452 = Utils.DOUBLE_EPSILON;

        d() {
        }
    }

    static class e {

        /* renamed from: ˊ reason: contains not printable characters */
        final Rect f453;

        /* renamed from: ˋ reason: contains not printable characters */
        final View f454;

        e(View view, e eVar) {
            this.f454 = view;
            if (eVar != null) {
                int i = eVar.f453.left;
                int left = i + view.getLeft();
                int top = eVar.f453.top + view.getTop();
                this.f453 = new Rect(left, top, view.getWidth() + left, view.getHeight() + top);
                return;
            }
            this.f453 = v.m390(view);
        }
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static /* synthetic */ Rect m390(View view) {
        int i = f436 + 35;
        f437 = i % 128;
        return i % 2 != 0 ? m394(view) : m394(view);
    }

    v() {
    }

    /* JADX WARNING: type inference failed for: r10v1, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v10, types: [java.util.Map] */
    /* JADX WARNING: type inference failed for: r10v3, types: [org.json.JSONObject] */
    /* JADX WARNING: type inference failed for: r5v11, types: [java.util.Map, java.util.HashMap] */
    /* JADX WARNING: type inference failed for: r5v12 */
    /* JADX WARNING: type inference failed for: r10v4 */
    /* JADX WARNING: type inference failed for: r5v18 */
    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0224, code lost:
        if (r13.f451.equals(r1.f440.f451) != false) goto L_0x0226;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0284, code lost:
        if (r9.equals(r1.f441) == false) goto L_0x0286;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
        if (r19.getWindowToken() != null) goto L_0x007f;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x01fd  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0200  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0204 A[SYNTHETIC, Splitter:B:122:0x0204] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x021a A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0230 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0231 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x0276 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0279 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x027d A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x027e A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x02b7 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02ba A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x02bd A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0356 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005a A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0084 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0087 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008b A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0096 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0098 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009b A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x009c A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b3 A[SYNTHETIC, Splitter:B:52:0x00b3] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00f1 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00f3 A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x013d A[Catch:{ Exception -> 0x0357 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0167 A[SYNTHETIC, Splitter:B:84:0x0167] */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m400(String str, View view) {
        DisplayMetrics displayMetrics;
        boolean z;
        boolean z2;
        float f;
        int i;
        int i2;
        Rect rect;
        d dVar;
        int width;
        boolean z3;
        boolean z4;
        Location r0;
        ? r5;
        int i3;
        View view2 = view;
        HashMap hashMap = new HashMap();
        String str2 = "{}";
        if (view2 != null) {
            if (VERSION.SDK_INT >= 17) {
                if (!(a.f241 == null)) {
                    int i4 = f436 + 95;
                    f437 = i4 % 128;
                    int i5 = i4 % 2;
                    Activity activity = (Activity) a.f241.get();
                    if (activity != null) {
                        displayMetrics = new DisplayMetrics();
                        activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
                        if (VERSION.SDK_INT < 19) {
                            if (view2 != null) {
                                if ((view.isAttachedToWindow() ? '%' : '!') != '!') {
                                    int i6 = f437 + 21;
                                    f436 = i6 % 128;
                                    int i7 = i6 % 2;
                                    z = true;
                                    boolean z5 = (view2 != null ? 'G' : '>') == '>' && view.hasWindowFocus();
                                    if (!(view2 == null)) {
                                        if (view.isShown()) {
                                            z2 = false;
                                            if (view2 != null) {
                                                int i8 = f437 + 65;
                                                f436 = i8 % 128;
                                                int i9 = i8 % 2;
                                                f = 0.0f;
                                            } else {
                                                f = m395(view);
                                            }
                                            hashMap.put("dr", Float.valueOf(displayMetrics.density));
                                            hashMap.put("dv", Double.valueOf(s.m373()));
                                            hashMap.put("adKey", str);
                                            String str3 = "isAttached";
                                            if (!z) {
                                                int i10 = f437 + 19;
                                                f436 = i10 % 128;
                                                int i11 = i10 % 2;
                                                i = 1;
                                            } else {
                                                i = 0;
                                            }
                                            hashMap.put(str3, Integer.valueOf(i));
                                            hashMap.put("inFocus", Integer.valueOf(!z5 ? 1 : 0));
                                            String str4 = "isHidden";
                                            if (!z2) {
                                                int i12 = f437 + 25;
                                                f436 = i12 % 128;
                                                int i13 = i12 % 2;
                                                i2 = 1;
                                            } else {
                                                i2 = 0;
                                            }
                                            hashMap.put(str4, Integer.valueOf(i2));
                                            hashMap.put("opacity", Float.valueOf(f));
                                            Rect rect2 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                            if (view2 == null) {
                                                int i14 = f437 + 61;
                                                f436 = i14 % 128;
                                                rect = i14 % 2 != 0 ? m394(view) : m394(view);
                                            } else {
                                                rect = new Rect(0, 0, 0, 0);
                                            }
                                            dVar = new d();
                                            width = rect.width() * rect.height();
                                            Object obj = 0;
                                            if (view2 != null && z && z5 && !z2) {
                                                int i15 = f437 + 37;
                                                f436 = i15 % 128;
                                                int i16 = i15 % 2;
                                                if (width > 0) {
                                                    Rect rect3 = new Rect(0, 0, 0, 0);
                                                    if (m399(view2, rect3)) {
                                                        int width2 = rect3.width() * rect3.height();
                                                        if (width2 < width) {
                                                            int i17 = f437 + 49;
                                                            f436 = i17 % 128;
                                                            if (!(i17 % 2 != 0)) {
                                                                AnonymousClass1.m244("VisibilityInfo", obj, "Ad is clipped");
                                                            } else {
                                                                AnonymousClass1.m244("VisibilityInfo", obj, "Ad is clipped");
                                                                super.hashCode();
                                                            }
                                                        }
                                                        if (view.getRootView() instanceof ViewGroup) {
                                                            dVar.f451 = rect3;
                                                            c r02 = m398(rect3, view2);
                                                            if (r02.f449) {
                                                                dVar.f450 = 1.0d;
                                                            } else {
                                                                int r03 = m396(rect3, r02.f448);
                                                                if (r03 > 0) {
                                                                    int i18 = f436 + 59;
                                                                    f437 = i18 % 128;
                                                                    int i19 = i18 % 2;
                                                                    double d2 = (double) r03;
                                                                    double d3 = (double) width2;
                                                                    Double.isNaN(d2);
                                                                    Double.isNaN(d3);
                                                                    dVar.f450 = d2 / d3;
                                                                }
                                                                double d4 = (double) (width2 - r03);
                                                                double d5 = (double) width;
                                                                Double.isNaN(d4);
                                                                Double.isNaN(d5);
                                                                dVar.f452 = d4 / d5;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (this.f443 != null && dVar.f452 == this.f440.f452) {
                                                i3 = f436 + 93;
                                                f437 = i3 % 128;
                                                if ((i3 % 2 != 0 ? '1' : 'Y') == 'Y') {
                                                    if ((dVar.f451.equals(this.f440.f451) ? ')' : '$') != '$') {
                                                    }
                                                }
                                                if (dVar.f450 != this.f440.f450) {
                                                    z3 = false;
                                                    hashMap.put("coveredPercent", Double.valueOf(dVar.f450));
                                                    if (this.f445 == null || !rect2.equals(this.f444)) {
                                                        this.f444 = rect2;
                                                        this.f445 = new JSONObject(m393(m392(rect2, displayMetrics)));
                                                        z3 = true;
                                                    }
                                                    z4 = true;
                                                    if (this.f446 == null) {
                                                    }
                                                    this.f441 = rect;
                                                    this.f446 = new JSONObject(m393(m392(rect, displayMetrics)));
                                                    z3 = true;
                                                    if (this.f438 == null || !hashMap.equals(this.f438)) {
                                                        this.f438 = hashMap;
                                                        z3 = true;
                                                    }
                                                    r0 = m.m318().m329();
                                                    if (m.m324(r0, this.f439)) {
                                                        this.f439 = r0;
                                                    } else {
                                                        z4 = z3;
                                                    }
                                                    if (!z4) {
                                                        JSONObject jSONObject = new JSONObject(this.f438);
                                                        jSONObject.accumulate("screen", this.f445);
                                                        jSONObject.accumulate(ParametersKeys.VIEW, this.f446);
                                                        jSONObject.accumulate(String.VISIBLE, this.f443);
                                                        jSONObject.accumulate("maybe", this.f443);
                                                        jSONObject.accumulate("visiblePercent", Double.valueOf(this.f440.f452));
                                                        if ((r0 != null ? 21 : 'V') == 21) {
                                                            String str5 = "location";
                                                            if (r0 == null) {
                                                                r5 = obj;
                                                            } else {
                                                                ? hashMap2 = new HashMap();
                                                                hashMap2.put(LocationConst.LATITUDE, Double.toString(r0.getLatitude()));
                                                                hashMap2.put(LocationConst.LONGITUDE, Double.toString(r0.getLongitude()));
                                                                hashMap2.put("timestamp", Long.toString(r0.getTime()));
                                                                hashMap2.put("horizontalAccuracy", Float.toString(r0.getAccuracy()));
                                                                r5 = hashMap2;
                                                            }
                                                            if ((r5 == 0 ? 29 : 'U') != 29) {
                                                                obj = new JSONObject(r5);
                                                            }
                                                            jSONObject.accumulate(str5, obj);
                                                        }
                                                        String obj2 = jSONObject.toString();
                                                        try {
                                                            this.f442 = obj2;
                                                            return;
                                                        } catch (Exception e2) {
                                                            e = e2;
                                                            str2 = obj2;
                                                            l.m310(e);
                                                            this.f442 = str2;
                                                        }
                                                    } else {
                                                        return;
                                                    }
                                                }
                                            }
                                            this.f440 = dVar;
                                            this.f443 = new JSONObject(m393(m392(this.f440.f451, displayMetrics)));
                                            z3 = true;
                                            hashMap.put("coveredPercent", Double.valueOf(dVar.f450));
                                            this.f444 = rect2;
                                            this.f445 = new JSONObject(m393(m392(rect2, displayMetrics)));
                                            z3 = true;
                                            z4 = true;
                                            if (this.f446 == null) {
                                            }
                                            this.f441 = rect;
                                            this.f446 = new JSONObject(m393(m392(rect, displayMetrics)));
                                            z3 = true;
                                            this.f438 = hashMap;
                                            z3 = true;
                                            r0 = m.m318().m329();
                                            if (m.m324(r0, this.f439)) {
                                            }
                                            if (!z4) {
                                            }
                                        }
                                    }
                                    z2 = true;
                                    if (view2 != null) {
                                    }
                                    hashMap.put("dr", Float.valueOf(displayMetrics.density));
                                    hashMap.put("dv", Double.valueOf(s.m373()));
                                    hashMap.put("adKey", str);
                                    String str32 = "isAttached";
                                    if (!z) {
                                    }
                                    hashMap.put(str32, Integer.valueOf(i));
                                    hashMap.put("inFocus", Integer.valueOf(!z5 ? 1 : 0));
                                    String str42 = "isHidden";
                                    if (!z2) {
                                    }
                                    hashMap.put(str42, Integer.valueOf(i2));
                                    hashMap.put("opacity", Float.valueOf(f));
                                    Rect rect22 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                                    if (view2 == null) {
                                    }
                                    dVar = new d();
                                    width = rect.width() * rect.height();
                                    Object obj3 = 0;
                                    int i152 = f437 + 37;
                                    f436 = i152 % 128;
                                    int i162 = i152 % 2;
                                    if (width > 0) {
                                    }
                                    i3 = f436 + 93;
                                    f437 = i3 % 128;
                                    if ((i3 % 2 != 0 ? '1' : 'Y') == 'Y') {
                                    }
                                    if (dVar.f450 != this.f440.f450) {
                                    }
                                }
                            }
                        } else if (view2 != null) {
                        }
                        z = false;
                        if ((view2 != null ? 'G' : '>') == '>') {
                        }
                        if (!(view2 == null)) {
                        }
                        z2 = true;
                        if (view2 != null) {
                        }
                        hashMap.put("dr", Float.valueOf(displayMetrics.density));
                        hashMap.put("dv", Double.valueOf(s.m373()));
                        hashMap.put("adKey", str);
                        String str322 = "isAttached";
                        if (!z) {
                        }
                        hashMap.put(str322, Integer.valueOf(i));
                        hashMap.put("inFocus", Integer.valueOf(!z5 ? 1 : 0));
                        String str422 = "isHidden";
                        if (!z2) {
                        }
                        hashMap.put(str422, Integer.valueOf(i2));
                        hashMap.put("opacity", Float.valueOf(f));
                        Rect rect222 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                        if (view2 == null) {
                        }
                        dVar = new d();
                        width = rect.width() * rect.height();
                        Object obj32 = 0;
                        int i1522 = f437 + 37;
                        f436 = i1522 % 128;
                        int i1622 = i1522 % 2;
                        if (width > 0) {
                        }
                        i3 = f436 + 93;
                        f437 = i3 % 128;
                        if ((i3 % 2 != 0 ? '1' : 'Y') == 'Y') {
                        }
                        if (dVar.f450 != this.f440.f450) {
                        }
                    }
                }
            }
            displayMetrics = view.getContext().getResources().getDisplayMetrics();
            if (VERSION.SDK_INT < 19) {
            }
            z = false;
            if ((view2 != null ? 'G' : '>') == '>') {
            }
            if (!(view2 == null)) {
            }
            z2 = true;
            if (view2 != null) {
            }
            hashMap.put("dr", Float.valueOf(displayMetrics.density));
            hashMap.put("dv", Double.valueOf(s.m373()));
            hashMap.put("adKey", str);
            String str3222 = "isAttached";
            if (!z) {
            }
            try {
                hashMap.put(str3222, Integer.valueOf(i));
                hashMap.put("inFocus", Integer.valueOf(!z5 ? 1 : 0));
                String str4222 = "isHidden";
                if (!z2) {
                }
                hashMap.put(str4222, Integer.valueOf(i2));
                hashMap.put("opacity", Float.valueOf(f));
                Rect rect2222 = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                if (view2 == null) {
                }
                dVar = new d();
                width = rect.width() * rect.height();
                Object obj322 = 0;
                int i15222 = f437 + 37;
                f436 = i15222 % 128;
                int i16222 = i15222 % 2;
                if (width > 0) {
                }
                i3 = f436 + 93;
                f437 = i3 % 128;
                if ((i3 % 2 != 0 ? '1' : 'Y') == 'Y') {
                }
                if (dVar.f450 != this.f440.f450) {
                }
            } catch (Exception e3) {
                e = e3;
                l.m310(e);
                this.f442 = str2;
            }
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    private static float m395(View view) {
        float alpha = view.getAlpha();
        int i = f437 + 73;
        f436 = i % 128;
        if (i % 2 != 0) {
        }
        while (true) {
            if ((view != null ? 'X' : 19) == 19) {
                break;
            }
            int i2 = f437 + 85;
            f436 = i2 % 128;
            int i3 = i2 % 2;
            if (view.getParent() == null) {
                break;
            }
            int i4 = f437 + 51;
            f436 = i4 % 128;
            int i5 = i4 % 2;
            if (((double) alpha) == Utils.DOUBLE_EPSILON) {
                break;
            }
            int i6 = f437 + 83;
            f436 = i6 % 128;
            int i7 = i6 % 2;
            if (!(view.getParent() instanceof View)) {
                break;
            }
            alpha *= ((View) view.getParent()).getAlpha();
            view = (View) view.getParent();
        }
        int i8 = f436 + 17;
        f437 = i8 % 128;
        if (i8 % 2 == 0) {
        }
        return alpha;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        r3 = new android.graphics.Rect(0, 0, 0, 0);
        r0 = f436 + 105;
        f437 = r0 % 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        if ((r0 % 2) != 0) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (r1 != false) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        if (r3 != null) goto L_0x001c;
     */
    /* renamed from: ॱ reason: contains not printable characters */
    static Rect m397(View view) {
        int i = f437 + 17;
        f436 = i % 128;
        boolean z = true;
        if (!(i % 2 == 0)) {
            if (view == null) {
                z = false;
            }
        }
        return m394(view);
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00a6 A[EDGE_INSN: B:82:0x00a6->B:49:0x00a6 ?: BREAK  
EDGE_INSN: B:82:0x00a6->B:49:0x00a6 ?: BREAK  
EDGE_INSN: B:82:0x00a6->B:49:0x00a6 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00a6 A[EDGE_INSN: B:82:0x00a6->B:49:0x00a6 ?: BREAK  
EDGE_INSN: B:82:0x00a6->B:49:0x00a6 ?: BREAK  , SYNTHETIC] */
    /* renamed from: ˊ reason: contains not printable characters */
    private static void m391(e eVar, Rect rect, c cVar) {
        boolean z;
        Rect rect2;
        boolean z2;
        int childCount;
        int i;
        View view = eVar.f454;
        if ((view.isShown()) && ((double) view.getAlpha()) > Utils.DOUBLE_EPSILON) {
            if (eVar.f454 instanceof ViewGroup) {
                boolean equals = ViewGroup.class.equals(eVar.f454.getClass().getSuperclass());
                View view2 = eVar.f454;
                if (VERSION.SDK_INT >= 19) {
                    if (!(view2.getBackground() == null)) {
                        if (view2.getBackground().getAlpha() != 0) {
                            z2 = false;
                            if (equals) {
                                if (z2) {
                                    z = false;
                                    ViewGroup viewGroup = (ViewGroup) eVar.f454;
                                    childCount = viewGroup.getChildCount();
                                    i = 0;
                                    while (true) {
                                        if ((i < childCount ? 16 : ',') == ',') {
                                            break;
                                        }
                                        int i2 = cVar.f447 + 1;
                                        cVar.f447 = i2;
                                        if (i2 <= 500) {
                                            m391(new e(viewGroup.getChildAt(i), eVar), rect, cVar);
                                            if (!cVar.f449) {
                                                i++;
                                            } else {
                                                return;
                                            }
                                        } else {
                                            return;
                                        }
                                    }
                                }
                            }
                            z = true;
                            ViewGroup viewGroup2 = (ViewGroup) eVar.f454;
                            childCount = viewGroup2.getChildCount();
                            i = 0;
                            while (true) {
                                if ((i < childCount ? 16 : ',') == ',') {
                                }
                                i++;
                            }
                        }
                    }
                    int i3 = f437 + 59;
                    f436 = i3 % 128;
                    int i4 = i3 % 2;
                }
                z2 = true;
                if (equals) {
                }
                z = true;
                ViewGroup viewGroup22 = (ViewGroup) eVar.f454;
                childCount = viewGroup22.getChildCount();
                i = 0;
                while (true) {
                    if ((i < childCount ? 16 : ',') == ',') {
                    }
                    i++;
                }
            } else {
                z = true;
            }
            if (z) {
                int i5 = f437 + 33;
                f436 = i5 % 128;
                int i6 = i5 % 2;
                Rect rect3 = eVar.f453;
                if (rect3.setIntersect(rect, rect3)) {
                    int i7 = f436 + 89;
                    f437 = i7 % 128;
                    int i8 = i7 % 2;
                    if (VERSION.SDK_INT >= 22) {
                        Rect rect4 = new Rect(0, 0, 0, 0);
                        if ((m399(eVar.f454, rect4) ? 'A' : '`') == 'A') {
                            int i9 = f436 + 61;
                            f437 = i9 % 128;
                            if (i9 % 2 == 0) {
                                rect2 = eVar.f453;
                                if (!rect2.setIntersect(rect4, rect2)) {
                                    return;
                                }
                            } else {
                                rect2 = eVar.f453;
                                if (!rect2.setIntersect(rect4, rect2)) {
                                    return;
                                }
                            }
                        } else {
                            return;
                        }
                    } else {
                        rect2 = rect3;
                    }
                    if (q.m343().f393) {
                        int i10 = f437 + 39;
                        f436 = i10 % 128;
                        int i11 = i10 % 2;
                        AnonymousClass1.m244("VisibilityInfo", eVar.f454, String.format(Locale.ROOT, "Covered by %s-%s alpha=%f", new Object[]{eVar.f454.getClass().getName(), rect2.toString(), Float.valueOf(eVar.f454.getAlpha())}));
                    }
                    cVar.f448.add(rect2);
                    if (rect2.contains(rect)) {
                        cVar.f449 = true;
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0047, code lost:
        r3 = f437 + 65;
        f436 = r3 % 128;
        r3 = r3 % 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        com.moat.analytics.mobile.ogury.e.AnonymousClass1.m243(3, "VisibilityInfo", r8, "Short-circuiting chain retrieval, reached max");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00bb, code lost:
        r0 = f437 + 73;
        f436 = r0 % 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c4, code lost:
        if ((r0 % 2) == 0) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        com.moat.analytics.mobile.ogury.e.AnonymousClass1.m243(5, "VisibilityInfo", r8, "Short-circuiting cover retrieval, reached max");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00cf, code lost:
        com.moat.analytics.mobile.ogury.e.AnonymousClass1.m243(r6, "VisibilityInfo", r8, "Short-circuiting cover retrieval, reached max");
     */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0146 A[Catch:{ Exception -> 0x0191 }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x015c  */
    @VisibleForTesting
    /* renamed from: ॱ reason: contains not printable characters */
    private static c m398(Rect rect, @NonNull View view) {
        int i;
        e eVar;
        int i2;
        boolean z;
        char c2;
        char c3;
        c cVar = new c();
        try {
            ArrayDeque arrayDeque = new ArrayDeque();
            View view2 = view;
            int i3 = 0;
            while (true) {
                i = 3;
                eVar = null;
                i2 = 1;
                if (view2.getParent() == null) {
                    int i4 = f437 + 49;
                    f436 = i4 % 128;
                    if ((i4 % 2 != 0 ? '.' : 7) != '.') {
                        if (view2 != view.getRootView()) {
                            break;
                        }
                    } else {
                        View rootView = view.getRootView();
                        super.hashCode();
                        if ((view2 == rootView ? (char) 2 : 25) != 2) {
                            break;
                        }
                    }
                }
                i3++;
                if (i3 > 50) {
                    break;
                }
                arrayDeque.add(view2);
                if (!(view2.getParent() instanceof View)) {
                    break;
                }
                Rect rect2 = rect;
                View view3 = view;
                int i5 = f436 + 109;
                f437 = i5 % 128;
                if (i5 % 2 == 0) {
                    view2 = (View) view2.getParent();
                    super.hashCode();
                } else {
                    view2 = (View) view2.getParent();
                }
            }
            if (arrayDeque.isEmpty()) {
                return cVar;
            }
            AnonymousClass1.m244("VisibilityInfo", view, "starting covering rect search");
            e eVar2 = eVar;
            loop1:
            while (true) {
                if (arrayDeque.isEmpty()) {
                    break;
                }
                View view4 = (View) arrayDeque.pollLast();
                e eVar3 = new e(view4, eVar2);
                if (view4.getParent() != null && (view4.getParent() instanceof ViewGroup)) {
                    ViewGroup viewGroup = (ViewGroup) view4.getParent();
                    int childCount = viewGroup.getChildCount();
                    int i6 = 0;
                    boolean z2 = false;
                    while (i6 < childCount) {
                        int i7 = f437 + 91;
                        f436 = i7 % 128;
                        int i8 = i7 % 2;
                        if (cVar.f447 >= 500) {
                            break loop1;
                        }
                        View childAt = viewGroup.getChildAt(i6);
                        if (childAt == view4) {
                            z2 = true;
                        } else {
                            cVar.f447 += i2;
                            if ((z2 ? 'H' : 'Z') == 'Z') {
                                if (VERSION.SDK_INT >= 21) {
                                    int i9 = f436 + 5;
                                    f437 = i9 % 128;
                                    int i10 = i9 % 2;
                                    if (childAt.getZ() > view4.getZ()) {
                                    }
                                }
                                z = false;
                                if (z) {
                                }
                            } else if (VERSION.SDK_INT >= 21 && childAt.getZ() < view4.getZ()) {
                                int i11 = f437 + 89;
                                f436 = i11 % 128;
                                if (i11 % 2 != 0) {
                                    c2 = 'E';
                                    c3 = '1';
                                } else {
                                    c3 = '1';
                                    c2 = '1';
                                }
                                if (c2 != c3) {
                                    super.hashCode();
                                }
                                z = false;
                                if (z) {
                                    m391(new e(childAt, eVar2), rect, cVar);
                                    i2 = 1;
                                    if (!(!cVar.f449)) {
                                        return cVar;
                                    }
                                    i6++;
                                    i = 3;
                                } else {
                                    i2 = 1;
                                }
                            }
                            z = true;
                            if (z) {
                            }
                        }
                        Rect rect3 = rect;
                        i6++;
                        i = 3;
                    }
                    continue;
                }
                Rect rect4 = rect;
                eVar2 = eVar3;
                i = 3;
            }
            return cVar;
        } catch (Exception e2) {
            l.m310(e2);
        }
    }

    @VisibleForTesting
    /* renamed from: ˏ reason: contains not printable characters */
    private static int m396(Rect rect, Set<Rect> set) {
        int i;
        int i2 = f436 + 41;
        f437 = i2 % 128;
        if (!(i2 % 2 == 0)) {
            if (set.isEmpty()) {
                return 0;
            }
            i = 0;
        } else if (set.isEmpty()) {
            return 1;
        } else {
            i = 1;
        }
        ArrayList<Rect> arrayList = new ArrayList<>();
        arrayList.addAll(set);
        Collections.sort(arrayList, new Comparator<Rect>() {
            public final /* synthetic */ int compare(Object obj, Object obj2) {
                return Integer.valueOf(((Rect) obj).top).compareTo(Integer.valueOf(((Rect) obj2).top));
            }
        });
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (true) {
            if ((it.hasNext() ? (char) 3 : 21) != 3) {
                break;
            }
            Rect rect2 = (Rect) it.next();
            arrayList2.add(Integer.valueOf(rect2.left));
            arrayList2.add(Integer.valueOf(rect2.right));
        }
        Collections.sort(arrayList2);
        int i3 = f436 + 29;
        f437 = i3 % 128;
        int i4 = i3 % 2;
        int i5 = i;
        int i6 = 0;
        while (true) {
            if (i6 >= arrayList2.size() - 1) {
                return i5;
            }
            int i7 = i6 + 1;
            if (!((Integer) arrayList2.get(i6)).equals(arrayList2.get(i7))) {
                Rect rect3 = new Rect(((Integer) arrayList2.get(i6)).intValue(), rect.top, ((Integer) arrayList2.get(i7)).intValue(), rect.bottom);
                int i8 = rect.top;
                for (Rect rect4 : arrayList) {
                    if (Rect.intersects(rect4, rect3)) {
                        if (!(rect4.bottom <= i8)) {
                            i5 += rect3.width() * (rect4.bottom - Math.max(i8, rect4.top));
                            i8 = rect4.bottom;
                        }
                        if (rect4.bottom == rect3.bottom) {
                            break;
                        }
                    }
                }
            }
            i6 = i7;
        }
    }

    /* renamed from: ˋ reason: contains not printable characters */
    private static Map<String, String> m393(Rect rect) {
        HashMap hashMap = new HashMap();
        hashMap.put(AvidJSONUtil.KEY_X, String.valueOf(rect.left));
        hashMap.put(AvidJSONUtil.KEY_Y, String.valueOf(rect.top));
        hashMap.put("w", String.valueOf(rect.right - rect.left));
        hashMap.put("h", String.valueOf(rect.bottom - rect.top));
        int i = f437 + 107;
        f436 = i % 128;
        if (i % 2 != 0) {
        }
        return hashMap;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    private static Rect m392(Rect rect, DisplayMetrics displayMetrics) {
        float f = displayMetrics.density;
        if (!(f == 0.0f)) {
            return new Rect(Math.round(((float) rect.left) / f), Math.round(((float) rect.top) / f), Math.round(((float) rect.right) / f), Math.round(((float) rect.bottom) / f));
        }
        int i = f436 + 27;
        f437 = i % 128;
        int i2 = i % 2;
        int i3 = f436 + 17;
        f437 = i3 % 128;
        if ((i3 % 2 == 0 ? 'K' : 'L') != 'K') {
        }
        return rect;
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static boolean m399(View view, Rect rect) {
        int i = f437 + 45;
        f436 = i % 128;
        if ((i % 2 == 0) ? !view.getGlobalVisibleRect(rect) : !view.getGlobalVisibleRect(rect)) {
            int i2 = f436 + 73;
            f437 = i2 % 128;
            return (i2 % 2 == 0 ? 'D' : 4) != 4 ? false : false;
        }
        int[] iArr = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationInWindow(iArr);
        int[] iArr2 = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationOnScreen(iArr2);
        rect.offset(iArr2[0] - iArr[0], iArr2[1] - iArr[1]);
        return true;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    private static Rect m394(View view) {
        int[] iArr = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        Rect rect = new Rect(i, i2, view.getWidth() + i, view.getHeight() + i2);
        int i3 = f437 + 81;
        f436 = i3 % 128;
        if (i3 % 2 != 0) {
        }
        return rect;
    }
}
