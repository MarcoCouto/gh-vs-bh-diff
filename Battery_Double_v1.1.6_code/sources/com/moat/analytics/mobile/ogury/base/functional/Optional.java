package com.moat.analytics.mobile.ogury.base.functional;

import java.util.NoSuchElementException;

public final class Optional<T> {

    /* renamed from: ˏ reason: contains not printable characters */
    private static final Optional<?> f246 = new Optional<>();

    /* renamed from: ˎ reason: contains not printable characters */
    private final T f247;

    private Optional() {
        this.f247 = null;
    }

    public static <T> Optional<T> empty() {
        return f246;
    }

    private Optional(T t) {
        if (t != null) {
            this.f247 = t;
            return;
        }
        throw new NullPointerException("Optional of null value.");
    }

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public final T get() {
        if (this.f247 != null) {
            return this.f247;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.f247 != null;
    }

    public final T orElse(T t) {
        return this.f247 != null ? this.f247 : t;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        Optional optional = (Optional) obj;
        return this.f247 == optional.f247 || !(this.f247 == null || optional.f247 == null || !this.f247.equals(optional.f247));
    }

    public final int hashCode() {
        if (this.f247 == null) {
            return 0;
        }
        return this.f247.hashCode();
    }

    public final String toString() {
        if (this.f247 == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", new Object[]{this.f247});
    }
}
