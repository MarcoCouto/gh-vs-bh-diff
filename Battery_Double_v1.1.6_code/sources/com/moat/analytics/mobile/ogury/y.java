package com.moat.analytics.mobile.ogury;

import android.view.View;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class y extends e implements ReactiveVideoTracker {

    /* renamed from: ˋॱ reason: contains not printable characters */
    private Integer f455;

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final String m404() {
        return "ReactiveVideoTracker";
    }

    public y(String str) {
        super(str);
        AnonymousClass1.m243(3, "ReactiveVideoTracker", this, "Initializing.");
        AnonymousClass1.m247("[SUCCESS] ", "ReactiveVideoTracker created");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public final Map<String, Object> m405() throws l {
        HashMap hashMap = new HashMap();
        View view = (View) this.f273.get();
        Integer valueOf = Integer.valueOf(0);
        Integer valueOf2 = Integer.valueOf(0);
        if (view != null) {
            valueOf = Integer.valueOf(view.getWidth());
            valueOf2 = Integer.valueOf(view.getHeight());
        }
        hashMap.put("duration", this.f455);
        hashMap.put("width", valueOf);
        hashMap.put("height", valueOf2);
        return hashMap;
    }

    public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            m225();
            m220();
            this.f455 = num;
            return super.m238(map, view);
        } catch (Exception e) {
            m222("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final JSONObject m402(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.f230 == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.f231.equals(MoatAdEvent.f227) && !m233(moatAdEvent.f231, this.f455)) {
            moatAdEvent.f230 = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.m235(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m403(List<String> list) throws l {
        if (this.f455.intValue() >= 1000) {
            super.m236(list);
            return;
        }
        throw new l(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", new Object[]{this.f455}));
    }
}
