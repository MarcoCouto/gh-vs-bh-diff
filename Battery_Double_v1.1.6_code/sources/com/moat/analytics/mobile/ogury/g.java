package com.moat.analytics.mobile.ogury;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class g {

    /* renamed from: ˋ reason: contains not printable characters */
    private static final g f310 = new g();

    /* renamed from: ˊ reason: contains not printable characters */
    private final ScheduledExecutorService f311 = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */

    /* renamed from: ˎ reason: contains not printable characters */
    public final Map<f, String> f312 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ˏ reason: contains not printable characters */
    public final Map<c, String> f313 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public ScheduledFuture<?> f314;
    /* access modifiers changed from: private */

    /* renamed from: ᐝ reason: contains not printable characters */
    public ScheduledFuture<?> f315;

    /* renamed from: ˋ reason: contains not printable characters */
    static g m277() {
        return f310;
    }

    private g() {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m283(final Context context, f fVar) {
        this.f312.put(fVar, "");
        if (this.f315 == null || this.f315.isDone()) {
            AnonymousClass1.m243(3, "JSUpdateLooper", this, "Starting metadata reporting loop");
            this.f315 = this.f311.scheduleWithFixedDelay(new Runnable() {
                public final void run() {
                    try {
                        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_METADATA"));
                        if (g.this.f312.isEmpty()) {
                            g.this.f315.cancel(true);
                        }
                    } catch (Exception e) {
                        l.m310(e);
                    }
                }
            }, 0, 50, TimeUnit.MILLISECONDS);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m281(f fVar) {
        StringBuilder sb = new StringBuilder("removeSetupNeededBridge");
        sb.append(fVar.hashCode());
        AnonymousClass1.m243(3, "JSUpdateLooper", this, sb.toString());
        this.f312.remove(fVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m282(final Context context, c cVar) {
        if (cVar != null) {
            StringBuilder sb = new StringBuilder("addActiveTracker");
            sb.append(cVar.hashCode());
            AnonymousClass1.m243(3, "JSUpdateLooper", this, sb.toString());
            if (!this.f313.containsKey(cVar)) {
                this.f313.put(cVar, "");
                if (this.f314 == null || this.f314.isDone()) {
                    AnonymousClass1.m243(3, "JSUpdateLooper", this, "Starting view update loop");
                    this.f314 = this.f311.scheduleWithFixedDelay(new Runnable() {
                        public final void run() {
                            try {
                                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_VIEW_INFO"));
                                if (g.this.f313.isEmpty()) {
                                    AnonymousClass1.m243(3, "JSUpdateLooper", g.this, "No more active trackers");
                                    g.this.f314.cancel(true);
                                }
                            } catch (Exception e) {
                                l.m310(e);
                            }
                        }
                    }, 0, (long) q.m343().f391, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m284(c cVar) {
        if (cVar != null) {
            StringBuilder sb = new StringBuilder("removeActiveTracker");
            sb.append(cVar.hashCode());
            AnonymousClass1.m243(3, "JSUpdateLooper", this, sb.toString());
            this.f313.remove(cVar);
        }
    }
}
