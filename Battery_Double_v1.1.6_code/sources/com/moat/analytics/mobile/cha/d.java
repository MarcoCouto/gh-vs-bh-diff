package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class d {

    /* renamed from: ʻ reason: contains not printable characters */
    private WeakReference<View> f42;

    /* renamed from: ʼ reason: contains not printable characters */
    private final boolean f43;

    /* renamed from: ʽ reason: contains not printable characters */
    final boolean f44;

    /* renamed from: ˊ reason: contains not printable characters */
    TrackerListener f45;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private boolean f46;

    /* renamed from: ˋ reason: contains not printable characters */
    final String f47;

    /* renamed from: ˎ reason: contains not printable characters */
    j f48 = this.f32.f17;

    /* renamed from: ˏ reason: contains not printable characters */
    WeakReference<WebView> f49;

    /* renamed from: ͺ reason: contains not printable characters */
    private boolean f50;

    /* renamed from: ॱ reason: contains not printable characters */
    o f51 = null;

    /* renamed from: ᐝ reason: contains not printable characters */
    private final u f52;

    @Deprecated
    public void setActivity(Activity activity) {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public abstract String m36();

    d(@Nullable View view, boolean z, boolean z2) {
        String str;
        a.m6(3, "BaseTracker", this, "Initializing.");
        if (z) {
            StringBuilder sb = new StringBuilder("m");
            sb.append(hashCode());
            str = sb.toString();
        } else {
            str = "";
        }
        this.f47 = str;
        this.f42 = new WeakReference<>(view);
        this.f43 = z;
        this.f44 = z2;
        this.f46 = false;
        this.f50 = false;
        this.f52 = new u();
    }

    public void setListener(TrackerListener trackerListener) {
        this.f45 = trackerListener;
    }

    public void removeListener() {
        this.f45 = null;
    }

    public void startTracking() {
        try {
            a.m6(3, "BaseTracker", this, "In startTracking method.");
            m39();
            if (this.f45 != null) {
                TrackerListener trackerListener = this.f45;
                StringBuilder sb = new StringBuilder("Tracking started on ");
                sb.append(a.m5((View) this.f42.get()));
                trackerListener.onTrackingStarted(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder("startTracking succeeded for ");
            sb2.append(a.m5((View) this.f42.get()));
            String sb3 = sb2.toString();
            a.m6(3, "BaseTracker", this, sb3);
            StringBuilder sb4 = new StringBuilder();
            sb4.append(m36());
            sb4.append(" ");
            sb4.append(sb3);
            a.m3("[SUCCESS] ", sb4.toString());
        } catch (Exception e) {
            m42("startTracking", e);
        }
    }

    @CallSuper
    public void stopTracking() {
        boolean z = false;
        try {
            a.m6(3, "BaseTracker", this, "In stopTracking method.");
            this.f50 = true;
            if (this.f48 != null) {
                this.f48.m96(this);
                z = true;
            }
        } catch (Exception e) {
            o.m130(e);
        }
        String str = "BaseTracker";
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        a.m6(3, str, this, sb.toString());
        String str2 = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m36());
        sb2.append(" stopTracking ");
        sb2.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : ParametersKeys.FAILED);
        sb2.append(" for ");
        sb2.append(a.m5((View) this.f42.get()));
        a.m3(str2, sb2.toString());
        if (this.f45 != null) {
            this.f45.onTrackingStopped("");
            this.f45 = null;
        }
    }

    @CallSuper
    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(a.m5(view));
        a.m6(3, "BaseTracker", this, sb.toString());
        this.f42 = new WeakReference<>(view);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˏ reason: contains not printable characters */
    public void m39() throws o {
        a.m6(3, "BaseTracker", this, "Attempting to start impression.");
        m38();
        if (this.f46) {
            throw new o("Tracker already started");
        } else if (!this.f50) {
            m37(new ArrayList());
            if (this.f48 != null) {
                this.f48.m97(this);
                this.f46 = true;
                a.m6(3, "BaseTracker", this, "Impression started.");
                return;
            }
            a.m6(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new o("Bridge is null");
        } else {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m41(WebView webView) throws o {
        if (webView != null) {
            this.f49 = new WeakReference<>(webView);
            if (this.f48 == null) {
                if (!(this.f43 || this.f44)) {
                    a.m6(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f49.get() != null) {
                        this.f48 = new j((WebView) this.f49.get(), e.f121);
                        a.m6(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f48 = null;
                        a.m6(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            if (this.f48 != null) {
                this.f48.m95(this);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m38() throws o {
        if (this.f51 != null) {
            StringBuilder sb = new StringBuilder("Tracker initialization failed: ");
            sb.append(this.f51.getMessage());
            throw new o(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final boolean m35() {
        return this.f46 && !this.f50;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʼ reason: contains not printable characters */
    public final View m33() {
        return (View) this.f42.get();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʽ reason: contains not printable characters */
    public final String m34() {
        this.f52.m196(this.f47, (View) this.f42.get());
        return this.f52.f214;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m42(String str, Exception exc) {
        try {
            o.m130(exc);
            String r3 = o.m129(str, exc);
            if (this.f45 != null) {
                this.f45.onTrackingFailedToStart(r3);
            }
            a.m6(3, "BaseTracker", this, r3);
            StringBuilder sb = new StringBuilder();
            sb.append(m36());
            sb.append(" ");
            sb.append(r3);
            a.m3("[ERROR] ", sb.toString());
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m40() throws o {
        if (this.f46) {
            throw new o("Tracker already started");
        } else if (this.f50) {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ˋ reason: contains not printable characters */
    public void m37(List<String> list) throws o {
        if (((View) this.f42.get()) == null && !this.f44) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new o(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ʻ reason: contains not printable characters */
    public final String m32() {
        return a.m5((View) this.f42.get());
    }
}
