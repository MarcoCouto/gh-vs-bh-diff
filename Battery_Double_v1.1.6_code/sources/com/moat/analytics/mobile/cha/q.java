package com.moat.analytics.mobile.cha;

import android.graphics.Rect;
import android.view.View;
import com.moat.analytics.mobile.cha.NativeDisplayTracker.MoatUserInteractionType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class q extends d implements NativeDisplayTracker {

    /* renamed from: ˊॱ reason: contains not printable characters */
    private final Map<String, String> f156;

    /* renamed from: ᐝ reason: contains not printable characters */
    private final Set<MoatUserInteractionType> f157 = new HashSet();

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final String m143() {
        return "NativeDisplayTracker";
    }

    q(View view, Map<String, String> map) {
        super(view, true, false);
        a.m6(3, "NativeDisplayTracker", this, "Initializing.");
        this.f156 = map;
        if (view == null) {
            String str = "Target view is null";
            StringBuilder sb = new StringBuilder("NativeDisplayTracker initialization not successful, ");
            sb.append(str);
            String sb2 = sb.toString();
            a.m6(3, "NativeDisplayTracker", this, sb2);
            a.m3("[ERROR] ", sb2);
            this.f51 = new o(str);
        } else if (map == null || map.isEmpty()) {
            StringBuilder sb3 = new StringBuilder("NativeDisplayTracker initialization not successful, ");
            sb3.append("AdIds is null or empty");
            String sb4 = sb3.toString();
            a.m6(3, "NativeDisplayTracker", this, sb4);
            a.m3("[ERROR] ", sb4);
            this.f51 = new o("AdIds is null or empty");
        } else {
            a aVar = ((f) f.getInstance()).f62;
            if (aVar == null) {
                String str2 = "prepareNativeDisplayTracking was not called successfully";
                StringBuilder sb5 = new StringBuilder("NativeDisplayTracker initialization not successful, ");
                sb5.append(str2);
                String sb6 = sb5.toString();
                a.m6(3, "NativeDisplayTracker", this, sb6);
                a.m3("[ERROR] ", sb6);
                this.f51 = new o(str2);
                return;
            }
            this.f48 = aVar.f17;
            try {
                super.m41(aVar.f15);
                if (this.f48 != null) {
                    this.f48.m100(m141());
                }
                StringBuilder sb7 = new StringBuilder("NativeDisplayTracker created for ");
                sb7.append(m32());
                sb7.append(", with adIds:");
                sb7.append(map.toString());
                a.m3("[SUCCESS] ", sb7.toString());
            } catch (o e) {
                this.f51 = e;
            }
        }
    }

    public final void reportUserInteractionEvent(MoatUserInteractionType moatUserInteractionType) {
        String str = "NativeDisplayTracker";
        try {
            StringBuilder sb = new StringBuilder("reportUserInteractionEvent:");
            sb.append(moatUserInteractionType.name());
            a.m6(3, str, this, sb.toString());
            if (!this.f157.contains(moatUserInteractionType)) {
                this.f157.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.f47);
                jSONObject.accumulate("event", moatUserInteractionType.name().toLowerCase());
                if (this.f48 != null) {
                    this.f48.m101(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            a.m7("NativeDisplayTracker", this, "Got JSON exception");
            o.m130(e);
        } catch (Exception e2) {
            o.m130(e2);
        }
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    private String m141() {
        String str = "";
        try {
            Map<String, String> map = this.f156;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 0; i < 8; i++) {
                StringBuilder sb = new StringBuilder("moatClientLevel");
                sb.append(i);
                String sb2 = sb.toString();
                if (map.containsKey(sb2)) {
                    linkedHashMap.put(sb2, map.get(sb2));
                }
            }
            for (int i2 = 0; i2 < 8; i2++) {
                StringBuilder sb3 = new StringBuilder("moatClientSlicer");
                sb3.append(i2);
                String sb4 = sb3.toString();
                if (map.containsKey(sb4)) {
                    linkedHashMap.put(sb4, map.get(sb4));
                }
            }
            for (String str2 : map.keySet()) {
                if (!linkedHashMap.containsKey(str2)) {
                    linkedHashMap.put(str2, (String) map.get(str2));
                }
            }
            String jSONObject = new JSONObject(linkedHashMap).toString();
            StringBuilder sb5 = new StringBuilder("Parsed ad ids = ");
            sb5.append(jSONObject);
            a.m6(3, "NativeDisplayTracker", this, sb5.toString());
            StringBuilder sb6 = new StringBuilder("{\"adIds\":");
            sb6.append(jSONObject);
            sb6.append(", \"adKey\":\"");
            sb6.append(this.f47);
            sb6.append("\", \"adSize\":");
            sb6.append(m142());
            sb6.append("}");
            return sb6.toString();
        } catch (Exception e) {
            o.m130(e);
            return str;
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private String m142() {
        try {
            Rect r0 = u.m189(super.m33());
            int width = r0.width();
            int height = r0.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            o.m130(e);
            return null;
        }
    }
}
