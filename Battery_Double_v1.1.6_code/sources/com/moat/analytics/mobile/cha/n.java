package com.moat.analytics.mobile.cha;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.github.mikephil.charting.utils.Utils;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class n implements LocationListener {

    /* renamed from: ˎ reason: contains not printable characters */
    private static n f132;

    /* renamed from: ʻ reason: contains not printable characters */
    private Location f133;

    /* renamed from: ˊ reason: contains not printable characters */
    private ScheduledExecutorService f134;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private boolean f135;

    /* renamed from: ˋ reason: contains not printable characters */
    private ScheduledFuture<?> f136;

    /* renamed from: ˏ reason: contains not printable characters */
    private ScheduledFuture<?> f137;

    /* renamed from: ॱ reason: contains not printable characters */
    private LocationManager f138;

    /* renamed from: ᐝ reason: contains not printable characters */
    private boolean f139;

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static n m116() {
        if (f132 == null) {
            f132 = new n();
        }
        return f132;
    }

    private n() {
        try {
            this.f135 = ((f) MoatAnalytics.getInstance()).f59;
            if (this.f135) {
                a.m6(3, "LocationManager", this, "Moat location services disabled");
                return;
            }
            this.f134 = Executors.newScheduledThreadPool(1);
            this.f138 = (LocationManager) c.m27().getSystemService("location");
            if (this.f138.getAllProviders().size() == 0) {
                a.m6(3, "LocationManager", this, "Device has no location providers");
            } else {
                m114();
            }
        } catch (Exception e) {
            o.m130(e);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: ˊ reason: contains not printable characters */
    public final Location m125() {
        if (this.f135 || this.f138 == null) {
            return null;
        }
        return this.f133;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m126() {
        m114();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m127() {
        m121(false);
    }

    public final void onLocationChanged(Location location) {
        String str = "LocationManager";
        try {
            StringBuilder sb = new StringBuilder("Received an updated location = ");
            sb.append(location.toString());
            a.m6(3, str, this, sb.toString());
            float currentTimeMillis = (float) ((System.currentTimeMillis() - location.getTime()) / 1000);
            if (location.hasAccuracy() && location.getAccuracy() <= 100.0f && currentTimeMillis < 600.0f) {
                this.f133 = m119(this.f133, location);
                a.m6(3, "LocationManager", this, "fetchCompleted");
                m121(true);
            }
        } catch (Exception e) {
            o.m130(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ reason: contains not printable characters */
    public void m114() {
        try {
            if (!this.f135) {
                if (this.f138 != null) {
                    if (this.f139) {
                        a.m6(3, "LocationManager", this, "already updating location");
                    }
                    a.m6(3, "LocationManager", this, "starting location fetch");
                    this.f133 = m119(this.f133, m111());
                    if (this.f133 != null) {
                        StringBuilder sb = new StringBuilder("Have a valid location, won't fetch = ");
                        sb.append(this.f133.toString());
                        a.m6(3, "LocationManager", this, sb.toString());
                        m117();
                        return;
                    }
                    m109();
                }
            }
        } catch (Exception e) {
            o.m130(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ reason: contains not printable characters */
    public void m121(boolean z) {
        try {
            a.m6(3, "LocationManager", this, "stopping location fetch");
            m110();
            m113();
            if (z) {
                m117();
            } else {
                m124();
            }
        } catch (Exception e) {
            o.m130(e);
        }
    }

    /* renamed from: ʽ reason: contains not printable characters */
    private Location m111() {
        Location location;
        try {
            boolean r1 = m118();
            boolean r2 = m123();
            if (r1 && r2) {
                location = m119(this.f138.getLastKnownLocation("gps"), this.f138.getLastKnownLocation("network"));
            } else if (r1) {
                location = this.f138.getLastKnownLocation("gps");
            } else if (!r2) {
                return null;
            } else {
                location = this.f138.getLastKnownLocation("network");
            }
            return location;
        } catch (SecurityException e) {
            o.m130(e);
            return null;
        }
    }

    /* renamed from: ʻ reason: contains not printable characters */
    private void m109() {
        try {
            if (!this.f139) {
                a.m6(3, "LocationManager", this, "Attempting to start update");
                if (m118()) {
                    a.m6(3, "LocationManager", this, "start updating gps location");
                    this.f138.requestLocationUpdates("gps", 0, 0.0f, this, Looper.getMainLooper());
                    this.f139 = true;
                }
                if (m123()) {
                    a.m6(3, "LocationManager", this, "start updating network location");
                    this.f138.requestLocationUpdates("network", 0, 0.0f, this, Looper.getMainLooper());
                    this.f139 = true;
                }
                if (this.f139) {
                    m113();
                    this.f137 = this.f134.schedule(new Runnable() {
                        public final void run() {
                            try {
                                a.m6(3, "LocationManager", this, "fetchTimedOut");
                                n.this.m121(true);
                            } catch (Exception e) {
                                o.m130(e);
                            }
                        }
                    }, 60, TimeUnit.SECONDS);
                }
            }
        } catch (SecurityException e) {
            o.m130(e);
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private void m110() {
        try {
            a.m6(3, "LocationManager", this, "Stopping to update location");
            boolean z = true;
            if (!(ContextCompat.checkSelfPermission(c.m27().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
                if (!(ContextCompat.checkSelfPermission(c.m27().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                    z = false;
                }
            }
            if (z && this.f138 != null) {
                this.f138.removeUpdates(this);
                this.f139 = false;
            }
        } catch (SecurityException e) {
            o.m130(e);
        }
    }

    /* renamed from: ˊॱ reason: contains not printable characters */
    private void m113() {
        if (this.f137 != null && !this.f137.isCancelled()) {
            this.f137.cancel(true);
            this.f137 = null;
        }
    }

    /* renamed from: ᐝ reason: contains not printable characters */
    private void m124() {
        if (this.f136 != null && !this.f136.isCancelled()) {
            this.f136.cancel(true);
            this.f136 = null;
        }
    }

    /* renamed from: ˏॱ reason: contains not printable characters */
    private void m117() {
        a.m6(3, "LocationManager", this, "Resetting fetch timer");
        m124();
        float f = 600.0f;
        if (this.f133 != null) {
            f = Math.max(600.0f - ((float) ((System.currentTimeMillis() - this.f133.getTime()) / 1000)), 0.0f);
        }
        this.f136 = this.f134.schedule(new Runnable() {
            public final void run() {
                try {
                    a.m6(3, "LocationManager", this, "fetchTimerCompleted");
                    n.this.m114();
                } catch (Exception e) {
                    o.m130(e);
                }
            }
        }, (long) f, TimeUnit.SECONDS);
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static Location m119(Location location, Location location2) {
        boolean r0 = m122(location);
        boolean r1 = m122(location2);
        if (r0) {
            return (r1 && location.getAccuracy() >= location.getAccuracy()) ? location2 : location;
        }
        if (!r1) {
            return null;
        }
        return location2;
    }

    /* renamed from: ॱ reason: contains not printable characters */
    private static boolean m122(Location location) {
        if (location == null) {
            return false;
        }
        if ((location.getLatitude() != Utils.DOUBLE_EPSILON || location.getLongitude() != Utils.DOUBLE_EPSILON) && location.getAccuracy() >= 0.0f && ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)) < 600.0f) {
            return true;
        }
        return false;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static boolean m115(Location location, Location location2) {
        if (location == location2) {
            return true;
        }
        return (location == null || location2 == null || location.getTime() != location2.getTime()) ? false : true;
    }

    /* renamed from: ͺ reason: contains not printable characters */
    private boolean m118() {
        return (ContextCompat.checkSelfPermission(c.m27().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) && this.f138.getProvider("gps") != null && this.f138.isProviderEnabled("gps");
    }

    /* renamed from: ॱˊ reason: contains not printable characters */
    private boolean m123() {
        boolean z;
        if (!(ContextCompat.checkSelfPermission(c.m27().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
            if (!(ContextCompat.checkSelfPermission(c.m27().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                z = false;
                return !z && this.f138.getProvider("network") != null && this.f138.isProviderEnabled("network");
            }
        }
        z = true;
        if (!z) {
        }
    }
}
