package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

final class c {
    /* access modifiers changed from: private */

    /* renamed from: ˊ reason: contains not printable characters */
    public static boolean f37 = false;

    /* renamed from: ˋ reason: contains not printable characters */
    private static Application f38 = null;

    /* renamed from: ˎ reason: contains not printable characters */
    private static boolean f39 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    static WeakReference<Activity> f40;
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public static int f41;

    static class a implements ActivityLifecycleCallbacks {
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        a() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            c.f41 = 1;
        }

        public final void onActivityStarted(Activity activity) {
            try {
                c.f40 = new WeakReference<>(activity);
                c.f41 = 2;
                if (!c.f37) {
                    m31(true);
                }
                c.f37 = true;
                StringBuilder sb = new StringBuilder("Activity started: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m6(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m130(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                c.f40 = new WeakReference<>(activity);
                c.f41 = 3;
                t.m174().m180();
                StringBuilder sb = new StringBuilder("Activity resumed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m6(3, "ActivityState", this, sb.toString());
                if (((f) MoatAnalytics.getInstance()).f61) {
                    e.m43(activity);
                }
            } catch (Exception e) {
                o.m130(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                c.f41 = 4;
                if (c.m25(activity)) {
                    c.f40 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity paused: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m6(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m130(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (c.f41 != 3) {
                    c.f37 = false;
                    m31(false);
                }
                c.f41 = 5;
                if (c.m25(activity)) {
                    c.f40 = new WeakReference<>(null);
                }
                StringBuilder sb = new StringBuilder("Activity stopped: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m6(3, "ActivityState", this, sb.toString());
            } catch (Exception e) {
                o.m130(e);
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(c.f41 == 3 || c.f41 == 5)) {
                    if (c.f37) {
                        m31(false);
                    }
                    c.f37 = false;
                }
                c.f41 = 6;
                StringBuilder sb = new StringBuilder("Activity destroyed: ");
                sb.append(activity.getClass());
                sb.append("@");
                sb.append(activity.hashCode());
                a.m6(3, "ActivityState", this, sb.toString());
                if (c.m25(activity)) {
                    c.f40 = new WeakReference<>(null);
                }
            } catch (Exception e) {
                o.m130(e);
            }
        }

        /* renamed from: ॱ reason: contains not printable characters */
        private static void m31(boolean z) {
            if (z) {
                a.m6(3, "ActivityState", null, "App became visible");
                if (t.m174().f183 == a.f194 && !((f) MoatAnalytics.getInstance()).f59) {
                    n.m116().m126();
                }
            } else {
                a.m6(3, "ActivityState", null, "App became invisible");
                if (t.m174().f183 == a.f194 && !((f) MoatAnalytics.getInstance()).f59) {
                    n.m116().m127();
                }
            }
        }
    }

    c() {
    }

    /* renamed from: ॱ reason: contains not printable characters */
    static void m29(Application application) {
        f38 = application;
        if (!f39) {
            f39 = true;
            f38.registerActivityLifecycleCallbacks(new a());
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static Application m27() {
        return f38;
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static /* synthetic */ boolean m25(Activity activity) {
        return f40 != null && f40.get() == activity;
    }
}
