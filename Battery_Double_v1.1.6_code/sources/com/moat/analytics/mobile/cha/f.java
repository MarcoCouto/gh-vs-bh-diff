package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import java.lang.ref.WeakReference;

final class f extends MoatAnalytics implements b {

    /* renamed from: ʻ reason: contains not printable characters */
    private boolean f55 = false;

    /* renamed from: ʼ reason: contains not printable characters */
    private String f56;

    /* renamed from: ʽ reason: contains not printable characters */
    private MoatOptions f57;

    /* renamed from: ˊ reason: contains not printable characters */
    WeakReference<Context> f58;

    /* renamed from: ˋ reason: contains not printable characters */
    boolean f59 = false;

    /* renamed from: ˎ reason: contains not printable characters */
    boolean f60 = false;

    /* renamed from: ˏ reason: contains not printable characters */
    boolean f61 = false;
    @Nullable

    /* renamed from: ॱ reason: contains not printable characters */
    a f62;

    f() {
    }

    public final void start(Application application) {
        start(new MoatOptions(), application);
    }

    @UiThread
    public final void prepareNativeDisplayTracking(String str) {
        this.f56 = str;
        if (t.m174().f183 != a.f195) {
            try {
                m44();
            } catch (Exception e) {
                o.m130(e);
            }
        }
    }

    @UiThread
    /* renamed from: ˏ reason: contains not printable characters */
    private void m44() {
        if (this.f62 == null) {
            this.f62 = new a(c.m27(), d.f22);
            this.f62.m10(this.f56);
            StringBuilder sb = new StringBuilder("Preparing native display tracking with partner code ");
            sb.append(this.f56);
            a.m6(3, "Analytics", this, sb.toString());
            StringBuilder sb2 = new StringBuilder("Prepared for native display tracking with partner code ");
            sb2.append(this.f56);
            a.m3("[SUCCESS] ", sb2.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final boolean m45() {
        return this.f55;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final boolean m46() {
        return this.f57 != null && this.f57.disableLocationServices;
    }

    /* renamed from: ˎ reason: contains not printable characters */
    public final void m47() throws o {
        o.m131();
        n.m116();
        if (this.f56 != null) {
            try {
                m44();
            } catch (Exception e) {
                o.m130(e);
            }
        }
    }

    public final void start(MoatOptions moatOptions, Application application) {
        try {
            if (this.f55) {
                a.m6(3, "Analytics", this, "Moat SDK has already been started.");
                return;
            }
            this.f57 = moatOptions;
            t.m174().m180();
            this.f59 = moatOptions.disableLocationServices;
            if (application != null) {
                if (moatOptions.loggingEnabled && r.m149(application.getApplicationContext())) {
                    this.f60 = true;
                }
                this.f58 = new WeakReference<>(application.getApplicationContext());
                this.f55 = true;
                this.f61 = moatOptions.autoTrackGMAInterstitials;
                c.m29(application);
                t.m174().m179((b) this);
                if (!moatOptions.disableAdIdCollection) {
                    r.m152(application);
                }
                a.m3("[SUCCESS] ", "Moat Analytics SDK Version 2.4.1 started");
                return;
            }
            throw new o("Moat Analytics SDK didn't start, application was null");
        } catch (Exception e) {
            o.m130(e);
        }
    }
}
