package com.moat.analytics.mobile.cha;

import android.support.annotation.VisibleForTesting;
import com.moat.analytics.mobile.cha.base.asserts.Asserts;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class p<T> implements InvocationHandler {
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public static final Object[] f145 = new Object[0];

    /* renamed from: ˊ reason: contains not printable characters */
    private final c<T> f146;

    /* renamed from: ˎ reason: contains not printable characters */
    private boolean f147;

    /* renamed from: ˏ reason: contains not printable characters */
    private final LinkedList<d> f148 = new LinkedList<>();

    /* renamed from: ॱ reason: contains not printable characters */
    private final Class<T> f149;

    /* renamed from: ᐝ reason: contains not printable characters */
    private T f150;

    interface c<T> {
        /* renamed from: ˋ reason: contains not printable characters */
        Optional<T> m138() throws o;
    }

    class d {
        /* access modifiers changed from: private */

        /* renamed from: ˊ reason: contains not printable characters */
        public final WeakReference[] f152;
        /* access modifiers changed from: private */

        /* renamed from: ˋ reason: contains not printable characters */
        public final Method f153;

        /* renamed from: ˎ reason: contains not printable characters */
        private final LinkedList<Object> f154;

        /* synthetic */ d(p pVar, Method method, Object[] objArr, byte b) {
            this(method, objArr);
        }

        private d(Method method, Object... objArr) {
            this.f154 = new LinkedList<>();
            if (objArr == null) {
                objArr = p.f145;
            }
            WeakReference[] weakReferenceArr = new WeakReference[objArr.length];
            int length = objArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Object obj = objArr[i];
                if ((obj instanceof Map) || (obj instanceof Integer) || (obj instanceof Double)) {
                    this.f154.add(obj);
                }
                int i3 = i2 + 1;
                weakReferenceArr[i2] = new WeakReference(obj);
                i++;
                i2 = i3;
            }
            this.f152 = weakReferenceArr;
            this.f153 = method;
        }
    }

    @VisibleForTesting
    private p(c<T> cVar, Class<T> cls) throws o {
        Asserts.checkNotNull(cVar);
        Asserts.checkNotNull(cls);
        this.f146 = cVar;
        this.f149 = cls;
        t.m174().m179((b) new b() {
            /* renamed from: ˎ reason: contains not printable characters */
            public final void m137() throws o {
                p.this.m134();
            }
        });
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static <T> T m133(c<T> cVar, Class<T> cls) throws o {
        ClassLoader classLoader = cls.getClassLoader();
        p pVar = new p(cVar, cls);
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, pVar);
    }

    /* renamed from: ˊ reason: contains not printable characters */
    private static Boolean m132(Method method) {
        try {
            if (Boolean.TYPE.equals(method.getReturnType())) {
                return Boolean.valueOf(true);
            }
        } catch (Exception e) {
            o.m130(e);
        }
        return null;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            Class declaringClass = method.getDeclaringClass();
            t r0 = t.m174();
            if (Object.class.equals(declaringClass)) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    return this.f149;
                }
                if (!"toString".equals(name)) {
                    return method.invoke(this, objArr);
                }
                Object invoke = method.invoke(this, objArr);
                return String.valueOf(invoke).replace(p.class.getName(), this.f149.getName());
            } else if (!this.f147 || this.f150 != null) {
                if (r0.f183 == a.f194) {
                    m134();
                    if (this.f150 != null) {
                        return method.invoke(this.f150, objArr);
                    }
                }
                if (r0.f183 == a.f195 && (!this.f147 || this.f150 != null)) {
                    if (this.f148.size() >= 15) {
                        this.f148.remove(5);
                    }
                    this.f148.add(new d(this, method, objArr, 0));
                }
                return m132(method);
            } else {
                this.f148.clear();
                return m132(method);
            }
        } catch (Exception e) {
            o.m130(e);
            return m132(method);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ reason: contains not printable characters */
    public void m134() throws o {
        if (!this.f147) {
            try {
                this.f150 = this.f146.m138().orElse(null);
            } catch (Exception e) {
                a.m8("OnOffTrackerProxy", this, "Could not create instance", e);
                o.m130(e);
            }
            this.f147 = true;
        }
        if (this.f150 != null) {
            Iterator it = this.f148.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                try {
                    Object[] objArr = new Object[dVar.f152.length];
                    WeakReference[] r3 = dVar.f152;
                    int length = r3.length;
                    int i = 0;
                    int i2 = 0;
                    while (i < length) {
                        int i3 = i2 + 1;
                        objArr[i2] = r3[i].get();
                        i++;
                        i2 = i3;
                    }
                    dVar.f153.invoke(this.f150, objArr);
                } catch (Exception e2) {
                    o.m130(e2);
                }
            }
            this.f148.clear();
        }
    }
}
