package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.support.annotation.FloatRange;
import android.telephony.TelephonyManager;
import com.chartboost.sdk.impl.b;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import java.lang.ref.WeakReference;

final class r {

    /* renamed from: ʻ reason: contains not printable characters */
    private static int f158 = 1;

    /* renamed from: ˊ reason: contains not printable characters */
    private static e f159 = null;

    /* renamed from: ˋ reason: contains not printable characters */
    private static d f160 = null;

    /* renamed from: ˎ reason: contains not printable characters */
    private static int f161 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˏ reason: contains not printable characters */
    public static String f162;

    /* renamed from: ॱ reason: contains not printable characters */
    private static int[] f163 = {-39340411, 1646369784, -593413711, -1069164445, -50787683, -1327220997, 423245644, -742130253, 54775946, -495304555, 1880137505, 1742082653, 65717847, 1497802820, 828947133, -614454858, 941569790, -1897799303};

    static class d {

        /* renamed from: ʽ reason: contains not printable characters */
        boolean f165;

        /* renamed from: ˊ reason: contains not printable characters */
        boolean f166;

        /* renamed from: ˋ reason: contains not printable characters */
        boolean f167;

        /* renamed from: ˎ reason: contains not printable characters */
        String f168;

        /* renamed from: ˏ reason: contains not printable characters */
        String f169;

        /* renamed from: ॱ reason: contains not printable characters */
        Integer f170;

        /* synthetic */ d(byte b) {
            this();
        }

        private d() {
            this.f168 = "_unknown_";
            this.f169 = "_unknown_";
            this.f170 = Integer.valueOf(-1);
            this.f167 = false;
            this.f166 = false;
            this.f165 = false;
            try {
                Context r0 = r.m153();
                if (r0 != null) {
                    this.f165 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService(PlaceFields.PHONE);
                    this.f168 = telephonyManager.getSimOperatorName();
                    this.f169 = telephonyManager.getNetworkOperatorName();
                    this.f170 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f167 = r.m144();
                    this.f166 = r.m149(r0);
                }
            } catch (Exception e) {
                o.m130(e);
            }
        }
    }

    static class e {

        /* renamed from: ˊ reason: contains not printable characters */
        private String f171;

        /* renamed from: ˋ reason: contains not printable characters */
        private String f172;
        /* access modifiers changed from: private */

        /* renamed from: ˏ reason: contains not printable characters */
        public boolean f173;

        /* renamed from: ॱ reason: contains not printable characters */
        private String f174;

        /* synthetic */ e(byte b) {
            this();
        }

        private e() {
            this.f173 = false;
            this.f171 = "_unknown_";
            this.f172 = "_unknown_";
            this.f174 = "_unknown_";
            try {
                Context r0 = r.m153();
                if (r0 != null) {
                    this.f173 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f172 = r0.getPackageName();
                    this.f171 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f174 = packageManager.getInstallerPackageName(this.f172);
                    return;
                }
                a.m6(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                o.m130(e);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˎ reason: contains not printable characters */
        public final String m158() {
            return this.f171;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ˋ reason: contains not printable characters */
        public final String m157() {
            return this.f172;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: ॱ reason: contains not printable characters */
        public final String m159() {
            return this.f174 != null ? this.f174 : "_unknown_";
        }
    }

    r() {
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    /* renamed from: ॱ reason: contains not printable characters */
    static double m154() {
        try {
            double r1 = (double) m145();
            double streamMaxVolume = (double) ((AudioManager) c.m27().getSystemService(m151(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamMaxVolume(3);
            Double.isNaN(r1);
            Double.isNaN(streamMaxVolume);
            return r1 / streamMaxVolume;
        } catch (Exception e2) {
            o.m130(e2);
            return Utils.DOUBLE_EPSILON;
        }
    }

    /* renamed from: ʼ reason: contains not printable characters */
    private static int m145() {
        try {
            return ((AudioManager) c.m27().getSystemService(m151(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamVolume(3);
        } catch (Exception e2) {
            o.m130(e2);
            return 0;
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static void m152(final Application application) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(application);
                        if (!advertisingIdInfo.isLimitAdTrackingEnabled()) {
                            r.f162 = advertisingIdInfo.getId();
                            StringBuilder sb = new StringBuilder("Retrieved Advertising ID = ");
                            sb.append(r.f162);
                            a.m6(3, "Util", this, sb.toString());
                            return;
                        }
                        a.m6(3, "Util", this, "User has limited ad tracking");
                    } catch (Exception e) {
                        o.m130(e);
                    }
                }
            });
        } catch (Exception e2) {
            o.m130(e2);
        }
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static String m150() {
        return f162;
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static Context m153() {
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f58;
        if (weakReference != null) {
            return (Context) weakReference.get();
        }
        return null;
    }

    /* renamed from: ˊ reason: contains not printable characters */
    static e m147() {
        if (f159 == null || !f159.f173) {
            f159 = new e(0);
        }
        return f159;
    }

    /* JADX WARNING: type inference failed for: r0v1, types: [char[]] */
    /* JADX WARNING: type inference failed for: r1v2, types: [char[]] */
    /* JADX WARNING: type inference failed for: r6v3, types: [char, int] */
    /* JADX WARNING: type inference failed for: r7v1, types: [char, int] */
    /* JADX WARNING: type inference failed for: r9v0, types: [char] */
    /* JADX WARNING: type inference failed for: r10v0, types: [char] */
    /* JADX WARNING: type inference failed for: r8v1, types: [char] */
    /* JADX WARNING: type inference failed for: r7v3, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=null, for r10v0, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=null, for r7v3, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=null, for r8v1, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char, code=null, for r9v0, types: [char] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char[], code=null, for r0v1, types: [char[]] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=char[], code=null, for r1v2, types: [char[]] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=null, for r6v3, types: [char, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=null, for r7v1, types: [char, int] */
    /* JADX WARNING: Unknown variable types count: 8 */
    /* renamed from: ˎ reason: contains not printable characters */
    private static String m151(int[] iArr, int i) {
        ? r0 = new char[4];
        ? r1 = new char[(iArr.length << 1)];
        int[] iArr2 = (int[]) f163.clone();
        int i2 = 0;
        while (true) {
            if (i2 >= iArr.length) {
                return new String(r1, 0, i);
            }
            r0[0] = iArr[i2] >>> 16;
            r0[1] = (char) iArr[i2];
            int i3 = i2 + 1;
            r0[2] = iArr[i3] >>> 16;
            r0[3] = (char) iArr[i3];
            b.a(r0, iArr2, false);
            int i4 = i2 << 1;
            r1[i4] = r0[0];
            r1[i4 + 1] = r0[1];
            r1[i4 + 2] = r0[2];
            r1[i4 + 3] = r0[3];
            i2 += 2;
        }
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static d m148() {
        if (f160 == null || !f160.f165) {
            f160 = new d(0);
        }
        return f160;
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static boolean m149(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    /* renamed from: ʻ reason: contains not printable characters */
    static /* synthetic */ boolean m144() {
        int i;
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f58;
        Context context = weakReference != null ? (Context) weakReference.get() : null;
        if (context != null) {
            int i2 = f158 + 27;
            f161 = i2 % 128;
            int i3 = i2 % 2;
            if ((VERSION.SDK_INT < 17 ? (char) 22 : 19) != 22) {
                int i4 = f158 + 87;
                f161 = i4 % 128;
                int i5 = i4 % 2;
                i = Global.getInt(context.getContentResolver(), m151(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            } else {
                i = Secure.getInt(context.getContentResolver(), m151(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            }
        } else {
            i = 0;
        }
        if (!(i == 1)) {
            return false;
        }
        int i6 = f161 + 33;
        f158 = i6 % 128;
        int i7 = i6 % 2;
        return true;
    }
}
