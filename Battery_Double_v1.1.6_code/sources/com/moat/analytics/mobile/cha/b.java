package com.moat.analytics.mobile.cha;

import android.os.Handler;
import android.support.annotation.CallSuper;
import android.text.TextUtils;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

abstract class b extends d {

    /* renamed from: ʻ reason: contains not printable characters */
    static final MoatAdEventType[] f23 = {MoatAdEventType.AD_EVT_FIRST_QUARTILE, MoatAdEventType.AD_EVT_MID_POINT, MoatAdEventType.AD_EVT_THIRD_QUARTILE};

    /* renamed from: ʼ reason: contains not printable characters */
    final Map<MoatAdEventType, Integer> f24;

    /* renamed from: ʼॱ reason: contains not printable characters */
    private final String f25;

    /* renamed from: ˊॱ reason: contains not printable characters */
    WeakReference<View> f26;

    /* renamed from: ˋॱ reason: contains not printable characters */
    private boolean f27;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private Map<String, String> f28;

    /* renamed from: ͺ reason: contains not printable characters */
    private Double f29;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ reason: contains not printable characters */
    public VideoTrackerListener f30;

    /* renamed from: ॱˋ reason: contains not printable characters */
    private final Set<MoatAdEventType> f31;
    /* access modifiers changed from: private */

    /* renamed from: ॱˎ reason: contains not printable characters */
    public final a f32 = new a(c.m27(), d.f21);

    /* renamed from: ᐝ reason: contains not printable characters */
    final Handler f33;

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public abstract Map<String, Object> m23() throws o;

    b(String str) {
        super(null, false, true);
        a.m6(3, "BaseVideoTracker", this, "Initializing.");
        this.f25 = str;
        try {
            super.m41(this.f32.f15);
        } catch (o e) {
            this.f51 = e;
        }
        this.f24 = new HashMap();
        this.f31 = new HashSet();
        this.f33 = new Handler();
        this.f27 = false;
        this.f29 = Double.valueOf(1.0d);
    }

    public void setVideoListener(VideoTrackerListener videoTrackerListener) {
        this.f30 = videoTrackerListener;
    }

    public void removeVideoListener() {
        this.f30 = null;
    }

    @CallSuper
    /* renamed from: ॱ reason: contains not printable characters */
    public boolean m21(Map<String, String> map, View view) {
        try {
            m38();
            m40();
            if (view == null) {
                a.m6(3, "BaseVideoTracker", this, "trackVideoAd received null video view instance");
            }
            this.f28 = map;
            this.f26 = new WeakReference<>(view);
            m19();
            String format = String.format("trackVideoAd tracking ids: %s | view: %s", new Object[]{new JSONObject(map).toString(), a.m5(view)});
            a.m6(3, "BaseVideoTracker", this, format);
            StringBuilder sb = new StringBuilder();
            sb.append(m36());
            sb.append(" ");
            sb.append(format);
            a.m3("[SUCCESS] ", sb.toString());
            if (this.f45 != null) {
                this.f45.onTrackingStarted(m32());
            }
            return true;
        } catch (Exception e) {
            m42("trackVideoAd", e);
            return false;
        }
    }

    public void changeTargetView(View view) {
        StringBuilder sb = new StringBuilder("changing view to ");
        sb.append(a.m5(view));
        a.m6(3, "BaseVideoTracker", this, sb.toString());
        this.f26 = new WeakReference<>(view);
        try {
            super.changeTargetView(view);
        } catch (Exception e) {
            o.m130(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public void m16(List<String> list) throws o {
        if (this.f28 == null) {
            list.add("Null adIds object");
        }
        if (list.isEmpty()) {
            super.m37(list);
            return;
        }
        throw new o(TextUtils.join(" and ", list));
    }

    public void stopTracking() {
        try {
            super.stopTracking();
            m20();
            if (this.f30 != null) {
                this.f30 = null;
            }
        } catch (Exception e) {
            o.m130(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˊ reason: contains not printable characters */
    public final Double m22() {
        return this.f29;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final void m19() throws o {
        super.changeTargetView((View) this.f26.get());
        super.m39();
        HashMap r0 = m23();
        Integer num = (Integer) r0.get("width");
        Integer num2 = (Integer) r0.get("height");
        Integer num3 = (Integer) r0.get("duration");
        a.m6(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "Player metadata: height = %d, width = %d, duration = %d", new Object[]{num2, num, num3}));
        this.f32.m11(this.f25, this.f28, num, num2, num3);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public JSONObject m18(MoatAdEvent moatAdEvent) {
        if (Double.isNaN(moatAdEvent.f4.doubleValue())) {
            moatAdEvent.f4 = this.f29;
        }
        return new JSONObject(moatAdEvent.m0());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏॱ reason: contains not printable characters */
    public final void m20() {
        if (!this.f27) {
            this.f27 = true;
            this.f33.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        a.m6(3, "BaseVideoTracker", this, "Shutting down.");
                        a r0 = b.this.f32;
                        a.m6(3, "GlobalWebView", r0, "Cleaning up");
                        r0.f17.m94();
                        r0.f17 = null;
                        r0.f15.destroy();
                        r0.f15 = null;
                        b.this.f30 = null;
                    } catch (Exception e) {
                        o.m130(e);
                    }
                }
            }, 500);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋॱ reason: contains not printable characters */
    public final boolean m17() {
        return this.f24.containsKey(MoatAdEventType.AD_EVT_COMPLETE) || this.f24.containsKey(MoatAdEventType.AD_EVT_STOPPED) || this.f24.containsKey(MoatAdEventType.AD_EVT_SKIPPED);
    }

    /* renamed from: ˋ reason: contains not printable characters */
    static boolean m13(Integer num, Integer num2) {
        int abs = Math.abs(num2.intValue() - num.intValue());
        double intValue = (double) num2.intValue();
        Double.isNaN(intValue);
        return ((double) abs) <= Math.min(750.0d, intValue * 0.05d);
    }

    public void setPlayerVolume(Double d) {
        Double valueOf = Double.valueOf(this.f29.doubleValue() * r.m154());
        if (!d.equals(this.f29)) {
            a.m6(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "player volume changed to %f ", new Object[]{d}));
            this.f29 = d;
            if (!valueOf.equals(Double.valueOf(this.f29.doubleValue() * r.m154()))) {
                dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_VOLUME_CHANGE, MoatAdEvent.f1, this.f29));
            }
        }
    }

    public void dispatchEvent(MoatAdEvent moatAdEvent) {
        try {
            JSONObject r0 = m18(moatAdEvent);
            boolean z = false;
            a.m6(3, "BaseVideoTracker", this, String.format("Received event: %s", new Object[]{r0.toString()}));
            StringBuilder sb = new StringBuilder();
            sb.append(m36());
            sb.append(String.format(" Received event: %s", new Object[]{r0.toString()}));
            a.m3("[SUCCESS] ", sb.toString());
            if (m35() && this.f48 != null) {
                this.f48.m102(this.f32.f14, r0);
                if (!this.f31.contains(moatAdEvent.f6)) {
                    this.f31.add(moatAdEvent.f6);
                    if (this.f30 != null) {
                        this.f30.onVideoEventReported(moatAdEvent.f6);
                    }
                }
            }
            MoatAdEventType moatAdEventType = moatAdEvent.f6;
            if (moatAdEventType == MoatAdEventType.AD_EVT_COMPLETE || moatAdEventType == MoatAdEventType.AD_EVT_STOPPED || moatAdEventType == MoatAdEventType.AD_EVT_SKIPPED) {
                z = true;
            }
            if (z) {
                this.f24.put(moatAdEventType, Integer.valueOf(1));
                if (this.f48 != null) {
                    this.f48.m96((d) this);
                }
                m20();
            }
        } catch (Exception e) {
            o.m130(e);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊॱ reason: contains not printable characters */
    public final Double m15() {
        return Double.valueOf(this.f29.doubleValue() * r.m154());
    }
}
