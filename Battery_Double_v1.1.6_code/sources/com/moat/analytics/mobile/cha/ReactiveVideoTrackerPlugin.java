package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.view.View;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import java.util.Map;

public class ReactiveVideoTrackerPlugin implements l<ReactiveVideoTracker> {
    /* access modifiers changed from: private */

    /* renamed from: ˊ reason: contains not printable characters */
    public final String f12;

    static class d implements ReactiveVideoTracker {
        public final void changeTargetView(View view) {
        }

        public final void dispatchEvent(MoatAdEvent moatAdEvent) {
        }

        public final void removeListener() {
        }

        public final void removeVideoListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void setPlayerVolume(Double d) {
        }

        public final void setVideoListener(VideoTrackerListener videoTrackerListener) {
        }

        public final void stopTracking() {
        }

        public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
            return false;
        }

        d() {
        }
    }

    public ReactiveVideoTrackerPlugin(String str) {
        this.f12 = str;
    }

    public ReactiveVideoTracker create() throws o {
        return (ReactiveVideoTracker) p.m133(new c<ReactiveVideoTracker>() {
            /* renamed from: ˋ reason: contains not printable characters */
            public final Optional<ReactiveVideoTracker> m2() {
                a.m3("[INFO] ", "Attempting to create ReactiveVideoTracker");
                return Optional.of(new w(ReactiveVideoTrackerPlugin.this.f12));
            }
        }, ReactiveVideoTracker.class);
    }

    public ReactiveVideoTracker createNoOp() {
        return new d();
    }
}
