package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.ads.AdActivity;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import java.lang.ref.WeakReference;

final class e {

    /* renamed from: ˊ reason: contains not printable characters */
    private static WeakReference<Activity> f53 = new WeakReference<>(null);
    @Nullable

    /* renamed from: ˋ reason: contains not printable characters */
    private static WebAdTracker f54;

    e() {
    }

    /* renamed from: ˎ reason: contains not printable characters */
    static void m43(Activity activity) {
        try {
            if (t.m174().f183 != a.f195) {
                String name = activity.getClass().getName();
                StringBuilder sb = new StringBuilder("Activity name: ");
                sb.append(name);
                a.m6(3, "GMAInterstitialHelper", activity, sb.toString());
                if (!name.contains(AdActivity.CLASS_NAME)) {
                    if (f54 != null) {
                        a.m6(3, "GMAInterstitialHelper", f53.get(), "Stopping to track GMA interstitial");
                        f54.stopTracking();
                        f54 = null;
                    }
                    f53 = new WeakReference<>(null);
                } else if (f53.get() == null || f53.get() != activity) {
                    View decorView = activity.getWindow().getDecorView();
                    if (decorView instanceof ViewGroup) {
                        Optional r0 = x.m202((ViewGroup) decorView, true);
                        if (r0.isPresent()) {
                            f53 = new WeakReference<>(activity);
                            WebView webView = (WebView) r0.get();
                            a.m6(3, "GMAInterstitialHelper", f53.get(), "Starting to track GMA interstitial");
                            WebAdTracker createWebAdTracker = MoatFactory.create().createWebAdTracker(webView);
                            f54 = createWebAdTracker;
                            createWebAdTracker.startTracking();
                            return;
                        }
                        a.m6(3, "GMAInterstitialHelper", activity, "Sorry, no WebView in this activity");
                    }
                }
            }
        } catch (Exception e) {
            o.m130(e);
        }
    }
}
