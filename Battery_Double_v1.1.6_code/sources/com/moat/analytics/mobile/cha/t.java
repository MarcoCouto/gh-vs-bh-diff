package com.moat.analytics.mobile.cha;

import android.os.Handler;
import android.os.Looper;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class t {

    /* renamed from: ʻ reason: contains not printable characters */
    private static t f176;
    /* access modifiers changed from: private */

    /* renamed from: ʽ reason: contains not printable characters */
    public static final Queue<e> f177 = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */

    /* renamed from: ʼ reason: contains not printable characters */
    public long f178 = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;

    /* renamed from: ˊ reason: contains not printable characters */
    volatile int f179 = 200;

    /* renamed from: ˊॱ reason: contains not printable characters */
    private long f180 = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;

    /* renamed from: ˋ reason: contains not printable characters */
    volatile boolean f181 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˋॱ reason: contains not printable characters */
    public final AtomicBoolean f182 = new AtomicBoolean(false);

    /* renamed from: ˎ reason: contains not printable characters */
    volatile int f183 = a.f195;

    /* renamed from: ˏ reason: contains not printable characters */
    volatile boolean f184 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ reason: contains not printable characters */
    public final AtomicBoolean f185 = new AtomicBoolean(false);

    /* renamed from: ॱ reason: contains not printable characters */
    volatile int f186 = 10;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ reason: contains not printable characters */
    public final AtomicInteger f187 = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: ॱˋ reason: contains not printable characters */
    public volatile long f188 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ᐝ reason: contains not printable characters */
    public Handler f189;

    enum a {
        ;
        

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f194 = 2;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f195 = 1;

        static {
            int[] iArr = {1, 2};
        }
    }

    interface b {
        /* renamed from: ˎ reason: contains not printable characters */
        void m182() throws o;
    }

    interface c {
        /* renamed from: ˏ reason: contains not printable characters */
        void m183(g gVar) throws o;
    }

    class d implements Runnable {

        /* renamed from: ˎ reason: contains not printable characters */
        private final String f197;
        /* access modifiers changed from: private */

        /* renamed from: ˏ reason: contains not printable characters */
        public final AnonymousClass2 f198;

        /* renamed from: ॱ reason: contains not printable characters */
        private final Handler f199;

        private d(String str, Handler handler, AnonymousClass2 r4) {
            this.f198 = r4;
            this.f199 = handler;
            StringBuilder sb = new StringBuilder("https://z.moatads.com/");
            sb.append(str);
            sb.append("/android/");
            sb.append(BuildConfig.REVISION.substring(0, 7));
            sb.append("/status.json");
            this.f197 = sb.toString();
        }

        /* renamed from: ˎ reason: contains not printable characters */
        private String m184() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f197);
            sb.append("?ts=");
            sb.append(System.currentTimeMillis());
            sb.append("&v=2.4.1");
            try {
                return (String) m.m108(sb.toString()).get();
            } catch (Exception unused) {
                return null;
            }
        }

        public final void run() {
            try {
                String r0 = m184();
                final g gVar = new g(r0);
                t.this.f184 = gVar.m53();
                t.this.f181 = gVar.m57();
                t.this.f179 = gVar.m56();
                t.this.f186 = gVar.m55();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            d.this.f198.m183(gVar);
                        } catch (Exception e) {
                            o.m130(e);
                        }
                    }
                });
                t.this.f188 = System.currentTimeMillis();
                t.this.f185.compareAndSet(true, false);
                if (r0 != null) {
                    t.this.f187.set(0);
                } else if (t.this.f187.incrementAndGet() < 10) {
                    t.this.m175(t.this.f178);
                }
            } catch (Exception e) {
                o.m130(e);
            }
            this.f199.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    class e {

        /* renamed from: ˎ reason: contains not printable characters */
        final Long f202;

        /* renamed from: ॱ reason: contains not printable characters */
        final b f204;

        e(Long l, b bVar) {
            this.f202 = l;
            this.f204 = bVar;
        }
    }

    /* renamed from: ˏ reason: contains not printable characters */
    static synchronized t m174() {
        t tVar;
        synchronized (t.class) {
            if (f176 == null) {
                f176 = new t();
            }
            tVar = f176;
        }
        return tVar;
    }

    private t() {
        try {
            this.f189 = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            o.m130(e2);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final void m180() {
        if (System.currentTimeMillis() - this.f188 > this.f180) {
            m175(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ reason: contains not printable characters */
    public void m175(final long j) {
        if (this.f185.compareAndSet(false, true)) {
            a.m6(3, "OnOff", this, "Performing status check.");
            new Thread() {
                public final void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    d dVar = new d(BuildConfig.NAMESPACE, handler, new c() {
                        /* renamed from: ˏ reason: contains not printable characters */
                        public final void m181(g gVar) throws o {
                            synchronized (t.f177) {
                                boolean z = ((f) MoatAnalytics.getInstance()).f60;
                                if (t.this.f183 != gVar.m54() || (t.this.f183 == a.f195 && z)) {
                                    t.this.f183 = gVar.m54();
                                    if (t.this.f183 == a.f195 && z) {
                                        t.this.f183 = a.f194;
                                    }
                                    if (t.this.f183 == a.f194) {
                                        a.m6(3, "OnOff", this, "Moat enabled - Version 2.4.1");
                                    }
                                    for (e eVar : t.f177) {
                                        if (t.this.f183 == a.f194) {
                                            eVar.f204.m182();
                                        }
                                    }
                                }
                                while (!t.f177.isEmpty()) {
                                    t.f177.remove();
                                }
                            }
                        }
                    });
                    handler.postDelayed(dVar, j);
                    Looper.loop();
                }
            }.start();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m179(b bVar) throws o {
        if (this.f183 == a.f194) {
            bVar.m182();
            return;
        }
        m168();
        f177.add(new e(Long.valueOf(System.currentTimeMillis()), bVar));
        if (this.f182.compareAndSet(false, true)) {
            this.f189.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (t.f177.size() > 0) {
                            t.m168();
                            t.this.f189.postDelayed(this, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                            return;
                        }
                        t.this.f182.compareAndSet(true, false);
                        t.this.f189.removeCallbacks(this);
                    } catch (Exception e) {
                        o.m130(e);
                    }
                }
            }, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ reason: contains not printable characters */
    public static void m168() {
        synchronized (f177) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator it = f177.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - ((e) it.next()).f202.longValue() >= ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS) {
                    it.remove();
                }
            }
            if (f177.size() >= 15) {
                for (int i = 0; i < 5; i++) {
                    f177.remove();
                }
            }
        }
    }
}
