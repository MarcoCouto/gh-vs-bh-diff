package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.support.annotation.UiThread;

public abstract class MoatAnalytics {

    /* renamed from: ˏ reason: contains not printable characters */
    private static MoatAnalytics f10;

    @UiThread
    public abstract void prepareNativeDisplayTracking(String str);

    public abstract void start(Application application);

    public abstract void start(MoatOptions moatOptions, Application application);

    public static synchronized MoatAnalytics getInstance() {
        MoatAnalytics moatAnalytics;
        synchronized (MoatAnalytics.class) {
            if (f10 == null) {
                try {
                    f10 = new f();
                } catch (Exception e) {
                    o.m130(e);
                    f10 = new com.moat.analytics.mobile.cha.NoOp.MoatAnalytics();
                }
            }
            moatAnalytics = f10;
        }
        return moatAnalytics;
    }
}
