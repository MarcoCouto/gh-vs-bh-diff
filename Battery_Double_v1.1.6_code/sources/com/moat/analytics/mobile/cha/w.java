package com.moat.analytics.mobile.cha;

import android.view.View;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class w extends b implements ReactiveVideoTracker {

    /* renamed from: ˋॱ reason: contains not printable characters */
    private Integer f224;

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final String m198() {
        return "ReactiveVideoTracker";
    }

    public w(String str) {
        super(str);
        a.m6(3, "ReactiveVideoTracker", this, "Initializing.");
        a.m3("[SUCCESS] ", "ReactiveVideoTracker created");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ᐝ reason: contains not printable characters */
    public final Map<String, Object> m201() throws o {
        HashMap hashMap = new HashMap();
        View view = (View) this.f26.get();
        Integer valueOf = Integer.valueOf(0);
        Integer valueOf2 = Integer.valueOf(0);
        if (view != null) {
            valueOf = Integer.valueOf(view.getWidth());
            valueOf2 = Integer.valueOf(view.getHeight());
        }
        hashMap.put("duration", this.f224);
        hashMap.put("width", valueOf);
        hashMap.put("height", valueOf2);
        return hashMap;
    }

    public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            m38();
            m40();
            this.f224 = num;
            return super.m21(map, view);
        } catch (Exception e) {
            m42("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final JSONObject m200(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.f6 == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.f5.equals(MoatAdEvent.f1) && !m13(moatAdEvent.f5, this.f224)) {
            moatAdEvent.f6 = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.m18(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m199(List<String> list) throws o {
        if (this.f224.intValue() >= 1000) {
            super.m16(list);
            return;
        }
        throw new o(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", new Object[]{this.f224}));
    }
}
