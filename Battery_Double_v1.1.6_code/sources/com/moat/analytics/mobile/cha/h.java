package com.moat.analytics.mobile.cha;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class h {

    /* renamed from: ˊ reason: contains not printable characters */
    private static final h f74 = new h();
    /* access modifiers changed from: private */

    /* renamed from: ʽ reason: contains not printable characters */
    public ScheduledFuture<?> f75;
    /* access modifiers changed from: private */

    /* renamed from: ˋ reason: contains not printable characters */
    public final Map<j, String> f76 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ˎ reason: contains not printable characters */
    public ScheduledFuture<?> f77;

    /* renamed from: ˏ reason: contains not printable characters */
    private final ScheduledExecutorService f78 = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */

    /* renamed from: ॱ reason: contains not printable characters */
    public final Map<d, String> f79 = new WeakHashMap();

    /* renamed from: ˊ reason: contains not printable characters */
    static h m58() {
        return f74;
    }

    private h() {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˋ reason: contains not printable characters */
    public final void m64(final Context context, j jVar) {
        if (jVar != null) {
            this.f76.put(jVar, "");
            if (this.f75 == null || this.f75.isDone()) {
                a.m6(3, "JSUpdateLooper", this, "Starting metadata reporting loop");
                this.f75 = this.f78.scheduleWithFixedDelay(new Runnable() {
                    public final void run() {
                        try {
                            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_METADATA"));
                            if (h.this.f76.isEmpty()) {
                                h.this.f75.cancel(true);
                            }
                        } catch (Exception e) {
                            o.m130(e);
                        }
                    }
                }, 0, 50, TimeUnit.MILLISECONDS);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˊ reason: contains not printable characters */
    public final void m63(j jVar) {
        if (jVar != null) {
            StringBuilder sb = new StringBuilder("removeSetupNeededBridge");
            sb.append(jVar.hashCode());
            a.m6(3, "JSUpdateLooper", this, sb.toString());
            this.f76.remove(jVar);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m65(final Context context, d dVar) {
        if (dVar != null) {
            StringBuilder sb = new StringBuilder("addActiveTracker");
            sb.append(dVar.hashCode());
            a.m6(3, "JSUpdateLooper", this, sb.toString());
            if (!this.f79.containsKey(dVar)) {
                this.f79.put(dVar, "");
                if (this.f77 == null || this.f77.isDone()) {
                    a.m6(3, "JSUpdateLooper", this, "Starting view update loop");
                    this.f77 = this.f78.scheduleWithFixedDelay(new Runnable() {
                        public final void run() {
                            try {
                                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_VIEW_INFO"));
                                if (h.this.f79.isEmpty()) {
                                    a.m6(3, "JSUpdateLooper", h.this, "No more active trackers");
                                    h.this.f77.cancel(true);
                                }
                            } catch (Exception e) {
                                o.m130(e);
                            }
                        }
                    }, 0, (long) t.m174().f179, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱ reason: contains not printable characters */
    public final void m66(d dVar) {
        if (dVar != null) {
            StringBuilder sb = new StringBuilder("removeActiveTracker");
            sb.append(dVar.hashCode());
            a.m6(3, "JSUpdateLooper", this, sb.toString());
            this.f79.remove(dVar);
        }
    }
}
