package com.moat.analytics.mobile.cha;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import java.lang.ref.WeakReference;
import java.util.Map;

final class k extends MoatFactory {
    k() throws o {
        if (!((f) f.getInstance()).m45()) {
            String str = "Failed to initialize MoatFactory";
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(", SDK was not started");
            String sb2 = sb.toString();
            a.m6(3, "Factory", this, sb2);
            a.m3("[ERROR] ", sb2);
            throw new o(str);
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull WebView webView) {
        try {
            final WeakReference weakReference = new WeakReference(webView);
            return (WebAdTracker) p.m133(new c<WebAdTracker>() {
                /* renamed from: ˋ reason: contains not printable characters */
                public final Optional<WebAdTracker> m106() {
                    WebView webView = (WebView) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create WebAdTracker for ");
                    sb.append(a.m5(webView));
                    String sb2 = sb.toString();
                    a.m6(3, "Factory", this, sb2);
                    a.m3("[INFO] ", sb2);
                    return Optional.of(new v(webView));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m130(e);
            return new e();
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull ViewGroup viewGroup) {
        try {
            final WeakReference weakReference = new WeakReference(viewGroup);
            return (WebAdTracker) p.m133(new c<WebAdTracker>() {
                /* renamed from: ˋ reason: contains not printable characters */
                public final Optional<WebAdTracker> m105() throws o {
                    ViewGroup viewGroup = (ViewGroup) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create WebAdTracker for adContainer ");
                    sb.append(a.m5(viewGroup));
                    String sb2 = sb.toString();
                    a.m6(3, "Factory", this, sb2);
                    a.m3("[INFO] ", sb2);
                    return Optional.of(new v(viewGroup));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m130(e);
            return new e();
        }
    }

    public final NativeDisplayTracker createNativeDisplayTracker(@NonNull View view, @NonNull final Map<String, String> map) {
        try {
            final WeakReference weakReference = new WeakReference(view);
            return (NativeDisplayTracker) p.m133(new c<NativeDisplayTracker>() {
                /* renamed from: ˋ reason: contains not printable characters */
                public final Optional<NativeDisplayTracker> m103() {
                    View view = (View) weakReference.get();
                    StringBuilder sb = new StringBuilder("Attempting to create NativeDisplayTracker for ");
                    sb.append(a.m5(view));
                    String sb2 = sb.toString();
                    a.m6(3, "Factory", this, sb2);
                    a.m3("[INFO] ", sb2);
                    return Optional.of(new q(view, map));
                }
            }, NativeDisplayTracker.class);
        } catch (Exception e) {
            o.m130(e);
            return new c();
        }
    }

    public final NativeVideoTracker createNativeVideoTracker(final String str) {
        try {
            return (NativeVideoTracker) p.m133(new c<NativeVideoTracker>() {
                /* renamed from: ˋ reason: contains not printable characters */
                public final Optional<NativeVideoTracker> m104() {
                    String str = "Attempting to create NativeVideoTracker";
                    a.m6(3, "Factory", this, str);
                    a.m3("[INFO] ", str);
                    return Optional.of(new s(str));
                }
            }, NativeVideoTracker.class);
        } catch (Exception e) {
            o.m130(e);
            return new b();
        }
    }

    public final <T> T createCustomTracker(l<T> lVar) {
        try {
            return lVar.create();
        } catch (Exception e) {
            o.m130(e);
            return lVar.createNoOp();
        }
    }
}
