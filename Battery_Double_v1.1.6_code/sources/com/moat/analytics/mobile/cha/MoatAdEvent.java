package com.moat.analytics.mobile.cha;

import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf(Utils.DOUBLE_EPSILON);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: ˋ reason: contains not printable characters */
    static final Integer f1 = Integer.valueOf(Integer.MIN_VALUE);

    /* renamed from: ˎ reason: contains not printable characters */
    private static final Double f2 = Double.valueOf(Double.NaN);

    /* renamed from: ʽ reason: contains not printable characters */
    private final Long f3;

    /* renamed from: ˊ reason: contains not printable characters */
    Double f4;

    /* renamed from: ˏ reason: contains not printable characters */
    Integer f5;

    /* renamed from: ॱ reason: contains not printable characters */
    MoatAdEventType f6;

    /* renamed from: ᐝ reason: contains not printable characters */
    private final Double f7;

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d) {
        this.f3 = Long.valueOf(System.currentTimeMillis());
        this.f6 = moatAdEventType;
        this.f4 = d;
        this.f5 = num;
        this.f7 = Double.valueOf(r.m154());
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, f2);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f1, f2);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˏ reason: contains not printable characters */
    public final Map<String, Object> m0() {
        HashMap hashMap = new HashMap();
        hashMap.put("adVolume", this.f4);
        hashMap.put("playhead", this.f5);
        hashMap.put("aTimeStamp", this.f3);
        hashMap.put("type", this.f6.toString());
        hashMap.put(RequestParameters.DEVICE_VOLUME, this.f7);
        return hashMap;
    }
}
