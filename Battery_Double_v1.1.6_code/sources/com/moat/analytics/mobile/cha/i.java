package com.moat.analytics.mobile.cha;

import android.support.annotation.CallSuper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class i extends b {

    /* renamed from: ˋॱ reason: contains not printable characters */
    private int f84 = c.f94;

    /* renamed from: ˏॱ reason: contains not printable characters */
    private int f85 = Integer.MIN_VALUE;

    /* renamed from: ͺ reason: contains not printable characters */
    private int f86 = Integer.MIN_VALUE;

    /* renamed from: ॱˊ reason: contains not printable characters */
    private double f87 = Double.NaN;

    /* renamed from: ॱˋ reason: contains not printable characters */
    private int f88 = Integer.MIN_VALUE;

    /* renamed from: ॱˎ reason: contains not printable characters */
    private int f89 = 0;

    enum c {
        ;
        

        /* renamed from: ˊ reason: contains not printable characters */
        public static final int f91 = 2;

        /* renamed from: ˋ reason: contains not printable characters */
        public static final int f92 = 4;

        /* renamed from: ˎ reason: contains not printable characters */
        public static final int f93 = 3;

        /* renamed from: ˏ reason: contains not printable characters */
        public static final int f94 = 1;

        /* renamed from: ॱ reason: contains not printable characters */
        public static final int f95 = 5;

        static {
            int[] iArr = {1, 2, 3, 4, 5};
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ͺ reason: contains not printable characters */
    public abstract boolean m69();

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˋ reason: contains not printable characters */
    public abstract Integer m71();

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱˎ reason: contains not printable characters */
    public abstract boolean m72();

    /* access modifiers changed from: 0000 */
    /* renamed from: ॱᐝ reason: contains not printable characters */
    public abstract Integer m73();

    i(String str) {
        super(str);
    }

    /* renamed from: ॱ reason: contains not printable characters */
    public final boolean m70(Map<String, String> map, View view) {
        try {
            boolean r4 = super.m21(map, view);
            if (!r4) {
                return r4;
            }
            this.f33.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (!i.this.m69() || i.this.m17()) {
                            i.this.m20();
                        } else if (Boolean.valueOf(i.this.m67()).booleanValue()) {
                            i.this.f33.postDelayed(this, 200);
                        } else {
                            i.this.m20();
                        }
                    } catch (Exception e) {
                        i.this.m20();
                        o.m130(e);
                    }
                }
            }, 200);
            return r4;
        } catch (Exception e) {
            a.m6(3, "IntervalVideoTracker", this, "Problem with video loop");
            m42("trackVideoAd", e);
            return false;
        }
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            o.m130(e);
        }
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.f87 = m15().doubleValue();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: ˎ reason: contains not printable characters */
    public final JSONObject m68(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.f5.equals(MoatAdEvent.f1)) {
            num = moatAdEvent.f5;
        } else {
            try {
                num = m71();
            } catch (Exception unused) {
                num = Integer.valueOf(this.f85);
            }
            moatAdEvent.f5 = num;
        }
        if (moatAdEvent.f5.intValue() < 0 || (moatAdEvent.f5.intValue() == 0 && moatAdEvent.f6 == MoatAdEventType.AD_EVT_COMPLETE && this.f85 > 0)) {
            num = Integer.valueOf(this.f85);
            moatAdEvent.f5 = num;
        }
        if (moatAdEvent.f6 == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.f86 == Integer.MIN_VALUE || !m13(num, Integer.valueOf(this.f86))) {
                this.f84 = c.f92;
                moatAdEvent.f6 = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.f84 = c.f95;
            }
        }
        return super.m18(moatAdEvent);
    }

    /* access modifiers changed from: 0000 */
    @CallSuper
    /* renamed from: ʻॱ reason: contains not printable characters */
    public final boolean m67() throws o {
        if (!m69() || m17()) {
            return false;
        }
        try {
            int intValue = m71().intValue();
            if (this.f85 >= 0 && intValue < 0) {
                return false;
            }
            this.f85 = intValue;
            if (intValue == 0) {
                return true;
            }
            int intValue2 = m73().intValue();
            boolean r4 = m72();
            double d = (double) intValue2;
            Double.isNaN(d);
            double d2 = d / 4.0d;
            double doubleValue = m15().doubleValue();
            MoatAdEventType moatAdEventType = null;
            if (intValue > this.f88) {
                this.f88 = intValue;
            }
            if (this.f86 == Integer.MIN_VALUE) {
                this.f86 = intValue2;
            }
            if (r4) {
                if (this.f84 == c.f94) {
                    moatAdEventType = MoatAdEventType.AD_EVT_START;
                    this.f84 = c.f93;
                } else if (this.f84 == c.f91) {
                    moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                    this.f84 = c.f93;
                } else {
                    double d3 = (double) intValue;
                    Double.isNaN(d3);
                    int floor = ((int) Math.floor(d3 / d2)) - 1;
                    if (floor >= 0 && floor < 3) {
                        MoatAdEventType moatAdEventType2 = f23[floor];
                        if (!this.f24.containsKey(moatAdEventType2)) {
                            this.f24.put(moatAdEventType2, Integer.valueOf(1));
                            moatAdEventType = moatAdEventType2;
                        }
                    }
                }
            } else if (this.f84 != c.f91) {
                moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                this.f84 = c.f91;
            }
            boolean z = moatAdEventType != null;
            if (!z && !Double.isNaN(this.f87) && Math.abs(this.f87 - doubleValue) > 0.05d) {
                moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                z = true;
            }
            if (z) {
                dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), m22()));
            }
            this.f87 = doubleValue;
            this.f89 = 0;
            return true;
        } catch (Exception unused) {
            int i = this.f89;
            this.f89 = i + 1;
            return i < 5;
        }
    }
}
