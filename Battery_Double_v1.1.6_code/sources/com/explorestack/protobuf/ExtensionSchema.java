package com.explorestack.protobuf;

import com.explorestack.protobuf.FieldSet.FieldDescriptorLite;
import java.io.IOException;
import java.util.Map.Entry;

abstract class ExtensionSchema<T extends FieldDescriptorLite<T>> {
    /* access modifiers changed from: 0000 */
    public abstract int extensionNumber(Entry<?, ?> entry);

    /* access modifiers changed from: 0000 */
    public abstract Object findExtensionByNumber(ExtensionRegistryLite extensionRegistryLite, MessageLite messageLite, int i);

    /* access modifiers changed from: 0000 */
    public abstract FieldSet<T> getExtensions(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract FieldSet<T> getMutableExtensions(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract boolean hasExtensions(MessageLite messageLite);

    /* access modifiers changed from: 0000 */
    public abstract void makeImmutable(Object obj);

    /* access modifiers changed from: 0000 */
    public abstract <UT, UB> UB parseExtension(Reader reader, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<T> fieldSet, UB ub, UnknownFieldSchema<UT, UB> unknownFieldSchema) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void parseLengthPrefixedMessageSetItem(Reader reader, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<T> fieldSet) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void parseMessageSetItem(ByteString byteString, Object obj, ExtensionRegistryLite extensionRegistryLite, FieldSet<T> fieldSet) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void serializeExtension(Writer writer, Entry<?, ?> entry) throws IOException;

    /* access modifiers changed from: 0000 */
    public abstract void setExtensions(Object obj, FieldSet<T> fieldSet);

    ExtensionSchema() {
    }
}
