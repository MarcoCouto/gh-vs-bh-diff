package com.explorestack.protobuf;

import com.explorestack.protobuf.Internal.BooleanList;
import com.explorestack.protobuf.Internal.DoubleList;
import com.explorestack.protobuf.Internal.FloatList;
import com.explorestack.protobuf.Internal.IntList;
import com.explorestack.protobuf.Internal.LongList;
import com.explorestack.protobuf.Internal.ProtobufList;

final class ProtobufLists {
    private ProtobufLists() {
    }

    public static <E> ProtobufList<E> emptyProtobufList() {
        return ProtobufArrayList.emptyList();
    }

    public static <E> ProtobufList<E> mutableCopy(ProtobufList<E> protobufList) {
        int size = protobufList.size();
        return protobufList.mutableCopyWithCapacity(size == 0 ? 10 : size * 2);
    }

    public static BooleanList emptyBooleanList() {
        return BooleanArrayList.emptyList();
    }

    public static BooleanList newBooleanList() {
        return new BooleanArrayList();
    }

    public static IntList emptyIntList() {
        return IntArrayList.emptyList();
    }

    public static IntList newIntList() {
        return new IntArrayList();
    }

    public static LongList emptyLongList() {
        return LongArrayList.emptyList();
    }

    public static LongList newLongList() {
        return new LongArrayList();
    }

    public static FloatList emptyFloatList() {
        return FloatArrayList.emptyList();
    }

    public static FloatList newFloatList() {
        return new FloatArrayList();
    }

    public static DoubleList emptyDoubleList() {
        return DoubleArrayList.emptyList();
    }

    public static DoubleList newDoubleList() {
        return new DoubleArrayList();
    }
}
