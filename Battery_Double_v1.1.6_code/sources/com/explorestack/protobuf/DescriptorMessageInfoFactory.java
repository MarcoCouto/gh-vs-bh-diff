package com.explorestack.protobuf;

import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor;
import com.explorestack.protobuf.Descriptors.FieldDescriptor.JavaType;
import com.explorestack.protobuf.Descriptors.FieldDescriptor.Type;
import com.explorestack.protobuf.Descriptors.OneofDescriptor;
import com.explorestack.protobuf.Internal.EnumVerifier;
import com.explorestack.protobuf.StructuralMessageInfo.Builder;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

final class DescriptorMessageInfoFactory implements MessageInfoFactory {
    private static final String GET_DEFAULT_INSTANCE_METHOD_NAME = "getDefaultInstance";
    private static final DescriptorMessageInfoFactory instance = new DescriptorMessageInfoFactory();
    private static IsInitializedCheckAnalyzer isInitializedCheckAnalyzer = new IsInitializedCheckAnalyzer();
    private static final Set<String> specialFieldNames = new HashSet(Arrays.asList(new String[]{"cached_size", "serialized_size", "class"}));

    static class IsInitializedCheckAnalyzer {
        private int index = 0;
        private final Map<Descriptor, Node> nodeCache = new HashMap();
        private final Map<Descriptor, Boolean> resultCache = new ConcurrentHashMap();
        private final Stack<Node> stack = new Stack<>();

        private static class Node {
            StronglyConnectedComponent component = null;
            final Descriptor descriptor;
            final int index;
            int lowLink;

            Node(Descriptor descriptor2, int i) {
                this.descriptor = descriptor2;
                this.index = i;
                this.lowLink = i;
            }
        }

        private static class StronglyConnectedComponent {
            final List<Descriptor> messages;
            boolean needsIsInitializedCheck;

            private StronglyConnectedComponent() {
                this.messages = new ArrayList();
                this.needsIsInitializedCheck = false;
            }
        }

        IsInitializedCheckAnalyzer() {
        }

        public boolean needsIsInitializedCheck(Descriptor descriptor) {
            Boolean bool = (Boolean) this.resultCache.get(descriptor);
            if (bool != null) {
                return bool.booleanValue();
            }
            synchronized (this) {
                Boolean bool2 = (Boolean) this.resultCache.get(descriptor);
                if (bool2 != null) {
                    boolean booleanValue = bool2.booleanValue();
                    return booleanValue;
                }
                boolean z = dfs(descriptor).component.needsIsInitializedCheck;
                return z;
            }
        }

        private Node dfs(Descriptor descriptor) {
            Node node;
            int i = this.index;
            this.index = i + 1;
            Node node2 = new Node(descriptor, i);
            this.stack.push(node2);
            this.nodeCache.put(descriptor, node2);
            for (FieldDescriptor fieldDescriptor : descriptor.getFields()) {
                if (fieldDescriptor.getJavaType() == JavaType.MESSAGE) {
                    Node node3 = (Node) this.nodeCache.get(fieldDescriptor.getMessageType());
                    if (node3 == null) {
                        node2.lowLink = Math.min(node2.lowLink, dfs(fieldDescriptor.getMessageType()).lowLink);
                    } else if (node3.component == null) {
                        node2.lowLink = Math.min(node2.lowLink, node3.lowLink);
                    }
                }
            }
            if (node2.index == node2.lowLink) {
                StronglyConnectedComponent stronglyConnectedComponent = new StronglyConnectedComponent();
                do {
                    node = (Node) this.stack.pop();
                    node.component = stronglyConnectedComponent;
                    stronglyConnectedComponent.messages.add(node.descriptor);
                } while (node != node2);
                analyze(stronglyConnectedComponent);
            }
            return node2;
        }

        private void analyze(StronglyConnectedComponent stronglyConnectedComponent) {
            boolean z;
            Iterator it = stronglyConnectedComponent.messages.iterator();
            loop0:
            while (true) {
                z = true;
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                Descriptor descriptor = (Descriptor) it.next();
                if (descriptor.isExtendable()) {
                    break;
                }
                Iterator it2 = descriptor.getFields().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        FieldDescriptor fieldDescriptor = (FieldDescriptor) it2.next();
                        if (fieldDescriptor.isRequired()) {
                            break loop0;
                        } else if (fieldDescriptor.getJavaType() == JavaType.MESSAGE) {
                            Node node = (Node) this.nodeCache.get(fieldDescriptor.getMessageType());
                            if (node.component != stronglyConnectedComponent && node.component.needsIsInitializedCheck) {
                                break loop0;
                            }
                        }
                    }
                }
            }
            stronglyConnectedComponent.needsIsInitializedCheck = z;
            for (Descriptor put : stronglyConnectedComponent.messages) {
                this.resultCache.put(put, Boolean.valueOf(stronglyConnectedComponent.needsIsInitializedCheck));
            }
        }
    }

    private static final class OneofState {
        private OneofInfo[] oneofs;

        private OneofState() {
            this.oneofs = new OneofInfo[2];
        }

        /* access modifiers changed from: 0000 */
        public OneofInfo getOneof(Class<?> cls, OneofDescriptor oneofDescriptor) {
            int index = oneofDescriptor.getIndex();
            if (index >= this.oneofs.length) {
                this.oneofs = (OneofInfo[]) Arrays.copyOf(this.oneofs, index * 2);
            }
            OneofInfo oneofInfo = this.oneofs[index];
            if (oneofInfo != null) {
                return oneofInfo;
            }
            OneofInfo newInfo = newInfo(cls, oneofDescriptor);
            this.oneofs[index] = newInfo;
            return newInfo;
        }

        private static OneofInfo newInfo(Class<?> cls, OneofDescriptor oneofDescriptor) {
            String access$200 = DescriptorMessageInfoFactory.snakeCaseToCamelCase(oneofDescriptor.getName());
            StringBuilder sb = new StringBuilder();
            sb.append(access$200);
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(access$200);
            sb3.append("Case_");
            return new OneofInfo(oneofDescriptor.getIndex(), DescriptorMessageInfoFactory.field(cls, sb3.toString()), DescriptorMessageInfoFactory.field(cls, sb2));
        }
    }

    private DescriptorMessageInfoFactory() {
    }

    public static DescriptorMessageInfoFactory getInstance() {
        return instance;
    }

    public boolean isSupported(Class<?> cls) {
        return GeneratedMessageV3.class.isAssignableFrom(cls);
    }

    public MessageInfo messageInfoFor(Class<?> cls) {
        if (GeneratedMessageV3.class.isAssignableFrom(cls)) {
            return convert(cls, descriptorForType(cls));
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Unsupported message type: ");
        sb.append(cls.getName());
        throw new IllegalArgumentException(sb.toString());
    }

    private static Message getDefaultInstance(Class<?> cls) {
        try {
            return (Message) cls.getDeclaredMethod(GET_DEFAULT_INSTANCE_METHOD_NAME, new Class[0]).invoke(null, new Object[0]);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to get default instance for message class ");
            sb.append(cls.getName());
            throw new IllegalArgumentException(sb.toString(), e);
        }
    }

    private static Descriptor descriptorForType(Class<?> cls) {
        return getDefaultInstance(cls).getDescriptorForType();
    }

    private static MessageInfo convert(Class<?> cls, Descriptor descriptor) {
        switch (descriptor.getFile().getSyntax()) {
            case PROTO2:
                return convertProto2(cls, descriptor);
            case PROTO3:
                return convertProto3(cls, descriptor);
            default:
                StringBuilder sb = new StringBuilder();
                sb.append("Unsupported syntax: ");
                sb.append(descriptor.getFile().getSyntax());
                throw new IllegalArgumentException(sb.toString());
        }
    }

    private static boolean needsIsInitializedCheck(Descriptor descriptor) {
        return isInitializedCheckAnalyzer.needsIsInitializedCheck(descriptor);
    }

    private static StructuralMessageInfo convertProto2(Class<?> cls, Descriptor descriptor) {
        Class<?> cls2 = cls;
        List fields = descriptor.getFields();
        Builder newBuilder = StructuralMessageInfo.newBuilder(fields.size());
        newBuilder.withDefaultInstance(getDefaultInstance(cls));
        newBuilder.withSyntax(ProtoSyntax.PROTO2);
        newBuilder.withMessageSetWireFormat(descriptor.getOptions().getMessageSetWireFormat());
        EnumVerifier enumVerifier = null;
        OneofState oneofState = new OneofState();
        Field field = null;
        int i = 0;
        int i2 = 0;
        int i3 = 1;
        while (i < fields.size()) {
            final FieldDescriptor fieldDescriptor = (FieldDescriptor) fields.get(i);
            boolean javaStringCheckUtf8 = fieldDescriptor.getFile().getOptions().getJavaStringCheckUtf8();
            EnumVerifier r14 = fieldDescriptor.getJavaType() == JavaType.ENUM ? new EnumVerifier() {
                public boolean isInRange(int i) {
                    return fieldDescriptor.getEnumType().findValueByNumber(i) != null;
                }
            } : enumVerifier;
            if (fieldDescriptor.getContainingOneof() != null) {
                newBuilder.withField(buildOneofMember(cls2, fieldDescriptor, oneofState, javaStringCheckUtf8, r14));
            } else {
                Field field2 = field(cls2, fieldDescriptor);
                int number = fieldDescriptor.getNumber();
                FieldType fieldType = getFieldType(fieldDescriptor);
                if (fieldDescriptor.isMapField()) {
                    final FieldDescriptor findFieldByNumber = fieldDescriptor.getMessageType().findFieldByNumber(2);
                    if (findFieldByNumber.getJavaType() == JavaType.ENUM) {
                        r14 = new EnumVerifier() {
                            public boolean isInRange(int i) {
                                return findFieldByNumber.getEnumType().findValueByNumber(i) != null;
                            }
                        };
                    }
                    newBuilder.withField(FieldInfo.forMapField(field2, number, SchemaUtil.getMapDefaultEntry(cls2, fieldDescriptor.getName()), r14));
                } else if (!fieldDescriptor.isRepeated()) {
                    if (field == null) {
                        field = bitField(cls2, i2);
                    }
                    if (fieldDescriptor.isRequired()) {
                        newBuilder.withField(FieldInfo.forProto2RequiredField(field2, number, fieldType, field, i3, javaStringCheckUtf8, r14));
                    } else {
                        newBuilder.withField(FieldInfo.forProto2OptionalField(field2, number, fieldType, field, i3, javaStringCheckUtf8, r14));
                    }
                } else if (r14 != null) {
                    if (fieldDescriptor.isPacked()) {
                        newBuilder.withField(FieldInfo.forPackedFieldWithEnumVerifier(field2, number, fieldType, r14, cachedSizeField(cls2, fieldDescriptor)));
                    } else {
                        newBuilder.withField(FieldInfo.forFieldWithEnumVerifier(field2, number, fieldType, r14));
                    }
                } else if (fieldDescriptor.getJavaType() == JavaType.MESSAGE) {
                    newBuilder.withField(FieldInfo.forRepeatedMessageField(field2, number, fieldType, getTypeForRepeatedMessageField(cls2, fieldDescriptor)));
                } else if (fieldDescriptor.isPacked()) {
                    newBuilder.withField(FieldInfo.forPackedField(field2, number, fieldType, cachedSizeField(cls2, fieldDescriptor)));
                } else {
                    newBuilder.withField(FieldInfo.forField(field2, number, fieldType, javaStringCheckUtf8));
                }
                i++;
                enumVerifier = null;
            }
            int i4 = i3 << 1;
            if (i4 == 0) {
                i2++;
                field = null;
                i3 = 1;
            } else {
                i3 = i4;
            }
            i++;
            enumVerifier = null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i5 = 0; i5 < fields.size(); i5++) {
            FieldDescriptor fieldDescriptor2 = (FieldDescriptor) fields.get(i5);
            if (fieldDescriptor2.isRequired() || (fieldDescriptor2.getJavaType() == JavaType.MESSAGE && needsIsInitializedCheck(fieldDescriptor2.getMessageType()))) {
                arrayList.add(Integer.valueOf(fieldDescriptor2.getNumber()));
            }
        }
        int[] iArr = new int[arrayList.size()];
        for (int i6 = 0; i6 < arrayList.size(); i6++) {
            iArr[i6] = ((Integer) arrayList.get(i6)).intValue();
        }
        newBuilder.withCheckInitialized(iArr);
        return newBuilder.build();
    }

    private static StructuralMessageInfo convertProto3(Class<?> cls, Descriptor descriptor) {
        List fields = descriptor.getFields();
        Builder newBuilder = StructuralMessageInfo.newBuilder(fields.size());
        newBuilder.withDefaultInstance(getDefaultInstance(cls));
        newBuilder.withSyntax(ProtoSyntax.PROTO3);
        OneofState oneofState = new OneofState();
        for (int i = 0; i < fields.size(); i++) {
            FieldDescriptor fieldDescriptor = (FieldDescriptor) fields.get(i);
            if (fieldDescriptor.getContainingOneof() != null) {
                newBuilder.withField(buildOneofMember(cls, fieldDescriptor, oneofState, true, null));
            } else if (fieldDescriptor.isMapField()) {
                newBuilder.withField(FieldInfo.forMapField(field(cls, fieldDescriptor), fieldDescriptor.getNumber(), SchemaUtil.getMapDefaultEntry(cls, fieldDescriptor.getName()), null));
            } else if (fieldDescriptor.isRepeated() && fieldDescriptor.getJavaType() == JavaType.MESSAGE) {
                newBuilder.withField(FieldInfo.forRepeatedMessageField(field(cls, fieldDescriptor), fieldDescriptor.getNumber(), getFieldType(fieldDescriptor), getTypeForRepeatedMessageField(cls, fieldDescriptor)));
            } else if (fieldDescriptor.isPacked()) {
                newBuilder.withField(FieldInfo.forPackedField(field(cls, fieldDescriptor), fieldDescriptor.getNumber(), getFieldType(fieldDescriptor), cachedSizeField(cls, fieldDescriptor)));
            } else {
                newBuilder.withField(FieldInfo.forField(field(cls, fieldDescriptor), fieldDescriptor.getNumber(), getFieldType(fieldDescriptor), true));
            }
        }
        return newBuilder.build();
    }

    private static FieldInfo buildOneofMember(Class<?> cls, FieldDescriptor fieldDescriptor, OneofState oneofState, boolean z, EnumVerifier enumVerifier) {
        OneofInfo oneof = oneofState.getOneof(cls, fieldDescriptor.getContainingOneof());
        FieldType fieldType = getFieldType(fieldDescriptor);
        return FieldInfo.forOneofMemberField(fieldDescriptor.getNumber(), fieldType, oneof, getOneofStoredType(cls, fieldDescriptor, fieldType), z, enumVerifier);
    }

    private static Class<?> getOneofStoredType(Class<?> cls, FieldDescriptor fieldDescriptor, FieldType fieldType) {
        switch (fieldType.getJavaType()) {
            case BOOLEAN:
                return Boolean.class;
            case BYTE_STRING:
                return ByteString.class;
            case DOUBLE:
                return Double.class;
            case FLOAT:
                return Float.class;
            case ENUM:
            case INT:
                return Integer.class;
            case LONG:
                return Long.class;
            case STRING:
                return String.class;
            case MESSAGE:
                return getOneofStoredTypeForMessage(cls, fieldDescriptor);
            default:
                StringBuilder sb = new StringBuilder();
                sb.append("Invalid type for oneof: ");
                sb.append(fieldType);
                throw new IllegalArgumentException(sb.toString());
        }
    }

    private static FieldType getFieldType(FieldDescriptor fieldDescriptor) {
        switch (fieldDescriptor.getType()) {
            case BOOL:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.BOOL;
                }
                return fieldDescriptor.isPacked() ? FieldType.BOOL_LIST_PACKED : FieldType.BOOL_LIST;
            case BYTES:
                return fieldDescriptor.isRepeated() ? FieldType.BYTES_LIST : FieldType.BYTES;
            case DOUBLE:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.DOUBLE;
                }
                return fieldDescriptor.isPacked() ? FieldType.DOUBLE_LIST_PACKED : FieldType.DOUBLE_LIST;
            case ENUM:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.ENUM;
                }
                return fieldDescriptor.isPacked() ? FieldType.ENUM_LIST_PACKED : FieldType.ENUM_LIST;
            case FIXED32:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.FIXED32;
                }
                return fieldDescriptor.isPacked() ? FieldType.FIXED32_LIST_PACKED : FieldType.FIXED32_LIST;
            case FIXED64:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.FIXED64;
                }
                return fieldDescriptor.isPacked() ? FieldType.FIXED64_LIST_PACKED : FieldType.FIXED64_LIST;
            case FLOAT:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.FLOAT;
                }
                return fieldDescriptor.isPacked() ? FieldType.FLOAT_LIST_PACKED : FieldType.FLOAT_LIST;
            case GROUP:
                return fieldDescriptor.isRepeated() ? FieldType.GROUP_LIST : FieldType.GROUP;
            case INT32:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.INT32;
                }
                return fieldDescriptor.isPacked() ? FieldType.INT32_LIST_PACKED : FieldType.INT32_LIST;
            case INT64:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.INT64;
                }
                return fieldDescriptor.isPacked() ? FieldType.INT64_LIST_PACKED : FieldType.INT64_LIST;
            case MESSAGE:
                if (fieldDescriptor.isMapField()) {
                    return FieldType.MAP;
                }
                return fieldDescriptor.isRepeated() ? FieldType.MESSAGE_LIST : FieldType.MESSAGE;
            case SFIXED32:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.SFIXED32;
                }
                return fieldDescriptor.isPacked() ? FieldType.SFIXED32_LIST_PACKED : FieldType.SFIXED32_LIST;
            case SFIXED64:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.SFIXED64;
                }
                return fieldDescriptor.isPacked() ? FieldType.SFIXED64_LIST_PACKED : FieldType.SFIXED64_LIST;
            case SINT32:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.SINT32;
                }
                return fieldDescriptor.isPacked() ? FieldType.SINT32_LIST_PACKED : FieldType.SINT32_LIST;
            case SINT64:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.SINT64;
                }
                return fieldDescriptor.isPacked() ? FieldType.SINT64_LIST_PACKED : FieldType.SINT64_LIST;
            case STRING:
                return fieldDescriptor.isRepeated() ? FieldType.STRING_LIST : FieldType.STRING;
            case UINT32:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.UINT32;
                }
                return fieldDescriptor.isPacked() ? FieldType.UINT32_LIST_PACKED : FieldType.UINT32_LIST;
            case UINT64:
                if (!fieldDescriptor.isRepeated()) {
                    return FieldType.UINT64;
                }
                return fieldDescriptor.isPacked() ? FieldType.UINT64_LIST_PACKED : FieldType.UINT64_LIST;
            default:
                StringBuilder sb = new StringBuilder();
                sb.append("Unsupported field type: ");
                sb.append(fieldDescriptor.getType());
                throw new IllegalArgumentException(sb.toString());
        }
    }

    private static Field bitField(Class<?> cls, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("bitField");
        sb.append(i);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        return field(cls, sb.toString());
    }

    private static Field field(Class<?> cls, FieldDescriptor fieldDescriptor) {
        return field(cls, getFieldName(fieldDescriptor));
    }

    private static Field cachedSizeField(Class<?> cls, FieldDescriptor fieldDescriptor) {
        return field(cls, getCachedSizeFieldName(fieldDescriptor));
    }

    /* access modifiers changed from: private */
    public static Field field(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Exception unused) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to find field ");
            sb.append(str);
            sb.append(" in message class ");
            sb.append(cls.getName());
            throw new IllegalArgumentException(sb.toString());
        }
    }

    static String getFieldName(FieldDescriptor fieldDescriptor) {
        String str;
        if (fieldDescriptor.getType() == Type.GROUP) {
            str = fieldDescriptor.getMessageType().getName();
        } else {
            str = fieldDescriptor.getName();
        }
        String str2 = specialFieldNames.contains(str) ? "__" : EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR;
        StringBuilder sb = new StringBuilder();
        sb.append(snakeCaseToCamelCase(str));
        sb.append(str2);
        return sb.toString();
    }

    private static String getCachedSizeFieldName(FieldDescriptor fieldDescriptor) {
        StringBuilder sb = new StringBuilder();
        sb.append(snakeCaseToCamelCase(fieldDescriptor.getName()));
        sb.append("MemoizedSerializedSize");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public static String snakeCaseToCamelCase(String str) {
        StringBuilder sb = new StringBuilder(str.length() + 1);
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != '_') {
                if (Character.isDigit(charAt)) {
                    sb.append(charAt);
                } else {
                    if (z) {
                        sb.append(Character.toUpperCase(charAt));
                        z = false;
                    } else if (i == 0) {
                        sb.append(Character.toLowerCase(charAt));
                    } else {
                        sb.append(charAt);
                    }
                }
            }
            z = true;
        }
        return sb.toString();
    }

    private static Class<?> getOneofStoredTypeForMessage(Class<?> cls, FieldDescriptor fieldDescriptor) {
        try {
            return cls.getDeclaredMethod(getterForField(fieldDescriptor.getType() == Type.GROUP ? fieldDescriptor.getMessageType().getName() : fieldDescriptor.getName()), new Class[0]).getReturnType();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Class<?> getTypeForRepeatedMessageField(Class<?> cls, FieldDescriptor fieldDescriptor) {
        try {
            return cls.getDeclaredMethod(getterForField(fieldDescriptor.getType() == Type.GROUP ? fieldDescriptor.getMessageType().getName() : fieldDescriptor.getName()), new Class[]{Integer.TYPE}).getReturnType();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String getterForField(String str) {
        String snakeCaseToCamelCase = snakeCaseToCamelCase(str);
        StringBuilder sb = new StringBuilder("get");
        sb.append(Character.toUpperCase(snakeCaseToCamelCase.charAt(0)));
        sb.append(snakeCaseToCamelCase.substring(1, snakeCaseToCamelCase.length()));
        return sb.toString();
    }
}
