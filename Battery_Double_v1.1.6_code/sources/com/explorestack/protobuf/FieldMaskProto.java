package com.explorestack.protobuf;

import com.explorestack.protobuf.Descriptors.Descriptor;
import com.explorestack.protobuf.Descriptors.FileDescriptor;
import com.explorestack.protobuf.GeneratedMessageV3.FieldAccessorTable;

public final class FieldMaskProto {
    private static FileDescriptor descriptor = FileDescriptor.internalBuildGeneratedFileFrom(new String[]{"\n google/protobuf/field_mask.proto\u0012\u000fgoogle.protobuf\"\u001a\n\tFieldMask\u0012\r\n\u0005paths\u0018\u0001 \u0003(\tB\u0001\n\u0013com.google.protobufB\u000eFieldMaskProtoP\u0001Z9google.golang.org/genproto/protobuf/field_mask;field_maskø\u0001\u0001¢\u0002\u0003GPBª\u0002\u001eGoogle.Protobuf.WellKnownTypesb\u0006proto3"}, new FileDescriptor[0]);
    static final Descriptor internal_static_google_protobuf_FieldMask_descriptor = ((Descriptor) getDescriptor().getMessageTypes().get(0));
    static final FieldAccessorTable internal_static_google_protobuf_FieldMask_fieldAccessorTable = new FieldAccessorTable(internal_static_google_protobuf_FieldMask_descriptor, new String[]{"Paths"});

    public static void registerAllExtensions(ExtensionRegistryLite extensionRegistryLite) {
    }

    private FieldMaskProto() {
    }

    public static void registerAllExtensions(ExtensionRegistry extensionRegistry) {
        registerAllExtensions((ExtensionRegistryLite) extensionRegistry);
    }

    public static FileDescriptor getDescriptor() {
        return descriptor;
    }
}
