package com.explorestack.protobuf;

final class NewInstanceSchemaFull implements NewInstanceSchema {
    NewInstanceSchemaFull() {
    }

    public Object newInstance(Object obj) {
        return ((GeneratedMessageV3) obj).newInstance(UnusedPrivateParameter.INSTANCE);
    }
}
