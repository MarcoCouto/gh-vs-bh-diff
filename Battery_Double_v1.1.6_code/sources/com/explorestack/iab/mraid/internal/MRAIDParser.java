package com.explorestack.iab.mraid.internal;

import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MRAIDParser {
    private static final String TAG = "MRAIDParser";

    public Map<String, String> parseCommandUrl(String str) {
        String[] split;
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("parseCommandUrl ");
        sb.append(str);
        MRAIDLog.d(str2, sb.toString());
        String substring = str.substring(8);
        HashMap hashMap = new HashMap();
        int indexOf = substring.indexOf(63);
        if (indexOf != -1) {
            String substring2 = substring.substring(0, indexOf);
            for (String str3 : substring.substring(indexOf + 1).split(RequestParameters.AMPERSAND)) {
                int indexOf2 = str3.indexOf(61);
                hashMap.put(str3.substring(0, indexOf2), str3.substring(indexOf2 + 1));
            }
            substring = substring2;
        }
        if (!isValidCommand(substring)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("command ");
            sb2.append(substring);
            sb2.append(" is unknown");
            MRAIDLog.w(sb2.toString());
            return null;
        } else if (!checkParamsForCommand(substring, hashMap)) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("command URL ");
            sb3.append(str);
            sb3.append(" is missing parameters");
            MRAIDLog.w(sb3.toString());
            return null;
        } else {
            HashMap hashMap2 = new HashMap();
            hashMap2.put(String.COMMAND, substring);
            hashMap2.putAll(hashMap);
            return hashMap2;
        }
    }

    private boolean isValidCommand(String str) {
        return Arrays.asList(new String[]{"close", "createCalendarEvent", "expand", MraidJsMethods.OPEN, MraidJsMethods.PLAY_VIDEO, MraidJsMethods.RESIZE, "setOrientationProperties", "setResizeProperties", MRAIDNativeFeature.STORE_PICTURE, "useCustomClose", "noFill", ParametersKeys.LOADED, AvidVideoPlaybackListenerImpl.AD_STARTED, AvidVideoPlaybackListenerImpl.AD_STOPPED, AvidVideoPlaybackListenerImpl.AD_SKIPPED, "AdSkippableStateChange", AvidVideoPlaybackListenerImpl.AD_VIDEO_START, AvidVideoPlaybackListenerImpl.AD_VIDEO_FIRST_QUARTILE, AvidVideoPlaybackListenerImpl.AD_VIDEO_MIDPOINT, AvidVideoPlaybackListenerImpl.AD_VIDEO_THIRD_QUARTILE, AvidVideoPlaybackListenerImpl.AD_VIDEO_COMPLETE, AvidVideoPlaybackListenerImpl.AD_USER_CLOSE, AvidVideoPlaybackListenerImpl.AD_PAUSED, AvidVideoPlaybackListenerImpl.AD_PLAYING, AvidVideoPlaybackListenerImpl.AD_CLICK_THRU, "AdLog", AvidVideoPlaybackListenerImpl.AD_ERROR}).contains(str);
    }

    private boolean checkParamsForCommand(String str, Map<String, String> map) {
        if (str.equals("createCalendarEvent")) {
            return map.containsKey("eventJSON");
        }
        if (str.equals(MraidJsMethods.OPEN) || str.equals(MraidJsMethods.PLAY_VIDEO) || str.equals(MRAIDNativeFeature.STORE_PICTURE)) {
            return map.containsKey("url");
        }
        boolean z = false;
        if (str.equals("setOrientationProperties")) {
            if (map.containsKey("allowOrientationChange") && map.containsKey("forceOrientation")) {
                z = true;
            }
            return z;
        } else if (str.equals("setResizeProperties")) {
            if (map.containsKey("width") && map.containsKey("height") && map.containsKey("offsetX") && map.containsKey("offsetY") && map.containsKey("customClosePosition") && map.containsKey("allowOffscreen")) {
                z = true;
            }
            return z;
        } else if (str.equals("useCustomClose")) {
            return map.containsKey("useCustomClose");
        } else {
            return true;
        }
    }
}
