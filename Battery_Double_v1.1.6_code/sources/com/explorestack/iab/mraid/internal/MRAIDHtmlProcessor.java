package com.explorestack.iab.mraid.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MRAIDHtmlProcessor {
    static final /* synthetic */ boolean $assertionsDisabled = false;

    @VisibleForTesting
    static String createMetaStyle() {
        return "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\" /><style>body { margin:0; padding:0;}*:not(input) { -webkit-touch-callout:none; -webkit-user-select:none; -webkit-text-size-adjust:none; }</style>";
    }

    public static String processRawHtml(String str) {
        StringBuffer stringBuffer = new StringBuffer(str);
        Matcher matcher = Pattern.compile("<script\\s+[^>]*\\bsrc\\s*=\\s*([\\\"\\'])mraid\\.js\\1[^>]*>\\s*</script>\\n*", 2).matcher(stringBuffer);
        if (matcher.find()) {
            stringBuffer.delete(matcher.start(), matcher.end());
        }
        boolean contains = str.contains("<html");
        boolean contains2 = str.contains("<head");
        boolean contains3 = str.contains("<body");
        if ((!contains && (contains2 || contains3)) || (contains && !contains3)) {
            return str;
        }
        if (!contains) {
            stringBuffer.insert(0, "<html><head></head><body><div align=\"center\"></div></body></html>");
        } else if (!contains2) {
            Matcher matcher2 = Pattern.compile("<html[^>]*>", 2).matcher(stringBuffer);
            for (int i = 0; matcher2.find(i); i = matcher2.end()) {
                stringBuffer.insert(matcher2.end(), "<head></head>");
            }
        }
        injectMetaStyle(stringBuffer);
        return stringBuffer.toString();
    }

    @VisibleForTesting
    static void injectMetaStyle(@Nullable StringBuffer stringBuffer) {
        if (!TextUtils.isEmpty(stringBuffer)) {
            Matcher matcher = Pattern.compile("<head[^>]*>", 2).matcher(stringBuffer);
            List findQuotes = findQuotes(stringBuffer.toString(), '\"', '\'');
            while (true) {
                if (!matcher.find()) {
                    break;
                }
                int end = matcher.end();
                if (canInsertMetaStyle(end, findQuotes)) {
                    stringBuffer.insert(end, createMetaStyle());
                    break;
                }
            }
        }
    }

    @VisibleForTesting
    @NonNull
    static List<Pair<Integer, Integer>> findQuotes(@Nullable String str, @Nullable char... cArr) {
        ArrayList arrayList = new ArrayList();
        if (TextUtils.isEmpty(str) || cArr == null) {
            return arrayList;
        }
        try {
            int[] iArr = new int[cArr.length];
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                int i2 = 0;
                while (true) {
                    if (i2 >= cArr.length) {
                        break;
                    } else if (charAt == cArr[i2]) {
                        int i3 = iArr[i2];
                        if (i3 > 0) {
                            arrayList.add(new Pair(Integer.valueOf(i3), Integer.valueOf(i)));
                            iArr[i2] = 0;
                        } else {
                            iArr[i2] = i;
                        }
                    } else {
                        i2++;
                    }
                }
            }
        } catch (Throwable unused) {
        }
        return arrayList;
    }

    private static boolean canInsertMetaStyle(int i, @NonNull List<Pair<Integer, Integer>> list) {
        for (Pair pair : list) {
            if (i >= ((Integer) pair.first).intValue() && i <= ((Integer) pair.second).intValue()) {
                return false;
            }
        }
        return true;
    }
}
