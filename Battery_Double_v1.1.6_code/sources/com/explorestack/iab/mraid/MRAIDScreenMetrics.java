package com.explorestack.iab.mraid;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.explorestack.iab.utils.Utils;

class MRAIDScreenMetrics {
    @NonNull
    private final Rect currentAdRect = new Rect();
    @NonNull
    private final Rect currentAdRectDips = new Rect();
    @NonNull
    private final Rect defaultAdRect = new Rect();
    @NonNull
    private final Rect defaultAdRectDips = new Rect();
    private final float density;
    @NonNull
    private final Rect rootViewRect = new Rect();
    @NonNull
    private final Rect rootViewRectDips = new Rect();
    @NonNull
    private final Rect screenRect = new Rect();
    @NonNull
    private final Rect screenRectDips = new Rect();

    MRAIDScreenMetrics(float f) {
        this.density = f;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void convertToDips(Rect rect, Rect rect2) {
        rect2.set(Utils.pixelsToIntDips((float) rect.left, this.density), Utils.pixelsToIntDips((float) rect.top, this.density), Utils.pixelsToIntDips((float) rect.right, this.density), Utils.pixelsToIntDips((float) rect.bottom, this.density));
    }

    public float getDensity() {
        return this.density;
    }

    /* access modifiers changed from: 0000 */
    public boolean setScreenSize(int i, int i2) {
        if (this.screenRect.width() == i && this.screenRect.height() == i2) {
            return false;
        }
        this.screenRect.set(0, 0, i, i2);
        convertToDips(this.screenRect, this.screenRectDips);
        return true;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Rect getScreenRect() {
        return this.screenRect;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Rect getScreenRectDips() {
        return this.screenRectDips;
    }

    /* access modifiers changed from: 0000 */
    public boolean setRootViewPosition(int i, int i2, int i3, int i4) {
        return setPosition(this.rootViewRect, this.rootViewRectDips, i, i2, i3, i4);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Rect getRootViewRect() {
        return this.rootViewRect;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Rect getRootViewRectDips() {
        return this.rootViewRectDips;
    }

    /* access modifiers changed from: 0000 */
    public boolean setCurrentAdPosition(int i, int i2, int i3, int i4) {
        return setPosition(this.currentAdRect, this.currentAdRectDips, i, i2, i3, i4);
    }

    /* access modifiers changed from: 0000 */
    public boolean setCurrentAdPosition(@NonNull Rect rect) {
        return setPosition(this.currentAdRect, this.currentAdRectDips, rect);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Rect getCurrentAdRect() {
        return this.currentAdRect;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Rect getCurrentAdRectDips() {
        return this.currentAdRectDips;
    }

    /* access modifiers changed from: 0000 */
    public boolean setDefaultAdPosition(int i, int i2, int i3, int i4) {
        return setPosition(this.defaultAdRect, this.defaultAdRectDips, i, i2, i3, i4);
    }

    /* access modifiers changed from: 0000 */
    public boolean setDefaultAdPosition(@NonNull Rect rect) {
        return setPosition(this.defaultAdRect, this.defaultAdRectDips, rect);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Rect getDefaultAdRect() {
        return this.defaultAdRect;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Rect getDefaultAdRectDips() {
        return this.defaultAdRectDips;
    }

    private boolean setPosition(Rect rect, Rect rect2, int i, int i2, int i3, int i4) {
        if (rect.left == i && rect.top == i2 && i + i3 == rect.right && i2 + i4 == rect.bottom) {
            return false;
        }
        rect.set(i, i2, i3 + i, i4 + i2);
        convertToDips(rect, rect2);
        return true;
    }

    private boolean setPosition(Rect rect, Rect rect2, Rect rect3) {
        if (rect.equals(rect3)) {
            return false;
        }
        rect.set(rect3);
        convertToDips(rect, rect2);
        return true;
    }
}
