package com.explorestack.iab.mraid;

public interface MRAIDViewListener {
    void mraidViewClose(MRAIDView mRAIDView);

    void mraidViewExpand(MRAIDView mRAIDView);

    void mraidViewLoaded(MRAIDView mRAIDView);

    void mraidViewNoFill(MRAIDView mRAIDView);

    boolean mraidViewResize(MRAIDView mRAIDView, int i, int i2, int i3, int i4);
}
