package com.explorestack.iab.mraid;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.explorestack.iab.IabSettings;
import com.explorestack.iab.mraid.internal.MRAIDHtmlProcessor;
import com.explorestack.iab.mraid.internal.MRAIDLog;
import com.explorestack.iab.mraid.internal.MRAIDLog.LOG_LEVEL;
import com.explorestack.iab.mraid.internal.MRAIDNativeFeatureManager;
import com.explorestack.iab.mraid.internal.MRAIDParser;
import com.explorestack.iab.mraid.properties.MRAIDOrientationProperties;
import com.explorestack.iab.mraid.properties.MRAIDResizeProperties;
import com.explorestack.iab.utils.Assets;
import com.explorestack.iab.utils.Utils;
import com.explorestack.iab.vast.view.CircleCountdownView;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

@SuppressLint({"ViewConstructor"})
public class MRAIDView extends RelativeLayout {
    private static final int CLOSE_REGION_SIZE = 50;
    private static final int PROGRESS_TIMER_INTERVAL = 40;
    public static final int STATE_DEFAULT = 1;
    public static final int STATE_EXPANDED = 2;
    public static final int STATE_HIDDEN = 4;
    public static final int STATE_LOADING = 0;
    public static final int STATE_RESIZED = 3;
    private static final String TAG = "MRAIDView";
    /* access modifiers changed from: private */
    public String baseUrl;
    /* access modifiers changed from: private */
    public CircleCountdownView closeRegion;
    private int closeTime;
    /* access modifiers changed from: private */
    public int closeTimerPosition;
    /* access modifiers changed from: private */
    @NonNull
    public Context context;
    /* access modifiers changed from: private */
    public WebView currentWebView;
    /* access modifiers changed from: private */
    public DisplayMetrics displayMetrics;
    private RelativeLayout expandedView;
    private GestureDetector gestureDetector;
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public Activity interstitialActivity;
    /* access modifiers changed from: private */
    public int interstitialInLayoutId;
    private boolean isActionBarShowing;
    /* access modifiers changed from: private */
    public boolean isCloseClickable;
    private boolean isClosing;
    private boolean isExpandingFromDefault;
    /* access modifiers changed from: private */
    public boolean isExpandingPart2;
    private boolean isForceNotFullScreen;
    private boolean isFullScreen;
    /* access modifiers changed from: private */
    public final boolean isInterstitial;
    /* access modifiers changed from: private */
    public boolean isLaidOut;
    /* access modifiers changed from: private */
    public boolean isPageFinished;
    private boolean isShown;
    /* access modifiers changed from: private */
    public final boolean isTag;
    /* access modifiers changed from: private */
    public boolean isTouched;
    /* access modifiers changed from: private */
    public boolean isViewable;
    /* access modifiers changed from: private */
    public MRAIDViewListener listener;
    private String mData;
    private String mraidJs;
    private MRAIDWebChromeClient mraidWebChromeClient;
    private MRAIDWebViewClient mraidWebViewClient;
    private MRAIDNativeFeatureListener nativeFeatureListener;
    private MRAIDNativeFeatureManager nativeFeatureManager;
    private MRAIDOrientationProperties orientationProperties;
    private int origTitleBarVisibility;
    private final int originalRequestedOrientation;
    /* access modifiers changed from: private */
    public boolean preload;
    private int previousLayoutHeight;
    private int previousLayoutWidth;
    private int previousWebViewLayoutHeight;
    private int previousWebViewLayoutWidth;
    private MRAIDResizeProperties resizeProperties;
    private RelativeLayout resizedView;
    private MRAIDScreenMetrics screenMetrics;
    /* access modifiers changed from: private */
    public int state;
    private View titleBar;
    private boolean useCustomClose;
    /* access modifiers changed from: private */
    public WebView webView;
    /* access modifiers changed from: private */
    public WebView webViewPart2;

    private class MRAIDWebChromeClient extends WebChromeClient {
        private MRAIDWebChromeClient() {
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            String str;
            if (consoleMessage == null || consoleMessage.message() == null) {
                return false;
            }
            if (!consoleMessage.message().contains("Uncaught ReferenceError")) {
                String str2 = "JS console";
                StringBuilder sb = new StringBuilder();
                sb.append(consoleMessage.message());
                if (consoleMessage.sourceId() == null) {
                    str = "";
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(" at ");
                    sb2.append(consoleMessage.sourceId());
                    str = sb2.toString();
                }
                sb.append(str);
                sb.append(":");
                sb.append(consoleMessage.lineNumber());
                MRAIDLog.i(str2, sb.toString());
            }
            if (consoleMessage.message().contains("AppodealAlert")) {
                Log.e("Appodeal", consoleMessage.message().replace("AppodealAlert:", ""));
            }
            return true;
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            MRAIDLog.d("JS alert", str2);
            return handlePopups(jsResult);
        }

        public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
            MRAIDLog.d("JS confirm", str2);
            return handlePopups(jsResult);
        }

        public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
            MRAIDLog.d("JS prompt", str2);
            return handlePopups(jsPromptResult);
        }

        private boolean handlePopups(JsResult jsResult) {
            jsResult.cancel();
            return true;
        }
    }

    private class MRAIDWebViewClient extends WebViewClient {
        private MRAIDWebViewClient() {
        }

        public void onPageFinished(WebView webView, String str) {
            String str2 = MRAIDView.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onPageFinished: ");
            sb.append(str);
            MRAIDLog.d(str2, sb.toString());
            super.onPageFinished(webView, str);
            if (MRAIDView.this.state == 0) {
                MRAIDView.this.isPageFinished = true;
                MRAIDView mRAIDView = MRAIDView.this;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("mraid.setPlacementType('");
                sb2.append(MRAIDView.this.isInterstitial ? VideoType.INTERSTITIAL : String.INLINE);
                sb2.append("');");
                mRAIDView.injectJavaScript(sb2.toString());
                MRAIDView.this.setSupportedServices();
                if (MRAIDView.this.isLaidOut) {
                    MRAIDView.this.injectSetScreenSize();
                    MRAIDView.this.injectSetMaxSize();
                    MRAIDView.this.injectSetDefaultPosition();
                    MRAIDView.this.injectSetCurrentPosition();
                    if (MRAIDView.this.isInterstitial) {
                        MRAIDView.this.showAsInterstitial(null, MRAIDView.this.interstitialInLayoutId);
                    } else {
                        MRAIDView.this.state = 1;
                        MRAIDView.this.fireStateChangeEvent();
                        MRAIDView.this.fireReadyEvent();
                        if (MRAIDView.this.isViewable) {
                            MRAIDView.this.fireViewableChangeEvent();
                        }
                    }
                }
                if (MRAIDView.this.listener != null && !str.equals("data:text/html,<html></html>") && MRAIDView.this.preload && !MRAIDView.this.isTag) {
                    MRAIDView.this.listener.mraidViewLoaded(MRAIDView.this);
                }
            }
            if (MRAIDView.this.isExpandingPart2) {
                MRAIDView.this.isExpandingPart2 = false;
                MRAIDView.this.handler.post(new Runnable() {
                    public void run() {
                        MRAIDView mRAIDView = MRAIDView.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("mraid.setPlacementType('");
                        sb.append(MRAIDView.this.isInterstitial ? VideoType.INTERSTITIAL : String.INLINE);
                        sb.append("');");
                        mRAIDView.injectJavaScript(sb.toString());
                        MRAIDView.this.setSupportedServices();
                        MRAIDView.this.injectSetScreenSize();
                        MRAIDView.this.injectSetDefaultPosition();
                        MRAIDLog.d(MRAIDView.TAG, "calling fireStateChangeEvent 2");
                        MRAIDView.this.fireStateChangeEvent();
                        MRAIDView.this.fireReadyEvent();
                        if (MRAIDView.this.isViewable) {
                            MRAIDView.this.fireViewableChangeEvent();
                        }
                    }
                });
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            String str3 = MRAIDView.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onReceivedError: ");
            sb.append(str);
            MRAIDLog.d(str3, sb.toString());
            super.onReceivedError(webView, i, str, str2);
        }

        @TargetApi(24)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            if (webResourceRequest.hasGesture()) {
                MRAIDView.this.isTouched = true;
            }
            return shouldOverrideUrlLoading(webView, webResourceRequest.getUrl().toString());
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            String str2 = MRAIDView.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("shouldOverrideUrlLoading: ");
            sb.append(str);
            MRAIDLog.d(str2, sb.toString());
            if (str.startsWith("mraid://")) {
                MRAIDView.this.parseCommandUrl(str);
                return true;
            }
            MRAIDView.this.open(str, webView);
            return true;
        }

        @TargetApi(26)
        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            MRAIDLog.d(MRAIDView.TAG, String.format("onRenderProcessGone didCrash: %s", new Object[]{Boolean.valueOf(renderProcessGoneDetail.didCrash())}));
            MRAIDView.this.removeView(MRAIDView.this.webView);
            MRAIDView.this.webView.destroy();
            MRAIDView.this.addWebView();
            MRAIDView.this.load();
            return true;
        }
    }

    public static class builder {
        String baseUrl;
        int closeTime = -1;
        @NonNull
        Context context;
        String data;
        int height;
        boolean isInterstitial = false;
        boolean isTag;
        @VisibleForTesting
        public MRAIDViewListener listener;
        @VisibleForTesting
        public MRAIDNativeFeatureListener nativeFeatureListener;
        boolean preload;
        String[] supportedNativeFeatures = null;
        boolean useLayout;
        int width;

        public builder(@NonNull Context context2, String str, int i, int i2) {
            this.context = context2;
            this.data = str;
            this.width = i;
            this.height = i2;
        }

        public builder setBaseUrl(String str) {
            this.baseUrl = str;
            return this;
        }

        public builder setSupportedNativeFeatures(String[] strArr) {
            this.supportedNativeFeatures = strArr;
            return this;
        }

        public builder setListener(MRAIDViewListener mRAIDViewListener) {
            this.listener = mRAIDViewListener;
            return this;
        }

        public builder setNativeFeatureListener(MRAIDNativeFeatureListener mRAIDNativeFeatureListener) {
            this.nativeFeatureListener = mRAIDNativeFeatureListener;
            return this;
        }

        public builder setIsInterstitial(boolean z) {
            this.isInterstitial = z;
            return this;
        }

        public builder setPreload(boolean z) {
            this.preload = z;
            return this;
        }

        public builder setCloseTime(int i) {
            this.closeTime = i;
            return this;
        }

        public builder setIsTag(boolean z) {
            this.isTag = z;
            return this;
        }

        public builder setUseLayout(boolean z) {
            this.useLayout = z;
            return this;
        }

        public MRAIDView build() {
            return new MRAIDView(this);
        }
    }

    private static String getOrientationString(int i) {
        switch (i) {
            case -1:
                return "UNSPECIFIED";
            case 0:
                return "LANDSCAPE";
            case 1:
                return "PORTRAIT";
            default:
                return "UNKNOWN";
        }
    }

    /* access modifiers changed from: private */
    public static String getVisibilityString(int i) {
        return i != 0 ? i != 4 ? i != 8 ? "UNKNOWN" : "GONE" : "INVISIBLE" : "VISIBLE";
    }

    private MRAIDView(builder builder2) {
        int i;
        super(builder2.context);
        this.interstitialInLayoutId = -1;
        this.isShown = false;
        this.isCloseClickable = true;
        this.isTouched = false;
        this.closeTimerPosition = 0;
        this.previousLayoutWidth = 0;
        this.previousLayoutHeight = 0;
        this.previousWebViewLayoutWidth = 0;
        this.previousWebViewLayoutHeight = 0;
        if (builder2.supportedNativeFeatures == null) {
            builder2.supportedNativeFeatures = new String[0];
        }
        this.context = builder2.context;
        this.baseUrl = builder2.baseUrl;
        this.isInterstitial = builder2.isInterstitial;
        this.preload = builder2.preload;
        this.closeTime = builder2.closeTime;
        this.isTag = builder2.isTag;
        this.state = 0;
        this.isViewable = false;
        this.useCustomClose = false;
        this.orientationProperties = new MRAIDOrientationProperties();
        this.resizeProperties = new MRAIDResizeProperties();
        this.nativeFeatureManager = new MRAIDNativeFeatureManager(this.context, new ArrayList(Arrays.asList(builder2.supportedNativeFeatures)));
        this.listener = builder2.listener;
        this.nativeFeatureListener = builder2.nativeFeatureListener;
        this.displayMetrics = new DisplayMetrics();
        if (this.context instanceof Activity) {
            ((Activity) this.context).getWindowManager().getDefaultDisplay().getMetrics(this.displayMetrics);
        } else {
            this.context.getResources().getDisplayMetrics().setTo(this.displayMetrics);
        }
        float f = this.displayMetrics.density;
        this.screenMetrics = new MRAIDScreenMetrics(f);
        if (this.context instanceof Activity) {
            this.originalRequestedOrientation = ((Activity) this.context).getRequestedOrientation();
        } else {
            this.originalRequestedOrientation = -1;
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("originalRequestedOrientation ");
        sb.append(getOrientationString(this.originalRequestedOrientation));
        MRAIDLog.d(str, sb.toString());
        this.gestureDetector = new GestureDetector(getContext(), new SimpleOnGestureListener() {
            public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
                return true;
            }
        });
        this.handler = new Handler(Looper.getMainLooper());
        this.mraidWebChromeClient = new MRAIDWebChromeClient();
        this.mraidWebViewClient = new MRAIDWebViewClient();
        String replace = getMraidJs().replace("%%VERSION%%", IabSettings.mediatorVersion);
        builder2.data = MRAIDHtmlProcessor.processRawHtml(builder2.data);
        Display defaultDisplay = ((WindowManager) this.context.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        int i2 = point.x;
        if (this.isInterstitial) {
            i = point.y;
        } else {
            i = Math.round(((float) builder2.height) * f);
        }
        float f2 = (float) i2;
        float f3 = (float) i;
        float f4 = f2 / f3;
        float f5 = ((float) builder2.width) / ((float) builder2.height);
        if (!Float.isNaN(f5)) {
            if (f5 <= f4) {
                i2 = Math.round(f3 * f5);
            } else {
                i = Math.round(f2 / f5);
            }
        }
        int round = Math.round(((float) i2) / f);
        int round2 = Math.round(((float) i) / f);
        if (round > builder2.width && round2 > builder2.height && builder2.width != 0 && builder2.height != 0) {
            round = builder2.width;
            round2 = builder2.height;
        }
        MRAIDLog.d(String.format("use layout: %s", new Object[]{Boolean.valueOf(builder2.useLayout)}));
        if (builder2.useLayout) {
            this.mData = String.format("<style type='text/css'>%s</style><script type='application/javascript'>%s</script><div class='appodeal-outer'><div class='appodeal-middle'><div class='appodeal-inner'>%s</div></div></div>", new Object[]{String.format("body, p {margin:0; padding:0} img {max-width:%dpx; max-height:%dpx} #appnext-interstitial {min-width:%dpx; min-height:%dpx;}img[width='%d'][height='%d'] {width: %dpx; height: %dpx} .appodeal-outer {display: table; position: absolute; height: 100%%; width: 100%%;}.appodeal-middle {display: table-cell; vertical-align: middle;}.appodeal-inner {margin-left: auto; margin-right: auto; width: %dpx; height: %dpx;}.ad_slug_table {margin-left: auto !important; margin-right: auto !important;} #ad[align='center'] {height: %dpx;} #voxelPlayer {position: relative !important;} #lsm_mobile_ad #wrapper, #lsm_overlay {position: relative !important;}", new Object[]{Integer.valueOf(round), Integer.valueOf(round2), Integer.valueOf(round), Integer.valueOf(round2), Integer.valueOf(builder2.width), Integer.valueOf(builder2.height), Integer.valueOf(round), Integer.valueOf(round2), Integer.valueOf(round), Integer.valueOf(round2), Integer.valueOf(round2)}), replace, builder2.data});
        } else {
            this.mData = String.format("<script type='application/javascript'>%s</script>%s", new Object[]{replace, builder2.data});
        }
        addWebView();
    }

    /* access modifiers changed from: private */
    public void addWebView() {
        this.webView = createWebView();
        setCurrentWebView(this.webView);
        addView(this.webView);
    }

    /* access modifiers changed from: private */
    public void setCurrentWebView(WebView webView2) {
        this.currentWebView = webView2;
        this.previousWebViewLayoutWidth = 0;
        this.previousWebViewLayoutHeight = 0;
    }

    public void load() {
        if (this.preload) {
            this.webView.loadDataWithBaseURL(this.baseUrl, this.mData, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        } else {
            this.listener.mraidViewLoaded(this);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("log level = ");
        sb.append(MRAIDLog.getLoggingLevel());
        MRAIDLog.d(sb.toString());
        if (MRAIDLog.getLoggingLevel() == LOG_LEVEL.verbose) {
            injectJavaScript(this.webView, "mraid.logLevel = mraid.LogLevelEnum.DEBUG;");
        } else if (MRAIDLog.getLoggingLevel() == LOG_LEVEL.debug) {
            injectJavaScript(this.webView, "mraid.logLevel = mraid.LogLevelEnum.DEBUG;");
        } else if (MRAIDLog.getLoggingLevel() == LOG_LEVEL.info) {
            injectJavaScript(this.webView, "mraid.logLevel = mraid.LogLevelEnum.INFO;");
        } else if (MRAIDLog.getLoggingLevel() == LOG_LEVEL.warning) {
            injectJavaScript(this.webView, "mraid.logLevel = mraid.LogLevelEnum.WARNING;");
        } else if (MRAIDLog.getLoggingLevel() == LOG_LEVEL.error) {
            injectJavaScript(this.webView, "mraid.logLevel = mraid.LogLevelEnum.ERROR;");
        } else if (MRAIDLog.getLoggingLevel() == LOG_LEVEL.none) {
            injectJavaScript(this.webView, "mraid.logLevel = mraid.LogLevelEnum.NONE;");
        }
    }

    public int getState() {
        return this.state;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"SetJavaScriptEnabled"})
    public WebView createWebView() {
        AnonymousClass2 r0 = new WebView(this.context.getApplicationContext()) {
            private static final String TAG = "MRAIDView-WebView";

            public void computeScroll() {
            }

            /* access modifiers changed from: protected */
            public boolean overScrollBy(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z) {
                return false;
            }

            /* access modifiers changed from: protected */
            public void onLayout(boolean z, int i, int i2, int i3, int i4) {
                super.onLayout(z, i, i2, i3, i4);
                MRAIDView.this.onLayoutWebView(this, z, i, i2, i3, i4);
            }

            public void onConfigurationChanged(Configuration configuration) {
                super.onConfigurationChanged(configuration);
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("onConfigurationChanged ");
                sb.append(configuration.orientation == 1 ? "portrait" : "landscape");
                MRAIDLog.d(str, sb.toString());
                if (!MRAIDView.this.isInterstitial) {
                    return;
                }
                if (MRAIDView.this.interstitialActivity != null) {
                    MRAIDView.this.interstitialActivity.getWindowManager().getDefaultDisplay().getMetrics(MRAIDView.this.displayMetrics);
                } else if (MRAIDView.this.context instanceof Activity) {
                    ((Activity) MRAIDView.this.context).getWindowManager().getDefaultDisplay().getMetrics(MRAIDView.this.displayMetrics);
                }
            }

            /* access modifiers changed from: protected */
            public void onVisibilityChanged(View view, int i) {
                super.onVisibilityChanged(view, i);
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("onVisibilityChanged ");
                sb.append(MRAIDView.getVisibilityString(i));
                MRAIDLog.d(str, sb.toString());
                if (MRAIDView.this.isInterstitial) {
                    MRAIDView.this.setViewable(i);
                }
            }

            public void onWindowFocusChanged(boolean z) {
                super.onWindowFocusChanged(z);
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("onWindowFocusChanged: ");
                sb.append(z);
                MRAIDLog.d(str, sb.toString());
                if (MRAIDView.this.isInterstitial) {
                    MRAIDView.this.setViewable(z ? getVisibility() : 8);
                }
                if (z) {
                    MRAIDView.this.resumeWebView(this);
                } else {
                    MRAIDView.this.pauseWebView(this);
                }
            }
        };
        r0.setLayoutParams(new LayoutParams(-1, -1));
        r0.setScrollContainer(false);
        r0.setVerticalScrollBarEnabled(false);
        r0.setHorizontalScrollBarEnabled(false);
        r0.setScrollBarStyle(33554432);
        r0.setFocusableInTouchMode(false);
        r0.setOnTouchListener(new OnTouchListener() {
            @SuppressLint({"ClickableViewAccessibility"})
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                    case 1:
                        MRAIDView.this.isTouched = true;
                        if (!view.hasFocus()) {
                            view.requestFocus();
                            break;
                        }
                        break;
                }
                return false;
            }
        });
        r0.getSettings().setJavaScriptEnabled(true);
        r0.getSettings().setDomStorageEnabled(true);
        if (VERSION.SDK_INT >= 17 && this.isInterstitial) {
            r0.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        r0.setWebChromeClient(this.mraidWebChromeClient);
        r0.setWebViewClient(this.mraidWebViewClient);
        r0.setBackgroundColor(0);
        return r0;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.gestureDetector.onTouchEvent(motionEvent)) {
            motionEvent.setAction(3);
        }
        return super.onTouchEvent(motionEvent);
    }

    public void clearView() {
        if (this.webView != null) {
            this.webView.setWebChromeClient(null);
            this.webView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    String str2 = MRAIDView.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("shouldOverrideUrlLoading: ");
                    sb.append(str);
                    MRAIDLog.d(str2, sb.toString());
                    return true;
                }

                @TargetApi(24)
                public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
                    String str = MRAIDView.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("shouldOverrideUrlLoading: ");
                    sb.append(webResourceRequest.getUrl().toString());
                    MRAIDLog.d(str, sb.toString());
                    return true;
                }
            });
            this.webView.loadUrl("about:blank");
        }
    }

    public void destroy() {
        this.context = null;
        this.nativeFeatureManager = null;
        this.listener = null;
        this.interstitialActivity = null;
        this.interstitialInLayoutId = -1;
        if (this.webView != null) {
            try {
                this.webView.removeAllViews();
                this.webView.setWebChromeClient(null);
                this.webView.setWebViewClient(null);
                this.webView.destroy();
                this.webView = null;
                this.currentWebView = null;
            } catch (Exception e) {
                MRAIDLog.e(e.getMessage());
            }
        }
    }

    /* access modifiers changed from: private */
    public void parseCommandUrl(String str) {
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("parseCommandUrl ");
        sb.append(str);
        MRAIDLog.d(str2, sb.toString());
        Map parseCommandUrl = new MRAIDParser().parseCommandUrl(str);
        String str3 = (String) parseCommandUrl.get(String.COMMAND);
        String[] strArr = {"createCalendarEvent", "expand", MraidJsMethods.OPEN, MraidJsMethods.PLAY_VIDEO, MRAIDNativeFeature.STORE_PICTURE, "useCustomClose"};
        String[] strArr2 = {"setOrientationProperties", "setResizeProperties"};
        try {
            if (Arrays.asList(new String[]{"close", MraidJsMethods.RESIZE, "noFill", ParametersKeys.LOADED}).contains(str3)) {
                getClass().getDeclaredMethod(str3, new Class[0]).invoke(this, new Object[0]);
            } else if (Arrays.asList(strArr).contains(str3)) {
                Method declaredMethod = getClass().getDeclaredMethod(str3, new Class[]{String.class});
                String str4 = str3.equals("createCalendarEvent") ? "eventJSON" : str3.equals("useCustomClose") ? "useCustomClose" : "url";
                declaredMethod.invoke(this, new Object[]{(String) parseCommandUrl.get(str4)});
            } else if (Arrays.asList(strArr2).contains(str3)) {
                getClass().getDeclaredMethod(str3, new Class[]{Map.class}).invoke(this, new Object[]{parseCommandUrl});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void noFill() {
        MRAIDLog.d("MRAIDView-JS callback", "noFill");
        if (this.listener != null) {
            this.listener.mraidViewNoFill(this);
        }
    }

    private void loaded() {
        MRAIDLog.d("MRAIDView-JS callback", ParametersKeys.LOADED);
        if (this.listener != null) {
            this.listener.mraidViewLoaded(this);
        }
    }

    /* access modifiers changed from: private */
    public void close() {
        MRAIDLog.d("MRAIDView-JS callback", "close");
        if (this.isCloseClickable || this.useCustomClose) {
            this.handler.post(new Runnable() {
                public void run() {
                    if (MRAIDView.this.state != 0 && ((MRAIDView.this.state != 1 || MRAIDView.this.isInterstitial) && MRAIDView.this.state != 4)) {
                        if (MRAIDView.this.state == 1 || MRAIDView.this.state == 2) {
                            MRAIDView.this.closeFromExpanded();
                        } else if (MRAIDView.this.state == 3) {
                            MRAIDView.this.closeFromResized();
                        }
                    }
                }
            });
        }
    }

    private void createCalendarEvent(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("createCalendarEvent ");
        sb.append(str);
        MRAIDLog.d("MRAIDView-JS callback", sb.toString());
        if (this.nativeFeatureListener != null) {
            this.nativeFeatureListener.mraidNativeFeatureCreateCalendarEvent(str);
        }
    }

    @TargetApi(11)
    private void expand(String str, final Activity activity, final int i) {
        String str2 = "MRAIDView-JS callback";
        StringBuilder sb = new StringBuilder();
        sb.append("expand ");
        sb.append(str != null ? str : "(1-part)");
        MRAIDLog.d(str2, sb.toString());
        if ((this.isInterstitial && this.state != 0) || (!this.isInterstitial && this.state != 1 && this.state != 3)) {
            return;
        }
        if (TextUtils.isEmpty(str)) {
            if (this.isInterstitial || this.state == 1) {
                if (this.webView.getParent() != null) {
                    ((ViewGroup) this.webView.getParent()).removeView(this.webView);
                } else {
                    removeView(this.webView);
                }
            } else if (this.state == 3) {
                removeResizeView();
            }
            expandHelper(this.webView, activity, i);
            return;
        }
        try {
            final String decode = URLDecoder.decode(str, "UTF-8");
            if (!decode.startsWith("http://") && !decode.startsWith("https://")) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.baseUrl);
                sb2.append(decode);
                decode = sb2.toString();
            }
            new Thread(new Runnable() {
                public void run() {
                    final String access$1500 = MRAIDView.this.getStringFromUrl(decode);
                    if (!TextUtils.isEmpty(access$1500)) {
                        ((Activity) MRAIDView.this.context).runOnUiThread(new Runnable() {
                            public void run() {
                                if (MRAIDView.this.state == 3) {
                                    MRAIDView.this.removeResizeView();
                                    MRAIDView.this.addView(MRAIDView.this.webView);
                                }
                                MRAIDView.this.webView.setWebChromeClient(null);
                                MRAIDView.this.webView.setWebViewClient(null);
                                MRAIDView.this.webViewPart2 = MRAIDView.this.createWebView();
                                MRAIDView.this.webViewPart2.loadDataWithBaseURL(MRAIDView.this.baseUrl, access$1500, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
                                MRAIDView.this.setCurrentWebView(MRAIDView.this.webViewPart2);
                                MRAIDView.this.isExpandingPart2 = true;
                                MRAIDView.this.expandHelper(MRAIDView.this.currentWebView, activity, i);
                            }
                        });
                        return;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("Could not load part 2 expanded content for URL: ");
                    sb.append(decode);
                    MRAIDLog.e(sb.toString());
                }
            }, "2-part-content").start();
        } catch (UnsupportedEncodingException unused) {
        }
    }

    private void open(String str) {
        open(str, null);
    }

    /* access modifiers changed from: private */
    public void open(String str, WebView webView2) {
        if (this.isTouched) {
            StringBuilder sb = new StringBuilder();
            sb.append("open ");
            sb.append(str);
            MRAIDLog.d("MRAIDView-JS callback", sb.toString());
            if (this.nativeFeatureListener == null) {
                return;
            }
            if (str.startsWith("sms")) {
                this.nativeFeatureListener.mraidNativeFeatureSendSms(str);
            } else if (str.startsWith("tel")) {
                this.nativeFeatureListener.mraidNativeFeatureCallTel(str);
            } else {
                this.nativeFeatureListener.mraidNativeFeatureOpenBrowser(str, webView2);
            }
        } else {
            MRAIDLog.d(TAG, "mraid view not touched");
        }
    }

    private void playVideo(String str) {
        try {
            String decode = URLDecoder.decode(str, "UTF-8");
            StringBuilder sb = new StringBuilder();
            sb.append("playVideo ");
            sb.append(decode);
            MRAIDLog.d("MRAIDView-JS callback", sb.toString());
            if (this.nativeFeatureListener != null) {
                this.nativeFeatureListener.mraidNativeFeaturePlayVideo(decode);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void resize() {
        MRAIDLog.d("MRAIDView-JS callback", MraidJsMethods.RESIZE);
        if (this.listener != null) {
            if (this.listener.mraidViewResize(this, this.resizeProperties.width, this.resizeProperties.height, this.resizeProperties.offsetX, this.resizeProperties.offsetY)) {
                this.state = 3;
                if (this.resizedView == null) {
                    this.resizedView = new RelativeLayout(this.context);
                    removeAllViews();
                    this.resizedView.addView(this.webView);
                    addCloseRegion(this.resizedView);
                    FrameLayout frameLayout = (FrameLayout) getRootView().findViewById(16908290);
                    if (frameLayout != null) {
                        frameLayout.addView(this.resizedView);
                    }
                }
                setCloseRegionPosition(this.resizedView);
                setResizedViewSize();
                setResizedViewPosition();
                this.handler.post(new Runnable() {
                    public void run() {
                        MRAIDView.this.fireStateChangeEvent();
                    }
                });
            }
        }
    }

    private void setOrientationProperties(Map<String, String> map) {
        boolean parseBoolean = Boolean.parseBoolean((String) map.get("allowOrientationChange"));
        String str = (String) map.get("forceOrientation");
        StringBuilder sb = new StringBuilder();
        sb.append("setOrientationProperties ");
        sb.append(parseBoolean);
        sb.append(" ");
        sb.append(str);
        MRAIDLog.d("MRAIDView-JS callback", sb.toString());
        if (this.orientationProperties.allowOrientationChange != parseBoolean || this.orientationProperties.forceOrientation != MRAIDOrientationProperties.forceOrientationFromString(str)) {
            this.orientationProperties.allowOrientationChange = parseBoolean;
            this.orientationProperties.forceOrientation = MRAIDOrientationProperties.forceOrientationFromString(str);
            if (this.isInterstitial || this.state == 2) {
                applyOrientationProperties();
            }
        }
    }

    private void setResizeProperties(Map<String, String> map) {
        int parseInt = Integer.parseInt((String) map.get("width"));
        int parseInt2 = Integer.parseInt((String) map.get("height"));
        int parseInt3 = Integer.parseInt((String) map.get("offsetX"));
        int parseInt4 = Integer.parseInt((String) map.get("offsetY"));
        String str = (String) map.get("customClosePosition");
        boolean parseBoolean = Boolean.parseBoolean((String) map.get("allowOffscreen"));
        StringBuilder sb = new StringBuilder();
        sb.append("setResizeProperties ");
        sb.append(parseInt);
        sb.append(" ");
        sb.append(parseInt2);
        sb.append(" ");
        sb.append(parseInt3);
        sb.append(" ");
        sb.append(parseInt4);
        sb.append(" ");
        sb.append(str);
        sb.append(" ");
        sb.append(parseBoolean);
        MRAIDLog.d("MRAIDView-JS callback", sb.toString());
        this.resizeProperties.width = parseInt;
        this.resizeProperties.height = parseInt2;
        this.resizeProperties.offsetX = parseInt3;
        this.resizeProperties.offsetY = parseInt4;
        this.resizeProperties.customClosePosition = MRAIDResizeProperties.customClosePositionFromString(str);
        this.resizeProperties.allowOffscreen = parseBoolean;
    }

    private void storePicture(String str) {
        try {
            String decode = URLDecoder.decode(str, "UTF-8");
            StringBuilder sb = new StringBuilder();
            sb.append("storePicture ");
            sb.append(decode);
            MRAIDLog.d("MRAIDView-JS callback", sb.toString());
            if (this.nativeFeatureListener != null) {
                this.nativeFeatureListener.mraidNativeFeatureStorePicture(decode);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void useCustomClose(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("useCustomClose ");
        sb.append(str);
        MRAIDLog.d("MRAIDView-JS callback", sb.toString());
        boolean parseBoolean = Boolean.parseBoolean(str);
        if (this.useCustomClose != parseBoolean) {
            this.useCustomClose = parseBoolean;
            if (parseBoolean) {
                removeDefaultCloseButton();
            } else {
                showDefaultCloseButton();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x008f, code lost:
        r8 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0091, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0092, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009c, code lost:
        if (r0 != null) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c2, code lost:
        if (r0 != null) goto L_0x009e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0091 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:10:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00c8 A[SYNTHETIC, Splitter:B:43:0x00c8] */
    public String getStringFromUrl(String str) {
        String str2;
        if (str.startsWith("file:///")) {
            return getStringFromFileUrl(str);
        }
        InputStream inputStream = null;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            int responseCode = httpURLConnection.getResponseCode();
            String str3 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("response code ");
            sb.append(responseCode);
            MRAIDLog.d(str3, sb.toString());
            if (responseCode == 200) {
                String str4 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("getContentLength ");
                sb2.append(httpURLConnection.getContentLength());
                MRAIDLog.d(str4, sb2.toString());
                InputStream inputStream2 = httpURLConnection.getInputStream();
                try {
                    byte[] bArr = new byte[1500];
                    StringBuilder sb3 = new StringBuilder();
                    while (true) {
                        int read = inputStream2.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        sb3.append(new String(bArr, 0, read));
                    }
                    str2 = sb3.toString();
                    String str5 = TAG;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("getStringFromUrl ok, length=");
                    sb4.append(str2.length());
                    MRAIDLog.d(str5, sb4.toString());
                    inputStream = inputStream2;
                } catch (IOException e) {
                    e = e;
                    str2 = null;
                    inputStream = inputStream2;
                    String str6 = TAG;
                    try {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("getStringFromUrl failed ");
                        sb5.append(e.getLocalizedMessage());
                        MRAIDLog.e(str6, sb5.toString());
                    } catch (Throwable th) {
                        th = th;
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException unused) {
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                }
            } else {
                str2 = null;
            }
            try {
                httpURLConnection.disconnect();
            } catch (IOException e2) {
                e = e2;
                String str62 = TAG;
                StringBuilder sb52 = new StringBuilder();
                sb52.append("getStringFromUrl failed ");
                sb52.append(e.getLocalizedMessage());
                MRAIDLog.e(str62, sb52.toString());
            }
        } catch (IOException e3) {
            e = e3;
            str2 = null;
            String str622 = TAG;
            StringBuilder sb522 = new StringBuilder();
            sb522.append("getStringFromUrl failed ");
            sb522.append(e.getLocalizedMessage());
            MRAIDLog.e(str622, sb522.toString());
        }
        return str2;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:10|11|12|13|14) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0048 */
    private String getStringFromFileUrl(String str) {
        BufferedReader bufferedReader;
        StringBuilder sb = new StringBuilder("");
        String[] split = str.split("/");
        if (split[3].equals("android_asset")) {
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(this.context.getAssets().open(split[4])));
                String readLine = bufferedReader.readLine();
                sb.append(readLine);
                while (readLine != null) {
                    readLine = bufferedReader.readLine();
                    sb.append(readLine);
                }
                try {
                    bufferedReader.close();
                } catch (Exception unused) {
                }
            } catch (IOException e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Error fetching file: ");
                sb2.append(e.getMessage());
                MRAIDLog.e(sb2.toString());
            } catch (Throwable th) {
                bufferedReader.close();
                throw th;
            }
            return sb.toString();
        }
        MRAIDLog.e("Unknown location to fetch file content");
        return "";
    }

    /* access modifiers changed from: protected */
    public void showAsInterstitial(Activity activity, int i) {
        this.interstitialActivity = activity;
        this.interstitialInLayoutId = i;
        expand(null, activity, i);
    }

    public Activity getInterstitialActivity() {
        return this.interstitialActivity;
    }

    /* access modifiers changed from: private */
    public void expandHelper(WebView webView2, Activity activity, int i) {
        if (!this.isInterstitial) {
            this.state = 2;
        }
        applyOrientationProperties();
        forceFullScreen();
        this.expandedView = new RelativeLayout(this.context);
        this.expandedView.addView(webView2);
        addCloseRegion(this.expandedView);
        setCloseRegionPosition(this.expandedView);
        if (!this.preload) {
            webView2.loadDataWithBaseURL(this.baseUrl, this.mData, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        }
        if (activity != null) {
            activity.addContentView(this.expandedView, new LayoutParams(-1, -1));
        } else if (i != -1) {
            ViewGroup viewGroup = (ViewGroup) ((Activity) this.context).findViewById(i);
            if (viewGroup != null) {
                viewGroup.addView(this.expandedView, new ViewGroup.LayoutParams(-1, -1));
            } else {
                ((Activity) this.context).addContentView(this.expandedView, new LayoutParams(-1, -1));
            }
        } else {
            ((Activity) this.context).addContentView(this.expandedView, new LayoutParams(-1, -1));
        }
        this.isExpandingFromDefault = true;
        if (this.isInterstitial) {
            this.isLaidOut = true;
            this.state = 1;
            fireStateChangeEvent();
        }
    }

    @NonNull
    private View obtainRootView() {
        View topmostView = getTopmostView(peekContext(), this);
        return topmostView instanceof ViewGroup ? (ViewGroup) topmostView : this;
    }

    @Nullable
    private static View getTopmostView(@Nullable Context context2, @Nullable View view) {
        View rootViewFromActivity = getRootViewFromActivity(context2);
        return rootViewFromActivity != null ? rootViewFromActivity : getRootViewFromView(view);
    }

    @Nullable
    private static View getRootViewFromActivity(@Nullable Context context2) {
        if (!(context2 instanceof Activity)) {
            return null;
        }
        return ((Activity) context2).getWindow().getDecorView().findViewById(16908290);
    }

    @Nullable
    private static View getRootViewFromView(@Nullable View view) {
        if (view == null) {
            return null;
        }
        View rootView = view.getRootView();
        if (rootView == null) {
            return null;
        }
        View findViewById = rootView.findViewById(16908290);
        if (findViewById != null) {
            rootView = findViewById;
        }
        return rootView;
    }

    private void setResizedViewSize() {
        int i = this.resizeProperties.width;
        int i2 = this.resizeProperties.height;
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("setResizedViewSize ");
        sb.append(i);
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(i2);
        Log.d(str, sb.toString());
        this.resizedView.setLayoutParams(new FrameLayout.LayoutParams((int) TypedValue.applyDimension(1, (float) i, this.displayMetrics), (int) TypedValue.applyDimension(1, (float) i2, this.displayMetrics)));
    }

    /* access modifiers changed from: private */
    public void setResizedViewPosition() {
        MRAIDLog.d(TAG, "setResizedViewPosition");
        if (this.resizedView != null) {
            int i = this.resizeProperties.width;
            int i2 = this.resizeProperties.height;
            int i3 = this.resizeProperties.offsetX;
            int applyDimension = (int) TypedValue.applyDimension(1, (float) i, this.displayMetrics);
            int applyDimension2 = (int) TypedValue.applyDimension(1, (float) i2, this.displayMetrics);
            int applyDimension3 = (int) TypedValue.applyDimension(1, (float) i3, this.displayMetrics);
            int applyDimension4 = (int) TypedValue.applyDimension(1, (float) this.resizeProperties.offsetY, this.displayMetrics);
            Rect defaultAdRect = this.screenMetrics.getDefaultAdRect();
            int i4 = defaultAdRect.left + applyDimension3;
            int i5 = defaultAdRect.top + applyDimension4;
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.resizedView.getLayoutParams();
            layoutParams.leftMargin = i4;
            layoutParams.topMargin = i5;
            this.resizedView.setLayoutParams(layoutParams);
            if (this.screenMetrics.setCurrentAdPosition(i4, i5, applyDimension, applyDimension2)) {
                injectSetCurrentPosition();
            }
        }
    }

    /* access modifiers changed from: private */
    public void closeFromExpanded() {
        if (this.state == 1 && this.isInterstitial) {
            this.state = 4;
            this.handler.post(new Runnable() {
                public void run() {
                    MRAIDView.this.fireStateChangeEvent();
                    if (MRAIDView.this.listener != null) {
                        MRAIDView.this.listener.mraidViewClose(MRAIDView.this);
                    }
                    MRAIDView.this.clearView();
                }
            });
        } else if (this.state == 2 || this.state == 3) {
            this.state = 1;
        }
        this.isClosing = true;
        this.expandedView.removeAllViews();
        try {
            FrameLayout frameLayout = (FrameLayout) ((Activity) this.context).findViewById(16908290);
            if (frameLayout != null) {
                frameLayout.removeView(this.expandedView);
            }
        } catch (Exception e) {
            MRAIDLog.e(TAG, e.getMessage());
        }
        this.expandedView = null;
        this.closeRegion = null;
        this.handler.post(new Runnable() {
            public void run() {
                MRAIDView.this.restoreOriginalOrientation();
                MRAIDView.this.restoreOriginalScreenState();
            }
        });
        if (this.webViewPart2 == null && this.webView != null) {
            addView(this.webView);
        } else if (!(this.webViewPart2 == null || this.webView == null || this.mraidWebChromeClient == null || this.mraidWebViewClient == null)) {
            this.webViewPart2.setWebChromeClient(null);
            this.webViewPart2.setWebViewClient(null);
            WebView webView2 = this.webViewPart2;
            this.webViewPart2 = null;
            webView2.destroy();
            this.webView.setWebChromeClient(this.mraidWebChromeClient);
            this.webView.setWebViewClient(this.mraidWebViewClient);
            setCurrentWebView(this.webView);
        }
        this.handler.post(new Runnable() {
            public void run() {
                MRAIDView.this.fireStateChangeEvent();
                if (MRAIDView.this.listener != null) {
                    MRAIDView.this.listener.mraidViewClose(MRAIDView.this);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void closeFromResized() {
        this.state = 1;
        this.isClosing = true;
        removeResizeView();
        addView(this.webView);
        this.handler.post(new Runnable() {
            public void run() {
                MRAIDView.this.fireStateChangeEvent();
                if (MRAIDView.this.listener != null) {
                    MRAIDView.this.listener.mraidViewClose(MRAIDView.this);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void removeResizeView() {
        this.resizedView.removeAllViews();
        FrameLayout frameLayout = (FrameLayout) ((Activity) this.context).findViewById(16908290);
        if (frameLayout != null) {
            frameLayout.removeView(this.resizedView);
        }
        this.resizedView = null;
        this.closeRegion = null;
    }

    private void forceFullScreen() {
        MRAIDLog.d(TAG, "forceFullScreen");
        Activity activity = (Activity) this.context;
        int i = activity.getWindow().getAttributes().flags;
        boolean z = false;
        this.isFullScreen = (i & 1024) != 0;
        this.isForceNotFullScreen = (i & 2048) != 0;
        this.origTitleBarVisibility = -9;
        ActionBar actionBar = activity.getActionBar();
        if (actionBar != null) {
            this.isActionBarShowing = actionBar.isShowing();
            actionBar.hide();
            z = true;
        }
        if (!z) {
            this.titleBar = null;
            try {
                this.titleBar = (View) activity.findViewById(16908310).getParent();
            } catch (NullPointerException unused) {
            }
            if (this.titleBar != null) {
                this.origTitleBarVisibility = this.titleBar.getVisibility();
                this.titleBar.setVisibility(8);
            }
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("isFullScreen ");
        sb.append(this.isFullScreen);
        MRAIDLog.d(str, sb.toString());
        String str2 = TAG;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("isForceNotFullScreen ");
        sb2.append(this.isForceNotFullScreen);
        MRAIDLog.d(str2, sb2.toString());
        String str3 = TAG;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("isActionBarShowing ");
        sb3.append(this.isActionBarShowing);
        MRAIDLog.d(str3, sb3.toString());
        String str4 = TAG;
        StringBuilder sb4 = new StringBuilder();
        sb4.append("origTitleBarVisibility ");
        sb4.append(getVisibilityString(this.origTitleBarVisibility));
        MRAIDLog.d(str4, sb4.toString());
    }

    /* access modifiers changed from: private */
    public void restoreOriginalScreenState() {
        if (this.context != null && (this.context instanceof Activity)) {
            Activity activity = (Activity) this.context;
            if (!this.isFullScreen) {
                activity.getWindow().clearFlags(1024);
            }
            if (this.isForceNotFullScreen) {
                activity.getWindow().addFlags(2048);
            }
            if (this.isActionBarShowing) {
                ActionBar actionBar = activity.getActionBar();
                if (actionBar != null) {
                    actionBar.show();
                }
            } else if (this.titleBar != null) {
                this.titleBar.setVisibility(this.origTitleBarVisibility);
            }
        }
    }

    private void addCloseRegion(View view) {
        this.closeRegion = new CircleCountdownView(this.context);
        this.closeRegion.setBackgroundColor(0);
        this.closeRegion.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MRAIDView.this.close();
            }
        });
        if (view == this.expandedView && !this.useCustomClose) {
            showDefaultCloseButton();
        }
        ((ViewGroup) view).addView(this.closeRegion);
    }

    private void showDefaultCloseButton() {
        if (this.closeRegion == null) {
            return;
        }
        if (this.closeTime != 0) {
            this.closeRegion.setVisibility(0);
            this.isCloseClickable = false;
            final int i = (this.closeTime == -1 ? 3 : this.closeTime) * 1000;
            final Handler handler2 = new Handler(Looper.getMainLooper());
            handler2.postDelayed(new Runnable() {
                public void run() {
                    if (MRAIDView.this.closeRegion != null) {
                        MRAIDView.this.closeTimerPosition = MRAIDView.this.closeTimerPosition + 40;
                        CircleCountdownView access$3000 = MRAIDView.this.closeRegion;
                        int access$3100 = (MRAIDView.this.closeTimerPosition * 100) / i;
                        double access$31002 = (double) (i - MRAIDView.this.closeTimerPosition);
                        Double.isNaN(access$31002);
                        access$3000.changePercentage(access$3100, (int) Math.ceil(access$31002 / 1000.0d));
                        if (MRAIDView.this.closeTimerPosition >= i) {
                            MRAIDView.this.closeRegion.setClickable(true);
                            MRAIDView.this.closeRegion.setImage(Assets.getBitmapFromBase64(Assets.close));
                            MRAIDView.this.isCloseClickable = true;
                            return;
                        }
                        handler2.postDelayed(this, 40);
                    }
                }
            }, 40);
            return;
        }
        this.closeRegion.setClickable(true);
        this.closeRegion.changePercentage(100, 0);
        this.closeRegion.setImage(Assets.getBitmapFromBase64(Assets.close));
    }

    private void removeDefaultCloseButton() {
        if (this.closeRegion != null) {
            this.closeRegion.setVisibility(4);
            this.closeTimerPosition = DefaultOggSeeker.MATCH_BYTE_RANGE;
            this.closeRegion.setClickable(false);
        }
    }

    private void setCloseRegionPosition(View view) {
        int applyDimension = (int) TypedValue.applyDimension(1, 50.0f, this.displayMetrics);
        LayoutParams layoutParams = new LayoutParams(applyDimension, applyDimension);
        if (view != this.expandedView) {
            if (view == this.resizedView) {
                switch (this.resizeProperties.customClosePosition) {
                    case 0:
                    case 4:
                        layoutParams.addRule(9);
                        break;
                    case 1:
                    case 3:
                    case 5:
                        layoutParams.addRule(14);
                        break;
                    case 2:
                    case 6:
                        layoutParams.addRule(11);
                        break;
                }
                switch (this.resizeProperties.customClosePosition) {
                    case 0:
                    case 1:
                    case 2:
                        layoutParams.addRule(10);
                        break;
                    case 3:
                        layoutParams.addRule(15);
                        break;
                    case 4:
                    case 5:
                    case 6:
                        layoutParams.addRule(12);
                        break;
                }
            }
        } else {
            layoutParams.addRule(10);
            layoutParams.addRule(11);
        }
        this.closeRegion.setLayoutParams(layoutParams);
    }

    private String getMraidJs() {
        if (TextUtils.isEmpty(this.mraidJs)) {
            return new String(Base64.decode(MraidAssets.mraidJS, 0));
        }
        return this.mraidJs;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public void injectJavaScript(String str) {
        injectJavaScript(this.currentWebView, str);
    }

    @SuppressLint({"NewApi"})
    private void injectJavaScript(WebView webView2, String str) {
        if (!TextUtils.isEmpty(str) && webView2 != null) {
            if (VERSION.SDK_INT >= 19) {
                String str2 = TAG;
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append("evaluating js: ");
                    sb.append(str);
                    MRAIDLog.d(str2, sb.toString());
                    webView2.evaluateJavascript(str, new ValueCallback<String>() {
                        public void onReceiveValue(String str) {
                        }
                    });
                } catch (Exception e) {
                    MRAIDLog.e(e.getMessage());
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("loading url: ");
                    sb2.append(str);
                    MRAIDLog.d(str3, sb2.toString());
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("javascript:");
                    sb3.append(str);
                    webView2.loadUrl(sb3.toString());
                }
            } else {
                String str4 = TAG;
                StringBuilder sb4 = new StringBuilder();
                sb4.append("loading url: ");
                sb4.append(str);
                MRAIDLog.d(str4, sb4.toString());
                StringBuilder sb5 = new StringBuilder();
                sb5.append("javascript:");
                sb5.append(str);
                webView2.loadUrl(sb5.toString());
            }
        }
    }

    /* access modifiers changed from: private */
    public void fireReadyEvent() {
        MRAIDLog.d(TAG, "fireReadyEvent");
        injectJavaScript("mraid.fireReadyEvent();");
    }

    /* access modifiers changed from: private */
    @SuppressLint({"DefaultLocale"})
    public void fireStateChangeEvent() {
        MRAIDLog.d(TAG, "fireStateChangeEvent");
        String[] strArr = {"loading", "default", "expanded", "resized", "hidden"};
        StringBuilder sb = new StringBuilder();
        sb.append("mraid.fireStateChangeEvent('");
        sb.append(strArr[this.state]);
        sb.append("');");
        injectJavaScript(sb.toString());
    }

    /* access modifiers changed from: private */
    public void fireViewableChangeEvent() {
        MRAIDLog.d(TAG, "fireViewableChangeEvent");
        StringBuilder sb = new StringBuilder();
        sb.append("mraid.fireViewableChangeEvent(");
        sb.append(this.isViewable);
        sb.append(");");
        injectJavaScript(sb.toString());
    }

    /* access modifiers changed from: private */
    public void injectSetCurrentPosition() {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("injectSetCurrentPosition (");
        sb.append(Utils.stringifyRect(this.screenMetrics.getCurrentAdRectDips()));
        sb.append(")");
        MRAIDLog.d(str, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("mraid.setCurrentPosition(");
        sb2.append(Utils.stringifyRect(this.screenMetrics.getCurrentAdRectDips()));
        sb2.append(");");
        injectJavaScript(sb2.toString());
    }

    /* access modifiers changed from: private */
    public void injectSetDefaultPosition() {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("injectSetDefaultPosition (");
        sb.append(Utils.stringifyRect(this.screenMetrics.getDefaultAdRectDips()));
        sb.append(")");
        MRAIDLog.d(str, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("mraid.setDefaultPosition(");
        sb2.append(Utils.stringifyRect(this.screenMetrics.getDefaultAdRectDips()));
        sb2.append(");");
        injectJavaScript(sb2.toString());
    }

    /* access modifiers changed from: private */
    public void injectSetMaxSize() {
        Rect rootViewRectDips = this.screenMetrics.getRootViewRectDips();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("injectSetMaxSize: ");
        sb.append(rootViewRectDips.width());
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(rootViewRectDips.height());
        MRAIDLog.d(str, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("mraid.setMaxSize(");
        sb2.append(rootViewRectDips.width());
        sb2.append(",");
        sb2.append(rootViewRectDips.height());
        sb2.append(");");
        injectJavaScript(sb2.toString());
    }

    /* access modifiers changed from: private */
    public void injectSetScreenSize() {
        Rect screenRectDips = this.screenMetrics.getScreenRectDips();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("injectSetScreenSize ");
        sb.append(screenRectDips.width());
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(screenRectDips.height());
        MRAIDLog.d(str, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("mraid.setScreenSize(");
        sb2.append(screenRectDips.width());
        sb2.append(",");
        sb2.append(screenRectDips.height());
        sb2.append(");");
        injectJavaScript(sb2.toString());
    }

    /* access modifiers changed from: private */
    public void setSupportedServices() {
        MRAIDLog.d(TAG, "setSupportedServices");
        StringBuilder sb = new StringBuilder();
        sb.append("mraid.setSupports(mraid.SUPPORTED_FEATURES.CALENDAR, ");
        sb.append(this.nativeFeatureManager.isCalendarSupported());
        sb.append(");");
        injectJavaScript(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("mraid.setSupports(mraid.SUPPORTED_FEATURES.INLINEVIDEO, ");
        sb2.append(this.nativeFeatureManager.isInlineVideoSupported());
        sb2.append(");");
        injectJavaScript(sb2.toString());
        StringBuilder sb3 = new StringBuilder();
        sb3.append("mraid.setSupports(mraid.SUPPORTED_FEATURES.SMS, ");
        sb3.append(this.nativeFeatureManager.isSmsSupported());
        sb3.append(");");
        injectJavaScript(sb3.toString());
        StringBuilder sb4 = new StringBuilder();
        sb4.append("mraid.setSupports(mraid.SUPPORTED_FEATURES.STOREPICTURE, ");
        sb4.append(this.nativeFeatureManager.isStorePictureSupported());
        sb4.append(");");
        injectJavaScript(sb4.toString());
        StringBuilder sb5 = new StringBuilder();
        sb5.append("mraid.setSupports(mraid.SUPPORTED_FEATURES.TEL, ");
        sb5.append(this.nativeFeatureManager.isTelSupported());
        sb5.append(");");
        injectJavaScript(sb5.toString());
    }

    /* access modifiers changed from: private */
    public void pauseWebView(WebView webView2) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("pauseWebView ");
        sb.append(webView2.toString());
        MRAIDLog.d(str, sb.toString());
        try {
            webView2.onPause();
        } catch (Exception e) {
            MRAIDLog.e(e.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void resumeWebView(WebView webView2) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("resumeWebView ");
        sb.append(webView2.toString());
        MRAIDLog.d(str, sb.toString());
        try {
            webView2.onResume();
        } catch (Exception e) {
            MRAIDLog.e(e.getMessage());
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onConfigurationChanged ");
        sb.append(configuration.orientation == 1 ? "portrait" : "landscape");
        MRAIDLog.d(str, sb.toString());
        if (this.interstitialActivity != null) {
            this.interstitialActivity.getWindowManager().getDefaultDisplay().getMetrics(this.displayMetrics);
        } else if (this.context instanceof Activity) {
            ((Activity) this.context).getWindowManager().getDefaultDisplay().getMetrics(this.displayMetrics);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        MRAIDLog.d(TAG, "onAttachedToWindow");
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        MRAIDLog.d(TAG, "onDetachedFromWindow");
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(view, i);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onVisibilityChanged ");
        sb.append(getVisibilityString(i));
        MRAIDLog.d(str, sb.toString());
        setViewable(i);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onWindowFocusChanged: ");
        sb.append(z);
        MRAIDLog.d(str, sb.toString());
        setViewable(z ? getVisibility() : 8);
    }

    /* access modifiers changed from: private */
    public void setViewable(int i) {
        boolean z = i == 0;
        if (z != this.isViewable) {
            this.isViewable = z;
            if (this.isPageFinished && this.isLaidOut) {
                fireViewableChangeEvent();
            }
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"DrawAllocation"})
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onLayout (");
        sb.append(this.state);
        sb.append(") ");
        sb.append(z);
        sb.append(" ");
        sb.append(i);
        sb.append(" ");
        sb.append(i2);
        sb.append(" ");
        sb.append(i3);
        sb.append(" ");
        sb.append(i4);
        MRAIDLog.w(str, sb.toString());
        int i5 = i3 - i;
        int i6 = i4 - i2;
        if (i5 == 0 || i6 == 0 || (this.previousLayoutWidth == i5 && this.previousLayoutHeight == i6)) {
            MRAIDLog.d(TAG, "onLayout ignored: size not changed");
            return;
        }
        this.previousLayoutWidth = i5;
        this.previousLayoutHeight = i6;
        if (this.state == 2 || this.state == 3) {
            calculateScreenSize();
            calculateMaxSize();
        }
        if (this.isClosing) {
            this.isClosing = false;
            this.screenMetrics.setCurrentAdPosition(this.screenMetrics.getDefaultAdRect());
            injectSetCurrentPosition();
        } else {
            calculateDefaultPosition();
        }
        if (this.state == 3 && z) {
            this.handler.post(new Runnable() {
                public void run() {
                    MRAIDView.this.setResizedViewPosition();
                }
            });
        }
        this.isLaidOut = true;
        if (this.state == 0 && this.isPageFinished && !this.isInterstitial) {
            this.state = 1;
            fireStateChangeEvent();
            fireReadyEvent();
            if (this.isViewable) {
                fireViewableChangeEvent();
            }
        }
    }

    /* access modifiers changed from: private */
    public void onLayoutWebView(WebView webView2, boolean z, int i, int i2, int i3, int i4) {
        boolean z2 = webView2 == this.currentWebView;
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onLayoutWebView ");
        sb.append(webView2 == this.webView ? "1 " : "2 ");
        sb.append(z2);
        sb.append(" (");
        sb.append(this.state);
        sb.append(") ");
        sb.append(z);
        sb.append(" ");
        sb.append(i);
        sb.append(" ");
        sb.append(i2);
        sb.append(" ");
        sb.append(i3);
        sb.append(" ");
        sb.append(i4);
        MRAIDLog.w(str, sb.toString());
        if (!z2) {
            MRAIDLog.d(TAG, "onLayoutWebView ignored, not current");
            return;
        }
        int i5 = i3 - i;
        int i6 = i4 - i2;
        if (i5 == 0 || i6 == 0 || (this.previousWebViewLayoutWidth == i5 && this.previousWebViewLayoutHeight == i6)) {
            MRAIDLog.d(TAG, "onLayoutWebView ignored: size not changed");
            return;
        }
        this.previousWebViewLayoutWidth = i5;
        this.previousWebViewLayoutHeight = i6;
        if (this.state == 0 || this.state == 1) {
            calculateScreenSize();
            calculateMaxSize();
        }
        if (!this.isClosing) {
            calculateCurrentPosition();
            if (this.isInterstitial && !this.screenMetrics.getDefaultAdRect().equals(this.screenMetrics.getCurrentAdRect())) {
                this.screenMetrics.setDefaultAdPosition(this.screenMetrics.getCurrentAdRect());
                injectSetDefaultPosition();
                injectSetCurrentPosition();
            }
        }
        if (this.isExpandingFromDefault) {
            this.isExpandingFromDefault = false;
            if (this.isInterstitial) {
                this.state = 1;
                this.isLaidOut = true;
            }
            if (!this.isExpandingPart2) {
                MRAIDLog.d(TAG, "calling fireStateChangeEvent 1");
                fireStateChangeEvent();
            }
            if (this.isInterstitial) {
                fireReadyEvent();
                if (this.isViewable) {
                    fireViewableChangeEvent();
                }
            }
            if (this.listener != null) {
                this.listener.mraidViewExpand(this);
            }
        }
    }

    @NonNull
    private Context peekContext() {
        return this.interstitialActivity != null ? this.interstitialActivity : this.context;
    }

    private void calculateScreenSize() {
        if (this.screenMetrics.setScreenSize(this.displayMetrics.widthPixels, this.displayMetrics.heightPixels)) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("calculateScreenSize screen size: ");
            sb.append(this.screenMetrics.getScreenRect().width());
            sb.append(AvidJSONUtil.KEY_X);
            sb.append(this.screenMetrics.getScreenRect().height());
            MRAIDLog.d(str, sb.toString());
            if (this.isPageFinished) {
                injectSetScreenSize();
                return;
            }
            return;
        }
        MRAIDLog.d(TAG, "calculateScreenSize skipped: size not changed");
    }

    private void calculateMaxSize() {
        View obtainRootView = obtainRootView();
        int[] iArr = new int[2];
        obtainRootView.getLocationOnScreen(iArr);
        if (this.screenMetrics.setRootViewPosition(iArr[0], iArr[1], obtainRootView.getWidth(), obtainRootView.getHeight())) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("calculateMaxSize: ");
            sb.append(this.screenMetrics.getRootViewRect());
            MRAIDLog.d(str, sb.toString());
            if (this.isPageFinished) {
                injectSetMaxSize();
                return;
            }
            return;
        }
        MRAIDLog.d(TAG, "calculateMaxSize skipped: size not changed");
    }

    private void calculateDefaultPosition() {
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        if (this.screenMetrics.setDefaultAdPosition(iArr[0], iArr[1], getWidth(), getHeight())) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("calculateDefaultPosition: ");
            sb.append(this.screenMetrics.getDefaultAdRect());
            MRAIDLog.d(str, sb.toString());
            if (this.isPageFinished) {
                injectSetDefaultPosition();
                return;
            }
            return;
        }
        MRAIDLog.d(TAG, "calculateDefaultPosition skipped: size not changed");
    }

    private void calculateCurrentPosition() {
        int[] iArr = new int[2];
        WebView webView2 = this.currentWebView;
        webView2.getLocationOnScreen(iArr);
        if (this.screenMetrics.setCurrentAdPosition(iArr[0], iArr[1], webView2.getWidth(), webView2.getHeight())) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("calculateCurrentPosition: ");
            sb.append(this.screenMetrics.getCurrentAdRect());
            MRAIDLog.d(str, sb.toString());
            if (this.isPageFinished) {
                injectSetCurrentPosition();
            }
        }
    }

    private void applyOrientationProperties() {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("applyOrientationProperties ");
        sb.append(this.orientationProperties.allowOrientationChange);
        sb.append(" ");
        sb.append(this.orientationProperties.forceOrientationString());
        MRAIDLog.d(str, sb.toString());
        if (this.interstitialActivity != null) {
            int i = 0;
            int i2 = getResources().getConfiguration().orientation == 1 ? 1 : 0;
            String str2 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("currentOrientation ");
            sb2.append(i2 != 0 ? "portrait" : "landscape");
            MRAIDLog.d(str2, sb2.toString());
            if (this.orientationProperties.forceOrientation == 0) {
                i = 1;
            } else if (this.orientationProperties.forceOrientation != 1) {
                i = this.orientationProperties.allowOrientationChange ? -1 : i2;
            }
            this.interstitialActivity.setRequestedOrientation(i);
        }
    }

    /* access modifiers changed from: private */
    public void restoreOriginalOrientation() {
        MRAIDLog.d(TAG, "restoreOriginalOrientation");
        if (this.interstitialActivity != null && this.interstitialActivity.getRequestedOrientation() != this.originalRequestedOrientation) {
            this.interstitialActivity.setRequestedOrientation(this.originalRequestedOrientation);
        }
    }

    public void show() {
        if (!this.preload && !this.isShown && this.webView != null) {
            this.isShown = true;
            this.webView.loadDataWithBaseURL(this.baseUrl, this.mData, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        }
    }

    public void setListener(MRAIDViewListener mRAIDViewListener) {
        this.listener = mRAIDViewListener;
    }

    public void setNativeFeatureListener(MRAIDNativeFeatureListener mRAIDNativeFeatureListener) {
        this.nativeFeatureListener = mRAIDNativeFeatureListener;
    }
}
