package com.explorestack.iab.utils;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.smaato.sdk.video.vast.model.ErrorCode;

public class CircularProgressDrawable extends Drawable implements Animatable {
    private static final int[] COLORS = {-16777216};
    private static final Interpolator LINEAR_INTERPOLATOR = new LinearInterpolator();
    private static final Interpolator MATERIAL_INTERPOLATOR = new Interpolator() {
        private final float mStepSize = (1.0f / ((float) (this.mValues.length - 1)));
        private final float[] mValues;

        {
            float[] fArr = new float[ErrorCode.DIFFERENT_LINEARITY_EXPECTED_ERROR];
            // fill-array-data instruction
            fArr[0] = 0;
            fArr[1] = 953267991;
            fArr[2] = 961656599;
            fArr[3] = 973279855;
            fArr[4] = 980151802;
            fArr[5] = 985104436;
            fArr[6] = 990057071;
            fArr[7] = 993063548;
            fArr[8] = 996929018;
            fArr[9] = 999734169;
            fArr[10] = 1002311149;
            fArr[11] = 1005102878;
            fArr[12] = 1007371158;
            fArr[13] = 1009089144;
            fArr[14] = 1010914506;
            fArr[15] = 1012954615;
            fArr[16] = 1015115520;
            fArr[17] = 1016296636;
            fArr[18] = 1017638814;
            fArr[19] = 1019034678;
            fArr[20] = 1020537917;
            fArr[21] = 1022148529;
            fArr[22] = 1023638346;
            fArr[23] = 1024551027;
            fArr[24] = 1025517394;
            fArr[25] = 1026564293;
            fArr[26] = 1027664878;
            fArr[27] = 1028819150;
            fArr[28] = 1030053954;
            fArr[29] = 1031342444;
            fArr[30] = 1032268546;
            fArr[31] = 1032993322;
            fArr[32] = 1033758363;
            fArr[33] = 1034550247;
            fArr[34] = 1035395819;
            fArr[35] = 1036281656;
            fArr[36] = 1037221180;
            fArr[37] = 1038187548;
            fArr[38] = 1039207603;
            fArr[39] = 1040234368;
            fArr[40] = 1040784661;
            fArr[41] = 1041368508;
            fArr[42] = 1041972488;
            fArr[43] = 1042603311;
            fArr[44] = 1043254267;
            fArr[45] = 1043932067;
            fArr[46] = 1044636710;
            fArr[47] = 1045361485;
            fArr[48] = 1046113105;
            fArr[49] = 1046884857;
            fArr[50] = 1047676741;
            fArr[51] = 1048488758;
            fArr[52] = 1048948454;
            fArr[53] = 1049374595;
            fArr[54] = 1049807448;
            fArr[55] = 1050247011;
            fArr[56] = 1050693285;
            fArr[57] = 1051142914;
            fArr[58] = 1051595899;
            fArr[59] = 1052052239;
            fArr[60] = 1052511935;
            fArr[61] = 1052971631;
            fArr[62] = 1053427971;
            fArr[63] = 1053884311;
            fArr[64] = 1054337296;
            fArr[65] = 1054790281;
            fArr[66] = 1055236555;
            fArr[67] = 1055676118;
            fArr[68] = 1056112325;
            fArr[69] = 1056541822;
            fArr[70] = 1056964608;
            fArr[71] = 1057172645;
            fArr[72] = 1057377328;
            fArr[73] = 1057576976;
            fArr[74] = 1057773270;
            fArr[75] = 1057966208;
            fArr[76] = 1058155790;
            fArr[77] = 1058340340;
            fArr[78] = 1058521534;
            fArr[79] = 1058697694;
            fArr[80] = 1058870500;
            fArr[81] = 1059039950;
            fArr[82] = 1059204366;
            fArr[83] = 1059365428;
            fArr[84] = 1059523133;
            fArr[85] = 1059675806;
            fArr[86] = 1059826801;
            fArr[87] = 1059972763;
            fArr[88] = 1060115369;
            fArr[89] = 1060254620;
            fArr[90] = 1060392193;
            fArr[91] = 1060524733;
            fArr[92] = 1060653918;
            fArr[93] = 1060781425;
            fArr[94] = 1060905576;
            fArr[95] = 1061026372;
            fArr[96] = 1061143813;
            fArr[97] = 1061257898;
            fArr[98] = 1061370305;
            fArr[99] = 1061481035;
            fArr[100] = 1061588409;
            fArr[101] = 1061692427;
            fArr[102] = 1061794768;
            fArr[103] = 1061893754;
            fArr[104] = 1061991062;
            fArr[105] = 1062086692;
            fArr[106] = 1062178967;
            fArr[107] = 1062269564;
            fArr[108] = 1062358483;
            fArr[109] = 1062444047;
            fArr[110] = 1062529611;
            fArr[111] = 1062611819;
            fArr[112] = 1062692350;
            fArr[113] = 1062771202;
            fArr[114] = 1062848378;
            fArr[115] = 1062922197;
            fArr[116] = 1062996017;
            fArr[117] = 1063068159;
            fArr[118] = 1063136946;
            fArr[119] = 1063205732;
            fArr[120] = 1063272841;
            fArr[121] = 1063336595;
            fArr[122] = 1063400348;
            fArr[123] = 1063462424;
            fArr[124] = 1063522822;
            fArr[125] = 1063583220;
            fArr[126] = 1063640262;
            fArr[127] = 1063697305;
            fArr[128] = 1063752670;
            fArr[129] = 1063806357;
            fArr[130] = 1063858366;
            fArr[131] = 1063908698;
            fArr[132] = 1063959029;
            fArr[133] = 1064007683;
            fArr[134] = 1064056337;
            fArr[135] = 1064101636;
            fArr[136] = 1064146934;
            fArr[137] = 1064190555;
            fArr[138] = 1064234176;
            fArr[139] = 1064276119;
            fArr[140] = 1064316384;
            fArr[141] = 1064356649;
            fArr[142] = 1064395237;
            fArr[143] = 1064433825;
            fArr[144] = 1064470734;
            fArr[145] = 1064505967;
            fArr[146] = 1064541199;
            fArr[147] = 1064574753;
            fArr[148] = 1064608308;
            fArr[149] = 1064640184;
            fArr[150] = 1064672061;
            fArr[151] = 1064702260;
            fArr[152] = 1064730781;
            fArr[153] = 1064759303;
            fArr[154] = 1064787824;
            fArr[155] = 1064814667;
            fArr[156] = 1064841511;
            fArr[157] = 1064866677;
            fArr[158] = 1064891843;
            fArr[159] = 1064915331;
            fArr[160] = 1064938819;
            fArr[161] = 1064960629;
            fArr[162] = 1064982440;
            fArr[163] = 1065002572;
            fArr[164] = 1065022705;
            fArr[165] = 1065042838;
            fArr[166] = 1065061292;
            fArr[167] = 1065079747;
            fArr[168] = 1065098202;
            fArr[169] = 1065114980;
            fArr[170] = 1065130079;
            fArr[171] = 1065146856;
            fArr[172] = 1065160278;
            fArr[173] = 1065175378;
            fArr[174] = 1065188799;
            fArr[175] = 1065202221;
            fArr[176] = 1065213965;
            fArr[177] = 1065225709;
            fArr[178] = 1065237453;
            fArr[179] = 1065247520;
            fArr[180] = 1065259264;
            fArr[181] = 1065267652;
            fArr[182] = 1065277719;
            fArr[183] = 1065286107;
            fArr[184] = 1065292818;
            fArr[185] = 1065301207;
            fArr[186] = 1065307918;
            fArr[187] = 1065314628;
            fArr[188] = 1065319662;
            fArr[189] = 1065326372;
            fArr[190] = 1065329728;
            fArr[191] = 1065334761;
            fArr[192] = 1065338117;
            fArr[193] = 1065341472;
            fArr[194] = 1065344827;
            fArr[195] = 1065348183;
            fArr[196] = 1065349861;
            fArr[197] = 1065351538;
            fArr[198] = 1065351538;
            fArr[199] = 1065353216;
            fArr[200] = 1065353216;
            this.mValues = fArr;
        }

        public float getInterpolation(float f) {
            if (f >= 1.0f) {
                return 1.0f;
            }
            if (f <= 0.0f) {
                return 0.0f;
            }
            int min = Math.min((int) (((float) (this.mValues.length - 1)) * f), this.mValues.length - 2);
            return this.mValues[min] + (((f - (((float) min) * this.mStepSize)) / this.mStepSize) * (this.mValues[min + 1] - this.mValues[min]));
        }
    };
    private Animator mAnimator;
    /* access modifiers changed from: private */
    public boolean mFinishing;
    private Resources mResources;
    @VisibleForTesting
    final Ring mRing = new Ring();
    private float mRotation;
    /* access modifiers changed from: private */
    public float mRotationCount;

    @VisibleForTesting
    static class Ring {
        int mAlpha = 255;
        final Paint mArrowPaint = new Paint();
        final Paint mBackgroundPaint = new Paint();
        final Paint mCirclePaint = new Paint();
        int mColorIndex;
        int[] mColors;
        int mCurrentColor;
        float mEndTrim = 0.0f;
        final Paint mPaint = new Paint();
        float mRingCenterRadius;
        float mRotation = 0.0f;
        float mStartTrim = 0.0f;
        float mStartingEndTrim;
        float mStartingRotation;
        float mStartingStartTrim;
        float mStrokeWidth = 5.0f;
        final RectF mTempBounds = new RectF();

        Ring() {
            this.mPaint.setStrokeCap(Cap.SQUARE);
            this.mPaint.setAntiAlias(true);
            this.mPaint.setStyle(Style.STROKE);
            this.mArrowPaint.setStyle(Style.FILL);
            this.mArrowPaint.setAntiAlias(true);
            this.mCirclePaint.setColor(0);
            this.mBackgroundPaint.setColor(0);
        }

        /* access modifiers changed from: 0000 */
        public void setStrokeCap(Cap cap) {
            this.mPaint.setStrokeCap(cap);
        }

        /* access modifiers changed from: 0000 */
        public void draw(Canvas canvas, Rect rect) {
            RectF rectF = this.mTempBounds;
            float f = this.mRingCenterRadius + (this.mStrokeWidth / 2.0f);
            if (this.mRingCenterRadius <= 0.0f) {
                f = (((float) Math.min(rect.width(), rect.height())) / 2.0f) - (this.mStrokeWidth / 2.0f);
            }
            rectF.set(((float) rect.centerX()) - f, ((float) rect.centerY()) - f, ((float) rect.centerX()) + f, ((float) rect.centerY()) + f);
            float f2 = (this.mStartTrim + this.mRotation) * 360.0f;
            float f3 = ((this.mEndTrim + this.mRotation) * 360.0f) - f2;
            this.mPaint.setColor(this.mCurrentColor);
            this.mPaint.setAlpha(this.mAlpha);
            float f4 = this.mStrokeWidth / 2.0f;
            rectF.inset(f4, f4);
            canvas.drawCircle(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, this.mCirclePaint);
            float f5 = -f4;
            rectF.inset(f5, f5);
            canvas.drawArc(rectF, f2, f3, false, this.mPaint);
        }

        /* access modifiers changed from: 0000 */
        public void setColors(@NonNull int[] iArr) {
            this.mColors = iArr;
            setColorIndex(0);
        }

        /* access modifiers changed from: 0000 */
        public void setColor(int i) {
            this.mCurrentColor = i;
        }

        /* access modifiers changed from: 0000 */
        public void setBackgroundColor(int i) {
            this.mCirclePaint.setColor(i);
        }

        /* access modifiers changed from: 0000 */
        public int getBackgroundColor() {
            return this.mCirclePaint.getColor();
        }

        /* access modifiers changed from: 0000 */
        public void setColorIndex(int i) {
            this.mColorIndex = i;
            this.mCurrentColor = this.mColors[this.mColorIndex];
        }

        /* access modifiers changed from: 0000 */
        public int getNextColor() {
            return this.mColors[getNextColorIndex()];
        }

        /* access modifiers changed from: 0000 */
        public int getNextColorIndex() {
            return (this.mColorIndex + 1) % this.mColors.length;
        }

        /* access modifiers changed from: 0000 */
        public void goToNextColor() {
            setColorIndex(getNextColorIndex());
        }

        /* access modifiers changed from: 0000 */
        public void setColorFilter(ColorFilter colorFilter) {
            this.mPaint.setColorFilter(colorFilter);
        }

        /* access modifiers changed from: 0000 */
        public void setAlpha(int i) {
            this.mAlpha = i;
        }

        /* access modifiers changed from: 0000 */
        public int getAlpha() {
            return this.mAlpha;
        }

        /* access modifiers changed from: 0000 */
        public void setStrokeWidth(float f) {
            this.mStrokeWidth = f;
            this.mPaint.setStrokeWidth(f);
        }

        /* access modifiers changed from: 0000 */
        public void setStartTrim(float f) {
            this.mStartTrim = f;
        }

        /* access modifiers changed from: 0000 */
        public float getStartTrim() {
            return this.mStartTrim;
        }

        /* access modifiers changed from: 0000 */
        public float getStartingStartTrim() {
            return this.mStartingStartTrim;
        }

        /* access modifiers changed from: 0000 */
        public float getStartingEndTrim() {
            return this.mStartingEndTrim;
        }

        /* access modifiers changed from: 0000 */
        public int getStartingColor() {
            return this.mColors[this.mColorIndex];
        }

        /* access modifiers changed from: 0000 */
        public void setEndTrim(float f) {
            this.mEndTrim = f;
        }

        /* access modifiers changed from: 0000 */
        public float getEndTrim() {
            return this.mEndTrim;
        }

        /* access modifiers changed from: 0000 */
        public void setRotation(float f) {
            this.mRotation = f;
        }

        /* access modifiers changed from: 0000 */
        public void setCenterRadius(float f) {
            this.mRingCenterRadius = f;
        }

        /* access modifiers changed from: 0000 */
        public float getStartingRotation() {
            return this.mStartingRotation;
        }

        /* access modifiers changed from: 0000 */
        public void storeOriginals() {
            this.mStartingStartTrim = this.mStartTrim;
            this.mStartingEndTrim = this.mEndTrim;
            this.mStartingRotation = this.mRotation;
        }

        /* access modifiers changed from: 0000 */
        public void resetOriginals() {
            this.mStartingStartTrim = 0.0f;
            this.mStartingEndTrim = 0.0f;
            this.mStartingRotation = 0.0f;
            setStartTrim(0.0f);
            setEndTrim(0.0f);
            setRotation(0.0f);
        }
    }

    private int evaluateColorChange(float f, int i, int i2) {
        int i3 = (i >> 24) & 255;
        int i4 = (i >> 16) & 255;
        int i5 = (i >> 8) & 255;
        int i6 = i & 255;
        return ((i3 + ((int) (((float) (((i2 >> 24) & 255) - i3)) * f))) << 24) | ((i4 + ((int) (((float) (((i2 >> 16) & 255) - i4)) * f))) << 16) | ((i5 + ((int) (((float) (((i2 >> 8) & 255) - i5)) * f))) << 8) | (i6 + ((int) (f * ((float) ((i2 & 255) - i6)))));
    }

    public int getOpacity() {
        return -3;
    }

    public CircularProgressDrawable(@NonNull Context context) {
        this.mResources = context.getResources();
        this.mRing.setColors(COLORS);
        setStrokeWidth(2.5f);
        setupAnimators();
    }

    private void setSizeParameters(float f, float f2, float f3, float f4) {
        Ring ring = this.mRing;
        float f5 = this.mResources.getDisplayMetrics().density;
        ring.setStrokeWidth(f2 * f5);
        ring.setCenterRadius(f * f5);
        ring.setColorIndex(0);
    }

    public void setStyle(int i) {
        if (i == 0) {
            setSizeParameters(11.0f, 3.0f, 12.0f, 6.0f);
        } else {
            setSizeParameters(7.5f, 2.5f, 10.0f, 5.0f);
        }
        invalidateSelf();
    }

    public void setStrokeWidth(float f) {
        this.mRing.setStrokeWidth(f);
        invalidateSelf();
    }

    public void setCenterRadius(float f) {
        this.mRing.setCenterRadius(f);
        invalidateSelf();
    }

    public void setStrokeCap(Cap cap) {
        this.mRing.setStrokeCap(cap);
        invalidateSelf();
    }

    public int getBackgroundColor() {
        return this.mRing.getBackgroundColor();
    }

    public void setBackgroundColor(int i) {
        this.mRing.setBackgroundColor(i);
        invalidateSelf();
    }

    public void setColorSchemeColors(int... iArr) {
        this.mRing.setColors(iArr);
        this.mRing.setColorIndex(0);
        invalidateSelf();
    }

    public void draw(@NonNull Canvas canvas) {
        Rect bounds = getBounds();
        canvas.save();
        canvas.rotate(this.mRotation, bounds.exactCenterX(), bounds.exactCenterY());
        this.mRing.draw(canvas, bounds);
        canvas.restore();
    }

    public void setAlpha(int i) {
        this.mRing.setAlpha(i);
        invalidateSelf();
    }

    public int getAlpha() {
        return this.mRing.getAlpha();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.mRing.setColorFilter(colorFilter);
        invalidateSelf();
    }

    private void setRotation(float f) {
        this.mRotation = f;
    }

    public boolean isRunning() {
        return this.mAnimator.isRunning();
    }

    public void start() {
        this.mAnimator.cancel();
        this.mRing.storeOriginals();
        if (this.mRing.getEndTrim() != this.mRing.getStartTrim()) {
            this.mFinishing = true;
            this.mAnimator.setDuration(666);
            this.mAnimator.start();
            return;
        }
        this.mRing.setColorIndex(0);
        this.mRing.resetOriginals();
        this.mAnimator.setDuration(1332);
        this.mAnimator.start();
    }

    public void stop() {
        this.mAnimator.cancel();
        setRotation(0.0f);
        this.mRing.setColorIndex(0);
        this.mRing.resetOriginals();
        invalidateSelf();
    }

    /* access modifiers changed from: private */
    public void updateRingColor(float f, Ring ring) {
        if (f > 0.75f) {
            ring.setColor(evaluateColorChange((f - 0.75f) / 0.25f, ring.getStartingColor(), ring.getNextColor()));
        } else {
            ring.setColor(ring.getStartingColor());
        }
    }

    private void applyFinishTranslation(float f, Ring ring) {
        updateRingColor(f, ring);
        float floor = (float) (Math.floor((double) (ring.getStartingRotation() / 0.8f)) + 1.0d);
        ring.setStartTrim(ring.getStartingStartTrim() + (((ring.getStartingEndTrim() - 0.01f) - ring.getStartingStartTrim()) * f));
        ring.setEndTrim(ring.getStartingEndTrim());
        ring.setRotation(ring.getStartingRotation() + ((floor - ring.getStartingRotation()) * f));
    }

    /* access modifiers changed from: private */
    public void applyTransformation(float f, Ring ring, boolean z) {
        float f2;
        float f3;
        if (this.mFinishing) {
            applyFinishTranslation(f, ring);
        } else if (f != 1.0f || z) {
            float startingRotation = ring.getStartingRotation();
            if (f < 0.5f) {
                float f4 = f / 0.5f;
                float startingStartTrim = ring.getStartingStartTrim();
                float f5 = startingStartTrim;
                f2 = (MATERIAL_INTERPOLATOR.getInterpolation(f4) * 0.79f) + startingStartTrim + 0.01f;
                f3 = f5;
            } else {
                f2 = ring.getStartingStartTrim() + 0.79f;
                f3 = f2 - (((1.0f - MATERIAL_INTERPOLATOR.getInterpolation((f - 0.5f) / 0.5f)) * 0.79f) + 0.01f);
            }
            float f6 = startingRotation + (0.20999998f * f);
            float f7 = (f + this.mRotationCount) * 216.0f;
            ring.setStartTrim(f3);
            ring.setEndTrim(f2);
            ring.setRotation(f6);
            setRotation(f7);
        }
    }

    private void setupAnimators() {
        final Ring ring = this.mRing;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        ofFloat.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                CircularProgressDrawable.this.updateRingColor(floatValue, ring);
                CircularProgressDrawable.this.applyTransformation(floatValue, ring, false);
                CircularProgressDrawable.this.invalidateSelf();
            }
        });
        ofFloat.setRepeatCount(-1);
        ofFloat.setRepeatMode(1);
        ofFloat.setInterpolator(LINEAR_INTERPOLATOR);
        ofFloat.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
                CircularProgressDrawable.this.mRotationCount = 0.0f;
            }

            public void onAnimationRepeat(Animator animator) {
                CircularProgressDrawable.this.applyTransformation(1.0f, ring, true);
                ring.storeOriginals();
                ring.goToNextColor();
                if (CircularProgressDrawable.this.mFinishing) {
                    CircularProgressDrawable.this.mFinishing = false;
                    animator.cancel();
                    animator.setDuration(1332);
                    animator.start();
                    return;
                }
                CircularProgressDrawable.this.mRotationCount = CircularProgressDrawable.this.mRotationCount + 1.0f;
            }
        });
        this.mAnimator = ofFloat;
    }
}
