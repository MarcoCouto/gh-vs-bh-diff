package com.explorestack.iab.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.support.annotation.VisibleForTesting;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public final class CircularProgressBar extends ProgressBar {
    private static final int RADIUS_DP = 20;
    private static final int WIDTH_DP = 4;
    @VisibleForTesting
    final Paint backgroundPaint = new Paint(1);
    @VisibleForTesting
    CircularProgressDrawable progressDrawable;

    public CircularProgressBar(Context context) {
        super(context);
        this.backgroundPaint.setColor(0);
        init(context);
    }

    public CircularProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.backgroundPaint.setColor(0);
        init(context);
    }

    public CircularProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.backgroundPaint.setColor(0);
        init(context);
    }

    /* access modifiers changed from: 0000 */
    public void init(Context context) {
        float f = getResources().getDisplayMetrics().density;
        int dpToPx = Utils.dpToPx(context, 8.0f);
        setPadding(dpToPx, dpToPx, dpToPx, dpToPx);
        this.progressDrawable = new CircularProgressDrawable(context);
        this.progressDrawable.setCenterRadius(20.0f * f);
        this.progressDrawable.setStrokeWidth(f * 4.0f);
        this.progressDrawable.setColorSchemeColors(-65536);
        this.progressDrawable.setStrokeCap(Cap.ROUND);
        setIndeterminateDrawable(this.progressDrawable);
        setIndeterminate(true);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        canvas.drawCircle(((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, ((float) Math.min(getWidth(), getHeight())) / 2.0f, this.backgroundPaint);
        super.onDraw(canvas);
    }

    public void setColorSchemeColors(int... iArr) {
        this.progressDrawable.setColorSchemeColors(iArr);
    }

    public void setProgressBackgroundColor(int i) {
        this.backgroundPaint.setColor(i);
    }
}
