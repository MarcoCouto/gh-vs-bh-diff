package com.explorestack.iab.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import com.explorestack.iab.mraid.MRAIDView;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TJAdUnitConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import okhttp3.internal.http.StatusLine;

public class Utils {
    private static final String TAG = "Utils";
    private static final Handler uiHandler = new Handler(Looper.getMainLooper());

    private static float pixelsToFloatDips(float f, float f2) {
        return f / f2;
    }

    @SuppressLint({"MissingPermission"})
    public static boolean isNetworkAvailable(Context context) {
        CommonLog.d(TAG, "Testing connectivity:");
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                CommonLog.d(TAG, "Connected to Internet");
                return true;
            }
        }
        CommonLog.d(TAG, "No Internet connection");
        return false;
    }

    public static void httpGetURL(@Nullable final String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                Executors.newSingleThreadExecutor().execute(new Runnable() {
                    /* JADX WARNING: Removed duplicated region for block: B:21:0x008b A[SYNTHETIC, Splitter:B:21:0x008b] */
                    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
                    public void run() {
                        HttpURLConnection httpURLConnection;
                        Exception e;
                        try {
                            CommonLog.d(Utils.TAG, String.format(Locale.ENGLISH, "Connection to URL: %s", new Object[]{str}));
                            URL url = new URL(str);
                            HttpURLConnection.setFollowRedirects(true);
                            httpURLConnection = (HttpURLConnection) url.openConnection();
                            try {
                                httpURLConnection.setConnectTimeout(5000);
                                httpURLConnection.setRequestProperty("Connection", "close");
                                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                                int responseCode = httpURLConnection.getResponseCode();
                                CommonLog.d(Utils.TAG, String.format(Locale.ENGLISH, "Response code: %d, for URL: %s", new Object[]{Integer.valueOf(responseCode), str}));
                                if (httpURLConnection == null) {
                                    return;
                                }
                            } catch (Exception e2) {
                                e = e2;
                                try {
                                    CommonLog.e(Utils.TAG, String.format(Locale.ENGLISH, "%s: %s: %s", new Object[]{str, e.getMessage(), e.toString()}));
                                    if (httpURLConnection == null) {
                                        return;
                                    }
                                    httpURLConnection.disconnect();
                                } catch (Throwable th) {
                                    th = th;
                                    if (httpURLConnection != null) {
                                    }
                                    throw th;
                                }
                            }
                        } catch (Exception e3) {
                            Exception exc = e3;
                            httpURLConnection = null;
                            e = exc;
                            CommonLog.e(Utils.TAG, String.format(Locale.ENGLISH, "%s: %s: %s", new Object[]{str, e.getMessage(), e.toString()}));
                            if (httpURLConnection == null) {
                            }
                            httpURLConnection.disconnect();
                        } catch (Throwable th2) {
                            th = th2;
                            httpURLConnection = null;
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.disconnect();
                                } catch (Exception unused) {
                                }
                            }
                            throw th;
                        }
                        try {
                            httpURLConnection.disconnect();
                        } catch (Exception unused2) {
                        }
                    }
                });
            } catch (Exception e) {
                CommonLog.e(TAG, e.getMessage());
            }
        } else {
            CommonLog.d(TAG, "url is null or empty");
        }
    }

    public static Pair<Integer, Integer> getScreenSize(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        return new Pair<>(Integer.valueOf(point.x), Integer.valueOf(point.y));
    }

    public static boolean isTablet(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        double d = (double) (((float) displayMetrics.widthPixels) / displayMetrics.xdpi);
        double d2 = (double) (((float) displayMetrics.heightPixels) / displayMetrics.ydpi);
        Double.isNaN(d);
        Double.isNaN(d);
        double d3 = d * d;
        Double.isNaN(d2);
        Double.isNaN(d2);
        return Math.sqrt(d3 + (d2 * d2)) >= 6.6d;
    }

    public static float getScreenDensity(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        return displayMetrics.density;
    }

    public static void onUiThread(Runnable runnable) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            runnable.run();
        } else {
            uiHandler.post(runnable);
        }
    }

    public static void onUiThread(Runnable runnable, long j) {
        uiHandler.postDelayed(runnable, j);
    }

    public static boolean openBrowser(final Context context, String str, final Runnable runnable) {
        final String validUrl = getValidUrl(str);
        if (isHttpUrl(validUrl)) {
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                public void run() {
                    Utils.launchUrl(context, Utils.findEndpoint(validUrl));
                    if (runnable != null) {
                        Utils.onUiThread(runnable);
                    }
                }
            });
            return true;
        }
        if (runnable != null) {
            onUiThread(runnable);
        }
        return launchUrl(context, validUrl);
    }

    public static String getValidUrl(String str) {
        try {
            new URL(str);
            return str;
        } catch (MalformedURLException unused) {
            try {
                return URLDecoder.decode(str, "UTF-8");
            } catch (UnsupportedEncodingException unused2) {
                return str;
            } catch (IllegalArgumentException unused3) {
                return str;
            }
        }
    }

    public static boolean isHttpUrl(String str) {
        return str.startsWith("http://") || str.startsWith("https://");
    }

    /* access modifiers changed from: private */
    public static boolean launchUrl(Context context, String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.setFlags(268435456);
            ComponentName pickBrowser = pickBrowser(context, intent);
            if (pickBrowser != null) {
                intent.setComponent(pickBrowser);
                context.startActivity(intent);
                return true;
            }
            String decode = URLDecoder.decode(str, "UTF-8");
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(decode));
            intent2.setFlags(268435456);
            ComponentName pickBrowser2 = pickBrowser(context, intent2);
            if (pickBrowser2 != null) {
                intent2.setComponent(pickBrowser2);
                context.startActivity(intent2);
                return true;
            }
            CommonLog.e(TAG, String.format("No activities to handle intent: %s", new Object[]{decode}));
            return false;
        } catch (Exception e) {
            CommonLog.e(TAG, (Throwable) e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:85:0x00e1 A[SYNTHETIC, Splitter:B:85:0x00e1] */
    static String findEndpoint(String str) {
        HttpURLConnection httpURLConnection;
        Exception e;
        try {
            URL url = new URL(str);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            try {
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.setConnectTimeout(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
                httpURLConnection.setReadTimeout(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
                switch (httpURLConnection.getResponseCode()) {
                    case 301:
                    case 302:
                    case 303:
                    case IronSourceConstants.OFFERWALL_OPENED /*305*/:
                    case StatusLine.HTTP_TEMP_REDIRECT /*307*/:
                        String headerField = httpURLConnection.getHeaderField("Location");
                        if (headerField == null) {
                            String url2 = url.toString();
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.disconnect();
                                } catch (Exception e2) {
                                    CommonLog.e(TAG, (Throwable) e2);
                                }
                            }
                            return url2;
                        } else if (isHttpUrl(headerField)) {
                            String findEndpoint = findEndpoint(httpURLConnection.getHeaderField("Location"));
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.disconnect();
                                } catch (Exception e3) {
                                    CommonLog.e(TAG, (Throwable) e3);
                                }
                            }
                            return findEndpoint;
                        } else if (new URI(headerField).getScheme() == null) {
                            try {
                                String url3 = new URL(url, headerField).toString();
                                if (url3.trim().length() > 0) {
                                    String findEndpoint2 = findEndpoint(url3);
                                    if (httpURLConnection != null) {
                                        try {
                                            httpURLConnection.disconnect();
                                        } catch (Exception e4) {
                                            CommonLog.e(TAG, (Throwable) e4);
                                        }
                                    }
                                    return findEndpoint2;
                                }
                                if (httpURLConnection != null) {
                                    try {
                                        httpURLConnection.disconnect();
                                    } catch (Exception e5) {
                                        CommonLog.e(TAG, (Throwable) e5);
                                    }
                                }
                                return headerField;
                            } catch (Exception e6) {
                                CommonLog.e(TAG, (Throwable) e6);
                                if (httpURLConnection != null) {
                                    try {
                                        httpURLConnection.disconnect();
                                    } catch (Exception e7) {
                                        CommonLog.e(TAG, (Throwable) e7);
                                    }
                                }
                                return headerField;
                            }
                        } else {
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.disconnect();
                                } catch (Exception e8) {
                                    CommonLog.e(TAG, (Throwable) e8);
                                }
                            }
                            return headerField;
                        }
                    default:
                        String url4 = url.toString();
                        if (httpURLConnection != null) {
                            try {
                                httpURLConnection.disconnect();
                            } catch (Exception e9) {
                                CommonLog.e(TAG, (Throwable) e9);
                            }
                        }
                        return url4;
                }
            } catch (Exception e10) {
                e = e10;
            }
            e = e10;
        } catch (Exception e11) {
            httpURLConnection = null;
            e = e11;
        } catch (Throwable th) {
            th = th;
            httpURLConnection = null;
            if (httpURLConnection != null) {
                try {
                    httpURLConnection.disconnect();
                } catch (Exception e12) {
                    CommonLog.e(TAG, (Throwable) e12);
                }
            }
            throw th;
        }
        try {
            CommonLog.e(TAG, (Throwable) e);
            if (httpURLConnection != null) {
                try {
                    httpURLConnection.disconnect();
                } catch (Exception e13) {
                    CommonLog.e(TAG, (Throwable) e13);
                }
            }
            return str;
        } catch (Throwable th2) {
            th = th2;
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    public static ComponentName pickBrowser(Context context, Intent intent) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (queryIntentActivities.isEmpty()) {
            return null;
        }
        for (ResolveInfo resolveInfo : queryIntentActivities) {
            if (resolveInfo.activityInfo.packageName.equals("com.android.vending")) {
                return new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
            }
        }
        return new ComponentName(((ResolveInfo) queryIntentActivities.get(0)).activityInfo.packageName, ((ResolveInfo) queryIntentActivities.get(0)).activityInfo.name);
    }

    public static int getScreenOrientation(Context context) {
        int rotation = ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRotation();
        int i = context.getResources().getConfiguration().orientation;
        if (i == 1) {
            switch (rotation) {
                case 2:
                case 3:
                    return 9;
                default:
                    return 1;
            }
        } else if (i != 2) {
            return 9;
        } else {
            switch (rotation) {
                case 2:
                case 3:
                    return 8;
                default:
                    return 0;
            }
        }
    }

    public static boolean isLandscapeOrientation(Context context) {
        int screenOrientation = getScreenOrientation(context);
        return screenOrientation == 0 || screenOrientation == 8 || screenOrientation == 6 || (VERSION.SDK_INT >= 18 && screenOrientation == 11);
    }

    public static int dpToPx(Context context, float f) {
        return (int) TypedValue.applyDimension(1, f, context.getResources().getDisplayMetrics());
    }

    public static int pixelsToIntDips(float f, Context context) {
        return pixelsToIntDips(f, getDensity(context));
    }

    public static int pixelsToIntDips(float f, float f2) {
        return (int) (pixelsToFloatDips(f, f2) + 0.5f);
    }

    private static float pixelsToFloatDips(float f, Context context) {
        return pixelsToFloatDips(f, getDensity(context));
    }

    private static float getDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    @NonNull
    public static String stringifyRect(@NonNull Rect rect) {
        StringBuilder sb = new StringBuilder();
        sb.append(rect.left);
        sb.append(",");
        sb.append(rect.top);
        sb.append(",");
        sb.append(rect.width());
        sb.append(",");
        sb.append(rect.height());
        return sb.toString();
    }

    public static void addBannerSpinnerView(@Nullable View view) {
        if (view instanceof MRAIDView) {
            ProgressBar progressBar = new ProgressBar(view.getContext());
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            progressBar.setLayoutParams(layoutParams);
            progressBar.setBackgroundColor(0);
            progressBar.setTag("Appodeal Spinner View");
            ((MRAIDView) view).addView(progressBar);
        }
    }

    public static void hideBannerSpinnerView(@Nullable final View view) {
        if (view instanceof MRAIDView) {
            view.post(new Runnable() {
                public void run() {
                    try {
                        int childCount = ((MRAIDView) view).getChildCount();
                        for (int i = 0; i < childCount; i++) {
                            View childAt = ((MRAIDView) view).getChildAt(i);
                            Object tag = childAt.getTag();
                            if (tag != null && tag.equals("Appodeal Spinner View")) {
                                childAt.setVisibility(8);
                            }
                        }
                    } catch (Exception e) {
                        CommonLog.e(Utils.TAG, (Throwable) e);
                    }
                }
            });
        }
    }

    public static void applyFullscreenActivityFlags(Activity activity) {
        activity.requestWindowFeature(1);
        activity.getWindow().addFlags(128);
        activity.getWindow().setFlags(1024, 1024);
        if (VERSION.SDK_INT > 15) {
            activity.getWindow().getDecorView().setSystemUiVisibility(4);
        }
        hideKeyboard(activity);
    }

    private static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService("input_method");
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus == null) {
            currentFocus = new View(activity);
        }
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }
}
