package com.explorestack.iab.vast.processor.url;

import android.os.Bundle;
import android.support.annotation.Nullable;

public interface UrlProcessor {
    @Nullable
    String prepare(@Nullable String str, @Nullable Bundle bundle);
}
