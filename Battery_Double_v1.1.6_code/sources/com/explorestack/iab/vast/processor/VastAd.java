package com.explorestack.iab.vast.processor;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.explorestack.iab.utils.Utils;
import com.explorestack.iab.vast.TrackingEvent;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.tags.AppodealExtensionTag;
import com.explorestack.iab.vast.tags.CompanionTag;
import com.explorestack.iab.vast.tags.LinearCreativeTag;
import com.explorestack.iab.vast.tags.MediaFileTag;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class VastAd implements Parcelable {
    public static final Creator<VastAd> CREATOR = new Creator<VastAd>() {
        public VastAd createFromParcel(Parcel parcel) {
            return new VastAd(parcel);
        }

        public VastAd[] newArray(int i) {
            return new VastAd[i];
        }
    };
    private AppodealExtensionTag appodealExtension;
    private ArrayList<String> clickTrackingUrlList;
    private ArrayList<CompanionTag> companionTagList;
    @NonNull
    private final LinearCreativeTag creativeTag;
    private ArrayList<String> errorUrlList;
    private ArrayList<String> impressionUrlList;
    @NonNull
    private final MediaFileTag pickedMediaFileTag;
    private EnumMap<TrackingEvent, List<String>> trackingEventListMap;
    @Nullable
    private VastRequest vastRequest;

    public int describeContents() {
        return 0;
    }

    VastAd(@NonNull LinearCreativeTag linearCreativeTag, @NonNull MediaFileTag mediaFileTag) {
        this.creativeTag = linearCreativeTag;
        this.pickedMediaFileTag = mediaFileTag;
    }

    protected VastAd(Parcel parcel) {
        this.creativeTag = (LinearCreativeTag) parcel.readSerializable();
        this.pickedMediaFileTag = (MediaFileTag) parcel.readSerializable();
        this.companionTagList = (ArrayList) parcel.readSerializable();
        this.impressionUrlList = parcel.createStringArrayList();
        this.errorUrlList = parcel.createStringArrayList();
        this.clickTrackingUrlList = parcel.createStringArrayList();
        this.trackingEventListMap = (EnumMap) parcel.readSerializable();
        this.appodealExtension = (AppodealExtensionTag) parcel.readSerializable();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.creativeTag);
        parcel.writeSerializable(this.pickedMediaFileTag);
        parcel.writeSerializable(this.companionTagList);
        parcel.writeStringList(this.impressionUrlList);
        parcel.writeStringList(this.errorUrlList);
        parcel.writeStringList(this.clickTrackingUrlList);
        parcel.writeSerializable(this.trackingEventListMap);
        parcel.writeSerializable(this.appodealExtension);
    }

    public void setVastRequest(@Nullable VastRequest vastRequest2) {
        this.vastRequest = vastRequest2;
    }

    public int getSkipOffsetSec() {
        return this.creativeTag.getSkipOffsetSec();
    }

    public String getClickThroughUrl() {
        if (this.creativeTag.getVideoClicksTag() != null) {
            return this.creativeTag.getVideoClicksTag().getClickThroughUrl();
        }
        return null;
    }

    @Nullable
    public String getAdParameters() {
        return this.creativeTag.getAdParameters();
    }

    @NonNull
    public MediaFileTag getPickedMediaFileTag() {
        return this.pickedMediaFileTag;
    }

    public List<String> getImpressionUrlList() {
        return this.impressionUrlList;
    }

    /* access modifiers changed from: 0000 */
    public void setImpressionUrlList(ArrayList<String> arrayList) {
        this.impressionUrlList = arrayList;
    }

    public List<String> getErrorUrlList() {
        return this.errorUrlList;
    }

    /* access modifiers changed from: 0000 */
    public void setErrorUrlList(ArrayList<String> arrayList) {
        this.errorUrlList = arrayList;
    }

    public List<String> getClickTrackingUrlList() {
        return this.clickTrackingUrlList;
    }

    /* access modifiers changed from: 0000 */
    public void setClickTrackingUrlList(ArrayList<String> arrayList) {
        this.clickTrackingUrlList = arrayList;
    }

    public Map<TrackingEvent, List<String>> getTrackingEventListMap() {
        return this.trackingEventListMap;
    }

    /* access modifiers changed from: 0000 */
    public void setTrackingEventListMap(EnumMap<TrackingEvent, List<String>> enumMap) {
        this.trackingEventListMap = enumMap;
    }

    /* access modifiers changed from: 0000 */
    public void setCompanionTagList(ArrayList<CompanionTag> arrayList) {
        this.companionTagList = arrayList;
    }

    public CompanionTag getCompanion(int i, int i2) {
        if (this.companionTagList == null || this.companionTagList.isEmpty()) {
            sendError(600);
            return null;
        }
        HashMap hashMap = new HashMap();
        Iterator it = this.companionTagList.iterator();
        while (it.hasNext()) {
            CompanionTag companionTag = (CompanionTag) it.next();
            int width = companionTag.getWidth();
            int height = companionTag.getHeight();
            if (width > -1 && height > -1) {
                float max = ((float) Math.max(width, height)) / ((float) Math.min(width, height));
                if (Math.min(width, height) >= 250 && ((double) max) <= 2.5d && companionTag.isValid(i, i2)) {
                    hashMap.put(Float.valueOf(((float) width) / ((float) height)), companionTag);
                }
            }
        }
        if (!hashMap.isEmpty()) {
            float f = ((float) i) / ((float) i2);
            Set<Float> keySet = hashMap.keySet();
            float floatValue = ((Float) keySet.iterator().next()).floatValue();
            for (Float floatValue2 : keySet) {
                float floatValue3 = floatValue2.floatValue();
                if (Math.abs(floatValue - f) > Math.abs(floatValue3 - f)) {
                    floatValue = floatValue3;
                }
            }
            return (CompanionTag) hashMap.get(Float.valueOf(floatValue));
        }
        sendError(600);
        return null;
    }

    public CompanionTag getBanner(Context context) {
        if (this.companionTagList == null || this.companionTagList.isEmpty()) {
            return null;
        }
        Iterator it = this.companionTagList.iterator();
        while (it.hasNext()) {
            CompanionTag companionTag = (CompanionTag) it.next();
            int width = companionTag.getWidth();
            int height = companionTag.getHeight();
            if (width > -1 && height > -1) {
                if (Utils.isTablet(context) && width == 728 && height == 90) {
                    return companionTag;
                }
                if (!Utils.isTablet(context) && width == 320 && height == 50) {
                    return companionTag;
                }
            }
        }
        return null;
    }

    public AppodealExtensionTag getAppodealExtension() {
        return this.appodealExtension;
    }

    /* access modifiers changed from: 0000 */
    public void setAppodealExtension(AppodealExtensionTag appodealExtensionTag) {
        this.appodealExtension = appodealExtensionTag;
    }

    /* access modifiers changed from: 0000 */
    public void sendError(int i) {
        if (this.vastRequest != null) {
            this.vastRequest.sendError(i);
        }
    }
}
