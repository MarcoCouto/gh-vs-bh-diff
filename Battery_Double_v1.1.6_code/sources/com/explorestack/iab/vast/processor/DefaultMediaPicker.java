package com.explorestack.iab.vast.processor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Pair;
import com.explorestack.iab.utils.Utils;
import com.explorestack.iab.vast.VastLog;
import com.explorestack.iab.vast.tags.LinearCreativeTag;
import com.explorestack.iab.vast.tags.MediaFileTag;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DefaultMediaPicker extends VastMediaPicker<MediaFileTag> {
    private static final String SUPPORTED_VIDEO_TYPE_REGEX = "video/.*(?i)(mp4|3gpp|mp2t|webm|matroska)";
    private static final String TAG = "DefaultMediaPicker";
    /* access modifiers changed from: private */
    public int deviceArea;
    private boolean isLandscapeOrientation;

    private class AreaComparator implements Comparator<Pair<LinearCreativeTag, MediaFileTag>> {
        private AreaComparator() {
        }

        public int compare(Pair<LinearCreativeTag, MediaFileTag> pair, Pair<LinearCreativeTag, MediaFileTag> pair2) {
            int width = ((MediaFileTag) pair.second).getWidth() * ((MediaFileTag) pair.second).getHeight();
            int width2 = ((MediaFileTag) pair2.second).getWidth() * ((MediaFileTag) pair2.second).getHeight();
            int abs = Math.abs(width - DefaultMediaPicker.this.deviceArea);
            int abs2 = Math.abs(width2 - DefaultMediaPicker.this.deviceArea);
            String str = DefaultMediaPicker.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("AreaComparator: obj1:");
            sb.append(abs);
            sb.append(" obj2:");
            sb.append(abs2);
            VastLog.d(str, sb.toString());
            if (abs < abs2) {
                return -1;
            }
            return abs > abs2 ? 1 : 0;
        }
    }

    public DefaultMediaPicker(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        setDeviceArea(displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.isLandscapeOrientation = Utils.isLandscapeOrientation(context);
    }

    public DefaultMediaPicker(int i, int i2) {
        setDeviceArea(i, i2);
    }

    private void setDeviceArea(int i, int i2) {
        this.deviceArea = i * i2;
    }

    public Pair<LinearCreativeTag, MediaFileTag> pickVideo(@Nullable List<Pair<LinearCreativeTag, MediaFileTag>> list) {
        if (list == null) {
            return null;
        }
        Collections.sort(list, new AreaComparator());
        return getBestMatch(list);
    }

    /* access modifiers changed from: protected */
    public boolean isMediaFileCompatible(MediaFileTag mediaFileTag) {
        return mediaFileTag.getType().matches(SUPPORTED_VIDEO_TYPE_REGEX);
    }

    /* access modifiers changed from: protected */
    public boolean isOrientationEquals(MediaFileTag mediaFileTag) {
        if ((mediaFileTag.getWidth() > mediaFileTag.getHeight()) == this.isLandscapeOrientation) {
            return true;
        }
        return false;
    }

    private Pair<LinearCreativeTag, MediaFileTag> getBestMatch(@NonNull List<Pair<LinearCreativeTag, MediaFileTag>> list) {
        VastLog.d(TAG, "getBestMatch");
        Pair<LinearCreativeTag, MediaFileTag> pair = null;
        for (Pair<LinearCreativeTag, MediaFileTag> pair2 : list) {
            if (isMediaFileCompatible((MediaFileTag) pair2.second)) {
                if (isOrientationEquals((MediaFileTag) pair2.second)) {
                    return pair2;
                }
                if (pair == null) {
                    pair = pair2;
                }
            }
        }
        return pair;
    }
}
