package com.explorestack.iab.vast.processor;

import com.explorestack.iab.vast.tags.VastXmlTag;

class WrapperAttributes {
    static final boolean DEF_ALLOW_MULTIPLE_ADS = true;
    static final boolean DEF_FALLBACK_ON_NO_AD = true;
    static final boolean DEF_FOLLOW_ADDITIONAL_WRAPPERS = true;
    private boolean allowMultipleAds;
    private boolean fallbackOnNoAd;
    private boolean followAdditionalWrappers;

    WrapperAttributes() {
        this(true, true, true);
    }

    WrapperAttributes(VastXmlTag vastXmlTag) {
        this(vastXmlTag.getBooleanAttributeValueByName("followAdditionalWrappers", true), vastXmlTag.getBooleanAttributeValueByName("allowMultipleAds", true), vastXmlTag.getBooleanAttributeValueByName("fallbackOnNoAd", true));
    }

    private WrapperAttributes(boolean z, boolean z2, boolean z3) {
        this.followAdditionalWrappers = z;
        this.allowMultipleAds = z2;
        this.fallbackOnNoAd = z3;
    }

    /* access modifiers changed from: 0000 */
    public boolean isFollowAdditionalWrappers() {
        return this.followAdditionalWrappers;
    }

    /* access modifiers changed from: 0000 */
    public boolean isAllowMultipleAds() {
        return this.allowMultipleAds;
    }

    /* access modifiers changed from: 0000 */
    public boolean isFallbackOnNoAd() {
        return this.fallbackOnNoAd;
    }
}
