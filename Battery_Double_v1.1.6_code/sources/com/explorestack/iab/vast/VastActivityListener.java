package com.explorestack.iab.vast;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.explorestack.iab.vast.activity.VastActivity;

public interface VastActivityListener extends VastErrorListener {
    void onVastClick(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest, @NonNull VastClickCallback vastClickCallback, @Nullable String str);

    void onVastComplete(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest);

    void onVastDismiss(@NonNull VastActivity vastActivity, @Nullable VastRequest vastRequest, boolean z);

    void onVastShown(@NonNull VastActivity vastActivity, @NonNull VastRequest vastRequest);
}
