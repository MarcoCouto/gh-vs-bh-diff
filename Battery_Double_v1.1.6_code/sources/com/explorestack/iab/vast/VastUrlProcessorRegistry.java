package com.explorestack.iab.vast;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.explorestack.iab.vast.processor.url.ErrorCodeUrlProcessor;
import com.explorestack.iab.vast.processor.url.UrlProcessor;
import java.util.ArrayList;
import java.util.List;

public class VastUrlProcessorRegistry {
    @VisibleForTesting
    static List<UrlProcessor> urlProcessors = new ArrayList<UrlProcessor>() {
        {
            add(new ErrorCodeUrlProcessor());
        }
    };

    public interface OnUrlReadyCallback {
        void onUrlReady(String str);
    }

    public static void register(UrlProcessor urlProcessor) {
        urlProcessors.add(urlProcessor);
    }

    public static void unregister(UrlProcessor urlProcessor) {
        urlProcessors.remove(urlProcessor);
    }

    public static void processUrls(@Nullable List<String> list, @Nullable Bundle bundle, @Nullable OnUrlReadyCallback onUrlReadyCallback) {
        if (list != null && !list.isEmpty() && onUrlReadyCallback != null) {
            for (String processUrl : list) {
                onUrlReadyCallback.onUrlReady(processUrl(processUrl, bundle));
            }
        }
    }

    public static String processUrl(@Nullable String str, @Nullable Bundle bundle) {
        if (str == null) {
            return null;
        }
        for (UrlProcessor prepare : urlProcessors) {
            str = prepare.prepare(str, bundle);
        }
        return str;
    }
}
