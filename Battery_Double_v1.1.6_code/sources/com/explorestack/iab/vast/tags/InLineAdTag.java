package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class InLineAdTag extends AdContentTag {
    InLineAdTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, "InLine");
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, "Creatives")) {
                    setCreativeTagList(parseCreatives(xmlPullParser));
                } else if (isEquals(name, "Extensions")) {
                    setExtensionTagList(parseExtensions(xmlPullParser));
                } else if (isEquals(name, "Impression")) {
                    addImpressionUrl(parseText(xmlPullParser));
                } else if (isEquals(name, "Error")) {
                    addErrorUrl(parseText(xmlPullParser));
                } else if (isEquals(name, "AdSystem")) {
                    setAdSystemTag(new AdSystemTag(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, "InLine");
    }
}
