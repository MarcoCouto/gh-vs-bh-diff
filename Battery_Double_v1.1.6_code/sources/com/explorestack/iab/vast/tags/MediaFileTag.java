package com.explorestack.iab.vast.tags;

import android.text.TextUtils;
import org.xmlpull.v1.XmlPullParser;

public class MediaFileTag extends VastXmlTag {
    private static final String[] supportedAttributes = {"delivery", "type", "width", "height", "codec", "id", "bitrate", "minBitrate", "maxBitrate", "scalable", "maintainAspectRatio", "apiFramework"};

    public boolean isTextSupported() {
        return true;
    }

    MediaFileTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }

    public boolean isValidTag() {
        return !TextUtils.isEmpty(getAttributeValueByName("type")) && !TextUtils.isEmpty(getAttributeValueByName("width")) && !TextUtils.isEmpty(getAttributeValueByName("height")) && !TextUtils.isEmpty(getText());
    }

    public int getWidth() {
        return getIntegerAttributeValueByName("width");
    }

    public int getHeight() {
        return getIntegerAttributeValueByName("height");
    }

    public String getType() {
        return getAttributeValueByName("type");
    }

    public String getApiFramework() {
        return getAttributeValueByName("apiFramework");
    }
}
