package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class AdSystemTag extends VastXmlTag {
    private static final String[] supportedAttributes = {"version"};

    public boolean isTextSupported() {
        return true;
    }

    AdSystemTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
