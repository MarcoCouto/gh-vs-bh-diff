package com.explorestack.iab.vast.tags;

import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class AdContentTag extends VastXmlTag {
    private AdSystemTag adSystemTag;
    private List<CreativeTag> creativeTagList;
    private List<String> errorUrlList;
    private List<ExtensionTag> extensionTagList;
    private List<String> impressionUrlList;

    AdContentTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
    }

    /* access modifiers changed from: 0000 */
    public AdSystemTag getAdSystemTag() {
        return this.adSystemTag;
    }

    /* access modifiers changed from: 0000 */
    public void setAdSystemTag(AdSystemTag adSystemTag2) {
        this.adSystemTag = adSystemTag2;
    }

    public List<CreativeTag> getCreativeTagList() {
        return this.creativeTagList;
    }

    /* access modifiers changed from: 0000 */
    public void setCreativeTagList(List<CreativeTag> list) {
        this.creativeTagList = list;
    }

    public List<ExtensionTag> getExtensionTagList() {
        return this.extensionTagList;
    }

    /* access modifiers changed from: 0000 */
    public void setExtensionTagList(List<ExtensionTag> list) {
        this.extensionTagList = list;
    }

    public List<String> getImpressionUrlList() {
        return this.impressionUrlList;
    }

    /* access modifiers changed from: 0000 */
    public void addImpressionUrl(String str) {
        if (this.impressionUrlList == null) {
            this.impressionUrlList = new ArrayList();
        }
        this.impressionUrlList.add(str);
    }

    public List<String> getErrorUrlList() {
        return this.errorUrlList;
    }

    /* access modifiers changed from: 0000 */
    public void addErrorUrl(String str) {
        if (this.errorUrlList == null) {
            this.errorUrlList = new ArrayList();
        }
        this.errorUrlList.add(str);
    }

    /* access modifiers changed from: 0000 */
    public List<CreativeTag> parseCreatives(XmlPullParser xmlPullParser) throws Exception {
        xmlPullParser.require(2, null, "Creatives");
        ArrayList arrayList = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (isEquals(xmlPullParser.getName(), "Creative")) {
                    arrayList.add(new CreativeTag(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, "Creatives");
        return arrayList;
    }

    /* access modifiers changed from: 0000 */
    public List<ExtensionTag> parseExtensions(XmlPullParser xmlPullParser) throws Exception {
        xmlPullParser.require(2, null, "Extensions");
        ArrayList arrayList = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (!isEquals(xmlPullParser.getName(), "Extension")) {
                    skip(xmlPullParser);
                } else if (isEquals(new ExtensionTag(xmlPullParser).getAttributeValueByName("type"), "appodeal")) {
                    arrayList.add(new AppodealExtensionTag(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, "Extensions");
        return arrayList;
    }
}
