package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class TrackingTag extends VastXmlTag {
    private static final String[] supportedAttributes = {"event"};

    TrackingTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
