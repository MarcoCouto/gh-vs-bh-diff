package com.explorestack.iab.vast.tags;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Pair;
import com.explorestack.iab.utils.Assets;
import com.explorestack.iab.vast.VastExtension;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.tapjoy.TJAdUnitConstants.String;
import org.xmlpull.v1.XmlPullParser;

public class AppodealExtensionTag extends ExtensionTag implements VastExtension {
    private int assetsBackgroundColor = 0;
    private int assetsColor = Assets.mainAssetsColor;
    private int closeXPosition = 11;
    private int closeYPosition = 10;
    private int companionCloseTime;
    private CompanionTag companionTag;
    private String ctaText;
    private int ctaXPosition = 11;
    private int ctaYPosition = 12;
    private int muteXPosition = 9;
    private int muteYPosition = 10;
    private boolean showCompanion = true;
    private boolean showCta = true;
    private boolean showMute = true;
    private boolean showProgress = true;
    private boolean videoClickable;

    AppodealExtensionTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, VastTagName.CTA_TEXT)) {
                    setCtaText(parseText(xmlPullParser));
                } else if (isEquals(name, VastTagName.SHOW_CTA)) {
                    setShowCta(parseBoolean(xmlPullParser));
                } else if (isEquals(name, VastTagName.SHOW_MUTE)) {
                    setShowMute(parseBoolean(xmlPullParser));
                } else if (isEquals(name, VastTagName.SHOW_COMPANION)) {
                    setShowCompanion(parseBoolean(xmlPullParser));
                } else if (isEquals(name, VastTagName.COMPANION_CLOSE_TIME)) {
                    int transformMinTimeToSec = transformMinTimeToSec(parseText(xmlPullParser));
                    if (transformMinTimeToSec > -1) {
                        setCompanionCloseTime(transformMinTimeToSec);
                    }
                } else if (isEquals(name, VastTagName.VIDEO_CLICKABLE)) {
                    setVideoClickable(parseBoolean(xmlPullParser));
                } else if (isEquals(name, VastTagName.CTA_X_POSITION)) {
                    int transformToXPosition = transformToXPosition(parseText(xmlPullParser));
                    if (transformToXPosition > -1) {
                        setCtaXPosition(transformToXPosition);
                    }
                } else if (isEquals(name, VastTagName.CTA_Y_POSITION)) {
                    int transformToYPosition = transformToYPosition(parseText(xmlPullParser));
                    if (transformToYPosition > -1) {
                        setCtaYPosition(transformToYPosition);
                    }
                } else if (isEquals(name, VastTagName.CLOSE_X_POSITION)) {
                    int transformToXPosition2 = transformToXPosition(parseText(xmlPullParser));
                    if (transformToXPosition2 > -1) {
                        setCloseXPosition(transformToXPosition2);
                    }
                } else if (isEquals(name, VastTagName.CLOSE_Y_POSITION)) {
                    int transformToYPosition2 = transformToYPosition(parseText(xmlPullParser));
                    if (transformToYPosition2 > -1) {
                        setCloseYPosition(transformToYPosition2);
                    }
                } else if (isEquals(name, VastTagName.MUTE_X_POSITION)) {
                    int transformToXPosition3 = transformToXPosition(parseText(xmlPullParser));
                    if (transformToXPosition3 > -1) {
                        setMuteXPosition(transformToXPosition3);
                    }
                } else if (isEquals(name, VastTagName.MUTE_Y_POSITION)) {
                    int transformToYPosition3 = transformToYPosition(parseText(xmlPullParser));
                    if (transformToYPosition3 > -1) {
                        setMuteYPosition(transformToYPosition3);
                    }
                } else if (isEquals(name, VastTagName.ASSETS_COLOR)) {
                    int transformColorHexToInt = transformColorHexToInt(parseText(xmlPullParser));
                    if (transformColorHexToInt != -1) {
                        setAssetsColor(transformColorHexToInt);
                    }
                } else if (isEquals(name, VastTagName.ASSETS_BACKGROUND_COLOR)) {
                    int transformColorHexToInt2 = transformColorHexToInt(parseText(xmlPullParser));
                    if (transformColorHexToInt2 != -1) {
                        setAssetsBackgroundColor(transformColorHexToInt2);
                    }
                } else if (isEquals(name, "Companion")) {
                    CompanionTag companionTag2 = new CompanionTag(xmlPullParser);
                    if (companionTag2.isValidTag() && companionTag2.isValid(ModuleDescriptor.MODULE_VERSION, 50)) {
                        setCompanionTag(companionTag2);
                    }
                } else if (isEquals(name, VastTagName.SHOW_PROGRESS)) {
                    setShowProgress(parseBoolean(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
    }

    public String getCtaText() {
        return this.ctaText;
    }

    public void setCtaText(String str) {
        this.ctaText = str;
    }

    public boolean isShowCta() {
        return this.showCta;
    }

    public void setShowCta(boolean z) {
        this.showCta = z;
    }

    public boolean isShowMute() {
        return this.showMute;
    }

    public void setShowMute(boolean z) {
        this.showMute = z;
    }

    public boolean isShowCompanion() {
        return this.showCompanion;
    }

    public void setShowCompanion(boolean z) {
        this.showCompanion = z;
    }

    public boolean isShowProgress() {
        return this.showProgress;
    }

    public void setShowProgress(boolean z) {
        this.showProgress = z;
    }

    public boolean isVideoClickable() {
        return this.videoClickable;
    }

    public void setVideoClickable(boolean z) {
        this.videoClickable = z;
    }

    public int getCompanionCloseTime() {
        return this.companionCloseTime;
    }

    public void setCompanionCloseTime(int i) {
        this.companionCloseTime = i;
    }

    /* access modifiers changed from: 0000 */
    public int getCtaXPosition() {
        return this.ctaXPosition;
    }

    private void setCtaXPosition(int i) {
        this.ctaXPosition = i;
    }

    /* access modifiers changed from: 0000 */
    public int getCtaYPosition() {
        return this.ctaYPosition;
    }

    private void setCtaYPosition(int i) {
        this.ctaYPosition = i;
    }

    public Pair<Integer, Integer> getCtaPosition() {
        return new Pair<>(Integer.valueOf(this.ctaXPosition), Integer.valueOf(this.ctaYPosition));
    }

    /* access modifiers changed from: 0000 */
    public int getCloseXPosition() {
        return this.closeXPosition;
    }

    private void setCloseXPosition(int i) {
        this.closeXPosition = i;
    }

    /* access modifiers changed from: 0000 */
    public int getCloseYPosition() {
        return this.closeYPosition;
    }

    private void setCloseYPosition(int i) {
        this.closeYPosition = i;
    }

    @NonNull
    public Pair<Integer, Integer> getClosePosition() {
        return new Pair<>(Integer.valueOf(this.closeXPosition), Integer.valueOf(this.closeYPosition));
    }

    /* access modifiers changed from: 0000 */
    public int getMuteXPosition() {
        return this.muteXPosition;
    }

    private void setMuteXPosition(int i) {
        this.muteXPosition = i;
    }

    /* access modifiers changed from: 0000 */
    public int getMuteYPosition() {
        return this.muteYPosition;
    }

    private void setMuteYPosition(int i) {
        this.muteYPosition = i;
    }

    public Pair<Integer, Integer> getMutePosition() {
        return new Pair<>(Integer.valueOf(this.muteXPosition), Integer.valueOf(this.muteYPosition));
    }

    public int getAssetsColor() {
        return this.assetsColor;
    }

    public void setAssetsColor(int i) {
        this.assetsColor = i;
    }

    public int getAssetsBackgroundColor() {
        return this.assetsBackgroundColor;
    }

    public void setAssetsBackgroundColor(int i) {
        this.assetsBackgroundColor = i;
    }

    public CompanionTag getCompanionTag() {
        return this.companionTag;
    }

    private void setCompanionTag(CompanionTag companionTag2) {
        this.companionTag = companionTag2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    private static int transformToXPosition(String str) {
        char c;
        if (!TextUtils.isEmpty(str)) {
            String lowerCase = str.toLowerCase();
            int hashCode = lowerCase.hashCode();
            if (hashCode == -1364013995) {
                if (lowerCase.equals(TtmlNode.CENTER)) {
                    c = 2;
                    switch (c) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                    }
                }
            } else if (hashCode == 3317767) {
                if (lowerCase.equals("left")) {
                    c = 0;
                    switch (c) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                    }
                }
            } else if (hashCode == 108511772 && lowerCase.equals("right")) {
                c = 1;
                switch (c) {
                    case 0:
                        return 9;
                    case 1:
                        return 11;
                    case 2:
                        return 14;
                    default:
                        return -1;
                }
            }
            c = 65535;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
            }
        } else {
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    private static int transformToYPosition(String str) {
        char c;
        if (!TextUtils.isEmpty(str)) {
            String lowerCase = str.toLowerCase();
            int hashCode = lowerCase.hashCode();
            if (hashCode == -1383228885) {
                if (lowerCase.equals(String.BOTTOM)) {
                    c = 1;
                    switch (c) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                    }
                }
            } else if (hashCode == -1364013995) {
                if (lowerCase.equals(TtmlNode.CENTER)) {
                    c = 2;
                    switch (c) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                    }
                }
            } else if (hashCode == 115029 && lowerCase.equals(String.TOP)) {
                c = 0;
                switch (c) {
                    case 0:
                        return 10;
                    case 1:
                        return 12;
                    case 2:
                        return 15;
                    default:
                        return -1;
                }
            }
            c = 65535;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
            }
        } else {
            return -1;
        }
    }
}
