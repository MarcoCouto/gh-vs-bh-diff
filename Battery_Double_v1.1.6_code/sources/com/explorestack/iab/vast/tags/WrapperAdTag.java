package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class WrapperAdTag extends AdContentTag {
    private static final String[] supportedAttributes = {"followAdditionalWrappers", "allowMultipleAds", "fallbackOnNoAd"};
    private String vastAdTagUri;

    public WrapperAdTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
        xmlPullParser.require(2, null, "Wrapper");
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                String name = xmlPullParser.getName();
                if (isEquals(name, "Creatives")) {
                    setCreativeTagList(parseCreatives(xmlPullParser));
                } else if (isEquals(name, "Extensions")) {
                    setExtensionTagList(parseExtensions(xmlPullParser));
                } else if (isEquals(name, "Impression")) {
                    addImpressionUrl(parseText(xmlPullParser));
                } else if (isEquals(name, "Error")) {
                    addErrorUrl(parseText(xmlPullParser));
                } else if (isEquals(name, "AdSystem")) {
                    setAdSystemTag(new AdSystemTag(xmlPullParser));
                } else if (isEquals(name, VastTagName.VAST_AD_TAG_URI)) {
                    setVastAdTagUri(parseText(xmlPullParser));
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, "Wrapper");
    }

    public String getVastAdTagUri() {
        return this.vastAdTagUri;
    }

    private void setVastAdTagUri(String str) {
        this.vastAdTagUri = str;
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
