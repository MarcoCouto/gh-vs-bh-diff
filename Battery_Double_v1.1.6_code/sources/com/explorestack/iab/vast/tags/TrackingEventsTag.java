package com.explorestack.iab.vast.tags;

import com.explorestack.iab.vast.TrackingEvent;
import com.explorestack.iab.vast.VastLog;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

class TrackingEventsTag extends VastXmlTag {
    private final EnumMap<TrackingEvent, List<String>> trackingEventListMap = new EnumMap<>(TrackingEvent.class);

    TrackingEventsTag(XmlPullParser xmlPullParser) throws Exception {
        Enum enumR;
        super(xmlPullParser);
        xmlPullParser.require(2, null, "TrackingEvents");
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() == 2) {
                if (isEquals(xmlPullParser.getName(), "Tracking")) {
                    String attributeValueByName = new TrackingTag(xmlPullParser).getAttributeValueByName("event");
                    try {
                        enumR = TrackingEvent.valueOf(attributeValueByName);
                    } catch (Exception unused) {
                        VastLog.d("VastXmlTag", String.format("Event: %s is not valid. Skipping it.", new Object[]{attributeValueByName}));
                        enumR = null;
                    }
                    if (enumR != null) {
                        String parseText = parseText(xmlPullParser);
                        List list = (List) this.trackingEventListMap.get(enumR);
                        if (list != null) {
                            list.add(parseText);
                        } else {
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(parseText);
                            this.trackingEventListMap.put(enumR, arrayList);
                        }
                    } else {
                        skip(xmlPullParser);
                    }
                } else {
                    skip(xmlPullParser);
                }
            }
        }
        xmlPullParser.require(3, null, "TrackingEvents");
    }

    /* access modifiers changed from: 0000 */
    public EnumMap<TrackingEvent, List<String>> getTrackingEventListMap() {
        return this.trackingEventListMap;
    }
}
