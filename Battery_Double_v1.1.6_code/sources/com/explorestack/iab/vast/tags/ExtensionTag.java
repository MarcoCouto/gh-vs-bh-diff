package com.explorestack.iab.vast.tags;

import org.xmlpull.v1.XmlPullParser;

public class ExtensionTag extends VastXmlTag {
    private static final String[] supportedAttributes = {"type"};

    ExtensionTag(XmlPullParser xmlPullParser) throws Exception {
        super(xmlPullParser);
    }

    public String[] getSupportedAttributes() {
        return supportedAttributes;
    }
}
