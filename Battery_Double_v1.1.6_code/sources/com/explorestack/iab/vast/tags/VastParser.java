package com.explorestack.iab.vast.tags;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Xml;
import com.explorestack.iab.vast.VastLog;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.xmlpull.v1.XmlPullParser;

public class VastParser {
    private static final String TAG = "VastXmlParser";

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0045 A[SYNTHETIC, Splitter:B:23:0x0045] */
    public static VastTag parseVast(@Nullable String str) throws Exception {
        if (!TextUtils.isEmpty(str)) {
            InputStream inputStream = null;
            try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes(Charset.defaultCharset().name()));
                try {
                    VastTag parseVast = parseVast((InputStream) byteArrayInputStream);
                    try {
                        byteArrayInputStream.close();
                    } catch (Exception e) {
                        VastLog.e(TAG, e.getMessage(), e);
                    }
                    return parseVast;
                } catch (Exception e2) {
                    e = e2;
                    inputStream = byteArrayInputStream;
                    try {
                        VastLog.e(TAG, e.getMessage(), e);
                        throw new Exception();
                    } catch (Throwable th) {
                        th = th;
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (Exception e3) {
                                VastLog.e(TAG, e3.getMessage(), e3);
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    inputStream = byteArrayInputStream;
                    if (inputStream != null) {
                    }
                    throw th;
                }
            } catch (Exception e4) {
                e = e4;
                VastLog.e(TAG, e.getMessage(), e);
                throw new Exception();
            }
        } else {
            throw new Exception();
        }
    }

    public static VastTag parseVast(InputStream inputStream) throws Exception {
        if (inputStream != null) {
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
            newPullParser.setInput(inputStream, null);
            int next = newPullParser.next();
            if (next != 1) {
                while (next != 3 && next != 1) {
                    if (newPullParser.getEventType() != 2) {
                        next = newPullParser.next();
                    } else if (VastXmlTag.isEquals(newPullParser.getName(), "VAST")) {
                        return new VastTag(newPullParser);
                    } else {
                        next = newPullParser.next();
                    }
                }
                return null;
            }
            throw new Exception();
        }
        throw new Exception();
    }
}
