package com.explorestack.iab.vast.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.ViewCompat;
import com.explorestack.iab.utils.Utils;
import com.explorestack.iab.vast.VastActivityListener;
import com.explorestack.iab.vast.VastClickCallback;
import com.explorestack.iab.vast.VastLog;
import com.explorestack.iab.vast.VastRequest;
import com.explorestack.iab.vast.activity.VastView.VastViewListener;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class VastActivity extends Activity {
    private static final String EXTRA_VAST_REQUEST = "com.explorestack.iab.vast.REQUEST";
    private static final String SAVED_IS_FINISHED_PERFORMED = "isFinishedPerformed";
    private static final String SAVED_IS_LOAD_PERFORMED = "isLoadPerformed";
    private static final int VAST_VIEW_ID = 1;
    @VisibleForTesting
    static final Map<String, WeakReference<VastActivityListener>> activityListenersReferences = new HashMap();
    private boolean isFinishPerformed;
    private boolean isLoadPerformed;
    /* access modifiers changed from: private */
    @Nullable
    public VastActivityListener listener;
    @Nullable
    private VastRequest vastRequest;
    @Nullable
    private VastView vastView;
    private final VastViewListener vastViewListener = new VastViewListener() {
        public void onOrientationRequested(@NonNull VastView vastView, @NonNull VastRequest vastRequest, int i) {
            VastActivity.this.setRequestedOrientation(VastActivity.this.toActivityOrientation(i));
        }

        public void onShown(@NonNull VastView vastView, @NonNull VastRequest vastRequest) {
            if (VastActivity.this.listener != null) {
                VastActivity.this.listener.onVastShown(VastActivity.this, vastRequest);
            }
        }

        public void onFinish(@NonNull VastView vastView, @NonNull VastRequest vastRequest, boolean z) {
            VastActivity.this.handleFinish(vastRequest, z);
        }

        public void onError(@NonNull VastView vastView, @Nullable VastRequest vastRequest, int i) {
            VastActivity.this.notifyError(vastRequest, i);
        }

        public void onClick(@NonNull VastView vastView, @NonNull VastRequest vastRequest, @NonNull VastClickCallback vastClickCallback, String str) {
            if (VastActivity.this.listener != null) {
                VastActivity.this.listener.onVastClick(VastActivity.this, vastRequest, vastClickCallback, str);
            }
        }

        public void onComplete(@NonNull VastView vastView, @NonNull VastRequest vastRequest) {
            if (VastActivity.this.listener != null) {
                VastActivity.this.listener.onVastComplete(VastActivity.this, vastRequest);
            }
        }
    };

    public static class Builder {
        @Nullable
        private VastActivityListener listener;
        @Nullable
        private VastRequest vastRequest;

        public Builder setRequest(@NonNull VastRequest vastRequest2) {
            this.vastRequest = vastRequest2;
            return this;
        }

        public Builder setListener(@Nullable VastActivityListener vastActivityListener) {
            this.listener = vastActivityListener;
            return this;
        }

        public boolean display(Context context) {
            if (this.vastRequest == null) {
                VastLog.e("VastRequest not provided");
            } else {
                try {
                    Intent intent = new Intent(context, VastActivity.class);
                    intent.putExtra(VastActivity.EXTRA_VAST_REQUEST, this.vastRequest);
                    if (this.listener != null) {
                        VastActivity.addListener(this.vastRequest, this.listener);
                    }
                    context.startActivity(intent);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    VastActivity.removeListener(this.vastRequest);
                }
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    public int toActivityOrientation(int i) {
        switch (i) {
            case 0:
                return -1;
            case 1:
                return 7;
            default:
                return 6;
        }
    }

    @Nullable
    private static VastActivityListener getListener(@NonNull VastRequest vastRequest2) {
        WeakReference weakReference = (WeakReference) activityListenersReferences.get(vastRequest2.getHash());
        if (weakReference != null && weakReference.get() != null) {
            return (VastActivityListener) weakReference.get();
        }
        activityListenersReferences.remove(vastRequest2.getHash());
        return null;
    }

    /* access modifiers changed from: private */
    public static void addListener(@NonNull VastRequest vastRequest2, @NonNull VastActivityListener vastActivityListener) {
        activityListenersReferences.put(vastRequest2.getHash(), new WeakReference(vastActivityListener));
    }

    /* access modifiers changed from: private */
    public static void removeListener(@NonNull VastRequest vastRequest2) {
        activityListenersReferences.remove(vastRequest2.getHash());
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        getWindow().setBackgroundDrawable(new ColorDrawable(ViewCompat.MEASURED_STATE_MASK));
        super.onCreate(bundle);
        this.vastRequest = (VastRequest) getIntent().getParcelableExtra(EXTRA_VAST_REQUEST);
        if (bundle != null && bundle.getBoolean(SAVED_IS_FINISHED_PERFORMED)) {
            finish();
        } else if (this.vastRequest == null) {
            notifyError(null, 405);
            handleFinish(null, false);
        } else {
            if (bundle == null) {
                int preferredVideoOrientation = this.vastRequest.getPreferredVideoOrientation();
                if (!(preferredVideoOrientation == 0 || preferredVideoOrientation == getResources().getConfiguration().orientation)) {
                    setRequestedOrientation(toActivityOrientation(preferredVideoOrientation));
                    try {
                        if ((getPackageManager().getActivityInfo(getComponentName(), 65536).configChanges & 128) == 0) {
                            return;
                        }
                    } catch (NameNotFoundException unused) {
                    }
                }
            }
            this.listener = getListener(this.vastRequest);
            this.vastView = new VastView(this);
            this.vastView.setId(1);
            this.vastView.setListener(this.vastViewListener);
            if (bundle == null || !bundle.getBoolean(SAVED_IS_LOAD_PERFORMED)) {
                this.isLoadPerformed = true;
                if (this.vastView.display(this.vastRequest)) {
                    Utils.applyFullscreenActivityFlags(this);
                    setContentView(this.vastView);
                }
            } else {
                Utils.applyFullscreenActivityFlags(this);
                setContentView(this.vastView);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean(SAVED_IS_LOAD_PERFORMED, this.isLoadPerformed);
        bundle.putBoolean(SAVED_IS_FINISHED_PERFORMED, this.isFinishPerformed);
    }

    public void onBackPressed() {
        if (this.vastView != null) {
            this.vastView.handleBackPress();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!isChangingConfigurations() && this.vastRequest != null) {
            removeListener(this.vastRequest);
        }
    }

    /* access modifiers changed from: private */
    public void notifyError(@Nullable VastRequest vastRequest2, int i) {
        if (this.listener != null) {
            this.listener.onVastError(this, vastRequest2, i);
        }
    }

    /* access modifiers changed from: private */
    public void handleFinish(@Nullable VastRequest vastRequest2, boolean z) {
        if (this.listener != null) {
            this.listener.onVastDismiss(this, vastRequest2, z);
        }
        this.isFinishPerformed = true;
        try {
            getWindow().clearFlags(128);
        } catch (Exception e) {
            VastLog.e(e.getMessage());
        }
        if (vastRequest2 != null) {
            setRequestedOrientation(toActivityOrientation(vastRequest2.getRequestedOrientation()));
        }
        finish();
    }
}
