package com.explorestack.iab.vast;

import android.support.annotation.NonNull;
import android.util.Pair;
import com.explorestack.iab.vast.tags.CompanionTag;

public interface VastExtension {
    int getAssetsBackgroundColor();

    int getAssetsColor();

    @NonNull
    Pair<Integer, Integer> getClosePosition();

    int getCompanionCloseTime();

    CompanionTag getCompanionTag();

    Pair<Integer, Integer> getCtaPosition();

    String getCtaText();

    Pair<Integer, Integer> getMutePosition();

    boolean isShowCta();

    boolean isShowMute();

    boolean isShowProgress();

    boolean isVideoClickable();
}
