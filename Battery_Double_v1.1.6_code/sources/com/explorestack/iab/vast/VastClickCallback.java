package com.explorestack.iab.vast;

public interface VastClickCallback {
    void clickHandleCanceled();

    void clickHandleError();

    void clickHandled();
}
