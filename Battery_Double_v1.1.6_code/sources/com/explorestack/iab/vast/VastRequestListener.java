package com.explorestack.iab.vast;

import android.support.annotation.NonNull;

public interface VastRequestListener extends VastErrorListener {
    void onVastLoaded(@NonNull VastRequest vastRequest);
}
