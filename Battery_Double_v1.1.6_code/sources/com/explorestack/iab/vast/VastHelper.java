package com.explorestack.iab.vast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import java.util.WeakHashMap;

public class VastHelper {
    private static boolean isInitialized = false;
    /* access modifiers changed from: private */
    public static boolean isScreenOn = false;
    private static final IntentFilter screenStateChangeFilter = new IntentFilter();
    @VisibleForTesting
    static final WeakHashMap<View, OnScreenStateChangeListener> screenStateChangeListeners = new WeakHashMap<>();
    private static final BroadcastReceiver screenStateChangeReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            synchronized (VastHelper.class) {
                VastHelper.isScreenOn = "android.intent.action.SCREEN_ON".equals(intent.getAction());
            }
            synchronized (VastHelper.screenStateChangeListeners) {
                for (OnScreenStateChangeListener onScreenStateChange : VastHelper.screenStateChangeListeners.values()) {
                    onScreenStateChange.onScreenStateChange(VastHelper.isScreenOn);
                }
            }
        }
    };

    public interface OnScreenStateChangeListener {
        void onScreenStateChange(boolean z);
    }

    static {
        screenStateChangeFilter.addAction("android.intent.action.SCREEN_ON");
        screenStateChangeFilter.addAction("android.intent.action.SCREEN_OFF");
    }

    private static synchronized void initialize(@NonNull Context context) {
        synchronized (VastHelper.class) {
            if (!isInitialized) {
                synchronized (VastHelper.class) {
                    if (!isInitialized) {
                        isScreenOn = ((PowerManager) context.getSystemService("power")).isScreenOn();
                        context.getApplicationContext().registerReceiver(screenStateChangeReceiver, screenStateChangeFilter);
                        isInitialized = true;
                    }
                }
            }
        }
    }

    public static void addScreenStateChangeListener(@NonNull View view, @NonNull OnScreenStateChangeListener onScreenStateChangeListener) {
        initialize(view.getContext());
        synchronized (screenStateChangeListeners) {
            screenStateChangeListeners.put(view, onScreenStateChangeListener);
        }
    }

    public static void removeScreenStateChangeListener(@NonNull View view) {
        if (isInitialized) {
            synchronized (screenStateChangeListeners) {
                screenStateChangeListeners.remove(view);
            }
        }
    }

    public static boolean isScreenOn(Context context) {
        initialize(context);
        return isScreenOn;
    }
}
