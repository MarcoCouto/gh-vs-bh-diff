package com.explorestack.iab;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.explorestack.iab";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 100100007;
    public static final String VERSION_NAME = "0.5.6";
}
