package com.smaato.sdk.flow;

import com.smaato.sdk.flow.Flow.Emitter;

public abstract class Subject<T> extends Flow<T> implements Emitter<T> {
}
