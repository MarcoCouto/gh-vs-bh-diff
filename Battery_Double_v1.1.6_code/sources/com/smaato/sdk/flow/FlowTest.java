package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class FlowTest<T> {
    private Subscriber<T> a = new Subscriber<T>(null) {
        public void onSubscribe(@NonNull Subscription subscription) {
            if (Subscriptions.a(FlowTest.this.b, subscription)) {
                if (null != null) {
                    null.onSubscribe(subscription);
                    return;
                }
                subscription.request(Long.MAX_VALUE);
            }
        }

        public void onNext(@NonNull T t) {
            FlowTest.this.c.add(t);
            if (null != null) {
                null.onNext(t);
            }
        }

        public void onError(@NonNull Throwable th) {
            FlowTest.a(FlowTest.this, th);
            if (null != null) {
                null.onError(th);
            }
        }

        public void onComplete() {
            FlowTest.b(FlowTest.this);
            if (null != null) {
                null.onComplete();
            }
        }
    };
    /* access modifiers changed from: private */
    public final AtomicReference<Subscription> b = new AtomicReference<>();
    private final List<T> c = new CopyOnWriteArrayList();
    private final List<Throwable> d = new CopyOnWriteArrayList();
    private final AtomicLong e = new AtomicLong();
    private final CountDownLatch f = new CountDownLatch(1);

    private FlowTest(@Nullable Subscriber<? super T> subscriber) {
    }

    @NonNull
    public static <T> FlowTest<T> apply(@NonNull Publisher<T> publisher) {
        FlowTest<T> flowTest = new FlowTest<>(null);
        publisher.subscribe(flowTest.a);
        return flowTest;
    }

    @NonNull
    public final FlowTest<T> await(long j, @NonNull TimeUnit timeUnit) throws InterruptedException {
        if (this.f.getCount() == 0) {
            return this;
        }
        this.f.await(j, timeUnit);
        return this;
    }

    @NonNull
    public final FlowTest<T> cancel() {
        Subscriptions.a(this.b);
        return this;
    }

    @NonNull
    public final FlowTest<T> assertComplete() {
        long j = this.e.get();
        if (j == 0) {
            throw a("Not completed");
        } else if (j <= 1) {
            return this;
        } else {
            StringBuilder sb = new StringBuilder("Multiple completions: ");
            sb.append(j);
            throw a(sb.toString());
        }
    }

    @NonNull
    public final FlowTest<T> assertNotComplete() {
        long j = this.e.get();
        if (j == 1) {
            throw a("Completed!");
        } else if (j <= 1) {
            return this;
        } else {
            StringBuilder sb = new StringBuilder("Multiple completions: ");
            sb.append(j);
            throw a(sb.toString());
        }
    }

    @NonNull
    public final FlowTest<T> assertNoErrors() {
        if (this.d.isEmpty()) {
            return this;
        }
        StringBuilder sb = new StringBuilder("Error(s) present: ");
        sb.append(this.d);
        throw a(sb.toString());
    }

    @NonNull
    public final FlowTest<T> assertHasErrors() {
        if (this.d.isEmpty()) {
            throw a("Has no errors");
        } else if (this.d.size() <= 1) {
            return this;
        } else {
            StringBuilder sb = new StringBuilder("Has multiple errors: ");
            sb.append(this.d.size());
            throw a(sb.toString());
        }
    }

    @NonNull
    public final List<T> values() {
        return Collections.unmodifiableList(this.c);
    }

    @Nullable
    public final Throwable error() {
        if (this.d.isEmpty()) {
            return null;
        }
        return (Throwable) this.d.get(0);
    }

    private AssertionError a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" (latch = ");
        sb.append(this.f.getCount());
        sb.append(", values = ");
        sb.append(this.c.size());
        sb.append(", errors = ");
        sb.append(this.d.size());
        sb.append(", completions = ");
        sb.append(this.e);
        sb.append(")");
        AssertionError assertionError = new AssertionError(sb.toString());
        if (!this.d.isEmpty() && this.d.size() == 1) {
            assertionError.initCause((Throwable) this.d.get(0));
        }
        return assertionError;
    }

    static /* synthetic */ void a(FlowTest flowTest, Throwable th) {
        flowTest.d.add(th);
        flowTest.f.countDown();
    }

    static /* synthetic */ void b(FlowTest flowTest) {
        flowTest.e.incrementAndGet();
        flowTest.f.countDown();
    }
}
