package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import java.util.Queue;

abstract class Exceptions {
    private Exceptions() {
    }

    @NonNull
    static Exception a(Queue<?> queue) {
        StringBuilder sb = new StringBuilder("Backpressure! Queue{size=");
        sb.append(queue.size());
        sb.append("}");
        return new IllegalStateException(sb.toString());
    }

    static void a(@NonNull Throwable th) {
        if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }
}
