package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import java.util.concurrent.atomic.AtomicReference;

class OpMap<T, U> extends Flow<U> {
    private final Publisher<T> a;
    private final Function<? super T, ? extends U> b;

    private static class MapSubscriber<T, U> extends SubscriptionArbiter implements Subscriber<T>, Subscription {
        private final AtomicReference<Subscription> a = new AtomicReference<>();
        private final Subscriber<? super U> b;
        private final Function<? super T, ? extends U> c;
        private volatile boolean d;

        MapSubscriber(Subscriber<? super U> subscriber, Function<? super T, ? extends U> function) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.b = subscriber;
            this.c = function;
        }

        public void onSubscribe(Subscription subscription) {
            if (Subscriptions.a(this.a, subscription)) {
                this.b.onSubscribe(this);
            }
        }

        public void onNext(T t) {
            if (!this.d && !b()) {
                try {
                    this.b.onNext(Objects.requireNonNull(this.c.apply(t), "The mapper returned a null value"));
                    a(1);
                } catch (Throwable th) {
                    this.b.onError(th);
                    Subscriptions.a(this.a);
                    cancel();
                }
            }
        }

        public void onError(Throwable th) {
            if (!this.d && !b()) {
                this.b.onError(th);
                this.d = true;
            }
        }

        public void onComplete() {
            if (!this.d && !b()) {
                this.b.onComplete();
                this.d = true;
            }
        }

        /* access modifiers changed from: protected */
        public void onRequested(long j) {
            ((Subscription) this.a.get()).request(j);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            Subscriptions.a(this.a);
        }
    }

    OpMap(Publisher<T> publisher, Function<? super T, ? extends U> function) {
        this.a = publisher;
        this.b = function;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super U> subscriber) {
        this.a.subscribe(new MapSubscriber(subscriber, this.b));
    }
}
