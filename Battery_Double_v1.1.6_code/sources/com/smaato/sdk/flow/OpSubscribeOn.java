package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

class OpSubscribeOn<T> extends Flow<T> {
    private final Publisher<T> a;
    private final Executor b;

    private static class SubscribeOnSubscriber<T> extends SubscriptionArbiter implements Subscriber<T>, Subscription {
        private final AtomicReference<Subscription> a = new AtomicReference<>();
        private final Subscriber<? super T> b;

        SubscribeOnSubscriber(Subscriber<? super T> subscriber) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.b = subscriber;
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            if (Subscriptions.a(this.a, subscription)) {
                long a2 = a();
                if (a2 != 0) {
                    subscription.request(a2);
                }
            }
        }

        public void onNext(@NonNull T t) {
            this.b.onNext(t);
        }

        public void onError(@NonNull Throwable th) {
            this.b.onError(th);
        }

        public void onComplete() {
            this.b.onComplete();
        }

        /* access modifiers changed from: protected */
        public void onRequested(long j) {
            Subscription subscription = (Subscription) this.a.get();
            if (subscription != null) {
                subscription.request(j);
            }
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            Subscriptions.a(this.a);
        }
    }

    OpSubscribeOn(Publisher<T> publisher, Executor executor) {
        this.a = publisher;
        this.b = executor;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        SubscribeOnSubscriber subscribeOnSubscriber = new SubscribeOnSubscriber(subscriber);
        subscriber.onSubscribe(subscribeOnSubscriber);
        try {
            this.b.execute(new Runnable(subscribeOnSubscriber) {
                private final /* synthetic */ SubscribeOnSubscriber f$1;

                {
                    this.f$1 = r2;
                }

                public final void run() {
                    OpSubscribeOn.this.a(this.f$1);
                }
            });
        } catch (Throwable th) {
            Exceptions.a(th);
            subscriber.onError(th);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(SubscribeOnSubscriber subscribeOnSubscriber) {
        this.a.subscribe(subscribeOnSubscriber);
    }
}
