package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;

final class BufferedSubscriber<T> implements Subscriber<T> {
    private final AtomicReference<Subscription> a = new AtomicReference<>();
    private final Queue<T> b = Subscriptions.a();
    private final Runnable c;
    private final Consumer<Throwable> d;
    private volatile boolean e;
    private volatile boolean f;

    BufferedSubscriber(Runnable runnable, Consumer<Throwable> consumer) {
        this.c = runnable;
        this.d = consumer;
    }

    public final void onSubscribe(@NonNull Subscription subscription) {
        if (Subscriptions.a(this.a, subscription)) {
            this.c.run();
        }
    }

    public final void onNext(@NonNull T t) {
        if (!this.e && !this.f) {
            if (!this.b.offer(t)) {
                try {
                    this.d.accept(Exceptions.a(this.b));
                } catch (Throwable th) {
                    Exceptions.a(th);
                }
            } else {
                this.c.run();
            }
        }
    }

    public final void onError(@NonNull Throwable th) {
        if (!this.e && !this.f) {
            try {
                this.d.accept(th);
            } catch (Throwable th2) {
                Exceptions.a(th2);
            }
        }
    }

    public final void onComplete() {
        if (!this.e && !this.f) {
            this.e = true;
            this.c.run();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j) {
        Subscription subscription = (Subscription) this.a.get();
        if (subscription != null) {
            subscription.request(j);
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.b.isEmpty();
    }

    /* access modifiers changed from: 0000 */
    public final T b() {
        return this.b.poll();
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        return !this.f && !this.e;
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        if (!this.f) {
            this.f = true;
            Subscriptions.a(this.a);
            this.b.clear();
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("BufferedSubscriber{buffer=");
        sb.append(this.b);
        sb.append(", done=");
        sb.append(this.e);
        sb.append(", cancelled=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
}
