package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.flow.Flow.Emitter;
import java.util.Queue;

class OpCreate<T> extends Flow<T> {
    private final Consumer<Emitter<? super T>> a;

    private static class BufferedEmitter<T> extends SubscriptionArbiter implements Emitter<T>, Subscription {
        private final Queue<T> a = Subscriptions.a();
        private final Subscriber<? super T> b;
        private volatile Throwable c;
        private volatile boolean d;

        BufferedEmitter(Subscriber<? super T> subscriber) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.b = subscriber;
        }

        public void onNext(@NonNull T t) {
            if (!this.d && !b()) {
                if (t == null) {
                    onError(new NullPointerException("onNext called with null"));
                } else if (!this.a.offer(t)) {
                    onError(Exceptions.a(this.a));
                } else {
                    drain();
                }
            }
        }

        public void onError(@NonNull Throwable th) {
            if (!this.d && !b()) {
                if (th == null) {
                    th = new NullPointerException("onError called with null");
                }
                this.c = th;
                this.d = true;
                drain();
            }
        }

        public void onComplete() {
            if (!this.d && !b()) {
                this.d = true;
                drain();
            }
        }

        /* access modifiers changed from: protected */
        public boolean drainLoop(long j) {
            long j2 = 0;
            while (j2 != j && !this.a.isEmpty()) {
                if (b()) {
                    this.a.clear();
                    return false;
                }
                this.b.onNext(this.a.poll());
                j2++;
            }
            if (!this.d || b() || !this.a.isEmpty()) {
                a(j2);
                return true;
            }
            if (this.c != null) {
                this.b.onError(this.c);
            } else {
                this.b.onComplete();
            }
            return false;
        }
    }

    OpCreate(Consumer<Emitter<? super T>> consumer) {
        this.a = consumer;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        BufferedEmitter bufferedEmitter = new BufferedEmitter(subscriber);
        subscriber.onSubscribe(bufferedEmitter);
        try {
            this.a.accept(bufferedEmitter);
        } catch (Throwable th) {
            Exceptions.a(th);
            subscriber.onError(th);
        }
    }
}
