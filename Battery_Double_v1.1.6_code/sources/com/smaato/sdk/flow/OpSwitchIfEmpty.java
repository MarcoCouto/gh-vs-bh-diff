package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

class OpSwitchIfEmpty<T> extends Flow<T> {
    private final Publisher<T> a;
    private final Callable<? extends Flow<? extends T>> b;

    private static class SwitchIfEmptySubscriber<T> extends SubscriptionArbiter implements Subscriber<T>, Subscription {
        private final AtomicReference<Subscription> a = new AtomicReference<>();
        private final Subscriber<? super T> b;
        private final Callable<? extends Publisher<? extends T>> c;
        private volatile boolean d = true;

        SwitchIfEmptySubscriber(Subscriber<? super T> subscriber, Callable<? extends Publisher<? extends T>> callable) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.b = subscriber;
            this.c = callable;
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            Subscription subscription2 = (Subscription) this.a.get();
            if (subscription2 != null) {
                subscription2.cancel();
            }
            if (this.a.compareAndSet(subscription2, subscription)) {
                if (subscription2 == null) {
                    this.b.onSubscribe(this);
                    return;
                }
                long a2 = a();
                if (a2 != 0) {
                    subscription.request(a2);
                }
            }
        }

        public void onNext(@NonNull T t) {
            this.b.onNext(t);
            a(1);
            this.d = false;
        }

        public void onError(@NonNull Throwable th) {
            this.b.onError(th);
        }

        public void onComplete() {
            if (!this.d) {
                this.b.onComplete();
                return;
            }
            this.d = false;
            try {
                ((Publisher) Objects.requireNonNull(this.c.call(), "The producer returned a null Publisher")).subscribe(this);
            } catch (Throwable th) {
                Exceptions.a(th);
                Subscriptions.a(this.a);
                this.b.onError(th);
            }
        }

        /* access modifiers changed from: protected */
        public void onRequested(long j) {
            ((Subscription) this.a.get()).request(j);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            Subscriptions.a(this.a);
        }
    }

    OpSwitchIfEmpty(Publisher<T> publisher, Callable<? extends Flow<? extends T>> callable) {
        this.a = publisher;
        this.b = callable;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        this.a.subscribe(new SwitchIfEmptySubscriber(subscriber, this.b));
    }
}
