package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.FunctionUtils;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Flow<T> implements Publisher<T> {

    public interface Emitter<T> {
        void onComplete();

        void onError(@NonNull Throwable th);

        void onNext(@NonNull T t);
    }

    public interface Executors {

        public static class NamedFactory implements ThreadFactory {
            private final AtomicInteger a = new AtomicInteger();
            private final String b;
            private final int c;

            public NamedFactory(@NonNull String str, int i) {
                this.b = (String) Objects.requireNonNull(str);
                this.c = i;
            }

            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable);
                StringBuilder sb = new StringBuilder();
                sb.append(this.b);
                sb.append("-");
                sb.append(this.a.incrementAndGet());
                thread.setName(sb.toString());
                thread.setPriority(this.c);
                thread.setDaemon(true);
                return thread;
            }
        }

        @NonNull
        Executor io();

        @NonNull
        Executor main();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Throwable a(Throwable th) throws Exception {
        return th;
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(@NonNull Subscriber<? super T> subscriber);

    @NonNull
    public static <T> Flow<T> empty() {
        return new OpEmpty();
    }

    @NonNull
    public static <T> Flow<T> error(@NonNull Throwable th) {
        Objects.requireNonNull(th, "error is null");
        return new OpError(new Callable(th) {
            private final /* synthetic */ Throwable f$0;

            {
                this.f$0 = r1;
            }

            public final Object call() {
                return Flow.a(this.f$0);
            }
        });
    }

    @NonNull
    public static <T> Flow<T> create(@NonNull Consumer<Emitter<? super T>> consumer) {
        Objects.requireNonNull(consumer, "source is null");
        return new OpCreate(consumer);
    }

    @NonNull
    @SafeVarargs
    public static <T> Flow<T> fromArray(@NonNull T... tArr) {
        Objects.requireNonNull(tArr, "array is null");
        if (tArr.length == 0) {
            return empty();
        }
        if (tArr.length == 1) {
            return single(new Callable(tArr) {
                private final /* synthetic */ Object[] f$0;

                {
                    this.f$0 = r1;
                }

                public final Object call() {
                    return Flow.a(this.f$0);
                }
            });
        }
        return new OpFromArray(tArr);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Object a(Object[] objArr) throws Exception {
        return objArr[0];
    }

    @NonNull
    public static <T> Flow<T> fromIterable(@NonNull Iterable<T> iterable) {
        Objects.requireNonNull(iterable, "iterable is null");
        return new OpFromIterable(iterable);
    }

    @NonNull
    public static <T> Flow<T> fromAction(@NonNull Runnable runnable) {
        Objects.requireNonNull(runnable, "action is null");
        return new OpFromAction(runnable);
    }

    @NonNull
    @SafeVarargs
    public static <T> Flow<T> concatArray(@NonNull Publisher<? extends T>... publisherArr) {
        Objects.requireNonNull(publisherArr, "sources is null");
        return new OpConcatArray(publisherArr);
    }

    @NonNull
    public static <T> Flow<T> maybe(@NonNull Callable<T> callable) {
        Objects.requireNonNull(callable, "producer is null");
        return new OpMaybe(callable);
    }

    @NonNull
    public static <T> Flow<T> single(@NonNull Callable<T> callable) {
        Objects.requireNonNull(callable, "producer is null");
        return new OpSingle(callable);
    }

    @NonNull
    public static <T> Subject<T> subject() {
        return new OpSubject();
    }

    @NonNull
    public <U> Flow<U> map(@NonNull Function<? super T, ? extends U> function) {
        Objects.requireNonNull(function, "mapper is null");
        return new OpMap(this, function);
    }

    @NonNull
    public <U> Flow<U> flatMap(@NonNull Function<? super T, ? extends Publisher<? extends U>> function) {
        Objects.requireNonNull(function, "mapper is null");
        return new OpFlatMap(this, function);
    }

    @NonNull
    public Flow<T> switchIfEmpty(@NonNull Callable<? extends Flow<? extends T>> callable) {
        Objects.requireNonNull(callable, "producer is null");
        return new OpSwitchIfEmpty(this, callable);
    }

    @NonNull
    public Flow<T> switchIfError(@NonNull Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        Objects.requireNonNull(function, "mapper is null");
        return new OpSwitchIfError(this, function);
    }

    @NonNull
    public Flow<T> concatWith(@NonNull Publisher<? extends T> publisher) {
        Objects.requireNonNull(publisher, "other is null");
        return concatArray(this, publisher);
    }

    @NonNull
    public Flow<T> subscribeOn(@NonNull Executor executor) {
        Objects.requireNonNull(executor, "executor is null");
        return new OpSubscribeOn(this, executor);
    }

    @NonNull
    public Flow<T> observeOn(@NonNull Executor executor) {
        Objects.requireNonNull(executor, "executor is null");
        return new OpObserveOn(this, executor);
    }

    @NonNull
    public Flow<T> doOnNext(@NonNull Consumer<T> consumer) {
        Objects.requireNonNull(consumer, "onNext is null");
        return new OpDoOnEach(this, consumer, FunctionUtils.emptyConsumer(), FunctionUtils.emptyAction());
    }

    @NonNull
    public Flow<T> doOnError(@NonNull Consumer<Throwable> consumer) {
        Objects.requireNonNull(consumer, "onError is null");
        return new OpDoOnEach(this, FunctionUtils.emptyConsumer(), consumer, FunctionUtils.emptyAction());
    }

    @NonNull
    public Flow<T> doOnComplete(@NonNull Runnable runnable) {
        Objects.requireNonNull(runnable, "onComplete is null");
        return new OpDoOnEach(this, FunctionUtils.emptyConsumer(), FunctionUtils.emptyConsumer(), runnable);
    }

    public void subscribe() {
        subscribe(FunctionUtils.emptyConsumer());
    }

    public void subscribe(@NonNull Consumer<T> consumer) {
        subscribe(consumer, FunctionUtils.emptyConsumer());
    }

    public void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2) {
        subscribe(consumer, consumer2, FunctionUtils.emptyAction());
    }

    public void subscribe(@NonNull Consumer<T> consumer, @NonNull Consumer<Throwable> consumer2, @NonNull Runnable runnable) {
        Objects.requireNonNull(consumer, "onNext is null");
        Objects.requireNonNull(consumer2, "onError is null");
        Objects.requireNonNull(runnable, "onComplete is null");
        subscribe((Subscriber<? super T>) new LambdaSubscriber<Object>(Subscriptions.b(), consumer, consumer2, runnable));
    }

    @NonNull
    public FlowTest<T> test() {
        return FlowTest.apply(this);
    }

    public final void subscribe(@NonNull Subscriber<? super T> subscriber) {
        Objects.requireNonNull(subscriber, "subscriber is null");
        try {
            a(subscriber);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable th) {
            Exceptions.a(th);
            subscriber.onError(th);
        }
    }
}
