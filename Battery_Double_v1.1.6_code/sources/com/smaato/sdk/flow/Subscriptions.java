package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

abstract class Subscriptions {
    static final Subscription a = new Subscription() {
        public final void cancel() {
        }

        public final void request(long j) {
        }
    };
    private static final Consumer<Subscription> b = $$Lambda$Subscriptions$6dLsqOgMkKs0BlLV7wvDgy_h8FY.INSTANCE;

    private Subscriptions() {
    }

    @NonNull
    static <U> Queue<U> a() {
        return new ConcurrentLinkedQueue();
    }

    static void a(long j) {
        if (j <= 0) {
            StringBuilder sb = new StringBuilder("§3.9 violated: request amount is negative [");
            sb.append(j);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    static boolean a(long j, @NonNull Consumer<Throwable> consumer) {
        if (j > 0) {
            return true;
        }
        try {
            StringBuilder sb = new StringBuilder("§3.9 violated: request amount is negative [");
            sb.append(j);
            sb.append(RequestParameters.RIGHT_BRACKETS);
            consumer.accept(new IllegalArgumentException(sb.toString()));
            return false;
        } catch (Throwable th) {
            throw new RuntimeException(th);
        }
    }

    static long a(@NonNull AtomicLong atomicLong, long j) {
        long j2;
        long j3;
        do {
            j2 = atomicLong.get();
            j3 = Long.MAX_VALUE;
            if (j2 == Long.MAX_VALUE) {
                return Long.MAX_VALUE;
            }
            long j4 = j2 + j;
            if (j4 >= 0) {
                j3 = j4;
            }
        } while (!atomicLong.compareAndSet(j2, j3));
        return j2;
    }

    static long b(@NonNull AtomicLong atomicLong, long j) {
        long j2;
        long j3;
        do {
            j2 = atomicLong.get();
            if (j2 == Long.MAX_VALUE) {
                return Long.MAX_VALUE;
            }
            j3 = j2 - j;
            if (j3 < 0) {
                StringBuilder sb = new StringBuilder("More produced than requested: ");
                sb.append(j3);
                throw new IllegalStateException(sb.toString());
            }
        } while (!atomicLong.compareAndSet(j2, j3));
        return j3;
    }

    static boolean a(@NonNull AtomicReference<Subscription> atomicReference, @NonNull Subscription subscription) {
        Objects.requireNonNull(atomicReference, "upstream is null");
        Objects.requireNonNull(subscription, "subscription is null");
        if (atomicReference.compareAndSet(null, subscription)) {
            return true;
        }
        subscription.cancel();
        return false;
    }

    static void a(@NonNull AtomicReference<Subscription> atomicReference) {
        Objects.requireNonNull(atomicReference, "upstream is null");
        if (((Subscription) atomicReference.get()) != a) {
            Subscription subscription = (Subscription) atomicReference.getAndSet(a);
            if (subscription != a && subscription != null) {
                subscription.cancel();
            }
        }
    }

    @NonNull
    static Consumer<Subscription> b() {
        return b;
    }
}
