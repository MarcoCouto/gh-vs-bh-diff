package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;

class OpDoOnEach<T> extends Flow<T> {
    private final Publisher<T> a;
    private final Consumer<T> b;
    private final Consumer<Throwable> c;
    private final Runnable d;

    private static final class DoOnEachSubscriber<T> implements Subscriber<T> {
        private final Subscriber<? super T> a;
        private final Consumer<T> b;
        private final Consumer<Throwable> c;
        private final Runnable d;

        DoOnEachSubscriber(Subscriber<? super T> subscriber, Consumer<T> consumer, Consumer<Throwable> consumer2, Runnable runnable) {
            this.a = subscriber;
            this.b = consumer;
            this.c = consumer2;
            this.d = runnable;
        }

        public final void onSubscribe(@NonNull Subscription subscription) {
            this.a.onSubscribe(subscription);
        }

        public final void onNext(@NonNull T t) {
            this.b.accept(t);
            this.a.onNext(t);
        }

        public final void onError(@NonNull Throwable th) {
            this.c.accept(th);
            this.a.onError(th);
        }

        public final void onComplete() {
            this.d.run();
            this.a.onComplete();
        }
    }

    OpDoOnEach(Publisher<T> publisher, Consumer<T> consumer, Consumer<Throwable> consumer2, Runnable runnable) {
        this.a = publisher;
        this.b = consumer;
        this.c = consumer2;
        this.d = runnable;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        this.a.subscribe(new DoOnEachSubscriber(subscriber, this.b, this.c, this.d));
    }
}
