package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

class OpSubject<T> extends Subject<T> {
    private final List<SubjectSubscription<T>> a = new CopyOnWriteArrayList();
    private volatile Throwable b;
    private volatile boolean c;

    private static class SubjectSubscription<T> extends AtomicLong implements Subscription {
        private final OpSubject<T> a;
        private final Subscriber<? super T> b;

        SubjectSubscription(OpSubject<T> opSubject, Subscriber<? super T> subscriber) {
            this.a = opSubject;
            this.b = subscriber;
        }

        /* access modifiers changed from: 0000 */
        public final void a(@NonNull T t) {
            if (get() != 0) {
                this.b.onNext(t);
                Subscriptions.b(this, 1);
            }
        }

        /* access modifiers changed from: 0000 */
        public final void a(@NonNull Throwable th) {
            this.b.onError(th);
        }

        /* access modifiers changed from: 0000 */
        public final void a() {
            this.b.onComplete();
        }

        public void request(long j) {
            Subscriber<? super T> subscriber = this.b;
            subscriber.getClass();
            if (Subscriptions.a(j, (Consumer<Throwable>) new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            })) {
                Subscriptions.a((AtomicLong) this, j);
            }
        }

        public void cancel() {
            this.a.a.remove(this);
        }
    }

    OpSubject() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        if (!this.c) {
            SubjectSubscription subjectSubscription = new SubjectSubscription(this, subscriber);
            if (this.a.add(subjectSubscription)) {
                subscriber.onSubscribe(subjectSubscription);
            }
        } else if (this.b != null) {
            OpError.a(subscriber, this.b);
        } else {
            OpEmpty.b(subscriber);
        }
    }

    public void onNext(@NonNull T t) {
        Objects.requireNonNull(t, "onNext called with null");
        for (SubjectSubscription a2 : this.a) {
            a2.a(t);
        }
    }

    public void onError(@NonNull Throwable th) {
        if (!this.c) {
            this.b = (Throwable) Objects.requireNonNull(th, "onError called with null");
            this.c = true;
            for (SubjectSubscription a2 : this.a) {
                a2.a(th);
            }
            this.a.clear();
            return;
        }
        throw new IllegalStateException(th);
    }

    public void onComplete() {
        if (!this.c) {
            this.c = true;
            for (SubjectSubscription a2 : this.a) {
                a2.a();
            }
            this.a.clear();
        }
    }
}
