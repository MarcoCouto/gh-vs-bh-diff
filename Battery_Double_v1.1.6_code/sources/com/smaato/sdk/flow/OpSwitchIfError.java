package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import java.util.concurrent.atomic.AtomicReference;

class OpSwitchIfError<T> extends Flow<T> {
    private final Publisher<T> a;
    private final Function<? super Throwable, ? extends Publisher<? extends T>> b;

    private static class SwitchIfErrorSubscriber<T> extends SubscriptionArbiter implements Subscriber<T>, Subscription {
        private final AtomicReference<Subscription> a = new AtomicReference<>();
        private final Subscriber<? super T> b;
        private final Function<? super Throwable, ? extends Publisher<? extends T>> c;

        SwitchIfErrorSubscriber(Subscriber<? super T> subscriber, Function<? super Throwable, ? extends Publisher<? extends T>> function) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.b = subscriber;
            this.c = function;
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            Subscription subscription2 = (Subscription) this.a.get();
            if (subscription2 != null) {
                subscription2.cancel();
            }
            if (this.a.compareAndSet(subscription2, subscription)) {
                if (subscription2 == null) {
                    this.b.onSubscribe(this);
                    return;
                }
                long a2 = a();
                if (a2 != 0) {
                    subscription.request(a2);
                }
            }
        }

        public void onNext(@NonNull T t) {
            this.b.onNext(t);
        }

        public void onError(@NonNull Throwable th) {
            try {
                ((Publisher) Objects.requireNonNull(this.c.apply(th), "The mapper returned a null Publisher")).subscribe(this);
            } catch (Throwable th2) {
                Exceptions.a(th2);
                Subscriptions.a(this.a);
                this.b.onError(th2);
            }
        }

        public void onComplete() {
            this.b.onComplete();
        }

        /* access modifiers changed from: protected */
        public void onRequested(long j) {
            ((Subscription) this.a.get()).request(j);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            Subscriptions.a(this.a);
        }
    }

    OpSwitchIfError(Publisher<T> publisher, Function<? super Throwable, ? extends Publisher<? extends T>> function) {
        this.a = publisher;
        this.b = function;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        this.a.subscribe(new SwitchIfErrorSubscriber(subscriber, this.b));
    }
}
