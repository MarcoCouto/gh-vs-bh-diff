package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;

class OpFlatMap<T, U> extends Flow<U> {
    private final Publisher<T> a;
    private final Function<? super T, ? extends Publisher<? extends U>> b;

    private static class FlatMapSubscriber<T, U> extends SubscriptionArbiter implements Subscriber<T>, Subscription {
        private final AtomicReference<Subscription> a = new AtomicReference<>();
        private final Queue<BufferedSubscriber<U>> b = Subscriptions.a();
        private final Subscriber<? super U> c;
        private final Function<? super T, ? extends Publisher<? extends U>> d;
        private volatile Throwable e;
        private volatile boolean f;

        FlatMapSubscriber(Subscriber<? super U> subscriber, Function<? super T, ? extends Publisher<? extends U>> function) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.c = subscriber;
            this.d = function;
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            if (Subscriptions.a(this.a, subscription)) {
                this.c.onSubscribe(this);
            }
        }

        public void onNext(@NonNull T t) {
            if (!this.f && !b()) {
                try {
                    Publisher publisher = (Publisher) Objects.requireNonNull(this.d.apply(t), "The mapper returned a null Publisher");
                    BufferedSubscriber bufferedSubscriber = new BufferedSubscriber(new Runnable() {
                        public final void run() {
                            FlatMapSubscriber.this.drain();
                        }
                    }, new Consumer() {
                        public final void accept(Object obj) {
                            FlatMapSubscriber.this.onError((Throwable) obj);
                        }
                    });
                    if (!this.b.offer(bufferedSubscriber)) {
                        onError(Exceptions.a(this.b));
                        Subscriptions.a(this.a);
                        return;
                    }
                    publisher.subscribe(bufferedSubscriber);
                } catch (Throwable th) {
                    Exceptions.a(th);
                    Subscriptions.a(this.a);
                    onError(th);
                }
            }
        }

        public void onError(@NonNull Throwable th) {
            if (!this.f && !b()) {
                this.f = true;
                this.e = th;
                drain();
            }
        }

        public void onComplete() {
            if (!this.f && !b()) {
                this.f = true;
                drain();
            }
        }

        /* access modifiers changed from: protected */
        public void onRequested(long j) {
            ((Subscription) this.a.get()).request(j);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            Subscriptions.a(this.a);
            c();
        }

        /* access modifiers changed from: protected */
        public boolean drainLoop(long j) {
            long j2 = 0;
            while (true) {
                if (j2 == j || b() || this.b.isEmpty()) {
                    break;
                }
                BufferedSubscriber bufferedSubscriber = (BufferedSubscriber) this.b.peek();
                while (j2 != j && !b() && !bufferedSubscriber.a()) {
                    this.c.onNext(bufferedSubscriber.b());
                    j2++;
                }
                a(j2);
                if (!b() && bufferedSubscriber.a()) {
                    if (bufferedSubscriber.c()) {
                        bufferedSubscriber.a(Math.max(1, j - j2));
                        break;
                    }
                    this.b.remove();
                }
            }
            if (b()) {
                c();
                return false;
            }
            if (this.f) {
                if (this.e != null) {
                    this.c.onError(this.e);
                    c();
                    return false;
                } else if (this.b.isEmpty()) {
                    this.c.onComplete();
                    return false;
                }
            }
            return true;
        }

        private void c() {
            while (!this.b.isEmpty()) {
                ((BufferedSubscriber) this.b.poll()).d();
            }
        }
    }

    OpFlatMap(Publisher<T> publisher, Function<? super T, ? extends Publisher<? extends U>> function) {
        this.a = publisher;
        this.b = function;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super U> subscriber) {
        this.a.subscribe(new FlatMapSubscriber(subscriber, this.b));
    }
}
