package com.smaato.sdk.flow;

import androidx.annotation.NonNull;

public interface Subscriber<T> {
    void onComplete();

    void onError(@NonNull Throwable th);

    void onNext(@NonNull T t);

    void onSubscribe(@NonNull Subscription subscription);
}
