package com.smaato.sdk.flow;

import androidx.annotation.NonNull;

public interface Publisher<T> {
    void subscribe(@NonNull Subscriber<? super T> subscriber);
}
