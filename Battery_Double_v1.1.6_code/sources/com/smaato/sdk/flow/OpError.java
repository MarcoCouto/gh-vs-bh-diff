package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import java.util.concurrent.Callable;

class OpError<T> extends Flow<T> {
    private final Callable<Throwable> a;

    OpError(Callable<Throwable> callable) {
        this.a = callable;
    }

    static <T> void a(@NonNull Subscriber<? super T> subscriber, @NonNull Throwable th) {
        subscriber.onSubscribe(OpEmpty.a);
        subscriber.onError(th);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        subscriber.onSubscribe(OpEmpty.a);
        try {
            subscriber.onError((Throwable) this.a.call());
        } catch (Throwable th) {
            Exceptions.a(th);
            subscriber.onError(th);
        }
    }
}
