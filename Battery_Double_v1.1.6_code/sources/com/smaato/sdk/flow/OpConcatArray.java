package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

class OpConcatArray<T> extends Flow<T> {
    private final Publisher<? extends T>[] a;

    private static class ConcatArraySubscriber<T> extends AtomicLong implements Subscriber<T>, Subscription {
        private final AtomicReference<Subscription> a = new AtomicReference<>();
        private final AtomicLong b = new AtomicLong();
        private final AtomicLong c = new AtomicLong();
        private final Subscriber<? super T> d;
        private final Publisher<? extends T>[] e;
        private volatile int f;
        private volatile Throwable g;

        ConcatArraySubscriber(Subscriber<? super T> subscriber, Publisher<? extends T>[] publisherArr) {
            this.d = subscriber;
            this.e = publisherArr;
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            if (this.a.compareAndSet((Subscription) this.a.get(), subscription)) {
                long j = get();
                if (j != 0) {
                    subscription.request(j);
                }
            }
        }

        public void onNext(@NonNull T t) {
            this.c.incrementAndGet();
            this.d.onNext(t);
        }

        public void onError(@NonNull Throwable th) {
            this.d.onError(th);
            this.g = th;
        }

        public void onComplete() {
            if (this.b.getAndIncrement() == 0) {
                int i = this.f;
                int length = this.e.length;
                while (i != length) {
                    Publisher<? extends T> publisher = this.e[i];
                    if (publisher == null) {
                        Subscriber<? super T> subscriber = this.d;
                        StringBuilder sb = new StringBuilder("The Publisher at index ");
                        sb.append(i);
                        sb.append(" is null");
                        subscriber.onError(new NullPointerException(sb.toString()));
                        return;
                    }
                    Subscriptions.b(this, this.c.getAndSet(0));
                    publisher.subscribe(this);
                    i++;
                    this.f = i;
                    if (this.b.decrementAndGet() == 0) {
                        return;
                    }
                }
                this.d.onComplete();
            }
        }

        public void request(long j) {
            Subscriber<? super T> subscriber = this.d;
            subscriber.getClass();
            if (Subscriptions.a(j, (Consumer<Throwable>) new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            })) {
                Subscriptions.a((AtomicLong) this, j);
                Subscription subscription = (Subscription) this.a.get();
                if (subscription != null) {
                    subscription.request(j);
                }
            }
        }

        public void cancel() {
            Subscriptions.a(this.a);
        }
    }

    OpConcatArray(Publisher<? extends T>[] publisherArr) {
        this.a = publisherArr;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        ConcatArraySubscriber concatArraySubscriber = new ConcatArraySubscriber(subscriber, this.a);
        subscriber.onSubscribe(concatArraySubscriber);
        concatArraySubscriber.onComplete();
    }
}
