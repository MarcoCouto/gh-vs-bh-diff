package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.atomic.AtomicReference;

class LambdaSubscriber<T> extends AtomicReference<Subscription> implements Subscriber<T>, Subscription {
    private final Consumer<? super Subscription> a;
    private final Consumer<? super T> b;
    private final Consumer<? super Throwable> c;
    private final Runnable d;

    LambdaSubscriber(Consumer<? super Subscription> consumer, Consumer<? super T> consumer2, Consumer<? super Throwable> consumer3, Runnable runnable) {
        this.a = consumer;
        this.b = consumer2;
        this.c = consumer3;
        this.d = runnable;
    }

    public void onSubscribe(@NonNull Subscription subscription) {
        if (Subscriptions.a((AtomicReference<Subscription>) this, subscription)) {
            try {
                this.a.accept(subscription);
            } catch (Throwable th) {
                Exceptions.a(th);
                subscription.cancel();
                onError(th);
            }
        }
    }

    public void onNext(@NonNull T t) {
        if (Subscriptions.a != get()) {
            try {
                this.b.accept(t);
            } catch (Throwable th) {
                Exceptions.a(th);
                ((Subscription) get()).cancel();
                onError(th);
            }
        }
    }

    public void onError(@NonNull Throwable th) {
        if (Subscriptions.a != get()) {
            lazySet(Subscriptions.a);
            this.c.accept(th);
        }
    }

    public void onComplete() {
        if (Subscriptions.a != get()) {
            lazySet(Subscriptions.a);
            this.d.run();
        }
    }

    public void request(long j) {
        if (Subscriptions.a(j, (Consumer<Throwable>) new Consumer() {
            public final void accept(Object obj) {
                LambdaSubscriber.this.onError((Throwable) obj);
            }
        })) {
            ((Subscription) get()).request(j);
        }
    }

    public void cancel() {
        Subscriptions.a((AtomicReference<Subscription>) this);
    }
}
