package com.smaato.sdk.flow;

import androidx.annotation.NonNull;

class OpEmpty<T> extends Flow<T> {
    static final Subscription a = new Subscription() {
        public final void cancel() {
        }

        public final void request(long j) {
            Subscriptions.a(j);
        }
    };

    OpEmpty() {
    }

    static <T> void b(@NonNull Subscriber<? super T> subscriber) {
        subscriber.onSubscribe(a);
        subscriber.onComplete();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        b(subscriber);
    }
}
