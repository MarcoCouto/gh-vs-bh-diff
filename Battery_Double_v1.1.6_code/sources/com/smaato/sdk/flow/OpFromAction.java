package com.smaato.sdk.flow;

import androidx.annotation.NonNull;

class OpFromAction<T> extends Flow<T> {
    private final Runnable a;

    OpFromAction(Runnable runnable) {
        this.a = runnable;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        subscriber.onSubscribe(OpEmpty.a);
        try {
            this.a.run();
            subscriber.onComplete();
        } catch (Throwable th) {
            Exceptions.a(th);
            subscriber.onError(th);
        }
    }
}
