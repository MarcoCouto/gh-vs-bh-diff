package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

class OpSingle<T> extends Flow<T> {
    private final Callable<T> a;

    private static class SingleSubscription<T> extends AtomicLong implements Subscription {
        private final Subscriber<? super T> a;
        private final Callable<T> b;

        public void cancel() {
        }

        SingleSubscription(Subscriber<? super T> subscriber, Callable<T> callable) {
            this.a = subscriber;
            this.b = callable;
        }

        public void request(long j) {
            Subscriber<? super T> subscriber = this.a;
            subscriber.getClass();
            if (Subscriptions.a(j, (Consumer<Throwable>) new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            }) && Subscriptions.a((AtomicLong) this, j) == 0) {
                try {
                    Object call = this.b.call();
                    if (call == null) {
                        this.a.onError(new NullPointerException("The value from producer is null"));
                    } else {
                        this.a.onNext(call);
                        this.a.onComplete();
                    }
                } catch (Throwable th) {
                    Exceptions.a(th);
                    this.a.onError(th);
                }
            }
        }
    }

    OpSingle(Callable<T> callable) {
        this.a = callable;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        subscriber.onSubscribe(new SingleSubscription(subscriber, this.a));
    }
}
