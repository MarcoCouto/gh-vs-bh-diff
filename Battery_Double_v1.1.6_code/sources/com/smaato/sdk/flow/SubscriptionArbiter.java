package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

abstract class SubscriptionArbiter implements Subscription {
    private final AtomicLong a = new AtomicLong();
    private final AtomicInteger b = new AtomicInteger();
    private final Consumer<Throwable> c;
    private volatile boolean d;

    /* access modifiers changed from: 0000 */
    public boolean drainLoop(long j) {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void onCancelled() {
    }

    /* access modifiers changed from: 0000 */
    public void onRequested(long j) {
    }

    SubscriptionArbiter(@NonNull Consumer<Throwable> consumer) {
        this.c = (Consumer) Objects.requireNonNull(consumer, "onError is null");
    }

    public final void request(long j) {
        if (Subscriptions.a(j, this.c)) {
            Subscriptions.a(this.a, j);
            onRequested(j);
            drain();
        }
    }

    public final void cancel() {
        if (!this.d) {
            this.d = true;
            onCancelled();
            drain();
        }
    }

    /* access modifiers changed from: 0000 */
    public void drain() {
        if (this.b.getAndIncrement() == 0) {
            int i = 1;
            while (drainLoop(this.a.get())) {
                i = this.b.addAndGet(-i);
                if (((long) i) == 0) {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final long a(long j) {
        return Subscriptions.b(this.a, j);
    }

    /* access modifiers changed from: 0000 */
    public final long a() {
        return this.a.get();
    }

    /* access modifiers changed from: 0000 */
    public final boolean b() {
        return this.d;
    }
}
