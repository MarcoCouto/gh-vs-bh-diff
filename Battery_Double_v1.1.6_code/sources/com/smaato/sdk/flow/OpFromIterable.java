package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.Iterator;

class OpFromIterable<T> extends Flow<T> {
    private final Iterable<T> a;

    private static class IteratorSubscription<T> extends SubscriptionArbiter implements Subscription {
        private final Subscriber<? super T> a;
        private final Iterator<T> b;

        IteratorSubscription(Subscriber<? super T> subscriber, Iterator<T> it) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.a = subscriber;
            this.b = it;
        }

        /* access modifiers changed from: 0000 */
        public boolean drainLoop(long j) {
            long j2 = 0;
            while (j2 != j && this.b.hasNext() && !b()) {
                try {
                    Object next = this.b.next();
                    if (next == null) {
                        this.a.onError(new NullPointerException("Iterator.next()returned a null value."));
                        return false;
                    }
                    this.a.onNext(next);
                    j2++;
                } catch (Throwable th) {
                    Exceptions.a(th);
                    this.a.onError(th);
                    return false;
                }
            }
            if (this.b.hasNext() || b()) {
                a(j2);
                return true;
            }
            this.a.onComplete();
            return false;
        }
    }

    OpFromIterable(Iterable<T> iterable) {
        this.a = iterable;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        try {
            Iterator it = this.a.iterator();
            try {
                if (!it.hasNext()) {
                    OpEmpty.b(subscriber);
                } else {
                    subscriber.onSubscribe(new IteratorSubscription(subscriber, it));
                }
            } catch (Throwable th) {
                Exceptions.a(th);
                OpError.a(subscriber, th);
            }
        } catch (Throwable th2) {
            Exceptions.a(th2);
            OpError.a(subscriber, th2);
        }
    }
}
