package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

class OpObserveOn<T> extends Flow<T> {
    private final Publisher<T> a;
    private final Executor b;

    private static class ObserveOnSubscriber<T> extends SubscriptionArbiter implements Subscriber<T>, Subscription, Runnable {
        private final AtomicReference<Subscription> a = new AtomicReference<>();
        private final Queue<T> b = Subscriptions.a();
        private final Subscriber<? super T> c;
        private final Executor d;
        private volatile Throwable e;
        private volatile boolean f;

        ObserveOnSubscriber(Subscriber<? super T> subscriber, Executor executor) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.c = subscriber;
            this.d = executor;
        }

        public void onSubscribe(@NonNull Subscription subscription) {
            if (Subscriptions.a(this.a, subscription)) {
                this.c.onSubscribe(this);
            }
        }

        public void onNext(@NonNull T t) {
            if (!this.b.offer(t)) {
                onError(Exceptions.a(this.b));
            } else {
                drain();
            }
        }

        public void onError(@NonNull Throwable th) {
            this.e = th;
            this.f = true;
            drain();
        }

        public void onComplete() {
            this.f = true;
            drain();
        }

        /* access modifiers changed from: protected */
        public void onRequested(long j) {
            ((Subscription) this.a.get()).request(j);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            Subscriptions.a(this.a);
        }

        /* access modifiers changed from: protected */
        public boolean drainLoop(long j) {
            for (long j2 = 0; j2 != j && !b() && !this.b.isEmpty(); j2++) {
                this.c.onNext(this.b.poll());
            }
            if (b()) {
                this.b.clear();
                return false;
            } else if (!this.f || !this.b.isEmpty()) {
                return true;
            } else {
                if (this.e != null) {
                    this.c.onError(this.e);
                } else {
                    this.c.onComplete();
                }
                return false;
            }
        }

        public void run() {
            super.drain();
        }

        /* access modifiers changed from: protected */
        public void drain() {
            this.d.execute(this);
        }
    }

    OpObserveOn(Publisher<T> publisher, Executor executor) {
        this.a = publisher;
        this.b = executor;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        this.a.subscribe(new ObserveOnSubscriber(subscriber, this.b));
    }
}
