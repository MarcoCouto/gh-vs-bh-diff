package com.smaato.sdk.flow;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;

class OpFromArray<T> extends Flow<T> {
    private final T[] a;

    private static class ArraySubscription<T> extends SubscriptionArbiter implements Subscription {
        private final Subscriber<? super T> a;
        private final T[] b;
        private volatile int c;

        ArraySubscription(Subscriber<? super T> subscriber, T[] tArr) {
            subscriber.getClass();
            super(new Consumer() {
                public final void accept(Object obj) {
                    Subscriber.this.onError((Throwable) obj);
                }
            });
            this.a = subscriber;
            this.b = tArr;
        }

        /* access modifiers changed from: 0000 */
        public boolean drainLoop(long j) {
            long j2;
            int length = this.b.length;
            int i = this.c;
            int i2 = 0;
            while (true) {
                j2 = (long) i2;
                if (j2 != j && i != length && !b()) {
                    T t = this.b[i];
                    if (t == null) {
                        Subscriber<? super T> subscriber = this.a;
                        StringBuilder sb = new StringBuilder("The element at index ");
                        sb.append(i);
                        sb.append(" is null");
                        subscriber.onError(new NullPointerException(sb.toString()));
                        return false;
                    }
                    this.a.onNext(t);
                    i++;
                    i2++;
                } else if (i == length || b()) {
                    a(j2);
                    this.c = i;
                    return true;
                } else {
                    this.a.onComplete();
                    return false;
                }
            }
            if (i == length) {
            }
            a(j2);
            this.c = i;
            return true;
        }
    }

    OpFromArray(T[] tArr) {
        this.a = tArr;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Subscriber<? super T> subscriber) {
        subscriber.onSubscribe(new ArraySubscription(subscriber, this.a));
    }
}
