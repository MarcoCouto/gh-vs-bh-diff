package com.smaato.sdk.richmedia;

public final class R {

    public static final class color {
        public static final int smaato_sdk_core_ui_ctrl_almost_white = 2131099799;
        public static final int smaato_sdk_core_ui_ctrl_black = 2131099800;
        public static final int smaato_sdk_core_ui_ctrl_grey = 2131099801;
        public static final int smaato_sdk_core_ui_semitransparent = 2131099802;
        public static final int smaato_sdk_richmedia_ui_semitransparent = 2131099803;

        private color() {
        }
    }

    public static final class dimen {
        public static final int smaato_sdk_core_activity_margin = 2131165373;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int smaato_sdk_core_back = 2131231091;
        public static final int smaato_sdk_core_back_disabled = 2131231092;
        public static final int smaato_sdk_core_background = 2131231093;
        public static final int smaato_sdk_core_browser_bottom_button_layout_bg = 2131231094;
        public static final int smaato_sdk_core_browser_progress_bar = 2131231095;
        public static final int smaato_sdk_core_browser_top_button_layout_bg = 2131231096;
        public static final int smaato_sdk_core_circle_close = 2131231097;
        public static final int smaato_sdk_core_close = 2131231098;
        public static final int smaato_sdk_core_forward = 2131231099;
        public static final int smaato_sdk_core_forward_disabled = 2131231100;
        public static final int smaato_sdk_core_ic_browser_background_selector = 2131231101;
        public static final int smaato_sdk_core_ic_browser_backward_selector = 2131231102;
        public static final int smaato_sdk_core_ic_browser_forward_selector = 2131231103;
        public static final int smaato_sdk_core_ic_browser_secure_connection = 2131231104;
        public static final int smaato_sdk_core_lock = 2131231105;
        public static final int smaato_sdk_core_open_in_browser = 2131231106;
        public static final int smaato_sdk_core_refresh = 2131231107;
        public static final int smaato_sdk_core_watermark = 2131231108;
        public static final int smaato_sdk_richmedia_progress_bar = 2131231109;

        private drawable() {
        }
    }

    public static final class id {
        public static final int btnBackward = 2131296342;
        public static final int btnClose = 2131296343;
        public static final int btnForward = 2131296344;
        public static final int btnLayoutBottom = 2131296345;
        public static final int btnLayoutTop = 2131296346;
        public static final int btnOpenExternal = 2131296347;
        public static final int btnRefresh = 2131296348;
        public static final int close = 2131296366;
        public static final int container = 2131296379;
        public static final int progressBar = 2131296508;
        public static final int tvHostname = 2131296616;
        public static final int webView = 2131296627;

        private id() {
        }
    }

    public static final class layout {
        public static final int smaato_sdk_core_activity_internal_browser = 2131427430;
        public static final int smaato_sdk_richmedia_layout_closable = 2131427433;

        private layout() {
        }
    }

    public static final class string {
        public static final int smaato_sdk_core_browser_hostname_content_description = 2131624215;
        public static final int smaato_sdk_core_btn_browser_backward_content_description = 2131624216;
        public static final int smaato_sdk_core_btn_browser_close_content_description = 2131624217;
        public static final int smaato_sdk_core_btn_browser_forward_content_description = 2131624218;
        public static final int smaato_sdk_core_btn_browser_open_content_description = 2131624219;
        public static final int smaato_sdk_core_btn_browser_refresh_content_description = 2131624220;
        public static final int smaato_sdk_core_fullscreen_dimension = 2131624221;
        public static final int smaato_sdk_core_no_external_browser_found = 2131624222;
        public static final int smaato_sdk_richmedia_collapse_mraid_ad = 2131624223;

        private string() {
        }
    }

    public static final class style {
        public static final int smaato_sdk_core_browserProgressBar = 2131689896;

        private style() {
        }
    }

    private R() {
    }
}
