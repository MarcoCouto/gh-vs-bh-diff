package com.smaato.sdk.richmedia.di;

public final class DiNames {
    public static final String MODULE_DI_NAME = "RichMediaModuleInterface";

    private DiNames() {
    }
}
