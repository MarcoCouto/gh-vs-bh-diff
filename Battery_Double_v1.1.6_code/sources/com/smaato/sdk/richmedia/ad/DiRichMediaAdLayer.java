package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.analytics.Analytics;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.tracker.ImpressionDetector;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.notifier.Timer;
import com.smaato.sdk.core.violationreporter.AdQualityViolationReporter;
import com.smaato.sdk.richmedia.ad.tracker.RichMediaVisibilityTrackerCreator;
import com.smaato.sdk.richmedia.mraid.MraidConfigurator;

public final class DiRichMediaAdLayer {

    private interface a extends Function<RichMediaAdObject, RichMediaAdInteractor> {
    }

    private DiRichMediaAdLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper, @NonNull String str) {
        Objects.requireNonNull(adPresenterNameShaper);
        Objects.requireNonNull(str);
        return DiRegistry.of(new Consumer(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                DiRichMediaAdLayer.a(AdPresenterNameShaper.this, this.f$1, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AdPresenterNameShaper adPresenterNameShaper, String str, DiRegistry diRegistry) {
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.RICH_MEDIA, InterstitialAdPresenter.class), AdPresenterBuilder.class, new ClassFactory(str, adPresenterNameShaper) {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ AdPresenterNameShaper f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiRichMediaAdLayer.a(this.f$0, this.f$1, diConstructor);
            }
        });
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.RICH_MEDIA, BannerAdPresenter.class), AdPresenterBuilder.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiRichMediaAdLayer.c(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory("RichMediaModuleInterfacebannerRichMedia", a.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiRichMediaAdLayer.b(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory("RichMediaModuleInterfaceinterstitialRichMedia", a.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiRichMediaAdLayer.a(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory(str, StateMachine.class, $$Lambda$DiRichMediaAdLayer$xyCS8xXOUtK32vkqBFrgw71poF0.INSTANCE);
        diRegistry.registerFactory("RichMediaModuleInterfacebannerRichMedia", ImpressionDetector.class, $$Lambda$DiRichMediaAdLayer$b24vYINmQjidaQBVqSpPlcNW4ug.INSTANCE);
        diRegistry.registerFactory("RichMediaModuleInterfaceinterstitialRichMedia", ImpressionDetector.class, $$Lambda$DiRichMediaAdLayer$vIvkib6hoIKwoxTNwA8sJXE9yho.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder a(String str, AdPresenterNameShaper adPresenterNameShaper, DiConstructor diConstructor) {
        d dVar = new d(DiLogLayer.getLoggerFrom(diConstructor), (RichMediaAdResponseParser) diConstructor.get(str, RichMediaAdResponseParser.class), (RichMediaVisibilityTrackerCreator) diConstructor.get(str, RichMediaVisibilityTrackerCreator.class), (Timer) diConstructor.get(adPresenterNameShaper.shapeName(AdFormat.INTERSTITIAL, InterstitialAdPresenter.class), Timer.class), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), (MraidConfigurator) diConstructor.get(str, MraidConfigurator.class), (Function) diConstructor.get("RichMediaModuleInterfaceinterstitialRichMedia", a.class), ((Analytics) diConstructor.get(Analytics.class)).getWebViewTracker());
        return dVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder c(String str, DiConstructor diConstructor) {
        b bVar = new b(DiLogLayer.getLoggerFrom(diConstructor), (RichMediaAdResponseParser) diConstructor.get(str, RichMediaAdResponseParser.class), (RichMediaVisibilityTrackerCreator) diConstructor.get(str, RichMediaVisibilityTrackerCreator.class), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), (MraidConfigurator) diConstructor.get(str, MraidConfigurator.class), (Function) diConstructor.get("RichMediaModuleInterfacebannerRichMedia", a.class), ((Analytics) diConstructor.get(Analytics.class)).getWebViewTracker());
        return bVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a b(String str, DiConstructor diConstructor) {
        return new a(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return DiRichMediaAdLayer.b(DiConstructor.this, this.f$1, (RichMediaAdObject) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RichMediaAdInteractor b(DiConstructor diConstructor, String str, RichMediaAdObject richMediaAdObject) {
        RichMediaAdInteractor richMediaAdInteractor = new RichMediaAdInteractor((Logger) diConstructor.get(Logger.class), richMediaAdObject, DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (StateMachine) diConstructor.get(str, StateMachine.class), (LinkResolver) diConstructor.get(LinkResolver.class), (AdQualityViolationReporter) diConstructor.get(AdQualityViolationReporter.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class), (ImpressionDetector) diConstructor.get("RichMediaModuleInterfacebannerRichMedia", ImpressionDetector.class));
        return richMediaAdInteractor;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a a(String str, DiConstructor diConstructor) {
        return new a(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return DiRichMediaAdLayer.a(DiConstructor.this, this.f$1, (RichMediaAdObject) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RichMediaAdInteractor a(DiConstructor diConstructor, String str, RichMediaAdObject richMediaAdObject) {
        RichMediaAdInteractor richMediaAdInteractor = new RichMediaAdInteractor((Logger) diConstructor.get(Logger.class), richMediaAdObject, DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (StateMachine) diConstructor.get(str, StateMachine.class), (LinkResolver) diConstructor.get(LinkResolver.class), (AdQualityViolationReporter) diConstructor.get(AdQualityViolationReporter.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class), (ImpressionDetector) diConstructor.get("RichMediaModuleInterfaceinterstitialRichMedia", ImpressionDetector.class));
        return richMediaAdInteractor;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ImpressionDetector b(DiConstructor diConstructor) {
        return new ImpressionDetector(State.CREATED);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ImpressionDetector a(DiConstructor diConstructor) {
        return new ImpressionDetector(State.IMPRESSED);
    }
}
