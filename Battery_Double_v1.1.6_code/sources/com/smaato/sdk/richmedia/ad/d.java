package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.notifier.Timer;
import com.smaato.sdk.richmedia.ad.tracker.RichMediaVisibilityTrackerCreator;
import com.smaato.sdk.richmedia.mraid.MraidConfigurator;

final class d extends e<InterstitialAdPresenter> {
    d(@NonNull Logger logger, @NonNull RichMediaAdResponseParser richMediaAdResponseParser, @NonNull RichMediaVisibilityTrackerCreator richMediaVisibilityTrackerCreator, @NonNull Timer timer, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull MraidConfigurator mraidConfigurator, @NonNull Function<RichMediaAdObject, RichMediaAdInteractor> function, @NonNull WebViewViewabilityTracker webViewViewabilityTracker) {
        $$Lambda$d$S3rhAAI_GKJOipHTTH6l8sgfS7w r0 = new Function(richMediaVisibilityTrackerCreator, timer, appBackgroundDetector, mraidConfigurator, webViewViewabilityTracker) {
            private final /* synthetic */ RichMediaVisibilityTrackerCreator f$1;
            private final /* synthetic */ Timer f$2;
            private final /* synthetic */ AppBackgroundDetector f$3;
            private final /* synthetic */ MraidConfigurator f$4;
            private final /* synthetic */ WebViewViewabilityTracker f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            public final Object apply(Object obj) {
                return d.a(Logger.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, (RichMediaAdInteractor) obj);
            }
        };
        RichMediaAdResponseParser richMediaAdResponseParser2 = richMediaAdResponseParser;
        Function<RichMediaAdObject, RichMediaAdInteractor> function2 = function;
        super(logger, richMediaAdResponseParser, function, r0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ InterstitialAdPresenter a(Logger logger, RichMediaVisibilityTrackerCreator richMediaVisibilityTrackerCreator, Timer timer, AppBackgroundDetector appBackgroundDetector, MraidConfigurator mraidConfigurator, WebViewViewabilityTracker webViewViewabilityTracker, RichMediaAdInteractor richMediaAdInteractor) {
        c cVar = new c(logger, richMediaAdInteractor, richMediaVisibilityTrackerCreator, timer, appBackgroundDetector, mraidConfigurator, webViewViewabilityTracker);
        return cVar;
    }
}
