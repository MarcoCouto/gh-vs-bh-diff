package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Error;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Listener;
import com.smaato.sdk.core.ad.AdPresenterBuilderException;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.richmedia.ad.RichMediaAdObject.Builder;
import java.io.UnsupportedEncodingException;

abstract class e<Presenter extends AdPresenter> implements AdPresenterBuilder {
    @NonNull
    private final Logger a;
    @NonNull
    private final RichMediaAdResponseParser b;
    @NonNull
    private final Function<RichMediaAdObject, RichMediaAdInteractor> c;
    @NonNull
    private final Function<RichMediaAdInteractor, Presenter> d;

    e(@NonNull Logger logger, @NonNull RichMediaAdResponseParser richMediaAdResponseParser, @NonNull Function<RichMediaAdObject, RichMediaAdInteractor> function, @NonNull Function<RichMediaAdInteractor, Presenter> function2) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (RichMediaAdResponseParser) Objects.requireNonNull(richMediaAdResponseParser);
        this.c = (Function) Objects.requireNonNull(function);
        this.d = (Function) Objects.requireNonNull(function2);
    }

    public void buildAdPresenter(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        Objects.requireNonNull(somaApiContext);
        RichMediaAdObject a2 = a(somaApiContext, listener);
        if (a2 != null) {
            this.a.info(LogDomain.AD, "parsed RichMediaAdObject = %s", a2);
            listener.onAdPresenterBuildSuccess(this, (AdPresenter) this.d.apply((RichMediaAdInteractor) this.c.apply(a2)));
        }
    }

    @Nullable
    private RichMediaAdObject a(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        ApiAdResponse apiAdResponse = somaApiContext.getApiAdResponse();
        try {
            try {
                RichMediaAdResponse parseResponse = this.b.parseResponse(new String(apiAdResponse.getBody(), apiAdResponse.getCharset()));
                try {
                    return new Builder().setSomaApiContext(somaApiContext).setWidth(parseResponse.b()).setHeight(parseResponse.c()).setContent(parseResponse.a()).setClickTrackingUrls(parseResponse.e()).setImpressionTrackingUrls(parseResponse.d()).setExtensions(parseResponse.f()).build();
                } catch (Exception e) {
                    this.a.error(LogDomain.AD, e, "Failed to build RichMediaAdObject", new Object[0]);
                    listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e));
                    return null;
                }
            } catch (a e2) {
                this.a.error(LogDomain.AD, e2, "Invalid AdResponse: %s", apiAdResponse);
                listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e2));
                return null;
            }
        } catch (UnsupportedEncodingException e3) {
            this.a.error(LogDomain.AD, e3, "Invalid AdResponse: %s. Cannot parse AdResponse with provided charset: %s", apiAdResponse, apiAdResponse.getCharset());
            listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e3));
            return null;
        }
    }
}
