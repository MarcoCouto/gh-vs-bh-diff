package com.smaato.sdk.richmedia.ad;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.tracker.ImpressionDetector;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Listener;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.violationreporter.AdQualityViolationReporter;
import java.util.concurrent.atomic.AtomicReference;

final class RichMediaAdInteractor extends AdInteractor<RichMediaAdObject> {
    @NonNull
    private final Logger a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final LinkResolver c;
    @NonNull
    private final AdQualityViolationReporter d;
    /* access modifiers changed from: private */
    @NonNull
    public AtomicReference<Task> e = new AtomicReference<>();
    @Nullable
    private Callback f;

    public interface Callback {
        void onImpressionTriggered();
    }

    RichMediaAdInteractor(@NonNull Logger logger, @NonNull RichMediaAdObject richMediaAdObject, @NonNull BeaconTracker beaconTracker, @NonNull StateMachine<Event, State> stateMachine, @NonNull LinkResolver linkResolver, @NonNull AdQualityViolationReporter adQualityViolationReporter, @NonNull OneTimeActionFactory oneTimeActionFactory, @NonNull ImpressionDetector impressionDetector) {
        super(richMediaAdObject, stateMachine, oneTimeActionFactory);
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.c = (LinkResolver) Objects.requireNonNull(linkResolver);
        this.d = (AdQualityViolationReporter) Objects.requireNonNull(adQualityViolationReporter);
        stateMachine.addListener(new Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                RichMediaAdInteractor.this.a((State) obj, (State) obj2, metadata);
            }
        });
        stateMachine.addListener(impressionDetector.stateListener);
        $$Lambda$RichMediaAdInteractor$ZouC58K5oGlL_MTDhvtejSDRxE8 r0 = new com.smaato.sdk.core.tracker.ImpressionDetector.Callback(impressionDetector, logger, beaconTracker, richMediaAdObject) {
            private final /* synthetic */ ImpressionDetector f$1;
            private final /* synthetic */ Logger f$2;
            private final /* synthetic */ BeaconTracker f$3;
            private final /* synthetic */ RichMediaAdObject f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void onImpressionStateDetected() {
                RichMediaAdInteractor.this.a(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        impressionDetector.setOnImpressionStateDetectedCallback(r0);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(ImpressionDetector impressionDetector, Logger logger, BeaconTracker beaconTracker, RichMediaAdObject richMediaAdObject) {
        impressionDetector.setOnImpressionStateDetectedCallback(null);
        logger.debug(LogDomain.AD, "Going to send impression beacons", new Object[0]);
        beaconTracker.trackBeaconUrls(richMediaAdObject.getImpressionTrackingUrls(), richMediaAdObject.getSomaApiContext());
        Objects.onNotNull(this.f, $$Lambda$m3Ktp1gi43if_DIEgrk6mpt0tc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull State state, @NonNull State state2, @Nullable Metadata metadata) {
        switch (state2) {
            case INIT:
            case CREATED:
            case ON_SCREEN:
            case IMPRESSED:
                return;
            case CLICKED:
                this.a.debug(LogDomain.AD, "event %s: going to send click beacons", state2);
                RichMediaAdObject richMediaAdObject = (RichMediaAdObject) getAdObject();
                this.b.trackBeaconUrls(richMediaAdObject.getClickTrackingUrls(), richMediaAdObject.getSomaApiContext());
                return;
            case COMPLETE:
            case TO_BE_DELETED:
                return;
            default:
                this.a.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    public final void a(@NonNull String str, @NonNull final UrlResolveListener urlResolveListener) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(urlResolveListener);
        if (this.e.get() == null) {
            Task handleClickThroughUrl = this.c.handleClickThroughUrl(((RichMediaAdObject) getAdObject()).getSomaApiContext(), str, new UrlResolveListener() {
                public final void onError() {
                    RichMediaAdInteractor.this.e.set(null);
                    urlResolveListener.onError();
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    RichMediaAdInteractor.this.e.set(null);
                    urlResolveListener.onSuccess(consumer);
                }
            });
            this.e.set(handleClickThroughUrl);
            handleClickThroughUrl.start();
        }
    }

    public final void a() {
        Objects.onNotNull(this.e.get(), $$Lambda$yaUpY0DGpVsMZlshO_jwvMPRY4g.INSTANCE);
        this.e.set(null);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str, @Nullable String str2) {
        this.d.reportAdViolation(str, ((RichMediaAdObject) getAdObject()).getSomaApiContext(), str2);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable Callback callback) {
        this.f = callback;
    }
}
