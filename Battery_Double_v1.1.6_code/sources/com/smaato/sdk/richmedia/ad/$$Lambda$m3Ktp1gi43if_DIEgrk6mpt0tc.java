package com.smaato.sdk.richmedia.ad;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.ad.RichMediaAdInteractor.Callback;

/* renamed from: com.smaato.sdk.richmedia.ad.-$$Lambda$m3Ktp-1gi43if_DIEgrk6mpt0tc reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$m3Ktp1gi43if_DIEgrk6mpt0tc implements Consumer {
    public static final /* synthetic */ $$Lambda$m3Ktp1gi43if_DIEgrk6mpt0tc INSTANCE = new $$Lambda$m3Ktp1gi43if_DIEgrk6mpt0tc();

    private /* synthetic */ $$Lambda$m3Ktp1gi43if_DIEgrk6mpt0tc() {
    }

    public final void accept(Object obj) {
        ((Callback) obj).onImpressionTriggered();
    }
}
