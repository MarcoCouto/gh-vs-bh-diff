package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.List;

public final class RichMediaAdObject implements AdObject {
    @NonNull
    private final SomaApiContext a;
    @NonNull
    private final String b;
    private final int c;
    private final int d;
    @NonNull
    private final List<String> e;
    @NonNull
    private final List<String> f;
    @Nullable
    private final Object g;

    public static final class Builder {
        private SomaApiContext a;
        private String b;
        private int c;
        private int d;
        private List<String> e;
        private List<String> f;
        private Object g;

        @NonNull
        public final Builder setSomaApiContext(@NonNull SomaApiContext somaApiContext) {
            this.a = somaApiContext;
            return this;
        }

        @NonNull
        public final Builder setContent(@NonNull String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public final Builder setWidth(int i) {
            this.c = i;
            return this;
        }

        @NonNull
        public final Builder setHeight(int i) {
            this.d = i;
            return this;
        }

        @NonNull
        public final Builder setImpressionTrackingUrls(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public final Builder setClickTrackingUrls(@Nullable List<String> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public final Builder setExtensions(@Nullable Object obj) {
            this.g = obj;
            return this;
        }

        @NonNull
        public final RichMediaAdObject build() {
            ArrayList arrayList = new ArrayList();
            if (this.a == null) {
                arrayList.add("somaApiContext");
            }
            if (this.b == null) {
                arrayList.add("content");
            }
            if (this.e == null) {
                arrayList.add("impressionTrackingUrls");
            }
            if (this.f == null) {
                arrayList.add("clickTrackingUrls");
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new IllegalStateException(sb.toString());
            } else if (this.e.isEmpty()) {
                throw new IllegalStateException("impressionTrackingUrls cannot be empty");
            } else if (!this.f.isEmpty()) {
                RichMediaAdObject richMediaAdObject = new RichMediaAdObject(this.a, this.b, this.c, this.d, this.e, this.f, this.g, 0);
                return richMediaAdObject;
            } else {
                throw new IllegalStateException("clickTrackingUrls cannot be empty");
            }
        }
    }

    /* synthetic */ RichMediaAdObject(SomaApiContext somaApiContext, String str, int i, int i2, List list, List list2, Object obj, byte b2) {
        this(somaApiContext, str, i, i2, list, list2, obj);
    }

    private RichMediaAdObject(@NonNull SomaApiContext somaApiContext, @NonNull String str, int i, int i2, @NonNull List<String> list, @NonNull List<String> list2, @Nullable Object obj) {
        this.a = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.b = (String) Objects.requireNonNull(str);
        this.c = i;
        this.d = i2;
        this.e = (List) Objects.requireNonNull(list);
        this.f = (List) Objects.requireNonNull(list2);
        this.g = obj;
    }

    @NonNull
    public final SomaApiContext getSomaApiContext() {
        return this.a;
    }

    @NonNull
    public final String getContent() {
        return this.b;
    }

    public final int getWidth() {
        return this.c;
    }

    public final int getHeight() {
        return this.d;
    }

    @NonNull
    public final List<String> getImpressionTrackingUrls() {
        return this.e;
    }

    @NonNull
    public final List<String> getClickTrackingUrls() {
        return this.f;
    }

    @Nullable
    public final Object getExtensions() {
        return this.g;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("RichMediaAdObject{somaApiContext=");
        sb.append(this.a);
        sb.append(", content='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", width=");
        sb.append(this.c);
        sb.append(", height=");
        sb.append(this.d);
        sb.append(", impressionTrackingUrls=");
        sb.append(this.e);
        sb.append(", clickTrackingUrls=");
        sb.append(this.f);
        sb.append(", extensions=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }
}
