package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.richmedia.ad.tracker.RichMediaVisibilityTrackerCreator;
import com.smaato.sdk.richmedia.mraid.MraidConfigurator;

final class b extends e<BannerAdPresenter> {
    b(@NonNull Logger logger, @NonNull RichMediaAdResponseParser richMediaAdResponseParser, @NonNull RichMediaVisibilityTrackerCreator richMediaVisibilityTrackerCreator, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull MraidConfigurator mraidConfigurator, @NonNull Function<RichMediaAdObject, RichMediaAdInteractor> function, @NonNull WebViewViewabilityTracker webViewViewabilityTracker) {
        $$Lambda$b$8Y1eKMiWurqv6DtrAoVr5xWW0oc r0 = new Function(richMediaVisibilityTrackerCreator, appBackgroundDetector, mraidConfigurator, webViewViewabilityTracker) {
            private final /* synthetic */ RichMediaVisibilityTrackerCreator f$1;
            private final /* synthetic */ AppBackgroundDetector f$2;
            private final /* synthetic */ MraidConfigurator f$3;
            private final /* synthetic */ WebViewViewabilityTracker f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final Object apply(Object obj) {
                return b.a(Logger.this, this.f$1, this.f$2, this.f$3, this.f$4, (RichMediaAdInteractor) obj);
            }
        };
        super(logger, richMediaAdResponseParser, function, r0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BannerAdPresenter a(Logger logger, RichMediaVisibilityTrackerCreator richMediaVisibilityTrackerCreator, AppBackgroundDetector appBackgroundDetector, MraidConfigurator mraidConfigurator, WebViewViewabilityTracker webViewViewabilityTracker, RichMediaAdInteractor richMediaAdInteractor) {
        a aVar = new a(logger, richMediaAdInteractor, richMediaVisibilityTrackerCreator, appBackgroundDetector, mraidConfigurator, webViewViewabilityTracker);
        return aVar;
    }
}
