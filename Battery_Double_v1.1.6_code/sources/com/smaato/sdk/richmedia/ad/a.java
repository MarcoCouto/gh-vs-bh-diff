package com.smaato.sdk.richmedia.ad;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdInteractor.TtlListener;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.BannerAdPresenter.Listener;
import com.smaato.sdk.core.ad.BaseAdPresenter;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.ad.tracker.RichMediaVisibilityTracker;
import com.smaato.sdk.richmedia.ad.tracker.RichMediaVisibilityTrackerCreator;
import com.smaato.sdk.richmedia.ad.tracker.VisibilityTrackerListener;
import com.smaato.sdk.richmedia.mraid.MraidConfigurator;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView.Callback;
import com.smaato.sdk.richmedia.widget.RichMediaWebView;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

final class a extends BaseAdPresenter implements BannerAdPresenter {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    /* access modifiers changed from: private */
    @NonNull
    public final RichMediaAdInteractor b;
    @NonNull
    private final RichMediaVisibilityTrackerCreator c;
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<RichMediaVisibilityTracker> d = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public final AppBackgroundDetector e;
    @NonNull
    private final MraidConfigurator f;
    /* access modifiers changed from: private */
    @NonNull
    public final WebViewViewabilityTracker g;
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<RichMediaAdContentView> h = new WeakReference<>(null);
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<Listener> i = new WeakReference<>(null);
    @NonNull
    private StateMachine.Listener<State> j;
    @NonNull
    private TtlListener k = new TtlListener() {
        public final void onTTLExpired(AdInteractor adInteractor) {
            a.this.a(adInteractor);
        }
    };

    /* renamed from: com.smaato.sdk.richmedia.ad.a$a reason: collision with other inner class name */
    final class C0061a implements Callback {
        private UrlResolveListener b;

        private C0061a() {
            this.b = new UrlResolveListener() {
                public final void onError() {
                    Threads.runOnUi(new Runnable(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$a$a$1$cJueGd0aUhPIN52pcD_0B09bnw4.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a() {
                    a.this.a.error(LogDomain.AD, "The url seems to be invalid", new Object[0]);
                    Objects.onNotNull(a.this.i.get(), new Consumer(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$a$a$1$q3EXjo0mmVz13GW_qvhyvwF3ncA.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                    Objects.onNotNull(a.this.h.get(), $$Lambda$a$a$1$xCZZBbNgyIJnzikIwhcDkudPc.INSTANCE);
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Listener listener) {
                    listener.onAdError(a.this);
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    Threads.runOnUi(new Runnable(this, consumer) {
                        private final /* synthetic */ AnonymousClass1 f$0;
                        private final /* synthetic */ Consumer f$1;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$a$a$1$WpRi_PFmIdjvPC6Gs_N0-hvMByg.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Consumer consumer) {
                    Objects.onNotNull(a.this.h.get(), new Consumer(consumer) {
                        private final /* synthetic */ Consumer f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$a$a$1$WgXOYNAtspGrkPuy4PMbhoN3nY4.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public static /* synthetic */ void a(Consumer consumer, RichMediaAdContentView richMediaAdContentView) {
                    consumer.accept(richMediaAdContentView.getContext());
                    richMediaAdContentView.showProgressIndicator(false);
                }
            };
        }

        /* synthetic */ C0061a(a aVar, byte b2) {
            this();
        }

        public final void onWebViewLoaded(@NonNull RichMediaAdContentView richMediaAdContentView) {
            a.this.g.registerAdView(richMediaAdContentView.getWebView());
            a.this.g.startTracking();
            Objects.onNotNull(a.this.d.get(), $$Lambda$u0fuGW6C4x96oi_UHwrAM_O6eeI.INSTANCE);
        }

        public final void onWebViewClicked(@NonNull RichMediaAdContentView richMediaAdContentView) {
            if (a.this.e.isAppInBackground()) {
                a.this.a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
            } else {
                a.this.b.onEvent(Event.CLICK);
            }
        }

        public final void onUrlClicked(@NonNull RichMediaAdContentView richMediaAdContentView, @NonNull String str) {
            if (a.this.e.isAppInBackground()) {
                a.this.a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
                return;
            }
            richMediaAdContentView.showProgressIndicator(true);
            a.this.b.a(str, this.b);
        }

        public final void onAdExpanded(@NonNull RichMediaAdContentView richMediaAdContentView) {
            if (a.this.e.isAppInBackground()) {
                a.this.a.info(LogDomain.AD, "skipping expand event, because app is in background", new Object[0]);
            } else {
                Objects.onNotNull(a.this.i.get(), new Consumer() {
                    public final void accept(Object obj) {
                        C0061a.this.c((Listener) obj);
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void c(Listener listener) {
            listener.onAdExpanded(a.this);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a() {
            Objects.onNotNull(a.this.i.get(), new Consumer() {
                public final void accept(Object obj) {
                    C0061a.this.b((Listener) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b(Listener listener) {
            listener.onAdUnload(a.this);
        }

        public final void onUnloadView(@NonNull RichMediaAdContentView richMediaAdContentView) {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    C0061a.this.a();
                }
            });
        }

        public final void onAdResized(@NonNull RichMediaAdContentView richMediaAdContentView) {
            if (a.this.e.isAppInBackground()) {
                a.this.a.info(LogDomain.AD, "skipping resize event, because app is in background", new Object[0]);
            } else {
                Objects.onNotNull(a.this.i.get(), $$Lambda$cYYIRl5OnGn12VImG_S6TxguU.INSTANCE);
            }
        }

        public final void onAdCollapsed(@NonNull RichMediaAdContentView richMediaAdContentView) {
            Objects.onNotNull(a.this.i.get(), $$Lambda$DnClyV8LMK4oneag9X9lq8FTdY.INSTANCE);
        }

        public final void onAdViolation(@NonNull String str, @Nullable String str2) {
            a.this.b.a(str, str2);
        }

        public final void registerFriendlyObstruction(@NonNull View view) {
            a.this.g.registerFriendlyObstruction(view);
        }

        public final void removeFriendlyObstruction(@NonNull View view) {
            a.this.g.removeFriendlyObstruction(view);
        }

        public final void updateAdView(@NonNull RichMediaWebView richMediaWebView) {
            a.this.g.updateAdView(richMediaWebView);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Listener listener) {
            listener.onAdError(a.this);
        }

        public final void onRenderProcessGone(@NonNull RichMediaAdContentView richMediaAdContentView) {
            Objects.onNotNull(a.this.i.get(), new Consumer() {
                public final void accept(Object obj) {
                    C0061a.this.a((Listener) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdInteractor adInteractor) {
        Objects.onNotNull(this.i.get(), new Consumer() {
            public final void accept(Object obj) {
                a.this.c((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onTTLExpired(this);
    }

    a(@NonNull Logger logger, @NonNull RichMediaAdInteractor richMediaAdInteractor, @NonNull RichMediaVisibilityTrackerCreator richMediaVisibilityTrackerCreator, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull MraidConfigurator mraidConfigurator, @NonNull WebViewViewabilityTracker webViewViewabilityTracker) {
        super(richMediaAdInteractor);
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (RichMediaAdInteractor) Objects.requireNonNull(richMediaAdInteractor);
        this.c = (RichMediaVisibilityTrackerCreator) Objects.requireNonNull(richMediaVisibilityTrackerCreator);
        this.e = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.f = (MraidConfigurator) Objects.requireNonNull(mraidConfigurator);
        this.g = (WebViewViewabilityTracker) Objects.requireNonNull(webViewViewabilityTracker);
        this.j = new StateMachine.Listener(webViewViewabilityTracker, richMediaAdInteractor, logger) {
            private final /* synthetic */ WebViewViewabilityTracker f$1;
            private final /* synthetic */ RichMediaAdInteractor f$2;
            private final /* synthetic */ Logger f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                a.this.a(this.f$1, this.f$2, this.f$3, (State) obj, (State) obj2, metadata);
            }
        };
        richMediaAdInteractor.addStateListener(this.j);
        richMediaAdInteractor.addTtlListener(this.k);
        richMediaAdInteractor.a((RichMediaAdInteractor.Callback) new RichMediaAdInteractor.Callback() {
            public final void onImpressionTriggered() {
                a.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(WebViewViewabilityTracker webViewViewabilityTracker, RichMediaAdInteractor richMediaAdInteractor, Logger logger, State state, State state2, Metadata metadata) {
        switch (state2) {
            case INIT:
            case CREATED:
            case COMPLETE:
                return;
            case IMPRESSED:
                webViewViewabilityTracker.trackImpression();
                return;
            case ON_SCREEN:
                Objects.onNotNull(this.h.get(), $$Lambda$hWCL2J3nKKdGo9lRAeAgOY8m5XQ.INSTANCE);
                return;
            case CLICKED:
                Objects.onNotNull(this.i.get(), new Consumer() {
                    public final void accept(Object obj) {
                        a.this.b((Listener) obj);
                    }
                });
                return;
            case TO_BE_DELETED:
                richMediaAdInteractor.removeStateListener(this.j);
                return;
            default:
                logger.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onAdImpressed(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        Objects.onNotNull(this.i.get(), new Consumer() {
            public final void accept(Object obj) {
                a.this.a((Listener) obj);
            }
        });
    }

    public final void initialize() {
        this.b.onEvent(Event.INITIALISE);
    }

    @MainThread
    @NonNull
    public final AdContentView getAdContentView(@NonNull Context context) {
        RichMediaAdObject richMediaAdObject = (RichMediaAdObject) this.b.getAdObject();
        RichMediaAdContentView createViewForBanner = this.f.createViewForBanner(context, richMediaAdObject, new C0061a(this, 0));
        createViewForBanner.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(View view) {
                a.this.b.onEvent(Event.ADDED_ON_SCREEN);
            }

            public final void onViewDetachedFromWindow(View view) {
                view.removeOnAttachStateChangeListener(this);
            }
        });
        this.d.set(this.c.createTracker(createViewForBanner, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                a.this.a();
            }
        }));
        this.h = new WeakReference<>(createViewForBanner);
        return createViewForBanner;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        this.b.onEvent(Event.IMPRESSION);
    }

    public final void setListener(@Nullable Listener listener) {
        this.i = new WeakReference<>(listener);
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        this.b.onEvent(Event.DESTROY);
        Objects.onNotNull(this.h.get(), new Consumer() {
            public final void accept(Object obj) {
                a.this.a((RichMediaAdContentView) obj);
            }
        });
        Objects.onNotNull(this.d.get(), new Consumer() {
            public final void accept(Object obj) {
                a.this.a((RichMediaVisibilityTracker) obj);
            }
        });
        this.g.stopTracking();
        this.b.a();
        this.i.clear();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaAdContentView richMediaAdContentView) {
        richMediaAdContentView.destroy();
        this.h.clear();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaVisibilityTracker richMediaVisibilityTracker) {
        richMediaVisibilityTracker.destroy();
        this.d.set(null);
    }
}
