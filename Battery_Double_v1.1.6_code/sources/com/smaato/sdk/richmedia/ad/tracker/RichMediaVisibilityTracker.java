package com.smaato.sdk.richmedia.ad.tracker;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.github.mikephil.charting.utils.Utils;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.PauseUnpauseListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import java.lang.ref.WeakReference;

public class RichMediaVisibilityTracker {
    @NonNull
    private WeakReference<View> a;
    private final double b;
    private final long c;
    @NonNull
    private final AppBackgroundAwareHandler d;
    @Nullable
    private VisibilityTrackerListener e;
    @NonNull
    private WeakReference<ViewTreeObserver> f = new WeakReference<>(null);
    /* access modifiers changed from: private */
    public boolean g = false;
    private boolean h = false;
    private boolean i = false;
    @NonNull
    private OnPreDrawListener j = new OnPreDrawListener() {
        public final boolean onPreDraw() {
            RichMediaVisibilityTracker.this.g = true;
            RichMediaVisibilityTracker.this.a();
            return true;
        }
    };

    @VisibleForTesting
    static class a implements PauseUnpauseListener {
        @VisibleForTesting
        long a;
        @VisibleForTesting
        boolean b = false;
        @VisibleForTesting
        long c = 0;
        @VisibleForTesting
        private long d = 0;

        a(long j) {
            this.a = j;
        }

        public final void onActionPaused() {
            this.d = SystemClock.uptimeMillis();
        }

        public final void onBeforeActionUnpaused() {
            long uptimeMillis = SystemClock.uptimeMillis() - this.d;
            this.d = 0;
            this.a += uptimeMillis;
        }
    }

    RichMediaVisibilityTracker(@NonNull Logger logger, @NonNull View view, double d2, long j2, @NonNull VisibilityTrackerListener visibilityTrackerListener, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler) {
        Objects.requireNonNull(logger);
        this.a = new WeakReference<>(Objects.requireNonNull(view));
        if (d2 <= Utils.DOUBLE_EPSILON || d2 > 1.0d) {
            throw new IllegalArgumentException("visibilityRatio should be in range (0..1]");
        }
        this.b = d2;
        if (((double) j2) >= Utils.DOUBLE_EPSILON) {
            this.c = j2;
            this.e = (VisibilityTrackerListener) Objects.requireNonNull(visibilityTrackerListener);
            this.d = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
            View rootView = view.getRootView();
            if (rootView != null) {
                ViewTreeObserver viewTreeObserver = rootView.getViewTreeObserver();
                this.f = new WeakReference<>(viewTreeObserver);
                if (!viewTreeObserver.isAlive()) {
                    logger.error(LogDomain.AD, "Cannot start RichMediaVisibilityTracker due to no available root view", new Object[0]);
                } else {
                    viewTreeObserver.addOnPreDrawListener(this.j);
                    return;
                }
            } else {
                logger.error(LogDomain.AD, "Cannot start RichMediaVisibilityTracker due to no available root view", new Object[0]);
            }
            return;
        }
        throw new IllegalArgumentException("visibilityTimeInMillis should be bigger or equal to 0");
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.e != null && this.g && this.i && this.a.get() != null && !this.h) {
            this.h = true;
            a(new a(SystemClock.uptimeMillis()));
        }
    }

    @MainThread
    public void requestStartTracking() {
        Threads.ensureMainThread();
        this.i = true;
        a();
    }

    private void a(@NonNull a aVar) {
        this.d.postDelayed("rich-media visibility tracker", new Runnable(aVar) {
            private final /* synthetic */ a f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                RichMediaVisibilityTracker.this.b(this.f$1);
            }
        }, 250, aVar);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(a aVar) {
        View view = (View) this.a.get();
        if (view != null) {
            long uptimeMillis = SystemClock.uptimeMillis();
            boolean a2 = a.a(view, this.b);
            if (aVar.b) {
                aVar.c += uptimeMillis - aVar.a;
                if (aVar.c >= this.c) {
                    Objects.onNotNull(this.e, $$Lambda$ZRw1a1F8uUHh6CN_JPq0mog3Qw.INSTANCE);
                    return;
                }
                aVar.a = uptimeMillis;
                aVar.b = a2;
                a(aVar);
                return;
            }
            aVar.a = uptimeMillis;
            aVar.b = a2;
            a(aVar);
        }
    }

    @MainThread
    public void destroy() {
        Threads.ensureMainThread();
        this.d.stop();
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.f.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnPreDrawListener(this.j);
        }
        this.a.clear();
        this.f.clear();
        this.e = null;
    }
}
