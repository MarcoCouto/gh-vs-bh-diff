package com.smaato.sdk.richmedia.ad.tracker;

import android.view.View;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;

public class RichMediaVisibilityTrackerCreator {
    @NonNull
    private final Logger a;
    private final double b;
    private final long c;
    @NonNull
    private final AppBackgroundDetector d;

    public RichMediaVisibilityTrackerCreator(@NonNull Logger logger, double d2, long j, @NonNull AppBackgroundDetector appBackgroundDetector) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = d2;
        this.c = j;
        this.d = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
    }

    @NonNull
    public RichMediaVisibilityTracker createTracker(@NonNull View view, @NonNull VisibilityTrackerListener visibilityTrackerListener) {
        View view2 = view;
        RichMediaVisibilityTracker richMediaVisibilityTracker = new RichMediaVisibilityTracker(this.a, view2, this.b, this.c, visibilityTrackerListener, new AppBackgroundAwareHandler(this.a, Threads.newUiHandler(), this.d));
        return richMediaVisibilityTracker;
    }
}
