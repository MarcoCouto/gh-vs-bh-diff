package com.smaato.sdk.richmedia.ad.tracker;

import android.graphics.Rect;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Threads;

final class a {
    @MainThread
    static boolean a(@NonNull View view, double d) {
        Threads.ensureMainThread();
        $$Lambda$PzjyZN0vWTQvhPzfQ0m_4MkcEw r0 = $$Lambda$PzjyZN0vWTQvhPzfQ0m_4MkcEw.INSTANCE;
        if (view.hasWindowFocus() && view.getWidth() > 0 && view.getHeight() > 0 && view.isShown()) {
            Rect rect = (Rect) r0.get();
            if (view.getGlobalVisibleRect(rect)) {
                double width = (double) (rect.width() * rect.height());
                double height = (double) (view.getHeight() * view.getWidth());
                Double.isNaN(height);
                if (width >= height * d) {
                    return true;
                }
            }
        }
        return false;
    }
}
