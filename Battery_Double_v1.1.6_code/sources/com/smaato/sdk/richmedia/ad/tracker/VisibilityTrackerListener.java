package com.smaato.sdk.richmedia.ad.tracker;

public interface VisibilityTrackerListener {
    void onVisibilityHappen();
}
