package com.smaato.sdk.richmedia.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Jsons;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.ad.RichMediaAdResponse.Builder;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class RichMediaAdResponseParser {
    @NonNull
    private final Logger a;

    static class a extends Exception {
        a(String str, Throwable th) {
            super(str, th);
        }
    }

    public RichMediaAdResponseParser(@NonNull Logger logger) {
        this.a = (Logger) Objects.requireNonNull(logger);
    }

    @NonNull
    public RichMediaAdResponse parseResponse(@NonNull String str) throws a {
        Builder builder = new Builder();
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject("richmedia");
            List stringList = Jsons.toStringList(jSONObject.getJSONArray("impressiontrackers"));
            List stringList2 = Jsons.toStringList(jSONObject.getJSONArray("clicktrackers"));
            JSONObject jSONObject2 = jSONObject.getJSONObject("mediadata");
            builder.setContent(jSONObject2.getString("content")).setWidth(Integer.parseInt(jSONObject2.getString("w"))).setHeight(Integer.parseInt(jSONObject2.getString("h"))).setImpressionTrackingUrls(stringList).setClickTrackingUrls(stringList2);
            return builder.build();
        } catch (NumberFormatException | JSONException e) {
            String format = String.format("Invalid JSON content: %s", new Object[]{str});
            this.a.error(LogDomain.AD, e, format, new Object[0]);
            throw new a(format, e);
        } catch (Exception e2) {
            String str2 = "Cannot build RichMediaAdResponse due to validation error";
            this.a.error(LogDomain.AD, e2, str2, new Object[0]);
            throw new a(str2, e2);
        }
    }
}
