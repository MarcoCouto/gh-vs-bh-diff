package com.smaato.sdk.richmedia.ad;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver.OnPreDrawListener;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.BaseAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter.Listener;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.Timer;
import com.smaato.sdk.richmedia.ad.tracker.RichMediaVisibilityTracker;
import com.smaato.sdk.richmedia.ad.tracker.RichMediaVisibilityTrackerCreator;
import com.smaato.sdk.richmedia.ad.tracker.VisibilityTrackerListener;
import com.smaato.sdk.richmedia.mraid.MraidConfigurator;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView.Callback;
import com.smaato.sdk.richmedia.widget.RichMediaWebView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

final class c extends BaseAdPresenter implements InterstitialAdPresenter {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    /* access modifiers changed from: private */
    @NonNull
    public final RichMediaAdInteractor b;
    @NonNull
    private final RichMediaVisibilityTrackerCreator c;
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<RichMediaVisibilityTracker> d = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public final AppBackgroundDetector e;
    @NonNull
    private final MraidConfigurator f;
    /* access modifiers changed from: private */
    @NonNull
    public final WebViewViewabilityTracker g;
    /* access modifiers changed from: private */
    @NonNull
    public final Timer h;
    /* access modifiers changed from: private */
    @NonNull
    public final List<WeakReference<View>> i = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<Listener> j = new WeakReference<>(null);
    @NonNull
    private StateMachine.Listener<State> k;
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<RichMediaAdContentView> l = new WeakReference<>(null);
    /* access modifiers changed from: private */
    @NonNull
    public final Timer.Listener m = new Timer.Listener() {
        public final void onTimePassed() {
            c.this.b();
        }
    };

    final class a implements Callback {
        private UrlResolveListener b;

        public final void onAdCollapsed(@NonNull RichMediaAdContentView richMediaAdContentView) {
        }

        public final void onAdExpanded(@NonNull RichMediaAdContentView richMediaAdContentView) {
        }

        public final void onAdResized(@NonNull RichMediaAdContentView richMediaAdContentView) {
        }

        public final void updateAdView(@NonNull RichMediaWebView richMediaWebView) {
        }

        private a() {
            this.b = new UrlResolveListener() {
                public final void onError() {
                    Threads.runOnUi(new Runnable(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$c$a$1$ltgtzwiIyzlzM5ZAl-2oy32EAcI.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a() {
                    c.this.a.error(LogDomain.AD, "The url seems to be invalid", new Object[0]);
                    Objects.onNotNull(c.this.l.get(), $$Lambda$c$a$1$uygpsgPqSb7nOKcyM9w7XMPCd1A.INSTANCE);
                    Objects.onNotNull(c.this.j.get(), new Consumer(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$c$a$1$S0-6xrDXXuxtvEnd1m95DBRFQbM.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Listener listener) {
                    listener.onAdError(c.this);
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    Threads.runOnUi(new Runnable(this, consumer) {
                        private final /* synthetic */ AnonymousClass1 f$0;
                        private final /* synthetic */ Consumer f$1;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$c$a$1$r9nNFMrM2_u8ACehvY15Wu3yDaY.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Consumer consumer) {
                    Objects.onNotNull(c.this.l.get(), new Consumer(consumer) {
                        private final /* synthetic */ Consumer f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.richmedia.ad.-$$Lambda$c$a$1$gBWQoEc4M_VSlBCYu1qUxMKU-xY.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:418)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:237)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:224)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public static /* synthetic */ void a(Consumer consumer, RichMediaAdContentView richMediaAdContentView) {
                    consumer.accept(richMediaAdContentView.getContext());
                    richMediaAdContentView.showProgressIndicator(false);
                }
            };
        }

        /* synthetic */ a(c cVar, byte b2) {
            this();
        }

        public final void onWebViewLoaded(@NonNull RichMediaAdContentView richMediaAdContentView) {
            c.this.g.registerAdView(richMediaAdContentView.getWebView());
            c.this.g.startTracking();
            for (WeakReference weakReference : c.this.i) {
                Object obj = weakReference.get();
                WebViewViewabilityTracker g = c.this.g;
                g.getClass();
                Objects.onNotNull(obj, new Consumer() {
                    public final void accept(Object obj) {
                        WebViewViewabilityTracker.this.registerFriendlyObstruction((View) obj);
                    }
                });
            }
            Objects.onNotNull(c.this.d.get(), $$Lambda$u0fuGW6C4x96oi_UHwrAM_O6eeI.INSTANCE);
        }

        public final void onWebViewClicked(@NonNull RichMediaAdContentView richMediaAdContentView) {
            if (c.this.e.isAppInBackground()) {
                c.this.a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
            } else {
                c.this.b.onEvent(Event.CLICK);
            }
        }

        public final void onUrlClicked(@NonNull RichMediaAdContentView richMediaAdContentView, @NonNull String str) {
            if (c.this.e.isAppInBackground()) {
                c.this.a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
                return;
            }
            richMediaAdContentView.showProgressIndicator(true);
            c.this.b.a(str, this.b);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a() {
            Objects.onNotNull(c.this.j.get(), new Consumer() {
                public final void accept(Object obj) {
                    a.this.b((Listener) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b(Listener listener) {
            listener.onAdUnload(c.this);
        }

        public final void onUnloadView(@NonNull RichMediaAdContentView richMediaAdContentView) {
            Threads.runOnUi(new Runnable() {
                public final void run() {
                    a.this.a();
                }
            });
        }

        public final void onAdViolation(@NonNull String str, @Nullable String str2) {
            c.this.b.a(str, str2);
        }

        public final void registerFriendlyObstruction(@NonNull View view) {
            c.this.g.registerFriendlyObstruction(view);
        }

        public final void removeFriendlyObstruction(@NonNull View view) {
            c.this.g.removeFriendlyObstruction(view);
        }

        public final void onRenderProcessGone(@NonNull RichMediaAdContentView richMediaAdContentView) {
            Objects.onNotNull(c.this.j.get(), new Consumer() {
                public final void accept(Object obj) {
                    a.this.a((Listener) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Listener listener) {
            listener.onAdError(c.this);
            listener.onClose(c.this);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        Objects.onNotNull(this.j.get(), $$Lambda$nk0D4u8wrNMCMfE3WzHxisIX_k.INSTANCE);
    }

    c(@NonNull Logger logger, @NonNull RichMediaAdInteractor richMediaAdInteractor, @NonNull RichMediaVisibilityTrackerCreator richMediaVisibilityTrackerCreator, @NonNull Timer timer, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull MraidConfigurator mraidConfigurator, @NonNull WebViewViewabilityTracker webViewViewabilityTracker) {
        super(richMediaAdInteractor);
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (RichMediaAdInteractor) Objects.requireNonNull(richMediaAdInteractor);
        this.c = (RichMediaVisibilityTrackerCreator) Objects.requireNonNull(richMediaVisibilityTrackerCreator);
        this.e = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.f = (MraidConfigurator) Objects.requireNonNull(mraidConfigurator);
        this.g = (WebViewViewabilityTracker) Objects.requireNonNull(webViewViewabilityTracker);
        this.h = (Timer) Objects.requireNonNull(timer);
        this.k = new StateMachine.Listener(richMediaAdInteractor, logger) {
            private final /* synthetic */ RichMediaAdInteractor f$1;
            private final /* synthetic */ Logger f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                c.this.a(this.f$1, this.f$2, (State) obj, (State) obj2, metadata);
            }
        };
        richMediaAdInteractor.addStateListener(this.k);
        richMediaAdInteractor.a((RichMediaAdInteractor.Callback) new RichMediaAdInteractor.Callback(webViewViewabilityTracker) {
            private final /* synthetic */ WebViewViewabilityTracker f$1;

            {
                this.f$1 = r2;
            }

            public final void onImpressionTriggered() {
                c.this.a(this.f$1);
            }
        });
        richMediaAdInteractor.onEvent(Event.INITIALISE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaAdInteractor richMediaAdInteractor, Logger logger, State state, State state2, Metadata metadata) {
        switch (state2) {
            case INIT:
            case CREATED:
            case IMPRESSED:
            case COMPLETE:
            case ON_SCREEN:
                return;
            case CLICKED:
                Objects.onNotNull(this.j.get(), new Consumer() {
                    public final void accept(Object obj) {
                        c.this.b((Listener) obj);
                    }
                });
                return;
            case TO_BE_DELETED:
                richMediaAdInteractor.removeStateListener(this.k);
                return;
            default:
                logger.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(WebViewViewabilityTracker webViewViewabilityTracker) {
        Objects.onNotNull(this.j.get(), new Consumer(webViewViewabilityTracker) {
            private final /* synthetic */ WebViewViewabilityTracker f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                c.this.a(this.f$1, (Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(WebViewViewabilityTracker webViewViewabilityTracker, Listener listener) {
        listener.onAdImpressed(this);
        webViewViewabilityTracker.trackImpression();
    }

    @MainThread
    @NonNull
    public final AdContentView getAdContentView(@NonNull Context context) {
        RichMediaAdObject richMediaAdObject = (RichMediaAdObject) this.b.getAdObject();
        final RichMediaAdContentView createViewForInterstitial = this.f.createViewForInterstitial(context, richMediaAdObject, new a(this, 0));
        createViewForInterstitial.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(View view) {
                c.this.b.onEvent(Event.ADDED_ON_SCREEN);
                Objects.onNotNull(c.this.l.get(), $$Lambda$hWCL2J3nKKdGo9lRAeAgOY8m5XQ.INSTANCE);
            }

            public final void onViewDetachedFromWindow(View view) {
                view.removeOnAttachStateChangeListener(this);
            }
        });
        createViewForInterstitial.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            public final boolean onPreDraw() {
                createViewForInterstitial.getViewTreeObserver().removeOnPreDrawListener(this);
                c.this.h.start(c.this.m);
                return true;
            }
        });
        this.d.set(this.c.createTracker(createViewForInterstitial, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                c.this.a();
            }
        }));
        this.l = new WeakReference<>(createViewForInterstitial);
        return createViewForInterstitial;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        this.b.onEvent(Event.IMPRESSION);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onClose(this);
    }

    public final void onCloseClicked() {
        Objects.onNotNull(this.j.get(), new Consumer() {
            public final void accept(Object obj) {
                c.this.a((Listener) obj);
            }
        });
    }

    public final void setListener(@Nullable Listener listener) {
        this.j = new WeakReference<>(listener);
    }

    public final void setFriendlyObstructionView(@NonNull View view) {
        this.i.add(new WeakReference(view));
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        this.b.onEvent(Event.DESTROY);
        this.b.a();
        this.i.clear();
        this.g.stopTracking();
        Objects.onNotNull(this.d.get(), new Consumer() {
            public final void accept(Object obj) {
                c.this.a((RichMediaVisibilityTracker) obj);
            }
        });
        Objects.onNotNull(this.l.get(), new Consumer() {
            public final void accept(Object obj) {
                c.this.a((RichMediaAdContentView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaVisibilityTracker richMediaVisibilityTracker) {
        this.d.set(null);
        richMediaVisibilityTracker.destroy();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaAdContentView richMediaAdContentView) {
        this.l.clear();
        richMediaAdContentView.destroy();
    }
}
