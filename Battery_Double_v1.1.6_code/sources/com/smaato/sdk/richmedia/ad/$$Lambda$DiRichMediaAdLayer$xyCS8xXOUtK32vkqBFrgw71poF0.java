package com.smaato.sdk.richmedia.ad;

import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.util.StateMachine.Builder;
import java.util.Arrays;

/* renamed from: com.smaato.sdk.richmedia.ad.-$$Lambda$DiRichMediaAdLayer$xyCS8xXOUtK32vkqBFrgw71poF0 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiRichMediaAdLayer$xyCS8xXOUtK32vkqBFrgw71poF0 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiRichMediaAdLayer$xyCS8xXOUtK32vkqBFrgw71poF0 INSTANCE = new $$Lambda$DiRichMediaAdLayer$xyCS8xXOUtK32vkqBFrgw71poF0();

    private /* synthetic */ $$Lambda$DiRichMediaAdLayer$xyCS8xXOUtK32vkqBFrgw71poF0() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().setInitialState(State.INIT).addTransition(Event.INITIALISE, Arrays.asList(new State[]{State.INIT, State.CREATED})).addTransition(Event.ADDED_ON_SCREEN, Arrays.asList(new State[]{State.CREATED, State.ON_SCREEN})).addTransition(Event.IMPRESSION, Arrays.asList(new State[]{State.ON_SCREEN, State.IMPRESSED})).addTransition(Event.CLICK, Arrays.asList(new State[]{State.ON_SCREEN, State.IMPRESSED, State.CLICKED, State.COMPLETE})).addTransition(Event.CLICK, Arrays.asList(new State[]{State.IMPRESSED, State.CLICKED, State.COMPLETE})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.INIT, State.TO_BE_DELETED})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.CREATED, State.TO_BE_DELETED})).addTransition(Event.EXPIRE_TTL, Arrays.asList(new State[]{State.ON_SCREEN, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.INIT, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.CREATED, State.TO_BE_DELETED})).addTransition(Event.AD_ERROR, Arrays.asList(new State[]{State.ON_SCREEN, State.TO_BE_DELETED})).addTransition(Event.DESTROY, Arrays.asList(new State[]{State.ON_SCREEN, State.CREATED})).addTransition(Event.DESTROY, Arrays.asList(new State[]{State.IMPRESSED, State.TO_BE_DELETED})).addTransition(Event.DESTROY, Arrays.asList(new State[]{State.COMPLETE, State.TO_BE_DELETED})).build();
    }
}
