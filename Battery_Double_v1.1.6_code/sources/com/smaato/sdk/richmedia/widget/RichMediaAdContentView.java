package com.smaato.sdk.richmedia.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.ui.WatermarkImageButton;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.ViewUtils;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.R;
import com.smaato.sdk.richmedia.ad.RichMediaAdObject;
import com.smaato.sdk.richmedia.mraid.RichMediaWebViewFactory;
import com.smaato.sdk.richmedia.mraid.Views;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidEnvironmentProperties.Builder;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidExpandProperties;
import com.smaato.sdk.richmedia.mraid.mvp.BaseView;
import com.smaato.sdk.richmedia.mraid.presenter.MraidPresenter;
import com.smaato.sdk.richmedia.mraid.presenter.ResizeParams;
import com.smaato.sdk.richmedia.widget.ResizeManager.Listener;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView.Callback;

@SuppressLint({"ViewConstructor"})
public final class RichMediaAdContentView extends AdContentView implements BaseView {
    @NonNull
    private final Logger a;
    /* access modifiers changed from: private */
    @NonNull
    public final RichMediaWebView b;
    @NonNull
    private final RichMediaAdObject c;
    @NonNull
    private final RichMediaWebViewFactory d;
    /* access modifiers changed from: private */
    @NonNull
    public final Callback e;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidPresenter f;
    @NonNull
    private final View g;
    @NonNull
    private final FrameLayout h;
    @Nullable
    private ResizeManager i;
    @Nullable
    private a j;
    /* access modifiers changed from: private */
    @Nullable
    public RichMediaWebView k;

    public interface Callback {
        void onAdCollapsed(@NonNull RichMediaAdContentView richMediaAdContentView);

        void onAdExpanded(@NonNull RichMediaAdContentView richMediaAdContentView);

        void onAdResized(@NonNull RichMediaAdContentView richMediaAdContentView);

        void onAdViolation(@NonNull String str, @Nullable String str2);

        void onRenderProcessGone(@NonNull RichMediaAdContentView richMediaAdContentView);

        void onUnloadView(@NonNull RichMediaAdContentView richMediaAdContentView);

        void onUrlClicked(@NonNull RichMediaAdContentView richMediaAdContentView, @NonNull String str);

        void onWebViewClicked(@NonNull RichMediaAdContentView richMediaAdContentView);

        void onWebViewLoaded(@NonNull RichMediaAdContentView richMediaAdContentView);

        void registerFriendlyObstruction(@NonNull View view);

        void removeFriendlyObstruction(@NonNull View view);

        void updateAdView(@NonNull RichMediaWebView richMediaWebView);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(View view) {
    }

    private RichMediaAdContentView(@NonNull Logger logger, @NonNull Context context, @NonNull RichMediaAdObject richMediaAdObject, @NonNull Callback callback, @NonNull RichMediaWebViewFactory richMediaWebViewFactory, @NonNull RichMediaWebView richMediaWebView, @NonNull MraidPresenter mraidPresenter) {
        super(context);
        this.a = logger;
        this.c = richMediaAdObject;
        this.e = callback;
        this.d = richMediaWebViewFactory;
        this.f = mraidPresenter;
        this.b = richMediaWebView;
        int dpToPx = UIUtils.dpToPx(context, (float) richMediaAdObject.getWidth());
        int dpToPx2 = UIUtils.dpToPx(context, (float) richMediaAdObject.getHeight());
        this.h = new FrameLayout(context);
        addView(this.h, generateDefaultLayoutParams(dpToPx, dpToPx2));
        richMediaWebView.setCallback(new com.smaato.sdk.richmedia.widget.RichMediaWebView.Callback() {
            public final void onWebViewLoaded() {
                RichMediaAdContentView.this.e.onWebViewLoaded(RichMediaAdContentView.this);
                RichMediaAdContentView.this.f.onHtmlLoaded();
            }

            public final void onWebViewClicked() {
                RichMediaAdContentView.this.e.onWebViewClicked(RichMediaAdContentView.this);
            }

            public final void onUrlClicked(@NonNull String str) {
                RichMediaAdContentView.this.e.onUrlClicked(RichMediaAdContentView.this, str);
            }

            public final void handleMraidUrl(@NonNull String str, boolean z) {
                RichMediaAdContentView.this.f.handleMraidUrl(str, z);
            }

            public final void onAdViolation(@NonNull String str, @NonNull String str2) {
                RichMediaAdContentView.this.e.onAdViolation(str, str2);
            }

            public final void onRenderProcessGone() {
                RichMediaAdContentView.this.e.onRenderProcessGone(RichMediaAdContentView.this);
            }
        });
        richMediaWebView.setId(R.id.webView);
        this.h.addView(richMediaWebView, new LayoutParams(-1, -1));
        this.h.addView(new WatermarkImageButton(getContext()));
        FrameLayout frameLayout = new FrameLayout(getContext());
        frameLayout.setBackgroundResource(R.color.smaato_sdk_richmedia_ui_semitransparent);
        frameLayout.setLayoutParams(new LayoutParams(-1, -1));
        frameLayout.setOnClickListener($$Lambda$RichMediaAdContentView$m5IkwVNTQ221vYNpzoeiXsJ5uvw.INSTANCE);
        ProgressBar progressBar = new ProgressBar(getContext());
        LayoutParams layoutParams = new LayoutParams(-2, -2, 17);
        progressBar.setIndeterminateDrawable(getResources().getDrawable(R.drawable.smaato_sdk_richmedia_progress_bar));
        progressBar.setLayoutParams(layoutParams);
        frameLayout.addView(progressBar);
        this.g = frameLayout;
        this.g.setVisibility(8);
        this.h.addView(this.g);
        setLayoutParams(new LayoutParams(dpToPx, dpToPx2, 17));
        this.f.setOnExpandCallback(new BiConsumer(richMediaWebView) {
            private final /* synthetic */ RichMediaWebView f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj, Object obj2) {
                RichMediaAdContentView.this.a(this.f$1, (String) obj, (MraidExpandProperties) obj2);
            }
        });
        this.f.setOnClickCallback(new Consumer(richMediaWebView, callback) {
            private final /* synthetic */ RichMediaWebView f$1;
            private final /* synthetic */ Callback f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                RichMediaAdContentView.this.a(this.f$1, this.f$2, (String) obj);
            }
        });
        this.f.setOnUnloadCallback(new Consumer(callback) {
            private final /* synthetic */ Callback f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                RichMediaAdContentView.this.a(this.f$1, (Whatever) obj);
            }
        });
        this.f.setResizeCallback(new Consumer(richMediaWebView) {
            private final /* synthetic */ RichMediaWebView f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                RichMediaAdContentView.this.a(this.f$1, (ResizeParams) obj);
            }
        });
        this.f.setOnCloseCallback(new Consumer() {
            public final void accept(Object obj) {
                RichMediaAdContentView.this.a((Whatever) obj);
            }
        });
        MraidPresenter mraidPresenter2 = this.f;
        callback.getClass();
        mraidPresenter2.setAdViolationCallback(new BiConsumer() {
            public final void accept(Object obj, Object obj2) {
                Callback.this.onAdViolation((String) obj, (String) obj2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaWebView richMediaWebView, String str, MraidExpandProperties mraidExpandProperties) {
        richMediaWebView.resetClickedFlag();
        if (this.j == null) {
            if (!TextUtils.isEmpty(str) && URLUtil.isNetworkUrl(str)) {
                FrameLayout frameLayout = new FrameLayout(getContext());
                WatermarkImageButton watermarkImageButton = new WatermarkImageButton(getContext());
                this.k = this.d.create(getContext());
                frameLayout.addView(this.k);
                frameLayout.addView(watermarkImageButton);
                this.k.setCallback(new RichMediaWebViewCallbackAdapter(frameLayout, true) {
                    private boolean a;
                    private /* synthetic */ FrameLayout b;
                    private /* synthetic */ boolean c = true;

                    {
                        this.b = r2;
                    }

                    public final void onWebViewLoaded() {
                        if (this.a) {
                            RichMediaAdContentView.this.f.onFailedToExpand();
                            return;
                        }
                        RichMediaAdContentView.this.a((View) this.b, this.c);
                        RichMediaAdContentView.this.e.updateAdView(RichMediaAdContentView.this.k);
                    }

                    public final void onAdViolation(@NonNull String str, @NonNull String str2) {
                        this.a = true;
                        RichMediaAdContentView.this.e.onAdViolation(str, str2);
                    }

                    public final void onRenderProcessGone() {
                        RichMediaAdContentView.this.f.onFailedToExpand();
                    }
                });
                this.k.loadUrlContent(str);
                return;
            }
            a((View) this.h, false);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaWebView richMediaWebView, Callback callback, String str) {
        richMediaWebView.resetClickedFlag();
        callback.onUrlClicked(this, str);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Callback callback, Whatever whatever) {
        callback.onUnloadView(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaWebView richMediaWebView, ResizeParams resizeParams) {
        richMediaWebView.resetClickedFlag();
        if (this.i == null) {
            this.i = new ResizeManager(this.a, this.h, resizeParams.maxSizeRectInPx);
            this.i.a((Listener) new Listener() {
                public final void onResized(@NonNull ImageButton imageButton) {
                    RichMediaAdContentView.this.f.onWasResized();
                    RichMediaAdContentView.this.e.onAdResized(RichMediaAdContentView.this);
                    RichMediaAdContentView.this.e.registerFriendlyObstruction(imageButton);
                }

                public final void onCloseClicked(@NonNull ImageButton imageButton) {
                    RichMediaAdContentView.this.f.handleClose();
                    RichMediaAdContentView.this.e.removeFriendlyObstruction(imageButton);
                }

                public final void onResizeFailed(@NonNull String str) {
                    RichMediaAdContentView.this.f.onFailedToResize(str);
                }
            });
        }
        this.i.a(resizeParams.resizeRectInPx);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Whatever whatever) {
        a();
    }

    @NonNull
    public static RichMediaAdContentView create(@NonNull Logger logger, @NonNull Context context, @NonNull RichMediaAdObject richMediaAdObject, @NonNull Callback callback, @NonNull RichMediaWebViewFactory richMediaWebViewFactory, @NonNull RichMediaWebView richMediaWebView, @NonNull MraidPresenter mraidPresenter) {
        RichMediaAdContentView richMediaAdContentView = new RichMediaAdContentView((Logger) Objects.requireNonNull(logger), (Context) Objects.requireNonNull(context), (RichMediaAdObject) Objects.requireNonNull(richMediaAdObject), (Callback) Objects.requireNonNull(callback), (RichMediaWebViewFactory) Objects.requireNonNull(richMediaWebViewFactory), (RichMediaWebView) Objects.requireNonNull(richMediaWebView), (MraidPresenter) Objects.requireNonNull(mraidPresenter));
        return richMediaAdContentView;
    }

    @MainThread
    public final void startWebViewLoading() {
        Threads.ensureMainThread();
        this.b.loadData(this.c.getContent(), new Builder(getContext().getPackageName(), this.c.getSomaApiContext().getApiAdRequest()).build());
    }

    @MainThread
    public final void showProgressIndicator(boolean z) {
        Threads.ensureMainThread();
        this.g.setVisibility(z ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f.attachView(this);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f.detachView();
    }

    @MainThread
    public final void destroy() {
        Threads.ensureMainThread();
        a();
        Objects.onNotNull(this.k, $$Lambda$yTIGBfm0jm_ViFU6QKdQMNCjZXU.INSTANCE);
        this.f.destroy();
        Handler newUiHandler = Threads.newUiHandler();
        RichMediaWebView richMediaWebView = this.b;
        richMediaWebView.getClass();
        newUiHandler.postDelayed(new Runnable() {
            public final void run() {
                RichMediaWebView.this.destroy();
            }
        }, 1000);
    }

    @NonNull
    public final RichMediaWebView getWebView() {
        return this.b;
    }

    /* access modifiers changed from: private */
    public void a(@NonNull View view, final boolean z) {
        this.j = new a();
        this.j.a(view, (C0062a) new C0062a() {
            public final void a(@NonNull ImageButton imageButton) {
                RichMediaAdContentView.this.f.onWasExpanded();
                RichMediaAdContentView.this.e.onAdExpanded(RichMediaAdContentView.this);
                RichMediaAdContentView.this.e.registerFriendlyObstruction(imageButton);
            }

            public final void b(@NonNull ImageButton imageButton) {
                RichMediaAdContentView.this.f.handleClose();
                RichMediaAdContentView.this.e.removeFriendlyObstruction(imageButton);
                if (z) {
                    RichMediaAdContentView.this.e.updateAdView(RichMediaAdContentView.this.b);
                }
            }
        });
    }

    private void a() {
        if ((this.i == null && this.j == null) ? false : true) {
            ViewUtils.removeFromParent(this.h);
            addView(this.h);
            Views.addOnPreDrawListener(this.h, new Runnable() {
                public final void run() {
                    RichMediaAdContentView.this.b();
                }
            });
        }
        Objects.onNotNull(this.i, new Consumer() {
            public final void accept(Object obj) {
                RichMediaAdContentView.this.a((ResizeManager) obj);
            }
        });
        Objects.onNotNull(this.j, new Consumer() {
            public final void accept(Object obj) {
                RichMediaAdContentView.this.a((a) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        this.f.onWasClosed();
        this.e.onAdCollapsed(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(ResizeManager resizeManager) {
        resizeManager.a();
        this.i = null;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(a aVar) {
        aVar.a();
        this.j = null;
    }
}
