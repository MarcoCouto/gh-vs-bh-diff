package com.smaato.sdk.richmedia.widget;

import androidx.annotation.NonNull;
import com.smaato.sdk.richmedia.widget.RichMediaWebView.Callback;

public class RichMediaWebViewCallbackAdapter implements Callback {
    public void handleMraidUrl(@NonNull String str, boolean z) {
    }

    public void onAdViolation(@NonNull String str, @NonNull String str2) {
    }

    public void onRenderProcessGone() {
    }

    public void onUrlClicked(@NonNull String str) {
    }

    public void onWebViewClicked() {
    }

    public void onWebViewLoaded() {
    }
}
