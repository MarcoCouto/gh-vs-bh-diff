package com.smaato.sdk.richmedia.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnShowListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.ViewUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.widget.ClosableView.OnCloseClickListener;

final class a {
    @Nullable
    private Dialog a;

    /* renamed from: com.smaato.sdk.richmedia.widget.a$a reason: collision with other inner class name */
    interface C0062a {
        void a(@NonNull ImageButton imageButton);

        void b(@NonNull ImageButton imageButton);
    }

    a() {
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull View view, @NonNull C0062a aVar) {
        Context context = view.getContext();
        ClosableView closableView = new ClosableView(context);
        closableView.setOnCloseClickListener(new OnCloseClickListener(aVar, closableView) {
            private final /* synthetic */ C0062a f$1;
            private final /* synthetic */ ClosableView f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onCloseClick() {
                a.this.b(this.f$1, this.f$2);
            }
        });
        ViewUtils.removeFromParent(view);
        closableView.a(view);
        this.a = new Dialog(context, 16973831);
        this.a.setContentView(closableView);
        this.a.setCanceledOnTouchOutside(false);
        this.a.setOnShowListener(new OnShowListener(closableView) {
            private final /* synthetic */ ClosableView f$1;

            {
                this.f$1 = r2;
            }

            public final void onShow(DialogInterface dialogInterface) {
                C0062a.this.a(this.f$1.getCloseButton());
            }
        });
        this.a.setOnKeyListener(new OnKeyListener(aVar, closableView) {
            private final /* synthetic */ C0062a f$1;
            private final /* synthetic */ ClosableView f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                return a.this.a(this.f$1, this.f$2, dialogInterface, i, keyEvent);
            }
        });
        this.a.show();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(C0062a aVar, ClosableView closableView) {
        a(aVar, closableView);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(C0062a aVar, ClosableView closableView, DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getAction() == 1) {
            a(aVar, closableView);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        Objects.onNotNull(this.a, new Consumer() {
            public final void accept(Object obj) {
                a.this.a((Dialog) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Dialog dialog) {
        dialog.dismiss();
        this.a = null;
    }

    private static void a(@NonNull C0062a aVar, @NonNull ClosableView closableView) {
        aVar.b(closableView.getCloseButton());
    }
}
