package com.smaato.sdk.richmedia.widget;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.R;

public final class ClosableView extends FrameLayout {
    @NonNull
    private FrameLayout a = ((FrameLayout) findViewById(R.id.container));
    @NonNull
    private ImageButton b = ((ImageButton) findViewById(R.id.close));
    @Nullable
    private OnCloseClickListener c;

    public interface OnCloseClickListener {
        void onCloseClick();
    }

    public ClosableView(@NonNull Context context) {
        super(context);
        LayoutInflater.from(getContext()).inflate(R.layout.smaato_sdk_richmedia_layout_closable, this, true);
        this.b.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                ClosableView.this.b(view);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(View view) {
        callOnCloseListener();
    }

    @NonNull
    public final ImageButton getCloseButton() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.a.getChildCount() > 0 && getParent() != null;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull View view) {
        this.a.addView(view, new LayoutParams(-1, -1));
    }

    public final void setOnCloseClickListener(@Nullable OnCloseClickListener onCloseClickListener) {
        this.c = onCloseClickListener;
    }

    public final void callOnCloseListener() {
        Objects.onNotNull(this.c, $$Lambda$83SwcdXIyxy0mQX9UE0wcqcKCyA.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@NonNull Rect rect, @NonNull Rect rect2) {
        Rect rect3 = new Rect();
        LayoutParams layoutParams = (LayoutParams) this.b.getLayoutParams();
        Gravity.apply(layoutParams.gravity, layoutParams.width, layoutParams.height, rect2, rect3);
        return rect.contains(rect3);
    }
}
