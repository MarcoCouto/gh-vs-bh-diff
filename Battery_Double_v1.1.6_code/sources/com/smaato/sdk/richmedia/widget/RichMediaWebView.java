package com.smaato.sdk.richmedia.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StubOnGestureListener;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.webview.BaseWebView;
import com.smaato.sdk.core.webview.BaseWebViewClient;
import com.smaato.sdk.core.webview.BaseWebViewClient.WebViewClientCallback;
import com.smaato.sdk.core.webview.WebViewHelperUtil;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidEnvironmentProperties;
import com.smaato.sdk.richmedia.util.MraidBridgeInterface;
import com.smaato.sdk.richmedia.util.RichMediaHtmlUtils;
import com.smaato.sdk.richmedia.widget.RichMediaWebView.Callback;

@SuppressLint({"ViewConstructor"})
public final class RichMediaWebView extends BaseWebView {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    @NonNull
    private final RichMediaHtmlUtils b;
    /* access modifiers changed from: private */
    @Nullable
    public Callback c;
    private boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    private boolean f = false;

    public interface Callback {
        void handleMraidUrl(@NonNull String str, boolean z);

        void onAdViolation(@NonNull String str, @NonNull String str2);

        void onRenderProcessGone();

        void onUrlClicked(@NonNull String str);

        void onWebViewClicked();

        void onWebViewLoaded();
    }

    public RichMediaWebView(@NonNull Context context, @NonNull Logger logger, @NonNull RichMediaHtmlUtils richMediaHtmlUtils, @NonNull MraidBridgeInterface mraidBridgeInterface) {
        super((Context) Objects.requireNonNull(context));
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (RichMediaHtmlUtils) Objects.requireNonNull(richMediaHtmlUtils);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        setVisibility(4);
        setBackgroundColor(getResources().getColor(17170445));
        WebSettings settings = getSettings();
        settings.setDisplayZoomControls(false);
        settings.setSupportZoom(false);
        BaseWebViewClient baseWebViewClient = new BaseWebViewClient();
        baseWebViewClient.setWebViewClientCallback(new WebViewClientCallback() {
            public final boolean shouldOverrideUrlLoading(@NonNull String str) {
                RichMediaWebView.this.a.debug(LogDomain.WIDGET, "shouldOverrideUrlLoading: %s", str);
                if (str.startsWith("smaato://")) {
                    Objects.onNotNull(RichMediaWebView.this.c, new Consumer(str) {
                        private final /* synthetic */ String f$1;

                        {
                            this.f$1 = r2;
                        }

                        public final void accept(Object obj) {
                            AnonymousClass2.this.c(this.f$1, (Callback) obj);
                        }
                    });
                    return true;
                } else if (str.startsWith("mraid://")) {
                    return true;
                } else {
                    if (RichMediaWebView.this.e) {
                        RichMediaWebView.this.e = false;
                        RichMediaWebView.this.a.debug(LogDomain.WIDGET, "shouldOverrideUrlLoading: going to call Callback::onUrlClicked() with %s", str);
                        Objects.onNotNull(RichMediaWebView.this.c, new Consumer(str) {
                            private final /* synthetic */ String f$0;

                            {
                                this.f$0 = r1;
                            }

                            public final void accept(Object obj) {
                                ((Callback) obj).onUrlClicked(this.f$0);
                            }
                        });
                        return true;
                    }
                    Objects.onNotNull(RichMediaWebView.this.c, new Consumer(str) {
                        private final /* synthetic */ String f$0;

                        {
                            this.f$0 = r1;
                        }

                        public final void accept(Object obj) {
                            ((Callback) obj).onAdViolation("AUTO_REDIRECT", this.f$0);
                        }
                    });
                    return true;
                }
            }

            /* access modifiers changed from: private */
            public /* synthetic */ void c(String str, Callback callback) {
                callback.handleMraidUrl(str, RichMediaWebView.this.e);
            }

            public final void onPageStartedLoading(@NonNull String str) {
                RichMediaWebView.this.a.debug(LogDomain.WIDGET, "onPageStartedLoading: %s", str);
            }

            public final void onPageFinishedLoading(@NonNull String str) {
                RichMediaWebView.this.a.debug(LogDomain.WIDGET, "onPageFinishedLoading: %s", str);
                RichMediaWebView.this.setVisibility(0);
                Objects.onNotNull(RichMediaWebView.this.c, $$Lambda$L14kfwa0l1zsvAGZIyaguiHFWzE.INSTANCE);
            }

            public final void onHttpError(@NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse) {
                RichMediaWebView.this.a.debug(LogDomain.WIDGET, "onHttpError: request = %s, errorResponse = %s", webResourceRequest, webResourceResponse);
            }

            public final void onGeneralError(int i, @NonNull String str, @NonNull String str2) {
                RichMediaWebView.this.a.debug(LogDomain.WIDGET, "onGeneralError: errorCode = %d, description = %s, failingUrl = %s", Integer.valueOf(i), str, str2);
            }

            public final void onRenderProcessGone() {
                Objects.onNotNull(RichMediaWebView.this.c, $$Lambda$l3UkVyLdA8nZg35COCp9BoswidI.INSTANCE);
            }
        });
        setWebViewClient(baseWebViewClient);
        setWebChromeClient(new WebChromeClient() {
            public final boolean onJsAlert(@NonNull WebView webView, @NonNull String str, @NonNull String str2, @NonNull JsResult jsResult) {
                Objects.onNotNull(RichMediaWebView.this.c, $$Lambda$RichMediaWebView$3$ODQfhTklRYZqst_0rAXhu7c1Ik.INSTANCE);
                jsResult.confirm();
                return true;
            }

            public final boolean onJsConfirm(@NonNull WebView webView, @NonNull String str, @NonNull String str2, @NonNull JsResult jsResult) {
                Objects.onNotNull(RichMediaWebView.this.c, $$Lambda$RichMediaWebView$3$sAXDRDeeLdRVjeVV0AHi4Z3fnM.INSTANCE);
                jsResult.confirm();
                return true;
            }

            public final boolean onJsPrompt(@NonNull WebView webView, @NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull JsPromptResult jsPromptResult) {
                Objects.onNotNull(RichMediaWebView.this.c, $$Lambda$RichMediaWebView$3$RQ2k7V5JulSOvMjPYBQRD2RFk.INSTANCE);
                jsPromptResult.confirm();
                return true;
            }

            public final boolean onJsBeforeUnload(@NonNull WebView webView, @NonNull String str, @NonNull String str2, @NonNull JsResult jsResult) {
                Objects.onNotNull(RichMediaWebView.this.c, $$Lambda$RichMediaWebView$3$0uNrhclBAR2NQ6GHGGavehFKePk.INSTANCE);
                jsResult.confirm();
                return true;
            }
        });
        mraidBridgeInterface.init(this, new Consumer() {
            public final void accept(Object obj) {
                RichMediaWebView.this.a((String) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str) {
        if (str.startsWith("smaato://")) {
            post(new Runnable(str) {
                private final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                public final void run() {
                    RichMediaWebView.this.b(this.f$1);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str, Callback callback) {
        callback.handleMraidUrl(str, this.e);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(String str) {
        Objects.onNotNull(this.c, new Consumer(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                RichMediaWebView.this.a(this.f$1, (Callback) obj);
            }
        });
    }

    public final void setCallback(@NonNull Callback callback) {
        this.c = callback;
    }

    @MainThread
    public final void loadData(@NonNull String str, @NonNull MraidEnvironmentProperties mraidEnvironmentProperties) {
        Threads.ensureMainThread();
        if (!this.f) {
            this.f = true;
            a();
            loadHtml(this.b.createHtml(str, getContext(), mraidEnvironmentProperties));
        }
    }

    @MainThread
    public final void loadUrlContent(@NonNull String str) {
        Threads.ensureMainThread();
        if (!this.f) {
            this.f = true;
            a();
            loadUrl(str);
        }
    }

    @MainThread
    public final void destroy() {
        Threads.ensureMainThread();
        if (this.d) {
            this.a.debug(LogDomain.WIDGET, "release() has been already called, ignoring this call", new Object[0]);
            return;
        }
        this.d = true;
        WebViewHelperUtil.resetAndDestroyWebViewSafely(this);
    }

    public final void resetClickedFlag() {
        this.e = false;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    private void a() {
        setOnTouchListener(new OnTouchListener(new GestureDetector(getContext(), new StubOnGestureListener() {
            public final boolean onSingleTapUp(MotionEvent motionEvent) {
                RichMediaWebView.this.e = true;
                Objects.onNotNull(RichMediaWebView.this.c, $$Lambda$qPQpBRepVvgjAy5lJxghA1FVBsc.INSTANCE);
                return true;
            }
        })) {
            private final /* synthetic */ GestureDetector f$0;

            {
                this.f$0 = r1;
            }

            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return this.f$0.onTouchEvent(motionEvent);
            }
        });
    }
}
