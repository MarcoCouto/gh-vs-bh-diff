package com.smaato.sdk.richmedia.widget;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.ViewUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.Views;
import com.smaato.sdk.richmedia.widget.ClosableView.OnCloseClickListener;
import com.smaato.sdk.richmedia.widget.ResizeManager.Listener;

final class ResizeManager {
    @NonNull
    private final Logger a;
    @NonNull
    private final Rect b;
    @NonNull
    private final View c;
    @NonNull
    private final ClosableView d;
    @Nullable
    private Listener e;

    public interface Listener {
        void onCloseClicked(@NonNull ImageButton imageButton);

        void onResizeFailed(@NonNull String str);

        void onResized(@NonNull ImageButton imageButton);
    }

    ResizeManager(@NonNull Logger logger, @NonNull View view, @NonNull Rect rect) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.c = (View) Objects.requireNonNull(view);
        this.b = (Rect) Objects.requireNonNull(rect);
        this.d = new ClosableView(view.getContext());
        this.d.setOnCloseClickListener(new OnCloseClickListener() {
            public final void onCloseClick() {
                ResizeManager.this.d();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onCloseClicked(this.d.getCloseButton());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d() {
        Objects.onNotNull(this.e, new Consumer() {
            public final void accept(Object obj) {
                ResizeManager.this.c((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Rect rect) {
        View rootView = ViewUtils.getRootView(this.c);
        if (!(rootView instanceof ViewGroup)) {
            a("Cannot find a root view for a resizable-view");
            return;
        }
        ViewGroup viewGroup = (ViewGroup) rootView;
        if (!this.d.a(this.b, rect)) {
            a("The close region cannot appear within the maximum allowed size");
            return;
        }
        if (!this.d.a()) {
            ViewUtils.removeFromParent(this.c);
            this.d.a(this.c);
            viewGroup.addView(this.d);
        }
        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) this.d.getLayoutParams();
        marginLayoutParams.width = rect.width();
        marginLayoutParams.height = rect.height();
        marginLayoutParams.topMargin = rect.top;
        marginLayoutParams.leftMargin = rect.left;
        this.d.setLayoutParams(marginLayoutParams);
        Views.addOnPreDrawListener(this.d, new Runnable() {
            public final void run() {
                ResizeManager.this.c();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onResized(this.d.getCloseButton());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c() {
        Objects.onNotNull(this.e, new Consumer() {
            public final void accept(Object obj) {
                ResizeManager.this.b((Listener) obj);
            }
        });
    }

    private void a(@NonNull String str) {
        this.a.error(LogDomain.RICH_MEDIA, str, new Object[0]);
        Objects.onNotNull(this.e, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Listener) obj).onResizeFailed(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        ViewUtils.removeFromParent(this.d);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        Threads.runOnNextUiFrame(new Runnable() {
            public final void run() {
                ResizeManager.this.b();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable Listener listener) {
        this.e = listener;
    }
}
