package com.smaato.sdk.richmedia.util;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.widget.RichMediaWebView;

public interface MraidBridgeInterface {
    void init(@NonNull RichMediaWebView richMediaWebView, @NonNull Consumer<String> consumer);
}
