package com.smaato.sdk.richmedia.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.pm.PackageManager.NameNotFoundException;
import androidx.annotation.NonNull;

public final class ActivityHelper {
    public final boolean isDestroyedOnOrientationChange(@NonNull Activity activity) {
        try {
            int i = activity.getPackageManager().getActivityInfo(new ComponentName(activity, activity.getClass()), 0).configChanges;
            boolean z = (i & 128) != 0;
            boolean z2 = (i & 1024) != 0;
            if (!z || !z2) {
                return true;
            }
            return false;
        } catch (NameNotFoundException unused) {
            return true;
        }
    }

    public final boolean isLockedByUserOrDeveloper(@NonNull Activity activity) {
        return DeviceUtils.a(activity) || a.a(activity);
    }
}
