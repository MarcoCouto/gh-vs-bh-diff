package com.smaato.sdk.richmedia.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings.System;
import android.view.View;
import android.view.Window;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.core.util.Intents;

public final class DeviceUtils {

    public enum ScreenOrientation {
        PORTRAIT,
        LANDSCAPE,
        UNKNOWN
    }

    private static boolean a(int i, int i2) {
        return (i & i2) != 0;
    }

    private DeviceUtils() {
    }

    public static boolean isLocationAvailable(@NonNull AppMetaData appMetaData) {
        return appMetaData.isPermissionGranted("android.permission.ACCESS_FINE_LOCATION") || appMetaData.isPermissionGranted("android.permission.ACCESS_COARSE_LOCATION");
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=android.webkit.WebView, code=android.view.View, for r4v0, types: [android.view.View, android.webkit.WebView] */
    public static boolean isInlineVideoSupported(@NonNull Context context, @NonNull View view) {
        if (context instanceof Activity) {
            while (view != null) {
                if (view.isHardwareAccelerated() && !a(view.getLayerType(), 1)) {
                    if (!(view.getParent() instanceof View)) {
                        break;
                    }
                    view = (View) view.getParent();
                } else {
                    return false;
                }
            }
            Window window = ((Activity) context).getWindow();
            if (window != null) {
                return a(window.getAttributes().flags, 16777216);
            }
        }
        return false;
    }

    public static boolean isSmsAvailable(@NonNull Context context) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("sms:"));
        return Intents.canHandleIntent(context, intent);
    }

    public static boolean isTelAvailable(@NonNull Context context) {
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:"));
        return Intents.canHandleIntent(context, intent);
    }

    @NonNull
    public static ScreenOrientation getScreenOrientation(@NonNull Context context) {
        switch (context.getResources().getConfiguration().orientation) {
            case 1:
                return ScreenOrientation.PORTRAIT;
            case 2:
                return ScreenOrientation.LANDSCAPE;
            default:
                return ScreenOrientation.UNKNOWN;
        }
    }

    public static int getActivityInfoOrientation(@NonNull ScreenOrientation screenOrientation) {
        switch (screenOrientation) {
            case PORTRAIT:
                return 1;
            case LANDSCAPE:
                return 0;
            default:
                return -1;
        }
    }

    public static boolean isOrientationLocked(@NonNull Context context) {
        if (a(context)) {
            return true;
        }
        if (!(context instanceof Activity)) {
            return false;
        }
        return a.b((Activity) context);
    }

    static boolean a(@NonNull Context context) {
        return System.getInt(context.getContentResolver(), "accelerometer_rotation", 0) == 0;
    }
}
