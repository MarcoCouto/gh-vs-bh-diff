package com.smaato.sdk.richmedia.util;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.AssetUtils;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidEnvironmentProperties;
import java.util.regex.Pattern;

public final class RichMediaHtmlUtils {
    @NonNull
    private final Logger a;
    @NonNull
    private final String b;

    public RichMediaHtmlUtils(@NonNull Logger logger, @NonNull String str) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = str;
    }

    @NonNull
    public final String createHtml(@NonNull String str, @NonNull Context context, @NonNull MraidEnvironmentProperties mraidEnvironmentProperties) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(context);
        Objects.requireNonNull(mraidEnvironmentProperties);
        if (Pattern.compile("(?i)<(html|body|head)[^>]*>").matcher(str).find()) {
            this.a.error(LogDomain.RICH_MEDIA, "Rich media HTML content has disallowed tag(s): html, head, or body.", new Object[0]);
            StringBuilder sb = new StringBuilder();
            sb.append(a(context, mraidEnvironmentProperties));
            sb.append(b(context));
            sb.append(str);
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder("<!DOCTYPE html><html lang='en' style='height:100%;'><head><meta name='viewport' content='width=device-width,height=device-height,initial-scale=1.0'/><style>html {height:100%%;}body {margin:0;padding:0;min-height:100%%;}img {display:block;max-height:100vh;max-width:100vw;margin-left:auto;margin-right:auto;}</style>");
        sb2.append(a(context, mraidEnvironmentProperties));
        sb2.append(b(context));
        sb2.append("</head><body>");
        sb2.append(str);
        sb2.append("</body></html>");
        return sb2.toString();
    }

    @NonNull
    private String a(@NonNull Context context, @NonNull MraidEnvironmentProperties mraidEnvironmentProperties) {
        StringBuilder sb = new StringBuilder();
        sb.append(a(context));
        sb.append("<script>");
        sb.append(a(mraidEnvironmentProperties));
        sb.append("</script>");
        return sb.toString();
    }

    @NonNull
    private String a(@NonNull Context context) {
        StringBuilder sb = new StringBuilder("<script>");
        sb.append(String.format(AssetUtils.getFileFromAssets(context, this.a, "mraid.js"), new Object[]{this.b}));
        sb.append("</script>");
        return sb.toString();
    }

    @NonNull
    private String b(@NonNull Context context) {
        StringBuilder sb = new StringBuilder("<script>");
        sb.append(AssetUtils.getFileFromAssets(context, this.a, "omsdk-v1.js"));
        sb.append("</script>");
        return sb.toString();
    }

    @NonNull
    private static String a(@NonNull MraidEnvironmentProperties mraidEnvironmentProperties) {
        StringBuilder sb = new StringBuilder();
        sb.append("window.MRAID_ENV = {\n");
        sb.append(String.format("version:'%s',\n", new Object[]{MraidEnvironmentProperties.VERSION}));
        sb.append(String.format("sdk: '%s',\n", new Object[]{MraidEnvironmentProperties.SDK}));
        sb.append(String.format("sdkVersion: '%s',\n", new Object[]{mraidEnvironmentProperties.sdkVersion}));
        sb.append(String.format("appId: '%s',\n", new Object[]{mraidEnvironmentProperties.appId}));
        Objects.onNotNull(mraidEnvironmentProperties.googleAdId, new Consumer(sb) {
            private final /* synthetic */ StringBuilder f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.append(String.format("ifa: '%s',\n", new Object[]{(String) obj}));
            }
        });
        Objects.onNotNull(mraidEnvironmentProperties.googleDnt, new Consumer(sb) {
            private final /* synthetic */ StringBuilder f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.append(String.format("limitAdTracking: %b,\n", new Object[]{(Boolean) obj}));
            }
        });
        Objects.onNotNull(mraidEnvironmentProperties.coppa, new Consumer(sb) {
            private final /* synthetic */ StringBuilder f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                RichMediaHtmlUtils.a(this.f$0, (Integer) obj);
            }
        });
        sb.append("};");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(StringBuilder sb, Integer num) {
        String str = "coppa: %b,\n";
        boolean z = true;
        Object[] objArr = new Object[1];
        if (num.intValue() != 1) {
            z = false;
        }
        objArr[0] = Boolean.valueOf(z);
        sb.append(String.format(str, objArr));
    }
}
