package com.smaato.sdk.richmedia.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;

final class a {
    static boolean a(@NonNull Activity activity) {
        try {
            return a(activity.getPackageManager().getActivityInfo(new ComponentName(activity, activity.getClass()), 0).screenOrientation, (Supplier<Boolean>) new Supplier(activity) {
                private final /* synthetic */ Activity f$0;

                {
                    this.f$0 = r1;
                }

                public final Object get() {
                    return Boolean.valueOf(a.a(this.f$0, (Function<Activity, Boolean>) $$Lambda$QZ8ax6aLYNgugM_yBocAkVYW4Ko.INSTANCE));
                }
            });
        } catch (NameNotFoundException unused) {
            return true;
        }
    }

    static boolean b(@NonNull Activity activity) {
        return a(activity.getRequestedOrientation(), (Supplier<Boolean>) new Supplier(activity) {
            private final /* synthetic */ Activity f$0;

            {
                this.f$0 = r1;
            }

            public final Object get() {
                return Boolean.valueOf(a.a(this.f$0, (Function<Activity, Boolean>) $$Lambda$wvTsCEDsgV0S_0Tfse8SKaac844.INSTANCE));
            }
        });
    }

    private static boolean a(int i, @NonNull Supplier<Boolean> supplier) {
        if (VERSION.SDK_INT >= 18) {
            return c(i, supplier);
        }
        return b(i, supplier);
    }

    @SuppressLint({"SwitchIntDef"})
    private static boolean b(int i, @NonNull Supplier<Boolean> supplier) {
        switch (i) {
            case 0:
            case 1:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return true;
            case 3:
                return ((Boolean) supplier.get()).booleanValue();
            default:
                return false;
        }
    }

    @RequiresApi(api = 18)
    @SuppressLint({"SwitchIntDef"})
    private static boolean c(int i, @NonNull Supplier<Boolean> supplier) {
        switch (i) {
            case 11:
            case 12:
            case 14:
                return true;
            default:
                return b(i, supplier);
        }
    }

    private static boolean a(@NonNull Activity activity, @NonNull Function<Activity, Boolean> function) {
        if (!activity.isChild()) {
            return false;
        }
        return ((Boolean) function.apply(activity.getParent())).booleanValue();
    }
}
