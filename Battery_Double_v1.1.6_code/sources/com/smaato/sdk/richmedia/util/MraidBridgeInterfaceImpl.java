package com.smaato.sdk.richmedia.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.webkit.JavascriptInterface;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.widget.RichMediaWebView;

@TargetApi(17)
public class MraidBridgeInterfaceImpl implements MraidBridgeInterface {
    @NonNull
    private String a;
    @NonNull
    private Consumer<String> b;

    public MraidBridgeInterfaceImpl(@NonNull String str) {
        this.a = (String) Objects.requireNonNull(str);
    }

    @JavascriptInterface
    public void call(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.b.accept(str);
        }
    }

    @SuppressLint({"AddJavascriptInterface"})
    public void init(@NonNull RichMediaWebView richMediaWebView, @NonNull Consumer<String> consumer) {
        this.b = (Consumer) Objects.requireNonNull(consumer);
        richMediaWebView.addJavascriptInterface(this, (String) Objects.requireNonNull(this.a));
    }
}
