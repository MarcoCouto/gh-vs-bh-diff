package com.smaato.sdk.richmedia.mraid.mvp;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.mvp.BaseView;
import java.lang.ref.WeakReference;

public abstract class BasePresenter<T extends BaseView> implements Presenter<T> {
    @NonNull
    private WeakReference<T> a = new WeakReference<>(null);

    @CallSuper
    public void attachView(@NonNull T t) {
        Threads.ensureMainThread();
        this.a = new WeakReference<>(t);
    }

    @CallSuper
    public void detachView() {
        Threads.ensureMainThread();
        this.a.clear();
    }

    /* access modifiers changed from: protected */
    public final void ifViewAttached(@NonNull Consumer<T> consumer) {
        Objects.requireNonNull(consumer);
        Threads.ensureMainThread();
        BaseView baseView = (BaseView) this.a.get();
        if (baseView != null) {
            consumer.accept(baseView);
        }
    }
}
