package com.smaato.sdk.richmedia.mraid.mvp;

import androidx.annotation.NonNull;

public interface Presenter<T> {
    void attachView(@NonNull T t);

    void destroy();

    void detachView();
}
