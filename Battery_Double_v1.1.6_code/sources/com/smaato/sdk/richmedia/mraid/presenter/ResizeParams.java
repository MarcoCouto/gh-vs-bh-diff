package com.smaato.sdk.richmedia.mraid.presenter;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public final class ResizeParams {
    @NonNull
    public final Rect maxSizeRectInPx;
    @NonNull
    public final Rect resizeRectInPx;

    ResizeParams(@NonNull Rect rect, @NonNull Rect rect2) {
        this.maxSizeRectInPx = (Rect) Objects.requireNonNull(rect);
        this.resizeRectInPx = (Rect) Objects.requireNonNull(rect2);
    }
}
