package com.smaato.sdk.richmedia.mraid.presenter;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.ViewUtils;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.ChangeNotifier.Listener;
import com.smaato.sdk.richmedia.framework.OrientationChangeWatcher;
import com.smaato.sdk.richmedia.mraid.MraidUtils;
import com.smaato.sdk.richmedia.mraid.RepeatableActionScheduler;
import com.smaato.sdk.richmedia.mraid.Views;
import com.smaato.sdk.richmedia.mraid.Views.ViewVisibilityContext;
import com.smaato.sdk.richmedia.mraid.bridge.ErrorListener;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsBridge;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsEvents;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidAppOrientation;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidAudioVolumeLevel;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidExpandProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidExposureProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidLocationProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidOrientationProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidResizeProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.State;
import com.smaato.sdk.richmedia.mraid.dataprovider.PlacementType;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor.Callback;
import com.smaato.sdk.richmedia.mraid.mvp.BasePresenter;
import com.smaato.sdk.richmedia.util.RectUtils;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView;
import com.smaato.sdk.richmedia.widget.RichMediaWebView;
import java.util.List;

public final class MraidPresenterImpl extends BasePresenter<RichMediaAdContentView> implements MraidPresenter {
    /* access modifiers changed from: private */
    @NonNull
    public final MraidInteractor a;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidJsBridge b;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidJsEvents c;
    @NonNull
    private final MraidJsMethods d;
    /* access modifiers changed from: private */
    @NonNull
    public final MraidJsProperties e;
    @NonNull
    private final RepeatableActionScheduler f;
    @NonNull
    private final OrientationChangeWatcher g;
    @NonNull
    private final Listener<MraidOrientationProperties> h;
    /* access modifiers changed from: private */
    @NonNull
    public final OrientationManager i;
    @NonNull
    private final AppBackgroundDetector j;
    @NonNull
    private final AppMetaData k;
    @NonNull
    private final LocationProvider l;
    /* access modifiers changed from: private */
    @Nullable
    public BiConsumer<String, MraidExpandProperties> m;
    @Nullable
    private Consumer<Whatever> n;
    /* access modifiers changed from: private */
    @Nullable
    public Consumer<String> o;
    /* access modifiers changed from: private */
    @Nullable
    public Consumer<ResizeParams> p;
    /* access modifiers changed from: private */
    @Nullable
    public Consumer<Whatever> q;
    @Nullable
    private BiConsumer<String, String> r;
    @Nullable
    private Listener<Whatever> s;
    @NonNull
    private final Callback t = new Callback() {
        public final void processExposureChange(@NonNull MraidExposureProperties mraidExposureProperties) {
            MraidPresenterImpl.this.c.fireExposureChangeEvent(mraidExposureProperties);
        }

        public final void processCurrentAppOrientationChange(@NonNull MraidAppOrientation mraidAppOrientation) {
            MraidPresenterImpl.this.e.setCurrentAppOrientation(mraidAppOrientation);
        }

        public final void processStateChange(@NonNull State state) {
            MraidPresenterImpl.this.c.fireStateChangeEvent(state);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(MraidOrientationProperties mraidOrientationProperties, RichMediaAdContentView richMediaAdContentView) {
            MraidPresenterImpl.this.i.a(richMediaAdContentView.getContext(), mraidOrientationProperties);
        }

        public final void processOrientationPropertiesChange(@NonNull MraidOrientationProperties mraidOrientationProperties) {
            MraidPresenterImpl.this.ifViewAttached(new Consumer(mraidOrientationProperties) {
                private final /* synthetic */ MraidOrientationProperties f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AnonymousClass1.this.a(this.f$1, (RichMediaAdContentView) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(RichMediaAdContentView richMediaAdContentView) {
            MraidPresenterImpl.this.i.a(richMediaAdContentView.getContext());
        }

        public final void processRestoreOriginalOrientation() {
            MraidPresenterImpl.this.ifViewAttached(new Consumer() {
                public final void accept(Object obj) {
                    AnonymousClass1.this.a((RichMediaAdContentView) obj);
                }
            });
        }

        public final void processClose() {
            Objects.onNotNull(MraidPresenterImpl.this.q, $$Lambda$MraidPresenterImpl$1$os4zBoXZCjsNb8qzcFGcfqOmh0.INSTANCE);
        }

        public final void processCurrentPositionChange(@NonNull Rect rect) {
            MraidPresenterImpl.this.e.setCurrentPosition(rect);
            MraidPresenterImpl.this.c.fireSizeChangeEvent(rect);
        }

        public final void processDefaultPositionChange(@NonNull Rect rect) {
            MraidPresenterImpl.this.e.setDefaultPosition(rect);
        }

        public final void processScreenSizeChange(@NonNull Rect rect) {
            MraidPresenterImpl.this.e.setScreenSize(new Size(rect.width(), rect.height()));
        }

        public final void processMaxSizeChange(@NonNull Rect rect) {
            MraidPresenterImpl.this.e.setMaxSize(new Size(rect.width(), rect.height()));
        }

        public final void processClickUrl(@NonNull String str) {
            Objects.onNotNull(MraidPresenterImpl.this.o, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((Consumer) obj).accept(this.f$0);
                }
            });
        }

        public final void processError(@NonNull String str, @NonNull String str2) {
            MraidPresenterImpl.this.c.fireErrorEvent(str, str2);
        }

        public final void processExpand(@Nullable String str) {
            MraidPresenterImpl.this.ifViewAttached(new Consumer((MraidOrientationProperties) MraidPresenterImpl.this.e.getOrientationPropertiesChangeSender().getValue(), str) {
                private final /* synthetic */ MraidOrientationProperties f$1;
                private final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    AnonymousClass1.this.a(this.f$1, this.f$2, (RichMediaAdContentView) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(MraidOrientationProperties mraidOrientationProperties, String str, RichMediaAdContentView richMediaAdContentView) {
            MraidPresenterImpl.this.i.a(richMediaAdContentView.getContext(), mraidOrientationProperties);
            Objects.onNotNull(MraidPresenterImpl.this.m, new Consumer(str) {
                private final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AnonymousClass1.this.a(this.f$1, (BiConsumer) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(String str, BiConsumer biConsumer) {
            biConsumer.accept(str, MraidPresenterImpl.this.e.getExpandProperties());
        }

        public final void processResize(@NonNull Rect rect, @NonNull Rect rect2) {
            MraidResizeProperties resizeProperties = MraidPresenterImpl.this.e.getResizeProperties();
            if (resizeProperties == null) {
                MraidPresenterImpl.this.a.handleFailedToResize("Resize properties should be set before resize");
                return;
            }
            MraidPresenterImpl.this.ifViewAttached(new Consumer(resizeProperties.getRectRelativeToMaxSize(rect, rect2), rect2) {
                private final /* synthetic */ Rect f$1;
                private final /* synthetic */ Rect f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    AnonymousClass1.this.a(this.f$1, this.f$2, (RichMediaAdContentView) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Rect rect, Rect rect2, RichMediaAdContentView richMediaAdContentView) {
            Context context = richMediaAdContentView.getContext();
            Rect mapToPx = RectUtils.mapToPx(context, rect);
            Objects.onNotNull(MraidPresenterImpl.this.p, new Consumer(RectUtils.mapToPx(context, rect2), mapToPx) {
                private final /* synthetic */ Rect f$0;
                private final /* synthetic */ Rect f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    ((Consumer) obj).accept(new ResizeParams(this.f$0, this.f$1));
                }
            });
        }

        public final void processAudioVolumeChange(@NonNull MraidAudioVolumeLevel mraidAudioVolumeLevel) {
            MraidPresenterImpl.this.c.fireAudioVolumeChangeEvent(mraidAudioVolumeLevel);
        }

        public final void processLocationPropertiesChange(@NonNull MraidLocationProperties mraidLocationProperties) {
            MraidPresenterImpl.this.e.setLocation(mraidLocationProperties);
        }

        public final void processPlacementType(@NonNull PlacementType placementType) {
            MraidPresenterImpl.this.e.setPlacementType(placementType);
        }

        public final void processSupportedFeatures(@NonNull List<String> list) {
            MraidPresenterImpl.this.e.setSupportedFeatures(list);
        }

        public final void processViewableChange(boolean z) {
            MraidPresenterImpl.this.c.fireViewableChangeEvent(z);
        }

        public final void processVisibilityParamsCheck() {
            MraidPresenterImpl.this.ifViewAttached(new Consumer() {
                public final void accept(Object obj) {
                    MraidPresenterImpl.this.b((RichMediaAdContentView) obj);
                }
            });
        }

        public final void processLoadCompleted() {
            MraidPresenterImpl.this.b.fireReadyEvent();
        }
    };
    @NonNull
    private final AppBackgroundDetector.Listener u = new AppBackgroundDetector.Listener() {
        public final void onAppEnteredInBackground() {
            MraidPresenterImpl.this.b();
        }

        public final void onAppEnteredInForeground() {
            MraidPresenterImpl.this.a();
        }
    };

    public MraidPresenterImpl(@NonNull MraidInteractor mraidInteractor, @NonNull MraidJsBridge mraidJsBridge, @NonNull MraidJsEvents mraidJsEvents, @NonNull MraidJsMethods mraidJsMethods, @NonNull MraidJsProperties mraidJsProperties, @NonNull RepeatableActionScheduler repeatableActionScheduler, @NonNull OrientationChangeWatcher orientationChangeWatcher, @NonNull OrientationManager orientationManager, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull AppMetaData appMetaData, @NonNull LocationProvider locationProvider) {
        this.a = (MraidInteractor) Objects.requireNonNull(mraidInteractor);
        this.b = (MraidJsBridge) Objects.requireNonNull(mraidJsBridge);
        this.c = (MraidJsEvents) Objects.requireNonNull(mraidJsEvents);
        this.d = (MraidJsMethods) Objects.requireNonNull(mraidJsMethods);
        this.e = (MraidJsProperties) Objects.requireNonNull(mraidJsProperties);
        this.f = (RepeatableActionScheduler) Objects.requireNonNull(repeatableActionScheduler);
        this.g = (OrientationChangeWatcher) Objects.requireNonNull(orientationChangeWatcher);
        this.i = (OrientationManager) Objects.requireNonNull(orientationManager);
        this.j = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.k = (AppMetaData) Objects.requireNonNull(appMetaData);
        this.l = (LocationProvider) Objects.requireNonNull(locationProvider);
        mraidInteractor.getClass();
        this.h = new Listener() {
            public final void onNextValue(Object obj) {
                MraidInteractor.this.handleOrientationPropertiesChange((MraidOrientationProperties) obj);
            }
        };
        appBackgroundDetector.addListener(this.u, false);
        this.a.setCallback(this.t);
        MraidJsProperties mraidJsProperties2 = this.e;
        Callback callback = this.t;
        callback.getClass();
        mraidJsProperties2.setErrorListener(new ErrorListener() {
            public final void onError(String str, String str2) {
                Callback.this.processError(str, str2);
            }
        });
        MraidJsMethods mraidJsMethods2 = this.d;
        MraidInteractor mraidInteractor2 = this.a;
        mraidInteractor2.getClass();
        mraidJsMethods2.setAddEventListenerCallback(new Consumer() {
            public final void accept(Object obj) {
                MraidInteractor.this.handleAddEventListener((String) obj);
            }
        });
        MraidJsMethods mraidJsMethods3 = this.d;
        MraidInteractor mraidInteractor3 = this.a;
        mraidInteractor3.getClass();
        mraidJsMethods3.setOpenCallback(new Consumer() {
            public final void accept(Object obj) {
                MraidInteractor.this.handleUrlOpen((String) obj);
            }
        });
        this.d.setResizeCallback(new Consumer() {
            public final void accept(Object obj) {
                MraidPresenterImpl.this.d((Whatever) obj);
            }
        });
        MraidJsMethods mraidJsMethods4 = this.d;
        MraidInteractor mraidInteractor4 = this.a;
        mraidInteractor4.getClass();
        mraidJsMethods4.setExpandCallback(new Consumer() {
            public final void accept(Object obj) {
                MraidInteractor.this.handleExpand((String) obj);
            }
        });
        this.d.setUnloadCallback(new Consumer() {
            public final void accept(Object obj) {
                MraidPresenterImpl.this.c((Whatever) obj);
            }
        });
        MraidJsMethods mraidJsMethods5 = this.d;
        MraidInteractor mraidInteractor5 = this.a;
        mraidInteractor5.getClass();
        mraidJsMethods5.setPlayVideoCallback(new Consumer() {
            public final void accept(Object obj) {
                MraidInteractor.this.handlePlayVideo((String) obj);
            }
        });
        this.d.setCloseCallback(new Consumer() {
            public final void accept(Object obj) {
                MraidPresenterImpl.this.b((Whatever) obj);
            }
        });
        this.d.setAdViolationCallback(new BiConsumer() {
            public final void accept(Object obj, Object obj2) {
                MraidPresenterImpl.this.a((String) obj, (String) obj2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Whatever whatever) {
        this.a.handleResize(this.e.getResizeProperties());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Whatever whatever) {
        Objects.onNotNull(this.n, new Consumer() {
            public final void accept(Object obj) {
                ((Consumer) obj).accept(Whatever.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Whatever whatever) {
        this.a.handleClose();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str, String str2) {
        Objects.onNotNull(this.r, new Consumer(str, str2) {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((BiConsumer) obj).accept(this.f$0, this.f$1);
            }
        });
    }

    public final void attachView(@NonNull RichMediaAdContentView richMediaAdContentView) {
        super.attachView(richMediaAdContentView);
        this.a.handleSupportedFeaturesChange(MraidUtils.getSupportedFeatures(richMediaAdContentView.getContext(), richMediaAdContentView.getWebView(), this.k));
        RepeatableActionScheduler repeatableActionScheduler = this.f;
        MraidInteractor mraidInteractor = this.a;
        mraidInteractor.getClass();
        repeatableActionScheduler.start(new Runnable() {
            public final void run() {
                MraidInteractor.this.handleVisibilityParamsCheck();
            }
        });
        a();
    }

    public final void detachView() {
        super.detachView();
        this.f.stop();
        b();
    }

    public final void destroy() {
        this.j.deleteListener(this.u);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(RichMediaAdContentView richMediaAdContentView) {
        Context context = richMediaAdContentView.getContext();
        RichMediaWebView webView = richMediaAdContentView.getWebView();
        this.a.handleDefaultPositionChange(RectUtils.mapToDp(context, Views.positionOnScreenOf(richMediaAdContentView)));
        this.a.handleCurrentPositionChange(RectUtils.mapToDp(context, Views.positionOnScreenOf(webView)));
        ViewVisibilityContext visibilityContextRelativeToRoot = Views.visibilityContextRelativeToRoot(webView);
        MraidExposureProperties valueOf = MraidExposureProperties.valueOf(visibilityContextRelativeToRoot.visibilityPercent, RectUtils.mapToDp(context, visibilityContextRelativeToRoot.visibleRect));
        this.a.handleExposureChange(valueOf);
        this.a.handleViewableChange(Views.isViewable(valueOf.exposedPercentage));
        this.a.handleAudioVolumeLevelChange(MraidAudioVolumeLevel.create(context));
        this.a.handleLocationPropertiesChange(MraidLocationProperties.create(this.l));
    }

    public final void onHtmlLoaded() {
        this.a.handleHtmlLoaded();
    }

    public final void handleMraidUrl(@NonNull String str, boolean z) {
        this.b.handleMraidUrl(str, z);
    }

    public final void setOnClickCallback(@Nullable Consumer<String> consumer) {
        this.o = consumer;
    }

    public final void setOnExpandCallback(@Nullable BiConsumer<String, MraidExpandProperties> biConsumer) {
        this.m = biConsumer;
    }

    public final void handleClose() {
        this.a.handleClose();
    }

    public final void setOnCloseCallback(@Nullable Consumer<Whatever> consumer) {
        this.q = consumer;
    }

    public final void setOnUnloadCallback(@Nullable Consumer<Whatever> consumer) {
        this.n = consumer;
    }

    public final void setResizeCallback(@Nullable Consumer<ResizeParams> consumer) {
        this.p = consumer;
    }

    public final void onWasResized() {
        this.a.handleWasResized();
    }

    public final void onFailedToResize(@NonNull String str) {
        this.a.handleFailedToResize(str);
    }

    public final void onFailedToExpand() {
        this.a.handleFailedToExpand();
    }

    public final void onWasExpanded() {
        this.a.handleWasExpanded();
    }

    public final void onWasClosed() {
        this.a.handleWasClosed();
    }

    public final void setAdViolationCallback(@Nullable BiConsumer<String, String> biConsumer) {
        this.r = biConsumer;
    }

    /* access modifiers changed from: private */
    public void a() {
        this.g.enable();
        this.s = new Listener() {
            public final void onNextValue(Object obj) {
                MraidPresenterImpl.this.a((Whatever) obj);
            }
        };
        this.g.getOrientationNotifier().addListener(this.s);
        this.e.getOrientationPropertiesChangeSender().addListener(this.h);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RichMediaAdContentView richMediaAdContentView) {
        Context context = richMediaAdContentView.getContext();
        this.a.handleOrientationChange(MraidAppOrientation.from(context));
        Size displaySizeInDp = UIUtils.getDisplaySizeInDp(context);
        Rect rect = new Rect(0, 0, displaySizeInDp.width, displaySizeInDp.height);
        this.a.handleScreenSizeInDpChange(rect);
        View rootView = ViewUtils.getRootView(richMediaAdContentView);
        if (rootView == null) {
            this.a.handleScreenMaxSizeInDpChange(rect);
            return;
        }
        this.a.handleScreenMaxSizeInDpChange(RectUtils.mapToDp(context, Views.positionOnScreenOf(rootView)));
    }

    /* access modifiers changed from: private */
    public void b() {
        this.g.disable();
        this.g.getOrientationNotifier().removeListener(this.s);
        this.e.getOrientationPropertiesChangeSender().removeListener(this.h);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Whatever whatever) {
        ifViewAttached(new Consumer() {
            public final void accept(Object obj) {
                MraidPresenterImpl.this.a((RichMediaAdContentView) obj);
            }
        });
    }
}
