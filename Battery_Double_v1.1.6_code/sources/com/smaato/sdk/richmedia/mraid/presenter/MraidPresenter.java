package com.smaato.sdk.richmedia.mraid.presenter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidExpandProperties;
import com.smaato.sdk.richmedia.mraid.mvp.Presenter;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView;

public interface MraidPresenter extends Presenter<RichMediaAdContentView> {
    void handleClose();

    void handleMraidUrl(@NonNull String str, boolean z);

    void onFailedToExpand();

    void onFailedToResize(@NonNull String str);

    void onHtmlLoaded();

    void onWasClosed();

    void onWasExpanded();

    void onWasResized();

    void setAdViolationCallback(@Nullable BiConsumer<String, String> biConsumer);

    void setOnClickCallback(@Nullable Consumer<String> consumer);

    void setOnCloseCallback(@Nullable Consumer<Whatever> consumer);

    void setOnExpandCallback(@Nullable BiConsumer<String, MraidExpandProperties> biConsumer);

    void setOnUnloadCallback(@Nullable Consumer<Whatever> consumer);

    void setResizeCallback(@Nullable Consumer<ResizeParams> consumer);
}
