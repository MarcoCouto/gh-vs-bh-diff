package com.smaato.sdk.richmedia.mraid.presenter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidOrientationProperties;
import com.smaato.sdk.richmedia.util.ActivityHelper;
import com.smaato.sdk.richmedia.util.DeviceUtils;
import com.smaato.sdk.richmedia.util.DeviceUtils.ScreenOrientation;

public final class OrientationManager {
    @NonNull
    private final Logger a;
    @NonNull
    private final ActivityHelper b;
    @Nullable
    private Integer c;

    public OrientationManager(@NonNull Logger logger, @NonNull ActivityHelper activityHelper) {
        this.a = logger;
        this.b = activityHelper;
    }

    /* access modifiers changed from: 0000 */
    @MainThread
    public final void a(@NonNull Context context, @NonNull MraidOrientationProperties mraidOrientationProperties) {
        Threads.ensureMainThread();
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            boolean z = false;
            if (this.b.isLockedByUserOrDeveloper(activity)) {
                this.a.error(LogDomain.MRAID, "Won't apply orientation properties. Reason: Activity is locked", new Object[0]);
            } else if (this.b.isDestroyedOnOrientationChange(activity)) {
                this.a.error(LogDomain.MRAID, "Won't apply orientation properties. Reason: Activity might be destroyed on orientation change", new Object[0]);
            } else {
                ScreenOrientation screenOrientation = mraidOrientationProperties.forceOrientation;
                if (screenOrientation == ScreenOrientation.PORTRAIT || screenOrientation == ScreenOrientation.LANDSCAPE) {
                    z = true;
                }
                if (z) {
                    a(activity, screenOrientation);
                } else if (mraidOrientationProperties.allowOrientationChange) {
                    activity.setRequestedOrientation(-1);
                } else {
                    a(activity, DeviceUtils.getScreenOrientation(context));
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @MainThread
    public final void a(@NonNull Context context) {
        Threads.ensureMainThread();
        if (this.c != null && (context instanceof Activity)) {
            ((Activity) context).setRequestedOrientation(this.c.intValue());
            this.c = null;
        }
    }

    private void a(@NonNull Activity activity, @NonNull ScreenOrientation screenOrientation) {
        this.c = Integer.valueOf(activity.getRequestedOrientation());
        activity.setRequestedOrientation(DeviceUtils.getActivityInfoOrientation(screenOrientation));
    }
}
