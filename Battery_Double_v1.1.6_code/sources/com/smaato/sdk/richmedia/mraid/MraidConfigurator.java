package com.smaato.sdk.richmedia.mraid;

import android.content.Context;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.richmedia.ad.RichMediaAdObject;
import com.smaato.sdk.richmedia.framework.OrientationChangeWatcher;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsBridge;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsEvents;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidDataProvider;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.Event;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.State;
import com.smaato.sdk.richmedia.mraid.dataprovider.PlacementType;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor;
import com.smaato.sdk.richmedia.mraid.presenter.MraidPresenter;
import com.smaato.sdk.richmedia.mraid.presenter.MraidPresenterImpl;
import com.smaato.sdk.richmedia.mraid.presenter.OrientationManager;
import com.smaato.sdk.richmedia.util.ActivityHelper;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView;
import com.smaato.sdk.richmedia.widget.RichMediaAdContentView.Callback;
import com.smaato.sdk.richmedia.widget.RichMediaWebView;

public final class MraidConfigurator {
    @NonNull
    private final AppMetaData a;
    @NonNull
    private final AppBackgroundAwareHandler b;
    @NonNull
    private final OrientationChangeWatcher c;
    @NonNull
    private final AppBackgroundDetector d;
    @NonNull
    private final Logger e;
    @NonNull
    private final LocationProvider f;
    @NonNull
    private final MraidStateMachineFactory g;
    @NonNull
    private final RichMediaWebViewFactory h;

    public MraidConfigurator(@NonNull AppMetaData appMetaData, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler, @NonNull OrientationChangeWatcher orientationChangeWatcher, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull Logger logger, @NonNull LocationProvider locationProvider, @NonNull MraidStateMachineFactory mraidStateMachineFactory, @NonNull RichMediaWebViewFactory richMediaWebViewFactory) {
        this.a = (AppMetaData) Objects.requireNonNull(appMetaData);
        this.b = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
        this.c = (OrientationChangeWatcher) Objects.requireNonNull(orientationChangeWatcher);
        this.d = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.f = (LocationProvider) Objects.requireNonNull(locationProvider);
        this.e = (Logger) Objects.requireNonNull(logger);
        this.g = (MraidStateMachineFactory) Objects.requireNonNull(mraidStateMachineFactory);
        this.h = (RichMediaWebViewFactory) Objects.requireNonNull(richMediaWebViewFactory);
    }

    @NonNull
    public final RichMediaAdContentView createViewForBanner(@NonNull Context context, @NonNull RichMediaAdObject richMediaAdObject, @NonNull Callback callback) {
        RichMediaWebView create = this.h.create(context);
        return RichMediaAdContentView.create(this.e, context, richMediaAdObject, callback, this.h, create, createPresenter(create, this.g.newInstanceForBanner(), PlacementType.INLINE));
    }

    @NonNull
    public final RichMediaAdContentView createViewForInterstitial(@NonNull Context context, @NonNull RichMediaAdObject richMediaAdObject, @NonNull Callback callback) {
        RichMediaWebView create = this.h.create(context);
        return RichMediaAdContentView.create(this.e, context, richMediaAdObject, callback, this.h, create, createPresenter(create, this.g.newInstanceForInterstitial(), PlacementType.INTERSTITIAL));
    }

    @VisibleForTesting
    @NonNull
    public final MraidPresenter createPresenter(@NonNull WebView webView, @NonNull StateMachine<Event, State> stateMachine, @NonNull PlacementType placementType) {
        WebView webView2 = webView;
        Context context = webView.getContext();
        PlacementType placementType2 = placementType;
        MraidDataProvider mraidDataProvider = new MraidDataProvider(context, placementType2, (State) stateMachine.getCurrentState(), this.f, MraidUtils.getSupportedFeatures(context, webView2, this.a));
        MraidInteractor mraidInteractor = new MraidInteractor(mraidDataProvider, stateMachine);
        MraidJsBridge mraidJsBridge = new MraidJsBridge(webView2, this.e);
        MraidJsEvents mraidJsEvents = new MraidJsEvents(this.e, mraidJsBridge);
        MraidJsMethods mraidJsMethods = new MraidJsMethods(mraidJsBridge);
        MraidJsProperties mraidJsProperties = new MraidJsProperties(this.e, mraidJsBridge);
        RepeatableActionScheduler repeatableActionScheduler = new RepeatableActionScheduler(this.e, this.b, 200);
        OrientationManager orientationManager = new OrientationManager(this.e, new ActivityHelper());
        MraidPresenterImpl mraidPresenterImpl = new MraidPresenterImpl(mraidInteractor, mraidJsBridge, mraidJsEvents, mraidJsMethods, mraidJsProperties, repeatableActionScheduler, this.c, orientationManager, this.d, this.a, this.f);
        return mraidPresenterImpl;
    }
}
