package com.smaato.sdk.richmedia.mraid;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.util.MraidBridgeInterface;
import com.smaato.sdk.richmedia.util.RichMediaHtmlUtils;
import com.smaato.sdk.richmedia.widget.RichMediaWebView;

public class RichMediaWebViewFactory {
    @NonNull
    private final MraidBridgeInterface a;
    @NonNull
    private final Logger b;
    @NonNull
    private final RichMediaHtmlUtils c;

    public RichMediaWebViewFactory(@NonNull MraidBridgeInterface mraidBridgeInterface, @NonNull Logger logger, @NonNull RichMediaHtmlUtils richMediaHtmlUtils) {
        this.a = (MraidBridgeInterface) Objects.requireNonNull(mraidBridgeInterface);
        this.b = (Logger) Objects.requireNonNull(logger);
        this.c = (RichMediaHtmlUtils) Objects.requireNonNull(richMediaHtmlUtils);
    }

    @NonNull
    public RichMediaWebView create(@NonNull Context context) {
        return new RichMediaWebView(context, this.b, this.c, this.a);
    }
}
