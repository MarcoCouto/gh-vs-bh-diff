package com.smaato.sdk.richmedia.mraid;

import android.content.Context;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.richmedia.util.DeviceUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class MraidUtils {
    private MraidUtils() {
    }

    @NonNull
    public static String[] getAllMraidFeatures() {
        return new String[]{"sms", "tel", MRAIDNativeFeature.CALENDAR, MRAIDNativeFeature.STORE_PICTURE, MRAIDNativeFeature.INLINE_VIDEO, "location", "vpaid"};
    }

    @NonNull
    public static List<String> getSupportedFeatures(@NonNull Context context, @NonNull WebView webView, @NonNull AppMetaData appMetaData) {
        ArrayList arrayList = new ArrayList();
        if (DeviceUtils.isSmsAvailable(context)) {
            arrayList.add("sms");
        }
        if (DeviceUtils.isTelAvailable(context)) {
            arrayList.add("tel");
        }
        if (DeviceUtils.isInlineVideoSupported(context, webView)) {
            arrayList.add(MRAIDNativeFeature.INLINE_VIDEO);
        }
        if (DeviceUtils.isLocationAvailable(appMetaData)) {
            arrayList.add("location");
        }
        return arrayList;
    }

    @Nullable
    public static Integer parseOptInt(@NonNull String str) {
        try {
            return Integer.valueOf(Integer.parseInt(str));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @Nullable
    public static Float parseOptFloat(@NonNull String str) {
        try {
            return Float.valueOf(Float.parseFloat(str));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @NonNull
    public static String format(@NonNull String str, @NonNull Object... objArr) {
        return String.format(Locale.US, str, objArr);
    }
}
