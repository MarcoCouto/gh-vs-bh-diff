package com.smaato.sdk.richmedia.mraid.interactor;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor.Callback;

/* renamed from: com.smaato.sdk.richmedia.mraid.interactor.-$$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg implements Consumer {
    public static final /* synthetic */ $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg INSTANCE = new $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg();

    private /* synthetic */ $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg() {
    }

    public final void accept(Object obj) {
        ((Callback) obj).processVisibilityParamsCheck();
    }
}
