package com.smaato.sdk.richmedia.mraid.interactor;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor.Callback;

/* renamed from: com.smaato.sdk.richmedia.mraid.interactor.-$$Lambda$MraidInteractor$jMTq-Ly2hg9nZDsDaTDVTTyN9-Y reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$MraidInteractor$jMTqLy2hg9nZDsDaTDVTTyN9Y implements Consumer {
    public static final /* synthetic */ $$Lambda$MraidInteractor$jMTqLy2hg9nZDsDaTDVTTyN9Y INSTANCE = new $$Lambda$MraidInteractor$jMTqLy2hg9nZDsDaTDVTTyN9Y();

    private /* synthetic */ $$Lambda$MraidInteractor$jMTqLy2hg9nZDsDaTDVTTyN9Y() {
    }

    public final void accept(Object obj) {
        ((Callback) obj).processError(MraidJsMethods.RESIZE, "Resize properties should be set before resize");
    }
}
