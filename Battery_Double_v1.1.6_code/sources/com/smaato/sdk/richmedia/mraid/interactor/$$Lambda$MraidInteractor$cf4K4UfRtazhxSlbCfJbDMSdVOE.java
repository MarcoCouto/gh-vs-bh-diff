package com.smaato.sdk.richmedia.mraid.interactor;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor.Callback;

/* renamed from: com.smaato.sdk.richmedia.mraid.interactor.-$$Lambda$MraidInteractor$cf4K4UfRtazhxSlbCfJbDMSdVOE reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$MraidInteractor$cf4K4UfRtazhxSlbCfJbDMSdVOE implements Consumer {
    public static final /* synthetic */ $$Lambda$MraidInteractor$cf4K4UfRtazhxSlbCfJbDMSdVOE INSTANCE = new $$Lambda$MraidInteractor$cf4K4UfRtazhxSlbCfJbDMSdVOE();

    private /* synthetic */ $$Lambda$MraidInteractor$cf4K4UfRtazhxSlbCfJbDMSdVOE() {
    }

    public final void accept(Object obj) {
        ((Callback) obj).processError(MraidJsMethods.PLAY_VIDEO, "An empty URL received");
    }
}
