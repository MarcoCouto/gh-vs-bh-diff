package com.smaato.sdk.richmedia.mraid.interactor;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor.Callback;

/* renamed from: com.smaato.sdk.richmedia.mraid.interactor.-$$Lambda$MraidInteractor$b-8QWlqKEebositjV77ts-pus80 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$MraidInteractor$b8QWlqKEebositjV77tspus80 implements Consumer {
    public static final /* synthetic */ $$Lambda$MraidInteractor$b8QWlqKEebositjV77tspus80 INSTANCE = new $$Lambda$MraidInteractor$b8QWlqKEebositjV77tspus80();

    private /* synthetic */ $$Lambda$MraidInteractor$b8QWlqKEebositjV77tspus80() {
    }

    public final void accept(Object obj) {
        ((Callback) obj).processError(MraidJsMethods.OPEN, "An empty URL received");
    }
}
