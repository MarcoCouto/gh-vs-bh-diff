package com.smaato.sdk.richmedia.mraid.interactor;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor.Callback;

/* renamed from: com.smaato.sdk.richmedia.mraid.interactor.-$$Lambda$MraidInteractor$tZqgmaII8Lb8pt7I1Fo7LxJT_7M reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$MraidInteractor$tZqgmaII8Lb8pt7I1Fo7LxJT_7M implements Consumer {
    public static final /* synthetic */ $$Lambda$MraidInteractor$tZqgmaII8Lb8pt7I1Fo7LxJT_7M INSTANCE = new $$Lambda$MraidInteractor$tZqgmaII8Lb8pt7I1Fo7LxJT_7M();

    private /* synthetic */ $$Lambda$MraidInteractor$tZqgmaII8Lb8pt7I1Fo7LxJT_7M() {
    }

    public final void accept(Object obj) {
        ((Callback) obj).processError(MraidJsMethods.RESIZE, "MRAID 3.0 specs violation (4.2.1 Ad States transition: expanded -> resized)");
    }
}
