package com.smaato.sdk.richmedia.mraid.interactor;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.ChangeNotifier.Listener;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsEvents;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidAppOrientation;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidAudioVolumeLevel;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidDataProvider;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidExposureProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidLocationProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidOrientationProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidResizeProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.Event;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.State;
import com.smaato.sdk.richmedia.mraid.dataprovider.PlacementType;
import com.smaato.sdk.richmedia.mraid.interactor.MraidInteractor.Callback;
import java.util.List;

public final class MraidInteractor {
    @NonNull
    private final MraidDataProvider a;
    @NonNull
    private final StateMachine<Event, State> b;
    @Nullable
    private Callback c;
    @NonNull
    private final Listener<MraidExposureProperties> d = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.a((MraidExposureProperties) obj);
        }
    };
    @NonNull
    private final Listener<MraidAppOrientation> e = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.a((MraidAppOrientation) obj);
        }
    };
    @NonNull
    private final Listener<MraidAudioVolumeLevel> f = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.a((MraidAudioVolumeLevel) obj);
        }
    };
    @NonNull
    private final Listener<Rect> g = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.a((Rect) obj);
        }
    };
    @NonNull
    private final Listener<Rect> h = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.b((Rect) obj);
        }
    };
    @NonNull
    private final Listener<Rect> i = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.c((Rect) obj);
        }
    };
    @NonNull
    private final Listener<Rect> j = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.d((Rect) obj);
        }
    };
    @NonNull
    private final Listener<State> k = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.a((State) obj);
        }
    };
    @NonNull
    private final Listener<List<String>> l = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.a((List) obj);
        }
    };
    @NonNull
    private final Listener<Boolean> m = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.a((Boolean) obj);
        }
    };
    @NonNull
    private final Listener<MraidLocationProperties> n = new Listener() {
        public final void onNextValue(Object obj) {
            MraidInteractor.this.a((MraidLocationProperties) obj);
        }
    };
    @Nullable
    private String o;

    public interface Callback {
        void processAudioVolumeChange(@NonNull MraidAudioVolumeLevel mraidAudioVolumeLevel);

        void processClickUrl(@NonNull String str);

        void processClose();

        void processCurrentAppOrientationChange(@NonNull MraidAppOrientation mraidAppOrientation);

        void processCurrentPositionChange(@NonNull Rect rect);

        void processDefaultPositionChange(@NonNull Rect rect);

        void processError(@NonNull String str, @NonNull String str2);

        void processExpand(@Nullable String str);

        void processExposureChange(@NonNull MraidExposureProperties mraidExposureProperties);

        void processLoadCompleted();

        void processLocationPropertiesChange(@NonNull MraidLocationProperties mraidLocationProperties);

        void processMaxSizeChange(@NonNull Rect rect);

        void processOrientationPropertiesChange(@NonNull MraidOrientationProperties mraidOrientationProperties);

        void processPlacementType(@NonNull PlacementType placementType);

        void processResize(@NonNull Rect rect, @NonNull Rect rect2);

        void processRestoreOriginalOrientation();

        void processScreenSizeChange(@NonNull Rect rect);

        void processStateChange(@NonNull State state);

        void processSupportedFeatures(@NonNull List<String> list);

        void processViewableChange(boolean z);

        void processVisibilityParamsCheck();
    }

    public MraidInteractor(@NonNull MraidDataProvider mraidDataProvider, @NonNull StateMachine<Event, State> stateMachine) {
        this.a = (MraidDataProvider) Objects.requireNonNull(mraidDataProvider);
        this.b = (StateMachine) Objects.requireNonNull(stateMachine);
        stateMachine.addListener(new StateMachine.Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                MraidInteractor.this.a((State) obj, (State) obj2, metadata);
            }
        });
        this.a.getOrientationChangeSender().addListener(this.e);
        this.a.getExposureChangeSender().addListener(this.d);
        this.a.getCurrentPositionInDpChangeSender().addListener(this.g);
        this.a.getDefaultPositionInDpChangeSender().addListener(this.h);
        this.a.getScreenSizeInDpSender().addListener(this.i);
        this.a.getMaxSizeInDpChangeSender().addListener(this.j);
        this.a.getAudioVolumeChangeSender().addListener(this.f);
        this.a.getStateChangeSender().addListener(this.k);
        this.a.getSupportedFeatures().addListener(this.l);
        this.a.getViewableChangeSender().addListener(this.m);
        this.a.getLocationPropertiesSender().addListener(this.n);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void f(Callback callback) {
        Rect rect = (Rect) this.a.getMaxSizeInDpChangeSender().getValue();
        callback.processResize((Rect) this.a.getCurrentPositionInDpChangeSender().getValue(), new Rect(0, 0, rect.width(), rect.height()));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(Callback callback) {
        callback.processExpand(this.o);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull State state) {
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                ((Callback) obj).processStateChange(State.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull MraidExposureProperties mraidExposureProperties) {
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                ((Callback) obj).processExposureChange(MraidExposureProperties.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull MraidAppOrientation mraidAppOrientation) {
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                ((Callback) obj).processCurrentAppOrientationChange(MraidAppOrientation.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull MraidAudioVolumeLevel mraidAudioVolumeLevel) {
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                ((Callback) obj).processAudioVolumeChange(MraidAudioVolumeLevel.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Rect rect) {
        Objects.onNotNull(this.c, new Consumer(rect) {
            private final /* synthetic */ Rect f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Callback) obj).processCurrentPositionChange(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull Rect rect) {
        Objects.onNotNull(this.c, new Consumer(rect) {
            private final /* synthetic */ Rect f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Callback) obj).processDefaultPositionChange(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(@NonNull Rect rect) {
        Objects.onNotNull(this.c, new Consumer(rect) {
            private final /* synthetic */ Rect f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Callback) obj).processScreenSizeChange(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void d(@NonNull Rect rect) {
        Objects.onNotNull(this.c, new Consumer(rect) {
            private final /* synthetic */ Rect f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Callback) obj).processMaxSizeChange(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull MraidLocationProperties mraidLocationProperties) {
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                ((Callback) obj).processLocationPropertiesChange(MraidLocationProperties.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Boolean bool) {
        Objects.onNotNull(this.c, new Consumer(bool) {
            private final /* synthetic */ Boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Callback) obj).processViewableChange(this.f$0.booleanValue());
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull List<String> list) {
        Objects.onNotNull(this.c, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Callback) obj).processSupportedFeatures(this.f$0);
            }
        });
    }

    public final void setCallback(@Nullable Callback callback) {
        this.c = callback;
    }

    public final void handleAddEventListener(@Nullable String str) {
        if (MraidJsEvents.AUDIO_VOLUME_CHANGE.equalsIgnoreCase(str)) {
            a((MraidAudioVolumeLevel) this.a.getAudioVolumeChangeSender().getValue());
        }
        if (MraidJsEvents.EXPOSURE_CHANGE.equalsIgnoreCase(str)) {
            a((MraidExposureProperties) this.a.getExposureChangeSender().getValue());
        }
    }

    public final void handleUrlOpen(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            Objects.onNotNull(this.c, $$Lambda$MraidInteractor$b8QWlqKEebositjV77tspus80.INSTANCE);
        } else {
            Objects.onNotNull(this.c, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((Callback) obj).processClickUrl(this.f$0);
                }
            });
        }
    }

    public final void handlePlayVideo(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            Objects.onNotNull(this.c, $$Lambda$MraidInteractor$cf4K4UfRtazhxSlbCfJbDMSdVOE.INSTANCE);
        } else {
            Objects.onNotNull(this.c, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((Callback) obj).processClickUrl(this.f$0);
                }
            });
        }
    }

    public final void handleResize(@Nullable MraidResizeProperties mraidResizeProperties) {
        if (mraidResizeProperties == null) {
            Objects.onNotNull(this.c, $$Lambda$MraidInteractor$jMTqLy2hg9nZDsDaTDVTTyN9Y.INSTANCE);
        } else if (this.b.getCurrentState() == State.EXPANDED) {
            Objects.onNotNull(this.c, $$Lambda$MraidInteractor$tZqgmaII8Lb8pt7I1Fo7LxJT_7M.INSTANCE);
        } else {
            this.b.onEvent(Event.RESIZE);
        }
    }

    public final void handleExposureChange(@NonNull MraidExposureProperties mraidExposureProperties) {
        this.a.getExposureChangeSender().newValue(mraidExposureProperties);
    }

    public final void handleOrientationChange(@NonNull MraidAppOrientation mraidAppOrientation) {
        this.a.getOrientationChangeSender().newValue(mraidAppOrientation);
    }

    public final void handleScreenSizeInDpChange(@NonNull Rect rect) {
        this.a.getScreenSizeInDpSender().newValue(rect);
    }

    public final void handleScreenMaxSizeInDpChange(@NonNull Rect rect) {
        this.a.getMaxSizeInDpChangeSender().newValue(rect);
    }

    public final void handleOrientationPropertiesChange(@NonNull MraidOrientationProperties mraidOrientationProperties) {
        boolean z = false;
        boolean z2 = this.b.getCurrentState() == State.EXPANDED;
        if (this.a.getPlacementType() == PlacementType.INTERSTITIAL) {
            z = true;
        }
        if (z2 || z) {
            Objects.onNotNull(this.c, new Consumer() {
                public final void accept(Object obj) {
                    ((Callback) obj).processOrientationPropertiesChange(MraidOrientationProperties.this);
                }
            });
        }
    }

    public final void handleAudioVolumeLevelChange(@NonNull MraidAudioVolumeLevel mraidAudioVolumeLevel) {
        this.a.getAudioVolumeChangeSender().newValue(mraidAudioVolumeLevel);
    }

    public final void handleExpand(@Nullable String str) {
        if (this.a.getPlacementType() != PlacementType.INTERSTITIAL) {
            this.o = str;
            this.b.onEvent(Event.EXPAND);
        }
    }

    public final void handleDefaultPositionChange(@NonNull Rect rect) {
        this.a.getDefaultPositionInDpChangeSender().newValue(e(rect));
    }

    public final void handleCurrentPositionChange(@NonNull Rect rect) {
        this.a.getCurrentPositionInDpChangeSender().newValue(e(rect));
    }

    public final void handleViewableChange(boolean z) {
        this.a.getViewableChangeSender().newValue(Boolean.valueOf(z));
    }

    @NonNull
    private Rect e(@NonNull Rect rect) {
        Rect rect2 = (Rect) this.a.getMaxSizeInDpChangeSender().getValue();
        Rect rect3 = (Rect) this.a.getScreenSizeInDpSender().getValue();
        int abs = Math.abs(rect3.left - rect2.left);
        int abs2 = Math.abs(rect3.top - rect2.top);
        return new Rect(rect.left - abs, rect.top - abs2, rect.right - abs, rect.bottom - abs2);
    }

    public final void handleSupportedFeaturesChange(@NonNull List<String> list) {
        this.a.getSupportedFeatures().newValue(list);
    }

    public final void handleLocationPropertiesChange(@NonNull MraidLocationProperties mraidLocationProperties) {
        this.a.getLocationPropertiesSender().newValue(mraidLocationProperties);
    }

    public final void handleWasResized() {
        Objects.onNotNull(this.c, $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg.INSTANCE);
        this.b.onEvent(Event.RESIZING_FINISHED);
    }

    public final void handleWasExpanded() {
        Objects.onNotNull(this.c, $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg.INSTANCE);
        this.b.onEvent(Event.EXPANDING_FINISHED);
    }

    public final void handleFailedToResize(@NonNull String str) {
        Objects.onNotNull(this.c, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Callback) obj).processError(MraidJsMethods.RESIZE, this.f$0);
            }
        });
        if (this.b.getCurrentState() == State.RESIZED) {
            Objects.onNotNull(this.c, $$Lambda$mXbheOT_Mbj7Jvu0AjDdR16YLs.INSTANCE);
        }
        this.b.onEvent(Event.ERROR);
    }

    public final void handleFailedToExpand() {
        this.b.onEvent(Event.ERROR);
    }

    public final void handleClose() {
        boolean z = false;
        boolean z2 = this.b.getCurrentState() == State.EXPANDED;
        if (this.a.getPlacementType() == PlacementType.INTERSTITIAL) {
            z = true;
        }
        if (z2 || z) {
            Objects.onNotNull(this.c, $$Lambda$Ne9GUvFZIfYk6obq2KKWVQUj54.INSTANCE);
        }
        this.b.onEvent(Event.CLOSE);
    }

    public final void handleWasClosed() {
        Objects.onNotNull(this.c, $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg.INSTANCE);
        this.b.onEvent(Event.CLOSE_FINISHED);
    }

    public final void handleVisibilityParamsCheck() {
        if (this.b.isTransitionAllowed(Event.VISIBILITY_PARAMS_CHECK)) {
            Objects.onNotNull(this.c, $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg.INSTANCE);
        }
    }

    public final void handleHtmlLoaded() {
        a((MraidAppOrientation) this.a.getOrientationChangeSender().getValue());
        c((Rect) this.a.getScreenSizeInDpSender().getValue());
        d((Rect) this.a.getMaxSizeInDpChangeSender().getValue());
        a((MraidLocationProperties) this.a.getLocationPropertiesSender().getValue());
        Objects.onNotNull(this.c, new Consumer() {
            public final void accept(Object obj) {
                ((Callback) obj).processPlacementType(PlacementType.this);
            }
        });
        a((List) this.a.getSupportedFeatures().getValue());
        a((MraidAudioVolumeLevel) this.a.getAudioVolumeChangeSender().getValue());
        Objects.onNotNull(this.c, $$Lambda$Mvj5VzbjZAQxuXS9_u_OO6qItxg.INSTANCE);
        this.b.onEvent(Event.LOAD_COMPLETE);
        Objects.onNotNull(this.c, $$Lambda$h1vrcoYjg4xUxqLW6SKvleziZlc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(State state, State state2, Metadata metadata) {
        switch (state2) {
            case RESIZE_IN_PROGRESS:
                Objects.onNotNull(this.c, new Consumer() {
                    public final void accept(Object obj) {
                        MraidInteractor.this.f((Callback) obj);
                    }
                });
                return;
            case EXPAND_IN_PROGRESS:
                Objects.onNotNull(this.c, new Consumer() {
                    public final void accept(Object obj) {
                        MraidInteractor.this.e((Callback) obj);
                    }
                });
                this.o = null;
                return;
            case CLOSE_IN_PROGRESS:
                Objects.onNotNull(this.c, $$Lambda$mXbheOT_Mbj7Jvu0AjDdR16YLs.INSTANCE);
                return;
            default:
                this.a.getStateChangeSender().newValue(state2);
                return;
        }
    }
}
