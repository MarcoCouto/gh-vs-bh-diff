package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.richmedia.mraid.MraidUtils;
import com.smaato.sdk.richmedia.mraid.exception.MraidException;
import java.util.ArrayList;
import java.util.Map;

public final class MraidExpandProperties {
    public final int heightDp;
    public final boolean isModal;
    public final int widthDp;

    public static class Builder {
        @Nullable
        private Integer a;
        @Nullable
        private Integer b;

        public Builder(@NonNull Map<String, String> map) {
            Integer num;
            String str = (String) map.get("width");
            Integer num2 = null;
            if (!TextUtils.isEmpty(str)) {
                Float parseOptFloat = MraidUtils.parseOptFloat(str);
                if (parseOptFloat == null) {
                    num = null;
                } else {
                    num = Integer.valueOf(parseOptFloat.intValue());
                }
                this.a = num;
            }
            String str2 = (String) map.get("height");
            if (!TextUtils.isEmpty(str2)) {
                Float parseOptFloat2 = MraidUtils.parseOptFloat(str2);
                if (parseOptFloat2 != null) {
                    num2 = Integer.valueOf(parseOptFloat2.intValue());
                }
                this.b = num2;
            }
        }

        @NonNull
        public MraidExpandProperties build() throws MraidException {
            ArrayList arrayList = new ArrayList();
            if (this.a == null) {
                arrayList.add("width");
            }
            if (this.b == null) {
                arrayList.add("height");
            }
            if (arrayList.isEmpty()) {
                if (this.a.intValue() <= 0) {
                    arrayList.add("width");
                }
                if (this.b.intValue() <= 0) {
                    arrayList.add("height");
                }
                if (arrayList.isEmpty()) {
                    return new MraidExpandProperties(this.a.intValue(), this.b.intValue(), 0);
                }
                StringBuilder sb = new StringBuilder("Invalid parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new MraidException(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder("Missing required parameter(s): ");
            sb2.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
            throw new MraidException(sb2.toString());
        }
    }

    /* synthetic */ MraidExpandProperties(int i, int i2, byte b) {
        this(i, i2);
    }

    private MraidExpandProperties(int i, int i2) {
        this.isModal = true;
        this.widthDp = i;
        this.heightDp = i2;
    }
}
