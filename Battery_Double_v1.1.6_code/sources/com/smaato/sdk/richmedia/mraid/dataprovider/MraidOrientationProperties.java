package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.util.DeviceUtils.ScreenOrientation;

public final class MraidOrientationProperties {
    public final boolean allowOrientationChange;
    @NonNull
    public final ScreenOrientation forceOrientation;

    public MraidOrientationProperties(boolean z, @NonNull ScreenOrientation screenOrientation) {
        this.allowOrientationChange = z;
        this.forceOrientation = (ScreenOrientation) Objects.requireNonNull(screenOrientation);
    }

    @NonNull
    public static MraidOrientationProperties createDefault() {
        return new MraidOrientationProperties(true, ScreenOrientation.UNKNOWN);
    }
}
