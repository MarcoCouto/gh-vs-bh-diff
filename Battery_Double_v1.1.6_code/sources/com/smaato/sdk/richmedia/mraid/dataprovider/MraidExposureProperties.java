package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;

public final class MraidExposureProperties {
    public final float exposedPercentage;
    @NonNull
    public final Rect visibleRectangleInDp;

    private MraidExposureProperties() {
        this(-1.0f, new Rect());
    }

    private MraidExposureProperties(float f, @NonNull Rect rect) {
        this.exposedPercentage = f;
        this.visibleRectangleInDp = rect;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MraidExposureProperties mraidExposureProperties = (MraidExposureProperties) obj;
        return Float.compare(mraidExposureProperties.exposedPercentage, this.exposedPercentage) == 0 && Objects.equals(this.visibleRectangleInDp, mraidExposureProperties.visibleRectangleInDp);
    }

    public final int hashCode() {
        return Objects.hash(Float.valueOf(this.exposedPercentage), this.visibleRectangleInDp);
    }

    @NonNull
    public static MraidExposureProperties valueOf(float f, @NonNull Rect rect) {
        return new MraidExposureProperties(f, rect);
    }

    @NonNull
    static MraidExposureProperties a() {
        return new MraidExposureProperties();
    }
}
