package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.util.DeviceUtils;
import com.smaato.sdk.richmedia.util.DeviceUtils.ScreenOrientation;

public final class MraidAppOrientation {
    public final boolean isLocked;
    @NonNull
    public final ScreenOrientation orientation;

    private MraidAppOrientation(boolean z, @NonNull ScreenOrientation screenOrientation) {
        this.isLocked = z;
        this.orientation = screenOrientation;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MraidAppOrientation mraidAppOrientation = (MraidAppOrientation) obj;
        return this.isLocked == mraidAppOrientation.isLocked && this.orientation == mraidAppOrientation.orientation;
    }

    public final int hashCode() {
        return Objects.hash(Boolean.valueOf(this.isLocked), this.orientation);
    }

    @NonNull
    public static MraidAppOrientation from(@NonNull Context context) {
        return new MraidAppOrientation(DeviceUtils.isOrientationLocked(context), DeviceUtils.getScreenOrientation(context));
    }
}
