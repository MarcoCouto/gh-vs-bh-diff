package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.LatLng;
import com.smaato.sdk.core.datacollector.LocationProvider;

public final class MraidLocationProperties {
    public static final int GPS_LOCATION_SERVICE_TYPE = 1;
    @Nullable
    public final LatLng latLng;

    private MraidLocationProperties(@Nullable LatLng latLng2) {
        this.latLng = latLng2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MraidLocationProperties mraidLocationProperties = (MraidLocationProperties) obj;
        if (this.latLng != null) {
            return this.latLng.equals(mraidLocationProperties.latLng);
        }
        return mraidLocationProperties.latLng == null;
    }

    public final int hashCode() {
        if (this.latLng != null) {
            return this.latLng.hashCode();
        }
        return 0;
    }

    @NonNull
    public static MraidLocationProperties create(@NonNull LocationProvider locationProvider) {
        return new MraidLocationProperties(locationProvider.getLocationData());
    }
}
