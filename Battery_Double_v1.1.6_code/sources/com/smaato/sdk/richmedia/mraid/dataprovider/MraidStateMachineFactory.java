package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Builder;
import java.util.Arrays;

public final class MraidStateMachineFactory {
    @NonNull
    private final State a;

    public enum Event {
        LOAD_COMPLETE,
        CLOSE,
        RESIZE,
        EXPAND,
        ERROR,
        RESIZING_FINISHED,
        EXPANDING_FINISHED,
        CLOSE_FINISHED,
        VISIBILITY_PARAMS_CHECK
    }

    public enum State {
        HIDDEN,
        LOADING,
        DEFAULT,
        RESIZED,
        EXPANDED,
        RESIZE_IN_PROGRESS,
        EXPAND_IN_PROGRESS,
        CLOSE_IN_PROGRESS
    }

    public MraidStateMachineFactory(@NonNull State state) {
        this.a = (State) Objects.requireNonNull(state);
    }

    @NonNull
    public final StateMachine<Event, State> newInstanceForBanner() {
        return new Builder().setInitialState(this.a).addTransition(Event.LOAD_COMPLETE, Arrays.asList(new State[]{State.LOADING, State.DEFAULT})).addTransition(Event.RESIZE, Arrays.asList(new State[]{State.DEFAULT, State.RESIZE_IN_PROGRESS})).addTransition(Event.RESIZE, Arrays.asList(new State[]{State.RESIZED, State.RESIZE_IN_PROGRESS})).addLoopTransition(Event.RESIZE, State.RESIZE_IN_PROGRESS).addTransition(Event.RESIZING_FINISHED, Arrays.asList(new State[]{State.RESIZE_IN_PROGRESS, State.RESIZED})).addTransition(Event.EXPAND, Arrays.asList(new State[]{State.DEFAULT, State.EXPAND_IN_PROGRESS})).addTransition(Event.EXPAND, Arrays.asList(new State[]{State.RESIZED, State.EXPAND_IN_PROGRESS})).addTransition(Event.EXPAND, Arrays.asList(new State[]{State.RESIZE_IN_PROGRESS, State.EXPAND_IN_PROGRESS})).addTransition(Event.EXPANDING_FINISHED, Arrays.asList(new State[]{State.EXPAND_IN_PROGRESS, State.EXPANDED})).addTransition(Event.CLOSE, Arrays.asList(new State[]{State.RESIZED, State.CLOSE_IN_PROGRESS})).addTransition(Event.CLOSE, Arrays.asList(new State[]{State.EXPANDED, State.CLOSE_IN_PROGRESS})).addTransition(Event.ERROR, Arrays.asList(new State[]{State.RESIZE_IN_PROGRESS, State.DEFAULT})).addTransition(Event.ERROR, Arrays.asList(new State[]{State.EXPAND_IN_PROGRESS, State.DEFAULT})).addTransition(Event.CLOSE_FINISHED, Arrays.asList(new State[]{State.CLOSE_IN_PROGRESS, State.DEFAULT})).addLoopTransition(Event.VISIBILITY_PARAMS_CHECK, State.DEFAULT).addLoopTransition(Event.VISIBILITY_PARAMS_CHECK, State.RESIZED).addLoopTransition(Event.VISIBILITY_PARAMS_CHECK, State.EXPANDED).build();
    }

    @NonNull
    public final StateMachine<Event, State> newInstanceForInterstitial() {
        return new Builder().setInitialState(this.a).addTransition(Event.LOAD_COMPLETE, Arrays.asList(new State[]{State.LOADING, State.DEFAULT})).addTransition(Event.CLOSE, Arrays.asList(new State[]{State.DEFAULT, State.HIDDEN})).addLoopTransition(Event.VISIBILITY_PARAMS_CHECK, State.DEFAULT).build();
    }
}
