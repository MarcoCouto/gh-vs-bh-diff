package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.richmedia.mraid.MraidUtils;
import com.smaato.sdk.richmedia.mraid.exception.MraidException;
import com.smaato.sdk.richmedia.util.RectUtils;
import java.util.ArrayList;
import java.util.Map;

public final class MraidResizeProperties {
    private int a;
    private int b;
    private int c;
    private int d;
    private boolean e;

    public static class Builder {
        @Nullable
        private Integer a;
        @Nullable
        private Integer b;
        @Nullable
        private Integer c;
        @Nullable
        private Integer d;
        @NonNull
        private Boolean e = Boolean.FALSE;

        public Builder(@NonNull Map<String, String> map) {
            Integer num;
            Integer num2;
            Integer num3;
            String str = (String) map.get("width");
            Integer num4 = null;
            if (!TextUtils.isEmpty(str)) {
                Float parseOptFloat = MraidUtils.parseOptFloat(str);
                if (parseOptFloat == null) {
                    num3 = null;
                } else {
                    num3 = Integer.valueOf(parseOptFloat.intValue());
                }
                this.a = num3;
            }
            String str2 = (String) map.get("height");
            if (!TextUtils.isEmpty(str2)) {
                Float parseOptFloat2 = MraidUtils.parseOptFloat(str2);
                if (parseOptFloat2 == null) {
                    num2 = null;
                } else {
                    num2 = Integer.valueOf(parseOptFloat2.intValue());
                }
                this.b = num2;
            }
            String str3 = (String) map.get("offsetX");
            if (!TextUtils.isEmpty(str3)) {
                Float parseOptFloat3 = MraidUtils.parseOptFloat(str3);
                if (parseOptFloat3 == null) {
                    num = null;
                } else {
                    num = Integer.valueOf(parseOptFloat3.intValue());
                }
                this.c = num;
            }
            String str4 = (String) map.get("offsetY");
            if (!TextUtils.isEmpty(str4)) {
                Float parseOptFloat4 = MraidUtils.parseOptFloat(str4);
                if (parseOptFloat4 != null) {
                    num4 = Integer.valueOf(parseOptFloat4.intValue());
                }
                this.d = num4;
            }
            String str5 = (String) map.get("allowOffscreen");
            if (!TextUtils.isEmpty(str5)) {
                this.e = Boolean.valueOf(Boolean.parseBoolean(str5));
            }
        }

        @NonNull
        public MraidResizeProperties build() throws MraidException {
            ArrayList arrayList = new ArrayList();
            if (this.a == null) {
                arrayList.add("width");
            }
            if (this.b == null) {
                arrayList.add("height");
            }
            if (this.c == null) {
                arrayList.add("offsetX");
            }
            if (this.d == null) {
                arrayList.add("offsetY");
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new MraidException(sb.toString());
            } else if (this.a.intValue() < 50 || this.b.intValue() < 50) {
                throw new MraidException("Expected resize dimension should be >= 50 dp");
            } else {
                MraidResizeProperties mraidResizeProperties = new MraidResizeProperties(this.a.intValue(), this.b.intValue(), this.c.intValue(), this.d.intValue(), this.e.booleanValue(), 0);
                return mraidResizeProperties;
            }
        }
    }

    /* synthetic */ MraidResizeProperties(int i, int i2, int i3, int i4, boolean z, byte b2) {
        this(i, i2, i3, i4, z);
    }

    private MraidResizeProperties(int i, int i2, int i3, int i4, boolean z) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = z;
    }

    @NonNull
    public final Rect getRectRelativeToMaxSize(@NonNull Rect rect, @NonNull Rect rect2) {
        int i = this.c;
        int i2 = this.d;
        if (!rect.isEmpty()) {
            i += rect.left;
            i2 += rect.top;
        }
        Rect rect3 = new Rect(i, i2, this.a + i, this.b + i2);
        if (this.e) {
            return rect3;
        }
        return RectUtils.adjust(rect3, rect2);
    }
}
