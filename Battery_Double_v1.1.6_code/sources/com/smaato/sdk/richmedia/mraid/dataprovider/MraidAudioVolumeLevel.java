package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.content.Context;
import android.media.AudioManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.exoplayer2.util.MimeTypes;
import com.smaato.sdk.core.util.Objects;
import java.util.Locale;

public final class MraidAudioVolumeLevel {
    @Nullable
    public final String audioVolumeLevel;

    private MraidAudioVolumeLevel(@Nullable String str) {
        this.audioVolumeLevel = str;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Objects.equals(this.audioVolumeLevel, ((MraidAudioVolumeLevel) obj).audioVolumeLevel);
    }

    public final int hashCode() {
        return Objects.hash(this.audioVolumeLevel);
    }

    @NonNull
    public static MraidAudioVolumeLevel create(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager == null) {
            return new MraidAudioVolumeLevel(null);
        }
        return new MraidAudioVolumeLevel(String.format(Locale.US, "%.1f", new Object[]{Float.valueOf((((float) audioManager.getStreamVolume(3)) * 100.0f) / ((float) audioManager.getStreamMaxVolume(3)))}));
    }
}
