package com.smaato.sdk.richmedia.mraid.dataprovider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.api.ApiAdRequest;
import com.smaato.sdk.core.util.Objects;

public final class MraidEnvironmentProperties {
    public static final String SDK = "SmaatoSDK Android";
    public static final String VERSION = "3.0";
    @NonNull
    public final String appId;
    @Nullable
    public final Integer coppa;
    @Nullable
    public final String googleAdId;
    @Nullable
    public final Boolean googleDnt;
    @NonNull
    public final String sdkVersion;

    public static final class Builder {
        @NonNull
        private final String a = SmaatoSdk.getVersion();
        @NonNull
        private final String b;
        @Nullable
        private final String c;
        @Nullable
        private final Boolean d;
        @Nullable
        private final Integer e;

        public Builder(@NonNull String str, @NonNull ApiAdRequest apiAdRequest) {
            Objects.requireNonNull(str);
            Objects.requireNonNull(apiAdRequest);
            this.b = str;
            this.c = apiAdRequest.getGoogleAdId();
            this.d = apiAdRequest.getGoogleDnt();
            this.e = apiAdRequest.getCoppa();
        }

        @NonNull
        public final MraidEnvironmentProperties build() {
            Objects.requireNonNull(this.a);
            Objects.requireNonNull(this.b);
            MraidEnvironmentProperties mraidEnvironmentProperties = new MraidEnvironmentProperties(this.a, this.b, this.c, this.d, this.e, 0);
            return mraidEnvironmentProperties;
        }
    }

    /* synthetic */ MraidEnvironmentProperties(String str, String str2, String str3, Boolean bool, Integer num, byte b) {
        this(str, str2, str3, bool, num);
    }

    private MraidEnvironmentProperties(@NonNull String str, @NonNull String str2, @Nullable String str3, @Nullable Boolean bool, @Nullable Integer num) {
        this.sdkVersion = str;
        this.appId = str2;
        this.googleAdId = str3;
        this.googleDnt = bool;
        this.coppa = num;
    }
}
