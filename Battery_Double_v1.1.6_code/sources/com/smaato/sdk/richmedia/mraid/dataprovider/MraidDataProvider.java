package com.smaato.sdk.richmedia.mraid.dataprovider;

import android.content.Context;
import android.graphics.Rect;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.notifier.ChangeSender;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.State;
import java.util.Collection;
import java.util.List;

public final class MraidDataProvider {
    @NonNull
    private final PlacementType a;
    @NonNull
    private final ChangeSender<MraidAppOrientation> b;
    @NonNull
    private final ChangeSender<MraidExposureProperties> c = ChangeSenderUtils.createUniqueValueChangeSender(MraidExposureProperties.a());
    @NonNull
    private final ChangeSender<MraidAudioVolumeLevel> d;
    @NonNull
    private final ChangeSender<Rect> e;
    @NonNull
    private final ChangeSender<Rect> f;
    @NonNull
    private final ChangeSender<Rect> g;
    @NonNull
    private final ChangeSender<Rect> h;
    @NonNull
    private final ChangeSender<State> i;
    @NonNull
    private final ChangeSender<MraidLocationProperties> j;
    @NonNull
    private final ChangeSender<List<String>> k;
    @NonNull
    private final ChangeSender<Boolean> l;

    public MraidDataProvider(@NonNull Context context, @NonNull PlacementType placementType, @NonNull State state, @NonNull LocationProvider locationProvider, @NonNull List<String> list) {
        Objects.requireNonNull(context);
        Objects.requireNonNull(locationProvider);
        Objects.requireNonNull(list);
        this.a = (PlacementType) Objects.requireNonNull(placementType);
        this.k = ChangeSenderUtils.createUniqueValueChangeSender(Lists.toImmutableList((Collection<T>) list));
        this.b = ChangeSenderUtils.createUniqueValueChangeSender(MraidAppOrientation.from(context));
        this.e = ChangeSenderUtils.createUniqueValueChangeSender(new Rect());
        this.f = ChangeSenderUtils.createUniqueValueChangeSender(new Rect());
        this.g = ChangeSenderUtils.createUniqueValueChangeSender(new Rect());
        Size displaySizeInDp = UIUtils.getDisplaySizeInDp(context);
        this.h = ChangeSenderUtils.createUniqueValueChangeSender(new Rect(0, 0, displaySizeInDp.width, displaySizeInDp.height));
        this.d = ChangeSenderUtils.createUniqueValueChangeSender(MraidAudioVolumeLevel.create(context));
        this.i = ChangeSenderUtils.createUniqueValueChangeSender(state);
        this.j = ChangeSenderUtils.createUniqueValueChangeSender(MraidLocationProperties.create(locationProvider));
        this.l = ChangeSenderUtils.createUniqueValueChangeSender(Boolean.FALSE);
    }

    @NonNull
    public final ChangeSender<MraidAppOrientation> getOrientationChangeSender() {
        return this.b;
    }

    @NonNull
    public final ChangeSender<MraidExposureProperties> getExposureChangeSender() {
        return this.c;
    }

    @NonNull
    public final ChangeSender<Rect> getDefaultPositionInDpChangeSender() {
        return this.e;
    }

    @NonNull
    public final ChangeSender<Rect> getCurrentPositionInDpChangeSender() {
        return this.f;
    }

    @NonNull
    public final ChangeSender<Rect> getMaxSizeInDpChangeSender() {
        return this.g;
    }

    @NonNull
    public final ChangeSender<Rect> getScreenSizeInDpSender() {
        return this.h;
    }

    @NonNull
    public final ChangeSender<MraidAudioVolumeLevel> getAudioVolumeChangeSender() {
        return this.d;
    }

    @NonNull
    public final ChangeSender<State> getStateChangeSender() {
        return this.i;
    }

    @NonNull
    public final ChangeSender<MraidLocationProperties> getLocationPropertiesSender() {
        return this.j;
    }

    @NonNull
    public final PlacementType getPlacementType() {
        return this.a;
    }

    @NonNull
    public final ChangeSender<List<String>> getSupportedFeatures() {
        return this.k;
    }

    @NonNull
    public final ChangeSender<Boolean> getViewableChangeSender() {
        return this.l;
    }
}
