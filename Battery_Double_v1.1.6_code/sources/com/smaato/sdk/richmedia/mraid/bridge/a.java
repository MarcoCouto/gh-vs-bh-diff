package com.smaato.sdk.richmedia.mraid.bridge;

import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.State;
import com.smaato.sdk.richmedia.mraid.dataprovider.PlacementType;
import com.smaato.sdk.richmedia.util.DeviceUtils.ScreenOrientation;

final class a {

    /* renamed from: com.smaato.sdk.richmedia.mraid.bridge.a$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[PlacementType.values().length];
        static final /* synthetic */ int[] b = new int[ScreenOrientation.values().length];
        static final /* synthetic */ int[] c = new int[State.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|(2:5|6)|7|9|10|11|12|13|14|15|17|18|19|20|21|22|23|25|26|27|28|30) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|5|6|7|9|10|11|12|13|14|15|17|18|19|20|21|22|23|25|26|27|28|30) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007a */
        static {
            try {
                c[State.HIDDEN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                c[State.EXPANDED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            c[State.RESIZED.ordinal()] = 3;
            c[State.DEFAULT.ordinal()] = 4;
            c[State.LOADING.ordinal()] = 5;
            b[ScreenOrientation.PORTRAIT.ordinal()] = 1;
            b[ScreenOrientation.LANDSCAPE.ordinal()] = 2;
            try {
                b[ScreenOrientation.UNKNOWN.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            a[PlacementType.INTERSTITIAL.ordinal()] = 1;
            try {
                a[PlacementType.INLINE.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }
}
