package com.smaato.sdk.richmedia.mraid.bridge;

import androidx.annotation.NonNull;

public interface ErrorListener {
    void onError(@NonNull String str, @NonNull String str2);
}
