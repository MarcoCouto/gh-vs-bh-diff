package com.smaato.sdk.richmedia.mraid.bridge;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.ChangeSender;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;
import com.smaato.sdk.richmedia.mraid.MraidUtils;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidAppOrientation;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidExpandProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidExpandProperties.Builder;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidLocationProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidOrientationProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidResizeProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.PlacementType;
import com.smaato.sdk.richmedia.mraid.exception.MraidException;
import com.smaato.sdk.richmedia.util.DeviceUtils.ScreenOrientation;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.List;
import java.util.Map;

public final class MraidJsProperties {
    @NonNull
    private final Logger a;
    @NonNull
    private final MraidJsBridge b;
    @Nullable
    private ErrorListener c;
    @Nullable
    private MraidResizeProperties d;
    @Nullable
    private MraidExpandProperties e;
    @NonNull
    private final ChangeSender<MraidOrientationProperties> f = ChangeSenderUtils.createUniqueValueChangeSender(MraidOrientationProperties.createDefault());

    public MraidJsProperties(@NonNull Logger logger, @NonNull MraidJsBridge mraidJsBridge) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (MraidJsBridge) Objects.requireNonNull(mraidJsBridge);
        this.b.a("setOrientationProperties", new MraidCommandHandler() {
            public final void handle(Map map, boolean z) {
                MraidJsProperties.this.c(map, z);
            }
        });
        this.b.a("setResizeProperties", new MraidCommandHandler() {
            public final void handle(Map map, boolean z) {
                MraidJsProperties.this.b(map, z);
            }
        });
        this.b.a("setExpandProperties", new MraidCommandHandler() {
            public final void handle(Map map, boolean z) {
                MraidJsProperties.this.a(map, z);
            }
        });
    }

    public final void setErrorListener(@Nullable ErrorListener errorListener) {
        this.c = errorListener;
    }

    @NonNull
    public final ChangeSender<MraidOrientationProperties> getOrientationPropertiesChangeSender() {
        return this.f;
    }

    @Nullable
    public final MraidResizeProperties getResizeProperties() {
        return this.d;
    }

    @Nullable
    public final MraidExpandProperties getExpandProperties() {
        return this.e;
    }

    public final void setCurrentAppOrientation(@NonNull MraidAppOrientation mraidAppOrientation) {
        String str;
        String str2 = "window.mraidbridge.setCurrentAppOrientation('%s', %b);";
        Object[] objArr = new Object[2];
        switch (AnonymousClass1.b[mraidAppOrientation.orientation.ordinal()]) {
            case 1:
                str = "portrait";
                break;
            case 2:
                str = "landscape";
                break;
            default:
                str = "none";
                break;
        }
        objArr[0] = str;
        objArr[1] = Boolean.valueOf(mraidAppOrientation.isLocked);
        this.b.a(MraidUtils.format(str2, objArr));
    }

    public final void setCurrentPosition(@NonNull Rect rect) {
        if (!rect.isEmpty()) {
            this.b.a(MraidUtils.format("window.mraidbridge.setCurrentPosition(%d, %d, %d, %d);", Integer.valueOf(rect.left), Integer.valueOf(rect.top), Integer.valueOf(rect.width()), Integer.valueOf(rect.height())));
        }
    }

    public final void setDefaultPosition(@NonNull Rect rect) {
        if (!rect.isEmpty()) {
            this.b.a(MraidUtils.format("window.mraidbridge.setDefaultPosition(%d, %d, %d, %d);", Integer.valueOf(rect.left), Integer.valueOf(rect.top), Integer.valueOf(rect.width()), Integer.valueOf(rect.height())));
        }
    }

    public final void setMaxSize(@NonNull Size size) {
        this.b.a(MraidUtils.format("window.mraidbridge.setMaxSize(%d, %d);", Integer.valueOf(size.width), Integer.valueOf(size.height)));
    }

    public final void setScreenSize(@NonNull Size size) {
        this.b.a(MraidUtils.format("window.mraidbridge.setScreenSize(%d, %d);", Integer.valueOf(size.width), Integer.valueOf(size.height)));
    }

    public final void setLocation(@NonNull MraidLocationProperties mraidLocationProperties) {
        if (mraidLocationProperties.latLng != null) {
            this.b.a(MraidUtils.format("window.mraidbridge.setCurrentLocation(%f, %f, %d, %f, %d);", Double.valueOf(mraidLocationProperties.latLng.getLatitude()), Double.valueOf(mraidLocationProperties.latLng.getLongitude()), Integer.valueOf(1), Float.valueOf(mraidLocationProperties.latLng.getLocationAccuracy()), Long.valueOf(mraidLocationProperties.latLng.getLocationTimestamp() / 1000)));
        }
    }

    public final void setPlacementType(@NonNull PlacementType placementType) {
        String str;
        String str2 = "window.mraidbridge.setPlacementType('%s');";
        try {
            Object[] objArr = new Object[1];
            switch (AnonymousClass1.a[placementType.ordinal()]) {
                case 1:
                    str = VideoType.INTERSTITIAL;
                    break;
                case 2:
                    str = String.INLINE;
                    break;
                default:
                    StringBuilder sb = new StringBuilder("Unknown placement type: ");
                    sb.append(placementType);
                    throw new IllegalArgumentException(sb.toString());
            }
            objArr[0] = str;
            this.b.a(MraidUtils.format(str2, objArr));
        } catch (IllegalArgumentException e2) {
            Logger logger = this.a;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb2 = new StringBuilder("Failed to call MRAID's setPlacementType method, reason: ");
            sb2.append(e2.getMessage());
            logger.error(logDomain, sb2.toString(), new Object[0]);
        }
    }

    public final void setSupportedFeatures(@NonNull List<String> list) {
        String[] allMraidFeatures;
        for (String str : MraidUtils.getAllMraidFeatures()) {
            this.b.a(MraidUtils.format("window.mraidbridge.setSupports('%s', %b);", str, Boolean.valueOf(list.contains(str))));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Map map, boolean z) {
        try {
            this.e = new Builder(map).build();
        } catch (MraidException e2) {
            Logger logger = this.a;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb = new StringBuilder("Failed to handle a command: setExpandProperties, reason: ");
            sb.append(e2.getMessage());
            logger.error(logDomain, sb.toString(), new Object[0]);
            Objects.onNotNull(this.c, new Consumer() {
                public final void accept(Object obj) {
                    ((ErrorListener) obj).onError("setExpandProperties", MraidException.this.getMessage());
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Map map, boolean z) {
        try {
            this.d = new MraidResizeProperties.Builder(map).build();
        } catch (MraidException e2) {
            Logger logger = this.a;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb = new StringBuilder("Failed to handle a command: setResizeProperties, reason: ");
            sb.append(e2.getMessage());
            logger.error(logDomain, sb.toString(), new Object[0]);
            Objects.onNotNull(this.c, new Consumer() {
                public final void accept(Object obj) {
                    ((ErrorListener) obj).onError("setResizeProperties", MraidException.this.getMessage());
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003d, code lost:
        if (r5.equals("landscape") == false) goto L_0x004a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0052  */
    public /* synthetic */ void c(Map map, boolean z) {
        ScreenOrientation screenOrientation;
        String str = (String) map.get("allowOrientationChange");
        char c2 = 1;
        boolean booleanValue = !TextUtils.isEmpty(str) ? Boolean.valueOf(str).booleanValue() : true;
        String str2 = (String) map.get("forceOrientation");
        if (!TextUtils.isEmpty(str2)) {
            int hashCode = str2.hashCode();
            if (hashCode == 729267099) {
                if (str2.equals("portrait")) {
                    c2 = 0;
                    switch (c2) {
                        case 0:
                            screenOrientation = ScreenOrientation.PORTRAIT;
                            break;
                        case 1:
                            screenOrientation = ScreenOrientation.LANDSCAPE;
                            break;
                    }
                }
            } else if (hashCode == 1430647483) {
            }
            c2 = 65535;
            switch (c2) {
                case 0:
                    break;
                case 1:
                    break;
            }
        }
        screenOrientation = ScreenOrientation.UNKNOWN;
        this.f.newValue(new MraidOrientationProperties(booleanValue, screenOrientation));
    }
}
