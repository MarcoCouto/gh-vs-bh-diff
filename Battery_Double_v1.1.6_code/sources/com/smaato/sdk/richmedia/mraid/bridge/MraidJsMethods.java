package com.smaato.sdk.richmedia.mraid.bridge;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.Map;

public final class MraidJsMethods {
    public static final String ADD_EVENT_LISTENER = "addEventListener";
    public static final String CLOSE = "close";
    public static final String EXPAND = "expand";
    public static final String OPEN = "open";
    public static final String PLAY_VIDEO = "playVideo";
    public static final String RESIZE = "resize";
    public static final String UNLOAD = "unload";
    @Nullable
    private Consumer<String> a;
    @Nullable
    private Consumer<String> b;
    @Nullable
    private Consumer<String> c;
    @Nullable
    private Consumer<String> d;
    @Nullable
    private Consumer<Whatever> e;
    @Nullable
    private Consumer<Whatever> f;
    @Nullable
    private Consumer<Whatever> g;
    @Nullable
    private BiConsumer<String, String> h;
    @NonNull
    private final MraidCommandHandler i = new MraidCommandHandler() {
        public final void handle(Map map, boolean z) {
            MraidJsMethods.this.g(map, z);
        }
    };
    @NonNull
    private final MraidCommandHandler j = new MraidCommandHandler() {
        public final void handle(Map map, boolean z) {
            MraidJsMethods.this.f(map, z);
        }
    };
    @NonNull
    private final MraidCommandHandler k = new MraidCommandHandler() {
        public final void handle(Map map, boolean z) {
            MraidJsMethods.this.e(map, z);
        }
    };
    @NonNull
    private final MraidCommandHandler l = new MraidCommandHandler() {
        public final void handle(Map map, boolean z) {
            MraidJsMethods.this.d(map, z);
        }
    };
    @NonNull
    private final MraidCommandHandler m = new MraidCommandHandler() {
        public final void handle(Map map, boolean z) {
            MraidJsMethods.this.c(map, z);
        }
    };
    @NonNull
    private final MraidCommandHandler n = new MraidCommandHandler() {
        public final void handle(Map map, boolean z) {
            MraidJsMethods.this.b(map, z);
        }
    };
    @NonNull
    private final MraidCommandHandler o = new MraidCommandHandler() {
        public final void handle(Map map, boolean z) {
            MraidJsMethods.this.a(map, z);
        }
    };

    /* access modifiers changed from: private */
    public /* synthetic */ void g(Map map, boolean z) {
        if (this.a != null) {
            this.a.accept(map.get("event"));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void f(Map map, boolean z) {
        String str = (String) map.get("url");
        if (!z) {
            Objects.onNotNull(this.h, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((BiConsumer) obj).accept("AUTO_OPEN", this.f$0);
                }
            });
            return;
        }
        if (this.b != null) {
            this.b.accept(str);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(Map map, boolean z) {
        String str = (String) map.get("url");
        if (!z) {
            Objects.onNotNull(this.h, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((BiConsumer) obj).accept("AUTO_EXPAND", this.f$0);
                }
            });
            return;
        }
        if (this.d != null) {
            this.d.accept(str);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Map map, boolean z) {
        String str = (String) map.get("uri");
        if (!z) {
            Objects.onNotNull(this.h, new Consumer(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    ((BiConsumer) obj).accept("AUTO_PLAY", this.f$0);
                }
            });
            return;
        }
        if (this.c != null) {
            this.c.accept(map.get("uri"));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Map map, boolean z) {
        if (this.g != null) {
            this.g.accept(Whatever.INSTANCE);
        }
        Objects.onNotNull(this.h, $$Lambda$MraidJsMethods$YrtYXWcfijdsgQ5Mqt1FAKLLNg.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Map map, boolean z) {
        if (!z) {
            Objects.onNotNull(this.h, $$Lambda$MraidJsMethods$ZZGHjqklukKS0wlr1UDG2wFtp0g.INSTANCE);
            return;
        }
        if (this.e != null) {
            this.e.accept(Whatever.INSTANCE);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Map map, boolean z) {
        if (this.f != null) {
            this.f.accept(Whatever.INSTANCE);
        }
    }

    public MraidJsMethods(@NonNull MraidJsBridge mraidJsBridge) {
        MraidJsBridge mraidJsBridge2 = (MraidJsBridge) Objects.requireNonNull(mraidJsBridge);
        mraidJsBridge2.a(ADD_EVENT_LISTENER, this.i);
        mraidJsBridge2.a(OPEN, this.j);
        mraidJsBridge2.a(PLAY_VIDEO, this.l);
        mraidJsBridge2.a("expand", this.k);
        mraidJsBridge2.a(UNLOAD, this.m);
        mraidJsBridge2.a(RESIZE, this.n);
        mraidJsBridge2.a("close", this.o);
    }

    public final void setAddEventListenerCallback(@Nullable Consumer<String> consumer) {
        this.a = consumer;
    }

    public final void setOpenCallback(@Nullable Consumer<String> consumer) {
        this.b = consumer;
    }

    public final void setPlayVideoCallback(@Nullable Consumer<String> consumer) {
        this.c = consumer;
    }

    public final void setResizeCallback(@Nullable Consumer<Whatever> consumer) {
        this.e = consumer;
    }

    public final void setExpandCallback(@Nullable Consumer<String> consumer) {
        this.d = consumer;
    }

    public final void setUnloadCallback(@Nullable Consumer<Whatever> consumer) {
        this.g = consumer;
    }

    public final void setCloseCallback(@Nullable Consumer<Whatever> consumer) {
        this.f = consumer;
    }

    public final void setAdViolationCallback(@Nullable BiConsumer<String, String> biConsumer) {
        this.h = biConsumer;
    }
}
