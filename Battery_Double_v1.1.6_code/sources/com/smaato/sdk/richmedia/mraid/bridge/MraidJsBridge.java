package com.smaato.sdk.richmedia.mraid.bridge;

import android.net.Uri;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.richmedia.mraid.MraidUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class MraidJsBridge {
    @NonNull
    private final WebView a;
    @NonNull
    private final Logger b;
    @NonNull
    private final Map<String, MraidCommandHandler> c = Collections.synchronizedMap(new HashMap());

    public MraidJsBridge(@NonNull WebView webView, @NonNull Logger logger) {
        this.a = (WebView) Objects.requireNonNull(webView);
        this.b = (Logger) Objects.requireNonNull(logger);
    }

    public final void handleMraidUrl(@NonNull String str, boolean z) {
        Uri parse = Uri.parse(str);
        Objects.onNotNull(parse.getHost(), new Consumer(parse, z) {
            private final /* synthetic */ Uri f$1;
            private final /* synthetic */ boolean f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                MraidJsBridge.this.a(this.f$1, this.f$2, (String) obj);
            }
        });
        a("window.mraidbridge.nativeCallComplete();");
    }

    public final void fireReadyEvent() {
        a("window.mraidbridge.fireReadyEvent();");
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str, @NonNull MraidCommandHandler mraidCommandHandler) {
        if (!TextUtils.isEmpty(str)) {
            this.c.put(str, mraidCommandHandler);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str) {
        Logger logger = this.b;
        LogDomain logDomain = LogDomain.MRAID;
        StringBuilder sb = new StringBuilder("Running script: ");
        sb.append(str);
        logger.info(logDomain, sb.toString(), new Object[0]);
        this.a.loadUrl(MraidUtils.format("javascript:%s", str));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Uri uri, boolean z, String str) {
        HashMap hashMap = new HashMap();
        for (String str2 : uri.getQueryParameterNames()) {
            String queryParameter = uri.getQueryParameter(str2);
            if (queryParameter != null) {
                hashMap.put(str2, queryParameter);
            }
        }
        Map immutableMap = Maps.toImmutableMap(hashMap);
        MraidCommandHandler mraidCommandHandler = (MraidCommandHandler) this.c.get(str);
        if (mraidCommandHandler == null) {
            Logger logger = this.b;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb = new StringBuilder("A handler for command \"");
            sb.append(str);
            sb.append("\" is not registered");
            logger.debug(logDomain, sb.toString(), new Object[0]);
            return;
        }
        mraidCommandHandler.handle(immutableMap, z);
    }
}
