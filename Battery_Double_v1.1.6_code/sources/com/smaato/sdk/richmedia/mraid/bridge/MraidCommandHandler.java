package com.smaato.sdk.richmedia.mraid.bridge;

import androidx.annotation.NonNull;
import java.util.Map;

public interface MraidCommandHandler {
    void handle(@NonNull Map<String, String> map, boolean z);
}
