package com.smaato.sdk.richmedia.mraid.bridge;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.richmedia.mraid.MraidUtils;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidAudioVolumeLevel;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidExposureProperties;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.State;
import com.smaato.sdk.richmedia.util.RectUtils;

public final class MraidJsEvents {
    public static final String AUDIO_VOLUME_CHANGE = "audioVolumeChange";
    public static final String EXPOSURE_CHANGE = "exposureChange";
    @NonNull
    private final Logger a;
    @NonNull
    private final MraidJsBridge b;

    public MraidJsEvents(Logger logger, @NonNull MraidJsBridge mraidJsBridge) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (MraidJsBridge) Objects.requireNonNull(mraidJsBridge);
    }

    public final void fireExposureChangeEvent(@NonNull MraidExposureProperties mraidExposureProperties) {
        if (mraidExposureProperties.exposedPercentage >= 0.0f) {
            this.b.a(MraidUtils.format("window.mraidbridge.fireExposureChangeEvent(%.2f, %s, %s);", Float.valueOf(mraidExposureProperties.exposedPercentage), RectUtils.rectToString(mraidExposureProperties.visibleRectangleInDp), "null"));
        }
    }

    public final void fireAudioVolumeChangeEvent(@NonNull MraidAudioVolumeLevel mraidAudioVolumeLevel) {
        this.b.a(MraidUtils.format("window.mraidbridge.fireAudioVolumeChangeEvent(%s);", mraidAudioVolumeLevel.audioVolumeLevel));
    }

    public final void fireErrorEvent(@NonNull String str, @NonNull String str2) {
        this.b.a(MraidUtils.format("window.mraidbridge.fireErrorEvent('%s', '%s');", str2, str));
    }

    public final void fireStateChangeEvent(@NonNull State state) {
        String str;
        String str2 = "window.mraidbridge.fireStateChangeEvent('%s');";
        try {
            Object[] objArr = new Object[1];
            switch (AnonymousClass1.c[state.ordinal()]) {
                case 1:
                    str = "hidden";
                    break;
                case 2:
                    str = "expanded";
                    break;
                case 3:
                    str = "resized";
                    break;
                case 4:
                    str = "default";
                    break;
                case 5:
                    str = "loading";
                    break;
                default:
                    StringBuilder sb = new StringBuilder("Unknown state: ");
                    sb.append(state);
                    throw new IllegalArgumentException(sb.toString());
            }
            objArr[0] = str;
            this.b.a(MraidUtils.format(str2, objArr));
        } catch (IllegalArgumentException e) {
            Logger logger = this.a;
            LogDomain logDomain = LogDomain.MRAID;
            StringBuilder sb2 = new StringBuilder("Failed to call MRAID's fireStateChangeEvent method, reason: ");
            sb2.append(e.getMessage());
            logger.error(logDomain, sb2.toString(), new Object[0]);
        }
    }

    public final void fireSizeChangeEvent(@NonNull Rect rect) {
        this.b.a(MraidUtils.format("window.mraidbridge.fireSizeChangeEvent(%d, %d);", Integer.valueOf(rect.width()), Integer.valueOf(rect.height())));
    }

    public final void fireViewableChangeEvent(boolean z) {
        this.b.a(MraidUtils.format("window.mraidbridge.fireViewableChangeEvent(%b);", Boolean.valueOf(z)));
    }
}
