package com.smaato.sdk.richmedia.mraid;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.ViewUtils;

public final class Views {

    public static final class ViewVisibilityContext {
        public final float visibilityPercent;
        @NonNull
        public final Rect visibleRect;

        /* synthetic */ ViewVisibilityContext(float f, Rect rect, byte b) {
            this(f, rect);
        }

        private ViewVisibilityContext(float f, @NonNull Rect rect) {
            this.visibilityPercent = f;
            this.visibleRect = rect;
        }
    }

    public static boolean isViewable(float f) {
        return f > 0.0f;
    }

    private Views() {
    }

    @NonNull
    public static Rect positionOnScreenOf(@NonNull View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        return new Rect(i, i2, view.getWidth() + i, view.getHeight() + i2);
    }

    private static boolean a(@NonNull View view) {
        if (view.hasWindowFocus() && view.getWidth() > 0 && view.getHeight() > 0) {
            return view.isShown();
        }
        return false;
    }

    public static void addOnPreDrawListener(@NonNull final View view, @NonNull final Runnable runnable) {
        view.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            public final boolean onPreDraw() {
                view.getViewTreeObserver().removeOnPreDrawListener(this);
                runnable.run();
                return true;
            }
        });
    }

    @NonNull
    public static ViewVisibilityContext visibilityContextRelativeToRoot(@NonNull View view) {
        Rect rect;
        float f;
        Rect rect2;
        View rootView = ViewUtils.getRootView(view);
        if (rootView == null) {
            rect = new Rect();
        } else {
            Rect positionOnScreenOf = positionOnScreenOf(rootView);
            if (!a(view)) {
                rect2 = new Rect();
            } else {
                rect2 = new Rect();
                if (!view.getGlobalVisibleRect(rect2)) {
                    rect2 = new Rect();
                }
            }
            rect2.offset(-positionOnScreenOf.left, -positionOnScreenOf.top);
            rect = rect2;
        }
        float width = (float) (rect.width() * rect.height());
        if (!a(view)) {
            f = 0.0f;
        } else {
            f = 100.0f * (width / ((float) (view.getWidth() * view.getHeight())));
        }
        return new ViewVisibilityContext(f, rect, 0);
    }
}
