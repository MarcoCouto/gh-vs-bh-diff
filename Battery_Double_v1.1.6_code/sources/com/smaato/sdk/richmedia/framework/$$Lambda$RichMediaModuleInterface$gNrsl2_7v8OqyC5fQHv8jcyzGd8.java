package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$gNrsl2_7v8OqyC5fQHv8jcyzGd8 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$gNrsl2_7v8OqyC5fQHv8jcyzGd8 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$gNrsl2_7v8OqyC5fQHv8jcyzGd8 INSTANCE = new $$Lambda$RichMediaModuleInterface$gNrsl2_7v8OqyC5fQHv8jcyzGd8();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$gNrsl2_7v8OqyC5fQHv8jcyzGd8() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(0.01d).visibilityTimeMillis(0).build();
    }
}
