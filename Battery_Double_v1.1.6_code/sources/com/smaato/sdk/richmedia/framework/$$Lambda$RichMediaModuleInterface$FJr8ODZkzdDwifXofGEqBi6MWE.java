package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$FJr8ODZkzdDwifXof-GEqBi6MWE reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$FJr8ODZkzdDwifXofGEqBi6MWE implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$FJr8ODZkzdDwifXofGEqBi6MWE INSTANCE = new $$Lambda$RichMediaModuleInterface$FJr8ODZkzdDwifXofGEqBi6MWE();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$FJr8ODZkzdDwifXofGEqBi6MWE() {
    }

    public final Object get(DiConstructor diConstructor) {
        return RichMediaModuleInterface.k(diConstructor);
    }
}
