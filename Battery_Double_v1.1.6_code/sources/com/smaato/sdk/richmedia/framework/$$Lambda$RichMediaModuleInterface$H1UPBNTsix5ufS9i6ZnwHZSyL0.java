package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$H1UPBNTsix5ufS9i-6ZnwHZSyL0 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$H1UPBNTsix5ufS9i6ZnwHZSyL0 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$H1UPBNTsix5ufS9i6ZnwHZSyL0 INSTANCE = new $$Lambda$RichMediaModuleInterface$H1UPBNTsix5ufS9i6ZnwHZSyL0();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$H1UPBNTsix5ufS9i6ZnwHZSyL0() {
    }

    public final Object get(DiConstructor diConstructor) {
        return RichMediaModuleInterface.l(diConstructor);
    }
}
