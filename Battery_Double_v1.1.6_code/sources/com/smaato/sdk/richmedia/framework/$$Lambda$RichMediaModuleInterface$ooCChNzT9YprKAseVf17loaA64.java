package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$ooCChNzT9YprKAseV-f17loaA64 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$ooCChNzT9YprKAseVf17loaA64 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$ooCChNzT9YprKAseVf17loaA64 INSTANCE = new $$Lambda$RichMediaModuleInterface$ooCChNzT9YprKAseVf17loaA64();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$ooCChNzT9YprKAseVf17loaA64() {
    }

    public final Object get(DiConstructor diConstructor) {
        return RichMediaModuleInterface.i(diConstructor);
    }
}
