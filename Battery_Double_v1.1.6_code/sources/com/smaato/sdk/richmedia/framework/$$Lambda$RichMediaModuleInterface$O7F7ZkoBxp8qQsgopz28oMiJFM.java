package com.smaato.sdk.richmedia.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$O7F7ZkoBxp8qQs-gopz28oMiJFM reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$O7F7ZkoBxp8qQsgopz28oMiJFM implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$O7F7ZkoBxp8qQsgopz28oMiJFM INSTANCE = new $$Lambda$RichMediaModuleInterface$O7F7ZkoBxp8qQsgopz28oMiJFM();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$O7F7ZkoBxp8qQsgopz28oMiJFM() {
    }

    public final Object get(DiConstructor diConstructor) {
        return RichMediaModuleInterface.j(diConstructor);
    }
}
