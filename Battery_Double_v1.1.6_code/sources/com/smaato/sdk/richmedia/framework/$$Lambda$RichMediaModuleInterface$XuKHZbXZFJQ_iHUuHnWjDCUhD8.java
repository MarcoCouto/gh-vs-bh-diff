package com.smaato.sdk.richmedia.framework;

import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.richmedia.framework.-$$Lambda$RichMediaModuleInterface$XuKHZbXZFJQ_iHUuHnWjDCUhD-8 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RichMediaModuleInterface$XuKHZbXZFJQ_iHUuHnWjDCUhD8 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$RichMediaModuleInterface$XuKHZbXZFJQ_iHUuHnWjDCUhD8 INSTANCE = new $$Lambda$RichMediaModuleInterface$XuKHZbXZFJQ_iHUuHnWjDCUhD8();

    private /* synthetic */ $$Lambda$RichMediaModuleInterface$XuKHZbXZFJQ_iHUuHnWjDCUhD8() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(0.01d).visibilityTimeMillis(AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS).build();
    }
}
