package com.smaato.sdk.richmedia.framework;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.notifier.ChangeSender;

public final class OrientationChangeWatcher {
    @NonNull
    private final Logger a;
    @NonNull
    private final a b;

    OrientationChangeWatcher(@NonNull Logger logger, @NonNull a aVar) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (a) Objects.requireNonNull(aVar);
    }

    public final void enable() {
        Threads.runOnUi(new Runnable() {
            public final void run() {
                OrientationChangeWatcher.this.a();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        if (this.b.canDetectOrientation()) {
            this.b.enable();
        } else {
            this.a.error(LogDomain.MRAID, "This Android version cannot detect orientation changes", new Object[0]);
        }
    }

    public final void disable() {
        a aVar = this.b;
        aVar.getClass();
        Threads.runOnUi(new Runnable() {
            public final void run() {
                a.this.disable();
            }
        });
    }

    @NonNull
    public final ChangeSender<Whatever> getOrientationNotifier() {
        return this.b.a();
    }
}
