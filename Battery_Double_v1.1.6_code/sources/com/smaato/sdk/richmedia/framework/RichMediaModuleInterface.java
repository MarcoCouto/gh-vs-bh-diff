package com.smaato.sdk.richmedia.framework;

import android.app.Application;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.datacollector.LocationProvider;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;
import com.smaato.sdk.richmedia.ad.DiRichMediaAdLayer;
import com.smaato.sdk.richmedia.ad.RichMediaAdLoaderPlugin;
import com.smaato.sdk.richmedia.ad.RichMediaAdResponseParser;
import com.smaato.sdk.richmedia.ad.tracker.RichMediaVisibilityTrackerCreator;
import com.smaato.sdk.richmedia.di.DiNames;
import com.smaato.sdk.richmedia.mraid.MraidConfigurator;
import com.smaato.sdk.richmedia.mraid.RichMediaWebViewFactory;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory;
import com.smaato.sdk.richmedia.mraid.dataprovider.MraidStateMachineFactory.State;
import com.smaato.sdk.richmedia.util.MraidBridgeInterface;
import com.smaato.sdk.richmedia.util.MraidBridgeInterfaceImpl;
import com.smaato.sdk.richmedia.util.NoOpMraidBridgeInterface;
import com.smaato.sdk.richmedia.util.RichMediaHtmlUtils;

public class RichMediaModuleInterface implements AdPresenterModuleInterface {
    /* access modifiers changed from: private */
    public static /* synthetic */ MraidBridgeInterface a(MraidBridgeInterface mraidBridgeInterface, DiConstructor diConstructor) {
        return mraidBridgeInterface;
    }

    @NonNull
    public String moduleDiName() {
        return DiNames.MODULE_DI_NAME;
    }

    @NonNull
    public String version() {
        return "21.3.1";
    }

    public boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls) {
        return adFormat == AdFormat.RICH_MEDIA && (cls.isAssignableFrom(InterstitialAdPresenter.class) || cls.isAssignableFrom(BannerAdPresenter.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderPlugin l(DiConstructor diConstructor) {
        return new RichMediaAdLoaderPlugin((AdPresenterNameShaper) diConstructor.get(AdPresenterNameShaper.class), new NullableFunction() {
            public final Object apply(Object obj) {
                return RichMediaModuleInterface.a(DiConstructor.this, (String) obj);
            }
        });
    }

    @NonNull
    public ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory() {
        return $$Lambda$RichMediaModuleInterface$H1UPBNTsix5ufS9i6ZnwHZSyL0.INSTANCE;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder a(DiConstructor diConstructor, String str) {
        return (AdPresenterBuilder) DiAdLayer.tryGetOrNull(diConstructor, str, AdPresenterBuilder.class);
    }

    @Nullable
    public DiRegistry moduleDiRegistry() {
        return DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                RichMediaModuleInterface.this.c((DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(DiRegistry diRegistry) {
        MraidBridgeInterface mraidBridgeInterface;
        diRegistry.registerFactory(moduleDiName(), RichMediaHtmlUtils.class, $$Lambda$RichMediaModuleInterface$FJr8ODZkzdDwifXofGEqBi6MWE.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), MraidConfigurator.class, $$Lambda$RichMediaModuleInterface$O7F7ZkoBxp8qQsgopz28oMiJFM.INSTANCE);
        diRegistry.registerFactory(MraidStateMachineFactory.class, $$Lambda$RichMediaModuleInterface$ooCChNzT9YprKAseVf17loaA64.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), RichMediaAdResponseParser.class, $$Lambda$RichMediaModuleInterface$1uZOuW3zzxO0tTA6PY0R6yGstYo.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), VisibilityPrivateConfig.class, $$Lambda$RichMediaModuleInterface$gNrsl2_7v8OqyC5fQHv8jcyzGd8.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), RichMediaVisibilityTrackerCreator.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return RichMediaModuleInterface.this.f(diConstructor);
            }
        });
        diRegistry.registerFactory("RichMediaModuleInterfaceCLOSE_BUTTON_VISIBILITY_TRACKER_DI_NAME", VisibilityPrivateConfig.class, $$Lambda$RichMediaModuleInterface$XuKHZbXZFJQ_iHUuHnWjDCUhD8.INSTANCE);
        diRegistry.registerFactory("RichMediaModuleInterfaceCLOSE_BUTTON_VISIBILITY_TRACKER_DI_NAME", RichMediaVisibilityTrackerCreator.class, $$Lambda$RichMediaModuleInterface$Vrmue0OdU250pbmx_2jAke9WH9A.INSTANCE);
        diRegistry.addFrom(DiRegistry.of($$Lambda$RichMediaModuleInterface$WwHTLt0U6a6ex1BdjaTJPjjAO4.INSTANCE));
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                RichMediaModuleInterface.this.a((DiRegistry) obj);
            }
        }));
        if (VERSION.SDK_INT >= 17) {
            mraidBridgeInterface = new MraidBridgeInterfaceImpl("smaNativeInterface");
        } else {
            mraidBridgeInterface = new NoOpMraidBridgeInterface();
        }
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                ((DiRegistry) obj).registerFactory(MraidBridgeInterface.class, new ClassFactory() {
                    public final Object get(DiConstructor diConstructor) {
                        return RichMediaModuleInterface.a(MraidBridgeInterface.this, diConstructor);
                    }
                });
            }
        }));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RichMediaHtmlUtils k(DiConstructor diConstructor) {
        return new RichMediaHtmlUtils(DiLogLayer.getLoggerFrom(diConstructor), "smaNativeInterface");
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ MraidConfigurator j(DiConstructor diConstructor) {
        MraidConfigurator mraidConfigurator = new MraidConfigurator((AppMetaData) diConstructor.get(AppMetaData.class), (AppBackgroundAwareHandler) diConstructor.get(AppBackgroundAwareHandler.class), (OrientationChangeWatcher) diConstructor.get(OrientationChangeWatcher.class), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), DiLogLayer.getLoggerFrom(diConstructor), (LocationProvider) diConstructor.get(LocationProvider.class), (MraidStateMachineFactory) diConstructor.get(MraidStateMachineFactory.class), (RichMediaWebViewFactory) diConstructor.get(RichMediaWebViewFactory.class));
        return mraidConfigurator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ MraidStateMachineFactory i(DiConstructor diConstructor) {
        return new MraidStateMachineFactory(State.LOADING);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RichMediaAdResponseParser h(DiConstructor diConstructor) {
        return new RichMediaAdResponseParser(DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ RichMediaVisibilityTrackerCreator f(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get(moduleDiName(), VisibilityPrivateConfig.class);
        RichMediaVisibilityTrackerCreator richMediaVisibilityTrackerCreator = new RichMediaVisibilityTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class));
        return richMediaVisibilityTrackerCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RichMediaVisibilityTrackerCreator d(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get("RichMediaModuleInterfaceCLOSE_BUTTON_VISIBILITY_TRACKER_DI_NAME", VisibilityPrivateConfig.class);
        RichMediaVisibilityTrackerCreator richMediaVisibilityTrackerCreator = new RichMediaVisibilityTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class));
        return richMediaVisibilityTrackerCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(DiRegistry diRegistry) {
        diRegistry.registerFactory(a.class, $$Lambda$RichMediaModuleInterface$zLgCzqXnjqWtGADZ_2fIqK8yvcs.INSTANCE);
        diRegistry.registerFactory(OrientationChangeWatcher.class, $$Lambda$RichMediaModuleInterface$lti8sZ7__5T9WA5m4UhaUY27A.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a c(DiConstructor diConstructor) {
        return new a((Application) diConstructor.get(Application.class), ChangeSenderUtils.createDebounceChangeSender(Whatever.INSTANCE, 500));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ OrientationChangeWatcher b(DiConstructor diConstructor) {
        return new OrientationChangeWatcher(DiLogLayer.getLoggerFrom(diConstructor), (a) diConstructor.get(a.class));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(RichMediaWebViewFactory.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return RichMediaModuleInterface.this.a(diConstructor);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ RichMediaWebViewFactory a(DiConstructor diConstructor) {
        return new RichMediaWebViewFactory((MraidBridgeInterface) diConstructor.get(MraidBridgeInterface.class), DiLogLayer.getLoggerFrom(diConstructor), (RichMediaHtmlUtils) diConstructor.get(moduleDiName(), RichMediaHtmlUtils.class));
    }

    @Nullable
    public DiRegistry moduleAdPresenterDiRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper) {
        return DiRichMediaAdLayer.createRegistry(adPresenterNameShaper, moduleDiName());
    }

    @NonNull
    public ExpectedManifestEntries getExpectedManifestEntries() {
        return ExpectedManifestEntries.EMPTY;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("RichMediaModuleInterface{supportedFormat: ");
        sb.append(AdFormat.RICH_MEDIA);
        sb.append("}");
        return sb.toString();
    }
}
