package com.smaato.sdk.core.network;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.ChangeNotifier;
import com.smaato.sdk.core.util.notifier.ChangeSender;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;

public class ConnectionStatusWatcher {
    private static SomaNetworkCallback a;
    @NonNull
    private final ChangeSender<Whatever> b = ChangeSenderUtils.createDebounceChangeSender(Whatever.INSTANCE, 500);

    private static final class ConnectionBroadcastReceiver extends BroadcastReceiver {
        @NonNull
        private final ChangeSender<Whatever> a;

        /* synthetic */ ConnectionBroadcastReceiver(ChangeSender changeSender, byte b) {
            this(changeSender);
        }

        private ConnectionBroadcastReceiver(@NonNull ChangeSender<Whatever> changeSender) {
            this.a = (ChangeSender) Objects.requireNonNull(changeSender, "Parameter changeSender cannot be null for ConnectionBroadcastReceiver::new");
        }

        public final void onReceive(Context context, Intent intent) {
            this.a.newValue(Whatever.INSTANCE);
        }
    }

    @RequiresApi(28)
    private static final class SomaNetworkCallback extends NetworkCallback {
        @NonNull
        private final ChangeSender<Whatever> a;

        /* synthetic */ SomaNetworkCallback(ChangeSender changeSender, byte b) {
            this(changeSender);
        }

        private SomaNetworkCallback(@NonNull ChangeSender<Whatever> changeSender) {
            this.a = (ChangeSender) Objects.requireNonNull(changeSender, "Parameter changeSender cannot be null for SomaNetworkCallback::new");
        }

        public final void onAvailable(Network network) {
            this.a.newValue(Whatever.INSTANCE);
        }

        public final void onLost(Network network) {
            this.a.newValue(Whatever.INSTANCE);
        }
    }

    ConnectionStatusWatcher(@NonNull Logger logger, @NonNull Application application) {
        Objects.requireNonNull(application, "Parameter application cannot be null for ConnectionStatusWatcher::new");
        Objects.requireNonNull(logger, "Parameter logger cannot be null for ConnectionStatusWatcher::new");
        if (VERSION.SDK_INT >= 28) {
            if (a == null) {
                Objects.onNotNull(application.getSystemService(ConnectivityManager.class), new Consumer() {
                    public final void accept(Object obj) {
                        ConnectionStatusWatcher.this.a((ConnectivityManager) obj);
                    }
                });
            }
            return;
        }
        application.registerReceiver(new ConnectionBroadcastReceiver(this.b, 0), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(ConnectivityManager connectivityManager) {
        a = new SomaNetworkCallback(this.b, 0);
        connectivityManager.registerDefaultNetworkCallback(a);
    }

    @NonNull
    public ChangeNotifier<Whatever> getStatusNotifier() {
        return this.b;
    }
}
