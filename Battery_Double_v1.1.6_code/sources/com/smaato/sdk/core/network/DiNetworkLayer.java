package com.smaato.sdk.core.network;

import android.app.Application;
import android.net.ConnectivityManager;
import android.os.Build.VERSION;
import android.security.NetworkSecurityPolicy;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.CoreDiNames;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.network.execution.ClickThroughUrlRedirectResolver;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.Executioner;
import com.smaato.sdk.core.network.execution.HttpTasksExecutioner;
import com.smaato.sdk.core.network.execution.HttpsOnlyPolicy;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.network.trackers.BeaconTrackerAdQualityViolationUtils;
import com.smaato.sdk.core.network.trackers.BeaconsExecutioner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Optional;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.violationreporter.AdQualityViolationReporter;
import java.util.concurrent.ExecutorService;

public final class DiNetworkLayer {
    private DiNetworkLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiNetworkLayer$0KkAUa1oFx1ed93mTDrahCZN07E.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(NetworkClient.class, $$Lambda$DiNetworkLayer$J4i5PuIWcmCKot8luoPM1Tcv5w.INSTANCE);
        diRegistry.registerFactory("network", UrlCreator.class, $$Lambda$DiNetworkLayer$rgfsNMuTBImQnPtDLtJI0b_im_c.INSTANCE);
        diRegistry.registerFactory("network", Executioner.class, $$Lambda$DiNetworkLayer$WpJfwU69T5bZpu9iZ2rgL1AIRVY.INSTANCE);
        diRegistry.registerSingletonFactory("network", ExecutorService.class, $$Lambda$DiNetworkLayer$SETjXja6vxDDXQXLwMJLzbe4S4.INSTANCE);
        diRegistry.registerSingletonFactory(NetworkStateMonitor.class, $$Lambda$DiNetworkLayer$zCzlgKwaS5IMXyJIBHN7DjeCCCc.INSTANCE);
        diRegistry.registerSingletonFactory(ConnectionStatusWatcher.class, $$Lambda$DiNetworkLayer$QlxZiskukNb4vBJgHY3DYYvzjXA.INSTANCE);
        diRegistry.registerFactory("network", ConnectivityManager.class, $$Lambda$DiNetworkLayer$7Nmwl8HpVi8SD9RNlkJFtCvQPSk.INSTANCE);
        diRegistry.registerSingletonFactory("network", BeaconTracker.class, $$Lambda$DiNetworkLayer$kB8zo5Cz0JTylJm_Pl22uDEqCPE.INSTANCE);
        diRegistry.registerSingletonFactory(BeaconTrackerAdQualityViolationUtils.class, $$Lambda$DiNetworkLayer$bfeONQzs9fGsrHJLTRQUE1efYZg.INSTANCE);
        diRegistry.registerSingletonFactory("network", BeaconsExecutioner.class, $$Lambda$DiNetworkLayer$WDXVzL26qt1aud6xCp0S2l6OsM.INSTANCE);
        diRegistry.registerSingletonFactory(NetworkActions.class, $$Lambda$DiNetworkLayer$4Fj39kws7aVXceL4caS0_tkhVo0.INSTANCE);
        diRegistry.registerSingletonFactory(ClickThroughUrlRedirectResolver.class, $$Lambda$DiNetworkLayer$qs_Fzwvh0_vFPzisfNlQ0Y5mCZ8.INSTANCE);
        diRegistry.registerSingletonFactory(HttpsOnlyPolicy.class, $$Lambda$DiNetworkLayer$o_06eZdwJ2GN74gXVJlWoIHX3GE.INSTANCE);
        diRegistry.registerSingletonFactory("networkSecurityPolicy", Optional.class, $$Lambda$DiNetworkLayer$_UyjgtRXrj1DGNFcigHrZZ_eqcA.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ NetworkClient n(DiConstructor diConstructor) {
        return new NetworkHttpClient(DiLogLayer.getLoggerFrom(diConstructor), (Executioner) diConstructor.get("network", Executioner.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ UrlCreator m(DiConstructor diConstructor) {
        return new UrlCreator();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Executioner l(DiConstructor diConstructor) {
        return new HttpTasksExecutioner(DiLogLayer.getLoggerFrom(diConstructor), getNetworkActionsFrom(diConstructor), (ExecutorService) diConstructor.get("network", ExecutorService.class), ErrorMapper.NETWORK_LAYER_EXCEPTION);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ NetworkStateMonitor j(DiConstructor diConstructor) {
        return new NetworkStateMonitor((ConnectivityManager) diConstructor.get("network", ConnectivityManager.class), (ConnectionStatusWatcher) diConstructor.get(ConnectionStatusWatcher.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConnectionStatusWatcher i(DiConstructor diConstructor) {
        return new ConnectionStatusWatcher(DiLogLayer.getLoggerFrom(diConstructor), (Application) diConstructor.get(Application.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConnectivityManager h(DiConstructor diConstructor) {
        return (ConnectivityManager) Objects.requireNonNull((ConnectivityManager) ((Application) diConstructor.get(Application.class)).getSystemService("connectivity"));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BeaconTracker g(DiConstructor diConstructor) {
        return new BeaconTracker(DiLogLayer.getLoggerFrom(diConstructor), (BeaconsExecutioner) diConstructor.get("network", BeaconsExecutioner.class), (BeaconTrackerAdQualityViolationUtils) diConstructor.get(BeaconTrackerAdQualityViolationUtils.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BeaconTrackerAdQualityViolationUtils f(DiConstructor diConstructor) {
        return new BeaconTrackerAdQualityViolationUtils((AdQualityViolationReporter) diConstructor.get(AdQualityViolationReporter.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BeaconsExecutioner e(DiConstructor diConstructor) {
        return new BeaconsExecutioner(DiLogLayer.getLoggerFrom(diConstructor), getNetworkActionsFrom(diConstructor), ErrorMapper.IDENTITY, (ExecutorService) diConstructor.get("network", ExecutorService.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ NetworkActions d(DiConstructor diConstructor) {
        return new NetworkActions(DiLogLayer.getLoggerFrom(diConstructor), (UrlCreator) diConstructor.get("network", UrlCreator.class), getNetworkStateMonitorFrom(diConstructor), getRedirectPolicyFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ClickThroughUrlRedirectResolver c(DiConstructor diConstructor) {
        return new ClickThroughUrlRedirectResolver(DiLogLayer.getLoggerFrom(diConstructor), getNetworkActionsFrom(diConstructor), getUrlCreatorFrom(diConstructor), getRedirectPolicyFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ HttpsOnlyPolicy b(DiConstructor diConstructor) {
        return new HttpsOnlyPolicy(DiLogLayer.getLoggerFrom(diConstructor), Lists.of((T[]) new String[]{(String) diConstructor.get(CoreDiNames.SOMA_API_URL, String.class), (String) diConstructor.get(CoreDiNames.SOMA_VIOLATIONS_AGGREGATOR_URL, String.class)}), getUrlCreatorFrom(diConstructor), getNetworkSecurityPolicyOptional(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Optional a(DiConstructor diConstructor) {
        if (VERSION.SDK_INT >= 23) {
            return Optional.of(NetworkSecurityPolicy.getInstance());
        }
        return Optional.empty();
    }

    @NonNull
    public static ExecutorService getNetworkingExecutorServiceFrom(@NonNull DiConstructor diConstructor) {
        return (ExecutorService) diConstructor.get("network", ExecutorService.class);
    }

    @NonNull
    public static HttpsOnlyPolicy getRedirectPolicyFrom(@NonNull DiConstructor diConstructor) {
        return (HttpsOnlyPolicy) diConstructor.get(HttpsOnlyPolicy.class);
    }

    @NonNull
    public static BeaconTracker getBeaconTrackerFrom(@NonNull DiConstructor diConstructor) {
        return (BeaconTracker) diConstructor.get("network", BeaconTracker.class);
    }

    @NonNull
    public static UrlCreator getUrlCreatorFrom(@NonNull DiConstructor diConstructor) {
        return (UrlCreator) diConstructor.get("network", UrlCreator.class);
    }

    @NonNull
    public static NetworkStateMonitor getNetworkStateMonitorFrom(@NonNull DiConstructor diConstructor) {
        return (NetworkStateMonitor) diConstructor.get(NetworkStateMonitor.class);
    }

    @NonNull
    public static NetworkActions getNetworkActionsFrom(@NonNull DiConstructor diConstructor) {
        return (NetworkActions) diConstructor.get(NetworkActions.class);
    }

    @NonNull
    public static Optional<NetworkSecurityPolicy> getNetworkSecurityPolicyOptional(@NonNull DiConstructor diConstructor) {
        return (Optional) diConstructor.get("networkSecurityPolicy", Optional.class);
    }

    @NonNull
    public static ClickThroughUrlRedirectResolver getUrlRedirectResolverFrom(@NonNull DiConstructor diConstructor) {
        return (ClickThroughUrlRedirectResolver) diConstructor.get(ClickThroughUrlRedirectResolver.class);
    }
}
