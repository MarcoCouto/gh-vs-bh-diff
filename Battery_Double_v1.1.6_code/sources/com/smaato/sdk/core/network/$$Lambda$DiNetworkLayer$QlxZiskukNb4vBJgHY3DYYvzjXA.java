package com.smaato.sdk.core.network;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.network.-$$Lambda$DiNetworkLayer$QlxZiskukNb4vBJgHY3DYYvzjXA reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiNetworkLayer$QlxZiskukNb4vBJgHY3DYYvzjXA implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiNetworkLayer$QlxZiskukNb4vBJgHY3DYYvzjXA INSTANCE = new $$Lambda$DiNetworkLayer$QlxZiskukNb4vBJgHY3DYYvzjXA();

    private /* synthetic */ $$Lambda$DiNetworkLayer$QlxZiskukNb4vBJgHY3DYYvzjXA() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiNetworkLayer.i(diConstructor);
    }
}
