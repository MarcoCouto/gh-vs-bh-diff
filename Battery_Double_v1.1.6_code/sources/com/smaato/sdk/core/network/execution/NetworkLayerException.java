package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkComponentException;

public final class NetworkLayerException extends Exception implements SdkComponentException<Error> {
    @NonNull
    private final Error a;
    @NonNull
    private final Exception b;

    public NetworkLayerException(@NonNull Error error, @NonNull Exception exc) {
        super(exc);
        this.a = (Error) Objects.requireNonNull(error);
        this.b = (Exception) Objects.requireNonNull(exc);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("NetworkLayerException { errorType = ");
        sb.append(this.a);
        sb.append(", reason = ");
        sb.append(this.b);
        sb.append(" }");
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NetworkLayerException networkLayerException = (NetworkLayerException) obj;
        return this.a == networkLayerException.a && Objects.equals(this.b, networkLayerException.b);
    }

    public final int hashCode() {
        return Objects.hash(this.a, this.b);
    }

    @NonNull
    public final Exception getReason() {
        return this.b;
    }

    @NonNull
    public final Error getErrorType() {
        return this.a;
    }
}
