package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import java.net.HttpURLConnection;

public interface RedirectConnection {
    @NonNull
    TaskStepResult<HttpURLConnection, Exception> go(@NonNull String str, int i);
}
