package com.smaato.sdk.core.network.execution;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.NetworkResponse;
import com.smaato.sdk.core.util.Objects;
import java.util.concurrent.ExecutorService;

public class HttpTasksExecutioner implements Executioner<NetworkRequest, NetworkResponse, NetworkLayerException> {
    @NonNull
    private final Logger a;
    @NonNull
    private final ExecutorService b;
    @NonNull
    private final NetworkActions c;
    @NonNull
    private final ErrorMapper<NetworkLayerException> d;

    public HttpTasksExecutioner(@NonNull Logger logger, @NonNull NetworkActions networkActions, @NonNull ExecutorService executorService, @NonNull ErrorMapper<NetworkLayerException> errorMapper) {
        this.c = (NetworkActions) Objects.requireNonNull(networkActions, "Parameter networkActions cannot be null for HttpTasksExecutioner::new");
        this.a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for HttpTasksExecutioner::new");
        this.b = (ExecutorService) Objects.requireNonNull(executorService, "Parameter executorService cannot be null for HttpTasksExecutioner::new");
        this.d = (ErrorMapper) Objects.requireNonNull(errorMapper, "Parameter errorMapper cannot be null for HttpTasksExecutioner::new");
    }

    @NonNull
    public Task submitRequest(@NonNull NetworkRequest networkRequest, @Nullable SomaApiContext somaApiContext, @NonNull Listener<NetworkResponse, NetworkLayerException> listener) {
        return HttpTask.a(this.a, this.c, this.b, networkRequest, this.d, listener);
    }
}
