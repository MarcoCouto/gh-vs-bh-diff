package com.smaato.sdk.core.network.exception;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public class HttpsOnlyPolicyViolationException extends Exception {
    @NonNull
    public final String violatedUrl;

    public HttpsOnlyPolicyViolationException(@NonNull String str) {
        this.violatedUrl = (String) Objects.requireNonNull(str);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HttpsOnlyPolicyViolationException{violatedUrl='");
        sb.append(this.violatedUrl);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
