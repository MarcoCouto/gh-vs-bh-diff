package com.smaato.sdk.core.network;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.core.network.-$$Lambda$DiNetworkLayer$SETjXja6vxDDXQXLwMJ-Lzbe4S4 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiNetworkLayer$SETjXja6vxDDXQXLwMJLzbe4S4 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiNetworkLayer$SETjXja6vxDDXQXLwMJLzbe4S4 INSTANCE = new $$Lambda$DiNetworkLayer$SETjXja6vxDDXQXLwMJLzbe4S4();

    private /* synthetic */ $$Lambda$DiNetworkLayer$SETjXja6vxDDXQXLwMJLzbe4S4() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newSingleThreadExecutor();
    }
}
