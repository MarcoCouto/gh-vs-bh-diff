package com.smaato.sdk.core.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class NetworkHttpResponse implements NetworkResponse {
    private int a;
    @NonNull
    private Map<String, List<String>> b;
    @Nullable
    private byte[] c;
    @NonNull
    private final String d;

    public NetworkHttpResponse(int i, @NonNull Map<String, List<String>> map, @Nullable byte[] bArr, @NonNull String str) {
        this.a = i;
        this.b = (Map) Objects.requireNonNull(map);
        this.c = bArr;
        this.d = (String) Objects.requireNonNull(str);
    }

    public int getResponseCode() {
        return this.a;
    }

    @NonNull
    public Map<String, List<String>> getHeaders() {
        return this.b;
    }

    @Nullable
    public byte[] getBody() {
        return this.c;
    }

    @NonNull
    public String getRequestUrl() {
        return this.d;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("NetworkHttpResponse{responseCode=");
        sb.append(this.a);
        sb.append(", headers=");
        sb.append(this.b);
        sb.append(", body");
        if (this.c == null) {
            str = "=null";
        } else {
            StringBuilder sb2 = new StringBuilder(".length=");
            sb2.append(this.c.length);
            sb2.append(" bytes");
            str = sb2.toString();
        }
        sb.append(str);
        sb.append(", requestUrl=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NetworkHttpResponse networkHttpResponse = (NetworkHttpResponse) obj;
        return this.a == networkHttpResponse.a && Objects.equals(this.b, networkHttpResponse.b) && Arrays.equals(this.c, networkHttpResponse.c) && Objects.equals(this.d, networkHttpResponse.d);
    }

    public int hashCode() {
        return (Objects.hash(Integer.valueOf(this.a), this.b, this.d) * 31) + Arrays.hashCode(this.c);
    }
}
