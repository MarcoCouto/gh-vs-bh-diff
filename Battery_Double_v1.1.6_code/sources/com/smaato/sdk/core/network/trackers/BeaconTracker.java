package com.smaato.sdk.core.network.trackers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class BeaconTracker {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    @NonNull
    private final BeaconsExecutioner b;
    /* access modifiers changed from: private */
    @NonNull
    public final BeaconTrackerAdQualityViolationUtils c;

    public BeaconTracker(@NonNull Logger logger, @NonNull BeaconsExecutioner beaconsExecutioner, @NonNull BeaconTrackerAdQualityViolationUtils beaconTrackerAdQualityViolationUtils) {
        this.a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for BeaconTracker");
        this.b = (BeaconsExecutioner) Objects.requireNonNull(beaconsExecutioner, "Parameter beaconsExecutioner cannot be null for BeaconTracker");
        this.c = (BeaconTrackerAdQualityViolationUtils) Objects.requireNonNull(beaconTrackerAdQualityViolationUtils);
    }

    public void trackBeaconUrls(@NonNull Collection<String> collection, @NonNull SomaApiContext somaApiContext) {
        trackBeaconUrls(collection, somaApiContext, null);
    }

    public void trackBeaconUrls(@NonNull Collection<String> collection, @NonNull SomaApiContext somaApiContext, @Nullable Listener<Whatever, Exception> listener) {
        Objects.requireNonNull(collection, "Parameter urls cannot be null for BeaconTracker::trackBeaconUrls");
        Objects.requireNonNull(somaApiContext, "Parameter somaApiContext cannot be null for BeaconTracker::trackBeaconUrls");
        Iterator it = new ArrayList(collection).iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (str != null) {
                trackBeaconUrl(str, somaApiContext, listener);
            }
        }
    }

    public void trackBeaconUrl(@NonNull final String str, @NonNull final SomaApiContext somaApiContext, @Nullable final Listener<Whatever, Exception> listener) {
        Objects.requireNonNull(str, "Parameter url cannot be null for BeaconTracker::trackBeaconUrl");
        this.b.submitRequest(str, somaApiContext, (Listener<Whatever, Exception>) new Listener<Whatever, Exception>() {
            public void onSuccess(@NonNull Task task, @NonNull Whatever whatever) {
                BeaconTracker.this.a.info(LogDomain.NETWORK, "Beacon was tracked successfully %s", str);
                if (listener != null) {
                    listener.onSuccess(task, whatever);
                }
            }

            public void onFailure(@NonNull Task task, @NonNull Exception exc) {
                BeaconTracker.this.a.error(LogDomain.NETWORK, exc, "Tracking Beacon failed with error [url: %s]", str);
                BeaconTracker.this.c.a(somaApiContext, exc);
                if (listener != null) {
                    listener.onFailure(task, exc);
                }
            }
        }).start();
    }

    public void trackBeaconUrl(@NonNull String str, @NonNull SomaApiContext somaApiContext) {
        trackBeaconUrl(str, somaApiContext, null);
    }
}
