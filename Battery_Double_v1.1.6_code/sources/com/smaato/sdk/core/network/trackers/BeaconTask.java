package com.smaato.sdk.core.network.trackers;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.network.execution.NetworkTask;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.Function;
import java.util.concurrent.ExecutorService;

class BeaconTask extends NetworkTask<Whatever> {
    BeaconTask(@NonNull Logger logger, @NonNull NetworkActions networkActions, @NonNull ErrorMapper<Exception> errorMapper, @NonNull ExecutorService executorService, @NonNull String str, @NonNull SomaApiContext somaApiContext, @NonNull Listener<Whatever, Exception> listener) {
        $$Lambda$BeaconTask$qigtmi0gzPYMtHTOSJ4I6Rp1gcw r0 = new Function(str, somaApiContext, logger, errorMapper, listener) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ SomaApiContext f$2;
            private final /* synthetic */ Logger f$3;
            private final /* synthetic */ ErrorMapper f$4;
            private final /* synthetic */ Listener f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            public final Object apply(Object obj) {
                return BeaconTask.createRunnable((NetworkTask) obj, NetworkActions.this, new BeaconNetworkRequest(this.f$1), this.f$2, $$Lambda$BeaconTask$l8OawmE2T13kkOpr90UQ8Kyy4.INSTANCE, BeaconTask.standardResultHandler(this.f$3, this.f$4, (NetworkTask) obj, this.f$5));
            }
        };
        super(executorService, r0);
    }
}
