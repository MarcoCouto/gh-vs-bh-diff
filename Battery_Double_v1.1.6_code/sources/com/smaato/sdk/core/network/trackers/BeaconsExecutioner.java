package com.smaato.sdk.core.network.trackers;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.Executioner;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import java.util.concurrent.ExecutorService;

public class BeaconsExecutioner implements Executioner<String, Whatever, Exception> {
    @NonNull
    private final ErrorMapper<Exception> a;
    @NonNull
    private final ExecutorService b;
    @NonNull
    private final NetworkActions c;
    @NonNull
    private final Logger d;

    public BeaconsExecutioner(@NonNull Logger logger, @NonNull NetworkActions networkActions, @NonNull ErrorMapper<Exception> errorMapper, @NonNull ExecutorService executorService) {
        this.d = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for BeaconsExecutioner::new");
        this.c = (NetworkActions) Objects.requireNonNull(networkActions, "Parameter networkActions cannot be null for BeaconsExecutioner::new");
        this.a = (ErrorMapper) Objects.requireNonNull(errorMapper, "Parameter errorMapper cannot be null for BeaconsExecutioner::new");
        this.b = (ExecutorService) Objects.requireNonNull(executorService, "Parameter executorService cannot be null for BeaconsExecutioner::new");
    }

    @NonNull
    public Task submitRequest(@NonNull String str, @NonNull SomaApiContext somaApiContext, @NonNull Listener<Whatever, Exception> listener) {
        BeaconTask beaconTask = new BeaconTask(this.d, this.c, this.a, this.b, str, somaApiContext, listener);
        return beaconTask;
    }
}
