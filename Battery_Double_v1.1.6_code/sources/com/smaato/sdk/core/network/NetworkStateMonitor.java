package com.smaato.sdk.core.network;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.notifier.ChangeNotifier;
import com.smaato.sdk.core.util.notifier.ChangeNotifier.Listener;
import com.smaato.sdk.core.util.notifier.ChangeSender;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;

public class NetworkStateMonitor {
    @NonNull
    private final ConnectivityManager a;
    @NonNull
    private final ChangeSender<Boolean> b = ChangeSenderUtils.createChangeSender(Boolean.valueOf(this.d));
    @NonNull
    private final Listener<Whatever> c = new Listener() {
        public final void onNextValue(Object obj) {
            NetworkStateMonitor.this.a((Whatever) obj);
        }
    };
    private boolean d = a();

    public NetworkStateMonitor(@NonNull ConnectivityManager connectivityManager, @NonNull ConnectionStatusWatcher connectionStatusWatcher) {
        this.a = (ConnectivityManager) Objects.requireNonNull(connectivityManager, "Parameter connectivityManager cannot be null for NetworkStateMonitor::new");
        Objects.requireNonNull(connectionStatusWatcher, "Parameter connectionStatusWatcher cannot be null for NetworkStateMonitor::new");
        connectionStatusWatcher.getStatusNotifier().addListener(this.c);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Whatever whatever) {
        boolean z = this.d;
        boolean isOnline = isOnline();
        if (z != isOnline) {
            this.b.newValue(Boolean.valueOf(isOnline));
        }
    }

    @NonNull
    public ChangeNotifier<Boolean> getNetworkStateChangeNotifier() {
        return this.b;
    }

    public boolean isOnline() {
        return a();
    }

    private boolean a() {
        NetworkInfo activeNetworkInfo = this.a.getActiveNetworkInfo();
        this.d = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        return this.d;
    }

    @Nullable
    public NetworkConnectionType getNetworkConnectionType() {
        NetworkInfo activeNetworkInfo = this.a.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return null;
        }
        if (activeNetworkInfo.getType() == 1) {
            return NetworkConnectionType.WIFI;
        }
        if (activeNetworkInfo.getType() == 0) {
            switch (activeNetworkInfo.getSubtype()) {
                case 1:
                case 2:
                case 4:
                case 7:
                case 11:
                case 16:
                    return NetworkConnectionType.CARRIER_2G;
                case 3:
                case 5:
                case 6:
                case 8:
                case 9:
                case 10:
                case 12:
                case 14:
                case 15:
                case 17:
                    return NetworkConnectionType.CARRIER_3G;
                case 13:
                case 18:
                case 19:
                    return NetworkConnectionType.CARRIER_4G;
                default:
                    return NetworkConnectionType.CARRIER_UNKNOWN;
            }
        } else if (activeNetworkInfo.getType() == 9) {
            return NetworkConnectionType.ETHERNET;
        } else {
            return NetworkConnectionType.OTHER;
        }
    }
}
