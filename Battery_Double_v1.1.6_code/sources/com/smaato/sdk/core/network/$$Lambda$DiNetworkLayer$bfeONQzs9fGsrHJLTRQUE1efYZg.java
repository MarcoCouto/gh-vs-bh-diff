package com.smaato.sdk.core.network;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.network.-$$Lambda$DiNetworkLayer$bfeONQzs9fGsrHJLTRQUE1efYZg reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiNetworkLayer$bfeONQzs9fGsrHJLTRQUE1efYZg implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiNetworkLayer$bfeONQzs9fGsrHJLTRQUE1efYZg INSTANCE = new $$Lambda$DiNetworkLayer$bfeONQzs9fGsrHJLTRQUE1efYZg();

    private /* synthetic */ $$Lambda$DiNetworkLayer$bfeONQzs9fGsrHJLTRQUE1efYZg() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiNetworkLayer.f(diConstructor);
    }
}
