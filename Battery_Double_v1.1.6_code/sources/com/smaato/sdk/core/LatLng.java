package com.smaato.sdk.core;

import android.os.SystemClock;

public final class LatLng {
    private final double a;
    private final double b;
    private final float c;
    private final long d;

    public LatLng(double d2, double d3, float f, long j) {
        this.a = d2;
        this.b = d3;
        this.c = f;
        this.d = j;
    }

    public final double getLatitude() {
        return this.a;
    }

    public final double getLongitude() {
        return this.b;
    }

    public final float getLocationAccuracy() {
        return this.c;
    }

    public final long getLocationTimestamp() {
        return this.d;
    }

    public final float getTimeSinceLastLocationUpdate() {
        return (float) (SystemClock.elapsedRealtime() - this.d);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LatLng latLng = (LatLng) obj;
        return Double.compare(latLng.a, this.a) == 0 && Double.compare(latLng.b, this.b) == 0 && Float.compare(latLng.c, this.c) == 0;
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.a);
        int i = (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        long doubleToLongBits2 = Double.doubleToLongBits(this.b);
        return (((i * 31) + ((int) ((doubleToLongBits2 >>> 32) ^ doubleToLongBits2))) * 31) + (this.c != 0.0f ? Float.floatToIntBits(this.c) : 0);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("LatLng{latitude=");
        sb.append(this.a);
        sb.append(", longitude=");
        sb.append(this.b);
        sb.append(", accuracy=");
        sb.append(this.c);
        sb.append(", timestamp=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }
}
