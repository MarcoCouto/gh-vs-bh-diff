package com.smaato.sdk.core;

import android.util.Log;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.LogLevel;
import com.smaato.sdk.core.util.Objects;

public final class Config {
    @NonNull
    private final LogLevel a;
    @NonNull
    private final AdContentRating b;
    private final boolean c;
    private final boolean d;

    public static class ConfigBuilder {
        private LogLevel a = LogLevel.INFO;
        private boolean b;
        private boolean c;
        private AdContentRating d = AdContentRating.MAX_AD_CONTENT_RATING_UNDEFINED;

        @NonNull
        public ConfigBuilder enableLogging(boolean z) {
            this.c = z;
            return this;
        }

        @NonNull
        public ConfigBuilder setLogLevel(@NonNull LogLevel logLevel) {
            if (logLevel != null) {
                this.a = logLevel;
            } else {
                Log.w(LogDomain.CORE.name(), String.format("setting logLevel to null is ignored, current value = %s", new Object[]{this.a}));
            }
            return this;
        }

        @NonNull
        public ConfigBuilder setHttpsOnly(boolean z) {
            this.b = z;
            return this;
        }

        @NonNull
        public ConfigBuilder setAdContentRating(@NonNull AdContentRating adContentRating) {
            this.d = adContentRating;
            return this;
        }

        @NonNull
        public Config build() {
            Config config = new Config(this.c, this.a, this.b, this.d, 0);
            return config;
        }
    }

    /* synthetic */ Config(boolean z, LogLevel logLevel, boolean z2, AdContentRating adContentRating, byte b2) {
        this(z, logLevel, z2, adContentRating);
    }

    private Config(boolean z, @NonNull LogLevel logLevel, boolean z2, @NonNull AdContentRating adContentRating) {
        this.c = z;
        this.a = (LogLevel) Objects.requireNonNull(logLevel);
        this.d = z2;
        this.b = (AdContentRating) Objects.requireNonNull(adContentRating);
    }

    @NonNull
    public static ConfigBuilder builder() {
        return new ConfigBuilder();
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final LogLevel b() {
        return this.a;
    }

    public final boolean isHttpsOnly() {
        return this.d;
    }

    @NonNull
    public final AdContentRating getAdContentRating() {
        return this.b;
    }
}
