package com.smaato.sdk.core.tracker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine.Listener;

public final class ImpressionDetector {
    @NonNull
    private final State a;
    @Nullable
    private Callback b;
    @NonNull
    public final Listener<State> stateListener = new Listener() {
        public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
            ImpressionDetector.this.a((State) obj, (State) obj2, metadata);
        }
    };

    public interface Callback {
        void onImpressionStateDetected();
    }

    public ImpressionDetector(@NonNull State state) {
        this.a = (State) Objects.requireNonNull(state);
    }

    public final void setOnImpressionStateDetectedCallback(@Nullable Callback callback) {
        this.b = callback;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(State state, State state2, Metadata metadata) {
        if (state2 == this.a) {
            Objects.onNotNull(this.b, $$Lambda$f9tAlkmanVF94Zd_vmyqUR8_pw.INSTANCE);
        }
    }
}
