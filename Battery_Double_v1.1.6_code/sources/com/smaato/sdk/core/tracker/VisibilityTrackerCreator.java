package com.smaato.sdk.core.tracker;

import android.view.View;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;

public class VisibilityTrackerCreator {
    @NonNull
    private final Logger a;
    private final double b;
    private final long c;
    @NonNull
    private final AppBackgroundDetector d;
    @NonNull
    private final String e;

    public VisibilityTrackerCreator(@NonNull Logger logger, double d2, long j, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull String str) {
        this.a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for VisibilityTrackerCreator::VisibilityTrackerCreator");
        this.b = d2;
        this.c = j;
        this.d = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.e = (String) Objects.requireNonNull(str);
    }

    @NonNull
    public VisibilityTracker createTracker(@NonNull View view, @NonNull VisibilityTrackerListener visibilityTrackerListener) {
        View view2 = view;
        VisibilityTrackerListener visibilityTrackerListener2 = visibilityTrackerListener;
        VisibilityTracker visibilityTracker = new VisibilityTracker(this.a, view2, this.b, this.c, visibilityTrackerListener2, new AppBackgroundAwareHandler(this.a, Threads.newUiHandler(), this.d), this.e);
        return visibilityTracker;
    }
}
