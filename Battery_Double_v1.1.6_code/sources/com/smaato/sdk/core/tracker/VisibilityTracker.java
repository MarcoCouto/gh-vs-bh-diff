package com.smaato.sdk.core.tracker;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.github.mikephil.charting.utils.Utils;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.appbgdetection.PauseUnpauseListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import java.lang.ref.WeakReference;

public class VisibilityTracker {
    @NonNull
    private WeakReference<View> a;
    @NonNull
    private final String b;
    private final double c;
    private final long d;
    @NonNull
    private final AppBackgroundAwareHandler e;
    @Nullable
    private VisibilityTrackerListener f;
    @NonNull
    private WeakReference<ViewTreeObserver> g = new WeakReference<>(null);
    @NonNull
    private OnPreDrawListener h = new OnPreDrawListener() {
        private boolean a = false;

        public boolean onPreDraw() {
            if (!this.a) {
                this.a = true;
                VisibilityTracker.this.a(new CheckContext(SystemClock.uptimeMillis()));
            }
            return true;
        }
    };

    @VisibleForTesting
    static class CheckContext implements PauseUnpauseListener {
        @VisibleForTesting
        long a;
        @VisibleForTesting
        boolean b = false;
        @VisibleForTesting
        long c = 0;
        @VisibleForTesting
        private long d = 0;

        CheckContext(long j) {
            this.a = j;
        }

        public void onActionPaused() {
            this.d = SystemClock.uptimeMillis();
        }

        public void onBeforeActionUnpaused() {
            long uptimeMillis = SystemClock.uptimeMillis() - this.d;
            this.d = 0;
            this.a += uptimeMillis;
        }
    }

    VisibilityTracker(@NonNull Logger logger, @NonNull View view, double d2, long j, @NonNull VisibilityTrackerListener visibilityTrackerListener, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler, @NonNull String str) {
        Objects.requireNonNull(view, "Parameter contentView cannot be null for VisibilityTracker::new");
        this.a = new WeakReference<>(view);
        this.b = (String) Objects.requireNonNull(str, "Parameter actionName cannot be null for VisibilityTracker::new");
        if (d2 <= Utils.DOUBLE_EPSILON || d2 > 1.0d) {
            throw new IllegalArgumentException("visibilityRatio should be in range (0..1]");
        }
        this.c = d2;
        if (j >= 0) {
            this.d = j;
            this.f = (VisibilityTrackerListener) Objects.requireNonNull(visibilityTrackerListener, "Parameter listener cannot be null for VisibilityTracker::new");
            this.e = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
            View rootView = view.getRootView();
            if (rootView != null) {
                ViewTreeObserver viewTreeObserver = rootView.getViewTreeObserver();
                this.g = new WeakReference<>(viewTreeObserver);
                if (!viewTreeObserver.isAlive()) {
                    logger.error(LogDomain.CORE, "Cannot set VisibilityTracker due to no available root view", new Object[0]);
                } else {
                    viewTreeObserver.addOnPreDrawListener(this.h);
                    return;
                }
            } else {
                logger.error(LogDomain.CORE, "Cannot set VisibilityTracker due to no available root view", new Object[0]);
            }
            return;
        }
        throw new IllegalArgumentException("visibilityTimeInMillis should be bigger or equal to 0");
    }

    /* access modifiers changed from: private */
    public void a(@NonNull CheckContext checkContext) {
        this.e.postDelayed(this.b, new Runnable(checkContext) {
            private final /* synthetic */ CheckContext f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                VisibilityTracker.this.b(this.f$1);
            }
        }, 250, checkContext);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(CheckContext checkContext) {
        View view = (View) this.a.get();
        if (view != null) {
            long uptimeMillis = SystemClock.uptimeMillis();
            boolean a2 = VisibilityTrackerUtils.a(view, this.c);
            if (checkContext.b) {
                checkContext.c += uptimeMillis - checkContext.a;
                if (checkContext.c >= this.d) {
                    Objects.onNotNull(this.f, $$Lambda$uYHkhh3aiLW8Nf5tif2DyOnenZA.INSTANCE);
                    return;
                }
                checkContext.a = uptimeMillis;
                checkContext.b = a2;
                a(checkContext);
                return;
            }
            checkContext.a = uptimeMillis;
            checkContext.b = a2;
            a(checkContext);
        }
    }

    @MainThread
    public void destroy() {
        Threads.ensureMainThread();
        this.e.stop();
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.g.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnPreDrawListener(this.h);
        }
        this.a.clear();
        this.g.clear();
        this.f = null;
    }
}
