package com.smaato.sdk.core;

import androidx.annotation.NonNull;

public interface Task {

    public interface Listener<Response, Error> {
        void onFailure(@NonNull Task task, @NonNull Error error);

        void onSuccess(@NonNull Task task, @NonNull Response response);
    }

    void cancel();

    void start();
}
