package com.smaato.sdk.core;

public enum AdContentRating {
    MAX_AD_CONTENT_RATING_UNDEFINED,
    MAX_AD_CONTENT_RATING_G,
    MAX_AD_CONTENT_RATING_PG,
    MAX_AD_CONTENT_RATING_T,
    MAX_AD_CONTENT_RATING_MA
}
