package com.smaato.sdk.core.deeplink;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;

public interface UrlResolveListener {
    void onError();

    void onSuccess(@NonNull Consumer<Context> consumer);
}
