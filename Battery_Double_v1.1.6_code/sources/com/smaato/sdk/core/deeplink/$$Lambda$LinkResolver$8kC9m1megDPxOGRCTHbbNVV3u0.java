package com.smaato.sdk.core.deeplink;

import android.content.pm.ResolveInfo;
import com.smaato.sdk.core.util.fi.NullableFunction;

/* renamed from: com.smaato.sdk.core.deeplink.-$$Lambda$LinkResolver$8kC9m1megDPxOGRCTHb-bNVV3u0 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$LinkResolver$8kC9m1megDPxOGRCTHbbNVV3u0 implements NullableFunction {
    public static final /* synthetic */ $$Lambda$LinkResolver$8kC9m1megDPxOGRCTHbbNVV3u0 INSTANCE = new $$Lambda$LinkResolver$8kC9m1megDPxOGRCTHbbNVV3u0();

    private /* synthetic */ $$Lambda$LinkResolver$8kC9m1megDPxOGRCTHbbNVV3u0() {
    }

    public final Object apply(Object obj) {
        return ((ResolveInfo) obj).activityInfo.targetActivity;
    }
}
