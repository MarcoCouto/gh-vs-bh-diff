package com.smaato.sdk.core.deeplink;

import android.app.Application;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.network.execution.ErrorMapper;

public final class DiDeepLinkLayer {
    private DiDeepLinkLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiDeepLinkLayer$j5dNaPS1HxxZQQioVVJAit_sX9E.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(LinkResolver.class, $$Lambda$DiDeepLinkLayer$OfnqHCGMTtrGuv9RRvuumbZDsQI.INSTANCE);
        diRegistry.registerSingletonFactory(RedirectResolver.class, $$Lambda$DiDeepLinkLayer$zAtT9kVFpl64apf4Sbw6ZHju1M0.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ LinkResolver b(DiConstructor diConstructor) {
        return new LinkResolver(DiLogLayer.getLoggerFrom(diConstructor), (Application) diConstructor.get(Application.class), (RedirectResolver) diConstructor.get(RedirectResolver.class), DiNetworkLayer.getUrlCreatorFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RedirectResolver a(DiConstructor diConstructor) {
        RedirectResolver redirectResolver = new RedirectResolver(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getNetworkingExecutorServiceFrom(diConstructor), DiNetworkLayer.getNetworkActionsFrom(diConstructor), ErrorMapper.IDENTITY, DiNetworkLayer.getUrlRedirectResolverFrom(diConstructor));
        return redirectResolver;
    }

    @NonNull
    public static LinkResolver getLinkResolverFrom(@NonNull DiConstructor diConstructor) {
        return (LinkResolver) diConstructor.get(LinkResolver.class);
    }
}
