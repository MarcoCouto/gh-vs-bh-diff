package com.smaato.sdk.core.deeplink;

import android.content.pm.ResolveInfo;
import com.smaato.sdk.core.util.fi.NullableFunction;

/* renamed from: com.smaato.sdk.core.deeplink.-$$Lambda$LinkResolver$ihf_GXVNDxE7sD-_BZFu5apEQQc reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$LinkResolver$ihf_GXVNDxE7sD_BZFu5apEQQc implements NullableFunction {
    public static final /* synthetic */ $$Lambda$LinkResolver$ihf_GXVNDxE7sD_BZFu5apEQQc INSTANCE = new $$Lambda$LinkResolver$ihf_GXVNDxE7sD_BZFu5apEQQc();

    private /* synthetic */ $$Lambda$LinkResolver$ihf_GXVNDxE7sD_BZFu5apEQQc() {
    }

    public final Object apply(Object obj) {
        return ((ResolveInfo) obj).activityInfo.targetActivity;
    }
}
