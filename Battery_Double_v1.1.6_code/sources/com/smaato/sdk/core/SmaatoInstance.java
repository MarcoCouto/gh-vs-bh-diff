package com.smaato.sdk.core;

import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.security.NetworkSecurityPolicy;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.analytics.DiAnalyticsLayer;
import com.smaato.sdk.core.api.DiApiLayer;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.browser.DiBrowserLayer;
import com.smaato.sdk.core.ccpa.CcpaDataStorage;
import com.smaato.sdk.core.config.DiConfiguration;
import com.smaato.sdk.core.configcheck.AppConfigChecker;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.datacollector.DiDataCollectorLayer;
import com.smaato.sdk.core.deeplink.DiDeepLinkLayer;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.CoreDiNames;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.repository.DiAdRepository;
import com.smaato.sdk.core.resourceloader.DiResourceLoaderLayer;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.core.util.HeaderUtils;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkConfigHintBuilder;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.memory.DiLeakProtection;
import com.smaato.sdk.core.violationreporter.DiAdQualityViolationReporterLayer;
import com.smaato.sdk.core.webview.DiWebViewLayer;
import com.smaato.sdk.flow.Flow.Executors;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class SmaatoInstance {
    @NonNull
    private final DiConstructor a;
    @NonNull
    private final String b;
    @NonNull
    private final CcpaDataStorage c;
    @Nullable
    private String d;
    @Nullable
    private String e;
    @Nullable
    private Gender f;
    @Nullable
    private Integer g;
    @Nullable
    private LatLng h;
    @Nullable
    private String i;
    @Nullable
    private String j;
    @Nullable
    private String k;
    private boolean l = false;
    private boolean m = false;
    private boolean n = true;

    /* access modifiers changed from: private */
    public static /* synthetic */ Application a(Application application, DiConstructor diConstructor) {
        return application;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdContentRating a(AdContentRating adContentRating, DiConstructor diConstructor) {
        return adContentRating;
    }

    SmaatoInstance(@NonNull Application application, @NonNull Config config, @NonNull List<DiRegistry> list, @NonNull ExpectedManifestEntries expectedManifestEntries, @NonNull String str) {
        this.b = (String) Objects.requireNonNull(str, "Parameter PublisherId cannot be null for SmaatoInstance::new");
        Application application2 = (Application) Objects.requireNonNull(application, "Parameter application cannot be null for SmaatoInstance::new");
        Config config2 = (Config) Objects.requireNonNull(config, "Parameter config cannot be null for SmaatoInstance::new");
        ExpectedManifestEntries expectedManifestEntries2 = (ExpectedManifestEntries) Objects.requireNonNull(expectedManifestEntries, "Parameter expectedManifestEntries cannot be null for SmaatoInstance::new");
        HashSet hashSet = new HashSet((List) Objects.requireNonNull(list, "Parameter diRegistriesOfModules cannot be null for SmaatoInstance::new"));
        boolean isHttpsOnly = config2.isHttpsOnly();
        if (!isHttpsOnly) {
            if (!(VERSION.SDK_INT >= 23 ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : true)) {
                Log.w(LogDomain.CORE.name(), "`httpsOnly` value overridden to TRUE, due to Network Security Configuration settings.");
                isHttpsOnly = true;
            }
        }
        AdContentRating adContentRating = config2.getAdContentRating();
        Collections.addAll(hashSet, new DiRegistry[]{DiRegistry.of(new Consumer(isHttpsOnly, application2, expectedManifestEntries2) {
            private final /* synthetic */ boolean f$1;
            private final /* synthetic */ Application f$2;
            private final /* synthetic */ ExpectedManifestEntries f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                SmaatoInstance.a(AdContentRating.this, this.f$1, this.f$2, this.f$3, (DiRegistry) obj);
            }
        }), DiLogLayer.createRegistry(config2.a(), config2.b()), DiAdLayer.createRegistry(isHttpsOnly, adContentRating), DiApiLayer.createRegistry(), DiNetworkLayer.createRegistry(), DiDeepLinkLayer.createRegistry(), DiBrowserLayer.createRegistry(), DiWebViewLayer.createRegistry(), DiDataCollectorLayer.createRegistry(), DiAdQualityViolationReporterLayer.createRegistry(), DiResourceLoaderLayer.createRegistry(), DiLeakProtection.createRegistry(), DiAdRepository.createRegistry(), DiConfiguration.createRegistry(), DiAnalyticsLayer.createRegistry(application2)});
        this.a = DiConstructor.create((Set<DiRegistry>) hashSet);
        this.c = (CcpaDataStorage) this.a.get(CcpaDataStorage.class);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final DiConstructor a() {
        return this.a;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AdContentRating adContentRating, boolean z, Application application, ExpectedManifestEntries expectedManifestEntries, DiRegistry diRegistry) {
        diRegistry.registerFactory(CoreDiNames.NAME_AD_CONTENT_RATING, AdContentRating.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return SmaatoInstance.a(AdContentRating.this, diConstructor);
            }
        });
        diRegistry.registerFactory(CoreDiNames.NAME_HTTPS_ONLY, Boolean.class, new ClassFactory(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return Boolean.valueOf(this.f$0);
            }
        });
        diRegistry.registerFactory(Application.class, new ClassFactory(application) {
            private final /* synthetic */ Application f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return SmaatoInstance.a(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory(CoreDiNames.SOMA_API_URL, String.class, $$Lambda$SmaatoInstance$1EAKuS9NNIxR8fb08T7PPARc0.INSTANCE);
        diRegistry.registerSingletonFactory(AppMetaData.class, $$Lambda$SmaatoInstance$FUq1c3ogEYbVE3tKUe5mmHyaw8o.INSTANCE);
        diRegistry.registerSingletonFactory(SdkConfigHintBuilder.class, $$Lambda$SmaatoInstance$2UGCDDmGddMjfWqDAYu4E74S_3Q.INSTANCE);
        diRegistry.registerSingletonFactory(AppConfigChecker.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return SmaatoInstance.a(ExpectedManifestEntries.this, diConstructor);
            }
        });
        diRegistry.registerSingletonFactory(AppBackgroundDetector.class, $$Lambda$SmaatoInstance$00IPGA4M7VdYdgwO4aoLRf1oU2Y.INSTANCE);
        diRegistry.registerSingletonFactory(HeaderUtils.class, $$Lambda$SmaatoInstance$2vDOjAPQng0eeCOy3VRrHwCy5Q.INSTANCE);
        diRegistry.registerSingletonFactory(Executors.class, $$Lambda$SmaatoInstance$Y1v6IBsu4Fxtl8rRJlZ8dTB09Rs.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AppMetaData e(DiConstructor diConstructor) {
        return new AppMetaData((Context) diConstructor.get(Application.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SdkConfigHintBuilder d(DiConstructor diConstructor) {
        return new SdkConfigHintBuilder();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AppConfigChecker a(ExpectedManifestEntries expectedManifestEntries, DiConstructor diConstructor) {
        return new AppConfigChecker(DiLogLayer.getLoggerFrom(diConstructor), expectedManifestEntries, (AppMetaData) diConstructor.get(AppMetaData.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AppBackgroundDetector c(DiConstructor diConstructor) {
        return new AppBackgroundDetector(DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ HeaderUtils b(DiConstructor diConstructor) {
        return new HeaderUtils();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Executors a(DiConstructor diConstructor) {
        return new CoreExecutors();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable String str) {
        this.d = str;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String c() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public final void b(@Nullable String str) {
        this.e = str;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final Gender d() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable Gender gender) {
        this.f = gender;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final Integer e() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable Integer num) {
        this.g = num;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final LatLng f() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable LatLng latLng) {
        this.h = latLng;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String g() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public final void c(@Nullable String str) {
        this.i = str;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String h() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public final void d(@Nullable String str) {
        this.j = str;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String i() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public final void e(@Nullable String str) {
        this.k = str;
    }

    /* access modifiers changed from: 0000 */
    public final boolean j() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        this.l = z;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String k() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final boolean l() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public final void b(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: 0000 */
    public final boolean m() {
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    public final void c(boolean z) {
        this.n = z;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String n() {
        return this.c.getUsPrivacyString();
    }
}
