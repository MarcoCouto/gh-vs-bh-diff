package com.smaato.sdk.core.framework;

public final class VisibilityPrivateConfig {
    /* access modifiers changed from: private */
    public final double a;
    /* access modifiers changed from: private */
    public final long b;

    public static class Builder {
        private double a;
        private long b;

        public Builder() {
        }

        public Builder(VisibilityPrivateConfig visibilityPrivateConfig) {
            this.a = visibilityPrivateConfig.a;
            this.b = visibilityPrivateConfig.b;
        }

        public Builder visibilityRatio(double d) {
            this.a = d;
            return this;
        }

        public Builder visibilityTimeMillis(long j) {
            this.b = j;
            return this;
        }

        public VisibilityPrivateConfig build() {
            VisibilityPrivateConfig visibilityPrivateConfig = new VisibilityPrivateConfig(this.a, this.b, 0);
            return visibilityPrivateConfig;
        }
    }

    /* synthetic */ VisibilityPrivateConfig(double d, long j, byte b2) {
        this(d, j);
    }

    private VisibilityPrivateConfig(double d, long j) {
        this.a = d;
        this.b = j;
    }

    public final double getVisibilityRatio() {
        return this.a;
    }

    public final long getVisibilityTimeMillis() {
        return this.b;
    }
}
