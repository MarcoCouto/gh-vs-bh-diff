package com.smaato.sdk.core.framework;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.api.ApiAdRequest;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.util.Objects;

public class SomaApiContext {
    @NonNull
    private final ApiAdRequest a;
    @NonNull
    private final ApiAdResponse b;

    public SomaApiContext(@NonNull ApiAdRequest apiAdRequest, @NonNull ApiAdResponse apiAdResponse) {
        this.a = (ApiAdRequest) Objects.requireNonNull(apiAdRequest, "Parameter apiAdRequest cannot be null for SomaApiContext::new");
        this.b = (ApiAdResponse) Objects.requireNonNull(apiAdResponse, "Parameter apiAdResponse cannot be null for SomaApiContext::new");
    }

    @NonNull
    public ApiAdRequest getApiAdRequest() {
        return this.a;
    }

    @NonNull
    public ApiAdResponse getApiAdResponse() {
        return this.b;
    }

    public boolean isHttpsOnly() {
        return this.a.getHttpsOnly().intValue() == 1;
    }
}
