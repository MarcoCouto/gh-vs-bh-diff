package com.smaato.sdk.core.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiRegistry;

public interface AdPresenterModuleInterface extends BaseModuleInterface {
    @NonNull
    ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory();

    @NonNull
    ExpectedManifestEntries getExpectedManifestEntries();

    boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls);

    @Nullable
    DiRegistry moduleAdPresenterDiRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper);

    @NonNull
    String moduleDiName();

    @Nullable
    DiRegistry moduleDiRegistry();

    @NonNull
    String version();
}
