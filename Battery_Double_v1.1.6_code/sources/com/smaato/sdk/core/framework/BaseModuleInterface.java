package com.smaato.sdk.core.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.di.DiRegistry;

public interface BaseModuleInterface {
    @NonNull
    String moduleDiName();

    @Nullable
    DiRegistry moduleDiRegistry();

    @NonNull
    String version();
}
