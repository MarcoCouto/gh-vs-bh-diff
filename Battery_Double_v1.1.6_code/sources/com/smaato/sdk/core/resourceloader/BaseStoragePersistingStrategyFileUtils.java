package com.smaato.sdk.core.resourceloader;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.PersistingStrategy.Error;
import com.smaato.sdk.core.util.Objects;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BaseStoragePersistingStrategyFileUtils {
    @NonNull
    private final Context a;
    @NonNull
    protected final Logger logger;

    public BaseStoragePersistingStrategyFileUtils(@NonNull Logger logger2, @NonNull Context context) {
        this.logger = (Logger) Objects.requireNonNull(logger2);
        this.a = (Context) Objects.requireNonNull(context);
    }

    @Nullable
    public File getResourceFileFromStorage(@NonNull String str, int i, @NonNull String str2) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(str2);
        for (File file : listResourceFiles(str, i)) {
            String name = file.getName();
            int indexOf = name.indexOf(".");
            if (indexOf == -1) {
                this.logger.info(LogDomain.RESOURCE_LOADER, "Skipping unknown file: %s", file);
            } else if (str2.equals(name.substring(0, indexOf))) {
                return file;
            }
        }
        return null;
    }

    @NonNull
    public File getTemporaryResourceFile(@NonNull File file, @NonNull String str) {
        Objects.requireNonNull(file);
        Objects.requireNonNull(str);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".tmp");
        return new File(file, sb.toString());
    }

    @NonNull
    public String getStorageResourceFullName(@NonNull String str, long j) {
        Objects.requireNonNull(str);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".");
        sb.append(j);
        return sb.toString();
    }

    public void clearExpiredResources(@NonNull String str, int i) {
        Objects.requireNonNull(str);
        for (File file : listResourceFiles(str, i)) {
            String name = file.getName();
            if (System.currentTimeMillis() > Long.valueOf(name.substring(name.indexOf(".") + 1)).longValue()) {
                deleteFile(file, "expired resource file");
            }
        }
    }

    public void deleteOldCaches(@NonNull String str, int i) {
        File[] listFiles;
        Objects.requireNonNull(str);
        File a2 = a(str);
        ArrayList arrayList = new ArrayList();
        for (File file : a2.listFiles()) {
            if (file.isDirectory()) {
                try {
                    if (i > Integer.valueOf(file.getName()).intValue()) {
                        arrayList.add(file);
                    }
                } catch (NumberFormatException unused) {
                    arrayList.add(file);
                }
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            File file2 = (File) it.next();
            for (File deleteFile : file2.listFiles()) {
                deleteFile(deleteFile, "old resource file");
            }
            deleteFile(file2, "old resource cache directory");
        }
    }

    @NonNull
    public List<File> listResourceFiles(@NonNull String str, int i) {
        File[] listFiles;
        Objects.requireNonNull(str);
        ArrayList arrayList = new ArrayList();
        for (File file : getCurrentDiskCacheDirectory(str, i).listFiles()) {
            if (!file.isDirectory() && !file.getName().endsWith(".tmp")) {
                arrayList.add(file);
            }
        }
        return arrayList;
    }

    public void deleteFile(@NonNull File file, @NonNull String str) {
        Objects.requireNonNull(file);
        Objects.requireNonNull(str);
        if (file.delete()) {
            this.logger.debug(LogDomain.RESOURCE_LOADER, "%s successfully deleted. Filename: %s", str, file.getName());
            return;
        }
        this.logger.error(LogDomain.RESOURCE_LOADER, "Cannot delete %S. Filename: %s", str, file.getName());
    }

    public void createDirectories(@NonNull File file) throws PersistingStrategyException {
        Objects.requireNonNull(file);
        if (!file.exists() && !file.mkdirs()) {
            Error error = Error.IO_ERROR;
            StringBuilder sb = new StringBuilder("cannot create directories for file: ");
            sb.append(file.getAbsolutePath());
            throw new PersistingStrategyException(error, new IOException(sb.toString()));
        }
    }

    @NonNull
    public File getCurrentDiskCacheDirectory(@NonNull String str, int i) {
        Objects.requireNonNull(str);
        return new File(a(str), String.valueOf(i));
    }

    public void deleteTemporaryResourceFileArtifacts(@NonNull String str, int i) {
        File[] listFiles;
        Objects.requireNonNull(str);
        Objects.requireNonNull(str);
        ArrayList<File> arrayList = new ArrayList<>();
        for (File file : getCurrentDiskCacheDirectory(str, i).listFiles()) {
            if (!file.isDirectory() && file.getName().endsWith(".tmp")) {
                arrayList.add(file);
            }
        }
        for (File deleteFile : arrayList) {
            deleteFile(deleteFile, "temporary resource file");
        }
    }

    @NonNull
    private File a(@NonNull String str) {
        Objects.requireNonNull(str);
        return new File(a(), str);
    }

    @NonNull
    private File a() {
        return new File(this.a.getCacheDir(), "com.smaato.sdk.cache");
    }
}
