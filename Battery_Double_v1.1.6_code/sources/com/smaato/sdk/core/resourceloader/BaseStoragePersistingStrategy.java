package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.PersistingStrategy.Error;
import com.smaato.sdk.core.util.Objects;
import java.io.File;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class BaseStoragePersistingStrategy<OutputResourceType> implements PersistingStrategy<OutputResourceType> {
    @NonNull
    private final Md5Digester a;
    @NonNull
    private final Map<String, ReadWriteLock> b = new HashMap();
    private boolean c;
    @NonNull
    protected final String descriptiveResourceName;
    @NonNull
    protected final BaseStoragePersistingStrategyFileUtils fileUtils;
    @NonNull
    protected final Logger logger;

    /* access modifiers changed from: protected */
    @Nullable
    public abstract OutputResourceType getResourceFromStorage(@NonNull String str) throws PersistingStrategyException;

    /* access modifiers changed from: protected */
    @NonNull
    public abstract OutputResourceType putResourceToStorage(@NonNull File file, @NonNull InputStream inputStream, @NonNull String str, long j) throws PersistingStrategyException;

    /* access modifiers changed from: protected */
    @NonNull
    public abstract Integer version();

    public BaseStoragePersistingStrategy(@NonNull Logger logger2, @NonNull String str, @NonNull BaseStoragePersistingStrategyFileUtils baseStoragePersistingStrategyFileUtils, @NonNull Md5Digester md5Digester) {
        this.logger = (Logger) Objects.requireNonNull(logger2);
        this.descriptiveResourceName = (String) Objects.requireNonNull(str);
        this.fileUtils = (BaseStoragePersistingStrategyFileUtils) Objects.requireNonNull(baseStoragePersistingStrategyFileUtils);
        this.a = (Md5Digester) Objects.requireNonNull(md5Digester);
    }

    @Nullable
    public final OutputResourceType get(@NonNull String str) throws PersistingStrategyException {
        Objects.requireNonNull(str);
        Lock readLock = a(str).readLock();
        readLock.lock();
        try {
            a();
            this.fileUtils.clearExpiredResources(this.descriptiveResourceName, version().intValue());
            String md5Hex = this.a.md5Hex(str);
            this.fileUtils.createDirectories(this.fileUtils.getCurrentDiskCacheDirectory(this.descriptiveResourceName, version().intValue()));
            OutputResourceType resourceFromStorage = getResourceFromStorage(md5Hex);
            if (resourceFromStorage != null) {
                this.logger.debug(LogDomain.RESOURCE_LOADER, "Resource with name %s found in cache", str);
            } else {
                this.logger.debug(LogDomain.RESOURCE_LOADER, "Resource with name %s NOT found in cache", str);
            }
            readLock.unlock();
            return resourceFromStorage;
        } catch (NoSuchAlgorithmException e) {
            this.logger.error(LogDomain.RESOURCE_LOADER, e, "Cannot generate a Md5 hash for a resource name", new Object[0]);
            throw new PersistingStrategyException(Error.GENERIC, e);
        } catch (Throwable th) {
            readLock.unlock();
            throw th;
        }
    }

    @NonNull
    public final OutputResourceType put(@NonNull InputStream inputStream, @NonNull String str, long j) throws PersistingStrategyException {
        Objects.requireNonNull(inputStream);
        Objects.requireNonNull(str);
        Lock writeLock = a(str).writeLock();
        writeLock.lock();
        try {
            a();
            this.fileUtils.clearExpiredResources(this.descriptiveResourceName, version().intValue());
            String storageResourceFullName = this.fileUtils.getStorageResourceFullName(this.a.md5Hex(str), j);
            File currentDiskCacheDirectory = this.fileUtils.getCurrentDiskCacheDirectory(this.descriptiveResourceName, version().intValue());
            this.fileUtils.createDirectories(currentDiskCacheDirectory);
            OutputResourceType putResourceToStorage = putResourceToStorage(currentDiskCacheDirectory, inputStream, storageResourceFullName, j);
            writeLock.unlock();
            return putResourceToStorage;
        } catch (NoSuchAlgorithmException e) {
            this.logger.error(LogDomain.RESOURCE_LOADER, e, "Cannot generate a Md5 hash for a resource name", new Object[0]);
            throw new PersistingStrategyException(Error.GENERIC, e);
        } catch (Throwable th) {
            writeLock.unlock();
            throw th;
        }
    }

    @NonNull
    private synchronized ReadWriteLock a(@NonNull String str) {
        ReadWriteLock readWriteLock;
        readWriteLock = (ReadWriteLock) this.b.get(str);
        if (readWriteLock == null) {
            readWriteLock = new ReentrantReadWriteLock();
            this.b.put(str, readWriteLock);
        }
        return readWriteLock;
    }

    private void a() throws PersistingStrategyException {
        if (!this.c) {
            this.fileUtils.createDirectories(this.fileUtils.getCurrentDiskCacheDirectory(this.descriptiveResourceName, version().intValue()));
            this.fileUtils.deleteOldCaches(this.descriptiveResourceName, version().intValue());
            this.fileUtils.deleteTemporaryResourceFileArtifacts(this.descriptiveResourceName, version().intValue());
            this.c = true;
        }
    }
}
