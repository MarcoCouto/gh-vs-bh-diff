package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.InputStream;

public interface PersistingStrategy<PersistedResourceType> {

    public enum Error {
        GENERIC,
        IO_ERROR,
        RESOURCE_EXPIRED
    }

    @Nullable
    PersistedResourceType get(@NonNull String str) throws PersistingStrategyException;

    @NonNull
    PersistedResourceType put(@NonNull InputStream inputStream, @NonNull String str, long j) throws PersistingStrategyException;
}
