package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;

public interface ResourceTransformer<PersistedResourceType, OutputResourceType> {
    @NonNull
    OutputResourceType transform(@NonNull PersistedResourceType persistedresourcetype);
}
