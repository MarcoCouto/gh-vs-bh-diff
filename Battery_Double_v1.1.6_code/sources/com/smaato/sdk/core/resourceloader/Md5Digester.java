package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Digester {
    @NonNull
    private final Charset a;
    @NonNull
    private final HexEncoder b;

    public Md5Digester(@NonNull Charset charset, @NonNull HexEncoder hexEncoder) {
        this.a = (Charset) Objects.requireNonNull(charset);
        this.b = (HexEncoder) Objects.requireNonNull(hexEncoder);
    }

    @NonNull
    public String md5Hex(@NonNull String str) throws NoSuchAlgorithmException {
        Objects.requireNonNull(str);
        return this.b.encodeHexString(a().digest(str.getBytes(this.a)));
    }

    @NonNull
    private synchronized MessageDigest a() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance("MD5");
    }
}
