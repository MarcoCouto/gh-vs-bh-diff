package com.smaato.sdk.core.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.ResourceLoader.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkComponentException;

public class ResourceLoaderException extends Exception implements SdkComponentException<Error> {
    @NonNull
    private final Error a;
    @NonNull
    private final Exception b;

    public ResourceLoaderException(@NonNull Error error, @NonNull Exception exc) {
        this.a = (Error) Objects.requireNonNull(error);
        this.b = (Exception) Objects.requireNonNull(exc);
    }

    @NonNull
    public Exception getReason() {
        return this.b;
    }

    @NonNull
    public Error getErrorType() {
        return this.a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ResourceLoaderException { errorType = ");
        sb.append(this.a);
        sb.append(", reason = ");
        sb.append(this.b);
        sb.append(" }");
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ResourceLoaderException resourceLoaderException = (ResourceLoaderException) obj;
        return this.a == resourceLoaderException.a && Objects.equals(this.b, resourceLoaderException.b);
    }

    public int hashCode() {
        return Objects.hash(this.a, this.b);
    }
}
