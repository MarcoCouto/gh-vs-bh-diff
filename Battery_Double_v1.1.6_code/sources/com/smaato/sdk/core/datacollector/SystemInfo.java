package com.smaato.sdk.core.datacollector;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.network.NetworkConnectionType;
import com.smaato.sdk.core.util.Objects;

public final class SystemInfo {
    @Nullable
    private final String a;
    @Nullable
    private final String b;
    @Nullable
    private final String c;
    @Nullable
    private final Boolean d;
    @Nullable
    private final String e;
    @Nullable
    private final NetworkConnectionType f;
    @NonNull
    private final String g;
    @NonNull
    private final String h;

    SystemInfo(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable Boolean bool, @Nullable String str4, @Nullable NetworkConnectionType networkConnectionType, @NonNull String str5, @NonNull String str6) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = bool;
        this.e = str4;
        this.f = networkConnectionType;
        this.h = (String) Objects.requireNonNull(str5, "Parameter userAgent cannot be null for SystemInfo::SystemInfo");
        this.g = (String) Objects.requireNonNull(str6, "Parameter packageName cannot be null for SystemInfo::SystemInfo");
    }

    @Nullable
    public final String getCarrierName() {
        return this.a;
    }

    @Nullable
    public final String getCarrierCode() {
        return this.b;
    }

    @Nullable
    public final String getGoogleAdvertisingId() {
        return this.c;
    }

    @Nullable
    public final Boolean isGoogleLimitAdTrackingEnabled() {
        return this.d;
    }

    @Nullable
    public final String getDeviceModelName() {
        return this.e;
    }

    @Nullable
    public final NetworkConnectionType getNetworkConnectionType() {
        return this.f;
    }

    @NonNull
    public final String getPackageName() {
        return this.g;
    }

    @NonNull
    public final String getUserAgent() {
        return this.h;
    }
}
