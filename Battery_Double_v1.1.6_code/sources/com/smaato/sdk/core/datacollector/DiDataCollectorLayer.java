package com.smaato.sdk.core.datacollector;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import androidx.annotation.NonNull;
import com.facebook.places.model.PlaceFields;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.gdpr.SomaGdprDataSource;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.core.util.Objects;
import java.util.concurrent.ExecutorService;

public final class DiDataCollectorLayer {
    private DiDataCollectorLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiDataCollectorLayer$EXdvsUFX2FARtBQVUObPtYZwgLA.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory("DATA_COLLECTION_EXECUTOR_SERVICE", ExecutorService.class, $$Lambda$DiDataCollectorLayer$0E1sFA3lGWNSrPigr0xSZwkSt_E.INSTANCE);
        diRegistry.registerSingletonFactory(DataCollector.class, $$Lambda$DiDataCollectorLayer$WJYwTCsozTP4sdUvmti_vgcKRxY.INSTANCE);
        diRegistry.registerSingletonFactory(TelephonyManager.class, $$Lambda$DiDataCollectorLayer$lxOYrg3WF1LeLMsOspTGQilsvDs.INSTANCE);
        diRegistry.registerSingletonFactory(ContentResolver.class, $$Lambda$DiDataCollectorLayer$NLgqeJuFOGDViDXekoOKKrB0PpU.INSTANCE);
        diRegistry.registerSingletonFactory(SystemInfoProvider.class, $$Lambda$DiDataCollectorLayer$jrnjcJHiXzHC9hh9AVaQYqDcmfw.INSTANCE);
        diRegistry.registerSingletonFactory(LocationProvider.class, $$Lambda$DiDataCollectorLayer$M6p6q01PMfYbXjjsOhstUR0k_Zk.INSTANCE);
        diRegistry.registerFactory(LocationManager.class, $$Lambda$DiDataCollectorLayer$blekUWFZXITNPdyOq0oY0L5ndS0.INSTANCE);
        diRegistry.registerFactory(LocationConfig.class, $$Lambda$DiDataCollectorLayer$VbFNRJlXC11lnCabBYHvD01rbM.INSTANCE);
        diRegistry.registerFactory(UserAgentProvider.class, $$Lambda$DiDataCollectorLayer$Zey8qDPsEfP0OcqHgNRxQeB_BZc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ DataCollector h(DiConstructor diConstructor) {
        return new DataCollector((SystemInfoProvider) diConstructor.get(SystemInfoProvider.class), (LocationProvider) diConstructor.get(LocationProvider.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ TelephonyManager g(DiConstructor diConstructor) {
        return (TelephonyManager) Objects.requireNonNull((TelephonyManager) ((Application) diConstructor.get(Application.class)).getSystemService(PlaceFields.PHONE));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ContentResolver f(DiConstructor diConstructor) {
        return (ContentResolver) Objects.requireNonNull(((Application) diConstructor.get(Application.class)).getContentResolver());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SystemInfoProvider e(DiConstructor diConstructor) {
        SystemInfoProvider systemInfoProvider = new SystemInfoProvider((Logger) diConstructor.get(Logger.class), (Context) diConstructor.get(Application.class), DiNetworkLayer.getNetworkStateMonitorFrom(diConstructor), (TelephonyManager) diConstructor.get(TelephonyManager.class), (ExecutorService) diConstructor.get("DATA_COLLECTION_EXECUTOR_SERVICE", ExecutorService.class), (UserAgentProvider) diConstructor.get(UserAgentProvider.class));
        return systemInfoProvider;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ LocationProvider d(DiConstructor diConstructor) {
        LocationProvider locationProvider = new LocationProvider(DiLogLayer.getLoggerFrom(diConstructor), (LocationManager) diConstructor.get(LocationManager.class), (LocationConfig) diConstructor.get(LocationConfig.class), (AppMetaData) diConstructor.get(AppMetaData.class), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), (SomaGdprDataSource) diConstructor.get(SomaGdprDataSource.class));
        return locationProvider;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ LocationManager c(DiConstructor diConstructor) {
        return (LocationManager) Objects.requireNonNull(((Application) diConstructor.get(Application.class)).getSystemService("location"));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ LocationConfig b(DiConstructor diConstructor) {
        LocationConfig locationConfig = new LocationConfig(DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, 0.0f, Long.MAX_VALUE);
        return locationConfig;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ UserAgentProvider a(DiConstructor diConstructor) {
        return new UserAgentProvider((Context) diConstructor.get(Application.class));
    }
}
