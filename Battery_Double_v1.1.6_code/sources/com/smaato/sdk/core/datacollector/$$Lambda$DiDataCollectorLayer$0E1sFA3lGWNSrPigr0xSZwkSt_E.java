package com.smaato.sdk.core.datacollector;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.core.datacollector.-$$Lambda$DiDataCollectorLayer$0E1sFA3lGWNSrPigr0xSZwkSt_E reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiDataCollectorLayer$0E1sFA3lGWNSrPigr0xSZwkSt_E implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiDataCollectorLayer$0E1sFA3lGWNSrPigr0xSZwkSt_E INSTANCE = new $$Lambda$DiDataCollectorLayer$0E1sFA3lGWNSrPigr0xSZwkSt_E();

    private /* synthetic */ $$Lambda$DiDataCollectorLayer$0E1sFA3lGWNSrPigr0xSZwkSt_E() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newSingleThreadExecutor();
    }
}
