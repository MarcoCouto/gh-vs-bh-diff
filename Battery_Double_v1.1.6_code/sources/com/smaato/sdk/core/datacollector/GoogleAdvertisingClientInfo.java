package com.smaato.sdk.core.datacollector;

import androidx.annotation.Nullable;

public final class GoogleAdvertisingClientInfo {
    @Nullable
    private final String a;
    private final boolean b;

    public GoogleAdvertisingClientInfo(@Nullable String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    @Nullable
    public final String getAdvertisingId() {
        return this.a;
    }

    public final boolean isLimitAdTrackingEnabled() {
        return this.b;
    }
}
