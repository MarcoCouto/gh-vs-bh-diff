package com.smaato.sdk.core.datacollector;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkStateMonitor;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Pair;
import com.smaato.sdk.core.util.reflection.MethodAccessor.Builder;
import com.smaato.sdk.core.util.reflection.MethodAccessor.MethodAccessingException;
import com.smaato.sdk.core.util.reflection.Reflections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class SystemInfoProvider {
    @NonNull
    private final Logger a;
    @NonNull
    private final Context b;
    @NonNull
    private final NetworkStateMonitor c;
    @NonNull
    private final TelephonyManager d;
    @NonNull
    private final ExecutorService e;
    @NonNull
    private final UserAgentProvider f;
    @Nullable
    private GoogleAdvertisingClientInfo g;
    @Nullable
    private Future h;

    SystemInfoProvider(@NonNull Logger logger, @NonNull Context context, @NonNull NetworkStateMonitor networkStateMonitor, @NonNull TelephonyManager telephonyManager, @NonNull ExecutorService executorService, @NonNull UserAgentProvider userAgentProvider) {
        this.a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for SystemInfoProvider::new");
        this.b = (Context) Objects.requireNonNull(context, "Parameter context cannot be null for SystemInfoProvider::new");
        this.c = (NetworkStateMonitor) Objects.requireNonNull(networkStateMonitor, "Parameter networkStateMonitor cannot be null for SystemInfoProvider::new");
        this.d = (TelephonyManager) Objects.requireNonNull(telephonyManager, "Parameter telephonyManager cannot be null for SystemInfoProvider::new");
        this.e = (ExecutorService) Objects.requireNonNull(executorService, "Parameter executorService cannot be null for SystemInfoProvider::new");
        this.f = (UserAgentProvider) Objects.requireNonNull(userAgentProvider, "Parameter userAgentProvider cannot be null for SystemInfoProvider::new");
    }

    @NonNull
    public SystemInfo getSystemInfoSnapshot() {
        String simOperatorName = this.d.getSimOperatorName();
        String simOperator = this.d.getSimOperator();
        GoogleAdvertisingClientInfo googleAdvertisingClientInfo = this.g;
        SystemInfo systemInfo = new SystemInfo(simOperatorName, simOperator, (String) Objects.transformOrNull(googleAdvertisingClientInfo, $$Lambda$GO_lA4WitxSmPAnr6G0BjX_TVKI.INSTANCE), (Boolean) Objects.transformOrNull(googleAdvertisingClientInfo, $$Lambda$gVylY46Wvwp1OInI80o4WgpqcIQ.INSTANCE), Build.MODEL, this.c.getNetworkConnectionType(), this.f.get(), this.b.getPackageName());
        if (Reflections.isClassInClasspath("com.google.android.gms.ads.identifier.AdvertisingIdClient")) {
            synchronized (this) {
                if (this.h == null) {
                    this.h = this.e.submit(new Runnable() {
                        public final void run() {
                            SystemInfoProvider.this.b();
                        }
                    });
                }
            }
        }
        return systemInfo;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        this.g = a();
        synchronized (this) {
            this.h = null;
        }
    }

    @Nullable
    private GoogleAdvertisingClientInfo a() {
        try {
            Object execute = new Builder().fromClassInstance("com.google.android.gms.ads.identifier.AdvertisingIdClient").setMethodName("getAdvertisingIdInfo").withParametersOfResolvedTypes(Pair.of(Context.class, this.b)).build().execute();
            if (execute != null) {
                return new GoogleAdvertisingClientInfo((String) new Builder().fromObjectInstance(execute).setMethodName("getId").build().execute(), ((Boolean) new Builder().fromObjectInstance(execute).setMethodName(RequestParameters.isLAT).build().execute()).booleanValue());
            }
            this.a.error(LogDomain.DATA_COLLECTOR, "Cannot fetch AdvertisingIdClient.Info: null received", new Object[0]);
            return null;
        } catch (MethodAccessingException | ClassNotFoundException e2) {
            this.a.error(LogDomain.DATA_COLLECTOR, e2, "Cannot fetch AdvertisingIdClient.Info", new Object[0]);
            return null;
        }
    }
}
