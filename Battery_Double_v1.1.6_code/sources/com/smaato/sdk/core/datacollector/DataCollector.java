package com.smaato.sdk.core.datacollector;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.LatLng;
import com.smaato.sdk.core.util.Objects;

public class DataCollector {
    @NonNull
    private final SystemInfoProvider a;
    @NonNull
    private final LocationProvider b;

    DataCollector(@NonNull SystemInfoProvider systemInfoProvider, @NonNull LocationProvider locationProvider) {
        this.a = (SystemInfoProvider) Objects.requireNonNull(systemInfoProvider, "Parameter systemInfoProvider cannot be null for DataCollector::new");
        this.b = (LocationProvider) Objects.requireNonNull(locationProvider, "Parameter locationProvider cannot be null for DataCollector::new");
    }

    @NonNull
    public SystemInfo getSystemInfo() {
        return this.a.getSystemInfoSnapshot();
    }

    @Nullable
    public LatLng getLocationData() {
        return this.b.getLocationData();
    }
}
