package com.smaato.sdk.core.datacollector;

import android.annotation.SuppressLint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.LatLng;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector.Listener;
import com.smaato.sdk.core.gdpr.PiiParam;
import com.smaato.sdk.core.gdpr.SomaGdprDataSource;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.core.util.Objects;

public class LocationProvider {
    @NonNull
    private final LocationConfig a;
    @NonNull
    private final SomaGdprDataSource b;
    /* access modifiers changed from: private */
    @NonNull
    public final LocationListener c = new LocationListener() {
        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }

        public void onLocationChanged(Location location) {
            LocationProvider.a(LocationProvider.this, location);
        }
    };
    @Nullable
    private Location d = null;
    private long e = 0;

    @SuppressLint({"MissingPermission"})
    LocationProvider(@NonNull Logger logger, @NonNull LocationManager locationManager, @NonNull LocationConfig locationConfig, @NonNull AppMetaData appMetaData, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull SomaGdprDataSource somaGdprDataSource) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(locationManager);
        this.a = (LocationConfig) Objects.requireNonNull(locationConfig);
        Objects.requireNonNull(appMetaData);
        Objects.requireNonNull(appBackgroundDetector);
        this.b = (SomaGdprDataSource) Objects.requireNonNull(somaGdprDataSource);
        final String str = "network";
        if (locationManager.getProviders(false).contains(str)) {
            final LocationManager locationManager2 = locationManager;
            final AppMetaData appMetaData2 = appMetaData;
            final LocationConfig locationConfig2 = locationConfig;
            final Logger logger2 = logger;
            AnonymousClass2 r0 = new Listener() {
                public void onAppEnteredInBackground() {
                    locationManager2.removeUpdates(LocationProvider.this.c);
                }

                public void onAppEnteredInForeground() {
                    if (appMetaData2.isPermissionGranted("android.permission.ACCESS_FINE_LOCATION") || appMetaData2.isPermissionGranted("android.permission.ACCESS_COARSE_LOCATION")) {
                        LocationProvider.a(LocationProvider.this, locationManager2.getLastKnownLocation(str));
                        locationManager2.requestLocationUpdates(str, locationConfig2.a(), locationConfig2.b(), LocationProvider.this.c, Looper.getMainLooper());
                        return;
                    }
                    logger2.warning(LogDomain.DATA_COLLECTOR, "No permissions granted to receive location", new Object[0]);
                }
            };
            appBackgroundDetector.addListener(r0, true);
            return;
        }
        logger.warning(LogDomain.DATA_COLLECTOR, "No coarse (network) provider of location", new Object[0]);
    }

    @Nullable
    public LatLng getLocationData() {
        if (this.d != null) {
            if (!(SystemClock.elapsedRealtime() - this.e >= this.a.c()) && this.b.getSomaGdprData().isUsageAllowedFor(PiiParam.GPS) && SmaatoSdk.isGPSEnabled()) {
                LatLng latLng = new LatLng(this.d.getLatitude(), this.d.getLongitude(), this.d.getAccuracy(), this.e);
                return latLng;
            }
        }
        return null;
    }

    static /* synthetic */ void a(LocationProvider locationProvider, Location location) {
        locationProvider.d = location;
        locationProvider.e = SystemClock.elapsedRealtime();
    }
}
