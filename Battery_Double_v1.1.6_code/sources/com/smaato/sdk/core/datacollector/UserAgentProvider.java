package com.smaato.sdk.core.datacollector;

import android.content.Context;
import android.os.Build.VERSION;
import android.webkit.WebSettings;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.NullableSupplier;
import com.smaato.sdk.core.util.fi.Supplier;

class UserAgentProvider implements Supplier<String> {
    @NonNull
    private final Context a;

    UserAgentProvider(@NonNull Context context) {
        this.a = (Context) Objects.requireNonNull(context);
    }

    @NonNull
    public String get() {
        String str;
        if (VERSION.SDK_INT >= 17) {
            str = WebSettings.getDefaultUserAgent(this.a);
        } else {
            str = (String) Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier() {
                public final Object get() {
                    return UserAgentProvider.this.a();
                }
            });
        }
        if (str == null) {
            str = System.getProperty("http.agent");
        }
        return str == null ? "" : str;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ String a() {
        return new WebView(this.a).getSettings().getUserAgentString();
    }
}
