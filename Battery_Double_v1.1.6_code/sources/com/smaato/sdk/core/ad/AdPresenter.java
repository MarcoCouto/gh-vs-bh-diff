package com.smaato.sdk.core.ad;

import android.content.Context;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ui.AdContentView;

public interface AdPresenter {

    public interface Listener<T extends AdPresenter> {
        void onAdClicked(@NonNull T t);

        void onAdError(@NonNull T t);

        void onAdImpressed(@NonNull T t);

        void onTTLExpired(@NonNull T t);
    }

    @NonNull
    AdContentView getAdContentView(@NonNull Context context);

    @NonNull
    AdInteractor<? extends AdObject> getAdInteractor();

    @NonNull
    String getAdSpaceId();

    @Nullable
    String getCreativeId();

    @NonNull
    String getPublisherId();

    @NonNull
    String getSessionId();

    boolean isInUse();

    boolean isValid();

    @MainThread
    void release();

    void releaseAccess();

    boolean retainAccess();
}
