package com.smaato.sdk.core.ad;

import androidx.annotation.Nullable;
import com.smaato.sdk.core.KeyValuePairs;

public class SharedKeyValuePairsHolder {
    @Nullable
    private KeyValuePairs a;

    @Nullable
    public KeyValuePairs getKeyValuePairs() {
        if (this.a == null) {
            return null;
        }
        return this.a.clone();
    }

    public void setKeyValuePairs(@Nullable KeyValuePairs keyValuePairs) {
        this.a = keyValuePairs == null ? null : keyValuePairs.clone();
    }
}
