package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkComponentExceptionUtil;
import com.smaato.sdk.core.violationreporter.AdQualityViolationException;
import com.smaato.sdk.core.violationreporter.AdQualityViolationReporter;

public final class AdLoaderAdQualityViolationUtils {
    @NonNull
    private final AdQualityViolationReporter a;

    public AdLoaderAdQualityViolationUtils(@NonNull AdQualityViolationReporter adQualityViolationReporter) {
        this.a = (AdQualityViolationReporter) Objects.requireNonNull(adQualityViolationReporter);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull AdPresenterBuilderException adPresenterBuilderException) {
        Exception rootReason = SdkComponentExceptionUtil.getRootReason(adPresenterBuilderException);
        if (rootReason instanceof AdQualityViolationException) {
            AdQualityViolationException adQualityViolationException = (AdQualityViolationException) rootReason;
            this.a.reportAdViolation(adQualityViolationException.adQualityViolationType, adQualityViolationException.somaApiContext, adQualityViolationException.violatedUrl, adQualityViolationException.originalUrl);
        }
    }
}
