package com.smaato.sdk.core.ad;

import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface InterstitialAdPresenter extends AdPresenter {

    public interface Listener extends com.smaato.sdk.core.ad.AdPresenter.Listener<InterstitialAdPresenter> {
        void onAdUnload(@NonNull InterstitialAdPresenter interstitialAdPresenter);

        void onClose(@NonNull InterstitialAdPresenter interstitialAdPresenter);

        void onShowCloseButton();
    }

    @MainThread
    void onCloseClicked();

    void setFriendlyObstructionView(@NonNull View view);

    void setListener(@Nullable Listener listener);
}
