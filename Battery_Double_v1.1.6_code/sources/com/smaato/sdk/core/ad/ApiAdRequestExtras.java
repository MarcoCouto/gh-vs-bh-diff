package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import java.util.HashMap;
import java.util.Map;

public class ApiAdRequestExtras {
    @NonNull
    private final AdFormat a;
    @NonNull
    private final Map<String, Object> b = new HashMap();

    public ApiAdRequestExtras(@NonNull AdFormat adFormat) {
        this.a = (AdFormat) Objects.requireNonNull(adFormat);
    }

    @NonNull
    public AdFormat adFormat() {
        return this.a;
    }

    public void addApiParamExtra(@NonNull String str, @NonNull Object obj) {
        this.b.put(str, obj);
    }

    @NonNull
    public Map<String, Object> toMap() {
        return new HashMap(this.b);
    }
}
