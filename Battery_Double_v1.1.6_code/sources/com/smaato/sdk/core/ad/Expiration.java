package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.CurrentTimeProvider;
import com.smaato.sdk.core.util.Objects;

public class Expiration {
    private final long a;
    @NonNull
    private final CurrentTimeProvider b;

    public Expiration(long j, @NonNull CurrentTimeProvider currentTimeProvider) {
        this.a = j;
        this.b = (CurrentTimeProvider) Objects.requireNonNull(currentTimeProvider);
    }

    public long getTimestamp() {
        return this.a;
    }

    public boolean isExpired() {
        return this.a <= this.b.currentMillisUtc();
    }

    public long getRemainingTime() {
        long currentMillisUtc = this.a - this.b.currentMillisUtc();
        if (currentMillisUtc > 0) {
            return currentMillisUtc;
        }
        return 0;
    }

    @NonNull
    public String toString() {
        return String.valueOf(this.a);
    }
}
