package com.smaato.sdk.core.ad;

import android.app.Application;
import android.content.Context;
import android.preference.PreferenceManager;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.ad.-$$Lambda$DiAdLayer$75eVD6m1KME1a8rBNHLzaT01Ubo reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiAdLayer$75eVD6m1KME1a8rBNHLzaT01Ubo implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiAdLayer$75eVD6m1KME1a8rBNHLzaT01Ubo INSTANCE = new $$Lambda$DiAdLayer$75eVD6m1KME1a8rBNHLzaT01Ubo();

    private /* synthetic */ $$Lambda$DiAdLayer$75eVD6m1KME1a8rBNHLzaT01Ubo() {
    }

    public final Object get(DiConstructor diConstructor) {
        return PreferenceManager.getDefaultSharedPreferences((Context) diConstructor.get(Application.class));
    }
}
