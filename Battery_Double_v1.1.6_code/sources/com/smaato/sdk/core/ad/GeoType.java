package com.smaato.sdk.core.ad;

public enum GeoType {
    GPS,
    USER_PROVIDED
}
