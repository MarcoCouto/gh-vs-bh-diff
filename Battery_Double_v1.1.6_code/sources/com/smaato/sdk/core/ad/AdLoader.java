package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.ErrorReporting;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.ad.AdLoader.Listener;
import com.smaato.sdk.core.api.ApiAdRequest;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.api.ApiConnector;
import com.smaato.sdk.core.api.ApiConnectorException;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.repository.AdTypeStrategy;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.SdkConfigHintBuilder;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.atomic.AtomicReference;

public class AdLoader {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    @NonNull
    private final ApiConnector b;
    @NonNull
    private final AdLoaderPlugin c;
    @NonNull
    private final AdRequestMapper d;
    /* access modifiers changed from: private */
    @NonNull
    public final AdLoaderAdQualityViolationUtils e;
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<TaskRequestHolder> f = new AtomicReference<>();
    @NonNull
    private final SdkConfigHintBuilder g;
    @Nullable
    private final ErrorReporting h;
    /* access modifiers changed from: private */
    @Nullable
    public Listener i;

    private final class AdPresenterBuilderListener implements com.smaato.sdk.core.ad.AdPresenterBuilder.Listener {
        @NonNull
        private final SomaApiContext a;

        /* synthetic */ AdPresenterBuilderListener(AdLoader adLoader, SomaApiContext somaApiContext, byte b2) {
            this(somaApiContext);
        }

        private AdPresenterBuilderListener(SomaApiContext somaApiContext) {
            this.a = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        }

        public final void onAdPresenterBuildSuccess(@NonNull AdPresenterBuilder adPresenterBuilder, @NonNull AdPresenter adPresenter) {
            Objects.requireNonNull(adPresenterBuilder);
            Objects.requireNonNull(adPresenter);
            AdLoader.this.a.debug(LogDomain.AD, "%s building with %s finished with success", AdPresenter.class.getSimpleName(), adPresenterBuilder.getClass().getSimpleName());
            Objects.onNotNull(AdLoader.this.i, new Consumer(adPresenter) {
                private final /* synthetic */ AdPresenter f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AdPresenterBuilderListener.this.a(this.f$1, (Listener) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(AdPresenter adPresenter, Listener listener) {
            listener.onAdLoadSuccess(AdLoader.this, adPresenter);
        }

        public final void onAdPresenterBuildError(@NonNull AdPresenterBuilder adPresenterBuilder, @NonNull AdPresenterBuilderException adPresenterBuilderException) {
            Error error;
            Objects.requireNonNull(adPresenterBuilder);
            Objects.requireNonNull(adPresenterBuilderException);
            AdLoader.this.a.error(LogDomain.AD, "Failed to build %s with %s. Error: %s", AdPresenter.class.getSimpleName(), adPresenterBuilder.getClass().getSimpleName(), adPresenterBuilderException);
            AdLoader.this.e.a(adPresenterBuilderException);
            switch (adPresenterBuilderException.getErrorType()) {
                case CANCELLED:
                    error = Error.CANCELLED;
                    break;
                case INVALID_RESPONSE:
                    error = Error.INVALID_RESPONSE;
                    break;
                case RESOURCE_EXPIRED:
                    error = Error.CREATIVE_RESOURCE_EXPIRED;
                    break;
                case TRANSPORT_NO_NETWORK_CONNECTION:
                    error = Error.NO_CONNECTION;
                    break;
                case TRANSPORT_TIMEOUT:
                case TRANSPORT_IO_ERROR:
                case TRANSPORT_IO_TOO_MANY_REDIRECTS:
                case TRANSPORT_GENERIC:
                    error = Error.NETWORK;
                    break;
                case GENERIC:
                    error = Error.PRESENTER_BUILDER_GENERIC;
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Unexpected %s %s: %s", new Object[]{AdPresenterBuilder.class.getSimpleName(), com.smaato.sdk.core.ad.AdPresenterBuilder.Error.class.getSimpleName(), adPresenterBuilderException}));
            }
            Objects.onNotNull(AdLoader.this.i, new Consumer(error, adPresenterBuilderException) {
                private final /* synthetic */ Error f$1;
                private final /* synthetic */ AdPresenterBuilderException f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    AdPresenterBuilderListener.this.a(this.f$1, this.f$2, (Listener) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Error error, AdPresenterBuilderException adPresenterBuilderException, Listener listener) {
            listener.onAdLoadError(AdLoader.this, new AdLoaderException(error, adPresenterBuilderException));
        }
    }

    public enum Error {
        NO_AD,
        BAD_REQUEST,
        PRESENTER_BUILDER_GENERIC,
        INVALID_RESPONSE,
        API,
        CANCELLED,
        NETWORK,
        NO_CONNECTION,
        CONFIGURATION_ERROR,
        INTERNAL,
        CREATIVE_RESOURCE_EXPIRED,
        TTL_EXPIRED,
        NO_MANDATORY_CACHE,
        TOO_MANY_REQUESTS
    }

    public interface Listener {
        void onAdLoadError(@NonNull AdLoader adLoader, @NonNull AdLoaderException adLoaderException);

        void onAdLoadSuccess(@NonNull AdLoader adLoader, @NonNull AdPresenter adPresenter);
    }

    @VisibleForTesting
    static final class TaskRequestHolder {
        @NonNull
        final Task a;
        @NonNull
        final ApiAdRequest b;
        @NonNull
        final Class<? extends AdPresenter> c;

        /* synthetic */ TaskRequestHolder(Task task, Class cls, ApiAdRequest apiAdRequest, byte b2) {
            this(task, cls, apiAdRequest);
        }

        private TaskRequestHolder(@NonNull Task task, @NonNull Class<? extends AdPresenter> cls, @NonNull ApiAdRequest apiAdRequest) {
            this.a = (Task) Objects.requireNonNull(task, "Parameter task cannot be null for TaskRequestHolder::new");
            this.c = (Class) Objects.requireNonNull(cls, "Parameter clazz cannot be null for TaskRequestHolder::new");
            this.b = (ApiAdRequest) Objects.requireNonNull(apiAdRequest, "Parameter apiAdRequest cannot be null for TaskRequestHolder::new");
        }
    }

    public AdLoader(@NonNull final Logger logger, @NonNull AdRequestMapper adRequestMapper, @NonNull AdLoaderPlugin adLoaderPlugin, @NonNull ApiConnector apiConnector, @NonNull SdkConfigHintBuilder sdkConfigHintBuilder, @NonNull AdLoaderAdQualityViolationUtils adLoaderAdQualityViolationUtils, @Nullable ErrorReporting errorReporting) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.d = (AdRequestMapper) Objects.requireNonNull(adRequestMapper);
        this.c = (AdLoaderPlugin) Objects.requireNonNull(adLoaderPlugin);
        this.b = (ApiConnector) Objects.requireNonNull(apiConnector);
        this.g = (SdkConfigHintBuilder) Objects.requireNonNull(sdkConfigHintBuilder);
        this.e = (AdLoaderAdQualityViolationUtils) Objects.requireNonNull(adLoaderAdQualityViolationUtils);
        this.h = errorReporting;
        this.b.setListener(new com.smaato.sdk.core.api.ApiConnector.Listener() {
            public void onAdRequestSuccess(@NonNull ApiConnector apiConnector, @NonNull Task task, @NonNull ApiAdResponse apiAdResponse) {
                Objects.requireNonNull(apiConnector);
                Objects.requireNonNull(apiAdResponse);
                TaskRequestHolder taskRequestHolder = (TaskRequestHolder) AdLoader.this.f.get();
                if (AdLoader.a(AdLoader.this, taskRequestHolder, task)) {
                    AdLoader.this.a(new SomaApiContext(taskRequestHolder.b, apiAdResponse), taskRequestHolder.c, (com.smaato.sdk.core.ad.AdPresenterBuilder.Listener) new AdPresenterBuilderListener(AdLoader.this, new SomaApiContext(taskRequestHolder.b, apiAdResponse), 0));
                }
            }

            public void onAdRequestError(@NonNull ApiConnector apiConnector, @NonNull Task task, @NonNull ApiConnectorException apiConnectorException) {
                Objects.requireNonNull(apiConnector);
                Objects.requireNonNull(apiConnectorException);
                if (AdLoader.a(AdLoader.this, (TaskRequestHolder) AdLoader.this.f.get(), task)) {
                    logger.error(LogDomain.AD, "Failed to perform ad request. Error: %s", apiConnectorException);
                    AdLoader.a(AdLoader.this, apiConnectorException);
                }
            }
        });
    }

    public void setListener(@NonNull Listener listener) {
        this.i = (Listener) Objects.requireNonNull(listener);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(UnresolvedServerAdFormatException unresolvedServerAdFormatException, Listener listener) {
        listener.onAdLoadError(this, new AdLoaderException(Error.CONFIGURATION_ERROR, unresolvedServerAdFormatException));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Exception exc, Listener listener) {
        listener.onAdLoadError(this, new AdLoaderException(Error.INTERNAL, exc));
    }

    /* access modifiers changed from: private */
    public void a(@NonNull SomaApiContext somaApiContext, @NonNull Class<? extends AdPresenter> cls, com.smaato.sdk.core.ad.AdPresenterBuilder.Listener listener) {
        this.a.debug(LogDomain.AD, "Requesting an Ad finished with success", new Object[0]);
        AdFormat adFormat = somaApiContext.getApiAdResponse().getAdFormat();
        AdPresenterBuilder adPresenterBuilder = this.c.getAdPresenterBuilder(adFormat, cls, this.a);
        if (adPresenterBuilder == null) {
            this.a.warning(LogDomain.AD, "No %s implementation was found for %s: %s", AdPresenterBuilder.class.getSimpleName(), AdFormat.class.getSimpleName(), adFormat);
            String buildSdkModuleMissedHintForAdFormat = this.g.buildSdkModuleMissedHintForAdFormat(adFormat);
            this.a.error(LogDomain.AD, buildSdkModuleMissedHintForAdFormat, new Object[0]);
            Objects.onNotNull(this.i, new Consumer(Error.CONFIGURATION_ERROR, buildSdkModuleMissedHintForAdFormat) {
                private final /* synthetic */ Error f$1;
                private final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    AdLoader.this.a(this.f$1, this.f$2, (Listener) obj);
                }
            });
            return;
        }
        adPresenterBuilder.buildAdPresenter(somaApiContext, listener);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Error error, String str, Listener listener) {
        listener.onAdLoadError(this, new AdLoaderException(error, new RuntimeException(str)));
    }

    public void cancel() {
        TaskRequestHolder taskRequestHolder = (TaskRequestHolder) this.f.get();
        if (taskRequestHolder == null) {
            this.a.debug(LogDomain.AD, "There is no request currently running. Nothing to cancel", new Object[0]);
            return;
        }
        this.a.debug(LogDomain.AD, "Canceling request: %s", taskRequestHolder);
        taskRequestHolder.a.cancel();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Error error, ApiConnectorException apiConnectorException, Listener listener) {
        listener.onAdLoadError(this, new AdLoaderException(error, apiConnectorException));
    }

    public void reportCacheEntryExpired(AdRequest adRequest, ApiAdResponse apiAdResponse, long j) {
        if (this.h != null) {
            this.h.report(new SomaApiContext(this.d.map(adRequest), apiAdResponse), Error.TTL_EXPIRED, j);
        }
    }

    public void buildAdPresenter(AdTypeStrategy adTypeStrategy, AdRequest adRequest, ApiAdResponse apiAdResponse, com.smaato.sdk.core.ad.AdPresenterBuilder.Listener listener) {
        a(new SomaApiContext(this.d.map(adRequest), apiAdResponse), adTypeStrategy.getAdPresenterClass(), listener);
    }

    public void requestAd(@NonNull AdRequest adRequest, @NonNull AdTypeStrategy adTypeStrategy) {
        try {
            Objects.requireNonNull(adRequest);
            Objects.requireNonNull(adTypeStrategy);
            Objects.onNotNull(this.f.get(), $$Lambda$AdLoader$qWHNxFdsXZCXCKD3DOn1MNUses.INSTANCE);
            Class adPresenterClass = adTypeStrategy.getAdPresenterClass();
            ApiAdRequest map = this.d.map(adRequest);
            Task performApiAdRequest = this.b.performApiAdRequest(map);
            this.f.set(new TaskRequestHolder(performApiAdRequest, adPresenterClass, map, 0));
            performApiAdRequest.start();
        } catch (UnresolvedServerAdFormatException e2) {
            this.a.error(LogDomain.AD, e2, "Configuration error: can not resolve ad format", new Object[0]);
            Objects.onNotNull(this.i, new Consumer(e2) {
                private final /* synthetic */ UnresolvedServerAdFormatException f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AdLoader.this.a(this.f$1, (Listener) obj);
                }
            });
        } catch (Exception e3) {
            this.a.error(LogDomain.AD, e3, "Internal error", new Object[0]);
            Objects.onNotNull(this.i, new Consumer(e3) {
                private final /* synthetic */ Exception f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AdLoader.this.a(this.f$1, (Listener) obj);
                }
            });
        }
    }

    static /* synthetic */ boolean a(AdLoader adLoader, TaskRequestHolder taskRequestHolder, Task task) {
        if (taskRequestHolder == null) {
            adLoader.a.info(LogDomain.AD, "There is no request currently running. Callback was not expected", new Object[0]);
            return false;
        } else if (task == taskRequestHolder.a) {
            return true;
        } else {
            adLoader.a.info(LogDomain.AD, "Unknown task. Current task=%s, received task=%s", taskRequestHolder.a, task);
            return false;
        }
    }

    static /* synthetic */ void a(AdLoader adLoader, ApiConnectorException apiConnectorException) {
        Error error;
        switch (apiConnectorException.getErrorType()) {
            case NO_AD:
                error = Error.NO_AD;
                break;
            case BAD_REQUEST:
                error = Error.BAD_REQUEST;
                break;
            case RESPONSE_MAPPING:
                error = Error.API;
                break;
            case TRANSPORT_NO_NETWORK_CONNECTION:
                error = Error.NO_CONNECTION;
                break;
            case TRANSPORT_TIMEOUT:
            case TRANSPORT_GENERIC:
            case TRANSPORT_IO_ERROR:
                error = Error.NETWORK;
                break;
            case CANCELLED:
                error = Error.CANCELLED;
                break;
            default:
                throw new IllegalArgumentException(String.format("Unexpected %s %s: %s", new Object[]{ApiConnector.class.getSimpleName(), com.smaato.sdk.core.api.ApiConnector.Error.class.getSimpleName(), apiConnectorException}));
        }
        Objects.onNotNull(adLoader.i, new Consumer(error, apiConnectorException) {
            private final /* synthetic */ Error f$1;
            private final /* synthetic */ ApiConnectorException f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                AdLoader.this.a(this.f$1, this.f$2, (Listener) obj);
            }
        });
    }
}
