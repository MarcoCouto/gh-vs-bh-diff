package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface BannerAdPresenter extends AdPresenter {

    public interface Listener extends com.smaato.sdk.core.ad.AdPresenter.Listener<BannerAdPresenter> {
        void onAdClosed();

        void onAdExpanded(@NonNull BannerAdPresenter bannerAdPresenter);

        void onAdResized();

        void onAdUnload(@NonNull BannerAdPresenter bannerAdPresenter);
    }

    void initialize();

    void setListener(@Nullable Listener listener);
}
