package com.smaato.sdk.core.ad;

public final class AdStateMachine {

    public enum Event {
        INITIALISE,
        ADDED_ON_SCREEN,
        IMPRESSION,
        CLICK,
        CLOSE,
        EXPIRE_TTL,
        AD_ERROR,
        DESTROY
    }

    public enum State {
        INIT,
        CREATED,
        ON_SCREEN,
        IMPRESSED,
        CLICKED,
        COMPLETE,
        TO_BE_DELETED
    }

    private AdStateMachine() {
    }
}
