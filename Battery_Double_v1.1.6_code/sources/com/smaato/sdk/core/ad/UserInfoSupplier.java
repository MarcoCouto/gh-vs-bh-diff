package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.ad.UserInfo.Builder;
import com.smaato.sdk.core.util.fi.Supplier;

public class UserInfoSupplier implements Supplier<UserInfo> {
    @NonNull
    public UserInfo get() {
        return new Builder().setKeywords(SmaatoSdk.getKeywords()).setSearchQuery(SmaatoSdk.getSearchQuery()).setGender(SmaatoSdk.getGender()).setAge(SmaatoSdk.getAge()).setLatLng(SmaatoSdk.getLatLng()).setRegion(SmaatoSdk.getRegion()).setZip(SmaatoSdk.getZip()).setLanguage(SmaatoSdk.getLanguage()).setCoppa(SmaatoSdk.getCoppa()).build();
    }
}
