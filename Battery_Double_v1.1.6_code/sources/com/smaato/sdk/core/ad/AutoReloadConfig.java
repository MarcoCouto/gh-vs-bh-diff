package com.smaato.sdk.core.ad;

public interface AutoReloadConfig {
    int defaultInterval();

    int maxInterval();

    int minInterval();
}
