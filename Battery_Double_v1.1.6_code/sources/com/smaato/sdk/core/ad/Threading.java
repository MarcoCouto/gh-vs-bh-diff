package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;

public interface Threading {
    void runOnBackgroundThread(@NonNull Runnable runnable);
}
