package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.util.Objects;

public class AdResponseCacheEntry {
    @NonNull
    public final ApiAdResponse apiAdResponse;
    public final long requestTimestamp;
    @NonNull
    public final String uniqueId;

    public AdResponseCacheEntry(@NonNull String str, @NonNull ApiAdResponse apiAdResponse2, long j) {
        this.uniqueId = (String) Objects.requireNonNull(str);
        this.apiAdResponse = (ApiAdResponse) Objects.requireNonNull(apiAdResponse2);
        this.requestTimestamp = j;
    }

    @NonNull
    public ApiAdResponse getApiAdResponse() {
        return this.apiAdResponse;
    }

    public long getRequestTimestamp() {
        return this.requestTimestamp;
    }
}
