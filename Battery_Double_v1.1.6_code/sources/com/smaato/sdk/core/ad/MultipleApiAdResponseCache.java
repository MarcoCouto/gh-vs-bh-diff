package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.config.ConfigurationRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class MultipleApiAdResponseCache implements ApiAdResponseCache {
    @NonNull
    private final ConfigurationRepository a;
    @NonNull
    private final Map<String, Queue<AdResponseCacheEntry>> b = new ConcurrentHashMap();

    public MultipleApiAdResponseCache(@NonNull ConfigurationRepository configurationRepository) {
        this.a = configurationRepository;
    }

    @Nullable
    public final AdResponseCacheEntry get(@NonNull String str) {
        return (AdResponseCacheEntry) a(str).poll();
    }

    @Nullable
    public final AdResponseCacheEntry getById(@NonNull String str, @NonNull String str2) {
        str2.getClass();
        for (AdResponseCacheEntry adResponseCacheEntry : a(str)) {
            if (adResponseCacheEntry.uniqueId.equals(str2)) {
                return adResponseCacheEntry;
            }
        }
        return null;
    }

    public final boolean put(@NonNull String str, @NonNull AdResponseCacheEntry adResponseCacheEntry) {
        return a(str).offer(adResponseCacheEntry);
    }

    public final void remove(@NonNull String str, @NonNull AdResponseCacheEntry adResponseCacheEntry) {
        Queue queue = (Queue) this.b.get(str);
        if (queue != null) {
            queue.remove(adResponseCacheEntry);
        }
    }

    @NonNull
    public final Collection<AdResponseCacheEntry> trim(@NonNull String str) {
        Queue<AdResponseCacheEntry> a2 = a(str);
        ArrayList arrayList = new ArrayList();
        for (AdResponseCacheEntry adResponseCacheEntry : a2) {
            if (adResponseCacheEntry.apiAdResponse.getExpiration().isExpired()) {
                arrayList.add(adResponseCacheEntry);
            }
        }
        a2.removeAll(arrayList);
        return arrayList;
    }

    public final int remainingCapacity(@NonNull String str) {
        return this.a.get().cachingCapacity - a(str).size();
    }

    public final int perKeyCapacity() {
        return this.a.get().cachingCapacity;
    }

    @NonNull
    private Queue<AdResponseCacheEntry> a(@NonNull String str) {
        Queue<AdResponseCacheEntry> queue = (Queue) this.b.get(str);
        if (queue != null) {
            return queue;
        }
        ConcurrentLinkedQueue concurrentLinkedQueue = new ConcurrentLinkedQueue();
        this.b.put(str, concurrentLinkedQueue);
        return concurrentLinkedQueue;
    }
}
