package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Joiner;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Arrays;

public class AdPresenterNameShaper {
    @NonNull
    public String shapeName(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls) {
        return Joiner.join((CharSequence) EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, (Iterable) Arrays.asList(new String[]{adFormat.toString(), cls.getSimpleName()}));
    }
}
