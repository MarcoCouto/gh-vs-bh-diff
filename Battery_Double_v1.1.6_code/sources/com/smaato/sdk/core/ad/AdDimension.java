package com.smaato.sdk.core.ad;

import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public enum AdDimension {
    XX_LARGE(ModuleDescriptor.MODULE_VERSION, 50),
    X_LARGE(300, 50),
    LARGE(216, 36),
    MEDIUM(168, 28),
    SMALL(120, 20),
    MEDIUM_RECTANGLE(300, 250),
    SKYSCRAPER(120, 600),
    LEADERBOARD(728, 90),
    FULLSCREEN_PORTRAIT(ModuleDescriptor.MODULE_VERSION, 480),
    FULLSCREEN_LANDSCAPE(480, ModuleDescriptor.MODULE_VERSION),
    FULLSCREEN_PORTRAIT_TABLET(768, 1024),
    FULLSCREEN_LANDSCAPE_TABLET(1024, 768);
    
    private final int a;
    private final int b;
    private final float c;

    private AdDimension(int i, int i2) {
        this.a = i;
        this.b = i2;
        this.c = ((float) i) / ((float) i2);
    }

    public final int getWidth() {
        return this.a;
    }

    public final int getHeight() {
        return this.b;
    }

    public final float getAspectRatio() {
        return this.c;
    }
}
