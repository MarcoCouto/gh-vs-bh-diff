package com.smaato.sdk.core.ad;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface RewardedAdPresenter extends AdPresenter {

    public interface Listener extends com.smaato.sdk.core.ad.AdPresenter.Listener<RewardedAdPresenter> {
        void onClose(@NonNull RewardedAdPresenter rewardedAdPresenter);

        void onCompleted(@NonNull RewardedAdPresenter rewardedAdPresenter);

        void onStart(@NonNull RewardedAdPresenter rewardedAdPresenter);
    }

    public interface OnCloseEnabledListener {
        void onClose(@NonNull RewardedAdPresenter rewardedAdPresenter);

        void onCloseEnabled(@NonNull RewardedAdPresenter rewardedAdPresenter);
    }

    @MainThread
    void onCloseClicked();

    void setListener(@Nullable Listener listener);

    void setOnCloseEnabledListener(@Nullable OnCloseEnabledListener onCloseEnabledListener);
}
