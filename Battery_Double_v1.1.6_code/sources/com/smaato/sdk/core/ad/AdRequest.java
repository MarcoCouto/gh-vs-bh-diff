package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;

public final class AdRequest {
    @NonNull
    public final AdSettings adSettings;
    @Nullable
    public final KeyValuePairs keyValuePairs;
    @NonNull
    public final UserInfo userInfo;

    public static final class Builder {
        @Nullable
        private AdSettings a;
        @Nullable
        private UserInfo b;
        @Nullable
        private KeyValuePairs c;

        @NonNull
        public final Builder setAdSettings(@NonNull AdSettings adSettings) {
            this.a = adSettings;
            return this;
        }

        @NonNull
        public final Builder setUserInfo(@NonNull UserInfo userInfo) {
            this.b = userInfo;
            return this;
        }

        @NonNull
        public final Builder setKeyValuePairs(@Nullable KeyValuePairs keyValuePairs) {
            this.c = keyValuePairs;
            return this;
        }

        @NonNull
        public final AdRequest build() {
            ArrayList arrayList = new ArrayList();
            if (this.a == null) {
                arrayList.add("adSettings");
            }
            if (this.b == null) {
                arrayList.add("userInfo");
            }
            if (arrayList.isEmpty()) {
                return new AdRequest(this.a, this.b, this.c, 0);
            }
            StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
            sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
            throw new IllegalStateException(sb.toString());
        }
    }

    /* synthetic */ AdRequest(AdSettings adSettings2, UserInfo userInfo2, KeyValuePairs keyValuePairs2, byte b) {
        this(adSettings2, userInfo2, keyValuePairs2);
    }

    private AdRequest(@NonNull AdSettings adSettings2, @NonNull UserInfo userInfo2, @Nullable KeyValuePairs keyValuePairs2) {
        this.adSettings = (AdSettings) Objects.requireNonNull(adSettings2);
        this.userInfo = (UserInfo) Objects.requireNonNull(userInfo2);
        this.keyValuePairs = keyValuePairs2;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdRequest{adSettings=");
        sb.append(this.adSettings);
        sb.append(", userInfo=");
        sb.append(this.userInfo);
        sb.append(", keyValuePairs=");
        sb.append(this.keyValuePairs);
        sb.append('}');
        return sb.toString();
    }
}
