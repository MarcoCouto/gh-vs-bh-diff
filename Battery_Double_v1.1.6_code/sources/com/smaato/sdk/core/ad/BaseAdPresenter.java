package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Threads;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class BaseAdPresenter implements AdPresenter {
    @NonNull
    private final AdInteractor<? extends AdObject> a;
    @NonNull
    private final AtomicBoolean b = new AtomicBoolean();

    /* access modifiers changed from: protected */
    public abstract void onDestroy();

    public BaseAdPresenter(@NonNull AdInteractor<? extends AdObject> adInteractor) {
        this.a = adInteractor;
    }

    public final boolean retainAccess() {
        return this.b.compareAndSet(false, true);
    }

    public final void releaseAccess() {
        this.b.set(false);
    }

    public final boolean isInUse() {
        return this.b.get();
    }

    @NonNull
    public final AdInteractor<? extends AdObject> getAdInteractor() {
        return this.a;
    }

    @NonNull
    public final String getPublisherId() {
        return this.a.getPublisherId();
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.a.getAdSpaceId();
    }

    @NonNull
    public final String getSessionId() {
        return this.a.getSessionId();
    }

    @Nullable
    public final String getCreativeId() {
        return this.a.getCreativeId();
    }

    public final boolean isValid() {
        return this.a.isValid();
    }

    public final void release() {
        Threads.ensureMainThread();
        onDestroy();
        releaseAccess();
    }
}
