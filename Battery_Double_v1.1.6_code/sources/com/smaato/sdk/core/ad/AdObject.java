package com.smaato.sdk.core.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;

public interface AdObject {
    @NonNull
    SomaApiContext getSomaApiContext();
}
