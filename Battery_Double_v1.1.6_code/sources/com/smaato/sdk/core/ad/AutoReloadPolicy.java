package com.smaato.sdk.core.ad;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;

public final class AutoReloadPolicy {
    @NonNull
    private final Logger a;
    @NonNull
    private final AutoReloadConfig b;
    private int c;
    @NonNull
    private final AppBackgroundAwareHandler d;

    public AutoReloadPolicy(@NonNull Logger logger, @NonNull AutoReloadConfig autoReloadConfig, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler) {
        this.a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for AutoReloadPolicy::new");
        this.b = (AutoReloadConfig) Objects.requireNonNull(autoReloadConfig, "Parameter autoReloadConfig cannot be null for AutoReloadPolicy::new");
        this.c = autoReloadConfig.defaultInterval();
        this.d = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
    }

    public final void setAutoReloadInterval(@IntRange(from = 0, to = 240) int i) {
        if (i == 0) {
            this.c = i;
            stopTimer();
            this.a.info(LogDomain.AD, "Ad auto-reload has been turned off.", new Object[0]);
            return;
        }
        int minInterval = this.b.minInterval();
        if (i < minInterval) {
            this.c = minInterval;
            this.a.info(LogDomain.AD, "Ad auto-reload interval %d is too small, setting %d seconds.", Integer.valueOf(i), Integer.valueOf(minInterval));
            return;
        }
        int maxInterval = this.b.maxInterval();
        if (i > maxInterval) {
            this.c = maxInterval;
            this.a.info(LogDomain.AD, "Ad auto-reload interval %f is too large, setting %f seconds.", Integer.valueOf(i), Integer.valueOf(maxInterval));
            return;
        }
        this.c = i;
        this.a.info(LogDomain.AD, "Ad auto-reload interval is set to %d seconds.", Integer.valueOf(i));
    }

    public final int getAutoReloadInterval() {
        return this.c;
    }

    public final boolean isAutoReloadEnabled() {
        return this.c > 0;
    }

    public final void stopTimer() {
        this.d.stop();
    }

    public final void startWithAction(@Nullable Runnable runnable) {
        if (runnable == null) {
            this.a.info(LogDomain.AD, "No action to perform", new Object[0]);
            return;
        }
        stopTimer();
        if (isAutoReloadEnabled()) {
            long j = (long) (this.c * 1000);
            this.a.info(LogDomain.AD, "starting timer for %d millis", Long.valueOf(j));
            this.d.postDelayed("Ad auto-reload timer", runnable, j, null);
        }
    }
}
