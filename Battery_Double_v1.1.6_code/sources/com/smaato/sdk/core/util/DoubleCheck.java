package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Supplier;

class DoubleCheck<T> implements Lazy<T> {
    private volatile Object a;
    private volatile Supplier<T> b;

    DoubleCheck(@NonNull Supplier<T> supplier) {
        this.b = supplier;
    }

    @NonNull
    public T get() {
        T t = this.a;
        if (t == null) {
            synchronized (this) {
                t = this.a;
                if (t == null) {
                    t = this.b.get();
                    this.a = t;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
