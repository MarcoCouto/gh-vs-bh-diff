package com.smaato.sdk.core.util;

import com.github.mikephil.charting.utils.Utils;

public final class Precision {
    public static final double EPSILON = Double.longBitsToDouble(4368491638549381120L);
    public static final double SAFE_MIN = Double.longBitsToDouble(4503599627370496L);
    private static final long a = Double.doubleToRawLongBits(Utils.DOUBLE_EPSILON);
    private static final long b = Double.doubleToRawLongBits(-0.0d);
    private static final int c = Float.floatToRawIntBits(0.0f);
    private static final int d = Float.floatToRawIntBits(-0.0f);

    private Precision() {
    }

    public static int compareTo(double d2, double d3, double d4) {
        if (equals(d2, d3, d4)) {
            return 0;
        }
        return d2 < d3 ? -1 : 1;
    }

    public static int compareTo(double d2, double d3, int i) {
        if (equals(d2, d3, i)) {
            return 0;
        }
        return d2 < d3 ? -1 : 1;
    }

    public static boolean equals(float f, float f2) {
        return equals(f, f2, 1);
    }

    public static boolean equalsIncludingNaN(float f, float f2) {
        if (f == f && f2 == f2) {
            return equals(f, f2, 1);
        }
        return !(((f > f ? 1 : (f == f ? 0 : -1)) != 0) ^ ((f2 > f2 ? 1 : (f2 == f2 ? 0 : -1)) != 0));
    }

    public static boolean equals(float f, float f2, float f3) {
        return equals(f, f2, 1) || Math.abs(f2 - f) <= f3;
    }

    public static boolean equalsIncludingNaN(float f, float f2, float f3) {
        return equalsIncludingNaN(f, f2) || Math.abs(f2 - f) <= f3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        if (r1 <= (r8 - r0)) goto L_0x0018;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0016, code lost:
        if (java.lang.Math.abs(r0 - r1) <= r8) goto L_0x0018;
     */
    public static boolean equals(float f, float f2, int i) {
        boolean z;
        int i2;
        int i3;
        int floatToRawIntBits = Float.floatToRawIntBits(f);
        int floatToRawIntBits2 = Float.floatToRawIntBits(f2);
        if (((floatToRawIntBits ^ floatToRawIntBits2) & Integer.MIN_VALUE) != 0) {
            if (floatToRawIntBits < floatToRawIntBits2) {
                int i4 = floatToRawIntBits2 - c;
                i2 = floatToRawIntBits - d;
                i3 = i4;
            } else {
                i3 = floatToRawIntBits - c;
                i2 = floatToRawIntBits2 - d;
            }
            if (i3 <= i) {
            }
            z = false;
            return !z && !Float.isNaN(f) && !Float.isNaN(f2);
        }
        z = true;
        if (!z) {
        }
    }

    public static boolean equalsIncludingNaN(float f, float f2, int i) {
        if (f == f && f2 == f2) {
            return equals(f, f2, i);
        }
        return !(((f > f ? 1 : (f == f ? 0 : -1)) != 0) ^ ((f2 > f2 ? 1 : (f2 == f2 ? 0 : -1)) != 0));
    }

    public static boolean equals(double d2, double d3) {
        return equals(d2, d3, 1);
    }

    public static boolean equalsIncludingNaN(double d2, double d3) {
        if (d2 == d2 && d3 == d3) {
            return equals(d2, d3, 1);
        }
        return !(((d2 > d2 ? 1 : (d2 == d2 ? 0 : -1)) != 0) ^ ((d3 > d3 ? 1 : (d3 == d3 ? 0 : -1)) != 0));
    }

    public static boolean equals(double d2, double d3, double d4) {
        return equals(d2, d3, 1) || Math.abs(d3 - d2) <= d4;
    }

    public static boolean equalsIncludingNaN(double d2, double d3, double d4) {
        return equalsIncludingNaN(d2, d3) || Math.abs(d3 - d2) <= d4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        if (r2 <= (r6 - r0)) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001d, code lost:
        if (java.lang.Math.abs(r0 - r2) <= ((long) r15)) goto L_0x001f;
     */
    public static boolean equals(double d2, double d3, int i) {
        boolean z;
        long j;
        long j2;
        long doubleToRawLongBits = Double.doubleToRawLongBits(d2);
        long doubleToRawLongBits2 = Double.doubleToRawLongBits(d3);
        if (((doubleToRawLongBits ^ doubleToRawLongBits2) & Long.MIN_VALUE) != 0) {
            if (doubleToRawLongBits < doubleToRawLongBits2) {
                long j3 = doubleToRawLongBits - b;
                j2 = doubleToRawLongBits2 - a;
                j = j3;
            } else {
                j2 = doubleToRawLongBits - a;
                j = doubleToRawLongBits2 - b;
            }
            long j4 = (long) i;
            if (j2 <= j4) {
            }
            z = false;
            return !z && !Double.isNaN(d2) && !Double.isNaN(d3);
        }
        z = true;
        if (!z) {
        }
    }

    public static boolean equalsIncludingNaN(double d2, double d3, int i) {
        if (d2 == d2 && d3 == d3) {
            return equals(d2, d3, i);
        }
        return !(((d3 > d3 ? 1 : (d3 == d3 ? 0 : -1)) != 0) ^ ((d2 > d2 ? 1 : (d2 == d2 ? 0 : -1)) != 0));
    }
}
