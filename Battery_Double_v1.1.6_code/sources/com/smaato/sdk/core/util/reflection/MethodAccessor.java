package com.smaato.sdk.core.util.reflection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Pair;
import java.lang.reflect.Method;
import java.util.ArrayList;

public final class MethodAccessor {
    @NonNull
    private final Class<?> a;
    @Nullable
    private final Object b;
    @NonNull
    private final String c;
    @Nullable
    private final Class[] d;
    @Nullable
    private final Object[] e;

    public static final class Builder {
        private Object a;
        private Class<?> b;
        private Class[] c;
        private Object[] d;
        private String e;

        @NonNull
        public final Builder fromObjectInstance(@NonNull Object obj) {
            this.a = Objects.requireNonNull(obj, "Parameter instance cannot be null for Builder::fromObjectInstance");
            this.b = obj.getClass();
            return this;
        }

        @NonNull
        public final Builder fromClassInstance(@NonNull String str) throws ClassNotFoundException {
            Objects.requireNonNull(str, "Parameter className cannot be null for Builder::fromClassInstance");
            this.b = Class.forName(str);
            return this;
        }

        @NonNull
        public final Builder fromResolvedClassInstance(@NonNull Class<?> cls) {
            this.b = (Class) Objects.requireNonNull(cls, "Parameter clazz cannot be null for Builder::fromResolvedClassInstance");
            return this;
        }

        @NonNull
        public final Builder setMethodName(@NonNull String str) {
            this.e = (String) Objects.requireNonNull(str, "Parameter methodName cannot be null for Builder::setMethodName");
            return this;
        }

        @SafeVarargs
        @NonNull
        public final Builder withParameters(@NonNull Pair<String, Object>... pairArr) throws ClassNotFoundException {
            this.c = new Class[pairArr.length];
            this.d = new Object[pairArr.length];
            for (int i = 0; i < pairArr.length; i++) {
                Pair pair = (Pair) Objects.requireNonNull(pairArr[i], String.format("Parameter classNameToObjectInstanceArray[%d] cannot be null for Builder::withParameters", new Object[]{Integer.valueOf(i)}));
                this.c[i] = Class.forName((String) pair.first);
                this.d[i] = pair.second;
            }
            return this;
        }

        @NonNull
        @SafeVarargs
        public final <T> Builder withParametersOfResolvedTypes(@NonNull Pair<Class<T>, T>... pairArr) {
            this.c = new Class[pairArr.length];
            this.d = new Object[pairArr.length];
            for (int i = 0; i < pairArr.length; i++) {
                Pair pair = (Pair) Objects.requireNonNull(pairArr[i], String.format("Parameter classToObjectInstanceArray[%d] cannot be null for Builder::withParametersOfResolvedTypes", new Object[]{Integer.valueOf(i)}));
                this.c[i] = (Class) pair.first;
                this.d[i] = pair.second;
            }
            return this;
        }

        @NonNull
        public final MethodAccessor build() {
            ArrayList arrayList = new ArrayList();
            if (this.b == null) {
                arrayList.add("clazz");
            }
            if (this.e == null) {
                arrayList.add("methodName");
            }
            if (arrayList.isEmpty()) {
                MethodAccessor methodAccessor = new MethodAccessor(this.b, this.a, this.e, this.c, this.d, 0);
                return methodAccessor;
            }
            StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
            sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
            throw new IllegalStateException(sb.toString());
        }
    }

    public static final class MethodAccessingException extends Exception {
        public MethodAccessingException(Throwable th) {
            super(th);
        }
    }

    /* synthetic */ MethodAccessor(Class cls, Object obj, String str, Class[] clsArr, Object[] objArr, byte b2) {
        this(cls, obj, str, clsArr, objArr);
    }

    private MethodAccessor(@NonNull Class<?> cls, @Nullable Object obj, @NonNull String str, @Nullable Class[] clsArr, @Nullable Object[] objArr) {
        this.a = (Class) Objects.requireNonNull(cls, "Parameter clazz cannot be null for MethodAccessor::MethodAccessor");
        this.b = obj;
        this.c = (String) Objects.requireNonNull(str, "Parameter methodName cannot be null for MethodAccessor::MethodAccessor");
        this.d = clsArr;
        this.e = objArr;
    }

    @Nullable
    public final Object execute() throws MethodAccessingException {
        try {
            return a().invoke(this.b, this.e);
        } catch (Exception e2) {
            throw new MethodAccessingException(e2);
        }
    }

    @NonNull
    private Method a() throws NoSuchMethodException {
        Class<?> cls = this.a;
        while (cls != null) {
            try {
                return cls.getDeclaredMethod(this.c, this.d);
            } catch (NoSuchMethodException unused) {
                cls = cls.getSuperclass();
            }
        }
        throw new NoSuchMethodException();
    }
}
