package com.smaato.sdk.core.util;

public final class Bytes {
    private Bytes() {
    }

    public static int getIntValueFrom(byte[] bArr, int i, int i2) {
        if (bArr.length <= 0) {
            return -1;
        }
        int i3 = i + i2;
        byte b = 0;
        while (i < i3) {
            int i4 = i;
            byte b2 = 0;
            for (int i5 = 0; i5 < 8 && i4 < i3; i5++) {
                b2 = (byte) (b2 | ((byte) (getByteFrom(bArr, i4) << (7 - i5))));
                i4++;
            }
            b = (b2 & 255) | (b << 8);
            i = i4;
        }
        return b >> ((8 - (i2 % 8)) % 8);
    }

    public static byte getByteFrom(byte[] bArr, int i) {
        int i2 = i / 8;
        return (byte) ((bArr[i2] >> (7 - (i % 8))) & 1);
    }
}
