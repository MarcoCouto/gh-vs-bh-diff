package com.smaato.sdk.core.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;

public final class Intents {
    private Intents() {
    }

    @NonNull
    public static Intent createViewIntent(@NonNull String str) {
        return new Intent("android.intent.action.VIEW", Uri.parse(str));
    }

    public static void startIntent(@NonNull Context context, @NonNull Intent intent) {
        Threads.runOnUi(new Runnable(context, intent) {
            private final /* synthetic */ Context f$0;
            private final /* synthetic */ Intent f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void run() {
                Intents.a(this.f$0, this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Context context, Intent intent) {
        if (canHandleIntent(context, intent)) {
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            context.startActivity(intent);
        }
    }

    public static boolean canHandleIntent(@NonNull Context context, @NonNull Intent intent) {
        if (!context.getPackageManager().queryIntentActivities(intent, 0).isEmpty()) {
            return true;
        }
        return false;
    }
}
