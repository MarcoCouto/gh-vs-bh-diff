package com.smaato.sdk.core.util;

import android.os.Handler;
import androidx.annotation.NonNull;

public class OneTimeAction {
    /* access modifiers changed from: private */
    @NonNull
    public final Handler a;
    /* access modifiers changed from: private */
    @NonNull
    public final Listener b;
    /* access modifiers changed from: private */
    public boolean c;
    @NonNull
    private final Runnable d = new Runnable() {
        public void run() {
            Threads.ensureHandlerThread(OneTimeAction.this.a);
            OneTimeAction.this.c = false;
            OneTimeAction.this.b.doAction();
        }
    };

    @FunctionalInterface
    public interface Listener {
        void doAction();
    }

    OneTimeAction(@NonNull Handler handler, @NonNull Listener listener) {
        this.a = (Handler) Objects.requireNonNull(handler);
        this.b = (Listener) Objects.requireNonNull(listener);
    }

    public void start(long j) {
        Threads.ensureHandlerThread(this.a);
        if (!this.c) {
            this.c = true;
            this.a.postDelayed(this.d, j);
        }
    }

    public void stop() {
        Threads.ensureHandlerThread(this.a);
        if (this.c) {
            this.a.removeCallbacks(this.d);
            this.c = false;
        }
    }

    public boolean isScheduled() {
        return this.c;
    }
}
