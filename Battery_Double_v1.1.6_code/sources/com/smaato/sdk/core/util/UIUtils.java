package com.smaato.sdk.core.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.fi.Function;

public final class UIUtils {
    public static int dpToPx(float f, float f2) {
        return (int) ((f * f2) + 0.5f);
    }

    private UIUtils() {
    }

    public static int dpToPx(@NonNull Context context, float f) {
        return (int) (f * context.getResources().getDisplayMetrics().density);
    }

    public static int pxToDp(@NonNull Context context, float f) {
        return Math.round(f / context.getResources().getDisplayMetrics().density);
    }

    public static int getDisplayWidthInDp() {
        return a((Function<DisplayMetrics, Integer>) $$Lambda$UIUtils$IucPmzO3gW6L2VLwwVaod8fCFQM.INSTANCE);
    }

    public static int getDisplayHeightInDp() {
        return a((Function<DisplayMetrics, Integer>) $$Lambda$UIUtils$zVN4snN6ddBpE4rfv3HnupEfiPQ.INSTANCE);
    }

    public static float getNormalizedSize(@Nullable Float f) {
        if (f == null || f.floatValue() <= 0.0f) {
            return 0.0f;
        }
        return f.floatValue();
    }

    @NonNull
    public static Size getDisplaySizeInDp(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return new Size(pxToDp(context, (float) displayMetrics.widthPixels), pxToDp(context, (float) displayMetrics.heightPixels));
    }

    private static int a(@NonNull Function<DisplayMetrics, Integer> function) {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        return (int) (((float) ((Integer) function.apply(displayMetrics)).intValue()) / displayMetrics.density);
    }
}
