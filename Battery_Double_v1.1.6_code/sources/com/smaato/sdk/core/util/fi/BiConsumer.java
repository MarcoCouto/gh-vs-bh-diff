package com.smaato.sdk.core.util.fi;

@FunctionalInterface
public interface BiConsumer<T, U> {
    void accept(T t, U u);
}
