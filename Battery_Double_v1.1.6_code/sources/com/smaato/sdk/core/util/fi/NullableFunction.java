package com.smaato.sdk.core.util.fi;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface NullableFunction<T, R> {

    /* renamed from: com.smaato.sdk.core.util.fi.NullableFunction$-CC reason: invalid class name */
    public final /* synthetic */ class CC {
        public static /* synthetic */ Object a(Object obj) {
            return obj;
        }

        @NonNull
        public static <T> NullableFunction<T, T> identity() {
            return $$Lambda$NullableFunction$gpyaOmSndhqcd5vsJPEhi8_Yu9g.INSTANCE;
        }
    }

    @Nullable
    R apply(@Nullable T t);
}
