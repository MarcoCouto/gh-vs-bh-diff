package com.smaato.sdk.core.util.fi;

public interface Cancellable {
    void cancel();
}
