package com.smaato.sdk.core.util.fi;

import androidx.annotation.NonNull;

@FunctionalInterface
public interface BiFunction<T, U, R> {
    @NonNull
    R apply(@NonNull T t, @NonNull U u);
}
