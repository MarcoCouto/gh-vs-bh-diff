package com.smaato.sdk.core.util.fi;

import androidx.annotation.NonNull;

public interface Function<T, R> {
    @NonNull
    R apply(@NonNull T t);
}
