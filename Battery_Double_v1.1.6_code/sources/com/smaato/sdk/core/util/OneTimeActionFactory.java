package com.smaato.sdk.core.util;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.OneTimeAction.Listener;

public class OneTimeActionFactory {
    @NonNull
    private final Handler a;

    public OneTimeActionFactory(@NonNull Handler handler) {
        this.a = (Handler) Objects.requireNonNull(handler);
    }

    @NonNull
    public OneTimeAction createOneTimeAction(@NonNull Listener listener) {
        return new OneTimeAction(this.a, listener);
    }
}
