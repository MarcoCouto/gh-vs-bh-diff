package com.smaato.sdk.core.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class ViewUtils {
    private ViewUtils() {
    }

    @Nullable
    public static View getRootView(@NonNull View view) {
        Context context = view.getContext();
        if (context instanceof Activity) {
            return ((Activity) context).getWindow().getDecorView().findViewById(16908290);
        }
        View rootView = view.getRootView();
        if (rootView == null) {
            return null;
        }
        View findViewById = rootView.findViewById(16908290);
        return findViewById == null ? rootView : findViewById;
    }

    @Nullable
    public static ViewGroup getParent(@NonNull View view) {
        ViewParent parent = view.getParent();
        if (parent != null && (parent instanceof ViewGroup)) {
            return (ViewGroup) parent;
        }
        return null;
    }

    public static void removeFromParent(@NonNull View view) {
        ViewGroup parent = getParent(view);
        if (parent != null) {
            parent.removeView(view);
        }
    }
}
