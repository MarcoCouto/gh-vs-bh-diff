package com.smaato.sdk.core.util;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class Metadata {
    @NonNull
    private final Bundle a;

    public static class Builder {
        @NonNull
        private final Bundle a = new Bundle();

        @NonNull
        public Builder putInt(@NonNull String str, int i) {
            this.a.putInt(str, i);
            return this;
        }

        @NonNull
        public Metadata build() {
            return new Metadata(this.a, 0);
        }
    }

    /* synthetic */ Metadata(Bundle bundle, byte b) {
        this(bundle);
    }

    private Metadata(@NonNull Bundle bundle) {
        this.a = (Bundle) Objects.requireNonNull(bundle);
    }

    @Nullable
    public final Integer getInt(@NonNull String str) {
        int i = this.a.getInt(str, -1);
        if (i == -1) {
            return null;
        }
        return Integer.valueOf(i);
    }
}
