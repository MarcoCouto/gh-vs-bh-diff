package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public final class Either<Left, Right> {
    @Nullable
    private final Left a;
    @Nullable
    private final Right b;

    @VisibleForTesting
    private Either(@Nullable Left left, @Nullable Right right) {
        if (left == null && right == null) {
            throw new IllegalArgumentException("Both parameters are null. Either left or right parameter should be not null");
        } else if (left == null || right == null) {
            this.a = left;
            this.b = right;
        } else {
            throw new IllegalArgumentException("Both parameters are not null. Either left or right parameter should be null");
        }
    }

    @Nullable
    public final Left left() {
        return this.a;
    }

    @Nullable
    public final Right right() {
        return this.b;
    }

    @NonNull
    public static <Left, Right> Either<Left, Right> left(@NonNull Left left) {
        return new Either<>(left, null);
    }

    @NonNull
    public static <Left, Right> Either<Left, Right> right(@NonNull Right right) {
        return new Either<>(null, right);
    }
}
