package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class Pair<T1, T2> {
    @Nullable
    public final T1 first;
    @Nullable
    public final T2 second;

    private Pair(@Nullable T1 t1, @Nullable T2 t2) {
        this.first = t1;
        this.second = t2;
    }

    @NonNull
    public static <T1, T2> Pair<T1, T2> of(@Nullable T1 t1, @Nullable T2 t2) {
        return new Pair<>(t1, t2);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("Pair{first=");
        sb.append(this.first);
        sb.append(", second=");
        sb.append(this.second);
        sb.append('}');
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Pair pair = (Pair) obj;
        if (this.first == null ? pair.first != null : !this.first.equals(pair.first)) {
            return false;
        }
        if (this.second != null) {
            return this.second.equals(pair.second);
        }
        return pair.second == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (this.first != null ? this.first.hashCode() : 0) * 31;
        if (this.second != null) {
            i = this.second.hashCode();
        }
        return hashCode + i;
    }
}
