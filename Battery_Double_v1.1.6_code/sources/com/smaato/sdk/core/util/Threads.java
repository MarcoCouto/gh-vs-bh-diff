package com.smaato.sdk.core.util;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.util.fi.NullableSupplier;
import com.smaato.sdk.core.util.fi.Supplier;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class Threads {
    @VisibleForTesting
    private static volatile Handler a;
    @Nullable
    private static Executor b;

    private Threads() {
    }

    public static void ensureMainThread() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("This method should be called only on MainThread");
        }
    }

    public static void ensureNotMainThread() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("This method should NOT be called on MainThread");
        }
    }

    public static boolean isHandlerThread(@NonNull Handler handler) {
        return handler.getLooper().getThread() == Thread.currentThread();
    }

    public static void ensureInvokedOnHandlerThread(@NonNull Handler handler, @NonNull Runnable runnable) {
        if (isHandlerThread(handler)) {
            runnable.run();
        } else {
            handler.post(runnable);
        }
    }

    public static void ensureHandlerThread(@NonNull Handler handler) {
        if (!isHandlerThread(handler)) {
            throw new IllegalStateException("This method should be called only from a thread bound to the handler");
        }
    }

    @NonNull
    public static Handler newUiHandler() {
        return new Handler(Looper.getMainLooper());
    }

    public static boolean runOnUi(@NonNull Runnable runnable) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return a().post(runnable);
        }
        runnable.run();
        return true;
    }

    public static boolean runOnNextUiFrame(@NonNull Runnable runnable) {
        return a().post(runnable);
    }

    public static boolean runOnHandlerThreadBlocking(@NonNull Handler handler, @NonNull Runnable runnable) {
        if (isHandlerThread(handler)) {
            runnable.run();
            return true;
        }
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        boolean post = handler.post(new Runnable(runnable, atomicBoolean) {
            private final /* synthetic */ Runnable f$0;
            private final /* synthetic */ AtomicBoolean f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void run() {
                Threads.b(this.f$0, this.f$1);
            }
        });
        if (post) {
            do {
            } while (!atomicBoolean.get());
        }
        return post;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(Runnable runnable, AtomicBoolean atomicBoolean) {
        runnable.run();
        atomicBoolean.set(true);
    }

    @NonNull
    public static <T> T runOnHandlerThreadBlocking(@NonNull Handler handler, @NonNull Supplier<T> supplier) {
        if (isHandlerThread(handler)) {
            return supplier.get();
        }
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        AtomicReference atomicReference = new AtomicReference();
        if (handler.post(new Runnable(atomicReference, supplier, atomicBoolean) {
            private final /* synthetic */ AtomicReference f$0;
            private final /* synthetic */ Supplier f$1;
            private final /* synthetic */ AtomicBoolean f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                Threads.a(this.f$0, this.f$1, this.f$2);
            }
        })) {
            do {
            } while (!atomicBoolean.get());
        }
        return atomicReference.get();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AtomicReference atomicReference, Supplier supplier, AtomicBoolean atomicBoolean) {
        atomicReference.set(supplier.get());
        atomicBoolean.set(true);
    }

    public static boolean runOnUiBlocking(@NonNull Runnable runnable) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
            return true;
        }
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        boolean post = a().post(new Runnable(runnable, atomicBoolean) {
            private final /* synthetic */ Runnable f$0;
            private final /* synthetic */ AtomicBoolean f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void run() {
                Threads.a(this.f$0, this.f$1);
            }
        });
        if (post) {
            do {
            } while (!atomicBoolean.get());
        }
        return post;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Runnable runnable, AtomicBoolean atomicBoolean) {
        runnable.run();
        atomicBoolean.set(true);
    }

    @Nullable
    public static <T> T runOnUiBlocking(@NonNull NullableSupplier<T> nullableSupplier) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            return nullableSupplier.get();
        }
        CountDownLatch countDownLatch = new CountDownLatch(1);
        AtomicReference atomicReference = new AtomicReference();
        if (a().post(new Runnable(atomicReference, nullableSupplier, countDownLatch) {
            private final /* synthetic */ AtomicReference f$0;
            private final /* synthetic */ NullableSupplier f$1;
            private final /* synthetic */ CountDownLatch f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                Threads.a(this.f$0, this.f$1, this.f$2);
            }
        })) {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                String name = LogDomain.CORE.name();
                StringBuilder sb = new StringBuilder("Internal error while executing on MainThread: ");
                sb.append(e.getMessage());
                Log.e(name, sb.toString());
                return null;
            }
        }
        return atomicReference.get();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AtomicReference atomicReference, NullableSupplier nullableSupplier, CountDownLatch countDownLatch) {
        atomicReference.set(nullableSupplier.get());
        countDownLatch.countDown();
    }

    public static void runOnBackgroundThread(@NonNull Runnable runnable) {
        b().execute(runnable);
    }

    @NonNull
    private static Handler a() {
        if (a == null) {
            synchronized (Threads.class) {
                if (a == null) {
                    a = newUiHandler();
                }
            }
        }
        return a;
    }

    @NonNull
    private static Executor b() {
        if (b == null) {
            synchronized (Threads.class) {
                if (b == null) {
                    b = Executors.newSingleThreadExecutor();
                }
            }
        }
        return b;
    }
}
