package com.smaato.sdk.core.util.notifier;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Threads;

public final class TimerUtils {
    private TimerUtils() {
    }

    @NonNull
    public static Timer createStandardTimer(long j) {
        return new StandardTimer(Threads.newUiHandler(), j);
    }

    @NonNull
    public static Timer createSingleTimer(long j) {
        return new SingleTimer(Threads.newUiHandler(), j);
    }
}
