package com.smaato.sdk.core.util.notifier;

import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.Timer.Listener;

public class StandardTimer implements Timer {
    @NonNull
    private final Handler a;
    @NonNull
    private final Consumer<Runnable> b;
    @Nullable
    private Runnable c;
    private final long d;

    StandardTimer(@NonNull Handler handler, long j) {
        this.a = (Handler) Objects.requireNonNull(handler);
        this.d = j;
        this.b = new Consumer(handler) {
            private final /* synthetic */ Handler f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                StandardTimer.this.a(this.f$1, (Runnable) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Handler handler, Runnable runnable) {
        handler.removeCallbacks(runnable);
        this.c = null;
    }

    public void start(@NonNull Listener listener) {
        Objects.onNotNull(this.c, this.b);
        this.c = new Runnable() {
            public final void run() {
                Objects.onNotNull(Listener.this, $$Lambda$pT498UUR2aZfhfxkzTbsb9200M.INSTANCE);
            }
        };
        this.a.postDelayed(this.c, this.d);
    }
}
