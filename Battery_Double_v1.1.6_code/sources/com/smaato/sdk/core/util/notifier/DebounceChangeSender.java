package com.smaato.sdk.core.util.notifier;

import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;

class DebounceChangeSender<D> extends StandardChangeSender<D> {
    @NonNull
    private final Handler b;
    private final long c;
    @Nullable
    private Runnable d;
    @NonNull
    private final Consumer<Runnable> e;

    DebounceChangeSender(@NonNull D d2, @NonNull Handler handler, long j) {
        super(d2);
        this.b = (Handler) Objects.requireNonNull(handler);
        this.c = j;
        this.e = new Consumer(handler) {
            private final /* synthetic */ Handler f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                DebounceChangeSender.this.a(this.f$1, (Runnable) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Handler handler, Runnable runnable) {
        handler.removeCallbacks(runnable);
        this.d = null;
    }

    public void newValue(@NonNull D d2) {
        synchronized (this.a) {
            Objects.onNotNull(this.d, this.e);
            this.d = new Runnable(d2) {
                private final /* synthetic */ Object f$1;

                {
                    this.f$1 = r2;
                }

                public final void run() {
                    DebounceChangeSender.this.a(this.f$1);
                }
            };
            this.b.postDelayed(this.d, this.c);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Object obj) {
        super.newValue(obj);
    }
}
