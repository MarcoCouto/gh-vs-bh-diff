package com.smaato.sdk.core.util.notifier;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.notifier.Timer.Listener;

public class SingleTimer extends StandardTimer {
    private volatile boolean a;

    SingleTimer(@NonNull Handler handler, long j) {
        super(handler, j);
    }

    public void start(@NonNull Listener listener) {
        if (this.a) {
            Objects.onNotNull(listener, $$Lambda$pT498UUR2aZfhfxkzTbsb9200M.INSTANCE);
        } else {
            super.start(new Listener(listener) {
                private final /* synthetic */ Listener f$1;

                {
                    this.f$1 = r2;
                }

                public final void onTimePassed() {
                    SingleTimer.this.a(this.f$1);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        this.a = true;
        Objects.onNotNull(listener, $$Lambda$pT498UUR2aZfhfxkzTbsb9200M.INSTANCE);
    }
}
