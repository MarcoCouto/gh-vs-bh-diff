package com.smaato.sdk.core.util.notifier;

import androidx.annotation.NonNull;

public interface Timer {

    public interface Listener {
        void onTimePassed();
    }

    void start(@NonNull Listener listener);
}
