package com.smaato.sdk.core.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Process;
import androidx.annotation.NonNull;

public final class AppMetaData {
    @NonNull
    private final Context a;

    public AppMetaData(@NonNull Context context) {
        this.a = (Context) Objects.requireNonNull(context, "Parameter context cannot be null for PermissionChecker::new");
    }

    public final boolean isPermissionGranted(@NonNull String str) {
        Objects.requireNonNull(str);
        try {
            if (this.a.checkPermission(str, Process.myPid(), Process.myUid()) == 0) {
                return true;
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public final boolean isActivityRegistered(@NonNull Class<? extends Activity> cls) {
        return Intents.canHandleIntent(this.a, new Intent(this.a, cls));
    }
}
