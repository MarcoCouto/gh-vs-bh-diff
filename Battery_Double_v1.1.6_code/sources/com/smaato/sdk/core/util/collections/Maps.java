package com.smaato.sdk.core.util.collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.BiConsumer;
import com.smaato.sdk.core.util.fi.BiFunction;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Predicate;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class Maps {
    private Maps() {
    }

    @NonNull
    public static <F, K, V> Map<K, V> toMap(@NonNull Iterable<F> iterable, @NonNull Function<F, K> function, @NonNull Function<F, V> function2) {
        HashMap hashMap = new HashMap();
        for (Object next : iterable) {
            hashMap.put(function.apply(next), function2.apply(next));
        }
        return hashMap;
    }

    @NonNull
    public static <F, K, V> Map<K, V> toMapWithOrder(@NonNull Iterable<F> iterable, @NonNull Function<F, K> function, @NonNull Function<F, V> function2) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object next : iterable) {
            linkedHashMap.put(function.apply(next), function2.apply(next));
        }
        return linkedHashMap;
    }

    @NonNull
    public static <K, V> List<K> filteredKeys(@NonNull Map<K, V> map, @NonNull Predicate<V> predicate) {
        ArrayList arrayList = new ArrayList();
        for (Entry entry : map.entrySet()) {
            if (predicate.test(entry.getValue())) {
                arrayList.add(entry.getKey());
            }
        }
        return arrayList;
    }

    @Nullable
    public static <K, V> K firstMatchedKey(@NonNull Map<K, V> map, @NonNull Predicate<V> predicate) {
        for (Entry entry : map.entrySet()) {
            if (predicate.test(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Nullable
    public static <K, V> Entry<K, V> firstMatchedEntry(@NonNull Map<K, V> map, @NonNull Predicate<V> predicate) {
        for (Entry<K, V> entry : map.entrySet()) {
            if (predicate.test(entry.getValue())) {
                return entry;
            }
        }
        return null;
    }

    @NonNull
    public static <K, V> Map<K, V> filter(@NonNull Map<K, V> map, @NonNull Predicate<V> predicate) {
        HashMap hashMap = new HashMap();
        for (Entry entry : map.entrySet()) {
            Object value = entry.getValue();
            if (predicate.test(value)) {
                hashMap.put(entry.getKey(), value);
            }
        }
        return hashMap;
    }

    @NonNull
    public static <K, V> Map<K, V> toImmutableMap(@Nullable Map<? extends K, ? extends V> map) {
        if (map == null || map.isEmpty()) {
            return Collections.emptyMap();
        }
        return Collections.unmodifiableMap(new HashMap(map));
    }

    public static <K, V> void forEach(@NonNull Map<K, V> map, @NonNull BiConsumer<? super K, ? super V> biConsumer) {
        Objects.requireNonNull(biConsumer);
        for (Entry entry : map.entrySet()) {
            try {
                biConsumer.accept(entry.getKey(), entry.getValue());
            } catch (IllegalStateException e) {
                throw new ConcurrentModificationException(e.getMessage());
            }
        }
    }

    @NonNull
    public static <K, V> Entry<K, V> entryOf(@NonNull K k, @NonNull V v) {
        return new SimpleImmutableEntry(k, v);
    }

    @NonNull
    public static <K, V, R> R reduce(@NonNull Map<K, V> map, @NonNull R r, @NonNull BiFunction<Entry<K, V>, R, R> biFunction) {
        Objects.requireNonNull(biFunction);
        for (Entry apply : map.entrySet()) {
            r = biFunction.apply(apply, r);
        }
        return r;
    }

    @NonNull
    @SafeVarargs
    public static <K, V> Map<K, V> merge(@NonNull Map<K, V>... mapArr) {
        HashMap hashMap = new HashMap();
        for (Map<K, V> putAll : mapArr) {
            hashMap.putAll(putAll);
        }
        return hashMap;
    }

    @NonNull
    @SafeVarargs
    public static <K, V> Map<K, V> mapOf(@NonNull Entry<K, V>... entryArr) {
        Objects.requireNonNull(entryArr, "entries is null");
        HashMap hashMap = new HashMap();
        for (int i = 0; i < entryArr.length; i++) {
            Entry<K, V> entry = entryArr[i];
            StringBuilder sb = new StringBuilder("entry at index ");
            sb.append(i);
            sb.append("is null");
            Objects.requireNonNull(entry, sb.toString());
            hashMap.put(entry.getKey(), entry.getValue());
        }
        return Collections.unmodifiableMap(hashMap);
    }
}
