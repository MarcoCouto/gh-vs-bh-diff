package com.smaato.sdk.core.util.collections;

import com.smaato.sdk.core.util.Objects;
import java.util.Iterator;

abstract class MappedIterator<F, T> implements Iterator<T> {
    final Iterator<? extends F> a;

    /* access modifiers changed from: 0000 */
    public abstract T a(F f);

    MappedIterator(Iterator<? extends F> it) {
        this.a = (Iterator) Objects.requireNonNull(it);
    }

    public final boolean hasNext() {
        return this.a.hasNext();
    }

    public final T next() {
        return a(this.a.next());
    }

    public final void remove() {
        this.a.remove();
    }
}
