package com.smaato.sdk.core.util.collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.util.fi.Predicate;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.AbstractSequentialList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public final class Lists {

    private static class MappingRandomAccessList<F, T> extends AbstractList<T> implements Serializable, RandomAccess {
        private static final long serialVersionUID = 0;
        @NonNull
        final NullableFunction<? super F, ? extends T> a;
        @NonNull
        private List<F> b;

        MappingRandomAccessList(@NonNull List<F> list, @NonNull NullableFunction<? super F, ? extends T> nullableFunction) {
            this.b = (List) Objects.requireNonNull(list);
            this.a = (NullableFunction) Objects.requireNonNull(nullableFunction);
        }

        public void clear() {
            this.b.clear();
        }

        public T get(int i) {
            return this.a.apply(this.b.get(i));
        }

        @NonNull
        public Iterator<T> iterator() {
            return listIterator();
        }

        @NonNull
        public ListIterator<T> listIterator(int i) {
            return new MappedListIterator<F, T>(this.b.listIterator(i)) {
                /* access modifiers changed from: 0000 */
                public final T a(F f) {
                    return MappingRandomAccessList.this.a.apply(f);
                }
            };
        }

        public boolean isEmpty() {
            return this.b.isEmpty();
        }

        public T remove(int i) {
            return this.a.apply(this.b.remove(i));
        }

        public int size() {
            return this.b.size();
        }
    }

    private static class MappingSequentialList<F, T> extends AbstractSequentialList<T> implements Serializable {
        private static final long serialVersionUID = 0;
        @NonNull
        final NullableFunction<? super F, ? extends T> a;
        @NonNull
        private List<F> b;

        MappingSequentialList(@NonNull List<F> list, @NonNull NullableFunction<? super F, ? extends T> nullableFunction) {
            this.b = (List) Objects.requireNonNull(list);
            this.a = (NullableFunction) Objects.requireNonNull(nullableFunction);
        }

        public void clear() {
            this.b.clear();
        }

        public int size() {
            return this.b.size();
        }

        @NonNull
        public ListIterator<T> listIterator(int i) {
            return new MappedListIterator<F, T>(this.b.listIterator(i)) {
                /* access modifiers changed from: 0000 */
                public final T a(F f) {
                    return MappingSequentialList.this.a.apply(f);
                }
            };
        }
    }

    private Lists() {
    }

    public static <T> boolean all(@NonNull Iterable<T> iterable, @NonNull Predicate<T> predicate) {
        for (T test : iterable) {
            if (!predicate.test(test)) {
                return false;
            }
        }
        return true;
    }

    public static <T> boolean any(@NonNull Iterable<T> iterable, @NonNull Predicate<T> predicate) {
        for (T test : iterable) {
            if (predicate.test(test)) {
                return true;
            }
        }
        return false;
    }

    @NonNull
    public static <F, T> List<T> mapLazy(@NonNull List<F> list, @NonNull NullableFunction<? super F, ? extends T> nullableFunction) {
        return list instanceof RandomAccess ? new MappingRandomAccessList(list, nullableFunction) : new MappingSequentialList(list, nullableFunction);
    }

    @NonNull
    public static <F, T> List<T> map(@NonNull List<F> list, @NonNull Function<? super F, ? extends T> function) {
        Objects.requireNonNull(list);
        Objects.requireNonNull(function);
        ArrayList arrayList = new ArrayList();
        for (Object next : list) {
            if (next != null) {
                arrayList.add(function.apply(next));
            }
        }
        return toImmutableList((Collection<T>) arrayList);
    }

    @NonNull
    @SafeVarargs
    public static <T> List<T> of(@NonNull T... tArr) {
        return Arrays.asList(tArr);
    }

    @NonNull
    @SafeVarargs
    public static <T> List<T> of(@NonNull Collection<T>... collectionArr) {
        ArrayList arrayList = new ArrayList();
        for (Collection<T> collection : collectionArr) {
            arrayList.getClass();
            Objects.onNotNull(collection, new Consumer(arrayList) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.addAll((Collection) obj);
                }
            });
        }
        return arrayList;
    }

    @Nullable
    public static <T> T filterFirst(@NonNull Iterable<T> iterable, @NonNull Predicate<T> predicate) {
        for (T next : iterable) {
            if (predicate.test(next)) {
                return next;
            }
        }
        return null;
    }

    public static <T> List<T> filter(@NonNull Iterable<T> iterable, @NonNull Predicate<T> predicate) {
        ArrayList arrayList = new ArrayList();
        for (Object next : iterable) {
            if (predicate.test(next)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @NonNull
    public static <T> List<T> toImmutableList(@Nullable Collection<T> collection) {
        if (collection == null || collection.isEmpty()) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(new ArrayList(collection));
    }

    @NonNull
    public static <T> List<T> toImmutableList(@NonNull Iterable<T> iterable) {
        ArrayList arrayList = new ArrayList();
        for (T add : iterable) {
            arrayList.add(add);
        }
        return toImmutableList((Collection<T>) arrayList);
    }

    @NonNull
    public static <T> List<T> toImmutableListOf(@NonNull T... tArr) {
        Objects.requireNonNull(tArr);
        return toImmutableList((Collection<T>) Arrays.asList(tArr));
    }

    @NonNull
    @SafeVarargs
    public static <T> List<T> join(@NonNull List<T>... listArr) {
        int i = 0;
        for (List<T> size : listArr) {
            i += size.size();
        }
        if (i == 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(i);
        for (List<T> addAll : listArr) {
            arrayList.addAll(addAll);
        }
        return toImmutableList((Collection<T>) arrayList);
    }
}
