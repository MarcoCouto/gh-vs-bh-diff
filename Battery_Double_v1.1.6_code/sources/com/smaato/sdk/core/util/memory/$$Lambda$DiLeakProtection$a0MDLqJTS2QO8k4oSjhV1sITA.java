package com.smaato.sdk.core.util.memory;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.util.memory.-$$Lambda$DiLeakProtection$a0MDLqJTS-2QO8k4-oSjhV1sITA reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiLeakProtection$a0MDLqJTS2QO8k4oSjhV1sITA implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiLeakProtection$a0MDLqJTS2QO8k4oSjhV1sITA INSTANCE = new $$Lambda$DiLeakProtection$a0MDLqJTS2QO8k4oSjhV1sITA();

    private /* synthetic */ $$Lambda$DiLeakProtection$a0MDLqJTS2QO8k4oSjhV1sITA() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiLeakProtection.a(diConstructor);
    }
}
