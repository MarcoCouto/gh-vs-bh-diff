package com.smaato.sdk.core.util;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdFormat;

public class SdkConfigHintBuilder {
    @NonNull
    public String buildSdkModuleMissedHintForAdFormat(@NonNull AdFormat adFormat) {
        String str;
        String str2;
        String str3 = "In order to show %s ads, add %s SOMA SDK module to your app build configuration";
        switch (adFormat) {
            case RICH_MEDIA:
                str = "Rich Media";
                str2 = "com.smaato.sdk.richmedia:module-richmedia";
                break;
            case VIDEO:
                str = "Video";
                str2 = "com.smaato.sdk.vast:module-video";
                break;
            default:
                return "";
        }
        return String.format(str3, new Object[]{str, str2});
    }
}
