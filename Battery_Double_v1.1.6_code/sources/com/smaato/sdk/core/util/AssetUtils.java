package com.smaato.sdk.core.util;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class AssetUtils {
    private AssetUtils() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r1.close();
     */
    @NonNull
    public static String getFileFromAssets(@NonNull Context context, @NonNull Logger logger, @NonNull String str) {
        BufferedReader bufferedReader;
        Objects.requireNonNull(context);
        Objects.requireNonNull(logger);
        Objects.requireNonNull(str);
        StringBuilder sb = new StringBuilder();
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(str), "UTF-8"));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append("\n");
            }
        } catch (IOException e) {
            logger.error(LogDomain.CORE, String.format("Could not read '%s' file from assets", new Object[]{str}), e);
        } catch (Throwable unused) {
        }
        return sb.toString();
        throw th;
    }
}
