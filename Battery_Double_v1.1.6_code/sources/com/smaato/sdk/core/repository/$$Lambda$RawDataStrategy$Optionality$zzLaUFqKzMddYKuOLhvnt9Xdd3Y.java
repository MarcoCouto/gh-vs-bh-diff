package com.smaato.sdk.core.repository;

import com.smaato.sdk.flow.Flow;

/* renamed from: com.smaato.sdk.core.repository.-$$Lambda$RawDataStrategy$Optionality$zzLaUFqKzMddYKuOLhvnt9Xdd3Y reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RawDataStrategy$Optionality$zzLaUFqKzMddYKuOLhvnt9Xdd3Y implements Type {
    public static final /* synthetic */ $$Lambda$RawDataStrategy$Optionality$zzLaUFqKzMddYKuOLhvnt9Xdd3Y INSTANCE = new $$Lambda$RawDataStrategy$Optionality$zzLaUFqKzMddYKuOLhvnt9Xdd3Y();

    private /* synthetic */ $$Lambda$RawDataStrategy$Optionality$zzLaUFqKzMddYKuOLhvnt9Xdd3Y() {
    }

    public final Object apply(Object obj) {
        return ((Flow) obj).flatMap($$Lambda$RawDataStrategy$Optionality$_zCuqCFHS3w4cH2fbkHfZ5hcI4Y.INSTANCE).switchIfEmpty($$Lambda$RawDataStrategy$Optionality$0tkIleBctEEn_0N6rEYr8LnV4.INSTANCE);
    }
}
