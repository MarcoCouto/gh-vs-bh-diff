package com.smaato.sdk.core.repository;

import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.flow.Flow;
import java.util.concurrent.Callable;

/* renamed from: com.smaato.sdk.core.repository.-$$Lambda$RawDataStrategy$Optionality$0tkIleBctEEn_0N6--rEYr8LnV4 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RawDataStrategy$Optionality$0tkIleBctEEn_0N6rEYr8LnV4 implements Callable {
    public static final /* synthetic */ $$Lambda$RawDataStrategy$Optionality$0tkIleBctEEn_0N6rEYr8LnV4 INSTANCE = new $$Lambda$RawDataStrategy$Optionality$0tkIleBctEEn_0N6rEYr8LnV4();

    private /* synthetic */ $$Lambda$RawDataStrategy$Optionality$0tkIleBctEEn_0N6rEYr8LnV4() {
    }

    public final Object call() {
        return Flow.error(new AdLoaderException(Error.NO_MANDATORY_CACHE, new Exception("Cached Ad Response is not found.")));
    }
}
