package com.smaato.sdk.core.repository;

import com.smaato.sdk.core.ad.AdResponseCacheEntry;
import com.smaato.sdk.core.util.fi.Function;

/* renamed from: com.smaato.sdk.core.repository.-$$Lambda$SJqi0etvAwssfF7IlT8ZO_Qz_QA reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$SJqi0etvAwssfF7IlT8ZO_Qz_QA implements Function {
    public static final /* synthetic */ $$Lambda$SJqi0etvAwssfF7IlT8ZO_Qz_QA INSTANCE = new $$Lambda$SJqi0etvAwssfF7IlT8ZO_Qz_QA();

    private /* synthetic */ $$Lambda$SJqi0etvAwssfF7IlT8ZO_Qz_QA() {
    }

    public final Object apply(Object obj) {
        return ((AdResponseCacheEntry) obj).getApiAdResponse();
    }
}
