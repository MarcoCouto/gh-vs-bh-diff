package com.smaato.sdk.core.repository;

import androidx.annotation.Nullable;

final class AutoValue_AdRequestParams extends AdRequestParams {
    private final String a;

    static final class Builder extends com.smaato.sdk.core.repository.AdRequestParams.Builder {
        private String a;

        Builder() {
        }

        public final com.smaato.sdk.core.repository.AdRequestParams.Builder setUBUniqueId(@Nullable String str) {
            this.a = str;
            return this;
        }

        public final AdRequestParams build() {
            return new AutoValue_AdRequestParams(this.a, 0);
        }
    }

    /* synthetic */ AutoValue_AdRequestParams(String str, byte b) {
        this(str);
    }

    private AutoValue_AdRequestParams(@Nullable String str) {
        this.a = str;
    }

    @Nullable
    public final String getUBUniqueId() {
        return this.a;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdRequestParams{UBUniqueId=");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AdRequestParams)) {
            return false;
        }
        AdRequestParams adRequestParams = (AdRequestParams) obj;
        if (this.a == null) {
            return adRequestParams.getUBUniqueId() == null;
        }
        return this.a.equals(adRequestParams.getUBUniqueId());
    }

    public final int hashCode() {
        return (this.a == null ? 0 : this.a.hashCode()) ^ 1000003;
    }
}
