package com.smaato.sdk.core.repository;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdRequest;
import com.smaato.sdk.flow.Flow;

public interface AdRepository {

    public interface Listener {
        void onAdLoadError(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdLoaderException adLoaderException);

        void onAdLoadSuccess(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdPresenter adPresenter);
    }

    @NonNull
    Flow<AdPresenter> loadAd(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdRequest adRequest);

    @MainThread
    void loadAd(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdRequest adRequest, @NonNull Listener listener);
}
