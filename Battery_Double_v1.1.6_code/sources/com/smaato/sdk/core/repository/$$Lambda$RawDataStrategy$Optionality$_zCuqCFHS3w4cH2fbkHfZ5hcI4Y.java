package com.smaato.sdk.core.repository;

import com.smaato.sdk.core.ad.AdResponseCacheEntry;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.flow.Flow;
import java.util.concurrent.Callable;

/* renamed from: com.smaato.sdk.core.repository.-$$Lambda$RawDataStrategy$Optionality$_zCuqCFHS3w4cH2fbkHfZ5hcI4Y reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RawDataStrategy$Optionality$_zCuqCFHS3w4cH2fbkHfZ5hcI4Y implements Function {
    public static final /* synthetic */ $$Lambda$RawDataStrategy$Optionality$_zCuqCFHS3w4cH2fbkHfZ5hcI4Y INSTANCE = new $$Lambda$RawDataStrategy$Optionality$_zCuqCFHS3w4cH2fbkHfZ5hcI4Y();

    private /* synthetic */ $$Lambda$RawDataStrategy$Optionality$_zCuqCFHS3w4cH2fbkHfZ5hcI4Y() {
    }

    public final Object apply(Object obj) {
        return Flow.single(new Callable((AdResponseCacheEntry) obj) {
            private final /* synthetic */ AdResponseCacheEntry f$0;

            public final 
/*
Method generation error in method: com.smaato.sdk.core.repository.-$$Lambda$RawDataStrategy$Optionality$pjCxu2rk2uwOcY2HBL17_Q6pSCk.call():null, dex: classes3.dex
            java.lang.NullPointerException
            	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
            	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
            	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
            	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
            	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
            	at jadx.core.codegen.InsnGen.addArgDot(InsnGen.java:88)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:682)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.inlineMethod(InsnGen.java:896)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:669)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.inlineMethod(InsnGen.java:896)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:669)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:303)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
            	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
            	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
            	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
            	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
            	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
            	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
            	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
            	at jadx.core.ProcessClass.process(ProcessClass.java:36)
            	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
            	at jadx.api.JavaClass.decompile(JavaClass.java:62)
            	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
            
*/
        }).map($$Lambda$SJqi0etvAwssfF7IlT8ZO_Qz_QA.INSTANCE);
    }
}
