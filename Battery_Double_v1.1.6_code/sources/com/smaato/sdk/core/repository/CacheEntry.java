package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine.Listener;
import java.util.concurrent.atomic.AtomicBoolean;

final class CacheEntry {
    @NonNull
    private final AdPresenter a;
    @NonNull
    private final AtomicBoolean b;
    @NonNull
    private final Listener<State> c = new Listener() {
        public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
            CacheEntry.this.a((State) obj, (State) obj2, metadata);
        }
    };

    CacheEntry(@NonNull AdPresenter adPresenter) {
        this.a = (AdPresenter) Objects.requireNonNull(adPresenter);
        this.b = new AtomicBoolean(!adPresenter.isValid());
        adPresenter.getAdInteractor().addStateListener(this.c);
    }

    public final boolean isReadyToBeRemoved() {
        return this.b.get();
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return !isReadyToBeRemoved() && !this.a.isInUse();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final AdPresenter b() {
        return this.a;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(State state, State state2, Metadata metadata) {
        switch (state2) {
            case IMPRESSED:
            case TO_BE_DELETED:
                this.a.getAdInteractor().removeStateListener(this.c);
                this.b.set(true);
                return;
            default:
                return;
        }
    }
}
