package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.ApiAdResponseCache;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.flow.Flow;
import io.fabric.sdk.android.services.events.EventsFilesManager;

public abstract class CoreAdTypeStrategy implements AdTypeStrategy {
    @NonNull
    private final RawDataStrategy a;
    @NonNull
    protected final String adSpaceId;
    @NonNull
    protected final String publisherId;

    /* access modifiers changed from: protected */
    @NonNull
    public abstract Iterable getParams();

    protected CoreAdTypeStrategy(@NonNull String str, @NonNull String str2, @NonNull RawDataStrategy rawDataStrategy) {
        this.a = (RawDataStrategy) Objects.requireNonNull(rawDataStrategy);
        this.publisherId = (String) Objects.requireNonNull(str);
        this.adSpaceId = (String) Objects.requireNonNull(str2);
    }

    @NonNull
    public String getUniqueKey() {
        return Joiner.join((CharSequence) EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, getParams());
    }

    @NonNull
    public Flow<ApiAdResponse> getRawDataStrategy(@NonNull ApiAdResponseCache apiAdResponseCache) {
        return (Flow) this.a.apply(apiAdResponseCache);
    }
}
