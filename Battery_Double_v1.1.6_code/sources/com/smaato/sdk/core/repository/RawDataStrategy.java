package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdResponseCacheEntry;
import com.smaato.sdk.core.ad.ApiAdResponseCache;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.repository.RawDataStrategy.Builder;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Pair;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.flow.Flow;
import java.util.concurrent.Callable;

public interface RawDataStrategy extends Function<ApiAdResponseCache, Flow<ApiAdResponse>> {

    /* renamed from: com.smaato.sdk.core.repository.RawDataStrategy$-CC reason: invalid class name */
    public final /* synthetic */ class CC {
        @NonNull
        public static <T> Builder<T> newBuilder(@NonNull Callable<T> callable) {
            Builder<T> builder = new Builder<>();
            builder.c = callable;
            return builder;
        }
    }

    public static final class Builder<T> {
        @Nullable
        private Type<T> a;
        @Nullable
        private Type b;
        /* access modifiers changed from: private */
        @Nullable
        public Callable<T> c;

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder<T> a(@Nullable Type<T> type) {
            this.a = type;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final Builder<T> a(@Nullable Type type) {
            this.b = type;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public final RawDataStrategy a() {
            this.c = (Callable) Objects.requireNonNull(this.c);
            this.b = (Type) Objects.requireNonNull(this.b);
            this.a = (Type) Objects.requireNonNull(this.a);
            return new RawDataStrategy() {
                public final Object apply(Object obj) {
                    return Builder.this.a((ApiAdResponseCache) obj);
                }
            };
        }

        /* access modifiers changed from: private */
        public /* synthetic */ Flow a(ApiAdResponseCache apiAdResponseCache) {
            return (Flow) this.b.apply(((Function) this.a.apply(this.c)).apply(apiAdResponseCache));
        }
    }

    public static final class Optionality {

        interface Type extends Function<Flow<AdResponseCacheEntry>, Flow<ApiAdResponse>> {
        }

        /* access modifiers changed from: private */
        public static /* synthetic */ AdResponseCacheEntry b(AdResponseCacheEntry adResponseCacheEntry) throws Exception {
            return adResponseCacheEntry;
        }

        /* access modifiers changed from: private */
        public static /* synthetic */ AdResponseCacheEntry d(AdResponseCacheEntry adResponseCacheEntry) throws Exception {
            return adResponseCacheEntry;
        }

        private Optionality() {
        }

        @NonNull
        static Type a() {
            return $$Lambda$RawDataStrategy$Optionality$zzLaUFqKzMddYKuOLhvnt9Xdd3Y.INSTANCE;
        }

        @NonNull
        static Type b() {
            return $$Lambda$RawDataStrategy$Optionality$YCeXxxFwxDISW6eu6WFGpCt2bXI.INSTANCE;
        }
    }

    public static final class Providing {

        interface Type<T> extends Function<Callable<T>, Function<ApiAdResponseCache, Flow<AdResponseCacheEntry>>> {
        }

        private Providing() {
        }

        static Type<Pair<String, String>> a() {
            return $$Lambda$RawDataStrategy$Providing$Z0SQOyB5_S4HOCHp0XqbnfBHwR4.INSTANCE;
        }

        /* access modifiers changed from: private */
        public static /* synthetic */ Function b(Callable callable) {
            return new Function(callable) {
                private final /* synthetic */ Callable f$0;

                {
                    this.f$0 = r1;
                }

                public final Object apply(Object obj) {
                    return Flow.single(this.f$0).flatMap(new Function() {
                        public final Object apply(Object obj) {
                            return Flow.maybe(new Callable(ApiAdResponseCache.this) {
                                private final /* synthetic */ ApiAdResponseCache f$1;

                                {
                                    this.f$1 = r2;
                                }

                                public final Object call() {
                                    return this.f$1.getById((String) Objects.requireNonNull(Pair.this.first), (String) Objects.requireNonNull(Pair.this.second));
                                }
                            });
                        }
                    });
                }
            };
        }

        @NonNull
        static Type<String> b() {
            return $$Lambda$RawDataStrategy$Providing$KdCAjTa0mHtRRxecEw40ak4fg1k.INSTANCE;
        }

        /* access modifiers changed from: private */
        public static /* synthetic */ Function a(Callable callable) {
            return new Function(callable) {
                private final /* synthetic */ Callable f$0;

                {
                    this.f$0 = r1;
                }

                public final Object apply(Object obj) {
                    return Flow.single(this.f$0).flatMap(new Function() {
                        public final Object apply(Object obj) {
                            return Flow.maybe(new Callable((String) obj) {
                                private final /* synthetic */ String f$1;

                                {
                                    this.f$1 = r2;
                                }

                                public final Object call() {
                                    return ApiAdResponseCache.this.get(this.f$1);
                                }
                            });
                        }
                    });
                }
            };
        }
    }
}
