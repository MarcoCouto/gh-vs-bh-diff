package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.ApiAdResponseCache;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.flow.Flow;

public interface AdTypeStrategy {
    @NonNull
    Class<? extends AdPresenter> getAdPresenterClass();

    @NonNull
    Flow<ApiAdResponse> getRawDataStrategy(@NonNull ApiAdResponseCache apiAdResponseCache);

    @NonNull
    String getUniqueKey();
}
