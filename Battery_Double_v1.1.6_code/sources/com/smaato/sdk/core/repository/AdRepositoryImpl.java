package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoader;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.ad.AdLoader.Listener;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterBuilderException;
import com.smaato.sdk.core.ad.AdRequest;
import com.smaato.sdk.core.ad.AdResponseCacheEntry;
import com.smaato.sdk.core.ad.ApiAdResponseCache;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.flow.Flow;
import com.smaato.sdk.flow.Flow.Emitter;
import com.smaato.sdk.flow.Flow.Executors;
import com.smaato.sdk.flow.Publisher;
import java.util.concurrent.Callable;

final class AdRepositoryImpl implements AdRepository {
    @NonNull
    private final Logger a;
    @NonNull
    private final ApiAdResponseCache b;
    @NonNull
    private final AdPresenterCache c;
    @NonNull
    private final AdLoadersRegistry d;
    @NonNull
    private final Executors e;
    @NonNull
    private Supplier<AdLoader> f;

    private static class AdLoaderListener implements Listener {
        @NonNull
        private final Emitter<? super AdPresenter> a;
        @NonNull
        private final AdLoadersRegistry b;
        @NonNull
        private final AdTypeStrategy c;

        AdLoaderListener(@NonNull Emitter<? super AdPresenter> emitter, @NonNull AdLoadersRegistry adLoadersRegistry, @NonNull AdTypeStrategy adTypeStrategy) {
            this.a = emitter;
            this.b = adLoadersRegistry;
            this.c = adTypeStrategy;
        }

        public void onAdLoadSuccess(@NonNull AdLoader adLoader, @NonNull AdPresenter adPresenter) {
            this.b.unregister(this.c.getUniqueKey(), adLoader);
            this.a.onNext(adPresenter);
            this.a.onComplete();
        }

        public void onAdLoadError(@NonNull AdLoader adLoader, @NonNull AdLoaderException adLoaderException) {
            this.b.unregister(this.c.getUniqueKey(), adLoader);
            this.a.onError(adLoaderException);
        }
    }

    private static class AdPresenterBuilderListener implements AdPresenterBuilder.Listener {
        @NonNull
        private final Emitter<? super AdPresenter> a;

        AdPresenterBuilderListener(@NonNull Emitter<? super AdPresenter> emitter) {
            this.a = emitter;
        }

        public void onAdPresenterBuildSuccess(@NonNull AdPresenterBuilder adPresenterBuilder, @NonNull AdPresenter adPresenter) {
            this.a.onNext(adPresenter);
            this.a.onComplete();
        }

        public void onAdPresenterBuildError(@NonNull AdPresenterBuilder adPresenterBuilder, @NonNull AdPresenterBuilderException adPresenterBuilderException) {
            this.a.onError(adPresenterBuilderException);
        }
    }

    private static final class TooManyRequestsException extends RuntimeException {
        /* synthetic */ TooManyRequestsException(byte b) {
            this();
        }

        private TooManyRequestsException() {
            super("Cache is full. Please use the one of previously loaded ADs.");
        }
    }

    AdRepositoryImpl(@NonNull Logger logger, @NonNull ApiAdResponseCache apiAdResponseCache, @NonNull AdPresenterCache adPresenterCache, @NonNull AdLoadersRegistry adLoadersRegistry, @NonNull Supplier<AdLoader> supplier, @NonNull Executors executors) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = apiAdResponseCache;
        this.c = (AdPresenterCache) Objects.requireNonNull(adPresenterCache);
        this.d = (AdLoadersRegistry) Objects.requireNonNull(adLoadersRegistry);
        this.f = (Supplier) Objects.requireNonNull(supplier);
        this.e = (Executors) Objects.requireNonNull(executors);
    }

    public final void loadAd(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdRequest adRequest, @NonNull AdRepository.Listener listener) {
        Objects.requireNonNull(listener);
        loadAd(adTypeStrategy, adRequest).subscribe(new Consumer(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepository.Listener.this.onAdLoadSuccess(this.f$1, (AdPresenter) obj);
            }
        }, new Consumer(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepository.Listener.this.onAdLoadError(this.f$1, (AdLoaderException) ((Throwable) obj));
            }
        });
    }

    @NonNull
    public final Flow<AdPresenter> loadAd(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdRequest adRequest) {
        Objects.requireNonNull(adRequest);
        Objects.requireNonNull(adTypeStrategy);
        return Flow.create(new Consumer(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.b(this.f$1, this.f$2, (Emitter) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Emitter emitter, AdPresenter adPresenter) {
        emitter.onNext(adPresenter);
        emitter.onComplete();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Emitter emitter, Throwable th) {
        if (th instanceof AdLoaderException) {
            emitter.onError(th);
        } else if (th instanceof TooManyRequestsException) {
            this.a.error(LogDomain.CORE, th.getMessage(), th);
            emitter.onError(new AdLoaderException(Error.TOO_MANY_REQUESTS, (TooManyRequestsException) th));
        } else {
            this.a.error(LogDomain.CORE, th.getMessage(), th);
            emitter.onComplete();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher b(AdTypeStrategy adTypeStrategy, AdRequest adRequest, AdPresenter adPresenter) {
        return a(adTypeStrategy);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Flow b(AdTypeStrategy adTypeStrategy, AdRequest adRequest) throws Exception {
        return a(adTypeStrategy);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, AdPresenter adPresenter) {
        return a(adTypeStrategy);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, Emitter emitter) {
        String uniqueKey = adTypeStrategy.getUniqueKey();
        if (this.d.remainingCapacity(uniqueKey) <= 0) {
            this.a.info(LogDomain.CORE, "Ad loading request for provided publisherId and adSpaceId was already in progress", new Object[0]);
            return;
        }
        AdLoader adLoader = (AdLoader) this.f.get();
        adLoader.setListener(new AdLoaderListener(emitter, this.d, adTypeStrategy));
        this.d.register(uniqueKey, adLoader);
        adLoader.requestAd(adRequest, adTypeStrategy);
    }

    @NonNull
    private Flow<AdPresenter> a(@NonNull AdTypeStrategy adTypeStrategy) {
        return Flow.create(new Consumer(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.a(this.f$1, (Emitter) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdTypeStrategy adTypeStrategy, Emitter emitter) {
        String uniqueKey = adTypeStrategy.getUniqueKey();
        AdPresenter adPresenter = this.c.get(uniqueKey);
        if (adPresenter != null && adPresenter.isValid() && adPresenter.retainAccess()) {
            emitter.onNext(adPresenter);
        } else if (this.c.remainingCapacity(uniqueKey) <= 0) {
            emitter.onError(new TooManyRequestsException(0));
            return;
        }
        emitter.onComplete();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, ApiAdResponse apiAdResponse, Emitter emitter) {
        ((AdLoader) this.f.get()).buildAdPresenter(adTypeStrategy, adRequest, apiAdResponse, new AdPresenterBuilderListener(emitter));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void c(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdPresenter adPresenter) {
        if (!this.c.put(adTypeStrategy.getUniqueKey(), adPresenter)) {
            throw new TooManyRequestsException(0);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Flow a(AdTypeStrategy adTypeStrategy, AdRequest adRequest) throws Exception {
        return Flow.create(new Consumer(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.a(this.f$1, this.f$2, (Emitter) obj);
            }
        }).doOnNext(new Consumer(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.b(this.f$1, (AdPresenter) obj);
            }
        }).flatMap(new Function(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object apply(Object obj) {
                return AdRepositoryImpl.this.a(this.f$1, this.f$2, (AdPresenter) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, ApiAdResponse apiAdResponse) {
        return Flow.create(new Consumer(adTypeStrategy, adRequest, apiAdResponse) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;
            private final /* synthetic */ ApiAdResponse f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.a(this.f$1, this.f$2, this.f$3, (Emitter) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdTypeStrategy adTypeStrategy, AdRequest adRequest, String str) {
        for (AdResponseCacheEntry adResponseCacheEntry : this.b.trim(adTypeStrategy.getUniqueKey())) {
            ((AdLoader) this.f.get()).reportCacheEntryExpired(adRequest, adResponseCacheEntry.apiAdResponse, adResponseCacheEntry.getRequestTimestamp());
        }
        this.c.trim(str);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(AdTypeStrategy adTypeStrategy, AdRequest adRequest, Emitter emitter) {
        Flow.fromAction(new Runnable(adTypeStrategy, adRequest, adTypeStrategy.getUniqueKey()) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;
            private final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void run() {
                AdRepositoryImpl.this.a(this.f$1, this.f$2, this.f$3);
            }
        }).concatWith(adTypeStrategy.getRawDataStrategy(this.b).flatMap(new Function(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object apply(Object obj) {
                return AdRepositoryImpl.this.a(this.f$1, this.f$2, (ApiAdResponse) obj);
            }
        }).doOnNext(new Consumer(adTypeStrategy) {
            private final /* synthetic */ AdTypeStrategy f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.c(this.f$1, (AdPresenter) obj);
            }
        }).flatMap(new Function(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object apply(Object obj) {
                return AdRepositoryImpl.this.b(this.f$1, this.f$2, (AdPresenter) obj);
            }
        })).switchIfEmpty(new Callable(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object call() {
                return AdRepositoryImpl.this.b(this.f$1, this.f$2);
            }
        }).switchIfEmpty(new Callable(adTypeStrategy, adRequest) {
            private final /* synthetic */ AdTypeStrategy f$1;
            private final /* synthetic */ AdRequest f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object call() {
                return AdRepositoryImpl.this.a(this.f$1, this.f$2);
            }
        }).subscribeOn(this.e.io()).observeOn(this.e.main()).subscribe(new Consumer() {
            public final void accept(Object obj) {
                AdRepositoryImpl.a(Emitter.this, (AdPresenter) obj);
            }
        }, new Consumer(emitter) {
            private final /* synthetic */ Emitter f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                AdRepositoryImpl.this.a(this.f$1, (Throwable) obj);
            }
        });
    }
}
