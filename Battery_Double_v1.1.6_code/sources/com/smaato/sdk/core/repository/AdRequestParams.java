package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class AdRequestParams {

    @com.google.auto.value.AutoValue.Builder
    public static abstract class Builder {
        @NonNull
        public abstract AdRequestParams build();

        @NonNull
        public abstract Builder setUBUniqueId(@Nullable String str);
    }

    @Nullable
    public abstract String getUBUniqueId();

    @NonNull
    public static Builder builder() {
        return new Builder();
    }

    @NonNull
    public Builder newBuilder() {
        return builder().setUBUniqueId(getUBUniqueId());
    }
}
