package com.smaato.sdk.core.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.repository.RawDataStrategy.CC;
import com.smaato.sdk.core.repository.RawDataStrategy.Optionality;
import com.smaato.sdk.core.repository.RawDataStrategy.Providing;
import com.smaato.sdk.core.util.Pair;
import com.smaato.sdk.core.util.TextUtils;
import java.util.concurrent.Callable;

public class RawDataStrategyFactory {
    /* access modifiers changed from: private */
    public static /* synthetic */ String a(String str) throws Exception {
        return str;
    }

    @NonNull
    public RawDataStrategy create(@NonNull String str, @Nullable AdRequestParams adRequestParams) {
        if (adRequestParams == null || TextUtils.isEmpty(adRequestParams.getUBUniqueId())) {
            return CC.newBuilder(new Callable(str) {
                private final /* synthetic */ String f$0;

                {
                    this.f$0 = r1;
                }

                public final Object call() {
                    return RawDataStrategyFactory.a(this.f$0);
                }
            }).a(Optionality.b()).a(Providing.b()).a();
        }
        return CC.newBuilder(new Callable(str, adRequestParams.getUBUniqueId()) {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final Object call() {
                return Pair.of(this.f$0, this.f$1);
            }
        }).a(Optionality.a()).a(Providing.a()).a();
    }
}
