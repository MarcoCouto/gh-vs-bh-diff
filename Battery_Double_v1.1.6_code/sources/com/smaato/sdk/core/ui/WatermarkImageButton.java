package com.smaato.sdk.core.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.smaato.sdk.core.R;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.util.Intents;

public final class WatermarkImageButton extends ImageButton {
    public WatermarkImageButton(@NonNull Context context) {
        super(context);
        a();
    }

    public WatermarkImageButton(@NonNull Context context, @NonNull AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public WatermarkImageButton(@NonNull Context context, @NonNull AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    @RequiresApi(api = 21)
    public WatermarkImageButton(@NonNull Context context, @NonNull AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a();
    }

    private void a() {
        if (!SmaatoSdk.isWatermarkEnabled()) {
            setVisibility(8);
            return;
        }
        setImageDrawable(getResources().getDrawable(R.drawable.smaato_sdk_core_watermark));
        LayoutParams layoutParams = new LayoutParams(-2, -2, 8388661);
        setPadding(0, 0, 0, 0);
        setLayoutParams(layoutParams);
        setBackgroundColor(getResources().getColor(17170445));
        setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                WatermarkImageButton.this.a(view);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(View view) {
        Intents.startIntent(getContext(), Intents.createViewIntent("https://www.smaato.com/privacy/"));
    }
}
