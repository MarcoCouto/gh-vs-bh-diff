package com.smaato.sdk.core.ui;

import android.view.View;
import android.view.View.OnClickListener;

public abstract class DoubleClickPreventionListener implements OnClickListener {
    private long a;

    /* access modifiers changed from: protected */
    public void processClick() {
    }

    public final void onClick(View view) {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.a >= 1000) {
            this.a = currentTimeMillis;
            processClick();
        }
    }
}
