package com.smaato.sdk.core.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.R;
import com.smaato.sdk.core.ui.DoubleClickPreventionListener;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.diinjection.Inject;
import com.smaato.sdk.core.util.fi.Consumer;

public class SmaatoSdkBrowserActivity extends Activity implements BrowserView {
    private static final String a = "com.smaato.sdk.core.browser.SmaatoSdkBrowserActivity";
    @Nullable
    private WebView b;
    @Nullable
    private TextView c;
    @Nullable
    private ProgressBar d;
    @Nullable
    private View e;
    @Nullable
    private View f;
    /* access modifiers changed from: private */
    @Inject
    @Nullable
    public BrowserPresenter g;

    public static Intent createIntent(@NonNull Context context, @NonNull String str) {
        Objects.requireNonNull(context, "Parameter context cannot be null for SmaatoSdkBrowserActivity::createIntent");
        Objects.requireNonNull(str, "Parameter url cannot be null for SmaatoSdkBrowserActivity::createIntent");
        Intent intent = new Intent(context, SmaatoSdkBrowserActivity.class);
        intent.addFlags(536870912);
        intent.putExtra("KEY_CTA_URL", str);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        AndroidsInjector.inject((Activity) this);
        if (this.g == null) {
            Log.e(a, "SmaatoSdk is not initialized.");
            finish();
            return;
        }
        setContentView(R.layout.smaato_sdk_core_activity_internal_browser);
        this.b = (WebView) findViewById(R.id.webView);
        this.d = (ProgressBar) findViewById(R.id.progressBar);
        findViewById(R.id.btnClose).setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                SmaatoSdkBrowserActivity.this.finish();
            }
        });
        findViewById(R.id.btnRefresh).setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                Objects.onNotNull(SmaatoSdkBrowserActivity.this.g, $$Lambda$OycmKNItiN79aTZc0jQAv7_zk.INSTANCE);
            }
        });
        this.e = findViewById(R.id.btnBackward);
        this.e.setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                Objects.onNotNull(SmaatoSdkBrowserActivity.this.g, $$Lambda$zg6nko2q9HAOFDuGvnWKuuxwxIc.INSTANCE);
            }
        });
        this.f = findViewById(R.id.btnForward);
        this.f.setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                Objects.onNotNull(SmaatoSdkBrowserActivity.this.g, $$Lambda$3Who52wYNLeQhFdpTsxGL2qI3mk.INSTANCE);
            }
        });
        findViewById(R.id.btnOpenExternal).setOnClickListener(new DoubleClickPreventionListener() {
            /* access modifiers changed from: protected */
            public void processClick() {
                Objects.onNotNull(SmaatoSdkBrowserActivity.this.g, $$Lambda$SZooymytBc3co7R0LbcHuAKC8E.INSTANCE);
            }
        });
        this.c = (TextView) findViewById(R.id.tvHostname);
        this.c.setOnLongClickListener(new OnLongClickListener() {
            public final boolean onLongClick(View view) {
                return SmaatoSdkBrowserActivity.this.a(view);
            }
        });
        if (this.b != null) {
            WebSettings settings = this.b.getSettings();
            settings.setUseWideViewPort(true);
            settings.setSupportZoom(true);
            settings.setBuiltInZoomControls(true);
            settings.setDisplayZoomControls(false);
        }
        if (this.b != null) {
            Objects.onNotNull(this.g, new Consumer() {
                public final void accept(Object obj) {
                    SmaatoSdkBrowserActivity.this.a((BrowserPresenter) obj);
                }
            });
        }
        Objects.onNotNull(this.g, new Consumer(getIntent().getStringExtra("KEY_CTA_URL")) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((BrowserPresenter) obj).loadUrl(this.f$0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Objects.onNotNull(this.g, $$Lambda$_xH7XsVTZB0hRcN5JSoYlEDrFv4.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Objects.onNotNull(this.g, $$Lambda$dWdInfO6XN4ddHAUECmtK31crFc.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Objects.onNotNull(this.g, $$Lambda$JO76bjlBJ5VXWw5msPJKAupQnvM.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Objects.onNotNull(this.g, $$Lambda$mhUkOmav4YNRhITdGxUXfCL8MfA.INSTANCE);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Objects.onNotNull(this.b, $$Lambda$MnnyuaHVDkjXez9DGXvzyGZK4c.INSTANCE);
        Objects.onNotNull(this.g, $$Lambda$R86vL1LYd4bfRfllP6LfOcvV7k.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(View view) {
        Objects.onNotNull(this.g, $$Lambda$3kQ6XBVCKF9lr3VvE5pBizjEdg.INSTANCE);
        return true;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(BrowserPresenter browserPresenter) {
        browserPresenter.initWithView(this, this.b);
    }

    public void showHostname(@Nullable String str) {
        Objects.onNotNull(this.c, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((TextView) obj).setText(this.f$0);
            }
        });
    }

    public void showConnectionSecure(boolean z) {
        Objects.onNotNull(this.c, new Consumer(z ? R.drawable.smaato_sdk_core_ic_browser_secure_connection : 0) {
            private final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((TextView) obj).setCompoundDrawablesWithIntrinsicBounds(this.f$0, 0, 0, 0);
            }
        });
    }

    public void setPageNavigationBackEnabled(boolean z) {
        Objects.onNotNull(this.e, new Consumer(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((View) obj).setEnabled(this.f$0);
            }
        });
    }

    public void setPageNavigationForwardEnabled(boolean z) {
        Objects.onNotNull(this.f, new Consumer(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((View) obj).setEnabled(this.f$0);
            }
        });
    }

    public void launchExternalBrowser(@NonNull Intent intent) {
        startActivity(intent);
        finish();
    }

    public void redirectToExternalApp(@NonNull Intent intent) {
        startActivity(intent);
    }

    public void showProgressIndicator() {
        Objects.onNotNull(this.d, $$Lambda$SmaatoSdkBrowserActivity$tnQeeNMgu8UWJYVB_w4SCi88nM.INSTANCE);
    }

    public void hideProgressIndicator() {
        Objects.onNotNull(this.d, $$Lambda$SmaatoSdkBrowserActivity$ZOwLWgBcLrpst4RHIOl4hbrq5XE.INSTANCE);
    }

    public void updateProgressIndicator(int i) {
        Objects.onNotNull(this.d, new Consumer(i) {
            private final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((ProgressBar) obj).setProgress(this.f$0);
            }
        });
    }

    public void closeBrowser() {
        finish();
    }
}
