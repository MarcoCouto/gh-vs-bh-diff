package com.smaato.sdk.core.browser;

import android.app.Application;
import android.os.Build.VERSION;
import android.webkit.CookieSyncManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class CookieSyncManagerHolder {
    @Nullable
    private final CookieSyncManager a;

    public CookieSyncManagerHolder(@NonNull Application application) {
        if (VERSION.SDK_INT < 21) {
            this.a = CookieSyncManager.createInstance(application);
        } else {
            this.a = null;
        }
    }

    @Nullable
    public final CookieSyncManager getCookieSyncManager() {
        return this.a;
    }
}
