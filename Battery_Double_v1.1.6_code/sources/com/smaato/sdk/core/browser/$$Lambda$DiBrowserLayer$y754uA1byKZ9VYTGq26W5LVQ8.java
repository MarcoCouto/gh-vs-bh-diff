package com.smaato.sdk.core.browser;

import android.webkit.CookieManager;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.browser.-$$Lambda$DiBrowserLayer$y754uA1b-yKZ9VY-TGq26W5LVQ8 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiBrowserLayer$y754uA1byKZ9VYTGq26W5LVQ8 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiBrowserLayer$y754uA1byKZ9VYTGq26W5LVQ8 INSTANCE = new $$Lambda$DiBrowserLayer$y754uA1byKZ9VYTGq26W5LVQ8();

    private /* synthetic */ $$Lambda$DiBrowserLayer$y754uA1byKZ9VYTGq26W5LVQ8() {
    }

    public final Object get(DiConstructor diConstructor) {
        return CookieManager.getInstance();
    }
}
