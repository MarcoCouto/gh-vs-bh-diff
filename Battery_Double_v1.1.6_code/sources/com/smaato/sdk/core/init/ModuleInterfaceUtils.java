package com.smaato.sdk.core.init;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.configcheck.CoreManifestEntries;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.ModuleInterface;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public final class ModuleInterfaceUtils {
    private ModuleInterfaceUtils() {
    }

    @NonNull
    public static List<ModuleInterface> getValidInitialisedModuleInterfaces(@NonNull ClassLoader classLoader, @NonNull Iterable<ModuleInterface> iterable) {
        ArrayList arrayList = new ArrayList();
        for (ModuleInterface moduleInterface : iterable) {
            if (moduleInterface.version().equals(SmaatoSdk.getVersion())) {
                moduleInterface.init(classLoader);
                arrayList.add(moduleInterface);
            }
        }
        return arrayList;
    }

    @NonNull
    public static ExpectedManifestEntries getExpectedManifestEntriesFromModules(@NonNull List<ModuleInterface> list) {
        HashSet hashSet = new HashSet(CoreManifestEntries.PERMISSIONS_MANDATORY);
        HashSet hashSet2 = new HashSet(CoreManifestEntries.ACTIVITIES);
        for (ModuleInterface expectedManifestEntries : list) {
            ExpectedManifestEntries expectedManifestEntries2 = expectedManifestEntries.getExpectedManifestEntries();
            hashSet.addAll(expectedManifestEntries2.getPermissionsMandatory());
            hashSet2.addAll(expectedManifestEntries2.getActivities());
        }
        return new ExpectedManifestEntries(hashSet, hashSet2);
    }

    @NonNull
    public static List<DiRegistry> getDiOfModules(@NonNull List<ModuleInterface> list) {
        return Lists.mapLazy(list, $$Lambda$ModuleInterfaceUtils$ahv6DkAcMMmO4HYiAynUfpprvc.INSTANCE);
    }

    /* access modifiers changed from: private */
    @NonNull
    public static DiRegistry a(@NonNull ModuleInterface moduleInterface) {
        return DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                ((DiRegistry) obj).registerFactory(ModuleInterface.this.moduleDiName(), AdLoaderPlugin.class, (ClassFactory) Objects.requireNonNull(ModuleInterface.this.getAdLoaderPluginFactory()));
            }
        }).addFrom(moduleInterface.moduleDiRegistry());
    }
}
