package com.smaato.sdk.core;

import android.app.Activity;
import android.app.Fragment;
import android.view.View;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.diinjection.Inject;
import com.smaato.sdk.core.util.diinjection.Named;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public final class AndroidsInjector {
    private AndroidsInjector() {
    }

    public static void inject(@NonNull View view) {
        Objects.requireNonNull(view, "Parameter view cannot be null for AndroidsInjector::inject");
        a(view);
    }

    public static void inject(@NonNull Activity activity) {
        Objects.requireNonNull(activity, "Parameter activity cannot be null for AndroidsInjector::inject");
        a(activity);
    }

    public static void inject(@NonNull Fragment fragment) {
        Objects.requireNonNull(fragment, "Parameter fragment cannot be null for AndroidsInjector::inject");
        a(fragment);
    }

    public static void injectStatic(@NonNull Class cls) {
        Objects.requireNonNull(cls, "Parameter clazz cannot be null for AndroidsInjector::inject");
        a((Object) cls, a(cls, Inject.class));
    }

    private static void a(@NonNull Object obj) {
        a(obj, a(obj.getClass(), Inject.class));
    }

    private static void a(@NonNull Object obj, @NonNull Set<Field> set) {
        for (Field field : set) {
            String value = field.isAnnotationPresent(Named.class) ? ((Named) field.getAnnotation(Named.class)).value() : null;
            field.setAccessible(true);
            try {
                Field[] declaredFields = SmaatoSdk.class.getDeclaredFields();
                int length = declaredFields.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    Field field2 = declaredFields[i];
                    if (field2.getType() == SmaatoInstance.class) {
                        field2.setAccessible(true);
                        field.set(obj, ((SmaatoInstance) field2.get(null)).a().get(value, field.getType()));
                        break;
                    }
                    i++;
                }
            } catch (Exception unused) {
            }
        }
    }

    @NonNull
    private static Set<Field> a(@NonNull Class<?> cls, @NonNull Class<? extends Annotation> cls2) {
        Field[] declaredFields;
        HashSet hashSet = new HashSet();
        String canonicalName = cls.getCanonicalName();
        while (cls != null && canonicalName != null && canonicalName.startsWith("com.smaato")) {
            for (Field field : cls.getDeclaredFields()) {
                if (field.isAnnotationPresent(cls2)) {
                    hashSet.add(field);
                }
            }
            cls = cls.getSuperclass();
            canonicalName = cls.getCanonicalName();
        }
        return hashSet;
    }
}
