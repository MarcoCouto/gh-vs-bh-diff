package com.smaato.sdk.core.configcheck;

import android.app.Activity;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.browser.SmaatoSdkBrowserActivity;
import com.smaato.sdk.core.util.collections.Lists;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class CoreManifestEntries {
    @NonNull
    public static final Set<Class<? extends Activity>> ACTIVITIES = Collections.unmodifiableSet(new HashSet(Lists.of((T[]) new Class[]{SmaatoSdkBrowserActivity.class})));
    @NonNull
    public static final Set<String> PERMISSIONS_MANDATORY = Collections.unmodifiableSet(new HashSet(Lists.of((T[]) new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"})));

    private CoreManifestEntries() {
    }
}
