package com.smaato.sdk.core.configcheck;

public final class AppConfigCheckResult {
    private final boolean a;
    private final boolean b;

    AppConfigCheckResult(boolean z, boolean z2) {
        this.a = z;
        this.b = z2;
    }

    public final boolean isAppConfiguredProperly() {
        return this.a && this.b;
    }
}
