package com.smaato.sdk.core.configcheck;

import android.app.Activity;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import java.util.Collections;
import java.util.Set;

public class ExpectedManifestEntries {
    public static final ExpectedManifestEntries EMPTY = new ExpectedManifestEntries(Collections.emptySet(), Collections.emptySet());
    @NonNull
    private final Set<String> a;
    @NonNull
    private final Set<Class<? extends Activity>> b;

    public ExpectedManifestEntries(@NonNull Set<String> set, @NonNull Set<Class<? extends Activity>> set2) {
        Objects.requireNonNull(set, "Parameter permissionsMandatory cannot be null for ExpectedManifestEntries::new");
        this.a = Collections.unmodifiableSet(set);
        Objects.requireNonNull(set2, "Parameter activities cannot be null for ExpectedManifestEntries::new");
        this.b = Collections.unmodifiableSet(set2);
    }

    @NonNull
    public Set<String> getPermissionsMandatory() {
        return this.a;
    }

    @NonNull
    public Set<Class<? extends Activity>> getActivities() {
        return this.b;
    }
}
