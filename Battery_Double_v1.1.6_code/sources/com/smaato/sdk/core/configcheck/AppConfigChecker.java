package com.smaato.sdk.core.configcheck;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.AppMetaData;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Predicate;
import java.util.List;

public final class AppConfigChecker {
    @NonNull
    private final Logger a;
    @NonNull
    private final ExpectedManifestEntries b;
    @NonNull
    private final AppMetaData c;

    public AppConfigChecker(@NonNull Logger logger, @NonNull ExpectedManifestEntries expectedManifestEntries, @NonNull AppMetaData appMetaData) {
        this.a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for AppManifestConfigChecker::new");
        this.b = (ExpectedManifestEntries) Objects.requireNonNull(expectedManifestEntries, "Parameter expectedManifestEntries cannot be null for AppManifestConfigChecker::new");
        this.c = (AppMetaData) Objects.requireNonNull(appMetaData, "Parameter permissionChecker cannot be null for AppManifestConfigChecker::new");
    }

    @NonNull
    public final AppConfigCheckResult check() {
        List filter = Lists.filter(this.b.getPermissionsMandatory(), new Predicate() {
            public final boolean test(Object obj) {
                return AppConfigChecker.this.a((String) obj);
            }
        });
        boolean isEmpty = filter.isEmpty();
        if (!isEmpty) {
            this.a.error(LogDomain.CONFIG_CHECK, "Mandatory permissions are not granted: %s", Joiner.join((CharSequence) ", ", (Iterable) filter));
        }
        List filter2 = Lists.filter(this.b.getActivities(), new Predicate() {
            public final boolean test(Object obj) {
                return AppConfigChecker.this.a((Class) obj);
            }
        });
        boolean isEmpty2 = filter2.isEmpty();
        if (!isEmpty2) {
            this.a.error(LogDomain.CONFIG_CHECK, "Mandatory activities are not declared in the application manifest: %s", Joiner.join((CharSequence) ", ", (Iterable) filter2));
        }
        return new AppConfigCheckResult(isEmpty, isEmpty2);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(String str) {
        return !this.c.isPermissionGranted(str);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(Class cls) {
        return !this.c.isActivityRegistered(cls);
    }
}
