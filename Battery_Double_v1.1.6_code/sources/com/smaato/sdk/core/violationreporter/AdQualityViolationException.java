package com.smaato.sdk.core.violationreporter;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.util.Objects;

public final class AdQualityViolationException extends Exception {
    @NonNull
    public final String adQualityViolationType;
    @NonNull
    public final String originalUrl;
    @NonNull
    public final SomaApiContext somaApiContext;
    @NonNull
    public final String violatedUrl;

    public AdQualityViolationException(@NonNull String str, @NonNull SomaApiContext somaApiContext2, @NonNull String str2, @NonNull String str3) {
        this.adQualityViolationType = (String) Objects.requireNonNull(str);
        this.somaApiContext = (SomaApiContext) Objects.requireNonNull(somaApiContext2);
        this.violatedUrl = (String) Objects.requireNonNull(str2);
        this.originalUrl = (String) Objects.requireNonNull(str3);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdQualityViolationException{adQualityViolationType=");
        sb.append(this.adQualityViolationType);
        sb.append(", somaApiContext=");
        sb.append(this.somaApiContext);
        sb.append(", violatedUrl='");
        sb.append(this.violatedUrl);
        sb.append('\'');
        sb.append(", originalUrl='");
        sb.append(this.originalUrl);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
