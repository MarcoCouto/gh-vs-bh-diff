package com.smaato.sdk.core.violationreporter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkClient;
import com.smaato.sdk.core.network.NetworkClient.Listener;
import com.smaato.sdk.core.network.NetworkHttpRequest.Builder;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.network.NetworkResponse;
import com.smaato.sdk.core.network.execution.NetworkLayerException;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;

public final class AdQualityViolationReporter {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    @NonNull
    private final NetworkClient b;
    @NonNull
    private final AdQualityViolationReportMapper c;
    @NonNull
    private final String d;
    @NonNull
    private final String e;
    @NonNull
    private final Listener f = new Listener() {
        public void onRequestSuccess(@NonNull NetworkClient networkClient, @NonNull Task task, @NonNull NetworkResponse networkResponse) {
            int responseCode = networkResponse.getResponseCode();
            if (responseCode == 200) {
                AdQualityViolationReporter.this.a.debug(LogDomain.CORE, "ad quality violation report request has been accepted by server", new Object[0]);
                return;
            }
            AdQualityViolationReporter.this.a.error(LogDomain.CORE, "ad quality violation report request has not been accepted, response code: %d", Integer.valueOf(responseCode));
        }

        public void onRequestError(@NonNull NetworkClient networkClient, @NonNull Task task, @NonNull NetworkLayerException networkLayerException) {
            AdQualityViolationReporter.this.a.error(LogDomain.CORE, "ad quality violation report request failed: %s", networkLayerException);
        }
    };

    public AdQualityViolationReporter(@NonNull Logger logger, @NonNull NetworkClient networkClient, @NonNull AdQualityViolationReportMapper adQualityViolationReportMapper, @NonNull String str, @NonNull String str2) {
        this.a = logger;
        this.b = (NetworkClient) Objects.requireNonNull(networkClient);
        this.c = (AdQualityViolationReportMapper) Objects.requireNonNull(adQualityViolationReportMapper);
        this.d = (String) Objects.requireNonNull(str);
        this.e = (String) Objects.requireNonNull(str2);
        this.b.setListener(this.f);
    }

    public final void reportAdViolation(@NonNull String str, @NonNull SomaApiContext somaApiContext, @Nullable String str2) {
        if (TextUtils.isEmpty(str2)) {
            str2 = "";
        }
        reportAdViolation(str, somaApiContext, str2, "");
    }

    public final void reportAdViolation(@NonNull String str, @NonNull SomaApiContext somaApiContext, @NonNull String str2, @NonNull String str3) {
        try {
            String jSONObject = this.c.a(str, somaApiContext, str2, str3, System.currentTimeMillis()).toJson().toString();
            this.a.debug(LogDomain.CORE, "going to send: %s", jSONObject);
            try {
                byte[] bytes = jSONObject.getBytes("UTF-8");
                Builder readTimeout = new Builder().setUrl(this.d).setMethod(Method.POST).setConnectionTimeout(15000).setReadTimeout(15000);
                HashMap hashMap = new HashMap();
                hashMap.put("Content-Type", Collections.singletonList("application/json; charset=utf-8"));
                hashMap.put("User-Agent", Collections.singletonList(this.e));
                this.b.performNetworkRequest(readTimeout.setHeaders(hashMap).setBody(bytes).build(), null).start();
            } catch (UnsupportedEncodingException e2) {
                throw new RuntimeException(e2);
            }
        } catch (Exception e3) {
            this.a.error(LogDomain.CORE, "failed to create ad quality violation report", e3);
        }
    }
}
