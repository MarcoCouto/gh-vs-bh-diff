package com.smaato.sdk.core.violationreporter;

import androidx.annotation.NonNull;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.json.JSONObject;

final class Report {
    @NonNull
    public final String adSpace;
    @NonNull
    public final String apiKey;
    @NonNull
    public final String apiVersion;
    @NonNull
    public final String asnId;
    @NonNull
    public final String bundleId;
    @NonNull
    public final String creativeId;
    @NonNull
    public final String error;
    @NonNull
    public final String originalUrl;
    @NonNull
    public final String platform;
    @NonNull
    public final String publisher;
    @NonNull
    public final String sci;
    @NonNull
    public final String sdkVersion;
    @NonNull
    public final String sessionId;
    @NonNull
    public final String timestamp;
    @NonNull
    public final String type;
    @NonNull
    public final String violatedUrl;

    public static class Builder {
        private String a;
        private String b;
        private String c;
        private String d;
        private String e;
        private String f;
        private String g;
        private String h;
        private String i;
        private String j;
        private String k;
        private String l;
        private String m;
        private String n;
        private String o;
        private String p;

        @NonNull
        public Builder setType(@NonNull String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setSci(@NonNull String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setTimestamp(@NonNull String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Builder setError(@NonNull String str) {
            this.d = str;
            return this;
        }

        @NonNull
        public Builder setSdkVersion(@NonNull String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public Builder setBundleId(@NonNull String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public Builder setViolatedUrl(@NonNull String str) {
            this.g = str;
            return this;
        }

        @NonNull
        public Builder setPublisher(@NonNull String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public Builder setPlatform(@NonNull String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public Builder setAdSpace(@NonNull String str) {
            this.j = str;
            return this;
        }

        @NonNull
        public Builder setSessionId(@NonNull String str) {
            this.k = str;
            return this;
        }

        @NonNull
        public Builder setApiKey(@NonNull String str) {
            this.l = str;
            return this;
        }

        @NonNull
        public Builder setApiVersion(@NonNull String str) {
            this.m = str;
            return this;
        }

        @NonNull
        public Builder setOriginalUrl(@NonNull String str) {
            this.n = str;
            return this;
        }

        @NonNull
        public Builder setCreativeId(@NonNull String str) {
            this.o = str;
            return this;
        }

        @NonNull
        public Builder setAsnId(@NonNull String str) {
            this.p = str;
            return this;
        }

        @NonNull
        public Report build() {
            ArrayList arrayList = new ArrayList();
            if (this.a == null) {
                arrayList.add("type");
            }
            if (this.b == null) {
                arrayList.add("sci");
            }
            if (this.c == null) {
                arrayList.add("timestamp");
            }
            if (this.d == null) {
                arrayList.add("error");
            }
            if (this.e == null) {
                arrayList.add(GeneralPropertiesWorker.SDK_VERSION);
            }
            if (this.f == null) {
                arrayList.add(RequestParameters.PACKAGE_NAME);
            }
            if (this.g == null) {
                arrayList.add("violatedUrl");
            }
            if (this.h == null) {
                arrayList.add("publisher");
            }
            if (this.i == null) {
                arrayList.add(TapjoyConstants.TJC_PLATFORM);
            }
            if (this.j == null) {
                arrayList.add("adSpace");
            }
            if (this.k == null) {
                arrayList.add("sessionId");
            }
            if (this.l == null) {
                arrayList.add("apiKey");
            }
            if (this.m == null) {
                arrayList.add(Constants.CONVERT_API_VERSION);
            }
            if (this.n == null) {
                arrayList.add("originalUrl");
            }
            if (this.o == null) {
                arrayList.add(RequestParameters.CREATIVE_ID);
            }
            if (this.p == null) {
                arrayList.add("asnId");
            }
            if (arrayList.isEmpty()) {
                Report report = new Report(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, 0);
                return report;
            }
            StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
            sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
            throw new IllegalArgumentException(sb.toString());
        }
    }

    /* synthetic */ Report(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, byte b) {
        this(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14, str15, str16);
    }

    private Report(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull String str4, @NonNull String str5, @NonNull String str6, @NonNull String str7, @NonNull String str8, @NonNull String str9, @NonNull String str10, @NonNull String str11, @NonNull String str12, @NonNull String str13, @NonNull String str14, @NonNull String str15, @NonNull String str16) {
        this.type = (String) Objects.requireNonNull(str);
        this.sci = (String) Objects.requireNonNull(str2);
        this.timestamp = (String) Objects.requireNonNull(str3);
        this.error = (String) Objects.requireNonNull(str4);
        this.sdkVersion = (String) Objects.requireNonNull(str5);
        this.bundleId = (String) Objects.requireNonNull(str6);
        this.violatedUrl = (String) Objects.requireNonNull(str7);
        this.publisher = (String) Objects.requireNonNull(str8);
        this.platform = (String) Objects.requireNonNull(str9);
        this.adSpace = (String) Objects.requireNonNull(str10);
        this.sessionId = (String) Objects.requireNonNull(str11);
        this.apiKey = (String) Objects.requireNonNull(str12);
        this.apiVersion = (String) Objects.requireNonNull(str13);
        this.originalUrl = (String) Objects.requireNonNull(str14);
        this.creativeId = (String) Objects.requireNonNull(str15);
        this.asnId = (String) Objects.requireNonNull(str16);
    }

    @NonNull
    public final JSONObject toJson() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("sci", this.sci);
        linkedHashMap.put("timestamp", this.timestamp);
        linkedHashMap.put("error", this.error);
        linkedHashMap.put("sdkversion", this.sdkVersion);
        linkedHashMap.put("bundleid", this.bundleId);
        linkedHashMap.put("type", this.type);
        linkedHashMap.put("violatedurl", this.violatedUrl);
        linkedHashMap.put("publisher", this.publisher);
        linkedHashMap.put(TapjoyConstants.TJC_PLATFORM, this.platform);
        linkedHashMap.put("adspace", this.adSpace);
        linkedHashMap.put("sessionid", this.sessionId);
        linkedHashMap.put("apikey", this.apiKey);
        linkedHashMap.put("apiversion", this.apiVersion);
        linkedHashMap.put("originalurl", this.originalUrl);
        linkedHashMap.put("creativeid", this.creativeId);
        linkedHashMap.put("asnid", this.asnId);
        return new JSONObject(linkedHashMap);
    }
}
