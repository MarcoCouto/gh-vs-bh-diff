package com.smaato.sdk.core.violationreporter;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.api.ApiAdRequest;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.HeaderUtils;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.violationreporter.Report.Builder;
import java.util.Map;

final class AdQualityViolationReportMapper {
    @NonNull
    private final Logger a;
    @NonNull
    private final HeaderUtils b;

    AdQualityViolationReportMapper(@NonNull Logger logger, @NonNull HeaderUtils headerUtils) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (HeaderUtils) Objects.requireNonNull(headerUtils);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Report a(@NonNull String str, @NonNull SomaApiContext somaApiContext, @NonNull String str2, @NonNull String str3, long j) {
        ApiAdRequest apiAdRequest = somaApiContext.getApiAdRequest();
        Map responseHeaders = somaApiContext.getApiAdResponse().getResponseHeaders();
        String extractHeaderMultiValue = this.b.extractHeaderMultiValue(responseHeaders, "X-SMT-SessionId");
        if (extractHeaderMultiValue == null) {
            this.a.warning(LogDomain.CORE, "header %s is not found in SOMA response", "X-SMT-SessionId");
        }
        String extractHeaderMultiValue2 = this.b.extractHeaderMultiValue(responseHeaders, "SCI");
        if (extractHeaderMultiValue2 == null) {
            this.a.warning(LogDomain.CORE, "header %s is not found in SOMA response", "SCI");
        }
        Builder timestamp = new Builder().setType(str).setSessionId(extractHeaderMultiValue).setOriginalUrl(str3).setViolatedUrl(str2).setTimestamp(String.valueOf(j));
        if (extractHeaderMultiValue2 == null) {
            extractHeaderMultiValue2 = "";
        }
        return timestamp.setSci(extractHeaderMultiValue2).setPublisher(apiAdRequest.getPublisherId()).setAdSpace(apiAdRequest.getAdSpaceId()).setApiVersion("").setBundleId(apiAdRequest.getBundle()).setError("").setPlatform("android").setSdkVersion(apiAdRequest.getClient()).setApiKey("").setCreativeId("").setAsnId("0").build();
    }
}
