package com.smaato.sdk.core.violationreporter;

import com.smaato.sdk.core.BuildConfig;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.violationreporter.-$$Lambda$DiAdQualityViolationReporterLayer$PH_A_Gv4653oGIchbQfTDMXNlyI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiAdQualityViolationReporterLayer$PH_A_Gv4653oGIchbQfTDMXNlyI implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiAdQualityViolationReporterLayer$PH_A_Gv4653oGIchbQfTDMXNlyI INSTANCE = new $$Lambda$DiAdQualityViolationReporterLayer$PH_A_Gv4653oGIchbQfTDMXNlyI();

    private /* synthetic */ $$Lambda$DiAdQualityViolationReporterLayer$PH_A_Gv4653oGIchbQfTDMXNlyI() {
    }

    public final Object get(DiConstructor diConstructor) {
        return BuildConfig.SOMA_VIOLATIONS_AGGREGATOR_URL;
    }
}
