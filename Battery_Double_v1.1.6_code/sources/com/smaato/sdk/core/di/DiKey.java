package com.smaato.sdk.core.di;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

final class DiKey {
    @Nullable
    private String a;
    @NonNull
    private Class b;

    DiKey(@Nullable String str, @NonNull Class cls) {
        this.a = str;
        this.b = cls;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DiKey diKey = (DiKey) obj;
        if (this.a == null ? diKey.a == null : this.a.equals(diKey.a)) {
            return this.b.equals(diKey.b);
        }
        return false;
    }

    public final int hashCode() {
        return ((this.a != null ? this.a.hashCode() : 0) * 31) + this.b.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("DiKey{name='");
        sb.append(this.a);
        sb.append('\'');
        sb.append(", clazz=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
