package com.smaato.sdk.core.di;

import androidx.annotation.NonNull;

public final class CoreDiNames {
    @NonNull
    public static final String NAME_AD_CONTENT_RATING = "ad_content_rating";
    @NonNull
    public static final String NAME_DEFAULT_SHARED_PREFERENCES = "default_SharedPreferences";
    @NonNull
    public static final String NAME_HTTPS_ONLY = "https_only";
    @NonNull
    public static final String SOMA_API_URL = "SOMA_API_URL";
    @NonNull
    public static final String SOMA_VIOLATIONS_AGGREGATOR_URL = "SOMA_VIOLATIONS_AGGREGATOR_URL";

    private CoreDiNames() {
    }
}
