package com.smaato.sdk.core.di;

import androidx.annotation.NonNull;

public interface ClassFactory<T> {
    @NonNull
    T get(DiConstructor diConstructor);
}
