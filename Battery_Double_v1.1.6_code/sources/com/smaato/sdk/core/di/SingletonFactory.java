package com.smaato.sdk.core.di;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

final class SingletonFactory<T> implements ClassFactory<T> {
    private static final Object a = new Object();
    private volatile ClassFactory<T> b;
    private volatile Object c = a;

    private SingletonFactory(ClassFactory<T> classFactory) {
        this.b = (ClassFactory) Objects.requireNonNull(classFactory);
    }

    @NonNull
    public final T get(DiConstructor diConstructor) {
        T t = this.c;
        if (t == a) {
            synchronized (this) {
                t = this.c;
                if (t == a) {
                    T t2 = this.b.get(diConstructor);
                    T t3 = this.c;
                    if (t3 != a) {
                        if (t3 != t2) {
                            StringBuilder sb = new StringBuilder("Scoped provider was invoked recursively returning different results: ");
                            sb.append(t3);
                            sb.append(" & ");
                            sb.append(t2);
                            sb.append(". This is likely due to a circular dependency.");
                            throw new IllegalStateException(sb.toString());
                        }
                    }
                    this.c = t2;
                    this.b = null;
                    t = t2;
                }
            }
        }
        return t;
    }

    public static <T> ClassFactory<T> wrap(ClassFactory<T> classFactory) {
        Objects.requireNonNull(classFactory);
        if (classFactory instanceof SingletonFactory) {
            return classFactory;
        }
        return new SingletonFactory(classFactory);
    }
}
