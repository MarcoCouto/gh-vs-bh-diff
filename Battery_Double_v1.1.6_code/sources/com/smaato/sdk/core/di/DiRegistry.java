package com.smaato.sdk.core.di;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class DiRegistry {
    private Map<DiKey, ClassFactory> a = new HashMap();

    private DiRegistry() {
    }

    public static DiRegistry of(Consumer<DiRegistry> consumer) {
        DiRegistry diRegistry = new DiRegistry();
        consumer.accept(diRegistry);
        return diRegistry;
    }

    public final <T> void registerFactory(Class<T> cls, ClassFactory<T> classFactory) {
        registerFactory(null, cls, classFactory);
    }

    public final <T> void registerFactory(@Nullable String str, @NonNull Class<T> cls, @NonNull ClassFactory<T> classFactory) {
        Objects.requireNonNull(classFactory);
        Objects.requireNonNull(cls);
        DiKey diKey = new DiKey(str, cls);
        a(diKey);
        this.a.put(diKey, classFactory);
    }

    public final <T> void registerSingletonFactory(@NonNull Class<T> cls, @NonNull ClassFactory<T> classFactory) {
        registerSingletonFactory(null, cls, classFactory);
    }

    public final <T> void registerSingletonFactory(@Nullable String str, @NonNull Class<T> cls, @NonNull ClassFactory<T> classFactory) {
        Objects.requireNonNull(classFactory);
        Objects.requireNonNull(cls);
        DiKey diKey = new DiKey(str, cls);
        a(diKey);
        this.a.put(diKey, SingletonFactory.wrap(classFactory));
    }

    private void a(DiKey diKey) {
        if (this.a.containsKey(diKey)) {
            StringBuilder sb = new StringBuilder("There is already registered factory for ");
            sb.append(diKey);
            throw new IllegalStateException(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public final Map<DiKey, ClassFactory> a() {
        return this.a;
    }

    @NonNull
    public final DiRegistry addFrom(@Nullable DiRegistry diRegistry) {
        if (diRegistry != null) {
            for (Entry entry : diRegistry.a.entrySet()) {
                DiKey diKey = (DiKey) entry.getKey();
                a(diKey);
                this.a.put(diKey, entry.getValue());
            }
        }
        return this;
    }
}
