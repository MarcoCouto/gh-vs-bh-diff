package com.smaato.sdk.core.gdpr;

enum CmpPurpose {
    INFORMATION_STORAGE_AND_ACCESS(1),
    AD_SELECTION_DELIVER_REPORTING(3);
    
    public final int id;

    private CmpPurpose(int i) {
        this.id = i;
    }
}
