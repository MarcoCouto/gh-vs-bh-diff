package com.smaato.sdk.core.gdpr;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.EnumMap;
import java.util.Map.Entry;

public class SomaGdprData {
    @NonNull
    private final SubjectToGdpr a;
    @NonNull
    private final String b;
    @NonNull
    private final EnumMap<PiiParam, Boolean> c;

    SomaGdprData(@NonNull SubjectToGdpr subjectToGdpr, @NonNull String str, @NonNull EnumMap<PiiParam, Boolean> enumMap) {
        this.a = (SubjectToGdpr) Objects.requireNonNull(subjectToGdpr, "subjectToGdpr must not be null for SomaGdprData::new");
        this.b = (String) Objects.requireNonNull(str, "consentString must not be null for SomaGdprData::new");
        this.c = new EnumMap<>((EnumMap) Objects.requireNonNull(enumMap, "piiParamToConsentMap must not be null for SomaGdprData::new"));
        for (Entry value : this.c.entrySet()) {
            if (value.getValue() == null) {
                throw new IllegalArgumentException("piiParamToConsentMap must not contain null value for SomaGdprData::new");
            }
        }
    }

    @NonNull
    public SubjectToGdpr getSubjectToGdpr() {
        return this.a;
    }

    @Nullable
    public Boolean isGdprEnabled() {
        if (this.a == SubjectToGdpr.CMP_GDPR_UNKNOWN) {
            return null;
        }
        return Boolean.valueOf(this.a == SubjectToGdpr.CMP_GDPR_ENABLED);
    }

    @NonNull
    public String getConsentString() {
        return this.b;
    }

    public boolean isUsageAllowedFor(@NonNull PiiParam piiParam) {
        return ((Boolean) this.c.get(piiParam)).booleanValue();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SomaGdprData{subjectToGdpr=");
        sb.append(this.a);
        sb.append(", consentString='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", piiParamToConsentMap=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}
