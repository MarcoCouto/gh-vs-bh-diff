package com.smaato.sdk.core.gdpr;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Predicate;
import java.util.EnumMap;

public final class SomaGdprUtils {
    @NonNull
    public final SomaGdprData createSomaGdprData(@NonNull CmpData cmpData) {
        PiiParam[] values;
        Objects.requireNonNull(cmpData, "cmpData must not be null for SomaGdprData::from");
        SubjectToGdpr subjectToGdpr = cmpData.getSubjectToGdpr();
        String consentString = cmpData.getConsentString();
        EnumMap enumMap = new EnumMap(PiiParam.class);
        for (PiiParam piiParam : PiiParam.values()) {
            boolean z = true;
            switch (cmpData.getSubjectToGdpr()) {
                case CMP_GDPR_ENABLED:
                    String consentString2 = cmpData.getConsentString();
                    if (!consentString2.isEmpty() && !consentString2.equals("0")) {
                        if (consentString2.equals("1")) {
                            if (piiParam != PiiParam.GPS) {
                                break;
                            }
                        } else {
                            String purposesString = cmpData.getPurposesString();
                            if (!purposesString.isEmpty() && !purposesString.matches("[01]+")) {
                                break;
                            } else {
                                String vendorsString = cmpData.getVendorsString();
                                if (!vendorsString.isEmpty()) {
                                    if (!vendorsString.matches("[01]+")) {
                                        break;
                                    }
                                }
                                if (a(cmpData.getVendorsString(), 82) && Lists.all(piiParam.purposes, new Predicate(cmpData) {
                                    private final /* synthetic */ CmpData f$1;

                                    {
                                        this.f$1 = r2;
                                    }

                                    public final boolean test(Object obj) {
                                        return SomaGdprUtils.this.a(this.f$1, (CmpPurpose) obj);
                                    }
                                })) {
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case CMP_GDPR_DISABLED:
                    break;
                default:
                    String consentString3 = cmpData.getConsentString();
                    if (!consentString3.isEmpty()) {
                        if (!consentString3.equals("0")) {
                            if (consentString3.equals("1")) {
                                if (piiParam != PiiParam.GPS) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        z = false;
                        break;
                    } else {
                        break;
                    }
            }
            enumMap.put(piiParam, Boolean.valueOf(z));
        }
        return new SomaGdprData(subjectToGdpr, consentString, enumMap);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean a(CmpData cmpData, CmpPurpose cmpPurpose) {
        return a(cmpData.getPurposesString(), cmpPurpose.id);
    }

    @VisibleForTesting
    private static boolean a(@NonNull String str, int i) {
        if (i > str.length() || i <= 0 || '1' != str.charAt(i - 1)) {
            return false;
        }
        return true;
    }
}
