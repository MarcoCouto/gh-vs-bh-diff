package com.smaato.sdk.core.gdpr;

import androidx.annotation.NonNull;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

public enum PiiParam {
    GPS(EnumSet.of(CmpPurpose.AD_SELECTION_DELIVER_REPORTING)),
    GOOGLE_AD_ID(EnumSet.of(CmpPurpose.INFORMATION_STORAGE_AND_ACCESS, CmpPurpose.AD_SELECTION_DELIVER_REPORTING)),
    GENDER(EnumSet.of(CmpPurpose.AD_SELECTION_DELIVER_REPORTING)),
    AGE(EnumSet.of(CmpPurpose.AD_SELECTION_DELIVER_REPORTING)),
    ZIP(EnumSet.of(CmpPurpose.AD_SELECTION_DELIVER_REPORTING)),
    DEVICE_MODEL(EnumSet.of(CmpPurpose.AD_SELECTION_DELIVER_REPORTING));
    
    @NonNull
    public final Set<CmpPurpose> purposes;

    private PiiParam(Set<CmpPurpose> set) {
        this.purposes = Collections.unmodifiableSet(set);
    }
}
