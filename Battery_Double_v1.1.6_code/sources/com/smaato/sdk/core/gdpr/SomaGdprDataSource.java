package com.smaato.sdk.core.gdpr;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public class SomaGdprDataSource {
    @NonNull
    private final IabCmpDataStorage a;
    @NonNull
    private final SomaGdprUtils b;

    public SomaGdprDataSource(@NonNull IabCmpDataStorage iabCmpDataStorage, @NonNull SomaGdprUtils somaGdprUtils) {
        this.a = (IabCmpDataStorage) Objects.requireNonNull(iabCmpDataStorage, "iabCmpDataStorage can not be null for SomaGdprDataSource::new");
        this.b = (SomaGdprUtils) Objects.requireNonNull(somaGdprUtils, "somaGdprUtils can not be null for SomaGdprDataSource::new");
    }

    @NonNull
    public SomaGdprData getSomaGdprData() {
        return this.b.createSomaGdprData(this.a.getCmpData());
    }
}
