package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.ApiAdRequestExtras;
import com.smaato.sdk.core.util.Objects;

public final class VideoTypeAdRequestExtrasProvider implements AdRequestExtrasProvider {
    @NonNull
    private final String a;

    public VideoTypeAdRequestExtrasProvider(@NonNull String str) {
        this.a = (String) Objects.requireNonNull(str);
    }

    public final void addApiAdRequestExtras(@NonNull ApiAdRequestExtras apiAdRequestExtras) {
        AdFormat adFormat = apiAdRequestExtras.adFormat();
        if (adFormat == AdFormat.VIDEO || adFormat == AdFormat.INTERSTITIAL) {
            apiAdRequestExtras.addApiParamExtra("videotype", this.a);
        }
    }
}
