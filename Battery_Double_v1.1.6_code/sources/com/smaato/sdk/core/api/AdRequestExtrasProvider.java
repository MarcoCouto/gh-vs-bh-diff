package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.ApiAdRequestExtras;

public interface AdRequestExtrasProvider {
    void addApiAdRequestExtras(@NonNull ApiAdRequestExtras apiAdRequestExtras);
}
