package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.Expiration;
import com.smaato.sdk.core.util.CurrentTimeProvider;
import com.smaato.sdk.core.util.Objects;

public final class DefaultAdExpirationTimestampProvider {
    @NonNull
    private final CurrentTimeProvider a;
    private final long b = 300000;

    DefaultAdExpirationTimestampProvider(@NonNull CurrentTimeProvider currentTimeProvider, long j) {
        this.a = (CurrentTimeProvider) Objects.requireNonNull(currentTimeProvider);
    }

    @NonNull
    public final Expiration defaultExpirationTimestampFor(@NonNull AdFormat adFormat) {
        Objects.requireNonNull(adFormat);
        return new Expiration(this.a.currentMillisUtc() + this.b, this.a);
    }
}
