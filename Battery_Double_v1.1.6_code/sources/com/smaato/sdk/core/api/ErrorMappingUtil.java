package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.api.ApiConnector.Error;
import com.smaato.sdk.core.api.ApiResponseMapper.MappingException;
import com.smaato.sdk.core.network.execution.NetworkLayerException;

final class ErrorMappingUtil {
    private ErrorMappingUtil() {
    }

    @NonNull
    static ApiConnectorException a(@NonNull NetworkLayerException networkLayerException) {
        Error error;
        switch (networkLayerException.getErrorType()) {
            case CANCELLED:
                error = Error.CANCELLED;
                break;
            case TIMEOUT:
                error = Error.TRANSPORT_TIMEOUT;
                break;
            case IO_ERROR:
            case IO_TOO_MANY_REDIRECTS:
                error = Error.TRANSPORT_IO_ERROR;
                break;
            case NO_NETWORK_CONNECTION:
                error = Error.TRANSPORT_NO_NETWORK_CONNECTION;
                break;
            default:
                error = Error.TRANSPORT_GENERIC;
                break;
        }
        return new ApiConnectorException(error, networkLayerException);
    }

    @NonNull
    static ApiConnectorException a(@NonNull MappingException mappingException) {
        Error error;
        switch (mappingException.type) {
            case NO_AD:
                error = Error.NO_AD;
                break;
            case BAD_REQUEST:
                error = Error.BAD_REQUEST;
                break;
            default:
                error = Error.RESPONSE_MAPPING;
                break;
        }
        return new ApiConnectorException(error, mappingException);
    }
}
