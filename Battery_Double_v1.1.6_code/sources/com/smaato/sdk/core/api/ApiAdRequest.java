package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.auto.value.AutoValue;
import java.util.Map;
import java.util.Set;

@AutoValue
public abstract class ApiAdRequest {

    @com.google.auto.value.AutoValue.Builder
    public static abstract class Builder {
        @NonNull
        public abstract ApiAdRequest build();

        @NonNull
        public abstract Builder setAdContentRating(@Nullable String str);

        @NonNull
        public abstract Builder setAdDimension(@Nullable String str);

        @NonNull
        public abstract Builder setAdFormat(@NonNull String str);

        @NonNull
        public abstract Builder setAdSpaceId(@NonNull String str);

        @NonNull
        public abstract Builder setAge(@Nullable Integer num);

        @NonNull
        public abstract Builder setBundle(@Nullable String str);

        @NonNull
        public abstract Builder setCarrierCode(@Nullable String str);

        @NonNull
        public abstract Builder setCarrierName(@Nullable String str);

        @NonNull
        public abstract Builder setClient(@Nullable String str);

        @NonNull
        public abstract Builder setConnection(@Nullable String str);

        @NonNull
        public abstract Builder setCoppa(@Nullable Integer num);

        @NonNull
        public abstract Builder setDeviceModel(@Nullable String str);

        @NonNull
        public abstract Builder setExtensions(@Nullable String str);

        @NonNull
        public abstract Builder setExtraParameters(@Nullable Map<String, Object> map);

        @NonNull
        public abstract Builder setGdpr(@Nullable Integer num);

        @NonNull
        public abstract Builder setGdprConsent(@Nullable String str);

        @NonNull
        public abstract Builder setGender(@Nullable String str);

        @NonNull
        public abstract Builder setGeoType(@Nullable Integer num);

        @NonNull
        public abstract Builder setGoogleAdId(@Nullable String str);

        @NonNull
        public abstract Builder setGoogleDnt(@Nullable Boolean bool);

        @NonNull
        public abstract Builder setGps(@Nullable String str);

        @NonNull
        public abstract Builder setHeaderClient(@Nullable String str);

        @NonNull
        public abstract Builder setHeight(@Nullable Integer num);

        @NonNull
        public abstract Builder setHttpsOnly(@Nullable Integer num);

        @NonNull
        public abstract Builder setKeyValuePairs(@Nullable Map<String, Set<String>> map);

        @NonNull
        public abstract Builder setKeywords(@Nullable String str);

        @NonNull
        public abstract Builder setLanguage(@Nullable String str);

        @NonNull
        public abstract Builder setMediationAdapterVersion(@Nullable String str);

        @NonNull
        public abstract Builder setMediationNetworkName(@Nullable String str);

        @NonNull
        public abstract Builder setMediationNetworkSDKVersion(@Nullable String str);

        @NonNull
        public abstract Builder setPublisherId(@NonNull String str);

        @NonNull
        public abstract Builder setRegion(@Nullable String str);

        @NonNull
        public abstract Builder setSearchQuery(@Nullable String str);

        @NonNull
        public abstract Builder setUsPrivacyString(@Nullable String str);

        @NonNull
        public abstract Builder setWidth(@Nullable Integer num);

        @NonNull
        public abstract Builder setZip(@Nullable String str);
    }

    @Nullable
    public abstract String getAdContentRating();

    @Nullable
    public abstract String getAdDimension();

    @NonNull
    public abstract String getAdFormat();

    @NonNull
    public abstract String getAdSpaceId();

    @Nullable
    public abstract Integer getAge();

    @Nullable
    public abstract String getBundle();

    @Nullable
    public abstract String getCarrierCode();

    @Nullable
    public abstract String getCarrierName();

    @Nullable
    public abstract String getClient();

    @Nullable
    public abstract String getConnection();

    @NonNull
    public abstract Integer getCoppa();

    @Nullable
    public abstract String getDeviceModel();

    @Nullable
    public abstract String getExtensions();

    @Nullable
    public abstract Map<String, Object> getExtraParameters();

    @Nullable
    public abstract Integer getGdpr();

    @Nullable
    public abstract String getGdprConsent();

    @Nullable
    public abstract String getGender();

    @Nullable
    public abstract Integer getGeoType();

    @Nullable
    public abstract String getGoogleAdId();

    @Nullable
    public abstract Boolean getGoogleDnt();

    @Nullable
    public abstract String getGps();

    @Nullable
    public abstract String getHeaderClient();

    @Nullable
    public abstract Integer getHeight();

    @NonNull
    public abstract Integer getHttpsOnly();

    @Nullable
    public abstract Map<String, Set<String>> getKeyValuePairs();

    @Nullable
    public abstract String getKeywords();

    @Nullable
    public abstract String getLanguage();

    @Nullable
    public abstract String getMediationAdapterVersion();

    @Nullable
    public abstract String getMediationNetworkName();

    @Nullable
    public abstract String getMediationNetworkSDKVersion();

    @NonNull
    public abstract String getPublisherId();

    @Nullable
    public abstract String getRegion();

    @Nullable
    public abstract String getSearchQuery();

    @Nullable
    public abstract String getUsPrivacyString();

    @Nullable
    public abstract Integer getWidth();

    @Nullable
    public abstract String getZip();

    @NonNull
    public static Builder builder() {
        return new Builder();
    }

    @NonNull
    public Builder newBuilder() {
        return builder().setPublisherId(getPublisherId()).setAdSpaceId(getAdSpaceId()).setAdFormat(getAdFormat()).setCoppa(getCoppa()).setHttpsOnly(getHttpsOnly()).setAdDimension(getAdDimension()).setWidth(getWidth()).setHeight(getHeight()).setMediationNetworkName(getMediationNetworkName()).setMediationNetworkSDKVersion(getMediationNetworkSDKVersion()).setMediationAdapterVersion(getMediationAdapterVersion()).setGdpr(getGdpr()).setGdprConsent(getGdprConsent()).setUsPrivacyString(getUsPrivacyString()).setKeywords(getKeywords()).setSearchQuery(getSearchQuery()).setGender(getGender()).setAge(getAge()).setGps(getGps()).setRegion(getRegion()).setZip(getZip()).setLanguage(getLanguage()).setGeoType(getGeoType()).setCarrierName(getCarrierName()).setCarrierCode(getCarrierCode()).setGoogleAdId(getGoogleAdId()).setGoogleDnt(getGoogleDnt()).setClient(getClient()).setConnection(getConnection()).setDeviceModel(getDeviceModel()).setBundle(getBundle()).setExtraParameters(getExtraParameters()).setHeaderClient(getHeaderClient()).setExtensions(getExtensions()).setAdContentRating(getAdContentRating());
    }
}
