package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.api.ApiConnector.Listener;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkClient;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;

public class ApiConnector {
    @NonNull
    private final Logger a;
    @NonNull
    private final NetworkClient b;
    @NonNull
    private final ApiRequestMapper c;
    @NonNull
    private final NetworkClientListener d;
    /* access modifiers changed from: private */
    @Nullable
    public Listener e;
    @NonNull
    private final Callback f = new Callback() {
        /* access modifiers changed from: private */
        public /* synthetic */ void a(Task task, ApiAdResponse apiAdResponse, Listener listener) {
            listener.onAdRequestSuccess(ApiConnector.this, task, apiAdResponse);
        }

        public void onAdRequestSuccess(@NonNull Task task, @NonNull ApiAdResponse apiAdResponse) {
            Objects.onNotNull(ApiConnector.this.e, new Consumer(task, apiAdResponse) {
                private final /* synthetic */ Task f$1;
                private final /* synthetic */ ApiAdResponse f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    AnonymousClass1.this.a(this.f$1, this.f$2, (Listener) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Task task, ApiConnectorException apiConnectorException, Listener listener) {
            listener.onAdRequestError(ApiConnector.this, task, apiConnectorException);
        }

        public void onAdRequestError(@NonNull Task task, @NonNull ApiConnectorException apiConnectorException) {
            Objects.onNotNull(ApiConnector.this.e, new Consumer(task, apiConnectorException) {
                private final /* synthetic */ Task f$1;
                private final /* synthetic */ ApiConnectorException f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    AnonymousClass1.this.a(this.f$1, this.f$2, (Listener) obj);
                }
            });
        }
    };

    public enum Error {
        NO_AD,
        BAD_REQUEST,
        RESPONSE_MAPPING,
        TRANSPORT_TIMEOUT,
        TRANSPORT_IO_ERROR,
        TRANSPORT_NO_NETWORK_CONNECTION,
        TRANSPORT_GENERIC,
        CANCELLED
    }

    public interface Listener {
        void onAdRequestError(@NonNull ApiConnector apiConnector, @NonNull Task task, @NonNull ApiConnectorException apiConnectorException);

        void onAdRequestSuccess(@NonNull ApiConnector apiConnector, @NonNull Task task, @NonNull ApiAdResponse apiAdResponse);
    }

    public ApiConnector(@NonNull Logger logger, @NonNull ApiRequestMapper apiRequestMapper, @NonNull ApiResponseMapper apiResponseMapper, @NonNull NetworkClient networkClient) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.c = (ApiRequestMapper) Objects.requireNonNull(apiRequestMapper);
        this.b = (NetworkClient) Objects.requireNonNull(networkClient);
        this.d = new NetworkClientListener(logger, apiResponseMapper, this.f);
        this.b.setListener(this.d);
    }

    public void setListener(@NonNull Listener listener) {
        this.e = (Listener) Objects.requireNonNull(listener);
    }

    @NonNull
    public Task performApiAdRequest(@NonNull ApiAdRequest apiAdRequest) {
        Objects.requireNonNull(apiAdRequest);
        return this.b.performNetworkRequest(this.c.map(apiAdRequest), null);
    }
}
