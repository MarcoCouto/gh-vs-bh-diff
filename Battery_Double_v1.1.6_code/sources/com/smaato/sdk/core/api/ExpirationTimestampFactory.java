package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.Expiration;
import com.smaato.sdk.core.util.CurrentTimeProvider;
import com.smaato.sdk.core.util.Objects;

public class ExpirationTimestampFactory {
    @NonNull
    private final CurrentTimeProvider a;

    ExpirationTimestampFactory(@NonNull CurrentTimeProvider currentTimeProvider) {
        this.a = (CurrentTimeProvider) Objects.requireNonNull(currentTimeProvider);
    }

    @NonNull
    public Expiration createExpirationTimestampFor(long j) {
        return new Expiration(j, this.a);
    }
}
