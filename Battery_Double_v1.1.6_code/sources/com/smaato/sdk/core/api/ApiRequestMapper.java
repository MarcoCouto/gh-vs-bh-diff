package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.smaato.sdk.core.hooks.Hook1061;
import com.smaato.sdk.core.network.NetworkHttpRequest.Builder;
import com.smaato.sdk.core.network.NetworkRequest;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

class ApiRequestMapper {
    @NonNull
    private final String a;
    @NonNull
    private final String b;

    static class ApiParams {
        ApiParams() {
        }
    }

    static class Headers {
        Headers() {
        }
    }

    ApiRequestMapper(@NonNull String str, @NonNull String str2) {
        this.a = (String) Objects.requireNonNull(str);
        this.b = (String) Objects.requireNonNull(str2);
    }

    @NonNull
    public NetworkRequest map(@NonNull ApiAdRequest apiAdRequest) {
        Objects.requireNonNull(apiAdRequest);
        Builder method = new Builder().setUrl(this.a).setMethod(Method.GET);
        HashMap hashMap = new HashMap();
        hashMap.put("pub", Hook1061.onGetPublisherId(apiAdRequest.getPublisherId(), apiAdRequest.getAdSpaceId()));
        hashMap.put("adspace", apiAdRequest.getAdSpaceId());
        hashMap.put("format", apiAdRequest.getAdFormat());
        Objects.onNotNull(apiAdRequest.getCoppa(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("coppa", String.valueOf((Integer) obj));
            }
        });
        hashMap.put("secure", String.valueOf(apiAdRequest.getHttpsOnly()));
        Objects.onNotNull(apiAdRequest.getAdDimension(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("dimension", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getWidth(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("width", String.valueOf((Integer) obj));
            }
        });
        Objects.onNotNull(apiAdRequest.getHeight(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("height", String.valueOf((Integer) obj));
            }
        });
        Objects.onNotNull(apiAdRequest.getMediationNetworkName(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("mnn", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getMediationNetworkSDKVersion(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("mnsv", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getMediationAdapterVersion(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("mav", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getGdpr(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("gdpr", String.valueOf((Integer) obj));
            }
        });
        Objects.onNotNull(apiAdRequest.getGdprConsent(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("gdpr_consent", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getUsPrivacyString(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("us_privacy", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getKeywords(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("kws", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getSearchQuery(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("qs", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getGender(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("gender", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getAge(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put(IronSourceSegment.AGE, String.valueOf((Integer) obj));
            }
        });
        Objects.onNotNull(apiAdRequest.getGps(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("gps", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getRegion(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put(TtmlNode.TAG_REGION, (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getZip(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("zip", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getLanguage(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("lang", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getGeoType(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("geotype", String.valueOf((Integer) obj));
            }
        });
        Objects.onNotNull(apiAdRequest.getAdContentRating(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("madcr", String.valueOf((String) obj));
            }
        });
        Objects.onNotNull(apiAdRequest.getCarrierName(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("carrier", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getCarrierCode(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("carriercode", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getGoogleAdId(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("googleadid", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getGoogleDnt(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("googlednt", String.valueOf((Boolean) obj));
            }
        });
        Objects.onNotNull(apiAdRequest.getClient(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("client", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getConnection(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("connection", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getDeviceModel(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("devicemodel", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getBundle(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put(String.BUNDLE, (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getExtraParameters(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ApiRequestMapper.b(this.f$0, (Map) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getExtensions(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.put("extensions", (String) obj);
            }
        });
        Objects.onNotNull(apiAdRequest.getKeyValuePairs(), new Consumer(hashMap) {
            private final /* synthetic */ HashMap f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ApiRequestMapper.a(this.f$0, (Map) obj);
            }
        });
        method.setQueryItems(hashMap);
        HashMap hashMap2 = new HashMap();
        ArrayList arrayList = new ArrayList();
        String headerClient = apiAdRequest.getHeaderClient();
        arrayList.getClass();
        Objects.onNotNull(headerClient, new Consumer(arrayList) {
            private final /* synthetic */ ArrayList f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((String) obj);
            }
        });
        hashMap2.put("X-SMT-Client", arrayList);
        hashMap2.put("User-Agent", Collections.singletonList(this.b));
        method.setHeaders(hashMap2);
        return method.build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(HashMap hashMap, Map map) {
        for (String str : map.keySet()) {
            hashMap.put(str, String.valueOf(map.get(str)));
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(HashMap hashMap, Map map) {
        for (Entry entry : map.entrySet()) {
            StringBuilder sb = new StringBuilder("cs_");
            sb.append((String) entry.getKey());
            hashMap.put(sb.toString(), Joiner.join((CharSequence) ",", (Iterable) entry.getValue()));
        }
    }
}
