package com.smaato.sdk.core.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Map;
import java.util.Set;

final class AutoValue_ApiAdRequest extends ApiAdRequest {
    private final Boolean A;
    private final String B;
    private final String C;
    private final String D;
    private final String E;
    private final Map<String, Object> F;
    private final Map<String, Set<String>> G;
    private final String H;
    private final String I;
    private final String J;
    private final String a;
    private final String b;
    private final String c;
    private final Integer d;
    private final Integer e;
    private final String f;
    private final Integer g;
    private final Integer h;
    private final String i;
    private final String j;
    private final String k;
    private final Integer l;
    private final String m;
    private final String n;
    private final String o;
    private final String p;
    private final String q;
    private final Integer r;
    private final String s;
    private final String t;
    private final String u;
    private final String v;
    private final Integer w;
    private final String x;
    private final String y;
    private final String z;

    static final class Builder extends com.smaato.sdk.core.api.ApiAdRequest.Builder {
        private Boolean A;
        private String B;
        private String C;
        private String D;
        private String E;
        private Map<String, Object> F;
        private Map<String, Set<String>> G;
        private String H;
        private String I;
        private String J;
        private String a;
        private String b;
        private String c;
        private Integer d;
        private Integer e;
        private String f;
        private Integer g;
        private Integer h;
        private String i;
        private String j;
        private String k;
        private Integer l;
        private String m;
        private String n;
        private String o;
        private String p;
        private String q;
        private Integer r;
        private String s;
        private String t;
        private String u;
        private String v;
        private Integer w;
        private String x;
        private String y;
        private String z;

        Builder() {
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setPublisherId(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null publisherId");
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setAdSpaceId(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null adSpaceId");
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setAdFormat(String str) {
            if (str != null) {
                this.c = str;
                return this;
            }
            throw new NullPointerException("Null adFormat");
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setCoppa(@Nullable Integer num) {
            if (num != null) {
                this.d = num;
                return this;
            }
            throw new NullPointerException("Null coppa");
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setHttpsOnly(@Nullable Integer num) {
            if (num != null) {
                this.e = num;
                return this;
            }
            throw new NullPointerException("Null httpsOnly");
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setAdDimension(@Nullable String str) {
            this.f = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setWidth(@Nullable Integer num) {
            this.g = num;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setHeight(@Nullable Integer num) {
            this.h = num;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setMediationNetworkName(@Nullable String str) {
            this.i = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setMediationNetworkSDKVersion(@Nullable String str) {
            this.j = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setMediationAdapterVersion(@Nullable String str) {
            this.k = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setGdpr(@Nullable Integer num) {
            this.l = num;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setGdprConsent(@Nullable String str) {
            this.m = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setUsPrivacyString(@Nullable String str) {
            this.n = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setKeywords(@Nullable String str) {
            this.o = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setSearchQuery(@Nullable String str) {
            this.p = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setGender(@Nullable String str) {
            this.q = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setAge(@Nullable Integer num) {
            this.r = num;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setGps(@Nullable String str) {
            this.s = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setRegion(@Nullable String str) {
            this.t = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setZip(@Nullable String str) {
            this.u = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setLanguage(@Nullable String str) {
            this.v = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setGeoType(@Nullable Integer num) {
            this.w = num;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setCarrierName(@Nullable String str) {
            this.x = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setCarrierCode(@Nullable String str) {
            this.y = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setGoogleAdId(@Nullable String str) {
            this.z = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setGoogleDnt(@Nullable Boolean bool) {
            this.A = bool;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setClient(@Nullable String str) {
            this.B = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setConnection(@Nullable String str) {
            this.C = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setDeviceModel(@Nullable String str) {
            this.D = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setBundle(@Nullable String str) {
            this.E = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setExtraParameters(@Nullable Map<String, Object> map) {
            this.F = map;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setKeyValuePairs(@Nullable Map<String, Set<String>> map) {
            this.G = map;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setHeaderClient(@Nullable String str) {
            this.H = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setExtensions(@Nullable String str) {
            this.I = str;
            return this;
        }

        public final com.smaato.sdk.core.api.ApiAdRequest.Builder setAdContentRating(@Nullable String str) {
            this.J = str;
            return this;
        }

        public final ApiAdRequest build() {
            String str = "";
            if (this.a == null) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(" publisherId");
                str = sb.toString();
            }
            if (this.b == null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(" adSpaceId");
                str = sb2.toString();
            }
            if (this.c == null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(" adFormat");
                str = sb3.toString();
            }
            if (this.d == null) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str);
                sb4.append(" coppa");
                str = sb4.toString();
            }
            if (this.e == null) {
                StringBuilder sb5 = new StringBuilder();
                sb5.append(str);
                sb5.append(" httpsOnly");
                str = sb5.toString();
            }
            if (str.isEmpty()) {
                AutoValue_ApiAdRequest autoValue_ApiAdRequest = new AutoValue_ApiAdRequest(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.s, this.t, this.u, this.v, this.w, this.x, this.y, this.z, this.A, this.B, this.C, this.D, this.E, this.F, this.G, this.H, this.I, this.J, 0);
                return autoValue_ApiAdRequest;
            }
            StringBuilder sb6 = new StringBuilder("Missing required properties:");
            sb6.append(str);
            throw new IllegalStateException(sb6.toString());
        }
    }

    /* synthetic */ AutoValue_ApiAdRequest(String str, String str2, String str3, Integer num, Integer num2, String str4, Integer num3, Integer num4, String str5, String str6, String str7, Integer num5, String str8, String str9, String str10, String str11, String str12, Integer num6, String str13, String str14, String str15, String str16, Integer num7, String str17, String str18, String str19, Boolean bool, String str20, String str21, String str22, String str23, Map map, Map map2, String str24, String str25, String str26, byte b2) {
        this(str, str2, str3, num, num2, str4, num3, num4, str5, str6, str7, num5, str8, str9, str10, str11, str12, num6, str13, str14, str15, str16, num7, str17, str18, str19, bool, str20, str21, str22, str23, map, map2, str24, str25, str26);
    }

    private AutoValue_ApiAdRequest(String str, String str2, String str3, Integer num, Integer num2, @Nullable String str4, @Nullable Integer num3, @Nullable Integer num4, @Nullable String str5, @Nullable String str6, @Nullable String str7, @Nullable Integer num5, @Nullable String str8, @Nullable String str9, @Nullable String str10, @Nullable String str11, @Nullable String str12, @Nullable Integer num6, @Nullable String str13, @Nullable String str14, @Nullable String str15, @Nullable String str16, @Nullable Integer num7, @Nullable String str17, @Nullable String str18, @Nullable String str19, @Nullable Boolean bool, @Nullable String str20, @Nullable String str21, @Nullable String str22, @Nullable String str23, @Nullable Map<String, Object> map, @Nullable Map<String, Set<String>> map2, @Nullable String str24, @Nullable String str25, @Nullable String str26) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = num;
        this.e = num2;
        this.f = str4;
        this.g = num3;
        this.h = num4;
        this.i = str5;
        this.j = str6;
        this.k = str7;
        this.l = num5;
        this.m = str8;
        this.n = str9;
        this.o = str10;
        this.p = str11;
        this.q = str12;
        this.r = num6;
        this.s = str13;
        this.t = str14;
        this.u = str15;
        this.v = str16;
        this.w = num7;
        this.x = str17;
        this.y = str18;
        this.z = str19;
        this.A = bool;
        this.B = str20;
        this.C = str21;
        this.D = str22;
        this.E = str23;
        this.F = map;
        this.G = map2;
        this.H = str24;
        this.I = str25;
        this.J = str26;
    }

    @NonNull
    public final String getPublisherId() {
        return this.a;
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.b;
    }

    @NonNull
    public final String getAdFormat() {
        return this.c;
    }

    @NonNull
    public final Integer getCoppa() {
        return this.d;
    }

    @NonNull
    public final Integer getHttpsOnly() {
        return this.e;
    }

    @Nullable
    public final String getAdDimension() {
        return this.f;
    }

    @Nullable
    public final Integer getWidth() {
        return this.g;
    }

    @Nullable
    public final Integer getHeight() {
        return this.h;
    }

    @Nullable
    public final String getMediationNetworkName() {
        return this.i;
    }

    @Nullable
    public final String getMediationNetworkSDKVersion() {
        return this.j;
    }

    @Nullable
    public final String getMediationAdapterVersion() {
        return this.k;
    }

    @Nullable
    public final Integer getGdpr() {
        return this.l;
    }

    @Nullable
    public final String getGdprConsent() {
        return this.m;
    }

    @Nullable
    public final String getUsPrivacyString() {
        return this.n;
    }

    @Nullable
    public final String getKeywords() {
        return this.o;
    }

    @Nullable
    public final String getSearchQuery() {
        return this.p;
    }

    @Nullable
    public final String getGender() {
        return this.q;
    }

    @Nullable
    public final Integer getAge() {
        return this.r;
    }

    @Nullable
    public final String getGps() {
        return this.s;
    }

    @Nullable
    public final String getRegion() {
        return this.t;
    }

    @Nullable
    public final String getZip() {
        return this.u;
    }

    @Nullable
    public final String getLanguage() {
        return this.v;
    }

    @Nullable
    public final Integer getGeoType() {
        return this.w;
    }

    @Nullable
    public final String getCarrierName() {
        return this.x;
    }

    @Nullable
    public final String getCarrierCode() {
        return this.y;
    }

    @Nullable
    public final String getGoogleAdId() {
        return this.z;
    }

    @Nullable
    public final Boolean getGoogleDnt() {
        return this.A;
    }

    @Nullable
    public final String getClient() {
        return this.B;
    }

    @Nullable
    public final String getConnection() {
        return this.C;
    }

    @Nullable
    public final String getDeviceModel() {
        return this.D;
    }

    @Nullable
    public final String getBundle() {
        return this.E;
    }

    @Nullable
    public final Map<String, Object> getExtraParameters() {
        return this.F;
    }

    @Nullable
    public final Map<String, Set<String>> getKeyValuePairs() {
        return this.G;
    }

    @Nullable
    public final String getHeaderClient() {
        return this.H;
    }

    @Nullable
    public final String getExtensions() {
        return this.I;
    }

    @Nullable
    public final String getAdContentRating() {
        return this.J;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ApiAdRequest{publisherId=");
        sb.append(this.a);
        sb.append(", adSpaceId=");
        sb.append(this.b);
        sb.append(", adFormat=");
        sb.append(this.c);
        sb.append(", coppa=");
        sb.append(this.d);
        sb.append(", httpsOnly=");
        sb.append(this.e);
        sb.append(", adDimension=");
        sb.append(this.f);
        sb.append(", width=");
        sb.append(this.g);
        sb.append(", height=");
        sb.append(this.h);
        sb.append(", mediationNetworkName=");
        sb.append(this.i);
        sb.append(", mediationNetworkSDKVersion=");
        sb.append(this.j);
        sb.append(", mediationAdapterVersion=");
        sb.append(this.k);
        sb.append(", gdpr=");
        sb.append(this.l);
        sb.append(", gdprConsent=");
        sb.append(this.m);
        sb.append(", usPrivacyString=");
        sb.append(this.n);
        sb.append(", keywords=");
        sb.append(this.o);
        sb.append(", searchQuery=");
        sb.append(this.p);
        sb.append(", gender=");
        sb.append(this.q);
        sb.append(", age=");
        sb.append(this.r);
        sb.append(", gps=");
        sb.append(this.s);
        sb.append(", region=");
        sb.append(this.t);
        sb.append(", zip=");
        sb.append(this.u);
        sb.append(", language=");
        sb.append(this.v);
        sb.append(", geoType=");
        sb.append(this.w);
        sb.append(", carrierName=");
        sb.append(this.x);
        sb.append(", carrierCode=");
        sb.append(this.y);
        sb.append(", googleAdId=");
        sb.append(this.z);
        sb.append(", googleDnt=");
        sb.append(this.A);
        sb.append(", client=");
        sb.append(this.B);
        sb.append(", connection=");
        sb.append(this.C);
        sb.append(", deviceModel=");
        sb.append(this.D);
        sb.append(", bundle=");
        sb.append(this.E);
        sb.append(", extraParameters=");
        sb.append(this.F);
        sb.append(", keyValuePairs=");
        sb.append(this.G);
        sb.append(", headerClient=");
        sb.append(this.H);
        sb.append(", extensions=");
        sb.append(this.I);
        sb.append(", adContentRating=");
        sb.append(this.J);
        sb.append("}");
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ApiAdRequest)) {
            return false;
        }
        ApiAdRequest apiAdRequest = (ApiAdRequest) obj;
        return this.a.equals(apiAdRequest.getPublisherId()) && this.b.equals(apiAdRequest.getAdSpaceId()) && this.c.equals(apiAdRequest.getAdFormat()) && this.d.equals(apiAdRequest.getCoppa()) && this.e.equals(apiAdRequest.getHttpsOnly()) && (this.f != null ? this.f.equals(apiAdRequest.getAdDimension()) : apiAdRequest.getAdDimension() == null) && (this.g != null ? this.g.equals(apiAdRequest.getWidth()) : apiAdRequest.getWidth() == null) && (this.h != null ? this.h.equals(apiAdRequest.getHeight()) : apiAdRequest.getHeight() == null) && (this.i != null ? this.i.equals(apiAdRequest.getMediationNetworkName()) : apiAdRequest.getMediationNetworkName() == null) && (this.j != null ? this.j.equals(apiAdRequest.getMediationNetworkSDKVersion()) : apiAdRequest.getMediationNetworkSDKVersion() == null) && (this.k != null ? this.k.equals(apiAdRequest.getMediationAdapterVersion()) : apiAdRequest.getMediationAdapterVersion() == null) && (this.l != null ? this.l.equals(apiAdRequest.getGdpr()) : apiAdRequest.getGdpr() == null) && (this.m != null ? this.m.equals(apiAdRequest.getGdprConsent()) : apiAdRequest.getGdprConsent() == null) && (this.n != null ? this.n.equals(apiAdRequest.getUsPrivacyString()) : apiAdRequest.getUsPrivacyString() == null) && (this.o != null ? this.o.equals(apiAdRequest.getKeywords()) : apiAdRequest.getKeywords() == null) && (this.p != null ? this.p.equals(apiAdRequest.getSearchQuery()) : apiAdRequest.getSearchQuery() == null) && (this.q != null ? this.q.equals(apiAdRequest.getGender()) : apiAdRequest.getGender() == null) && (this.r != null ? this.r.equals(apiAdRequest.getAge()) : apiAdRequest.getAge() == null) && (this.s != null ? this.s.equals(apiAdRequest.getGps()) : apiAdRequest.getGps() == null) && (this.t != null ? this.t.equals(apiAdRequest.getRegion()) : apiAdRequest.getRegion() == null) && (this.u != null ? this.u.equals(apiAdRequest.getZip()) : apiAdRequest.getZip() == null) && (this.v != null ? this.v.equals(apiAdRequest.getLanguage()) : apiAdRequest.getLanguage() == null) && (this.w != null ? this.w.equals(apiAdRequest.getGeoType()) : apiAdRequest.getGeoType() == null) && (this.x != null ? this.x.equals(apiAdRequest.getCarrierName()) : apiAdRequest.getCarrierName() == null) && (this.y != null ? this.y.equals(apiAdRequest.getCarrierCode()) : apiAdRequest.getCarrierCode() == null) && (this.z != null ? this.z.equals(apiAdRequest.getGoogleAdId()) : apiAdRequest.getGoogleAdId() == null) && (this.A != null ? this.A.equals(apiAdRequest.getGoogleDnt()) : apiAdRequest.getGoogleDnt() == null) && (this.B != null ? this.B.equals(apiAdRequest.getClient()) : apiAdRequest.getClient() == null) && (this.C != null ? this.C.equals(apiAdRequest.getConnection()) : apiAdRequest.getConnection() == null) && (this.D != null ? this.D.equals(apiAdRequest.getDeviceModel()) : apiAdRequest.getDeviceModel() == null) && (this.E != null ? this.E.equals(apiAdRequest.getBundle()) : apiAdRequest.getBundle() == null) && (this.F != null ? this.F.equals(apiAdRequest.getExtraParameters()) : apiAdRequest.getExtraParameters() == null) && (this.G != null ? this.G.equals(apiAdRequest.getKeyValuePairs()) : apiAdRequest.getKeyValuePairs() == null) && (this.H != null ? this.H.equals(apiAdRequest.getHeaderClient()) : apiAdRequest.getHeaderClient() == null) && (this.I != null ? this.I.equals(apiAdRequest.getExtensions()) : apiAdRequest.getExtensions() == null) && (this.J != null ? this.J.equals(apiAdRequest.getAdContentRating()) : apiAdRequest.getAdContentRating() == null);
    }

    public final int hashCode() {
        int i2 = 0;
        int hashCode = (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode()) * 1000003) ^ this.e.hashCode()) * 1000003) ^ (this.f == null ? 0 : this.f.hashCode())) * 1000003) ^ (this.g == null ? 0 : this.g.hashCode())) * 1000003) ^ (this.h == null ? 0 : this.h.hashCode())) * 1000003) ^ (this.i == null ? 0 : this.i.hashCode())) * 1000003) ^ (this.j == null ? 0 : this.j.hashCode())) * 1000003) ^ (this.k == null ? 0 : this.k.hashCode())) * 1000003) ^ (this.l == null ? 0 : this.l.hashCode())) * 1000003) ^ (this.m == null ? 0 : this.m.hashCode())) * 1000003) ^ (this.n == null ? 0 : this.n.hashCode())) * 1000003) ^ (this.o == null ? 0 : this.o.hashCode())) * 1000003) ^ (this.p == null ? 0 : this.p.hashCode())) * 1000003) ^ (this.q == null ? 0 : this.q.hashCode())) * 1000003) ^ (this.r == null ? 0 : this.r.hashCode())) * 1000003) ^ (this.s == null ? 0 : this.s.hashCode())) * 1000003) ^ (this.t == null ? 0 : this.t.hashCode())) * 1000003) ^ (this.u == null ? 0 : this.u.hashCode())) * 1000003) ^ (this.v == null ? 0 : this.v.hashCode())) * 1000003) ^ (this.w == null ? 0 : this.w.hashCode())) * 1000003) ^ (this.x == null ? 0 : this.x.hashCode())) * 1000003) ^ (this.y == null ? 0 : this.y.hashCode())) * 1000003) ^ (this.z == null ? 0 : this.z.hashCode())) * 1000003) ^ (this.A == null ? 0 : this.A.hashCode())) * 1000003) ^ (this.B == null ? 0 : this.B.hashCode())) * 1000003) ^ (this.C == null ? 0 : this.C.hashCode())) * 1000003) ^ (this.D == null ? 0 : this.D.hashCode())) * 1000003) ^ (this.E == null ? 0 : this.E.hashCode())) * 1000003) ^ (this.F == null ? 0 : this.F.hashCode())) * 1000003) ^ (this.G == null ? 0 : this.G.hashCode())) * 1000003) ^ (this.H == null ? 0 : this.H.hashCode())) * 1000003) ^ (this.I == null ? 0 : this.I.hashCode())) * 1000003;
        if (this.J != null) {
            i2 = this.J.hashCode();
        }
        return hashCode ^ i2;
    }
}
