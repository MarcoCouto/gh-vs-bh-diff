package com.smaato.sdk.core;

public enum Gender {
    FEMALE,
    MALE,
    OTHER
}
