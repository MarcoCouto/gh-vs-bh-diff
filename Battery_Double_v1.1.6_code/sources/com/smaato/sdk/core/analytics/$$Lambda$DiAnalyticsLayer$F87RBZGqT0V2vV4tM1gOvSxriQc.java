package com.smaato.sdk.core.analytics;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.analytics.-$$Lambda$DiAnalyticsLayer$F87RBZGqT0V2vV4tM1gOvSxriQc reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiAnalyticsLayer$F87RBZGqT0V2vV4tM1gOvSxriQc implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiAnalyticsLayer$F87RBZGqT0V2vV4tM1gOvSxriQc INSTANCE = new $$Lambda$DiAnalyticsLayer$F87RBZGqT0V2vV4tM1gOvSxriQc();

    private /* synthetic */ $$Lambda$DiAnalyticsLayer$F87RBZGqT0V2vV4tM1gOvSxriQc() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiAnalyticsLayer.a(diConstructor);
    }
}
