package com.smaato.sdk.core.analytics;

import android.webkit.WebView;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

public interface WebViewViewabilityTracker extends ViewabilityTracker {
    @MainThread
    void registerAdView(@NonNull WebView webView);

    @MainThread
    void updateAdView(@NonNull WebView webView);
}
