package com.smaato.sdk.core.analytics;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.core.analytics.-$$Lambda$DiAnalyticsLayer$XZ4yrexI5ZgaNTVt61qXcGEjCec reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiAnalyticsLayer$XZ4yrexI5ZgaNTVt61qXcGEjCec implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiAnalyticsLayer$XZ4yrexI5ZgaNTVt61qXcGEjCec INSTANCE = new $$Lambda$DiAnalyticsLayer$XZ4yrexI5ZgaNTVt61qXcGEjCec();

    private /* synthetic */ $$Lambda$DiAnalyticsLayer$XZ4yrexI5ZgaNTVt61qXcGEjCec() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiAnalyticsLayer.b(diConstructor);
    }
}
