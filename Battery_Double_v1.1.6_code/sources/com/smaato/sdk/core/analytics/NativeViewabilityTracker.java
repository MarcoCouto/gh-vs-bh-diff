package com.smaato.sdk.core.analytics;

import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

public interface NativeViewabilityTracker extends ViewabilityTracker {
    @MainThread
    void registerAdView(@NonNull View view);
}
