package com.smaato.sdk.core.analytics;

import android.view.View;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.analytics.ViewabilityTracker;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.collections.Iterables;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.Collection;
import java.util.List;

abstract class BaseViewabilityTrackerComposite<T extends ViewabilityTracker> implements ViewabilityTracker {
    @NonNull
    private final List<T> a;

    BaseViewabilityTrackerComposite(@NonNull List<T> list) {
        this.a = Lists.toImmutableList((Collection<T>) list);
    }

    public final void startTracking() {
        a($$Lambda$OpOKEwdnWj6hOzcgmxWrQbiIROc.INSTANCE);
    }

    public final void stopTracking() {
        a($$Lambda$uDorTzt2PJ9zriW72C6ocYX_OxY.INSTANCE);
    }

    public final void trackImpression() {
        a($$Lambda$nkzhK3hMMJQO1nw8z4bB3V1PKfw.INSTANCE);
    }

    public final void registerFriendlyObstruction(@NonNull View view) {
        a(new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((ViewabilityTracker) obj).registerFriendlyObstruction(this.f$0);
            }
        });
    }

    public final void removeFriendlyObstruction(@NonNull View view) {
        a(new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((ViewabilityTracker) obj).removeFriendlyObstruction(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Consumer consumer) {
        Iterables.forEach(this.a, consumer);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Consumer<T> consumer) {
        Threads.runOnUi(new Runnable(consumer) {
            private final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                BaseViewabilityTrackerComposite.this.b(this.f$1);
            }
        });
    }
}
