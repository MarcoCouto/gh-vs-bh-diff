package com.smaato.sdk.core.analytics;

import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;

final class NativeViewabilityTrackerComposite extends BaseViewabilityTrackerComposite<NativeViewabilityTracker> implements NativeViewabilityTracker {
    @MainThread
    public final void registerAdView(@NonNull View view) {
        a(new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((NativeViewabilityTracker) obj).registerAdView(this.f$0);
            }
        });
    }
}
