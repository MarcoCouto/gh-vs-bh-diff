package com.smaato.sdk.core.analytics;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;

public final class ViewabilityVerificationResource {
    @NonNull
    private final String a;
    @NonNull
    private final String b;
    @NonNull
    private final String c;
    @Nullable
    private final String d;
    private final boolean e;

    public ViewabilityVerificationResource(@NonNull String str, @NonNull String str2, @NonNull String str3, @Nullable String str4, boolean z) {
        this.a = (String) Objects.requireNonNull(str);
        this.b = (String) Objects.requireNonNull(str2);
        this.c = (String) Objects.requireNonNull(str3);
        this.d = str4;
        this.e = z;
    }

    @NonNull
    public final String getVendor() {
        return this.a;
    }

    @Nullable
    public final String getParameters() {
        return this.d;
    }

    @NonNull
    public final String getJsScriptUrl() {
        return this.b;
    }

    @NonNull
    public final String getApiFramework() {
        return this.c;
    }

    public final boolean isNoBrowser() {
        return this.e;
    }
}
