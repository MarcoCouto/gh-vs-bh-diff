package com.smaato.sdk.core.analytics;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.DiRegistry;

public interface ViewabilityPlugin {
    @NonNull
    DiRegistry diRegistry();

    @NonNull
    String getName();

    void init(@NonNull Context context);
}
