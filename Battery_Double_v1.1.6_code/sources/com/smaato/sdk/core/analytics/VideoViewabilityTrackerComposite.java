package com.smaato.sdk.core.analytics;

import android.view.View;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker.VideoProps;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.List;
import java.util.Map;

final class VideoViewabilityTrackerComposite extends BaseViewabilityTrackerComposite<VideoViewabilityTracker> implements VideoViewabilityTracker {
    VideoViewabilityTrackerComposite(@NonNull List<VideoViewabilityTracker> list) {
        super(list);
    }

    public final void registerAdView(@NonNull View view, @NonNull Map<String, List<ViewabilityVerificationResource>> map) {
        a(new Consumer(view, map) {
            private final /* synthetic */ View f$0;
            private final /* synthetic */ Map f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((VideoViewabilityTracker) obj).registerAdView(this.f$0, this.f$1);
            }
        });
    }

    public final void trackPlayerStateChange() {
        a($$Lambda$j9o5e7knS7YfG3HTHucvp_0M0c.INSTANCE);
    }

    public final void trackVideoClicked() {
        a($$Lambda$P6HnH67F6dHmXxp1nJC1IvaMnCw.INSTANCE);
    }

    public final void trackLoaded(@NonNull VideoProps videoProps) {
        a(new Consumer() {
            public final void accept(Object obj) {
                ((VideoViewabilityTracker) obj).trackLoaded(VideoProps.this);
            }
        });
    }

    public final void trackStarted(float f, float f2) {
        a(new Consumer(f, f2) {
            private final /* synthetic */ float f$0;
            private final /* synthetic */ float f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((VideoViewabilityTracker) obj).trackStarted(this.f$0, this.f$1);
            }
        });
    }

    public final void trackFirstQuartile() {
        a($$Lambda$YLSFC7pzLcTr6i1EQ4EomGB31bo.INSTANCE);
    }

    public final void trackMidPoint() {
        a($$Lambda$7JlpH4e3LLIrizBh5UPhHS4YbFw.INSTANCE);
    }

    public final void trackThirdQuartile() {
        a($$Lambda$1W1AUgj46JigaW0ZO2uCuqK1fY.INSTANCE);
    }

    public final void trackCompleted() {
        a($$Lambda$wVmN2eKjTySHLtjWNEQAkTlaans.INSTANCE);
    }

    public final void trackPaused() {
        a($$Lambda$9qgZZ_UoWXHktEe3ooCtHuEvsPU.INSTANCE);
    }

    public final void trackResumed() {
        a($$Lambda$nbLQ8VrFEIYThh_aUW_IYv9aR4.INSTANCE);
    }

    public final void trackBufferStart() {
        a($$Lambda$FxDo3AkqAlpzrueWOkrnL56bI_g.INSTANCE);
    }

    public final void trackBufferFinish() {
        a($$Lambda$PjCfKl4NhVelqkD8e58VEVxLdgI.INSTANCE);
    }

    public final void trackPlayerVolumeChanged(float f) {
        a(new Consumer(f) {
            private final /* synthetic */ float f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((VideoViewabilityTracker) obj).trackPlayerVolumeChanged(this.f$0);
            }
        });
    }

    public final void trackSkipped() {
        a($$Lambda$3rX8vFa9X4MxpGol7VV2zlK98rY.INSTANCE);
    }
}
