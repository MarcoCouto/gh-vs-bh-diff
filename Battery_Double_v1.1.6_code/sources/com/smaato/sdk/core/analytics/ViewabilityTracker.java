package com.smaato.sdk.core.analytics;

import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

public interface ViewabilityTracker {
    @MainThread
    void registerFriendlyObstruction(@NonNull View view);

    @MainThread
    void removeFriendlyObstruction(@NonNull View view);

    @MainThread
    void startTracking();

    @MainThread
    void stopTracking();

    @MainThread
    void trackImpression();
}
