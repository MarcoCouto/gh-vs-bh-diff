package com.smaato.sdk.core.analytics;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.util.collections.Iterables;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public final class DiAnalyticsLayer {

    interface VideoTrackerProvider extends Function<String, VideoViewabilityTracker> {
    }

    interface WebViewTrackerProvider extends Function<String, WebViewViewabilityTracker> {
    }

    private DiAnalyticsLayer() {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, DiRegistry diRegistry) {
        Iterables.forEach(list, new Consumer() {
            public final void accept(Object obj) {
                DiRegistry.this.addFrom(((ViewabilityPlugin) obj).diRegistry());
            }
        });
        diRegistry.registerSingletonFactory(Analytics.class, new ClassFactory(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiAnalyticsLayer.a(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory(WebViewTrackerProvider.class, $$Lambda$DiAnalyticsLayer$XZ4yrexI5ZgaNTVt61qXcGEjCec.INSTANCE);
        diRegistry.registerFactory(VideoTrackerProvider.class, $$Lambda$DiAnalyticsLayer$F87RBZGqT0V2vV4tM1gOvSxriQc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Analytics a(List list, DiConstructor diConstructor) {
        return new Analytics(list, (WebViewTrackerProvider) diConstructor.get(WebViewTrackerProvider.class), (VideoTrackerProvider) diConstructor.get(VideoTrackerProvider.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ WebViewTrackerProvider b(DiConstructor diConstructor) {
        return new WebViewTrackerProvider() {
            public final Object apply(Object obj) {
                return DiAnalyticsLayer.b(DiConstructor.this, (String) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ WebViewViewabilityTracker b(DiConstructor diConstructor, String str) {
        return (WebViewViewabilityTracker) diConstructor.get(str, WebViewViewabilityTracker.class);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoTrackerProvider a(DiConstructor diConstructor) {
        return new VideoTrackerProvider() {
            public final Object apply(Object obj) {
                return DiAnalyticsLayer.a(DiConstructor.this, (String) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoViewabilityTracker a(DiConstructor diConstructor, String str) {
        return (VideoViewabilityTracker) diConstructor.get(str, VideoViewabilityTracker.class);
    }

    @NonNull
    public static DiRegistry createRegistry(@NonNull Context context) {
        ServiceLoader<ViewabilityPlugin> load = ServiceLoader.load(ViewabilityPlugin.class, context.getClassLoader());
        ArrayList arrayList = new ArrayList();
        for (ViewabilityPlugin viewabilityPlugin : load) {
            if (!viewabilityPlugin.getName().isEmpty()) {
                viewabilityPlugin.init(context);
                arrayList.add(viewabilityPlugin);
            }
        }
        return DiRegistry.of(new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                DiAnalyticsLayer.a(this.f$0, (DiRegistry) obj);
            }
        });
    }
}
