package com.smaato.sdk.core.analytics;

import android.view.View;
import androidx.annotation.NonNull;
import java.util.List;
import java.util.Map;

public interface VideoViewabilityTracker extends ViewabilityTracker {

    public static class VideoProps {
        public final boolean isAutoPlay;
        public final boolean isSkippable;
        public final float skipOffset;

        private VideoProps(boolean z, float f) {
            this.isAutoPlay = true;
            this.isSkippable = z;
            this.skipOffset = f;
        }

        private VideoProps(boolean z) {
            this(false, Float.MIN_VALUE);
        }

        @NonNull
        public static VideoProps buildForSkippableVideo(float f) {
            return new VideoProps(true, f);
        }

        @NonNull
        public static VideoProps buildForNonSkippableVideo() {
            return new VideoProps(false);
        }
    }

    void registerAdView(@NonNull View view, @NonNull Map<String, List<ViewabilityVerificationResource>> map);

    void trackBufferFinish();

    void trackBufferStart();

    void trackCompleted();

    void trackFirstQuartile();

    void trackImpression();

    void trackLoaded(@NonNull VideoProps videoProps);

    void trackMidPoint();

    void trackPaused();

    void trackPlayerStateChange();

    void trackPlayerVolumeChanged(float f);

    void trackResumed();

    void trackSkipped();

    void trackStarted(float f, float f2);

    void trackThirdQuartile();

    void trackVideoClicked();
}
