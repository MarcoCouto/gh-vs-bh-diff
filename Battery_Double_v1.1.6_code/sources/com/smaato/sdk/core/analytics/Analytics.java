package com.smaato.sdk.core.analytics;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.Function;
import java.util.List;

public final class Analytics {
    @NonNull
    private final List<ViewabilityPlugin> a;
    @NonNull
    private final WebViewTrackerProvider b;
    @NonNull
    private final VideoTrackerProvider c;

    Analytics(@NonNull Iterable<ViewabilityPlugin> iterable, @NonNull WebViewTrackerProvider webViewTrackerProvider, @NonNull VideoTrackerProvider videoTrackerProvider) {
        this.a = Lists.toImmutableList(iterable);
        this.b = (WebViewTrackerProvider) Objects.requireNonNull(webViewTrackerProvider);
        this.c = (VideoTrackerProvider) Objects.requireNonNull(videoTrackerProvider);
    }

    @NonNull
    public final List<String> getConnectedPluginNames() {
        return Lists.map(this.a, $$Lambda$q3CjFSc9xS7GM0ITl1kJY2SfieA.INSTANCE);
    }

    @NonNull
    public final WebViewViewabilityTracker getWebViewTracker() {
        return new WebViewViewabilityTrackerComposite(Lists.map(this.a, new Function() {
            public final Object apply(Object obj) {
                return Analytics.this.b((ViewabilityPlugin) obj);
            }
        }));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ WebViewViewabilityTracker b(ViewabilityPlugin viewabilityPlugin) {
        return (WebViewViewabilityTracker) this.b.apply(viewabilityPlugin.getName());
    }

    @NonNull
    public final VideoViewabilityTracker getVideoTracker() {
        return new VideoViewabilityTrackerComposite(Lists.map(this.a, new Function() {
            public final Object apply(Object obj) {
                return Analytics.this.a((ViewabilityPlugin) obj);
            }
        }));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ VideoViewabilityTracker a(ViewabilityPlugin viewabilityPlugin) {
        return (VideoViewabilityTracker) this.c.apply(viewabilityPlugin.getName());
    }
}
