package com.smaato.sdk.core.analytics;

import android.webkit.WebView;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.List;

final class WebViewViewabilityTrackerComposite extends BaseViewabilityTrackerComposite<WebViewViewabilityTracker> implements WebViewViewabilityTracker {
    WebViewViewabilityTrackerComposite(@NonNull List<WebViewViewabilityTracker> list) {
        super(list);
    }

    public final void registerAdView(@NonNull WebView webView) {
        a(new Consumer(webView) {
            private final /* synthetic */ WebView f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((WebViewViewabilityTracker) obj).registerAdView(this.f$0);
            }
        });
    }

    public final void updateAdView(@NonNull WebView webView) {
        a(new Consumer(webView) {
            private final /* synthetic */ WebView f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((WebViewViewabilityTracker) obj).updateAdView(this.f$0);
            }
        });
    }
}
