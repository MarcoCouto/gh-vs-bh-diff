package com.smaato.sdk.core.log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.appodeal.ads.AppodealNetworks;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.core.util.Objects;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class LoggerImpl implements Logger {
    @NonNull
    private static final Map<LogDomain, String> a = new HashMap();
    @NonNull
    private static final String b = LoggerImpl.class.getName();
    @NonNull
    private static final Pattern c = Pattern.compile("(\\$\\d+)+$");
    @NonNull
    private final List<LogWriter> d = new ArrayList();
    @NonNull
    private final ThreadLocal<String> e = new ThreadLocal<>();
    @NonNull
    private final Environment f;

    enum Environment {
        DEBUG,
        RELEASE
    }

    static {
        a.put(LogDomain.CORE, "core");
        a.put(LogDomain.AD, "ad");
        a.put(LogDomain.API, "api");
        a.put(LogDomain.NETWORK, "network");
        a.put(LogDomain.LOGGER, "log");
        a.put(LogDomain.FRAMEWORK, "framework");
        a.put(LogDomain.WIDGET, "widget");
        a.put(LogDomain.UTIL, "util");
        a.put(LogDomain.BROWSER, "browser");
        a.put(LogDomain.CONFIG_CHECK, "configcheck");
        a.put(LogDomain.DATA_COLLECTOR, "datacollector");
        a.put(LogDomain.VAST, AppodealNetworks.VAST);
        a.put(LogDomain.INTERSTITIAL, VideoType.INTERSTITIAL);
        a.put(LogDomain.RICH_MEDIA, "richmedia");
        a.put(LogDomain.RESOURCE_LOADER, "resourceloader");
        a.put(LogDomain.MRAID, AppodealNetworks.MRAID);
        a.put(LogDomain.UNIFIED_BIDDING, "ub");
    }

    LoggerImpl(@NonNull Environment environment) {
        this.f = (Environment) Objects.requireNonNull(environment, "Parameter environment cannot be null for LoggerImpl::new");
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull LogWriter logWriter) {
        Objects.requireNonNull(logWriter);
        this.d.add(logWriter);
    }

    public final void log(@NonNull LogLevel logLevel, @NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        a(logLevel, logDomain, null, str, objArr);
    }

    public final void log(@NonNull LogLevel logLevel, @NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        a(logLevel, logDomain, th, str, objArr);
    }

    public final void debug(@NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        a(LogLevel.DEBUG, logDomain, null, str, objArr);
    }

    public final void debug(@NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        a(LogLevel.DEBUG, logDomain, th, str, objArr);
    }

    public final void info(@NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        a(LogLevel.INFO, logDomain, null, str, objArr);
    }

    public final void info(@NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        a(LogLevel.INFO, logDomain, th, str, objArr);
    }

    public final void warning(@NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        a(LogLevel.WARNING, logDomain, null, str, objArr);
    }

    public final void warning(@NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        a(LogLevel.WARNING, logDomain, th, str, objArr);
    }

    public final void error(@NonNull LogDomain logDomain, @NonNull String str, @Nullable Object... objArr) {
        a(LogLevel.ERROR, logDomain, null, str, objArr);
    }

    public final void error(@NonNull LogDomain logDomain, @NonNull Throwable th, @NonNull String str, @Nullable Object... objArr) {
        a(LogLevel.ERROR, logDomain, th, str, objArr);
    }

    public final void setExplicitOneShotTag(@NonNull String str) {
        Objects.requireNonNull(str);
        this.e.set(str);
    }

    private void a(@NonNull LogLevel logLevel, @NonNull LogDomain logDomain, @Nullable Throwable th, @NonNull String str, @Nullable Object... objArr) {
        String str2;
        Objects.requireNonNull(logLevel);
        Objects.requireNonNull(logDomain);
        Objects.requireNonNull(str);
        String str3 = "SmaatoSDK: ";
        String a2 = a(logDomain);
        if (this.f == Environment.DEBUG) {
            StringBuilder sb = new StringBuilder();
            sb.append(a2);
            sb.append(a());
            sb.append(": ");
            a2 = sb.toString();
            b(logDomain);
        }
        boolean z = false;
        Iterator it = this.d.iterator();
        while (true) {
            if (it.hasNext()) {
                if (((LogWriter) it.next()).isLoggable(logLevel)) {
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (z) {
            if (str.length() != 0) {
                if (objArr != null && objArr.length > 0) {
                    str = a(str, objArr);
                }
                str2 = str;
                if (th != null) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append("\n");
                    sb2.append(a(th));
                    str2 = sb2.toString();
                }
            } else if (th != null) {
                str2 = a(th);
            } else {
                return;
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append(a2);
            sb3.append(str2);
            a(logLevel, sb3.toString(), str3);
        }
    }

    @VisibleForTesting
    @NonNull
    private static String a(@NonNull LogDomain logDomain) {
        StringBuilder sb = new StringBuilder();
        sb.append(logDomain.name());
        sb.append(": ");
        return sb.toString();
    }

    private void a(@NonNull LogLevel logLevel, @NonNull String str, @NonNull String str2) {
        for (LogWriter logWriter : this.d) {
            if (logWriter.isLoggable(logLevel)) {
                logWriter.log(logLevel, str2, str);
            }
        }
    }

    @Nullable
    private String a() {
        String str = (String) this.e.get();
        if (str != null) {
            this.e.remove();
            return str;
        }
        StackTraceElement b2 = b();
        if (b2 != null) {
            return a(b2);
        }
        return b;
    }

    @Nullable
    private StackTraceElement b() {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        if (stackTrace.length > 4) {
            return stackTrace[4];
        }
        error(LogDomain.LOGGER, new IllegalStateException("Not enough stacktrace elements: might be a proguard issue"), "Synthetic stack trace", new Object[0]);
        return null;
    }

    @NonNull
    private String a(@NonNull StackTraceElement stackTraceElement) {
        Objects.requireNonNull(stackTraceElement);
        String b2 = b(stackTraceElement);
        return b2.substring(b2.lastIndexOf(46) + 1);
    }

    @NonNull
    private static String b(@NonNull StackTraceElement stackTraceElement) {
        Objects.requireNonNull(stackTraceElement);
        String className = stackTraceElement.getClassName();
        Matcher matcher = c.matcher(className);
        return matcher.find() ? matcher.replaceAll("") : className;
    }

    @NonNull
    private static String a(@NonNull String str, @Nullable Object[] objArr) {
        Objects.requireNonNull(str);
        return String.format(str, objArr);
    }

    @VisibleForTesting
    private static String a(@NonNull Throwable th) {
        Objects.requireNonNull(th);
        StringWriter stringWriter = new StringWriter(256);
        PrintWriter printWriter = new PrintWriter(stringWriter, false);
        th.printStackTrace(printWriter);
        printWriter.flush();
        return stringWriter.toString();
    }

    private void b(@NonNull LogDomain logDomain) {
        StackTraceElement b2 = b();
        if (b2 != null) {
            String b3 = b(b2);
            a(logDomain, b3.substring(0, b3.lastIndexOf(46)));
        }
    }

    private void a(@NonNull LogDomain logDomain, @NonNull String str) {
        String[] split = str.split("\\.");
        int length = split.length;
        int i = 0;
        while (i < length) {
            String str2 = split[i];
            String str3 = (String) a.get(logDomain);
            if (str3 == null) {
                StringBuilder sb = new StringBuilder("Unknown LogDomain (");
                sb.append(logDomain);
                sb.append(") is not found in LOG_DOMAIN_TO_PACKAGE_NAME_MAP");
                String sb2 = sb.toString();
                LogLevel logLevel = LogLevel.ERROR;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(a(LogDomain.LOGGER));
                sb3.append(sb2);
                a(logLevel, sb3.toString(), "SmaatoSDK: ");
                return;
            } else if (!str3.equals(str2)) {
                i++;
            } else {
                return;
            }
        }
        StringBuilder sb4 = new StringBuilder("LogDomain = ");
        sb4.append(logDomain.name());
        sb4.append(" was not found in a caller classpath: ");
        sb4.append(str);
        sb4.append(". Looks like an inappropriate LogDomain is used.");
        String sb5 = sb4.toString();
        LogLevel logLevel2 = LogLevel.ERROR;
        StringBuilder sb6 = new StringBuilder();
        sb6.append(a(LogDomain.LOGGER));
        sb6.append(sb5);
        a(logLevel2, sb6.toString(), "SmaatoSDK: ");
    }
}
