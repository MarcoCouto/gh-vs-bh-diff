package com.smaato.sdk.core.log;

import android.os.Build.VERSION;
import android.util.Log;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

class ConsoleLogWriter extends LogWriter {
    ConsoleLogWriter(@NonNull LogLevel logLevel) {
        super(logLevel);
    }

    /* access modifiers changed from: protected */
    public void log(@NonNull LogLevel logLevel, @NonNull String str, @NonNull String str2) {
        int i;
        Objects.requireNonNull(logLevel);
        Objects.requireNonNull(str);
        Objects.requireNonNull(str2);
        Objects.requireNonNull(str);
        int i2 = 0;
        if (VERSION.SDK_INT < 24 && str.length() > 23) {
            str = str.substring(0, 23);
        }
        int length = str2.length();
        while (i2 < length) {
            int indexOf = str2.indexOf(10, i2);
            if (indexOf == -1) {
                indexOf = length;
            }
            while (true) {
                int min = Math.min(indexOf, i2 + 4000);
                String substring = str2.substring(i2, min);
                switch (logLevel) {
                    case DEBUG:
                        i = 3;
                        break;
                    case INFO:
                        i = 4;
                        break;
                    case WARNING:
                        i = 5;
                        break;
                    case ERROR:
                        i = 6;
                        break;
                    default:
                        StringBuilder sb = new StringBuilder("Unknown level: ");
                        sb.append(logLevel);
                        throw new IllegalArgumentException(sb.toString());
                }
                Log.println(i, str, substring);
                if (min >= indexOf) {
                    i2 = min + 1;
                } else {
                    i2 = min;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isLoggable(@NonNull LogLevel logLevel) {
        Objects.requireNonNull(logLevel);
        return logLevel.ordinal() >= getLogLevel().ordinal();
    }
}
