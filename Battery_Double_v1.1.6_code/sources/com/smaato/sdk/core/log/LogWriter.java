package com.smaato.sdk.core.log;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

abstract class LogWriter {
    private final LogLevel a;

    /* access modifiers changed from: protected */
    public abstract boolean isLoggable(@NonNull LogLevel logLevel);

    /* access modifiers changed from: protected */
    public abstract void log(@NonNull LogLevel logLevel, @NonNull String str, @NonNull String str2);

    LogWriter(@NonNull LogLevel logLevel) {
        Objects.requireNonNull(logLevel);
        this.a = logLevel;
    }

    /* access modifiers changed from: protected */
    public final LogLevel getLogLevel() {
        return this.a;
    }
}
