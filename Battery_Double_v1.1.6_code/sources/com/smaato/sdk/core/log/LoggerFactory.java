package com.smaato.sdk.core.log;

import android.util.Log;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

final class LoggerFactory {
    public static final LogLevel DEFAULT_CONSOLE_LOG_LEVEL = LogLevel.WARNING;
    private static volatile Logger a;

    private LoggerFactory() {
    }

    public static void initializeLogger(@NonNull LogLevel logLevel) {
        Objects.requireNonNull(logLevel);
        if (a == null) {
            synchronized (LoggerFactory.class) {
                if (a == null) {
                    LoggerImpl loggerImpl = new LoggerImpl(Environment.RELEASE);
                    loggerImpl.a((LogWriter) new ConsoleLogWriter(logLevel));
                    a = loggerImpl;
                }
            }
        }
    }

    @NonNull
    public static Logger getLogger() {
        if (a == null) {
            synchronized (LoggerFactory.class) {
                if (a == null) {
                    Log.e(LoggerFactory.class.getName(), "Logger was not initialized! Going to initialize with a default console log level");
                    initializeLogger(DEFAULT_CONSOLE_LOG_LEVEL);
                }
            }
        }
        return a;
    }
}
