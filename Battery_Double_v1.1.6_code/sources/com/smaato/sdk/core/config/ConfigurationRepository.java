package com.smaato.sdk.core.config;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.util.Objects;
import java.util.Collections;
import java.util.Map;

public final class ConfigurationRepository {
    @NonNull
    private final Map<String, Configuration> a;
    @NonNull
    private final Configuration b;

    public ConfigurationRepository(@NonNull Map<String, Configuration> map, @NonNull Configuration configuration) {
        this.a = Collections.synchronizedMap((Map) Objects.requireNonNull(map));
        this.b = (Configuration) Objects.requireNonNull(configuration);
    }

    @NonNull
    public final Configuration get() {
        Configuration configuration = (Configuration) this.a.get(SmaatoSdk.getPublisherId());
        return configuration == null ? this.b : configuration;
    }
}
