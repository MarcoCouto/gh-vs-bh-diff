package com.smaato.sdk.core.config;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.util.Objects;

public final class Configuration {
    public final int cachingCapacity;
    @NonNull
    public final State impressionState;

    public Configuration(int i, @NonNull State state) {
        this.cachingCapacity = i;
        this.impressionState = (State) Objects.requireNonNull(state);
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Configuration configuration = (Configuration) obj;
        return this.cachingCapacity == configuration.cachingCapacity && this.impressionState == configuration.impressionState;
    }

    public final int hashCode() {
        return (this.cachingCapacity * 31) + this.impressionState.hashCode();
    }
}
