package com.smaato.sdk.core.webview;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;

public final class DiWebViewLayer {
    private DiWebViewLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiWebViewLayer$TymlGrpPzLVOHhqcfB6P6ei4Ktk.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(BaseWebViewClient.class, $$Lambda$DiWebViewLayer$B7Bei6SB_nKJte6AyZWG8PlcHk.INSTANCE);
        diRegistry.registerFactory(BaseWebChromeClient.class, $$Lambda$DiWebViewLayer$ARLT6NE0mIeaAeLSVOjpVEBSVlc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BaseWebViewClient b(DiConstructor diConstructor) {
        return new BaseWebViewClient();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BaseWebChromeClient a(DiConstructor diConstructor) {
        return new BaseWebChromeClient();
    }

    @NonNull
    public static BaseWebViewClient getBaseWebViewClientFrom(@NonNull DiConstructor diConstructor) {
        return (BaseWebViewClient) diConstructor.get(BaseWebViewClient.class);
    }

    @NonNull
    public static BaseWebChromeClient getBaseWebChromeClientFrom(@NonNull DiConstructor diConstructor) {
        return (BaseWebChromeClient) diConstructor.get(BaseWebChromeClient.class);
    }
}
