package com.smaato.sdk.core.webview;

import android.os.Build.VERSION;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.util.Objects;

public final class WebViewHelperUtil {
    private WebViewHelperUtil() {
    }

    @VisibleForTesting
    static void a(@NonNull WebView webView) {
        Objects.requireNonNull(webView, "Parameter webView cannot be null for WebViewHelperUtil::destroyWebViewSafely");
        ViewGroup viewGroup = (ViewGroup) webView.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(webView);
        }
        webView.removeAllViews();
        webView.destroy();
    }

    public static void resetAndDestroyWebViewSafely(@NonNull final WebView webView) {
        Objects.requireNonNull(webView);
        webView.stopLoading();
        BaseWebViewClient baseWebViewClient = new BaseWebViewClient();
        baseWebViewClient.setWebViewClientCallback(new StubWebViewClientCallback() {
            public final void onPageFinishedLoading(@NonNull String str) {
                if ("about:blank".equals(str)) {
                    Log.d(LogDomain.CORE.name(), "going to release web-view");
                    WebViewHelperUtil.a(webView);
                }
            }
        });
        webView.setWebViewClient(baseWebViewClient);
        webView.loadUrl("about:blank");
    }

    @NonNull
    public static String getCenterContentCss() {
        return VERSION.SDK_INT < 19 ? "display: -webkit-box;-webkit-box-align: center;-webkit-box-pack: center;" : "display: flex;align-items: center;justify-content: center;";
    }
}
