package com.smaato.sdk.core.webview;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.webview.BaseWebViewClient.WebViewClientCallback;

public class BaseWebViewClient extends WebViewClient {
    @Nullable
    private WebViewClientCallback a;

    public interface WebViewClientCallback {
        void onGeneralError(int i, @NonNull String str, @NonNull String str2);

        @TargetApi(23)
        void onHttpError(@NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse);

        void onPageFinishedLoading(@NonNull String str);

        void onPageStartedLoading(@NonNull String str);

        @TargetApi(26)
        void onRenderProcessGone();

        boolean shouldOverrideUrlLoading(@NonNull String str);
    }

    public void setWebViewClientCallback(@Nullable WebViewClientCallback webViewClientCallback) {
        this.a = webViewClientCallback;
    }

    public void onPageStarted(@NonNull WebView webView, @NonNull String str, @Nullable Bitmap bitmap) {
        Objects.onNotNull(this.a, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((WebViewClientCallback) obj).onPageStartedLoading(this.f$0);
            }
        });
    }

    public void onPageFinished(@NonNull WebView webView, @NonNull String str) {
        Objects.onNotNull(this.a, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((WebViewClientCallback) obj).onPageFinishedLoading(this.f$0);
            }
        });
    }

    public boolean shouldOverrideUrlLoading(@NonNull WebView webView, @NonNull String str) {
        return a(str);
    }

    @TargetApi(24)
    public boolean shouldOverrideUrlLoading(@NonNull WebView webView, @NonNull WebResourceRequest webResourceRequest) {
        return a(webResourceRequest.getUrl().toString());
    }

    private boolean a(@NonNull String str) {
        if (this.a == null) {
            return false;
        }
        return this.a.shouldOverrideUrlLoading(str);
    }

    @TargetApi(23)
    public void onReceivedHttpError(@NonNull WebView webView, @NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceResponse webResourceResponse) {
        Objects.onNotNull(this.a, new Consumer(webResourceRequest, webResourceResponse) {
            private final /* synthetic */ WebResourceRequest f$0;
            private final /* synthetic */ WebResourceResponse f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((WebViewClientCallback) obj).onHttpError(this.f$0, this.f$1);
            }
        });
    }

    public void onReceivedError(@NonNull WebView webView, int i, @NonNull String str, @NonNull String str2) {
        Objects.onNotNull(this.a, new Consumer(i, str, str2) {
            private final /* synthetic */ int f$0;
            private final /* synthetic */ String f$1;
            private final /* synthetic */ String f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                ((WebViewClientCallback) obj).onGeneralError(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    @TargetApi(23)
    public void onReceivedError(@NonNull WebView webView, @NonNull WebResourceRequest webResourceRequest, @NonNull WebResourceError webResourceError) {
        Objects.onNotNull(this.a, new Consumer(webResourceError, webResourceRequest) {
            private final /* synthetic */ WebResourceError f$0;
            private final /* synthetic */ WebResourceRequest f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((WebViewClientCallback) obj).onGeneralError(this.f$0.getErrorCode(), this.f$0.getDescription().toString(), this.f$1.getUrl().toString());
            }
        });
    }

    @TargetApi(26)
    public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
        Objects.onNotNull(this.a, $$Lambda$bSVuxij1dwzUrJgQd2IEdzwsD0.INSTANCE);
        return true;
    }
}
