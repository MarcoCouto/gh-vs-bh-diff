package com.smaato.sdk.core.webview;

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.webview.BaseWebChromeClient.WebChromeClientCallback;

public class BaseWebChromeClient extends WebChromeClient {
    @Nullable
    private WebChromeClientCallback a;

    public interface WebChromeClientCallback {
        void onProgressChanged(int i);
    }

    public void setWebChromeClientCallback(@Nullable WebChromeClientCallback webChromeClientCallback) {
        this.a = webChromeClientCallback;
    }

    public void onProgressChanged(@NonNull WebView webView, int i) {
        Objects.onNotNull(this.a, new Consumer(i) {
            private final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((WebChromeClientCallback) obj).onProgressChanged(this.f$0);
            }
        });
    }
}
