package com.smaato.sdk.core.webview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import androidx.annotation.NonNull;

public class BaseWebView extends WebView {
    public BaseWebView(Context context) {
        super(context);
        a();
    }

    public BaseWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public BaseWebView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    @TargetApi(21)
    public BaseWebView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        a();
    }

    private void a() {
        if (VERSION.SDK_INT < 18) {
            getSettings().setPluginState(PluginState.OFF);
        }
        b();
    }

    public void loadHtml(@NonNull String str) {
        loadDataWithBaseURL(null, str, "text/html;charset=utf-8", "UTF-8", null);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void b() {
        getSettings().setJavaScriptEnabled(true);
        c();
    }

    private void c() {
        getSettings().setAllowContentAccess(false);
        getSettings().setAllowFileAccess(false);
        getSettings().setAllowUniversalAccessFromFileURLs(false);
    }
}
