package com.smaato.sdk.core.appbgdetection;

import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector.Listener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;

public class AppBackgroundAwareHandler {
    /* access modifiers changed from: private */
    @NonNull
    public final Handler a;
    @NonNull
    private final AppBackgroundDetector b;
    @NonNull
    private Logger c;
    @Nullable
    private PausableAction d;
    @NonNull
    private final Listener e = new Listener() {
        /* access modifiers changed from: private */
        public /* synthetic */ void b() {
            AppBackgroundAwareHandler.c(AppBackgroundAwareHandler.this);
        }

        public void onAppEnteredInBackground() {
            Threads.ensureInvokedOnHandlerThread(AppBackgroundAwareHandler.this.a, new Runnable() {
                public final void run() {
                    AnonymousClass1.this.b();
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a() {
            AppBackgroundAwareHandler.b(AppBackgroundAwareHandler.this);
        }

        public void onAppEnteredInForeground() {
            Threads.ensureInvokedOnHandlerThread(AppBackgroundAwareHandler.this.a, new Runnable() {
                public final void run() {
                    AnonymousClass1.this.a();
                }
            });
        }
    };

    public AppBackgroundAwareHandler(@NonNull Logger logger, @NonNull Handler handler, @NonNull AppBackgroundDetector appBackgroundDetector) {
        this.c = (Logger) Objects.requireNonNull(logger);
        this.a = (Handler) Objects.requireNonNull(handler);
        this.b = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
    }

    public void postDelayed(@NonNull String str, @NonNull Runnable runnable, long j, @Nullable PauseUnpauseListener pauseUnpauseListener) {
        Objects.requireNonNull(str);
        if (j > 0) {
            Objects.requireNonNull(runnable);
            Handler handler = this.a;
            $$Lambda$AppBackgroundAwareHandler$vUzcr7eZzGeBNOJsXTKsnZBrHA r1 = new Runnable(str, runnable, j, pauseUnpauseListener) {
                private final /* synthetic */ String f$1;
                private final /* synthetic */ Runnable f$2;
                private final /* synthetic */ long f$3;
                private final /* synthetic */ PauseUnpauseListener f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r6;
                }

                public final void run() {
                    AppBackgroundAwareHandler.this.a(this.f$1, this.f$2, this.f$3, this.f$4);
                }
            };
            Threads.ensureInvokedOnHandlerThread(handler, r1);
            return;
        }
        StringBuilder sb = new StringBuilder("delay must be positive for ");
        sb.append(getClass().getSimpleName());
        sb.append("::postDelayed");
        throw new IllegalArgumentException(sb.toString());
    }

    public void stop() {
        Threads.ensureInvokedOnHandlerThread(this.a, new Runnable() {
            public final void run() {
                AppBackgroundAwareHandler.this.a();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Runnable runnable) {
        this.d = null;
        this.b.deleteListener(this.e);
        runnable.run();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        Threads.ensureHandlerThread(this.a);
        if (this.d != null) {
            this.b.deleteListener(this.e);
            this.a.removeCallbacks(this.d);
            this.d = null;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str, Runnable runnable, long j, PauseUnpauseListener pauseUnpauseListener) {
        Threads.ensureHandlerThread(this.a);
        if (this.d != null) {
            this.a.removeCallbacks(this.d);
            this.b.deleteListener(this.e);
            this.d = null;
        }
        PausableAction pausableAction = new PausableAction(str, this.a, new Runnable(runnable) {
            private final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                AppBackgroundAwareHandler.this.a(this.f$1);
            }
        }, j, pauseUnpauseListener);
        this.d = pausableAction;
        this.a.postDelayed(this.d, j);
        this.b.addListener(this.e, true);
    }

    static /* synthetic */ void b(AppBackgroundAwareHandler appBackgroundAwareHandler) {
        Threads.ensureHandlerThread(appBackgroundAwareHandler.a);
        if (appBackgroundAwareHandler.d != null && appBackgroundAwareHandler.d.a()) {
            appBackgroundAwareHandler.d.c();
            appBackgroundAwareHandler.c.info(LogDomain.CORE, "resumed %s", appBackgroundAwareHandler.d.name);
        }
    }

    static /* synthetic */ void c(AppBackgroundAwareHandler appBackgroundAwareHandler) {
        Threads.ensureHandlerThread(appBackgroundAwareHandler.a);
        if (appBackgroundAwareHandler.d != null && !appBackgroundAwareHandler.d.a()) {
            appBackgroundAwareHandler.d.b();
            appBackgroundAwareHandler.c.info(LogDomain.CORE, "paused %s", appBackgroundAwareHandler.d.name);
        }
    }
}
