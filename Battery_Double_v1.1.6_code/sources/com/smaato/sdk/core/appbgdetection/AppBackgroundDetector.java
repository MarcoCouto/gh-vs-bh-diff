package com.smaato.sdk.core.appbgdetection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.lifecycle.ProcessLifecycleOwner;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.Iterator;

public class AppBackgroundDetector {
    @NonNull
    private final ArrayList<Listener> a = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean b;

    public interface Listener {
        void onAppEnteredInBackground();

        void onAppEnteredInForeground();
    }

    public AppBackgroundDetector(@NonNull final Logger logger) {
        Objects.requireNonNull(logger, "logger must not be null for AppBackgroundDetector::new");
        ProcessLifecycleOwner.get().setListener(new com.smaato.sdk.core.lifecycle.ProcessLifecycleOwner.Listener() {
            public void onFirstActivityStarted() {
                logger.info(LogDomain.CORE, "app entered foreground", new Object[0]);
                AppBackgroundDetector.this.b = true;
                AppBackgroundDetector.b(AppBackgroundDetector.this, AppBackgroundDetector.this.b);
            }

            public void onLastActivityStopped() {
                logger.info(LogDomain.CORE, "app entered background", new Object[0]);
                AppBackgroundDetector.this.b = false;
                AppBackgroundDetector.b(AppBackgroundDetector.this, AppBackgroundDetector.this.b);
            }
        });
    }

    public synchronized boolean isAppInBackground() {
        return !this.b;
    }

    public synchronized void addListener(@NonNull Listener listener, boolean z) {
        Objects.requireNonNull(listener, "listener can not be null");
        if (!this.a.contains(listener)) {
            this.a.add(listener);
            if (z) {
                a(listener, this.b);
            }
        }
    }

    private static void a(@NonNull Listener listener, boolean z) {
        if (z) {
            listener.onAppEnteredInForeground();
        } else {
            listener.onAppEnteredInBackground();
        }
    }

    public synchronized void deleteListener(@Nullable Listener listener) {
        this.a.remove(listener);
    }

    static /* synthetic */ void b(AppBackgroundDetector appBackgroundDetector, boolean z) {
        Iterator it = new ArrayList(appBackgroundDetector.a).iterator();
        while (it.hasNext()) {
            a((Listener) it.next(), z);
        }
    }
}
