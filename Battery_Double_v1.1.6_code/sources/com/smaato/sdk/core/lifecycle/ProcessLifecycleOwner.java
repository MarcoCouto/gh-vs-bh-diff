package com.smaato.sdk.core.lifecycle;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.ActivityLifecycleCallbacksAdapter;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;

public final class ProcessLifecycleOwner {
    private static final ProcessLifecycleOwner a = new ProcessLifecycleOwner();
    @NonNull
    private final Runnable b = new Runnable() {
        public final void run() {
            ProcessLifecycleOwner.this.b();
        }
    };
    @Nullable
    private Handler c;
    @Nullable
    private Listener d;
    private int e = 0;
    private int f = 0;
    private boolean g = true;
    private boolean h = true;
    private boolean i;

    public interface Listener {
        void onFirstActivityStarted();

        void onLastActivityStopped();
    }

    private ProcessLifecycleOwner() {
    }

    @NonNull
    public static ProcessLifecycleOwner get() {
        return a;
    }

    static void a(@NonNull Context context, @NonNull Handler handler) {
        ProcessLifecycleOwner processLifecycleOwner = a;
        processLifecycleOwner.c = handler;
        if (context.getApplicationContext() instanceof Application) {
            ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacksAdapter() {
                public void onActivityStarted(@NonNull Activity activity) {
                    ProcessLifecycleOwner.a(ProcessLifecycleOwner.this);
                }

                public void onActivityResumed(@NonNull Activity activity) {
                    ProcessLifecycleOwner.b(ProcessLifecycleOwner.this);
                }

                public void onActivityPaused(Activity activity) {
                    ProcessLifecycleOwner.c(ProcessLifecycleOwner.this);
                }

                public void onActivityStopped(Activity activity) {
                    ProcessLifecycleOwner.d(ProcessLifecycleOwner.this);
                }
            });
        }
    }

    public final void setListener(@NonNull Listener listener) {
        this.d = listener;
        if (this.i) {
            listener.onFirstActivityStarted();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Handler handler) {
        handler.removeCallbacks(this.b);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Handler handler) {
        handler.postDelayed(this.b, 700);
    }

    private void a() {
        if (this.e == 0 && this.g) {
            Objects.onNotNull(this.d, $$Lambda$Z7lSATaGlgWdfsCunN3IqpaFE0g.INSTANCE);
            this.h = true;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        if (this.f == 0) {
            this.g = true;
        }
        a();
    }

    static /* synthetic */ void a(ProcessLifecycleOwner processLifecycleOwner) {
        processLifecycleOwner.e++;
        if (processLifecycleOwner.e == 1 && processLifecycleOwner.h) {
            Objects.onNotNull(processLifecycleOwner.d, $$Lambda$HwtApuc3QUgaBQWQZ1ml2AI8KI.INSTANCE);
            processLifecycleOwner.i = true;
            processLifecycleOwner.h = false;
        }
    }

    static /* synthetic */ void b(ProcessLifecycleOwner processLifecycleOwner) {
        processLifecycleOwner.f++;
        if (processLifecycleOwner.f == 1) {
            if (processLifecycleOwner.g) {
                processLifecycleOwner.g = false;
                return;
            }
            Objects.onNotNull(processLifecycleOwner.c, new Consumer() {
                public final void accept(Object obj) {
                    ProcessLifecycleOwner.this.b((Handler) obj);
                }
            });
        }
    }

    static /* synthetic */ void c(ProcessLifecycleOwner processLifecycleOwner) {
        processLifecycleOwner.f--;
        if (processLifecycleOwner.f == 0) {
            Objects.onNotNull(processLifecycleOwner.c, new Consumer() {
                public final void accept(Object obj) {
                    ProcessLifecycleOwner.this.a((Handler) obj);
                }
            });
        }
    }

    static /* synthetic */ void d(ProcessLifecycleOwner processLifecycleOwner) {
        processLifecycleOwner.e--;
        processLifecycleOwner.a();
    }
}
