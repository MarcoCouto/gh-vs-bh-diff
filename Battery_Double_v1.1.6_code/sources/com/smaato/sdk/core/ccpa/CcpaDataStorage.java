package com.smaato.sdk.core.ccpa;

import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public final class CcpaDataStorage {
    @NonNull
    private final SharedPreferences a;

    public CcpaDataStorage(@NonNull SharedPreferences sharedPreferences) {
        this.a = (SharedPreferences) Objects.requireNonNull(sharedPreferences);
    }

    @NonNull
    public final String getUsPrivacyString() {
        return this.a.getString("IABUSPrivacy_String", "");
    }
}
