package com.smaato.sdk.interstitial;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.repository.AdRequestParams;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.diinjection.Inject;

public final class Interstitial {
    private static final String a = "Interstitial";
    @Nullable
    private static String b;
    @Nullable
    private static String c;
    @Nullable
    private static String d;
    @Inject
    @Nullable
    private static volatile f e;

    private Interstitial() {
    }

    public static void loadAd(@NonNull String str, @NonNull EventListener eventListener) {
        loadAd(str, eventListener, null);
    }

    public static void loadAd(@NonNull String str, @NonNull EventListener eventListener, @Nullable AdRequestParams adRequestParams) {
        f a2 = a();
        if (a2 == null) {
            Log.e(a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
            return;
        }
        if (!SmaatoSdk.isGPSEnabled()) {
            Log.w(a, "Usage of the GPS coordinates for advertising purposes is disabled. You can change that by setting setGPSLocation to TRUE.");
        }
        if (eventListener == null) {
            Log.e(a, "Failed to proceed with Interstitial::loadAd. Missing required parameter: eventListener");
            return;
        }
        String publisherId = SmaatoSdk.getPublisherId();
        if (TextUtils.isEmpty(publisherId)) {
            Log.e(a, "Failed to proceed with Interstitial::loadAd. Missing required parameter: publisherId");
            Threads.runOnUi(new Runnable(publisherId, str) {
                private final /* synthetic */ String f$1;
                private final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    EventListener.this.onAdFailedToLoad(new InterstitialRequestError(InterstitialError.INVALID_REQUEST, this.f$1, this.f$2));
                }
            });
        } else if (TextUtils.isEmpty(str)) {
            Log.e(a, "Failed to proceed with Interstitial::loadAd. Missing required parameter: adSpaceId");
            Threads.runOnUi(new Runnable(publisherId, str) {
                private final /* synthetic */ String f$1;
                private final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    EventListener.this.onAdFailedToLoad(new InterstitialRequestError(InterstitialError.INVALID_REQUEST, this.f$1, this.f$2));
                }
            });
        } else {
            a2.a(publisherId, str, eventListener, AdFormat.INTERSTITIAL, b, c, d, adRequestParams);
        }
    }

    public static void setMediationNetworkName(@Nullable String str) {
        b = str;
    }

    public static void setMediationNetworkSDKVersion(@Nullable String str) {
        c = str;
    }

    public static void setMediationAdapterVersion(@Nullable String str) {
        d = str;
    }

    @Nullable
    public static KeyValuePairs getKeyValuePairs() {
        f a2 = a();
        if (a2 != null) {
            return a2.a();
        }
        Log.e(a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
        return null;
    }

    public static void setKeyValuePairs(@Nullable KeyValuePairs keyValuePairs) {
        f a2 = a();
        if (a2 == null) {
            Log.e(a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
        } else {
            a2.a(keyValuePairs);
        }
    }

    @Nullable
    private static f a() {
        if (e == null) {
            synchronized (f.class) {
                if (e == null) {
                    AndroidsInjector.injectStatic(Interstitial.class);
                }
            }
        }
        return e;
    }
}
