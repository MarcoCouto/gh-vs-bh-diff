package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.repository.CoreAdTypeStrategy;
import com.smaato.sdk.core.repository.RawDataStrategy;
import java.util.Arrays;

final class c extends CoreAdTypeStrategy {
    c(@NonNull String str, @NonNull String str2, @NonNull RawDataStrategy rawDataStrategy) {
        super(str, str2, rawDataStrategy);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final Iterable getParams() {
        return Arrays.asList(new String[]{this.publisherId, this.adSpaceId});
    }

    @NonNull
    public final Class<? extends AdPresenter> getAdPresenterClass() {
        return InterstitialAdPresenter.class;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (!this.publisherId.equals(cVar.publisherId)) {
            return false;
        }
        return this.adSpaceId.equals(cVar.adSpaceId);
    }

    public final int hashCode() {
        return (this.publisherId.hashCode() * 31) + this.adSpaceId.hashCode();
    }
}
