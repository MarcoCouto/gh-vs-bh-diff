package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.CollectionUtils;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.NullableFunction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class InterstitialServerAdFormatResolvingFunction implements NullableFunction<Collection<AdFormat>, AdFormat> {
    @NonNull
    private static final List<AdFormat> a = Lists.of((T[]) new AdFormat[]{AdFormat.STATIC_IMAGE, AdFormat.RICH_MEDIA});
    @NonNull
    private static final List<AdFormat> b = Lists.of((T[]) new AdFormat[]{AdFormat.STATIC_IMAGE, AdFormat.VIDEO});
    private final List<AdFormat> c;

    public InterstitialServerAdFormatResolvingFunction(@NonNull Collection<AdFormat> collection) {
        this.c = Lists.toImmutableList((Collection) Objects.requireNonNull(collection));
    }

    @Nullable
    public AdFormat apply(@Nullable Collection<AdFormat> collection) {
        if (collection == null || collection.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.c);
        if (!arrayList.retainAll(collection)) {
            return AdFormat.INTERSTITIAL;
        }
        if (arrayList.size() == 1) {
            return (AdFormat) arrayList.get(0);
        }
        if (CollectionUtils.equalsByElements(a, arrayList)) {
            return AdFormat.DISPLAY;
        }
        if (CollectionUtils.equalsByElements(b, arrayList)) {
            return AdFormat.VIDEO;
        }
        return null;
    }
}
