package com.smaato.sdk.interstitial;

import android.app.Application;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.FullscreenAdDimensionMapper;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.ad.UserInfoSupplier;
import com.smaato.sdk.core.api.AdRequestExtrasProvider;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.core.api.VideoTypeAdRequestExtrasProvider;
import com.smaato.sdk.core.config.Configuration;
import com.smaato.sdk.core.config.ConfigurationRepository;
import com.smaato.sdk.core.config.DiConfiguration;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.repository.AdLoadersRegistry;
import com.smaato.sdk.core.repository.AdPresenterCache;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.DiAdRepository.Provider;
import com.smaato.sdk.core.repository.MultipleAdLoadersRegistry;
import com.smaato.sdk.core.repository.MultipleAdPresenterCache;
import com.smaato.sdk.core.repository.RawDataStrategyFactory;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.Timer;
import java.util.HashMap;
import java.util.WeakHashMap;

public final class DiInterstitial {
    private static final State a = State.IMPRESSED;

    private DiInterstitial() {
    }

    @NonNull
    public static DiRegistry createRegistry(@NonNull String str) {
        Objects.requireNonNull(str);
        return DiRegistry.of(new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                DiInterstitial.a(this.f$0, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(String str, DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(f.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiInterstitial.c(this.f$0, diConstructor);
            }
        });
        diRegistry.registerSingletonFactory(str, SharedKeyValuePairsHolder.class, $$Lambda$DiInterstitial$3w76Lb2Pg_j5beASo1v5_LF94.INSTANCE);
        diRegistry.registerSingletonFactory(e.class, $$Lambda$DiInterstitial$ZW_wSdkU4_F7D6DnqSJhBW9rcs.INSTANCE);
        diRegistry.registerSingletonFactory(b.class, $$Lambda$DiInterstitial$1HxhXbieb0062hf8BFwoHlVdQrE.INSTANCE);
        diRegistry.registerFactory(str, AdRequestExtrasProvider.class, $$Lambda$DiInterstitial$Ov0xO_YRXj2HlGmd7arB5_n90.INSTANCE);
        diRegistry.registerSingletonFactory(str, AdPresenterCache.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiInterstitial.b(this.f$0, diConstructor);
            }
        });
        diRegistry.registerSingletonFactory(str, AdLoadersRegistry.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiInterstitial.a(this.f$0, diConstructor);
            }
        });
        diRegistry.registerSingletonFactory(str, ConfigurationRepository.class, $$Lambda$DiInterstitial$K03kdlYKw11PPpuVh6NWD3eso.INSTANCE);
        diRegistry.registerFactory(new AdPresenterNameShaper().shapeName(AdFormat.INTERSTITIAL, InterstitialAdPresenter.class), Timer.class, $$Lambda$DiInterstitial$scOZNuV8bbvntOuG2WpDzNlYohI.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f c(String str, DiConstructor diConstructor) {
        f fVar = new f((AdRepository) ((Provider) diConstructor.get(Provider.class)).apply(str), (e) diConstructor.get(e.class), (Application) diConstructor.get(Application.class), (UserInfoSupplier) diConstructor.get(UserInfoSupplier.class), (SharedKeyValuePairsHolder) diConstructor.get(str, SharedKeyValuePairsHolder.class), (FullscreenAdDimensionMapper) diConstructor.get(FullscreenAdDimensionMapper.class), (RawDataStrategyFactory) diConstructor.get(RawDataStrategyFactory.class));
        return fVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SharedKeyValuePairsHolder f(DiConstructor diConstructor) {
        return new SharedKeyValuePairsHolder();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ e e(DiConstructor diConstructor) {
        e eVar = new e(new HashMap(), new HashMap(), new WeakHashMap(), (b) diConstructor.get(b.class), $$Lambda$Wd7bUiJ6rzarh6euqzITU7sSM.INSTANCE);
        return eVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b d(DiConstructor diConstructor) {
        return new b();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdRequestExtrasProvider c(DiConstructor diConstructor) {
        return new VideoTypeAdRequestExtrasProvider(VideoType.INTERSTITIAL);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterCache b(String str, DiConstructor diConstructor) {
        return new MultipleAdPresenterCache((ConfigurationRepository) diConstructor.get(str, ConfigurationRepository.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoadersRegistry a(String str, DiConstructor diConstructor) {
        return new MultipleAdLoadersRegistry((ConfigurationRepository) diConstructor.get(str, ConfigurationRepository.class), DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConfigurationRepository b(DiConstructor diConstructor) {
        return (ConfigurationRepository) ((DiConfiguration.Provider) diConstructor.get(DiConfiguration.Provider.class)).apply(new Configuration(1, a));
    }
}
