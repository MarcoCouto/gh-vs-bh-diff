package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.NullableSupplier;
import java.util.HashMap;
import java.util.UUID;

final class b {
    @NonNull
    private HashMap<UUID, InterstitialAdPresenter> a = new HashMap<>();

    b() {
    }

    /* access modifiers changed from: private */
    public /* synthetic */ InterstitialAdPresenter b(UUID uuid, InterstitialAdPresenter interstitialAdPresenter) {
        return (InterstitialAdPresenter) this.a.put(uuid, interstitialAdPresenter);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull UUID uuid, @NonNull InterstitialAdPresenter interstitialAdPresenter) {
        Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier(uuid, interstitialAdPresenter) {
            private final /* synthetic */ UUID f$1;
            private final /* synthetic */ InterstitialAdPresenter f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object get() {
                return b.this.b(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ InterstitialAdPresenter d(UUID uuid) {
        return (InterstitialAdPresenter) this.a.get(uuid);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final InterstitialAdPresenter a(@NonNull UUID uuid) {
        return (InterstitialAdPresenter) Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier(uuid) {
            private final /* synthetic */ UUID f$1;

            {
                this.f$1 = r2;
            }

            public final Object get() {
                return b.this.d(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ InterstitialAdPresenter c(UUID uuid) {
        return (InterstitialAdPresenter) this.a.remove(uuid);
    }

    /* access modifiers changed from: 0000 */
    public final void b(@NonNull UUID uuid) {
        Threads.runOnUiBlocking((NullableSupplier<T>) new NullableSupplier(uuid) {
            private final /* synthetic */ UUID f$1;

            {
                this.f$1 = r2;
            }

            public final Object get() {
                return b.this.c(this.f$1);
            }
        });
    }
}
