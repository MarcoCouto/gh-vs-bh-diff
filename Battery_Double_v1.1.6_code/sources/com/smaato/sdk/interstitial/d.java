package com.smaato.sdk.interstitial;

import com.smaato.sdk.core.ad.AdLoader.Error;

final class d {

    /* renamed from: com.smaato.sdk.interstitial.d$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[Error.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(30:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|30) */
        /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            a[Error.NO_AD.ordinal()] = 1;
            a[Error.NO_MANDATORY_CACHE.ordinal()] = 2;
            a[Error.BAD_REQUEST.ordinal()] = 3;
            a[Error.PRESENTER_BUILDER_GENERIC.ordinal()] = 4;
            a[Error.INVALID_RESPONSE.ordinal()] = 5;
            a[Error.API.ordinal()] = 6;
            a[Error.CONFIGURATION_ERROR.ordinal()] = 7;
            a[Error.INTERNAL.ordinal()] = 8;
            a[Error.CANCELLED.ordinal()] = 9;
            a[Error.TTL_EXPIRED.ordinal()] = 10;
            a[Error.TOO_MANY_REQUESTS.ordinal()] = 11;
            a[Error.NETWORK.ordinal()] = 12;
            a[Error.NO_CONNECTION.ordinal()] = 13;
            a[Error.CREATIVE_RESOURCE_EXPIRED.ordinal()] = 14;
        }
    }
}
