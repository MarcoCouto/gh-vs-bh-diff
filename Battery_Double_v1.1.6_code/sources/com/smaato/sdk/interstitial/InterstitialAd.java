package com.smaato.sdk.interstitial;

import android.app.Activity;
import android.support.v4.view.ViewCompat;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class InterstitialAd {
    @ColorInt
    protected int backgroundColor = ViewCompat.MEASURED_STATE_MASK;

    @NonNull
    public abstract String getAdSpaceId();

    @Nullable
    public abstract String getCreativeId();

    @NonNull
    public abstract String getSessionId();

    public abstract boolean isAvailableForPresentation();

    /* access modifiers changed from: protected */
    public abstract void showAdInternal(@NonNull Activity activity);

    public final void showAd(@NonNull Activity activity) {
        showAdInternal(activity);
    }

    public void setBackgroundColor(@ColorInt int i) {
        this.backgroundColor = i;
    }
}
