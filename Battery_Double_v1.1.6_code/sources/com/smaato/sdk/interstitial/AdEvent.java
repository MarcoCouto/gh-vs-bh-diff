package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public class AdEvent {
    @NonNull
    private final a a;
    @NonNull
    private final Object b;

    enum a {
        LOADED,
        OPEN,
        IMPRESS,
        CLICK,
        CLOSE,
        ERROR,
        TTL_EXPIRED
    }

    AdEvent(@NonNull a aVar, @NonNull Object obj) {
        this.a = (a) Objects.requireNonNull(aVar);
        this.b = Objects.requireNonNull(obj);
    }

    @NonNull
    public a getType() {
        return this.a;
    }

    @NonNull
    public Object getContent() {
        return this.b;
    }
}
