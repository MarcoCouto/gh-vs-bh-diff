package com.smaato.sdk.interstitial;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdInteractor.TtlListener;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.util.Intents;
import com.smaato.sdk.core.util.Objects;
import java.util.UUID;

final class a extends InterstitialAd {
    @NonNull
    private final UUID a;
    @NonNull
    private final String b;
    @NonNull
    private final InterstitialAdPresenter c;
    @NonNull
    private final TtlListener d;
    @NonNull
    private final EventListener e;
    @NonNull
    private final b f;

    a(@NonNull UUID uuid, @NonNull String str, @NonNull InterstitialAdPresenter interstitialAdPresenter, @NonNull b bVar, @NonNull EventListener eventListener) {
        this.a = (UUID) Objects.requireNonNull(uuid);
        this.b = (String) Objects.requireNonNull(str);
        this.c = (InterstitialAdPresenter) Objects.requireNonNull(interstitialAdPresenter);
        this.f = (b) Objects.requireNonNull(bVar);
        this.e = (EventListener) Objects.requireNonNull(eventListener);
        this.d = new TtlListener(eventListener) {
            private final /* synthetic */ EventListener f$1;

            {
                this.f$1 = r2;
            }

            public final void onTTLExpired(AdInteractor adInteractor) {
                a.this.a(this.f$1, adInteractor);
            }
        };
        interstitialAdPresenter.getAdInteractor().addTtlListener(this.d);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(EventListener eventListener, AdInteractor adInteractor) {
        eventListener.onAdTTLExpired(this);
    }

    /* access modifiers changed from: protected */
    public final void showAdInternal(@NonNull Activity activity) {
        if (this.c.isValid()) {
            this.f.a(this.a, this.c);
            Intents.startIntent(activity, InterstitialAdActivity.createIntent(activity, this.a, this.b, this.backgroundColor));
            return;
        }
        this.e.onAdError(this, InterstitialError.CREATIVE_RESOURCE_EXPIRED);
    }

    @NonNull
    public final String getSessionId() {
        return this.c.getSessionId();
    }

    @Nullable
    public final String getCreativeId() {
        return this.c.getCreativeId();
    }

    @NonNull
    public final String getAdSpaceId() {
        return this.c.getAdSpaceId();
    }

    public final boolean isAvailableForPresentation() {
        return this.c.isValid();
    }
}
