package com.smaato.sdk.interstitial;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.interstitial.-$$Lambda$DiInterstitial$3w76Lb2P-g_j-5beASo1v5_LF94 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiInterstitial$3w76Lb2Pg_j5beASo1v5_LF94 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiInterstitial$3w76Lb2Pg_j5beASo1v5_LF94 INSTANCE = new $$Lambda$DiInterstitial$3w76Lb2Pg_j5beASo1v5_LF94();

    private /* synthetic */ $$Lambda$DiInterstitial$3w76Lb2Pg_j5beASo1v5_LF94() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiInterstitial.f(diConstructor);
    }
}
