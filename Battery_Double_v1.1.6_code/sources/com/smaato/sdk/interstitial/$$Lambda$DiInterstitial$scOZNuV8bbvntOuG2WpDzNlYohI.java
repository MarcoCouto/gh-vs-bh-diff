package com.smaato.sdk.interstitial;

import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.util.notifier.TimerUtils;

/* renamed from: com.smaato.sdk.interstitial.-$$Lambda$DiInterstitial$scOZNuV8bbvntOuG2WpDzNlYohI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiInterstitial$scOZNuV8bbvntOuG2WpDzNlYohI implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiInterstitial$scOZNuV8bbvntOuG2WpDzNlYohI INSTANCE = new $$Lambda$DiInterstitial$scOZNuV8bbvntOuG2WpDzNlYohI();

    private /* synthetic */ $$Lambda$DiInterstitial$scOZNuV8bbvntOuG2WpDzNlYohI() {
    }

    public final Object get(DiConstructor diConstructor) {
        return TimerUtils.createSingleTimer(AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
    }
}
