package com.smaato.sdk.interstitial.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.api.AdRequestExtrasProvider;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.ModuleInterface;
import com.smaato.sdk.core.init.AdPresenterModuleInterfaceUtils;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.FunctionUtils;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.interstitial.DiInterstitial;
import com.smaato.sdk.interstitial.InterstitialAdActivity;
import com.smaato.sdk.interstitial.InterstitialServerAdFormatResolvingFunction;
import com.smaato.sdk.interstitial.ad.InterstitialAdLoaderPlugin;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

public class InterstitialModuleInterface implements ModuleInterface {
    @NonNull
    private static final List<AdFormat> a = Lists.toImmutableListOf(AdFormat.VIDEO, AdFormat.STATIC_IMAGE, AdFormat.RICH_MEDIA);
    @Nullable
    private volatile List<AdPresenterModuleInterface> b;
    private boolean c;
    @Nullable
    private String d;

    @NonNull
    public String moduleDiName() {
        return "InterstitialModuleInterface";
    }

    @NonNull
    public String version() {
        return "21.3.1";
    }

    public synchronized void init(@NonNull ClassLoader classLoader) {
        a((Iterable<AdPresenterModuleInterface>) ServiceLoader.load(AdPresenterModuleInterface.class, classLoader));
    }

    public boolean isInitialised() {
        return this.c;
    }

    @NonNull
    public Class<? extends AdPresenter> getSupportedAdPresenterInterface() {
        return InterstitialAdPresenter.class;
    }

    @VisibleForTesting
    private synchronized void a(@NonNull Iterable<AdPresenterModuleInterface> iterable) {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    this.b = AdPresenterModuleInterfaceUtils.getValidModuleInterfaces("21.3.1", iterable);
                    Map mapWithOrder = Maps.toMapWithOrder(a, FunctionUtils.identity(), new Function() {
                        public final Object apply(Object obj) {
                            return Boolean.valueOf(InterstitialModuleInterface.this.a((AdFormat) obj));
                        }
                    });
                    this.c = Lists.any(mapWithOrder.values(), $$Lambda$orGLThciAnXWGWqy4r8Z91E5vck.INSTANCE);
                    if (!this.c) {
                        this.d = String.format("In order to show ads of %s format at least one of %s modules should be added to your project configuration. Missing module(s): %s", new Object[]{AdFormat.INTERSTITIAL, a, Maps.filteredKeys(mapWithOrder, $$Lambda$InterstitialModuleInterface$hhVt4rGUo5IHM5MbsxKVS3RPCjg.INSTANCE)});
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean a(Boolean bool) {
        return !bool.booleanValue();
    }

    @NonNull
    private List<AdPresenterModuleInterface> a() {
        List<AdPresenterModuleInterface> list = this.b;
        if (list != null) {
            return list;
        }
        throw new IllegalStateException("init() method should have been called first for this module: smaato-sdk-interstitial");
    }

    public boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        if (adFormat != AdFormat.INTERSTITIAL) {
            return a(adFormat);
        }
        if (!this.c) {
            logger.error(LogDomain.FRAMEWORK, this.d, new Object[0]);
        }
        return this.c;
    }

    /* access modifiers changed from: private */
    public boolean a(@NonNull AdFormat adFormat) {
        return Lists.any(a(), new Predicate() {
            public final boolean test(Object obj) {
                return ((AdPresenterModuleInterface) obj).isFormatSupported(AdFormat.this, InterstitialAdPresenter.class);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ AdLoaderPlugin a(DiConstructor diConstructor) {
        InterstitialAdLoaderPlugin interstitialAdLoaderPlugin = new InterstitialAdLoaderPlugin(a(), new NullableFunction() {
            public final Object apply(Object obj) {
                return InterstitialModuleInterface.a(DiConstructor.this, (AdPresenterModuleInterface) obj);
            }
        }, new InterstitialServerAdFormatResolvingFunction(a), (AdRequestExtrasProvider) diConstructor.get(moduleDiName(), AdRequestExtrasProvider.class), a, this.d);
        return interstitialAdLoaderPlugin;
    }

    @NonNull
    public ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory() {
        return new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return InterstitialModuleInterface.this.a(diConstructor);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderPlugin a(DiConstructor diConstructor, AdPresenterModuleInterface adPresenterModuleInterface) {
        return (AdLoaderPlugin) DiAdLayer.tryGetOrNull(diConstructor, adPresenterModuleInterface.moduleDiName(), AdLoaderPlugin.class);
    }

    @Nullable
    public DiRegistry moduleDiRegistry() {
        return DiInterstitial.createRegistry(moduleDiName());
    }

    @NonNull
    public ExpectedManifestEntries getExpectedManifestEntries() {
        return new ExpectedManifestEntries(Collections.emptySet(), Sets.toImmutableSetOf(InterstitialAdActivity.class));
    }

    @NonNull
    public String toString() {
        StringBuilder sb = new StringBuilder("InterstitialModuleInterface{supportedFormat: ");
        sb.append(AdFormat.INTERSTITIAL);
        sb.append("}");
        return sb.toString();
    }
}
