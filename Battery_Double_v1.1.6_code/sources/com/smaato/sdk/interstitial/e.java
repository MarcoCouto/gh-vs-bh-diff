package com.smaato.sdk.interstitial;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.fi.Supplier;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;

class e {
    @NonNull
    private final Map<String, LinkedHashSet<AdEvent>> a;
    @NonNull
    private final Map<String, WeakReference<EventListener>> b;
    @NonNull
    private final WeakHashMap<EventListener, InterstitialAd> c;
    @NonNull
    private final b d;
    @NonNull
    private final Supplier<UUID> e;

    e(@NonNull Map<String, LinkedHashSet<AdEvent>> map, @NonNull Map<String, WeakReference<EventListener>> map2, @NonNull WeakHashMap<EventListener, InterstitialAd> weakHashMap, @NonNull b bVar, @NonNull Supplier<UUID> supplier) {
        this.a = (Map) Objects.requireNonNull(map);
        this.b = Collections.synchronizedMap((Map) Objects.requireNonNull(map2));
        this.c = (WeakHashMap) Objects.requireNonNull(weakHashMap);
        this.d = (b) Objects.requireNonNull(bVar);
        this.e = (Supplier) Objects.requireNonNull(supplier);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str, @NonNull a aVar) {
        a(str, new AdEvent(aVar, Whatever.INSTANCE));
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str, @NonNull AdEvent adEvent) {
        Threads.runOnUi(new Runnable(str, adEvent) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ AdEvent f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                e.this.b(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(String str, AdEvent adEvent) {
        if (b(str) != null) {
            LinkedHashSet linkedHashSet = (LinkedHashSet) this.a.get(str);
            if (linkedHashSet == null) {
                linkedHashSet = new LinkedHashSet();
                this.a.put(str, linkedHashSet);
            }
            linkedHashSet.add(adEvent);
            a(str);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull AdPresenter adPresenter, @NonNull String str) {
        if (adPresenter instanceof InterstitialAdPresenter) {
            Threads.runOnUi(new Runnable(str, (InterstitialAdPresenter) adPresenter) {
                private final /* synthetic */ String f$1;
                private final /* synthetic */ InterstitialAdPresenter f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    e.this.a(this.f$1, this.f$2);
                }
            });
            return;
        }
        adPresenter.releaseAccess();
        a(new InterstitialRequestError(InterstitialError.INTERNAL_ERROR, adPresenter.getPublisherId(), adPresenter.getAdSpaceId()), str);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str, InterstitialAdPresenter interstitialAdPresenter) {
        EventListener b2 = b(str);
        if (b2 != null) {
            this.b.remove(str);
            if (interstitialAdPresenter.isValid()) {
                UUID uuid = (UUID) this.e.get();
                String uuid2 = uuid.toString();
                a aVar = new a(uuid, uuid2, interstitialAdPresenter, this.d, b2);
                this.b.put(uuid2, new WeakReference(b2));
                this.c.put(b2, aVar);
                b2.onAdLoaded(aVar);
                return;
            }
            b2.onAdFailedToLoad(new InterstitialRequestError(InterstitialError.CREATIVE_RESOURCE_EXPIRED, interstitialAdPresenter.getPublisherId(), interstitialAdPresenter.getAdSpaceId()));
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull InterstitialRequestError interstitialRequestError, @NonNull String str) {
        Threads.runOnUi(new Runnable(str, interstitialRequestError) {
            private final /* synthetic */ String f$1;
            private final /* synthetic */ InterstitialRequestError f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                e.this.a(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str, InterstitialRequestError interstitialRequestError) {
        EventListener b2 = b(str);
        if (b2 != null) {
            this.b.remove(str);
            b2.onAdFailedToLoad(interstitialRequestError);
        }
    }

    private void a(@NonNull String str) {
        EventListener b2 = b(str);
        if (b2 != null) {
            InterstitialAd interstitialAd = (InterstitialAd) this.c.get(b2);
            if (interstitialAd != null) {
                Set set = (Set) this.a.get(str);
                if (set != null) {
                    Iterator it = new ArrayList(set).iterator();
                    while (it.hasNext()) {
                        AdEvent adEvent = (AdEvent) it.next();
                        set.remove(adEvent);
                        switch (adEvent.getType()) {
                            case OPEN:
                                b2.onAdOpened(interstitialAd);
                                break;
                            case CLICK:
                                b2.onAdClicked(interstitialAd);
                                break;
                            case CLOSE:
                                b2.onAdClosed(interstitialAd);
                                break;
                            case ERROR:
                                b2.onAdError(interstitialAd, (InterstitialError) adEvent.getContent());
                                break;
                            case IMPRESS:
                                b2.onAdImpression(interstitialAd);
                                break;
                            case TTL_EXPIRED:
                                b2.onAdTTLExpired(interstitialAd);
                                break;
                            default:
                                throw new IllegalArgumentException("Unexpected AdEvent");
                        }
                    }
                }
            }
        }
    }

    @Nullable
    private EventListener b(@NonNull String str) {
        WeakReference weakReference = (WeakReference) this.b.get(str);
        if (weakReference != null) {
            return (EventListener) weakReference.get();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull String str, @NonNull EventListener eventListener) {
        Objects.requireNonNull(str);
        Objects.requireNonNull(eventListener);
        this.b.put(str, new WeakReference(eventListener));
    }
}
