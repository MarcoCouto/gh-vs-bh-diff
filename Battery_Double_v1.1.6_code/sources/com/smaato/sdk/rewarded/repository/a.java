package com.smaato.sdk.rewarded.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.util.Objects;

final class a<T extends AdPresenter> {
    @NonNull
    private final T a;
    @NonNull
    private final Object b;

    a(@NonNull T t, @NonNull Object obj) {
        this.a = (AdPresenter) Objects.requireNonNull(t);
        this.b = Objects.requireNonNull(obj);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final T a() {
        return this.a;
    }
}
