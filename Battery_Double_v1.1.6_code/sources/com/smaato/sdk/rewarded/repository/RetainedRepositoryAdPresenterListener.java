package com.smaato.sdk.rewarded.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine.Listener;

public abstract class RetainedRepositoryAdPresenterListener implements Listener<State> {
    @NonNull
    private final RewardedAdPresenter a;

    /* access modifiers changed from: 0000 */
    public abstract void a(@NonNull RewardedAdPresenter rewardedAdPresenter);

    RetainedRepositoryAdPresenterListener(@NonNull RewardedAdPresenter rewardedAdPresenter) {
        this.a = (RewardedAdPresenter) Objects.requireNonNull(rewardedAdPresenter);
    }

    public void onStateChanged(@NonNull State state, @NonNull State state2, @Nullable Metadata metadata) {
        switch (state2) {
            case INIT:
            case CREATED:
            case ON_SCREEN:
            case CLICKED:
            case IMPRESSED:
                return;
            case TO_BE_DELETED:
                a(this.a);
                return;
            default:
                throw new IllegalArgumentException(String.format("Unexpected AdStateMachine.State: %s", new Object[]{state2}));
        }
    }
}
