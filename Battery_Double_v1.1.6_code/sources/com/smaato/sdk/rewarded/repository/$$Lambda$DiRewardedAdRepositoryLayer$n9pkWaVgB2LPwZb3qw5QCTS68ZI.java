package com.smaato.sdk.rewarded.repository;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.rewarded.repository.-$$Lambda$DiRewardedAdRepositoryLayer$n9pkWaVgB2LPwZb3qw5QCTS68ZI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiRewardedAdRepositoryLayer$n9pkWaVgB2LPwZb3qw5QCTS68ZI implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiRewardedAdRepositoryLayer$n9pkWaVgB2LPwZb3qw5QCTS68ZI INSTANCE = new $$Lambda$DiRewardedAdRepositoryLayer$n9pkWaVgB2LPwZb3qw5QCTS68ZI();

    private /* synthetic */ $$Lambda$DiRewardedAdRepositoryLayer$n9pkWaVgB2LPwZb3qw5QCTS68ZI() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newSingleThreadExecutor();
    }
}
