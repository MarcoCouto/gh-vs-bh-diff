package com.smaato.sdk.rewarded.repository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.util.Objects;
import java.util.Map;

public class RetainedAdPresenterRepository {
    @NonNull
    private final Map<String, a<RewardedAdPresenter>> a;

    RetainedAdPresenterRepository(@NonNull Map<String, a<RewardedAdPresenter>> map) {
        this.a = (Map) Objects.requireNonNull(map);
    }

    @Nullable
    public synchronized RewardedAdPresenter get(@NonNull String str) {
        a aVar = (a) this.a.get(str);
        if (aVar == null) {
            return null;
        }
        return (RewardedAdPresenter) aVar.a();
    }

    @NonNull
    public synchronized String put(@NonNull RewardedAdPresenter rewardedAdPresenter, @NonNull final String str) {
        AnonymousClass1 r0 = new RetainedRepositoryAdPresenterListener(rewardedAdPresenter) {
            public final void a(@NonNull RewardedAdPresenter rewardedAdPresenter) {
                rewardedAdPresenter.getAdInteractor().removeStateListener(this);
                RetainedAdPresenterRepository.this.a(str);
            }
        };
        rewardedAdPresenter.getAdInteractor().addStateListener(r0);
        this.a.put(str, new a(rewardedAdPresenter, r0));
        return str;
    }

    /* access modifiers changed from: 0000 */
    public final synchronized void a(@NonNull String str) {
        this.a.remove(str);
    }
}
