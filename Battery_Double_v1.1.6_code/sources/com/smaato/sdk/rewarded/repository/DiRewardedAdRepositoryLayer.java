package com.smaato.sdk.rewarded.repository;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.config.Configuration;
import com.smaato.sdk.core.config.ConfigurationRepository;
import com.smaato.sdk.core.config.DiConfiguration.Provider;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.repository.AdLoadersRegistry;
import com.smaato.sdk.core.repository.AdPresenterCache;
import com.smaato.sdk.core.repository.MultipleAdLoadersRegistry;
import com.smaato.sdk.core.repository.MultipleAdPresenterCache;
import com.smaato.sdk.rewarded.framework.RewardedAdModuleInterface;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;

public final class DiRewardedAdRepositoryLayer {
    private static final State a = State.IMPRESSED;

    private DiRewardedAdRepositoryLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiRewardedAdRepositoryLayer$23Iw5MQHRwuC25OOPl2sDYcPmTY.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(RetainedAdPresenterRepository.class, $$Lambda$DiRewardedAdRepositoryLayer$vXUDEcSVIhHtuH7vW1Zm8mYejWE.INSTANCE);
        diRegistry.registerFactory("RewardedAdModuleInterface.RewardedAdLoadExecutioner", ExecutorService.class, $$Lambda$DiRewardedAdRepositoryLayer$n9pkWaVgB2LPwZb3qw5QCTS68ZI.INSTANCE);
        diRegistry.registerSingletonFactory(RewardedAdModuleInterface.MODULE_DI_NAME, AdPresenterCache.class, $$Lambda$DiRewardedAdRepositoryLayer$mwgN9yR42YX2xWUr607nGHhpL0w.INSTANCE);
        diRegistry.registerSingletonFactory(RewardedAdModuleInterface.MODULE_DI_NAME, AdLoadersRegistry.class, $$Lambda$DiRewardedAdRepositoryLayer$1VFsU0J_guhAInX0bmtYHpg6TQ.INSTANCE);
        diRegistry.registerSingletonFactory(RewardedAdModuleInterface.MODULE_DI_NAME, ConfigurationRepository.class, $$Lambda$DiRewardedAdRepositoryLayer$kFiB7Rlf5Uv42gN9d00DrMrIk.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RetainedAdPresenterRepository e(DiConstructor diConstructor) {
        return new RetainedAdPresenterRepository(new HashMap());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterCache c(DiConstructor diConstructor) {
        return new MultipleAdPresenterCache((ConfigurationRepository) diConstructor.get(RewardedAdModuleInterface.MODULE_DI_NAME, ConfigurationRepository.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoadersRegistry b(DiConstructor diConstructor) {
        return new MultipleAdLoadersRegistry((ConfigurationRepository) diConstructor.get(RewardedAdModuleInterface.MODULE_DI_NAME, ConfigurationRepository.class), DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ConfigurationRepository a(DiConstructor diConstructor) {
        return (ConfigurationRepository) ((Provider) diConstructor.get(Provider.class)).apply(new Configuration(1, a));
    }
}
