package com.smaato.sdk.rewarded;

import com.smaato.sdk.core.util.fi.Supplier;
import java.util.UUID;

/* renamed from: com.smaato.sdk.rewarded.-$$Lambda$DiRewardedAds$ITAc98ikUL8w-lvM5kEXSwAIpM8 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiRewardedAds$ITAc98ikUL8wlvM5kEXSwAIpM8 implements Supplier {
    public static final /* synthetic */ $$Lambda$DiRewardedAds$ITAc98ikUL8wlvM5kEXSwAIpM8 INSTANCE = new $$Lambda$DiRewardedAds$ITAc98ikUL8wlvM5kEXSwAIpM8();

    private /* synthetic */ $$Lambda$DiRewardedAds$ITAc98ikUL8wlvM5kEXSwAIpM8() {
    }

    public final Object get() {
        return UUID.randomUUID().toString();
    }
}
