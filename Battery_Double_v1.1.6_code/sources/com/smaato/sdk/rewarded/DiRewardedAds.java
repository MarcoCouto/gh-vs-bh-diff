package com.smaato.sdk.rewarded;

import android.app.Application;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.api.AdRequestExtrasProvider;
import com.smaato.sdk.core.api.VideoTypeAdRequestExtrasProvider;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.DiAdRepository.Provider;
import com.smaato.sdk.core.repository.RawDataStrategyFactory;
import com.smaato.sdk.rewarded.framework.RewardedAdModuleInterface;
import com.smaato.sdk.rewarded.repository.DiRewardedAdRepositoryLayer;
import com.smaato.sdk.rewarded.repository.RetainedAdPresenterRepository;

public final class DiRewardedAds {
    private DiRewardedAds() {
    }

    @NonNull
    public static DiRegistry createRewardedAdsRegistry() {
        return DiRegistry.of($$Lambda$DiRewardedAds$80ZssHAFLknMvTbeH86kRFLvKjs.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(d.class, $$Lambda$DiRewardedAds$uVAfbmwStf5rrvuT5qkCBz7kc.INSTANCE);
        diRegistry.registerSingletonFactory(RewardedAdModuleInterface.MODULE_DI_NAME, SharedKeyValuePairsHolder.class, $$Lambda$DiRewardedAds$GoW0S0ZyEvmnwLgqYT0Bwfi7kY.INSTANCE);
        diRegistry.registerFactory(a.class, $$Lambda$DiRewardedAds$kAru_4Y31i2DgOjKqsvNr6SIAcY.INSTANCE);
        diRegistry.registerFactory(b.class, $$Lambda$DiRewardedAds$m4uNsCWU_EAQ6gC4yM_m9AQO064.INSTANCE);
        diRegistry.registerFactory(RewardedAdModuleInterface.MODULE_DI_NAME, AdRequestExtrasProvider.class, $$Lambda$DiRewardedAds$aw3kyKt85kduUbXg643ltXwPUdg.INSTANCE);
        diRegistry.addFrom(DiRewardedAdRepositoryLayer.createRegistry());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ d e(DiConstructor diConstructor) {
        d dVar = new d((b) diConstructor.get(b.class), (AdRepository) ((Provider) diConstructor.get(Provider.class)).apply(RewardedAdModuleInterface.MODULE_DI_NAME), (a) diConstructor.get(a.class), DiAdLayer.getUserInfoFactoryFrom(diConstructor), (SharedKeyValuePairsHolder) diConstructor.get(RewardedAdModuleInterface.MODULE_DI_NAME, SharedKeyValuePairsHolder.class), DiAdLayer.getFullscreenAdDimensionMapperFrom(diConstructor), (Application) diConstructor.get(Application.class), DiLogLayer.getLoggerFrom(diConstructor), (RawDataStrategyFactory) diConstructor.get(RawDataStrategyFactory.class));
        return dVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SharedKeyValuePairsHolder d(DiConstructor diConstructor) {
        return new SharedKeyValuePairsHolder();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a c(DiConstructor diConstructor) {
        return new a();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b b(DiConstructor diConstructor) {
        return new b((RetainedAdPresenterRepository) diConstructor.get(RetainedAdPresenterRepository.class), $$Lambda$DiRewardedAds$ITAc98ikUL8wlvM5kEXSwAIpM8.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdRequestExtrasProvider a(DiConstructor diConstructor) {
        return new VideoTypeAdRequestExtrasProvider("rewarded");
    }
}
