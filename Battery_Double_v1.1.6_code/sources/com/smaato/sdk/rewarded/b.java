package com.smaato.sdk.rewarded;

import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.rewarded.repository.RetainedAdPresenterRepository;

final class b {
    @NonNull
    private final RetainedAdPresenterRepository a;
    @NonNull
    private final Supplier<String> b;

    b(@NonNull RetainedAdPresenterRepository retainedAdPresenterRepository, @NonNull Supplier<String> supplier) {
        this.a = (RetainedAdPresenterRepository) Objects.requireNonNull(retainedAdPresenterRepository);
        this.b = (Supplier) Objects.requireNonNull(supplier);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final RewardedInterstitialAd a(@NonNull Context context, @NonNull Handler handler, @NonNull Logger logger, @NonNull RewardedAdPresenter rewardedAdPresenter, @NonNull EventListener eventListener) {
        c cVar = new c(context, handler, logger, rewardedAdPresenter, eventListener, this.a, this.b);
        return cVar;
    }
}
