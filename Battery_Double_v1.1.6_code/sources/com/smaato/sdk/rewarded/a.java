package com.smaato.sdk.rewarded;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.util.Objects;

class a {
    a() {
    }

    @NonNull
    public static RewardedError a(@NonNull Error error) {
        Objects.requireNonNull(error, "Parameter adLoaderError cannot be null for BannerErrorMapperUtil::map");
        switch (error) {
            case NO_AD:
            case NO_MANDATORY_CACHE:
                return RewardedError.NO_AD_AVAILABLE;
            case BAD_REQUEST:
                return RewardedError.INVALID_REQUEST;
            case PRESENTER_BUILDER_GENERIC:
            case INVALID_RESPONSE:
            case API:
            case CONFIGURATION_ERROR:
            case INTERNAL:
            case TTL_EXPIRED:
            case TOO_MANY_REQUESTS:
                return RewardedError.INTERNAL_ERROR;
            case NETWORK:
            case NO_CONNECTION:
                return RewardedError.NETWORK_ERROR;
            case CREATIVE_RESOURCE_EXPIRED:
                return RewardedError.CREATIVE_RESOURCE_EXPIRED;
            default:
                return RewardedError.INTERNAL_ERROR;
        }
    }
}
