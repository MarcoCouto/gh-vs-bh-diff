package com.smaato.sdk.rewarded;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public enum RewardedError {
    NO_AD_AVAILABLE("No ad is currently available matching the requested parameters."),
    INVALID_REQUEST("Invalid ad request (e.g. PublisherId or AdSpaceId is incorrect)."),
    NETWORK_ERROR("The ad request has not been completed due to a network connectivity issue."),
    INTERNAL_ERROR("An internal error happened (e.g. the ad server responded with an invalid response)."),
    CREATIVE_RESOURCE_EXPIRED("A creative resource's TTL expired.");
    
    @NonNull
    private String a;

    private RewardedError(String str) {
        this.a = (String) Objects.requireNonNull(str);
    }

    public final String toString() {
        return String.format("[%s]: %s", new Object[]{name(), this.a});
    }
}
