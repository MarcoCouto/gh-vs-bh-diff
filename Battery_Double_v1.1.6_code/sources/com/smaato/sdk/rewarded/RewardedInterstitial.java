package com.smaato.sdk.rewarded;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.repository.AdRequestParams;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.diinjection.Inject;

public final class RewardedInterstitial {
    private static final String a = "RewardedInterstitial";
    @Inject
    @Nullable
    private static volatile d b;
    @Nullable
    private static String c;
    @Nullable
    private static String d;
    @Nullable
    private static String e;

    private RewardedInterstitial() {
    }

    @Nullable
    private static d a() {
        if (b == null) {
            synchronized (RewardedInterstitial.class) {
                if (b == null) {
                    AndroidsInjector.injectStatic(RewardedInterstitial.class);
                }
            }
        }
        return b;
    }

    public static void loadAd(@NonNull String str, @NonNull EventListener eventListener) {
        loadAd(str, eventListener, null);
    }

    public static void loadAd(@NonNull String str, @NonNull EventListener eventListener, @Nullable AdRequestParams adRequestParams) {
        d a2 = a();
        if (a2 == null) {
            Log.e(a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
            return;
        }
        if (!SmaatoSdk.isGPSEnabled()) {
            Log.w(a, "Usage of the GPS coordinates for advertising purposes is disabled. You can change that by setting setGPSLocation to TRUE.");
        }
        String publisherId = SmaatoSdk.getPublisherId();
        if (TextUtils.isEmpty(publisherId)) {
            Log.e(a, "Failed to proceed with RewardedInterstitial::loadAd. Missing required parameter: publisherId");
            Threads.runOnUi(new Runnable(publisherId, str) {
                private final /* synthetic */ String f$1;
                private final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    EventListener.this.onAdFailedToLoad(new RewardedRequestError(RewardedError.INVALID_REQUEST, this.f$1, this.f$2));
                }
            });
            return;
        }
        a2.a(publisherId, str, eventListener, null, c, d, e, adRequestParams);
    }

    @Nullable
    public static KeyValuePairs getKeyValuePairs() {
        d a2 = a();
        if (a2 != null) {
            return a2.a();
        }
        Log.e(a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
        return null;
    }

    public static void setKeyValuePairs(@Nullable KeyValuePairs keyValuePairs) {
        d a2 = a();
        if (a2 == null) {
            Log.e(a, "SmaatoSdk is not initialized. SmaatoSdk.init() should be called before ad request");
        } else {
            a2.a(keyValuePairs);
        }
    }

    public static void setMediationNetworkName(@Nullable String str) {
        c = str;
    }

    public static void setMediationNetworkSDKVersion(@Nullable String str) {
        d = str;
    }

    public static void setMediationAdapterVersion(@Nullable String str) {
        e = str;
    }
}
