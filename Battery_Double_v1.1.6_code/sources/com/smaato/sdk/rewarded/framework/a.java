package com.smaato.sdk.rewarded.framework;

import android.app.Activity;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.rewarded.widget.RewardedInterstitialAdActivity;
import java.util.Collections;
import java.util.Set;

final class a {
    @NonNull
    static final Set<String> a = Collections.emptySet();
    @NonNull
    static final Set<Class<? extends Activity>> b = Sets.toImmutableSetOf(RewardedInterstitialAdActivity.class);
}
