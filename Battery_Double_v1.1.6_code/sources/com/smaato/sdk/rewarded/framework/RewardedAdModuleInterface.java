package com.smaato.sdk.rewarded.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.api.AdRequestExtrasProvider;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.ModuleInterface;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.util.fi.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class RewardedAdModuleInterface implements ModuleInterface {
    @NonNull
    public static final String MODULE_DI_NAME = "RewardedAdModuleInterface";
    @NonNull
    private static final String a = String.format("In order to show ads of %1$s format at least one of %1$s modules should be added to your project configuration.", new Object[]{AdFormat.VIDEO});
    @Nullable
    private volatile List<AdPresenterModuleInterface> b;
    private boolean c;

    @NonNull
    public String moduleDiName() {
        return MODULE_DI_NAME;
    }

    @NonNull
    public String version() {
        return "21.3.1";
    }

    public void init(@NonNull ClassLoader classLoader) {
        a((Iterable<AdPresenterModuleInterface>) ServiceLoader.load(AdPresenterModuleInterface.class, classLoader));
    }

    public boolean isInitialised() {
        return this.c;
    }

    @NonNull
    public Class<? extends AdPresenter> getSupportedAdPresenterInterface() {
        return RewardedAdPresenter.class;
    }

    @VisibleForTesting
    private synchronized void a(@NonNull Iterable<AdPresenterModuleInterface> iterable) {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    ArrayList arrayList = new ArrayList();
                    for (AdPresenterModuleInterface adPresenterModuleInterface : iterable) {
                        if (adPresenterModuleInterface.version().equals("21.3.1")) {
                            arrayList.add(adPresenterModuleInterface);
                        }
                    }
                    this.b = arrayList;
                    this.c = a(AdFormat.VIDEO);
                }
            }
        }
    }

    @NonNull
    private List<AdPresenterModuleInterface> a() {
        List<AdPresenterModuleInterface> list = this.b;
        if (list != null) {
            return list;
        }
        throw new IllegalStateException("init() method should have been called first for this module: smaato-sdk-rewarded-ads");
    }

    public boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        if (adFormat != AdFormat.VIDEO) {
            return a(adFormat);
        }
        if (!this.c) {
            logger.error(LogDomain.FRAMEWORK, a, new Object[0]);
        }
        return this.c;
    }

    private boolean a(@NonNull AdFormat adFormat) {
        return Lists.any(a(), new Predicate() {
            public final boolean test(Object obj) {
                return ((AdPresenterModuleInterface) obj).isFormatSupported(AdFormat.this, RewardedAdPresenter.class);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ AdLoaderPlugin a(DiConstructor diConstructor) {
        return new RewardedAdLoaderPlugin(a(), new NullableFunction() {
            public final Object apply(Object obj) {
                return RewardedAdModuleInterface.a(DiConstructor.this, (AdPresenterModuleInterface) obj);
            }
        }, (AdRequestExtrasProvider) diConstructor.get(moduleDiName(), AdRequestExtrasProvider.class), a);
    }

    @NonNull
    public ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory() {
        return new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return RewardedAdModuleInterface.this.a(diConstructor);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderPlugin a(DiConstructor diConstructor, AdPresenterModuleInterface adPresenterModuleInterface) {
        if (adPresenterModuleInterface == null) {
            return null;
        }
        return (AdLoaderPlugin) DiAdLayer.tryGetOrNull(diConstructor, adPresenterModuleInterface.moduleDiName(), AdLoaderPlugin.class);
    }

    @Nullable
    public DiRegistry moduleDiRegistry() {
        return DiRegistry.of($$Lambda$RewardedAdModuleInterface$Df5Hr8oiKtPJHtuBm_YhT0lnc8.INSTANCE);
    }

    @NonNull
    public ExpectedManifestEntries getExpectedManifestEntries() {
        return new ExpectedManifestEntries(a.a, a.b);
    }

    @NonNull
    public String toString() {
        StringBuilder sb = new StringBuilder("RewardedAdModuleInterface{supportedFormat: ");
        sb.append(AdFormat.VIDEO);
        sb.append("}");
        return sb.toString();
    }
}
