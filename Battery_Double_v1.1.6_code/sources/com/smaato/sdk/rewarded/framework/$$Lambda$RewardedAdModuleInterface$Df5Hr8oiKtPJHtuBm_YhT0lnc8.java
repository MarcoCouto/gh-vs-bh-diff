package com.smaato.sdk.rewarded.framework;

import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.rewarded.DiRewardedAds;

/* renamed from: com.smaato.sdk.rewarded.framework.-$$Lambda$RewardedAdModuleInterface$Df5Hr8oiKt-PJHtuBm_YhT0lnc8 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$RewardedAdModuleInterface$Df5Hr8oiKtPJHtuBm_YhT0lnc8 implements Consumer {
    public static final /* synthetic */ $$Lambda$RewardedAdModuleInterface$Df5Hr8oiKtPJHtuBm_YhT0lnc8 INSTANCE = new $$Lambda$RewardedAdModuleInterface$Df5Hr8oiKtPJHtuBm_YhT0lnc8();

    private /* synthetic */ $$Lambda$RewardedAdModuleInterface$Df5Hr8oiKtPJHtuBm_YhT0lnc8() {
    }

    public final void accept(Object obj) {
        ((DiRegistry) obj).addFrom(DiRewardedAds.createRewardedAdsRegistry());
    }
}
