package com.smaato.sdk.rewarded.util;

import android.util.Pair;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public class NonNullPair<T1, T2> extends Pair<T1, T2> {
    public NonNullPair(@NonNull T1 t1, @NonNull T2 t2) {
        super(Objects.requireNonNull(t1), Objects.requireNonNull(t2));
    }
}
