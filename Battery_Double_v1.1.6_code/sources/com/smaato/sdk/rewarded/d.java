package com.smaato.sdk.rewarded;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Gender;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.LatLng;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdRequest;
import com.smaato.sdk.core.ad.AdSettings;
import com.smaato.sdk.core.ad.AdSettings.Builder;
import com.smaato.sdk.core.ad.FullscreenAdDimensionMapper;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.ad.UserInfo;
import com.smaato.sdk.core.ad.UserInfoSupplier;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.AdRepository.Listener;
import com.smaato.sdk.core.repository.AdRequestParams;
import com.smaato.sdk.core.repository.AdTypeStrategy;
import com.smaato.sdk.core.repository.RawDataStrategyFactory;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.rewarded.repository.RewardedAdTypeStrategy;
import java.lang.ref.WeakReference;

final class d {
    /* access modifiers changed from: private */
    @NonNull
    public final b a;
    @NonNull
    private final AdRepository b;
    /* access modifiers changed from: private */
    @NonNull
    public final a c;
    @NonNull
    private final UserInfoSupplier d;
    @NonNull
    private final SharedKeyValuePairsHolder e;
    /* access modifiers changed from: private */
    @NonNull
    public final Logger f;
    @NonNull
    private final FullscreenAdDimensionMapper g;
    /* access modifiers changed from: private */
    @NonNull
    public final Application h;
    @NonNull
    private final RawDataStrategyFactory i;

    d(@NonNull b bVar, @NonNull AdRepository adRepository, @NonNull a aVar, @NonNull UserInfoSupplier userInfoSupplier, @NonNull SharedKeyValuePairsHolder sharedKeyValuePairsHolder, @NonNull FullscreenAdDimensionMapper fullscreenAdDimensionMapper, @NonNull Application application, @NonNull Logger logger, @NonNull RawDataStrategyFactory rawDataStrategyFactory) {
        this.a = (b) Objects.requireNonNull(bVar);
        this.b = (AdRepository) Objects.requireNonNull(adRepository);
        this.c = (a) Objects.requireNonNull(aVar);
        this.d = (UserInfoSupplier) Objects.requireNonNull(userInfoSupplier);
        this.e = (SharedKeyValuePairsHolder) Objects.requireNonNull(sharedKeyValuePairsHolder);
        this.g = (FullscreenAdDimensionMapper) Objects.requireNonNull(fullscreenAdDimensionMapper);
        this.h = (Application) Objects.requireNonNull(application);
        this.f = (Logger) Objects.requireNonNull(logger);
        this.i = (RawDataStrategyFactory) Objects.requireNonNull(rawDataStrategyFactory);
    }

    @Nullable
    private AdRequest a(@NonNull String str, @NonNull String str2, @Nullable UserInfo userInfo, @Nullable String str3, @Nullable String str4, @Nullable String str5) {
        UserInfo userInfo2;
        boolean z;
        try {
            AdSettings build = new Builder().setPublisherId(str).setAdSpaceId(str2).setAdFormat(AdFormat.VIDEO).setAdDimension(this.g.getDimension(this.h.getString(R.string.smaato_sdk_core_fullscreen_dimension))).setWidth(UIUtils.getDisplayWidthInDp()).setHeight(UIUtils.getDisplayHeightInDp()).setMediationNetworkName(str3).setMediationNetworkSDKVersion(str4).setMediationAdapterVersion(str5).build();
            if (userInfo == null) {
                userInfo2 = this.d.get();
            } else {
                String keywords = userInfo.getKeywords() != null ? userInfo.getKeywords() : SmaatoSdk.getKeywords();
                String searchQuery = userInfo.getSearchQuery() != null ? userInfo.getSearchQuery() : SmaatoSdk.getSearchQuery();
                Gender gender = userInfo.getGender() != null ? userInfo.getGender() : SmaatoSdk.getGender();
                Integer age = userInfo.getAge() != null ? userInfo.getAge() : SmaatoSdk.getAge();
                LatLng latLng = userInfo.getLatLng() != null ? userInfo.getLatLng() : SmaatoSdk.getLatLng();
                String region = userInfo.getRegion() != null ? userInfo.getRegion() : SmaatoSdk.getRegion();
                String zip = userInfo.getZip() != null ? userInfo.getZip() : SmaatoSdk.getZip();
                String language = userInfo.getLanguage() != null ? userInfo.getLanguage() : SmaatoSdk.getLanguage();
                if (!userInfo.getCoppa()) {
                    if (!SmaatoSdk.getCoppa()) {
                        z = false;
                        userInfo2 = new UserInfo.Builder().setKeywords(keywords).setSearchQuery(searchQuery).setGender(gender).setAge(age).setLatLng(latLng).setRegion(region).setZip(zip).setLanguage(language).setCoppa(z).build();
                    }
                }
                z = true;
                userInfo2 = new UserInfo.Builder().setKeywords(keywords).setSearchQuery(searchQuery).setGender(gender).setAge(age).setLatLng(latLng).setRegion(region).setZip(zip).setLanguage(language).setCoppa(z).build();
            }
            return new AdRequest.Builder().setAdSettings(build).setUserInfo(userInfo2).setKeyValuePairs(this.e.getKeyValuePairs()).build();
        } catch (Exception e2) {
            this.f.error(LogDomain.REWARDED, "Failed to proceed with RewardedInterstitial::loadAd. %s", e2.getMessage());
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final KeyValuePairs a() {
        return this.e.getKeyValuePairs();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable KeyValuePairs keyValuePairs) {
        this.e.setKeyValuePairs(keyValuePairs);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final String str, @NonNull final String str2, @NonNull EventListener eventListener, @Nullable UserInfo userInfo, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable AdRequestParams adRequestParams) {
        String str6 = str;
        String str7 = str2;
        String str8 = "eventListener";
        boolean z = false;
        if (eventListener == null) {
            this.f.error(LogDomain.REWARDED, "Failed to proceed with RewardedInterstitial::loadAd. Missing required parameter: %s", str8);
            z = true;
        }
        if (!z) {
            AdRequest a2 = a(str, str2, null, str3, str4, str5);
            if (a2 == null) {
                Threads.runOnUi(new Runnable(str, str2) {
                    private final /* synthetic */ String f$1;
                    private final /* synthetic */ String f$2;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    public final void run() {
                        EventListener.this.onAdFailedToLoad(new RewardedRequestError(RewardedError.INVALID_REQUEST, this.f$1, this.f$2));
                    }
                });
                return;
            }
            RewardedAdTypeStrategy rewardedAdTypeStrategy = new RewardedAdTypeStrategy(str, str2, this.i.create(str2, adRequestParams));
            final WeakReference weakReference = new WeakReference(eventListener);
            this.b.loadAd(rewardedAdTypeStrategy, a2, new Listener() {
                public final void onAdLoadSuccess(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdPresenter adPresenter) {
                    if (adPresenter instanceof RewardedAdPresenter) {
                        Objects.onNotNull(weakReference.get(), new Consumer(adPresenter) {
                            private final /* synthetic */ AdPresenter f$1;

                            {
                                this.f$1 = r2;
                            }

                            public final void accept(Object obj) {
                                AnonymousClass1.this.a(this.f$1, (EventListener) obj);
                            }
                        });
                        return;
                    }
                    adPresenter.releaseAccess();
                    onAdLoadError(adTypeStrategy, new AdLoaderException(Error.INVALID_RESPONSE, new Exception("Bad response type for Rewarded Interstitial")));
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(AdPresenter adPresenter, EventListener eventListener) {
                    Threads.runOnUi(new Runnable(eventListener, adPresenter) {
                        private final /* synthetic */ EventListener f$1;
                        private final /* synthetic */ AdPresenter f$2;

                        {
                            this.f$1 = r2;
                            this.f$2 = r3;
                        }

                        public final void run() {
                            AnonymousClass1.this.a(this.f$1, this.f$2);
                        }
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(EventListener eventListener, AdPresenter adPresenter) {
                    eventListener.onAdLoaded(d.this.a.a(d.this.h, Threads.newUiHandler(), d.this.f, (RewardedAdPresenter) adPresenter, eventListener));
                }

                public final void onAdLoadError(@NonNull AdTypeStrategy adTypeStrategy, @NonNull AdLoaderException adLoaderException) {
                    Objects.onNotNull(weakReference.get(), new Consumer(adLoaderException, str, str2) {
                        private final /* synthetic */ AdLoaderException f$1;
                        private final /* synthetic */ String f$2;
                        private final /* synthetic */ String f$3;

                        {
                            this.f$1 = r2;
                            this.f$2 = r3;
                            this.f$3 = r4;
                        }

                        public final void accept(Object obj) {
                            AnonymousClass1.this.a(this.f$1, this.f$2, this.f$3, (EventListener) obj);
                        }
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(AdLoaderException adLoaderException, String str, String str2, EventListener eventListener) {
                    $$Lambda$d$1$d5PxKz8J0g9SlbPanxXwAI7UjIs r0 = new Runnable(eventListener, adLoaderException, str, str2) {
                        private final /* synthetic */ EventListener f$1;
                        private final /* synthetic */ AdLoaderException f$2;
                        private final /* synthetic */ String f$3;
                        private final /* synthetic */ String f$4;

                        {
                            this.f$1 = r2;
                            this.f$2 = r3;
                            this.f$3 = r4;
                            this.f$4 = r5;
                        }

                        public final void run() {
                            AnonymousClass1.this.a(this.f$1, this.f$2, this.f$3, this.f$4);
                        }
                    };
                    Threads.runOnUi(r0);
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(EventListener eventListener, AdLoaderException adLoaderException, String str, String str2) {
                    d.this.c;
                    eventListener.onAdFailedToLoad(new RewardedRequestError(a.a(adLoaderException.getErrorType()), str, str2));
                }
            });
        }
    }
}
