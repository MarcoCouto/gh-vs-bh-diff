package com.smaato.sdk.banner;

public final class BuildConfig {
    @Deprecated
    public static final String APPLICATION_ID = "com.smaato.sdk.banner";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final String LIBRARY_PACKAGE_NAME = "com.smaato.sdk.banner";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "21.3.1";
}
