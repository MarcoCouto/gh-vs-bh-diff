package com.smaato.sdk.banner.widget;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.banner.ad.BannerAdSize;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.repository.CoreAdTypeStrategy;
import com.smaato.sdk.core.repository.RawDataStrategy;
import java.io.Serializable;
import java.util.Arrays;

final class a extends CoreAdTypeStrategy {
    @Nullable
    private final BannerAdSize a;

    a(@NonNull String str, @NonNull String str2, @NonNull RawDataStrategy rawDataStrategy, @Nullable BannerAdSize bannerAdSize) {
        super(str, str2, rawDataStrategy);
        this.a = bannerAdSize;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final Iterable getParams() {
        if (this.a == null) {
            return Arrays.asList(new String[]{this.publisherId, this.adSpaceId});
        }
        return Arrays.asList(new Serializable[]{this.publisherId, this.adSpaceId, Integer.valueOf(this.a.ordinal())});
    }

    @NonNull
    public final Class<? extends AdPresenter> getAdPresenterClass() {
        return BannerAdPresenter.class;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return this.publisherId.equals(aVar.publisherId) && this.adSpaceId.equals(aVar.adSpaceId) && this.a == aVar.a;
    }

    public final int hashCode() {
        return (((this.publisherId.hashCode() * 31) + this.adSpaceId.hashCode()) * 31) + (this.a != null ? this.a.hashCode() : 0);
    }
}
