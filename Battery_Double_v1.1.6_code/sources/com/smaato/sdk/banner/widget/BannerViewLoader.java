package com.smaato.sdk.banner.widget;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.banner.ad.BannerAdSize;
import com.smaato.sdk.core.KeyValuePairs;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoader.Error;
import com.smaato.sdk.core.ad.AdLoaderException;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdRequest;
import com.smaato.sdk.core.ad.AdSettings;
import com.smaato.sdk.core.ad.AdSettings.Builder;
import com.smaato.sdk.core.ad.AutoReloadPolicy;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.BannerAdPresenter.Listener;
import com.smaato.sdk.core.ad.SharedKeyValuePairsHolder;
import com.smaato.sdk.core.ad.UserInfo;
import com.smaato.sdk.core.ad.UserInfoSupplier;
import com.smaato.sdk.core.appbgdetection.AppBackgroundAwareHandler;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkStateMonitor;
import com.smaato.sdk.core.repository.AdRepository;
import com.smaato.sdk.core.repository.AdRequestParams;
import com.smaato.sdk.core.repository.RawDataStrategyFactory;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.fi.NullableSupplier;
import com.smaato.sdk.core.util.memory.LeakProtection;
import com.smaato.sdk.core.util.notifier.ChangeNotifier;
import com.smaato.sdk.flow.Flow;
import com.smaato.sdk.flow.Flow.Emitter;
import com.smaato.sdk.flow.Flow.Executors;
import com.smaato.sdk.flow.Publisher;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public final class BannerViewLoader {
    @NonNull
    private final Logger a;
    /* access modifiers changed from: private */
    @NonNull
    public final AutoReloadPolicy b;
    @NonNull
    private final AppBackgroundAwareHandler c;
    @NonNull
    private final UserInfoSupplier d;
    @NonNull
    private final NetworkStateMonitor e;
    @NonNull
    private final LeakProtection f;
    @NonNull
    private final AdRepository g;
    @NonNull
    private final SharedKeyValuePairsHolder h;
    @NonNull
    private final BlockingUtils i;
    @NonNull
    private final RawDataStrategyFactory j;
    /* access modifiers changed from: private */
    @NonNull
    public final Executors k;
    @NonNull
    private final AtomicReference<Runnable> l = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<Runnable> m = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<AdPresenter> n = new WeakReference<>(null);
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<BannerView> o = new WeakReference<>(null);
    @Nullable
    private Listener p;
    @NonNull
    private final Consumer<AdPresenter> q = new Consumer() {
        public final void accept(Object obj) {
            BannerViewLoader.this.a((AdPresenter) obj);
        }
    };
    @Nullable
    private ChangeNotifier.Listener<Boolean> r;
    /* access modifiers changed from: private */
    @Nullable
    public b s;
    /* access modifiers changed from: private */
    @NonNull
    public final Runnable t = new Runnable() {
        public final void run() {
            BannerViewLoader.this.n();
        }
    };
    @NonNull
    private final Consumer<Throwable> u = new Consumer() {
        public final void accept(Object obj) {
            BannerViewLoader.this.a((Throwable) obj);
        }
    };

    private class a implements Listener {
        private a() {
        }

        /* synthetic */ a(BannerViewLoader bannerViewLoader, byte b) {
            this();
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void e() {
            Objects.onNotNull(BannerViewLoader.this.o.get(), $$Lambda$6pvaFapotc797IUO_tpZnMhOAjg.INSTANCE);
            BannerViewLoader.this.a(BannerViewLoader.this.t);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void d() {
            Objects.onNotNull(BannerViewLoader.this.o.get(), $$Lambda$8iLCwlChtkJahuWHP31sIuBDG6g.INSTANCE);
            if (BannerViewLoader.this.b.isAutoReloadEnabled()) {
                BannerViewLoader.this.m.set(BannerViewLoader.this.t);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void onAdError(@NonNull BannerAdPresenter bannerAdPresenter) {
            BannerViewLoader.this.k.main().execute(new Runnable(bannerAdPresenter) {
                private final /* synthetic */ BannerAdPresenter f$1;

                {
                    this.f$1 = r2;
                }

                public final void run() {
                    a.this.b(this.f$1);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b(BannerAdPresenter bannerAdPresenter) {
            Objects.onNotNull(BannerViewLoader.this.n.get(), new Consumer(bannerAdPresenter) {
                private final /* synthetic */ BannerAdPresenter f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    a.this.a(this.f$1, (AdPresenter) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(BannerAdPresenter bannerAdPresenter, AdPresenter adPresenter) {
            if (adPresenter == bannerAdPresenter) {
                Objects.onNotNull(BannerViewLoader.this.s, new Consumer() {
                    public final void accept(Object obj) {
                        BannerViewLoader.this.a((b) obj);
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void c() {
            Objects.onNotNull(BannerViewLoader.this.n.get(), new Consumer() {
                public final void accept(Object obj) {
                    a.this.a((AdPresenter) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(AdPresenter adPresenter) {
            Objects.onNotNull(BannerViewLoader.this.o.get(), $$Lambda$o12ZmfRSmYPWpsUJeY1GpnbVwM.INSTANCE);
        }

        public final void onAdExpanded(@NonNull BannerAdPresenter bannerAdPresenter) {
            BannerViewLoader.this.k.main().execute(new Runnable() {
                public final void run() {
                    BannerViewLoader.this.h();
                }
            });
        }

        public final void onAdUnload(@NonNull BannerAdPresenter bannerAdPresenter) {
            if (TextUtils.isEmpty(BannerViewLoader.this.s == null ? null : BannerViewLoader.this.s.e)) {
                onAdError(bannerAdPresenter);
            } else {
                BannerViewLoader.this.k.main().execute(new Runnable() {
                    public final void run() {
                        a.this.b();
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b() {
            Objects.onNotNull(BannerViewLoader.this.o.get(), $$Lambda$BannerViewLoader$a$hFHv4uejS9c61sKprNVIWpQTvE8.INSTANCE);
        }

        public final void onAdClosed() {
            BannerViewLoader.this.a(BannerViewLoader.this.t);
        }

        public final void onAdResized() {
            BannerViewLoader.this.k.main().execute(new Runnable() {
                public final void run() {
                    a.this.a();
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a() {
            BannerViewLoader.this.m.set(null);
            BannerViewLoader.this.h();
        }

        public final /* synthetic */ void onTTLExpired(@NonNull AdPresenter adPresenter) {
            BannerViewLoader.this.k.main().execute(new Runnable() {
                public final void run() {
                    a.this.c();
                }
            });
        }

        public final /* synthetic */ void onAdClicked(@NonNull AdPresenter adPresenter) {
            BannerViewLoader.this.k.main().execute(new Runnable() {
                public final void run() {
                    a.this.d();
                }
            });
        }

        public final /* synthetic */ void onAdImpressed(@NonNull AdPresenter adPresenter) {
            BannerViewLoader.this.k.main().execute(new Runnable() {
                public final void run() {
                    a.this.e();
                }
            });
        }
    }

    static final class b {
        @Nullable
        final AdRequestParams a;
        @Nullable
        final String b;
        @Nullable
        final String c;
        @Nullable
        final BannerAdSize d;
        @Nullable
        final String e;
        @Nullable
        final String f;
        @Nullable
        final String g;

        b(@Nullable AdRequestParams adRequestParams, @Nullable String str, @Nullable String str2, @Nullable BannerAdSize bannerAdSize, @Nullable String str3, @Nullable String str4, @Nullable String str5) {
            this.b = str;
            this.c = str2;
            this.d = bannerAdSize;
            this.e = str3;
            this.f = str4;
            this.g = str5;
            this.a = adRequestParams;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdPresenter adPresenter) {
        if (!(adPresenter instanceof BannerAdPresenter)) {
            adPresenter.releaseAccess();
            Objects.onNotNull(this.o.get(), $$Lambda$BannerViewLoader$ZBopLIiCz8ALz3BqO1XaySbwXsg.INSTANCE);
            return;
        }
        Objects.onNotNull(this.n.get(), $$Lambda$LSD7uGamlg8DnWY8NYy91cOxow.INSTANCE);
        BannerAdPresenter bannerAdPresenter = (BannerAdPresenter) adPresenter;
        Objects.onNotNull(this.o.get(), $$Lambda$yqHc2JzDvjC4zMGnrUn3K26B4Yg.INSTANCE);
        this.p = new a(this, 0);
        this.n = new WeakReference<>(bannerAdPresenter);
        bannerAdPresenter.setListener(this.p);
        bannerAdPresenter.initialize();
        Objects.onNotNull(this.o.get(), new Consumer() {
            public final void accept(Object obj) {
                ((BannerView) obj).a((AdPresenter) BannerAdPresenter.this);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void n() {
        a(this.s);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Throwable th) {
        BannerError bannerError;
        if (th instanceof AdLoaderException) {
            AdLoaderException adLoaderException = (AdLoaderException) th;
            Error errorType = adLoaderException.getErrorType();
            if (errorType != Error.CANCELLED) {
                Objects.requireNonNull(errorType, "Parameter adLoaderError cannot be null for BannerErrorMapperUtil::map");
                switch (AnonymousClass1.a[errorType.ordinal()]) {
                    case 1:
                    case 2:
                        bannerError = BannerError.NO_AD_AVAILABLE;
                        break;
                    case 3:
                        bannerError = BannerError.INVALID_REQUEST;
                        break;
                    case 4:
                    case 5:
                        bannerError = BannerError.NETWORK_ERROR;
                        break;
                    case 6:
                        bannerError = BannerError.CREATIVE_RESOURCE_EXPIRED;
                        break;
                    default:
                        bannerError = BannerError.INTERNAL_ERROR;
                        break;
                }
                Objects.onNotNull(this.o.get(), new Consumer() {
                    public final void accept(Object obj) {
                        ((BannerView) obj).a(BannerError.this);
                    }
                });
                switch (errorType) {
                    case PRESENTER_BUILDER_GENERIC:
                    case INVALID_RESPONSE:
                    case NO_AD:
                    case API:
                    case CREATIVE_RESOURCE_EXPIRED:
                        a(this.t);
                        return;
                    case NETWORK:
                    case NO_CONNECTION:
                        if (this.e.isOnline()) {
                            this.c.postDelayed("Auto-retry", this.t, 30000, null);
                            return;
                        }
                        this.r = new ChangeNotifier.Listener() {
                            public final void onNextValue(Object obj) {
                                BannerViewLoader.this.a((Boolean) obj);
                            }
                        };
                        this.e.getNetworkStateChangeNotifier().addListener(this.r);
                        return;
                    case BAD_REQUEST:
                    case CONFIGURATION_ERROR:
                    case INTERNAL:
                    case CANCELLED:
                    case TTL_EXPIRED:
                    case TOO_MANY_REQUESTS:
                    case NO_MANDATORY_CACHE:
                        return;
                    default:
                        this.a.warning(LogDomain.WIDGET, "unexpected errorType %s", errorType);
                        break;
                }
            } else {
                this.a.info(LogDomain.WIDGET, "Ignoring AdLoader adLoaderException: %s", adLoaderException);
            }
        }
    }

    public BannerViewLoader(@NonNull Logger logger, @NonNull AutoReloadPolicy autoReloadPolicy, @NonNull AppBackgroundAwareHandler appBackgroundAwareHandler, @NonNull UserInfoSupplier userInfoSupplier, @NonNull NetworkStateMonitor networkStateMonitor, @NonNull LeakProtection leakProtection, @NonNull AdRepository adRepository, @NonNull SharedKeyValuePairsHolder sharedKeyValuePairsHolder, @NonNull Executors executors, @NonNull BlockingUtils blockingUtils, @NonNull RawDataStrategyFactory rawDataStrategyFactory) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (AutoReloadPolicy) Objects.requireNonNull(autoReloadPolicy);
        this.c = (AppBackgroundAwareHandler) Objects.requireNonNull(appBackgroundAwareHandler);
        this.d = (UserInfoSupplier) Objects.requireNonNull(userInfoSupplier);
        this.e = (NetworkStateMonitor) Objects.requireNonNull(networkStateMonitor);
        this.f = (LeakProtection) Objects.requireNonNull(leakProtection);
        this.g = (AdRepository) Objects.requireNonNull(adRepository);
        this.h = (SharedKeyValuePairsHolder) Objects.requireNonNull(sharedKeyValuePairsHolder);
        this.k = (Executors) Objects.requireNonNull(executors);
        this.i = (BlockingUtils) Objects.requireNonNull(blockingUtils);
        this.j = (RawDataStrategyFactory) Objects.requireNonNull(rawDataStrategyFactory);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ b m() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final b a() {
        return (b) a((NullableSupplier<T>) new NullableSupplier() {
            public final Object get() {
                return BannerViewLoader.this.m();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull BannerView bannerView) {
        Objects.requireNonNull(bannerView, "Parameter bannerView cannot be null for BannerViewLoader::setView");
        Flow.fromAction(new Runnable(bannerView) {
            private final /* synthetic */ BannerView f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                BannerViewLoader.this.b(this.f$1);
            }
        }).subscribeOn(this.k.main()).subscribe();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(BannerView bannerView) {
        this.o = new WeakReference<>(bannerView);
        this.f.listenToObject(bannerView, new Runnable() {
            public final void run() {
                BannerViewLoader.this.e();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull b bVar) {
        Objects.requireNonNull(bVar);
        Objects.requireNonNull(bVar);
        Flow.single(new Callable(bVar) {
            private final /* synthetic */ b f$1;

            {
                this.f$1 = r2;
            }

            public final Object call() {
                return BannerViewLoader.this.d(this.f$1);
            }
        }).flatMap(new Function() {
            public final Object apply(Object obj) {
                return BannerViewLoader.this.b((b) obj);
            }
        }).flatMap(new Function(this.h.getKeyValuePairs()) {
            private final /* synthetic */ KeyValuePairs f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return BannerViewLoader.this.a(this.f$1, (AdSettings) obj);
            }
        }).flatMap(new Function(bVar) {
            private final /* synthetic */ b f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return BannerViewLoader.this.a(this.f$1, (AdRequest) obj);
            }
        }).doOnNext(this.q).doOnError(this.u).subscribeOn(this.k.io()).observeOn(this.k.main()).subscribe();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ b d(b bVar) throws Exception {
        this.s = bVar;
        return bVar;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher a(KeyValuePairs keyValuePairs, AdSettings adSettings) {
        return Flow.create(new Consumer(adSettings, this.d.get(), keyValuePairs) {
            private final /* synthetic */ AdSettings f$1;
            private final /* synthetic */ UserInfo f$2;
            private final /* synthetic */ KeyValuePairs f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                BannerViewLoader.this.a(this.f$1, this.f$2, this.f$3, (Emitter) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Publisher a(b bVar, AdRequest adRequest) {
        g();
        String adSpaceId = adRequest.adSettings.getAdSpaceId();
        return this.g.loadAd(new a(adRequest.adSettings.getPublisherId(), adSpaceId, this.j.create(adSpaceId, bVar.a), bVar.d), adRequest);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(b bVar, Emitter emitter) {
        Flow single = Flow.single(new Callable() {
            public final Object call() {
                return BannerViewLoader.c(b.this);
            }
        });
        emitter.getClass();
        single.subscribe(new Consumer() {
            public final void accept(Object obj) {
                Emitter.this.onNext((AdSettings) obj);
            }
        }, new Consumer(emitter) {
            private final /* synthetic */ Emitter f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                BannerViewLoader.this.b(this.f$1, (Throwable) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    @NonNull
    public Flow<AdSettings> b(@NonNull b bVar) {
        return Flow.create(new Consumer(bVar) {
            private final /* synthetic */ b f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                BannerViewLoader.this.a(this.f$1, (Emitter) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdSettings c(b bVar) throws Exception {
        return new Builder().setPublisherId(bVar.b).setAdSpaceId(bVar.c).setAdFormat(AdFormat.DISPLAY).setAdDimension(bVar.d != null ? bVar.d.adDimension : null).setMediationNetworkName(bVar.e).setMediationNetworkSDKVersion(bVar.f).setMediationAdapterVersion(bVar.g).build();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Emitter emitter, Throwable th) {
        this.a.error(LogDomain.WIDGET, th.getMessage(), th);
        emitter.onError(new AdLoaderException(Error.BAD_REQUEST, new Exception(th)));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdSettings adSettings, UserInfo userInfo, KeyValuePairs keyValuePairs, Emitter emitter) {
        Flow single = Flow.single(new Callable(userInfo, keyValuePairs) {
            private final /* synthetic */ UserInfo f$1;
            private final /* synthetic */ KeyValuePairs f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final Object call() {
                return new AdRequest.Builder().setAdSettings(AdSettings.this).setUserInfo(this.f$1).setKeyValuePairs(this.f$2).build();
            }
        });
        emitter.getClass();
        single.subscribe(new Consumer() {
            public final void accept(Object obj) {
                Emitter.this.onNext((AdRequest) obj);
            }
        }, new Consumer(emitter) {
            private final /* synthetic */ Emitter f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                BannerViewLoader.this.a(this.f$1, (Throwable) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Emitter emitter, Throwable th) {
        this.a.error(LogDomain.WIDGET, th.getMessage(), th);
        emitter.onError(new AdLoaderException(Error.BAD_REQUEST, new Exception(th)));
    }

    private void g() {
        this.m.set(null);
        this.c.stop();
        this.e.getNetworkStateChangeNotifier().removeListener(this.r);
        this.r = null;
        h();
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Runnable runnable) {
        this.l.set(runnable);
        this.b.startWithAction(runnable);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ Integer l() {
        return Integer.valueOf(this.b.getAutoReloadInterval());
    }

    /* access modifiers changed from: 0000 */
    public final int b() {
        return ((Integer) a((NullableSupplier<T>) new NullableSupplier() {
            public final Object get() {
                return BannerViewLoader.this.l();
            }
        })).intValue();
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2) {
        this.i.a((Runnable) new Runnable(i2) {
            private final /* synthetic */ int f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                BannerViewLoader.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(int i2) {
        this.b.setAutoReloadInterval(i2);
        this.b.startWithAction((Runnable) this.l.get());
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String c() {
        return (String) a((NullableSupplier<T>) new NullableSupplier() {
            public final Object get() {
                return BannerViewLoader.this.k();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ String k() {
        AdPresenter adPresenter = (AdPresenter) this.n.get();
        if (adPresenter == null) {
            return null;
        }
        return adPresenter.getSessionId();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final String d() {
        return (String) a((NullableSupplier<T>) new NullableSupplier() {
            public final Object get() {
                return BannerViewLoader.this.j();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ String j() {
        AdPresenter adPresenter = (AdPresenter) this.n.get();
        if (adPresenter == null) {
            return null;
        }
        return adPresenter.getCreativeId();
    }

    /* access modifiers changed from: private */
    public void h() {
        this.l.set(null);
        this.b.stopTimer();
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        this.i.a((Runnable) new Runnable() {
            public final void run() {
                BannerViewLoader.this.i();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void i() {
        g();
        Objects.onNotNull(this.n.get(), $$Lambda$LSD7uGamlg8DnWY8NYy91cOxow.INSTANCE);
        this.n.clear();
        this.o.clear();
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2, int i3, int i4, int i5) {
        Executor main = this.k.main();
        $$Lambda$BannerViewLoader$NTosHYsJe6SA7oj6dybXygKT1c r1 = new Runnable(i2, i3, i4, i5) {
            private final /* synthetic */ int f$1;
            private final /* synthetic */ int f$2;
            private final /* synthetic */ int f$3;
            private final /* synthetic */ int f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void run() {
                BannerViewLoader.this.b(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        main.execute(r1);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(int i2, int i3, int i4, int i5) {
        this.a.error(LogDomain.WIDGET, "Content size[%d x %d] is bigger than BannerView [%d x %d]", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Boolean bool) {
        if (bool.booleanValue()) {
            a(this.s);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final KeyValuePairs f() {
        return this.h.getKeyValuePairs();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable KeyValuePairs keyValuePairs) {
        this.h.setKeyValuePairs(keyValuePairs);
    }

    @Nullable
    private <T> T a(@NonNull NullableSupplier<T> nullableSupplier) {
        return this.i.a(nullableSupplier);
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        Flow flow;
        if (z) {
            AtomicReference<Runnable> atomicReference = this.m;
            atomicReference.getClass();
            flow = Flow.maybe(new Callable(atomicReference) {
                private final /* synthetic */ AtomicReference f$0;

                {
                    this.f$0 = r1;
                }

                public final Object call() {
                    return (Runnable) this.f$0.get();
                }
            }).flatMap($$Lambda$V6XazwWvcCa1hdgZ7EvFmgDn2mc.INSTANCE);
        } else {
            flow = Flow.empty();
        }
        flow.subscribeOn(this.k.main()).subscribe();
    }
}
