package com.smaato.sdk.banner.widget;

import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.NullableSupplier;
import com.smaato.sdk.flow.Flow;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public class BlockingUtils {
    @NonNull
    private final Executor a;

    public BlockingUtils(@NonNull Executor executor) {
        this.a = (Executor) Objects.requireNonNull(executor);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final <T> T a(@NonNull NullableSupplier<T> nullableSupplier) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            return nullableSupplier.get();
        }
        AtomicReference atomicReference = new AtomicReference();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Flow.fromAction(new Runnable(atomicReference, nullableSupplier, countDownLatch) {
            private final /* synthetic */ AtomicReference f$0;
            private final /* synthetic */ NullableSupplier f$1;
            private final /* synthetic */ CountDownLatch f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                BlockingUtils.a(this.f$0, this.f$1, this.f$2);
            }
        }).subscribeOn(this.a).subscribe();
        try {
            countDownLatch.await();
            return atomicReference.get();
        } catch (InterruptedException unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AtomicReference atomicReference, NullableSupplier nullableSupplier, CountDownLatch countDownLatch) {
        atomicReference.set(nullableSupplier.get());
        countDownLatch.countDown();
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@NonNull Runnable runnable) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
            return true;
        }
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Flow subscribeOn = Flow.fromAction(runnable).subscribeOn(this.a);
        countDownLatch.getClass();
        subscribeOn.doOnComplete(new Runnable(countDownLatch) {
            private final /* synthetic */ CountDownLatch f$0;

            {
                this.f$0 = r1;
            }

            public final void run() {
                this.f$0.countDown();
            }
        }).subscribe();
        try {
            countDownLatch.await();
            return true;
        } catch (InterruptedException unused) {
            return false;
        }
    }
}
