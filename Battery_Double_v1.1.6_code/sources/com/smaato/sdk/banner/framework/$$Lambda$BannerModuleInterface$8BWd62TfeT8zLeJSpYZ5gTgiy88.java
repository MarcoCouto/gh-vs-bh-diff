package com.smaato.sdk.banner.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.banner.framework.-$$Lambda$BannerModuleInterface$8BWd62TfeT8zLeJSpYZ5gTgiy88 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$BannerModuleInterface$8BWd62TfeT8zLeJSpYZ5gTgiy88 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$BannerModuleInterface$8BWd62TfeT8zLeJSpYZ5gTgiy88 INSTANCE = new $$Lambda$BannerModuleInterface$8BWd62TfeT8zLeJSpYZ5gTgiy88();

    private /* synthetic */ $$Lambda$BannerModuleInterface$8BWd62TfeT8zLeJSpYZ5gTgiy88() {
    }

    public final Object get(DiConstructor diConstructor) {
        return BannerModuleInterface.f(diConstructor);
    }
}
