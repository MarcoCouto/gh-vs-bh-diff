package com.smaato.sdk.banner.ad;

import com.google.android.exoplayer2.extractor.ts.PsExtractor;

public enum AutoReloadInterval {
    DISABLED(0),
    DEFAULT(60),
    VERY_SHORT(10),
    SHORT(30),
    NORMAL(60),
    LONG(120),
    VERY_LONG(PsExtractor.VIDEO_STREAM_MASK);
    
    private final int a;

    private AutoReloadInterval(int i) {
        this.a = i;
    }

    public final int getSeconds() {
        return this.a;
    }
}
