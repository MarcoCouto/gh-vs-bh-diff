package com.smaato.sdk.banner.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.ApiAdRequestExtras;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.util.fi.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class BannerAdLoaderPlugin implements AdLoaderPlugin {
    @NonNull
    private final Iterable<? extends AdPresenterModuleInterface> a;
    @NonNull
    private final ArrayList<AdFormat> b = new ArrayList<>();
    private final boolean c;
    private final boolean d;
    @Nullable
    private final String e;
    @NonNull
    private NullableFunction<AdPresenterModuleInterface, AdLoaderPlugin> f;

    public BannerAdLoaderPlugin(@NonNull Iterable<? extends AdPresenterModuleInterface> iterable, @NonNull NullableFunction<AdPresenterModuleInterface, AdLoaderPlugin> nullableFunction, @NonNull List<AdFormat> list, @Nullable String str) {
        this.a = (Iterable) Objects.requireNonNull(iterable);
        this.f = nullableFunction;
        this.b.addAll((Collection) Objects.requireNonNull(list));
        this.c = Lists.any(this.b, new Predicate() {
            public final boolean test(Object obj) {
                return BannerAdLoaderPlugin.this.a((AdFormat) obj);
            }
        });
        this.d = Lists.all(this.b, new Predicate() {
            public final boolean test(Object obj) {
                return BannerAdLoaderPlugin.this.a((AdFormat) obj);
            }
        });
        this.e = str;
    }

    @Nullable
    public AdPresenterBuilder getAdPresenterBuilder(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls, @NonNull Logger logger) {
        if (!BannerAdPresenter.class.isAssignableFrom(cls)) {
            return null;
        }
        for (AdPresenterModuleInterface adPresenterModuleInterface : this.a) {
            if (adPresenterModuleInterface.isFormatSupported(adFormat, BannerAdPresenter.class)) {
                AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.f.apply(adPresenterModuleInterface);
                if (adLoaderPlugin != null) {
                    AdPresenterBuilder adPresenterBuilder = adLoaderPlugin.getAdPresenterBuilder(adFormat, cls, logger);
                    if (adPresenterBuilder != null) {
                        return adPresenterBuilder;
                    }
                } else {
                    continue;
                }
            }
        }
        return null;
    }

    public void addApiAdRequestExtras(@NonNull ApiAdRequestExtras apiAdRequestExtras, @NonNull Logger logger) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            AdFormat adFormat = (AdFormat) it.next();
            for (AdPresenterModuleInterface adPresenterModuleInterface : this.a) {
                if (adPresenterModuleInterface.isFormatSupported(adFormat, BannerAdPresenter.class)) {
                    AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.f.apply(adPresenterModuleInterface);
                    if (adLoaderPlugin != null) {
                        adLoaderPlugin.addApiAdRequestExtras(apiAdRequestExtras, logger);
                    }
                }
            }
        }
    }

    @Nullable
    public AdFormat resolveAdFormatToServerAdFormat(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        if (adFormat != AdFormat.DISPLAY) {
            return a(adFormat, logger);
        }
        if (!this.c) {
            logger.error(LogDomain.FRAMEWORK, this.e, new Object[0]);
        } else if (this.d) {
            return AdFormat.DISPLAY;
        } else {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                AdFormat a2 = a((AdFormat) it.next(), logger);
                if (a2 != null) {
                    return a2;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public boolean a(@NonNull AdFormat adFormat) {
        for (AdPresenterModuleInterface isFormatSupported : this.a) {
            if (isFormatSupported.isFormatSupported(adFormat, BannerAdPresenter.class)) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    private AdFormat a(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        for (AdPresenterModuleInterface adPresenterModuleInterface : this.a) {
            if (adPresenterModuleInterface.isFormatSupported(adFormat, BannerAdPresenter.class)) {
                AdLoaderPlugin adLoaderPlugin = (AdLoaderPlugin) this.f.apply(adPresenterModuleInterface);
                if (adLoaderPlugin != null) {
                    return adLoaderPlugin.resolveAdFormatToServerAdFormat(adFormat, logger);
                }
            }
        }
        return null;
    }
}
