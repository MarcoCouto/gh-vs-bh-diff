package com.smaato.sdk.banner.ad;

import com.smaato.sdk.core.ad.AutoReloadConfig;

public class BannerAutoReloadConfig implements AutoReloadConfig {
    private static final int a = 10;
    private static final int b = 240;
    private static final int c = AutoReloadInterval.DEFAULT.getSeconds();

    public int maxInterval() {
        return 240;
    }

    public int minInterval() {
        return 10;
    }

    public int defaultInterval() {
        return c;
    }
}
