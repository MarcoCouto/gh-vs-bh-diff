package com.smaato.sdk.image.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.NetworkResourceStreamPreparationStrategy;
import com.smaato.sdk.core.util.HeaderUtils;
import com.smaato.sdk.core.util.IOUtils;
import com.smaato.sdk.core.util.Objects;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

public class a implements NetworkResourceStreamPreparationStrategy {
    @NonNull
    private final HeaderUtils a;

    public a(@NonNull HeaderUtils headerUtils) {
        this.a = (HeaderUtils) Objects.requireNonNull(headerUtils);
    }

    @NonNull
    public InputStream prepare(@NonNull URLConnection uRLConnection) throws IOException {
        Throwable th;
        InputStream inputStream;
        Throwable th2;
        Throwable th3;
        Objects.requireNonNull(uRLConnection);
        if (!this.a.isChunkedTransferEncoding(uRLConnection.getHeaderFields())) {
            return uRLConnection.getInputStream();
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            inputStream = uRLConnection.getInputStream();
            try {
                IOUtils.copy(inputStream, byteArrayOutputStream);
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                if (inputStream != null) {
                    inputStream.close();
                }
                byteArrayOutputStream.close();
                return byteArrayInputStream;
            } catch (Throwable th4) {
                Throwable th5 = th4;
                th2 = r2;
                th3 = th5;
            }
        } catch (Throwable unused) {
        }
        throw th;
        throw th3;
        if (inputStream != null) {
            if (th2 != null) {
                try {
                    inputStream.close();
                } catch (Throwable unused2) {
                }
            } else {
                inputStream.close();
            }
        }
        throw th3;
    }
}
