package com.smaato.sdk.image.resourceloader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.resourceloader.PersistingStrategy;
import com.smaato.sdk.core.util.Objects;
import java.io.InputStream;

public class b implements PersistingStrategy<InputStream> {
    @Nullable
    public InputStream get(@NonNull String str) {
        return null;
    }

    @NonNull
    public InputStream put(@NonNull InputStream inputStream, @NonNull String str, long j) {
        return (InputStream) Objects.requireNonNull(inputStream);
    }
}
