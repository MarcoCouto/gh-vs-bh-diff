package com.smaato.sdk.image.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.resourceloader.NetworkResourceStreamPreparationStrategy;
import com.smaato.sdk.core.resourceloader.PersistingStrategy;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.resourceloader.ResourceLoadingNetworkTaskCreator;
import com.smaato.sdk.core.resourceloader.ResourceTransformer;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.HeaderUtils;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.image.ad.c;
import com.smaato.sdk.image.ad.e;
import com.smaato.sdk.image.ad.h;
import com.smaato.sdk.image.ad.j;
import com.smaato.sdk.image.resourceloader.a;
import com.smaato.sdk.image.resourceloader.b;
import java.util.concurrent.ExecutorService;

public class ImageModuleInterface implements AdPresenterModuleInterface {
    @NonNull
    public String moduleDiName() {
        return "ImageModuleInterface";
    }

    @NonNull
    public String version() {
        return "21.3.1";
    }

    public boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls) {
        return adFormat == AdFormat.STATIC_IMAGE && (cls.isAssignableFrom(InterstitialAdPresenter.class) || cls.isAssignableFrom(BannerAdPresenter.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderPlugin l(DiConstructor diConstructor) {
        return new e((AdPresenterNameShaper) diConstructor.get(AdPresenterNameShaper.class), new NullableFunction() {
            public final Object apply(Object obj) {
                return ImageModuleInterface.a(DiConstructor.this, (String) obj);
            }
        });
    }

    @NonNull
    public ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory() {
        return $$Lambda$ImageModuleInterface$YS9WvvfH9x0YIvgRHM6dSB7lCJk.INSTANCE;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder a(DiConstructor diConstructor, String str) {
        return (AdPresenterBuilder) DiAdLayer.tryGetOrNull(diConstructor, str, AdPresenterBuilder.class);
    }

    @Nullable
    public DiRegistry moduleDiRegistry() {
        return DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                ImageModuleInterface.this.a((DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ h k(DiConstructor diConstructor) {
        return new h();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ j j(DiConstructor diConstructor) {
        return new j(DiLogLayer.getLoggerFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ VisibilityTrackerCreator h(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get(moduleDiName(), VisibilityPrivateConfig.class);
        VisibilityTrackerCreator visibilityTrackerCreator = new VisibilityTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), moduleDiName());
        return visibilityTrackerCreator;
    }

    @Nullable
    public DiRegistry moduleAdPresenterDiRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper) {
        return c.createRegistry(adPresenterNameShaper, moduleDiName());
    }

    @NonNull
    public ExpectedManifestEntries getExpectedManifestEntries() {
        return ExpectedManifestEntries.EMPTY;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ ResourceLoader g(DiConstructor diConstructor) {
        Logger loggerFrom = DiLogLayer.getLoggerFrom(diConstructor);
        Objects.requireNonNull(diConstructor);
        ResourceLoader resourceLoader = new ResourceLoader(loggerFrom, (ResourceLoadingNetworkTaskCreator) diConstructor.get(moduleDiName(), ResourceLoadingNetworkTaskCreator.class), (ExecutorService) diConstructor.get("ImageModuleInterfaceRESOURCE_LOADING_DEVICE_LOCAL_EXECUTOR", ExecutorService.class), (PersistingStrategy) diConstructor.get(b.class), (ResourceTransformer) diConstructor.get(com.smaato.sdk.image.resourceloader.c.class));
        return resourceLoader;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ResourceLoadingNetworkTaskCreator e(DiConstructor diConstructor) {
        ResourceLoadingNetworkTaskCreator resourceLoadingNetworkTaskCreator = new ResourceLoadingNetworkTaskCreator(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getNetworkActionsFrom(diConstructor), ErrorMapper.NETWORK_LAYER_EXCEPTION, (ExecutorService) diConstructor.get("ImageModuleInterfaceRESOURCE_LOADING_NETWORK_EXECUTOR", ExecutorService.class), (NetworkResourceStreamPreparationStrategy) diConstructor.get(a.class));
        return resourceLoadingNetworkTaskCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a c(DiConstructor diConstructor) {
        return new a((HeaderUtils) diConstructor.get(HeaderUtils.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b b(DiConstructor diConstructor) {
        return new b();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ com.smaato.sdk.image.resourceloader.c a(DiConstructor diConstructor) {
        return new com.smaato.sdk.image.resourceloader.c();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ImageModuleInterface{supportedFormat: ");
        sb.append(AdFormat.STATIC_IMAGE);
        sb.append("}");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(moduleDiName(), ResourceLoader.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return ImageModuleInterface.this.g(diConstructor);
            }
        });
        diRegistry.registerSingletonFactory("ImageModuleInterfaceRESOURCE_LOADING_DEVICE_LOCAL_EXECUTOR", ExecutorService.class, $$Lambda$ImageModuleInterface$2juPZGuBl2t1Unw2B0x1BmL4h4.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), ResourceLoadingNetworkTaskCreator.class, $$Lambda$ImageModuleInterface$CVl0RyRKM_ugvLzepJv2_QG6L74.INSTANCE);
        diRegistry.registerSingletonFactory("ImageModuleInterfaceRESOURCE_LOADING_NETWORK_EXECUTOR", ExecutorService.class, $$Lambda$ImageModuleInterface$FBHsX9q98x8LPjKInnIdVgCMres.INSTANCE);
        diRegistry.registerFactory(a.class, $$Lambda$ImageModuleInterface$Livkv5uyNv1sR0EW6pqx09JeLk.INSTANCE);
        diRegistry.registerFactory(b.class, $$Lambda$ImageModuleInterface$fdE8ZNDbtcEz7kfM19bvB5S5cFc.INSTANCE);
        diRegistry.registerFactory(com.smaato.sdk.image.resourceloader.c.class, $$Lambda$ImageModuleInterface$ZuL6bx8aaQ_sSxBCNIfqwhJVqU.INSTANCE);
        diRegistry.registerSingletonFactory(h.class, $$Lambda$ImageModuleInterface$MXojvaD6ZBp5VDEGtcINaeKvY8.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), j.class, $$Lambda$ImageModuleInterface$HrW1k9cDjI4r74ZUmeXKFarH9aA.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), VisibilityPrivateConfig.class, $$Lambda$ImageModuleInterface$oGnVHMS7RNvDRR6UpSEzRyTNx4U.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), VisibilityTrackerCreator.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return ImageModuleInterface.this.h(diConstructor);
            }
        });
    }
}
