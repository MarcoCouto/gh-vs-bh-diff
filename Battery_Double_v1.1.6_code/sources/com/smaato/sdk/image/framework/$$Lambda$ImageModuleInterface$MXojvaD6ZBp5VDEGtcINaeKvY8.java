package com.smaato.sdk.image.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.image.framework.-$$Lambda$ImageModuleInterface$MXojvaD6ZBp5VDEGtcIN-aeKvY8 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ImageModuleInterface$MXojvaD6ZBp5VDEGtcINaeKvY8 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$ImageModuleInterface$MXojvaD6ZBp5VDEGtcINaeKvY8 INSTANCE = new $$Lambda$ImageModuleInterface$MXojvaD6ZBp5VDEGtcINaeKvY8();

    private /* synthetic */ $$Lambda$ImageModuleInterface$MXojvaD6ZBp5VDEGtcINaeKvY8() {
    }

    public final Object get(DiConstructor diConstructor) {
        return ImageModuleInterface.k(diConstructor);
    }
}
