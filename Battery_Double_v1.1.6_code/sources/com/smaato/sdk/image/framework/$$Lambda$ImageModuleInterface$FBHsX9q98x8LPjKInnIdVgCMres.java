package com.smaato.sdk.image.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import java.util.concurrent.Executors;

/* renamed from: com.smaato.sdk.image.framework.-$$Lambda$ImageModuleInterface$FBHsX9q98x8LPjKInnIdVgCMres reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ImageModuleInterface$FBHsX9q98x8LPjKInnIdVgCMres implements ClassFactory {
    public static final /* synthetic */ $$Lambda$ImageModuleInterface$FBHsX9q98x8LPjKInnIdVgCMres INSTANCE = new $$Lambda$ImageModuleInterface$FBHsX9q98x8LPjKInnIdVgCMres();

    private /* synthetic */ $$Lambda$ImageModuleInterface$FBHsX9q98x8LPjKInnIdVgCMres() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Executors.newFixedThreadPool(2);
    }
}
