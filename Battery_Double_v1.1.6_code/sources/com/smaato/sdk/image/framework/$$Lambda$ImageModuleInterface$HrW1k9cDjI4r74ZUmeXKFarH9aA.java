package com.smaato.sdk.image.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.image.framework.-$$Lambda$ImageModuleInterface$HrW1k9cDjI4r74ZUmeXKFarH9aA reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ImageModuleInterface$HrW1k9cDjI4r74ZUmeXKFarH9aA implements ClassFactory {
    public static final /* synthetic */ $$Lambda$ImageModuleInterface$HrW1k9cDjI4r74ZUmeXKFarH9aA INSTANCE = new $$Lambda$ImageModuleInterface$HrW1k9cDjI4r74ZUmeXKFarH9aA();

    private /* synthetic */ $$Lambda$ImageModuleInterface$HrW1k9cDjI4r74ZUmeXKFarH9aA() {
    }

    public final Object get(DiConstructor diConstructor) {
        return ImageModuleInterface.j(diConstructor);
    }
}
