package com.smaato.sdk.image.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.image.framework.-$$Lambda$ImageModuleInterface$oGnVHMS7RNvDRR6UpSEzRyTNx4U reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ImageModuleInterface$oGnVHMS7RNvDRR6UpSEzRyTNx4U implements ClassFactory {
    public static final /* synthetic */ $$Lambda$ImageModuleInterface$oGnVHMS7RNvDRR6UpSEzRyTNx4U INSTANCE = new $$Lambda$ImageModuleInterface$oGnVHMS7RNvDRR6UpSEzRyTNx4U();

    private /* synthetic */ $$Lambda$ImageModuleInterface$oGnVHMS7RNvDRR6UpSEzRyTNx4U() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(0.01d).visibilityTimeMillis(0).build();
    }
}
