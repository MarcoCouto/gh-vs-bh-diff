package com.smaato.sdk.image.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.R;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.ui.WatermarkImageButton;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.image.ad.f;

@SuppressLint({"ViewConstructor"})
public final class a extends AdContentView {
    @NonNull
    private final OnClickListener a;
    @NonNull
    private View b;

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(View view) {
    }

    private a(@NonNull Context context, @NonNull f fVar, @NonNull OnClickListener onClickListener) {
        super(context);
        this.a = onClickListener;
        ImageView imageView = new ImageView(getContext());
        int dpToPx = UIUtils.dpToPx(getContext(), (float) fVar.getWidth());
        int dpToPx2 = UIUtils.dpToPx(getContext(), (float) fVar.getHeight());
        addView(imageView, generateDefaultLayoutParams(dpToPx, dpToPx2));
        addView(new WatermarkImageButton(getContext()));
        FrameLayout frameLayout = new FrameLayout(getContext());
        frameLayout.setBackgroundResource(R.color.smaato_sdk_core_ui_semitransparent);
        frameLayout.setLayoutParams(new LayoutParams(dpToPx, dpToPx2));
        frameLayout.setOnClickListener($$Lambda$a$31m_r_JIEO5iBJhJmzZEc9A9U.INSTANCE);
        ProgressBar progressBar = new ProgressBar(getContext());
        progressBar.setLayoutParams(new LayoutParams(-2, -2, 17));
        frameLayout.addView(progressBar);
        this.b = frameLayout;
        this.b.setVisibility(8);
        addView(this.b);
        imageView.setImageBitmap(fVar.getBitmap());
        setLayoutParams(new LayoutParams(dpToPx, dpToPx2, 17));
        super.setOnClickListener(this.a);
    }

    @NonNull
    public static a create(@NonNull Context context, @NonNull f fVar, @NonNull OnClickListener onClickListener) {
        return new a((Context) Objects.requireNonNull(context, "Parameter context cannot be null for StaticImageAdContentView::create"), (f) Objects.requireNonNull(fVar, "Parameter imageAdObject cannot be null for StaticImageAdContentView::create"), (OnClickListener) Objects.requireNonNull(onClickListener, "Parameter internalClickListener cannot be null for StaticImageAdContentView::create"));
    }

    public final void setOnClickListener(@Nullable OnClickListener onClickListener) {
        super.setOnClickListener(new OnClickListener(onClickListener) {
            private final /* synthetic */ OnClickListener f$1;

            {
                this.f$1 = r2;
            }

            public final void onClick(View view) {
                a.this.a(this.f$1, view);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(OnClickListener onClickListener, View view) {
        this.a.onClick(view);
        Objects.onNotNull(onClickListener, new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((OnClickListener) obj).onClick(this.f$0);
            }
        });
    }

    @MainThread
    public final void showProgressIndicator(boolean z) {
        this.b.setVisibility(z ? 0 : 8);
    }
}
