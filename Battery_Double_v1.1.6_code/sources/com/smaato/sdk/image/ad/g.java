package com.smaato.sdk.image.ad;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Error;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Listener;
import com.smaato.sdk.core.ad.AdPresenterBuilderErrorMapper;
import com.smaato.sdk.core.ad.AdPresenterBuilderException;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.resourceloader.ResourceLoaderException;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.image.ad.f.a;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

abstract class g<Presenter extends AdPresenter> implements AdPresenterBuilder {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    @NonNull
    private final j b;
    @NonNull
    private final ResourceLoader<InputStream, Bitmap> c;
    /* access modifiers changed from: private */
    @NonNull
    public final h d;
    @NonNull
    private final Function<f, d> e;
    @NonNull
    private final Function<d, Presenter> f;

    g(@NonNull Logger logger, @NonNull j jVar, @NonNull ResourceLoader<InputStream, Bitmap> resourceLoader, @NonNull h hVar, @NonNull Function<f, d> function, @NonNull Function<d, Presenter> function2) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.e = (Function) Objects.requireNonNull(function);
        this.f = (Function) Objects.requireNonNull(function2);
        this.b = (j) Objects.requireNonNull(jVar);
        this.c = (ResourceLoader) Objects.requireNonNull(resourceLoader);
        this.d = (h) Objects.requireNonNull(hVar);
    }

    public void buildAdPresenter(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        Objects.requireNonNull(somaApiContext, "Parameter somaApiContext cannot be null for ImageAdPresenterBuilder::buildAdPresenter");
        ApiAdResponse apiAdResponse = somaApiContext.getApiAdResponse();
        try {
            try {
                final i parseResponse = this.b.parseResponse(new String(apiAdResponse.getBody(), apiAdResponse.getCharset()));
                this.a.info(LogDomain.AD, "Loading image from address %s", parseResponse.getImageUrl());
                String imageUrl = parseResponse.getImageUrl();
                ResourceLoader<InputStream, Bitmap> resourceLoader = this.c;
                final SomaApiContext somaApiContext2 = somaApiContext;
                final Listener listener2 = listener;
                final String str = imageUrl;
                AnonymousClass1 r5 = new ResourceLoader.Listener<Bitmap>() {
                    public final void onResourceLoaded(@NonNull Bitmap bitmap) {
                        g.a(g.this, parseResponse, somaApiContext2, bitmap, listener2);
                    }

                    public final void onFailure(@NonNull ResourceLoaderException resourceLoaderException) {
                        g.this.a.error(LogDomain.AD, "Failed to load Image url: %s with error: %s", str, resourceLoaderException);
                        listener2.onAdPresenterBuildError(g.this, AdPresenterBuilderErrorMapper.mapError(g.this.d.substituteReasonWithAdQualityViolationExceptionIfRequired(somaApiContext2, resourceLoaderException)));
                    }
                };
                resourceLoader.loadResource(imageUrl, somaApiContext, r5);
            } catch (a e2) {
                this.a.error(LogDomain.AD, e2, "Invalid AdResponse: %s", apiAdResponse);
                listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e2));
            }
        } catch (UnsupportedEncodingException e3) {
            this.a.error(LogDomain.AD, e3, "Invalid AdResponse: %s. Cannot parse AdResponse with provided charset: %s", apiAdResponse, apiAdResponse.getCharset());
            listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e3));
        }
    }

    static /* synthetic */ void a(g gVar, i iVar, SomaApiContext somaApiContext, Bitmap bitmap, Listener listener) {
        try {
            f build = new a().setSomaApiContext(somaApiContext).setBitmap(bitmap).setImageUrl(iVar.getImageUrl()).setWidth(iVar.getWidth()).setHeight(iVar.getHeight()).setClickUrl(iVar.getClickUrl()).setClickTrackingUrls(iVar.getClickTrackingUrls()).setImpressionTrackingUrls(iVar.getImpressionTrackingUrls()).setExtensions(iVar.getExtensions()).build();
            Bitmap bitmap2 = build.getBitmap();
            int width = bitmap2.getWidth();
            int height = bitmap2.getHeight();
            int width2 = build.getWidth();
            int height2 = build.getHeight();
            if (!(width == width2 && height == height2)) {
                gVar.a.error(LogDomain.AD, "Image dimensions do not match response dimensions Image[%d x %d] should be [%d x %d]", Integer.valueOf(width), Integer.valueOf(height), Integer.valueOf(width2), Integer.valueOf(height2));
            }
            listener.onAdPresenterBuildSuccess(gVar, (AdPresenter) gVar.f.apply((d) gVar.e.apply(build)));
        } catch (Exception e2) {
            gVar.a.error(LogDomain.AD, e2, "Failed to build ImageAdObject", new Object[0]);
            listener.onAdPresenterBuildError(gVar, new AdPresenterBuilderException(Error.INVALID_RESPONSE, e2));
        }
    }
}
