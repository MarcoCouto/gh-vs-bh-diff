package com.smaato.sdk.image.ad;

import androidx.annotation.NonNull;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Jsons;
import com.smaato.sdk.core.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;

public class j {
    @NonNull
    private final Logger a;

    static class a extends Exception {
        a(String str, Throwable th) {
            super(str, th);
        }
    }

    public j(@NonNull Logger logger) {
        this.a = (Logger) Objects.requireNonNull(logger, "Parameter logger cannot be null for ImageAdResponseParser::new");
    }

    @NonNull
    public i parseResponse(@NonNull String str) throws a {
        com.smaato.sdk.image.ad.i.a aVar = new com.smaato.sdk.image.ad.i.a();
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject(MessengerShareContentUtility.MEDIA_IMAGE);
            aVar.setImpressionTrackingUrls(Jsons.toStringList(jSONObject.getJSONArray("impressiontrackers")));
            aVar.setClickTrackingUrls(Jsons.toStringList(jSONObject.getJSONArray("clicktrackers")));
            JSONObject jSONObject2 = jSONObject.getJSONObject("img");
            String string = jSONObject2.getString("url");
            String string2 = jSONObject2.getString("w");
            String string3 = jSONObject2.getString("h");
            aVar.setImageUrl(string).setWidth(Integer.parseInt(string2)).setHeight(Integer.parseInt(string3)).setClickUrl(jSONObject2.getString("ctaurl"));
            return aVar.build();
        } catch (NumberFormatException | JSONException e) {
            String format = String.format("Invalid JSON content: %s", new Object[]{str});
            this.a.error(LogDomain.AD, e, format, new Object[0]);
            throw new a(format, e);
        } catch (Exception e2) {
            String str2 = "Cannot build ImageAdResponse due to validation error";
            this.a.error(LogDomain.AD, e2, str2, new Object[0]);
            throw new a(str2, e2);
        }
    }
}
