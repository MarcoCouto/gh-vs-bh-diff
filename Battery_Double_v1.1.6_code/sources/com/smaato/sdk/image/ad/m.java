package com.smaato.sdk.image.ad;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.notifier.Timer;
import java.io.InputStream;

final class m extends g<InterstitialAdPresenter> {
    m(@NonNull Logger logger, @NonNull Function<f, d> function, @NonNull j jVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull Timer timer, @NonNull ResourceLoader<InputStream, Bitmap> resourceLoader, @NonNull AppBackgroundDetector appBackgroundDetector, @NonNull h hVar) {
        super(logger, jVar, resourceLoader, hVar, function, new Function(visibilityTrackerCreator, timer, appBackgroundDetector) {
            private final /* synthetic */ VisibilityTrackerCreator f$1;
            private final /* synthetic */ Timer f$2;
            private final /* synthetic */ AppBackgroundDetector f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final Object apply(Object obj) {
                return m.a(Logger.this, this.f$1, this.f$2, this.f$3, (d) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ InterstitialAdPresenter a(Logger logger, VisibilityTrackerCreator visibilityTrackerCreator, Timer timer, AppBackgroundDetector appBackgroundDetector, d dVar) {
        l lVar = new l(logger, dVar, visibilityTrackerCreator, timer, appBackgroundDetector);
        return lVar;
    }
}
