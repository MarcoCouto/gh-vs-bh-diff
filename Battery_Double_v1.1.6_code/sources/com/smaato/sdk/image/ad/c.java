package com.smaato.sdk.image.ad;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.BannerAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.tracker.ImpressionDetector;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.core.util.notifier.Timer;
import java.io.InputStream;

public class c {

    private interface a extends Function<f, d> {
    }

    @NonNull
    public static DiRegistry createRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper, @NonNull String str) {
        Objects.requireNonNull(adPresenterNameShaper);
        Objects.requireNonNull(str);
        return DiRegistry.of(new Consumer(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                c.a(AdPresenterNameShaper.this, this.f$1, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AdPresenterNameShaper adPresenterNameShaper, String str, DiRegistry diRegistry) {
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.STATIC_IMAGE, InterstitialAdPresenter.class), AdPresenterBuilder.class, new ClassFactory(str, adPresenterNameShaper) {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ AdPresenterNameShaper f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final Object get(DiConstructor diConstructor) {
                return c.a(this.f$0, this.f$1, diConstructor);
            }
        });
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.STATIC_IMAGE, BannerAdPresenter.class), AdPresenterBuilder.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return c.d(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory("ImageModuleInterfacebannerImage", a.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return c.c(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory("ImageModuleInterfaceinterstitialImage", a.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return c.b(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory(str, StateMachine.class, $$Lambda$c$D18L0LgvnNRUH0OX2Us7Y16RrWo.INSTANCE);
        diRegistry.registerFactory("ImageModuleInterfacebannerImage", ImpressionDetector.class, $$Lambda$c$517E4z31XSu33tvdHiDZkPY70E.INSTANCE);
        diRegistry.registerFactory("ImageModuleInterfaceinterstitialImage", ImpressionDetector.class, $$Lambda$c$C6DejJPK3D7vDJEjV7Fu6u4kXe0.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder a(String str, AdPresenterNameShaper adPresenterNameShaper, DiConstructor diConstructor) {
        m mVar = new m(DiLogLayer.getLoggerFrom(diConstructor), (Function) diConstructor.get("ImageModuleInterfaceinterstitialImage", a.class), (j) diConstructor.get(str, j.class), (VisibilityTrackerCreator) diConstructor.get(str, VisibilityTrackerCreator.class), (Timer) diConstructor.get(adPresenterNameShaper.shapeName(AdFormat.INTERSTITIAL, InterstitialAdPresenter.class), Timer.class), a(str, diConstructor), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), (h) diConstructor.get(h.class));
        return mVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder d(String str, DiConstructor diConstructor) {
        b bVar = new b(DiLogLayer.getLoggerFrom(diConstructor), (Function) diConstructor.get("ImageModuleInterfacebannerImage", a.class), (j) diConstructor.get(str, j.class), (VisibilityTrackerCreator) diConstructor.get(str, VisibilityTrackerCreator.class), a(str, diConstructor), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), (h) diConstructor.get(h.class));
        return bVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a c(String str, DiConstructor diConstructor) {
        return new a(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return c.b(DiConstructor.this, this.f$1, (f) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ d b(DiConstructor diConstructor, String str, f fVar) {
        d dVar = new d((Logger) diConstructor.get(Logger.class), fVar, DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (StateMachine) diConstructor.get(str, StateMachine.class), (LinkResolver) diConstructor.get(LinkResolver.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class), (ImpressionDetector) diConstructor.get("ImageModuleInterfacebannerImage", ImpressionDetector.class));
        return dVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a b(String str, DiConstructor diConstructor) {
        return new a(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return c.a(DiConstructor.this, this.f$1, (f) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ d a(DiConstructor diConstructor, String str, f fVar) {
        d dVar = new d((Logger) diConstructor.get(Logger.class), fVar, DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (StateMachine) diConstructor.get(str, StateMachine.class), (LinkResolver) diConstructor.get(LinkResolver.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class), (ImpressionDetector) diConstructor.get("ImageModuleInterfaceinterstitialImage", ImpressionDetector.class));
        return dVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ImpressionDetector b(DiConstructor diConstructor) {
        return new ImpressionDetector(State.CREATED);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ImpressionDetector a(DiConstructor diConstructor) {
        return new ImpressionDetector(State.IMPRESSED);
    }

    @NonNull
    private static ResourceLoader<InputStream, Bitmap> a(@NonNull String str, @NonNull DiConstructor diConstructor) {
        Objects.requireNonNull(diConstructor);
        return (ResourceLoader) diConstructor.get(str, ResourceLoader.class);
    }
}
