package com.smaato.sdk.image.ad;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.tracker.ImpressionDetector;
import com.smaato.sdk.core.tracker.ImpressionDetector.Callback;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Listener;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.concurrent.atomic.AtomicReference;

public final class d extends AdInteractor<f> {
    @NonNull
    private final Logger a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final LinkResolver c;
    /* access modifiers changed from: private */
    @NonNull
    public AtomicReference<Task> d = new AtomicReference<>();
    @Nullable
    private a e;

    public interface a {
        void onImpressionTriggered();
    }

    public d(@NonNull Logger logger, @NonNull f fVar, @NonNull BeaconTracker beaconTracker, @NonNull StateMachine<Event, State> stateMachine, @NonNull LinkResolver linkResolver, @NonNull OneTimeActionFactory oneTimeActionFactory, @NonNull ImpressionDetector impressionDetector) {
        super(fVar, stateMachine, oneTimeActionFactory);
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.c = (LinkResolver) Objects.requireNonNull(linkResolver);
        stateMachine.addListener(new Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                d.this.a((State) obj, (State) obj2, metadata);
            }
        });
        stateMachine.addListener(impressionDetector.stateListener);
        $$Lambda$d$aJ9vZcc5iye8BI2QOvsFqyiTALg r0 = new Callback(impressionDetector, logger, beaconTracker, fVar) {
            private final /* synthetic */ ImpressionDetector f$1;
            private final /* synthetic */ Logger f$2;
            private final /* synthetic */ BeaconTracker f$3;
            private final /* synthetic */ f f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void onImpressionStateDetected() {
                d.this.a(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        impressionDetector.setOnImpressionStateDetectedCallback(r0);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(ImpressionDetector impressionDetector, Logger logger, BeaconTracker beaconTracker, f fVar) {
        impressionDetector.setOnImpressionStateDetectedCallback(null);
        logger.debug(LogDomain.AD, "Going to send impression beacons", new Object[0]);
        beaconTracker.trackBeaconUrls(fVar.getImpressionTrackingUrls(), fVar.getSomaApiContext());
        Objects.onNotNull(this.e, $$Lambda$Aifr3fzuGjJ6Hj9GE2ti6m_sj8.INSTANCE);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull State state, @NonNull State state2, @Nullable Metadata metadata) {
        switch (state2) {
            case CREATED:
            case ON_SCREEN:
            case IMPRESSED:
                return;
            case CLICKED:
                this.a.debug(LogDomain.AD, "event %s: going to send impression beacons", state2);
                f fVar = (f) getAdObject();
                this.b.trackBeaconUrls(fVar.getClickTrackingUrls(), fVar.getSomaApiContext());
                return;
            case COMPLETE:
            case TO_BE_DELETED:
                return;
            default:
                this.a.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    public final void resolveClickUrl(@NonNull final UrlResolveListener urlResolveListener) {
        Objects.requireNonNull(urlResolveListener, "Parameter urlResolveListener cannot be null for ImageAdInteractor::resolveClickUrl");
        if (this.d.get() == null) {
            f fVar = (f) getAdObject();
            Task handleClickThroughUrl = this.c.handleClickThroughUrl(fVar.getSomaApiContext(), fVar.getClickUrl(), new UrlResolveListener() {
                public final void onError() {
                    d.this.d.set(null);
                    urlResolveListener.onError();
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    d.this.d.set(null);
                    urlResolveListener.onSuccess(consumer);
                }
            });
            this.d.set(handleClickThroughUrl);
            handleClickThroughUrl.start();
        }
    }

    public final void stopUrlResolving() {
        Objects.onNotNull(this.d.get(), $$Lambda$yaUpY0DGpVsMZlshO_jwvMPRY4g.INSTANCE);
        this.d.set(null);
    }

    public final void setOnImpressionTriggered(@Nullable a aVar) {
        this.e = aVar;
    }
}
