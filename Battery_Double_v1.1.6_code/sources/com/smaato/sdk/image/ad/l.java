package com.smaato.sdk.image.ad;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnPreDrawListener;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.BaseAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter.Listener;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTracker;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.tracker.VisibilityTrackerListener;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.Timer;
import com.smaato.sdk.image.ad.d.a;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

final class l extends BaseAdPresenter implements InterstitialAdPresenter {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    /* access modifiers changed from: private */
    @NonNull
    public final d b;
    @NonNull
    private final VisibilityTrackerCreator c;
    /* access modifiers changed from: private */
    @NonNull
    public final AppBackgroundDetector d;
    /* access modifiers changed from: private */
    @NonNull
    public final Timer e;
    @NonNull
    private final AtomicReference<VisibilityTracker> f = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public WeakReference<Listener> g = new WeakReference<>(null);
    @NonNull
    private StateMachine.Listener<State> h;
    /* access modifiers changed from: private */
    @NonNull
    public final Timer.Listener i = new Timer.Listener() {
        public final void onTimePassed() {
            l.this.c();
        }
    };

    public final void setFriendlyObstructionView(@NonNull View view) {
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c() {
        Objects.onNotNull(this.g.get(), $$Lambda$nk0D4u8wrNMCMfE3WzHxisIX_k.INSTANCE);
    }

    l(@NonNull Logger logger, @NonNull d dVar, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull Timer timer, @NonNull AppBackgroundDetector appBackgroundDetector) {
        super(dVar);
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (d) Objects.requireNonNull(dVar);
        this.c = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.d = (AppBackgroundDetector) Objects.requireNonNull(appBackgroundDetector);
        this.e = (Timer) Objects.requireNonNull(timer);
        this.h = new StateMachine.Listener(dVar, logger) {
            private final /* synthetic */ d f$1;
            private final /* synthetic */ Logger f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                l.this.a(this.f$1, this.f$2, (State) obj, (State) obj2, metadata);
            }
        };
        dVar.addStateListener(this.h);
        dVar.setOnImpressionTriggered(new a() {
            public final void onImpressionTriggered() {
                l.this.b();
            }
        });
        dVar.onEvent(Event.INITIALISE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(d dVar, Logger logger, State state, State state2, Metadata metadata) {
        switch (state2) {
            case INIT:
            case CREATED:
            case ON_SCREEN:
            case IMPRESSED:
            case COMPLETE:
                return;
            case CLICKED:
                Objects.onNotNull(this.g.get(), new Consumer() {
                    public final void accept(Object obj) {
                        l.this.c((Listener) obj);
                    }
                });
                return;
            case TO_BE_DELETED:
                dVar.removeStateListener(this.h);
                return;
            default:
                logger.error(LogDomain.AD, "Unexpected type of new state: %s", state2);
                return;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                l.this.b((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdImpressed(this);
    }

    @NonNull
    public final AdContentView getAdContentView(@NonNull Context context) {
        final AtomicReference atomicReference = new AtomicReference(null);
        final com.smaato.sdk.image.ui.a create = com.smaato.sdk.image.ui.a.create(context, (f) this.b.getAdObject(), new OnClickListener() {
            private UrlResolveListener c = new UrlResolveListener() {
                public final void onError() {
                    Threads.runOnUi(new Runnable(this, atomicReference) {
                        private final /* synthetic */ AnonymousClass1 f$0;
                        private final /* synthetic */ AtomicReference f$1;

                        public final 
/*
Method generation error in method: com.smaato.sdk.image.ad.-$$Lambda$l$1$1$dG9i7c4eBSl6uHaTCe5_f3rqyF0.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.ClassGen.addInsnBody(ClassGen.java:435)
                        	at jadx.core.codegen.ClassGen.addField(ClassGen.java:376)
                        	at jadx.core.codegen.ClassGen.addFields(ClassGen.java:346)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:223)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(AtomicReference atomicReference) {
                    l.this.a.error(LogDomain.AD, "The url seems to be invalid", new Object[0]);
                    Objects.onNotNull(l.this.g.get(), new Consumer(this) {
                        private final /* synthetic */ AnonymousClass1 f$0;

                        public final 
/*
Method generation error in method: com.smaato.sdk.image.ad.-$$Lambda$l$1$1$EgmsN6zz3_aRozBaoNOc7KILzcA.accept(java.lang.Object):null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.ClassGen.addInsnBody(ClassGen.java:435)
                        	at jadx.core.codegen.ClassGen.addField(ClassGen.java:376)
                        	at jadx.core.codegen.ClassGen.addFields(ClassGen.java:346)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:223)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                    Objects.onNotNull(atomicReference.get(), $$Lambda$l$1$1$u43YmsPaubBN7u8gpdaCpGQXnaw.INSTANCE);
                }

                /* access modifiers changed from: private */
                public /* synthetic */ void a(Listener listener) {
                    listener.onAdError(l.this);
                }

                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    Threads.runOnUi(new Runnable(atomicReference, consumer) {
                        private final /* synthetic */ AtomicReference f$0;
                        private final /* synthetic */ Consumer f$1;

                        public final 
/*
Method generation error in method: com.smaato.sdk.image.ad.-$$Lambda$l$1$1$S-rgdi25hCsGz299_awd2K6xY1Y.run():null, dex: classes3.dex
                        java.lang.NullPointerException
                        	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
                        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.ClassGen.addInsnBody(ClassGen.java:435)
                        	at jadx.core.codegen.ClassGen.addField(ClassGen.java:376)
                        	at jadx.core.codegen.ClassGen.addFields(ClassGen.java:346)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:223)
                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
                        	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
                        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
                        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
                        
*/
                    });
                }

                /* access modifiers changed from: private */
                public static /* synthetic */ void a(Consumer consumer, com.smaato.sdk.image.ui.a aVar) {
                    consumer.accept(aVar.getContext());
                    aVar.showProgressIndicator(false);
                }
            };

            public final void onClick(View view) {
                if (l.this.d.isAppInBackground()) {
                    l.this.a.info(LogDomain.AD, "skipping click event, because app is in background", new Object[0]);
                    return;
                }
                ((com.smaato.sdk.image.ui.a) view).showProgressIndicator(true);
                l.this.b.resolveClickUrl(this.c);
                l.this.b.onEvent(Event.CLICK);
            }
        });
        atomicReference.set(create);
        this.f.set(this.c.createTracker(create, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                l.this.a();
            }
        }));
        create.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(View view) {
                l.this.b.onEvent(Event.ADDED_ON_SCREEN);
            }

            public final void onViewDetachedFromWindow(View view) {
                view.removeOnAttachStateChangeListener(this);
            }
        });
        create.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            public final boolean onPreDraw() {
                create.getViewTreeObserver().removeOnPreDrawListener(this);
                l.this.e.start(l.this.i);
                return true;
            }
        });
        return create;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        this.b.onEvent(Event.IMPRESSION);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onClose(this);
    }

    public final void onCloseClicked() {
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                l.this.a((Listener) obj);
            }
        });
    }

    public final void setListener(@Nullable Listener listener) {
        this.g = new WeakReference<>(listener);
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        this.b.onEvent(Event.DESTROY);
        this.b.stopUrlResolving();
        Objects.onNotNull(this.f.get(), new Consumer() {
            public final void accept(Object obj) {
                l.this.a((VisibilityTracker) obj);
            }
        });
        this.g.clear();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(VisibilityTracker visibilityTracker) {
        this.f.set(null);
        visibilityTracker.destroy();
    }
}
