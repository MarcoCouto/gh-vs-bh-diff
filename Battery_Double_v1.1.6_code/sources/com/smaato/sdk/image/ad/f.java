package com.smaato.sdk.image.ad;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.List;

public final class f implements AdObject {
    @NonNull
    private final SomaApiContext a;
    @NonNull
    private final String b;
    @NonNull
    private final Bitmap c;
    private final int d;
    private final int e;
    @NonNull
    private final String f;
    @NonNull
    private final List<String> g;
    @NonNull
    private final List<String> h;
    @Nullable
    private final Object i;

    public static final class a {
        private Bitmap a;
        private int b;
        private int c;
        private String d;
        private List<String> e;
        private List<String> f;
        private Object g;
        private SomaApiContext h;
        private String i;

        @NonNull
        public final a setImageUrl(@NonNull String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public final a setSomaApiContext(@NonNull SomaApiContext somaApiContext) {
            this.h = somaApiContext;
            return this;
        }

        @NonNull
        public final a setBitmap(@NonNull Bitmap bitmap) {
            this.a = bitmap;
            return this;
        }

        @NonNull
        public final a setWidth(int i2) {
            this.b = i2;
            return this;
        }

        @NonNull
        public final a setHeight(int i2) {
            this.c = i2;
            return this;
        }

        @NonNull
        public final a setClickUrl(@NonNull String str) {
            this.d = str;
            return this;
        }

        @NonNull
        public final a setImpressionTrackingUrls(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public final a setClickTrackingUrls(@Nullable List<String> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public final a setExtensions(@Nullable Object obj) {
            this.g = obj;
            return this;
        }

        @NonNull
        public final f build() {
            ArrayList arrayList = new ArrayList();
            if (this.h == null) {
                arrayList.add("somaApiContext");
            }
            if (this.i == null) {
                arrayList.add("imageUrl");
            }
            if (this.a == null) {
                arrayList.add("bitmap");
            }
            if (this.d == null) {
                arrayList.add("clickUrl");
            }
            if (this.e == null) {
                arrayList.add("impressionTrackingUrls");
            }
            if (this.f == null) {
                arrayList.add("clickTrackingUrls");
            }
            if (!arrayList.isEmpty()) {
                StringBuilder sb = new StringBuilder("Missing required parameter(s): ");
                sb.append(Joiner.join((CharSequence) ", ", (Iterable) arrayList));
                throw new IllegalStateException(sb.toString());
            } else if (this.e.isEmpty()) {
                throw new IllegalStateException("impressionTrackingUrls cannot be empty");
            } else if (!this.f.isEmpty()) {
                f fVar = new f(this.h, this.i, this.a, this.b, this.c, this.d, this.e, this.f, this.g, 0);
                return fVar;
            } else {
                throw new IllegalStateException("clickTrackingUrls cannot be empty");
            }
        }
    }

    /* synthetic */ f(SomaApiContext somaApiContext, String str, Bitmap bitmap, int i2, int i3, String str2, List list, List list2, Object obj, byte b2) {
        this(somaApiContext, str, bitmap, i2, i3, str2, list, list2, obj);
    }

    private f(@NonNull SomaApiContext somaApiContext, @NonNull String str, @NonNull Bitmap bitmap, int i2, int i3, @NonNull String str2, @NonNull List<String> list, @NonNull List<String> list2, @Nullable Object obj) {
        this.a = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.b = (String) Objects.requireNonNull(str);
        this.c = (Bitmap) Objects.requireNonNull(bitmap);
        this.d = i2;
        this.e = i3;
        this.f = (String) Objects.requireNonNull(str2);
        this.g = (List) Objects.requireNonNull(list);
        this.h = (List) Objects.requireNonNull(list2);
        this.i = obj;
    }

    @NonNull
    public final SomaApiContext getSomaApiContext() {
        return this.a;
    }

    @NonNull
    public final Bitmap getBitmap() {
        return this.c;
    }

    public final int getWidth() {
        return this.d;
    }

    public final int getHeight() {
        return this.e;
    }

    @NonNull
    public final String getClickUrl() {
        return this.f;
    }

    @NonNull
    public final List<String> getImpressionTrackingUrls() {
        return this.g;
    }

    @NonNull
    public final List<String> getClickTrackingUrls() {
        return this.h;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ImageAdObject{somaApiContext=");
        sb.append(this.a);
        sb.append(", imageUrl='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", bitmap=");
        sb.append(this.c);
        sb.append(", width=");
        sb.append(this.d);
        sb.append(", height=");
        sb.append(this.e);
        sb.append(", clickUrl='");
        sb.append(this.f);
        sb.append('\'');
        sb.append(", impressionTrackingUrls=");
        sb.append(this.g);
        sb.append(", clickTrackingUrls=");
        sb.append(this.h);
        sb.append(", extensions=");
        sb.append(this.i);
        sb.append('}');
        return sb.toString();
    }
}
