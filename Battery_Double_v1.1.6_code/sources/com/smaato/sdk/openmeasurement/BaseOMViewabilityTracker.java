package com.smaato.sdk.openmeasurement;

import android.view.View;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.analytics.ViewabilityTracker;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;

abstract class BaseOMViewabilityTracker<T extends ViewabilityTracker> implements ViewabilityTracker {
    @NonNull
    private final Logger a;
    @NonNull
    private final T b;

    BaseOMViewabilityTracker(@NonNull Logger logger, @NonNull T t) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (ViewabilityTracker) Objects.requireNonNull(t);
    }

    public final void startTracking() {
        a($$Lambda$OpOKEwdnWj6hOzcgmxWrQbiIROc.INSTANCE);
    }

    public final void stopTracking() {
        a($$Lambda$uDorTzt2PJ9zriW72C6ocYX_OxY.INSTANCE);
    }

    public final void trackImpression() {
        a($$Lambda$nkzhK3hMMJQO1nw8z4bB3V1PKfw.INSTANCE);
    }

    public final void registerFriendlyObstruction(@NonNull View view) {
        a(new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((ViewabilityTracker) obj).registerFriendlyObstruction(this.f$0);
            }
        });
    }

    public final void removeFriendlyObstruction(@NonNull View view) {
        a(new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((ViewabilityTracker) obj).removeFriendlyObstruction(this.f$0);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Consumer<T> consumer) {
        try {
            consumer.accept(this.b);
        } catch (IllegalArgumentException | IllegalStateException e) {
            this.a.error(LogDomain.OPEN_MEASUREMENT, e, "WebViewViewabilityTracker failed to perform action", new Object[0]);
        }
    }
}
