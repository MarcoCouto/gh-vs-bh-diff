package com.smaato.sdk.openmeasurement;

import com.iab.omid.library.smaato.adsession.video.InteractionType;
import com.iab.omid.library.smaato.adsession.video.VideoEvents;
import com.smaato.sdk.core.util.fi.Consumer;

/* renamed from: com.smaato.sdk.openmeasurement.-$$Lambda$OMVideoViewabilityTracker$fn-YG9C1R95EGinYVfhl0mEKBlY reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$OMVideoViewabilityTracker$fnYG9C1R95EGinYVfhl0mEKBlY implements Consumer {
    public static final /* synthetic */ $$Lambda$OMVideoViewabilityTracker$fnYG9C1R95EGinYVfhl0mEKBlY INSTANCE = new $$Lambda$OMVideoViewabilityTracker$fnYG9C1R95EGinYVfhl0mEKBlY();

    private /* synthetic */ $$Lambda$OMVideoViewabilityTracker$fnYG9C1R95EGinYVfhl0mEKBlY() {
    }

    public final void accept(Object obj) {
        ((VideoEvents) obj).adUserInteraction(InteractionType.CLICK);
    }
}
