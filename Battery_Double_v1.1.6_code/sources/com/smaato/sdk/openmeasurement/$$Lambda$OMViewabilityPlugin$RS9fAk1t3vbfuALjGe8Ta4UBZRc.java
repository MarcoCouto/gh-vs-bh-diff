package com.smaato.sdk.openmeasurement;

import android.app.Application;
import android.content.Context;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.util.AssetUtils;

/* renamed from: com.smaato.sdk.openmeasurement.-$$Lambda$OMViewabilityPlugin$RS9fAk1t3vbfuALjGe8Ta4UBZRc reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$OMViewabilityPlugin$RS9fAk1t3vbfuALjGe8Ta4UBZRc implements ClassFactory {
    public static final /* synthetic */ $$Lambda$OMViewabilityPlugin$RS9fAk1t3vbfuALjGe8Ta4UBZRc INSTANCE = new $$Lambda$OMViewabilityPlugin$RS9fAk1t3vbfuALjGe8Ta4UBZRc();

    private /* synthetic */ $$Lambda$OMViewabilityPlugin$RS9fAk1t3vbfuALjGe8Ta4UBZRc() {
    }

    public final Object get(DiConstructor diConstructor) {
        return AssetUtils.getFileFromAssets((Context) diConstructor.get(Application.class), DiLogLayer.getLoggerFrom(diConstructor), "omsdk-v1.js");
    }
}
