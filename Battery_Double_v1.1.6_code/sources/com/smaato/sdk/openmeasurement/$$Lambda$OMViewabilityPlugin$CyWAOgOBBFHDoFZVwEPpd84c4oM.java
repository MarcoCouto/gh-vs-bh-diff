package com.smaato.sdk.openmeasurement;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.openmeasurement.-$$Lambda$OMViewabilityPlugin$CyWAOgOBBFHDoFZVwEPpd84c4oM reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$OMViewabilityPlugin$CyWAOgOBBFHDoFZVwEPpd84c4oM implements ClassFactory {
    public static final /* synthetic */ $$Lambda$OMViewabilityPlugin$CyWAOgOBBFHDoFZVwEPpd84c4oM INSTANCE = new $$Lambda$OMViewabilityPlugin$CyWAOgOBBFHDoFZVwEPpd84c4oM();

    private /* synthetic */ $$Lambda$OMViewabilityPlugin$CyWAOgOBBFHDoFZVwEPpd84c4oM() {
    }

    public final Object get(DiConstructor diConstructor) {
        return OMViewabilityPlugin.f(diConstructor);
    }
}
