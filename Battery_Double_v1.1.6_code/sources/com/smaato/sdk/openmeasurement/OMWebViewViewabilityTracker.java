package com.smaato.sdk.openmeasurement;

import android.view.View;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.iab.omid.library.smaato.adsession.AdEvents;
import com.iab.omid.library.smaato.adsession.AdSession;
import com.iab.omid.library.smaato.adsession.AdSessionConfiguration;
import com.iab.omid.library.smaato.adsession.AdSessionContext;
import com.iab.omid.library.smaato.adsession.Owner;
import com.iab.omid.library.smaato.adsession.Partner;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;

public final class OMWebViewViewabilityTracker implements WebViewViewabilityTracker {
    @NonNull
    private final Partner a;
    @Nullable
    private AdSession b;
    @Nullable
    private AdEvents c;

    OMWebViewViewabilityTracker(@NonNull Partner partner) {
        this.a = (Partner) Objects.requireNonNull(partner);
    }

    public final void registerAdView(@NonNull WebView webView) {
        this.b = AdSession.createAdSession(AdSessionConfiguration.createAdSessionConfiguration(Owner.NATIVE, null, false), AdSessionContext.createHtmlAdSessionContext(this.a, webView, ""));
        this.b.registerAdView(webView);
        this.c = AdEvents.createAdEvents(this.b);
    }

    public final void updateAdView(@NonNull WebView webView) {
        Objects.onNotNull(this.b, new Consumer(webView) {
            private final /* synthetic */ WebView f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((AdSession) obj).registerAdView(this.f$0);
            }
        });
    }

    public final void startTracking() {
        Objects.onNotNull(this.b, $$Lambda$UeW1rIvgKn_eYOVfT2yqI1ACPM.INSTANCE);
    }

    public final void stopTracking() {
        Objects.onNotNull(this.b, new Consumer() {
            public final void accept(Object obj) {
                OMWebViewViewabilityTracker.this.a((AdSession) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdSession adSession) {
        adSession.finish();
        this.b = null;
        this.c = null;
    }

    public final void trackImpression() {
        Objects.onNotNull(this.c, $$Lambda$grKjDTBS0Q9SYAhDXaZ3lPHax9E.INSTANCE);
    }

    public final void registerFriendlyObstruction(@NonNull View view) {
        Objects.onNotNull(this.b, new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((AdSession) obj).addFriendlyObstruction(this.f$0);
            }
        });
    }

    public final void removeFriendlyObstruction(@NonNull View view) {
        Objects.onNotNull(this.b, new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((AdSession) obj).removeFriendlyObstruction(this.f$0);
            }
        });
    }
}
