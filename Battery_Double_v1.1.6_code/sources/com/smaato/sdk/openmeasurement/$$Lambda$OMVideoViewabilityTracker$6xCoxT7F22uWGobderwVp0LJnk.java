package com.smaato.sdk.openmeasurement;

import com.iab.omid.library.smaato.adsession.video.PlayerState;
import com.iab.omid.library.smaato.adsession.video.VideoEvents;
import com.smaato.sdk.core.util.fi.Consumer;

/* renamed from: com.smaato.sdk.openmeasurement.-$$Lambda$OMVideoViewabilityTracker$6xCoxT7F22uWG-obderwVp0LJnk reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$OMVideoViewabilityTracker$6xCoxT7F22uWGobderwVp0LJnk implements Consumer {
    public static final /* synthetic */ $$Lambda$OMVideoViewabilityTracker$6xCoxT7F22uWGobderwVp0LJnk INSTANCE = new $$Lambda$OMVideoViewabilityTracker$6xCoxT7F22uWGobderwVp0LJnk();

    private /* synthetic */ $$Lambda$OMVideoViewabilityTracker$6xCoxT7F22uWGobderwVp0LJnk() {
    }

    public final void accept(Object obj) {
        ((VideoEvents) obj).playerStateChange(PlayerState.FULLSCREEN);
    }
}
