package com.smaato.sdk.openmeasurement;

import android.content.Context;
import androidx.annotation.NonNull;
import com.iab.omid.library.smaato.Omid;
import com.iab.omid.library.smaato.adsession.Partner;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.analytics.ViewabilityPlugin;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;

public final class OMViewabilityPlugin implements ViewabilityPlugin {
    @NonNull
    public final String getName() {
        return "omid";
    }

    public final void init(@NonNull Context context) {
        Threads.runOnUi(new Runnable(context) {
            private final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            public final void run() {
                Omid.activateWithOmidApiVersion(Omid.getVersion(), this.f$0);
            }
        });
    }

    @NonNull
    public final DiRegistry diRegistry() {
        return DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                OMViewabilityPlugin.this.a((DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(Partner.class, $$Lambda$OMViewabilityPlugin$SQ2OGyQjsfBWAfRu2JwMxmQONE.INSTANCE);
        diRegistry.registerFactory(getName(), WebViewViewabilityTracker.class, $$Lambda$OMViewabilityPlugin$CyWAOgOBBFHDoFZVwEPpd84c4oM.INSTANCE);
        diRegistry.registerFactory(OMWebViewViewabilityTracker.class, $$Lambda$OMViewabilityPlugin$4xBA_g3mqfgHjxLffp51LrIZY.INSTANCE);
        diRegistry.registerFactory(getName(), VideoViewabilityTracker.class, $$Lambda$OMViewabilityPlugin$BZW6C8_XghrHooerpwpRRwmD9gM.INSTANCE);
        diRegistry.registerFactory(OMVideoViewabilityTracker.class, $$Lambda$OMViewabilityPlugin$HNxyYGxJDW88qIvy92Xov6GRMb4.INSTANCE);
        diRegistry.registerFactory(OMVideoResourceMapper.class, $$Lambda$OMViewabilityPlugin$i4NCsNrlb1Tm7MPXSHeOzLpIDTY.INSTANCE);
        diRegistry.registerSingletonFactory("OMID_JS", String.class, $$Lambda$OMViewabilityPlugin$RS9fAk1t3vbfuALjGe8Ta4UBZRc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ WebViewViewabilityTracker f(DiConstructor diConstructor) {
        return new OMWebViewViewabilityTrackerDecorator(DiLogLayer.getLoggerFrom(diConstructor), (OMWebViewViewabilityTracker) diConstructor.get(OMWebViewViewabilityTracker.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ OMWebViewViewabilityTracker e(DiConstructor diConstructor) {
        return new OMWebViewViewabilityTracker((Partner) diConstructor.get(Partner.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoViewabilityTracker d(DiConstructor diConstructor) {
        return new OMVideoViewabilityTrackerDecorator(DiLogLayer.getLoggerFrom(diConstructor), (OMVideoViewabilityTracker) diConstructor.get(OMVideoViewabilityTracker.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ OMVideoViewabilityTracker c(DiConstructor diConstructor) {
        return new OMVideoViewabilityTracker((Partner) diConstructor.get(Partner.class), (String) diConstructor.get("OMID_JS", String.class), "", (OMVideoResourceMapper) diConstructor.get(OMVideoResourceMapper.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ OMVideoResourceMapper b(DiConstructor diConstructor) {
        return new OMVideoResourceMapper("omid");
    }
}
