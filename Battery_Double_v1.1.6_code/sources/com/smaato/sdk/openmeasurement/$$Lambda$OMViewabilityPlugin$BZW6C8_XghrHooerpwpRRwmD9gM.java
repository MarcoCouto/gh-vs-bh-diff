package com.smaato.sdk.openmeasurement;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.openmeasurement.-$$Lambda$OMViewabilityPlugin$BZW6C8_XghrHooerpwpRRwmD9gM reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$OMViewabilityPlugin$BZW6C8_XghrHooerpwpRRwmD9gM implements ClassFactory {
    public static final /* synthetic */ $$Lambda$OMViewabilityPlugin$BZW6C8_XghrHooerpwpRRwmD9gM INSTANCE = new $$Lambda$OMViewabilityPlugin$BZW6C8_XghrHooerpwpRRwmD9gM();

    private /* synthetic */ $$Lambda$OMViewabilityPlugin$BZW6C8_XghrHooerpwpRRwmD9gM() {
    }

    public final Object get(DiConstructor diConstructor) {
        return OMViewabilityPlugin.d(diConstructor);
    }
}
