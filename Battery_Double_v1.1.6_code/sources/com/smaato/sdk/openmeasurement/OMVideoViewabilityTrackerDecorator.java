package com.smaato.sdk.openmeasurement;

import android.view.View;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker.VideoProps;
import com.smaato.sdk.core.analytics.ViewabilityVerificationResource;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.List;
import java.util.Map;

public final class OMVideoViewabilityTrackerDecorator extends BaseOMViewabilityTracker<OMVideoViewabilityTracker> implements VideoViewabilityTracker {
    OMVideoViewabilityTrackerDecorator(@NonNull Logger logger, @NonNull OMVideoViewabilityTracker oMVideoViewabilityTracker) {
        super(logger, oMVideoViewabilityTracker);
    }

    public final void registerAdView(@NonNull View view, @NonNull Map<String, List<ViewabilityVerificationResource>> map) {
        a(new Consumer(view, map) {
            private final /* synthetic */ View f$0;
            private final /* synthetic */ Map f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((OMVideoViewabilityTracker) obj).registerAdView(this.f$0, this.f$1);
            }
        });
    }

    public final void trackPlayerStateChange() {
        a($$Lambda$4qKMuQEOoaGeek_oM2xDIVuPOLA.INSTANCE);
    }

    public final void trackVideoClicked() {
        a($$Lambda$L0jRFJrdHwKQyN2E4L6E6XK7vJc.INSTANCE);
    }

    public final void trackLoaded(@NonNull VideoProps videoProps) {
        a(new Consumer() {
            public final void accept(Object obj) {
                ((OMVideoViewabilityTracker) obj).trackLoaded(VideoProps.this);
            }
        });
    }

    public final void trackStarted(float f, float f2) {
        a(new Consumer(f, f2) {
            private final /* synthetic */ float f$0;
            private final /* synthetic */ float f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((OMVideoViewabilityTracker) obj).trackStarted(this.f$0, this.f$1);
            }
        });
    }

    public final void trackFirstQuartile() {
        a($$Lambda$cEg4gfOYGoIFgsSuVmU5IshiaE8.INSTANCE);
    }

    public final void trackMidPoint() {
        a($$Lambda$_hdvW7TDhmbOKm4HMMEP_4l4BXM.INSTANCE);
    }

    public final void trackThirdQuartile() {
        a($$Lambda$pkK2nP7EPxvdUkabehuXd34fET4.INSTANCE);
    }

    public final void trackCompleted() {
        a($$Lambda$s0IdYArUcGIM_6jHJwQNOSyaFd8.INSTANCE);
    }

    public final void trackPaused() {
        a($$Lambda$Yv6ZtJtUPzseXx4mA_G53A2NhN0.INSTANCE);
    }

    public final void trackResumed() {
        a($$Lambda$4Fy7zEXR_nC_2pgJfTRAD774jcE.INSTANCE);
    }

    public final void trackBufferStart() {
        a($$Lambda$4qG35F7vq1hWzpQ22RJjfCcXkYs.INSTANCE);
    }

    public final void trackBufferFinish() {
        a($$Lambda$3dhSIQ1nP1Asl3hve4kg7o7ESg.INSTANCE);
    }

    public final void trackPlayerVolumeChanged(float f) {
        a(new Consumer(f) {
            private final /* synthetic */ float f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((OMVideoViewabilityTracker) obj).trackPlayerVolumeChanged(this.f$0);
            }
        });
    }

    public final void trackSkipped() {
        a($$Lambda$dqYaXbrlhToa1MzlQUu_5ksU.INSTANCE);
    }
}
