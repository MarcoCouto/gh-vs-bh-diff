package com.smaato.sdk.openmeasurement;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.openmeasurement.-$$Lambda$OMViewabilityPlugin$HNxyYGxJDW88qIvy92Xov6GRMb4 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$OMViewabilityPlugin$HNxyYGxJDW88qIvy92Xov6GRMb4 implements ClassFactory {
    public static final /* synthetic */ $$Lambda$OMViewabilityPlugin$HNxyYGxJDW88qIvy92Xov6GRMb4 INSTANCE = new $$Lambda$OMViewabilityPlugin$HNxyYGxJDW88qIvy92Xov6GRMb4();

    private /* synthetic */ $$Lambda$OMViewabilityPlugin$HNxyYGxJDW88qIvy92Xov6GRMb4() {
    }

    public final Object get(DiConstructor diConstructor) {
        return OMViewabilityPlugin.c(diConstructor);
    }
}
