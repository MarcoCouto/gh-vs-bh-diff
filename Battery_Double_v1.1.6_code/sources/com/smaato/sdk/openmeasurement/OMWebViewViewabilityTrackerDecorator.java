package com.smaato.sdk.openmeasurement;

import android.webkit.WebView;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.analytics.WebViewViewabilityTracker;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.fi.Consumer;

public final class OMWebViewViewabilityTrackerDecorator extends BaseOMViewabilityTracker<OMWebViewViewabilityTracker> implements WebViewViewabilityTracker {
    OMWebViewViewabilityTrackerDecorator(@NonNull Logger logger, @NonNull OMWebViewViewabilityTracker oMWebViewViewabilityTracker) {
        super(logger, oMWebViewViewabilityTracker);
    }

    public final void registerAdView(@NonNull WebView webView) {
        a(new Consumer(webView) {
            private final /* synthetic */ WebView f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((OMWebViewViewabilityTracker) obj).registerAdView(this.f$0);
            }
        });
    }

    public final void updateAdView(@NonNull WebView webView) {
        a(new Consumer(webView) {
            private final /* synthetic */ WebView f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((OMWebViewViewabilityTracker) obj).updateAdView(this.f$0);
            }
        });
    }
}
