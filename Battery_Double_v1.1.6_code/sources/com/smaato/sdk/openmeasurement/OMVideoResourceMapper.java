package com.smaato.sdk.openmeasurement;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.iab.omid.library.smaato.adsession.VerificationScriptResource;
import com.smaato.sdk.core.analytics.ViewabilityVerificationResource;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Function;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

final class OMVideoResourceMapper implements Function<List<ViewabilityVerificationResource>, List<VerificationScriptResource>> {
    @NonNull
    private final String a;

    OMVideoResourceMapper(@NonNull String str) {
        this.a = (String) Objects.requireNonNull(str);
    }

    @NonNull
    public final List<VerificationScriptResource> apply(@NonNull List<ViewabilityVerificationResource> list) {
        VerificationScriptResource verificationScriptResource;
        ArrayList arrayList = new ArrayList();
        for (ViewabilityVerificationResource viewabilityVerificationResource : list) {
            if (viewabilityVerificationResource.getApiFramework().equals(this.a) && viewabilityVerificationResource.isNoBrowser()) {
                URL a2 = a(viewabilityVerificationResource.getJsScriptUrl());
                if (a2 != null) {
                    String vendor = viewabilityVerificationResource.getVendor();
                    String parameters = viewabilityVerificationResource.getParameters();
                    if (TextUtils.isEmpty(parameters) && TextUtils.isEmpty(vendor)) {
                        verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithoutParameters(a2);
                    } else if (TextUtils.isEmpty(parameters)) {
                        verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithoutParameters(vendor, a2);
                    } else {
                        verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithParameters(vendor, a2, parameters);
                    }
                    arrayList.add(verificationScriptResource);
                }
            }
        }
        return arrayList;
    }

    @Nullable
    private static URL a(@NonNull String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException unused) {
            return null;
        }
    }
}
