package com.smaato.sdk.openmeasurement;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.openmeasurement.-$$Lambda$OMViewabilityPlugin$i4NCsNrlb1Tm7MPXSHeOzLpIDTY reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$OMViewabilityPlugin$i4NCsNrlb1Tm7MPXSHeOzLpIDTY implements ClassFactory {
    public static final /* synthetic */ $$Lambda$OMViewabilityPlugin$i4NCsNrlb1Tm7MPXSHeOzLpIDTY INSTANCE = new $$Lambda$OMViewabilityPlugin$i4NCsNrlb1Tm7MPXSHeOzLpIDTY();

    private /* synthetic */ $$Lambda$OMViewabilityPlugin$i4NCsNrlb1Tm7MPXSHeOzLpIDTY() {
    }

    public final Object get(DiConstructor diConstructor) {
        return OMViewabilityPlugin.b(diConstructor);
    }
}
