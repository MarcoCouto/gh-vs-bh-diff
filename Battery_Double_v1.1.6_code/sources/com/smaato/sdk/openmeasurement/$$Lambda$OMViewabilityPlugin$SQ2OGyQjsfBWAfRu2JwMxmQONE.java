package com.smaato.sdk.openmeasurement;

import com.iab.omid.library.smaato.adsession.Partner;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.openmeasurement.-$$Lambda$OMViewabilityPlugin$SQ2OGyQjsfBWAfRu2JwMxmQO-NE reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$OMViewabilityPlugin$SQ2OGyQjsfBWAfRu2JwMxmQONE implements ClassFactory {
    public static final /* synthetic */ $$Lambda$OMViewabilityPlugin$SQ2OGyQjsfBWAfRu2JwMxmQONE INSTANCE = new $$Lambda$OMViewabilityPlugin$SQ2OGyQjsfBWAfRu2JwMxmQONE();

    private /* synthetic */ $$Lambda$OMViewabilityPlugin$SQ2OGyQjsfBWAfRu2JwMxmQONE() {
    }

    public final Object get(DiConstructor diConstructor) {
        return Partner.createPartner("Smaato", "21.3.1");
    }
}
