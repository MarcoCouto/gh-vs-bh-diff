package com.smaato.sdk.openmeasurement;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.iab.omid.library.smaato.adsession.AdEvents;
import com.iab.omid.library.smaato.adsession.AdSession;
import com.iab.omid.library.smaato.adsession.AdSessionConfiguration;
import com.iab.omid.library.smaato.adsession.AdSessionContext;
import com.iab.omid.library.smaato.adsession.Owner;
import com.iab.omid.library.smaato.adsession.Partner;
import com.iab.omid.library.smaato.adsession.video.Position;
import com.iab.omid.library.smaato.adsession.video.VastProperties;
import com.iab.omid.library.smaato.adsession.video.VideoEvents;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker.VideoProps;
import com.smaato.sdk.core.analytics.ViewabilityVerificationResource;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class OMVideoViewabilityTracker implements VideoViewabilityTracker {
    @NonNull
    private final Partner a;
    @NonNull
    private final String b;
    @NonNull
    private final String c;
    @NonNull
    private final OMVideoResourceMapper d;
    @Nullable
    private AdSession e;
    @Nullable
    private VideoEvents f;
    @Nullable
    private AdEvents g;

    OMVideoViewabilityTracker(@NonNull Partner partner, @NonNull String str, @NonNull String str2, @NonNull OMVideoResourceMapper oMVideoResourceMapper) {
        this.a = (Partner) Objects.requireNonNull(partner);
        this.b = (String) Objects.requireNonNull(str);
        this.c = (String) Objects.requireNonNull(str2);
        this.d = (OMVideoResourceMapper) Objects.requireNonNull(oMVideoResourceMapper);
    }

    public final void registerAdView(@NonNull View view, @NonNull Map<String, List<ViewabilityVerificationResource>> map) {
        Owner owner = Owner.NATIVE;
        AdSessionConfiguration createAdSessionConfiguration = AdSessionConfiguration.createAdSessionConfiguration(owner, owner, false);
        List list = (List) map.get("omid");
        Partner partner = this.a;
        String str = this.b;
        OMVideoResourceMapper oMVideoResourceMapper = this.d;
        if (list == null) {
            list = Collections.emptyList();
        }
        this.e = AdSession.createAdSession(createAdSessionConfiguration, AdSessionContext.createNativeAdSessionContext(partner, str, oMVideoResourceMapper.apply(list), this.c));
        this.e.registerAdView(view);
        this.g = AdEvents.createAdEvents(this.e);
        this.f = VideoEvents.createVideoEvents(this.e);
    }

    public final void trackPlayerStateChange() {
        Objects.onNotNull(this.f, $$Lambda$OMVideoViewabilityTracker$6xCoxT7F22uWGobderwVp0LJnk.INSTANCE);
    }

    public final void trackVideoClicked() {
        Objects.onNotNull(this.f, $$Lambda$OMVideoViewabilityTracker$fnYG9C1R95EGinYVfhl0mEKBlY.INSTANCE);
    }

    public final void startTracking() {
        Objects.onNotNull(this.e, $$Lambda$UeW1rIvgKn_eYOVfT2yqI1ACPM.INSTANCE);
    }

    public final void trackLoaded(@NonNull VideoProps videoProps) {
        Objects.onNotNull(this.f, new Consumer() {
            public final void accept(Object obj) {
                OMVideoViewabilityTracker.a(VideoProps.this, (VideoEvents) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(VideoProps videoProps, VideoEvents videoEvents) {
        VastProperties vastProperties;
        if (videoProps.isSkippable) {
            float f2 = videoProps.skipOffset;
            videoProps.getClass();
            vastProperties = VastProperties.createVastPropertiesForSkippableVideo(f2, true, Position.STANDALONE);
        } else {
            videoProps.getClass();
            vastProperties = VastProperties.createVastPropertiesForNonSkippableVideo(true, Position.STANDALONE);
        }
        videoEvents.loaded(vastProperties);
    }

    public final void trackImpression() {
        Objects.onNotNull(this.g, $$Lambda$grKjDTBS0Q9SYAhDXaZ3lPHax9E.INSTANCE);
    }

    public final void stopTracking() {
        Objects.onNotNull(this.e, new Consumer() {
            public final void accept(Object obj) {
                OMVideoViewabilityTracker.this.a((AdSession) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdSession adSession) {
        adSession.finish();
        this.e = null;
        this.g = null;
    }

    public final void trackStarted(float f2, float f3) {
        Objects.onNotNull(this.f, new Consumer(f2, f3) {
            private final /* synthetic */ float f$0;
            private final /* synthetic */ float f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((VideoEvents) obj).start(this.f$0, this.f$1);
            }
        });
    }

    public final void trackFirstQuartile() {
        Objects.onNotNull(this.f, $$Lambda$iJzM379Wq_J2A8snUC3ROUoCSTk.INSTANCE);
    }

    public final void trackMidPoint() {
        Objects.onNotNull(this.f, $$Lambda$ZaNzf5LOL_cv0VW1KiNMKAUSjjA.INSTANCE);
    }

    public final void trackThirdQuartile() {
        Objects.onNotNull(this.f, $$Lambda$hDzJ1kZap6NlLAo2Oo_X_nVdmZg.INSTANCE);
    }

    public final void trackCompleted() {
        Objects.onNotNull(this.f, $$Lambda$fOSXaWdxoL8vll06wRbS5UyuV50.INSTANCE);
    }

    public final void trackPaused() {
        Objects.onNotNull(this.f, $$Lambda$oxEwTzdNdgb0gvpJNvJGhnFzCuQ.INSTANCE);
    }

    public final void trackResumed() {
        Objects.onNotNull(this.f, $$Lambda$9OCyTWa1GGhPQwoWo0_ATgzpsqo.INSTANCE);
    }

    public final void trackBufferStart() {
        Objects.onNotNull(this.f, $$Lambda$pA4Tibpn1Wupy1RNrnV6F0x7xyo.INSTANCE);
    }

    public final void trackBufferFinish() {
        Objects.onNotNull(this.f, $$Lambda$k_j5Rgx638jBRt98_HLW8NU7EPA.INSTANCE);
    }

    public final void trackPlayerVolumeChanged(float f2) {
        Objects.onNotNull(this.f, new Consumer(f2) {
            private final /* synthetic */ float f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((VideoEvents) obj).volumeChange(this.f$0);
            }
        });
    }

    public final void trackSkipped() {
        Objects.onNotNull(this.f, $$Lambda$O3bbCF9uGmtUAf5UGshhLudtbQA.INSTANCE);
    }

    public final void registerFriendlyObstruction(@NonNull View view) {
        Objects.onNotNull(this.e, new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((AdSession) obj).addFriendlyObstruction(this.f$0);
            }
        });
    }

    public final void removeFriendlyObstruction(@NonNull View view) {
        Objects.onNotNull(this.e, new Consumer(view) {
            private final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((AdSession) obj).removeFriendlyObstruction(this.f$0);
            }
        });
    }
}
