package com.smaato.sdk.video.fi;

import androidx.annotation.NonNull;

@FunctionalInterface
public interface CheckedFunction<T, R> {
    @NonNull
    R apply(@NonNull T t) throws Exception;
}
