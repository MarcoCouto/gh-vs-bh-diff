package com.smaato.sdk.video.fi;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public final class NonNullConsumerUtils {
    private NonNullConsumerUtils() {
    }

    @NonNull
    public static <T> NonNullConsumer<T> andThen(@NonNull NonNullConsumer<? super T> nonNullConsumer, @NonNull NonNullConsumer<? super T> nonNullConsumer2) {
        Objects.requireNonNull(nonNullConsumer2);
        return new NonNullConsumer(nonNullConsumer2) {
            private final /* synthetic */ NonNullConsumer f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                NonNullConsumerUtils.a(NonNullConsumer.this, this.f$1, obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(NonNullConsumer nonNullConsumer, NonNullConsumer nonNullConsumer2, Object obj) {
        nonNullConsumer.accept(obj);
        nonNullConsumer2.accept(obj);
    }
}
