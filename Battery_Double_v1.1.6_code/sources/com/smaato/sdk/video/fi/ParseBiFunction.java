package com.smaato.sdk.video.fi;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

@FunctionalInterface
public interface ParseBiFunction<T, U, R> {
    @NonNull
    R apply(@NonNull T t, @Nullable U u);
}
