package com.smaato.sdk.video.fi;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;

public final class CheckedFunctionUtils {
    /* access modifiers changed from: private */
    public static /* synthetic */ Object a(Object obj) throws Exception {
        return obj;
    }

    private CheckedFunctionUtils() {
    }

    public static <T> CheckedFunction<T, T> identity() {
        return $$Lambda$CheckedFunctionUtils$hmFVINYi4Ffr0NvBZG_9REJO8Ls.INSTANCE;
    }

    public static <V, T, R> CheckedFunction<V, R> compose(@NonNull CheckedFunction<? super T, ? extends R> checkedFunction, @NonNull CheckedFunction<? super V, ? extends T> checkedFunction2) {
        Objects.requireNonNull(checkedFunction2);
        return new CheckedFunction(checkedFunction2) {
            private final /* synthetic */ CheckedFunction f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return CheckedFunction.this.apply(this.f$1.apply(obj));
            }
        };
    }

    public static <V, T, R> CheckedFunction<T, V> andThen(@NonNull CheckedFunction<? super T, ? extends R> checkedFunction, @NonNull CheckedFunction<? super R, ? extends V> checkedFunction2) {
        Objects.requireNonNull(checkedFunction2);
        return new CheckedFunction(checkedFunction) {
            private final /* synthetic */ CheckedFunction f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return CheckedFunction.this.apply(this.f$1.apply(obj));
            }
        };
    }
}
