package com.smaato.sdk.video.framework;

import androidx.annotation.NonNull;

public final class VideoDiNames {
    @NonNull
    public static final String MODULE_DI_NAME = "VideoModuleInterface";
    @NonNull
    public static final String VIDEO_RESOURCE_LOADER_DI_NAME = "VideoModuleInterfaceVideoResource";

    private VideoDiNames() {
    }
}
