package com.smaato.sdk.video.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.video.framework.-$$Lambda$VideoModuleInterface$r4VBXOhjZITrCiHjX-OWBSP_l1k reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VideoModuleInterface$r4VBXOhjZITrCiHjXOWBSP_l1k implements ClassFactory {
    public static final /* synthetic */ $$Lambda$VideoModuleInterface$r4VBXOhjZITrCiHjXOWBSP_l1k INSTANCE = new $$Lambda$VideoModuleInterface$r4VBXOhjZITrCiHjXOWBSP_l1k();

    private /* synthetic */ $$Lambda$VideoModuleInterface$r4VBXOhjZITrCiHjXOWBSP_l1k() {
    }

    public final Object get(DiConstructor diConstructor) {
        return VideoModuleInterface.v(diConstructor);
    }
}
