package com.smaato.sdk.video.framework;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.DiAdLayer;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.configcheck.ExpectedManifestEntries;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.CoreDiNames;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.AdPresenterModuleInterface;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.resourceloader.DiResourceLoaderLayer;
import com.smaato.sdk.core.resourceloader.NetworkResourceStreamPreparationStrategy;
import com.smaato.sdk.core.resourceloader.PersistingStrategy;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.resourceloader.ResourceLoadingNetworkTaskCreator;
import com.smaato.sdk.core.resourceloader.ResourceTransformer;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.core.webview.BaseWebChromeClient;
import com.smaato.sdk.core.webview.BaseWebViewClient;
import com.smaato.sdk.video.ad.DiVideoAdLayer;
import com.smaato.sdk.video.ad.MediaFileResourceLoaderListenerCreator;
import com.smaato.sdk.video.ad.VastErrorTrackerCreator;
import com.smaato.sdk.video.ad.VideoAdLoaderPlugin;
import com.smaato.sdk.video.ad.VideoStateMachineFactory;
import com.smaato.sdk.video.network.StaticWrapperLoaderExecutioner;
import com.smaato.sdk.video.network.VastParsingConsumerFactory;
import com.smaato.sdk.video.resourceloader.VideoNetworkResourceStreamPreparationStrategy;
import com.smaato.sdk.video.resourceloader.VideoPersistingStrategy;
import com.smaato.sdk.video.resourceloader.VideoResourceTransformer;
import com.smaato.sdk.video.vast.browser.VastWebComponentSecurityPolicy;
import com.smaato.sdk.video.vast.build.InLineAdContainerPicker;
import com.smaato.sdk.video.vast.build.InLineChecker;
import com.smaato.sdk.video.vast.build.VastCompanionPicker;
import com.smaato.sdk.video.vast.build.VastCompanionScenarioMapper;
import com.smaato.sdk.video.vast.build.VastCompanionScenarioMerger;
import com.smaato.sdk.video.vast.build.VastIconScenarioMapper;
import com.smaato.sdk.video.vast.build.VastIconScenarioPicker;
import com.smaato.sdk.video.vast.build.VastLinearMediaFilePicker;
import com.smaato.sdk.video.vast.build.VastMediaFileScenarioMapper;
import com.smaato.sdk.video.vast.build.VastMediaFileScenarioMerger;
import com.smaato.sdk.video.vast.build.VastMediaFileScenarioWrapperMapper;
import com.smaato.sdk.video.vast.build.VastScenarioCreativeDataMapper;
import com.smaato.sdk.video.vast.build.VastScenarioMapper;
import com.smaato.sdk.video.vast.build.VastScenarioPicker;
import com.smaato.sdk.video.vast.build.VastScenarioWrapperMapper;
import com.smaato.sdk.video.vast.build.VastScenarioWrapperMerger;
import com.smaato.sdk.video.vast.build.VastTreeBuilder;
import com.smaato.sdk.video.vast.build.VastWrapperCompanionScenarioPicker;
import com.smaato.sdk.video.vast.build.VideoClicksMerger;
import com.smaato.sdk.video.vast.build.ViewableImpressionMerger;
import com.smaato.sdk.video.vast.build.WrapperAdContainerPicker;
import com.smaato.sdk.video.vast.build.WrapperLoader;
import com.smaato.sdk.video.vast.build.WrapperLoaderErrorMapper;
import com.smaato.sdk.video.vast.build.WrapperResolver;
import com.smaato.sdk.video.vast.config.DefaultWrapperResolverConfig;
import com.smaato.sdk.video.vast.config.WrapperResolverConfig;
import com.smaato.sdk.video.vast.parser.RegistryXmlParser;
import com.smaato.sdk.video.vast.parser.RegistryXmlParserFactory;
import com.smaato.sdk.video.vast.parser.VastResponseParser;
import com.smaato.sdk.video.vast.player.DiPlayerLayer;
import com.smaato.sdk.video.vast.tracking.macro.DiMacros;
import com.smaato.sdk.video.vast.tracking.macro.MacrosInjectorProviderFunction;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.xmlpull.v1.XmlPullParser;

public class VideoModuleInterface implements AdPresenterModuleInterface {
    @NonNull
    public String moduleDiName() {
        return VideoDiNames.MODULE_DI_NAME;
    }

    @NonNull
    public String version() {
        return "21.3.1";
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ WrapperResolverConfig R(DiConstructor diConstructor) {
        return new DefaultWrapperResolverConfig();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void k(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(StaticWrapperLoaderExecutioner.class, $$Lambda$VideoModuleInterface$o9zjz7UYNt4AC9Ar_kIlxzM2bec.INSTANCE);
        diRegistry.registerFactory(VastParsingConsumerFactory.class, $$Lambda$VideoModuleInterface$RlVaZHijul7vl1oP9zDvGVoDPXM.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ StaticWrapperLoaderExecutioner Q(DiConstructor diConstructor) {
        return new StaticWrapperLoaderExecutioner(DiNetworkLayer.getNetworkActionsFrom(diConstructor), (VastParsingConsumerFactory) diConstructor.get(VastParsingConsumerFactory.class), Executors.newSingleThreadExecutor());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastParsingConsumerFactory P(DiConstructor diConstructor) {
        return new VastParsingConsumerFactory((Logger) diConstructor.get(Logger.class), (VastResponseParser) diConstructor.get(VastResponseParser.class), ErrorMapper.STANDARD);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void i(DiRegistry diRegistry) {
        diRegistry.registerFactory(VastWebComponentSecurityPolicy.class, $$Lambda$VideoModuleInterface$E3U3tQMU6PnInm48gAVxADPlYo.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), BaseWebViewClient.class, $$Lambda$VideoModuleInterface$8sRLdFe0aviQ6fWWEjJIeeh1uk.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), BaseWebChromeClient.class, $$Lambda$VideoModuleInterface$RkAbIgs3HVQVqOxq7zUdlMJzoQ.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastWebComponentSecurityPolicy O(DiConstructor diConstructor) {
        return new VastWebComponentSecurityPolicy((Logger) diConstructor.get(Logger.class), CoreDiNames.SOMA_API_URL, DiNetworkLayer.getUrlCreatorFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BaseWebViewClient N(DiConstructor diConstructor) {
        return new BaseWebViewClient();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BaseWebChromeClient M(DiConstructor diConstructor) {
        return new BaseWebChromeClient();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void h(DiRegistry diRegistry) {
        diRegistry.registerFactory(moduleDiName(), VisibilityPrivateConfig.class, $$Lambda$VideoModuleInterface$Xhpp1wqWZD2AqPz_EgxrTPbaso.INSTANCE);
        diRegistry.registerFactory(moduleDiName(), VisibilityTrackerCreator.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return VideoModuleInterface.this.K(diConstructor);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ VisibilityTrackerCreator K(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get(moduleDiName(), VisibilityPrivateConfig.class);
        VisibilityTrackerCreator visibilityTrackerCreator = new VisibilityTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), moduleDiName());
        return visibilityTrackerCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void g(DiRegistry diRegistry) {
        diRegistry.registerFactory(VideoClicksMerger.class, $$Lambda$VideoModuleInterface$W5FE9cSy9nC8ojJrau85e1ZqcmU.INSTANCE);
        diRegistry.registerFactory(VastMediaFileScenarioMerger.class, $$Lambda$VideoModuleInterface$zW3WEMbf0SYXtOoBZ3btoQLLSWg.INSTANCE);
        diRegistry.registerFactory(VastWrapperCompanionScenarioPicker.class, $$Lambda$VideoModuleInterface$NFCS0bkvlhxj3Vl8C42sZCJOaUs.INSTANCE);
        diRegistry.registerFactory(ViewableImpressionMerger.class, $$Lambda$VideoModuleInterface$JyvNUTcKnkNGKkoweSDJB7MPcuA.INSTANCE);
        diRegistry.registerFactory(VastCompanionScenarioMerger.class, $$Lambda$VideoModuleInterface$Usug14Pa9AgoG9u004UGvY2F5MM.INSTANCE);
        diRegistry.registerFactory(VastScenarioWrapperMerger.class, $$Lambda$VideoModuleInterface$imF8_WPf2Hc3gRWSIEKGahMNj34.INSTANCE);
        diRegistry.registerFactory(VastLinearMediaFilePicker.class, $$Lambda$VideoModuleInterface$RFTJnvfLhS0XARI1HaZgRKsJkho.INSTANCE);
        diRegistry.registerFactory(VastCompanionPicker.class, $$Lambda$VideoModuleInterface$sJmiUiT8Afddcq1CHHgGM_6qbg.INSTANCE);
        diRegistry.registerFactory(VastCompanionScenarioMapper.class, $$Lambda$VideoModuleInterface$B1Gr4GyP3DaZJ1BZouWWusNgsvw.INSTANCE);
        diRegistry.registerFactory(VastIconScenarioMapper.class, $$Lambda$VideoModuleInterface$9H40NBV1TBwmT8nUVx4h5JLvWas.INSTANCE);
        diRegistry.registerFactory(VastIconScenarioPicker.class, $$Lambda$VideoModuleInterface$CkpffRhviV8lK0koEuuQf64R5U.INSTANCE);
        diRegistry.registerFactory(VastMediaFileScenarioMapper.class, $$Lambda$VideoModuleInterface$H51mLW1kQXmRXTAMOnmnx6GJMY8.INSTANCE);
        diRegistry.registerFactory(VastScenarioCreativeDataMapper.class, $$Lambda$VideoModuleInterface$EhnGtsSfRUK8bvqJmf9_nu2SnyQ.INSTANCE);
        diRegistry.registerFactory(VastScenarioMapper.class, $$Lambda$VideoModuleInterface$gH4qQFzFcaZu8_RbQzPyVD8mgY.INSTANCE);
        diRegistry.registerFactory(VastMediaFileScenarioWrapperMapper.class, $$Lambda$VideoModuleInterface$r4VBXOhjZITrCiHjXOWBSP_l1k.INSTANCE);
        diRegistry.registerFactory(VastScenarioWrapperMapper.class, $$Lambda$VideoModuleInterface$5JDevjZP5BVxKiQ4lCr5pa_lnY.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoClicksMerger J(DiConstructor diConstructor) {
        return new VideoClicksMerger();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastMediaFileScenarioMerger I(DiConstructor diConstructor) {
        return new VastMediaFileScenarioMerger((VideoClicksMerger) diConstructor.get(VideoClicksMerger.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastWrapperCompanionScenarioPicker H(DiConstructor diConstructor) {
        return new VastWrapperCompanionScenarioPicker();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ViewableImpressionMerger G(DiConstructor diConstructor) {
        return new ViewableImpressionMerger();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastCompanionScenarioMerger F(DiConstructor diConstructor) {
        return new VastCompanionScenarioMerger();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastScenarioWrapperMerger E(DiConstructor diConstructor) {
        return new VastScenarioWrapperMerger((VastMediaFileScenarioMerger) diConstructor.get(VastMediaFileScenarioMerger.class), (VastWrapperCompanionScenarioPicker) diConstructor.get(VastWrapperCompanionScenarioPicker.class), (VastCompanionScenarioMerger) diConstructor.get(VastCompanionScenarioMerger.class), (ViewableImpressionMerger) diConstructor.get(ViewableImpressionMerger.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastLinearMediaFilePicker D(DiConstructor diConstructor) {
        return new VastLinearMediaFilePicker();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastCompanionPicker C(DiConstructor diConstructor) {
        return new VastCompanionPicker();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastCompanionScenarioMapper B(DiConstructor diConstructor) {
        return new VastCompanionScenarioMapper();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastIconScenarioMapper A(DiConstructor diConstructor) {
        return new VastIconScenarioMapper();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastIconScenarioPicker z(DiConstructor diConstructor) {
        return new VastIconScenarioPicker((VastIconScenarioMapper) diConstructor.get(VastIconScenarioMapper.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastMediaFileScenarioMapper y(DiConstructor diConstructor) {
        return new VastMediaFileScenarioMapper((VastIconScenarioPicker) diConstructor.get(VastIconScenarioPicker.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastScenarioCreativeDataMapper x(DiConstructor diConstructor) {
        return new VastScenarioCreativeDataMapper();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastScenarioMapper w(DiConstructor diConstructor) {
        VastScenarioMapper vastScenarioMapper = new VastScenarioMapper((VastLinearMediaFilePicker) diConstructor.get(VastLinearMediaFilePicker.class), (VastCompanionPicker) diConstructor.get(VastCompanionPicker.class), (VastCompanionScenarioMapper) diConstructor.get(VastCompanionScenarioMapper.class), (VastMediaFileScenarioMapper) diConstructor.get(VastMediaFileScenarioMapper.class), (VastScenarioCreativeDataMapper) diConstructor.get(VastScenarioCreativeDataMapper.class));
        return vastScenarioMapper;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastMediaFileScenarioWrapperMapper v(DiConstructor diConstructor) {
        return new VastMediaFileScenarioWrapperMapper((VastIconScenarioPicker) diConstructor.get(VastIconScenarioPicker.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastScenarioWrapperMapper u(DiConstructor diConstructor) {
        return new VastScenarioWrapperMapper((VastCompanionPicker) diConstructor.get(VastCompanionPicker.class), (VastCompanionScenarioMapper) diConstructor.get(VastCompanionScenarioMapper.class), (VastMediaFileScenarioWrapperMapper) diConstructor.get(VastMediaFileScenarioWrapperMapper.class), (VastScenarioCreativeDataMapper) diConstructor.get(VastScenarioCreativeDataMapper.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void f(DiRegistry diRegistry) {
        diRegistry.registerFactory(InLineAdContainerPicker.class, $$Lambda$VideoModuleInterface$PpFRCWf265rnbh4bnxcj1rRtO1o.INSTANCE);
        diRegistry.registerFactory(WrapperAdContainerPicker.class, $$Lambda$VideoModuleInterface$WY9qW1gGChYefhV_BdY6TDnL4as.INSTANCE);
        diRegistry.registerFactory(VastScenarioPicker.class, $$Lambda$VideoModuleInterface$GKXl5ddiAeOgHZ2V227h8t2sNPU.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ InLineAdContainerPicker t(DiConstructor diConstructor) {
        return new InLineAdContainerPicker();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ WrapperAdContainerPicker s(DiConstructor diConstructor) {
        return new WrapperAdContainerPicker();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastScenarioPicker r(DiConstructor diConstructor) {
        VastScenarioPicker vastScenarioPicker = new VastScenarioPicker((InLineAdContainerPicker) diConstructor.get(InLineAdContainerPicker.class), (WrapperAdContainerPicker) diConstructor.get(WrapperAdContainerPicker.class), (VastScenarioWrapperMerger) diConstructor.get(VastScenarioWrapperMerger.class), (VastScenarioMapper) diConstructor.get(VastScenarioMapper.class), (VastScenarioWrapperMapper) diConstructor.get(VastScenarioWrapperMapper.class));
        return vastScenarioPicker;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(DiRegistry diRegistry) {
        diRegistry.registerFactory(VastTreeBuilder.class, $$Lambda$VideoModuleInterface$zl5GhYOhuvumLrtjgcZLjWUNxAc.INSTANCE);
        diRegistry.registerFactory(VastResponseParser.class, $$Lambda$VideoModuleInterface$yzTNRd3LH0pzLR6W8oOAcdc72yI.INSTANCE);
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$ZF7Tpxi8FbF07hhbjtNzWVXqiS0.INSTANCE));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastTreeBuilder q(DiConstructor diConstructor) {
        return new VastTreeBuilder((VastResponseParser) diConstructor.get(VastResponseParser.class), (WrapperResolver) diConstructor.get(WrapperResolver.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastResponseParser p(DiConstructor diConstructor) {
        return new VastResponseParser((RegistryXmlParser) diConstructor.get(RegistryXmlParser.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(DiRegistry diRegistry) {
        diRegistry.registerFactory(WrapperResolver.class, $$Lambda$VideoModuleInterface$OhkAuGF1LkaBEWiZBD75KSBF11M.INSTANCE);
        diRegistry.registerFactory(WrapperLoader.class, $$Lambda$VideoModuleInterface$yKhMdSO6_GIevdhXFldLMiIL8.INSTANCE);
        diRegistry.registerFactory(WrapperLoaderErrorMapper.class, $$Lambda$VideoModuleInterface$vcAXxNaoBzVUXhIRcHSWBp_LY.INSTANCE);
        diRegistry.registerFactory(InLineChecker.class, $$Lambda$VideoModuleInterface$12osAcLz6fIC36AbKdv56MEpnRM.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ WrapperResolver o(DiConstructor diConstructor) {
        return new WrapperResolver(((WrapperResolverConfig) diConstructor.get("WRAPPER_RESOLVER_CONFIG", WrapperResolverConfig.class)).getMaxDepth(), (WrapperLoader) diConstructor.get(WrapperLoader.class), (InLineChecker) diConstructor.get(InLineChecker.class), (WrapperAdContainerPicker) diConstructor.get(WrapperAdContainerPicker.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ WrapperLoader n(DiConstructor diConstructor) {
        return new WrapperLoader((WrapperLoaderErrorMapper) diConstructor.get(WrapperLoaderErrorMapper.class), (StaticWrapperLoaderExecutioner) diConstructor.get(StaticWrapperLoaderExecutioner.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ WrapperLoaderErrorMapper m(DiConstructor diConstructor) {
        return new WrapperLoaderErrorMapper();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ InLineChecker l(DiConstructor diConstructor) {
        return new InLineChecker();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(DiRegistry diRegistry) {
        diRegistry.registerSingletonFactory(VideoDiNames.VIDEO_RESOURCE_LOADER_DI_NAME, ResourceLoader.class, new ClassFactory() {
            public final Object get(DiConstructor diConstructor) {
                return VideoModuleInterface.this.k(diConstructor);
            }
        });
        diRegistry.registerSingletonFactory("VideoModuleInterfaceVIDEO_RESOURCE_LOADING_DEVICE_LOCAL_EXECUTOR", ExecutorService.class, $$Lambda$VideoModuleInterface$9dkBwjMCqZhEtPzjwUaxLZX4iQw.INSTANCE);
        diRegistry.registerFactory(VideoDiNames.VIDEO_RESOURCE_LOADER_DI_NAME, ResourceLoadingNetworkTaskCreator.class, $$Lambda$VideoModuleInterface$R7O5ssVd_z96kSedtRVTdj1u42c.INSTANCE);
        diRegistry.registerSingletonFactory("VideoModuleInterfaceVIDEO_RESOURCE_LOADING_NETWORK_EXECUTOR", ExecutorService.class, $$Lambda$VideoModuleInterface$lUPbJV6KNH2XBawmBnvHk7to9KI.INSTANCE);
        diRegistry.registerFactory(VideoNetworkResourceStreamPreparationStrategy.class, $$Lambda$VideoModuleInterface$2vzMChx6Uw1Hr470isFvx4Aroes.INSTANCE);
        diRegistry.registerFactory(VideoPersistingStrategy.class, $$Lambda$VideoModuleInterface$CyX21yGGf7XvtGLYN3IxY8KC334.INSTANCE);
        diRegistry.registerFactory(VideoResourceTransformer.class, $$Lambda$VideoModuleInterface$tRJ5p8sxGJpHIRCwKRCfMOao4Pk.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ ResourceLoader k(DiConstructor diConstructor) {
        ResourceLoader resourceLoader = new ResourceLoader(DiLogLayer.getLoggerFrom(diConstructor), (ResourceLoadingNetworkTaskCreator) diConstructor.get(VideoDiNames.VIDEO_RESOURCE_LOADER_DI_NAME, ResourceLoadingNetworkTaskCreator.class), (ExecutorService) diConstructor.get("VideoModuleInterfaceVIDEO_RESOURCE_LOADING_DEVICE_LOCAL_EXECUTOR", ExecutorService.class), (PersistingStrategy) diConstructor.get(VideoPersistingStrategy.class), (ResourceTransformer) diConstructor.get(VideoResourceTransformer.class));
        return resourceLoader;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ ResourceLoadingNetworkTaskCreator i(DiConstructor diConstructor) {
        ResourceLoadingNetworkTaskCreator resourceLoadingNetworkTaskCreator = new ResourceLoadingNetworkTaskCreator(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getNetworkActionsFrom(diConstructor), ErrorMapper.NETWORK_LAYER_EXCEPTION, (ExecutorService) diConstructor.get("VideoModuleInterfaceVIDEO_RESOURCE_LOADING_NETWORK_EXECUTOR", ExecutorService.class), (NetworkResourceStreamPreparationStrategy) diConstructor.get(VideoNetworkResourceStreamPreparationStrategy.class));
        return resourceLoadingNetworkTaskCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoNetworkResourceStreamPreparationStrategy g(DiConstructor diConstructor) {
        return new VideoNetworkResourceStreamPreparationStrategy();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoPersistingStrategy f(DiConstructor diConstructor) {
        return new VideoPersistingStrategy(DiLogLayer.getLoggerFrom(diConstructor), "video/vast", DiResourceLoaderLayer.getBaseStoragePersistingStrategyFileUtils(diConstructor), DiResourceLoaderLayer.getMd5Digester(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoResourceTransformer e(DiConstructor diConstructor) {
        return new VideoResourceTransformer();
    }

    public boolean isFormatSupported(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls) {
        return adFormat == AdFormat.VIDEO && (cls.isAssignableFrom(RewardedAdPresenter.class) || cls.isAssignableFrom(InterstitialAdPresenter.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdLoaderPlugin d(DiConstructor diConstructor) {
        return new VideoAdLoaderPlugin((AdPresenterNameShaper) diConstructor.get(AdPresenterNameShaper.class), new NullableFunction() {
            public final Object apply(Object obj) {
                return VideoModuleInterface.a(DiConstructor.this, (String) obj);
            }
        });
    }

    @NonNull
    public ClassFactory<AdLoaderPlugin> getAdLoaderPluginFactory() {
        return $$Lambda$VideoModuleInterface$IFz342Ntqkta4dzYLc8X8oekP8.INSTANCE;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder a(DiConstructor diConstructor, String str) {
        return (AdPresenterBuilder) DiAdLayer.tryGetOrNull(diConstructor, str, AdPresenterBuilder.class);
    }

    @Nullable
    public DiRegistry moduleDiRegistry() {
        return DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.a((DiRegistry) obj);
            }
        });
    }

    @Nullable
    public DiRegistry moduleAdPresenterDiRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper) {
        return DiRegistry.of(new Consumer(adPresenterNameShaper) {
            private final /* synthetic */ AdPresenterNameShaper f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                VideoModuleInterface.this.a(this.f$1, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdPresenterNameShaper adPresenterNameShaper, DiRegistry diRegistry) {
        diRegistry.registerFactory(VastErrorTrackerCreator.class, $$Lambda$VideoModuleInterface$8zRaebMduL9n9LK6hWZzqNmSvLI.INSTANCE);
        diRegistry.registerFactory(MediaFileResourceLoaderListenerCreator.class, $$Lambda$VideoModuleInterface$gejODrtxnjF3tkk6mlgWkOJ9CbE.INSTANCE);
        diRegistry.addFrom(DiVideoAdLayer.createRegistry(adPresenterNameShaper, moduleDiName()));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastErrorTrackerCreator b(DiConstructor diConstructor) {
        return new VastErrorTrackerCreator((Logger) diConstructor.get(Logger.class), DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (MacrosInjectorProviderFunction) diConstructor.get(MacrosInjectorProviderFunction.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ MediaFileResourceLoaderListenerCreator a(DiConstructor diConstructor) {
        return new MediaFileResourceLoaderListenerCreator();
    }

    @NonNull
    public ExpectedManifestEntries getExpectedManifestEntries() {
        return ExpectedManifestEntries.EMPTY;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VideoModuleInterface{supportedFormat: ");
        sb.append(AdFormat.VIDEO);
        sb.append("}");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$yAtHFQg2lyDJdhUU4YOwTSpn0L8.INSTANCE));
        diRegistry.registerFactory(moduleDiName(), XmlPullParser.class, $$Lambda$VideoModuleInterface$POBREcnOLuaZyLMxB8995JHted0.INSTANCE);
        diRegistry.registerSingletonFactory(RegistryXmlParser.class, new RegistryXmlParserFactory());
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.j((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.b((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiPlayerLayer.createRegistry());
        diRegistry.addFrom(DiMacros.createRegistry());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(DiRegistry diRegistry) {
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.i((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$dBAWhnxLsCP7mZ6umRVDM_TAQZc.INSTANCE));
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$LWqXMoNNqu_DwCxOeg3h4fOeDG0.INSTANCE));
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.e((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of($$Lambda$VideoModuleInterface$UaQgqN46Ya_kvTP7EwZogXTfRYc.INSTANCE));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void j(DiRegistry diRegistry) {
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.h((DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of(new Consumer() {
            public final void accept(Object obj) {
                VideoModuleInterface.this.c((DiRegistry) obj);
            }
        }));
        diRegistry.registerFactory(moduleDiName(), StateMachine.class, new VideoStateMachineFactory(State.INIT));
    }
}
