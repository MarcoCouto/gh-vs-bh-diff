package com.smaato.sdk.video.framework;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.video.framework.-$$Lambda$VideoModuleInterface$Xhpp1wqWZD2AqPz_EgxrTPbas-o reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VideoModuleInterface$Xhpp1wqWZD2AqPz_EgxrTPbaso implements ClassFactory {
    public static final /* synthetic */ $$Lambda$VideoModuleInterface$Xhpp1wqWZD2AqPz_EgxrTPbaso INSTANCE = new $$Lambda$VideoModuleInterface$Xhpp1wqWZD2AqPz_EgxrTPbaso();

    private /* synthetic */ $$Lambda$VideoModuleInterface$Xhpp1wqWZD2AqPz_EgxrTPbaso() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(0.01d).visibilityTimeMillis(0).build();
    }
}
