package com.smaato.sdk.video.utils;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;

public class RepeatableAction implements Runnable {
    @NonNull
    private final Handler a;
    @NonNull
    private final Listener b;
    private final long c;
    private boolean d;

    @FunctionalInterface
    public interface Listener {
        void doAction();
    }

    private RepeatableAction(@NonNull Handler handler, long j, @NonNull Listener listener) {
        this.a = (Handler) Objects.requireNonNull(handler);
        this.c = 50;
        this.b = (Listener) Objects.requireNonNull(listener);
    }

    public RepeatableAction(@NonNull Handler handler, @NonNull Listener listener) {
        this(handler, 50, listener);
    }

    public void start() {
        Threads.ensureHandlerThread(this.a);
        if (!this.d) {
            this.a.postDelayed(this, this.c);
            this.d = true;
        }
    }

    public void stop() {
        Threads.ensureHandlerThread(this.a);
        if (this.d) {
            this.a.removeCallbacks(this);
            this.d = false;
        }
    }

    public void run() {
        Threads.ensureHandlerThread(this.a);
        this.d = false;
        start();
        this.b.doAction();
    }
}
