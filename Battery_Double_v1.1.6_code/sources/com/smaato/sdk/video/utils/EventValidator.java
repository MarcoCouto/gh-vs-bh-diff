package com.smaato.sdk.video.utils;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public final class EventValidator<Event, State> {
    @NonNull
    private final Map<Event, List<? extends State>> a;

    public static class Builder<Event, State> {
        @NonNull
        private final Map<Event, List<? extends State>> a = new HashMap();

        @NonNull
        public Builder<Event, State> setValidStatesForEvent(@NonNull Event event, @NonNull List<? extends State> list) {
            Objects.requireNonNull(event, "Parameter event can not be null");
            ArrayList<Object> arrayList = new ArrayList<>((Collection) Objects.requireNonNull(list, "Parameter states can not be null"));
            for (Object requireNonNull : arrayList) {
                Objects.requireNonNull(requireNonNull, "a state can not be null");
            }
            if (arrayList.size() <= new HashSet(list).size()) {
                this.a.put(event, arrayList);
                return this;
            }
            throw new IllegalArgumentException("a states must consist of unique states");
        }

        @NonNull
        public EventValidator<Event, State> build() {
            if (!this.a.isEmpty()) {
                return new EventValidator<>(this.a, 0);
            }
            throw new IllegalStateException("At least one valid event for states should be added.");
        }
    }

    /* synthetic */ EventValidator(Map map, byte b) {
        this(map);
    }

    private EventValidator(@NonNull Map<Event, List<? extends State>> map) {
        this.a = map;
    }

    public final boolean isValid(@NonNull Event event, @NonNull State state) {
        List list = (List) this.a.get(event);
        if (list == null) {
            return false;
        }
        return list.contains(state);
    }
}
