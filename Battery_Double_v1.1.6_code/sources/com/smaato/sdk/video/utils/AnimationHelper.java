package com.smaato.sdk.video.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import androidx.annotation.NonNull;

public class AnimationHelper {
    private final long a;

    public AnimationHelper(long j) {
        this.a = j;
    }

    public void showWithAnim(@NonNull View view) {
        view.setAlpha(0.0f);
        view.setVisibility(0);
        view.animate().alpha(1.0f).setDuration(this.a).start();
    }

    public void hideWithAnim(@NonNull final View view) {
        view.animate().alpha(0.0f).setDuration(this.a).setListener(new AnimatorListenerAdapter() {
            public final void onAnimationEnd(Animator animator) {
                view.setVisibility(8);
            }
        }).start();
    }
}
