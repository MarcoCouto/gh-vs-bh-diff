package com.smaato.sdk.video.resourceloader;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.NetworkResourceStreamPreparationStrategy;
import com.smaato.sdk.core.util.Objects;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

public class VideoNetworkResourceStreamPreparationStrategy implements NetworkResourceStreamPreparationStrategy {
    @NonNull
    public InputStream prepare(@NonNull URLConnection uRLConnection) throws IOException {
        return ((URLConnection) Objects.requireNonNull(uRLConnection)).getInputStream();
    }
}
