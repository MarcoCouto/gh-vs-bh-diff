package com.smaato.sdk.video.resourceloader;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.BaseStoragePersistingStrategy;
import com.smaato.sdk.core.resourceloader.BaseStoragePersistingStrategyFileUtils;
import com.smaato.sdk.core.resourceloader.Md5Digester;
import com.smaato.sdk.core.resourceloader.PersistingStrategy.Error;
import com.smaato.sdk.core.resourceloader.PersistingStrategyException;
import com.smaato.sdk.core.util.IOUtils;
import com.smaato.sdk.core.util.Objects;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public final class VideoPersistingStrategy extends BaseStoragePersistingStrategy<Uri> {
    public VideoPersistingStrategy(@NonNull Logger logger, @NonNull String str, @NonNull BaseStoragePersistingStrategyFileUtils baseStoragePersistingStrategyFileUtils, @NonNull Md5Digester md5Digester) {
        super(logger, str, baseStoragePersistingStrategyFileUtils, md5Digester);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public final Uri getResourceFromStorage(@NonNull String str) {
        Objects.requireNonNull(str);
        File resourceFileFromStorage = this.fileUtils.getResourceFileFromStorage(this.descriptiveResourceName, version().intValue(), str);
        if (resourceFileFromStorage != null) {
            return Uri.fromFile(resourceFileFromStorage);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ae A[Catch:{ all -> 0x00b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b1 A[Catch:{ all -> 0x00b9 }] */
    @NonNull
    public final Uri putResourceToStorage(@NonNull File file, @NonNull InputStream inputStream, @NonNull String str, long j) throws PersistingStrategyException {
        BufferedOutputStream bufferedOutputStream;
        BufferedInputStream bufferedInputStream;
        Exception e;
        Objects.requireNonNull(file);
        Objects.requireNonNull(inputStream);
        Objects.requireNonNull(str);
        if (System.currentTimeMillis() <= j) {
            File temporaryResourceFile = this.fileUtils.getTemporaryResourceFile(file, str);
            File file2 = new File(file, str);
            try {
                bufferedInputStream = new BufferedInputStream(inputStream, 16384);
                try {
                    bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(temporaryResourceFile), 16384);
                    try {
                        IOUtils.copy(bufferedInputStream, bufferedOutputStream, 16384);
                        bufferedInputStream.close();
                        bufferedOutputStream.flush();
                        bufferedOutputStream.close();
                        if (!temporaryResourceFile.renameTo(file2)) {
                            throw new IllegalStateException(String.format("Cannot rename temporary file. Temporary filename:%s, destination filename:%s", new Object[]{temporaryResourceFile.getName(), file2.getName()}));
                        } else if (System.currentTimeMillis() <= j) {
                            Uri fromFile = Uri.fromFile(file2);
                            IOUtils.closeSilently(bufferedInputStream);
                            IOUtils.closeSilently(bufferedOutputStream);
                            this.fileUtils.deleteFile(temporaryResourceFile, "temporary resource file");
                            return fromFile;
                        } else {
                            throw new PersistingStrategyException(Error.RESOURCE_EXPIRED, new IllegalStateException("Resource downloading took too much time. Resource expired and is being deleted"));
                        }
                    } catch (Exception e2) {
                        e = e2;
                        try {
                            this.logger.error(LogDomain.RESOURCE_LOADER, e, "Resource persisting failed", new Object[0]);
                            this.fileUtils.deleteFile(file2, "resource file");
                            if (e instanceof PersistingStrategyException) {
                            }
                        } catch (Throwable th) {
                            th = th;
                            IOUtils.closeSilently(bufferedInputStream);
                            IOUtils.closeSilently(bufferedOutputStream);
                            this.fileUtils.deleteFile(temporaryResourceFile, "temporary resource file");
                            throw th;
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    bufferedOutputStream = null;
                    this.logger.error(LogDomain.RESOURCE_LOADER, e, "Resource persisting failed", new Object[0]);
                    this.fileUtils.deleteFile(file2, "resource file");
                    if (e instanceof PersistingStrategyException) {
                        throw ((PersistingStrategyException) e);
                    }
                    throw new PersistingStrategyException(Error.IO_ERROR, e);
                } catch (Throwable th2) {
                    th = th2;
                    bufferedOutputStream = null;
                    IOUtils.closeSilently(bufferedInputStream);
                    IOUtils.closeSilently(bufferedOutputStream);
                    this.fileUtils.deleteFile(temporaryResourceFile, "temporary resource file");
                    throw th;
                }
            } catch (Exception e4) {
                bufferedInputStream = null;
                e = e4;
                bufferedOutputStream = null;
                this.logger.error(LogDomain.RESOURCE_LOADER, e, "Resource persisting failed", new Object[0]);
                this.fileUtils.deleteFile(file2, "resource file");
                if (e instanceof PersistingStrategyException) {
                }
            } catch (Throwable th3) {
                th = th3;
                bufferedOutputStream = null;
                bufferedInputStream = null;
                IOUtils.closeSilently(bufferedInputStream);
                IOUtils.closeSilently(bufferedOutputStream);
                this.fileUtils.deleteFile(temporaryResourceFile, "temporary resource file");
                throw th;
            }
        } else {
            throw new PersistingStrategyException(Error.RESOURCE_EXPIRED, new IllegalStateException("Resource already expired"));
        }
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final Integer version() {
        return Integer.valueOf(1);
    }
}
