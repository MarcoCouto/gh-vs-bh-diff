package com.smaato.sdk.video.resourceloader;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.ResourceTransformer;
import com.smaato.sdk.core.util.Objects;

public class VideoResourceTransformer implements ResourceTransformer<Uri, Uri> {
    @NonNull
    public Uri transform(@NonNull Uri uri) {
        return (Uri) Objects.requireNonNull(uri);
    }
}
