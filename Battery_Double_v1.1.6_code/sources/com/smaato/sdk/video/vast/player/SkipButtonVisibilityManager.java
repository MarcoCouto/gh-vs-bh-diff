package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class SkipButtonVisibilityManager {

    static class a extends SkipButtonVisibilityManager {
        public final void a(long j, @NonNull VideoPlayerView videoPlayerView) {
        }

        private a() {
        }

        /* synthetic */ a(byte b) {
            this();
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(long j, @NonNull VideoPlayerView videoPlayerView);

    @NonNull
    public static SkipButtonVisibilityManager create(@Nullable VideoTimings videoTimings) {
        if (videoTimings == null) {
            return new a(0);
        }
        return videoTimings.isVideoSkippable ? new SkipButtonVisibilityManagerImpl(videoTimings.skipOffsetMillis, videoTimings.videoDurationMillis) : new a(0);
    }
}
