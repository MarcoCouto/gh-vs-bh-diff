package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;

public class SkipButtonVisibilityManagerImpl extends SkipButtonVisibilityManager {
    private final long a;
    private final long b;

    SkipButtonVisibilityManagerImpl(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j, @NonNull VideoPlayerView videoPlayerView) {
        if (j >= this.a && j < this.b) {
            videoPlayerView.a();
        }
    }
}
