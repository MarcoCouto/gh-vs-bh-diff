package com.smaato.sdk.video.vast.player;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StubOnGestureListener;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.R;
import com.smaato.sdk.video.vast.widget.CircularProgressBar;
import com.smaato.sdk.video.vast.widget.VastSurfaceHolder;
import com.smaato.sdk.video.vast.widget.VastSurfaceHolder.OnSurfaceAvailableListener;
import com.smaato.sdk.video.vast.widget.VastSurfaceHolder.OnSurfaceChangedListener;
import com.smaato.sdk.video.vast.widget.VastSurfaceHolder.OnSurfaceDestroyedListener;

public abstract class VideoPlayerView extends FrameLayout {
    @NonNull
    private ImageButton a;
    @NonNull
    private ImageButton b;
    @NonNull
    private CircularProgressBar c = ((CircularProgressBar) findViewById(R.id.smaato_sdk_video_video_progress));
    @NonNull
    private View d;
    /* access modifiers changed from: private */
    @Nullable
    public e e;

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Surface surface, int i, int i2, e eVar) {
    }

    /* access modifiers changed from: protected */
    @NonNull
    public abstract VastSurfaceHolder initVastSurfaceHolder(@NonNull Context context);

    protected VideoPlayerView(Context context) {
        super(context);
        inflate(context, R.layout.smaato_sdk_video_player_view, this);
        VastSurfaceHolder initVastSurfaceHolder = initVastSurfaceHolder(context);
        initVastSurfaceHolder.setOnSurfaceAvailableListener(new OnSurfaceAvailableListener() {
            public final void onSurfaceAvailable(Surface surface, int i, int i2) {
                VideoPlayerView.this.a(surface, i, i2);
            }
        });
        initVastSurfaceHolder.setOnSurfaceChangedListener(new OnSurfaceChangedListener() {
            public final void onSurfaceChanged(Surface surface, int i, int i2) {
                VideoPlayerView.this.b(surface, i, i2);
            }
        });
        initVastSurfaceHolder.setOnSurfaceDestroyedListener(new OnSurfaceDestroyedListener() {
            public final void onSurfaceDestroyed(Surface surface) {
                VideoPlayerView.this.a(surface);
            }
        });
        GestureDetector gestureDetector = new GestureDetector(getContext(), new StubOnGestureListener() {
            public final boolean onSingleTapUp(@NonNull MotionEvent motionEvent) {
                Objects.onNotNull(VideoPlayerView.this.e, new Consumer(motionEvent) {
                    private final /* synthetic */ MotionEvent f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        ((e) obj).a(this.f$0.getX(), this.f$0.getY());
                    }
                });
                return true;
            }
        });
        View view = initVastSurfaceHolder.getView();
        view.setId(R.id.smaato_sdk_video_surface_holder_view_id);
        view.setOnTouchListener(new OnTouchListener(gestureDetector) {
            private final /* synthetic */ GestureDetector f$0;

            {
                this.f$0 = r1;
            }

            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return this.f$0.onTouchEvent(motionEvent);
            }
        });
        ((FrameLayout) findViewById(R.id.smaato_sdk_video_player_surface_layout)).addView(view, new LayoutParams(-1, -1));
        this.d = view;
        ImageButton imageButton = (ImageButton) findViewById(R.id.smaato_sdk_video_skip_button);
        imageButton.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                VideoPlayerView.this.b(view);
            }
        });
        this.a = imageButton;
        ImageButton imageButton2 = (ImageButton) findViewById(R.id.smaato_sdk_video_mute_button);
        imageButton2.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                VideoPlayerView.this.a(view);
            }
        });
        this.b = imageButton2;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable e eVar) {
        Threads.ensureMainThread();
        this.e = eVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        setMeasuredDimension(size, size2);
        if (size > 0 && size2 > 0) {
            Objects.onNotNull(this.e, new Consumer(size, size2) {
                private final /* synthetic */ int f$1;
                private final /* synthetic */ int f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void accept(Object obj) {
                    VideoPlayerView.this.a(this.f$1, this.f$2, (e) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(int i, int i2, e eVar) {
        eVar.a(this, i, i2);
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z) {
        Threads.runOnUi(new Runnable(z) {
            private final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                VideoPlayerView.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(boolean z) {
        this.b.setImageResource(z ? R.drawable.smaato_sdk_video_muted : R.drawable.smaato_sdk_video_unmuted);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Objects.onNotNull(this.e, $$Lambda$39eqcQKXdTLBAmE0IY5mACeMabE.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i, int i2) {
        Threads.runOnUi(new Runnable(i, i2) {
            private final /* synthetic */ int f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                VideoPlayerView.this.b(this.f$1, this.f$2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(int i, int i2) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.d.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        layoutParams.gravity = 17;
        this.d.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(View view) {
        Objects.onNotNull(this.e, $$Lambda$OUXchi6ZFVi1gm0D7mwZ0jMY_Lw.INSTANCE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(View view) {
        Objects.onNotNull(this.e, $$Lambda$H1cktjRHVzQuIgqqMO43zHeV9A.INSTANCE);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Surface surface, int i, int i2) {
        Objects.onNotNull(this.e, new Consumer(surface) {
            private final /* synthetic */ Surface f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((e) obj).a(this.f$0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(@NonNull Surface surface, int i, int i2) {
        Objects.onNotNull(this.e, new Consumer(surface, i, i2) {
            private final /* synthetic */ Surface f$0;
            private final /* synthetic */ int f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                VideoPlayerView.a(this.f$0, this.f$1, this.f$2, (e) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Surface surface) {
        Objects.onNotNull(this.e, new Consumer(surface) {
            private final /* synthetic */ Surface f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((e) obj).e();
            }
        });
        surface.release();
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j, long j2) {
        long j3 = j;
        long j4 = j2;
        $$Lambda$VideoPlayerView$kThi1LrBjqOI_PHHPs3SVhCvSiw r1 = new Runnable(j3, j4, String.valueOf(((int) (j2 / 1000)) - ((int) (j / 1000)))) {
            private final /* synthetic */ long f$1;
            private final /* synthetic */ long f$2;
            private final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r4;
                this.f$3 = r6;
            }

            public final void run() {
                VideoPlayerView.this.a(this.f$1, this.f$2, this.f$3);
            }
        };
        Threads.runOnUi(r1);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(long j, long j2, String str) {
        this.c.setProgress((float) j, (float) j2, str);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        Threads.runOnUi(new Runnable() {
            public final void run() {
                VideoPlayerView.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        if (!(this.a.getVisibility() == 0)) {
            this.a.setAlpha(0.0f);
            this.a.setVisibility(0);
            this.a.animate().alpha(1.0f).setDuration(300).start();
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (!(VERSION.SDK_INT >= 18 && isInLayout())) {
            requestLayout();
        }
    }
}
