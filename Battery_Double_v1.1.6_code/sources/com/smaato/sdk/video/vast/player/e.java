package com.smaato.sdk.video.vast.player;

import android.view.Surface;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.tracker.VisibilityTracker;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.tracker.VisibilityTrackerListener;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.utils.RepeatableAction;
import com.smaato.sdk.video.utils.RepeatableAction.Listener;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.player.VideoPlayer.LifecycleListener;
import com.smaato.sdk.video.vast.player.VideoPlayer.OnVolumeChangeListener;
import com.smaato.sdk.video.vast.player.exception.VideoPlayerException;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

final class e {
    @NonNull
    private final VideoPlayer a;
    @NonNull
    private final VastMediaFileScenario b;
    @NonNull
    private final VideoViewResizeManager c;
    @NonNull
    private final VisibilityTrackerCreator d;
    @NonNull
    private final SkipButtonVisibilityManager e;
    /* access modifiers changed from: private */
    @NonNull
    public final RepeatableAction f;
    @NonNull
    private final AtomicReference<VisibilityTracker> g;
    /* access modifiers changed from: private */
    @Nullable
    public a h;
    @NonNull
    private final LifecycleListener i = new LifecycleListener() {
        public final void onReleased(@NonNull VideoPlayer videoPlayer) {
        }

        public final void onSeekComplete(@NonNull VideoPlayer videoPlayer) {
        }

        public final void onStarted(@NonNull VideoPlayer videoPlayer) {
            e.this.f.start();
            Objects.onNotNull(e.this.h, new Consumer() {
                public final void accept(Object obj) {
                    ((a) obj).a(VideoPlayer.this.getDuration(), VideoPlayer.this.getCurrentVolume());
                }
            });
        }

        public final void onResumed(@NonNull VideoPlayer videoPlayer) {
            e.this.f.start();
            Objects.onNotNull(e.this.h, $$Lambda$QQJsMBs7QLr37X5wLMjYW7cf70g.INSTANCE);
        }

        public final void onPaused(@NonNull VideoPlayer videoPlayer) {
            Objects.onNotNull(e.this.h, $$Lambda$FQpV_OKwVOjYIbDQvsrjh2aKNqQ.INSTANCE);
            e.this.f.stop();
        }

        public final void onStopped(@NonNull VideoPlayer videoPlayer) {
            e.this.f.stop();
        }

        public final void onCompleted(@NonNull VideoPlayer videoPlayer) {
            Objects.onNotNull(e.this.h, $$Lambda$5y3zd3hDuhC8O_zefxKOGkgCHlQ.INSTANCE);
            e.this.f.stop();
        }

        public final void onReset(@NonNull VideoPlayer videoPlayer) {
            e.this.f.stop();
        }

        public final void onError(@NonNull VideoPlayer videoPlayer, @NonNull VideoPlayerException videoPlayerException) {
            Objects.onNotNull(e.this.h, $$Lambda$e$1$b_9Qq4MfNS4eiAvRzlvHsXpm6y8.INSTANCE);
            e.this.f.stop();
        }
    };
    @NonNull
    private WeakReference<VideoPlayerView> j = new WeakReference<>(null);
    private long k;

    interface a {
        void a();

        void a(float f, float f2);

        void a(int i);

        void a(long j, float f);

        void a(long j, long j2);

        void b();

        void c();

        void d();

        void e();

        void f();

        void g();
    }

    e(@NonNull VideoPlayer videoPlayer, @NonNull VastMediaFileScenario vastMediaFileScenario, @NonNull VideoViewResizeManager videoViewResizeManager, @NonNull SkipButtonVisibilityManager skipButtonVisibilityManager, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull RepeatableActionFactory repeatableActionFactory) {
        this.a = (VideoPlayer) Objects.requireNonNull(videoPlayer);
        this.b = (VastMediaFileScenario) Objects.requireNonNull(vastMediaFileScenario);
        this.c = (VideoViewResizeManager) Objects.requireNonNull(videoViewResizeManager);
        this.e = (SkipButtonVisibilityManager) Objects.requireNonNull(skipButtonVisibilityManager);
        this.d = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.f = (RepeatableAction) Objects.requireNonNull(repeatableActionFactory.createRepeatableAction(new Listener() {
            public final void doAction() {
                e.this.h();
            }
        }));
        this.g = new AtomicReference<>();
        videoPlayer.setLifecycleListener(this.i);
        videoPlayer.setOnVolumeChangeListener(new OnVolumeChangeListener() {
            public final void onVolumeChanged(float f) {
                e.this.a(f);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(float f2) {
        boolean z = f2 == 0.0f;
        Objects.onNotNull(this.j.get(), new Consumer(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((VideoPlayerView) obj).a(this.f$0);
            }
        });
        Objects.onNotNull(this.h, new Consumer(z) {
            private final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                e.a(this.f$0, (a) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(boolean z, a aVar) {
        if (z) {
            aVar.c();
        } else {
            aVar.d();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable a aVar) {
        this.h = aVar;
    }

    /* access modifiers changed from: private */
    public void h() {
        long currentPositionMillis = this.a.getCurrentPositionMillis();
        if (currentPositionMillis != this.k) {
            this.k = currentPositionMillis;
            long j2 = this.k;
            long duration = this.a.getDuration();
            Objects.onNotNull(this.h, new Consumer(j2, duration) {
                private final /* synthetic */ long f$0;
                private final /* synthetic */ long f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r3;
                }

                public final void accept(Object obj) {
                    ((a) obj).a(this.f$0, this.f$1);
                }
            });
            Object obj = this.j.get();
            $$Lambda$e$1qBWU7xrOgqQU5sWBKtCPfBKiVk r5 = new Consumer(j2, duration) {
                private final /* synthetic */ long f$1;
                private final /* synthetic */ long f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r4;
                }

                public final void accept(Object obj) {
                    e.this.a(this.f$1, this.f$2, (VideoPlayerView) obj);
                }
            };
            Objects.onNotNull(obj, r5);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull VideoPlayerView videoPlayerView) {
        this.j = new WeakReference<>(videoPlayerView);
        videoPlayerView.a(this);
        videoPlayerView.a(this.a.getCurrentVolume() == 0.0f);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.j.clear();
        i();
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.j.clear();
        i();
        this.a.stop();
        this.a.release();
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        this.a.pause();
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        this.a.start();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Surface surface) {
        Objects.onNotNull(this.j.get(), new Consumer() {
            public final void accept(Object obj) {
                e.this.b((VideoPlayerView) obj);
            }
        });
        this.a.setSurface(surface);
        this.a.start();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(VideoPlayerView videoPlayerView) {
        this.g.set(this.d.createTracker(videoPlayerView, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                e.this.j();
            }
        }));
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        i();
        this.a.setSurface(null);
        this.a.pause();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull VideoPlayerView videoPlayerView, int i2, int i3) {
        this.c.resizeToContainerSizes(videoPlayerView, i2, i3);
    }

    /* access modifiers changed from: 0000 */
    public final void a(float f2, float f3) {
        Objects.onNotNull(this.h, new Consumer(f2, f3) {
            private final /* synthetic */ float f$0;
            private final /* synthetic */ float f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((a) obj).a(this.f$0, this.f$1);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        Objects.onNotNull(this.h, $$Lambda$O5doed3MRjmWDVP216RFy69QO8.INSTANCE);
        b();
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        float f2 = 0.0f;
        boolean z = this.a.getCurrentVolume() == 0.0f;
        VideoPlayer videoPlayer = this.a;
        if (z) {
            f2 = 1.0f;
        }
        videoPlayer.setVolume(f2);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void j() {
        Objects.onNotNull(this.h, $$Lambda$QRAtt8antgocj7iC1dOhsZgeWY.INSTANCE);
    }

    private void i() {
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                e.this.a((VisibilityTracker) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(VisibilityTracker visibilityTracker) {
        visibilityTracker.destroy();
        this.g.set(null);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(long j2, long j3, VideoPlayerView videoPlayerView) {
        videoPlayerView.a(j2, j3);
        this.e.a(j2, videoPlayerView);
    }
}
