package com.smaato.sdk.video.vast.player;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTracker;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.tracker.VisibilityTrackerListener;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.smaato.sdk.video.vast.player.ComponentClickHandler.ClickCallback;
import com.smaato.sdk.video.vast.widget.VastVideoPlayerView;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenter;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenter.Listener;
import java.lang.ref.WeakReference;

public class VastVideoPlayerPresenter {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    /* access modifiers changed from: private */
    @NonNull
    public final VastVideoPlayerModel b;
    @NonNull
    private final VisibilityTrackerCreator c;
    @NonNull
    private final e d;
    @NonNull
    private final VastElementPresenter e;
    @NonNull
    private final VastElementPresenter f;
    /* access modifiers changed from: private */
    @NonNull
    public final StateMachine<a, b> g;
    @NonNull
    private WeakReference<VastVideoPlayerView> h = new WeakReference<>(null);
    @NonNull
    private final Listener i = new Listener() {
        /* access modifiers changed from: private */
        public /* synthetic */ void a(Consumer consumer) {
            VastVideoPlayerPresenter.this.a(consumer);
        }

        public final void onVastElementClicked(@Nullable String str) {
            VastVideoPlayerPresenter.this.b.b(str, new ClickCallback() {
                public final void onUrlResolved(Consumer consumer) {
                    AnonymousClass1.this.a(consumer);
                }
            });
        }

        public final void onVastElementRendered() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onIconRendered", new Object[0]);
            VastVideoPlayerPresenter.this.b.m();
        }

        public final void onVastElementError(int i) {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onIconError", new Object[0]);
            VastVideoPlayerPresenter.this.b.b(i);
        }

        public final void onRenderProcessGone() {
            VastVideoPlayerPresenter.this.b.b();
            VastVideoPlayerPresenter.this.k();
        }
    };
    @NonNull
    private final a j = new a() {
        public final void a(long j, float f) {
            VastVideoPlayerPresenter.this.a.info(LogDomain.VAST, "VAST video has started", new Object[0]);
            VastVideoPlayerPresenter.this.b.a((float) j, f);
        }

        public final void a() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onVideoImpression", new Object[0]);
            VastVideoPlayerPresenter.this.b.e();
        }

        public final void a(long j, long j2) {
            VastVideoPlayerPresenter.this.b.a(j, j2);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(Consumer consumer) {
            VastVideoPlayerPresenter.b(VastVideoPlayerPresenter.this, consumer);
        }

        public final void a(float f, float f2) {
            VastVideoPlayerPresenter.this.b.a(f, f2, (ClickCallback) new ClickCallback() {
                public final void onUrlResolved(Consumer consumer) {
                    AnonymousClass2.this.a(consumer);
                }
            });
        }

        public final void b() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onVideoSkipped", new Object[0]);
            VastVideoPlayerPresenter.this.b.f();
            VastVideoPlayerPresenter.this.g.onEvent(a.VIDEO_SKIPPED);
        }

        public final void c() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onMuteClicked", new Object[0]);
            VastVideoPlayerPresenter.this.b.g();
        }

        public final void d() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onUnmuteClicked", new Object[0]);
            VastVideoPlayerPresenter.this.b.h();
        }

        public final void e() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onVideoCompleted", new Object[0]);
            VastVideoPlayerPresenter.this.b.i();
            VastVideoPlayerPresenter.this.g.onEvent(a.VIDEO_COMPLETED);
        }

        public final void a(int i) {
            VastVideoPlayerPresenter.this.a.error(LogDomain.VAST, "onVideoError", new Object[0]);
            VastVideoPlayerPresenter.this.b.a((int) ErrorCode.GENERAL_LINEAR_ERROR);
            VastVideoPlayerPresenter.this.g.onEvent(a.ERROR);
        }

        public final void f() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onVideoPaused", new Object[0]);
            VastVideoPlayerPresenter.this.b.j();
        }

        public final void g() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onVideoResumed", new Object[0]);
            VastVideoPlayerPresenter.this.b.k();
        }
    };
    @Nullable
    private VisibilityTracker k;
    /* access modifiers changed from: private */
    public boolean l;
    @NonNull
    private final StateMachine.Listener<b> m = new StateMachine.Listener() {
        public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
            VastVideoPlayerPresenter.this.a((b) obj, (b) obj2, metadata);
        }
    };
    @NonNull
    private final Listener n = new Listener() {
        /* access modifiers changed from: private */
        public /* synthetic */ void a(Consumer consumer) {
            VastVideoPlayerPresenter.b(VastVideoPlayerPresenter.this, consumer);
        }

        public final void onVastElementClicked(@Nullable String str) {
            VastVideoPlayerPresenter.this.b.a(str, (ClickCallback) new ClickCallback() {
                public final void onUrlResolved(Consumer consumer) {
                    AnonymousClass3.this.a(consumer);
                }
            });
        }

        public final void onVastElementRendered() {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onCompanionRendered", new Object[0]);
            VastVideoPlayerPresenter.this.b.l();
        }

        public final void onVastElementError(int i) {
            VastVideoPlayerPresenter.this.a.debug(LogDomain.VAST, "onCompanionError", new Object[0]);
            VastVideoPlayerPresenter.this.b.c(i);
            VastVideoPlayerPresenter.this.l = true;
        }

        public final void onRenderProcessGone() {
            VastVideoPlayerPresenter.this.b.b();
            VastVideoPlayerPresenter.this.k();
        }
    };

    /* access modifiers changed from: private */
    public /* synthetic */ void a(b bVar, b bVar2, Metadata metadata) {
        a(bVar2);
    }

    VastVideoPlayerPresenter(@NonNull Logger logger, @NonNull VastVideoPlayerModel vastVideoPlayerModel, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VastElementPresenter vastElementPresenter, @NonNull VastElementPresenter vastElementPresenter2, @NonNull e eVar, @NonNull StateMachine<a, b> stateMachine) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (VastVideoPlayerModel) Objects.requireNonNull(vastVideoPlayerModel);
        this.c = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.f = (VastElementPresenter) Objects.requireNonNull(vastElementPresenter);
        this.e = (VastElementPresenter) Objects.requireNonNull(vastElementPresenter2);
        this.d = (e) Objects.requireNonNull(eVar);
        this.g = (StateMachine) Objects.requireNonNull(stateMachine);
        this.d.a(this.j);
        this.f.setListener(this.n);
        this.e.setListener(this.i);
        this.g.addListener(this.m);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void l() {
        this.g.onEvent(a.CLICKED);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Consumer<Context> consumer) {
        Threads.runOnUi(new Runnable(consumer) {
            private final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                VastVideoPlayerPresenter.this.b(this.f$1);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Consumer consumer) {
        Objects.onNotNull(this.h.get(), new Consumer() {
            public final void accept(Object obj) {
                Consumer.this.accept(((VastVideoPlayerView) obj).getContext());
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VastVideoPlayerModel a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull VastVideoPlayerView vastVideoPlayerView) {
        c();
        this.h = new WeakReference<>(vastVideoPlayerView);
        vastVideoPlayerView.getIconView().setPresenter(this.e);
        vastVideoPlayerView.getCompanionAdView().setPresenter(this.f);
        VisibilityTrackerCreator visibilityTrackerCreator = this.c;
        VastVideoPlayerModel vastVideoPlayerModel = this.b;
        vastVideoPlayerModel.getClass();
        this.k = visibilityTrackerCreator.createTracker(vastVideoPlayerView, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                VastVideoPlayerModel.this.a();
            }
        });
        a((b) this.g.getCurrentState());
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        this.g.onEvent(a.CLOSE_BUTTON_CLICKED);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        j();
        Objects.onNotNull(this.h.get(), new Consumer() {
            public final void accept(Object obj) {
                VastVideoPlayerPresenter.this.b((VastVideoPlayerView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(VastVideoPlayerView vastVideoPlayerView) {
        this.h.clear();
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        this.b.d();
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        this.d.c();
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        this.d.d();
    }

    private void a(@NonNull b bVar) {
        if (!this.l || bVar != b.SHOW_COMPANION) {
            switch (bVar) {
                case SHOW_VIDEO:
                    i();
                    return;
                case SHOW_COMPANION:
                    h();
                    return;
                case CLOSE_PLAYER:
                    k();
                    return;
                default:
                    Logger logger = this.a;
                    LogDomain logDomain = LogDomain.VAST;
                    StringBuilder sb = new StringBuilder("Unknown state for VastVideoPlayer: ");
                    sb.append(bVar);
                    logger.error(logDomain, sb.toString(), new Object[0]);
                    k();
                    return;
            }
        } else {
            k();
        }
    }

    private void g() {
        this.d.b();
        c();
    }

    private void h() {
        VastVideoPlayerView vastVideoPlayerView = (VastVideoPlayerView) this.h.get();
        if (vastVideoPlayerView != null) {
            vastVideoPlayerView.hidePlayer();
            vastVideoPlayerView.showCompanion();
        }
    }

    private void i() {
        VideoPlayerView videoPlayerView;
        VastVideoPlayerView vastVideoPlayerView = (VastVideoPlayerView) this.h.get();
        if (vastVideoPlayerView == null) {
            videoPlayerView = null;
        } else {
            videoPlayerView = vastVideoPlayerView.getVideoPlayerView();
        }
        e eVar = this.d;
        eVar.getClass();
        Objects.onNotNull(videoPlayerView, new Consumer() {
            public final void accept(Object obj) {
                e.this.a((VideoPlayerView) obj);
            }
        });
    }

    private void j() {
        if (this.k != null) {
            this.k.destroy();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        this.b.c();
        g();
    }

    static /* synthetic */ void b(VastVideoPlayerPresenter vastVideoPlayerPresenter, Consumer consumer) {
        vastVideoPlayerPresenter.a(consumer);
        Threads.runOnUi(new Runnable() {
            public final void run() {
                VastVideoPlayerPresenter.this.l();
            }
        });
    }
}
