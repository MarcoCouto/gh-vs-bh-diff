package com.smaato.sdk.video.vast.player;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.player.VastVideoPlayer.EventListener;

/* renamed from: com.smaato.sdk.video.vast.player.-$$Lambda$oTG8YGM3hyR7DZO-xDO_ltfxKKw reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$oTG8YGM3hyR7DZOxDO_ltfxKKw implements Consumer {
    public static final /* synthetic */ $$Lambda$oTG8YGM3hyR7DZOxDO_ltfxKKw INSTANCE = new $$Lambda$oTG8YGM3hyR7DZOxDO_ltfxKKw();

    private /* synthetic */ $$Lambda$oTG8YGM3hyR7DZOxDO_ltfxKKw() {
    }

    public final void accept(Object obj) {
        ((EventListener) obj).onPaused();
    }
}
