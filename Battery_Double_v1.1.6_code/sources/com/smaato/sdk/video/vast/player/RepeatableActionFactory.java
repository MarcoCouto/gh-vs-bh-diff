package com.smaato.sdk.video.vast.player;

import android.os.Handler;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.utils.RepeatableAction;
import com.smaato.sdk.video.utils.RepeatableAction.Listener;

public class RepeatableActionFactory {
    @NonNull
    private final Handler a;

    public RepeatableActionFactory(@NonNull Handler handler) {
        this.a = (Handler) Objects.requireNonNull(handler);
    }

    @NonNull
    public RepeatableAction createRepeatableAction(@NonNull Listener listener) {
        return new RepeatableAction(this.a, listener);
    }
}
