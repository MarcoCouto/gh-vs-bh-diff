package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.video.vast.model.MediaFile;

public class VideoViewResizeManager {
    @NonNull
    private final Size a;

    private VideoViewResizeManager(@NonNull Size size) {
        this.a = size;
    }

    @NonNull
    public static VideoViewResizeManager create(@NonNull MediaFile mediaFile) {
        int i = 0;
        int round = mediaFile.width == null ? 0 : Math.round(mediaFile.width.floatValue());
        if (mediaFile.height != null) {
            i = Math.round(mediaFile.height.floatValue());
        }
        if (round == 0 || i == 0) {
            round = 16;
            i = 9;
        }
        return new VideoViewResizeManager(new Size(round, i));
    }

    public void resizeToContainerSizes(@NonNull VideoPlayerView videoPlayerView, int i, int i2) {
        float f = (float) i;
        float f2 = (float) i2;
        if (f / f2 > ((float) this.a.width) / ((float) this.a.height)) {
            i = Math.round(((float) this.a.width) * (f2 / ((float) this.a.height)));
        } else {
            i2 = Math.round(((float) this.a.height) * (f / ((float) this.a.width)));
        }
        videoPlayerView.a(i, i2);
    }
}
