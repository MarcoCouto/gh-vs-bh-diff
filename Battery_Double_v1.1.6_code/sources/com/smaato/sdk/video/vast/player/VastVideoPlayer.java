package com.smaato.sdk.video.vast.player;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.widget.VastVideoPlayerView;

public class VastVideoPlayer {
    /* access modifiers changed from: private */
    @NonNull
    public final VastVideoPlayerPresenter a;
    @NonNull
    private final VastVideoPlayerViewFactory b;

    public interface EventListener {
        void onAdClick();

        void onAdError();

        void onClose();

        void onCompanionShown();

        void onComplete();

        void onFirstQuartile();

        void onMidPoint();

        void onMute();

        void onPaused();

        void onResumed();

        void onSkipped();

        void onStart(float f, float f2);

        void onThirdQuartile();

        void onUnmute();

        void onVideoImpression();
    }

    VastVideoPlayer(@NonNull VastVideoPlayerPresenter vastVideoPlayerPresenter, @NonNull VastVideoPlayerViewFactory vastVideoPlayerViewFactory) {
        this.a = (VastVideoPlayerPresenter) Objects.requireNonNull(vastVideoPlayerPresenter);
        this.b = (VastVideoPlayerViewFactory) Objects.requireNonNull(vastVideoPlayerViewFactory);
    }

    @NonNull
    public AdContentView getNewVideoPlayerView(@NonNull Context context) {
        Objects.requireNonNull(context);
        final VastVideoPlayerView a2 = VastVideoPlayerViewFactory.a(context);
        a2.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(@NonNull View view) {
                VastVideoPlayer.this.a.a(a2);
            }

            public final void onViewDetachedFromWindow(@NonNull View view) {
                view.removeOnAttachStateChangeListener(this);
                VastVideoPlayer.this.a.c();
            }
        });
        return a2;
    }

    public void setEventListener(@NonNull EventListener eventListener) {
        this.a.a().a(eventListener);
    }

    public void loaded() {
        this.a.d();
    }

    public void pause() {
        this.a.e();
    }

    public void resume() {
        this.a.f();
    }

    public void onCloseClicked() {
        this.a.b();
    }
}
