package com.smaato.sdk.video.vast.player;

import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.util.fi.Consumer;

/* renamed from: com.smaato.sdk.video.vast.player.-$$Lambda$DiPlayerLayer$LombumqJYPK-KhbxnGZN18RZrEs reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiPlayerLayer$LombumqJYPKKhbxnGZN18RZrEs implements Consumer {
    public static final /* synthetic */ $$Lambda$DiPlayerLayer$LombumqJYPKKhbxnGZN18RZrEs INSTANCE = new $$Lambda$DiPlayerLayer$LombumqJYPKKhbxnGZN18RZrEs();

    private /* synthetic */ $$Lambda$DiPlayerLayer$LombumqJYPKKhbxnGZN18RZrEs() {
    }

    public final void accept(Object obj) {
        ((DiRegistry) obj).registerFactory(RepeatableActionFactory.class, $$Lambda$DiPlayerLayer$JhcDzORy2dbBMR6ZH0S5ZZKJSH4.INSTANCE);
    }
}
