package com.smaato.sdk.video.vast.player;

import android.net.Uri;
import android.view.Surface;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.player.exception.VideoPlayerException;

public interface VideoPlayer {

    public interface LifecycleListener {
        void onCompleted(@NonNull VideoPlayer videoPlayer);

        void onError(@NonNull VideoPlayer videoPlayer, @NonNull VideoPlayerException videoPlayerException);

        void onPaused(@NonNull VideoPlayer videoPlayer);

        void onReleased(@NonNull VideoPlayer videoPlayer);

        void onReset(@NonNull VideoPlayer videoPlayer);

        void onResumed(@NonNull VideoPlayer videoPlayer);

        void onSeekComplete(@NonNull VideoPlayer videoPlayer);

        void onStarted(@NonNull VideoPlayer videoPlayer);

        void onStopped(@NonNull VideoPlayer videoPlayer);
    }

    public interface OnVolumeChangeListener {
        void onVolumeChanged(float f);
    }

    public interface PrepareListener {
        void onError(@NonNull VideoPlayer videoPlayer, @NonNull VideoPlayerException videoPlayerException);

        void onInitialized(@NonNull VideoPlayer videoPlayer);

        void onPrepared(@NonNull VideoPlayer videoPlayer);

        void onPreparing(@NonNull VideoPlayer videoPlayer);
    }

    long getCurrentPositionMillis();

    @FloatRange(from = 0.0d, to = 1.0d)
    float getCurrentVolume();

    long getDuration();

    void pause();

    void prepare();

    void release();

    void reset();

    void seekTo(int i);

    void setDataSource(@NonNull Uri uri);

    void setDataSource(@NonNull String str);

    void setLifecycleListener(@Nullable LifecycleListener lifecycleListener);

    void setOnVolumeChangeListener(@Nullable OnVolumeChangeListener onVolumeChangeListener);

    void setPrepareListener(@Nullable PrepareListener prepareListener);

    void setSurface(@Nullable Surface surface);

    void setVolume(@FloatRange(from = 0.0d, to = 1.0d) float f);

    void start();

    void stop();
}
