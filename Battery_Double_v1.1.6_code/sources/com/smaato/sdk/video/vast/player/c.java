package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.notifier.ChangeSenderUtils;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.player.VastVideoPlayerModel.Quartile;
import com.smaato.sdk.video.vast.tracking.VastBeaconTrackerCreator;
import com.smaato.sdk.video.vast.tracking.VastErrorTracker;
import com.smaato.sdk.video.vast.tracking.VastEventTrackerCreator;

class c {
    @NonNull
    private final LinkResolver a;
    @NonNull
    private final VastEventTrackerCreator b;
    @NonNull
    private final VastBeaconTrackerCreator c;
    private final boolean d = true;

    c(@NonNull LinkResolver linkResolver, @NonNull VastEventTrackerCreator vastEventTrackerCreator, @NonNull VastBeaconTrackerCreator vastBeaconTrackerCreator, boolean z) {
        this.a = (LinkResolver) Objects.requireNonNull(linkResolver);
        this.b = (VastEventTrackerCreator) Objects.requireNonNull(vastEventTrackerCreator);
        this.c = (VastBeaconTrackerCreator) Objects.requireNonNull(vastBeaconTrackerCreator);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VastVideoPlayerModel a(@NonNull Logger logger, @NonNull VastScenario vastScenario, @NonNull SomaApiContext somaApiContext, @NonNull VastErrorTracker vastErrorTracker, boolean z) {
        ComponentClickHandler componentClickHandler = new ComponentClickHandler(logger, somaApiContext, this.a, vastScenario.vastMediaFileScenario.videoClicks);
        VastErrorTracker vastErrorTracker2 = vastErrorTracker;
        VastVideoPlayerModel vastVideoPlayerModel = new VastVideoPlayerModel(vastErrorTracker2, this.b.createEventTracker(vastScenario, somaApiContext), this.c.createBeaconTracker(vastScenario, somaApiContext), componentClickHandler, this.d, z, ChangeSenderUtils.createUniqueValueChangeSender(Quartile.ZERO));
        return vastVideoPlayerModel;
    }
}
