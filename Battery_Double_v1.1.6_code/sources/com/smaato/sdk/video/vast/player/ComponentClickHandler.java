package com.smaato.sdk.video.vast.player;

import android.content.Context;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.deeplink.UrlResolveListener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.VastBeacon;
import com.smaato.sdk.video.vast.model.VideoClicks;
import java.util.concurrent.atomic.AtomicReference;

class ComponentClickHandler {
    @Nullable
    private final VideoClicks a;
    @NonNull
    private final LinkResolver b;
    @NonNull
    private final SomaApiContext c;
    /* access modifiers changed from: private */
    @NonNull
    public final Logger d;
    /* access modifiers changed from: private */
    @NonNull
    public final AtomicReference<Task> e = new AtomicReference<>();

    public interface ClickCallback {
        void onUrlResolved(@NonNull Consumer<Context> consumer);
    }

    ComponentClickHandler(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull LinkResolver linkResolver, @Nullable VideoClicks videoClicks) {
        this.b = (LinkResolver) Objects.requireNonNull(linkResolver);
        this.c = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.d = (Logger) Objects.requireNonNull(logger);
        this.a = videoClicks;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable final String str, @NonNull final ClickCallback clickCallback) {
        if (TextUtils.isEmpty(str)) {
            VastBeacon vastBeacon = this.a == null ? null : this.a.clickThrough;
            if (vastBeacon == null) {
                str = null;
            } else {
                str = vastBeacon.uri;
            }
        }
        if (TextUtils.isEmpty(str)) {
            this.d.error(LogDomain.VAST, "Cannot handle click due to a missing URL", new Object[0]);
            return;
        }
        if (this.e.get() == null) {
            Task handleClickThroughUrl = this.b.handleClickThroughUrl(this.c, str, new UrlResolveListener() {
                public final void onSuccess(@NonNull Consumer<Context> consumer) {
                    clickCallback.onUrlResolved(consumer);
                    ComponentClickHandler.this.e.set(null);
                }

                public final void onError() {
                    Logger b2 = ComponentClickHandler.this.d;
                    LogDomain logDomain = LogDomain.VAST;
                    StringBuilder sb = new StringBuilder("Seems to be an invalid URL: ");
                    sb.append(str);
                    b2.error(logDomain, sb.toString(), new Object[0]);
                    ComponentClickHandler.this.e.set(null);
                }
            });
            this.e.set(handleClickThroughUrl);
            handleClickThroughUrl.start();
        }
    }
}
