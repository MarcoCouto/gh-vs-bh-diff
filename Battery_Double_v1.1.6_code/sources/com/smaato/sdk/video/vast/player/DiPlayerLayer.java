package com.smaato.sdk.video.vast.player;

import android.app.Application;
import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.appbgdetection.AppBackgroundDetector;
import com.smaato.sdk.core.deeplink.LinkResolver;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig;
import com.smaato.sdk.core.log.DiLogLayer;
import com.smaato.sdk.core.network.DiNetworkLayer;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.framework.VideoDiNames;
import com.smaato.sdk.video.utils.AnimationHelper;
import com.smaato.sdk.video.utils.EventValidator;
import com.smaato.sdk.video.vast.browser.VastWebComponentSecurityPolicy;
import com.smaato.sdk.video.vast.player.system.SystemMediaPlayerActionValidatorFactory;
import com.smaato.sdk.video.vast.player.system.SystemMediaPlayerCreator;
import com.smaato.sdk.video.vast.player.system.SystemMediaPlayerStateMachineFactory;
import com.smaato.sdk.video.vast.player.system.SystemMediaPlayerTransitionValidatorFactory;
import com.smaato.sdk.video.vast.tracking.VastBeaconTrackerCreator;
import com.smaato.sdk.video.vast.tracking.VastEventTrackerCreator;
import com.smaato.sdk.video.vast.tracking.macro.MacrosInjectorProviderFunction;
import com.smaato.sdk.video.vast.utils.VastScenarioResourceDataConverter;
import com.smaato.sdk.video.vast.widget.SurfaceViewVideoPlayerViewFactory;
import com.smaato.sdk.video.vast.widget.VideoPlayerViewFactory;
import com.smaato.sdk.video.vast.widget.companion.CompanionErrorCodeStrategy;
import com.smaato.sdk.video.vast.widget.companion.CompanionPresenterFactory;
import com.smaato.sdk.video.vast.widget.icon.IconErrorCodeStrategy;
import com.smaato.sdk.video.vast.widget.icon.IconPresenterFactory;

public final class DiPlayerLayer {
    private DiPlayerLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiPlayerLayer$Ef7we3qWgWlRnkhnGreelk_gRw.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void f(DiRegistry diRegistry) {
        diRegistry.registerFactory(VastEventTrackerCreator.class, $$Lambda$DiPlayerLayer$gv6EUgPBJBJxaTXVv7Gbxz4gjAs.INSTANCE);
        diRegistry.registerFactory(VastBeaconTrackerCreator.class, $$Lambda$DiPlayerLayer$PN9ciPiv97lng9uKJI9K1s3mA8.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastEventTrackerCreator t(DiConstructor diConstructor) {
        return new VastEventTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (MacrosInjectorProviderFunction) diConstructor.get(MacrosInjectorProviderFunction.class), DiNetworkLayer.getNetworkingExecutorServiceFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastBeaconTrackerCreator s(DiConstructor diConstructor) {
        return new VastBeaconTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), DiNetworkLayer.getBeaconTrackerFrom(diConstructor), (MacrosInjectorProviderFunction) diConstructor.get(MacrosInjectorProviderFunction.class), DiNetworkLayer.getNetworkingExecutorServiceFrom(diConstructor));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void e(DiRegistry diRegistry) {
        diRegistry.registerFactory(VastVideoPlayerCreator.class, $$Lambda$DiPlayerLayer$5cAu7HnhWYGqIE4seJUwmciWt8.INSTANCE);
        diRegistry.registerFactory(VastVideoPlayerViewFactory.class, $$Lambda$DiPlayerLayer$dOcijQdLWHAq4bq2YCBeAD1rfjM.INSTANCE);
        diRegistry.registerFactory(VideoPlayerViewFactory.class, $$Lambda$DiPlayerLayer$8_lOTukw0UwPuv8onnfyljkeGrk.INSTANCE);
        diRegistry.registerFactory(c.class, $$Lambda$DiPlayerLayer$WblYj4Ezwq4_1aBkDuGBnFpwLFI.INSTANCE);
        diRegistry.registerFactory(d.class, $$Lambda$DiPlayerLayer$VAn6O8LFCD3KwKjWGh9NrLAd42k.INSTANCE);
        diRegistry.registerFactory(VastVideoPlayerStateMachineFactory.class, $$Lambda$DiPlayerLayer$FygVLPoMFSq1nKMLSm_p7QYxTdA.INSTANCE);
        diRegistry.registerFactory(f.class, $$Lambda$DiPlayerLayer$8ECptD7ZrQUXx7ArU6nXU0Vn354.INSTANCE);
        diRegistry.registerFactory(VideoPlayerPreparer.class, $$Lambda$DiPlayerLayer$Uq4z9oX6Y3qZfWScgjyvX4hc5ao.INSTANCE);
        diRegistry.registerFactory(VastScenarioResourceDataConverter.class, $$Lambda$DiPlayerLayer$EqLVUsLb4dSq3CKX1NuP0uepS8.INSTANCE);
        diRegistry.addFrom(DiRegistry.of($$Lambda$DiPlayerLayer$LombumqJYPKKhbxnGZN18RZrEs.INSTANCE));
        diRegistry.addFrom(DiRegistry.of($$Lambda$DiPlayerLayer$kvpXfA8yoRvBbkz66d230z77cEg.INSTANCE));
        diRegistry.addFrom(DiRegistry.of($$Lambda$DiPlayerLayer$jZxLwfZbEHCziyzJBbrZCA2iuCA.INSTANCE));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastVideoPlayerCreator r(DiConstructor diConstructor) {
        return new VastVideoPlayerCreator((VastVideoPlayerViewFactory) diConstructor.get(VastVideoPlayerViewFactory.class), (c) diConstructor.get(c.class), (d) diConstructor.get(d.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastVideoPlayerViewFactory q(DiConstructor diConstructor) {
        return new VastVideoPlayerViewFactory();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoPlayerViewFactory p(DiConstructor diConstructor) {
        return new SurfaceViewVideoPlayerViewFactory();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ c o(DiConstructor diConstructor) {
        return new c((LinkResolver) diConstructor.get(LinkResolver.class), (VastEventTrackerCreator) diConstructor.get(VastEventTrackerCreator.class), (VastBeaconTrackerCreator) diConstructor.get(VastBeaconTrackerCreator.class), true);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ d n(DiConstructor diConstructor) {
        d dVar = new d((f) diConstructor.get(f.class), (CompanionPresenterFactory) diConstructor.get(CompanionPresenterFactory.class), (IconPresenterFactory) diConstructor.get(IconPresenterFactory.class), (VisibilityTrackerCreator) diConstructor.get(VideoDiNames.MODULE_DI_NAME, VisibilityTrackerCreator.class), (VastVideoPlayerStateMachineFactory) diConstructor.get(VastVideoPlayerStateMachineFactory.class));
        return dVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastVideoPlayerStateMachineFactory m(DiConstructor diConstructor) {
        return new VastVideoPlayerStateMachineFactory(b.SHOW_VIDEO);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f l(DiConstructor diConstructor) {
        return new f((VideoPlayerPreparer) diConstructor.get(VideoPlayerPreparer.class), (VisibilityTrackerCreator) diConstructor.get(VideoDiNames.MODULE_DI_NAME, VisibilityTrackerCreator.class), (RepeatableActionFactory) diConstructor.get(RepeatableActionFactory.class), true);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoPlayerPreparer k(DiConstructor diConstructor) {
        return new VideoPlayerPreparer((VideoPlayerCreator) diConstructor.get(VideoPlayerCreator.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VastScenarioResourceDataConverter j(DiConstructor diConstructor) {
        return new VastScenarioResourceDataConverter();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(DiRegistry diRegistry) {
        diRegistry.registerFactory(CompanionErrorCodeStrategy.class, $$Lambda$DiPlayerLayer$cmXp6rwSGZ_Zh88YJrNWyqYiM6s.INSTANCE);
        diRegistry.registerFactory(CompanionPresenterFactory.class, $$Lambda$DiPlayerLayer$9bdEwMlxUVzXkjZFnTvZLRXOsg.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ CompanionErrorCodeStrategy i(DiConstructor diConstructor) {
        return new CompanionErrorCodeStrategy();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ CompanionPresenterFactory h(DiConstructor diConstructor) {
        return new CompanionPresenterFactory((VastScenarioResourceDataConverter) diConstructor.get(VastScenarioResourceDataConverter.class), (VisibilityTrackerCreator) diConstructor.get("VAST_ELEMENT_VISIBILITY", VisibilityTrackerCreator.class), (VastWebComponentSecurityPolicy) diConstructor.get(VastWebComponentSecurityPolicy.class), (CompanionErrorCodeStrategy) diConstructor.get(CompanionErrorCodeStrategy.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(DiRegistry diRegistry) {
        diRegistry.registerFactory(IconErrorCodeStrategy.class, $$Lambda$DiPlayerLayer$loEEaw7lvqFFid5XyGiySXTB3A.INSTANCE);
        diRegistry.registerFactory(IconPresenterFactory.class, $$Lambda$DiPlayerLayer$J25UVvB7pCFCB0AZrQZJpAdnUug.INSTANCE);
        diRegistry.registerFactory("ICON_ANIMATION_HELPER", AnimationHelper.class, $$Lambda$DiPlayerLayer$rugJiji13ahYthEJioDL9Y3GkIc.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ IconErrorCodeStrategy g(DiConstructor diConstructor) {
        return new IconErrorCodeStrategy();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ IconPresenterFactory f(DiConstructor diConstructor) {
        IconPresenterFactory iconPresenterFactory = new IconPresenterFactory((VastScenarioResourceDataConverter) diConstructor.get(VastScenarioResourceDataConverter.class), (VisibilityTrackerCreator) diConstructor.get("VAST_ELEMENT_VISIBILITY", VisibilityTrackerCreator.class), (VastWebComponentSecurityPolicy) diConstructor.get(VastWebComponentSecurityPolicy.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class), (AnimationHelper) diConstructor.get("ICON_ANIMATION_HELPER", AnimationHelper.class), (IconErrorCodeStrategy) diConstructor.get(IconErrorCodeStrategy.class));
        return iconPresenterFactory;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AnimationHelper e(DiConstructor diConstructor) {
        return new AnimationHelper(300);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RepeatableActionFactory d(DiConstructor diConstructor) {
        return new RepeatableActionFactory(Threads.newUiHandler());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory("VAST_ELEMENT_VISIBILITY", VisibilityPrivateConfig.class, $$Lambda$DiPlayerLayer$m70jjqKAUTexm2Gh6HykXswJaRI.INSTANCE);
        diRegistry.registerFactory("VAST_ELEMENT_VISIBILITY", VisibilityTrackerCreator.class, $$Lambda$DiPlayerLayer$Wr1EhOTbYRUE_pzH3kIszEJJ90.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VisibilityTrackerCreator b(DiConstructor diConstructor) {
        VisibilityPrivateConfig visibilityPrivateConfig = (VisibilityPrivateConfig) diConstructor.get("VAST_ELEMENT_VISIBILITY", VisibilityPrivateConfig.class);
        VisibilityTrackerCreator visibilityTrackerCreator = new VisibilityTrackerCreator(DiLogLayer.getLoggerFrom(diConstructor), visibilityPrivateConfig.getVisibilityRatio(), visibilityPrivateConfig.getVisibilityTimeMillis(), (AppBackgroundDetector) diConstructor.get(AppBackgroundDetector.class), "VAST_ELEMENT_VISIBILITY");
        return visibilityTrackerCreator;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(String str, String str2, DiRegistry diRegistry) {
        diRegistry.registerFactory(VideoPlayerCreator.class, new ClassFactory(str, str2) {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiPlayerLayer.a(this.f$0, this.f$1, diConstructor);
            }
        });
        diRegistry.registerFactory(SystemMediaPlayerStateMachineFactory.class, $$Lambda$DiPlayerLayer$lRHRI4rbExCMUK4HhIte555QCAc.INSTANCE);
        diRegistry.registerFactory(str, EventValidator.class, new SystemMediaPlayerActionValidatorFactory());
        diRegistry.registerFactory(str2, EventValidator.class, new SystemMediaPlayerTransitionValidatorFactory());
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VideoPlayerCreator a(String str, String str2, DiConstructor diConstructor) {
        return new SystemMediaPlayerCreator((Context) diConstructor.get(Application.class), (SystemMediaPlayerStateMachineFactory) diConstructor.get(SystemMediaPlayerStateMachineFactory.class), (EventValidator) diConstructor.get(str, EventValidator.class), (EventValidator) diConstructor.get(str2, EventValidator.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ SystemMediaPlayerStateMachineFactory a(DiConstructor diConstructor) {
        return new SystemMediaPlayerStateMachineFactory(MediaPlayerState.IDLE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void g(DiRegistry diRegistry) {
        diRegistry.addFrom(DiRegistry.of($$Lambda$DiPlayerLayer$aaHUWot9UXKrBeBzscf6H8N8ro8.INSTANCE));
        diRegistry.addFrom(DiRegistry.of($$Lambda$DiPlayerLayer$nX3ZwgWNxoR07GZAsicqfG2UmGw.INSTANCE));
        diRegistry.addFrom(DiRegistry.of(new Consumer("VideoModuleInterfaceSystemMediaPlayerActionValidator", "VideoModuleInterfaceSystemMediaPlayerTransitionValidator") {
            private final /* synthetic */ String f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                DiPlayerLayer.a(this.f$0, this.f$1, (DiRegistry) obj);
            }
        }));
        diRegistry.addFrom(DiRegistry.of($$Lambda$DiPlayerLayer$ThrfS03bcFK0pcF9yTuHJngtNZQ.INSTANCE));
    }
}
