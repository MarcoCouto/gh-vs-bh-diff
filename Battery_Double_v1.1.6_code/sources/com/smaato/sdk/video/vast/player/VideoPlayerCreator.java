package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;

public interface VideoPlayerCreator {
    @NonNull
    VideoPlayer createVideoPlayer(@NonNull Logger logger);
}
