package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.tracking.VastErrorTracker;

public class VastVideoPlayerCreator {
    @NonNull
    private final VastVideoPlayerViewFactory a;
    @NonNull
    private final c b;
    @NonNull
    private final d c;

    VastVideoPlayerCreator(@NonNull VastVideoPlayerViewFactory vastVideoPlayerViewFactory, @NonNull c cVar, @NonNull d dVar) {
        this.a = (VastVideoPlayerViewFactory) Objects.requireNonNull(vastVideoPlayerViewFactory);
        this.b = (c) Objects.requireNonNull(cVar);
        this.c = (d) Objects.requireNonNull(dVar);
    }

    public void createVastVideoPlayer(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull VastScenario vastScenario, @NonNull VastErrorTracker vastErrorTracker, @NonNull NonNullConsumer<Either<VastVideoPlayer, Exception>> nonNullConsumer, @NonNull VideoTimings videoTimings) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(somaApiContext);
        Objects.requireNonNull(vastScenario);
        Objects.requireNonNull(vastErrorTracker);
        Objects.requireNonNull(nonNullConsumer);
        VideoTimings videoTimings2 = videoTimings;
        SomaApiContext somaApiContext2 = somaApiContext;
        VastVideoPlayerModel a2 = this.b.a(logger, vastScenario, somaApiContext2, vastErrorTracker, videoTimings2.isVideoClickable);
        Logger logger2 = logger;
        NonNullConsumer<Either<VastVideoPlayer, Exception>> nonNullConsumer2 = nonNullConsumer;
        this.c.a(logger, somaApiContext2, vastScenario, a2, vastErrorTracker, new NonNullConsumer(logger, nonNullConsumer) {
            private final /* synthetic */ Logger f$1;
            private final /* synthetic */ NonNullConsumer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                VastVideoPlayerCreator.this.a(this.f$1, this.f$2, (Either) obj);
            }
        }, videoTimings2);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, NonNullConsumer nonNullConsumer, Either either) {
        Exception exc = (Exception) either.right();
        if (exc != null) {
            nonNullConsumer.accept(Either.right(exc));
        } else {
            nonNullConsumer.accept(Either.left(new VastVideoPlayer((VastVideoPlayerPresenter) Objects.requireNonNull(either.left()), this.a)));
        }
    }
}
