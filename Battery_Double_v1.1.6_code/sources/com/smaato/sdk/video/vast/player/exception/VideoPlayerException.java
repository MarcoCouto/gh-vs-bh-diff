package com.smaato.sdk.video.vast.player.exception;

import androidx.annotation.NonNull;

public abstract class VideoPlayerException extends Exception {
    public VideoPlayerException(@NonNull String str) {
        super(str);
    }
}
