package com.smaato.sdk.video.vast.player;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.player.VastVideoPlayer.EventListener;

/* renamed from: com.smaato.sdk.video.vast.player.-$$Lambda$O7DCh2FczmlysYEc7Z-gafTB4FI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$O7DCh2FczmlysYEc7ZgafTB4FI implements Consumer {
    public static final /* synthetic */ $$Lambda$O7DCh2FczmlysYEc7ZgafTB4FI INSTANCE = new $$Lambda$O7DCh2FczmlysYEc7ZgafTB4FI();

    private /* synthetic */ $$Lambda$O7DCh2FczmlysYEc7ZgafTB4FI() {
    }

    public final void accept(Object obj) {
        ((EventListener) obj).onMute();
    }
}
