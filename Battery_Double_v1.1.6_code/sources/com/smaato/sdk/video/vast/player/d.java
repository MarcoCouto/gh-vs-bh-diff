package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.tracking.VastErrorTracker;
import com.smaato.sdk.video.vast.widget.companion.CompanionPresenterFactory;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenter;
import com.smaato.sdk.video.vast.widget.icon.IconPresenterFactory;

class d {
    @NonNull
    private final VisibilityTrackerCreator a;
    @NonNull
    private final f b;
    @NonNull
    private final CompanionPresenterFactory c;
    @NonNull
    private final IconPresenterFactory d;
    @NonNull
    private final VastVideoPlayerStateMachineFactory e;

    d(@NonNull f fVar, @NonNull CompanionPresenterFactory companionPresenterFactory, @NonNull IconPresenterFactory iconPresenterFactory, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VastVideoPlayerStateMachineFactory vastVideoPlayerStateMachineFactory) {
        this.b = (f) Objects.requireNonNull(fVar);
        this.c = (CompanionPresenterFactory) Objects.requireNonNull(companionPresenterFactory);
        this.d = (IconPresenterFactory) Objects.requireNonNull(iconPresenterFactory);
        this.a = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.e = (VastVideoPlayerStateMachineFactory) Objects.requireNonNull(vastVideoPlayerStateMachineFactory);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull VastScenario vastScenario, @NonNull VastVideoPlayerModel vastVideoPlayerModel, @NonNull VastErrorTracker vastErrorTracker, @NonNull NonNullConsumer<Either<VastVideoPlayerPresenter, Exception>> nonNullConsumer, @NonNull VideoTimings videoTimings) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(somaApiContext);
        Objects.requireNonNull(vastVideoPlayerModel);
        Objects.requireNonNull(nonNullConsumer);
        f fVar = this.b;
        VastScenario vastScenario2 = vastScenario;
        VastMediaFileScenario vastMediaFileScenario = vastScenario2.vastMediaFileScenario;
        $$Lambda$d$mSAnkJwLEB1X5bo0MVQBmhPhl6Q r0 = new NonNullConsumer(logger, somaApiContext, vastScenario2, vastVideoPlayerModel, nonNullConsumer) {
            private final /* synthetic */ Logger f$1;
            private final /* synthetic */ SomaApiContext f$2;
            private final /* synthetic */ VastScenario f$3;
            private final /* synthetic */ VastVideoPlayerModel f$4;
            private final /* synthetic */ NonNullConsumer f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            public final void accept(Object obj) {
                d.this.a(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, (Either) obj);
            }
        };
        fVar.a(logger, vastMediaFileScenario, vastErrorTracker, (NonNullConsumer<Either<e, Exception>>) r0, videoTimings);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, SomaApiContext somaApiContext, VastScenario vastScenario, VastVideoPlayerModel vastVideoPlayerModel, NonNullConsumer nonNullConsumer, Either either) {
        Exception exc = (Exception) either.right();
        if (exc != null) {
            nonNullConsumer.accept(Either.right(exc));
            return;
        }
        e eVar = (e) Objects.requireNonNull(either.left());
        VastElementPresenter create = this.d.create(logger, vastScenario.vastMediaFileScenario, somaApiContext);
        Logger logger2 = logger;
        VastVideoPlayerModel vastVideoPlayerModel2 = vastVideoPlayerModel;
        VastVideoPlayerPresenter vastVideoPlayerPresenter = new VastVideoPlayerPresenter(logger2, vastVideoPlayerModel2, this.a, this.c.create(logger, vastScenario, somaApiContext), create, eVar, this.e.create(vastScenario));
        nonNullConsumer.accept(Either.left(vastVideoPlayerPresenter));
    }
}
