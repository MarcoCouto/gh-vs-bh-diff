package com.smaato.sdk.video.vast.player;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.widget.VastVideoPlayerView;

public class VastVideoPlayerViewFactory {
    @NonNull
    static VastVideoPlayerView a(@NonNull Context context) {
        return new VastVideoPlayerView(context);
    }
}
