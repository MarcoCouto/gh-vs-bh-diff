package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Builder;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import com.smaato.sdk.video.vast.model.VastScenario;
import java.util.Arrays;

public class VastVideoPlayerStateMachineFactory {
    @NonNull
    private final b a;

    VastVideoPlayerStateMachineFactory(@NonNull b bVar) {
        this.a = (b) Objects.requireNonNull(bVar);
    }

    @NonNull
    public StateMachine<a, b> create(@NonNull VastScenario vastScenario) {
        VastCompanionScenario vastCompanionScenario = vastScenario.vastCompanionScenario;
        Builder builder = new Builder();
        b bVar = vastCompanionScenario == null ? b.CLOSE_PLAYER : b.SHOW_COMPANION;
        builder.setInitialState(this.a).addTransition(a.ERROR, Arrays.asList(new b[]{b.SHOW_VIDEO, b.CLOSE_PLAYER})).addTransition(a.ERROR, Arrays.asList(new b[]{b.SHOW_COMPANION, b.CLOSE_PLAYER})).addTransition(a.CLICKED, Arrays.asList(new b[]{b.SHOW_VIDEO, b.CLOSE_PLAYER})).addTransition(a.CLICKED, Arrays.asList(new b[]{b.SHOW_COMPANION, b.CLOSE_PLAYER})).addTransition(a.VIDEO_COMPLETED, Arrays.asList(new b[]{b.SHOW_VIDEO, bVar})).addTransition(a.VIDEO_SKIPPED, Arrays.asList(new b[]{b.SHOW_VIDEO, bVar})).addTransition(a.CLOSE_BUTTON_CLICKED, Arrays.asList(new b[]{b.SHOW_VIDEO, b.CLOSE_PLAYER})).addTransition(a.CLOSE_BUTTON_CLICKED, Arrays.asList(new b[]{b.SHOW_COMPANION, b.CLOSE_PLAYER}));
        return builder.build();
    }
}
