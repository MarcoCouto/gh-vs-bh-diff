package com.smaato.sdk.video.vast.player;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.framework.VisibilityPrivateConfig.Builder;

/* renamed from: com.smaato.sdk.video.vast.player.-$$Lambda$DiPlayerLayer$m70jjqKAUTexm2Gh6HykXswJaRI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiPlayerLayer$m70jjqKAUTexm2Gh6HykXswJaRI implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiPlayerLayer$m70jjqKAUTexm2Gh6HykXswJaRI INSTANCE = new $$Lambda$DiPlayerLayer$m70jjqKAUTexm2Gh6HykXswJaRI();

    private /* synthetic */ $$Lambda$DiPlayerLayer$m70jjqKAUTexm2Gh6HykXswJaRI() {
    }

    public final Object get(DiConstructor diConstructor) {
        return new Builder().visibilityRatio(1.0d).visibilityTimeMillis(100).build();
    }
}
