package com.smaato.sdk.video.vast.player;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.MediaFile;
import com.smaato.sdk.video.vast.player.VideoPlayer.PrepareListener;
import com.smaato.sdk.video.vast.player.exception.VideoPlayerException;

public class VideoPlayerPreparer {
    @NonNull
    private final VideoPlayerCreator a;

    public VideoPlayerPreparer(@NonNull VideoPlayerCreator videoPlayerCreator) {
        this.a = (VideoPlayerCreator) Objects.requireNonNull(videoPlayerCreator);
    }

    public void prepareNewVideoPlayer(@NonNull final Logger logger, @NonNull MediaFile mediaFile, @NonNull final NonNullConsumer<Either<VideoPlayer, Exception>> nonNullConsumer) {
        Objects.requireNonNull(logger);
        final Uri parse = Uri.parse(mediaFile.url);
        VideoPlayer createVideoPlayer = this.a.createVideoPlayer(logger);
        createVideoPlayer.setPrepareListener(new PrepareListener() {
            public final void onPreparing(@NonNull VideoPlayer videoPlayer) {
            }

            public final void onInitialized(@NonNull VideoPlayer videoPlayer) {
                logger.debug(LogDomain.VAST, "VAST VideoPlayer initialised. Preparing...", new Object[0]);
                videoPlayer.prepare();
            }

            public final void onPrepared(@NonNull VideoPlayer videoPlayer) {
                logger.debug(LogDomain.VAST, "VAST VideoPlayer prepared with DataSource: %s", parse);
                videoPlayer.setPrepareListener(null);
                nonNullConsumer.accept(Either.left(videoPlayer));
            }

            public final void onError(@NonNull VideoPlayer videoPlayer, @NonNull VideoPlayerException videoPlayerException) {
                logger.error(LogDomain.VAST, String.format("Unable to prepare VAST VideoPlayer with DataSource: %s", new Object[]{parse}), new Object[0]);
                videoPlayer.setPrepareListener(null);
                nonNullConsumer.accept(Either.right(videoPlayerException));
            }
        });
        logger.debug(LogDomain.VAST, "Initialising VAST VideoPlayer with DataSource: %s", parse);
        createVideoPlayer.setDataSource(parse.toString());
    }
}
