package com.smaato.sdk.video.vast.player;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.notifier.ChangeNotifier.Listener;
import com.smaato.sdk.core.util.notifier.ChangeSender;
import com.smaato.sdk.video.vast.model.VastBeaconEvent;
import com.smaato.sdk.video.vast.model.VastEvent;
import com.smaato.sdk.video.vast.player.ComponentClickHandler.ClickCallback;
import com.smaato.sdk.video.vast.player.VastVideoPlayer.EventListener;
import com.smaato.sdk.video.vast.player.VastVideoPlayerModel.Quartile;
import com.smaato.sdk.video.vast.tracking.VastBeaconTracker;
import com.smaato.sdk.video.vast.tracking.VastErrorTracker;
import com.smaato.sdk.video.vast.tracking.VastEventTracker;
import com.smaato.sdk.video.vast.tracking.macro.PlayerState;
import com.smaato.sdk.video.vast.tracking.macro.PlayerState.Builder;
import java.util.concurrent.atomic.AtomicReference;

final class VastVideoPlayerModel {
    @NonNull
    private final VastBeaconTracker a;
    @NonNull
    private final VastEventTracker b;
    @NonNull
    private final VastErrorTracker c;
    @NonNull
    private final AtomicReference<EventListener> d = new AtomicReference<>();
    @NonNull
    private final ComponentClickHandler e;
    @NonNull
    private final ChangeSender<Quartile> f;
    @NonNull
    private final Listener<Quartile> g = new Listener() {
        public final void onNextValue(Object obj) {
            VastVideoPlayerModel.this.a((Quartile) obj);
        }
    };
    private final boolean h;
    private boolean i;
    private long j;
    private float k;
    private float l;

    public enum Quartile {
        ZERO,
        FIRST,
        MID,
        THIRD
    }

    final class a implements ClickCallback {
        @NonNull
        private final ClickCallback a;

        /* synthetic */ a(VastVideoPlayerModel vastVideoPlayerModel, ClickCallback clickCallback, byte b2) {
            this(clickCallback);
        }

        private a(ClickCallback clickCallback) {
            this.a = clickCallback;
        }

        public final void onUrlResolved(@NonNull Consumer<Context> consumer) {
            this.a.onUrlResolved(consumer);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
    }

    VastVideoPlayerModel(@NonNull VastErrorTracker vastErrorTracker, @NonNull VastEventTracker vastEventTracker, @NonNull VastBeaconTracker vastBeaconTracker, @NonNull ComponentClickHandler componentClickHandler, boolean z, boolean z2, @NonNull ChangeSender<Quartile> changeSender) {
        this.c = (VastErrorTracker) Objects.requireNonNull(vastErrorTracker);
        this.b = (VastEventTracker) Objects.requireNonNull(vastEventTracker);
        this.a = (VastBeaconTracker) Objects.requireNonNull(vastBeaconTracker);
        this.e = (ComponentClickHandler) Objects.requireNonNull(componentClickHandler);
        this.i = z;
        this.h = z2;
        this.f = changeSender;
        changeSender.addListener(this.g);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull EventListener eventListener) {
        this.d.set(eventListener);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        Objects.onNotNull(this.d.get(), $$Lambda$rxzlszxUvTfhZTPxg_FsLlOtmzk.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        Objects.onNotNull(this.d.get(), $$Lambda$hcuRu8OsTOVrv3fWzCW4zS7dHA.INSTANCE);
        this.b.triggerEventByName(VastEvent.CLOSE_LINEAR, n());
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        this.b.triggerEventByName(VastEvent.LOADED, n());
    }

    /* access modifiers changed from: 0000 */
    public final void a(float f2, float f3) {
        Objects.onNotNull(this.d.get(), new Consumer(f2, f3) {
            private final /* synthetic */ float f$0;
            private final /* synthetic */ float f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ((EventListener) obj).onStart(this.f$0, this.f$1);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        a(VastBeaconEvent.SMAATO_VIEWABLE_IMPRESSION);
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        this.b.triggerEventByName(VastEvent.SKIP, n());
        Objects.onNotNull(this.d.get(), $$Lambda$Dh_Ymce1nc8aiUkBl7y3AeHH19Q.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        this.i = true;
        this.b.triggerEventByName(VastEvent.MUTE, n());
        Objects.onNotNull(this.d.get(), $$Lambda$O7DCh2FczmlysYEc7ZgafTB4FI.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        this.i = false;
        this.b.triggerEventByName(VastEvent.UNMUTE, n());
        Objects.onNotNull(this.d.get(), $$Lambda$qiSj9ER82EqvUFANsAKa9ze8.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void i() {
        Objects.onNotNull(this.d.get(), $$Lambda$5XODgJXw9fkBonBTry1OvLS83Fg.INSTANCE);
        this.b.triggerEventByName(VastEvent.COMPLETE, n());
    }

    /* access modifiers changed from: 0000 */
    public final void j() {
        this.b.triggerEventByName(VastEvent.PAUSE, n());
        Objects.onNotNull(this.d.get(), $$Lambda$oTG8YGM3hyR7DZOxDO_ltfxKKw.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void a(long j2, long j3) {
        this.j = j2;
        this.b.triggerProgressDependentEvent(n(), j3);
        float f2 = ((float) j2) / ((float) j3);
        if (f2 >= 0.01f) {
            a(VastBeaconEvent.SMAATO_IMPRESSION);
            Objects.onNotNull(this.d.get(), $$Lambda$rjmACulradfMI0bMLMkbGk0j5pM.INSTANCE);
        }
        Quartile quartile = Quartile.ZERO;
        if (f2 >= 0.25f && f2 < 0.5f) {
            quartile = Quartile.FIRST;
        } else if (f2 >= 0.5f && f2 < 0.75f) {
            quartile = Quartile.MID;
        } else if (f2 >= 0.75f) {
            quartile = Quartile.THIRD;
        }
        this.f.newValue(quartile);
    }

    /* access modifiers changed from: 0000 */
    public final void k() {
        this.b.triggerEventByName(VastEvent.RESUME, n());
        Objects.onNotNull(this.d.get(), $$Lambda$2gw2hPvCv9LlOxBvCXtbvmr4DSE.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void l() {
        this.b.triggerEventByName(VastEvent.CREATIVE_VIEW, n());
        Objects.onNotNull(this.d.get(), $$Lambda$XJZ4N7GNxDaJJrqnASUMZPLIM2M.INSTANCE);
    }

    /* access modifiers changed from: 0000 */
    public final void m() {
        a(VastBeaconEvent.SMAATO_ICON_VIEW_TRACKING);
    }

    /* access modifiers changed from: 0000 */
    public final void a(int i2) {
        d(i2);
    }

    /* access modifiers changed from: 0000 */
    public final void b(int i2) {
        d(i2);
    }

    /* access modifiers changed from: 0000 */
    public final void c(int i2) {
        d(i2);
    }

    private void d(int i2) {
        this.c.track(new Builder().setOffsetMillis(this.j).setMuted(this.i).setErrorCode(i2).setClickPositionX(this.k).setClickPositionY(this.l).build());
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Quartile quartile) {
        EventListener eventListener = (EventListener) this.d.get();
        if (eventListener != null) {
            switch (quartile) {
                case FIRST:
                    eventListener.onFirstQuartile();
                    return;
                case MID:
                    eventListener.onMidPoint();
                    return;
                case THIRD:
                    eventListener.onThirdQuartile();
                    break;
            }
        }
    }

    private void a(@NonNull VastBeaconEvent vastBeaconEvent) {
        this.a.trigger(vastBeaconEvent, n());
    }

    /* access modifiers changed from: 0000 */
    public final void a(float f2, float f3, @NonNull ClickCallback clickCallback) {
        if (this.h) {
            this.k = f2;
            this.l = f3;
            a(VastBeaconEvent.SMAATO_VIDEO_CLICK_TRACKING);
            Objects.onNotNull(this.d.get(), $$Lambda$7kWMfJwzYbFCTxButyHgxNnTet8.INSTANCE);
            this.e.a(null, new a(this, clickCallback, 0));
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable String str, @NonNull ClickCallback clickCallback) {
        a(VastBeaconEvent.SMAATO_COMPANION_CLICK_TRACKING);
        Objects.onNotNull(this.d.get(), $$Lambda$7kWMfJwzYbFCTxButyHgxNnTet8.INSTANCE);
        this.e.a(str, new a(this, clickCallback, 0));
    }

    /* access modifiers changed from: 0000 */
    public final void b(@Nullable String str, @NonNull ClickCallback clickCallback) {
        a(VastBeaconEvent.SMAATO_ICON_CLICK_TRACKING);
        Objects.onNotNull(this.d.get(), $$Lambda$7kWMfJwzYbFCTxButyHgxNnTet8.INSTANCE);
        this.e.a(str, clickCallback);
    }

    @NonNull
    private PlayerState n() {
        return new Builder().setOffsetMillis(this.j).setMuted(this.i).setClickPositionX(this.k).setClickPositionY(this.l).build();
    }
}
