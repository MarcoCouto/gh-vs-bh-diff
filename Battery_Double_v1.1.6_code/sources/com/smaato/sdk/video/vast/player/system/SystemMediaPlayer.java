package com.smaato.sdk.video.vast.player.system;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.Uri;
import android.view.Surface;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Metadata.Builder;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.StateMachine.Listener;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.utils.EventValidator;
import com.smaato.sdk.video.vast.player.MediaPlayerAction;
import com.smaato.sdk.video.vast.player.MediaPlayerState;
import com.smaato.sdk.video.vast.player.MediaPlayerTransition;
import com.smaato.sdk.video.vast.player.VideoPlayer;
import com.smaato.sdk.video.vast.player.VideoPlayer.LifecycleListener;
import com.smaato.sdk.video.vast.player.VideoPlayer.OnVolumeChangeListener;
import com.smaato.sdk.video.vast.player.VideoPlayer.PrepareListener;
import com.smaato.sdk.video.vast.player.exception.VideoPlayerException;
import java.io.IOException;

public class SystemMediaPlayer implements VideoPlayer {
    @NonNull
    private final MediaPlayer a;
    @NonNull
    private final StateMachine<MediaPlayerTransition, MediaPlayerState> b;
    @NonNull
    private final EventValidator<MediaPlayerAction, MediaPlayerState> c;
    @NonNull
    private final EventValidator<MediaPlayerTransition, MediaPlayerState> d;
    @NonNull
    private final Logger e;
    @NonNull
    private Context f;
    @Nullable
    private PrepareListener g;
    @Nullable
    private LifecycleListener h;
    @Nullable
    private OnVolumeChangeListener i;
    private float j = -1.0f;

    SystemMediaPlayer(@NonNull Context context, @NonNull MediaPlayer mediaPlayer, @NonNull StateMachine<MediaPlayerTransition, MediaPlayerState> stateMachine, @NonNull EventValidator<MediaPlayerAction, MediaPlayerState> eventValidator, @NonNull EventValidator<MediaPlayerTransition, MediaPlayerState> eventValidator2, @NonNull Logger logger) {
        this.f = (Context) Objects.requireNonNull(context, "Parameter context should not be null for SystemMediaPlayer::new");
        this.a = (MediaPlayer) Objects.requireNonNull(mediaPlayer, "Parameter mediaPlayer should not be null for SystemMediaPlayer::new");
        this.b = (StateMachine) Objects.requireNonNull(stateMachine, "Parameter mediaPlayerStatMachine should not be null for SystemMediaPlayer::new");
        this.c = (EventValidator) Objects.requireNonNull(eventValidator, "Parameter mediaPlayerActionsValidator should not be null for SystemMediaPlayer::new");
        this.d = (EventValidator) Objects.requireNonNull(eventValidator2, "Parameter mediaPlayerTransitionsValidator should not be null for SystemMediaPlayer::new");
        this.e = (Logger) Objects.requireNonNull(logger, "Parameter logger should not be null for SystemMediaPlayer::new");
        mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
            public final void onCompletion(MediaPlayer mediaPlayer) {
                SystemMediaPlayer.this.b(mediaPlayer);
            }
        });
        mediaPlayer.setOnErrorListener(new OnErrorListener() {
            public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                return SystemMediaPlayer.this.b(mediaPlayer, i, i2);
            }
        });
        mediaPlayer.setOnInfoListener(new OnInfoListener() {
            public final boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                return SystemMediaPlayer.this.a(mediaPlayer, i, i2);
            }
        });
        mediaPlayer.setOnPreparedListener(new OnPreparedListener() {
            public final void onPrepared(MediaPlayer mediaPlayer) {
                SystemMediaPlayer.this.c(mediaPlayer);
            }
        });
        mediaPlayer.setOnSeekCompleteListener(new OnSeekCompleteListener() {
            public final void onSeekComplete(MediaPlayer mediaPlayer) {
                SystemMediaPlayer.this.a(mediaPlayer);
            }
        });
        stateMachine.addListener(new Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                SystemMediaPlayer.this.a((MediaPlayerState) obj, (MediaPlayerState) obj2, metadata);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull MediaPlayer mediaPlayer) {
        if (a(MediaPlayerAction.SEEK_TO) && this.h != null) {
            this.h.onSeekComplete(this);
        }
    }

    /* access modifiers changed from: private */
    public void b(@NonNull MediaPlayer mediaPlayer) {
        if (a(MediaPlayerTransition.ON_COMPLETE)) {
            this.b.onEvent(MediaPlayerTransition.ON_COMPLETE);
        } else {
            this.b.onEvent(MediaPlayerTransition.ON_ERROR);
        }
    }

    /* access modifiers changed from: private */
    public void c(@NonNull MediaPlayer mediaPlayer) {
        if (a(MediaPlayerTransition.ON_PREPARED)) {
            this.b.onEvent(MediaPlayerTransition.ON_PREPARED);
        } else {
            this.b.onEvent(MediaPlayerTransition.ON_ERROR);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(@NonNull MediaPlayer mediaPlayer, int i2, int i3) {
        this.e.info(LogDomain.VAST, "MediaPlayer Info: [what: %d, extra: %d]; For more details check android.media.MediaPlayer info codes", Integer.valueOf(i2), Integer.valueOf(i3));
        return false;
    }

    /* access modifiers changed from: private */
    public boolean b(@NonNull MediaPlayer mediaPlayer, int i2, int i3) {
        this.e.error(LogDomain.VAST, "MediaPlayer Error: [what: %d, extra: %d]; For more details check android.media.MediaPlayer error codes", Integer.valueOf(i2), Integer.valueOf(i3));
        this.b.onEvent(MediaPlayerTransition.ON_ERROR, new Builder().putInt("what", i2).putInt("extra", i3).build());
        return true;
    }

    /* access modifiers changed from: private */
    public void a(@NonNull MediaPlayerState mediaPlayerState, @NonNull MediaPlayerState mediaPlayerState2, @Nullable Metadata metadata) {
        switch (mediaPlayerState2) {
            case IDLE:
                if (this.h != null) {
                    this.h.onReset(this);
                    return;
                }
                break;
            case INITIALIZED:
                if (this.g != null) {
                    this.g.onInitialized(this);
                    return;
                }
                break;
            case PREPARING:
                if (this.g != null) {
                    this.g.onPreparing(this);
                    return;
                }
                break;
            case PREPARED:
                if (this.g != null) {
                    this.g.onPrepared(this);
                    return;
                }
                break;
            case STARTED:
                if (this.h != null) {
                    this.h.onStarted(this);
                    return;
                }
                break;
            case RESUMED:
                if (this.h != null) {
                    this.h.onResumed(this);
                    return;
                }
                break;
            case PAUSED:
                if (this.h != null) {
                    this.h.onPaused(this);
                    return;
                }
                break;
            case STOPPED:
                if (this.h != null) {
                    this.h.onStopped(this);
                    return;
                }
                break;
            case PLAYBACK_COMPLETED:
                if (this.h != null) {
                    this.h.onCompleted(this);
                    return;
                }
                break;
            case ERROR:
                VideoPlayerException a2 = a.a(metadata);
                if (this.h != null) {
                    this.h.onError(this, a2);
                }
                if (this.g != null) {
                    this.g.onError(this, a2);
                    return;
                }
                break;
            case END:
                if (this.h != null) {
                    this.h.onReleased(this);
                    return;
                }
                break;
            default:
                throw new IllegalArgumentException(String.format("Unexpected MediaPlayerState: %s", new Object[]{mediaPlayerState2}));
        }
    }

    public void setDataSource(@NonNull Uri uri) {
        if (a(MediaPlayerTransition.SET_DATA_SOURCE)) {
            try {
                this.a.setDataSource(this.f, uri);
                this.b.onEvent(MediaPlayerTransition.SET_DATA_SOURCE);
            } catch (IOException | IllegalArgumentException | SecurityException e2) {
                this.e.error(LogDomain.VAST, "Unable to set DataSource uri:[%s] to MediaPlayer. Exception %s", uri, e2);
                this.b.onEvent(MediaPlayerTransition.ON_ERROR);
            }
        }
    }

    public void setDataSource(@NonNull String str) {
        if (a(MediaPlayerTransition.SET_DATA_SOURCE)) {
            try {
                this.a.setDataSource(str);
                this.b.onEvent(MediaPlayerTransition.SET_DATA_SOURCE);
            } catch (IOException | IllegalArgumentException | SecurityException e2) {
                this.e.error(LogDomain.VAST, "Unable to set DataSource path:[%s] to MediaPlayer. Exception %s", str, e2);
                this.b.onEvent(MediaPlayerTransition.ON_ERROR);
            }
        }
    }

    public void prepare() {
        if (a(MediaPlayerTransition.PREPARE_ASYNC)) {
            try {
                this.b.onEvent(MediaPlayerTransition.PREPARE_ASYNC);
                this.a.prepare();
            } catch (IOException e2) {
                this.e.error(LogDomain.VAST, "Unable to prepare DataSource for MediaPlayer. Exception %s", e2);
                this.b.onEvent(MediaPlayerTransition.ON_ERROR);
            }
        }
    }

    public void start() {
        if (a(MediaPlayerTransition.START)) {
            this.a.start();
            this.b.onEvent(MediaPlayerTransition.START);
        }
    }

    public void pause() {
        if (a(MediaPlayerTransition.PAUSE)) {
            this.a.pause();
            this.b.onEvent(MediaPlayerTransition.PAUSE);
        }
    }

    public void stop() {
        if (a(MediaPlayerTransition.STOP)) {
            this.a.stop();
            this.b.onEvent(MediaPlayerTransition.STOP);
        }
    }

    public void seekTo(int i2) {
        if (a(MediaPlayerAction.SEEK_TO)) {
            this.a.seekTo(i2);
        }
    }

    public void reset() {
        if (a(MediaPlayerTransition.RESET)) {
            this.a.reset();
            this.b.onEvent(MediaPlayerTransition.RESET);
        }
    }

    public void release() {
        if (a(MediaPlayerTransition.RELEASE)) {
            this.a.release();
            this.a.setOnCompletionListener(null);
            this.a.setOnErrorListener(null);
            this.a.setOnInfoListener(null);
            this.a.setOnPreparedListener(null);
            this.a.setOnSeekCompleteListener(null);
            this.i = null;
            this.b.onEvent(MediaPlayerTransition.RELEASE);
            this.b.deleteListeners();
        }
    }

    public void setPrepareListener(@Nullable PrepareListener prepareListener) {
        this.g = prepareListener;
    }

    public void setLifecycleListener(@Nullable LifecycleListener lifecycleListener) {
        this.h = lifecycleListener;
    }

    public void setSurface(@Nullable Surface surface) {
        if (a(MediaPlayerAction.SET_SURFACE)) {
            this.a.setSurface(surface);
        }
    }

    public void setOnVolumeChangeListener(@Nullable OnVolumeChangeListener onVolumeChangeListener) {
        this.i = onVolumeChangeListener;
    }

    public void setVolume(@FloatRange(from = 0.0d, to = 1.0d) float f2) {
        if ((Math.abs(f2 - this.j) > 0.0f) && a(MediaPlayerAction.SET_VOLUME)) {
            this.a.setVolume(f2, f2);
            this.j = f2;
            Objects.onNotNull(this.i, new Consumer() {
                public final void accept(Object obj) {
                    SystemMediaPlayer.this.a((OnVolumeChangeListener) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(OnVolumeChangeListener onVolumeChangeListener) {
        onVolumeChangeListener.onVolumeChanged(this.j);
    }

    public float getCurrentVolume() {
        return this.j;
    }

    public long getCurrentPositionMillis() {
        if (a(MediaPlayerAction.GET_CURRENT_POSITION)) {
            return (long) this.a.getCurrentPosition();
        }
        return 0;
    }

    public long getDuration() {
        if (a(MediaPlayerAction.GET_DURATION)) {
            return (long) this.a.getDuration();
        }
        return 0;
    }

    @NonNull
    public MediaPlayerState getState() {
        return (MediaPlayerState) this.b.getCurrentState();
    }

    private boolean a(@NonNull MediaPlayerAction mediaPlayerAction) {
        MediaPlayerState mediaPlayerState = (MediaPlayerState) this.b.getCurrentState();
        if (this.c.isValid(mediaPlayerAction, mediaPlayerState)) {
            return true;
        }
        this.e.error(LogDomain.VAST, "Invalid MediaPlayer state: %s, for action: %s ", mediaPlayerState, mediaPlayerAction);
        return false;
    }

    private boolean a(@NonNull MediaPlayerTransition mediaPlayerTransition) {
        MediaPlayerState mediaPlayerState = (MediaPlayerState) this.b.getCurrentState();
        if (this.d.isValid(mediaPlayerTransition, mediaPlayerState)) {
            return true;
        }
        this.e.error(LogDomain.VAST, "Invalid MediaPlayer state: %s, for transition: %s ", mediaPlayerState, mediaPlayerTransition);
        return false;
    }
}
