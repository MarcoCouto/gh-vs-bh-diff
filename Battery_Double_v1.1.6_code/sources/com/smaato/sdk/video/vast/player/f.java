package com.smaato.sdk.video.vast.player;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.smaato.sdk.video.vast.model.MediaFile;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.player.exception.MalformedVideoPlayerException;
import com.smaato.sdk.video.vast.player.exception.UnsupportedVideoPlayerException;
import com.smaato.sdk.video.vast.tracking.VastErrorTracker;
import com.smaato.sdk.video.vast.tracking.macro.PlayerState.Builder;

class f {
    @NonNull
    private final VisibilityTrackerCreator a;
    @NonNull
    private final VideoPlayerPreparer b;
    @NonNull
    private final RepeatableActionFactory c;
    private final boolean d = true;

    f(@NonNull VideoPlayerPreparer videoPlayerPreparer, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull RepeatableActionFactory repeatableActionFactory, boolean z) {
        this.b = (VideoPlayerPreparer) Objects.requireNonNull(videoPlayerPreparer);
        this.a = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.c = (RepeatableActionFactory) Objects.requireNonNull(repeatableActionFactory);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Logger logger, @NonNull VastMediaFileScenario vastMediaFileScenario, @NonNull VastErrorTracker vastErrorTracker, @NonNull NonNullConsumer<Either<e, Exception>> nonNullConsumer, @NonNull VideoTimings videoTimings) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(nonNullConsumer);
        VideoPlayerPreparer videoPlayerPreparer = this.b;
        MediaFile mediaFile = vastMediaFileScenario.mediaFile;
        $$Lambda$f$YN3hdFBDhAGWW_sU7s4AJRwgCs r2 = new NonNullConsumer(vastMediaFileScenario, vastErrorTracker, nonNullConsumer, videoTimings) {
            private final /* synthetic */ VastMediaFileScenario f$1;
            private final /* synthetic */ VastErrorTracker f$2;
            private final /* synthetic */ NonNullConsumer f$3;
            private final /* synthetic */ VideoTimings f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void accept(Object obj) {
                f.this.a(this.f$1, this.f$2, this.f$3, this.f$4, (Either) obj);
            }
        };
        videoPlayerPreparer.prepareNewVideoPlayer(logger, mediaFile, r2);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(VastMediaFileScenario vastMediaFileScenario, VastErrorTracker vastErrorTracker, NonNullConsumer nonNullConsumer, VideoTimings videoTimings, Either either) {
        MediaFile mediaFile = vastMediaFileScenario.mediaFile;
        Exception exc = (Exception) either.right();
        if (exc == null) {
            VideoPlayer videoPlayer = (VideoPlayer) Objects.requireNonNull(either.left());
            if (Math.abs(videoPlayer.getDuration() - vastMediaFileScenario.duration) > 3000) {
                vastErrorTracker.track(new Builder().setErrorCode(202).build());
                nonNullConsumer.accept(Either.right(new Exception("Video player expecting different duration")));
                return;
            }
            VideoViewResizeManager create = VideoViewResizeManager.create(mediaFile);
            SkipButtonVisibilityManager create2 = SkipButtonVisibilityManager.create(videoTimings);
            videoPlayer.setVolume(this.d ? 0.0f : 1.0f);
            e eVar = new e(videoPlayer, vastMediaFileScenario, create, create2, this.a, this.c);
            nonNullConsumer.accept(Either.left(eVar));
            return;
        }
        try {
            throw exc;
        } catch (MalformedVideoPlayerException | UnsupportedVideoPlayerException unused) {
            vastErrorTracker.track(new Builder().setErrorCode(405).build());
        } catch (Exception unused2) {
            vastErrorTracker.track(new Builder().setErrorCode(ErrorCode.GENERAL_LINEAR_ERROR).build());
        }
        nonNullConsumer.accept(Either.right(exc));
    }
}
