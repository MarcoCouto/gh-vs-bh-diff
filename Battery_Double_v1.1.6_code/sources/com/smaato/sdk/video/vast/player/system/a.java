package com.smaato.sdk.video.vast.player.system;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.video.vast.player.exception.IOVideoPlayerException;
import com.smaato.sdk.video.vast.player.exception.MalformedVideoPlayerException;
import com.smaato.sdk.video.vast.player.exception.TimeoutVideoPlayerException;
import com.smaato.sdk.video.vast.player.exception.UnknownVideoPlayerException;
import com.smaato.sdk.video.vast.player.exception.UnsupportedVideoPlayerException;
import com.smaato.sdk.video.vast.player.exception.VideoPlayerException;

final class a {
    @NonNull
    static VideoPlayerException a(@Nullable Metadata metadata) {
        Integer num = null;
        Integer num2 = metadata == null ? null : metadata.getInt("what");
        if (metadata != null) {
            num = metadata.getInt("extra");
        }
        if (num2 == null) {
            return new UnknownVideoPlayerException();
        }
        if (num2.intValue() != 1) {
            return new UnknownVideoPlayerException();
        }
        if (num == null) {
            return new UnknownVideoPlayerException();
        }
        int intValue = num.intValue();
        if (intValue == -1010) {
            return new UnsupportedVideoPlayerException();
        }
        if (intValue == -1007) {
            return new MalformedVideoPlayerException();
        }
        if (intValue == -1004) {
            return new IOVideoPlayerException();
        }
        if (intValue != -110) {
            return new UnknownVideoPlayerException();
        }
        return new TimeoutVideoPlayerException();
    }
}
