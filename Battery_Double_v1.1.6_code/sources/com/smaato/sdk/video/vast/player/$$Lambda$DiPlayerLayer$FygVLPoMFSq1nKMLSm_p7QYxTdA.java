package com.smaato.sdk.video.vast.player;

import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;

/* renamed from: com.smaato.sdk.video.vast.player.-$$Lambda$DiPlayerLayer$FygVLPoMFSq1nKMLSm_p7QYxTdA reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$DiPlayerLayer$FygVLPoMFSq1nKMLSm_p7QYxTdA implements ClassFactory {
    public static final /* synthetic */ $$Lambda$DiPlayerLayer$FygVLPoMFSq1nKMLSm_p7QYxTdA INSTANCE = new $$Lambda$DiPlayerLayer$FygVLPoMFSq1nKMLSm_p7QYxTdA();

    private /* synthetic */ $$Lambda$DiPlayerLayer$FygVLPoMFSq1nKMLSm_p7QYxTdA() {
    }

    public final Object get(DiConstructor diConstructor) {
        return DiPlayerLayer.m(diConstructor);
    }
}
