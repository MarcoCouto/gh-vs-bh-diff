package com.smaato.sdk.video.vast.player.system;

import android.content.Context;
import android.media.MediaPlayer;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.utils.EventValidator;
import com.smaato.sdk.video.vast.player.MediaPlayerAction;
import com.smaato.sdk.video.vast.player.MediaPlayerState;
import com.smaato.sdk.video.vast.player.MediaPlayerTransition;
import com.smaato.sdk.video.vast.player.VideoPlayer;
import com.smaato.sdk.video.vast.player.VideoPlayerCreator;

public class SystemMediaPlayerCreator implements VideoPlayerCreator {
    @NonNull
    private final Context a;
    @NonNull
    private final SystemMediaPlayerStateMachineFactory b;
    @NonNull
    private final EventValidator<MediaPlayerAction, MediaPlayerState> c;
    @NonNull
    private final EventValidator<MediaPlayerTransition, MediaPlayerState> d;

    public SystemMediaPlayerCreator(@NonNull Context context, @NonNull SystemMediaPlayerStateMachineFactory systemMediaPlayerStateMachineFactory, @NonNull EventValidator<MediaPlayerAction, MediaPlayerState> eventValidator, @NonNull EventValidator<MediaPlayerTransition, MediaPlayerState> eventValidator2) {
        this.a = (Context) Objects.requireNonNull(context);
        this.b = (SystemMediaPlayerStateMachineFactory) Objects.requireNonNull(systemMediaPlayerStateMachineFactory);
        this.c = (EventValidator) Objects.requireNonNull(eventValidator);
        this.d = (EventValidator) Objects.requireNonNull(eventValidator2);
    }

    @NonNull
    public VideoPlayer createVideoPlayer(@NonNull Logger logger) {
        Objects.requireNonNull(logger);
        SystemMediaPlayer systemMediaPlayer = new SystemMediaPlayer(this.a, new MediaPlayer(), this.b.create(), this.c, this.d, logger);
        return systemMediaPlayer;
    }
}
