package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Creative {
    public static final String AD_ID = "adId";
    public static final String API_FRAMEWORK = "apiFramework";
    public static final String COMPANION_ADS = "CompanionAds";
    public static final String CREATIVE_EXTENSIONS = "CreativeExtensions";
    public static final String ID = "id";
    public static final String LINEAR = "Linear";
    public static final String NAME = "Creative";
    public static final String SEQUENCE = "sequence";
    public static final String UNIVERSAL_AD_ID = "UniversalAdId";
    @Nullable
    public final String adId;
    @Nullable
    public final String apiFramework;
    @Nullable
    public final CompanionAds companionAds;
    @Nullable
    public final String id;
    @Nullable
    public final Linear linear;
    @Nullable
    public final Integer sequence;
    @NonNull
    public final UniversalAdId universalAdId;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;
        @Nullable
        private Integer c;
        @Nullable
        private String d;
        @Nullable
        private UniversalAdId e;
        @Nullable
        private Linear f;
        @Nullable
        private CompanionAds g;

        @NonNull
        public Builder setId(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setAdId(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setSequence(@Nullable Integer num) {
            this.c = num;
            return this;
        }

        @NonNull
        public Builder setApiFramework(@Nullable String str) {
            this.d = str;
            return this;
        }

        @NonNull
        public Builder setLinear(@Nullable Linear linear) {
            this.f = linear;
            return this;
        }

        @NonNull
        public Builder setCompanionAds(@Nullable CompanionAds companionAds) {
            this.g = companionAds;
            return this;
        }

        @NonNull
        public Builder setUniversalAdId(@Nullable UniversalAdId universalAdId) {
            this.e = universalAdId;
            return this;
        }

        @NonNull
        public Creative build() {
            if (this.e == null) {
                this.e = UniversalAdId.DEFAULT;
            }
            Creative creative = new Creative(this.e, this.a, this.b, this.c, this.d, this.f, this.g);
            return creative;
        }
    }

    Creative(@NonNull UniversalAdId universalAdId2, @Nullable String str, @Nullable String str2, @Nullable Integer num, @Nullable String str3, @Nullable Linear linear2, @Nullable CompanionAds companionAds2) {
        this.id = str;
        this.adId = str2;
        this.sequence = num;
        this.apiFramework = str3;
        this.universalAdId = universalAdId2;
        this.linear = linear2;
        this.companionAds = companionAds2;
    }

    public boolean hasCompanions() {
        return this.companionAds != null && !this.companionAds.companions.isEmpty();
    }
}
