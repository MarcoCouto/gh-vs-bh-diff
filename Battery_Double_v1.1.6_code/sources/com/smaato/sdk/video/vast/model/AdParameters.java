package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;

public class AdParameters {
    public static final String NAME = "AdParameters";
    public static final String XML_ENCODED = "xmlEncoded";
    @NonNull
    public final String parameters;
    @Nullable
    public final Boolean xmlEncoded;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private Boolean b;

        @NonNull
        public Builder setParameters(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setXmlEncoded(@Nullable Boolean bool) {
            this.b = bool;
            return this;
        }

        @NonNull
        public AdParameters build() throws VastElementMissingException {
            VastModels.requireNonNull(this.a, "Cannot build AdParameters: parameters are missing");
            return new AdParameters(this.a, this.b);
        }
    }

    AdParameters(@NonNull String str, @Nullable Boolean bool) {
        this.parameters = str;
        this.xmlEncoded = bool;
    }
}
