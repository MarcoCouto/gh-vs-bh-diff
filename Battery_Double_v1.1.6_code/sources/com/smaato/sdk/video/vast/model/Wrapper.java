package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class Wrapper {
    public static final String AD_SYSTEM = "AdSystem";
    public static final String AD_VERIFICATIONS = "AdVerifications";
    public static final String ALLOW_MULTIPLE_ADS = "allowMultipleAds";
    public static final String BLOCKED_AD_CATEGORIES = "BlockedAdCategories";
    public static final String CREATIVES = "Creatives";
    public static final String ERROR = "Error";
    public static final String EXTENSIONS = "Extensions";
    public static final String FALLBACK_ON_NO_AD = "fallbackOnNoAd";
    public static final String FOLLOW_ADDITIONAL_WRAPPERS = "followAdditionalWrappers";
    public static final String IMPRESSION = "Impression";
    public static final String NAME = "Wrapper";
    public static final String VAST_AD_TAG_URI = "VastAdTagURI";
    public static final String VIEWABLE_IMPRESSION = "ViewableImpression";
    @Nullable
    public final AdSystem adSystem;
    @NonNull
    public final List<Verification> adVerifications;
    @Nullable
    public final Boolean allowMultipleAds;
    @Nullable
    public final String blockedAdCategories;
    @NonNull
    public final List<Creative> creatives;
    @NonNull
    public final List<String> errors;
    @NonNull
    public final List<Extension> extensions;
    @Nullable
    public final Boolean fallbackOnNoAd;
    public final boolean followAdditionalWrappers;
    @NonNull
    public final List<VastBeacon> impressions;
    @Nullable
    public final String vastAdTagUri;
    @Nullable
    public final VastTree vastTree;
    @Nullable
    public final ViewableImpression viewableImpression;

    public static class Builder {
        @Nullable
        private AdSystem a;
        @Nullable
        private List<VastBeacon> b;
        @Nullable
        private List<String> c;
        @Nullable
        private ViewableImpression d;
        @Nullable
        private Boolean e;
        @Nullable
        private Boolean f;
        @Nullable
        private Boolean g;
        @Nullable
        private String h;
        @Nullable
        private String i;
        @Nullable
        private List<Verification> j;
        @Nullable
        private List<Creative> k;
        @Nullable
        private VastTree l;
        @Nullable
        private List<Extension> m;

        public Builder() {
        }

        public Builder(@NonNull Wrapper wrapper) {
            this.e = Boolean.valueOf(wrapper.followAdditionalWrappers);
            this.a = wrapper.adSystem;
            this.b = wrapper.impressions;
            this.c = wrapper.errors;
            this.d = wrapper.viewableImpression;
            this.f = wrapper.allowMultipleAds;
            this.g = wrapper.fallbackOnNoAd;
            this.h = wrapper.vastAdTagUri;
            this.j = wrapper.adVerifications;
            this.i = wrapper.blockedAdCategories;
            this.k = wrapper.creatives;
            this.l = wrapper.vastTree;
            this.m = wrapper.extensions;
        }

        @NonNull
        public Builder setAdSystem(@Nullable AdSystem adSystem) {
            this.a = adSystem;
            return this;
        }

        @NonNull
        public Builder setImpressions(@Nullable List<VastBeacon> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setErrors(@Nullable List<String> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public Builder setViewableImpression(@Nullable ViewableImpression viewableImpression) {
            this.d = viewableImpression;
            return this;
        }

        @NonNull
        public Builder setFollowAdditionalWrappers(@Nullable Boolean bool) {
            this.e = bool;
            return this;
        }

        @NonNull
        public Builder setAllowMultipleAds(@Nullable Boolean bool) {
            this.f = bool;
            return this;
        }

        @NonNull
        public Builder setFallbackOnNoAd(@Nullable Boolean bool) {
            this.g = bool;
            return this;
        }

        @NonNull
        public Builder setVastAdTagUri(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public Builder setAdVerifications(@Nullable List<Verification> list) {
            this.j = list;
            return this;
        }

        @NonNull
        public Builder setCreatives(@Nullable List<Creative> list) {
            this.k = list;
            return this;
        }

        @NonNull
        public Builder setBlockedAdCategories(@Nullable String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public Builder setVastTree(@Nullable VastTree vastTree) {
            this.l = vastTree;
            return this;
        }

        public Builder setExtensions(@Nullable List<Extension> list) {
            this.m = list;
            return this;
        }

        @NonNull
        public Wrapper build() {
            this.e = Boolean.valueOf(this.e == null ? true : this.e.booleanValue());
            this.b = VastModels.toImmutableList(this.b);
            this.j = VastModels.toImmutableList(this.j);
            this.k = VastModels.toImmutableList(this.k);
            this.c = VastModels.toImmutableList(this.c);
            this.m = VastModels.toImmutableList(this.m);
            Wrapper wrapper = new Wrapper(this.e.booleanValue(), this.b, this.j, this.k, this.c, this.a, this.d, this.f, this.g, this.h, this.i, this.l, this.m);
            return wrapper;
        }
    }

    Wrapper(boolean z, @NonNull List<VastBeacon> list, @NonNull List<Verification> list2, @NonNull List<Creative> list3, @NonNull List<String> list4, @Nullable AdSystem adSystem2, @Nullable ViewableImpression viewableImpression2, @Nullable Boolean bool, @Nullable Boolean bool2, @Nullable String str, @Nullable String str2, @Nullable VastTree vastTree2, @NonNull List<Extension> list5) {
        this.followAdditionalWrappers = z;
        this.adSystem = adSystem2;
        this.impressions = list;
        this.errors = list4;
        this.viewableImpression = viewableImpression2;
        this.allowMultipleAds = bool;
        this.fallbackOnNoAd = bool2;
        this.vastAdTagUri = str;
        this.adVerifications = list2;
        this.creatives = list3;
        this.blockedAdCategories = str2;
        this.vastTree = vastTree2;
        this.extensions = list5;
    }

    @NonNull
    public Builder newBuilder() {
        return new Builder(this);
    }
}
