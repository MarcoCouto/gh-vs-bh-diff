package com.smaato.sdk.video.vast.model;

import android.webkit.URLUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;

public class MediaFile implements Sized {
    public static final String API_FRAMEWORK = "apiFramework";
    public static final String BITRATE = "bitrate";
    public static final String CODEC = "codec";
    public static final String DELIVERY = "delivery";
    public static final String FILE_SIZE = "fileSize";
    public static final String HEIGHT = "height";
    public static final String ID = "id";
    public static final String MAINTAIN_ASPECT_RATIO = "maintainAspectRatio";
    public static final String MAX_BITRATE = "maxBitrate";
    public static final String MEDIA_TYPE = "mediaType";
    public static final String MIN_BITRATE = "minBitrate";
    public static final String NAME = "MediaFile";
    public static final String SCALABLE = "scalable";
    public static final String TYPE = "type";
    public static final String URL = "url";
    public static final String WIDTH = "width";
    @Nullable
    public final String apiFramework;
    @Nullable
    public final Integer bitrate;
    @Nullable
    public final String codec;
    @Nullable
    public final Delivery delivery;
    @Nullable
    public final Integer fileSize;
    @Nullable
    public final Float height;
    @Nullable
    public final String id;
    @Nullable
    public final Boolean maintainAspectRatio;
    @Nullable
    public final Integer maxBitrate;
    @Nullable
    public final String mediaType;
    @Nullable
    public final Integer minBitrate;
    @Nullable
    public final Boolean scalable;
    @Nullable
    public final String type;
    @NonNull
    public final String url;
    @Nullable
    public final Float width;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;
        @Nullable
        private String c;
        @Nullable
        private Float d;
        @Nullable
        private Float e;
        @Nullable
        private String f;
        @Nullable
        private Integer g;
        @Nullable
        private Integer h;
        @Nullable
        private Integer i;
        @Nullable
        private Boolean j;
        @Nullable
        private Boolean k;
        @Nullable
        private String l;
        @Nullable
        private Integer m;
        @Nullable
        private String n;
        @Nullable
        private Delivery o;

        public Builder() {
        }

        public Builder(@NonNull MediaFile mediaFile) {
            this.a = mediaFile.url;
            this.b = mediaFile.id;
            this.c = mediaFile.type;
            this.d = mediaFile.width;
            this.e = mediaFile.height;
            this.f = mediaFile.codec;
            this.g = mediaFile.bitrate;
            this.h = mediaFile.minBitrate;
            this.i = mediaFile.maxBitrate;
            this.j = mediaFile.scalable;
            this.k = mediaFile.maintainAspectRatio;
            this.l = mediaFile.apiFramework;
            this.m = mediaFile.fileSize;
            this.n = mediaFile.mediaType;
            this.o = mediaFile.delivery;
        }

        @NonNull
        public Builder setUrl(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setId(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setType(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Builder setWidth(@Nullable Float f2) {
            this.d = f2;
            return this;
        }

        @NonNull
        public Builder setHeight(@Nullable Float f2) {
            this.e = f2;
            return this;
        }

        @NonNull
        public Builder setCodec(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public Builder setBitrate(@Nullable Integer num) {
            this.g = num;
            return this;
        }

        @NonNull
        public Builder setMinBitrate(@Nullable Integer num) {
            this.h = num;
            return this;
        }

        @NonNull
        public Builder setMaxBitrate(@Nullable Integer num) {
            this.i = num;
            return this;
        }

        @NonNull
        public Builder setScalable(@Nullable Boolean bool) {
            this.j = bool;
            return this;
        }

        @NonNull
        public Builder setMaintainAspectRatio(@Nullable Boolean bool) {
            this.k = bool;
            return this;
        }

        @NonNull
        public Builder setApiFramework(@Nullable String str) {
            this.l = str;
            return this;
        }

        @NonNull
        public Builder setFileSize(@Nullable Integer num) {
            this.m = num;
            return this;
        }

        @NonNull
        public Builder setMediaType(@Nullable String str) {
            this.n = str;
            return this;
        }

        @NonNull
        public Builder setDelivery(@Nullable Delivery delivery) {
            this.o = delivery;
            return this;
        }

        @NonNull
        public MediaFile build() throws VastElementMissingException {
            if (URLUtil.isValidUrl(this.a)) {
                String str = this.a;
                String str2 = this.c;
                Float f2 = this.d;
                Float f3 = this.e;
                String str3 = this.b;
                String str4 = this.f;
                Integer num = this.g;
                Integer num2 = this.h;
                Integer num3 = this.i;
                Boolean bool = this.j;
                Boolean bool2 = this.k;
                String str5 = this.l;
                Integer num4 = this.m;
                Integer num5 = num4;
                Integer num6 = num5;
                MediaFile mediaFile = new MediaFile(str, str2, f2, f3, str3, str4, num, num2, num3, bool, bool2, str5, num6, this.n, this.o);
                return mediaFile;
            }
            throw new VastElementMissingException("Cannot build MediaFile: uri is missing");
        }
    }

    MediaFile(@NonNull String str, @Nullable String str2, @Nullable Float f, @Nullable Float f2, @Nullable String str3, @Nullable String str4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool, @Nullable Boolean bool2, @Nullable String str5, @Nullable Integer num4, @Nullable String str6, @Nullable Delivery delivery2) {
        this.type = str2;
        this.width = f;
        this.height = f2;
        this.id = str3;
        this.codec = str4;
        this.url = str;
        this.bitrate = num;
        this.minBitrate = num2;
        this.maxBitrate = num3;
        this.scalable = bool;
        this.maintainAspectRatio = bool2;
        this.apiFramework = str5;
        this.fileSize = num4;
        this.mediaType = str6;
        this.delivery = delivery2;
    }

    @Nullable
    public Float getHeight() {
        return this.height;
    }

    @Nullable
    public Float getWidth() {
        return this.width;
    }

    @NonNull
    public Builder newBuilder() {
        return new Builder(this);
    }

    public boolean isVpaid() {
        return "vpaid".equalsIgnoreCase(this.apiFramework);
    }
}
