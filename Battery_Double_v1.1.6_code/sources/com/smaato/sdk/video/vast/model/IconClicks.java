package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class IconClicks {
    public static final String ICON_CLICK_THROUGH = "IconClickThrough";
    public static final String ICON_CLICK_TRACKING = "IconClickTracking";
    public static final String NAME = "IconClicks";
    @Nullable
    public final String iconClickThrough;
    @NonNull
    public final List<VastBeacon> iconClickTrackings;

    public static class Builder {
        @Nullable
        private List<VastBeacon> a;
        @Nullable
        private String b;

        @NonNull
        public Builder setIconClickTrackings(@Nullable List<VastBeacon> list) {
            this.a = list;
            return this;
        }

        @NonNull
        public Builder setIconClickThrough(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public IconClicks build() {
            return new IconClicks(VastModels.toImmutableList(this.a), this.b);
        }
    }

    IconClicks(@NonNull List<VastBeacon> list, @Nullable String str) {
        this.iconClickTrackings = list;
        this.iconClickThrough = str;
    }
}
