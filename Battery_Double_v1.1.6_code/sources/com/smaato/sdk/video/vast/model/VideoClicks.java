package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class VideoClicks {
    public static final String CLICK_THROUGH = "ClickThrough";
    public static final String CLICK_TRACKING = "ClickTracking";
    public static final String CUSTOM_CLICK = "CustomClick";
    public static final String NAME = "VideoClicks";
    @Nullable
    public final VastBeacon clickThrough;
    @NonNull
    public final List<VastBeacon> clickTrackings;
    @NonNull
    public final List<VastBeacon> customClicks;

    public static class Builder {
        @Nullable
        private VastBeacon a;
        @Nullable
        private List<VastBeacon> b;
        @Nullable
        private List<VastBeacon> c;

        @NonNull
        public Builder setClickThrough(@Nullable VastBeacon vastBeacon) {
            this.a = vastBeacon;
            return this;
        }

        @NonNull
        public Builder setClickTrackings(@Nullable List<VastBeacon> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setCustomClicks(@Nullable List<VastBeacon> list) {
            this.c = list;
            return this;
        }

        public VideoClicks build() {
            this.b = VastModels.toImmutableList(this.b);
            this.c = VastModels.toImmutableList(this.c);
            return new VideoClicks(this.b, this.c, this.a);
        }
    }

    VideoClicks(@NonNull List<VastBeacon> list, @NonNull List<VastBeacon> list2, @Nullable VastBeacon vastBeacon) {
        this.clickThrough = vastBeacon;
        this.clickTrackings = list;
        this.customClicks = list2;
    }
}
