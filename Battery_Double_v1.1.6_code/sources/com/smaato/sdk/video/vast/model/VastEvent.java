package com.smaato.sdk.video.vast.model;

import android.support.v4.app.NotificationCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.smaato.sdk.core.util.Objects;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public enum VastEvent {
    START("start", true),
    FIRST_QUARTILE("firstQuartile", true),
    MID_POINT("midpoint", true),
    THIRD_QUARTILE("thirdQuartile", true),
    COMPLETE("complete", true),
    OTHER_AD_INTERACTION("otherAdInteraction", false),
    PROGRESS(NotificationCompat.CATEGORY_PROGRESS, false),
    CREATIVE_VIEW("creativeView", true),
    PAUSE("pause", false),
    RESUME("resume", false),
    REWIND("rewind", false),
    SKIP("skip", false),
    MUTE(Events.CREATIVE_MUTE, false),
    UNMUTE(Events.CREATIVE_UNMUTE, false),
    PLAYER_EXPAND("playerExpand", false),
    PLAYER_COLLAPSE("playerCollapse", false),
    LOADED(ParametersKeys.LOADED, true),
    CLOSE_LINEAR("closeLinear", true);
    
    public static final Set<VastEvent> EVENTS_WITH_OFFSET = null;
    @NonNull
    public final String key;
    public final boolean oneTime;

    static {
        EVENTS_WITH_OFFSET = Collections.unmodifiableSet(new HashSet<VastEvent>() {
            {
                add(VastEvent.PROGRESS);
                add(VastEvent.START);
                add(VastEvent.FIRST_QUARTILE);
                add(VastEvent.MID_POINT);
                add(VastEvent.THIRD_QUARTILE);
            }
        });
    }

    private VastEvent(String str, boolean z) {
        this.key = (String) Objects.requireNonNull(str);
        this.oneTime = z;
    }

    @Nullable
    public static VastEvent parse(@Nullable String str) {
        VastEvent[] values;
        for (VastEvent vastEvent : values()) {
            if (vastEvent.key.equalsIgnoreCase(str)) {
                return vastEvent;
            }
        }
        return null;
    }
}
