package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class Linear {
    public static final String AD_PARAMETERS = "AdParameters";
    public static final String DURATION = "Duration";
    public static final String ICONS = "Icons";
    public static final String MEDIA_FILES = "MediaFiles";
    public static final String NAME = "Linear";
    public static final String SKIPOFFSET = "skipoffset";
    public static final String TRACKING_EVENTS = "TrackingEvents";
    public static final String VIDEO_CLICKS = "VideoClicks";
    @Nullable
    public final AdParameters adParameters;
    @Nullable
    public final String duration;
    @NonNull
    public final List<Icon> icons;
    @NonNull
    public final List<MediaFile> mediaFiles;
    @Nullable
    public final String skipOffset;
    @NonNull
    public final List<Tracking> trackingEvents;
    @Nullable
    public final VideoClicks videoClicks;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;
        @Nullable
        private AdParameters c;
        @Nullable
        private List<MediaFile> d;
        @Nullable
        private VideoClicks e;
        @Nullable
        private List<Tracking> f;
        @Nullable
        private List<Icon> g;

        @NonNull
        public Builder setSkipOffset(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setDuration(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setAdParameters(@Nullable AdParameters adParameters) {
            this.c = adParameters;
            return this;
        }

        @NonNull
        public Builder setMediaFiles(@Nullable List<MediaFile> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public Builder setVideoClicks(@Nullable VideoClicks videoClicks) {
            this.e = videoClicks;
            return this;
        }

        @NonNull
        public Builder setTrackingEvents(@Nullable List<Tracking> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public Builder setIcons(@Nullable List<Icon> list) {
            this.g = list;
            return this;
        }

        @NonNull
        public Linear build() {
            Linear linear = new Linear(VastModels.toImmutableList(this.d), VastModels.toImmutableList(this.f), VastModels.toImmutableList(this.g), this.c, this.b, this.a, this.e);
            return linear;
        }
    }

    Linear(@NonNull List<MediaFile> list, @NonNull List<Tracking> list2, @NonNull List<Icon> list3, @Nullable AdParameters adParameters2, @Nullable String str, @Nullable String str2, @Nullable VideoClicks videoClicks2) {
        this.adParameters = adParameters2;
        this.duration = str;
        this.skipOffset = str2;
        this.mediaFiles = list;
        this.videoClicks = videoClicks2;
        this.trackingEvents = list2;
        this.icons = list3;
    }
}
