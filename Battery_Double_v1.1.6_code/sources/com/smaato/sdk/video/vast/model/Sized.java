package com.smaato.sdk.video.vast.model;

import androidx.annotation.Nullable;

public interface Sized {
    @Nullable
    Float getHeight();

    @Nullable
    Float getWidth();
}
