package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class CompanionAds {
    public static final String COMPANION = "Companion";
    public static final String NAME = "CompanionAds";
    public static final String REQUIRED = "required";
    @NonNull
    public final List<Companion> companions;
    @Nullable
    public final Required required;

    public static class Builder {
        @Nullable
        private List<Companion> a;
        @Nullable
        private Required b;

        @NonNull
        public Builder setCompanions(@Nullable List<Companion> list) {
            this.a = list;
            return this;
        }

        @NonNull
        public Builder setRequired(@Nullable Required required) {
            this.b = required;
            return this;
        }

        @NonNull
        public CompanionAds build() throws VastElementMissingException {
            VastModels.requireNonEmpty(this.a, "Cannot build CompanionAds: companions are missing");
            return new CompanionAds(VastModels.toImmutableList(this.a), this.b);
        }
    }

    public enum Required {
        ALL,
        ANY,
        NONE;

        @Nullable
        public static Required parse(@Nullable String str) {
            Required[] values;
            for (Required required : values()) {
                if (required.name().equalsIgnoreCase(str)) {
                    return required;
                }
            }
            return null;
        }
    }

    CompanionAds(@NonNull List<Companion> list, @Nullable Required required2) {
        this.companions = list;
        this.required = required2;
    }
}
