package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;

public class VastBeacon {
    public static final String ID = "id";
    @Nullable
    public final String id;
    @NonNull
    public final String uri;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;

        @NonNull
        public Builder setId(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setUri(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public VastBeacon build() throws VastElementMissingException {
            VastModels.requireNonNull(this.a, "Cannot build VastBeacon: uri is missing");
            return new VastBeacon(this.a, this.b);
        }
    }

    VastBeacon(@NonNull String str, @Nullable String str2) {
        this.uri = str;
        this.id = str2;
    }
}
