package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public final class Verification {
    public static final String JAVASCRIPT_RESOURCE = "JavaScriptResource";
    public static final String NAME = "Verification";
    public static final String VENDOR = "vendor";
    public static final String VERIFICATION_PARAMETERS = "VerificationParameters";
    public static final String VIEWABLE_IMPRESSION = "ViewableImpression";
    @NonNull
    public final List<JavaScriptResource> javaScriptResources;
    @NonNull
    public final String vendor;
    @Nullable
    public final String verificationParameters;
    @Nullable
    public final ViewableImpression viewableImpression;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private List<JavaScriptResource> b;
        @Nullable
        private String c;
        @Nullable
        private ViewableImpression d;

        @NonNull
        public Builder setVendor(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setJavaScriptResources(@Nullable List<JavaScriptResource> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setVerificationParameters(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Builder setViewableImpression(@Nullable ViewableImpression viewableImpression) {
            this.d = viewableImpression;
            return this;
        }

        @NonNull
        public Verification build() throws VastElementMissingException {
            this.b = VastModels.toImmutableList(this.b);
            VastModels.requireNonNull(this.a, "Cannot build Verification: vendor is missing");
            Verification verification = new Verification(this.b, this.a, this.c, this.d, 0);
            return verification;
        }
    }

    /* synthetic */ Verification(List list, String str, String str2, ViewableImpression viewableImpression2, byte b) {
        this(list, str, str2, viewableImpression2);
    }

    private Verification(@NonNull List<JavaScriptResource> list, @NonNull String str, @Nullable String str2, @Nullable ViewableImpression viewableImpression2) {
        this.vendor = str;
        this.javaScriptResources = list;
        this.verificationParameters = str2;
        this.viewableImpression = viewableImpression2;
    }
}
