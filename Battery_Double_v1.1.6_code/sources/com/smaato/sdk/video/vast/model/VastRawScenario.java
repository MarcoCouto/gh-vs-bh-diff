package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public final class VastRawScenario {
    @Nullable
    public final AdSystem adSystem;
    @Nullable
    public final String adTitle;
    @NonNull
    public final List<Verification> adVerifications;
    @Nullable
    public final String advertiser;
    @NonNull
    public final List<String> blockedAdCategories;
    @NonNull
    public final List<Category> categories;
    @Nullable
    public final String description;
    @NonNull
    public final List<String> errors;
    @NonNull
    public final List<VastBeacon> impressions;
    @NonNull
    public final List<VastCompanionScenario> vastCompanionScenarios;
    @NonNull
    public final List<VastRawMediaFileScenario> vastRawMediaFileScenarios;
    @Nullable
    public final ViewableImpression viewableImpression;

    public static class Builder {
        @Nullable
        private List<VastRawMediaFileScenario> a;
        @Nullable
        private List<VastCompanionScenario> b;
        @Nullable
        private List<Verification> c;
        @Nullable
        private List<VastBeacon> d;
        @Nullable
        private List<Category> e;
        @Nullable
        private List<String> f;
        @Nullable
        private AdSystem g;
        @Nullable
        private String h;
        @Nullable
        private String i;
        @Nullable
        private String j;
        @Nullable
        private ViewableImpression k;
        @Nullable
        private List<String> l;

        public Builder() {
        }

        public Builder(@NonNull VastRawScenario vastRawScenario) {
            this.d = vastRawScenario.impressions;
            this.c = vastRawScenario.adVerifications;
            this.e = vastRawScenario.categories;
            this.f = vastRawScenario.errors;
            this.g = vastRawScenario.adSystem;
            this.h = vastRawScenario.adTitle;
            this.i = vastRawScenario.description;
            this.j = vastRawScenario.advertiser;
            this.k = vastRawScenario.viewableImpression;
            this.a = vastRawScenario.vastRawMediaFileScenarios;
            this.b = vastRawScenario.vastCompanionScenarios;
            this.l = vastRawScenario.blockedAdCategories;
        }

        @NonNull
        public Builder setAdSystem(@Nullable AdSystem adSystem) {
            this.g = adSystem;
            return this;
        }

        @NonNull
        public Builder setAdTitle(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public Builder setDescription(@Nullable String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public Builder setAdvertiser(@Nullable String str) {
            this.j = str;
            return this;
        }

        @NonNull
        public Builder setViewableImpression(@Nullable ViewableImpression viewableImpression) {
            this.k = viewableImpression;
            return this;
        }

        @NonNull
        public Builder setAdVerifications(@NonNull List<Verification> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public Builder setVastMediaFileScenarios(@Nullable List<VastRawMediaFileScenario> list) {
            this.a = list;
            return this;
        }

        @NonNull
        public Builder setVastCompanionScenarios(@Nullable List<VastCompanionScenario> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setImpressions(@Nullable List<VastBeacon> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public Builder setCategories(@Nullable List<Category> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public Builder setErrors(@Nullable List<String> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public Builder setBlockedAdCategories(@Nullable List<String> list) {
            this.l = list;
            return this;
        }

        @NonNull
        public VastRawScenario build() {
            VastRawScenario vastRawScenario = new VastRawScenario(VastModels.toImmutableList(this.d), VastModels.toImmutableList(this.c), VastModels.toImmutableList(this.e), VastModels.toImmutableList(this.f), VastModels.toImmutableList(this.a), VastModels.toImmutableList(this.b), VastModels.toImmutableList(this.l), this.g, this.h, this.i, this.j, this.k, 0);
            return vastRawScenario;
        }
    }

    /* synthetic */ VastRawScenario(List list, List list2, List list3, List list4, List list5, List list6, List list7, AdSystem adSystem2, String str, String str2, String str3, ViewableImpression viewableImpression2, byte b) {
        this(list, list2, list3, list4, list5, list6, list7, adSystem2, str, str2, str3, viewableImpression2);
    }

    private VastRawScenario(@NonNull List<VastBeacon> list, @NonNull List<Verification> list2, @NonNull List<Category> list3, @NonNull List<String> list4, @NonNull List<VastRawMediaFileScenario> list5, @NonNull List<VastCompanionScenario> list6, @NonNull List<String> list7, @Nullable AdSystem adSystem2, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable ViewableImpression viewableImpression2) {
        this.impressions = (List) Objects.requireNonNull(list);
        this.adVerifications = (List) Objects.requireNonNull(list2);
        this.categories = (List) Objects.requireNonNull(list3);
        this.errors = (List) Objects.requireNonNull(list4);
        this.vastRawMediaFileScenarios = (List) Objects.requireNonNull(list5);
        this.vastCompanionScenarios = (List) Objects.requireNonNull(list6);
        this.blockedAdCategories = (List) Objects.requireNonNull(list7);
        this.adSystem = adSystem2;
        this.adTitle = str;
        this.description = str2;
        this.advertiser = str3;
        this.viewableImpression = viewableImpression2;
    }

    @NonNull
    public final Builder newBuilder() {
        return new Builder(this);
    }
}
