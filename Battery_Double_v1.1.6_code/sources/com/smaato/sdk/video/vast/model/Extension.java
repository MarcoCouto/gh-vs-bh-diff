package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class Extension {
    public static final String NAME = "Extension";
    public static final String TYPE = "type";
    @NonNull
    public final List<Verification> adVerifications;
    @Nullable
    public final String type;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private List<Verification> b;

        @NonNull
        public Builder setType(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setAdVerifications(@Nullable List<Verification> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Extension build() {
            this.b = VastModels.toImmutableList(this.b);
            return new Extension(this.a, this.b);
        }
    }

    public Extension(@Nullable String str, @NonNull List<Verification> list) {
        this.type = str;
        this.adVerifications = list;
    }
}
