package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public final class VastScenario {
    @Nullable
    public final String adServingId;
    @Nullable
    public final AdSystem adSystem;
    @Nullable
    public final String adTitle;
    @NonNull
    public final List<Verification> adVerifications;
    @Nullable
    public final Advertiser advertiser;
    @NonNull
    public final List<String> blockedAdCategories;
    @NonNull
    public final List<Category> categories;
    @Nullable
    public final String description;
    @NonNull
    public final List<String> errors;
    @NonNull
    public final List<VastBeacon> impressions;
    @Nullable
    public final VastCompanionScenario vastCompanionScenario;
    @NonNull
    public final VastMediaFileScenario vastMediaFileScenario;
    @Nullable
    public final ViewableImpression viewableImpression;

    public static class Builder {
        @Nullable
        private VastMediaFileScenario a;
        @Nullable
        private VastCompanionScenario b;
        @Nullable
        private List<Verification> c;
        @Nullable
        private List<VastBeacon> d;
        @Nullable
        private List<Category> e;
        @Nullable
        private List<String> f;
        @Nullable
        private AdSystem g;
        @Nullable
        private String h;
        @Nullable
        private String i;
        @Nullable
        private Advertiser j;
        @Nullable
        private ViewableImpression k;
        @Nullable
        private List<String> l;
        @Nullable
        private String m;

        public Builder() {
        }

        public Builder(@NonNull VastScenario vastScenario) {
            this.d = vastScenario.impressions;
            this.c = vastScenario.adVerifications;
            this.e = vastScenario.categories;
            this.f = vastScenario.errors;
            this.g = vastScenario.adSystem;
            this.h = vastScenario.adTitle;
            this.i = vastScenario.description;
            this.j = vastScenario.advertiser;
            this.k = vastScenario.viewableImpression;
            this.a = vastScenario.vastMediaFileScenario;
            this.b = vastScenario.vastCompanionScenario;
            this.l = vastScenario.blockedAdCategories;
            this.m = vastScenario.adServingId;
        }

        @NonNull
        public Builder setAdSystem(@Nullable AdSystem adSystem) {
            this.g = adSystem;
            return this;
        }

        @NonNull
        public Builder setAdTitle(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public Builder setDescription(@Nullable String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public Builder setAdvertiser(@Nullable Advertiser advertiser) {
            this.j = advertiser;
            return this;
        }

        @NonNull
        public Builder setViewableImpression(@Nullable ViewableImpression viewableImpression) {
            this.k = viewableImpression;
            return this;
        }

        @NonNull
        public Builder setAdVerifications(@NonNull List<Verification> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public Builder setVastMediaFileScenario(@Nullable VastMediaFileScenario vastMediaFileScenario) {
            this.a = vastMediaFileScenario;
            return this;
        }

        @NonNull
        public Builder setVastCompanionScenario(@Nullable VastCompanionScenario vastCompanionScenario) {
            this.b = vastCompanionScenario;
            return this;
        }

        @NonNull
        public Builder setImpressions(@Nullable List<VastBeacon> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public Builder setCategories(@Nullable List<Category> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public Builder setErrors(@Nullable List<String> list) {
            this.f = list;
            return this;
        }

        @NonNull
        public Builder setBlockedAdCategories(@Nullable List<String> list) {
            this.l = list;
            return this;
        }

        @NonNull
        public Builder setAdServingId(@Nullable String str) {
            this.m = str;
            return this;
        }

        @NonNull
        public VastScenario build() {
            VastScenario vastScenario = new VastScenario(VastModels.toImmutableList(this.d), VastModels.toImmutableList(this.c), VastModels.toImmutableList(this.e), VastModels.toImmutableList(this.f), VastModels.toImmutableList(this.l), (VastMediaFileScenario) Objects.requireNonNull(this.a), this.b, this.g, this.h, this.i, this.j, this.k, this.m, 0);
            return vastScenario;
        }
    }

    /* synthetic */ VastScenario(List list, List list2, List list3, List list4, List list5, VastMediaFileScenario vastMediaFileScenario2, VastCompanionScenario vastCompanionScenario2, AdSystem adSystem2, String str, String str2, Advertiser advertiser2, ViewableImpression viewableImpression2, String str3, byte b) {
        this(list, list2, list3, list4, list5, vastMediaFileScenario2, vastCompanionScenario2, adSystem2, str, str2, advertiser2, viewableImpression2, str3);
    }

    private VastScenario(@NonNull List<VastBeacon> list, @NonNull List<Verification> list2, @NonNull List<Category> list3, @NonNull List<String> list4, @NonNull List<String> list5, @NonNull VastMediaFileScenario vastMediaFileScenario2, @Nullable VastCompanionScenario vastCompanionScenario2, @Nullable AdSystem adSystem2, @Nullable String str, @Nullable String str2, @Nullable Advertiser advertiser2, @Nullable ViewableImpression viewableImpression2, @Nullable String str3) {
        this.impressions = (List) Objects.requireNonNull(list);
        this.adVerifications = (List) Objects.requireNonNull(list2);
        this.categories = (List) Objects.requireNonNull(list3);
        this.errors = (List) Objects.requireNonNull(list4);
        this.vastMediaFileScenario = (VastMediaFileScenario) Objects.requireNonNull(vastMediaFileScenario2);
        this.blockedAdCategories = (List) Objects.requireNonNull(list5);
        this.vastCompanionScenario = vastCompanionScenario2;
        this.adSystem = adSystem2;
        this.adTitle = str;
        this.description = str2;
        this.advertiser = advertiser2;
        this.viewableImpression = viewableImpression2;
        this.adServingId = str3;
    }

    @NonNull
    public final Builder newBuilder() {
        return new Builder(this);
    }
}
