package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Advertiser {
    public static final String ID = "id";
    public static final String NAME = "Advertiser";
    @Nullable
    public final String id;
    @Nullable
    public final String name;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;

        @NonNull
        public Builder setId(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setName(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Advertiser build() {
            return new Advertiser(this.a, this.b);
        }
    }

    Advertiser(@Nullable String str, @Nullable String str2) {
        this.id = str;
        this.name = str2;
    }
}
