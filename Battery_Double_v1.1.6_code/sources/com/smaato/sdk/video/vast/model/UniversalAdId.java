package com.smaato.sdk.video.vast.model;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class UniversalAdId {
    public static final UniversalAdId DEFAULT = new UniversalAdId("unknown", "unknown", "unknown");
    public static final String ID_REGISTRY = "idRegistry";
    public static final String ID_VALUE = "idValue";
    public static final String NAME = "UniversalAdId";
    @NonNull
    public final String idRegistry;
    @NonNull
    public final String idValue;
    @NonNull
    public final String value;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;
        @Nullable
        private String c;

        @NonNull
        public Builder setIdRegistry(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setIdValue(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setValue(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public UniversalAdId build() {
            if (TextUtils.isEmpty(this.c) && TextUtils.isEmpty(this.b) && TextUtils.isEmpty(this.a)) {
                return UniversalAdId.DEFAULT;
            }
            if (TextUtils.isEmpty(this.a)) {
                this.a = "unknown";
            }
            if (TextUtils.isEmpty(this.b)) {
                this.b = "unknown";
            }
            if (TextUtils.isEmpty(this.c)) {
                this.c = "unknown";
            }
            return new UniversalAdId(this.a, this.b, this.c);
        }
    }

    public UniversalAdId(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        this.idRegistry = str;
        this.idValue = str2;
        this.value = str3;
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        UniversalAdId universalAdId = (UniversalAdId) obj;
        if (this.idRegistry.equals(universalAdId.idRegistry) && this.idValue.equals(universalAdId.idValue)) {
            return this.value.equals(universalAdId.value);
        }
        return false;
    }

    public int hashCode() {
        return (((this.idRegistry.hashCode() * 31) + this.idValue.hashCode()) * 31) + this.value.hashCode();
    }
}
