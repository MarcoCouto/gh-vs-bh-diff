package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;

public class JavaScriptResource {
    public static final String API_FRAMEWORK = "apiFramework";
    public static final String BROWSER_OPTIONAL = "browserOptional";
    public static final String NAME = "JavaScriptResource";
    public static final String URI = "uri";
    @NonNull
    public final String apiFramework;
    public final boolean browserOptional;
    @NonNull
    public final String uri;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;
        private boolean c;

        @NonNull
        public Builder setUri(@NonNull String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setApiFramework(@NonNull String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setBrowserOptional(@NonNull String str) {
            this.c = Boolean.valueOf(str).booleanValue();
            return this;
        }

        @NonNull
        public JavaScriptResource build() throws VastElementMissingException {
            VastModels.requireNonNull(this.a, "Cannot build JavaScriptResource: uri is missing");
            VastModels.requireNonNull(this.b, "Cannot build JavaScriptResource: apiFramework is missing");
            return new JavaScriptResource(this.a, this.b, this.c);
        }
    }

    public JavaScriptResource(@NonNull String str, @NonNull String str2, boolean z) {
        this.uri = str;
        this.apiFramework = str2;
        this.browserOptional = z;
    }
}
