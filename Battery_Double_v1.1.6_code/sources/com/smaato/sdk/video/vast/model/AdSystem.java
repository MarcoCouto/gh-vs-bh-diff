package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AdSystem {
    public static final String NAME = "AdSystem";
    public static final String VERSION = "version";
    @Nullable
    public final String adServerName;
    @Nullable
    public final String version;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;

        @NonNull
        public Builder setServerName(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setVersion(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public AdSystem build() {
            return new AdSystem(this.a, this.b);
        }
    }

    AdSystem(@Nullable String str, @Nullable String str2) {
        this.adServerName = str;
        this.version = str2;
    }
}
