package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class Companion implements Sized {
    public static final String AD_PARAMETERS = "AdParameters";
    public static final String AD_SLOT_ID = "adSlotID";
    public static final String ALT_TEXT = "AltText";
    public static final String API_FRAMEWORK = "apiFramework";
    public static final String ASSET_HEIGHT = "assetHeight";
    public static final String ASSET_WIDTH = "assetWidth";
    public static final String COMPANION_CLICK_THROUGH = "CompanionClickThrough";
    public static final String COMPANION_CLICK_TRACKING = "CompanionClickTracking";
    public static final String EXPANDED_HEIGHT = "expandedHeight";
    public static final String EXPANDED_WIDTH = "expandedWidth";
    public static final String HEIGHT = "height";
    public static final String HTML_RESOURCE = "HTMLResource";
    public static final String ID = "id";
    public static final String IFRAME_RESOURCE = "IFrameResource";
    public static final String NAME = "Companion";
    public static final String PX_RATIO = "pxratio";
    public static final String RENDERING_MODE = "renderingMode";
    public static final String STATIC_RESOURCE = "StaticResource";
    public static final String TRACKING_EVENTS = "TrackingEvents";
    public static final String WIDTH = "width";
    @Nullable
    public final AdParameters adParameters;
    @Nullable
    public final String adSlotID;
    @Nullable
    public final String altText;
    @Nullable
    public final String apiFramework;
    @Nullable
    public final Float assetHeight;
    @Nullable
    public final Float assetWidth;
    @Nullable
    public final String companionClickThrough;
    @NonNull
    public final List<VastBeacon> companionClickTrackings;
    @Nullable
    public final Float expandedHeight;
    @Nullable
    public final Float expandedWidth;
    @Nullable
    public final Float height;
    @NonNull
    public final List<String> htmlResources;
    @NonNull
    public final List<String> iFrameResources;
    @Nullable
    public final String id;
    @Nullable
    public final Float pxRatio;
    @Nullable
    public final String renderingMode;
    @NonNull
    public final List<StaticResource> staticResources;
    @NonNull
    public final List<Tracking> trackingEvents;
    @Nullable
    public final Float width;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private Float b;
        @Nullable
        private Float c;
        @Nullable
        private Float d;
        @Nullable
        private Float e;
        @Nullable
        private Float f;
        @Nullable
        private Float g;
        @Nullable
        private String h;
        @Nullable
        private String i;
        @Nullable
        private Float j;
        @Nullable
        private String k;
        @Nullable
        private AdParameters l;
        @Nullable
        private List<StaticResource> m;
        @Nullable
        private List<String> n;
        @Nullable
        private List<String> o;
        @Nullable
        private String p;
        @Nullable
        private List<VastBeacon> q;
        @Nullable
        private List<Tracking> r;
        @NonNull
        private String s = "end-card";

        @NonNull
        public Builder setId(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setWidth(@Nullable Float f2) {
            this.b = f2;
            return this;
        }

        @NonNull
        public Builder setHeight(@Nullable Float f2) {
            this.c = f2;
            return this;
        }

        @NonNull
        public Builder setAssetWidth(@Nullable Float f2) {
            this.d = f2;
            return this;
        }

        @NonNull
        public Builder setAssetHeight(@Nullable Float f2) {
            this.e = f2;
            return this;
        }

        @NonNull
        public Builder setExpandedWidth(@Nullable Float f2) {
            this.f = f2;
            return this;
        }

        @NonNull
        public Builder setExpandedHeight(@Nullable Float f2) {
            this.g = f2;
            return this;
        }

        @NonNull
        public Builder setApiFramework(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public Builder setAdSlotID(@Nullable String str) {
            this.i = str;
            return this;
        }

        @NonNull
        public Builder setPxRatio(@Nullable Float f2) {
            this.j = f2;
            return this;
        }

        @NonNull
        public Builder setAltText(@Nullable String str) {
            this.k = str;
            return this;
        }

        @NonNull
        public Builder setCompanionClickThrough(@Nullable String str) {
            this.p = str;
            return this;
        }

        @NonNull
        public Builder setAdParameters(@Nullable AdParameters adParameters) {
            this.l = adParameters;
            return this;
        }

        @NonNull
        public Builder setStaticResources(@Nullable List<StaticResource> list) {
            this.m = list;
            return this;
        }

        @NonNull
        public Builder setIFrameResources(@Nullable List<String> list) {
            this.n = list;
            return this;
        }

        @NonNull
        public Builder setHtmlResources(@Nullable List<String> list) {
            this.o = list;
            return this;
        }

        @NonNull
        public Builder setCompanionClickTrackings(@Nullable List<VastBeacon> list) {
            this.q = list;
            return this;
        }

        @NonNull
        public Builder setTrackingEvents(@Nullable List<Tracking> list) {
            this.r = list;
            return this;
        }

        @NonNull
        public Builder setRenderingMode(String str) {
            this.s = str;
            return this;
        }

        @NonNull
        public Companion build() {
            this.q = VastModels.toImmutableList(this.q);
            this.r = VastModels.toImmutableList(this.r);
            this.m = VastModels.toImmutableList(this.m);
            this.n = VastModels.toImmutableList(this.n);
            this.o = VastModels.toImmutableList(this.o);
            Companion companion = r2;
            Companion companion2 = new Companion(this.q, this.r, this.m, this.n, this.o, this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.p, this.l, this.s);
            return companion;
        }
    }

    Companion(@NonNull List<VastBeacon> list, @NonNull List<Tracking> list2, @NonNull List<StaticResource> list3, @NonNull List<String> list4, @NonNull List<String> list5, @Nullable String str, @Nullable Float f, @Nullable Float f2, @Nullable Float f3, @Nullable Float f4, @Nullable Float f5, @Nullable Float f6, @Nullable String str2, @Nullable String str3, @Nullable Float f7, @Nullable String str4, @Nullable String str5, @Nullable AdParameters adParameters2, @Nullable String str6) {
        this.id = str;
        this.width = f;
        this.height = f2;
        this.assetWidth = f3;
        this.assetHeight = f4;
        this.expandedWidth = f5;
        this.expandedHeight = f6;
        this.apiFramework = str2;
        this.adSlotID = str3;
        this.pxRatio = f7;
        this.altText = str4;
        this.companionClickThrough = str5;
        this.adParameters = adParameters2;
        this.staticResources = list3;
        this.iFrameResources = list4;
        this.htmlResources = list5;
        this.companionClickTrackings = list;
        this.trackingEvents = list2;
        this.renderingMode = str6;
    }

    @Nullable
    public Float getHeight() {
        return this.height;
    }

    @Nullable
    public Float getWidth() {
        return this.width;
    }
}
