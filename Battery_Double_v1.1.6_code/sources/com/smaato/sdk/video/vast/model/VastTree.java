package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class VastTree {
    public static final String AD = "Ad";
    public static final String ERROR = "Error";
    public static final String VAST = "VAST";
    public static final String VERSION = "version";
    @NonNull
    public final List<Ad> ads;
    @NonNull
    public final List<String> errors;
    @Nullable
    public final String version;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private List<String> b;
        @Nullable
        private List<Ad> c;

        public Builder() {
        }

        public Builder(@NonNull VastTree vastTree) {
            this.a = vastTree.version;
            this.b = vastTree.errors;
            this.c = vastTree.ads;
        }

        @NonNull
        public Builder setAds(@Nullable List<Ad> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public Builder setErrors(@Nullable List<String> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setVersion(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public VastTree build() {
            return new VastTree(VastModels.toImmutableList(this.c), VastModels.toImmutableList(this.b), this.a);
        }
    }

    VastTree(@NonNull List<Ad> list, @NonNull List<String> list2, @Nullable String str) {
        this.ads = (List) Objects.requireNonNull(list);
        this.errors = (List) Objects.requireNonNull(list2);
        this.version = str;
    }

    @NonNull
    public Builder newBuilder() {
        return new Builder(this);
    }
}
