package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;

public class Tracking {
    public static final String EVENT = "event";
    public static final String NAME = "Tracking";
    public static final String OFFSET = "offset";
    public static final String URL = "url";
    @Nullable
    public final String offset;
    @NonNull
    public final String url;
    @NonNull
    public final VastEvent vastEvent;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private VastEvent b;
        @Nullable
        private String c;

        @NonNull
        public Builder setUrl(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setVastEvent(@Nullable VastEvent vastEvent) {
            this.b = vastEvent;
            return this;
        }

        @NonNull
        public Builder setOffset(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Tracking build() throws VastElementMissingException {
            String str;
            VastModels.requireNonNull(this.b, "Cannot build Tracking: event is missing");
            VastModels.requireNonNull(this.a, "Cannot build Tracking: url is missing");
            if (TextUtils.isEmpty(this.c)) {
                switch (this.b) {
                    case THIRD_QUARTILE:
                        str = "75%";
                        break;
                    case MID_POINT:
                        str = "50%";
                        break;
                    case FIRST_QUARTILE:
                        str = "25%";
                        break;
                    case START:
                        str = "0%";
                        break;
                    default:
                        str = null;
                        break;
                }
            } else {
                str = this.c;
            }
            this.c = str;
            return new Tracking(this.b, this.a, this.c);
        }
    }

    Tracking(@NonNull VastEvent vastEvent2, @NonNull String str, @Nullable String str2) {
        this.vastEvent = vastEvent2;
        this.url = str;
        this.offset = str2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Tracking tracking = (Tracking) obj;
        if (!this.url.equals(tracking.url) || this.vastEvent != tracking.vastEvent) {
            return false;
        }
        if (this.offset != null) {
            return this.offset.equals(tracking.offset);
        }
        return tracking.offset == null;
    }

    public int hashCode() {
        return (((this.url.hashCode() * 31) + this.vastEvent.hashCode()) * 31) + (this.offset != null ? this.offset.hashCode() : 0);
    }
}
