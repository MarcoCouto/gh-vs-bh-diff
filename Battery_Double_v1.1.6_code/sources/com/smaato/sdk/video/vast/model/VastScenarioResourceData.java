package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;

public final class VastScenarioResourceData {
    @Nullable
    public final String htmlResources;
    @Nullable
    public final String iFrameResources;
    @Nullable
    public final StaticResource staticResources;

    public static class Builder {
        @Nullable
        private StaticResource a;
        @Nullable
        private String b;
        @Nullable
        private String c;

        @NonNull
        public Builder setStaticResource(@Nullable StaticResource staticResource) {
            this.a = staticResource;
            return this;
        }

        @NonNull
        public Builder setIFrameResources(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setHtmlResources(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public VastScenarioResourceData build() throws VastElementMissingException {
            if (this.a != null || this.b != null || this.c != null) {
                return new VastScenarioResourceData(this.a, this.b, this.c, 0);
            }
            throw new VastElementMissingException("Cannot build VastScenarioResourceData: staticResources, iFrameResources and htmlResources are missing");
        }
    }

    /* synthetic */ VastScenarioResourceData(StaticResource staticResource, String str, String str2, byte b) {
        this(staticResource, str, str2);
    }

    private VastScenarioResourceData(@Nullable StaticResource staticResource, @Nullable String str, @Nullable String str2) {
        this.staticResources = staticResource;
        this.iFrameResources = str;
        this.htmlResources = str2;
    }
}
