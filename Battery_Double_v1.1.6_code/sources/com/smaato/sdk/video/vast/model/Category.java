package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;

public class Category {
    public static final String AUTHORITY = "authority";
    public static final String NAME = "Category";
    @Nullable
    public final String authority;
    @NonNull
    public final String categoryCode;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private String b;

        @NonNull
        public Builder setAuthority(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setCategoryCode(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Category build() throws VastElementMissingException {
            VastModels.requireNonNull(this.b, "Cannot build Category: categoryCode is missing");
            return new Category(this.b, this.a);
        }
    }

    Category(@NonNull String str, @Nullable String str2) {
        this.categoryCode = str;
        this.authority = str2;
    }
}
