package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.ArrayList;
import java.util.List;

public final class VastCompanionScenario implements Sized {
    @Nullable
    public final AdParameters adParameters;
    @Nullable
    public final String adSlotID;
    @Nullable
    public final String altText;
    @Nullable
    public final String apiFramework;
    @Nullable
    public final Float assetHeight;
    @Nullable
    public final Float assetWidth;
    @Nullable
    public final String companionClickThrough;
    @NonNull
    public final List<VastBeacon> companionClickTrackings;
    @Nullable
    public final Float expandedHeight;
    @Nullable
    public final Float expandedWidth;
    @Nullable
    public final Float height;
    @Nullable
    public final String id;
    @Nullable
    public final Float pxRatio;
    @NonNull
    public final VastScenarioResourceData resourceData;
    @NonNull
    public final List<Tracking> trackingEvents;
    @NonNull
    public final VastScenarioCreativeData vastScenarioCreativeData;
    @Nullable
    public final Float width;

    public static class Builder {
        @Nullable
        private VastScenarioResourceData a;
        @Nullable
        private List<VastBeacon> b;
        @Nullable
        private String c;
        @Nullable
        private List<Tracking> d;
        @Nullable
        private String e;
        @Nullable
        private Float f;
        @Nullable
        private Float g;
        @Nullable
        private Float h;
        @Nullable
        private Float i;
        @Nullable
        private Float j;
        @Nullable
        private Float k;
        @Nullable
        private Float l;
        @Nullable
        private String m;
        @Nullable
        private String n;
        @Nullable
        private String o;
        @Nullable
        private AdParameters p;
        @Nullable
        private VastScenarioCreativeData q;

        public Builder() {
        }

        public Builder(@NonNull VastCompanionScenario vastCompanionScenario) {
            this.a = vastCompanionScenario.resourceData;
            this.b = vastCompanionScenario.companionClickTrackings;
            this.c = vastCompanionScenario.companionClickThrough;
            this.d = vastCompanionScenario.trackingEvents;
            this.e = vastCompanionScenario.id;
            this.f = vastCompanionScenario.width;
            this.g = vastCompanionScenario.height;
            this.h = vastCompanionScenario.assetWidth;
            this.i = vastCompanionScenario.assetHeight;
            this.j = vastCompanionScenario.expandedWidth;
            this.k = vastCompanionScenario.expandedHeight;
            this.l = vastCompanionScenario.pxRatio;
            this.m = vastCompanionScenario.apiFramework;
            this.n = vastCompanionScenario.adSlotID;
            this.o = vastCompanionScenario.altText;
            this.p = vastCompanionScenario.adParameters;
            this.q = vastCompanionScenario.vastScenarioCreativeData;
        }

        @NonNull
        public Builder setVastScenarioResourceData(@Nullable VastScenarioResourceData vastScenarioResourceData) {
            this.a = vastScenarioResourceData;
            return this;
        }

        @NonNull
        public Builder setWidth(@Nullable Float f2) {
            this.f = f2;
            return this;
        }

        @NonNull
        public Builder setHeight(@Nullable Float f2) {
            this.g = f2;
            return this;
        }

        @NonNull
        public Builder setTrackingEvents(@Nullable List<Tracking> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public Builder setCompanionClickTrackings(@Nullable List<VastBeacon> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setId(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public Builder setCompanionClickThrough(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Builder setAssetWidth(@Nullable Float f2) {
            this.h = f2;
            return this;
        }

        @NonNull
        public Builder setAssetHeight(@Nullable Float f2) {
            this.i = f2;
            return this;
        }

        @NonNull
        public Builder setExpandedWidth(@Nullable Float f2) {
            this.j = f2;
            return this;
        }

        @NonNull
        public Builder setExpandedHeight(@Nullable Float f2) {
            this.k = f2;
            return this;
        }

        @NonNull
        public Builder setPxRatio(@Nullable Float f2) {
            this.l = f2;
            return this;
        }

        @NonNull
        public Builder setAdSlotID(@Nullable String str) {
            this.n = str;
            return this;
        }

        @NonNull
        public Builder setAltText(@Nullable String str) {
            this.o = str;
            return this;
        }

        @NonNull
        public Builder setApiFramework(@Nullable String str) {
            this.m = str;
            return this;
        }

        @NonNull
        public Builder setAdParameters(@Nullable AdParameters adParameters) {
            this.p = adParameters;
            return this;
        }

        @NonNull
        public Builder setVastScenarioCreativeData(@Nullable VastScenarioCreativeData vastScenarioCreativeData) {
            this.q = vastScenarioCreativeData;
            return this;
        }

        @NonNull
        public VastCompanionScenario build() {
            Objects.requireNonNull(this.a, "Cannot build VastCompanionScenario: resourceData is missing");
            Objects.requireNonNull(this.q, "Cannot build VastMediaFileScenario: vastScenarioCreativeData is missing");
            VastCompanionScenario vastCompanionScenario = new VastCompanionScenario(this.a, this.q, VastModels.toImmutableList(this.b), VastModels.toImmutableList(this.d), this.c, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, 0);
            return vastCompanionScenario;
        }
    }

    /* synthetic */ VastCompanionScenario(VastScenarioResourceData vastScenarioResourceData, VastScenarioCreativeData vastScenarioCreativeData2, List list, List list2, String str, String str2, Float f, Float f2, Float f3, Float f4, Float f5, Float f6, Float f7, String str3, String str4, String str5, AdParameters adParameters2, byte b) {
        this(vastScenarioResourceData, vastScenarioCreativeData2, list, list2, str, str2, f, f2, f3, f4, f5, f6, f7, str3, str4, str5, adParameters2);
    }

    private VastCompanionScenario(@NonNull VastScenarioResourceData vastScenarioResourceData, @NonNull VastScenarioCreativeData vastScenarioCreativeData2, @NonNull List<VastBeacon> list, @NonNull List<Tracking> list2, @Nullable String str, @Nullable String str2, @Nullable Float f, @Nullable Float f2, @Nullable Float f3, @Nullable Float f4, @Nullable Float f5, @Nullable Float f6, @Nullable Float f7, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable AdParameters adParameters2) {
        this.resourceData = vastScenarioResourceData;
        this.vastScenarioCreativeData = vastScenarioCreativeData2;
        List<VastBeacon> list3 = list;
        this.companionClickTrackings = new ArrayList(list);
        this.companionClickThrough = str;
        this.trackingEvents = list2;
        this.id = str2;
        this.width = f;
        this.height = f2;
        this.assetWidth = f3;
        this.assetHeight = f4;
        this.expandedWidth = f5;
        this.expandedHeight = f6;
        this.pxRatio = f7;
        this.apiFramework = str3;
        this.adSlotID = str4;
        this.altText = str5;
        this.adParameters = adParameters2;
    }

    @NonNull
    public final Builder newBuilder() {
        return new Builder(this);
    }

    @Nullable
    public final Float getHeight() {
        return this.height;
    }

    @Nullable
    public final Float getWidth() {
        return this.width;
    }
}
