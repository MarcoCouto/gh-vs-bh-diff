package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class ViewableImpression {
    public static final String ID = "id";
    public static final String NAME = "ViewableImpression";
    public static final String NOT_VIEWABLE = "NotViewable";
    public static final String VIEWABLE = "Viewable";
    public static final String VIEW_UNDETERMINED = "ViewUndetermined";
    @Nullable
    public final String id;
    @NonNull
    public final List<String> notViewable;
    @NonNull
    public final List<String> viewUndetermined;
    @NonNull
    public final List<String> viewable;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private List<String> b;
        @Nullable
        private List<String> c;
        @Nullable
        private List<String> d;

        @NonNull
        public Builder setId(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setViewable(@Nullable List<String> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setNotViewable(@Nullable List<String> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public Builder setViewUndetermined(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public ViewableImpression build() {
            this.b = VastModels.toImmutableList(this.b);
            this.c = VastModels.toImmutableList(this.c);
            this.d = VastModels.toImmutableList(this.d);
            return new ViewableImpression(this.b, this.c, this.d, this.a);
        }
    }

    ViewableImpression(@NonNull List<String> list, @NonNull List<String> list2, @NonNull List<String> list3, @Nullable String str) {
        this.id = str;
        this.viewable = list;
        this.notViewable = list2;
        this.viewUndetermined = list3;
    }
}
