package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Ad {
    public static final String AD_TYPE = "adType";
    public static final String CONDITIONAL_AD = "conditionalAd";
    public static final String ID = "id";
    public static final String INLINE = "InLine";
    public static final String NAME = "Ad";
    public static final String SEQUENCE = "sequence";
    public static final String WRAPPER = "Wrapper";
    @NonNull
    public final VideoAdType adType;
    @Nullable
    public final Boolean conditionalAd;
    @Nullable
    public final String id;
    @Nullable
    public final InLine inLine;
    @Nullable
    public final Integer sequence;
    @Nullable
    public final Wrapper wrapper;

    public static class Builder {
        @NonNull
        private VideoAdType a = VideoAdType.VIDEO;
        @Nullable
        private String b;
        @Nullable
        private InLine c;
        @Nullable
        private Wrapper d;
        @Nullable
        private Integer e;
        @Nullable
        private Boolean f;

        public Builder() {
        }

        public Builder(@NonNull Ad ad) {
            this.c = ad.inLine;
            this.d = ad.wrapper;
            this.b = ad.id;
            this.e = ad.sequence;
            this.f = ad.conditionalAd;
            this.a = ad.adType;
        }

        @NonNull
        public Builder setId(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setInLine(@Nullable InLine inLine) {
            this.c = inLine;
            return this;
        }

        @NonNull
        public Builder setWrapper(@Nullable Wrapper wrapper) {
            this.d = wrapper;
            return this;
        }

        @NonNull
        public Builder setSequence(@Nullable Integer num) {
            this.e = num;
            return this;
        }

        @NonNull
        public Builder setConditionalAd(@Nullable Boolean bool) {
            this.f = bool;
            return this;
        }

        @NonNull
        public Builder setAdType(@Nullable String str) {
            VideoAdType parse = VideoAdType.parse(str);
            if (parse == null) {
                parse = this.a;
            }
            this.a = parse;
            return this;
        }

        @NonNull
        public Ad build() {
            Ad ad = new Ad(this.b, this.c, this.d, this.e, this.f, this.a);
            return ad;
        }
    }

    Ad(@Nullable String str, @Nullable InLine inLine2, @Nullable Wrapper wrapper2, @Nullable Integer num, @Nullable Boolean bool, @NonNull VideoAdType videoAdType) {
        this.inLine = inLine2;
        this.wrapper = wrapper2;
        this.id = str;
        this.sequence = num;
        this.conditionalAd = bool;
        this.adType = videoAdType;
    }

    @NonNull
    public Builder newBuilder() {
        return new Builder(this);
    }
}
