package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public final class VastRawMediaFileScenario {
    @Nullable
    public final AdParameters adParameters;
    public final long duration;
    @Nullable
    public final MediaFile mediaFile;
    public final long skipOffset;
    @NonNull
    public final List<Tracking> trackingEvents;
    @Nullable
    public final VastIconScenario vastIconScenario;
    @NonNull
    public final VastScenarioCreativeData vastScenarioCreativeData;
    @Nullable
    public final VideoClicks videoClicks;

    public static class Builder {
        @Nullable
        private MediaFile a;
        @Nullable
        private List<Tracking> b;
        @Nullable
        private AdParameters c;
        @Nullable
        private VideoClicks d;
        @Nullable
        private VastIconScenario e;
        @Nullable
        private VastScenarioCreativeData f;
        private long g;
        private long h;

        /* synthetic */ Builder(VastRawMediaFileScenario vastRawMediaFileScenario, byte b2) {
            this(vastRawMediaFileScenario);
        }

        public Builder() {
        }

        private Builder(@NonNull VastRawMediaFileScenario vastRawMediaFileScenario) {
            this.b = vastRawMediaFileScenario.trackingEvents;
            this.f = vastRawMediaFileScenario.vastScenarioCreativeData;
            this.a = vastRawMediaFileScenario.mediaFile;
            this.g = vastRawMediaFileScenario.duration;
            this.h = vastRawMediaFileScenario.skipOffset;
            this.c = vastRawMediaFileScenario.adParameters;
            this.d = vastRawMediaFileScenario.videoClicks;
            this.e = vastRawMediaFileScenario.vastIconScenario;
        }

        @NonNull
        public Builder setTrackingEvents(@Nullable List<Tracking> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setMediaFile(@Nullable MediaFile mediaFile) {
            this.a = mediaFile;
            return this;
        }

        @NonNull
        public Builder setDuration(long j) {
            this.g = j;
            return this;
        }

        @NonNull
        public Builder setAdParameters(@Nullable AdParameters adParameters) {
            this.c = adParameters;
            return this;
        }

        @NonNull
        public Builder setSkipOffset(long j) {
            this.h = j;
            return this;
        }

        @NonNull
        public Builder setVideoClicks(@Nullable VideoClicks videoClicks) {
            this.d = videoClicks;
            return this;
        }

        @NonNull
        public Builder setVastIconScenario(@Nullable VastIconScenario vastIconScenario) {
            this.e = vastIconScenario;
            return this;
        }

        @NonNull
        public Builder setVastScenarioCreativeData(@Nullable VastScenarioCreativeData vastScenarioCreativeData) {
            this.f = vastScenarioCreativeData;
            return this;
        }

        @NonNull
        public VastRawMediaFileScenario build() {
            Objects.requireNonNull(this.f, "Cannot build VastMediaFileScenario: vastScenarioCreativeData is missing");
            VastRawMediaFileScenario vastRawMediaFileScenario = new VastRawMediaFileScenario(VastModels.toImmutableList(this.b), this.f, this.a, this.g, this.h, this.c, this.d, this.e, 0);
            return vastRawMediaFileScenario;
        }
    }

    /* synthetic */ VastRawMediaFileScenario(List list, VastScenarioCreativeData vastScenarioCreativeData2, MediaFile mediaFile2, long j, long j2, AdParameters adParameters2, VideoClicks videoClicks2, VastIconScenario vastIconScenario2, byte b) {
        this(list, vastScenarioCreativeData2, mediaFile2, j, j2, adParameters2, videoClicks2, vastIconScenario2);
    }

    private VastRawMediaFileScenario(@NonNull List<Tracking> list, @NonNull VastScenarioCreativeData vastScenarioCreativeData2, @Nullable MediaFile mediaFile2, long j, long j2, @Nullable AdParameters adParameters2, @Nullable VideoClicks videoClicks2, @Nullable VastIconScenario vastIconScenario2) {
        this.mediaFile = mediaFile2;
        this.vastScenarioCreativeData = vastScenarioCreativeData2;
        this.duration = j;
        this.skipOffset = j2;
        this.trackingEvents = list;
        this.adParameters = adParameters2;
        this.videoClicks = videoClicks2;
        this.vastIconScenario = vastIconScenario2;
    }

    public final Builder newBuilder() {
        return new Builder(this, 0);
    }
}
