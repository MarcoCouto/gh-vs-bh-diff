package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public final class VastIconScenario {
    @Nullable
    public final String apiFramework;
    public final long duration;
    @Nullable
    public final Float height;
    @Nullable
    public final IconClicks iconClicks;
    @NonNull
    public final List<String> iconViewTrackings;
    public final long offset;
    @Nullable
    public final String program;
    @Nullable
    public final Float pxRatio;
    @NonNull
    public final VastScenarioResourceData resourceData;
    @Nullable
    public final Float width;
    @Nullable
    public final String xPosition;
    @Nullable
    public final String yPosition;

    public static class Builder {
        @Nullable
        private VastScenarioResourceData a;
        @Nullable
        private List<String> b;
        @Nullable
        private Float c;
        @Nullable
        private Float d;
        @Nullable
        private String e;
        @Nullable
        private String f;
        @Nullable
        private String g;
        @Nullable
        private Float h;
        @Nullable
        private IconClicks i;
        @Nullable
        private String j;
        private long k;
        private long l;

        @NonNull
        public Builder setVastScenarioResourceData(@Nullable VastScenarioResourceData vastScenarioResourceData) {
            this.a = vastScenarioResourceData;
            return this;
        }

        @NonNull
        public Builder setIconViewTrackings(@Nullable List<String> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public Builder setWidth(@Nullable Float f2) {
            this.c = f2;
            return this;
        }

        @NonNull
        public Builder setHeight(@Nullable Float f2) {
            this.d = f2;
            return this;
        }

        @NonNull
        public Builder setProgram(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public Builder setXPosition(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public Builder setYPosition(@Nullable String str) {
            this.g = str;
            return this;
        }

        @NonNull
        public Builder setOffset(long j2) {
            this.k = j2;
            return this;
        }

        @NonNull
        public Builder setPxRatio(@Nullable Float f2) {
            this.h = f2;
            return this;
        }

        @NonNull
        public Builder setIconClicks(@Nullable IconClicks iconClicks) {
            this.i = iconClicks;
            return this;
        }

        @NonNull
        public Builder setApiFramework(@Nullable String str) {
            this.j = str;
            return this;
        }

        @NonNull
        public Builder setDuration(long j2) {
            this.l = j2;
            return this;
        }

        @NonNull
        public VastIconScenario build() throws VastElementMissingException {
            VastModels.requireNonNull(this.a, "Cannot build VastIconScenario: resourceData is missing");
            VastIconScenario vastIconScenario = new VastIconScenario(this.a, VastModels.toImmutableList(this.b), this.c, this.d, this.e, this.f, this.g, this.k, this.l, this.h, this.i, this.j, 0);
            return vastIconScenario;
        }
    }

    /* synthetic */ VastIconScenario(VastScenarioResourceData vastScenarioResourceData, List list, Float f, Float f2, String str, String str2, String str3, long j, long j2, Float f3, IconClicks iconClicks2, String str4, byte b) {
        this(vastScenarioResourceData, list, f, f2, str, str2, str3, j, j2, f3, iconClicks2, str4);
    }

    private VastIconScenario(@NonNull VastScenarioResourceData vastScenarioResourceData, @NonNull List<String> list, @Nullable Float f, @Nullable Float f2, @Nullable String str, @Nullable String str2, @Nullable String str3, long j, long j2, @Nullable Float f3, @Nullable IconClicks iconClicks2, @Nullable String str4) {
        this.iconViewTrackings = list;
        this.resourceData = vastScenarioResourceData;
        this.program = str;
        this.width = f;
        this.height = f2;
        this.xPosition = str2;
        this.yPosition = str3;
        this.offset = j;
        this.duration = j2;
        this.pxRatio = f3;
        this.iconClicks = iconClicks2;
        this.apiFramework = str4;
    }
}
