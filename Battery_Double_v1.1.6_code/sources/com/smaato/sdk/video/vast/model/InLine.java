package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.ArrayList;
import java.util.List;

public class InLine {
    public static final String ADVERTISER = "Advertiser";
    public static final String AD_SERVING_ID = "AdServingId";
    public static final String AD_SYSTEM = "AdSystem";
    public static final String AD_TITLE = "AdTitle";
    public static final String AD_VERIFICATIONS = "AdVerifications";
    public static final String CATEGORY = "Category";
    public static final String CREATIVES = "Creatives";
    public static final String DESCRIPTION = "Description";
    public static final String ERROR = "Error";
    public static final String EXTENSIONS = "Extensions";
    public static final String IMPRESSION = "Impression";
    public static final String NAME = "InLine";
    public static final String VIEWABLE_IMPRESSION = "ViewableImpression";
    @Nullable
    public final String adServingId;
    @Nullable
    public final AdSystem adSystem;
    @Nullable
    public final String adTitle;
    @NonNull
    public final List<Verification> adVerifications;
    @Nullable
    public final Advertiser advertiser;
    @NonNull
    public final List<Category> categories;
    @NonNull
    public final List<Creative> creatives;
    @Nullable
    public final String description;
    @NonNull
    public final List<String> errors;
    @NonNull
    public final List<Extension> extensions;
    @NonNull
    public final List<VastBeacon> impressions;
    @Nullable
    public final ViewableImpression viewableImpression;

    public static class Builder {
        @Nullable
        private AdSystem a;
        @Nullable
        private String b;
        @Nullable
        private String c;
        @Nullable
        private List<VastBeacon> d;
        @Nullable
        private List<Category> e;
        @Nullable
        private String f;
        @Nullable
        private Advertiser g;
        @Nullable
        private List<String> h;
        @Nullable
        private ViewableImpression i;
        @Nullable
        private List<Creative> j;
        @Nullable
        private List<Verification> k;
        @Nullable
        private List<Extension> l;

        @NonNull
        public Builder setAdSystem(@Nullable AdSystem adSystem) {
            this.a = adSystem;
            return this;
        }

        @NonNull
        public Builder setAdTitle(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setAdServingId(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Builder setImpressions(@Nullable List<VastBeacon> list) {
            this.d = list;
            return this;
        }

        @NonNull
        public Builder setCategories(@Nullable List<Category> list) {
            this.e = list;
            return this;
        }

        @NonNull
        public Builder setDescription(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public Builder setAdvertiser(@Nullable Advertiser advertiser) {
            this.g = advertiser;
            return this;
        }

        @NonNull
        public Builder setErrors(@Nullable List<String> list) {
            this.h = list;
            return this;
        }

        @NonNull
        public Builder setViewableImpression(@Nullable ViewableImpression viewableImpression) {
            this.i = viewableImpression;
            return this;
        }

        @NonNull
        public Builder setCreatives(@Nullable List<Creative> list) {
            this.j = list;
            return this;
        }

        @NonNull
        public Builder setAdVerifications(@Nullable List<Verification> list) {
            this.k = list;
            return this;
        }

        @NonNull
        public Builder setExtensions(@Nullable List<Extension> list) {
            this.l = list;
            return this;
        }

        @NonNull
        public InLine build() {
            ArrayList arrayList = new ArrayList();
            if (this.k != null && !this.k.isEmpty()) {
                arrayList.addAll(this.k);
            }
            if (this.l != null && !this.l.isEmpty()) {
                for (Extension extension : this.l) {
                    List<Verification> list = extension.adVerifications;
                    arrayList.getClass();
                    Objects.onNotNull(list, new Consumer(arrayList) {
                        private final /* synthetic */ List f$0;

                        {
                            this.f$0 = r1;
                        }

                        public final void accept(Object obj) {
                            this.f$0.addAll((List) obj);
                        }
                    });
                }
            }
            this.d = VastModels.toImmutableList(this.d);
            this.j = VastModels.toImmutableList(this.j);
            this.k = VastModels.toImmutableList(arrayList);
            this.e = VastModels.toImmutableList(this.e);
            this.h = VastModels.toImmutableList(this.h);
            this.l = VastModels.toImmutableList(this.l);
            InLine inLine = new InLine(this.d, this.j, this.k, this.e, this.h, this.a, this.b, this.c, this.f, this.g, this.i, this.l);
            return inLine;
        }
    }

    public InLine(@NonNull List<VastBeacon> list, @NonNull List<Creative> list2, @NonNull List<Verification> list3, @NonNull List<Category> list4, @NonNull List<String> list5, @Nullable AdSystem adSystem2, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable Advertiser advertiser2, @Nullable ViewableImpression viewableImpression2, @NonNull List<Extension> list6) {
        this.adSystem = adSystem2;
        this.adTitle = str;
        this.adServingId = str2;
        this.impressions = list;
        this.categories = list4;
        this.description = str3;
        this.advertiser = advertiser2;
        this.errors = list5;
        this.viewableImpression = viewableImpression2;
        this.creatives = list2;
        this.adVerifications = list3;
        this.extensions = list6;
    }
}
