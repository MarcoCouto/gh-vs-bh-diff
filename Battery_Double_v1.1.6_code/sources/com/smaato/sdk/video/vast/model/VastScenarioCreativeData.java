package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class VastScenarioCreativeData {
    @Nullable
    public final String adId;
    @Nullable
    public final String apiFramework;
    @Nullable
    public final String id;
    @Nullable
    public final Integer sequence;
    @NonNull
    public final UniversalAdId universalAdId;

    public static class Builder {
        @Nullable
        private UniversalAdId a;
        @Nullable
        private String b;
        @Nullable
        private String c;
        @Nullable
        private Integer d;
        @Nullable
        private String e;

        @NonNull
        public Builder setUniversalAdId(@Nullable UniversalAdId universalAdId) {
            this.a = universalAdId;
            return this;
        }

        @NonNull
        public Builder setId(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setAdId(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Builder setSequence(@Nullable Integer num) {
            this.d = num;
            return this;
        }

        @NonNull
        public Builder setApiFramework(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public VastScenarioCreativeData build() {
            if (this.a == null) {
                this.a = UniversalAdId.DEFAULT;
            }
            VastScenarioCreativeData vastScenarioCreativeData = new VastScenarioCreativeData(this.a, this.b, this.c, this.d, this.e, 0);
            return vastScenarioCreativeData;
        }
    }

    /* synthetic */ VastScenarioCreativeData(UniversalAdId universalAdId2, String str, String str2, Integer num, String str3, byte b) {
        this(universalAdId2, str, str2, num, str3);
    }

    private VastScenarioCreativeData(@NonNull UniversalAdId universalAdId2, @Nullable String str, @Nullable String str2, @Nullable Integer num, @Nullable String str3) {
        this.universalAdId = universalAdId2;
        this.id = str;
        this.adId = str2;
        this.sequence = num;
        this.apiFramework = str3;
    }
}
