package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.regex.Pattern;

public final class StaticResource {
    public static final String CREATIVE_TYPE = "creativeType";
    public static final String NAME = "StaticResource";
    /* access modifiers changed from: private */
    public static final Pattern a = Pattern.compile("(image/[^\\s;]+)");
    @NonNull
    public final CreativeType creativeType;
    @NonNull
    public final String uri;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private CreativeType b;

        @NonNull
        public Builder setUri(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setCreativeType(@Nullable CreativeType creativeType) {
            this.b = creativeType;
            return this;
        }

        @NonNull
        public StaticResource build() throws VastElementMissingException {
            CreativeType creativeType = this.b;
            if (creativeType == null) {
                creativeType = CreativeType.UNKNOWN;
            }
            return new StaticResource((String) VastModels.requireNonNull(this.a, "Cannot build StaticResource: uri is missing"), creativeType, 0);
        }
    }

    public enum CreativeType {
        JAVASCRIPT,
        IMAGE,
        UNKNOWN;

        @Nullable
        public static CreativeType parse(@Nullable String str) {
            if (!TextUtils.isEmpty(str)) {
                if (StaticResource.a.matcher(str.trim()).find()) {
                    return IMAGE;
                }
                if (WebRequest.CONTENT_TYPE_JAVASCRIPT.equalsIgnoreCase(str)) {
                    return JAVASCRIPT;
                }
            }
            return null;
        }
    }

    /* synthetic */ StaticResource(String str, CreativeType creativeType2, byte b) {
        this(str, creativeType2);
    }

    private StaticResource(@NonNull String str, @NonNull CreativeType creativeType2) {
        this.uri = (String) Objects.requireNonNull(str);
        this.creativeType = (CreativeType) Objects.requireNonNull(creativeType2);
    }
}
