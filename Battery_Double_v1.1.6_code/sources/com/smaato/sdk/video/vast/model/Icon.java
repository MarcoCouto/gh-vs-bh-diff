package com.smaato.sdk.video.vast.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.utils.VastModels;
import java.util.List;

public class Icon {
    public static final String API_FRAMEWORK = "apiFramework";
    public static final String DURATION = "duration";
    public static final String HEIGHT = "height";
    public static final String HTML_RESOURCE = "HTMLResource";
    public static final String ICON_CLICKS = "IconClicks";
    public static final String ICON_VIEW_TRACKING = "IconViewTracking";
    public static final String I_FRAME_RESOURCE = "IFrameResource";
    public static final String NAME = "Icon";
    public static final String OFFSET = "offset";
    public static final String PROGRAM = "program";
    public static final String PX_RATIO = "pxratio";
    public static final String STATIC_RESOURCE = "StaticResource";
    public static final String WIDTH = "width";
    public static final String X_POSITION = "xPosition";
    public static final String Y_POSITION = "yPosition";
    @Nullable
    public final String apiFramework;
    @Nullable
    public final String duration;
    @Nullable
    public final Float height;
    @NonNull
    public final List<String> htmlResources;
    @NonNull
    public final List<String> iFrameResources;
    @Nullable
    public final IconClicks iconClicks;
    @NonNull
    public final List<String> iconViewTrackings;
    @Nullable
    public final String offset;
    @Nullable
    public final String program;
    @Nullable
    public final Float pxRatio;
    @NonNull
    public final List<StaticResource> staticResources;
    @Nullable
    public final Float width;
    @Nullable
    public final String xPosition;
    @Nullable
    public final String yPosition;

    public static class Builder {
        @Nullable
        private List<String> a;
        @Nullable
        private String b;
        @Nullable
        private Float c;
        @Nullable
        private Float d;
        @Nullable
        private String e;
        @Nullable
        private String f;
        @Nullable
        private String g;
        @Nullable
        private String h;
        @Nullable
        private Float i;
        @Nullable
        private IconClicks j;
        @Nullable
        private List<StaticResource> k;
        @Nullable
        private List<String> l;
        @Nullable
        private List<String> m;
        @Nullable
        private String n;

        @NonNull
        public Builder setProgram(@Nullable String str) {
            this.b = str;
            return this;
        }

        @NonNull
        public Builder setWidth(@Nullable Float f2) {
            this.c = f2;
            return this;
        }

        @NonNull
        public Builder setHeight(@Nullable Float f2) {
            this.d = f2;
            return this;
        }

        @NonNull
        public Builder setXPosition(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public Builder setYPosition(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public Builder setOffset(@Nullable String str) {
            this.g = str;
            return this;
        }

        @NonNull
        public Builder setDuration(@Nullable String str) {
            this.h = str;
            return this;
        }

        @NonNull
        public Builder setPxRatio(@Nullable Float f2) {
            this.i = f2;
            return this;
        }

        @NonNull
        public Builder setIconViewTrackings(@Nullable List<String> list) {
            this.a = list;
            return this;
        }

        @NonNull
        public Builder setIconClicks(@Nullable IconClicks iconClicks) {
            this.j = iconClicks;
            return this;
        }

        @NonNull
        public Builder setStaticResources(@Nullable List<StaticResource> list) {
            this.k = list;
            return this;
        }

        @NonNull
        public Builder setIFrameResources(@Nullable List<String> list) {
            this.l = list;
            return this;
        }

        @NonNull
        public Builder setHtmlResources(@Nullable List<String> list) {
            this.m = list;
            return this;
        }

        @NonNull
        public Builder setApiFramework(@Nullable String str) {
            this.n = str;
            return this;
        }

        @NonNull
        public Icon build() {
            this.a = VastModels.toImmutableList(this.a);
            this.k = VastModels.toImmutableList(this.k);
            this.l = VastModels.toImmutableList(this.l);
            this.m = VastModels.toImmutableList(this.m);
            Icon icon = new Icon(this.a, this.k, this.l, this.m, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.n);
            return icon;
        }
    }

    Icon(@NonNull List<String> list, @NonNull List<StaticResource> list2, @NonNull List<String> list3, @NonNull List<String> list4, @Nullable String str, @Nullable Float f, @Nullable Float f2, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable Float f3, @Nullable IconClicks iconClicks2, @Nullable String str6) {
        this.program = str;
        this.width = f;
        this.height = f2;
        this.xPosition = str2;
        this.yPosition = str3;
        this.offset = str4;
        this.duration = str5;
        this.pxRatio = f3;
        this.iconViewTrackings = list;
        this.iconClicks = iconClicks2;
        this.staticResources = list2;
        this.iFrameResources = list3;
        this.htmlResources = list4;
        this.apiFramework = str6;
    }
}
