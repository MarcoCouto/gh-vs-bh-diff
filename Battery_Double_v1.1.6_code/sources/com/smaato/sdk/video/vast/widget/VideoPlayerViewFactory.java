package com.smaato.sdk.video.vast.widget;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.player.VideoPlayerView;

public interface VideoPlayerViewFactory {
    @NonNull
    VideoPlayerView getVideoPlayerView(@NonNull Context context);
}
