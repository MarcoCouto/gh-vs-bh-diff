package com.smaato.sdk.video.vast.widget.element;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;

public interface VastElementPresentationManager {
    void prepare(@NonNull VastElementView vastElementView, @NonNull Consumer<VastElementException> consumer);
}
