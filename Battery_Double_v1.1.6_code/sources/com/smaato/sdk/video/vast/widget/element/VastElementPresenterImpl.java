package com.smaato.sdk.video.vast.widget.element;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTracker;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.tracker.VisibilityTrackerListener;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.browser.VastWebComponentSecurityPolicy;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenter.Listener;
import java.lang.ref.WeakReference;

public class VastElementPresenterImpl implements VastElementPresenter {
    @NonNull
    private final VastWebComponentSecurityPolicy a;
    @NonNull
    private final SomaApiContext b;
    @NonNull
    private final VisibilityTrackerCreator c;
    @NonNull
    private final VastElementPresentationManager d;
    @NonNull
    private final VastElementErrorCodeStrategy e;
    @NonNull
    private final Logger f;
    @NonNull
    private WeakReference<VastElementView> g = new WeakReference<>(null);
    @Nullable
    private VisibilityTracker h;
    @Nullable
    private Listener i;

    public void onContentStartedToLoad() {
    }

    public VastElementPresenterImpl(@NonNull Logger logger, @NonNull VastElementPresentationManager vastElementPresentationManager, @NonNull VastWebComponentSecurityPolicy vastWebComponentSecurityPolicy, @NonNull SomaApiContext somaApiContext, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VastElementErrorCodeStrategy vastElementErrorCodeStrategy) {
        this.f = (Logger) Objects.requireNonNull(logger);
        this.d = (VastElementPresentationManager) Objects.requireNonNull(vastElementPresentationManager);
        this.a = (VastWebComponentSecurityPolicy) Objects.requireNonNull(vastWebComponentSecurityPolicy);
        this.b = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.c = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.e = (VastElementErrorCodeStrategy) Objects.requireNonNull(vastElementErrorCodeStrategy);
    }

    public boolean isValidUrl(@NonNull String str) {
        if (this.a.validateUrl(this.b, str)) {
            return true;
        }
        onError(new SecurityViolationException());
        return false;
    }

    @CallSuper
    public void onContentLoaded() {
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                VastElementPresenterImpl.this.b((VastElementView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(VastElementView vastElementView) {
        this.h = this.c.createTracker(vastElementView, new VisibilityTrackerListener() {
            public final void onVisibilityHappen() {
                VastElementPresenterImpl.this.a();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        Objects.onNotNull(this.i, $$Lambda$ZYG3zl19ehiXJxD0lLZ5rSa7jbI.INSTANCE);
    }

    public void onConfigurationChanged() {
        Objects.onNotNull(this.g.get(), new Consumer() {
            public final void accept(Object obj) {
                VastElementPresenterImpl.this.a((VastElementView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(VastElementView vastElementView) {
        this.d.prepare(vastElementView, new Consumer() {
            public final void accept(Object obj) {
                VastElementPresenterImpl.this.onError((VastElementException) obj);
            }
        });
    }

    @CallSuper
    public void attachView(@NonNull VastElementView vastElementView) {
        this.g = new WeakReference<>(vastElementView);
        this.d.prepare(vastElementView, new Consumer() {
            public final void accept(Object obj) {
                VastElementPresenterImpl.this.onError((VastElementException) obj);
            }
        });
    }

    @CallSuper
    public void detachView() {
        this.g.clear();
        Objects.onNotNull(this.h, $$Lambda$180cT8NvrkJZ64YGAeZod3HsJqI.INSTANCE);
    }

    public void onClicked(@Nullable String str) {
        Objects.onNotNull(this.i, new Consumer(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                ((Listener) obj).onVastElementClicked(this.f$0);
            }
        });
    }

    public void onError(@NonNull VastElementException vastElementException) {
        this.f.debug(LogDomain.VAST, String.format("VastElement error: %s", new Object[]{vastElementException.toString()}), new Object[0]);
        Objects.onNotNull(this.i, new Consumer(vastElementException) {
            private final /* synthetic */ VastElementException f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                VastElementPresenterImpl.this.a(this.f$1, (Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(VastElementException vastElementException, Listener listener) {
        listener.onVastElementError(this.e.getVastErrorCode(vastElementException));
    }

    /* access modifiers changed from: protected */
    @Nullable
    public VastElementView getView() {
        return (VastElementView) this.g.get();
    }

    public void setListener(@Nullable Listener listener) {
        this.i = listener;
    }

    public void onRenderProcessGone() {
        Objects.onNotNull(this.i, $$Lambda$k_4PMag9ZoV_6iAJ3PP2g68so.INSTANCE);
    }
}
