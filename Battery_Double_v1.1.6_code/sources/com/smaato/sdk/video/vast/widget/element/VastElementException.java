package com.smaato.sdk.video.vast.widget.element;

import androidx.annotation.NonNull;

public abstract class VastElementException extends Exception {
    public VastElementException() {
    }

    public VastElementException(@NonNull String str) {
        super(str);
    }
}
