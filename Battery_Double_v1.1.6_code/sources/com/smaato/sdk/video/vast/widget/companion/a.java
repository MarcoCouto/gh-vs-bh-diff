package com.smaato.sdk.video.vast.widget.companion;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import com.smaato.sdk.video.vast.model.VastScenarioResourceData;
import com.smaato.sdk.video.vast.utils.VastScenarioResourceDataConverter;
import com.smaato.sdk.video.vast.widget.element.VastElementException;
import com.smaato.sdk.video.vast.widget.element.VastElementLoadingException;
import com.smaato.sdk.video.vast.widget.element.VastElementPresentationManager;
import com.smaato.sdk.video.vast.widget.element.VastElementView;

final class a implements VastElementPresentationManager {
    @NonNull
    private final Logger a;
    @NonNull
    private final VastCompanionScenario b;
    @NonNull
    private final VastScenarioResourceDataConverter c;

    a(@NonNull Logger logger, @NonNull VastCompanionScenario vastCompanionScenario, @NonNull VastScenarioResourceDataConverter vastScenarioResourceDataConverter) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (VastCompanionScenario) Objects.requireNonNull(vastCompanionScenario);
        this.c = (VastScenarioResourceDataConverter) Objects.requireNonNull(vastScenarioResourceDataConverter);
    }

    public final void prepare(@NonNull VastElementView vastElementView, @NonNull Consumer<VastElementException> consumer) {
        VastCompanionScenario vastCompanionScenario = this.b;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i = displayMetrics.widthPixels;
        int i2 = displayMetrics.heightPixels;
        float f = displayMetrics.density;
        float dpToPx = (float) UIUtils.dpToPx(UIUtils.getNormalizedSize(vastCompanionScenario.width), f);
        float dpToPx2 = (float) UIUtils.dpToPx(UIUtils.getNormalizedSize(vastCompanionScenario.height), f);
        if (dpToPx <= 0.0f) {
            dpToPx = (float) i;
        }
        if (dpToPx2 <= 0.0f) {
            dpToPx2 = (float) i2;
        }
        float f2 = (float) i;
        if (dpToPx > f2) {
            dpToPx2 = (dpToPx2 / dpToPx) * f2;
        } else {
            f2 = dpToPx;
        }
        float f3 = (float) i2;
        if (dpToPx2 > f3) {
            f2 = (f2 / dpToPx2) * f3;
            dpToPx2 = f3;
        }
        Size size = new Size((int) f2, (int) dpToPx2);
        VastScenarioResourceData vastScenarioResourceData = this.b.resourceData;
        String uriFromResources = this.c.getUriFromResources(vastScenarioResourceData, size.width, size.height);
        if (TextUtils.isEmpty(uriFromResources)) {
            consumer.accept(new VastElementLoadingException(String.format("Error while preparing Companion. Unable to convert Companion resource: %s", new Object[]{vastScenarioResourceData})));
            return;
        }
        vastElementView.load(uriFromResources);
        vastElementView.setSize(size.width, size.height);
    }
}
