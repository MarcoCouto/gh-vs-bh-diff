package com.smaato.sdk.video.vast.widget;

import android.content.Context;
import android.view.SurfaceView;
import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.player.VideoPlayerView;

public class SurfaceViewVideoPlayerView extends VideoPlayerView {
    public SurfaceViewVideoPlayerView(@NonNull Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public VastSurfaceHolder initVastSurfaceHolder(@NonNull Context context) {
        return new SurfaceViewVastSurfaceHolder(new SurfaceView(context));
    }
}
