package com.smaato.sdk.video.vast.widget;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout.LayoutParams;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.AndroidsInjector;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.diinjection.Inject;
import com.smaato.sdk.video.R;
import com.smaato.sdk.video.vast.player.VideoPlayerView;
import com.smaato.sdk.video.vast.widget.element.VastElementView;

public class VastVideoPlayerView extends AdContentView {
    @Inject
    @NonNull
    private VideoPlayerViewFactory a;
    @NonNull
    private VideoPlayerView b;
    @NonNull
    private VastElementView c = ((VastElementView) findViewById(R.id.smaato_sdk_video_icon_view_id));
    @NonNull
    private VastElementView d = ((VastElementView) findViewById(R.id.smaato_sdk_video_companion_view_id));

    public VastVideoPlayerView(@NonNull Context context) {
        super(context);
        AndroidsInjector.inject((View) this);
        inflate(context, R.layout.smaato_sdk_video_vast_video_player_view, this);
        this.b = this.a.getVideoPlayerView(context);
        this.b.setId(R.id.smaato_sdk_video_video_player_view_id);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.smaato_sdk_video_player_layout);
        frameLayout.removeAllViews();
        frameLayout.addView(this.b, new LayoutParams(-1, -1));
    }

    @NonNull
    public VastElementView getIconView() {
        return this.c;
    }

    @NonNull
    public VastElementView getCompanionAdView() {
        return this.d;
    }

    @NonNull
    public VideoPlayerView getVideoPlayerView() {
        return this.b;
    }

    public void showCompanion() {
        Threads.runOnUi(new Runnable() {
            public final void run() {
                VastVideoPlayerView.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        this.d.setVisibility(0);
        this.d.requestFocus();
    }

    public void hidePlayer() {
        Threads.runOnUi(new Runnable() {
            public final void run() {
                VastVideoPlayerView.this.a();
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        Objects.onNotNull(this.b, $$Lambda$VastVideoPlayerView$yf_QZ0J6FURGYa68_fcvnOa9B0o.INSTANCE);
    }
}
