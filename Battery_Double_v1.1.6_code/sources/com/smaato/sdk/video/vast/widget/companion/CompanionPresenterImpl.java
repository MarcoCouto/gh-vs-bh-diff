package com.smaato.sdk.video.vast.widget.companion;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.video.vast.browser.VastWebComponentSecurityPolicy;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import com.smaato.sdk.video.vast.widget.element.VastElementErrorCodeStrategy;
import com.smaato.sdk.video.vast.widget.element.VastElementPresentationManager;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenterImpl;

public class CompanionPresenterImpl extends VastElementPresenterImpl {
    @NonNull
    private final VastCompanionScenario a;

    CompanionPresenterImpl(@NonNull Logger logger, @NonNull VastElementPresentationManager vastElementPresentationManager, @NonNull VastWebComponentSecurityPolicy vastWebComponentSecurityPolicy, @NonNull SomaApiContext somaApiContext, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VastElementErrorCodeStrategy vastElementErrorCodeStrategy, @NonNull VastCompanionScenario vastCompanionScenario) {
        super(logger, vastElementPresentationManager, vastWebComponentSecurityPolicy, somaApiContext, visibilityTrackerCreator, vastElementErrorCodeStrategy);
        this.a = (VastCompanionScenario) Objects.requireNonNull(vastCompanionScenario);
    }

    public void onClicked(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            str = this.a.companionClickThrough;
        }
        super.onClicked(str);
    }
}
