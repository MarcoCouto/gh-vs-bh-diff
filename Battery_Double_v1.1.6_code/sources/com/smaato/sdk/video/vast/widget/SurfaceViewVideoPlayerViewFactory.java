package com.smaato.sdk.video.vast.widget;

import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.player.VideoPlayerView;

public class SurfaceViewVideoPlayerViewFactory implements VideoPlayerViewFactory {
    @NonNull
    public VideoPlayerView getVideoPlayerView(@NonNull Context context) {
        return new SurfaceViewVideoPlayerView(context);
    }
}
