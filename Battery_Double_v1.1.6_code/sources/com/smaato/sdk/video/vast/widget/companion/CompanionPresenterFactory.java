package com.smaato.sdk.video.vast.widget.companion;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.browser.VastWebComponentSecurityPolicy;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.utils.VastScenarioResourceDataConverter;
import com.smaato.sdk.video.vast.widget.element.NoOpVastElementPresenter;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenter;

public final class CompanionPresenterFactory {
    @NonNull
    private final VastScenarioResourceDataConverter a;
    @NonNull
    private final VisibilityTrackerCreator b;
    @NonNull
    private final VastWebComponentSecurityPolicy c;
    @NonNull
    private final CompanionErrorCodeStrategy d;

    public CompanionPresenterFactory(@NonNull VastScenarioResourceDataConverter vastScenarioResourceDataConverter, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VastWebComponentSecurityPolicy vastWebComponentSecurityPolicy, @NonNull CompanionErrorCodeStrategy companionErrorCodeStrategy) {
        this.a = (VastScenarioResourceDataConverter) Objects.requireNonNull(vastScenarioResourceDataConverter);
        this.b = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.c = (VastWebComponentSecurityPolicy) Objects.requireNonNull(vastWebComponentSecurityPolicy);
        this.d = (CompanionErrorCodeStrategy) Objects.requireNonNull(companionErrorCodeStrategy);
    }

    @NonNull
    public final VastElementPresenter create(@NonNull Logger logger, @NonNull VastScenario vastScenario, @NonNull SomaApiContext somaApiContext) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(somaApiContext);
        VastCompanionScenario vastCompanionScenario = vastScenario.vastCompanionScenario;
        if (vastCompanionScenario == null) {
            return new NoOpVastElementPresenter();
        }
        CompanionPresenterImpl companionPresenterImpl = new CompanionPresenterImpl(logger, new a(logger, vastCompanionScenario, this.a), this.c, somaApiContext, this.b, this.d, vastCompanionScenario);
        return companionPresenterImpl;
    }
}
