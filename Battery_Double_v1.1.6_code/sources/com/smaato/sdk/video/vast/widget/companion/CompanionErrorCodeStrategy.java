package com.smaato.sdk.video.vast.widget.companion;

import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.widget.element.VastElementErrorCodeStrategy;
import com.smaato.sdk.video.vast.widget.element.VastElementException;
import com.smaato.sdk.video.vast.widget.element.VastElementLoadingException;

public final class CompanionErrorCodeStrategy implements VastElementErrorCodeStrategy {
    public final int getVastErrorCode(@NonNull VastElementException vastElementException) {
        return vastElementException instanceof VastElementLoadingException ? 603 : 900;
    }
}
