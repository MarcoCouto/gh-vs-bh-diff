package com.smaato.sdk.video.vast.widget.element;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenter.Listener;

public class NoOpVastElementPresenter implements VastElementPresenter {
    public void attachView(@NonNull VastElementView vastElementView) {
    }

    public void detachView() {
    }

    public boolean isValidUrl(@NonNull String str) {
        return false;
    }

    public void onClicked(@Nullable String str) {
    }

    public void onConfigurationChanged() {
    }

    public void onContentLoaded() {
    }

    public void onContentStartedToLoad() {
    }

    public void onError(@NonNull VastElementException vastElementException) {
    }

    public void onRenderProcessGone() {
    }

    public void setListener(@Nullable Listener listener) {
    }
}
