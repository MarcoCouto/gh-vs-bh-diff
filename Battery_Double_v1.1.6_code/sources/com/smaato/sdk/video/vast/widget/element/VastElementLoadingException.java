package com.smaato.sdk.video.vast.widget.element;

import androidx.annotation.NonNull;

public final class VastElementLoadingException extends VastElementException {
    public VastElementLoadingException(@NonNull String str) {
        super(str);
    }
}
