package com.smaato.sdk.video.vast.widget.icon;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.VastIconScenario;
import com.smaato.sdk.video.vast.utils.VastScenarioResourceDataConverter;
import com.smaato.sdk.video.vast.widget.element.VastElementException;
import com.smaato.sdk.video.vast.widget.element.VastElementLoadingException;
import com.smaato.sdk.video.vast.widget.element.VastElementPresentationManager;
import com.smaato.sdk.video.vast.widget.element.VastElementView;

final class a implements VastElementPresentationManager {
    @NonNull
    private final Logger a;
    @NonNull
    private final VastIconScenario b;
    @NonNull
    private final VastScenarioResourceDataConverter c;

    a(@NonNull Logger logger, @NonNull VastIconScenario vastIconScenario, @NonNull VastScenarioResourceDataConverter vastScenarioResourceDataConverter) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (VastIconScenario) Objects.requireNonNull(vastIconScenario);
        this.c = (VastScenarioResourceDataConverter) Objects.requireNonNull(vastScenarioResourceDataConverter);
    }

    public final void prepare(@NonNull VastElementView vastElementView, @NonNull Consumer<VastElementException> consumer) {
        float f = Resources.getSystem().getDisplayMetrics().density;
        float max = Math.max(Math.min(UIUtils.getNormalizedSize(this.b.width), 50.0f), 12.0f);
        float max2 = Math.max(Math.min(UIUtils.getNormalizedSize(this.b.height), 50.0f), 12.0f);
        int dpToPx = UIUtils.dpToPx(max, f);
        int dpToPx2 = UIUtils.dpToPx(max2, f);
        String uriFromResources = this.c.getUriFromResources(this.b.resourceData, dpToPx, dpToPx2);
        if (TextUtils.isEmpty(uriFromResources)) {
            consumer.accept(new VastElementLoadingException(String.format("Error while preparing Icon. Unable to convert Icon resource: %s", new Object[]{uriFromResources})));
            return;
        }
        vastElementView.load(uriFromResources);
        vastElementView.setSize(dpToPx, dpToPx2);
    }
}
