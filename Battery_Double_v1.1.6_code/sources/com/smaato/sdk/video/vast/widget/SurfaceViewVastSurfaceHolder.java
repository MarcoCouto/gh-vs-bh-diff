package com.smaato.sdk.video.vast.widget;

import android.graphics.Rect;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.widget.VastSurfaceHolder.OnSurfaceAvailableListener;
import com.smaato.sdk.video.vast.widget.VastSurfaceHolder.OnSurfaceChangedListener;
import com.smaato.sdk.video.vast.widget.VastSurfaceHolder.OnSurfaceDestroyedListener;

public class SurfaceViewVastSurfaceHolder implements VastSurfaceHolder {
    @NonNull
    private final SurfaceView a;
    /* access modifiers changed from: private */
    @Nullable
    public OnSurfaceAvailableListener b;
    /* access modifiers changed from: private */
    @Nullable
    public OnSurfaceChangedListener c;
    /* access modifiers changed from: private */
    @Nullable
    public OnSurfaceDestroyedListener d;

    SurfaceViewVastSurfaceHolder(@NonNull SurfaceView surfaceView) {
        this.a = surfaceView;
        surfaceView.getHolder().addCallback(new Callback() {
            public final void surfaceCreated(SurfaceHolder surfaceHolder) {
                Objects.onNotNull(SurfaceViewVastSurfaceHolder.this.b, new Consumer(surfaceHolder) {
                    private final /* synthetic */ SurfaceHolder f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        AnonymousClass1.a(this.f$0, (OnSurfaceAvailableListener) obj);
                    }
                });
            }

            /* access modifiers changed from: private */
            public static /* synthetic */ void a(SurfaceHolder surfaceHolder, OnSurfaceAvailableListener onSurfaceAvailableListener) {
                Surface surface = surfaceHolder.getSurface();
                if (surface != null) {
                    Rect surfaceFrame = surfaceHolder.getSurfaceFrame();
                    onSurfaceAvailableListener.onSurfaceAvailable(surface, surfaceFrame.width(), surfaceFrame.height());
                }
            }

            public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
                Objects.onNotNull(SurfaceViewVastSurfaceHolder.this.c, new Consumer(surfaceHolder, i2, i3) {
                    private final /* synthetic */ SurfaceHolder f$0;
                    private final /* synthetic */ int f$1;
                    private final /* synthetic */ int f$2;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    public final void accept(Object obj) {
                        AnonymousClass1.a(this.f$0, this.f$1, this.f$2, (OnSurfaceChangedListener) obj);
                    }
                });
            }

            /* access modifiers changed from: private */
            public static /* synthetic */ void a(SurfaceHolder surfaceHolder, int i, int i2, OnSurfaceChangedListener onSurfaceChangedListener) {
                Surface surface = surfaceHolder.getSurface();
                if (surface != null) {
                    onSurfaceChangedListener.onSurfaceChanged(surface, i, i2);
                }
            }

            public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                Objects.onNotNull(SurfaceViewVastSurfaceHolder.this.d, new Consumer(surfaceHolder) {
                    private final /* synthetic */ SurfaceHolder f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        AnonymousClass1.a(this.f$0, (OnSurfaceDestroyedListener) obj);
                    }
                });
            }

            /* access modifiers changed from: private */
            public static /* synthetic */ void a(SurfaceHolder surfaceHolder, OnSurfaceDestroyedListener onSurfaceDestroyedListener) {
                Surface surface = surfaceHolder.getSurface();
                if (surface != null) {
                    onSurfaceDestroyedListener.onSurfaceDestroyed(surface);
                }
            }
        });
    }

    @NonNull
    public View getView() {
        return this.a;
    }

    @Nullable
    public Surface getSurface() {
        return this.a.getHolder().getSurface();
    }

    public void setOnSurfaceAvailableListener(@Nullable OnSurfaceAvailableListener onSurfaceAvailableListener) {
        this.b = onSurfaceAvailableListener;
    }

    public void setOnSurfaceChangedListener(@Nullable OnSurfaceChangedListener onSurfaceChangedListener) {
        this.c = onSurfaceChangedListener;
    }

    public void setOnSurfaceDestroyedListener(@Nullable OnSurfaceDestroyedListener onSurfaceDestroyedListener) {
        this.d = onSurfaceDestroyedListener;
    }
}
