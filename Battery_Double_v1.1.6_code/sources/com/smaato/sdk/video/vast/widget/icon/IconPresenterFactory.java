package com.smaato.sdk.video.vast.widget.icon;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.video.utils.AnimationHelper;
import com.smaato.sdk.video.vast.browser.VastWebComponentSecurityPolicy;
import com.smaato.sdk.video.vast.model.VastIconScenario;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.utils.VastScenarioResourceDataConverter;
import com.smaato.sdk.video.vast.widget.element.NoOpVastElementPresenter;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenter;

public final class IconPresenterFactory {
    @NonNull
    private final VastScenarioResourceDataConverter a;
    @NonNull
    private final VisibilityTrackerCreator b;
    @NonNull
    private final VastWebComponentSecurityPolicy c;
    @NonNull
    private final OneTimeActionFactory d;
    @NonNull
    private final AnimationHelper e;
    @NonNull
    private final IconErrorCodeStrategy f;

    public IconPresenterFactory(@NonNull VastScenarioResourceDataConverter vastScenarioResourceDataConverter, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VastWebComponentSecurityPolicy vastWebComponentSecurityPolicy, @NonNull OneTimeActionFactory oneTimeActionFactory, @NonNull AnimationHelper animationHelper, @NonNull IconErrorCodeStrategy iconErrorCodeStrategy) {
        this.a = (VastScenarioResourceDataConverter) Objects.requireNonNull(vastScenarioResourceDataConverter);
        this.b = (VisibilityTrackerCreator) Objects.requireNonNull(visibilityTrackerCreator);
        this.c = (VastWebComponentSecurityPolicy) Objects.requireNonNull(vastWebComponentSecurityPolicy);
        this.d = (OneTimeActionFactory) Objects.requireNonNull(oneTimeActionFactory);
        this.e = (AnimationHelper) Objects.requireNonNull(animationHelper);
        this.f = (IconErrorCodeStrategy) Objects.requireNonNull(iconErrorCodeStrategy);
    }

    @NonNull
    public final VastElementPresenter create(@NonNull Logger logger, @NonNull VastMediaFileScenario vastMediaFileScenario, @NonNull SomaApiContext somaApiContext) {
        VastMediaFileScenario vastMediaFileScenario2 = vastMediaFileScenario;
        VastIconScenario vastIconScenario = vastMediaFileScenario2.vastIconScenario;
        if (vastIconScenario == null) {
            return new NoOpVastElementPresenter();
        }
        Logger logger2 = logger;
        Logger logger3 = logger;
        b bVar = new b(logger3, new a(logger, vastIconScenario, this.a), this.c, somaApiContext, this.b, this.f, vastIconScenario, this.d, this.e, vastMediaFileScenario2.duration);
        return bVar;
    }
}
