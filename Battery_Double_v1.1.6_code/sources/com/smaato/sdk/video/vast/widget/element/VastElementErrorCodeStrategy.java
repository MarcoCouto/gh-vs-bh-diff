package com.smaato.sdk.video.vast.widget.element;

import androidx.annotation.NonNull;

public interface VastElementErrorCodeStrategy {
    int getVastErrorCode(@NonNull VastElementException vastElementException);
}
