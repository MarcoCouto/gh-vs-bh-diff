package com.smaato.sdk.video.vast.widget.icon;

import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.widget.element.VastElementErrorCodeStrategy;
import com.smaato.sdk.video.vast.widget.element.VastElementException;

public final class IconErrorCodeStrategy implements VastElementErrorCodeStrategy {
    public final int getVastErrorCode(@NonNull VastElementException vastElementException) {
        return 900;
    }
}
