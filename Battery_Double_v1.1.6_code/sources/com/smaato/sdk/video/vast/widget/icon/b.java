package com.smaato.sdk.video.vast.widget.icon;

import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.tracker.VisibilityTrackerCreator;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeAction.Listener;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.utils.AnimationHelper;
import com.smaato.sdk.video.vast.browser.VastWebComponentSecurityPolicy;
import com.smaato.sdk.video.vast.model.IconClicks;
import com.smaato.sdk.video.vast.model.VastIconScenario;
import com.smaato.sdk.video.vast.widget.element.VastElementErrorCodeStrategy;
import com.smaato.sdk.video.vast.widget.element.VastElementPresentationManager;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenterImpl;
import com.smaato.sdk.video.vast.widget.element.VastElementView;

final class b extends VastElementPresenterImpl {
    @NonNull
    private final VastIconScenario a;
    @NonNull
    private final OneTimeActionFactory b;
    @NonNull
    private final AnimationHelper c;
    private final long d;
    private long e;

    b(@NonNull Logger logger, @NonNull VastElementPresentationManager vastElementPresentationManager, @NonNull VastWebComponentSecurityPolicy vastWebComponentSecurityPolicy, @NonNull SomaApiContext somaApiContext, @NonNull VisibilityTrackerCreator visibilityTrackerCreator, @NonNull VastElementErrorCodeStrategy vastElementErrorCodeStrategy, @NonNull VastIconScenario vastIconScenario, @NonNull OneTimeActionFactory oneTimeActionFactory, @NonNull AnimationHelper animationHelper, long j) {
        super(logger, vastElementPresentationManager, vastWebComponentSecurityPolicy, somaApiContext, visibilityTrackerCreator, vastElementErrorCodeStrategy);
        this.a = (VastIconScenario) Objects.requireNonNull(vastIconScenario);
        this.b = (OneTimeActionFactory) Objects.requireNonNull(oneTimeActionFactory);
        this.c = (AnimationHelper) Objects.requireNonNull(animationHelper);
        this.d = j;
    }

    public final void onContentStartedToLoad() {
        this.e = SystemClock.uptimeMillis();
    }

    public final void onClicked(@Nullable String str) {
        String str2;
        IconClicks iconClicks = this.a.iconClicks;
        if (iconClicks == null) {
            str2 = null;
        } else {
            str2 = iconClicks.iconClickThrough;
        }
        super.onClicked(str2);
    }

    public final void onContentLoaded() {
        super.onContentLoaded();
        long uptimeMillis = SystemClock.uptimeMillis() - this.e;
        this.b.createOneTimeAction(new Listener(uptimeMillis) {
            private final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            public final void doAction() {
                b.this.a(this.f$1);
            }
        }).start(Math.max(this.a.offset - uptimeMillis, 0));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(long j) {
        Objects.onNotNull(getView(), new Consumer(j) {
            private final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                b.this.a(this.f$1, (VastElementView) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(long j, VastElementView vastElementView) {
        this.c.showWithAnim(vastElementView);
        long j2 = this.a.duration;
        if (((float) j2) <= 0.0f) {
            j2 = this.d - j;
        }
        if (((float) j2) > 0.0f) {
            this.b.createOneTimeAction(new Listener() {
                public final void doAction() {
                    b.this.a();
                }
            }).start(j2);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a() {
        VastElementView view = getView();
        AnimationHelper animationHelper = this.c;
        animationHelper.getClass();
        Objects.onNotNull(view, new Consumer() {
            public final void accept(Object obj) {
                AnimationHelper.this.hideWithAnim((VastElementView) obj);
            }
        });
    }
}
