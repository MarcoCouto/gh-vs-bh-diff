package com.smaato.sdk.video.vast.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.R;

public class CircularProgressBar extends View {
    @Nullable
    private String a;
    @NonNull
    private RectF b = new RectF();
    @NonNull
    private Rect c = new Rect();
    @NonNull
    private Paint d = new Paint(1);
    @NonNull
    private Paint e = new Paint(1);
    @NonNull
    private Paint f = new Paint(1);
    private float g = 0.0f;
    private float h = 100.0f;
    private float i = getResources().getDimension(R.dimen.smaato_sdk_video_default_background_stroke_width);
    private float j = getResources().getDimension(R.dimen.smaato_sdk_video_default_stroke_width);
    private int k = ViewCompat.MEASURED_STATE_MASK;
    private int l = -7829368;
    private float m = 48.0f;

    /* JADX INFO: finally extract failed */
    public CircularProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.smaato_sdk_video_circular_progress_bar, 0, 0);
        try {
            this.i = obtainStyledAttributes.getDimension(R.styleable.smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_progressbar_width, this.i);
            this.j = obtainStyledAttributes.getDimension(R.styleable.smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_background_progressbar_width, this.j);
            this.k = obtainStyledAttributes.getInt(R.styleable.smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_progressbar_color, this.k);
            this.l = obtainStyledAttributes.getInt(R.styleable.smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_background_progressbar_color, this.l);
            this.m = obtainStyledAttributes.getDimension(R.styleable.smaato_sdk_video_circular_progress_bar_smaato_sdk_video_cpb_label_font_size, this.m);
            obtainStyledAttributes.recycle();
            this.d.setColor(this.l);
            this.d.setStyle(Style.STROKE);
            this.d.setStrokeWidth(this.j);
            this.e.setColor(this.k);
            this.e.setStyle(Style.FILL);
            this.f.setColor(this.l);
            this.f.setTextSize(this.m);
            this.f.setStyle(Style.FILL_AND_STROKE);
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawOval(this.b, this.e);
        String str = this.a;
        float width = this.b.width() / 2.0f;
        float height = this.b.height() / 2.0f;
        if (!TextUtils.isEmpty(str)) {
            this.f.getTextBounds(str, 0, str.length(), this.c);
            canvas.drawText(str, width - (((float) this.c.width()) / 2.0f), height + (((float) this.c.height()) / 2.0f), this.f);
        }
        Canvas canvas2 = canvas;
        canvas2.drawArc(this.b, 270.0f, ((100.0f - ((this.g / this.h) * 100.0f)) * -360.0f) / 100.0f, false, this.d);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int min = Math.min(getDefaultSize(getSuggestedMinimumWidth(), i2), getDefaultSize(getSuggestedMinimumHeight(), i3));
        setMeasuredDimension(min, min);
        float f2 = (this.i > this.j ? this.i : this.j) / 2.0f;
        float f3 = ((float) min) - f2;
        this.b.set(f2, f2, f3, f3);
    }

    @MainThread
    public void setProgress(float f2, float f3, @NonNull String str) {
        if (f3 < 0.0f) {
            f3 = 100.0f;
        }
        boolean z = false;
        boolean z2 = Math.abs(this.h - f3) > 0.0f;
        if (z2) {
            this.h = f3;
        }
        if (f2 > this.h) {
            f2 = this.h;
        }
        boolean z3 = Math.abs(this.g - f2) > 0.0f;
        if (z3) {
            this.g = f2;
        }
        boolean z4 = !TextUtils.equals(this.a, str);
        if (z4) {
            this.a = str;
        }
        if (z3 || z2 || z4) {
            z = true;
        }
        if (z) {
            requestLayout();
            invalidate();
        }
    }

    public float getProgress() {
        return this.g;
    }

    public float getProgressMax() {
        return this.h;
    }
}
