package com.smaato.sdk.video.vast.widget.element;

import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.widget.element.VastElementPresenter.Listener;

/* renamed from: com.smaato.sdk.video.vast.widget.element.-$$Lambda$ZYG3zl19ehiXJxD0lLZ5rSa7jbI reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ZYG3zl19ehiXJxD0lLZ5rSa7jbI implements Consumer {
    public static final /* synthetic */ $$Lambda$ZYG3zl19ehiXJxD0lLZ5rSa7jbI INSTANCE = new $$Lambda$ZYG3zl19ehiXJxD0lLZ5rSa7jbI();

    private /* synthetic */ $$Lambda$ZYG3zl19ehiXJxD0lLZ5rSa7jbI() {
    }

    public final void accept(Object obj) {
        ((Listener) obj).onVastElementRendered();
    }
}
