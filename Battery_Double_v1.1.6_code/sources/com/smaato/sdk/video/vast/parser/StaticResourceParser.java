package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.CheckedFunction;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.StaticResource;
import com.smaato.sdk.video.vast.model.StaticResource.Builder;
import com.smaato.sdk.video.vast.model.StaticResource.CreativeType;
import java.util.ArrayList;
import java.util.List;

public class StaticResourceParser implements XmlClassParser<StaticResource> {
    private static final CheckedFunction<String, CreativeType> a = $$Lambda$StaticResourceParser$Lcr5qMYm0isnCIsrz2Caq15sI0.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ CreativeType a(String str) throws Exception {
        return (CreativeType) Objects.requireNonNull(CreativeType.parse(str));
    }

    @NonNull
    public ParseResult<StaticResource> parse(@NonNull RegistryXmlParser registryXmlParser) {
        StaticResource staticResource;
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        CheckedFunction<String, CreativeType> checkedFunction = a;
        builder.getClass();
        $$Lambda$yvWI1m6FKiFcM0z27rHeKY5o3qY r4 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setCreativeType((CreativeType) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseTypedAttribute = registryXmlParser.parseTypedAttribute("creativeType", checkedFunction, r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        builder.getClass();
        parseTypedAttribute.parseString(new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setUri((String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("StaticResource", new Exception("Unable to parse StaticResource uri", (Exception) obj)));
            }
        });
        try {
            staticResource = builder.build();
        } catch (VastElementMissingException e) {
            arrayList.add(ParseError.buildFrom("StaticResource", e));
            staticResource = null;
        }
        return new ParseResult.Builder().setResult(staticResource).setErrors(arrayList).build();
    }
}
