package com.smaato.sdk.video.vast.parser;

import android.util.Xml;
import androidx.annotation.NonNull;
import java.io.UnsupportedEncodingException;

public final class XmlEncodingUtils {
    private XmlEncodingUtils() {
    }

    public static boolean isSupported(@NonNull String str) {
        try {
            return Xml.findEncodingByName(str) != null;
        } catch (UnsupportedEncodingException unused) {
            return false;
        }
    }
}
