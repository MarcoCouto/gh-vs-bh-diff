package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.CompanionAds;
import com.smaato.sdk.video.vast.model.Creative;
import com.smaato.sdk.video.vast.model.Creative.Builder;
import com.smaato.sdk.video.vast.model.Linear;
import com.smaato.sdk.video.vast.model.UniversalAdId;
import java.util.ArrayList;
import java.util.List;

public class CreativeParser implements XmlClassParser<Creative> {
    private static final String[] a = {"UniversalAdId", "CompanionAds", "Linear"};

    @NonNull
    public ParseResult<Creative> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        builder.getClass();
        $$Lambda$cwejmya3H3d7C710WzI45rGf0rA r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setId((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$5oazT5nbNbk5tYB87t81DUOIfpk r4 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setSequence((Integer) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseIntegerAttribute = registryXmlParser.parseStringAttribute("id", r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseIntegerAttribute("sequence", r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String str = Creative.AD_ID;
        builder.getClass();
        $$Lambda$mzbA3RGmuTQLxBiOCdswY4xpZSI r42 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setAdId((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$2R7Y1DNJOK6V5gZpiE3PNGAYmHo r43 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setApiFramework((String) obj);
            }
        };
        arrayList.getClass();
        parseIntegerAttribute.parseStringAttribute(str, r42, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("apiFramework", r43, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseTags(a, new Consumer(builder, arrayList) {
            private final /* synthetic */ Builder f$1;
            private final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                CreativeParser.a(RegistryXmlParser.this, this.f$1, this.f$2, (String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("Creative", new Exception("Unable to parse tags in Creative", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, Builder builder, List list, String str) {
        if (str.equalsIgnoreCase("UniversalAdId")) {
            registryXmlParser.parseClass("UniversalAdId", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    CreativeParser.c(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if (str.equalsIgnoreCase("CompanionAds")) {
            registryXmlParser.parseClass("CompanionAds", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    CreativeParser.b(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else {
            if (str.equalsIgnoreCase("Linear")) {
                registryXmlParser.parseClass("Linear", new NonNullConsumer(list) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        CreativeParser.a(Builder.this, this.f$1, (ParseResult) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(Builder builder, List list, ParseResult parseResult) {
        builder.setUniversalAdId((UniversalAdId) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(Builder builder, List list, ParseResult parseResult) {
        builder.setCompanionAds((CompanionAds) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Builder builder, List list, ParseResult parseResult) {
        builder.setLinear((Linear) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
