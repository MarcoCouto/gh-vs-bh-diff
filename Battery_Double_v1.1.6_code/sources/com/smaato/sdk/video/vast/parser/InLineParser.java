package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.AdSystem;
import com.smaato.sdk.video.vast.model.Advertiser;
import com.smaato.sdk.video.vast.model.InLine;
import com.smaato.sdk.video.vast.model.InLine.Builder;
import com.smaato.sdk.video.vast.model.ViewableImpression;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class InLineParser implements XmlClassParser<InLine> {
    private static final String[] a = {"AdSystem", InLine.AD_TITLE, InLine.AD_SERVING_ID, "Impression", "Category", InLine.DESCRIPTION, "Advertiser", "Error", "ViewableImpression", "AdVerifications", "Creatives", "Extensions"};

    @NonNull
    public ParseResult<InLine> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        String[] strArr = a;
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        builder.setImpressions(arrayList2);
        builder.setCategories(arrayList3);
        builder.setAdVerifications(arrayList5);
        builder.setExtensions(arrayList6);
        builder.setErrors(arrayList4);
        $$Lambda$InLineParser$MoMFu_bas0yLnaJbkBB0mJOJZs r0 = new Consumer(registryXmlParser, builder, arrayList, arrayList2, arrayList3, arrayList4, arrayList5, arrayList6) {
            private final /* synthetic */ RegistryXmlParser f$1;
            private final /* synthetic */ Builder f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;
            private final /* synthetic */ List f$5;
            private final /* synthetic */ List f$6;
            private final /* synthetic */ List f$7;
            private final /* synthetic */ List f$8;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
            }

            public final void accept(Object obj) {
                InLineParser.this.a(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, (String) obj);
            }
        };
        registryXmlParser.parseTags(strArr, r0, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("InLine", new Exception("Unable to parse tags in InLine")));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RegistryXmlParser registryXmlParser, Builder builder, List list, List list2, List list3, List list4, List list5, List list6, String str) {
        if ("AdSystem".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("AdSystem", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    InLineParser.a(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if (InLine.AD_TITLE.equalsIgnoreCase(str)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setAdTitle((String) obj);
                }
            }, new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(InLine.AD_TITLE, new Exception("Unable to parse AdTitle value", (Exception) obj)));
                }
            });
        } else if (InLine.AD_SERVING_ID.equalsIgnoreCase(str)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setAdServingId((String) obj);
                }
            }, new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(InLine.AD_SERVING_ID, new Exception("Unable to parse AdServingId value", (Exception) obj)));
                }
            });
        } else if ("Impression".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("Impression", new NonNullConsumer(list2, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    InLineParser.a(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("Category".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("Category", new NonNullConsumer(list3, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    InLineParser.b(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        } else if (InLine.DESCRIPTION.equalsIgnoreCase(str)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setDescription((String) obj);
                }
            }, new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(InLine.DESCRIPTION, new Exception("Unable to parse Description value", (Exception) obj)));
                }
            });
        } else if ("Advertiser".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("Advertiser", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    InLineParser.b(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("Error".equalsIgnoreCase(str)) {
            list4.getClass();
            registryXmlParser.parseString(new Consumer(list4) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("Error", new Exception("Unable to parse Error value", (Exception) obj)));
                }
            });
        } else if ("ViewableImpression".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("ViewableImpression", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    InLineParser.c(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("Creatives".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("Creatives", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    InLineParser.d(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("AdVerifications".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("AdVerifications", new NonNullConsumer(list5, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    InLineParser.c(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        } else {
            if ("Extensions".equalsIgnoreCase(str)) {
                registryXmlParser.parseClass("Extensions", new NonNullConsumer(list6, list) {
                    private final /* synthetic */ List f$0;
                    private final /* synthetic */ List f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        InLineParser.d(this.f$0, this.f$1, (ParseResult) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(List list, List list2, ParseResult parseResult) {
        if (parseResult.value != null) {
            list.addAll((Collection) parseResult.value);
        }
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(List list, List list2, ParseResult parseResult) {
        if (parseResult.value != null) {
            list.addAll((Collection) parseResult.value);
        }
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(Builder builder, List list, ParseResult parseResult) {
        builder.setCreatives((List) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(Builder builder, List list, ParseResult parseResult) {
        builder.setViewableImpression((ViewableImpression) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(Builder builder, List list, ParseResult parseResult) {
        if (parseResult.value != null) {
            builder.setAdvertiser((Advertiser) parseResult.value);
        }
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(List list, List list2, ParseResult parseResult) {
        if (parseResult.value != null) {
            list.add(parseResult.value);
        }
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        if (parseResult.value != null) {
            list.add(parseResult.value);
        }
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Builder builder, List list, ParseResult parseResult) {
        builder.setAdSystem((AdSystem) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
