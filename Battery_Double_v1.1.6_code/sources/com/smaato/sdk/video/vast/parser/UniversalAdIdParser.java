package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.UniversalAdId;
import com.smaato.sdk.video.vast.model.UniversalAdId.Builder;
import java.util.ArrayList;
import java.util.List;

public class UniversalAdIdParser implements XmlClassParser<UniversalAdId> {
    @NonNull
    public ParseResult<UniversalAdId> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        String str = UniversalAdId.ID_REGISTRY;
        builder.getClass();
        $$Lambda$XlkkLawgbjwRWP98PtdnxKTzI r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setIdRegistry((String) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseStringAttribute = registryXmlParser.parseStringAttribute(str, r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String str2 = UniversalAdId.ID_VALUE;
        builder.getClass();
        $$Lambda$G7t6zx2zNENJ_LtT5hNaZlRsTDI r32 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setIdValue((String) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseStringAttribute2 = parseStringAttribute.parseStringAttribute(str2, r32, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        builder.getClass();
        parseStringAttribute2.parseString(new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setValue((String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("UniversalAdId", new Exception("Unable to parse UniversalAdId value", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }
}
