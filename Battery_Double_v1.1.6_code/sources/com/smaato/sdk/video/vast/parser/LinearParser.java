package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.AdParameters;
import com.smaato.sdk.video.vast.model.Linear;
import com.smaato.sdk.video.vast.model.Linear.Builder;
import com.smaato.sdk.video.vast.model.VideoClicks;
import java.util.ArrayList;
import java.util.List;

public class LinearParser implements XmlClassParser<Linear> {
    private static final String[] a = {"Duration", "AdParameters", "MediaFiles", "VideoClicks", "TrackingEvents", Linear.ICONS};

    @NonNull
    public ParseResult<Linear> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        builder.getClass();
        $$Lambda$kzfa88dDfR3Dg0AUqwLxQEI9Wz8 r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setSkipOffset((String) obj);
            }
        };
        arrayList.getClass();
        registryXmlParser.parseStringAttribute("skipoffset", r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseTags(a, new Consumer(builder, arrayList) {
            private final /* synthetic */ Builder f$1;
            private final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                LinearParser.a(RegistryXmlParser.this, this.f$1, this.f$2, (String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("Linear", new Exception("Unable to parse tags in Linear")));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, Builder builder, List list, String str) {
        if ("Duration".equalsIgnoreCase(str)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setDuration((String) obj);
                }
            }, new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("Duration", new Exception("Unable to parse Duration value", (Exception) obj)));
                }
            });
        } else if ("AdParameters".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("AdParameters", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    LinearParser.e(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("MediaFiles".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("MediaFiles", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    LinearParser.d(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("VideoClicks".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("VideoClicks", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    LinearParser.c(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("TrackingEvents".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("TrackingEvents", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    LinearParser.b(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else {
            if (Linear.ICONS.equalsIgnoreCase(str)) {
                registryXmlParser.parseClass(Linear.ICONS, new NonNullConsumer(list) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        LinearParser.a(Builder.this, this.f$1, (ParseResult) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void e(Builder builder, List list, ParseResult parseResult) {
        builder.setAdParameters((AdParameters) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(Builder builder, List list, ParseResult parseResult) {
        builder.setMediaFiles((List) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(Builder builder, List list, ParseResult parseResult) {
        builder.setVideoClicks((VideoClicks) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(Builder builder, List list, ParseResult parseResult) {
        builder.setTrackingEvents((List) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Builder builder, List list, ParseResult parseResult) {
        builder.setIcons((List) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
