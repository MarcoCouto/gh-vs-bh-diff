package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.collections.Lists;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class ParseResult<Result> {
    @NonNull
    public final List<ParseError> errors;
    @Nullable
    public final Result value;

    public static class Builder<Result> {
        @Nullable
        private Result a;
        @Nullable
        private List<ParseError> b;

        @NonNull
        public Builder<Result> setResult(@Nullable Result result) {
            this.a = result;
            return this;
        }

        @NonNull
        public Builder<Result> setErrors(@Nullable List<ParseError> list) {
            this.b = list;
            return this;
        }

        @NonNull
        public ParseResult<Result> build() {
            if (this.a != null || this.b != null) {
                return new ParseResult<>(Lists.toImmutableList((Collection<T>) this.b), this.a, 0);
            }
            throw new IllegalStateException("ParseResult should contain value or list of errors at least");
        }
    }

    /* synthetic */ ParseResult(List list, Object obj, byte b) {
        this(list, obj);
    }

    private ParseResult(@NonNull List<ParseError> list, @Nullable Result result) {
        this.errors = list;
        this.value = result;
    }

    @NonNull
    public static <Result> ParseResult<Result> error(@NonNull String str, @Nullable Exception exc) {
        return new ParseResult<>(Collections.singletonList(ParseError.buildFrom(str, exc)), null);
    }

    @NonNull
    public static <Result> ParseResult<Result> error(@NonNull ParseError parseError) {
        return new ParseResult<>(Collections.singletonList(parseError), null);
    }
}
