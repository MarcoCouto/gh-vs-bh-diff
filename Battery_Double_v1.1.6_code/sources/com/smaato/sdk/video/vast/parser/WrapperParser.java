package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.AdSystem;
import com.smaato.sdk.video.vast.model.VastBeacon;
import com.smaato.sdk.video.vast.model.ViewableImpression;
import com.smaato.sdk.video.vast.model.Wrapper;
import com.smaato.sdk.video.vast.model.Wrapper.Builder;
import java.util.ArrayList;
import java.util.List;

public class WrapperParser implements XmlClassParser<Wrapper> {
    private static final String[] a = {"Impression", Wrapper.VAST_AD_TAG_URI, "AdSystem", "Error", "ViewableImpression", "AdVerifications", "Creatives", Wrapper.BLOCKED_AD_CATEGORIES};

    @NonNull
    public ParseResult<Wrapper> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        builder.getClass();
        $$Lambda$2yKP6zL0XHlBsuPjsEn68NIXa8 r1 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setFollowAdditionalWrappers((Boolean) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$3QeM2lJR4RBQR2kba_BJxkwEBmg r2 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setAllowMultipleAds((Boolean) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$HNnXBJwCIy9GHfi6pxIEBusnAw8 r22 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setFallbackOnNoAd((Boolean) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseBooleanAttribute = registryXmlParser.parseBooleanAttribute("followAdditionalWrappers", r1, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseBooleanAttribute("allowMultipleAds", r2, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseBooleanAttribute("fallbackOnNoAd", r22, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String[] strArr = a;
        ArrayList arrayList2 = new ArrayList();
        builder.setImpressions(arrayList2);
        ArrayList arrayList3 = new ArrayList();
        builder.setErrors(arrayList3);
        $$Lambda$WrapperParser$Jw1B5GhTNstn5cEn2ArZ0_yr2c r0 = new Consumer(arrayList2, arrayList, builder, arrayList3) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ Builder f$3;
            private final /* synthetic */ List f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void accept(Object obj) {
                WrapperParser.a(RegistryXmlParser.this, this.f$1, this.f$2, this.f$3, this.f$4, (String) obj);
            }
        };
        parseBooleanAttribute.parseTags(strArr, r0, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("Wrapper", new Exception("Unable to parse tags in Wrapper", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, List list, List list2, Builder builder, List list3, String str) {
        if (str.equalsIgnoreCase("Impression")) {
            registryXmlParser.parseClass("Impression", new NonNullConsumer(list, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    WrapperParser.a(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        } else if (str.equalsIgnoreCase(Wrapper.VAST_AD_TAG_URI)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setVastAdTagUri((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(Wrapper.VAST_AD_TAG_URI, new Exception("Unable to parse VastAdTagUri in Wrapper", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase(Wrapper.BLOCKED_AD_CATEGORIES)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setBlockedAdCategories((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(Wrapper.BLOCKED_AD_CATEGORIES, new Exception("Unable to parse BlockedAdCategories in Wrapper", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase("AdSystem")) {
            registryXmlParser.parseClass("AdSystem", new NonNullConsumer(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    WrapperParser.d(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if (str.equalsIgnoreCase("Error")) {
            list3.getClass();
            registryXmlParser.parseString(new Consumer(list3) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("Error", new Exception("Unable to parse Error in Wrapper", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase("ViewableImpression")) {
            registryXmlParser.parseClass("ViewableImpression", new NonNullConsumer(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    WrapperParser.c(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if (str.equalsIgnoreCase("AdVerifications")) {
            registryXmlParser.parseClass("AdVerifications", new NonNullConsumer(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    WrapperParser.b(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else {
            if (str.equalsIgnoreCase("Creatives")) {
                registryXmlParser.parseClass("Creatives", new NonNullConsumer(list2) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        WrapperParser.a(Builder.this, this.f$1, (ParseResult) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        Result result = parseResult.value;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((VastBeacon) obj);
            }
        });
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void d(Builder builder, List list, ParseResult parseResult) {
        builder.setAdSystem((AdSystem) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void c(Builder builder, List list, ParseResult parseResult) {
        builder.setViewableImpression((ViewableImpression) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(Builder builder, List list, ParseResult parseResult) {
        Result result = parseResult.value;
        builder.getClass();
        Objects.onNotNull(result, new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setAdVerifications((List) obj);
            }
        });
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Builder builder, List list, ParseResult parseResult) {
        Result result = parseResult.value;
        builder.getClass();
        Objects.onNotNull(result, new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setCreatives((List) obj);
            }
        });
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
