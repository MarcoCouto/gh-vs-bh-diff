package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.Icon;
import com.smaato.sdk.video.vast.model.Icon.Builder;
import com.smaato.sdk.video.vast.model.IconClicks;
import java.util.ArrayList;
import java.util.List;

public class IconParser implements XmlClassParser<Icon> {
    private static final String[] a = {"StaticResource", "IFrameResource", "HTMLResource", "IconClicks", Icon.ICON_VIEW_TRACKING};

    @NonNull
    public ParseResult<Icon> parse(@NonNull RegistryXmlParser registryXmlParser) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        Builder builder = new Builder();
        builder.setIconViewTrackings(arrayList);
        builder.setStaticResources(arrayList2);
        builder.setIFrameResources(arrayList3);
        builder.setHtmlResources(arrayList4);
        ArrayList arrayList5 = new ArrayList();
        String str = Icon.PROGRAM;
        builder.getClass();
        $$Lambda$yv3b2fIi6yUZmhhFSIfVnMeNGaA r1 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setProgram((String) obj);
            }
        };
        arrayList5.getClass();
        builder.getClass();
        $$Lambda$rokjuhw1FKiT9r0Vd_F47JGvos r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setWidth((Float) obj);
            }
        };
        arrayList5.getClass();
        builder.getClass();
        $$Lambda$ZNbZQwrjvwMr99OGAprWOw27JkI r32 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setHeight((Float) obj);
            }
        };
        arrayList5.getClass();
        RegistryXmlParser parseFloatAttribute = registryXmlParser.parseStringAttribute(str, r1, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("width", r3, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("height", r32, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String str2 = Icon.X_POSITION;
        builder.getClass();
        $$Lambda$ao0Z_xNc5rm2D3C9IeGz_III r33 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setXPosition((String) obj);
            }
        };
        arrayList5.getClass();
        RegistryXmlParser parseStringAttribute = parseFloatAttribute.parseStringAttribute(str2, r33, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String str3 = Icon.Y_POSITION;
        builder.getClass();
        $$Lambda$P6GMSECdlYfyF8QbpqHewwX_iRU r34 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setYPosition((String) obj);
            }
        };
        arrayList5.getClass();
        builder.getClass();
        $$Lambda$GUZUaXUcCllr0gqSVpjX0uXl0 r35 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setDuration((String) obj);
            }
        };
        arrayList5.getClass();
        builder.getClass();
        $$Lambda$iEOW1T7S8nta5hVhVBEUNl1ZNU r36 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setOffset((String) obj);
            }
        };
        arrayList5.getClass();
        builder.getClass();
        $$Lambda$NAVde81aQeQWTu_KPVV5aqvo_I r37 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setApiFramework((String) obj);
            }
        };
        arrayList5.getClass();
        builder.getClass();
        $$Lambda$Eh7SXOfwCRvewePHF812c78bsU r38 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setPxRatio((Float) obj);
            }
        };
        arrayList5.getClass();
        parseStringAttribute.parseStringAttribute(str3, r34, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("duration", r35, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("offset", r36, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("apiFramework", r37, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("pxratio", r38, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String[] strArr = a;
        $$Lambda$IconParser$QxiUS9MHIptWD5yOgwYSpi3GRak r0 = new Consumer(arrayList2, arrayList5, arrayList3, arrayList4, arrayList, builder) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;
            private final /* synthetic */ List f$5;
            private final /* synthetic */ Builder f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            public final void accept(Object obj) {
                IconParser.a(RegistryXmlParser.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (String) obj);
            }
        };
        registryXmlParser.parseTags(strArr, r0, new Consumer(arrayList5) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom(Icon.NAME, new Exception("Unable to parse URL value", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList5).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, List list, List list2, List list3, List list4, List list5, Builder builder, String str) {
        if (str.equalsIgnoreCase("StaticResource")) {
            registryXmlParser.parseClass("StaticResource", new NonNullConsumer(list, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    IconParser.a(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        } else if (str.equalsIgnoreCase("IFrameResource")) {
            list3.getClass();
            registryXmlParser.parseString(new Consumer(list3) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("IFrameResource", new Exception("Unable to parse IFrameResource value", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase("HTMLResource")) {
            list4.getClass();
            registryXmlParser.parseString(new Consumer(list4) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("HTMLResource", new Exception("Unable to parse HTMLResource value", (Exception) obj)));
                }
            });
        } else if (str.equalsIgnoreCase(Icon.ICON_VIEW_TRACKING)) {
            list5.getClass();
            registryXmlParser.parseString(new Consumer(list5) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(Icon.ICON_VIEW_TRACKING, new Exception("Unable to parse IconViewTracking value", (Exception) obj)));
                }
            });
        } else {
            if (str.equalsIgnoreCase("IconClicks")) {
                registryXmlParser.parseClass("IconClicks", new NonNullConsumer(list2) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        IconParser.a(Builder.this, this.f$1, (ParseResult) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        if (parseResult.value != null) {
            list.add(parseResult.value);
        }
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Builder builder, List list, ParseResult parseResult) {
        builder.setIconClicks((IconClicks) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
