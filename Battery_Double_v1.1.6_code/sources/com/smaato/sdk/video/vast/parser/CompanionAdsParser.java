package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.CheckedFunction;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.Companion;
import com.smaato.sdk.video.vast.model.CompanionAds;
import com.smaato.sdk.video.vast.model.CompanionAds.Builder;
import com.smaato.sdk.video.vast.model.CompanionAds.Required;
import java.util.ArrayList;
import java.util.List;

public class CompanionAdsParser implements XmlClassParser<CompanionAds> {
    private static final CheckedFunction<String, Required> a = $$Lambda$CompanionAdsParser$kiTNKjVl6SH0bp0Aeg4w_RCZjug.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ Required a(String str) throws Exception {
        return (Required) Objects.requireNonNull(Required.parse(str));
    }

    @NonNull
    public ParseResult<CompanionAds> parse(@NonNull RegistryXmlParser registryXmlParser) {
        CompanionAds companionAds;
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        builder.setCompanions(arrayList);
        ArrayList arrayList2 = new ArrayList();
        CheckedFunction<String, Required> checkedFunction = a;
        builder.getClass();
        $$Lambda$dSLfw8c8Is_Ly24LeE6Njeyr4JM r5 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setRequired((Required) obj);
            }
        };
        arrayList2.getClass();
        registryXmlParser.parseTypedAttribute("required", checkedFunction, r5, new Consumer(arrayList2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseTags(new String[]{"Companion"}, new Consumer(arrayList, arrayList2) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                RegistryXmlParser.this.parseClass("Companion", new NonNullConsumer(this.f$1, this.f$2) {
                    private final /* synthetic */ List f$0;
                    private final /* synthetic */ List f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        CompanionAdsParser.a(this.f$0, this.f$1, (ParseResult) obj);
                    }
                });
            }
        }, new Consumer(arrayList2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("CompanionAds", new Exception("Unable to parse tags in CompanionAds", (Exception) obj)));
            }
        });
        try {
            companionAds = builder.build();
        } catch (VastElementMissingException e) {
            arrayList2.add(ParseError.buildFrom("CompanionAds", e));
            companionAds = null;
        }
        return new ParseResult.Builder().setResult(companionAds).setErrors(arrayList2).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        Result result = parseResult.value;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((Companion) obj);
            }
        });
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
