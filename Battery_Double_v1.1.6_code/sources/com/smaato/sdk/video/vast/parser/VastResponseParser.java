package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.VastTree;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParserException;

public class VastResponseParser {
    @NonNull
    private final RegistryXmlParser a;

    public VastResponseParser(@NonNull RegistryXmlParser registryXmlParser) {
        this.a = (RegistryXmlParser) Objects.requireNonNull(registryXmlParser, "Parameter xmlPullParser cannot be null for VastResponseParser::new");
    }

    public void parseVastResponse(@NonNull Logger logger, @NonNull InputStream inputStream, @Nullable String str, @NonNull NonNullConsumer<ParseResult<VastTree>> nonNullConsumer) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(inputStream);
        Objects.requireNonNull(nonNullConsumer);
        try {
            this.a.prepare(inputStream, str).parseClass("VAST", nonNullConsumer);
        } catch (IOException | XmlPullParserException e) {
            nonNullConsumer.accept(ParseResult.error("VAST", e));
        }
    }
}
