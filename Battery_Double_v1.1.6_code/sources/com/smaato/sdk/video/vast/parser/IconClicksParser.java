package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.IconClicks;
import com.smaato.sdk.video.vast.model.IconClicks.Builder;
import com.smaato.sdk.video.vast.model.VastBeacon;
import java.util.ArrayList;
import java.util.List;

public class IconClicksParser implements XmlClassParser<IconClicks> {
    private static final String[] a = {IconClicks.ICON_CLICK_THROUGH, IconClicks.ICON_CLICK_TRACKING};

    @NonNull
    public ParseResult<IconClicks> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        builder.setIconClickTrackings(arrayList2);
        registryXmlParser.parseTags(a, new Consumer(builder, arrayList, arrayList2) {
            private final /* synthetic */ Builder f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                IconClicksParser.a(RegistryXmlParser.this, this.f$1, this.f$2, this.f$3, (String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("IconClicks", new Exception("Unable to parse IconClicks value", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, Builder builder, List list, List list2, String str) {
        if (str.equalsIgnoreCase(IconClicks.ICON_CLICK_THROUGH)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setIconClickThrough((String) obj);
                }
            }, new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(IconClicks.ICON_CLICK_THROUGH, new Exception("Unable to parse IconClickThrough value", (Exception) obj)));
                }
            });
            return;
        }
        if (str.equalsIgnoreCase(IconClicks.ICON_CLICK_TRACKING)) {
            registryXmlParser.parseClass(IconClicks.ICON_CLICK_TRACKING, new NonNullConsumer(list2, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    IconClicksParser.a(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        Result result = parseResult.value;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((VastBeacon) obj);
            }
        });
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
