package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.Advertiser;
import com.smaato.sdk.video.vast.model.Advertiser.Builder;
import java.util.ArrayList;
import java.util.List;

final class a implements XmlClassParser<Advertiser> {
    /* access modifiers changed from: private */
    public static /* synthetic */ void a(ParseError parseError) {
    }

    a() {
    }

    @NonNull
    public final ParseResult<Advertiser> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        builder.getClass();
        RegistryXmlParser parseStringAttribute = registryXmlParser.parseStringAttribute("id", new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setId((String) obj);
            }
        }, $$Lambda$a$5zdlx60k33lKy93T2VYkueLhoU0.INSTANCE);
        builder.getClass();
        parseStringAttribute.parseString(new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setName((String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("Advertiser", new Exception("Unable to parse Advertiser name value", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }
}
