package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.CheckedFunction;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.Delivery;
import com.smaato.sdk.video.vast.model.MediaFile;
import com.smaato.sdk.video.vast.model.MediaFile.Builder;
import java.util.ArrayList;
import java.util.List;

public class MediaFileParser implements XmlClassParser<MediaFile> {
    private static final CheckedFunction<String, Delivery> a = $$Lambda$MediaFileParser$WBCby7tnztieVtZM13xzKJfQTw.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(ParseError parseError) {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(ParseError parseError) {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Delivery a(String str) throws Exception {
        return (Delivery) Objects.requireNonNull(Delivery.parse(str));
    }

    @NonNull
    public ParseResult<MediaFile> parse(@NonNull RegistryXmlParser registryXmlParser) {
        MediaFile mediaFile;
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        builder.getClass();
        $$Lambda$unsKlz8Z4fkgFwrVNHo3qsoiSHA r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setId((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$ZAsY1k7c4x3La6_cZjNUXSGHUCU r32 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setType((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$3frXeIoewdLY61q0ohGd5uRaOM0 r33 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setWidth((Float) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$1vDea_KCz1y3uM3dWO1S9t_63Lw r34 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setHeight((Float) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$SHpG9dpemQ8avvDrmud__v8dzHA r35 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setCodec((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$z8od1j9ilRznrRPKmVJ61IYNpg0 r36 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setBitrate((Integer) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$ce_K628IBZMmfb2GQsxQMaw458s r37 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setMinBitrate((Integer) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$l2x__AX6OStWdZtAG_J0fRiwGZo r38 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setMaxBitrate((Integer) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$rsEIbzJoFmPqbMgdGNpVxbHNPOk r39 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setScalable((Boolean) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$Eg1FQLQ0rcIHqXxkb2E1JU0KVdg r310 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setMaintainAspectRatio((Boolean) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$TVRicI9np0ytVMmXnPBaT_WU r311 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setApiFramework((String) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseStringAttribute = registryXmlParser.parseStringAttribute("id", r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("type", r32, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("width", r33, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("height", r34, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("codec", r35, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseIntegerAttribute("bitrate", r36, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseIntegerAttribute("minBitrate", r37, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseIntegerAttribute("maxBitrate", r38, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseBooleanAttribute("scalable", r39, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseBooleanAttribute("maintainAspectRatio", r310, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("apiFramework", r311, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String str = MediaFile.FILE_SIZE;
        builder.getClass();
        builder.getClass();
        CheckedFunction<String, Delivery> checkedFunction = a;
        builder.getClass();
        $$Lambda$scbo28Nq1BgfZgInm7ZdkLqYeqE r4 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setDelivery((Delivery) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseTypedAttribute = parseStringAttribute.parseIntegerAttribute(str, new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setFileSize((Integer) obj);
            }
        }, $$Lambda$MediaFileParser$s5GE4hNayhAyKD_fWboM4pZFrE.INSTANCE).parseStringAttribute("mediaType", new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setMediaType((String) obj);
            }
        }, $$Lambda$MediaFileParser$VbRvzpJwTcyl_ZxPk8ajhzMkH00.INSTANCE).parseTypedAttribute("delivery", checkedFunction, r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        builder.getClass();
        parseTypedAttribute.parseString(new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setUrl((String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("url", new Exception("Unable to parse URL value", (Exception) obj)));
            }
        });
        try {
            mediaFile = builder.build();
        } catch (VastElementMissingException e) {
            arrayList.add(ParseError.buildFrom("MediaFile", e));
            mediaFile = null;
        }
        return new ParseResult.Builder().setResult(mediaFile).setErrors(arrayList).build();
    }
}
