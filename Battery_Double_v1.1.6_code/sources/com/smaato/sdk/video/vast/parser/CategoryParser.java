package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.Category;
import com.smaato.sdk.video.vast.model.Category.Builder;
import java.util.ArrayList;
import java.util.List;

public class CategoryParser implements XmlClassParser<Category> {
    @NonNull
    public ParseResult<Category> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Category category;
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        String str = Category.AUTHORITY;
        builder.getClass();
        $$Lambda$aju9r0m1Fd8WC7bsvmEjLKLOvtE r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setAuthority((String) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseStringAttribute = registryXmlParser.parseStringAttribute(str, r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        builder.getClass();
        parseStringAttribute.parseString(new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setCategoryCode((String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("Category", new Exception("Unable to parse Category code value", (Exception) obj)));
            }
        });
        try {
            category = builder.build();
        } catch (VastElementMissingException e) {
            arrayList.add(ParseError.buildFrom("Category", e));
            category = null;
        }
        return new ParseResult.Builder().setResult(category).setErrors(arrayList).build();
    }
}
