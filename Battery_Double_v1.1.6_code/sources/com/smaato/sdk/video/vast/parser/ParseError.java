package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import java.util.Collections;
import java.util.List;

public class ParseError {
    @NonNull
    public final String enclosingName;
    @Nullable
    public final Exception exception;
    @Nullable
    public final List<ParseError> nestedErrors;

    public static class Builder {
        @Nullable
        private String a;
        @Nullable
        private Exception b;
        @Nullable
        private List<ParseError> c;

        @NonNull
        public Builder setEnclosingName(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        public Builder setException(@Nullable Exception exc) {
            this.b = exc;
            return this;
        }

        @NonNull
        public Builder setNestedErrors(@Nullable List<ParseError> list) {
            this.c = list;
            return this;
        }

        @NonNull
        public ParseError build() {
            Objects.requireNonNull(this.a);
            return new ParseError(this.a, this.b, this.c == null ? null : Collections.unmodifiableList(this.c));
        }
    }

    public ParseError(@NonNull String str, @Nullable Exception exc, @Nullable List<ParseError> list) {
        this.enclosingName = str;
        this.exception = exc;
        this.nestedErrors = list;
    }

    @NonNull
    public static ParseError buildFrom(@NonNull String str, @Nullable Exception exc) {
        return new Builder().setEnclosingName(str).setException(exc).build();
    }
}
