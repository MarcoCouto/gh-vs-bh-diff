package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.AdParameters;
import com.smaato.sdk.video.vast.model.AdParameters.Builder;
import java.util.ArrayList;
import java.util.List;

public class AdParametersParser implements XmlClassParser<AdParameters> {
    @NonNull
    public ParseResult<AdParameters> parse(@NonNull RegistryXmlParser registryXmlParser) {
        AdParameters adParameters;
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        String str = AdParameters.XML_ENCODED;
        builder.getClass();
        $$Lambda$GMQXzbr4LDT9P5SvZ2mQWFWj0y0 r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setXmlEncoded((Boolean) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseBooleanAttribute = registryXmlParser.parseBooleanAttribute(str, r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        builder.getClass();
        parseBooleanAttribute.parseString(new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setParameters((String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("AdParameters", new Exception("Unable to parse AdParameters body", (Exception) obj)));
            }
        });
        try {
            adParameters = builder.build();
        } catch (VastElementMissingException e) {
            arrayList.add(ParseError.buildFrom("AdParameters", e));
            adParameters = null;
        }
        return new ParseResult.Builder().setResult(adParameters).setErrors(arrayList).build();
    }
}
