package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.model.VastTree.Builder;
import java.util.ArrayList;
import java.util.List;

public class VastTreeParser implements XmlClassParser<VastTree> {
    private static final String[] a = {"Ad", "Error"};

    @NonNull
    public ParseResult<VastTree> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        builder.setAds(arrayList2);
        ArrayList arrayList3 = new ArrayList();
        builder.setErrors(arrayList3);
        builder.getClass();
        $$Lambda$7gSlXvjxKRsofVzXkiN88eEhHmU r5 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setVersion((String) obj);
            }
        };
        arrayList.getClass();
        registryXmlParser.parseStringAttribute("version", r5, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseTags(a, new Consumer(arrayList3, arrayList, arrayList2) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                VastTreeParser.a(RegistryXmlParser.this, this.f$1, this.f$2, this.f$3, (String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("VAST", new Exception("Unable to parse tags in Vast", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, List list, List list2, List list3, String str) {
        if (str.equalsIgnoreCase("Error")) {
            registryXmlParser.parseString(new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    VastTreeParser.a(this.f$0, (String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("Error", new Exception("Unable to parse Error in Vast", (Exception) obj)));
                }
            });
            return;
        }
        if (str.equalsIgnoreCase("Ad")) {
            registryXmlParser.parseClass("Ad", new NonNullConsumer(list3, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    VastTreeParser.a(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, String str) {
        list.getClass();
        Objects.onNotNull(str, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((String) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        list.add(parseResult.value);
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
