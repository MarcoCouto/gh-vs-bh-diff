package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.ViewableImpression;
import com.smaato.sdk.video.vast.model.ViewableImpression.Builder;
import java.util.ArrayList;
import java.util.List;

public class ViewableImpressionParser implements XmlClassParser<ViewableImpression> {
    private static final String[] a = {ViewableImpression.VIEWABLE, ViewableImpression.NOT_VIEWABLE, ViewableImpression.VIEW_UNDETERMINED};

    @NonNull
    public ParseResult<ViewableImpression> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        builder.setViewable(arrayList);
        builder.setNotViewable(arrayList2);
        builder.setViewUndetermined(arrayList3);
        builder.getClass();
        $$Lambda$fJuddM24Nm3tky2jE0a8_ydZtwE r2 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setId((String) obj);
            }
        };
        arrayList4.getClass();
        RegistryXmlParser parseStringAttribute = registryXmlParser.parseStringAttribute("id", r2, new Consumer(arrayList4) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String[] strArr = a;
        $$Lambda$ViewableImpressionParser$4lspEQs4JFX1wSeZTuKQKU6qOo r1 = new Consumer(arrayList, arrayList4, arrayList2, arrayList3) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void accept(Object obj) {
                ViewableImpressionParser.a(RegistryXmlParser.this, this.f$1, this.f$2, this.f$3, this.f$4, (String) obj);
            }
        };
        parseStringAttribute.parseTags(strArr, r1, new Consumer(arrayList4) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("ViewableImpression", new Exception("Unable to parse tags in ViewableImpression")));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList4).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, List list, List list2, List list3, List list4, String str) {
        if (ViewableImpression.VIEWABLE.equalsIgnoreCase(str)) {
            list.getClass();
            registryXmlParser.parseString(new Consumer(list) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(ViewableImpression.VIEWABLE, new Exception("Unable to parse ViewableImpression value", (Exception) obj)));
                }
            });
        } else if (ViewableImpression.NOT_VIEWABLE.equalsIgnoreCase(str)) {
            list3.getClass();
            registryXmlParser.parseString(new Consumer(list3) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(ViewableImpression.NOT_VIEWABLE, new Exception("Unable to parse NotViewableImpression value", (Exception) obj)));
                }
            });
        } else {
            if (ViewableImpression.VIEW_UNDETERMINED.equalsIgnoreCase(str)) {
                list4.getClass();
                registryXmlParser.parseString(new Consumer(list4) {
                    private final /* synthetic */ List f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        this.f$0.add((String) obj);
                    }
                }, new Consumer(list2) {
                    private final /* synthetic */ List f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void accept(Object obj) {
                        this.f$0.add(ParseError.buildFrom(ViewableImpression.VIEW_UNDETERMINED, new Exception("Unable to parse ViewUndetermined value", (Exception) obj)));
                    }
                });
            }
        }
    }
}
