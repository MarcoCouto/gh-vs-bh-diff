package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.CheckedFunction;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.Tracking;
import com.smaato.sdk.video.vast.model.Tracking.Builder;
import com.smaato.sdk.video.vast.model.VastEvent;
import java.util.ArrayList;
import java.util.List;

public class TrackingParser implements XmlClassParser<Tracking> {
    private static final CheckedFunction<String, VastEvent> a = $$Lambda$TrackingParser$tbRi0XLT1e2FDbGSnvstU59ognY.INSTANCE;

    /* access modifiers changed from: private */
    public static /* synthetic */ VastEvent a(String str) throws Exception {
        return (VastEvent) Objects.requireNonNull(VastEvent.parse(str));
    }

    @NonNull
    public ParseResult<Tracking> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Tracking tracking;
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        CheckedFunction<String, VastEvent> checkedFunction = a;
        builder.getClass();
        $$Lambda$poGZR8o9RM16tmY_HdfjTgPxACE r4 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setVastEvent((VastEvent) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$ZAtcYw9JDBMSDAMTzqFcLbPL3U r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setOffset((String) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseStringAttribute = registryXmlParser.parseTypedAttribute("event", checkedFunction, r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("offset", r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        builder.getClass();
        parseStringAttribute.parseString(new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setUrl((String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("url", new Exception("Unable to parse URL value", (Exception) obj)));
            }
        });
        try {
            tracking = builder.build();
        } catch (VastElementMissingException e) {
            arrayList.add(ParseError.buildFrom("Tracking", e));
            tracking = null;
        }
        return new ParseResult.Builder().setResult(tracking).setErrors(arrayList).build();
    }
}
