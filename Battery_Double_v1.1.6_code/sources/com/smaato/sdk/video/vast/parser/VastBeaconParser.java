package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.VastBeacon;
import com.smaato.sdk.video.vast.model.VastBeacon.Builder;
import java.util.ArrayList;
import java.util.List;

public class VastBeaconParser implements XmlClassParser<VastBeacon> {
    @NonNull
    private final String a;

    public VastBeaconParser(@NonNull String str) {
        this.a = str;
    }

    @NonNull
    public ParseResult<VastBeacon> parse(@NonNull RegistryXmlParser registryXmlParser) {
        VastBeacon vastBeacon;
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        builder.getClass();
        $$Lambda$uM8QrVJlz49MCn0YwYZIcsBSg1s r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setId((String) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseStringAttribute = registryXmlParser.parseStringAttribute("id", r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        builder.getClass();
        parseStringAttribute.parseString(new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setUri((String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                VastBeaconParser.this.a(this.f$1, (Exception) obj);
            }
        });
        try {
            vastBeacon = builder.build();
        } catch (VastElementMissingException e) {
            arrayList.add(ParseError.buildFrom(this.a, e));
            vastBeacon = null;
        }
        return new ParseResult.Builder().setResult(vastBeacon).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(List list, Exception exc) {
        list.add(ParseError.buildFrom(this.a, new Exception("Unable to parse UniversalAdId value", exc)));
    }
}
