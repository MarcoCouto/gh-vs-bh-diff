package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.Ad;
import com.smaato.sdk.video.vast.model.Ad.Builder;
import com.smaato.sdk.video.vast.model.InLine;
import com.smaato.sdk.video.vast.model.Wrapper;
import java.util.ArrayList;
import java.util.List;

public class AdParser implements XmlClassParser<Ad> {
    private static final String[] a = {"InLine", "Wrapper"};

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(ParseError parseError) {
    }

    @NonNull
    public ParseResult<Ad> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        builder.getClass();
        $$Lambda$9qZmBe39t4N5fXyKLg58FAaBnEE r3 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setId((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$0MhdXTYCtk3yW2Kfvap06hQjYHM r4 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setSequence((Integer) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseIntegerAttribute = registryXmlParser.parseStringAttribute("id", r3, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseIntegerAttribute("sequence", r4, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String str = Ad.CONDITIONAL_AD;
        builder.getClass();
        $$Lambda$mJVVUdb6M4DFAOLcbEmRSPwJ10g r42 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setConditionalAd((Boolean) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseBooleanAttribute = parseIntegerAttribute.parseBooleanAttribute(str, r42, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String str2 = Ad.AD_TYPE;
        builder.getClass();
        parseBooleanAttribute.parseStringAttribute(str2, new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setAdType((String) obj);
            }
        }, $$Lambda$AdParser$tATY5vdEgE2rZMCpDcteVyLriuA.INSTANCE).parseTags(a, new Consumer(builder, arrayList) {
            private final /* synthetic */ Builder f$1;
            private final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                AdParser.a(RegistryXmlParser.this, this.f$1, this.f$2, (String) obj);
            }
        }, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("Ad", new Exception("Unable to parse tags in Ad", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, Builder builder, List list, String str) {
        if (str.equalsIgnoreCase("InLine")) {
            registryXmlParser.parseClass("InLine", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AdParser.b(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
            return;
        }
        if (str.equalsIgnoreCase("Wrapper")) {
            registryXmlParser.parseClass("Wrapper", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    AdParser.a(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(Builder builder, List list, ParseResult parseResult) {
        builder.setInLine((InLine) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Builder builder, List list, ParseResult parseResult) {
        builder.setWrapper((Wrapper) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
