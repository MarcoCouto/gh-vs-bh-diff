package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.VastBeacon;
import com.smaato.sdk.video.vast.model.VideoClicks;
import com.smaato.sdk.video.vast.model.VideoClicks.Builder;
import java.util.ArrayList;
import java.util.List;

public class VideoClicksParser implements XmlClassParser<VideoClicks> {
    private static final String[] a = {"ClickThrough", "ClickTracking", "CustomClick"};

    @NonNull
    public ParseResult<VideoClicks> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        builder.setClickTrackings(arrayList2);
        ArrayList arrayList3 = new ArrayList();
        builder.setCustomClicks(arrayList3);
        String[] strArr = a;
        $$Lambda$VideoClicksParser$lsItFGKB6xLKqK2IPSkBtW4WFwA r0 = new Consumer(builder, arrayList, arrayList2, arrayList3) {
            private final /* synthetic */ Builder f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void accept(Object obj) {
                VideoClicksParser.a(RegistryXmlParser.this, this.f$1, this.f$2, this.f$3, this.f$4, (String) obj);
            }
        };
        registryXmlParser.parseTags(strArr, r0, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("VideoClicks", new Exception("Unable to parse tags in CompanionAds", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, Builder builder, List list, List list2, List list3, String str) {
        if (str.equalsIgnoreCase("ClickThrough")) {
            registryXmlParser.parseClass("ClickThrough", new NonNullConsumer(list) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    VideoClicksParser.a(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if (str.equalsIgnoreCase("ClickTracking")) {
            registryXmlParser.parseClass("ClickTracking", new NonNullConsumer(list2, list) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    VideoClicksParser.b(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        } else {
            if (str.equalsIgnoreCase("CustomClick")) {
                registryXmlParser.parseClass("CustomClick", new NonNullConsumer(list3, list) {
                    private final /* synthetic */ List f$0;
                    private final /* synthetic */ List f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        VideoClicksParser.a(this.f$0, this.f$1, (ParseResult) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Builder builder, List list, ParseResult parseResult) {
        builder.setClickThrough((VastBeacon) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(List list, List list2, ParseResult parseResult) {
        Result result = parseResult.value;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((VastBeacon) obj);
            }
        });
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        Result result = parseResult.value;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((VastBeacon) obj);
            }
        });
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }
}
