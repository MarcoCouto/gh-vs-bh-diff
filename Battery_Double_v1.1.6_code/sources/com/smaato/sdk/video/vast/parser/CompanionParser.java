package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.AdParameters;
import com.smaato.sdk.video.vast.model.Companion;
import com.smaato.sdk.video.vast.model.Companion.Builder;
import com.smaato.sdk.video.vast.model.StaticResource;
import com.smaato.sdk.video.vast.model.VastBeacon;
import java.util.ArrayList;
import java.util.List;

public class CompanionParser implements XmlClassParser<Companion> {
    private static final String[] a = {"StaticResource", "IFrameResource", "HTMLResource", Companion.ALT_TEXT, "CompanionClickThrough", "CompanionClickTracking", "TrackingEvents", "AdParameters"};

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(ParseError parseError) {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(RegistryXmlParser registryXmlParser, List list, List list2, List list3, List list4, Builder builder, List list5, String str) {
        if ("StaticResource".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("StaticResource", new NonNullConsumer(list, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    CompanionParser.b(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("IFrameResource".equalsIgnoreCase(str)) {
            list3.getClass();
            registryXmlParser.parseString(new Consumer(list3) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("IFrameResource", new Exception("Unable to parse IFrameResource", (Exception) obj)));
                }
            });
        } else if ("HTMLResource".equalsIgnoreCase(str)) {
            list4.getClass();
            registryXmlParser.parseString(new Consumer(list4) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("HTMLResource", new Exception("Unable to parse HtmlResource", (Exception) obj)));
                }
            });
        } else if (Companion.ALT_TEXT.equalsIgnoreCase(str)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setAltText((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom(Companion.ALT_TEXT, new Exception("Unable to parse AltText", (Exception) obj)));
                }
            });
        } else if ("AdParameters".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("AdParameters", new NonNullConsumer(list2) {
                private final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    CompanionParser.a(Builder.this, this.f$1, (ParseResult) obj);
                }
            });
        } else if ("CompanionClickThrough".equalsIgnoreCase(str)) {
            builder.getClass();
            registryXmlParser.parseString(new Consumer() {
                public final void accept(Object obj) {
                    Builder.this.setCompanionClickThrough((String) obj);
                }
            }, new Consumer(list2) {
                private final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                public final void accept(Object obj) {
                    this.f$0.add(ParseError.buildFrom("CompanionClickThrough", new Exception("Unable to parse CompanionClickThrough", (Exception) obj)));
                }
            });
        } else if ("CompanionClickTracking".equalsIgnoreCase(str)) {
            registryXmlParser.parseClass("CompanionClickTracking", new NonNullConsumer(list5, list2) {
                private final /* synthetic */ List f$0;
                private final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    CompanionParser.a(this.f$0, this.f$1, (ParseResult) obj);
                }
            });
        } else {
            if ("TrackingEvents".equalsIgnoreCase(str)) {
                registryXmlParser.parseClass("TrackingEvents", new NonNullConsumer(list2) {
                    private final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void accept(Object obj) {
                        CompanionParser.b(Builder.this, this.f$1, (ParseResult) obj);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(List list, List list2, ParseResult parseResult) {
        Result result = parseResult.value;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((StaticResource) obj);
            }
        });
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void b(Builder builder, List list, ParseResult parseResult) {
        Result result = parseResult.value;
        builder.getClass();
        Objects.onNotNull(result, new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setTrackingEvents((List) obj);
            }
        });
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        Result result = parseResult.value;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((VastBeacon) obj);
            }
        });
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Builder builder, List list, ParseResult parseResult) {
        builder.setAdParameters((AdParameters) parseResult.value);
        List<ParseError> list2 = parseResult.errors;
        list.getClass();
        Objects.onNotNull(list2, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    @NonNull
    public ParseResult<Companion> parse(@NonNull RegistryXmlParser registryXmlParser) {
        Builder builder = new Builder();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        builder.setTrackingEvents(arrayList2);
        builder.setCompanionClickTrackings(arrayList3);
        builder.getClass();
        $$Lambda$0bqD4vE97fmlyD9aGLPSVjMZkc r1 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setId((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$cM8VjUhnn2f8NBGBx8leKUtNM r2 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setWidth((Float) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$ZtLXwswPwv38PmvRBU04Ati_S_s r22 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setHeight((Float) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$uueFx5UvWAe3aTPAGMCXwYd188 r23 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setAssetWidth((Float) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$0vpqm4Sm7U3qpcRNBwys1rir1GM r24 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setAssetHeight((Float) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$3stO5lIoH2q55yvu29O_Y09O83Y r25 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setExpandedWidth((Float) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$gUHPt2xIDPeQ56Dc0RfEUJXkMw r26 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setExpandedHeight((Float) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$uRUrS2NLRcvquzD_plaWBybqNbc r27 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setApiFramework((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$jYqVO29_cdzAHocviVNCkKlXQU r28 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setAdSlotID((String) obj);
            }
        };
        arrayList.getClass();
        builder.getClass();
        $$Lambda$odwJlie7KRIW0811CexQqUrGlco r29 = new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setPxRatio((Float) obj);
            }
        };
        arrayList.getClass();
        RegistryXmlParser parseFloatAttribute = registryXmlParser.parseStringAttribute("id", r1, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("width", r2, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("height", r22, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("assetWidth", r23, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("assetHeight", r24, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("expandedWidth", r25, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("expandedHeight", r26, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("apiFramework", r27, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseStringAttribute("adSlotID", r28, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        }).parseFloatAttribute("pxratio", r29, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add((ParseError) obj);
            }
        });
        String str = Companion.RENDERING_MODE;
        builder.getClass();
        parseFloatAttribute.parseStringAttribute(str, new Consumer() {
            public final void accept(Object obj) {
                Builder.this.setRenderingMode((String) obj);
            }
        }, $$Lambda$CompanionParser$4hBs9KA1WeuCQ__x9SOODJalA.INSTANCE);
        String[] strArr = a;
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        ArrayList arrayList7 = new ArrayList();
        builder.setCompanionClickTrackings(arrayList4);
        builder.setStaticResources(arrayList5);
        builder.setIFrameResources(arrayList6);
        builder.setHtmlResources(arrayList7);
        $$Lambda$CompanionParser$tjusmpdZis9vrAI0ax4Z1E0PdM r0 = new Consumer(arrayList5, arrayList, arrayList6, arrayList7, builder, arrayList4) {
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;
            private final /* synthetic */ List f$4;
            private final /* synthetic */ Builder f$5;
            private final /* synthetic */ List f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            public final void accept(Object obj) {
                CompanionParser.a(RegistryXmlParser.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (String) obj);
            }
        };
        registryXmlParser.parseTags(strArr, r0, new Consumer(arrayList) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(ParseError.buildFrom("Companion", new Exception("Unable to parse tags in Companion", (Exception) obj)));
            }
        });
        return new ParseResult.Builder().setResult(builder.build()).setErrors(arrayList).build();
    }
}
