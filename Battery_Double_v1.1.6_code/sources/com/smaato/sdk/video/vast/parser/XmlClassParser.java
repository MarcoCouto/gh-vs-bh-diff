package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;

public interface XmlClassParser<Result> {
    @NonNull
    ParseResult<Result> parse(@NonNull RegistryXmlParser registryXmlParser);
}
