package com.smaato.sdk.video.vast.parser;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.parser.ParseResult.Builder;
import java.util.ArrayList;
import java.util.List;

public class ArrayXmlClassParser<Result> implements XmlClassParser<List<Result>> {
    @NonNull
    private final String a;
    @NonNull
    private final String b;

    public ArrayXmlClassParser(@NonNull String str, @NonNull String str2) {
        this.a = str;
        this.b = str2;
    }

    @NonNull
    public ParseResult<List<Result>> parse(@NonNull RegistryXmlParser registryXmlParser) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        registryXmlParser.parseTags(new String[]{this.b}, new Consumer(registryXmlParser, arrayList, arrayList2) {
            private final /* synthetic */ RegistryXmlParser f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ List f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                ArrayXmlClassParser.this.a(this.f$1, this.f$2, this.f$3, (String) obj);
            }
        }, new Consumer(arrayList2) {
            private final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ArrayXmlClassParser.this.a(this.f$1, (Exception) obj);
            }
        });
        return new Builder().setResult(arrayList).setErrors(arrayList2).build();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(RegistryXmlParser registryXmlParser, List list, List list2, String str) {
        registryXmlParser.parseClass(this.b, new NonNullConsumer(list, list2) {
            private final /* synthetic */ List f$0;
            private final /* synthetic */ List f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                ArrayXmlClassParser.a(this.f$0, this.f$1, (ParseResult) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(List list, List list2, ParseResult parseResult) {
        Result result = parseResult.value;
        list.getClass();
        Objects.onNotNull(result, new Consumer(list) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.add(obj);
            }
        });
        List<ParseError> list3 = parseResult.errors;
        list2.getClass();
        Objects.onNotNull(list3, new Consumer(list2) {
            private final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            public final void accept(Object obj) {
                this.f$0.addAll((List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(List list, Exception exc) {
        String str = this.a;
        StringBuilder sb = new StringBuilder("Unable to parse ");
        sb.append(this.b);
        sb.append(" elements in ");
        sb.append(this.a);
        list.add(ParseError.buildFrom(str, new Exception(sb.toString(), exc)));
    }
}
