package com.smaato.sdk.video.vast.exceptions.wrapper;

import androidx.annotation.NonNull;

public final class WrapperRequestTimeoutException extends Exception {
    public WrapperRequestTimeoutException(@NonNull String str) {
        super(str);
    }
}
