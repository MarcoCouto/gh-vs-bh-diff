package com.smaato.sdk.video.vast.exceptions.wrapper;

import androidx.annotation.NonNull;

public final class GeneralWrapperErrorException extends Exception {
    public GeneralWrapperErrorException(@NonNull String str) {
        super(str);
    }
}
