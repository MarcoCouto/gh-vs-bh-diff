package com.smaato.sdk.video.vast.exceptions;

import androidx.annotation.Nullable;

public class VastElementMissingException extends Exception {
    public VastElementMissingException(@Nullable String str) {
        super(str);
    }
}
