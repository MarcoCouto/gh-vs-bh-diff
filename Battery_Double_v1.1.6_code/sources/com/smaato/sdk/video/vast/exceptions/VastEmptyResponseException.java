package com.smaato.sdk.video.vast.exceptions;

import androidx.annotation.Nullable;

public class VastEmptyResponseException extends Exception {
    public VastEmptyResponseException(@Nullable String str) {
        super(str);
    }
}
