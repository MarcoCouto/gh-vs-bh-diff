package com.smaato.sdk.video.vast.tracking;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.tracking.macro.MacroInjector;
import com.smaato.sdk.video.vast.tracking.macro.PlayerState;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class VastErrorTracker {
    @NonNull
    private final Logger a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final Set<String> c;
    @NonNull
    private final SomaApiContext d;
    @NonNull
    private final MacroInjector e;

    public VastErrorTracker(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull SomaApiContext somaApiContext, @NonNull MacroInjector macroInjector, @NonNull Collection<String> collection) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.d = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.e = (MacroInjector) Objects.requireNonNull(macroInjector);
        this.c = new HashSet((Collection) Objects.requireNonNull(collection));
    }

    public void track(@NonNull PlayerState playerState) {
        if (this.c.isEmpty()) {
            this.a.info(LogDomain.VAST, "Wanted to track VastError [%d], but no beacon URLs available", playerState.errorCode);
            return;
        }
        this.a.info(LogDomain.VAST, "Tracking VastError [%d]", playerState.errorCode);
        for (String str : this.c) {
            if (!TextUtils.isEmpty(str)) {
                this.b.trackBeaconUrl(this.e.injectMacros(str, playerState), this.d);
            }
        }
    }
}
