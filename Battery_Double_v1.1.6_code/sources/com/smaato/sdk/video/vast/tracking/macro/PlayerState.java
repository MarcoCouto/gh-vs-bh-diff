package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class PlayerState {
    @Nullable
    public final Float clickPositionX;
    @Nullable
    public final Float clickPositionY;
    @Nullable
    public final Integer errorCode;
    @Nullable
    public final Boolean isMuted;
    @Nullable
    public final Long offsetMillis;

    public static class Builder {
        @Nullable
        private Float a;
        @Nullable
        private Float b;
        @Nullable
        private Boolean c;
        @Nullable
        private Long d;
        @Nullable
        private Integer e;

        @NonNull
        public Builder setClickPositionX(float f) {
            this.a = Float.valueOf(f);
            return this;
        }

        @NonNull
        public Builder setClickPositionY(float f) {
            this.b = Float.valueOf(f);
            return this;
        }

        @NonNull
        public Builder setMuted(boolean z) {
            this.c = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public Builder setOffsetMillis(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @NonNull
        public Builder setErrorCode(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @NonNull
        public PlayerState build() {
            PlayerState playerState = new PlayerState(this.a, this.b, this.c, this.d, this.e, 0);
            return playerState;
        }
    }

    /* synthetic */ PlayerState(Float f, Float f2, Boolean bool, Long l, Integer num, byte b) {
        this(f, f2, bool, l, num);
    }

    private PlayerState(@Nullable Float f, @Nullable Float f2, @Nullable Boolean bool, @Nullable Long l, @Nullable Integer num) {
        this.clickPositionX = f;
        this.clickPositionY = f2;
        this.isMuted = bool;
        this.offsetMillis = l;
        this.errorCode = num;
    }
}
