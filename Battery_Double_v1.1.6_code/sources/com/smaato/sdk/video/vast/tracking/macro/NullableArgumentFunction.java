package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface NullableArgumentFunction<T, R> {
    @NonNull
    R apply(@Nullable T t);
}
