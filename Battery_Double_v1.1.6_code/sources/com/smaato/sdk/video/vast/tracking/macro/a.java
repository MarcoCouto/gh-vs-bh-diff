package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.video.utils.DateFormatUtils;
import com.smaato.sdk.video.vast.model.UniversalAdId;
import com.smaato.sdk.video.vast.model.VastScenario;
import java.util.Map;
import java.util.Map.Entry;

class a {
    @NonNull
    private final DateFormatUtils a;
    @Nullable
    private final VastScenario b;
    @Nullable
    private final UniversalAdId c;

    a(@NonNull DateFormatUtils dateFormatUtils, @Nullable VastScenario vastScenario, @Nullable UniversalAdId universalAdId) {
        this.a = (DateFormatUtils) Objects.requireNonNull(dateFormatUtils);
        this.b = vastScenario;
        this.c = universalAdId;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> a(@NonNull PlayerState playerState) {
        String str;
        String str2;
        String str3;
        Long l = playerState.offsetMillis;
        if (l == null) {
            str = "-2";
        } else {
            str = this.a.offsetFromTimeInterval(l.longValue());
        }
        Entry[] entryArr = new Entry[15];
        entryArr[0] = Maps.entryOf("[CONTENTPLAYHEAD]", str);
        entryArr[1] = Maps.entryOf("[MEDIAPLAYHEAD]", str);
        entryArr[2] = Maps.entryOf("[BREAKPOSITION]", "4");
        String str4 = "[BLOCKEDADCATEGORIES]";
        if (this.b == null) {
            str2 = "-2";
        } else {
            str2 = Joiner.join((CharSequence) ",", (Iterable) this.b.blockedAdCategories);
        }
        entryArr[3] = Maps.entryOf(str4, str2);
        entryArr[4] = Maps.entryOf("[ADCATEGORIES]", "-1");
        entryArr[5] = Maps.entryOf("[ADCOUNT]", "1");
        entryArr[6] = Maps.entryOf("[TRANSACTIONID]", "-1");
        entryArr[7] = Maps.entryOf("[PLACEMENTTYPE]", "5");
        entryArr[8] = Maps.entryOf("[ADTYPE]", "video");
        String str5 = "[UNIVERSALADID]";
        if (this.c == null) {
            str3 = "-2";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(this.c.idRegistry);
            sb.append(" ");
            sb.append(this.c.idValue);
            str3 = sb.toString();
        }
        entryArr[9] = Maps.entryOf(str5, str3);
        entryArr[10] = Maps.entryOf("[BREAKMAXDURATION]", "60");
        entryArr[11] = Maps.entryOf("[BREAKMINDURATION]", "1");
        entryArr[12] = Maps.entryOf("[BREAKMAXADS]", "1");
        entryArr[13] = Maps.entryOf("[BREAKMINADLENGTH]", "1");
        entryArr[14] = Maps.entryOf("[BREAKMAXADLENGTH]", "60");
        return Maps.mapOf(entryArr);
    }
}
