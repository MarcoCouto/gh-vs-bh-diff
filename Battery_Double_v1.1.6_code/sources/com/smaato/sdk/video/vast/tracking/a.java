package com.smaato.sdk.video.vast.tracking;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.video.vast.model.Tracking;
import com.smaato.sdk.video.vast.model.VastEvent;
import com.smaato.sdk.video.vast.utils.VastVideoPlayerTimeConverterUtils;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class a {
    @NonNull
    private final SparseArray<Set<Tracking>> a;
    @NonNull
    private final Set<Tracking> b = Collections.synchronizedSet(new HashSet());

    private a(@NonNull SparseArray<Set<Tracking>> sparseArray) {
        this.a = sparseArray;
    }

    @NonNull
    static a a(@NonNull Map<VastEvent, List<Tracking>> map, long j, @NonNull Logger logger) {
        SparseArray sparseArray = new SparseArray();
        for (VastEvent vastEvent : VastEvent.EVENTS_WITH_OFFSET) {
            Objects.onNotNull(map.get(vastEvent), new Consumer(j, logger, sparseArray) {
                private final /* synthetic */ long f$0;
                private final /* synthetic */ Logger f$1;
                private final /* synthetic */ SparseArray f$2;

                {
                    this.f$0 = r1;
                    this.f$1 = r3;
                    this.f$2 = r4;
                }

                public final void accept(Object obj) {
                    a.a(this.f$0, this.f$1, this.f$2, (List) obj);
                }
            });
        }
        return new a(sparseArray);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.smaato.sdk.video.vast.model.Tracking>, for r7v0, types: [java.util.List, java.util.List<com.smaato.sdk.video.vast.model.Tracking>] */
    public static /* synthetic */ void a(long j, Logger logger, SparseArray sparseArray, List<Tracking> list) {
        for (Tracking tracking : list) {
            int convertOffsetStringToPercentage = VastVideoPlayerTimeConverterUtils.convertOffsetStringToPercentage(tracking.offset, j, logger);
            if (convertOffsetStringToPercentage >= 0) {
                Set set = (Set) sparseArray.get(convertOffsetStringToPercentage);
                if (set != null) {
                    set.add(tracking);
                } else {
                    HashSet hashSet = new HashSet();
                    hashSet.add(tracking);
                    sparseArray.append(convertOffsetStringToPercentage, hashSet);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Set<Tracking> a(long j, long j2) {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < this.a.size(); i++) {
            if (((long) this.a.keyAt(i)) <= (100 * j) / j2) {
                hashSet.addAll(Sets.retainToSet((Collection) this.a.valueAt(i), new Predicate() {
                    public final boolean test(Object obj) {
                        return a.this.b((Tracking) obj);
                    }
                }));
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean b(Tracking tracking) {
        return !this.b.contains(tracking);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Tracking tracking) {
        this.b.add(tracking);
    }
}
