package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.LatLng;
import com.smaato.sdk.core.datacollector.DataCollector;
import com.smaato.sdk.core.datacollector.SystemInfo;
import com.smaato.sdk.core.gdpr.PiiParam;
import com.smaato.sdk.core.gdpr.SomaGdprDataSource;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.collections.Maps;
import java.util.Map;
import java.util.Map.Entry;

final class d {
    @NonNull
    private final DataCollector a;
    @NonNull
    private final SomaGdprDataSource b;

    d(@NonNull DataCollector dataCollector, @NonNull SomaGdprDataSource somaGdprDataSource) {
        this.a = (DataCollector) Objects.requireNonNull(dataCollector);
        this.b = (SomaGdprDataSource) Objects.requireNonNull(somaGdprDataSource);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> a() {
        String str;
        String str2;
        SystemInfo systemInfo = this.a.getSystemInfo();
        Entry[] entryArr = new Entry[8];
        String str3 = "[IFA]";
        if (this.b.getSomaGdprData().isUsageAllowedFor(PiiParam.GOOGLE_AD_ID)) {
            str = systemInfo.getGoogleAdvertisingId();
            if (TextUtils.isEmpty(str)) {
                str = "-2";
            }
        } else {
            str = "-2";
        }
        entryArr[0] = Maps.entryOf(str3, str);
        entryArr[1] = Maps.entryOf("[IFATYPE]", "aaid");
        entryArr[2] = Maps.entryOf("[CLIENTUA]", "unknown");
        entryArr[3] = Maps.entryOf("[SERVERUA]", "-1");
        entryArr[4] = Maps.entryOf("[DEVICEUA]", TextUtils.isEmpty(systemInfo.getUserAgent()) ? "-2" : systemInfo.getUserAgent());
        entryArr[5] = Maps.entryOf("[SERVERSIDE]", "0");
        entryArr[6] = Maps.entryOf("[DEVICEIP]", "-1");
        String str4 = "[LATLONG]";
        LatLng locationData = this.a.getLocationData();
        if (locationData == null) {
            str2 = "-2";
        } else {
            str2 = Joiner.join((CharSequence) ",", Double.valueOf(locationData.getLatitude()), Double.valueOf(locationData.getLongitude()));
        }
        entryArr[7] = Maps.entryOf(str4, str2);
        return Maps.mapOf(entryArr);
    }
}
