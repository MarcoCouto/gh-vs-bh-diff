package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.datacollector.DataCollector;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.collections.Maps;
import java.util.Map;
import java.util.Map.Entry;

final class h {
    @NonNull
    private final DataCollector a;

    h(@NonNull DataCollector dataCollector) {
        this.a = (DataCollector) Objects.requireNonNull(dataCollector);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> a() {
        Entry[] entryArr = new Entry[3];
        entryArr[0] = Maps.entryOf("[DOMAIN]", "-1");
        entryArr[1] = Maps.entryOf("[PAGEURL]", "-1");
        String str = "[APPBUNDLE]";
        String packageName = this.a.getSystemInfo().getPackageName();
        if (TextUtils.isEmpty(packageName)) {
            packageName = "-2";
        }
        entryArr[2] = Maps.entryOf(str, packageName);
        return Maps.mapOf(entryArr);
    }
}
