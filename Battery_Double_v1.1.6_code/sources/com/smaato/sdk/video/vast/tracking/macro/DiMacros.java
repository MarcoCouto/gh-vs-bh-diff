package com.smaato.sdk.video.vast.tracking.macro;

import android.app.Application;
import android.content.Context;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.datacollector.DataCollector;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.gdpr.SomaGdprDataSource;
import com.smaato.sdk.core.util.UIUtils;
import com.smaato.sdk.video.utils.DateFormatUtils;
import com.smaato.sdk.video.utils.RandomUtils;
import com.smaato.sdk.video.utils.UriUtils;
import com.smaato.sdk.video.vast.model.VastScenario;

public final class DiMacros {

    private interface a extends NullableArgumentFunction<VastScenario, a> {
    }

    private interface b extends NullableArgumentFunction<VastScenario, g> {
    }

    private DiMacros() {
    }

    @NonNull
    public static DiRegistry createRegistry() {
        return DiRegistry.of($$Lambda$DiMacros$aESHZZhumLNBdvnqu9G0FCmo0Rs.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(DiRegistry diRegistry) {
        diRegistry.registerFactory(MacrosInjectorProviderFunction.class, $$Lambda$DiMacros$ltCgWyCfuIcp2s8uItDvgOfJgmA.INSTANCE);
        diRegistry.registerFactory(UriUtils.class, $$Lambda$DiMacros$qbiRd3BFvS5VYtrmIjdeLWUNTSE.INSTANCE);
        diRegistry.registerFactory(DateFormatUtils.class, $$Lambda$DiMacros$6Sbtv8HXC_l8kQ7aJB_wLst15jM.INSTANCE);
        diRegistry.registerFactory(RandomUtils.class, $$Lambda$DiMacros$kYlXFyTynwukFU4I57NIEv35A.INSTANCE);
        diRegistry.registerFactory(a.class, $$Lambda$DiMacros$IIzLBqa46o3Mi2NI1p50mCo8pc.INSTANCE);
        diRegistry.registerFactory(b.class, $$Lambda$DiMacros$zUVVpWpJBUk51CvjIPMssAfsK9w.INSTANCE);
        diRegistry.registerFactory(d.class, $$Lambda$DiMacros$0j3GBC88ysvM5JvsOuWmmZxbnGg.INSTANCE);
        diRegistry.registerFactory(f.class, $$Lambda$DiMacros$Eq3D8lZFJWE4nJEFzxgc_gzU23o.INSTANCE);
        diRegistry.registerFactory(b.class, $$Lambda$DiMacros$usDCYso_73b0kAHqNHv7Q9WHA8A.INSTANCE);
        diRegistry.registerFactory(h.class, $$Lambda$DiMacros$akKkl3E4NkSRQ3iPq1NwyO0UBJE.INSTANCE);
        diRegistry.registerFactory(i.class, $$Lambda$DiMacros$aVdfv2VPqIl_Q5I7JeANGriao.INSTANCE);
        diRegistry.registerFactory(j.class, $$Lambda$DiMacros$rjbZ1dakQ0Kly42pWVDe9rl9Wrw.INSTANCE);
        diRegistry.registerFactory(c.class, $$Lambda$DiMacros$_cPD95HtM6JbU0ZbWjMp0t6udUc.INSTANCE);
        diRegistry.registerFactory(e.class, $$Lambda$DiMacros$173LAyI9vDzMKdoIvMC7bd4S7Yo.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ MacroInjector c(DiConstructor diConstructor, VastScenario vastScenario) {
        MacroInjector macroInjector = new MacroInjector((UriUtils) diConstructor.get(UriUtils.class), (a) ((a) diConstructor.get(a.class)).apply(vastScenario), (b) diConstructor.get(b.class), (d) diConstructor.get(d.class), (f) diConstructor.get(f.class), (g) ((b) diConstructor.get(b.class)).apply(vastScenario), (h) diConstructor.get(h.class), (i) diConstructor.get(i.class), (j) diConstructor.get(j.class), (c) diConstructor.get(c.class), (e) diConstructor.get(e.class));
        return macroInjector;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ MacrosInjectorProviderFunction o(DiConstructor diConstructor) {
        return new MacrosInjectorProviderFunction() {
            public final Object apply(Object obj) {
                return DiMacros.c(DiConstructor.this, (VastScenario) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ UriUtils n(DiConstructor diConstructor) {
        return new UriUtils();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ DateFormatUtils m(DiConstructor diConstructor) {
        return new DateFormatUtils();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RandomUtils l(DiConstructor diConstructor) {
        return new RandomUtils();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a b(DiConstructor diConstructor, VastScenario vastScenario) {
        return new a((DateFormatUtils) diConstructor.get(DateFormatUtils.class), vastScenario, vastScenario == null ? null : vastScenario.vastMediaFileScenario.vastScenarioCreativeData.universalAdId);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a k(DiConstructor diConstructor) {
        return new a() {
            public final Object apply(Object obj) {
                return DiMacros.b(DiConstructor.this, (VastScenario) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b j(DiConstructor diConstructor) {
        return new b();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ d i(DiConstructor diConstructor) {
        return new d((DataCollector) diConstructor.get(DataCollector.class), (SomaGdprDataSource) diConstructor.get(SomaGdprDataSource.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ f h(DiConstructor diConstructor) {
        return new f((DateFormatUtils) diConstructor.get(DateFormatUtils.class), (RandomUtils) diConstructor.get(RandomUtils.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ g a(DiConstructor diConstructor, VastScenario vastScenario) {
        $$Lambda$DiMacros$bVm81I9uiCVyAFZYCg9NSruAAA4 r1 = new a() {
            public final Object get() {
                return UIUtils.getDisplaySizeInDp((Context) DiConstructor.this.get(Application.class));
            }
        };
        DateFormatUtils dateFormatUtils = (DateFormatUtils) diConstructor.get(DateFormatUtils.class);
        String str = null;
        String str2 = vastScenario == null ? null : vastScenario.vastMediaFileScenario.mediaFile.url;
        if (vastScenario != null) {
            str = vastScenario.adServingId;
        }
        return new g(r1, dateFormatUtils, str2, str);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b f(DiConstructor diConstructor) {
        return new b() {
            public final Object apply(Object obj) {
                return DiMacros.a(DiConstructor.this, (VastScenario) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ h e(DiConstructor diConstructor) {
        return new h((DataCollector) diConstructor.get(DataCollector.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ i d(DiConstructor diConstructor) {
        return new i((SomaGdprDataSource) diConstructor.get(SomaGdprDataSource.class), (DataCollector) diConstructor.get(DataCollector.class), $$Lambda$kJf7gS7aF8TWO1QSeV4IogFXqPY.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ j c(DiConstructor diConstructor) {
        return new j();
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ c b(DiConstructor diConstructor) {
        return new c(new a() {
            public final Object apply(Object obj) {
                return Integer.valueOf(UIUtils.pxToDp((Context) DiConstructor.this.get(Application.class), ((Float) obj).floatValue()));
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ e a(DiConstructor diConstructor) {
        return new e();
    }
}
