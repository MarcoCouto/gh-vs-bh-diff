package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Size;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.core.util.fi.Supplier;
import com.smaato.sdk.video.utils.DateFormatUtils;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.Map;
import java.util.Map.Entry;

final class g {
    @NonNull
    private final DateFormatUtils a;
    @NonNull
    private final a b;
    @Nullable
    private final String c;
    @Nullable
    private final String d;

    interface a extends Supplier<Size> {
    }

    g(@NonNull a aVar, @NonNull DateFormatUtils dateFormatUtils, @Nullable String str, @Nullable String str2) {
        this.b = (a) Objects.requireNonNull(aVar);
        this.a = (DateFormatUtils) Objects.requireNonNull(dateFormatUtils);
        this.c = str;
        this.d = str2;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> a(@NonNull PlayerState playerState) {
        String str;
        Size size = (Size) this.b.get();
        Entry[] entryArr = new Entry[9];
        String str2 = "[PLAYERSTATE]";
        Boolean bool = playerState.isMuted;
        String str3 = bool == null ? "-2" : bool.booleanValue() ? "fullscreen,muted" : Events.CREATIVE_FULLSCREEN;
        entryArr[0] = Maps.entryOf(str2, str3);
        entryArr[1] = Maps.entryOf("[INVENTORYSTATE]", "skippable,mautoplayed");
        entryArr[2] = Maps.entryOf("[PLAYERSIZE]", Joiner.join((CharSequence) ",", Integer.valueOf(size.width), Integer.valueOf(size.height)));
        String str4 = "[ADPLAYHEAD]";
        Long l = playerState.offsetMillis;
        if (l == null) {
            str = "-2";
        } else {
            str = this.a.offsetFromTimeInterval(l.longValue());
        }
        entryArr[3] = Maps.entryOf(str4, str);
        entryArr[4] = Maps.entryOf("[ASSETURI]", TextUtils.isEmpty(this.c) ? "-2" : this.c);
        entryArr[5] = Maps.entryOf("[CONTENTID]", "-1");
        entryArr[6] = Maps.entryOf("[CONTENTURI]", "-1");
        entryArr[7] = Maps.entryOf("[PODSEQUENCE]", "-1");
        entryArr[8] = Maps.entryOf("[ADSERVINGID]", TextUtils.isEmpty(this.d) ? "-2" : this.d);
        return Maps.mapOf(entryArr);
    }
}
