package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Iterables;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.model.VastBeacon;
import com.smaato.sdk.video.vast.model.VastBeaconEvent;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import com.smaato.sdk.video.vast.model.VastIconScenario;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.tracking.macro.MacroInjector;
import com.smaato.sdk.video.vast.tracking.macro.MacrosInjectorProviderFunction;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

public class VastBeaconTrackerCreator {
    @NonNull
    private final Logger a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final MacrosInjectorProviderFunction c;
    @NonNull
    private final ExecutorService d;
    @NonNull
    private final Function<VastBeacon, String> e = $$Lambda$VastBeaconTrackerCreator$4F1PMggteiZndpvW0wgGm2y6HQ.INSTANCE;

    public VastBeaconTrackerCreator(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull MacrosInjectorProviderFunction macrosInjectorProviderFunction, @NonNull ExecutorService executorService) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.c = (MacrosInjectorProviderFunction) Objects.requireNonNull(macrosInjectorProviderFunction);
        this.d = (ExecutorService) Objects.requireNonNull(executorService);
    }

    @NonNull
    public VastBeaconTracker createBeaconTracker(@NonNull VastScenario vastScenario, @NonNull SomaApiContext somaApiContext) {
        Logger logger = this.a;
        BeaconTracker beaconTracker = this.b;
        MacroInjector macroInjector = (MacroInjector) this.c.apply(vastScenario);
        HashMap hashMap = new HashMap();
        a(hashMap, VastBeaconEvent.SMAATO_IMPRESSION, Iterables.map(vastScenario.impressions, this.e));
        if (vastScenario.viewableImpression != null) {
            a(hashMap, VastBeaconEvent.SMAATO_VIEWABLE_IMPRESSION, vastScenario.viewableImpression.viewable);
        }
        VastMediaFileScenario vastMediaFileScenario = vastScenario.vastMediaFileScenario;
        if (vastMediaFileScenario.videoClicks != null) {
            a(hashMap, VastBeaconEvent.SMAATO_VIDEO_CLICK_TRACKING, Iterables.map(vastMediaFileScenario.videoClicks.clickTrackings, this.e));
        }
        VastIconScenario vastIconScenario = vastMediaFileScenario.vastIconScenario;
        if (vastIconScenario != null) {
            if (vastIconScenario.iconClicks != null) {
                a(hashMap, VastBeaconEvent.SMAATO_ICON_CLICK_TRACKING, Iterables.map(vastIconScenario.iconClicks.iconClickTrackings, this.e));
            }
            a(hashMap, VastBeaconEvent.SMAATO_ICON_VIEW_TRACKING, vastIconScenario.iconViewTrackings);
        }
        VastCompanionScenario vastCompanionScenario = vastScenario.vastCompanionScenario;
        if (vastCompanionScenario != null) {
            a(hashMap, VastBeaconEvent.SMAATO_COMPANION_CLICK_TRACKING, Iterables.map(vastCompanionScenario.companionClickTrackings, this.e));
        }
        VastBeaconTracker vastBeaconTracker = new VastBeaconTracker(logger, somaApiContext, beaconTracker, macroInjector, new b(Collections.unmodifiableMap(hashMap)), this.d);
        return vastBeaconTracker;
    }

    private static void a(@NonNull Map<VastBeaconEvent, Set<String>> map, @NonNull VastBeaconEvent vastBeaconEvent, @NonNull Iterable<String> iterable) {
        if (!map.containsKey(vastBeaconEvent)) {
            map.put(vastBeaconEvent, Collections.unmodifiableSet(Sets.toSet(iterable)));
        } else {
            throw new IllegalArgumentException(String.format("beaconsEventsMap already contains %s event", new Object[]{vastBeaconEvent}));
        }
    }
}
