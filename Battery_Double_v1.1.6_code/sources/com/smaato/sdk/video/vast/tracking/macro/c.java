package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.core.util.fi.Function;
import java.util.Map;
import java.util.Map.Entry;

final class c {
    @NonNull
    private final a a;

    interface a extends Function<Float, Integer> {
    }

    c(@NonNull a aVar) {
        this.a = (a) Objects.requireNonNull(aVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> a(@Nullable Float f, @Nullable Float f2) {
        String str;
        Entry[] entryArr = new Entry[1];
        String str2 = "[CLICKPOS]";
        if (f == null || f2 == null) {
            str = "-2";
        } else if (f.floatValue() <= 0.0f || f2.floatValue() <= 0.0f) {
            str = "-2";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(this.a.apply(f));
            sb.append(",");
            sb.append(this.a.apply(f2));
            str = sb.toString();
        }
        entryArr[0] = Maps.entryOf(str2, str);
        return Maps.mapOf(entryArr);
    }
}
