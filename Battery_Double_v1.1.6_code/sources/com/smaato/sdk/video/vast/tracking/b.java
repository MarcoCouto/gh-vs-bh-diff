package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.VastBeaconEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class b {
    @NonNull
    private final Map<VastBeaconEvent, Collection<String>> a;
    @NonNull
    private final Set<VastBeaconEvent> b = Collections.synchronizedSet(new HashSet());

    b(@NonNull Map<VastBeaconEvent, Collection<String>> map) {
        this.a = new HashMap((Map) Objects.requireNonNull(map));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Set<String> a(@NonNull VastBeaconEvent vastBeaconEvent) {
        if (this.b.contains(vastBeaconEvent)) {
            return Collections.emptySet();
        }
        if (this.a.containsKey(vastBeaconEvent)) {
            Collection collection = (Collection) this.a.get(vastBeaconEvent);
            if (collection != null) {
                return Collections.unmodifiableSet(new HashSet(collection));
            }
        }
        return Collections.emptySet();
    }

    /* access modifiers changed from: 0000 */
    public final void b(@NonNull VastBeaconEvent vastBeaconEvent) {
        this.b.add(vastBeaconEvent);
    }

    /* access modifiers changed from: 0000 */
    public final boolean c(@NonNull VastBeaconEvent vastBeaconEvent) {
        return this.b.contains(vastBeaconEvent);
    }
}
