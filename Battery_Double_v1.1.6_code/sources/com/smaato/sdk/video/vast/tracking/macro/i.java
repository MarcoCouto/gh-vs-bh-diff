package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.datacollector.DataCollector;
import com.smaato.sdk.core.gdpr.SomaGdprData;
import com.smaato.sdk.core.gdpr.SomaGdprDataSource;
import com.smaato.sdk.core.gdpr.SubjectToGdpr;
import com.smaato.sdk.core.util.Joiner;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.core.util.fi.Supplier;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

final class i {
    @NonNull
    private final SomaGdprDataSource a;
    @NonNull
    private final DataCollector b;
    @NonNull
    private final a c;

    interface a extends Supplier<Boolean> {
    }

    i(@NonNull SomaGdprDataSource somaGdprDataSource, @NonNull DataCollector dataCollector, @NonNull a aVar) {
        this.a = (SomaGdprDataSource) Objects.requireNonNull(somaGdprDataSource);
        this.b = (DataCollector) Objects.requireNonNull(dataCollector);
        this.c = (a) Objects.requireNonNull(aVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> a() {
        SomaGdprData somaGdprData = this.a.getSomaGdprData();
        Entry[] entryArr = new Entry[3];
        String str = "[LIMITADTRACKING]";
        Boolean isGoogleLimitAdTrackingEnabled = this.b.getSystemInfo().isGoogleLimitAdTrackingEnabled();
        String str2 = isGoogleLimitAdTrackingEnabled == null ? "-2" : isGoogleLimitAdTrackingEnabled.booleanValue() ? "1" : "0";
        entryArr[0] = Maps.entryOf(str, str2);
        String str3 = "[REGULATIONS]";
        ArrayList arrayList = new ArrayList();
        if (((Boolean) this.c.get()).booleanValue()) {
            arrayList.add("coppa");
        }
        if (!somaGdprData.getConsentString().isEmpty()) {
            arrayList.add("gdpr");
        } else if (somaGdprData.getSubjectToGdpr() != SubjectToGdpr.CMP_GDPR_UNKNOWN && somaGdprData.getSubjectToGdpr() == SubjectToGdpr.CMP_GDPR_ENABLED) {
            arrayList.add("gdpr");
        }
        entryArr[1] = Maps.entryOf(str3, arrayList.isEmpty() ? "-2" : Joiner.join((CharSequence) ",", (Iterable) arrayList));
        String str4 = "[GDPRCONSENT]";
        String consentString = somaGdprData.getConsentString();
        if (TextUtils.isEmpty(consentString)) {
            consentString = "-2";
        }
        entryArr[2] = Maps.entryOf(str4, consentString);
        return Maps.mapOf(entryArr);
    }
}
