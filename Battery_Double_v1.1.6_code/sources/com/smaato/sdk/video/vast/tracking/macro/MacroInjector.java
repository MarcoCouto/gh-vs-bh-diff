package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.core.util.fi.BiFunction;
import com.smaato.sdk.video.utils.UriUtils;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class MacroInjector {
    @NonNull
    private final UriUtils a;
    @NonNull
    private final a b;
    @NonNull
    private final b c;
    @NonNull
    private final d d;
    @NonNull
    private final f e;
    @NonNull
    private final g f;
    @NonNull
    private final h g;
    @NonNull
    private final i h;
    @NonNull
    private final j i;
    @NonNull
    private final c j;
    @NonNull
    private final e k;

    MacroInjector(@NonNull UriUtils uriUtils, @NonNull a aVar, @NonNull b bVar, @NonNull d dVar, @NonNull f fVar, @NonNull g gVar, @NonNull h hVar, @NonNull i iVar, @NonNull j jVar, @NonNull c cVar, @NonNull e eVar) {
        this.a = (UriUtils) Objects.requireNonNull(uriUtils);
        this.b = (a) Objects.requireNonNull(aVar);
        this.c = (b) Objects.requireNonNull(bVar);
        this.d = (d) Objects.requireNonNull(dVar);
        this.e = (f) Objects.requireNonNull(fVar);
        this.f = (g) Objects.requireNonNull(gVar);
        this.g = (h) Objects.requireNonNull(hVar);
        this.h = (i) Objects.requireNonNull(iVar);
        this.i = (j) Objects.requireNonNull(jVar);
        this.j = (c) Objects.requireNonNull(cVar);
        this.k = (e) Objects.requireNonNull(eVar);
    }

    @NonNull
    public final String injectMacros(@NonNull String str, @NonNull PlayerState playerState) {
        return a(str, a(playerState));
    }

    @NonNull
    public final Set<String> injectMacros(@NonNull Collection<String> collection, @NonNull PlayerState playerState) {
        Map a2 = a(playerState);
        HashSet hashSet = new HashSet(collection.size());
        for (String a3 : collection) {
            hashSet.add(a(a3, a2));
        }
        return hashSet;
    }

    @NonNull
    private String a(@NonNull String str, @NonNull Map<String, String> map) {
        return (String) Maps.reduce(map, str, new BiFunction() {
            public final Object apply(Object obj, Object obj2) {
                return MacroInjector.this.a((Entry) obj, (String) obj2);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ String a(Entry entry, String str) {
        return str.replace((CharSequence) entry.getKey(), this.a.encodeQueryString((String) entry.getValue()));
    }

    @NonNull
    private Map<String, String> a(@NonNull PlayerState playerState) {
        return Maps.merge(this.b.a(playerState), b.a(), this.d.a(), this.e.a(), this.f.a(playerState), this.g.a(), this.h.a(), j.a(), this.j.a(playerState.clickPositionX, playerState.clickPositionY), e.a(playerState.errorCode));
    }
}
