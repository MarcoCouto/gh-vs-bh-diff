package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Maps;
import com.smaato.sdk.video.utils.DateFormatUtils;
import com.smaato.sdk.video.utils.RandomUtils;
import java.util.Map;

final class f {
    @NonNull
    private final DateFormatUtils a;
    @NonNull
    private final RandomUtils b;

    f(@NonNull DateFormatUtils dateFormatUtils, @NonNull RandomUtils randomUtils) {
        this.a = (DateFormatUtils) Objects.requireNonNull(dateFormatUtils);
        this.b = (RandomUtils) Objects.requireNonNull(randomUtils);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Map<String, String> a() {
        return Maps.mapOf(Maps.entryOf("[TIMESTAMP]", this.a.currentTimestamp()), Maps.entryOf("[CACHEBUSTING]", this.b.random8DigitNumber()));
    }
}
