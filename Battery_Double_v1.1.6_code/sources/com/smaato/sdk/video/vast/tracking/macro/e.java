package com.smaato.sdk.video.vast.tracking.macro;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.collections.Maps;
import java.util.Map;
import java.util.Map.Entry;

final class e {
    e() {
    }

    @NonNull
    static Map<String, String> a(@Nullable Integer num) {
        String str;
        Entry[] entryArr = new Entry[1];
        String str2 = "[ERRORCODE]";
        if (num == null) {
            str = "-2";
        } else {
            str = String.valueOf(num);
        }
        entryArr[0] = Maps.entryOf(str2, str);
        return Maps.mapOf(entryArr);
    }
}
