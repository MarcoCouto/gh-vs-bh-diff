package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.Tracking;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import com.smaato.sdk.video.vast.model.VastEvent;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.tracking.macro.MacroInjector;
import com.smaato.sdk.video.vast.tracking.macro.MacrosInjectorProviderFunction;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class VastEventTrackerCreator {
    @NonNull
    private final Logger a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final MacrosInjectorProviderFunction c;
    @NonNull
    private final ExecutorService d;

    public VastEventTrackerCreator(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull MacrosInjectorProviderFunction macrosInjectorProviderFunction, @NonNull ExecutorService executorService) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.c = (MacrosInjectorProviderFunction) Objects.requireNonNull(macrosInjectorProviderFunction);
        this.d = (ExecutorService) Objects.requireNonNull(executorService);
    }

    private static void a(@NonNull Map<VastEvent, LinkedList<Tracking>> map, @NonNull List<Tracking> list) {
        for (Tracking tracking : list) {
            if (!map.containsKey(tracking.vastEvent)) {
                map.put(tracking.vastEvent, new LinkedList());
            }
            Objects.onNotNull(map.get(tracking.vastEvent), new Consumer() {
                public final void accept(Object obj) {
                    ((LinkedList) obj).add(Tracking.this);
                }
            });
        }
    }

    @NonNull
    public VastEventTracker createEventTracker(@NonNull VastScenario vastScenario, @NonNull SomaApiContext somaApiContext) {
        HashMap hashMap = new HashMap();
        a((Map<VastEvent, LinkedList<Tracking>>) hashMap, vastScenario.vastMediaFileScenario.trackingEvents);
        VastCompanionScenario vastCompanionScenario = vastScenario.vastCompanionScenario;
        if (vastCompanionScenario != null) {
            a((Map<VastEvent, LinkedList<Tracking>>) hashMap, vastCompanionScenario.trackingEvents);
        }
        Map unmodifiableMap = Collections.unmodifiableMap(hashMap);
        VastEventTracker vastEventTracker = new VastEventTracker(this.a, this.b, unmodifiableMap, a.a(unmodifiableMap, vastScenario.vastMediaFileScenario.duration, this.a), (MacroInjector) this.c.apply(vastScenario), somaApiContext, this.d);
        return vastEventTracker;
    }
}
