package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Threads;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Predicate;
import com.smaato.sdk.video.vast.model.Tracking;
import com.smaato.sdk.video.vast.model.VastEvent;
import com.smaato.sdk.video.vast.tracking.macro.MacroInjector;
import com.smaato.sdk.video.vast.tracking.macro.PlayerState;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

public class VastEventTracker {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final Map<VastEvent, List<Tracking>> c;
    @NonNull
    private final Set<VastEvent> d = Collections.synchronizedSet(new HashSet());
    @NonNull
    private final a e;
    @NonNull
    private final MacroInjector f;
    @NonNull
    private final SomaApiContext g;
    @NonNull
    private final ExecutorService h;

    VastEventTracker(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull Map<VastEvent, List<Tracking>> map, @NonNull a aVar, @NonNull MacroInjector macroInjector, @NonNull SomaApiContext somaApiContext, @NonNull ExecutorService executorService) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.f = (MacroInjector) Objects.requireNonNull(macroInjector);
        this.g = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.c = (Map) Objects.requireNonNull(map);
        this.h = (ExecutorService) Objects.requireNonNull(executorService);
        this.e = (a) Objects.requireNonNull(aVar);
    }

    public void triggerProgressDependentEvent(@NonNull PlayerState playerState, long j) {
        Long l = playerState.offsetMillis;
        if (l != null) {
            a((Collection<Tracking>) this.e.a(l.longValue(), j), playerState);
        }
    }

    public void triggerEventByName(@NonNull VastEvent vastEvent, PlayerState playerState) {
        Objects.onNotNull(this.c.get(vastEvent), new Consumer(playerState) {
            private final /* synthetic */ PlayerState f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                VastEventTracker.this.a(this.f$1, (List) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Collection<Tracking> collection, @NonNull PlayerState playerState) {
        Set retainToSet = Sets.retainToSet(collection, new Predicate() {
            public final boolean test(Object obj) {
                return VastEventTracker.this.b((Tracking) obj);
            }
        });
        if (!retainToSet.isEmpty()) {
            a(retainToSet);
            this.h.execute(new Runnable(retainToSet, playerState) {
                private final /* synthetic */ Set f$1;
                private final /* synthetic */ PlayerState f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    VastEventTracker.this.a(this.f$1, this.f$2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ boolean b(Tracking tracking) {
        return !tracking.vastEvent.oneTime || !this.d.contains(tracking.vastEvent);
    }

    private void a(@NonNull Tracking tracking) {
        this.e.a(tracking);
        this.d.add(tracking.vastEvent);
    }

    private void a(@NonNull Set<Tracking> set) {
        for (Tracking a2 : set) {
            a(a2);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.Set, code=java.util.Set<com.smaato.sdk.video.vast.model.Tracking>, for r6v0, types: [java.util.Set<com.smaato.sdk.video.vast.model.Tracking>, java.util.Collection, java.util.Set] */
    public /* synthetic */ void a(Set<Tracking> set, PlayerState playerState) {
        for (final Tracking tracking : set) {
            Threads.ensureNotMainThread();
            this.b.trackBeaconUrl(this.f.injectMacros(tracking.url, playerState), this.g, new Listener<Whatever, Exception>() {
                public final /* synthetic */ void onFailure(@NonNull Task task, @NonNull Object obj) {
                    VastEventTracker.this.a.error(LogDomain.VAST, (Exception) obj, "Tracking Vast event failed with exception: %s", tracking.vastEvent);
                }

                public final /* synthetic */ void onSuccess(@NonNull Task task, @NonNull Object obj) {
                    VastEventTracker.this.a.info(LogDomain.VAST, "Vast event was tracked successfully %s", tracking.vastEvent);
                }
            });
        }
    }
}
