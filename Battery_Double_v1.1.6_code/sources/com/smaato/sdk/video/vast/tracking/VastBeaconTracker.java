package com.smaato.sdk.video.vast.tracking;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.Whatever;
import com.smaato.sdk.video.vast.model.VastBeaconEvent;
import com.smaato.sdk.video.vast.tracking.macro.MacroInjector;
import com.smaato.sdk.video.vast.tracking.macro.PlayerState;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ExecutorService;

public class VastBeaconTracker {
    /* access modifiers changed from: private */
    @NonNull
    public final Logger a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final b c;
    @NonNull
    private final SomaApiContext d;
    @NonNull
    private final MacroInjector e;
    @NonNull
    private final ExecutorService f;

    VastBeaconTracker(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull BeaconTracker beaconTracker, @NonNull MacroInjector macroInjector, @NonNull b bVar, @NonNull ExecutorService executorService) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.e = (MacroInjector) Objects.requireNonNull(macroInjector);
        this.c = (b) Objects.requireNonNull(bVar);
        this.d = (SomaApiContext) Objects.requireNonNull(somaApiContext);
        this.f = (ExecutorService) Objects.requireNonNull(executorService);
    }

    public void trigger(@NonNull VastBeaconEvent vastBeaconEvent, @NonNull PlayerState playerState) {
        if (!this.c.c(vastBeaconEvent)) {
            if (this.f.isShutdown()) {
                this.a.error(LogDomain.VAST, "Attempt to trigger event: %s on a already shutdown beacon tracker", vastBeaconEvent);
                return;
            }
            this.f.execute(new Runnable(vastBeaconEvent, playerState) {
                private final /* synthetic */ VastBeaconEvent f$1;
                private final /* synthetic */ PlayerState f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                public final void run() {
                    VastBeaconTracker.this.a(this.f$1, this.f$2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(final VastBeaconEvent vastBeaconEvent, PlayerState playerState) {
        Set set;
        Set a2 = this.c.a(vastBeaconEvent);
        if (!a2.isEmpty()) {
            set = Collections.unmodifiableSet(this.e.injectMacros((Collection<String>) a2, playerState));
        } else {
            set = Collections.emptySet();
        }
        if (!set.isEmpty()) {
            this.c.b(vastBeaconEvent);
            this.b.trackBeaconUrls(set, this.d, new Listener<Whatever, Exception>() {
                public final /* synthetic */ void onFailure(@NonNull Task task, @NonNull Object obj) {
                    VastBeaconTracker.this.a.error(LogDomain.VAST, (Exception) obj, "Tracking Vast beacon failed with exception: %s", vastBeaconEvent);
                }

                public final /* synthetic */ void onSuccess(@NonNull Task task, @NonNull Object obj) {
                    VastBeaconTracker.this.a.info(LogDomain.VAST, "Vast beacon was tracked successfully %s", vastBeaconEvent);
                }
            });
        }
    }
}
