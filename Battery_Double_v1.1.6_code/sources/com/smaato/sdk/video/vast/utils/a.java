package com.smaato.sdk.video.vast.utils;

import com.smaato.sdk.video.vast.model.StaticResource.CreativeType;

final class a {

    /* renamed from: com.smaato.sdk.video.vast.utils.a$1 reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[CreativeType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            a[CreativeType.IMAGE.ordinal()] = 1;
            a[CreativeType.JAVASCRIPT.ordinal()] = 2;
            try {
                a[CreativeType.UNKNOWN.ordinal()] = 3;
            } catch (NoSuchFieldError unused) {
            }
        }
    }
}
