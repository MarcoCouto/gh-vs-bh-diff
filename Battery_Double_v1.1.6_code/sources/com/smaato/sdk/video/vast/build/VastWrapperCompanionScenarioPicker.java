package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.compare.SizeComparator;
import com.smaato.sdk.video.vast.model.UniversalAdId;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VastWrapperCompanionScenarioPicker {
    @Nullable
    public VastCompanionScenario pickWrapperCompanionScenario(@NonNull UniversalAdId universalAdId, @NonNull List<VastCompanionScenario> list, @NonNull VastConfigurationSettings vastConfigurationSettings) {
        ArrayList arrayList;
        ArrayList arrayList2;
        Objects.requireNonNull(universalAdId, "Parameter universalAdId should not be null for VastWrapperCompanionScenarioPicker::pickWrapperCompanionScenario");
        Objects.requireNonNull(list, "Parameter wrapperVastCompanionScenarios should not be null for VastWrapperCompanionScenarioPicker::pickWrapperCompanionScenario");
        Objects.requireNonNull(vastConfigurationSettings, "Parameter vastConfigurationSettings should not be null for VastWrapperCompanionScenarioPicker::pickWrapperCompanionScenario");
        if (list.isEmpty()) {
            return null;
        }
        if (universalAdId.equals(UniversalAdId.DEFAULT)) {
            arrayList2 = new ArrayList(list);
        } else {
            arrayList = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (VastCompanionScenario vastCompanionScenario : list) {
                if (vastCompanionScenario.vastScenarioCreativeData.universalAdId.equals(universalAdId)) {
                    arrayList.add(vastCompanionScenario);
                } else if (vastCompanionScenario.vastScenarioCreativeData.universalAdId.equals(UniversalAdId.DEFAULT)) {
                    arrayList3.add(vastCompanionScenario);
                }
            }
            if (arrayList.isEmpty()) {
                if (arrayList3.isEmpty()) {
                    arrayList2 = new ArrayList(list);
                } else {
                    arrayList = arrayList3;
                }
            }
            Collections.sort(arrayList, new SizeComparator(vastConfigurationSettings));
            return (VastCompanionScenario) arrayList.get(0);
        }
        arrayList = arrayList2;
        Collections.sort(arrayList, new SizeComparator(vastConfigurationSettings));
        return (VastCompanionScenario) arrayList.get(0);
    }
}
