package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Sets;
import java.util.Collections;
import java.util.Set;

public final class VastResult<Result> {
    @NonNull
    public final Set<Integer> errors;
    @Nullable
    public final Result value;

    public static class Builder<Result> {
        @Nullable
        private Result a;
        @Nullable
        private Set<Integer> b;

        @NonNull
        public Builder<Result> setResult(@Nullable Result result) {
            this.a = result;
            return this;
        }

        @NonNull
        public Builder<Result> setErrors(@Nullable Set<Integer> set) {
            this.b = set;
            return this;
        }

        @NonNull
        public VastResult<Result> build() {
            if (this.a != null || this.b != null) {
                return new VastResult<>(Sets.toImmutableSet(this.b), this.a, 0);
            }
            throw new IllegalStateException("VastResult should contain value or list of errors at least");
        }
    }

    /* synthetic */ VastResult(Set set, Object obj, byte b) {
        this(set, obj);
    }

    private VastResult(@NonNull Set<Integer> set, @Nullable Result result) {
        this.errors = (Set) Objects.requireNonNull(set);
        this.value = result;
    }

    @NonNull
    public static <Result> VastResult<Result> error(int i) {
        return new VastResult<>(Collections.singleton(Integer.valueOf(i)), null);
    }

    @NonNull
    public static <Result> VastResult<Result> error(@NonNull Set<Integer> set) {
        return new VastResult<>(set, null);
    }
}
