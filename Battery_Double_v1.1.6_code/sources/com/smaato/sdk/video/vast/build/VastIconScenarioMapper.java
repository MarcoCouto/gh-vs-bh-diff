package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.Icon;
import com.smaato.sdk.video.vast.model.StaticResource;
import com.smaato.sdk.video.vast.model.VastIconScenario;
import com.smaato.sdk.video.vast.model.VastScenarioResourceData;
import com.smaato.sdk.video.vast.model.VastScenarioResourceData.Builder;
import com.smaato.sdk.video.vast.utils.VastVideoPlayerTimeConverterUtils;

public class VastIconScenarioMapper {
    @Nullable
    static VastIconScenario a(@NonNull Logger logger, @NonNull Icon icon) {
        Objects.requireNonNull(logger);
        try {
            VastScenarioResourceData build = new Builder().setStaticResource(icon.staticResources.isEmpty() ? null : (StaticResource) icon.staticResources.get(0)).setHtmlResources(icon.htmlResources.isEmpty() ? null : (String) icon.htmlResources.get(0)).setIFrameResources(icon.iFrameResources.isEmpty() ? null : (String) icon.iFrameResources.get(0)).build();
            long convertDurationStringToMilliseconds = VastVideoPlayerTimeConverterUtils.convertDurationStringToMilliseconds(icon.duration, logger);
            return new VastIconScenario.Builder().setVastScenarioResourceData(build).setIconClicks(icon.iconClicks).setIconViewTrackings(icon.iconViewTrackings).setWidth(icon.width).setHeight(icon.height).setXPosition(icon.xPosition).setYPosition(icon.yPosition).setPxRatio(icon.pxRatio).setOffset(VastVideoPlayerTimeConverterUtils.convertOffsetStringToMilliseconds(icon.offset, convertDurationStringToMilliseconds, logger)).setProgram(icon.program).setApiFramework(icon.apiFramework).setDuration(convertDurationStringToMilliseconds).build();
        } catch (VastElementMissingException e) {
            logger.error(LogDomain.VAST, e, "Cannot build VastIconScenario", new Object[0]);
            return null;
        }
    }
}
