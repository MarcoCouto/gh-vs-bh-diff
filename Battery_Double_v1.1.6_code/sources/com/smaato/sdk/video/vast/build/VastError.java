package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class VastError {
    @Nullable
    public final String assetUrl;
    public final int code;
    public final long offsetMillis;

    public static class Builder {
        private int a;
        private long b;
        @Nullable
        private String c;

        public Builder(int i) {
            this.a = i;
        }

        @NonNull
        public Builder setAssetUrl(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        public Builder setOffsetMillis(long j) {
            this.b = j;
            return this;
        }

        @NonNull
        public VastError build() {
            VastError vastError = new VastError(this.a, this.b, this.c, 0);
            return vastError;
        }
    }

    /* synthetic */ VastError(int i, long j, String str, byte b) {
        this(i, j, str);
    }

    private VastError(int i, long j, @Nullable String str) {
        this.code = i;
        this.offsetMillis = j;
        this.assetUrl = str;
    }
}
