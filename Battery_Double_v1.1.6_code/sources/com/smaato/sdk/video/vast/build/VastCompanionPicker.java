package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.compare.SizeComparator;
import com.smaato.sdk.video.vast.model.Companion;
import com.smaato.sdk.video.vast.model.CompanionAds;
import com.smaato.sdk.video.vast.model.Creative;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VastCompanionPicker {
    @Nullable
    public Companion pickCompanion(@NonNull CompanionAds companionAds, @NonNull VastConfigurationSettings vastConfigurationSettings) {
        Objects.requireNonNull(companionAds, "Parameter companionAds should not be null for VastCompanionPicker::pickCompanion");
        Objects.requireNonNull(vastConfigurationSettings, "Parameter vastConfigurationSettings should not be null for VastCompanionPicker::pickCompanion");
        ArrayList<Companion> arrayList = new ArrayList<>(companionAds.companions);
        Collections.sort(arrayList, new SizeComparator(vastConfigurationSettings));
        for (Companion companion : arrayList) {
            if (!companion.staticResources.isEmpty() || !companion.iFrameResources.isEmpty()) {
                return companion;
            }
            if (!companion.htmlResources.isEmpty()) {
                return companion;
            }
        }
        return null;
    }

    @Nullable
    public Companion pickCompanion(@NonNull List<Creative> list, @NonNull VastConfigurationSettings vastConfigurationSettings) {
        Objects.requireNonNull(list, "Parameter creatives should not be null for VastCompanionPicker::pickCompanion");
        Objects.requireNonNull(vastConfigurationSettings, "Parameter vastConfigurationSettings should not be null for VastCompanionPicker::pickCompanion");
        ArrayList arrayList = new ArrayList();
        for (Creative creative : list) {
            if (creative.companionAds != null) {
                Companion pickCompanion = pickCompanion(creative.companionAds, vastConfigurationSettings);
                if (pickCompanion != null) {
                    arrayList.add(pickCompanion);
                }
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        Collections.sort(arrayList, new SizeComparator(vastConfigurationSettings));
        return (Companion) arrayList.get(0);
    }
}
