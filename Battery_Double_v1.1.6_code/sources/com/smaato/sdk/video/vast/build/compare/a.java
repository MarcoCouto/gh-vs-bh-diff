package com.smaato.sdk.video.vast.build.compare;

import com.ironsource.mediationsdk.utils.IronSourceConstants;

enum a {
    LOW(360, 600),
    MEDIUM(576, IronSourceConstants.RV_API_SHOW_CALLED),
    HIGH(720, 2000);
    
    public final int d;
    public final int e;

    private a(int i, int i2) {
        this.d = i;
        this.e = i2;
    }
}
