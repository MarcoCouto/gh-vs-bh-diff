package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.UniversalAdId;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.model.VastRawMediaFileScenario;
import java.util.List;

public class VastMediaFileScenarioMerger {
    @NonNull
    private VideoClicksMerger a;

    public VastMediaFileScenarioMerger(@NonNull VideoClicksMerger videoClicksMerger) {
        this.a = (VideoClicksMerger) Objects.requireNonNull(videoClicksMerger, "Parameter videoClicksMerger should not be null for VastMediaFileScenarioMerger::new");
    }

    @NonNull
    public VastMediaFileScenario merge(@NonNull VastMediaFileScenario vastMediaFileScenario, @NonNull List<VastRawMediaFileScenario> list) {
        Objects.requireNonNull(vastMediaFileScenario, "Parameter mediaFileScenario should not be null for VastMediaFileScenarioMerger::merge");
        Objects.requireNonNull(list, "Parameter wrapperMediaFileScenarios should not be null for VastMediaFileScenarioMerger::merge");
        VastMediaFileScenario vastMediaFileScenario2 = vastMediaFileScenario;
        for (VastRawMediaFileScenario vastRawMediaFileScenario : list) {
            UniversalAdId universalAdId = vastMediaFileScenario.vastScenarioCreativeData.universalAdId;
            UniversalAdId universalAdId2 = vastRawMediaFileScenario.vastScenarioCreativeData.universalAdId;
            if (universalAdId.equals(universalAdId2) || universalAdId2.equals(UniversalAdId.DEFAULT)) {
                vastMediaFileScenario2 = vastMediaFileScenario2.newBuilder().setVideoClicks(this.a.merge(vastMediaFileScenario2.videoClicks, vastRawMediaFileScenario.videoClicks)).setTrackingEvents(VastScenarioMergeUtils.merge(vastMediaFileScenario2.trackingEvents, vastRawMediaFileScenario.trackingEvents)).setVastIconScenario(vastMediaFileScenario2.vastIconScenario == null ? vastRawMediaFileScenario.vastIconScenario : vastMediaFileScenario2.vastIconScenario).build();
            }
        }
        return vastMediaFileScenario2;
    }
}
