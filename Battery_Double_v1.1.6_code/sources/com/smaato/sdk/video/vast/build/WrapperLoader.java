package com.smaato.sdk.video.vast.build;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.network.StaticWrapperLoaderExecutioner;
import com.smaato.sdk.video.vast.exceptions.wrapper.GeneralWrapperErrorException;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.model.Wrapper;
import com.smaato.sdk.video.vast.parser.ParseResult;

public class WrapperLoader {
    /* access modifiers changed from: private */
    @NonNull
    public final WrapperLoaderErrorMapper a;
    @NonNull
    private final StaticWrapperLoaderExecutioner b;

    public WrapperLoader(@NonNull WrapperLoaderErrorMapper wrapperLoaderErrorMapper, @NonNull StaticWrapperLoaderExecutioner staticWrapperLoaderExecutioner) {
        this.a = (WrapperLoaderErrorMapper) Objects.requireNonNull(wrapperLoaderErrorMapper);
        this.b = (StaticWrapperLoaderExecutioner) Objects.requireNonNull(staticWrapperLoaderExecutioner);
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull final Wrapper wrapper, @NonNull final NonNullConsumer<ParseResult<VastTree>> nonNullConsumer) {
        if (TextUtils.isEmpty(wrapper.vastAdTagUri)) {
            nonNullConsumer.accept(ParseResult.error(Wrapper.VAST_AD_TAG_URI, new GeneralWrapperErrorException("Cannot resolve wrapper: vastAdTagUri is missing")));
        } else {
            this.b.submitRequest(wrapper.vastAdTagUri, somaApiContext, (Listener<ParseResult<VastTree>, Error>) new Listener<ParseResult<VastTree>, Error>() {
                public final /* synthetic */ void onFailure(@NonNull Task task, @NonNull Object obj) {
                    Error error = (Error) obj;
                    String format = String.format("Failed to load Vast url: %s due to error: %s", new Object[]{wrapper.vastAdTagUri, error});
                    logger.error(LogDomain.VAST, format, new Object[0]);
                    NonNullConsumer nonNullConsumer = nonNullConsumer;
                    WrapperLoader.this.a;
                    nonNullConsumer.accept(ParseResult.error("Wrapper", WrapperLoaderErrorMapper.a(error, format)));
                }

                public final /* synthetic */ void onSuccess(@NonNull Task task, @NonNull Object obj) {
                    nonNullConsumer.accept((ParseResult) obj);
                }
            }).start();
        }
    }
}
