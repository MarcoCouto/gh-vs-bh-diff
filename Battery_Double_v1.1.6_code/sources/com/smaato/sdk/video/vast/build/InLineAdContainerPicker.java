package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.Ad;
import com.smaato.sdk.video.vast.model.InLine;
import java.util.List;

public class InLineAdContainerPicker {
    @Nullable
    static a<InLine> a(@NonNull List<Ad> list) {
        if (list.size() == 1) {
            Ad ad = (Ad) list.get(0);
            if (ad.inLine != null) {
                return new a<>(ad, ad.inLine);
            }
        } else {
            for (Ad ad2 : list) {
                if (ad2.inLine != null && ad2.sequence == null) {
                    return new a<>(ad2, ad2.inLine);
                }
            }
        }
        return null;
    }
}
