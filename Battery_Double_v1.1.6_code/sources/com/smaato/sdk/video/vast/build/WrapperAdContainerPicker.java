package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.Ad;
import com.smaato.sdk.video.vast.model.Wrapper;
import java.util.List;

public class WrapperAdContainerPicker {
    @Nullable
    static a<Wrapper> a(@NonNull List<Ad> list) {
        if (list.size() == 1) {
            Ad ad = (Ad) list.get(0);
            if (ad.wrapper != null) {
                return new a<>(ad, ad.wrapper);
            }
        } else {
            for (Ad ad2 : list) {
                if (ad2.wrapper != null && ad2.sequence == null) {
                    return new a<>(ad2, ad2.wrapper);
                }
            }
        }
        return null;
    }
}
