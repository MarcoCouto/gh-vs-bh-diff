package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.build.MediaFileResult.Builder;
import com.smaato.sdk.video.vast.build.compare.AverageBitratePicker;
import com.smaato.sdk.video.vast.build.compare.BitrateComparator;
import com.smaato.sdk.video.vast.build.compare.MediaFileComparator;
import com.smaato.sdk.video.vast.build.compare.SizeComparator;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.smaato.sdk.video.vast.model.MediaFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class VastLinearMediaFilePicker {
    @NonNull
    static MediaFileResult a(@NonNull List<MediaFile> list, @NonNull VastConfigurationSettings vastConfigurationSettings) {
        if (list.isEmpty()) {
            return new Builder().build();
        }
        HashSet hashSet = new HashSet();
        ArrayList<MediaFile> arrayList = new ArrayList<>(list);
        Collections.sort(arrayList, new MediaFileComparator(new SizeComparator(vastConfigurationSettings), new BitrateComparator(new AverageBitratePicker(vastConfigurationSettings).getAverageBitrate(), "wifi".equalsIgnoreCase(vastConfigurationSettings.connectionType))));
        for (MediaFile mediaFile : arrayList) {
            if (mediaFile.isVpaid()) {
                hashSet.add(Integer.valueOf(ErrorCode.GENERAL_VPAID_ERROR));
            } else {
                String substring = mediaFile.url.substring(mediaFile.url.lastIndexOf(".") + 1);
                if ((mediaFile.type != null && mediaFile.type.matches("video/.*(?i)(mp4|3gp|mp2t|webm|mkv)")) || substring.matches("(?i)^(mp4|3gp|mp2t|webm|mkv)$")) {
                    return new Builder().setMediaFile(mediaFile).build();
                }
            }
        }
        hashSet.add(Integer.valueOf(403));
        return new Builder().setErrors(hashSet).build();
    }
}
