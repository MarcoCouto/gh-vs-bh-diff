package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.video.vast.model.VastScenario;
import java.util.Set;

public final class VastScenarioResult {
    @NonNull
    public final Set<String> errorUrls;
    @NonNull
    public final Set<Integer> errors;
    @Nullable
    public final VastScenario vastScenario;

    public static class Builder {
        @Nullable
        private VastScenario a;
        @Nullable
        private Set<String> b;
        @Nullable
        private Set<Integer> c;

        @NonNull
        public Builder setVastScenario(@Nullable VastScenario vastScenario) {
            this.a = vastScenario;
            return this;
        }

        @NonNull
        public Builder setErrors(@Nullable Set<Integer> set) {
            this.c = set;
            return this;
        }

        @NonNull
        public Builder setErrorUrls(@Nullable Set<String> set) {
            this.b = set;
            return this;
        }

        @NonNull
        public VastScenarioResult build() {
            return new VastScenarioResult(Sets.toImmutableSet(this.b), Sets.toImmutableSet(this.c), this.a, 0);
        }
    }

    /* synthetic */ VastScenarioResult(Set set, Set set2, VastScenario vastScenario2, byte b) {
        this(set, set2, vastScenario2);
    }

    private VastScenarioResult(@NonNull Set<String> set, @NonNull Set<Integer> set2, @Nullable VastScenario vastScenario2) {
        this.errorUrls = (Set) Objects.requireNonNull(set);
        this.errors = (Set) Objects.requireNonNull(set2);
        this.vastScenario = vastScenario2;
    }
}
