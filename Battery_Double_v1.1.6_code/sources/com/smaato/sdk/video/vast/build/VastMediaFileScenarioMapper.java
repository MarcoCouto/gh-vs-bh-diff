package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.Linear;
import com.smaato.sdk.video.vast.model.MediaFile;
import com.smaato.sdk.video.vast.model.VastIconScenario;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario.Builder;
import com.smaato.sdk.video.vast.model.VastScenarioCreativeData;
import com.smaato.sdk.video.vast.utils.VastVideoPlayerTimeConverterUtils;

public class VastMediaFileScenarioMapper {
    @NonNull
    private final VastIconScenarioPicker a;

    public VastMediaFileScenarioMapper(@NonNull VastIconScenarioPicker vastIconScenarioPicker) {
        this.a = (VastIconScenarioPicker) Objects.requireNonNull(vastIconScenarioPicker, "Parameter vastIconScenarioPicker should not be null for VastMediaFileScenarioMapper::new");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VastMediaFileScenario a(@NonNull Logger logger, @NonNull MediaFile mediaFile, @NonNull Linear linear, @NonNull VastScenarioCreativeData vastScenarioCreativeData) {
        Objects.requireNonNull(linear);
        Objects.requireNonNull(mediaFile);
        Objects.requireNonNull(linear);
        Objects.requireNonNull(vastScenarioCreativeData);
        VastIconScenario pickIconScenario = this.a.pickIconScenario(logger, linear.icons);
        long convertDurationStringToMilliseconds = VastVideoPlayerTimeConverterUtils.convertDurationStringToMilliseconds(linear.duration, logger);
        return new Builder().setVastScenarioCreativeData(vastScenarioCreativeData).setTrackingEvents(linear.trackingEvents).setMediaFile(mediaFile).setVastIconScenario(pickIconScenario).setVideoClicks(linear.videoClicks).setAdParameters(linear.adParameters).setSkipOffset(VastVideoPlayerTimeConverterUtils.convertOffsetStringToMilliseconds(linear.skipOffset, convertDurationStringToMilliseconds, logger)).setDuration(convertDurationStringToMilliseconds).build();
    }
}
