package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.build.VastResult.Builder;
import com.smaato.sdk.video.vast.exceptions.wrapper.GeneralWrapperErrorException;
import com.smaato.sdk.video.vast.exceptions.wrapper.WrapperRequestTimeoutException;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.model.Wrapper;
import com.smaato.sdk.video.vast.parser.ParseError;
import com.smaato.sdk.video.vast.parser.ParseResult;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class WrapperResolver {
    private final int a;
    @NonNull
    private final WrapperLoader b;
    @NonNull
    private final InLineChecker c;
    @NonNull
    private final WrapperAdContainerPicker d;

    public WrapperResolver(int i, @NonNull WrapperLoader wrapperLoader, @NonNull InLineChecker inLineChecker, @NonNull WrapperAdContainerPicker wrapperAdContainerPicker) {
        if (i >= 0) {
            this.c = inLineChecker;
            this.d = wrapperAdContainerPicker;
            this.a = i;
            this.b = wrapperLoader;
            return;
        }
        throw new IllegalArgumentException("Cannot construct WrapperResolver: maxDepth can't be negative");
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull VastTree vastTree, boolean z, int i, @NonNull NonNullConsumer<VastResult<VastTree>> nonNullConsumer) {
        VastTree vastTree2 = vastTree;
        NonNullConsumer<VastResult<VastTree>> nonNullConsumer2 = nonNullConsumer;
        Builder result = new Builder().setResult(vastTree);
        if (vastTree2.ads.isEmpty()) {
            result.setErrors(Collections.singleton(Integer.valueOf(303)));
            nonNullConsumer2.accept(result.build());
        } else if (InLineChecker.a(vastTree2.ads)) {
            nonNullConsumer2.accept(result.build());
        } else if (!z) {
            result.setErrors(Collections.singleton(Integer.valueOf(303)));
            nonNullConsumer2.accept(result.build());
        } else {
            a a2 = WrapperAdContainerPicker.a(vastTree2.ads);
            if (a2 == null) {
                result.setErrors(Collections.singleton(Integer.valueOf(303)));
                nonNullConsumer2.accept(result.build());
            } else if (i > this.a) {
                result.setErrors(Collections.singleton(Integer.valueOf(302)));
                nonNullConsumer2.accept(result.build());
            } else {
                WrapperLoader wrapperLoader = this.b;
                Wrapper wrapper = (Wrapper) a2.a;
                $$Lambda$WrapperResolver$zNbgpVJs6GnhWtJ1XQNfIUJ5lGY r0 = new NonNullConsumer(logger, somaApiContext, a2, i, nonNullConsumer, vastTree) {
                    private final /* synthetic */ Logger f$1;
                    private final /* synthetic */ SomaApiContext f$2;
                    private final /* synthetic */ a f$3;
                    private final /* synthetic */ int f$4;
                    private final /* synthetic */ NonNullConsumer f$5;
                    private final /* synthetic */ VastTree f$6;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                        this.f$4 = r5;
                        this.f$5 = r6;
                        this.f$6 = r7;
                    }

                    public final void accept(Object obj) {
                        WrapperResolver.this.a(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, (ParseResult) obj);
                    }
                };
                Logger logger2 = logger;
                SomaApiContext somaApiContext2 = somaApiContext;
                wrapperLoader.a(logger, somaApiContext, wrapper, r0);
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Logger logger, SomaApiContext somaApiContext, a aVar, int i, NonNullConsumer nonNullConsumer, VastTree vastTree, ParseResult parseResult) {
        a aVar2 = aVar;
        ParseResult parseResult2 = parseResult;
        $$Lambda$WrapperResolver$lHlmBO8qCgEVYVLj93ylBQNt7o r2 = new NonNullConsumer(nonNullConsumer, vastTree, aVar) {
            private final /* synthetic */ NonNullConsumer f$1;
            private final /* synthetic */ VastTree f$2;
            private final /* synthetic */ a f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                WrapperResolver.this.a(this.f$1, this.f$2, this.f$3, (VastResult) obj);
            }
        };
        Builder builder = new Builder();
        HashSet hashSet = new HashSet();
        builder.setErrors(hashSet);
        if (!parseResult2.errors.isEmpty()) {
            hashSet.addAll(Lists.mapLazy(parseResult2.errors, $$Lambda$WrapperResolver$Y5pDSiDYuLXQlzShpscFqKIBpU0.INSTANCE));
            hashSet.remove(null);
        }
        if (parseResult2.value == null) {
            if (!parseResult2.errors.isEmpty()) {
                hashSet.add(Integer.valueOf(100));
            }
            r2.accept(builder.build());
            return;
        }
        a(logger, somaApiContext, (VastTree) parseResult2.value, ((Wrapper) aVar2.a).followAdditionalWrappers, i + 1, new NonNullConsumer(hashSet, builder, parseResult2, r2) {
            private final /* synthetic */ Set f$0;
            private final /* synthetic */ Builder f$1;
            private final /* synthetic */ ParseResult f$2;
            private final /* synthetic */ NonNullConsumer f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                WrapperResolver.a(this.f$0, this.f$1, this.f$2, this.f$3, (VastResult) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ Integer a(ParseError parseError) {
        Exception exc = parseError == null ? null : parseError.exception;
        if (exc instanceof GeneralWrapperErrorException) {
            return Integer.valueOf(300);
        }
        if (exc instanceof WrapperRequestTimeoutException) {
            return Integer.valueOf(301);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Set set, Builder builder, ParseResult parseResult, NonNullConsumer nonNullConsumer, VastResult vastResult) {
        set.addAll(vastResult.errors);
        if (vastResult.value != null) {
            builder.setResult(vastResult.value);
        } else {
            builder.setResult(parseResult.value);
        }
        nonNullConsumer.accept(builder.build());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(NonNullConsumer nonNullConsumer, VastTree vastTree, a aVar, VastResult vastResult) {
        Builder builder = new Builder();
        HashSet hashSet = new HashSet(vastResult.errors);
        builder.setErrors(hashSet);
        VastTree vastTree2 = (VastTree) vastResult.value;
        if (vastTree2 == null) {
            hashSet.add(Integer.valueOf(303));
            builder.setResult(vastTree);
        } else {
            builder.setResult(a.a(vastTree2, vastTree, aVar));
        }
        nonNullConsumer.accept(builder.build());
    }
}
