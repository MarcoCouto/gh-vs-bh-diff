package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.VastScenarioResult.Builder;
import com.smaato.sdk.video.vast.model.InLine;
import com.smaato.sdk.video.vast.model.VastRawScenario;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.model.Wrapper;
import java.util.HashSet;

public class VastScenarioPicker {
    @NonNull
    private final InLineAdContainerPicker a;
    @NonNull
    private final WrapperAdContainerPicker b;
    @NonNull
    private final VastScenarioWrapperMerger c;
    @NonNull
    private final VastScenarioMapper d;
    @NonNull
    private final VastScenarioWrapperMapper e;

    public VastScenarioPicker(@NonNull InLineAdContainerPicker inLineAdContainerPicker, @NonNull WrapperAdContainerPicker wrapperAdContainerPicker, @NonNull VastScenarioWrapperMerger vastScenarioWrapperMerger, @NonNull VastScenarioMapper vastScenarioMapper, @NonNull VastScenarioWrapperMapper vastScenarioWrapperMapper) {
        this.a = (InLineAdContainerPicker) Objects.requireNonNull(inLineAdContainerPicker, "Parameter inLineAdContainerPicker should be null for VastScenarioPicker::new");
        this.b = (WrapperAdContainerPicker) Objects.requireNonNull(wrapperAdContainerPicker, "Parameter wrapperAdContainerPicker should be null for VastScenarioPicker::new");
        this.c = (VastScenarioWrapperMerger) Objects.requireNonNull(vastScenarioWrapperMerger, "Parameter vastScenarioWrapperMerger should be null for VastScenarioPicker::new");
        this.d = (VastScenarioMapper) Objects.requireNonNull(vastScenarioMapper, "Parameter vastScenarioMapper should be null for VastScenarioPicker::new");
        this.e = (VastScenarioWrapperMapper) Objects.requireNonNull(vastScenarioWrapperMapper, "Parameter vastScenarioWrapperMapper should be null for VastScenarioPicker::new");
    }

    @NonNull
    public VastScenarioResult pickVastScenario(@NonNull Logger logger, @NonNull VastTree vastTree, @NonNull VastConfigurationSettings vastConfigurationSettings) {
        Objects.requireNonNull(logger, "Parameter logger should not be null for VastScenarioPicker::pickVastScenario");
        Objects.requireNonNull(vastTree, "Parameter vastTree should not be null for VastScenarioPicker::pickVastScenario");
        Objects.requireNonNull(vastConfigurationSettings, "Parameter vastConfigurationSettings should not be null for VastScenarioPicker::pickVastScenario");
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet(vastTree.errors);
        Builder errorUrls = new Builder().setErrors(hashSet).setErrorUrls(hashSet2);
        if (vastTree.ads.isEmpty()) {
            return errorUrls.build();
        }
        a a2 = InLineAdContainerPicker.a(vastTree.ads);
        if (a2 != null) {
            VastScenarioResult a3 = this.d.a(logger, (InLine) a2.a, vastConfigurationSettings);
            hashSet.addAll(a3.errors);
            hashSet2.addAll(a3.errorUrls);
            return errorUrls.setVastScenario(a3.vastScenario).build();
        }
        a a4 = WrapperAdContainerPicker.a(vastTree.ads);
        if (a4 != null) {
            Wrapper wrapper = (Wrapper) a4.a;
            hashSet2.addAll(wrapper.errors);
            if (wrapper.vastTree != null) {
                VastRawScenario a5 = this.e.a(logger, wrapper, vastConfigurationSettings);
                hashSet2.addAll(a5.errors);
                VastScenarioResult pickVastScenario = pickVastScenario(logger, wrapper.vastTree, vastConfigurationSettings);
                hashSet.addAll(pickVastScenario.errors);
                hashSet2.addAll(pickVastScenario.errorUrls);
                if (pickVastScenario.vastScenario != null) {
                    errorUrls.setVastScenario(this.c.a(pickVastScenario.vastScenario, a5, vastConfigurationSettings));
                }
            }
        }
        return errorUrls.build();
    }
}
