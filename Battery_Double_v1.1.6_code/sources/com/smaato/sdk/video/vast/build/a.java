package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Lists;
import com.smaato.sdk.core.util.fi.NullableFunction;
import com.smaato.sdk.video.vast.model.Ad;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.model.Wrapper;

class a<VastModel> {
    @NonNull
    public final VastModel a;
    @NonNull
    private Ad b;

    /* access modifiers changed from: private */
    public static /* synthetic */ Ad a(Ad ad, Ad ad2, Ad ad3) {
        return ad3 == ad ? ad2 : ad3;
    }

    @NonNull
    static VastTree a(@NonNull VastTree vastTree, @NonNull VastTree vastTree2, @NonNull a<Wrapper> aVar) {
        if (vastTree2.ads.contains(aVar.b)) {
            return vastTree2.newBuilder().setAds(Lists.mapLazy(vastTree2.ads, new NullableFunction(aVar.b.newBuilder().setWrapper(((Wrapper) aVar.a).newBuilder().setVastTree(vastTree).build()).build()) {
                private final /* synthetic */ Ad f$1;

                {
                    this.f$1 = r2;
                }

                public final Object apply(Object obj) {
                    return a.a(Ad.this, this.f$1, (Ad) obj);
                }
            })).build();
        }
        throw new IllegalArgumentException("parentVastTree parameter should contains same ad that passed in parentWrapperContainer. Wrong argument passed for WrapperMergeUtilsTest::mergeParsedResultWithParents");
    }

    a(Ad ad, VastModel vastmodel) {
        Objects.requireNonNull(ad, "Parameter ad cannot be null for AdContainer::new");
        Objects.requireNonNull(vastmodel, "Parameter model cannot be null for AdContainer::new");
        this.b = ad;
        this.a = vastmodel;
    }
}
