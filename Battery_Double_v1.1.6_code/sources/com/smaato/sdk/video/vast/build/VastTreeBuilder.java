package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.build.VastResult.Builder;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.parser.ParseResult;
import com.smaato.sdk.video.vast.parser.VastResponseParser;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class VastTreeBuilder {
    @NonNull
    private final VastResponseParser a;
    @NonNull
    private final WrapperResolver b;

    public VastTreeBuilder(@NonNull VastResponseParser vastResponseParser, @NonNull WrapperResolver wrapperResolver) {
        this.a = (VastResponseParser) Objects.requireNonNull(vastResponseParser);
        this.b = (WrapperResolver) Objects.requireNonNull(wrapperResolver);
    }

    public void buildVastTree(@NonNull Logger logger, @NonNull SomaApiContext somaApiContext, @NonNull InputStream inputStream, @Nullable String str, @NonNull NonNullConsumer<VastResult<VastTree>> nonNullConsumer) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(somaApiContext);
        Objects.requireNonNull(inputStream);
        this.a.parseVastResponse(logger, inputStream, str, new NonNullConsumer(nonNullConsumer, logger, somaApiContext) {
            private final /* synthetic */ NonNullConsumer f$1;
            private final /* synthetic */ Logger f$2;
            private final /* synthetic */ SomaApiContext f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void accept(Object obj) {
                VastTreeBuilder.this.a(this.f$1, this.f$2, this.f$3, (ParseResult) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(NonNullConsumer nonNullConsumer, Logger logger, SomaApiContext somaApiContext, ParseResult parseResult) {
        HashSet hashSet = new HashSet();
        VastTree vastTree = (VastTree) parseResult.value;
        if (vastTree != null) {
            if (vastTree.ads.isEmpty() && !parseResult.errors.isEmpty()) {
                nonNullConsumer.accept(VastResult.error(Collections.singleton(Integer.valueOf(100))));
            }
            this.b.a(logger, somaApiContext, vastTree, true, 0, new NonNullConsumer(hashSet, nonNullConsumer) {
                private final /* synthetic */ Set f$0;
                private final /* synthetic */ NonNullConsumer f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    VastTreeBuilder.a(this.f$0, this.f$1, (VastResult) obj);
                }
            });
            return;
        }
        nonNullConsumer.accept(VastResult.error(Collections.singleton(Integer.valueOf(100))));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Set set, NonNullConsumer nonNullConsumer, VastResult vastResult) {
        Builder builder = new Builder();
        set.addAll(vastResult.errors);
        builder.setErrors(set).setResult(vastResult.value);
        nonNullConsumer.accept(builder.build());
    }
}
