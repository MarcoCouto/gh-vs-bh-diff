package com.smaato.sdk.video.vast.build;

import androidx.annotation.Nullable;

public class VastConfigurationSettings {
    @Nullable
    public final String connectionType;
    public final int displayHeight;
    public final int displayWidth;

    public VastConfigurationSettings(int i, int i2, @Nullable String str) {
        this.displayWidth = i;
        this.displayHeight = i2;
        this.connectionType = str;
    }
}
