package com.smaato.sdk.video.vast.build.compare;

import android.support.graphics.drawable.PathInterpolatorCompat;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.VastConfigurationSettings;

public class AverageBitratePicker {
    @NonNull
    private final VastConfigurationSettings a;

    public AverageBitratePicker(@NonNull VastConfigurationSettings vastConfigurationSettings) {
        this.a = (VastConfigurationSettings) Objects.requireNonNull(vastConfigurationSettings, "configurationSettings can not be null in AverageBitratePicker");
    }

    public int getAverageBitrate() {
        int max = Math.max(this.a.displayHeight, this.a.displayWidth);
        if (max <= a.LOW.d) {
            return a.LOW.e;
        }
        if (max <= a.MEDIUM.d) {
            return a.MEDIUM.e;
        }
        return max <= a.HIGH.d ? a.HIGH.e : PathInterpolatorCompat.MAX_NUM_POINTS;
    }
}
