package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.collections.Sets;
import com.smaato.sdk.video.vast.model.MediaFile;
import java.util.Set;

public final class MediaFileResult {
    @NonNull
    public final Set<Integer> errors;
    @Nullable
    public final MediaFile mediaFile;

    public static class Builder {
        @Nullable
        private MediaFile a;
        @Nullable
        private Set<Integer> b;

        @NonNull
        public Builder setMediaFile(@Nullable MediaFile mediaFile) {
            this.a = mediaFile;
            return this;
        }

        @NonNull
        public Builder setErrors(@Nullable Set<Integer> set) {
            this.b = set;
            return this;
        }

        @NonNull
        public MediaFileResult build() {
            return new MediaFileResult(Sets.toImmutableSet(this.b), this.a, 0);
        }
    }

    /* synthetic */ MediaFileResult(Set set, MediaFile mediaFile2, byte b) {
        this(set, mediaFile2);
    }

    private MediaFileResult(@NonNull Set<Integer> set, @Nullable MediaFile mediaFile2) {
        this.errors = (Set) Objects.requireNonNull(set);
        this.mediaFile = mediaFile2;
    }
}
