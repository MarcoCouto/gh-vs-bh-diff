package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.Linear;
import com.smaato.sdk.video.vast.model.VastIconScenario;
import com.smaato.sdk.video.vast.model.VastRawMediaFileScenario;
import com.smaato.sdk.video.vast.model.VastRawMediaFileScenario.Builder;
import com.smaato.sdk.video.vast.model.VastScenarioCreativeData;
import com.smaato.sdk.video.vast.utils.VastVideoPlayerTimeConverterUtils;

public class VastMediaFileScenarioWrapperMapper {
    @NonNull
    private final VastIconScenarioPicker a;

    public VastMediaFileScenarioWrapperMapper(@NonNull VastIconScenarioPicker vastIconScenarioPicker) {
        this.a = (VastIconScenarioPicker) Objects.requireNonNull(vastIconScenarioPicker, "Parameter vastIconScenarioPicker should not be null for VastMediaFileScenarioMapper::new");
    }

    @NonNull
    public VastRawMediaFileScenario mapMediaFileScenario(@NonNull Logger logger, @NonNull Linear linear, @NonNull VastScenarioCreativeData vastScenarioCreativeData) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(vastScenarioCreativeData);
        VastIconScenario pickIconScenario = this.a.pickIconScenario(logger, linear.icons);
        long convertDurationStringToMilliseconds = VastVideoPlayerTimeConverterUtils.convertDurationStringToMilliseconds(linear.duration, logger);
        return new Builder().setVastScenarioCreativeData(vastScenarioCreativeData).setVastIconScenario(pickIconScenario).setTrackingEvents(linear.trackingEvents).setVideoClicks(linear.videoClicks).setAdParameters(linear.adParameters).setSkipOffset(VastVideoPlayerTimeConverterUtils.convertOffsetStringToMilliseconds(linear.skipOffset, convertDurationStringToMilliseconds, logger)).setDuration(convertDurationStringToMilliseconds).build();
    }
}
