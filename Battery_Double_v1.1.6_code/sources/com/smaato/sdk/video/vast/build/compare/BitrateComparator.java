package com.smaato.sdk.video.vast.build.compare;

import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.MediaFile;
import java.util.Comparator;

public class BitrateComparator implements Comparator<MediaFile> {
    private final int a;
    private final boolean b;

    public BitrateComparator(int i, boolean z) {
        this.a = i;
        this.b = z;
    }

    public int compare(@Nullable MediaFile mediaFile, @Nullable MediaFile mediaFile2) {
        if ((mediaFile == null) ^ (mediaFile2 == null)) {
            return mediaFile == null ? 1 : -1;
        }
        if (mediaFile == null) {
            return 0;
        }
        float f = 0.0f;
        float intValue = mediaFile.bitrate == null ? 0.0f : (float) mediaFile.bitrate.intValue();
        if (mediaFile2.bitrate != null) {
            f = (float) mediaFile2.bitrate.intValue();
        }
        if (this.b) {
            return Float.compare(Math.abs(((float) this.a) - intValue), Math.abs(((float) this.a) - f));
        }
        return Float.compare(intValue, f);
    }
}
