package com.smaato.sdk.video.vast.build;

import androidx.annotation.Nullable;
import com.smaato.sdk.video.vast.model.ViewableImpression;
import com.smaato.sdk.video.vast.model.ViewableImpression.Builder;

public class ViewableImpressionMerger {
    @Nullable
    public ViewableImpression merge(@Nullable ViewableImpression viewableImpression, @Nullable ViewableImpression viewableImpression2) {
        if (viewableImpression == null) {
            return viewableImpression2;
        }
        if (viewableImpression2 == null) {
            return viewableImpression;
        }
        return new Builder().setId(viewableImpression.id).setViewable(VastScenarioMergeUtils.merge(viewableImpression.viewable, viewableImpression2.viewable)).setNotViewable(VastScenarioMergeUtils.merge(viewableImpression.notViewable, viewableImpression2.notViewable)).setViewUndetermined(VastScenarioMergeUtils.merge(viewableImpression.viewUndetermined, viewableImpression2.viewUndetermined)).build();
    }
}
