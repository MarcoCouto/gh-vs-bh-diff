package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.Companion;
import com.smaato.sdk.video.vast.model.Creative;
import com.smaato.sdk.video.vast.model.VastRawScenario;
import com.smaato.sdk.video.vast.model.VastRawScenario.Builder;
import com.smaato.sdk.video.vast.model.VastScenarioCreativeData;
import com.smaato.sdk.video.vast.model.Wrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class VastScenarioWrapperMapper {
    @NonNull
    private final VastCompanionPicker a;
    @NonNull
    private final VastCompanionScenarioMapper b;
    @NonNull
    private final VastMediaFileScenarioWrapperMapper c;
    @NonNull
    private final VastScenarioCreativeDataMapper d;

    public VastScenarioWrapperMapper(@NonNull VastCompanionPicker vastCompanionPicker, @NonNull VastCompanionScenarioMapper vastCompanionScenarioMapper, @NonNull VastMediaFileScenarioWrapperMapper vastMediaFileScenarioWrapperMapper, @NonNull VastScenarioCreativeDataMapper vastScenarioCreativeDataMapper) {
        this.a = (VastCompanionPicker) Objects.requireNonNull(vastCompanionPicker, "Parameter vastCompanionPicker should be null for VastScenarioPicker::new");
        this.b = (VastCompanionScenarioMapper) Objects.requireNonNull(vastCompanionScenarioMapper, "Parameter vastCompanionScenarioMapper should be null for VastScenarioPicker::new");
        this.c = (VastMediaFileScenarioWrapperMapper) Objects.requireNonNull(vastMediaFileScenarioWrapperMapper, "Parameter vastMediaFileScenarioWrapperMapper should be null for VastScenarioPicker::new");
        this.d = (VastScenarioCreativeDataMapper) Objects.requireNonNull(vastScenarioCreativeDataMapper, "Parameter vastScenarioCreativeDataMapper should be null for VastScenarioPicker::new");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VastRawScenario a(@NonNull Logger logger, @NonNull Wrapper wrapper, @NonNull VastConfigurationSettings vastConfigurationSettings) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(vastConfigurationSettings);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (Creative creative : wrapper.creatives) {
            VastScenarioCreativeData mapVastScenarioCreativeData = this.d.mapVastScenarioCreativeData(creative);
            if (creative.linear != null) {
                arrayList.add(this.c.mapMediaFileScenario(logger, creative.linear, mapVastScenarioCreativeData));
            }
            if (creative.companionAds != null) {
                Companion pickCompanion = this.a.pickCompanion(creative.companionAds, vastConfigurationSettings);
                if (pickCompanion != null) {
                    arrayList2.add(this.b.mapVastCompanionScenario(logger, pickCompanion, mapVastScenarioCreativeData));
                }
            }
        }
        return new Builder().setAdSystem(wrapper.adSystem).setAdVerifications(wrapper.adVerifications).setImpressions(wrapper.impressions).setErrors(wrapper.errors).setViewableImpression(wrapper.viewableImpression).setVastCompanionScenarios(arrayList2).setVastMediaFileScenarios(arrayList).setBlockedAdCategories(wrapper.blockedAdCategories == null ? Collections.emptyList() : Arrays.asList(wrapper.blockedAdCategories.split("\\s*,\\s*"))).build();
    }
}
