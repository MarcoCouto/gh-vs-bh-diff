package com.smaato.sdk.video.vast.build.compare;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.MediaFile;
import java.util.Comparator;

public class MediaFileComparator implements Comparator<MediaFile> {
    @NonNull
    private final SizeComparator<MediaFile> a;
    @NonNull
    private final BitrateComparator b;

    public MediaFileComparator(@NonNull SizeComparator<MediaFile> sizeComparator, @NonNull BitrateComparator bitrateComparator) {
        this.a = (SizeComparator) Objects.requireNonNull(sizeComparator, "sizeComparator can not be null in MediaFileComparator");
        this.b = (BitrateComparator) Objects.requireNonNull(bitrateComparator, "bitrateComparator cannot be null in MediaFileComparator");
    }

    public int compare(@Nullable MediaFile mediaFile, @Nullable MediaFile mediaFile2) {
        int compare = this.a.compare(mediaFile, mediaFile2);
        return compare == 0 ? this.b.compare(mediaFile, mediaFile2) : compare;
    }
}
