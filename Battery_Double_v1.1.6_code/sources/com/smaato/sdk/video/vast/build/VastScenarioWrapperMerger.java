package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.UniversalAdId;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.model.VastRawScenario;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.model.VastScenario.Builder;

public class VastScenarioWrapperMerger {
    @NonNull
    private final VastMediaFileScenarioMerger a;
    @NonNull
    private final VastWrapperCompanionScenarioPicker b;
    @NonNull
    private final ViewableImpressionMerger c;
    @NonNull
    private final VastCompanionScenarioMerger d;

    public VastScenarioWrapperMerger(@NonNull VastMediaFileScenarioMerger vastMediaFileScenarioMerger, @NonNull VastWrapperCompanionScenarioPicker vastWrapperCompanionScenarioPicker, @NonNull VastCompanionScenarioMerger vastCompanionScenarioMerger, @NonNull ViewableImpressionMerger viewableImpressionMerger) {
        this.a = (VastMediaFileScenarioMerger) Objects.requireNonNull(vastMediaFileScenarioMerger, "Parameter vastMediaFileScenarioMerger should be null for VastScenarioWrapperMerger::new");
        this.b = (VastWrapperCompanionScenarioPicker) Objects.requireNonNull(vastWrapperCompanionScenarioPicker, "Parameter wrapperCompanionScenarioPicker should be null for VastScenarioWrapperMerger::new");
        this.d = (VastCompanionScenarioMerger) Objects.requireNonNull(vastCompanionScenarioMerger, "Parameter vastCompanionScenarioMerger should be null for VastScenarioWrapperMerger::new");
        this.c = (ViewableImpressionMerger) Objects.requireNonNull(viewableImpressionMerger, "Parameter viewableImpressionMerger should be null for VastScenarioWrapperMerger::new");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VastScenario a(@NonNull VastScenario vastScenario, @NonNull VastRawScenario vastRawScenario, @NonNull VastConfigurationSettings vastConfigurationSettings) {
        VastCompanionScenario vastCompanionScenario;
        Builder blockedAdCategories = vastScenario.newBuilder().setImpressions(VastScenarioMergeUtils.merge(vastScenario.impressions, vastRawScenario.impressions)).setAdVerifications(VastScenarioMergeUtils.merge(vastScenario.adVerifications, vastRawScenario.adVerifications)).setCategories(VastScenarioMergeUtils.merge(vastScenario.categories, vastRawScenario.categories)).setErrors(VastScenarioMergeUtils.merge(vastScenario.errors, vastRawScenario.errors)).setViewableImpression(this.c.merge(vastScenario.viewableImpression, vastRawScenario.viewableImpression)).setBlockedAdCategories(VastScenarioMergeUtils.merge(vastScenario.blockedAdCategories, vastRawScenario.blockedAdCategories));
        VastMediaFileScenario vastMediaFileScenario = vastScenario.vastMediaFileScenario;
        blockedAdCategories.setVastMediaFileScenario(this.a.merge(vastMediaFileScenario, vastRawScenario.vastRawMediaFileScenarios));
        UniversalAdId universalAdId = vastMediaFileScenario.vastScenarioCreativeData.universalAdId;
        VastCompanionScenario vastCompanionScenario2 = vastScenario.vastCompanionScenario;
        if (vastCompanionScenario2 == null) {
            vastCompanionScenario = this.b.pickWrapperCompanionScenario(universalAdId, vastRawScenario.vastCompanionScenarios, vastConfigurationSettings);
        } else {
            vastCompanionScenario = this.d.merge(vastCompanionScenario2, vastRawScenario.vastCompanionScenarios);
        }
        if (vastCompanionScenario != null) {
            blockedAdCategories.setVastCompanionScenario(vastCompanionScenario);
        }
        return blockedAdCategories.build();
    }
}
