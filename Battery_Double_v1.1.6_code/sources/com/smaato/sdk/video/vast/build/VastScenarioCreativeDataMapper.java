package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.model.Creative;
import com.smaato.sdk.video.vast.model.VastScenarioCreativeData;
import com.smaato.sdk.video.vast.model.VastScenarioCreativeData.Builder;

public class VastScenarioCreativeDataMapper {
    @NonNull
    public VastScenarioCreativeData mapVastScenarioCreativeData(@NonNull Creative creative) {
        return new Builder().setUniversalAdId(creative.universalAdId).setAdId(creative.adId).setId(creative.id).setApiFramework(creative.apiFramework).setSequence(creative.sequence).build();
    }
}
