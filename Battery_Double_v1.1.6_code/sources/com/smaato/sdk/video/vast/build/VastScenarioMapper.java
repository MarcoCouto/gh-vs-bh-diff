package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.VastScenarioResult.Builder;
import com.smaato.sdk.video.vast.build.compare.AverageBitratePicker;
import com.smaato.sdk.video.vast.build.compare.BitrateComparator;
import com.smaato.sdk.video.vast.build.compare.MediaFileComparator;
import com.smaato.sdk.video.vast.build.compare.SizeComparator;
import com.smaato.sdk.video.vast.model.Companion;
import com.smaato.sdk.video.vast.model.Creative;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.smaato.sdk.video.vast.model.InLine;
import com.smaato.sdk.video.vast.model.Linear;
import com.smaato.sdk.video.vast.model.MediaFile;
import com.smaato.sdk.video.vast.model.VastCompanionScenario;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.model.VastScenarioCreativeData;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class VastScenarioMapper {
    @NonNull
    private final VastLinearMediaFilePicker a;
    @NonNull
    private final VastCompanionPicker b;
    @NonNull
    private final VastCompanionScenarioMapper c;
    @NonNull
    private final VastMediaFileScenarioMapper d;
    @NonNull
    private final VastScenarioCreativeDataMapper e;

    static final class a {
        @NonNull
        public final Creative a;
        @NonNull
        public final Linear b;

        /* synthetic */ a(Creative creative, Linear linear, byte b2) {
            this(creative, linear);
        }

        private a(@NonNull Creative creative, @NonNull Linear linear) {
            this.a = (Creative) Objects.requireNonNull(creative);
            this.b = (Linear) Objects.requireNonNull(linear);
        }
    }

    public VastScenarioMapper(@NonNull VastLinearMediaFilePicker vastLinearMediaFilePicker, @NonNull VastCompanionPicker vastCompanionPicker, @NonNull VastCompanionScenarioMapper vastCompanionScenarioMapper, @NonNull VastMediaFileScenarioMapper vastMediaFileScenarioMapper, @NonNull VastScenarioCreativeDataMapper vastScenarioCreativeDataMapper) {
        this.a = (VastLinearMediaFilePicker) Objects.requireNonNull(vastLinearMediaFilePicker, "Parameter vastLinearMediaFilePicker should be null for VastScenarioPicker::new");
        this.b = (VastCompanionPicker) Objects.requireNonNull(vastCompanionPicker, "Parameter vastCompanionPicker should be null for VastScenarioPicker::new");
        this.c = (VastCompanionScenarioMapper) Objects.requireNonNull(vastCompanionScenarioMapper, "Parameter vastCompanionScenarioMapper should be null for VastScenarioPicker::new");
        this.d = (VastMediaFileScenarioMapper) Objects.requireNonNull(vastMediaFileScenarioMapper, "Parameter vastMediaFileScenarioMapper should be null for VastScenarioPicker::new");
        this.e = (VastScenarioCreativeDataMapper) Objects.requireNonNull(vastScenarioCreativeDataMapper, "Parameter vastScenarioCreativeDataMapper should be null for VastScenarioPicker::new");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VastScenarioResult a(@NonNull Logger logger, @NonNull InLine inLine, @NonNull VastConfigurationSettings vastConfigurationSettings) {
        Objects.requireNonNull(logger);
        Objects.requireNonNull(vastConfigurationSettings);
        HashSet hashSet = new HashSet();
        Builder errorUrls = new Builder().setErrors(hashSet).setErrorUrls(new HashSet(inLine.errors));
        TreeMap treeMap = new TreeMap(new MediaFileComparator(new SizeComparator(vastConfigurationSettings), new BitrateComparator(new AverageBitratePicker(vastConfigurationSettings).getAverageBitrate(), "wifi".equalsIgnoreCase(vastConfigurationSettings.connectionType))));
        Set a2 = a(inLine, vastConfigurationSettings, treeMap);
        if (treeMap.isEmpty()) {
            if (a2.isEmpty()) {
                hashSet.add(Integer.valueOf(ErrorCode.GENERAL_LINEAR_ERROR));
            } else {
                hashSet.addAll(a2);
            }
            return errorUrls.build();
        }
        Entry firstEntry = treeMap.firstEntry();
        Creative creative = ((a) firstEntry.getValue()).a;
        Linear linear = ((a) firstEntry.getValue()).b;
        MediaFile mediaFile = (MediaFile) firstEntry.getKey();
        VastScenarioCreativeData mapVastScenarioCreativeData = this.e.mapVastScenarioCreativeData(creative);
        VastMediaFileScenario a3 = this.d.a(logger, mediaFile, linear, mapVastScenarioCreativeData);
        VastCompanionScenario vastCompanionScenario = null;
        Companion pickCompanion = creative.companionAds != null ? this.b.pickCompanion(creative.companionAds, vastConfigurationSettings) : null;
        if (pickCompanion == null) {
            pickCompanion = this.b.pickCompanion(inLine.creatives, vastConfigurationSettings);
        }
        if (pickCompanion != null) {
            vastCompanionScenario = this.c.mapVastCompanionScenario(logger, pickCompanion, mapVastScenarioCreativeData);
        } else if (creative.hasCompanions()) {
            hashSet.add(Integer.valueOf(600));
        }
        return errorUrls.setVastScenario(new VastScenario.Builder().setAdSystem(inLine.adSystem).setAdTitle(inLine.adTitle).setAdVerifications(inLine.adVerifications).setAdvertiser(inLine.advertiser).setCategories(inLine.categories).setDescription(inLine.description).setErrors(inLine.errors).setImpressions(inLine.impressions).setViewableImpression(inLine.viewableImpression).setVastMediaFileScenario(a3).setVastCompanionScenario(vastCompanionScenario).setAdServingId(inLine.adServingId).build()).build();
    }

    private Set<Integer> a(@NonNull InLine inLine, @NonNull VastConfigurationSettings vastConfigurationSettings, @NonNull TreeMap<MediaFile, a> treeMap) {
        HashSet hashSet = new HashSet();
        Iterator it = inLine.creatives.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Creative creative = (Creative) it.next();
            Linear linear = creative.linear;
            if (linear != null && !linear.mediaFiles.isEmpty()) {
                MediaFileResult a2 = VastLinearMediaFilePicker.a(linear.mediaFiles, vastConfigurationSettings);
                if (a2.mediaFile != null) {
                    treeMap.put(a2.mediaFile, new a(creative, linear, 0));
                    break;
                }
                hashSet.addAll(a2.errors);
            }
        }
        return hashSet;
    }
}
