package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.model.Ad;
import java.util.List;

public class InLineChecker {
    static boolean a(@NonNull List<Ad> list) {
        if (list.size() == 1) {
            return ((Ad) list.get(0)).inLine != null;
        }
        for (Ad ad : list) {
            if (ad.inLine != null && ad.sequence == null) {
                return true;
            }
        }
        return false;
    }
}
