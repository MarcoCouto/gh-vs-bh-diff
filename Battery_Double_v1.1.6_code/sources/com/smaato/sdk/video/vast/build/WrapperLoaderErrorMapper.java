package com.smaato.sdk.video.vast.build;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.video.vast.exceptions.wrapper.GeneralWrapperErrorException;
import com.smaato.sdk.video.vast.exceptions.wrapper.WrapperRequestTimeoutException;

public class WrapperLoaderErrorMapper {
    @NonNull
    static Exception a(@NonNull Error error, @NonNull String str) {
        switch (error) {
            case TIMEOUT:
            case IO_ERROR:
            case IO_TOO_MANY_REDIRECTS:
            case NO_NETWORK_CONNECTION:
                return new WrapperRequestTimeoutException(str);
            default:
                return new GeneralWrapperErrorException(str);
        }
    }
}
