package com.smaato.sdk.video.vast.browser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.UrlCreator;
import com.smaato.sdk.core.util.Objects;

public class VastWebComponentSecurityPolicy {
    @NonNull
    private final Logger a;
    @NonNull
    private final String b;
    @NonNull
    private final UrlCreator c;

    public VastWebComponentSecurityPolicy(@NonNull Logger logger, @NonNull String str, @NonNull UrlCreator urlCreator) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (String) Objects.requireNonNull(str);
        this.c = (UrlCreator) Objects.requireNonNull(urlCreator);
    }

    public boolean validateUrl(@Nullable SomaApiContext somaApiContext, @NonNull String str) {
        if ((somaApiContext == null && str.startsWith(this.b)) || !this.c.isSupportedForNetworking(str)) {
            return true;
        }
        String extractScheme = this.c.extractScheme(str);
        boolean z = this.c.isSecureScheme(extractScheme) || (this.c.isInsecureScheme(extractScheme) && somaApiContext != null && !somaApiContext.isHttpsOnly());
        if (!z) {
            this.a.error(LogDomain.VAST, "Invalid url or violation of httpsOnly rule: Url: %s , SomaApiContext: %s", str, somaApiContext);
        }
        return z;
    }
}
