package com.smaato.sdk.video.network;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.network.exception.TaskCancelledException;
import com.smaato.sdk.core.network.execution.ErrorMapper;
import com.smaato.sdk.core.network.execution.TaskStepResult;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.parser.ParseResult;
import com.smaato.sdk.video.vast.parser.VastResponseParser;
import java.io.ByteArrayInputStream;

public class VastParsingConsumerFactory {
    @NonNull
    private final Logger a;
    @NonNull
    private final VastResponseParser b;
    @NonNull
    private final ErrorMapper<Error> c;

    public VastParsingConsumerFactory(@NonNull Logger logger, @NonNull VastResponseParser vastResponseParser, @NonNull ErrorMapper<Error> errorMapper) {
        this.a = logger;
        this.b = vastResponseParser;
        this.c = errorMapper;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final Consumer<TaskStepResult<a, Exception>> a(@NonNull Task task, @NonNull Listener<ParseResult<VastTree>, Error> listener) {
        return new Consumer(listener, task) {
            private final /* synthetic */ Listener f$1;
            private final /* synthetic */ Task f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                VastParsingConsumerFactory.this.a(this.f$1, this.f$2, (TaskStepResult) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener, Task task, TaskStepResult taskStepResult) {
        if (taskStepResult.success != null) {
            this.b.parseVastResponse(this.a, new ByteArrayInputStream(((a) taskStepResult.success).a), ((a) taskStepResult.success).b, new NonNullConsumer(task) {
                private final /* synthetic */ Task f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    Listener.this.onSuccess(this.f$1, (ParseResult) obj);
                }
            });
        } else if (taskStepResult.isCancelled) {
            listener.onFailure(task, this.c.map(new TaskCancelledException()));
        } else if (taskStepResult.error != null) {
            listener.onFailure(task, this.c.map((Exception) taskStepResult.error));
        } else {
            this.a.error(LogDomain.NETWORK, "Network Task finished in unexpected state: %s", taskStepResult);
            listener.onFailure(task, this.c.map(new Exception("Generic")));
        }
    }
}
