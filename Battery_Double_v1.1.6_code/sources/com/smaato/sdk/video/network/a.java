package com.smaato.sdk.video.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.util.Objects;

final class a {
    @NonNull
    public final byte[] a;
    @Nullable
    public final String b;

    a(@NonNull byte[] bArr, @Nullable String str) {
        Objects.requireNonNull(bArr, "Parameter responseBody cannot be null for BodyEncodingPair::new");
        this.a = bArr;
        this.b = str;
    }
}
