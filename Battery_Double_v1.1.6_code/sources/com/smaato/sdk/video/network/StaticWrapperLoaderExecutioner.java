package com.smaato.sdk.video.network;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.Task;
import com.smaato.sdk.core.Task.Listener;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.network.NetworkClient.Error;
import com.smaato.sdk.core.network.NetworkHttpRequest.Builder;
import com.smaato.sdk.core.network.NetworkRequest.Method;
import com.smaato.sdk.core.network.execution.Executioner;
import com.smaato.sdk.core.network.execution.NetworkActions;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.parser.ParseResult;
import java.util.concurrent.ExecutorService;

public class StaticWrapperLoaderExecutioner implements Executioner<String, ParseResult<VastTree>, Error> {
    @NonNull
    private final ExecutorService a;
    @NonNull
    private final VastParsingConsumerFactory b;
    @NonNull
    private final NetworkActions c;

    public StaticWrapperLoaderExecutioner(@NonNull NetworkActions networkActions, @NonNull VastParsingConsumerFactory vastParsingConsumerFactory, @NonNull ExecutorService executorService) {
        this.c = (NetworkActions) Objects.requireNonNull(networkActions, "Parameter networkActions cannot be null for StaticWrapperLoaderExecutioner::new");
        this.a = (ExecutorService) Objects.requireNonNull(executorService, "Parameter executorService cannot be null for StaticWrapperLoaderExecutioner::new");
        this.b = (VastParsingConsumerFactory) Objects.requireNonNull(vastParsingConsumerFactory, "Parameter vastParsingConsumerFactory cannot be null for StaticWrapperLoaderExecutioner::new");
    }

    @NonNull
    public Task submitRequest(@NonNull String str, @Nullable SomaApiContext somaApiContext, @NonNull Listener<ParseResult<VastTree>, Error> listener) {
        Objects.requireNonNull(somaApiContext, "Cannot load Vast Wrapper without SomaApiContext");
        b bVar = new b(this.c, new Builder().setMethod(Method.GET).setUrl(str).build(), this.b, this.a, somaApiContext, listener);
        return bVar;
    }
}
