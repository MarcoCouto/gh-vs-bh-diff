package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdObject;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.util.Objects;

final class c implements AdObject {
    @NonNull
    private final SomaApiContext a;

    c(@NonNull SomaApiContext somaApiContext) {
        this.a = (SomaApiContext) Objects.requireNonNull(somaApiContext);
    }

    @NonNull
    public final SomaApiContext getSomaApiContext() {
        return this.a;
    }
}
