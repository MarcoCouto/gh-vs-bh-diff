package com.smaato.sdk.video.ad;

import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdInteractor.TtlListener;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.ad.BaseAdPresenter;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker.VideoProps;
import com.smaato.sdk.core.analytics.ViewabilityVerificationResource;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Metadata;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.StateMachine.Listener;
import com.smaato.sdk.video.vast.player.VastVideoPlayer;
import com.smaato.sdk.video.vast.player.VastVideoPlayer.EventListener;
import com.smaato.sdk.video.vast.player.VideoTimings;
import java.util.List;
import java.util.Map;

abstract class d extends BaseAdPresenter implements AdPresenter {
    @NonNull
    private final VastVideoPlayer a;
    /* access modifiers changed from: private */
    @NonNull
    public final b b;
    /* access modifiers changed from: private */
    @NonNull
    public final VideoViewabilityTracker c;
    /* access modifiers changed from: private */
    @NonNull
    public final VideoTimings d;
    /* access modifiers changed from: private */
    @NonNull
    public final Map<String, List<ViewabilityVerificationResource>> e;
    @NonNull
    private final EventListener f = new EventListener() {
        public final void onStart(float f, float f2) {
            d.this.b.onEvent(Event.ADDED_ON_SCREEN);
            d.this.c.trackStarted(f, f2);
        }

        public final void onMute() {
            d.this.c.trackPlayerVolumeChanged(0.0f);
        }

        public final void onUnmute() {
            d.this.c.trackPlayerVolumeChanged(1.0f);
        }

        public final void onSkipped() {
            d.this.c.trackSkipped();
        }

        public final void onPaused() {
            d.this.c.trackPaused();
        }

        public final void onResumed() {
            d.this.c.trackResumed();
        }

        public final void onFirstQuartile() {
            d.this.c.trackFirstQuartile();
        }

        public final void onMidPoint() {
            d.this.c.trackMidPoint();
        }

        public final void onThirdQuartile() {
            d.this.c.trackThirdQuartile();
        }

        public final void onClose() {
            d.this.b.addStateListener(new Listener() {
                public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                    AnonymousClass1.this.b((State) obj, (State) obj2, metadata);
                }
            });
            d.this.b.onEvent(Event.CLOSE);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void b(State state, State state2, Metadata metadata) {
            if (state2 == State.TO_BE_DELETED) {
                d.this.e();
            }
        }

        public final void onComplete() {
            d.this.c();
            d.this.c.trackCompleted();
        }

        public final void onCompanionShown() {
            d.this.h();
        }

        public final void onVideoImpression() {
            d.this.b.onEvent(Event.IMPRESSION);
        }

        public final void onAdClick() {
            d.this.b.onEvent(Event.CLICK);
        }

        public final void onAdError() {
            d.this.b.addStateListener(new Listener() {
                public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                    AnonymousClass1.this.a((State) obj, (State) obj2, metadata);
                }
            });
            d.this.b.onEvent(Event.AD_ERROR);
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(State state, State state2, Metadata metadata) {
            if (state2 == State.TO_BE_DELETED) {
                d.this.f();
            }
        }
    };
    @NonNull
    private final Listener<State> g = new Listener() {
        public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
            d.this.b((State) obj, (State) obj2, metadata);
        }
    };
    @NonNull
    private TtlListener h = new TtlListener() {
        public final void onTTLExpired(AdInteractor adInteractor) {
            d.this.a(adInteractor);
        }
    };

    /* access modifiers changed from: 0000 */
    public abstract void a();

    /* access modifiers changed from: 0000 */
    public abstract void b();

    /* access modifiers changed from: 0000 */
    public abstract void c();

    /* access modifiers changed from: 0000 */
    public abstract void d();

    /* access modifiers changed from: 0000 */
    public abstract void e();

    /* access modifiers changed from: 0000 */
    public abstract void f();

    /* access modifiers changed from: 0000 */
    public abstract void g();

    /* access modifiers changed from: 0000 */
    public abstract void h();

    /* access modifiers changed from: private */
    public /* synthetic */ void a(AdInteractor adInteractor) {
        d();
    }

    d(@NonNull VastVideoPlayer vastVideoPlayer, @NonNull b bVar, @NonNull VideoViewabilityTracker videoViewabilityTracker, @NonNull VideoTimings videoTimings, @NonNull Map<String, List<ViewabilityVerificationResource>> map) {
        super(bVar);
        this.a = (VastVideoPlayer) Objects.requireNonNull(vastVideoPlayer);
        this.b = (b) Objects.requireNonNull(bVar);
        this.c = (VideoViewabilityTracker) Objects.requireNonNull(videoViewabilityTracker);
        this.d = (VideoTimings) Objects.requireNonNull(videoTimings);
        this.e = (Map) Objects.requireNonNull(map);
        this.a.setEventListener(this.f);
        bVar.addStateListener(this.g);
        bVar.addTtlListener(this.h);
        bVar.onEvent(Event.INITIALISE);
    }

    @NonNull
    public AdContentView getAdContentView(@NonNull Context context) {
        AdContentView newVideoPlayerView = this.a.getNewVideoPlayerView(context);
        newVideoPlayerView.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(@NonNull View view) {
                d.this.c.registerAdView(view, d.this.e);
                d.this.c.startTracking();
                d.this.c.trackPlayerStateChange();
                d.this.c.trackLoaded(d.this.d.isVideoSkippable ? VideoProps.buildForSkippableVideo((float) d.this.d.skipOffsetMillis) : VideoProps.buildForNonSkippableVideo());
            }

            public final void onViewDetachedFromWindow(@NonNull View view) {
                view.removeOnAttachStateChangeListener(this);
                d.this.c.stopTracking();
            }
        });
        return newVideoPlayerView;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.removeStateListener(this.g);
        this.b.addStateListener(new Listener() {
            public final void onStateChanged(Object obj, Object obj2, Metadata metadata) {
                d.this.a((State) obj, (State) obj2, metadata);
            }
        });
        this.b.onEvent(Event.CLOSE);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(State state, State state2, Metadata metadata) {
        if (state2 == State.TO_BE_DELETED) {
            e();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(State state, State state2, Metadata metadata) {
        switch (state2) {
            case INIT:
            case CREATED:
            case COMPLETE:
            case TO_BE_DELETED:
                return;
            case ON_SCREEN:
                a();
                return;
            case IMPRESSED:
                b();
                this.c.trackImpression();
                return;
            case CLICKED:
                g();
                this.c.trackVideoClicked();
                return;
            default:
                StringBuilder sb = new StringBuilder("Unexpected state for RewardedVideoAdPresenter ");
                sb.append(state2);
                throw new IllegalStateException(sb.toString());
        }
    }
}
