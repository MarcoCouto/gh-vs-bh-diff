package com.smaato.sdk.video.ad;

import com.smaato.sdk.core.util.collections.Iterables;
import com.smaato.sdk.core.util.fi.BiFunction;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.model.Verification;
import java.util.HashMap;

/* renamed from: com.smaato.sdk.video.ad.-$$Lambda$VerificationResourceMapper$ouTqEZPLOohKTSBRDNbPs5fmi78 reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$VerificationResourceMapper$ouTqEZPLOohKTSBRDNbPs5fmi78 implements BiFunction {
    public static final /* synthetic */ $$Lambda$VerificationResourceMapper$ouTqEZPLOohKTSBRDNbPs5fmi78 INSTANCE = new $$Lambda$VerificationResourceMapper$ouTqEZPLOohKTSBRDNbPs5fmi78();

    private /* synthetic */ $$Lambda$VerificationResourceMapper$ouTqEZPLOohKTSBRDNbPs5fmi78() {
    }

    public final Object apply(Object obj, Object obj2) {
        return Iterables.forEach(((Verification) obj).javaScriptResources, new Consumer((HashMap) obj2, (Verification) obj) {
            private final /* synthetic */ HashMap f$0;
            private final /* synthetic */ Verification f$1;

            public final 
/*
Method generation error in method: com.smaato.sdk.video.ad.-$$Lambda$VerificationResourceMapper$VrFTmmhKcNtCspS7Cl8hfH3CTCA.accept(java.lang.Object):null, dex: classes3.dex
            java.lang.NullPointerException
            	at jadx.core.codegen.ClassGen.useType(ClassGen.java:442)
            	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:109)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:311)
            	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
            	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
            	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:661)
            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:595)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:353)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:773)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:713)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.inlineMethod(InsnGen.java:896)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:669)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:95)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:469)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.inlineMethod(InsnGen.java:896)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:669)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:357)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:223)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:105)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:303)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:239)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:213)
            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:210)
            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:203)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:316)
            	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:262)
            	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:225)
            	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
            	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:76)
            	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
            	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:32)
            	at jadx.core.codegen.CodeGen.generate(CodeGen.java:20)
            	at jadx.core.ProcessClass.process(ProcessClass.java:36)
            	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
            	at jadx.api.JavaClass.decompile(JavaClass.java:62)
            	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
            
*/
        });
    }
}
