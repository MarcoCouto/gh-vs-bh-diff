package com.smaato.sdk.video.ad;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter.Listener;
import com.smaato.sdk.core.ad.RewardedAdPresenter.OnCloseEnabledListener;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.analytics.ViewabilityVerificationResource;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.player.VastVideoPlayer;
import com.smaato.sdk.video.vast.player.VideoTimings;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

public class RewardedVideoAdPresenter extends d implements RewardedAdPresenter {
    @NonNull
    private final VastVideoPlayer a;
    @NonNull
    private WeakReference<Listener> b = new WeakReference<>(null);
    @NonNull
    private WeakReference<OnCloseEnabledListener> c = new WeakReference<>(null);

    @NonNull
    public /* bridge */ /* synthetic */ AdContentView getAdContentView(@NonNull Context context) {
        return super.getAdContentView(context);
    }

    RewardedVideoAdPresenter(@NonNull VastVideoPlayer vastVideoPlayer, @NonNull b bVar, @NonNull VideoViewabilityTracker videoViewabilityTracker, @NonNull VideoTimings videoTimings, @NonNull Map<String, List<ViewabilityVerificationResource>> map) {
        super(vastVideoPlayer, bVar, videoViewabilityTracker, videoTimings, map);
        this.a = vastVideoPlayer;
    }

    public void setListener(@Nullable Listener listener) {
        this.b = new WeakReference<>(listener);
    }

    public void setOnCloseEnabledListener(@Nullable OnCloseEnabledListener onCloseEnabledListener) {
        this.c = new WeakReference<>(onCloseEnabledListener);
    }

    public void onCloseClicked() {
        this.a.onCloseClicked();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void g(Listener listener) {
        listener.onStart(this);
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.g((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void f(Listener listener) {
        listener.onAdImpressed(this);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.f((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(Listener listener) {
        listener.onCompleted(this);
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.e((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Listener listener) {
        listener.onTTLExpired(this);
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.d((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onClose(this);
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.c((Listener) obj);
            }
        });
        Objects.onNotNull(this.c.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.b((OnCloseEnabledListener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(OnCloseEnabledListener onCloseEnabledListener) {
        onCloseEnabledListener.onClose(this);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdError(this);
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.b((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.a((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(OnCloseEnabledListener onCloseEnabledListener) {
        onCloseEnabledListener.onCloseEnabled(this);
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        Objects.onNotNull(this.c.get(), new Consumer() {
            public final void accept(Object obj) {
                RewardedVideoAdPresenter.this.a((OnCloseEnabledListener) obj);
            }
        });
    }
}
