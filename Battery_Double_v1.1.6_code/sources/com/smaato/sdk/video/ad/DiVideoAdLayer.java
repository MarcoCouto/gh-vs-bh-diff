package com.smaato.sdk.video.ad;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.RewardedAdPresenter;
import com.smaato.sdk.core.analytics.Analytics;
import com.smaato.sdk.core.di.ClassFactory;
import com.smaato.sdk.core.di.DiConstructor;
import com.smaato.sdk.core.di.DiRegistry;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.framework.VideoDiNames;
import com.smaato.sdk.video.vast.build.VastScenarioPicker;
import com.smaato.sdk.video.vast.build.VastTreeBuilder;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.player.VastVideoPlayerCreator;
import com.smaato.sdk.video.vast.player.VideoTimings;

public final class DiVideoAdLayer {

    private static class a implements Function<VastMediaFileScenario, VideoTimings> {
        private a() {
        }

        /* synthetic */ a(byte b) {
            this();
        }

        @NonNull
        public /* synthetic */ Object apply(@NonNull Object obj) {
            return VideoTimings.create((VastMediaFileScenario) obj, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, true, true);
        }
    }

    private static class b implements Function<VastMediaFileScenario, VideoTimings> {
        private b() {
        }

        /* synthetic */ b(byte b) {
            this();
        }

        @NonNull
        public /* synthetic */ Object apply(@NonNull Object obj) {
            return VideoTimings.create((VastMediaFileScenario) obj, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, false, false);
        }
    }

    private interface c extends Function<c, b> {
    }

    private DiVideoAdLayer() {
    }

    @NonNull
    public static DiRegistry createRegistry(@NonNull AdPresenterNameShaper adPresenterNameShaper, @NonNull String str) {
        Objects.requireNonNull(adPresenterNameShaper);
        Objects.requireNonNull(str);
        return DiRegistry.of(new Consumer(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final void accept(Object obj) {
                DiVideoAdLayer.a(AdPresenterNameShaper.this, this.f$1, (DiRegistry) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(AdPresenterNameShaper adPresenterNameShaper, String str, DiRegistry diRegistry) {
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.VIDEO, RewardedAdPresenter.class), AdPresenterBuilder.class, $$Lambda$DiVideoAdLayer$8hU3q60x27xlQZA6g2OZltSCAOU.INSTANCE);
        diRegistry.registerFactory(adPresenterNameShaper.shapeName(AdFormat.VIDEO, InterstitialAdPresenter.class), AdPresenterBuilder.class, $$Lambda$DiVideoAdLayer$eMOlZQbxKa1aQBsOFsyWxkdBks.INSTANCE);
        diRegistry.registerFactory(c.class, new ClassFactory(str) {
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final Object get(DiConstructor diConstructor) {
                return DiVideoAdLayer.a(this.f$0, diConstructor);
            }
        });
        diRegistry.registerFactory(a.class, $$Lambda$DiVideoAdLayer$zgfWk8t5Syhq_Ig6BeNeB1zu_c.INSTANCE);
        diRegistry.registerFactory(b.class, $$Lambda$DiVideoAdLayer$ML7YolgxXNXRMYOR_RPwxbQo3k.INSTANCE);
        diRegistry.registerFactory(VerificationResourceMapper.class, $$Lambda$DiVideoAdLayer$xeF6SuiikH7_0g0OOVlmXXXgmB8.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder f(DiConstructor diConstructor) {
        RewardedVideoAdPresenterBuilder rewardedVideoAdPresenterBuilder = new RewardedVideoAdPresenterBuilder((Logger) diConstructor.get(Logger.class), (Function) diConstructor.get(c.class), (VastScenarioPicker) diConstructor.get(VastScenarioPicker.class), (VastTreeBuilder) diConstructor.get(VastTreeBuilder.class), (VastVideoPlayerCreator) diConstructor.get(VastVideoPlayerCreator.class), a(diConstructor), (VastErrorTrackerCreator) diConstructor.get(VastErrorTrackerCreator.class), (MediaFileResourceLoaderListenerCreator) diConstructor.get(MediaFileResourceLoaderListenerCreator.class), ((Analytics) diConstructor.get(Analytics.class)).getVideoTracker(), (Function) diConstructor.get(b.class), (VerificationResourceMapper) diConstructor.get(VerificationResourceMapper.class));
        return rewardedVideoAdPresenterBuilder;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ AdPresenterBuilder e(DiConstructor diConstructor) {
        a aVar = new a((Logger) diConstructor.get(Logger.class), (Function) diConstructor.get(c.class), (VastScenarioPicker) diConstructor.get(VastScenarioPicker.class), (VastTreeBuilder) diConstructor.get(VastTreeBuilder.class), (VastVideoPlayerCreator) diConstructor.get(VastVideoPlayerCreator.class), a(diConstructor), (VastErrorTrackerCreator) diConstructor.get(VastErrorTrackerCreator.class), (MediaFileResourceLoaderListenerCreator) diConstructor.get(MediaFileResourceLoaderListenerCreator.class), ((Analytics) diConstructor.get(Analytics.class)).getVideoTracker(), (Function) diConstructor.get(a.class), (VerificationResourceMapper) diConstructor.get(VerificationResourceMapper.class));
        return aVar;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ c a(String str, DiConstructor diConstructor) {
        return new c(str) {
            private final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            public final Object apply(Object obj) {
                return DiVideoAdLayer.a(DiConstructor.this, this.f$1, (c) obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b a(DiConstructor diConstructor, String str, c cVar) {
        return new b((Logger) diConstructor.get(Logger.class), cVar, (StateMachine) diConstructor.get(str, StateMachine.class), (OneTimeActionFactory) diConstructor.get(OneTimeActionFactory.class));
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ a d(DiConstructor diConstructor) {
        return new a(0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ b c(DiConstructor diConstructor) {
        return new b(0);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ VerificationResourceMapper b(DiConstructor diConstructor) {
        return new VerificationResourceMapper();
    }

    @NonNull
    private static ResourceLoader<Uri, Uri> a(@NonNull DiConstructor diConstructor) {
        return (ResourceLoader) diConstructor.get(VideoDiNames.VIDEO_RESOURCE_LOADER_DI_NAME, ResourceLoader.class);
    }
}
