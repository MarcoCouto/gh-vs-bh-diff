package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.analytics.ViewabilityVerificationResource;
import com.smaato.sdk.core.util.collections.Iterables;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.model.JavaScriptResource;
import com.smaato.sdk.video.vast.model.Verification;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class VerificationResourceMapper implements Function<List<Verification>, Map<String, List<ViewabilityVerificationResource>>> {
    @NonNull
    public final Map<String, List<ViewabilityVerificationResource>> apply(@NonNull List<Verification> list) {
        return (Map) Iterables.reduce(list, new HashMap(), $$Lambda$VerificationResourceMapper$ouTqEZPLOohKTSBRDNbPs5fmi78.INSTANCE);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(HashMap hashMap, Verification verification, JavaScriptResource javaScriptResource) {
        String str = javaScriptResource.apiFramework;
        List list = (List) hashMap.get(str);
        if (list == null) {
            list = new ArrayList();
            hashMap.put(str, list);
        }
        ViewabilityVerificationResource viewabilityVerificationResource = new ViewabilityVerificationResource(verification.vendor, javaScriptResource.uri, javaScriptResource.apiFramework, verification.verificationParameters, javaScriptResource.browserOptional);
        list.add(viewabilityVerificationResource);
    }
}
