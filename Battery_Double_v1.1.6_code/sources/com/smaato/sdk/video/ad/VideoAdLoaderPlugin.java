package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.AdFormat;
import com.smaato.sdk.core.ad.AdLoaderPlugin;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterNameShaper;
import com.smaato.sdk.core.ad.ApiAdRequestExtras;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.NullableFunction;

public class VideoAdLoaderPlugin implements AdLoaderPlugin {
    @NonNull
    private static final Integer a = Integer.valueOf(1);
    @NonNull
    private final AdPresenterNameShaper b;
    @NonNull
    private final NullableFunction<String, AdPresenterBuilder> c;

    public VideoAdLoaderPlugin(@NonNull AdPresenterNameShaper adPresenterNameShaper, @NonNull NullableFunction<String, AdPresenterBuilder> nullableFunction) {
        this.b = (AdPresenterNameShaper) Objects.requireNonNull(adPresenterNameShaper);
        this.c = (NullableFunction) Objects.requireNonNull(nullableFunction);
    }

    @Nullable
    public AdPresenterBuilder getAdPresenterBuilder(@NonNull AdFormat adFormat, @NonNull Class<? extends AdPresenter> cls, @NonNull Logger logger) {
        return (AdPresenterBuilder) this.c.apply(this.b.shapeName(adFormat, cls));
    }

    public void addApiAdRequestExtras(@NonNull ApiAdRequestExtras apiAdRequestExtras, @NonNull Logger logger) {
        apiAdRequestExtras.addApiParamExtra("vastver", "4.1");
        apiAdRequestExtras.addApiParamExtra("privacyIcon", a);
    }

    @Nullable
    public AdFormat resolveAdFormatToServerAdFormat(@NonNull AdFormat adFormat, @NonNull Logger logger) {
        if (adFormat == AdFormat.VIDEO) {
            return AdFormat.VIDEO;
        }
        return null;
    }
}
