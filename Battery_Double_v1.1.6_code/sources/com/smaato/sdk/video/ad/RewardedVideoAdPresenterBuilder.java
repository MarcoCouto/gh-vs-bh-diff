package com.smaato.sdk.video.ad;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Listener;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.build.VastScenarioPicker;
import com.smaato.sdk.video.vast.build.VastTreeBuilder;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.player.VastVideoPlayerCreator;
import com.smaato.sdk.video.vast.player.VideoTimings;

public class RewardedVideoAdPresenterBuilder extends e<RewardedVideoAdPresenter> {
    public /* bridge */ /* synthetic */ void buildAdPresenter(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        super.buildAdPresenter(somaApiContext, listener);
    }

    public RewardedVideoAdPresenterBuilder(@NonNull Logger logger, @NonNull Function<c, b> function, @NonNull VastScenarioPicker vastScenarioPicker, @NonNull VastTreeBuilder vastTreeBuilder, @NonNull VastVideoPlayerCreator vastVideoPlayerCreator, @NonNull ResourceLoader<Uri, Uri> resourceLoader, @NonNull VastErrorTrackerCreator vastErrorTrackerCreator, @NonNull MediaFileResourceLoaderListenerCreator mediaFileResourceLoaderListenerCreator, @NonNull VideoViewabilityTracker videoViewabilityTracker, @NonNull Function<VastMediaFileScenario, VideoTimings> function2, @NonNull VerificationResourceMapper verificationResourceMapper) {
        super(logger, vastScenarioPicker, vastTreeBuilder, vastVideoPlayerCreator, resourceLoader, vastErrorTrackerCreator, mediaFileResourceLoaderListenerCreator, function, function2, new Function() {
            public final Object apply(Object obj) {
                return RewardedVideoAdPresenterBuilder.a(VideoViewabilityTracker.this, (a) obj);
            }
        }, verificationResourceMapper);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ RewardedVideoAdPresenter a(VideoViewabilityTracker videoViewabilityTracker, a aVar) {
        RewardedVideoAdPresenter rewardedVideoAdPresenter = new RewardedVideoAdPresenter(aVar.a, aVar.b, videoViewabilityTracker, aVar.c, aVar.d);
        return rewardedVideoAdPresenter;
    }
}
