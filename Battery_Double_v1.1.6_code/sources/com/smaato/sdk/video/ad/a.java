package com.smaato.sdk.video.ad;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.vast.build.VastScenarioPicker;
import com.smaato.sdk.video.vast.build.VastTreeBuilder;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.player.VastVideoPlayerCreator;
import com.smaato.sdk.video.vast.player.VideoTimings;

final class a extends e<InterstitialAdPresenter> {
    a(@NonNull Logger logger, @NonNull Function<c, b> function, @NonNull VastScenarioPicker vastScenarioPicker, @NonNull VastTreeBuilder vastTreeBuilder, @NonNull VastVideoPlayerCreator vastVideoPlayerCreator, @NonNull ResourceLoader<Uri, Uri> resourceLoader, @NonNull VastErrorTrackerCreator vastErrorTrackerCreator, @NonNull MediaFileResourceLoaderListenerCreator mediaFileResourceLoaderListenerCreator, @NonNull VideoViewabilityTracker videoViewabilityTracker, @NonNull Function<VastMediaFileScenario, VideoTimings> function2, @NonNull VerificationResourceMapper verificationResourceMapper) {
        super(logger, vastScenarioPicker, vastTreeBuilder, vastVideoPlayerCreator, resourceLoader, vastErrorTrackerCreator, mediaFileResourceLoaderListenerCreator, function, function2, new Function() {
            public final Object apply(Object obj) {
                return a.a(VideoViewabilityTracker.this, (a) obj);
            }
        }, verificationResourceMapper);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ InterstitialAdPresenter a(VideoViewabilityTracker videoViewabilityTracker, a aVar) {
        InterstitialVideoAdPresenter interstitialVideoAdPresenter = new InterstitialVideoAdPresenter(aVar.a, aVar.b, videoViewabilityTracker, aVar.c, aVar.d);
        return interstitialVideoAdPresenter;
    }
}
