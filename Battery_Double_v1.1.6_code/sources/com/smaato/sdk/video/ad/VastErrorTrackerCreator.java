package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.network.trackers.BeaconTracker;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.build.VastScenarioResult;
import com.smaato.sdk.video.vast.tracking.VastErrorTracker;
import com.smaato.sdk.video.vast.tracking.macro.MacroInjector;
import com.smaato.sdk.video.vast.tracking.macro.MacrosInjectorProviderFunction;

public class VastErrorTrackerCreator {
    @NonNull
    private final Logger a;
    @NonNull
    private final BeaconTracker b;
    @NonNull
    private final MacrosInjectorProviderFunction c;

    public VastErrorTrackerCreator(@NonNull Logger logger, @NonNull BeaconTracker beaconTracker, @NonNull MacrosInjectorProviderFunction macrosInjectorProviderFunction) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (BeaconTracker) Objects.requireNonNull(beaconTracker);
        this.c = (MacrosInjectorProviderFunction) Objects.requireNonNull(macrosInjectorProviderFunction);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final VastErrorTracker a(@NonNull SomaApiContext somaApiContext, @NonNull VastScenarioResult vastScenarioResult) {
        Objects.requireNonNull(somaApiContext);
        Objects.requireNonNull(vastScenarioResult);
        VastErrorTracker vastErrorTracker = new VastErrorTracker(this.a, this.b, somaApiContext, (MacroInjector) this.c.apply(vastScenarioResult.vastScenario), vastScenarioResult.errorUrls);
        return vastErrorTracker;
    }
}
