package com.smaato.sdk.video.ad;

import android.content.res.Resources;
import android.net.Uri;
import android.util.DisplayMetrics;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdPresenter;
import com.smaato.sdk.core.ad.AdPresenterBuilder;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Error;
import com.smaato.sdk.core.ad.AdPresenterBuilder.Listener;
import com.smaato.sdk.core.ad.AdPresenterBuilderErrorMapper;
import com.smaato.sdk.core.ad.AdPresenterBuilderException;
import com.smaato.sdk.core.analytics.ViewabilityVerificationResource;
import com.smaato.sdk.core.api.ApiAdResponse;
import com.smaato.sdk.core.framework.SomaApiContext;
import com.smaato.sdk.core.log.LogDomain;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.resourceloader.ResourceLoader;
import com.smaato.sdk.core.resourceloader.ResourceLoaderException;
import com.smaato.sdk.core.util.Either;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.TextUtils;
import com.smaato.sdk.core.util.fi.Function;
import com.smaato.sdk.video.fi.NonNullConsumer;
import com.smaato.sdk.video.vast.build.VastConfigurationSettings;
import com.smaato.sdk.video.vast.build.VastResult;
import com.smaato.sdk.video.vast.build.VastScenarioPicker;
import com.smaato.sdk.video.vast.build.VastScenarioResult;
import com.smaato.sdk.video.vast.build.VastTreeBuilder;
import com.smaato.sdk.video.vast.model.Category;
import com.smaato.sdk.video.vast.model.Delivery;
import com.smaato.sdk.video.vast.model.ErrorCode;
import com.smaato.sdk.video.vast.model.MediaFile;
import com.smaato.sdk.video.vast.model.VastMediaFileScenario;
import com.smaato.sdk.video.vast.model.VastScenario;
import com.smaato.sdk.video.vast.model.VastTree;
import com.smaato.sdk.video.vast.player.VastVideoPlayer;
import com.smaato.sdk.video.vast.player.VastVideoPlayerCreator;
import com.smaato.sdk.video.vast.player.VideoTimings;
import com.smaato.sdk.video.vast.tracking.VastErrorTracker;
import com.smaato.sdk.video.vast.tracking.macro.PlayerState.Builder;
import java.io.ByteArrayInputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

abstract class e<Presenter extends AdPresenter> implements AdPresenterBuilder {
    @NonNull
    private final Logger a;
    @NonNull
    private final VastScenarioPicker b;
    @NonNull
    private final VastTreeBuilder c;
    @NonNull
    private final VastVideoPlayerCreator d;
    @NonNull
    private final ResourceLoader<Uri, Uri> e;
    @NonNull
    private final VastErrorTrackerCreator f;
    @NonNull
    private final MediaFileResourceLoaderListenerCreator g;
    @NonNull
    private final Function<c, b> h;
    @NonNull
    private final Function<a, Presenter> i;
    @NonNull
    private final Function<VastMediaFileScenario, VideoTimings> j;
    @NonNull
    private final VerificationResourceMapper k;

    static class a {
        @NonNull
        final VastVideoPlayer a;
        @NonNull
        final b b;
        @NonNull
        final VideoTimings c;
        @NonNull
        final Map<String, List<ViewabilityVerificationResource>> d;

        a(@NonNull VastVideoPlayer vastVideoPlayer, @NonNull b bVar, @NonNull VideoTimings videoTimings, @NonNull Map<String, List<ViewabilityVerificationResource>> map) {
            this.a = vastVideoPlayer;
            this.b = bVar;
            this.c = videoTimings;
            this.d = map;
        }
    }

    final class b implements a {
        @NonNull
        private final SomaApiContext a;
        @NonNull
        private final VastErrorTracker b;
        @NonNull
        private final Listener c;

        /* synthetic */ b(e eVar, SomaApiContext somaApiContext, VastErrorTracker vastErrorTracker, Listener listener, byte b2) {
            this(somaApiContext, vastErrorTracker, listener);
        }

        private b(SomaApiContext somaApiContext, @NonNull VastErrorTracker vastErrorTracker, @NonNull Listener listener) {
            this.a = (SomaApiContext) Objects.requireNonNull(somaApiContext);
            this.b = (VastErrorTracker) Objects.requireNonNull(vastErrorTracker);
            this.c = (Listener) Objects.requireNonNull(listener);
        }

        public final void a(@NonNull VastScenario vastScenario) {
            e.this.a(vastScenario, this.a, this.b, (NonNullConsumer<Either<VastVideoPlayer, Exception>>) new NonNullConsumer(vastScenario) {
                private final /* synthetic */ VastScenario f$1;

                {
                    this.f$1 = r2;
                }

                public final void accept(Object obj) {
                    b.this.a(this.f$1, (Either) obj);
                }
            });
        }

        /* access modifiers changed from: private */
        public /* synthetic */ void a(VastScenario vastScenario, Either either) {
            e.this.a(either, this.a, vastScenario, this.c);
        }

        public final void a(@NonNull Exception exc) {
            AdPresenterBuilderException adPresenterBuilderException;
            this.b.track(new Builder().setErrorCode(ErrorCode.GENERAL_LINEAR_ERROR).build());
            if (exc instanceof ResourceLoaderException) {
                adPresenterBuilderException = AdPresenterBuilderErrorMapper.mapError((ResourceLoaderException) exc);
            } else {
                adPresenterBuilderException = new AdPresenterBuilderException(Error.GENERIC, exc);
            }
            this.c.onAdPresenterBuildError(e.this, adPresenterBuilderException);
        }
    }

    e(@NonNull Logger logger, @NonNull VastScenarioPicker vastScenarioPicker, @NonNull VastTreeBuilder vastTreeBuilder, @NonNull VastVideoPlayerCreator vastVideoPlayerCreator, @NonNull ResourceLoader<Uri, Uri> resourceLoader, @NonNull VastErrorTrackerCreator vastErrorTrackerCreator, @NonNull MediaFileResourceLoaderListenerCreator mediaFileResourceLoaderListenerCreator, @NonNull Function<c, b> function, @NonNull Function<VastMediaFileScenario, VideoTimings> function2, @NonNull Function<a, Presenter> function3, @NonNull VerificationResourceMapper verificationResourceMapper) {
        this.a = (Logger) Objects.requireNonNull(logger);
        this.b = (VastScenarioPicker) Objects.requireNonNull(vastScenarioPicker);
        this.c = (VastTreeBuilder) Objects.requireNonNull(vastTreeBuilder);
        this.d = (VastVideoPlayerCreator) Objects.requireNonNull(vastVideoPlayerCreator);
        this.e = (ResourceLoader) Objects.requireNonNull(resourceLoader);
        this.f = (VastErrorTrackerCreator) Objects.requireNonNull(vastErrorTrackerCreator);
        this.g = (MediaFileResourceLoaderListenerCreator) Objects.requireNonNull(mediaFileResourceLoaderListenerCreator);
        this.h = (Function) Objects.requireNonNull(function);
        this.j = (Function) Objects.requireNonNull(function2);
        this.i = (Function) Objects.requireNonNull(function3);
        this.k = (VerificationResourceMapper) Objects.requireNonNull(verificationResourceMapper);
    }

    public void buildAdPresenter(@NonNull SomaApiContext somaApiContext, @NonNull Listener listener) {
        Objects.requireNonNull(somaApiContext, "Parameter somaApiContext cannot be null for VideoAdPresenterBuilder::buildAdPresenter");
        ApiAdResponse apiAdResponse = somaApiContext.getApiAdResponse();
        this.c.buildVastTree(this.a, somaApiContext, new ByteArrayInputStream(apiAdResponse.getBody()), apiAdResponse.getCharset(), new NonNullConsumer(somaApiContext, listener) {
            private final /* synthetic */ SomaApiContext f$1;
            private final /* synthetic */ Listener f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void accept(Object obj) {
                e.this.a(this.f$1, this.f$2, (VastResult) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull Either<VastVideoPlayer, Exception> either, @NonNull SomaApiContext somaApiContext, @NonNull VastScenario vastScenario, @NonNull Listener listener) {
        Exception exc = (Exception) either.right();
        if (exc != null) {
            a(exc.getMessage(), listener);
            return;
        }
        VastVideoPlayer vastVideoPlayer = (VastVideoPlayer) Objects.requireNonNull(either.left());
        listener.onAdPresenterBuildSuccess(this, (AdPresenter) this.i.apply(new a(vastVideoPlayer, (b) this.h.apply(new c(somaApiContext)), (VideoTimings) this.j.apply(vastScenario.vastMediaFileScenario), this.k.apply(vastScenario.adVerifications))));
        vastVideoPlayer.loaded();
    }

    /* access modifiers changed from: private */
    public void a(@NonNull VastScenario vastScenario, @NonNull SomaApiContext somaApiContext, @NonNull VastErrorTracker vastErrorTracker, @NonNull NonNullConsumer<Either<VastVideoPlayer, Exception>> nonNullConsumer) {
        this.d.createVastVideoPlayer(this.a, somaApiContext, vastScenario, vastErrorTracker, nonNullConsumer, (VideoTimings) this.j.apply(vastScenario.vastMediaFileScenario));
    }

    private void a(@NonNull String str, @NonNull Listener listener) {
        this.a.error(LogDomain.VAST, str, new Object[0]);
        listener.onAdPresenterBuildError(this, new AdPresenterBuilderException(Error.INVALID_RESPONSE, new RuntimeException(str)));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(SomaApiContext somaApiContext, Listener listener, VastResult vastResult) {
        if (vastResult.value == null) {
            a("Failed to build RewardedVideoAdPresenter: VAST parse result is empty", listener);
            return;
        }
        HashSet<Integer> hashSet = new HashSet<>(vastResult.errors);
        Logger logger = this.a;
        VastTree vastTree = (VastTree) vastResult.value;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        String connection = somaApiContext.getApiAdRequest().getConnection();
        Integer width = somaApiContext.getApiAdRequest().getWidth();
        Integer height = somaApiContext.getApiAdRequest().getHeight();
        if (width == null) {
            width = Integer.valueOf(displayMetrics.widthPixels);
        }
        if (height == null) {
            height = Integer.valueOf(displayMetrics.heightPixels);
        }
        VastScenarioResult pickVastScenario = this.b.pickVastScenario(logger, vastTree, new VastConfigurationSettings(width.intValue(), height.intValue(), connection));
        hashSet.addAll(pickVastScenario.errors);
        VastErrorTracker a2 = this.f.a(somaApiContext, pickVastScenario);
        for (Integer intValue : hashSet) {
            a2.track(new Builder().setErrorCode(intValue.intValue()).build());
        }
        VastScenario vastScenario = pickVastScenario.vastScenario;
        if (vastScenario == null) {
            a("Failed to build VastAdPresenter: Unable to pick proper VAST scenario to play", listener);
            return;
        }
        for (Category category : vastScenario.categories) {
            if (vastScenario.blockedAdCategories.contains(category.categoryCode)) {
                a2.track(new Builder().setErrorCode(ErrorCode.INLINE_CATEGORY_VIOLATES_BLOCKED_CATEGORIES_ERROR).build());
                a("Failed to build VastAdPresenter: Inline Category violates Wrapper BlockedAdCategories", listener);
                return;
            }
        }
        VastMediaFileScenario vastMediaFileScenario = vastScenario.vastMediaFileScenario;
        if (!vastMediaFileScenario.hasValidDuration()) {
            a2.track(new Builder().setErrorCode(ErrorCode.GENERAL_LINEAR_ERROR).build());
            a("Failed to build VastAdPresenter: Invalid value of expected duration", listener);
            return;
        }
        MediaFile mediaFile = vastMediaFileScenario.mediaFile;
        if (TextUtils.isEmpty(mediaFile.url)) {
            a2.track(new Builder().setErrorCode(ErrorCode.GENERAL_LINEAR_ERROR).build());
            a("Failed to build VastAdPresenter: Empty URL of MediaFile", listener);
            return;
        }
        Delivery delivery = mediaFile.delivery;
        if (delivery == Delivery.PROGRESSIVE) {
            b bVar = new b(this, somaApiContext, a2, listener, 0);
            this.e.loadResource(mediaFile.url, somaApiContext, MediaFileResourceLoaderListenerCreator.a(vastScenario, bVar));
        } else if (delivery == Delivery.STREAMING) {
            a(vastScenario, somaApiContext, a2, (NonNullConsumer<Either<VastVideoPlayer, Exception>>) new NonNullConsumer(somaApiContext, vastScenario, listener) {
                private final /* synthetic */ SomaApiContext f$1;
                private final /* synthetic */ VastScenario f$2;
                private final /* synthetic */ Listener f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                public final void accept(Object obj) {
                    e.this.a(this.f$1, this.f$2, this.f$3, (Either) obj);
                }
            });
        } else {
            a2.track(new Builder().setErrorCode(405).build());
            a("Failed to build RewardedVideoAdPresenter: Unknown delivery method", listener);
        }
    }
}
