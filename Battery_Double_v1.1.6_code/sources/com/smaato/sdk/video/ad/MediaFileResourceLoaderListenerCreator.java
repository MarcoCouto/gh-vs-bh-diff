package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.video.vast.model.VastScenario;

public final class MediaFileResourceLoaderListenerCreator {
    @NonNull
    static MediaFileResourceLoaderListener a(@NonNull VastScenario vastScenario, @NonNull a aVar) {
        return new MediaFileResourceLoaderListener(vastScenario, aVar);
    }
}
