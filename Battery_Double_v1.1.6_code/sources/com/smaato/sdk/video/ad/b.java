package com.smaato.sdk.video.ad;

import androidx.annotation.NonNull;
import com.smaato.sdk.core.ad.AdInteractor;
import com.smaato.sdk.core.ad.AdStateMachine.Event;
import com.smaato.sdk.core.ad.AdStateMachine.State;
import com.smaato.sdk.core.log.Logger;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.OneTimeActionFactory;
import com.smaato.sdk.core.util.StateMachine;

final class b extends AdInteractor<c> {
    @NonNull
    private final Logger a;

    public b(@NonNull Logger logger, @NonNull c cVar, @NonNull StateMachine<Event, State> stateMachine, @NonNull OneTimeActionFactory oneTimeActionFactory) {
        super(cVar, stateMachine, oneTimeActionFactory);
        this.a = (Logger) Objects.requireNonNull(logger);
        stateMachine.onEvent(Event.INITIALISE);
    }
}
