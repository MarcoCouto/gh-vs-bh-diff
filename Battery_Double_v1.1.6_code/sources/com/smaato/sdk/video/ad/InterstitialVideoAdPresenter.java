package com.smaato.sdk.video.ad;

import android.content.Context;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.smaato.sdk.core.ad.InterstitialAdPresenter;
import com.smaato.sdk.core.ad.InterstitialAdPresenter.Listener;
import com.smaato.sdk.core.analytics.VideoViewabilityTracker;
import com.smaato.sdk.core.analytics.ViewabilityVerificationResource;
import com.smaato.sdk.core.ui.AdContentView;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.core.util.fi.Consumer;
import com.smaato.sdk.video.vast.player.VastVideoPlayer;
import com.smaato.sdk.video.vast.player.VideoTimings;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

public class InterstitialVideoAdPresenter extends d implements InterstitialAdPresenter {
    @NonNull
    private final VastVideoPlayer a;
    @NonNull
    private WeakReference<Listener> b = new WeakReference<>(null);

    /* access modifiers changed from: 0000 */
    public final void a() {
    }

    /* access modifiers changed from: 0000 */
    public final void c() {
    }

    public void setFriendlyObstructionView(@NonNull View view) {
    }

    @NonNull
    public /* bridge */ /* synthetic */ AdContentView getAdContentView(@NonNull Context context) {
        return super.getAdContentView(context);
    }

    InterstitialVideoAdPresenter(@NonNull VastVideoPlayer vastVideoPlayer, @NonNull b bVar, @NonNull VideoViewabilityTracker videoViewabilityTracker, @NonNull VideoTimings videoTimings, @NonNull Map<String, List<ViewabilityVerificationResource>> map) {
        super(vastVideoPlayer, bVar, videoViewabilityTracker, videoTimings, map);
        this.a = vastVideoPlayer;
    }

    @MainThread
    public void onCloseClicked() {
        this.a.onCloseClicked();
    }

    public void setListener(@Nullable Listener listener) {
        this.b = new WeakReference<>(listener);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e(Listener listener) {
        listener.onAdImpressed(this);
    }

    /* access modifiers changed from: 0000 */
    public final void b() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                InterstitialVideoAdPresenter.this.e((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void d(Listener listener) {
        listener.onTTLExpired(this);
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                InterstitialVideoAdPresenter.this.d((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void c(Listener listener) {
        listener.onClose(this);
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                InterstitialVideoAdPresenter.this.c((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(Listener listener) {
        listener.onAdError(this);
    }

    /* access modifiers changed from: 0000 */
    public final void f() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                InterstitialVideoAdPresenter.this.b((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Listener listener) {
        listener.onAdClicked(this);
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        Objects.onNotNull(this.b.get(), new Consumer() {
            public final void accept(Object obj) {
                InterstitialVideoAdPresenter.this.a((Listener) obj);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void h() {
        Objects.onNotNull(this.b.get(), $$Lambda$nk0D4u8wrNMCMfE3WzHxisIX_k.INSTANCE);
    }
}
