package com.smaato.sdk.video.ad;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.smaato.sdk.core.resourceloader.ResourceLoader.Listener;
import com.smaato.sdk.core.resourceloader.ResourceLoaderException;
import com.smaato.sdk.core.util.Objects;
import com.smaato.sdk.video.vast.exceptions.VastElementMissingException;
import com.smaato.sdk.video.vast.model.VastScenario;

public final class MediaFileResourceLoaderListener implements Listener<Uri> {
    @NonNull
    private final VastScenario a;
    @NonNull
    private final a b;

    interface a {
        void a(@NonNull VastScenario vastScenario);

        void a(@NonNull Exception exc);
    }

    MediaFileResourceLoaderListener(@NonNull VastScenario vastScenario, @NonNull a aVar) {
        this.a = (VastScenario) Objects.requireNonNull(vastScenario);
        this.b = (a) Objects.requireNonNull(aVar);
    }

    public final void onResourceLoaded(@NonNull Uri uri) {
        try {
            this.b.a(this.a.newBuilder().setVastMediaFileScenario(this.a.vastMediaFileScenario.newBuilder().setMediaFile(this.a.vastMediaFileScenario.mediaFile.newBuilder().setUrl(uri.toString()).build()).build()).build());
        } catch (VastElementMissingException e) {
            a(e);
        }
    }

    public final void onFailure(@NonNull ResourceLoaderException resourceLoaderException) {
        a(resourceLoaderException);
    }

    private void a(@NonNull Exception exc) {
        this.b.a(exc);
    }
}
