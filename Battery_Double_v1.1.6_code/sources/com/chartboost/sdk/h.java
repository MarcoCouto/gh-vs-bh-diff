package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.chartboost.sdk.Chartboost.CBPIDataUseConsent;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.d;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.ak;
import com.chartboost.sdk.impl.al;
import com.chartboost.sdk.impl.am;
import com.chartboost.sdk.impl.an;
import com.chartboost.sdk.impl.aq;
import com.chartboost.sdk.impl.ar;
import com.chartboost.sdk.impl.ay;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.e;
import com.chartboost.sdk.impl.l;
import com.chartboost.sdk.impl.m;
import com.chartboost.sdk.impl.o;
import com.chartboost.sdk.impl.s;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class h {
    private static h v;
    public final Executor a;
    final l b;
    public final d c;
    public final e d;
    public final c e;
    public final e f;
    public final c g;
    public final aj h;
    public final m i;
    public final ar j;
    public final e k;
    public final c l;
    public final AtomicReference<com.chartboost.sdk.Model.e> m;
    final SharedPreferences n;
    public final com.chartboost.sdk.Tracking.a o;
    public final Handler p;
    public final c q;
    public final am r;
    boolean s = true;
    boolean t = false;
    boolean u = true;
    private final s w;

    public class a implements Runnable {
        final int a;
        String b = null;
        boolean c = false;
        boolean d = false;

        a(int i) {
            this.a = i;
        }

        public void run() {
            try {
                switch (this.a) {
                    case 0:
                        h.this.d();
                        return;
                    case 1:
                        i.t = this.c;
                        return;
                    case 2:
                        i.v = this.d;
                        if (!this.d || !h.f()) {
                            h.this.i.b();
                            return;
                        } else {
                            h.this.i.a();
                            return;
                        }
                    case 3:
                        al alVar = new al("api/install", h.this.j, h.this.o, 2, null);
                        alVar.l = true;
                        h.this.h.a(alVar);
                        Executor executor = h.this.a;
                        e eVar = h.this.d;
                        eVar.getClass();
                        com.chartboost.sdk.impl.e.a aVar = new com.chartboost.sdk.impl.e.a(0, null, null, null);
                        executor.execute(aVar);
                        Executor executor2 = h.this.a;
                        e eVar2 = h.this.f;
                        eVar2.getClass();
                        com.chartboost.sdk.impl.e.a aVar2 = new com.chartboost.sdk.impl.e.a(0, null, null, null);
                        executor2.execute(aVar2);
                        Executor executor3 = h.this.a;
                        e eVar3 = h.this.k;
                        eVar3.getClass();
                        com.chartboost.sdk.impl.e.a aVar3 = new com.chartboost.sdk.impl.e.a(0, null, null, null);
                        executor3.execute(aVar3);
                        h.this.a.execute(new a(4));
                        h.this.u = false;
                        return;
                    case 4:
                        h.this.i.a();
                        return;
                    case 5:
                        if (i.d != null) {
                            i.d.didFailToLoadMoreApps(this.b, CBImpressionError.END_POINT_DISABLED);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } catch (Exception e2) {
                StringBuilder sb = new StringBuilder();
                sb.append("run (");
                sb.append(this.a);
                sb.append(")");
                com.chartboost.sdk.Tracking.a.a(a.class, sb.toString(), e2);
            }
        }
    }

    public static h a() {
        return v;
    }

    static void a(h hVar) {
        v = hVar;
    }

    h(Activity activity, String str, String str2, s sVar, ScheduledExecutorService scheduledExecutorService, Handler handler, Executor executor) {
        JSONObject jSONObject;
        s sVar2 = sVar;
        ScheduledExecutorService scheduledExecutorService2 = scheduledExecutorService;
        Handler handler2 = handler;
        g a2 = g.a();
        i.n = activity.getApplicationContext();
        this.c = (d) a2.a(new d(i.n));
        ak akVar = (ak) a2.a(new ak());
        i iVar = (i) a2.a(new i());
        aj ajVar = new aj(scheduledExecutorService, (aq) a2.a(new aq()), akVar, iVar, handler, executor);
        this.h = (aj) a2.a(ajVar);
        SharedPreferences a3 = a(i.n);
        try {
            jSONObject = new JSONObject(a3.getString("config", "{}"));
        } catch (Exception e2) {
            CBLogging.b("Sdk", "Unable to process config");
            e2.printStackTrace();
            jSONObject = new JSONObject();
        }
        AtomicReference<com.chartboost.sdk.Model.e> atomicReference = new AtomicReference<>(null);
        if (!b.a(atomicReference, jSONObject, a3)) {
            atomicReference.set(new com.chartboost.sdk.Model.e(new JSONObject()));
        }
        this.w = sVar2;
        this.a = scheduledExecutorService2;
        this.m = atomicReference;
        this.n = a3;
        this.p = handler2;
        f fVar = new f(sVar2, i.n, atomicReference);
        if (!((com.chartboost.sdk.Model.e) atomicReference.get()).y) {
            i.w = "";
        } else {
            a(i.n, null, a3);
        }
        ak akVar2 = akVar;
        f fVar2 = fVar;
        AtomicReference<com.chartboost.sdk.Model.e> atomicReference2 = atomicReference;
        SharedPreferences sharedPreferences = a3;
        ar arVar = new ar(i.n, str, this.c, akVar2, atomicReference2, a3, iVar);
        this.j = (ar) a2.a(arVar);
        this.o = (com.chartboost.sdk.Tracking.a) a2.a(new com.chartboost.sdk.Tracking.a(atomicReference));
        l lVar = new l(scheduledExecutorService, fVar2, this.h, akVar2, atomicReference2, iVar, this.o);
        this.b = (l) a2.a(lVar);
        d dVar = (d) a2.a(new d((ay) g.a().a(new ay(handler2)), this.b, atomicReference, handler2));
        d dVar2 = dVar;
        d dVar3 = dVar;
        d dVar4 = dVar;
        this.r = (am) a2.a(new am(scheduledExecutorService2, this.h, akVar, handler2));
        ak akVar3 = akVar;
        g gVar = a2;
        c cVar = new c(activity, akVar3, this, this.o, handler, dVar);
        this.q = (c) gVar.a(cVar);
        f fVar3 = fVar2;
        an anVar = (an) gVar.a(new an(fVar3));
        an anVar2 = anVar;
        an anVar3 = anVar;
        an anVar4 = anVar;
        this.e = c.c();
        this.g = c.a();
        this.l = c.b();
        g gVar2 = gVar;
        e eVar = new e(this.e, scheduledExecutorService, this.b, fVar3, this.h, akVar3, this.j, atomicReference, sharedPreferences, iVar, this.o, handler, this.q, this.r, dVar4, anVar4);
        this.d = (e) gVar2.a(eVar);
        e eVar2 = new e(this.g, scheduledExecutorService, this.b, fVar3, this.h, akVar3, this.j, atomicReference, sharedPreferences, iVar, this.o, handler, this.q, this.r, dVar3, anVar3);
        this.f = (e) gVar2.a(eVar2);
        e eVar3 = new e(this.l, scheduledExecutorService, this.b, fVar3, this.h, akVar3, this.j, atomicReference, sharedPreferences, iVar, this.o, handler, this.q, this.r, dVar2, anVar2);
        this.k = (e) gVar2.a(eVar3);
        m mVar = new m(this.b, fVar3, this.h, this.j, this.o, atomicReference);
        this.i = (m) gVar2.a(mVar);
        i.l = str;
        i.m = str2;
        SharedPreferences sharedPreferences2 = sharedPreferences;
        if (!sharedPreferences2.contains("cbLimitTrack") || sharedPreferences2.contains("cbGDPR")) {
            i.x = CBPIDataUseConsent.valueOf(sharedPreferences2.getInt("cbGDPR", i.x.getValue()));
        } else {
            i.x = sharedPreferences2.getBoolean("cbLimitTrack", false) ? CBPIDataUseConsent.NO_BEHAVIORAL : CBPIDataUseConsent.UNKNOWN;
        }
        if (s.a().a(19)) {
            o.a(activity.getApplication(), ((com.chartboost.sdk.Model.e) atomicReference.get()).J, !((com.chartboost.sdk.Model.e) atomicReference.get()).K, !((com.chartboost.sdk.Model.e) atomicReference.get()).L);
        }
    }

    private static SharedPreferences a(Context context) {
        return context.getSharedPreferences("cbPrefs", 0);
    }

    /* access modifiers changed from: 0000 */
    public void a(final Runnable runnable) {
        this.s = true;
        al alVar = new al("/api/config", this.j, this.o, 1, new com.chartboost.sdk.impl.al.a() {
            public void a(al alVar, JSONObject jSONObject) {
                h.this.s = false;
                JSONObject a2 = com.chartboost.sdk.Libraries.e.a(jSONObject, ServerResponseWrapper.RESPONSE_FIELD);
                if (a2 != null && b.a(h.this.m, a2, h.this.n)) {
                    h.this.n.edit().putString("config", a2.toString()).apply();
                }
                if (runnable != null) {
                    runnable.run();
                }
                if (!h.this.t) {
                    a aVar = i.d;
                    if (aVar != null) {
                        aVar.didInitialize();
                    }
                    h.this.t = true;
                }
            }

            public void a(al alVar, CBError cBError) {
                h.this.s = false;
                if (runnable != null) {
                    runnable.run();
                }
                if (!h.this.t) {
                    a aVar = i.d;
                    if (aVar != null) {
                        aVar.didInitialize();
                    }
                    h.this.t = true;
                }
            }
        });
        alVar.l = true;
        this.h.a(alVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(Activity activity) {
        if (this.w.a(23)) {
            b.a((Context) activity);
        }
        if (!this.u && !this.q.e()) {
            this.b.c();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (i.n == null) {
            CBLogging.b("Sdk", "The context must be set through the Chartboost method onCreate() before calling startSession().");
        } else {
            g();
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        this.p.postDelayed(new a(0), 500);
    }

    private void g() {
        this.o.a();
        if (!this.u) {
            a((Runnable) new a(3));
        }
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        this.o.b();
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        if (!this.t) {
            if (i.d != null) {
                i.d.didInitialize();
            }
            this.t = true;
        }
    }

    public static void b(Runnable runnable) {
        s a2 = s.a();
        if (!a2.e()) {
            a2.a.post(runnable);
        } else {
            runnable.run();
        }
    }

    static boolean f() {
        h a2 = a();
        if (a2 == null || !((com.chartboost.sdk.Model.e) a2.m.get()).c) {
            return true;
        }
        try {
            throw new Exception("Chartboost Integration Warning: your account has been disabled for this session. This app has no active publishing campaigns, please create a publishing campaign in the Chartboost dashboard and wait at least 30 minutes to re-enable. If you need assistance, please visit http://chartboo.st/publishing .");
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    public static void a(Context context, WebView webView, SharedPreferences sharedPreferences) {
        String userAgentString;
        String property = System.getProperty("http.agent");
        try {
            if (VERSION.SDK_INT != 16) {
                userAgentString = WebSettings.getDefaultUserAgent(context);
            } else if (webView != null) {
                userAgentString = webView.getSettings().getUserAgentString();
            } else if (!sharedPreferences.contains("user_agent")) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    userAgentString = new WebView(context.getApplicationContext()).getSettings().getUserAgentString();
                }
                i.w = property;
                if (VERSION.SDK_INT != 16) {
                    sharedPreferences.edit().putString("user_agent", property).apply();
                    return;
                }
                return;
            } else {
                userAgentString = sharedPreferences.getString("user_agent", i.w);
            }
            property = userAgentString;
        } catch (Exception unused) {
        }
        i.w = property;
        if (VERSION.SDK_INT != 16) {
        }
    }

    static void a(Context context, CBPIDataUseConsent cBPIDataUseConsent) {
        i.x = cBPIDataUseConsent;
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            a2.edit().putInt("cbGDPR", cBPIDataUseConsent.getValue()).apply();
        }
    }
}
