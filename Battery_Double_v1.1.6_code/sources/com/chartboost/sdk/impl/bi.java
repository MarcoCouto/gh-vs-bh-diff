package com.chartboost.sdk.impl;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.impl.bh.b;
import org.json.JSONObject;

public class bi implements Runnable {
    private final bf a;
    private final bh b;
    private final int c;
    private final JSONObject d;
    private final String e;

    bi(bf bfVar, bh bhVar, int i, String str, JSONObject jSONObject) {
        this.a = bfVar;
        this.b = bhVar;
        this.c = i;
        this.e = str;
        this.d = jSONObject;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:113:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:52:0x0147 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:87:0x0286 */
    public void run() {
        try {
            switch (this.c) {
                case 0:
                    this.b.b((JSONObject) null);
                    return;
                case 1:
                    this.b.h();
                    return;
                case 2:
                    float f = (float) this.d.getDouble("duration");
                    StringBuilder sb = new StringBuilder();
                    sb.append("######### JS->Native Video current player duration");
                    float f2 = f * 1000.0f;
                    sb.append(f2);
                    CBLogging.a("NativeBridgeCommand", sb.toString());
                    this.b.a(f2);
                    this.b.e("Parsing exception unknown field for current player duration");
                    CBLogging.b("NativeBridgeCommand", "Cannot find duration parameter for the video");
                    return;
                case 3:
                    try {
                        String string = this.d.getString("message");
                        String name = bg.class.getName();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("JS->Native Debug message: ");
                        sb2.append(string);
                        Log.d(name, sb2.toString());
                        this.b.c(string);
                        return;
                    } catch (Exception unused) {
                        CBLogging.b("NativeBridgeCommand", "Exception occured while parsing the message for webview debug track event");
                        this.b.c("Exception occured while parsing the message for webview debug track event");
                        return;
                    }
                case 4:
                    try {
                        String string2 = this.d.getString("message");
                        String name2 = bg.class.getName();
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("JS->Native Error message: ");
                        sb3.append(string2);
                        Log.d(name2, sb3.toString());
                        this.b.d(string2);
                        return;
                    } catch (Exception unused2) {
                        CBLogging.b("NativeBridgeCommand", "Error message is empty");
                        this.b.d("");
                        return;
                    }
                case 5:
                    try {
                        String string3 = this.d.getString("url");
                        if (!string3.startsWith("http://") && !string3.startsWith("https://")) {
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("http://");
                            sb4.append(string3);
                            string3 = sb4.toString();
                        }
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(string3));
                        b y = this.b.e();
                        if (y != null) {
                            Context context = y.getContext();
                            if (context != null && intent.resolveActivity(context.getPackageManager()) != null) {
                                context.startActivity(intent);
                                String name3 = bg.class.getName();
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("JS->Native Track MRAID openUrl: ");
                                sb5.append(string3);
                                CBLogging.a(name3, sb5.toString());
                                return;
                            }
                            return;
                        }
                        return;
                    } catch (ActivityNotFoundException e2) {
                        a.a(getClass(), "ActivityNotFoundException occured when opening a url in a browser", (Exception) e2);
                        CBLogging.b("NativeBridgeCommand", "ActivityNotFoundException occured when opening a url in a browser");
                        return;
                    } catch (Exception e3) {
                        a.a(getClass(), "Exception while opening a browser view with MRAID url", e3);
                        CBLogging.b("NativeBridgeCommand", "Exception while opening a browser view with MRAID url");
                        return;
                    }
                case 6:
                    this.b.z();
                    return;
                case 7:
                    float f3 = (float) this.d.getDouble("duration");
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("######### JS->Native Video total player duration");
                    float f4 = f3 * 1000.0f;
                    sb6.append(f4);
                    CBLogging.a("NativeBridgeCommand", sb6.toString());
                    this.b.b(f4);
                    this.b.e("Parsing exception unknown field for total player duration");
                    CBLogging.b("NativeBridgeCommand", "Cannot find duration parameter for the video");
                    return;
                case 8:
                    try {
                        String string4 = this.d.getString("event");
                        this.b.b(string4);
                        String name4 = bg.class.getName();
                        StringBuilder sb7 = new StringBuilder();
                        sb7.append("JS->Native Track VAST event message: ");
                        sb7.append(string4);
                        Log.d(name4, sb7.toString());
                        return;
                    } catch (Exception unused3) {
                        CBLogging.b("NativeBridgeCommand", "Exception occured while parsing the message for webview tracking VAST events");
                        return;
                    }
                case 9:
                    this.a.onHideCustomView();
                    this.b.b(1);
                    this.b.w();
                    return;
                case 10:
                    try {
                        String string5 = this.d.getString("name");
                        if (!s.a().a((CharSequence) string5)) {
                            this.b.m = string5;
                        }
                    } catch (Exception unused4) {
                        CBLogging.b("NativeBridgeCommand", "Cannot find video file name");
                        this.b.e("Parsing exception unknown field for video pause");
                    }
                    this.b.b(3);
                    return;
                case 11:
                    try {
                        String string6 = this.d.getString("name");
                        if (!s.a().a((CharSequence) string6)) {
                            this.b.m = string6;
                        }
                    } catch (Exception unused5) {
                        CBLogging.b("NativeBridgeCommand", "Cannot find video file name");
                        this.b.e("Parsing exception unknown field for video play");
                    }
                    this.b.b(2);
                    return;
                case 12:
                    try {
                        String string7 = this.d.getString("name");
                        if (!s.a().a((CharSequence) string7)) {
                            this.b.m = string7;
                        }
                        this.b.x();
                        return;
                    } catch (Exception unused6) {
                        CBLogging.b("NativeBridgeCommand", "Cannot find video file name");
                        this.b.e("Parsing exception unknown field for video replay");
                        return;
                    }
                case 13:
                    try {
                        String string8 = this.d.getString("message");
                        String name5 = bg.class.getName();
                        StringBuilder sb8 = new StringBuilder();
                        sb8.append("JS->Native Warning message: ");
                        sb8.append(string8);
                        Log.d(name5, sb8.toString());
                        this.b.e(string8);
                        return;
                    } catch (Exception unused7) {
                        CBLogging.b("NativeBridgeCommand", "Warning message is empty");
                        this.b.e("");
                        return;
                    }
                case 14:
                    try {
                        this.b.c(this.d);
                        return;
                    } catch (Exception unused8) {
                        CBLogging.b("NativeBridgeCommand", "Invalid set orientation command");
                        return;
                    }
                default:
                    return;
            }
        } catch (Exception e4) {
            Class cls = getClass();
            StringBuilder sb9 = new StringBuilder();
            sb9.append("run(");
            sb9.append(this.e);
            sb9.append(")");
            a.a(cls, sb9.toString(), e4);
        }
        Class cls2 = getClass();
        StringBuilder sb92 = new StringBuilder();
        sb92.append("run(");
        sb92.append(this.e);
        sb92.append(")");
        a.a(cls2, sb92.toString(), e4);
    }
}
