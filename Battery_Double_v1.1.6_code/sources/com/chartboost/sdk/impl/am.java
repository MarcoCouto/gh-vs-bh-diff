package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError.CBClickError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.i;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.Executor;

public class am {
    final ak a;
    final Handler b;
    private final Executor c;
    private final aj d;

    public am(Executor executor, aj ajVar, ak akVar, Handler handler) {
        this.c = executor;
        this.d = ajVar;
        this.a = akVar;
        this.b = handler;
    }

    public void a(c cVar, boolean z, String str, CBClickError cBClickError, al alVar) {
        if (cVar != null) {
            cVar.x = false;
            if (cVar.b()) {
                cVar.l = 4;
            }
        }
        if (!z) {
            if (i.d != null) {
                i.d.didFailToRecordClick(str, cBClickError);
            }
        } else if (cVar != null && cVar.w != null) {
            this.d.a(cVar.w);
        } else if (alVar != null) {
            this.d.a(alVar);
        }
    }

    public void a(c cVar, String str, Activity activity, al alVar) {
        try {
            String scheme = new URI(str).getScheme();
            if (scheme == null) {
                a(cVar, false, str, CBClickError.URI_INVALID, alVar);
            } else if (scheme.equals("http") || scheme.equals("https")) {
                final String str2 = str;
                final c cVar2 = cVar;
                final Activity activity2 = activity;
                final al alVar2 = alVar;
                AnonymousClass1 r1 = new Runnable() {
                    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002f, code lost:
                        if (r2 != null) goto L_0x0031;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
                        r2.disconnect();
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0045, code lost:
                        if (r2 != null) goto L_0x0031;
                     */
                    /* JADX WARNING: Removed duplicated region for block: B:25:0x004b A[SYNTHETIC, Splitter:B:25:0x004b] */
                    public void run() {
                        HttpURLConnection httpURLConnection;
                        Throwable e2;
                        try {
                            String str = str2;
                            if (am.this.a.c()) {
                                try {
                                    httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
                                    try {
                                        httpURLConnection.setInstanceFollowRedirects(false);
                                        httpURLConnection.setConnectTimeout(10000);
                                        httpURLConnection.setReadTimeout(10000);
                                        String headerField = httpURLConnection.getHeaderField("Location");
                                        if (headerField != null) {
                                            str = headerField;
                                        }
                                    } catch (Exception e3) {
                                        e2 = e3;
                                        try {
                                            CBLogging.a("CBURLOpener", "Exception raised while opening a HTTP Conection", e2);
                                        } catch (Throwable th) {
                                            th = th;
                                            if (httpURLConnection != null) {
                                            }
                                            throw th;
                                        }
                                    }
                                } catch (Exception e4) {
                                    Throwable th2 = e4;
                                    httpURLConnection = null;
                                    e2 = th2;
                                    CBLogging.a("CBURLOpener", "Exception raised while opening a HTTP Conection", e2);
                                } catch (Throwable th3) {
                                    th = th3;
                                    httpURLConnection = null;
                                    if (httpURLConnection != null) {
                                        httpURLConnection.disconnect();
                                    }
                                    throw th;
                                }
                            }
                            a(str);
                        } catch (Exception e5) {
                            a.a(am.class, "open followTask", e5);
                        }
                    }

                    private void a(final String str) {
                        AnonymousClass1 r0 = new Runnable() {
                            public void run() {
                                try {
                                    am.this.a(cVar2, str, (Context) activity2, alVar2);
                                } catch (Exception e) {
                                    a.a(am.class, "open openOnUiThread Runnable.run", e);
                                }
                            }
                        };
                        if (activity2 != null) {
                            activity2.runOnUiThread(r0);
                        } else {
                            am.this.b.post(r0);
                        }
                    }
                };
                this.c.execute(r1);
            } else {
                a(cVar, str, (Context) activity, alVar);
            }
        } catch (URISyntaxException unused) {
            a(cVar, false, str, CBClickError.URI_INVALID, alVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar, String str, Context context, al alVar) {
        String str2;
        Exception e;
        String str3;
        if (cVar != null && cVar.b()) {
            cVar.l = 5;
        }
        if (context == null) {
            context = i.n;
        }
        if (context == null) {
            a(cVar, false, str, CBClickError.NO_HOST_ACTIVITY, alVar);
            return;
        }
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            intent.setData(Uri.parse(str));
            context.startActivity(intent);
        } catch (Exception unused) {
            if (str.startsWith("market://")) {
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append("http://market.android.com/");
                    sb.append(str.substring(9));
                    String sb2 = sb.toString();
                    try {
                        Intent intent2 = new Intent("android.intent.action.VIEW");
                        if (!(context instanceof Activity)) {
                            intent2.addFlags(268435456);
                        }
                        intent2.setData(Uri.parse(sb2));
                        context.startActivity(intent2);
                        str2 = sb2;
                    } catch (Exception e2) {
                        e = e2;
                        str3 = sb2;
                        CBLogging.a("CBURLOpener", "Exception raised openeing an inavld playstore URL", e);
                        a(cVar, false, str3, CBClickError.URI_UNRECOGNIZED, alVar);
                        return;
                    }
                } catch (Exception e3) {
                    str3 = str;
                    e = e3;
                    CBLogging.a("CBURLOpener", "Exception raised openeing an inavld playstore URL", e);
                    a(cVar, false, str3, CBClickError.URI_UNRECOGNIZED, alVar);
                    return;
                }
            } else {
                a(cVar, false, str, CBClickError.URI_UNRECOGNIZED, alVar);
            }
        }
        str2 = str;
        a(cVar, true, str2, null, alVar);
    }

    public boolean a(String str) {
        boolean z = false;
        try {
            Context context = i.n;
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            intent.setData(Uri.parse(str));
            if (context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0) {
                z = true;
            }
            return z;
        } catch (Exception e) {
            CBLogging.a("CBURLOpener", "Cannot open URL", e);
            a.a(am.class, "canOpenURL", e);
            return false;
        }
    }

    public void a(c cVar, String str, al alVar) {
        a(cVar, str, cVar != null ? cVar.g.a() : null, alVar);
    }
}
