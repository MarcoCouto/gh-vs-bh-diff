package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.c;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import com.facebook.appevents.UserDataStore;
import com.facebook.share.internal.ShareConstants;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class al extends af<JSONObject> {
    public final JSONObject a = new JSONObject();
    public final a k;
    public boolean l = false;
    protected final ar m;
    private final String n;
    private String o;
    private final com.chartboost.sdk.Tracking.a p;

    public interface a {
        void a(al alVar, CBError cBError);

        void a(al alVar, JSONObject jSONObject);
    }

    public al(String str, ar arVar, com.chartboost.sdk.Tracking.a aVar, int i, a aVar2) {
        super(HttpRequest.METHOD_POST, a(str), i, null);
        this.n = str;
        this.m = arVar;
        this.p = aVar;
        this.k = aVar2;
    }

    public static String a(String str) {
        String str2 = "%s%s%s";
        Object[] objArr = new Object[3];
        objArr[0] = "https://live.chartboost.com";
        objArr[1] = (str == null || !str.startsWith("/")) ? "/" : "";
        if (str == null) {
            str = "";
        }
        objArr[2] = str;
        return String.format(str2, objArr);
    }

    public void a(String str, Object obj) {
        e.a(this.a, str, obj);
    }

    /* access modifiers changed from: protected */
    public void c() {
        a("app", (Object) this.m.s);
        a("model", (Object) this.m.f);
        a(TapjoyConstants.TJC_DEVICE_TYPE_NAME, (Object) this.m.t);
        a("actual_device_type", (Object) this.m.u);
        a("os", (Object) this.m.g);
        a(UserDataStore.COUNTRY, (Object) this.m.h);
        a("language", (Object) this.m.i);
        a("sdk", (Object) this.m.l);
        a("user_agent", (Object) i.w);
        a("timestamp", (Object) String.valueOf(TimeUnit.MILLISECONDS.toSeconds(this.m.e.a())));
        a(SettingsJsonConstants.SESSION_KEY, (Object) Integer.valueOf(this.m.d.getInt("cbPrefSessionCount", 0)));
        a("reachability", (Object) Integer.valueOf(this.m.b.a()));
        a("scale", (Object) this.m.r);
        a("is_portrait", (Object) Boolean.valueOf(CBUtility.a(CBUtility.a())));
        a(String.BUNDLE, (Object) this.m.j);
        a("bundle_id", (Object) this.m.k);
        a("carrier", (Object) this.m.v);
        a("custom_id", (Object) i.b);
        a("mediation", (Object) i.i);
        if (i.e != null) {
            a("framework_version", (Object) i.g);
            a("wrapper_version", (Object) i.c);
        }
        a("rooted_device", (Object) Boolean.valueOf(this.m.w));
        a(TapjoyConstants.TJC_DEVICE_TIMEZONE, (Object) this.m.x);
        a("mobile_network", (Object) this.m.y);
        a("dw", (Object) this.m.o);
        a("dh", (Object) this.m.p);
        a("dpi", (Object) this.m.q);
        a("w", (Object) this.m.m);
        a("h", (Object) this.m.n);
        a("commit_hash", (Object) "7fc7bc32841a43689553f0e08928c7ad6ed7e23b");
        com.chartboost.sdk.Libraries.d.a a2 = this.m.a.a();
        a("identity", (Object) a2.b);
        if (a2.a != -1) {
            boolean z = true;
            if (a2.a != 1) {
                z = false;
            }
            a("limit_ad_tracking", (Object) Boolean.valueOf(z));
        }
        a("pidatauseconsent", (Object) Integer.valueOf(i.x.getValue()));
        String str = ((com.chartboost.sdk.Model.e) this.m.c.get()).a;
        if (!s.a().a((CharSequence) str)) {
            a("config_variant", (Object) str);
        }
        a("certification_providers", (Object) o.e());
    }

    public String d() {
        return e();
    }

    public String e() {
        if (this.n == null) {
            return "/";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.n.startsWith("/") ? "" : "/");
        sb.append(this.n);
        return sb.toString();
    }

    private void a(ai aiVar, CBError cBError) {
        Object obj;
        String str;
        String str2;
        com.chartboost.sdk.Libraries.e.a[] aVarArr = new com.chartboost.sdk.Libraries.e.a[5];
        aVarArr[0] = e.a("endpoint", (Object) e());
        String str3 = "statuscode";
        if (aiVar == null) {
            obj = "None";
        } else {
            obj = Integer.valueOf(aiVar.a);
        }
        aVarArr[1] = e.a(str3, obj);
        String str4 = "error";
        if (cBError == null) {
            str = "None";
        } else {
            str = cBError.a().toString();
        }
        aVarArr[2] = e.a(str4, (Object) str);
        String str5 = "errorDescription";
        if (cBError == null) {
            str2 = "None";
        } else {
            str2 = cBError.b();
        }
        aVarArr[3] = e.a(str5, (Object) str2);
        aVarArr[4] = e.a("retryCount", (Object) Integer.valueOf(0));
        this.p.a("request_manager", ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, cBError == null ? "success" : "failure", (String) null, (String) null, (String) null, e.a(aVarArr));
    }

    public void b(String str) {
        this.o = str;
    }

    public ag a() {
        c();
        String jSONObject = this.a.toString();
        String str = i.l;
        String b = c.b(c.a(String.format(Locale.US, "%s %s\n%s\n%s", new Object[]{this.b, d(), i.m, jSONObject}).getBytes()));
        HashMap hashMap = new HashMap();
        hashMap.put("Accept", "application/json");
        hashMap.put("X-Chartboost-Client", CBUtility.b());
        hashMap.put("X-Chartboost-API", "7.5.0");
        hashMap.put("X-Chartboost-App", str);
        hashMap.put("X-Chartboost-Signature", b);
        return new ag(hashMap, jSONObject.getBytes(), "application/json");
    }

    public ah<JSONObject> a(ai aiVar) {
        try {
            if (aiVar.b == null) {
                return ah.a(new CBError(com.chartboost.sdk.Model.CBError.a.INVALID_RESPONSE, "Response is not a valid json object"));
            }
            JSONObject jSONObject = new JSONObject(new String(aiVar.b));
            StringBuilder sb = new StringBuilder();
            sb.append("Request ");
            sb.append(e());
            sb.append(" succeeded. Response code: ");
            sb.append(aiVar.a);
            sb.append(", body: ");
            sb.append(jSONObject.toString(4));
            CBLogging.c("CBRequest", sb.toString());
            if (this.l) {
                int optInt = jSONObject.optInt("status");
                if (optInt == 404) {
                    return ah.a(new CBError(com.chartboost.sdk.Model.CBError.a.HTTP_NOT_FOUND, "404 error from server"));
                }
                if (optInt < 200 || optInt > 299) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Request failed due to status code ");
                    sb2.append(optInt);
                    sb2.append(" in message");
                    String sb3 = sb2.toString();
                    CBLogging.b("CBRequest", sb3);
                    return ah.a(new CBError(com.chartboost.sdk.Model.CBError.a.UNEXPECTED_RESPONSE, sb3));
                }
            }
            return ah.a(jSONObject);
        } catch (Exception e) {
            com.chartboost.sdk.Tracking.a.a(getClass(), "parseServerResponse", e);
            return ah.a(new CBError(com.chartboost.sdk.Model.CBError.a.MISCELLANEOUS, e.getLocalizedMessage()));
        }
    }

    public void a(JSONObject jSONObject, ai aiVar) {
        if (!(this.k == null || jSONObject == null)) {
            this.k.a(this, jSONObject);
        }
        if (this.p != null) {
            a(aiVar, (CBError) null);
        }
    }

    public void a(CBError cBError, ai aiVar) {
        if (cBError != null) {
            if (this.k != null) {
                this.k.a(this, cBError);
            }
            if (this.p != null) {
                a(aiVar, cBError);
            }
        }
    }
}
