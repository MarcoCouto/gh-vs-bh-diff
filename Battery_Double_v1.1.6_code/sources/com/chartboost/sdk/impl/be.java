package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.e.a;

@SuppressLint({"ViewConstructor"})
public class be extends RelativeLayout {
    private a a;
    private az b;
    private az c;
    private final c d;

    public void b() {
    }

    public be(Context context, c cVar) {
        super(context);
        this.d = cVar;
        if (cVar.p.m == 0) {
            this.b = new az(context);
            addView(this.b, new LayoutParams(-1, -1));
            this.c = new az(context);
            addView(this.c, new LayoutParams(-1, -1));
            this.c.setVisibility(8);
        }
    }

    public void a() {
        if (this.a == null) {
            this.a = this.d.k();
            if (this.a != null) {
                addView(this.a, new LayoutParams(-1, -1));
                this.a.a();
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        performClick();
        return true;
    }

    public az c() {
        return this.b;
    }

    public View d() {
        return this.a;
    }

    public c e() {
        return this.d;
    }

    public boolean f() {
        return this.a != null && this.a.getVisibility() == 0;
    }

    public boolean performClick() {
        super.performClick();
        return true;
    }
}
