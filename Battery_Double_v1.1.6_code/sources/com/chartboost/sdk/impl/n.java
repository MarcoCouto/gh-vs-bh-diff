package com.chartboost.sdk.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class n {
    /* JADX WARNING: Can't wrap try/catch for region: R(5:39|40|41|42|43) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00ed */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0114 A[SYNTHETIC, Splitter:B:65:0x0114] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0119 A[SYNTHETIC, Splitter:B:69:0x0119] */
    public static String a(File file, Map<String, String> map) throws Exception {
        BufferedReader bufferedReader;
        FileReader fileReader;
        int i;
        FileReader fileReader2 = null;
        try {
            fileReader = new FileReader(file);
            try {
                bufferedReader = new BufferedReader(fileReader);
                try {
                    HashMap hashMap = new HashMap();
                    for (Entry entry : map.entrySet()) {
                        String str = (String) entry.getKey();
                        if (str.startsWith("{{") || str.startsWith("{%")) {
                            hashMap.put(str, entry.getValue());
                        }
                    }
                    Set<Entry> entrySet = hashMap.entrySet();
                    int i2 = 0;
                    for (Entry value : entrySet) {
                        i2 += ((String) value.getValue()).length() * 3;
                    }
                    StringBuilder sb = new StringBuilder(((int) file.length()) + i2);
                    StringBuilder sb2 = new StringBuilder(2048);
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        int indexOf = readLine.indexOf("{{");
                        int indexOf2 = readLine.indexOf("{%");
                        if (indexOf == -1 || indexOf2 == -1) {
                            i = Math.max(indexOf, indexOf2);
                        } else {
                            i = Math.min(indexOf, indexOf2);
                        }
                        if (i == -1) {
                            sb.append(readLine);
                        } else {
                            sb2.setLength(0);
                            sb2.append(readLine);
                            for (Entry entry2 : entrySet) {
                                String str2 = (String) entry2.getKey();
                                String str3 = (String) entry2.getValue();
                                int length = str2.length();
                                while (true) {
                                    i = sb2.indexOf(str2, i);
                                    if (-1 != i) {
                                        sb2.replace(i, i + length, str3);
                                        i += str3.length();
                                    }
                                }
                            }
                            sb.append(sb2);
                        }
                        sb.append("\n");
                    }
                    String sb3 = sb.toString();
                    if (!sb3.contains("{{")) {
                        bufferedReader.close();
                        fileReader.close();
                        return sb3;
                    }
                    throw new IllegalArgumentException("Missing required template parameter");
                } catch (OutOfMemoryError e) {
                    e = e;
                    fileReader2 = fileReader;
                    try {
                        throw new Exception(e);
                    } catch (Throwable th) {
                        th = th;
                        fileReader = fileReader2;
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (IOException unused) {
                            }
                        }
                        if (fileReader != null) {
                            try {
                                fileReader.close();
                            } catch (IOException unused2) {
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (bufferedReader != null) {
                    }
                    if (fileReader != null) {
                    }
                    throw th;
                }
            } catch (OutOfMemoryError e2) {
                e = e2;
                bufferedReader = null;
                fileReader2 = fileReader;
                throw new Exception(e);
            } catch (Throwable th3) {
                th = th3;
                bufferedReader = null;
                if (bufferedReader != null) {
                }
                if (fileReader != null) {
                }
                throw th;
            }
        } catch (OutOfMemoryError e3) {
            e = e3;
            bufferedReader = null;
            throw new Exception(e);
        } catch (Throwable th4) {
            th = th4;
            fileReader = null;
            bufferedReader = null;
            if (bufferedReader != null) {
            }
            if (fileReader != null) {
            }
            throw th;
        }
    }
}
