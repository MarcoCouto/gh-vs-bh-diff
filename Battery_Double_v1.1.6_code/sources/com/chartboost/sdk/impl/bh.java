package com.chartboost.sdk.impl;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Window;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.d;
import com.chartboost.sdk.e;
import com.chartboost.sdk.g;
import com.chartboost.sdk.h;
import com.chartboost.sdk.i;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.util.List;
import org.json.JSONObject;

public class bh extends e {
    int A = 0;
    int B = 0;
    int C = 0;
    int D = 0;
    int E = 0;
    int F = -1;
    boolean G = true;
    int H = -1;
    private final f I;
    private final aj J;
    private String K = null;
    private float L = 0.0f;
    private float M = 0.0f;
    private boolean N = false;
    private int O = 0;
    final com.chartboost.sdk.Tracking.a j;
    final d k;
    final SharedPreferences l;
    public String m = "UNKNOWN";
    String n = null;
    protected int o = 1;
    long p = 0;
    long q = 0;
    boolean r = false;
    int s = 0;
    int t = 0;
    int u = 0;
    int v = 0;
    int w = 0;
    int x = 0;
    int y = 0;
    int z = 0;

    private class a extends WebViewClient {
        @TargetApi(24)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return false;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return false;
        }

        private a() {
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            if (VERSION.SDK_INT >= 26) {
                PackageInfo currentWebViewPackage = WebView.getCurrentWebViewPackage();
                if (currentWebViewPackage != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("WebView version: ");
                    sb.append(currentWebViewPackage.versionName);
                    CBLogging.a("CBWebViewProtocol", sb.toString());
                    return;
                }
                a(at.a(i.n).equalsIgnoreCase("watch") ? "No webview support." : "Device was not set up correctly.");
            }
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            bh.this.r = true;
            bh.this.q = System.currentTimeMillis();
            StringBuilder sb = new StringBuilder();
            sb.append("Total web view load response time ");
            sb.append((bh.this.q - bh.this.p) / 1000);
            CBLogging.a("CBWebViewProtocol", sb.toString());
            Context context = webView.getContext();
            if (context != null) {
                bh.this.c(context);
                bh.this.d(context);
                bh.this.o();
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error loading ");
            sb.append(str2);
            sb.append(": ");
            sb.append(str);
            a(sb.toString());
        }

        @TargetApi(23)
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            if (webResourceRequest.isForMainFrame()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Error loading ");
                sb.append(webResourceRequest.getUrl().toString());
                sb.append(": ");
                sb.append(webResourceError.getDescription());
                a(sb.toString());
            }
        }

        @TargetApi(23)
        public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
            String str = "CBWebViewProtocol";
            StringBuilder sb = new StringBuilder();
            sb.append("Error loading ");
            sb.append(webResourceRequest.getUrl().toString());
            sb.append(": ");
            sb.append(webResourceResponse == null ? "unknown error" : webResourceResponse.getReasonPhrase());
            CBLogging.a(str, sb.toString());
        }

        @TargetApi(26)
        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            String str;
            if (renderProcessGoneDetail.didCrash()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Webview crashed: ");
                sb.append(renderProcessGoneDetail.toString());
                str = sb.toString();
            } else {
                str = "Webview killed, likely due to low memory";
            }
            a(str);
            return true;
        }

        private void a(String str) {
            CBLogging.b("CBWebViewProtocol", str);
            bh.this.a(CBImpressionError.WEB_VIEW_CLIENT_RECEIVED_ERROR);
            bh.this.r = true;
            bh.this.k.d(bh.this.e);
        }
    }

    public class b extends com.chartboost.sdk.e.a {
        public bg c;
        public bf d;
        public RelativeLayout e;
        public RelativeLayout f;

        /* access modifiers changed from: protected */
        public void a(int i, int i2) {
        }

        public b(Context context, String str) {
            super(context);
            setFocusable(false);
            g a = g.a();
            this.e = (RelativeLayout) a.a(new RelativeLayout(context));
            this.f = (RelativeLayout) a.a(new RelativeLayout(context));
            this.c = (bg) a.a(new bg(context));
            h.a(context, this.c, bh.this.l);
            this.c.setWebViewClient((WebViewClient) a.a(new a()));
            bf bfVar = new bf(this.e, this.f, null, this.c, bh.this, bh.this.a);
            this.d = (bf) a.a(bfVar);
            this.c.setWebChromeClient(this.d);
            if (s.a().a(19)) {
                bg bgVar = this.c;
                bg.setWebContentsDebuggingEnabled(true);
            }
            this.c.loadDataWithBaseURL(bh.this.n, str, WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
            this.e.addView(this.c);
            this.c.getSettings().setSupportZoom(false);
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            this.e.setLayoutParams(layoutParams);
            this.c.setLayoutParams(layoutParams);
            this.c.setBackgroundColor(0);
            this.f.setVisibility(8);
            this.f.setLayoutParams(layoutParams);
            addView(this.e);
            addView(this.f);
            bh.this.p = System.currentTimeMillis();
            if (context instanceof Activity) {
                bh.this.F = ((Activity) context).getRequestedOrientation();
            } else {
                bh.this.F = -1;
            }
            o.a(this.c, bh.this.e.p.D);
            bh.this.a.postDelayed(new Runnable(bh.this) {
                public void run() {
                    if (!bh.this.r) {
                        CBLogging.a("CBWebViewProtocol", "Webview seems to be taking more time loading the html content, so closing the view.");
                        bh.this.a(CBImpressionError.WEB_VIEW_PAGE_LOAD_TIMEOUT);
                    }
                }
            }, 3000);
        }
    }

    public String a(int i) {
        switch (i) {
            case -1:
                return "none";
            case 0:
                return "landscape";
            case 1:
                return "portrait";
            default:
                return "error";
        }
    }

    public bh(c cVar, f fVar, aj ajVar, SharedPreferences sharedPreferences, com.chartboost.sdk.Tracking.a aVar, Handler handler, com.chartboost.sdk.c cVar2, d dVar) {
        super(cVar, handler, cVar2);
        this.I = fVar;
        this.J = ajVar;
        this.j = aVar;
        this.k = dVar;
        this.l = sharedPreferences;
    }

    /* access modifiers changed from: protected */
    public com.chartboost.sdk.e.a b(Context context) {
        return new b(context, this.K);
    }

    public boolean a(JSONObject jSONObject) {
        File file = this.I.d().a;
        if (file == null) {
            CBLogging.b("CBWebViewProtocol", "External Storage path is unavailable or media not mounted");
            a(CBImpressionError.ERROR_LOADING_WEB_VIEW);
            return false;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("file://");
        sb.append(file.getAbsolutePath());
        sb.append("/");
        this.n = sb.toString();
        if (s.a().a((CharSequence) this.e.p.p)) {
            CBLogging.b("CBWebViewProtocol", "Invalid adId being passed in the response");
            a(CBImpressionError.ERROR_DISPLAYING_VIEW);
            return false;
        }
        String str = this.e.o;
        if (str == null) {
            CBLogging.b("CBWebViewProtocol", "No html data found in memory");
            a(CBImpressionError.ERROR_LOADING_WEB_VIEW);
            return false;
        }
        this.K = str;
        return true;
    }

    public void h() {
        super.h();
        r();
    }

    public void b(String str) {
        if (this.e.p.y != null && !TextUtils.isEmpty(str)) {
            List<String> list = (List) this.e.p.y.get(str);
            if (list != null) {
                for (String str2 : list) {
                    if (!str2.isEmpty()) {
                        this.J.a(new af(HttpRequest.METHOD_GET, str2, 2, null));
                        StringBuilder sb = new StringBuilder();
                        sb.append("###### Sending VAST Tracking Event: ");
                        sb.append(str2);
                        CBLogging.a("CBWebViewProtocol", sb.toString());
                    }
                }
            }
        }
    }

    public void c(String str) {
        this.j.a(this.e.a.a(this.e.p.m), this.e.m, this.e.o(), str);
    }

    public void d(String str) {
        if (s.a().a((CharSequence) str)) {
            str = "Unknown Webview error";
        }
        this.j.a(this.e.a.a(this.e.p.m), this.e.m, this.e.o(), str, true);
        StringBuilder sb = new StringBuilder();
        sb.append("Webview error occurred closing the webview");
        sb.append(str);
        CBLogging.b("CBWebViewProtocol", sb.toString());
        a(CBImpressionError.ERROR_LOADING_WEB_VIEW);
        h();
    }

    public void e(String str) {
        if (s.a().a((CharSequence) str)) {
            str = "Unknown Webview warning message";
        }
        this.j.b(this.e.a.a(this.e.p.m), this.e.m, this.e.o(), str);
        StringBuilder sb = new StringBuilder();
        sb.append("Webview warning occurred closing the webview");
        sb.append(str);
        CBLogging.d("CBWebViewProtocol", sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public void o() {
        b y2 = e();
        if (y2 == null || !this.r) {
            this.B = this.x;
            this.C = this.y;
            this.D = this.z;
            this.E = this.A;
            return;
        }
        int[] iArr = new int[2];
        y2.getLocationInWindow(iArr);
        int i = iArr[0];
        int i2 = iArr[1] - this.w;
        int width = y2.getWidth();
        int height = y2.getHeight();
        this.x = i;
        this.y = i2;
        this.z = i + width;
        this.A = i2 + height;
        this.B = this.x;
        this.C = this.y;
        this.D = this.z;
        this.E = this.A;
    }

    public String p() {
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("allowOrientationChange", (Object) Boolean.valueOf(this.G)), com.chartboost.sdk.Libraries.e.a("forceOrientation", (Object) a(this.H))).toString();
    }

    public int f(String str) {
        if (str.equals("portrait")) {
            return 1;
        }
        return str.equals("landscape") ? 0 : -1;
    }

    public void c(JSONObject jSONObject) {
        this.G = jSONObject.optBoolean("allowOrientationChange", this.G);
        this.H = f(jSONObject.optString("forceOrientation", a(this.H)));
        q();
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r0.getResources().getConfiguration().orientation == 1) goto L_0x0015;
     */
    public void q() {
        Activity b2 = this.b.b();
        if (b2 != null && !CBUtility.a(b2)) {
            int i = 0;
            if (this.H != 1) {
                if (this.H != 0) {
                    if (this.G) {
                        i = -1;
                    }
                }
                b2.setRequestedOrientation(i);
            }
            i = 1;
            b2.setRequestedOrientation(i);
        }
    }

    /* access modifiers changed from: 0000 */
    public void r() {
        Activity b2 = this.b.b();
        if (b2 != null && !CBUtility.a(b2)) {
            if (b2.getRequestedOrientation() != this.F) {
                b2.setRequestedOrientation(this.F);
            }
            this.G = true;
            this.H = -1;
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.s = displayMetrics.widthPixels;
        this.t = displayMetrics.heightPixels;
    }

    /* access modifiers changed from: 0000 */
    public void d(Context context) {
        if (context instanceof Activity) {
            Window window = ((Activity) context).getWindow();
            Rect rect = new Rect();
            window.getDecorView().getWindowVisibleDisplayFrame(rect);
            this.w = a(window);
            if (this.s == 0 || this.t == 0) {
                c(context);
            }
            int width = rect.width();
            int i = this.t - this.w;
            if (!(width == this.u && i == this.v)) {
                this.u = width;
                this.v = i;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int a(Window window) {
        return window.findViewById(16908290).getTop();
    }

    public String s() {
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("width", (Object) Integer.valueOf(this.u)), com.chartboost.sdk.Libraries.e.a("height", (Object) Integer.valueOf(this.v))).toString();
    }

    public String t() {
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("width", (Object) Integer.valueOf(this.s)), com.chartboost.sdk.Libraries.e.a("height", (Object) Integer.valueOf(this.t))).toString();
    }

    public String u() {
        o();
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a(AvidJSONUtil.KEY_X, (Object) Integer.valueOf(this.x)), com.chartboost.sdk.Libraries.e.a(AvidJSONUtil.KEY_Y, (Object) Integer.valueOf(this.y)), com.chartboost.sdk.Libraries.e.a("width", (Object) Integer.valueOf(this.z)), com.chartboost.sdk.Libraries.e.a("height", (Object) Integer.valueOf(this.A))).toString();
    }

    public String v() {
        o();
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a(AvidJSONUtil.KEY_X, (Object) Integer.valueOf(this.B)), com.chartboost.sdk.Libraries.e.a(AvidJSONUtil.KEY_Y, (Object) Integer.valueOf(this.C)), com.chartboost.sdk.Libraries.e.a("width", (Object) Integer.valueOf(this.D)), com.chartboost.sdk.Libraries.e.a("height", (Object) Integer.valueOf(this.E))).toString();
    }

    public boolean l() {
        if (this.O == 2 && this.e.a.a == 1) {
            return true;
        }
        d();
        h();
        return true;
    }

    public void m() {
        super.m();
        final b y2 = e();
        if (y2 != null && y2.c != null) {
            this.a.post(new Runnable() {
                public void run() {
                    if (y2.c != null) {
                        bh.this.g("onForeground");
                        y2.c.onResume();
                    }
                }
            });
            this.j.d(this.m, this.e.o());
        }
    }

    public void n() {
        super.n();
        final b y2 = e();
        if (y2 != null && y2.c != null) {
            this.a.post(new Runnable() {
                public void run() {
                    if (y2.c != null) {
                        bh.this.g("onBackground");
                        y2.c.onPause();
                    }
                }
            });
            this.j.e(this.m, this.e.o());
        }
    }

    public void w() {
        if (this.o <= 1) {
            this.e.e();
            this.o++;
        }
    }

    public void d() {
        o.d();
        b y2 = e();
        if (y2 != null) {
            if (y2.c != null) {
                CBLogging.a("CBWebViewProtocol", "Destroying the webview object and cleaning up the references");
                y2.c.destroy();
                y2.c = null;
            }
            if (y2.d != null) {
                y2.d = null;
            }
            if (y2.e != null) {
                y2.e = null;
            }
            if (y2.f != null) {
                y2.f = null;
            }
        }
        super.d();
    }

    public void x() {
        this.j.c(this.m, this.e.o());
    }

    public void b(int i) {
        this.O = i;
    }

    /* renamed from: y */
    public b e() {
        return (b) super.e();
    }

    public void a(float f) {
        this.M = f;
    }

    public void b(float f) {
        this.L = f;
    }

    public float j() {
        return this.L;
    }

    public float k() {
        return this.M;
    }

    public void z() {
        if (this.e.l == 2 && !this.N) {
            this.j.d("", this.e.o());
            this.e.p();
            this.N = true;
            o.c();
        }
    }

    /* access modifiers changed from: 0000 */
    public void g(String str) {
        if (e().c != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("javascript:Chartboost.EventHandler.handleNativeEvent(\"");
            sb.append(str);
            sb.append("\", \"\")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Calling native to javascript: ");
            sb3.append(sb2);
            CBLogging.a("CBWebViewProtocol", sb3.toString());
            e().c.loadUrl(sb2);
        }
    }
}
