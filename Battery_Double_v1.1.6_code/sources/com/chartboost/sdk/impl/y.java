package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.h;

@SuppressLint({"ViewConstructor"})
public class y extends ab {
    private LinearLayout b;
    private ba c;
    private TextView d;

    /* access modifiers changed from: protected */
    public int b() {
        return 48;
    }

    public y(Context context, x xVar) {
        super(context, xVar);
    }

    /* access modifiers changed from: protected */
    public View a() {
        Context context = getContext();
        int round = Math.round(getContext().getResources().getDisplayMetrics().density * 8.0f);
        this.b = new LinearLayout(context);
        this.b.setOrientation(0);
        this.b.setGravity(17);
        int a = CBUtility.a(36, context);
        this.c = new ba(context);
        this.c.setPadding(round, round, round, round);
        LayoutParams layoutParams = new LayoutParams(a, a);
        this.c.setScaleType(ScaleType.FIT_CENTER);
        this.d = new TextView(context);
        this.d.setPadding(round / 2, round, round, round);
        this.d.setTextColor(-15264491);
        this.d.setTextSize(2, 16.0f);
        this.d.setTypeface(null, 1);
        this.d.setGravity(17);
        this.b.addView(this.c, layoutParams);
        this.b.addView(this.d, new LayoutParams(-2, -1));
        return this.b;
    }

    public void a(h hVar) {
        this.c.a(hVar);
        this.c.setScaleType(ScaleType.FIT_CENTER);
    }

    public void a(String str) {
        this.d.setText(str);
    }
}
