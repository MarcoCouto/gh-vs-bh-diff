package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.i;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

class j implements Comparable<j> {
    final int a;
    final String b;
    final String c;
    final String d;
    final AtomicInteger e;
    final AtomicInteger f;
    private final i g;
    private final AtomicReference<h> h;
    private final long i;

    j(i iVar, int i2, String str, String str2, String str3, AtomicInteger atomicInteger, AtomicReference<h> atomicReference, long j, AtomicInteger atomicInteger2) {
        this.g = iVar;
        this.a = i2;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = atomicInteger;
        this.h = atomicReference;
        this.i = j;
        this.f = atomicInteger2;
        atomicInteger.incrementAndGet();
    }

    /* renamed from: a */
    public int compareTo(j jVar) {
        return this.a - jVar.a;
    }

    /* access modifiers changed from: 0000 */
    public void a(Executor executor, boolean z) {
        if (this.e.decrementAndGet() == 0 || !z) {
            h hVar = (h) this.h.getAndSet(null);
            if (hVar != null) {
                executor.execute(new i(hVar, z, (int) TimeUnit.NANOSECONDS.toMillis(this.g.b() - this.i), this.f.get()));
            }
        }
    }
}
