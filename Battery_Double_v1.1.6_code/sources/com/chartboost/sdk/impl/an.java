package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.h.a;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class an {
    private final f a;
    private final Map<String, a> b = new HashMap();

    public an(f fVar) {
        this.a = fVar;
    }

    public a a(String str) {
        if (!b(str)) {
            if (this.b.containsKey(str)) {
                this.b.remove(str);
            }
            return null;
        } else if (this.b.containsKey(str)) {
            return (a) this.b.get(str);
        } else {
            a aVar = new a(str, new File(this.a.d().d, String.format("%s%s", new Object[]{str, ".png"})), this.a);
            this.b.put(str, aVar);
            return aVar;
        }
    }

    private boolean b(String str) {
        return this.a.b(String.format("%s%s", new Object[]{str, ".png"}));
    }
}
