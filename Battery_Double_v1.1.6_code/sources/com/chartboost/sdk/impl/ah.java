package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;

public class ah<T> {
    public final T a;
    public final CBError b;

    public static <T> ah<T> a(T t) {
        return new ah<>(t, null);
    }

    public static <T> ah<T> a(CBError cBError) {
        return new ah<>(null, cBError);
    }

    private ah(T t, CBError cBError) {
        this.a = t;
        this.b = cBError;
    }
}
