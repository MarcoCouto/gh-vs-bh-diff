package com.chartboost.sdk.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.tapjoy.TapjoyConstants;
import java.util.UUID;

public class at {
    public static String a(Context context) {
        PackageManager packageManager = context.getPackageManager();
        int i = context.getResources().getConfiguration().uiMode & 15;
        int i2 = context.getResources().getConfiguration().screenLayout & 15;
        if (packageManager.hasSystemFeature("org.chromium.arc.device_management") || ((Build.BRAND != null && Build.BRAND.equals("chromium") && Build.MANUFACTURER.equals("chromium")) || (Build.DEVICE != null && Build.DEVICE.matches(".+_cheets")))) {
            return "chromebook";
        }
        if (packageManager.hasSystemFeature("android.hardware.type.watch") || i == 6) {
            return "watch";
        }
        if (packageManager.hasSystemFeature("android.hardware.type.television") || i == 4) {
            return "tv";
        }
        return ((Build.MANUFACTURER == null || !Build.MANUFACTURER.equalsIgnoreCase("Amazon")) && i2 != 4) ? PlaceFields.PHONE : "tablet";
    }

    public static int b(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        if (telephonyManager != null) {
            return telephonyManager.getNetworkType();
        }
        return 0;
    }

    public static String c(Context context) {
        String str = null;
        try {
            Object obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("cb.limit.aid");
            if ((obj instanceof Integer) && ((Integer) obj).intValue() == 1) {
                return null;
            }
        } catch (Exception unused) {
        }
        String string = Secure.getString(context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
        if (!"9774d56d682e549c".equals(string)) {
            str = string;
        }
        if (str == null) {
            str = d(context);
        }
        return str;
    }

    private static String d(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("cbPrefs", 0);
        if (sharedPreferences == null) {
            return UUID.randomUUID().toString();
        }
        String string = sharedPreferences.getString("cbUUID", null);
        if (string != null) {
            return string;
        }
        String uuid = UUID.randomUUID().toString();
        Editor edit = sharedPreferences.edit();
        edit.putString("cbUUID", uuid);
        edit.apply();
        return uuid;
    }
}
