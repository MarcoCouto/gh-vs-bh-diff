package com.chartboost.sdk.impl;

import android.os.Handler;
import com.chartboost.sdk.Libraries.i;
import java.util.concurrent.Executor;

public class aj {
    private final Executor a;
    private final Executor b;
    private final aq c;
    private final ak d;
    private final i e;
    private final Handler f;

    public aj(Executor executor, aq aqVar, ak akVar, i iVar, Handler handler, Executor executor2) {
        this.a = executor2;
        this.b = executor;
        this.c = aqVar;
        this.d = akVar;
        this.e = iVar;
        this.f = handler;
    }

    public <T> void a(af<T> afVar) {
        Executor executor = this.a;
        ap apVar = new ap(this.b, this.c, this.d, this.e, this.f, afVar);
        executor.execute(apVar);
    }
}
