package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.util.HashMap;

class k extends af<Void> {
    final j a;
    private final l k;
    private final ak l;

    k(l lVar, ak akVar, j jVar, File file) {
        super(HttpRequest.METHOD_GET, jVar.c, 2, file);
        this.j = 1;
        this.k = lVar;
        this.l = akVar;
        this.a = jVar;
    }

    public ag a() {
        HashMap hashMap = new HashMap();
        hashMap.put("X-Chartboost-App", i.l);
        hashMap.put("X-Chartboost-Client", CBUtility.b());
        hashMap.put("X-Chartboost-Reachability", Integer.toString(this.l.a()));
        return new ag(hashMap, null, null);
    }

    public void a(Void voidR, ai aiVar) {
        this.k.a(this, null, null);
    }

    public void a(CBError cBError, ai aiVar) {
        this.k.a(this, cBError, aiVar);
    }
}
