package com.chartboost.sdk.impl;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;

public class bj {
    public static final BigInteger a = BigInteger.valueOf(PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
    public static final BigInteger b = a.multiply(a);
    public static final BigInteger c = a.multiply(b);
    public static final BigInteger d = a.multiply(c);
    public static final BigInteger e = a.multiply(d);
    public static final BigInteger f = a.multiply(e);
    public static final BigInteger g = BigInteger.valueOf(PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID).multiply(BigInteger.valueOf(1152921504606846976L));
    public static final BigInteger h = a.multiply(g);
    public static final File[] i = new File[0];
    private static final Charset j = Charset.forName("UTF-8");

    public static FileInputStream a(File file) throws IOException {
        if (!file.exists()) {
            StringBuilder sb = new StringBuilder();
            sb.append("File '");
            sb.append(file);
            sb.append("' does not exist");
            throw new FileNotFoundException(sb.toString());
        } else if (file.isDirectory()) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("File '");
            sb2.append(file);
            sb2.append("' exists but is a directory");
            throw new IOException(sb2.toString());
        } else if (file.canRead()) {
            return new FileInputStream(file);
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("File '");
            sb3.append(file);
            sb3.append("' cannot be read");
            throw new IOException(sb3.toString());
        }
    }

    public static byte[] b(File file) throws IOException {
        FileInputStream fileInputStream;
        try {
            fileInputStream = a(file);
            try {
                byte[] a2 = bk.a((InputStream) fileInputStream, file.length());
                bk.a((InputStream) fileInputStream);
                return a2;
            } catch (Throwable th) {
                th = th;
                bk.a((InputStream) fileInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            bk.a((InputStream) fileInputStream);
            throw th;
        }
    }
}
