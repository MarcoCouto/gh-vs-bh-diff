package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public abstract class bd extends View {
    private Bitmap a = null;
    private Canvas b = null;

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas);

    public bd(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        try {
            getClass().getMethod("setLayerType", new Class[]{Integer.TYPE, Paint.class}).invoke(this, new Object[]{Integer.valueOf(1), null});
        } catch (Exception unused) {
        }
    }

    private boolean b(Canvas canvas) {
        try {
            return ((Boolean) Canvas.class.getMethod("isHardwareAccelerated", new Class[0]).invoke(canvas, new Object[0])).booleanValue();
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        Canvas canvas2;
        boolean b2 = b(canvas);
        if (b2) {
            if (!(this.a != null && this.a.getWidth() == canvas.getWidth() && this.a.getHeight() == canvas.getHeight())) {
                if (this.a != null && !this.a.isRecycled()) {
                    this.a.recycle();
                }
                try {
                    this.a = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
                    this.b = new Canvas(this.a);
                } catch (Throwable unused) {
                    return;
                }
            }
            this.a.eraseColor(0);
            canvas2 = canvas;
            canvas = this.b;
        } else {
            canvas2 = null;
        }
        a(canvas);
        if (b2) {
            canvas2.drawBitmap(this.a, 0.0f, 0.0f, null);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.a != null && !this.a.isRecycled()) {
            this.a.recycle();
        }
        this.a = null;
    }
}
