package com.chartboost.sdk.impl;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public final class bc extends Animation {
    private final float a;
    private final float b;
    private final float c;
    private final float d;
    private boolean e = true;
    private Camera f;

    public bc(float f2, float f3, float f4, float f5, boolean z) {
        this.a = f2;
        this.b = f3;
        this.c = f4;
        this.d = f5;
        this.e = z;
    }

    public void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.f = new Camera();
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        float f3 = this.a + ((this.b - this.a) * f2);
        Camera camera = this.f;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.e) {
            camera.rotateY(f3);
        } else {
            camera.rotateX(f3);
        }
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-this.c, -this.d);
        matrix.postTranslate(this.c, this.d);
    }
}
