package com.chartboost.sdk.impl;

import android.util.Log;
import java.io.File;

public class as {
    private static String a = "CBTrace";
    private static final boolean b = a();

    private static boolean a() {
        try {
            if (!Log.isLoggable(a, 4) || !s.a().c().equals("mounted")) {
                return false;
            }
            File b2 = s.a().b();
            if (b2 != null) {
                return new File(b2, ".chartboost/log_trace").exists();
            }
            return false;
        } catch (Throwable unused) {
            return false;
        }
    }

    public static void a(String str, String str2) {
        if (b) {
            String str3 = a;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(": ");
            sb.append(str2);
            Log.i(str3, sb.toString());
        }
    }

    public static void a(String str, boolean z) {
        if (b) {
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(": ");
            sb.append(z);
            Log.i(str2, sb.toString());
        }
    }

    public static void a(String str, Object obj) {
        if (!b) {
            return;
        }
        if (obj != null) {
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(": ");
            sb.append(obj.getClass().getName());
            sb.append(" ");
            sb.append(obj.hashCode());
            Log.i(str2, sb.toString());
            return;
        }
        String str3 = a;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(": null");
        Log.i(str3, sb2.toString());
    }

    public static void a(String str) {
        if (b) {
            Log.i(a, str);
        }
    }
}
