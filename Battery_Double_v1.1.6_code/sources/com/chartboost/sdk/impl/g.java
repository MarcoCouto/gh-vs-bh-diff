package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.al.a;
import org.json.JSONObject;

public class g implements a {
    private final e a;
    private final String b;

    public g(e eVar, String str) {
        this.a = eVar;
        this.b = str;
    }

    public void a(al alVar, JSONObject jSONObject) {
        if (this.a.f.h || i.t) {
            synchronized (this.a) {
                this.a.b(this.b);
            }
        }
    }

    public void a(al alVar, CBError cBError) {
        if (this.a.f.h) {
            synchronized (this.a) {
                this.a.b(this.b);
            }
        }
    }
}
