package com.chartboost.sdk.Libraries;

import android.content.Context;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.bj;
import com.chartboost.sdk.impl.s;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONObject;

public class f {
    public final File a;
    public final File b;
    private final AtomicReference<e> c;
    private final g d;
    private final AtomicReference<g> e;
    private s f;

    public f(s sVar, Context context, AtomicReference<e> atomicReference) {
        g[] gVarArr;
        g[] gVarArr2;
        f fVar = this;
        fVar.f = sVar;
        fVar.d = new g(context.getCacheDir());
        fVar.e = new AtomicReference<>();
        fVar.c = atomicReference;
        try {
            File b2 = sVar.b();
            if (b2 != null) {
                fVar.e.set(new g(b2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        fVar.b = new File(fVar.d.a, "track");
        fVar.a = new File(fVar.d.a, SettingsJsonConstants.SESSION_KEY);
        g[] gVarArr3 = {fVar.d, (g) fVar.e.get()};
        int length = gVarArr3.length;
        int i = 0;
        while (i < length) {
            g gVar = gVarArr3[i];
            try {
                boolean z = gVar == fVar.d;
                if (gVar == null || (!z && !a())) {
                    gVarArr = gVarArr3;
                    i++;
                    gVarArr3 = gVarArr;
                    fVar = this;
                    AtomicReference<e> atomicReference2 = atomicReference;
                } else {
                    long currentTimeMillis = System.currentTimeMillis() - TimeUnit.DAYS.toMillis((long) ((e) atomicReference.get()).w);
                    File file = new File(gVar.a, "templates");
                    if (file.exists()) {
                        File[] listFiles = file.listFiles();
                        if (listFiles != null) {
                            int length2 = listFiles.length;
                            int i2 = 0;
                            while (i2 < length2) {
                                File file2 = listFiles[i2];
                                if (file2.isDirectory()) {
                                    File[] listFiles2 = file2.listFiles();
                                    if (listFiles2 != null) {
                                        int length3 = listFiles2.length;
                                        int i3 = 0;
                                        while (i3 < length3) {
                                            File file3 = listFiles2[i3];
                                            if (!z) {
                                                if (file3.lastModified() < currentTimeMillis) {
                                                }
                                                gVarArr = gVarArr3;
                                                i3++;
                                                gVarArr3 = gVarArr;
                                                AtomicReference<e> atomicReference3 = atomicReference;
                                            }
                                            if (!file3.delete()) {
                                                String str = "FileCache";
                                                StringBuilder sb = new StringBuilder();
                                                gVarArr = gVarArr3;
                                                try {
                                                    sb.append("Unable to delete ");
                                                    sb.append(file3.getPath());
                                                    CBLogging.b(str, sb.toString());
                                                    i3++;
                                                    gVarArr3 = gVarArr;
                                                    AtomicReference<e> atomicReference32 = atomicReference;
                                                } catch (Exception e3) {
                                                    e = e3;
                                                    StringBuilder sb2 = new StringBuilder();
                                                    sb2.append("Exception while cleaning up templates directory at ");
                                                    sb2.append(gVar.f.getPath());
                                                    CBLogging.a("FileCache", sb2.toString(), e);
                                                    e.printStackTrace();
                                                    i++;
                                                    gVarArr3 = gVarArr;
                                                    fVar = this;
                                                    AtomicReference<e> atomicReference22 = atomicReference;
                                                }
                                            }
                                            gVarArr = gVarArr3;
                                            i3++;
                                            gVarArr3 = gVarArr;
                                            AtomicReference<e> atomicReference322 = atomicReference;
                                        }
                                    }
                                    gVarArr2 = gVarArr3;
                                    File[] listFiles3 = file2.listFiles();
                                    if (listFiles3 != null && listFiles3.length == 0 && !file2.delete()) {
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append("Unable to delete ");
                                        sb3.append(file2.getPath());
                                        CBLogging.b("FileCache", sb3.toString());
                                    }
                                } else {
                                    gVarArr2 = gVarArr3;
                                }
                                i2++;
                                gVarArr3 = gVarArr2;
                                AtomicReference<e> atomicReference4 = atomicReference;
                            }
                        }
                    }
                    gVarArr = gVarArr3;
                    File file4 = new File(gVar.a, ".adId");
                    if (file4.exists() && ((z || file4.lastModified() < currentTimeMillis) && !file4.delete())) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Unable to delete ");
                        sb4.append(file4.getPath());
                        CBLogging.b("FileCache", sb4.toString());
                    }
                    i++;
                    gVarArr3 = gVarArr;
                    fVar = this;
                    AtomicReference<e> atomicReference222 = atomicReference;
                }
            } catch (Exception e4) {
                e = e4;
                gVarArr = gVarArr3;
                StringBuilder sb22 = new StringBuilder();
                sb22.append("Exception while cleaning up templates directory at ");
                sb22.append(gVar.f.getPath());
                CBLogging.a("FileCache", sb22.toString(), e);
                e.printStackTrace();
                i++;
                gVarArr3 = gVarArr;
                fVar = this;
                AtomicReference<e> atomicReference2222 = atomicReference;
            }
        }
    }

    public synchronized byte[] a(File file) {
        byte[] bArr;
        if (file == null) {
            return null;
        }
        try {
            bArr = bj.b(file);
        } catch (Exception e2) {
            CBLogging.a("FileCache", "Error loading cache from disk", e2);
            a.a(getClass(), "readByteArrayFromDisk", e2);
            bArr = null;
        }
        return bArr;
    }

    public String a(String str) {
        File file = new File(d().g, str);
        if (file.exists()) {
            return file.getPath();
        }
        return null;
    }

    public boolean a() {
        try {
            String c2 = this.f.c();
            if (c2 != null && c2.equals("mounted") && !i.o) {
                return true;
            }
        } catch (Exception e2) {
            a.a(getClass(), "isExternalStorageAvailable", e2);
        }
        CBLogging.e("FileCache", "External Storage unavailable");
        return false;
    }

    public boolean b(String str) {
        if (d().d == null || str == null) {
            return false;
        }
        return new File(d().d, str).exists();
    }

    public JSONArray b() {
        JSONArray jSONArray = new JSONArray();
        String[] list = d().g.list();
        if (list != null) {
            for (String str : list) {
                if (!str.equals(".nomedia") && !str.endsWith(".tmp")) {
                    jSONArray.put(str);
                }
            }
        }
        return jSONArray;
    }

    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            File file = d().a;
            for (String str : ((e) this.c.get()).x) {
                if (!str.equals("templates")) {
                    File file2 = new File(file, str);
                    JSONArray jSONArray = new JSONArray();
                    if (file2.exists()) {
                        String[] list = file2.list();
                        if (list != null) {
                            for (String str2 : list) {
                                if (!str2.equals(".nomedia") && !str2.endsWith(".tmp")) {
                                    jSONArray.put(str2);
                                }
                            }
                        }
                    }
                    e.a(jSONObject, str, jSONArray);
                }
            }
        } catch (Exception e2) {
            a.a(getClass(), "getWebViewCacheAssets", e2);
        }
        return jSONObject;
    }

    public g d() {
        if (a()) {
            g gVar = (g) this.e.get();
            if (gVar == null) {
                try {
                    File b2 = this.f.b();
                    if (b2 != null) {
                        this.e.compareAndSet(null, new g(b2));
                        gVar = (g) this.e.get();
                    }
                } catch (Exception e2) {
                    a.a(getClass(), "currentLocations", e2);
                }
            }
            if (gVar != null) {
                return gVar;
            }
        }
        return this.d;
    }

    public long b(File file) {
        long j = 0;
        if (file != null) {
            try {
                if (file.isDirectory()) {
                    File[] listFiles = file.listFiles();
                    if (listFiles == null) {
                        return 0;
                    }
                    for (File b2 : listFiles) {
                        j += b(b2);
                    }
                    return j;
                }
            } catch (Exception e2) {
                a.a(f.class, "getFolderSize", e2);
                return 0;
            }
        }
        if (file != null) {
            return file.length();
        }
        return 0;
    }

    public JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        g gVar = (g) this.e.get();
        if (gVar != null) {
            e.a(jSONObject, ".chartboost-external-folder-size", Long.valueOf(b(gVar.a)));
        }
        e.a(jSONObject, ".chartboost-internal-folder-size", Long.valueOf(b(this.d.a)));
        File file = d().a;
        String[] list = file.list();
        if (list != null && list.length > 0) {
            for (String file2 : list) {
                File file3 = new File(file, file2);
                JSONObject jSONObject2 = new JSONObject();
                StringBuilder sb = new StringBuilder();
                sb.append(file3.getName());
                sb.append("-size");
                e.a(jSONObject2, sb.toString(), Long.valueOf(b(file3)));
                String[] list2 = file3.list();
                if (list2 != null) {
                    e.a(jSONObject2, "count", Integer.valueOf(list2.length));
                }
                e.a(jSONObject, file3.getName(), jSONObject2);
            }
        }
        return jSONObject;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0042 A[SYNTHETIC, Splitter:B:29:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    public void c(File file) {
        RandomAccessFile randomAccessFile = null;
        try {
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
            try {
                randomAccessFile2.seek(0);
                int read = randomAccessFile2.read();
                randomAccessFile2.seek(0);
                randomAccessFile2.write(read);
                try {
                    randomAccessFile2.close();
                } catch (IOException unused) {
                }
            } catch (FileNotFoundException e2) {
                e = e2;
                randomAccessFile = randomAccessFile2;
                CBLogging.a("FileCache", "File not found when attempting to touch", e);
                if (randomAccessFile == null) {
                }
                randomAccessFile.close();
            } catch (IOException e3) {
                e = e3;
                randomAccessFile = randomAccessFile2;
                try {
                    CBLogging.a("FileCache", "IOException when attempting to touch file", e);
                    if (randomAccessFile == null) {
                    }
                    randomAccessFile.close();
                } catch (Throwable th) {
                    th = th;
                    if (randomAccessFile != null) {
                        try {
                            randomAccessFile.close();
                        } catch (IOException unused2) {
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                randomAccessFile = randomAccessFile2;
                if (randomAccessFile != null) {
                }
                throw th;
            }
        } catch (FileNotFoundException e4) {
            e = e4;
            CBLogging.a("FileCache", "File not found when attempting to touch", e);
            if (randomAccessFile == null) {
                return;
            }
            randomAccessFile.close();
        } catch (IOException e5) {
            e = e5;
            CBLogging.a("FileCache", "IOException when attempting to touch file", e);
            if (randomAccessFile == null) {
                return;
            }
            randomAccessFile.close();
        }
    }
}
