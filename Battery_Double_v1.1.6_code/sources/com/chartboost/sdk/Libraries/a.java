package com.chartboost.sdk.Libraries;

import android.content.Context;
import com.chartboost.sdk.impl.s;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;

public class a {
    public int a;
    public String b;

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002d  */
    public a(Context context) {
        Info info;
        try {
            info = s.a().a(context);
        } catch (IllegalStateException unused) {
            CBLogging.b("ContentValues", "This should have been called off the main thread.");
            info = null;
            if (info == null) {
            }
        } catch (IOException unused2) {
            CBLogging.b("ContentValues", "The connection to Google Play Services failed.");
            info = null;
            if (info == null) {
            }
        } catch (GooglePlayServicesRepairableException unused3) {
            CBLogging.b("ContentValues", "There was a recoverable error connecting to Google Play Services.");
            info = null;
            if (info == null) {
            }
        } catch (GooglePlayServicesNotAvailableException unused4) {
            info = null;
            if (info == null) {
            }
        }
        if (info == null) {
            this.a = -1;
            this.b = null;
        } else if (info.isLimitAdTrackingEnabled()) {
            this.a = 1;
            this.b = null;
        } else {
            this.a = 0;
            this.b = info.getId();
        }
    }
}
