package com.chartboost.sdk.Libraries;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.view.Display;
import android.view.WindowManager;
import com.chartboost.sdk.Chartboost.CBFramework;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.i;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class CBUtility {
    public static boolean a(int i) {
        return i == 0 || i == 2;
    }

    public static boolean b(int i) {
        return i == 1 || i == 3;
    }

    private CBUtility() {
    }

    public static float a(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static int a(int i, Context context) {
        return Math.round(((float) i) * a(context));
    }

    public static float a(float f, Context context) {
        return f * a(context);
    }

    public static int a() {
        Context context = i.n;
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        int rotation = defaultDisplay.getRotation();
        boolean z = defaultDisplay.getWidth() != defaultDisplay.getHeight() ? defaultDisplay.getWidth() < defaultDisplay.getHeight() : context.getResources().getConfiguration().orientation != 2;
        if (!(rotation == 0 || rotation == 2)) {
            z = !z;
        }
        if (z) {
            switch (rotation) {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                default:
                    return 0;
            }
        } else {
            switch (rotation) {
                case 1:
                    return 2;
                case 2:
                    return 3;
                case 3:
                    return 0;
                default:
                    return 1;
            }
        }
    }

    public static void throwProguardError(Exception exc) {
        if (exc instanceof NoSuchMethodException) {
            CBLogging.b("CBUtility", "Chartboost library error! Have you used proguard on your application? Make sure to add the line '-keep class com.chartboost.sdk.** { *; }' to your proguard config file.");
        } else if (exc == null || exc.getMessage() == null) {
            CBLogging.b("CBUtility", "Unknown Proguard error");
        } else {
            CBLogging.b("CBUtility", exc.getMessage());
        }
    }

    public static String b() {
        String str = "%s %s %s";
        Object[] objArr = new Object[3];
        objArr[0] = "Chartboost-Android-SDK";
        objArr[1] = i.e == null ? "" : i.e;
        objArr[2] = "7.5.0";
        return String.format(str, objArr);
    }

    public static boolean c() {
        return e() || f() || g();
    }

    public static String d() {
        SimpleDateFormat simpleDateFormat;
        if (VERSION.SDK_INT >= 18) {
            simpleDateFormat = new SimpleDateFormat("ZZZZ", Locale.US);
        } else {
            simpleDateFormat = new SimpleDateFormat("'GMT'ZZZZ", Locale.US);
        }
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        return simpleDateFormat.format(new Date());
    }

    private static boolean e() {
        String str = Build.TAGS;
        return str != null && str.contains("test-keys");
    }

    private static boolean f() {
        return new File("/system/app/Superuser.apk").exists();
    }

    private static boolean g() {
        for (String file : new String[]{"/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su"}) {
            if (new File(file).exists()) {
                return true;
            }
        }
        return false;
    }

    public static void a(Activity activity, int i, e eVar) {
        if (activity != null && !a(activity)) {
            if ((i == 1 && eVar.y && eVar.C) || (i == 0 && eVar.e && eVar.h)) {
                int a = a();
                if (a == 0) {
                    activity.setRequestedOrientation(1);
                } else if (a == 2) {
                    activity.setRequestedOrientation(9);
                } else if (a == 1) {
                    activity.setRequestedOrientation(0);
                } else {
                    activity.setRequestedOrientation(8);
                }
            }
        }
    }

    public static void b(Activity activity, int i, e eVar) {
        if (activity != null && !a(activity)) {
            if ((i == 1 && eVar.y && eVar.C) || (i == 0 && eVar.e && eVar.h)) {
                activity.setRequestedOrientation(-1);
            }
        }
    }

    public static boolean a(CBFramework cBFramework) {
        return i.e != null && i.e == cBFramework;
    }

    public static ArrayList<File> a(File file, boolean z) {
        if (file == null) {
            return null;
        }
        ArrayList<File> arrayList = new ArrayList<>();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile() && !file2.getName().equals(".nomedia")) {
                    arrayList.add(file2);
                } else if (file2.isDirectory() && z) {
                    arrayList.addAll(a(file2, z));
                }
            }
        }
        return arrayList;
    }

    public static boolean a(Activity activity) {
        boolean z = true;
        if (activity == null || activity.getWindow() == null || activity.getWindow().getDecorView() == null || activity.getWindow().getDecorView().getBackground() == null) {
            return true;
        }
        if (VERSION.SDK_INT != 26 || activity.getApplicationInfo().targetSdkVersion <= 26 || activity.getWindow().getDecorView().getBackground().getAlpha() == 255) {
            z = false;
        }
        return z;
    }
}
