package com.chartboost.sdk.Libraries;

import com.chartboost.sdk.Tracking.a;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public final class c {
    private c() {
    }

    public static synchronized byte[] a(byte[] bArr) {
        synchronized (c.class) {
            if (bArr == null) {
                return null;
            }
            try {
                MessageDigest instance = MessageDigest.getInstance(CommonUtils.SHA1_INSTANCE);
                instance.update(bArr);
                byte[] digest = instance.digest();
                return digest;
            } catch (NoSuchAlgorithmException e) {
                a.a(c.class, "sha1", (Exception) e);
                return null;
            } catch (Exception e2) {
                a.a(c.class, "sha1", e2);
                return null;
            }
        }
    }

    public static String b(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        BigInteger bigInteger = new BigInteger(1, bArr);
        Locale locale = Locale.US;
        StringBuilder sb = new StringBuilder();
        sb.append("%0");
        sb.append(bArr.length << 1);
        sb.append(AvidJSONUtil.KEY_X);
        return String.format(locale, sb.toString(), new Object[]{bigInteger});
    }
}
