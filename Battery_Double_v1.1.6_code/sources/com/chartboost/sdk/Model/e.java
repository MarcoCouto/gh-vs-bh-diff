package com.chartboost.sdk.Model;

import android.os.Build;
import android.os.Build.VERSION;
import com.chartboost.sdk.Chartboost.CBPIDataUseConsent;
import com.chartboost.sdk.Libraries.b;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.o;
import com.chartboost.sdk.impl.s;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants.String;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class e {
    public final boolean A;
    public final int B;
    public final boolean C;
    public final int D;
    public final boolean E;
    public final String F;
    public final String G;
    public final String H;
    public final String I;
    public final boolean J;
    public final boolean K;
    public final boolean L;
    public final String a;
    public final boolean b;
    public final boolean c;
    public final List<String> d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final int i;
    public final boolean j;
    public final boolean k;
    public final boolean l;
    public final boolean m;
    public final boolean n;
    public final boolean o;
    public final boolean p;
    public final boolean q;
    public final boolean r;
    public final long s;
    public final int t;
    public final int u;
    public final int v;
    public final int w;
    public final List<String> x;
    public final boolean y;
    public final boolean z;

    public e(JSONObject jSONObject) {
        this.a = jSONObject.optString("configVariant");
        this.b = jSONObject.optBoolean("prefetchDisable");
        this.c = jSONObject.optBoolean("publisherDisable");
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("invalidateFolderList");
        boolean z2 = false;
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                String optString = optJSONArray.optString(i2);
                if (!optString.isEmpty()) {
                    arrayList.add(optString);
                }
            }
        }
        this.d = Collections.unmodifiableList(arrayList);
        JSONObject optJSONObject = jSONObject.optJSONObject("native");
        if (optJSONObject == null) {
            optJSONObject = new JSONObject();
        }
        boolean z3 = true;
        this.e = optJSONObject.optBoolean(String.ENABLED, true);
        this.f = optJSONObject.optBoolean("inplayEnabled", true);
        this.g = optJSONObject.optBoolean("interstitialEnabled", true);
        this.h = optJSONObject.optBoolean("lockOrientation");
        this.i = optJSONObject.optInt("prefetchSession", 3);
        this.j = optJSONObject.optBoolean("rewardVideoEnabled", true);
        JSONObject optJSONObject2 = jSONObject.optJSONObject("trackingLevels");
        if (optJSONObject2 == null) {
            optJSONObject2 = new JSONObject();
        }
        this.k = optJSONObject2.optBoolean("critical", true);
        this.r = optJSONObject2.optBoolean("includeStackTrace", true);
        this.l = optJSONObject2.optBoolean("error");
        this.m = optJSONObject2.optBoolean("debug");
        this.n = optJSONObject2.optBoolean(SettingsJsonConstants.SESSION_KEY);
        this.o = optJSONObject2.optBoolean("system");
        this.p = optJSONObject2.optBoolean("timing");
        this.q = optJSONObject2.optBoolean("user");
        this.s = jSONObject.optLong("getAdRetryBaseMs", b.b);
        this.t = jSONObject.optInt("getAdRetryMaxBackoffExponent", 5);
        JSONObject optJSONObject3 = jSONObject.optJSONObject(ParametersKeys.WEB_VIEW);
        if (optJSONObject3 == null) {
            optJSONObject3 = new JSONObject();
        }
        boolean z4 = !"Amazon".equalsIgnoreCase(Build.MANUFACTURER) || VERSION.SDK_INT >= 21;
        this.u = optJSONObject3.optInt("cacheMaxBytes", 104857600);
        int optInt = optJSONObject3.optInt("cacheMaxUnits", 10);
        if (optInt <= 0) {
            optInt = 10;
        }
        this.v = optInt;
        this.w = (int) TimeUnit.SECONDS.toDays((long) optJSONObject3.optInt("cacheTTLs", b.a));
        ArrayList arrayList2 = new ArrayList();
        JSONArray optJSONArray2 = optJSONObject3.optJSONArray("directories");
        if (optJSONArray2 != null) {
            int length2 = optJSONArray2.length();
            for (int i3 = 0; i3 < length2; i3++) {
                String optString2 = optJSONArray2.optString(i3);
                if (!optString2.isEmpty()) {
                    arrayList2.add(optString2);
                }
            }
        }
        this.x = Collections.unmodifiableList(arrayList2);
        this.y = z4 && optJSONObject3.optBoolean(String.ENABLED, a());
        this.z = optJSONObject3.optBoolean("inplayEnabled", true);
        this.A = optJSONObject3.optBoolean("interstitialEnabled", true);
        int optInt2 = optJSONObject3.optInt("invalidatePendingImpression", 3);
        if (optInt2 <= 0) {
            optInt2 = 3;
        }
        this.B = optInt2;
        this.C = optJSONObject3.optBoolean("lockOrientation", true);
        this.D = optJSONObject3.optInt("prefetchSession", 3);
        this.E = optJSONObject3.optBoolean("rewardVideoEnabled", true);
        this.F = optJSONObject3.optString("version", "v2");
        this.G = String.format("%s/%s%s", new Object[]{ParametersKeys.WEB_VIEW, this.F, "/interstitial/get"});
        this.H = String.format("%s/%s/%s", new Object[]{ParametersKeys.WEB_VIEW, this.F, "prefetch"});
        this.I = String.format("%s/%s%s", new Object[]{ParametersKeys.WEB_VIEW, this.F, "/reward/get"});
        ArrayList arrayList3 = new ArrayList();
        boolean z5 = i.x != CBPIDataUseConsent.NO_BEHAVIORAL;
        if (i.x == CBPIDataUseConsent.NO_BEHAVIORAL) {
            z3 = false;
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject("certificationProviders");
        if (optJSONObject4 != null) {
            JSONObject optJSONObject5 = optJSONObject4.optJSONObject("moat");
            if (optJSONObject5 != null) {
                arrayList3.add("moat");
                z2 = optJSONObject5.optBoolean("loggingEnabled", false);
                z5 = optJSONObject5.optBoolean("locationEnabled", z5);
                z3 = optJSONObject5.optBoolean("idfaCollectionEnabled", z3);
            }
        }
        this.J = z2;
        this.K = z5;
        this.L = z3;
        o.a((List<String>) arrayList3);
    }

    private static boolean a() {
        int[] iArr = {4, 4, 2};
        String d2 = s.a().d();
        if (d2 == null || d2.length() <= 0) {
            return false;
        }
        String[] split = d2.replaceAll("[^\\d.]", "").split("\\.");
        int i2 = 0;
        while (i2 < split.length && i2 < iArr.length) {
            try {
                if (Integer.valueOf(split[i2]).intValue() > iArr[i2]) {
                    return true;
                }
                if (Integer.valueOf(split[i2]).intValue() < iArr[i2]) {
                    return false;
                }
                i2++;
            } catch (NumberFormatException unused) {
                return false;
            }
        }
        return false;
    }
}
