package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.impl.s;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public final class b {
    static void a(String str) {
        if (i.e == null) {
            CBLogging.b("CBConfig", "Set a valid CBFramework first");
        } else if (TextUtils.isEmpty(str)) {
            CBLogging.b("CBConfig", "Invalid Version String");
        } else {
            i.c = str;
        }
    }

    public static boolean a(AtomicReference<e> atomicReference, JSONObject jSONObject, SharedPreferences sharedPreferences) {
        try {
            atomicReference.set(new e(jSONObject));
            return true;
        } catch (Exception e) {
            a.a(b.class, "updateConfig", e);
            return false;
        }
    }

    public static boolean a() {
        return b() && c();
    }

    private static boolean c() {
        h a = h.a();
        if (a == null) {
            return false;
        }
        if (a.q.d != null) {
            return true;
        }
        try {
            throw new Exception("Chartboost Weak Activity reference is null");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    static boolean a(Activity activity) {
        if (activity != null) {
            return true;
        }
        try {
            throw new Exception("Invalid activity context: Host Activity object is null, Please send a valid activity object");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    static boolean b() {
        try {
            if (h.a() == null) {
                throw new Exception("SDK Initialization error. SDK seems to be not initialized properly, check for any integration issues");
            } else if (i.n == null) {
                throw new Exception("SDK Initialization error. Activity context seems to be not initialized properly, host activity or application context is being sent as null");
            } else if (TextUtils.isEmpty(i.l)) {
                throw new Exception("SDK Initialization error. AppId is missing");
            } else if (!TextUtils.isEmpty(i.m)) {
                return true;
            } else {
                throw new Exception("SDK Initialization error. AppSignature is missing");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(Context context) {
        int i;
        int i2;
        int i3;
        int i4;
        if (context != null) {
            try {
                if (s.a().a(23)) {
                    i4 = context.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE");
                    i3 = context.checkSelfPermission("android.permission.ACCESS_NETWORK_STATE");
                    i2 = context.checkSelfPermission("android.permission.INTERNET");
                    i = context.checkSelfPermission("android.permission.READ_PHONE_STATE");
                } else {
                    i4 = context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE");
                    i3 = context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE");
                    i2 = context.checkCallingOrSelfPermission("android.permission.INTERNET");
                    i = context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE");
                }
                i.o = i4 != 0;
                i.p = i2 != 0;
                i.q = i3 != 0;
                i.r = i != 0;
                if (i.p) {
                    throw new RuntimeException("Please add the permission : android.permission.INTERNET in your android manifest.xml");
                } else if (!i.q) {
                    return true;
                } else {
                    throw new RuntimeException("Please add the permission : android.permission.ACCESS_NETWORK_STATE in your android manifest.xml");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            throw new RuntimeException("Invalid activity context passed during intitalization");
        }
    }

    public static boolean b(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(context, CBImpressionActivity.class);
        boolean z = false;
        List queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        if (queryIntentActivities.isEmpty()) {
            return false;
        }
        ActivityInfo activityInfo = ((ResolveInfo) queryIntentActivities.get(0)).activityInfo;
        if (!((activityInfo.flags & 512) == 0 || (activityInfo.flags & 32) == 0 || (activityInfo.configChanges & 128) == 0 || (activityInfo.configChanges & 32) == 0 || (activityInfo.configChanges & 1024) == 0)) {
            z = true;
        }
        return z;
    }

    public static String a(e eVar) {
        return !eVar.y ? "native" : "web";
    }
}
