package com.chartboost.sdk.Tracking;

import android.text.TextUtils;
import android.util.Base64;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.h;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.al;
import com.chartboost.sdk.impl.ar;
import com.unity3d.services.purchasing.core.TransactionDetailsUtilities;
import java.util.EnumMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class CBAnalytics {

    public enum CBIAPPurchaseInfo {
        PRODUCT_ID,
        PRODUCT_TITLE,
        PRODUCT_DESCRIPTION,
        PRODUCT_PRICE,
        PRODUCT_CURRENCY_CODE,
        GOOGLE_PURCHASE_DATA,
        GOOGLE_PURCHASE_SIGNATURE,
        AMAZON_PURCHASE_TOKEN,
        AMAZON_USER_ID
    }

    public enum CBIAPType {
        GOOGLE_PLAY,
        AMAZON
    }

    public enum CBLevelType {
        HIGHEST_LEVEL_REACHED(1),
        CURRENT_AREA(2),
        CHARACTER_LEVEL(3),
        OTHER_SEQUENTIAL(4),
        OTHER_NONSEQUENTIAL(5);
        
        private final int a;

        private CBLevelType(int i) {
            this.a = i;
        }

        public int getLevelType() {
            return this.a;
        }
    }

    private CBAnalytics() {
    }

    public synchronized void trackInAppPurchaseEvent(EnumMap<CBIAPPurchaseInfo, String> enumMap, CBIAPType cBIAPType) {
        if (!(enumMap == null || cBIAPType == null)) {
            if (!TextUtils.isEmpty((CharSequence) enumMap.get(CBIAPPurchaseInfo.PRODUCT_ID)) && !TextUtils.isEmpty((CharSequence) enumMap.get(CBIAPPurchaseInfo.PRODUCT_TITLE)) && !TextUtils.isEmpty((CharSequence) enumMap.get(CBIAPPurchaseInfo.PRODUCT_DESCRIPTION)) && !TextUtils.isEmpty((CharSequence) enumMap.get(CBIAPPurchaseInfo.PRODUCT_PRICE))) {
                if (!TextUtils.isEmpty((CharSequence) enumMap.get(CBIAPPurchaseInfo.PRODUCT_CURRENCY_CODE))) {
                    a((String) enumMap.get(CBIAPPurchaseInfo.PRODUCT_ID), (String) enumMap.get(CBIAPPurchaseInfo.PRODUCT_TITLE), (String) enumMap.get(CBIAPPurchaseInfo.PRODUCT_DESCRIPTION), (String) enumMap.get(CBIAPPurchaseInfo.PRODUCT_PRICE), (String) enumMap.get(CBIAPPurchaseInfo.PRODUCT_CURRENCY_CODE), (String) enumMap.get(CBIAPPurchaseInfo.GOOGLE_PURCHASE_DATA), (String) enumMap.get(CBIAPPurchaseInfo.GOOGLE_PURCHASE_SIGNATURE), (String) enumMap.get(CBIAPPurchaseInfo.AMAZON_USER_ID), (String) enumMap.get(CBIAPPurchaseInfo.AMAZON_PURCHASE_TOKEN), cBIAPType);
                    return;
                }
            }
        }
        CBLogging.b("CBPostInstallTracker", "Null object is passed. Please pass a valid value object");
    }

    private static synchronized void a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, CBIAPType cBIAPType) {
        CBIAPType cBIAPType2 = cBIAPType;
        synchronized (CBAnalytics.class) {
            if (i.n == null) {
                CBLogging.b("CBPostInstallTracker", "You need call Chartboost.init() before calling any public API's");
                return;
            }
            h a = h.a();
            if (a == null) {
                CBLogging.b("CBPostInstallTracker", "You need call Chartboost.startWithAppId() before tracking in-app purchases");
            } else if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4) || TextUtils.isEmpty(str5)) {
                CBLogging.b("CBPostInstallTracker", "Null object is passed. Please pass a valid value object");
            } else {
                try {
                    String str10 = str4;
                    Matcher matcher = Pattern.compile("(\\d+\\.\\d+)|(\\d+)").matcher(str4);
                    matcher.find();
                    String group = matcher.group();
                    if (TextUtils.isEmpty(group)) {
                        CBLogging.b("CBPostInstallTracker", "Invalid price object");
                        return;
                    }
                    float parseFloat = Float.parseFloat(group);
                    JSONObject jSONObject = null;
                    if (cBIAPType2 == CBIAPType.GOOGLE_PLAY) {
                        if (!TextUtils.isEmpty(str6)) {
                            if (!TextUtils.isEmpty(str7)) {
                                jSONObject = e.a(e.a("purchaseData", (Object) str6), e.a("purchaseSignature", (Object) str7), e.a("type", (Object) Integer.valueOf(CBIAPType.GOOGLE_PLAY.ordinal())));
                            }
                        }
                        CBLogging.b("CBPostInstallTracker", "Null object is passed for for purchase data or purchase signature");
                        return;
                    } else if (cBIAPType2 == CBIAPType.AMAZON) {
                        if (!TextUtils.isEmpty(str8)) {
                            if (!TextUtils.isEmpty(str9)) {
                                jSONObject = e.a(e.a("userID", (Object) str8), e.a("purchaseToken", (Object) str9), e.a("type", (Object) Integer.valueOf(CBIAPType.AMAZON.ordinal())));
                            }
                        }
                        CBLogging.b("CBPostInstallTracker", "Null object is passed for for amazon user id or amazon purchase token");
                        return;
                    }
                    if (jSONObject == null) {
                        CBLogging.b("CBPostInstallTracker", "Error while parsing the receipt to a JSON Object, ");
                        return;
                    }
                    String str11 = str2;
                    String str12 = str3;
                    String str13 = str;
                    JSONObject a2 = e.a(e.a("localized-title", (Object) str2), e.a("localized-description", (Object) str3), e.a("price", (Object) Float.valueOf(parseFloat)), e.a("currency", (Object) str5), e.a("productID", (Object) str), e.a(TransactionDetailsUtilities.RECEIPT, (Object) Base64.encodeToString(jSONObject.toString().getBytes(), 2)));
                    a(a.h, a.j, a.o, a2, "iap", cBIAPType);
                } catch (IllegalStateException unused) {
                    CBLogging.b("CBPostInstallTracker", "Invalid price object");
                }
            }
        }
    }

    public static synchronized void trackInAppGooglePlayPurchaseEvent(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        synchronized (CBAnalytics.class) {
            a(str5, str, str2, str3, str4, str6, str7, null, null, CBIAPType.GOOGLE_PLAY);
        }
    }

    public static synchronized void trackInAppAmazonStorePurchaseEvent(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        synchronized (CBAnalytics.class) {
            a(str5, str, str2, str3, str4, null, null, str6, str7, CBIAPType.AMAZON);
        }
    }

    public static synchronized void trackLevelInfo(String str, CBLevelType cBLevelType, int i, String str2) {
        synchronized (CBAnalytics.class) {
            trackLevelInfo(str, cBLevelType, i, 0, str2);
        }
    }

    public static synchronized void trackLevelInfo(String str, CBLevelType cBLevelType, int i, int i2, String str2) {
        synchronized (CBAnalytics.class) {
            h a = h.a();
            if (a == null) {
                CBLogging.b("CBPostInstallTracker", "trackLevelInfo: SDK is not initialized");
            } else if (TextUtils.isEmpty(str)) {
                CBLogging.b("CBPostInstallTracker", "Invalid value: event label cannot be empty or null");
            } else if (cBLevelType == null) {
                CBLogging.b("CBPostInstallTracker", "Invalid value: level type cannot be empty or null, please choose from one of the CBLevelType enum values");
            } else if (i < 0 || i2 < 0) {
                CBLogging.b("CBPostInstallTracker", "Invalid value: Level number should be > 0");
            } else if (str2.isEmpty()) {
                CBLogging.b("CBPostInstallTracker", "Invalid value: description cannot be empty or null");
            } else {
                JSONObject a2 = e.a(e.a("event_label", (Object) str), e.a("event_field", (Object) Integer.valueOf(cBLevelType.getLevelType())), e.a("main_level", (Object) Integer.valueOf(i)), e.a("sub_level", (Object) Integer.valueOf(i2)), e.a("description", (Object) str2), e.a("timestamp", (Object) Long.valueOf(System.currentTimeMillis())), e.a("data_type", (Object) "level_info"));
                JSONArray jSONArray = new JSONArray();
                jSONArray.put(a2);
                a(a.h, a.j, a.o, jSONArray, "tracking");
            }
        }
    }

    private static synchronized void a(aj ajVar, ar arVar, a aVar, JSONObject jSONObject, String str, CBIAPType cBIAPType) {
        synchronized (CBAnalytics.class) {
            al alVar = new al(String.format(Locale.US, "%s%s", new Object[]{"/post-install-event/", str}), arVar, aVar, 2, null);
            alVar.a(str, (Object) jSONObject);
            alVar.l = true;
            alVar.b(str);
            ajVar.a(alVar);
        }
    }

    private static synchronized void a(aj ajVar, ar arVar, a aVar, JSONArray jSONArray, String str) {
        synchronized (CBAnalytics.class) {
            al alVar = new al("/post-install-event/".concat("tracking"), arVar, aVar, 2, null);
            alVar.a("track_info", (Object) jSONArray);
            alVar.l = true;
            alVar.b(str);
            ajVar.a(alVar);
        }
    }
}
