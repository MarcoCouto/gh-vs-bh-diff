package com.chartboost.sdk;

import android.app.Activity;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.c.C0016c;
import com.chartboost.sdk.impl.ay;
import com.chartboost.sdk.impl.be;
import com.chartboost.sdk.impl.l;
import com.chartboost.sdk.impl.s;
import java.util.concurrent.atomic.AtomicReference;

public class d {
    final ay a;
    be b = null;
    private final l c;
    private final AtomicReference<e> d;
    private final Handler e;
    private int f = -1;

    public d(ay ayVar, l lVar, AtomicReference<e> atomicReference, Handler handler) {
        this.a = ayVar;
        this.c = lVar;
        this.d = atomicReference;
        this.e = handler;
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        if (cVar.l != 0) {
            e(cVar);
        }
    }

    private void e(c cVar) {
        if (this.b == null || this.b.e() == cVar) {
            int i = 1;
            boolean z = cVar.l != 2;
            cVar.l = 2;
            Activity b2 = cVar.g.b();
            CBImpressionError cBImpressionError = b2 == null ? CBImpressionError.NO_HOST_ACTIVITY : null;
            if (cBImpressionError == null) {
                cBImpressionError = cVar.j();
            }
            if (cBImpressionError != null) {
                CBLogging.b("CBViewController", "Unable to create the view while trying th display the impression");
                cVar.a(cBImpressionError);
                return;
            }
            if (this.b == null) {
                this.b = (be) g.a().a(new be(b2, cVar));
                b2.addContentView(this.b, new LayoutParams(-1, -1));
            }
            CBUtility.a(b2, cVar.p.m, (e) this.d.get());
            if (s.a().a(11) && this.f == -1 && (cVar.n == 1 || cVar.n == 2)) {
                this.f = b2.getWindow().getDecorView().getSystemUiVisibility();
                Chartboost.setActivityAttrs(b2);
            }
            this.b.a();
            CBLogging.e("CBViewController", "Displaying the impression");
            cVar.s = this.b;
            if (z) {
                if (cVar.p.m == 0) {
                    this.b.c().a(this.a, cVar.p);
                }
                if (cVar.p.m == 1) {
                    i = 6;
                }
                Integer a2 = ay.a(cVar.p.z);
                if (a2 != null) {
                    i = a2.intValue();
                }
                cVar.m();
                c cVar2 = cVar.g;
                cVar2.getClass();
                C0016c cVar3 = new C0016c(12);
                cVar3.d = cVar;
                this.a.a(i, cVar, (Runnable) cVar3, this);
                this.c.a();
            }
            return;
        }
        CBLogging.b("CBViewController", "Impression already visible");
        cVar.a(CBImpressionError.IMPRESSION_ALREADY_VISIBLE);
    }

    public void b(final c cVar) {
        CBLogging.e("CBViewController", "Dismissing impression");
        final Activity b2 = cVar.g.b();
        AnonymousClass1 r1 = new Runnable() {
            public void run() {
                cVar.l = 4;
                int i = 1;
                if (cVar.p.m == 1) {
                    i = 6;
                }
                Integer a2 = ay.a(cVar.p.z);
                if (a2 != null) {
                    i = a2.intValue();
                }
                c cVar = cVar.g;
                cVar.getClass();
                C0016c cVar2 = new C0016c(13);
                cVar2.d = cVar;
                cVar2.b = b2;
                d.this.a.a(i, cVar, (Runnable) cVar2);
            }
        };
        if (cVar.t) {
            cVar.a((Runnable) r1);
        } else {
            r1.run();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar, Activity activity) {
        c cVar2 = cVar.g;
        cVar2.getClass();
        C0016c cVar3 = new C0016c(14);
        cVar3.d = cVar;
        this.e.post(cVar3);
        cVar.l();
        CBUtility.b(activity, cVar.p.m, (e) this.d.get());
        if (this.f == -1) {
            return;
        }
        if (cVar.n == 1 || cVar.n == 2) {
            activity.getWindow().getDecorView().setSystemUiVisibility(this.f);
            this.f = -1;
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(c cVar) {
        CBLogging.e("CBViewController", "Removing impression silently");
        cVar.i();
        try {
            ((ViewGroup) this.b.getParent()).removeView(this.b);
        } catch (Exception e2) {
            CBLogging.a("CBViewController", "Exception removing impression silently", e2);
            a.a(getClass(), "removeImpressionSilently", e2);
        }
        this.b = null;
    }

    public void d(c cVar) {
        CBLogging.e("CBViewController", "Removing impression");
        cVar.l = 5;
        cVar.h();
        this.b = null;
        this.c.b();
        Handler handler = this.e;
        com.chartboost.sdk.impl.c cVar2 = cVar.a;
        cVar2.getClass();
        handler.post(new com.chartboost.sdk.impl.c.a(3, cVar.m, null));
        if (cVar.v()) {
            Handler handler2 = this.e;
            com.chartboost.sdk.impl.c cVar3 = cVar.a;
            cVar3.getClass();
            handler2.post(new com.chartboost.sdk.impl.c.a(2, cVar.m, null));
        }
        a(cVar.g);
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        CBLogging.e("CBViewController", "Attempting to close impression activity");
        Activity b2 = cVar.b();
        if (b2 != null && (b2 instanceof CBImpressionActivity)) {
            CBLogging.e("CBViewController", "Closing impression activity");
            cVar.f();
            b2.finish();
        }
    }

    public be a() {
        return this.b;
    }
}
