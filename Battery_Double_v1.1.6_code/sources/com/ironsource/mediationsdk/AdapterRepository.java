package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.IronSource.AD_UNIT;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class AdapterRepository {
    private static final String IRONSOURCE_ADAPTER_PREFIX = "IronSource";
    private static final Object lock = new Object();
    private static AdapterRepository mInstance = new AdapterRepository();
    private ConcurrentHashMap<String, AbstractAdapter> adapters = new ConcurrentHashMap<>();
    private Boolean mAdapterDebug;
    private Integer mAge;
    private String mAppKey;
    private Boolean mConsent;
    private AtomicBoolean mDidIronSourceEarlyInit = new AtomicBoolean(false);
    private String mGender;
    private ConcurrentHashMap<String, String> mMetaData = new ConcurrentHashMap<>();
    private String mUserId;

    public static AdapterRepository getInstance() {
        return mInstance;
    }

    private AdapterRepository() {
    }

    public void setInitParams(String str, String str2) {
        this.mAppKey = str;
        this.mUserId = str2;
    }

    public void setConsent(boolean z) {
        synchronized (lock) {
            this.mConsent = Boolean.valueOf(z);
            for (AbstractAdapter consent : this.adapters.values()) {
                setConsent(consent);
            }
        }
    }

    private void setConsent(AbstractAdapter abstractAdapter) {
        try {
            if (this.mConsent != null) {
                abstractAdapter.setConsent(this.mConsent.booleanValue());
            }
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("error while setting consent of ");
            sb.append(abstractAdapter.getProviderName());
            sb.append(": ");
            sb.append(th.getLocalizedMessage());
            logInternal(sb.toString());
            th.printStackTrace();
        }
    }

    public AbstractAdapter getAdapter(ProviderSettings providerSettings, JSONObject jSONObject, Activity activity) {
        return getAdapter(providerSettings, jSONObject, activity, false);
    }

    public AbstractAdapter getAdapter(ProviderSettings providerSettings, JSONObject jSONObject, Activity activity, boolean z) {
        String str;
        String adapterKey = getAdapterKey(providerSettings);
        if (z) {
            str = "IronSource";
        } else {
            str = providerSettings.getProviderTypeForReflection();
        }
        return getAdapterInternal(adapterKey, str, jSONObject, activity);
    }

    private AbstractAdapter getAdapterInternal(String str, String str2, JSONObject jSONObject, Activity activity) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" (");
        sb.append(str2);
        sb.append(") - Getting adapter");
        logInternal(sb.toString());
        synchronized (lock) {
            if (this.adapters.containsKey(str)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(" was already allocated");
                logInternal(sb2.toString());
                AbstractAdapter abstractAdapter = (AbstractAdapter) this.adapters.get(str);
                return abstractAdapter;
            }
            AbstractAdapter adapterByReflection = getAdapterByReflection(str, str2);
            if (adapterByReflection == null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(" adapter was not loaded");
                logErrorInternal(sb3.toString());
                return null;
            }
            StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append(" was allocated (adapter version: ");
            sb4.append(adapterByReflection.getVersion());
            sb4.append(", sdk version: ");
            sb4.append(adapterByReflection.getCoreSDKVersion());
            sb4.append(")");
            logInternal(sb4.toString());
            adapterByReflection.setLogListener(IronSourceLoggerManager.getLogger());
            setMetaData(adapterByReflection);
            setConsent(adapterByReflection);
            setAge(adapterByReflection);
            setGender(adapterByReflection);
            setAdaptersDebug(adapterByReflection);
            earlyInitAdapter(jSONObject, adapterByReflection, str2, activity);
            this.adapters.put(str, adapterByReflection);
            return adapterByReflection;
        }
    }

    private void earlyInitAdapter(JSONObject jSONObject, AbstractAdapter abstractAdapter, String str, Activity activity) {
        if ((str.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME) || str.equalsIgnoreCase("IronSource")) && this.mDidIronSourceEarlyInit.compareAndSet(false, true)) {
            StringBuilder sb = new StringBuilder();
            sb.append("SDK5 earlyInit  <");
            sb.append(str);
            sb.append(">");
            logInternal(sb.toString());
            abstractAdapter.earlyInit(activity, this.mAppKey, this.mUserId, jSONObject);
        }
    }

    private AbstractAdapter getAdapterByReflection(String str, String str2) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("com.ironsource.adapters.");
            sb.append(str2.toLowerCase());
            sb.append(".");
            sb.append(str2);
            sb.append("Adapter");
            Class cls = Class.forName(sb.toString());
            return (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, new Class[]{String.class}).invoke(cls, new Object[]{str});
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Error while loading adapter - exception = ");
            sb2.append(e);
            logErrorInternal(sb2.toString());
            return null;
        }
    }

    private void logErrorInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("AdapterRepository: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 3);
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("AdapterRepository: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    public void setAge(int i) {
        synchronized (lock) {
            this.mAge = Integer.valueOf(i);
            for (AbstractAdapter age : this.adapters.values()) {
                setAge(age);
            }
        }
    }

    private void setAge(AbstractAdapter abstractAdapter) {
        if (this.mAge != null) {
            try {
                abstractAdapter.setAge(this.mAge.intValue());
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("error while setting age of ");
                sb.append(abstractAdapter.getProviderName());
                sb.append(": ");
                sb.append(th.getLocalizedMessage());
                logInternal(sb.toString());
                th.printStackTrace();
            }
        }
    }

    public void setGender(String str) {
        synchronized (lock) {
            this.mGender = str;
            for (AbstractAdapter gender : this.adapters.values()) {
                setGender(gender);
            }
        }
    }

    private void setGender(AbstractAdapter abstractAdapter) {
        if (this.mGender != null) {
            try {
                abstractAdapter.setGender(this.mGender);
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("error while setting gender of ");
                sb.append(abstractAdapter.getProviderName());
                sb.append(": ");
                sb.append(th.getLocalizedMessage());
                logInternal(sb.toString());
                th.printStackTrace();
            }
        }
    }

    public void setAdaptersDebug(boolean z) {
        synchronized (lock) {
            this.mAdapterDebug = Boolean.valueOf(z);
            for (AbstractAdapter adaptersDebug : this.adapters.values()) {
                setAdaptersDebug(adaptersDebug);
            }
        }
    }

    private void setAdaptersDebug(AbstractAdapter abstractAdapter) {
        if (this.mAdapterDebug != null) {
            try {
                abstractAdapter.setAdapterDebug(this.mAdapterDebug);
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("error while setting adapterDebug of ");
                sb.append(abstractAdapter.getProviderName());
                sb.append(": ");
                sb.append(th.getLocalizedMessage());
                logInternal(sb.toString());
                th.printStackTrace();
            }
        }
    }

    public void setMetaData(String str, String str2) {
        synchronized (lock) {
            this.mMetaData.put(str, str2);
            for (AbstractAdapter abstractAdapter : this.adapters.values()) {
                try {
                    abstractAdapter.setMetaData(str, str2);
                } catch (Throwable th) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("error while setting metadata of ");
                    sb.append(abstractAdapter.getProviderName());
                    sb.append(": ");
                    sb.append(th.getLocalizedMessage());
                    logInternal(sb.toString());
                    th.printStackTrace();
                }
            }
        }
    }

    private void setMetaData(AbstractAdapter abstractAdapter) {
        for (String str : this.mMetaData.keySet()) {
            try {
                abstractAdapter.setMetaData(str, (String) this.mMetaData.get(str));
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("error while setting metadata of ");
                sb.append(abstractAdapter.getProviderName());
                sb.append(": ");
                sb.append(th.getLocalizedMessage());
                logInternal(sb.toString());
                th.printStackTrace();
            }
        }
    }

    public String getBiddingData(AD_UNIT ad_unit, ProviderSettings providerSettings) {
        Map map;
        String adapterKey = getAdapterKey(providerSettings);
        if (adapterKey == null) {
            return null;
        }
        AbstractAdapter abstractAdapter = (AbstractAdapter) this.adapters.get(adapterKey);
        if (abstractAdapter == null) {
            return null;
        }
        switch (ad_unit) {
            case REWARDED_VIDEO:
                map = abstractAdapter.getRvBiddingData(null);
                break;
            case INTERSTITIAL:
                map = abstractAdapter.getIsBiddingData(null);
                break;
            default:
                map = null;
                break;
        }
        if (map == null) {
            return null;
        }
        Object obj = map.get(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY);
        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
    }

    public ConcurrentHashMap<String, String> getMetaData() {
        return this.mMetaData;
    }

    private String getAdapterKey(ProviderSettings providerSettings) {
        return providerSettings.isMultipleInstances() ? providerSettings.getProviderTypeForReflection() : providerSettings.getProviderName();
    }

    public void onPause(Activity activity) {
        for (AbstractAdapter onPause : getUniqueAdapters()) {
            onPause.onPause(activity);
        }
    }

    public void onResume(Activity activity) {
        for (AbstractAdapter onResume : getUniqueAdapters()) {
            onResume.onResume(activity);
        }
    }

    private Set<AbstractAdapter> getUniqueAdapters() {
        TreeSet treeSet = new TreeSet(new Comparator<AbstractAdapter>() {
            public int compare(AbstractAdapter abstractAdapter, AbstractAdapter abstractAdapter2) {
                return abstractAdapter.getClass().getSimpleName().compareTo(abstractAdapter2.getClass().getSimpleName());
            }
        });
        treeSet.addAll(this.adapters.values());
        return treeSet;
    }
}
