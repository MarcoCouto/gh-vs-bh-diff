package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AuctionDataUtils.AuctionData;
import com.ironsource.mediationsdk.IronSource.AD_UNIT;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyIsManagerListener;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.constants.Constants.JSMethods;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;

class DemandOnlyIsManager implements DemandOnlyIsManagerListener {
    private AuctionSettings mAuctionSettings;
    private Context mContext;
    private ConcurrentHashMap<String, DemandOnlyIsSmash> mSmashes = new ConcurrentHashMap<>();

    DemandOnlyIsManager(Activity activity, List<ProviderSettings> list, InterstitialConfigurations interstitialConfigurations, String str, String str2) {
        this.mContext = activity.getApplicationContext();
        this.mAuctionSettings = interstitialConfigurations.getInterstitialAuctionSettings();
        for (ProviderSettings providerSettings : list) {
            if (providerSettings.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME) || providerSettings.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.IRONSOURCE_CONFIG_NAME)) {
                AbstractAdapter adapter = AdapterRepository.getInstance().getAdapter(providerSettings, providerSettings.getRewardedVideoSettings(), activity, true);
                if (adapter != null) {
                    DemandOnlyIsSmash demandOnlyIsSmash = new DemandOnlyIsSmash(activity, str, str2, providerSettings, this, interstitialConfigurations.getInterstitialAdaptersSmartLoadTimeout(), adapter);
                    this.mSmashes.put(providerSettings.getSubProviderId(), demandOnlyIsSmash);
                }
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("cannot load ");
                sb.append(providerSettings.getProviderTypeForReflection());
                logInternal(sb.toString());
            }
        }
    }

    public void loadInterstitialWithAdm(String str, String str2, boolean z) {
        try {
            if (!this.mSmashes.containsKey(str)) {
                sendMediationEvent(2500, str);
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, ErrorBuilder.buildNonExistentInstanceError("Interstitial"));
                return;
            }
            DemandOnlyIsSmash demandOnlyIsSmash = (DemandOnlyIsSmash) this.mSmashes.get(str);
            if (z) {
                if (!demandOnlyIsSmash.isBidder()) {
                    IronSourceError buildLoadFailedError = ErrorBuilder.buildLoadFailedError("loadInterstitialWithAdm in IAB flow must be called by bidder instances");
                    logInternal(buildLoadFailedError.getErrorMessage());
                    ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, buildLoadFailedError);
                    sendProviderEvent(IronSourceConstants.IS_INSTANCE_LOAD_FAILED, demandOnlyIsSmash);
                } else {
                    AuctionData auctionDataFromResponse = AuctionDataUtils.getInstance().getAuctionDataFromResponse(AuctionDataUtils.getInstance().decodeAdmResponse(str2));
                    AuctionResponseItem auctionResponseItem = AuctionDataUtils.getInstance().getAuctionResponseItem(demandOnlyIsSmash.getInstanceName(), auctionDataFromResponse.getWaterfall());
                    if (auctionResponseItem != null) {
                        demandOnlyIsSmash.setDynamicDemandSourceIdByServerData(auctionResponseItem.getServerData());
                        demandOnlyIsSmash.loadInterstitial(auctionResponseItem.getServerData(), auctionDataFromResponse.getAuctionId(), auctionResponseItem.getBurls());
                        sendProviderEvent(2002, demandOnlyIsSmash);
                    } else {
                        IronSourceError buildLoadFailedError2 = ErrorBuilder.buildLoadFailedError("loadInterstitialWithAdm invalid enriched adm");
                        logInternal(buildLoadFailedError2.getErrorMessage());
                        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, buildLoadFailedError2);
                        sendProviderEvent(IronSourceConstants.IS_INSTANCE_LOAD_FAILED, demandOnlyIsSmash);
                    }
                }
            } else if (!demandOnlyIsSmash.isBidder()) {
                sendProviderEvent(2002, demandOnlyIsSmash);
                demandOnlyIsSmash.loadInterstitial("", "", null);
            } else {
                IronSourceError buildLoadFailedError3 = ErrorBuilder.buildLoadFailedError("loadInterstitialWithAdm in non IAB flow must be called by non bidder instances");
                logInternal(buildLoadFailedError3.getErrorMessage());
                ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, buildLoadFailedError3);
                sendProviderEvent(IronSourceConstants.IS_INSTANCE_LOAD_FAILED, demandOnlyIsSmash);
            }
        } catch (Exception unused) {
            IronSourceError buildLoadFailedError4 = ErrorBuilder.buildLoadFailedError("loadInterstitialWithAdm exception");
            logInternal(buildLoadFailedError4.getErrorMessage());
            ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(str, buildLoadFailedError4);
        }
    }

    public String getBiddingData() {
        sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_IS_GET_BIDDING_DATA_CALLED, null);
        if (this.mSmashes != null) {
            String biddingData = AdapterRepository.getInstance().getBiddingData(AD_UNIT.INTERSTITIAL, ((DemandOnlyIsSmash) ((Entry) this.mSmashes.entrySet().iterator().next()).getValue()).getAdapterConfig().getProviderSettings());
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(2);
            String sessionId = IronSourceObject.getInstance().getSessionId();
            if (biddingData != null) {
                HashMap hashMap = new HashMap();
                hashMap.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, biddingData);
                HashMap hashMap2 = new HashMap();
                hashMap2.put(IronSourceConstants.IRONSOURCE_CONFIG_NAME, hashMap);
                try {
                    new JSONObject();
                    return AuctionDataUtils.getInstance().encryptToken(AuctionDataUtils.getInstance().enrichToken(this.mContext, hashMap2, null, null, sessionDepth, sessionId, this.mAuctionSettings));
                } catch (JSONException unused) {
                    logInternal("getBiddingData() got error during enrich token");
                    sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_IS_GET_BIDDING_DATA_ENRICH_TOKEN_ERROR, null);
                    return null;
                }
            }
        }
        logInternal("called getBiddingData() with no smashes");
        sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_IS_GET_BIDDING_DATA_RETURNED_NULL, null);
        return null;
    }

    public void showInterstitial(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(2500, str);
            ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdShowFailed(str, ErrorBuilder.buildNonExistentInstanceError("Interstitial"));
            return;
        }
        DemandOnlyIsSmash demandOnlyIsSmash = (DemandOnlyIsSmash) this.mSmashes.get(str);
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_SHOW, demandOnlyIsSmash);
        demandOnlyIsSmash.showInterstitial();
    }

    public boolean isInterstitialReady(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(2500, str);
            return false;
        }
        DemandOnlyIsSmash demandOnlyIsSmash = (DemandOnlyIsSmash) this.mSmashes.get(str);
        if (demandOnlyIsSmash.isInterstitialReady()) {
            sendProviderEvent(IronSourceConstants.IS_INSTANCE_READY_TRUE, demandOnlyIsSmash);
            return true;
        }
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_READY_FALSE, demandOnlyIsSmash);
        return false;
    }

    public void onInterstitialAdReady(DemandOnlyIsSmash demandOnlyIsSmash, long j) {
        logSmashCallback(demandOnlyIsSmash, "onInterstitialAdReady");
        sendProviderEvent(2003, demandOnlyIsSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdReady(demandOnlyIsSmash.getSubProviderId());
    }

    public void onInterstitialAdLoadFailed(IronSourceError ironSourceError, DemandOnlyIsSmash demandOnlyIsSmash, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdLoadFailed error=");
        sb.append(ironSourceError.toString());
        logSmashCallback(demandOnlyIsSmash, sb.toString());
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_LOAD_FAILED, demandOnlyIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{"duration", Long.valueOf(j)}});
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdLoadFailed(demandOnlyIsSmash.getSubProviderId(), ironSourceError);
    }

    public void onInterstitialAdOpened(DemandOnlyIsSmash demandOnlyIsSmash) {
        logSmashCallback(demandOnlyIsSmash, "onInterstitialAdOpened");
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_OPENED, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdOpened(demandOnlyIsSmash.getSubProviderId());
        if (demandOnlyIsSmash.isBidder()) {
            for (String str : demandOnlyIsSmash.mBUrl) {
                if (str != null) {
                    AuctionDataUtils.getInstance().sendResponse(str);
                }
            }
        }
    }

    public void onInterstitialAdClosed(DemandOnlyIsSmash demandOnlyIsSmash) {
        logSmashCallback(demandOnlyIsSmash, "onInterstitialAdClosed");
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_CLOSED, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdClosed(demandOnlyIsSmash.getSubProviderId());
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError, DemandOnlyIsSmash demandOnlyIsSmash) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdShowFailed error=");
        sb.append(ironSourceError.toString());
        logSmashCallback(demandOnlyIsSmash, sb.toString());
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_SHOW_FAILED, demandOnlyIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdShowFailed(demandOnlyIsSmash.getSubProviderId(), ironSourceError);
    }

    public void onInterstitialAdClicked(DemandOnlyIsSmash demandOnlyIsSmash) {
        logSmashCallback(demandOnlyIsSmash, JSMethods.ON_INTERSTITIAL_AD_CLICKED);
        sendProviderEvent(2006, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.getInstance().onInterstitialAdClicked(demandOnlyIsSmash.getSubProviderId());
    }

    public void onInterstitialAdVisible(DemandOnlyIsSmash demandOnlyIsSmash) {
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_VISIBLE, demandOnlyIsSmash);
        logSmashCallback(demandOnlyIsSmash, "onInterstitialAdVisible");
    }

    private void sendProviderEvent(int i, DemandOnlyIsSmash demandOnlyIsSmash) {
        sendProviderEvent(i, demandOnlyIsSmash, null);
    }

    private void sendProviderEvent(int i, DemandOnlyIsSmash demandOnlyIsSmash, Object[][] objArr) {
        Map providerEventData = demandOnlyIsSmash.getProviderEventData();
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("IS sendProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }

    private void sendMediationEvent(int i, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_DEMAND_ONLY, Integer.valueOf(1));
        String str2 = "spId";
        if (str == null) {
            str = "";
        }
        hashMap.put(str2, str);
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyIsManager ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void logSmashCallback(DemandOnlyIsSmash demandOnlyIsSmash, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyIsManager ");
        sb.append(demandOnlyIsSmash.getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }
}
