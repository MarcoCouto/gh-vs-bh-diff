package com.ironsource.mediationsdk.sdk;

import android.app.Activity;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import org.json.JSONObject;

public interface BannerAdapterApi {
    void destroyBanner(JSONObject jSONObject);

    void initBanners(Activity activity, String str, String str2, JSONObject jSONObject, BannerSmashListener bannerSmashListener);

    void loadBanner(IronSourceBannerLayout ironSourceBannerLayout, JSONObject jSONObject, BannerSmashListener bannerSmashListener);

    void reloadBanner(JSONObject jSONObject);

    boolean shouldBindBannerViewOnReload();
}
