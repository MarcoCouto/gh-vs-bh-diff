package com.ironsource.mediationsdk;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.text.TextUtils;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.ironsource.mediationsdk.AuctionDataUtils.AuctionData;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class AuctionHandler {
    private static final int SERVER_REQUEST_TIMEOUT = 15000;
    private final String AUCTION_INTERNAL_ERROR_LOSS_CODE = "1";
    private final String AUCTION_LOSS_MACRO = "${AUCTION_LOSS}";
    private final String AUCTION_LOST_TO_NON_BIDDER_LOSS_CODE = "103";
    private final String AUCTION_NOT_HIGHEST_RTB_BIDDER_LOSS_CODE = "102";
    private final String AUCTION_PRICE_MACRO = "${AUCTION_PRICE}";
    private final String PLACEMENT_NAME_MACRO = "${PLACEMENT_NAME}";
    private String mAdUnit;
    private AuctionEventListener mAuctionListener;
    private String mSessionId;
    private AuctionSettings mSettings;

    static class AuctionHttpRequestTask extends AsyncTask<Object, Void, Boolean> {
        private String mAuctionFallback = FacebookRequestErrorClassification.KEY_OTHER;
        private String mAuctionId;
        private WeakReference<AuctionEventListener> mAuctionListener;
        private int mCurrentAuctionTrial;
        private int mErrorCode;
        private String mErrorMessage;
        private JSONObject mRequestData;
        private long mRequestStartTime;
        private List<AuctionResponseItem> mWaterfall;

        AuctionHttpRequestTask(AuctionEventListener auctionEventListener) {
            this.mAuctionListener = new WeakReference<>(auctionEventListener);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00c3  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00dc  */
        public Boolean doInBackground(Object... objArr) {
            HttpURLConnection httpURLConnection;
            Exception e;
            this.mRequestStartTime = new Date().getTime();
            try {
                URL url = new URL(objArr[0]);
                this.mRequestData = objArr[1];
                boolean booleanValue = objArr[2].booleanValue();
                int intValue = objArr[3].intValue();
                long longValue = objArr[4].longValue();
                this.mCurrentAuctionTrial = 0;
                HttpURLConnection httpURLConnection2 = null;
                while (this.mCurrentAuctionTrial < intValue) {
                    try {
                        long time = new Date().getTime();
                        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Auction Handler: auction trial ");
                        sb.append(this.mCurrentAuctionTrial + 1);
                        sb.append(" out of ");
                        sb.append(intValue);
                        sb.append(" max trials");
                        logger.log(ironSourceTag, sb.toString(), 0);
                        httpURLConnection = prepareAuctionRequest(url, longValue);
                        try {
                            sendAuctionRequest(httpURLConnection, this.mRequestData);
                            int responseCode = httpURLConnection.getResponseCode();
                            if (responseCode != 200) {
                                this.mErrorCode = 1001;
                                this.mErrorMessage = String.valueOf(responseCode);
                                httpURLConnection.disconnect();
                                if (this.mCurrentAuctionTrial < intValue - 1) {
                                    waitUntilNextTrial(longValue, time);
                                }
                                httpURLConnection2 = httpURLConnection;
                                this.mCurrentAuctionTrial++;
                            } else {
                                try {
                                    handleResponse(readResponse(httpURLConnection), booleanValue);
                                    httpURLConnection.disconnect();
                                    return Boolean.valueOf(true);
                                } catch (JSONException unused) {
                                    this.mErrorCode = 1002;
                                    this.mErrorMessage = "failed parsing auction response";
                                    this.mAuctionFallback = "parsing";
                                    httpURLConnection.disconnect();
                                    return Boolean.valueOf(false);
                                }
                            }
                        } catch (SocketTimeoutException unused2) {
                            if (httpURLConnection != null) {
                            }
                            this.mErrorCode = 1006;
                            this.mErrorMessage = "Connection timed out";
                            httpURLConnection2 = httpURLConnection;
                            this.mCurrentAuctionTrial++;
                        } catch (Exception e2) {
                            e = e2;
                            if (httpURLConnection != null) {
                            }
                            this.mErrorCode = 1000;
                            this.mErrorMessage = e.getMessage();
                            this.mAuctionFallback = FacebookRequestErrorClassification.KEY_OTHER;
                            return Boolean.valueOf(false);
                        }
                    } catch (SocketTimeoutException unused3) {
                        httpURLConnection = httpURLConnection2;
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.mErrorCode = 1006;
                        this.mErrorMessage = "Connection timed out";
                        httpURLConnection2 = httpURLConnection;
                        this.mCurrentAuctionTrial++;
                    } catch (Exception e3) {
                        httpURLConnection = httpURLConnection2;
                        e = e3;
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.mErrorCode = 1000;
                        this.mErrorMessage = e.getMessage();
                        this.mAuctionFallback = FacebookRequestErrorClassification.KEY_OTHER;
                        return Boolean.valueOf(false);
                    }
                }
                this.mCurrentAuctionTrial = intValue - 1;
                this.mAuctionFallback = "trials_fail";
                return Boolean.valueOf(false);
            } catch (Exception e4) {
                this.mErrorCode = 1007;
                this.mErrorMessage = e4.getMessage();
                this.mCurrentAuctionTrial = 0;
                this.mAuctionFallback = FacebookRequestErrorClassification.KEY_OTHER;
                return Boolean.valueOf(false);
            }
        }

        private void waitUntilNextTrial(long j, long j2) {
            long time = j - (new Date().getTime() - j2);
            if (time > 0) {
                SystemClock.sleep(time);
            }
        }

        private void sendAuctionRequest(HttpURLConnection httpURLConnection, JSONObject jSONObject) throws IOException {
            OutputStream outputStream = httpURLConnection.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(String.format("{\"request\" : \"%1$s\"}", new Object[]{IronSourceAES.encode(IronSourceUtils.KEY, jSONObject.toString())}));
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStreamWriter.close();
            outputStream.close();
        }

        private HttpURLConnection prepareAuctionRequest(URL url, long j) throws IOException {
            int i = (int) j;
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            httpURLConnection.setReadTimeout(i);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            return httpURLConnection;
        }

        private void handleResponse(String str, boolean z) throws JSONException {
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (z) {
                    jSONObject = new JSONObject(IronSourceAES.decode(IronSourceUtils.KEY, jSONObject.getString(ServerResponseWrapper.RESPONSE_FIELD)));
                }
                AuctionData auctionDataFromResponse = AuctionDataUtils.getInstance().getAuctionDataFromResponse(jSONObject);
                this.mAuctionId = auctionDataFromResponse.getAuctionId();
                this.mWaterfall = auctionDataFromResponse.getWaterfall();
                this.mErrorCode = auctionDataFromResponse.getErrorCode();
                this.mErrorMessage = auctionDataFromResponse.getErrorMessage();
                return;
            }
            throw new JSONException("empty response");
        }

        private String readResponse(HttpURLConnection httpURLConnection) throws IOException {
            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    bufferedReader.close();
                    inputStreamReader.close();
                    return sb.toString();
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            AuctionEventListener auctionEventListener = (AuctionEventListener) this.mAuctionListener.get();
            if (auctionEventListener != null) {
                long time = new Date().getTime() - this.mRequestStartTime;
                if (bool.booleanValue()) {
                    auctionEventListener.onAuctionSuccess(this.mWaterfall, this.mAuctionId, this.mCurrentAuctionTrial + 1, time);
                } else {
                    auctionEventListener.onAuctionFailed(this.mErrorCode, this.mErrorMessage, this.mCurrentAuctionTrial + 1, this.mAuctionFallback, time);
                }
            }
        }
    }

    public AuctionHandler(String str, AuctionSettings auctionSettings, AuctionEventListener auctionEventListener) {
        this.mAdUnit = str;
        this.mSettings = auctionSettings;
        this.mAuctionListener = auctionEventListener;
        this.mSessionId = IronSourceObject.getInstance().getSessionId();
    }

    public void executeAuction(Context context, Map<String, Object> map, List<String> list, AuctionHistory auctionHistory, int i) {
        try {
            boolean z = IronSourceUtils.getSerr() == 1;
            new AuctionHttpRequestTask(this.mAuctionListener).execute(new Object[]{this.mSettings.getUrl(), generateRequest(context, map, list, auctionHistory, i, z), Boolean.valueOf(z), Integer.valueOf(this.mSettings.getNumOfMaxTrials()), Long.valueOf(this.mSettings.getTrialsInterval())});
        } catch (Exception e) {
            this.mAuctionListener.onAuctionFailed(1000, e.getMessage(), 0, FacebookRequestErrorClassification.KEY_OTHER, 0);
        }
    }

    public void reportImpression(AuctionResponseItem auctionResponseItem, String str) {
        for (String replace : auctionResponseItem.getBurls()) {
            AuctionDataUtils.getInstance().sendResponse(replace.replace("${PLACEMENT_NAME}", str));
        }
    }

    public void reportLoadSuccess(AuctionResponseItem auctionResponseItem) {
        for (String sendResponse : auctionResponseItem.getNurls()) {
            AuctionDataUtils.getInstance().sendResponse(sendResponse);
        }
    }

    public void reportAuctionLose(CopyOnWriteArrayList<ProgSmash> copyOnWriteArrayList, ConcurrentHashMap<String, AuctionResponseItem> concurrentHashMap, AuctionResponseItem auctionResponseItem) {
        String price = auctionResponseItem.getPrice();
        Iterator it = copyOnWriteArrayList.iterator();
        boolean z = false;
        boolean z2 = false;
        while (it.hasNext()) {
            ProgSmash progSmash = (ProgSmash) it.next();
            String instanceName = progSmash.getInstanceName();
            if (instanceName.equals(auctionResponseItem.getInstanceName())) {
                z = true;
                z2 = progSmash.isBidder();
            } else {
                AuctionResponseItem auctionResponseItem2 = (AuctionResponseItem) concurrentHashMap.get(instanceName);
                String str = z ? z2 ? "102" : "103" : "1";
                for (String replace : auctionResponseItem2.getLurls()) {
                    AuctionDataUtils.getInstance().sendResponse(replace.replace("${AUCTION_PRICE}", price).replace("${AUCTION_LOSS}", str));
                }
            }
        }
    }

    private JSONObject generateRequest(Context context, Map<String, Object> map, List<String> list, AuctionHistory auctionHistory, int i, boolean z) throws JSONException {
        new JSONObject();
        JSONObject enrichToken = AuctionDataUtils.getInstance().enrichToken(context, map, list, auctionHistory, i, this.mSessionId, this.mSettings);
        enrichToken.put("adUnit", this.mAdUnit);
        enrichToken.put("doNotEncryptResponse", z ? "false" : "true");
        return enrichToken;
    }
}
