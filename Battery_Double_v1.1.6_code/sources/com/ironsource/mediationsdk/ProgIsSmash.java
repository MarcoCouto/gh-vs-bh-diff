package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.sdk.constants.Constants.JSMethods;
import com.smaato.sdk.core.api.VideoType;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ProgIsSmash extends ProgSmash implements InterstitialSmashListener {
    private Activity mActivity;
    private String mAppKey;
    /* access modifiers changed from: private */
    public ProgIsManagerListener mListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;
    private int mLoadTimeoutSecs;
    /* access modifiers changed from: private */
    public SMASH_STATE mState = SMASH_STATE.NO_INIT;
    private Timer mTimer;
    private final Object mTimerLock = new Object();
    private String mUserId;

    protected enum SMASH_STATE {
        NO_INIT,
        INIT_IN_PROGRESS,
        INIT_SUCCESS,
        LOAD_IN_PROGRESS,
        LOADED,
        LOAD_FAILED
    }

    public ProgIsSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, ProgIsManagerListener progIsManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getInterstitialSettings()), abstractAdapter);
        this.mActivity = activity;
        this.mAppKey = str;
        this.mUserId = str2;
        this.mListener = progIsManagerListener;
        this.mTimer = null;
        this.mLoadTimeoutSecs = i;
        this.mAdapter.addInterstitialListener(this);
    }

    public Map<String, Object> getBiddingData() {
        Map<String, Object> map = null;
        try {
            if (isBidder()) {
                map = this.mAdapter.getIsBiddingData(this.mAdUnitSettings);
            }
            return map;
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("getBiddingData exception: ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
            return null;
        }
    }

    public boolean isLoadingInProgress() {
        return this.mState == SMASH_STATE.INIT_IN_PROGRESS || this.mState == SMASH_STATE.LOAD_IN_PROGRESS;
    }

    public void initForBidding() {
        logInternal("initForBidding()");
        setState(SMASH_STATE.INIT_IN_PROGRESS);
        setCustomParams();
        try {
            this.mAdapter.initInterstitialForBidding(this.mActivity, this.mAppKey, this.mUserId, this.mAdUnitSettings, this);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append(getInstanceName());
            sb.append("loadInterstitial exception : ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
            onInterstitialInitFailed(new IronSourceError(IronSourceError.ERROR_IS_INIT_EXCEPTION, th.getLocalizedMessage()));
        }
    }

    public void loadInterstitial(String str) {
        try {
            this.mLoadStartTime = new Date().getTime();
            logInternal(JSMethods.LOAD_INTERSTITIAL);
            setIsLoadCandidate(false);
            if (isBidder()) {
                startTimer();
                setState(SMASH_STATE.LOAD_IN_PROGRESS);
                this.mAdapter.loadInterstitial(this.mAdUnitSettings, this, str);
            } else if (this.mState == SMASH_STATE.NO_INIT) {
                startTimer();
                setState(SMASH_STATE.INIT_IN_PROGRESS);
                setCustomParams();
                this.mAdapter.initInterstitial(this.mActivity, this.mAppKey, this.mUserId, this.mAdUnitSettings, this);
            } else {
                startTimer();
                setState(SMASH_STATE.LOAD_IN_PROGRESS);
                this.mAdapter.loadInterstitial(this.mAdUnitSettings, this);
            }
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadInterstitial exception: ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
        }
    }

    public void showInterstitial() {
        try {
            this.mAdapter.showInterstitial(this.mAdUnitSettings, this);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append(getInstanceName());
            sb.append("showInterstitial exception : ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
            this.mListener.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_IS_SHOW_EXCEPTION, th.getLocalizedMessage()), this);
        }
    }

    public void setCappedPerSession() {
        this.mAdapter.setMediationState(MEDIATION_STATE.CAPPED_PER_SESSION, VideoType.INTERSTITIAL);
    }

    public boolean isReadyToShow() {
        try {
            return this.mAdapter.isInterstitialReady(this.mAdUnitSettings);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("isReadyToShow exception: ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void setState(SMASH_STATE smash_state) {
        StringBuilder sb = new StringBuilder();
        sb.append("current state=");
        sb.append(this.mState);
        sb.append(", new state=");
        sb.append(smash_state);
        logInternal(sb.toString());
        this.mState = smash_state;
    }

    private void setCustomParams() {
        try {
            String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
            if (!TextUtils.isEmpty(mediationSegment)) {
                this.mAdapter.setMediationSegment(mediationSegment);
            }
            String pluginType = ConfigFile.getConfigFile().getPluginType();
            if (!TextUtils.isEmpty(pluginType)) {
                this.mAdapter.setPluginData(pluginType, ConfigFile.getConfigFile().getPluginFrameworkVersion());
            }
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("setCustomParams() ");
            sb.append(e.getMessage());
            logInternal(sb.toString());
        }
    }

    public void onInterstitialInitSuccess() {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialInitSuccess state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        if (this.mState == SMASH_STATE.INIT_IN_PROGRESS) {
            stopTimer();
            if (isBidder()) {
                setState(SMASH_STATE.INIT_SUCCESS);
            } else {
                setState(SMASH_STATE.LOAD_IN_PROGRESS);
                startTimer();
                try {
                    this.mAdapter.loadInterstitial(this.mAdUnitSettings, this);
                } catch (Throwable th) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("onInterstitialInitSuccess exception: ");
                    sb2.append(th.getLocalizedMessage());
                    logInternalError(sb2.toString());
                    th.printStackTrace();
                }
            }
            this.mListener.onInterstitialInitSuccess(this);
        }
    }

    public void onInterstitialInitFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialInitFailed error");
        sb.append(ironSourceError.getErrorMessage());
        sb.append(" state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        if (this.mState == SMASH_STATE.INIT_IN_PROGRESS) {
            stopTimer();
            setState(SMASH_STATE.NO_INIT);
            this.mListener.onInterstitialInitFailed(ironSourceError, this);
            if (!isBidder()) {
                this.mListener.onInterstitialAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
            }
        }
    }

    public void onInterstitialAdReady() {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdReady state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
            setState(SMASH_STATE.LOADED);
            this.mListener.onInterstitialAdReady(this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onInterstitialAdLoadFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdLoadFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        sb.append(" state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
            setState(SMASH_STATE.LOAD_FAILED);
            this.mListener.onInterstitialAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onInterstitialAdOpened() {
        logAdapterCallback("onInterstitialAdOpened");
        this.mListener.onInterstitialAdOpened(this);
    }

    public void onInterstitialAdClosed() {
        logAdapterCallback("onInterstitialAdClosed");
        this.mListener.onInterstitialAdClosed(this);
    }

    public void onInterstitialAdShowSucceeded() {
        logAdapterCallback("onInterstitialAdShowSucceeded");
        this.mListener.onInterstitialAdShowSucceeded(this);
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdShowFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        logAdapterCallback(sb.toString());
        this.mListener.onInterstitialAdShowFailed(ironSourceError, this);
    }

    public void onInterstitialAdClicked() {
        logAdapterCallback(JSMethods.ON_INTERSTITIAL_AD_CLICKED);
        this.mListener.onInterstitialAdClicked(this);
    }

    public void onInterstitialAdVisible() {
        logAdapterCallback("onInterstitialAdVisible");
        this.mListener.onInterstitialAdVisible(this);
    }

    private void stopTimer() {
        synchronized (this.mTimerLock) {
            if (this.mTimer != null) {
                this.mTimer.cancel();
                this.mTimer = null;
            }
        }
    }

    private void startTimer() {
        synchronized (this.mTimerLock) {
            logInternal("start timer");
            stopTimer();
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    ProgIsSmash progIsSmash = ProgIsSmash.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("timed out state=");
                    sb.append(ProgIsSmash.this.mState.name());
                    sb.append(" isBidder=");
                    sb.append(ProgIsSmash.this.isBidder());
                    progIsSmash.logInternal(sb.toString());
                    if (ProgIsSmash.this.mState != SMASH_STATE.INIT_IN_PROGRESS || !ProgIsSmash.this.isBidder()) {
                        ProgIsSmash.this.setState(SMASH_STATE.LOAD_FAILED);
                        ProgIsSmash.this.mListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("timed out"), ProgIsSmash.this, new Date().getTime() - ProgIsSmash.this.mLoadStartTime);
                        return;
                    }
                    ProgIsSmash.this.setState(SMASH_STATE.NO_INIT);
                }
            }, (long) (this.mLoadTimeoutSecs * 1000));
        }
    }

    private void logAdapterCallback(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("ProgIsSmash ");
        sb.append(getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.ADAPTER_CALLBACK, sb.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("ProgIsSmash ");
        sb.append(getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }

    private void logInternalError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("ProgIsSmash ");
        sb.append(getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 3);
    }
}
