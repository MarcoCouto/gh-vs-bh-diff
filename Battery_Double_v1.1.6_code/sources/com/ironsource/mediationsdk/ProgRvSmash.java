package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class ProgRvSmash extends ProgSmash implements RewardedVideoSmashListener {
    private static final int errorCode_adClosed = 5009;
    private static final int errorCode_biddingDataException = 5001;
    private static final int errorCode_initFailed = 5008;
    private static final int errorCode_initSuccess = 5007;
    private static final int errorCode_isReadyException = 5002;
    private static final int errorCode_loadException = 5005;
    private static final int errorCode_loadInProgress = 5003;
    private static final int errorCode_showFailed = 5006;
    private static final int errorCode_showInProgress = 5004;
    private Activity mActivity;
    private String mAppKey;
    private String mAuctionFallback;
    private String mAuctionFallbackToLoad;
    private String mAuctionIdToLoad;
    private String mAuctionServerDataToLoad;
    private int mAuctionTrial;
    private int mAuctionTrialToLoad;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId;
    private Placement mCurrentPlacement;
    private boolean mIsShowCandidate;
    /* access modifiers changed from: private */
    public ProgRvManagerListener mListener;
    private long mLoadStartTime;
    private int mLoadTimeoutSecs;
    private int mSessionDepth;
    private int mSessionDepthToLoad;
    private boolean mShouldLoadAfterClose;
    private boolean mShouldLoadAfterLoad;
    /* access modifiers changed from: private */
    public SMASH_STATE mState = SMASH_STATE.NO_INIT;
    /* access modifiers changed from: private */
    public final Object mStateLock = new Object();
    private Timer mTimer;
    private final Object mTimerLock = new Object();
    private String mUserId;

    protected enum SMASH_STATE {
        NO_INIT,
        INIT_IN_PROGRESS,
        NOT_LOADED,
        LOAD_IN_PROGRESS,
        LOADED,
        SHOW_IN_PROGRESS
    }

    private boolean shouldAddAuctionParams(int i) {
        return i == 1001 || i == 1002 || i == 1200 || i == 1005 || i == 1203 || i == 1201 || i == 1202 || i == 1006 || i == 1010;
    }

    public void onRewardedVideoLoadSuccess() {
    }

    public ProgRvSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, ProgRvManagerListener progRvManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getRewardedVideoSettings()), abstractAdapter);
        this.mActivity = activity;
        this.mAppKey = str;
        this.mUserId = str2;
        this.mListener = progRvManagerListener;
        this.mTimer = null;
        this.mLoadTimeoutSecs = i;
        this.mAdapter.addRewardedVideoListener(this);
        this.mShouldLoadAfterClose = false;
        this.mShouldLoadAfterLoad = false;
        this.mIsShowCandidate = false;
        this.mCurrentPlacement = null;
        this.mCurrentAuctionId = "";
        this.mSessionDepth = 1;
        resetAuctionParams();
    }

    private void resetAuctionParams() {
        this.mAuctionIdToLoad = "";
        this.mAuctionTrialToLoad = -1;
        this.mAuctionFallbackToLoad = "";
        this.mAuctionServerDataToLoad = "";
        this.mSessionDepthToLoad = this.mSessionDepth;
    }

    public boolean isLoadingInProgress() {
        return this.mState == SMASH_STATE.INIT_IN_PROGRESS || this.mState == SMASH_STATE.LOAD_IN_PROGRESS;
    }

    public Map<String, Object> getBiddingData() {
        Map<String, Object> map = null;
        try {
            if (isBidder()) {
                map = this.mAdapter.getRvBiddingData(this.mAdUnitSettings);
            }
            return map;
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("getBiddingData exception: ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
            sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_biddingDataException)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, th.getLocalizedMessage()}});
            return null;
        }
    }

    public void initForBidding() {
        logInternal("initForBidding()");
        setState(SMASH_STATE.INIT_IN_PROGRESS);
        setCustomParams();
        try {
            this.mAdapter.initRvForBidding(this.mActivity, this.mAppKey, this.mUserId, this.mAdUnitSettings, this);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("initForBidding exception: ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
            onRewardedVideoInitFailed(new IronSourceError(IronSourceError.ERROR_RV_INIT_EXCEPTION, th.getLocalizedMessage()));
        }
    }

    public void unloadVideo() {
        if (isBidder()) {
            this.mIsShowCandidate = false;
        }
    }

    public boolean isRewardedVideoAvailable() {
        return this.mAdapter.isRewardedVideoAvailable(this.mAdUnitSettings);
    }

    public boolean isReadyToShow() {
        boolean z = true;
        try {
            if (!isBidder()) {
                return isRewardedVideoAvailable();
            }
            if (!this.mIsShowCandidate || this.mState != SMASH_STATE.LOADED || !isRewardedVideoAvailable()) {
                z = false;
            }
            return z;
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("isReadyToShow exception: ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
            sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_isReadyException)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, th.getLocalizedMessage()}});
            return false;
        }
    }

    public void loadVideo(String str, String str2, int i, String str3, int i2) {
        SMASH_STATE smash_state;
        String str4 = str2;
        StringBuilder sb = new StringBuilder();
        sb.append("loadVideo() auctionId: ");
        sb.append(str2);
        sb.append(" state: ");
        sb.append(this.mState);
        logInternal(sb.toString());
        setIsLoadCandidate(false);
        this.mIsShowCandidate = true;
        synchronized (this.mStateLock) {
            smash_state = this.mState;
            if (!(this.mState == SMASH_STATE.LOAD_IN_PROGRESS || this.mState == SMASH_STATE.SHOW_IN_PROGRESS)) {
                setState(SMASH_STATE.LOAD_IN_PROGRESS);
            }
        }
        if (smash_state == SMASH_STATE.LOAD_IN_PROGRESS) {
            sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_loadInProgress)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "load during load"}});
            this.mShouldLoadAfterLoad = true;
            updateFutureAuctionData(str, str2, i, str3, i2);
            this.mListener.onLoadError(this, str2);
        } else if (smash_state == SMASH_STATE.SHOW_IN_PROGRESS) {
            sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_showInProgress)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "load during show"}});
            this.mShouldLoadAfterClose = true;
            updateFutureAuctionData(str, str2, i, str3, i2);
        } else {
            this.mCurrentAuctionId = str4;
            this.mAuctionTrial = i;
            this.mAuctionFallback = str3;
            this.mSessionDepth = i2;
            startLoadTimer();
            this.mLoadStartTime = new Date().getTime();
            sendProviderEvent(1001);
            try {
                if (isBidder()) {
                    String str5 = str;
                    this.mAdapter.loadVideo(this.mAdUnitSettings, this, str);
                } else if (smash_state == SMASH_STATE.NO_INIT) {
                    setCustomParams();
                    this.mAdapter.initRewardedVideo(this.mActivity, this.mAppKey, this.mUserId, this.mAdUnitSettings, this);
                } else {
                    this.mAdapter.fetchRewardedVideo(this.mAdUnitSettings);
                }
            } catch (Throwable th) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("loadVideo exception: ");
                sb2.append(th.getLocalizedMessage());
                logInternalError(sb2.toString());
                th.printStackTrace();
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_loadException)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, th.getLocalizedMessage()}});
            }
        }
    }

    public void reportShowChance(boolean z, int i) {
        this.mSessionDepth = i;
        Object[][] objArr = new Object[1][];
        Object[] objArr2 = new Object[2];
        objArr2[0] = "status";
        objArr2[1] = z ? "true" : "false";
        objArr[0] = objArr2;
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW_CHANCE, objArr);
    }

    public void showVideo(Placement placement, int i) {
        stopLoadTimer();
        logInternal("showVideo()");
        this.mCurrentPlacement = placement;
        this.mSessionDepth = i;
        setState(SMASH_STATE.SHOW_IN_PROGRESS);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW);
        try {
            this.mAdapter.showRewardedVideo(this.mAdUnitSettings, this);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("showVideo exception: ");
            sb.append(th.getLocalizedMessage());
            logInternalError(sb.toString());
            th.printStackTrace();
            onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_RV_SHOW_EXCEPTION, th.getLocalizedMessage()));
        }
    }

    public void setCappedPerSession() {
        this.mAdapter.setMediationState(MEDIATION_STATE.CAPPED_PER_SESSION, "rewardedvideo");
        sendProviderEvent(IronSourceConstants.RV_CAP_SESSION);
    }

    /* access modifiers changed from: private */
    public void setState(SMASH_STATE smash_state) {
        StringBuilder sb = new StringBuilder();
        sb.append("current state=");
        sb.append(this.mState);
        sb.append(", new state=");
        sb.append(smash_state);
        logInternal(sb.toString());
        synchronized (this.mStateLock) {
            this.mState = smash_state;
        }
    }

    private void setCustomParams() {
        try {
            String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
            if (!TextUtils.isEmpty(mediationSegment)) {
                this.mAdapter.setMediationSegment(mediationSegment);
            }
            String pluginType = ConfigFile.getConfigFile().getPluginType();
            if (!TextUtils.isEmpty(pluginType)) {
                this.mAdapter.setPluginData(pluginType, ConfigFile.getConfigFile().getPluginFrameworkVersion());
            }
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("setCustomParams() ");
            sb.append(e.getMessage());
            logInternal(sb.toString());
        }
    }

    public void onRewardedVideoAvailabilityChanged(boolean z) {
        boolean z2;
        stopLoadTimer();
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAvailabilityChanged available=");
        sb.append(z);
        sb.append(" state=");
        sb.append(this.mState.name());
        logAdapterCallback(sb.toString());
        synchronized (this.mStateLock) {
            if (this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
                setState(z ? SMASH_STATE.LOADED : SMASH_STATE.NOT_LOADED);
                z2 = false;
            } else {
                z2 = true;
            }
        }
        if (z2) {
            if (z) {
                sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_TRUE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, this.mState.name()}});
            } else {
                sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_RV_LOAD_UNEXPECTED_CALLBACK)}, new Object[]{"duration", Long.valueOf(getElapsedTime())}, new Object[]{IronSourceConstants.EVENTS_EXT1, this.mState.name()}});
            }
            return;
        }
        sendProviderEvent(z ? 1002 : IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{"duration", Long.valueOf(getElapsedTime())}});
        if (this.mShouldLoadAfterLoad) {
            this.mShouldLoadAfterLoad = false;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("onRewardedVideoAvailabilityChanged to ");
            sb2.append(z);
            sb2.append("and mShouldLoadAfterLoad is true - calling loadVideo");
            logInternal(sb2.toString());
            loadVideo(this.mAuctionServerDataToLoad, this.mAuctionIdToLoad, this.mAuctionTrialToLoad, this.mAuctionFallbackToLoad, this.mSessionDepthToLoad);
            resetAuctionParams();
            return;
        }
        if (z) {
            this.mListener.onLoadSuccess(this, this.mCurrentAuctionId);
        } else {
            this.mListener.onLoadError(this, this.mCurrentAuctionId);
        }
    }

    public void onRewardedVideoLoadFailed(IronSourceError ironSourceError) {
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED_REASON, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{"duration", Long.valueOf(getElapsedTime())}});
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdShowFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        logAdapterCallback(sb.toString());
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
        synchronized (this.mStateLock) {
            if (this.mState != SMASH_STATE.SHOW_IN_PROGRESS) {
                Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_showFailed)};
                StringBuilder sb2 = new StringBuilder();
                sb2.append("showFailed: ");
                sb2.append(this.mState);
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb2.toString()}});
                return;
            }
            setState(SMASH_STATE.NOT_LOADED);
            this.mListener.onRewardedVideoAdShowFailed(ironSourceError, this);
        }
    }

    public void onRewardedVideoInitSuccess() {
        logAdapterCallback("onRewardedVideoInitSuccess");
        synchronized (this.mStateLock) {
            if (this.mState != SMASH_STATE.INIT_IN_PROGRESS) {
                Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_initSuccess)};
                StringBuilder sb = new StringBuilder();
                sb.append("initSuccess: ");
                sb.append(this.mState);
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb.toString()}});
                return;
            }
            setState(SMASH_STATE.NOT_LOADED);
        }
    }

    public void onRewardedVideoInitFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoInitFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        logAdapterCallback(sb.toString());
        stopLoadTimer();
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_RV_LOAD_FAIL_DUE_TO_INIT)}, new Object[]{"duration", Long.valueOf(getElapsedTime())}});
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED_REASON, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{"duration", Long.valueOf(getElapsedTime())}});
        synchronized (this.mStateLock) {
            if (this.mState != SMASH_STATE.INIT_IN_PROGRESS) {
                Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_initFailed)};
                StringBuilder sb2 = new StringBuilder();
                sb2.append("initFailed: ");
                sb2.append(this.mState);
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb2.toString()}});
                return;
            }
            setState(SMASH_STATE.NO_INIT);
            this.mListener.onLoadError(this, this.mCurrentAuctionId);
        }
    }

    public void onRewardedVideoAdOpened() {
        logAdapterCallback("onRewardedVideoAdOpened");
        this.mListener.onRewardedVideoAdOpened(this);
        sendProviderEventWithPlacement(1005);
    }

    public void onRewardedVideoAdStarted() {
        logAdapterCallback("onRewardedVideoAdStarted");
        this.mListener.onRewardedVideoAdStarted(this);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_STARTED);
    }

    public void onRewardedVideoAdVisible() {
        logAdapterCallback("onRewardedVideoAdVisible");
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_VISIBLE);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0053, code lost:
        r10.mListener.onRewardedVideoAdClosed(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x005a, code lost:
        if (r10.mShouldLoadAfterClose == false) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x005c, code lost:
        logInternal("onRewardedVideoAdClosed and mShouldLoadAfterClose is true - calling loadVideo");
        r10.mShouldLoadAfterClose = false;
        loadVideo(r10.mAuctionServerDataToLoad, r10.mAuctionIdToLoad, r10.mAuctionTrialToLoad, r10.mAuctionFallbackToLoad, r10.mSessionDepthToLoad);
        resetAuctionParams();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0074, code lost:
        return;
     */
    public void onRewardedVideoAdClosed() {
        logAdapterCallback("onRewardedVideoAdClosed");
        synchronized (this.mStateLock) {
            if (this.mState != SMASH_STATE.SHOW_IN_PROGRESS) {
                sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_CLOSED);
                Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(errorCode_adClosed)};
                StringBuilder sb = new StringBuilder();
                sb.append("adClosed: ");
                sb.append(this.mState);
                sendProviderEvent(IronSourceConstants.RV_SMASH_UNEXPECTED_STATE, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb.toString()}});
                return;
            }
            setState(SMASH_STATE.NOT_LOADED);
        }
    }

    public void onRewardedVideoAdEnded() {
        logAdapterCallback("onRewardedVideoAdEnded");
        this.mListener.onRewardedVideoAdEnded(this);
        sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_ENDED);
    }

    public void onRewardedVideoAdRewarded() {
        logAdapterCallback("onRewardedVideoAdRewarded");
        this.mListener.onRewardedVideoAdRewarded(this, this.mCurrentPlacement);
        Map providerEventData = getProviderEventData();
        if (this.mCurrentPlacement != null) {
            providerEventData.put(IronSourceConstants.EVENTS_PLACEMENT_NAME, this.mCurrentPlacement.getPlacementName());
            providerEventData.put(IronSourceConstants.EVENTS_REWARD_NAME, this.mCurrentPlacement.getRewardName());
            providerEventData.put(IronSourceConstants.EVENTS_REWARD_AMOUNT, Integer.valueOf(this.mCurrentPlacement.getRewardAmount()));
        }
        if (!TextUtils.isEmpty(IronSourceObject.getInstance().getDynamicUserId())) {
            providerEventData.put(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceObject.getInstance().getDynamicUserId());
        }
        if (IronSourceObject.getInstance().getRvServerParams() != null) {
            for (String str : IronSourceObject.getInstance().getRvServerParams().keySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append("custom_");
                sb.append(str);
                providerEventData.put(sb.toString(), IronSourceObject.getInstance().getRvServerParams().get(str));
            }
        }
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            providerEventData.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (shouldAddAuctionParams(1010)) {
            RewardedVideoEventsManager.getInstance().setEventAuctionParams(providerEventData, this.mAuctionTrial, this.mAuctionFallback);
        }
        providerEventData.put("sessionDepth", Integer.valueOf(this.mSessionDepth));
        EventData eventData = new EventData(1010, new JSONObject(providerEventData));
        StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(Long.toString(eventData.getTimeStamp()));
        sb2.append(this.mAppKey);
        sb2.append(getInstanceName());
        eventData.addToAdditionalData(IronSourceConstants.EVENTS_TRANS_ID, IronSourceUtils.getTransId(sb2.toString()));
        RewardedVideoEventsManager.getInstance().log(eventData);
    }

    public void onRewardedVideoAdClicked() {
        logAdapterCallback("onRewardedVideoAdClicked");
        this.mListener.onRewardedVideoAdClicked(this, this.mCurrentPlacement);
        sendProviderEventWithPlacement(1006);
    }

    private void stopLoadTimer() {
        synchronized (this.mTimerLock) {
            if (this.mTimer != null) {
                this.mTimer.cancel();
                this.mTimer = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public long getElapsedTime() {
        return new Date().getTime() - this.mLoadStartTime;
    }

    private void updateFutureAuctionData(String str, String str2, int i, String str3, int i2) {
        this.mAuctionIdToLoad = str2;
        this.mAuctionServerDataToLoad = str;
        this.mAuctionTrialToLoad = i;
        this.mAuctionFallbackToLoad = str3;
        this.mSessionDepthToLoad = i2;
    }

    private void startLoadTimer() {
        synchronized (this.mTimerLock) {
            stopLoadTimer();
            this.mTimer = new Timer();
            this.mTimer.schedule(new TimerTask() {
                public void run() {
                    int i;
                    boolean z;
                    int i2;
                    String str = "Rewarded Video - load instance time out";
                    synchronized (ProgRvSmash.this.mStateLock) {
                        if (ProgRvSmash.this.mState != SMASH_STATE.LOAD_IN_PROGRESS) {
                            if (ProgRvSmash.this.mState != SMASH_STATE.INIT_IN_PROGRESS) {
                                z = false;
                                i = IronSourceError.ERROR_CODE_GENERIC;
                            }
                        }
                        if (ProgRvSmash.this.mState == SMASH_STATE.LOAD_IN_PROGRESS) {
                            i2 = 1025;
                        } else {
                            str = "Rewarded Video - init instance time out";
                            i2 = IronSourceError.ERROR_RV_INIT_FAILED_TIMEOUT;
                        }
                        ProgRvSmash.this.setState(SMASH_STATE.NOT_LOADED);
                        i = i2;
                        z = true;
                    }
                    ProgRvSmash.this.logInternal(str);
                    if (z) {
                        ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{"duration", Long.valueOf(ProgRvSmash.this.getElapsedTime())}});
                        ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED_REASON, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str}, new Object[]{"duration", Long.valueOf(ProgRvSmash.this.getElapsedTime())}});
                        ProgRvSmash.this.mListener.onLoadError(ProgRvSmash.this, ProgRvSmash.this.mCurrentAuctionId);
                        return;
                    }
                    ProgRvSmash.this.sendProviderEvent(IronSourceConstants.RV_INSTANCE_AVAILABILITY_FALSE, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1025)}, new Object[]{"duration", Long.valueOf(ProgRvSmash.this.getElapsedTime())}, new Object[]{IronSourceConstants.EVENTS_EXT1, ProgRvSmash.this.mState.name()}});
                }
            }, (long) (this.mLoadTimeoutSecs * 1000));
        }
    }

    private void logAdapterCallback(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("ProgRvSmash ");
        sb.append(getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.ADAPTER_CALLBACK, sb.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("ProgRvSmash ");
        sb.append(getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }

    private void logInternalError(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("ProgRvSmash ");
        sb.append(getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 3);
    }

    private void sendProviderEventWithPlacement(int i) {
        sendProviderEventWithPlacement(i, null);
    }

    public void sendProviderEventWithPlacement(int i, Object[][] objArr) {
        sendProviderEvent(i, objArr, true);
    }

    private void sendProviderEvent(int i) {
        sendProviderEvent(i, null, false);
    }

    public void sendProviderEvent(int i, Object[][] objArr) {
        sendProviderEvent(i, objArr, false);
    }

    private void sendProviderEvent(int i, Object[][] objArr, boolean z) {
        Map providerEventData = getProviderEventData();
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            providerEventData.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && this.mCurrentPlacement != null && !TextUtils.isEmpty(this.mCurrentPlacement.getPlacementName())) {
            providerEventData.put(IronSourceConstants.EVENTS_PLACEMENT_NAME, this.mCurrentPlacement.getPlacementName());
        }
        if (shouldAddAuctionParams(i)) {
            RewardedVideoEventsManager.getInstance().setEventAuctionParams(providerEventData, this.mAuctionTrial, this.mAuctionFallback);
        }
        providerEventData.put("sessionDepth", Integer.valueOf(this.mSessionDepth));
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append(getInstanceName());
                sb.append(" smash: RV sendMediationEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }
}
