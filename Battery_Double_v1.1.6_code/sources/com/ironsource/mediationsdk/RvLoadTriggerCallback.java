package com.ironsource.mediationsdk;

/* compiled from: RvLoadTrigger */
interface RvLoadTriggerCallback {
    void onLoadTriggered();
}
