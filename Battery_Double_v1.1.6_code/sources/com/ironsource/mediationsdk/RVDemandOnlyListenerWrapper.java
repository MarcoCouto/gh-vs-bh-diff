package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyRewardedVideoListener;

public class RVDemandOnlyListenerWrapper {
    private static final RVDemandOnlyListenerWrapper sInstance = new RVDemandOnlyListenerWrapper();
    /* access modifiers changed from: private */
    public ISDemandOnlyRewardedVideoListener mListener = null;

    public static RVDemandOnlyListenerWrapper getInstance() {
        return sInstance;
    }

    private RVDemandOnlyListenerWrapper() {
    }

    public void setListener(ISDemandOnlyRewardedVideoListener iSDemandOnlyRewardedVideoListener) {
        this.mListener = iSDemandOnlyRewardedVideoListener;
    }

    public void onRewardedVideoLoadSuccess(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.mListener.onRewardedVideoAdLoadSuccess(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onRewardedVideoAdLoadSuccess() instanceId=");
                    sb.append(str);
                    rVDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onRewardedVideoAdLoadFailed(final String str, final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.mListener.onRewardedVideoAdLoadFailed(str, ironSourceError);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onRewardedVideoAdLoadFailed() instanceId=");
                    sb.append(str);
                    sb.append("error=");
                    sb.append(ironSourceError.getErrorMessage());
                    rVDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onRewardedVideoAdOpened(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.mListener.onRewardedVideoAdOpened(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onRewardedVideoAdOpened() instanceId=");
                    sb.append(str);
                    rVDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onRewardedVideoAdClosed(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.mListener.onRewardedVideoAdClosed(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onRewardedVideoAdClosed() instanceId=");
                    sb.append(str);
                    rVDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onRewardedVideoAdShowFailed(final String str, final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.mListener.onRewardedVideoAdShowFailed(str, ironSourceError);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onRewardedVideoAdShowFailed() instanceId=");
                    sb.append(str);
                    sb.append("error=");
                    sb.append(ironSourceError.getErrorMessage());
                    rVDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onRewardedVideoAdClicked(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.mListener.onRewardedVideoAdClicked(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onRewardedVideoAdClicked() instanceId=");
                    sb.append(str);
                    rVDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onRewardedVideoAdRewarded(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.mListener.onRewardedVideoAdRewarded(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onRewardedVideoAdRewarded() instanceId=");
                    sb.append(str);
                    rVDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, str, 1);
    }
}
