package com.ironsource.mediationsdk.config;

public class VersionInfo {
    public static final String BUILD_DATE = "2020-04-07T11:56:54Z";
    public static final long BUILD_UNIX_TIME = 1586260614691L;
    public static final String GIT_DATE = "2020-04-07T11:55:28Z";
    public static final int GIT_REVISION = 5820;
    public static final String GIT_SHA = "1f78c117f646a937ebf1d940d73b01769049c7cb";
    public static final String MAVEN_GROUP = "";
    public static final String MAVEN_NAME = "Android-SSP-unit-testing-develop-release-master-branches";
    public static final String VERSION = "6.16.0";
}
