package com.ironsource.mediationsdk.config;

import com.ironsource.mediationsdk.logger.IronSourceError;

public class ConfigValidationResult {
    private IronSourceError mIronSourceError = null;
    private boolean mIsValid = true;

    public void setInvalid(IronSourceError ironSourceError) {
        this.mIsValid = false;
        this.mIronSourceError = ironSourceError;
    }

    public void setValid() {
        this.mIsValid = true;
        this.mIronSourceError = null;
    }

    public boolean isValid() {
        return this.mIsValid;
    }

    public IronSourceError getIronSourceError() {
        return this.mIronSourceError;
    }

    public String toString() {
        if (isValid()) {
            StringBuilder sb = new StringBuilder();
            sb.append("valid:");
            sb.append(this.mIsValid);
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("valid:");
        sb2.append(this.mIsValid);
        sb2.append(", IronSourceError:");
        sb2.append(this.mIronSourceError);
        return sb2.toString();
    }
}
