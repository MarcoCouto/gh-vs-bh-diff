package com.ironsource.mediationsdk;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class AuctionResponseItem {
    private List<String> mBurls;
    private String mInstanceName;
    private boolean mIsValid;
    private List<String> mLurls;
    private List<String> mNurls;
    private String mPrice;
    private String mServerData;

    public AuctionResponseItem(String str) {
        this.mInstanceName = str;
        this.mServerData = "";
        this.mPrice = "";
        this.mBurls = new ArrayList();
        this.mLurls = new ArrayList();
        this.mNurls = new ArrayList();
        this.mIsValid = true;
    }

    public AuctionResponseItem(JSONObject jSONObject) {
        this.mIsValid = false;
        try {
            this.mInstanceName = jSONObject.getString("instance");
            if (jSONObject.has("adMarkup")) {
                this.mServerData = jSONObject.getString("adMarkup");
            } else if (jSONObject.has("serverData")) {
                this.mServerData = jSONObject.getString("serverData");
            } else {
                this.mServerData = "";
            }
            if (jSONObject.has("price")) {
                this.mPrice = jSONObject.getString("price");
            } else {
                this.mPrice = "0";
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("notifications");
            this.mBurls = new ArrayList();
            if (optJSONObject.has("burl")) {
                JSONArray jSONArray = optJSONObject.getJSONArray("burl");
                for (int i = 0; i < jSONArray.length(); i++) {
                    this.mBurls.add(jSONArray.getString(i));
                }
            }
            this.mLurls = new ArrayList();
            if (optJSONObject.has("lurl")) {
                JSONArray jSONArray2 = optJSONObject.getJSONArray("lurl");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    this.mLurls.add(jSONArray2.getString(i2));
                }
            }
            this.mNurls = new ArrayList();
            if (optJSONObject.has("nurl")) {
                JSONArray jSONArray3 = optJSONObject.getJSONArray("nurl");
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    this.mNurls.add(jSONArray3.getString(i3));
                }
            }
            this.mIsValid = true;
        } catch (Exception unused) {
        }
    }

    public String getInstanceName() {
        return this.mInstanceName;
    }

    public String getServerData() {
        return this.mServerData;
    }

    public String getPrice() {
        return this.mPrice;
    }

    public List<String> getBurls() {
        return this.mBurls;
    }

    public List<String> getLurls() {
        return this.mLurls;
    }

    public List<String> getNurls() {
        return this.mNurls;
    }

    public boolean isValid() {
        return this.mIsValid;
    }
}
