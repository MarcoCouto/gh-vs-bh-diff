package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyRvManagerListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class DemandOnlyRvSmash extends DemandOnlySmash implements RewardedVideoSmashListener {
    /* access modifiers changed from: private */
    public DemandOnlyRvManagerListener mListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;

    public void onRewardedVideoAdEnded() {
    }

    public void onRewardedVideoAdStarted() {
    }

    public void onRewardedVideoAvailabilityChanged(boolean z) {
    }

    public void onRewardedVideoInitFailed(IronSourceError ironSourceError) {
    }

    public void onRewardedVideoInitSuccess() {
    }

    DemandOnlyRvSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, DemandOnlyRvManagerListener demandOnlyRvManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getInterstitialSettings()), abstractAdapter);
        this.mAdapterConfig = new AdapterConfig(providerSettings, providerSettings.getRewardedVideoSettings());
        this.mAdUnitSettings = this.mAdapterConfig.getAdUnitSetings();
        this.mAdapter = abstractAdapter;
        this.mListener = demandOnlyRvManagerListener;
        this.mLoadTimeoutSecs = i;
        this.mAdapter.initRvForDemandOnly(activity, str, str2, this.mAdUnitSettings, this);
    }

    public void loadRewardedVideo(String str, String str2, List<String> list) {
        StringBuilder sb = new StringBuilder();
        sb.append("loadRewardedVideo state=");
        sb.append(getStateString());
        logInternal(sb.toString());
        SMASH_STATE compareAndSetState = compareAndSetState(new SMASH_STATE[]{SMASH_STATE.NOT_LOADED, SMASH_STATE.LOADED}, SMASH_STATE.LOAD_IN_PROGRESS);
        if (compareAndSetState == SMASH_STATE.NOT_LOADED || compareAndSetState == SMASH_STATE.LOADED) {
            this.mLoadStartTime = new Date().getTime();
            startLoadTimer();
            if (isBidder()) {
                this.mAuctionId = str2;
                this.mBUrl = list;
                this.mAdapter.loadVideoForDemandOnly(this.mAdUnitSettings, this, str);
                return;
            }
            this.mAdapter.loadVideoForDemandOnly(this.mAdUnitSettings, this);
        } else if (compareAndSetState == SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_ALREADY_IN_PROGRESS, "load already in progress"), this, 0);
        } else {
            this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_DURING_SHOW, "cannot load because show is in progress"), this, 0);
        }
    }

    public void showRewardedVideo() {
        StringBuilder sb = new StringBuilder();
        sb.append("showRewardedVideo state=");
        sb.append(getStateString());
        logInternal(sb.toString());
        if (compareAndSetState(SMASH_STATE.LOADED, SMASH_STATE.SHOW_IN_PROGRESS)) {
            this.mAdapter.showRewardedVideo(this.mAdUnitSettings, this);
            return;
        }
        this.mListener.onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_CALL_LOAD_BEFORE_SHOW, "load must be called before show"), this);
    }

    public boolean isRewardedVideoAvailable() {
        return this.mAdapter.isRewardedVideoAvailable(this.mAdUnitSettings);
    }

    private void startLoadTimer() {
        logInternal("start timer");
        startTimer(new TimerTask() {
            public void run() {
                DemandOnlyRvSmash demandOnlyRvSmash = DemandOnlyRvSmash.this;
                StringBuilder sb = new StringBuilder();
                sb.append("load timed out state=");
                sb.append(DemandOnlyRvSmash.this.getStateString());
                demandOnlyRvSmash.logInternal(sb.toString());
                if (DemandOnlyRvSmash.this.compareAndSetState(SMASH_STATE.LOAD_IN_PROGRESS, SMASH_STATE.NOT_LOADED)) {
                    DemandOnlyRvSmash.this.mListener.onRewardedVideoAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_RV_LOAD_TIMED_OUT, "load timed out"), DemandOnlyRvSmash.this, new Date().getTime() - DemandOnlyRvSmash.this.mLoadStartTime);
                }
            }
        });
    }

    public void onRewardedVideoLoadSuccess() {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoLoadSuccess state=");
        sb.append(getStateString());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (compareAndSetState(SMASH_STATE.LOAD_IN_PROGRESS, SMASH_STATE.LOADED)) {
            this.mListener.onRewardedVideoLoadSuccess(this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onRewardedVideoLoadFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoLoadFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        sb.append(" state=");
        sb.append(getStateString());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (compareAndSetState(SMASH_STATE.LOAD_IN_PROGRESS, SMASH_STATE.NOT_LOADED)) {
            this.mListener.onRewardedVideoAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onRewardedVideoAdOpened() {
        logAdapterCallback("onRewardedVideoAdOpened");
        this.mListener.onRewardedVideoAdOpened(this);
    }

    public void onRewardedVideoAdClosed() {
        setState(SMASH_STATE.NOT_LOADED);
        logAdapterCallback("onRewardedVideoAdClosed");
        this.mListener.onRewardedVideoAdClosed(this);
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
        setState(SMASH_STATE.NOT_LOADED);
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdClosed error=");
        sb.append(ironSourceError);
        logAdapterCallback(sb.toString());
        this.mListener.onRewardedVideoAdShowFailed(ironSourceError, this);
    }

    public void onRewardedVideoAdClicked() {
        logAdapterCallback("onRewardedVideoAdClicked");
        this.mListener.onRewardedVideoAdClicked(this);
    }

    public void onRewardedVideoAdVisible() {
        logAdapterCallback("onRewardedVideoAdVisible");
        this.mListener.onRewardedVideoAdVisible(this);
    }

    public void onRewardedVideoAdRewarded() {
        logAdapterCallback("onRewardedVideoAdRewarded");
        this.mListener.onRewardedVideoAdRewarded(this);
    }

    private void logAdapterCallback(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyRewardedVideoSmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.ADAPTER_CALLBACK, sb.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyRewardedVideoSmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }
}
