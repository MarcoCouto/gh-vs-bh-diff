package com.ironsource.mediationsdk;

import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class AuctionHistory {
    public static final int DISABLED_FEATURE = 0;
    public static final int NO_LIMIT = -1;
    private ConcurrentHashMap<String, ArrayList<ISAuctionPerformance>> auctionPerformance = new ConcurrentHashMap<>();
    private int historyLimit;

    public enum ISAuctionPerformance {
        ISAuctionPerformanceDidntAttemptToLoad,
        ISAuctionPerformanceFailedToLoad,
        ISAuctionPerformanceLoadedSuccessfully,
        ISAuctionPerformanceFailedToShow,
        ISAuctionPerformanceShowedSuccessfully,
        ISAuctionPerformanceNotPartOfWaterfall
    }

    public AuctionHistory(List<String> list, int i) {
        this.historyLimit = i;
        for (String put : list) {
            this.auctionPerformance.put(put, new ArrayList());
        }
    }

    public void storeWaterfallPerformance(ConcurrentHashMap<String, ISAuctionPerformance> concurrentHashMap) {
        if (this.historyLimit != 0) {
            for (String str : this.auctionPerformance.keySet()) {
                ISAuctionPerformance iSAuctionPerformance = ISAuctionPerformance.ISAuctionPerformanceNotPartOfWaterfall;
                if (concurrentHashMap.containsKey(str)) {
                    iSAuctionPerformance = (ISAuctionPerformance) concurrentHashMap.get(str);
                }
                ArrayList arrayList = (ArrayList) this.auctionPerformance.get(str);
                if (this.historyLimit != -1 && arrayList.size() == this.historyLimit) {
                    arrayList.remove(0);
                }
                arrayList.add(iSAuctionPerformance);
            }
        }
    }

    public String getStoredPerformanceForInstance(String str) {
        ArrayList arrayList = (ArrayList) this.auctionPerformance.get(str);
        if (arrayList == null || arrayList.isEmpty()) {
            return "";
        }
        Iterator it = arrayList.iterator();
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(((ISAuctionPerformance) it.next()).ordinal());
        String sb2 = sb.toString();
        while (it.hasNext()) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb2);
            sb3.append(",");
            String sb4 = sb3.toString();
            StringBuilder sb5 = new StringBuilder();
            sb5.append(sb4);
            sb5.append(((ISAuctionPerformance) it.next()).ordinal());
            sb2 = sb5.toString();
        }
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb6 = new StringBuilder();
        sb6.append(str);
        sb6.append(" stored performance: : ");
        sb6.append(sb2);
        logger.log(ironSourceTag, sb6.toString(), 1);
        return sb2;
    }
}
