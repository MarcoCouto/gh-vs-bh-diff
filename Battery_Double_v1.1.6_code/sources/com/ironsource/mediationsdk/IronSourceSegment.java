package com.ironsource.mediationsdk;

import android.text.TextUtils;
import android.util.Pair;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

public class IronSourceSegment {
    public static final String AGE = "age";
    public static final String GENDER = "gen";
    public static final String IAPT = "iapt";
    public static final String LEVEL = "lvl";
    public static final String PAYING = "pay";
    private static final String SEGMENT_NAME = "segName";
    public static final String USER_CREATION_DATE = "ucd";
    private final String CUSTOM = "custom";
    private final int MAX_CUSTOMS = 5;
    private double MAX_IAPT = 999999.99d;
    private int MAX_LEVEL = 999999;
    private int mAge = -1;
    private Vector<Pair<String, String>> mCustoms = new Vector<>();
    private String mGender;
    private double mIapt = -1.0d;
    private AtomicBoolean mIsPaying = null;
    private int mLevel = -1;
    private String mSegmentName;
    private long mUcd = 0;

    public String getSegmentName() {
        return this.mSegmentName;
    }

    public int getAge() {
        return this.mAge;
    }

    public String getGender() {
        return this.mGender;
    }

    public int getLevel() {
        return this.mLevel;
    }

    public AtomicBoolean getIsPaying() {
        return this.mIsPaying;
    }

    public double getIapt() {
        return this.mIapt;
    }

    public long getUcd() {
        return this.mUcd;
    }

    public void setAge(int i) {
        if (i <= 0 || i > 199) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("setAge( ");
            sb.append(i);
            sb.append(" ) age must be between 1-199");
            logger.log(ironSourceTag, sb.toString(), 2);
            return;
        }
        this.mAge = i;
    }

    public void setGender(String str) {
        if (TextUtils.isEmpty(str) || (!str.toLowerCase().equals("male") && !str.toLowerCase().equals("female"))) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("setGender( ");
            sb.append(str);
            sb.append(" ) is invalid");
            logger.log(ironSourceTag, sb.toString(), 2);
            return;
        }
        this.mGender = str;
    }

    public void setLevel(int i) {
        if (i <= 0 || i >= this.MAX_LEVEL) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("setLevel( ");
            sb.append(i);
            sb.append(" ) level must be between 1-");
            sb.append(this.MAX_LEVEL);
            logger.log(ironSourceTag, sb.toString(), 2);
            return;
        }
        this.mLevel = i;
    }

    public void setIsPaying(boolean z) {
        if (this.mIsPaying == null) {
            this.mIsPaying = new AtomicBoolean();
        }
        this.mIsPaying.set(z);
    }

    public void setIAPTotal(double d) {
        if (d <= Utils.DOUBLE_EPSILON || d >= this.MAX_IAPT) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("setIAPTotal( ");
            sb.append(d);
            sb.append(" ) iapt must be between 0-");
            sb.append(this.MAX_IAPT);
            logger.log(ironSourceTag, sb.toString(), 2);
            return;
        }
        this.mIapt = Math.floor(d * 100.0d) / 100.0d;
    }

    public void setUserCreationDate(long j) {
        if (j > 0) {
            this.mUcd = j;
            return;
        }
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("setUserCreationDate( ");
        sb.append(j);
        sb.append(" ) is an invalid timestamp");
        logger.log(ironSourceTag, sb.toString(), 2);
    }

    public void setSegmentName(String str) {
        if (!validateAlphanumeric(str) || !validateLength(str, 1, 32)) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("setSegmentName( ");
            sb.append(str);
            sb.append(" ) segment name must be alphanumeric and 1-32 in length");
            logger.log(ironSourceTag, sb.toString(), 2);
            return;
        }
        this.mSegmentName = str;
    }

    public void setCustom(String str, String str2) {
        try {
            if (!validateAlphanumeric(str) || !validateAlphanumeric(str2) || !validateLength(str, 1, 32) || !validateLength(str2, 1, 32)) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("setCustom( ");
                sb.append(str);
                sb.append(" , ");
                sb.append(str2);
                sb.append(" ) key and value must be alphanumeric and 1-32 in length");
                logger.log(ironSourceTag, sb.toString(), 2);
                return;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("custom_");
            sb2.append(str);
            String sb3 = sb2.toString();
            if (this.mCustoms.size() < 5) {
                this.mCustoms.add(new Pair(sb3, str2));
                return;
            }
            this.mCustoms.remove(0);
            this.mCustoms.add(new Pair(sb3, str2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: 0000 */
    public Vector<Pair<String, String>> getSegmentData() {
        Vector<Pair<String, String>> vector = new Vector<>();
        if (this.mAge != -1) {
            String str = AGE;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mAge);
            sb.append("");
            vector.add(new Pair(str, sb.toString()));
        }
        if (!TextUtils.isEmpty(this.mGender)) {
            vector.add(new Pair(GENDER, this.mGender));
        }
        if (this.mLevel != -1) {
            String str2 = LEVEL;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.mLevel);
            sb2.append("");
            vector.add(new Pair(str2, sb2.toString()));
        }
        if (this.mIsPaying != null) {
            String str3 = PAYING;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(this.mIsPaying);
            sb3.append("");
            vector.add(new Pair(str3, sb3.toString()));
        }
        if (this.mIapt != -1.0d) {
            String str4 = IAPT;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(this.mIapt);
            sb4.append("");
            vector.add(new Pair(str4, sb4.toString()));
        }
        if (this.mUcd != 0) {
            String str5 = USER_CREATION_DATE;
            StringBuilder sb5 = new StringBuilder();
            sb5.append(this.mUcd);
            sb5.append("");
            vector.add(new Pair(str5, sb5.toString()));
        }
        if (!TextUtils.isEmpty(this.mSegmentName)) {
            vector.add(new Pair(SEGMENT_NAME, this.mSegmentName));
        }
        vector.addAll(this.mCustoms);
        return vector;
    }

    private boolean validateAlphanumeric(String str) {
        if (str == null) {
            return false;
        }
        return str.matches("^[a-zA-Z0-9]*$");
    }

    private boolean validateLength(String str, int i, int i2) {
        return str != null && str.length() >= i && str.length() <= i2;
    }
}
