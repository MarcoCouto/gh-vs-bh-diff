package com.ironsource.mediationsdk;

import android.text.TextUtils;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class DemandOnlySmash {
    protected JSONObject mAdUnitSettings;
    protected AbstractAdapter mAdapter;
    protected AdapterConfig mAdapterConfig;
    protected String mAuctionId;
    protected List<String> mBUrl;
    protected String mDynamicDemandSourceId;
    int mLoadTimeoutSecs;
    private Timer mLoadTimer;
    private SMASH_STATE mState;
    private final Object mStateLock = new Object();
    private final Object mTimerLock = new Object();

    protected enum SMASH_STATE {
        NOT_LOADED,
        LOAD_IN_PROGRESS,
        LOADED,
        SHOW_IN_PROGRESS
    }

    public DemandOnlySmash(AdapterConfig adapterConfig, AbstractAdapter abstractAdapter) {
        this.mAdapterConfig = adapterConfig;
        this.mAdapter = abstractAdapter;
        this.mAdUnitSettings = adapterConfig.getAdUnitSetings();
        this.mState = SMASH_STATE.NOT_LOADED;
        this.mLoadTimer = null;
        this.mAuctionId = "";
        this.mBUrl = new ArrayList();
    }

    public String getInstanceName() {
        return this.mAdapterConfig.getProviderName();
    }

    public String getSubProviderId() {
        return this.mAdapterConfig.getSubProviderId();
    }

    public String getAuctionId() {
        return this.mAuctionId;
    }

    public List<String> getbURL() {
        return this.mBUrl;
    }

    public Map<String, Object> getProviderEventData() {
        HashMap hashMap = new HashMap();
        try {
            hashMap.put("providerAdapterVersion", this.mAdapter != null ? this.mAdapter.getVersion() : "");
            hashMap.put("providerSDKVersion", this.mAdapter != null ? this.mAdapter.getCoreSDKVersion() : "");
            hashMap.put("spId", this.mAdapterConfig.getSubProviderId());
            hashMap.put("provider", this.mAdapterConfig.getAdSourceNameForEvents());
            hashMap.put(IronSourceConstants.EVENTS_DEMAND_ONLY, Integer.valueOf(1));
            if (isBidder()) {
                hashMap.put(IronSourceConstants.EVENTS_PROGRAMMATIC, Integer.valueOf(1));
                hashMap.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, Integer.valueOf(2));
                if (!TextUtils.isEmpty(this.mAuctionId)) {
                    hashMap.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mAuctionId);
                }
            } else {
                hashMap.put(IronSourceConstants.EVENTS_PROGRAMMATIC, Integer.valueOf(0));
                hashMap.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, Integer.valueOf(1));
            }
            if (!TextUtils.isEmpty(this.mDynamicDemandSourceId)) {
                hashMap.put(IronSourceConstants.EVENTS_DYNAMIC_DEMAND_SOURCE_ID, this.mDynamicDemandSourceId);
            }
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
            StringBuilder sb = new StringBuilder();
            sb.append("getProviderEventData ");
            sb.append(getInstanceName());
            sb.append(")");
            logger.logException(ironSourceTag, sb.toString(), e);
        }
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    public boolean compareAndSetState(SMASH_STATE smash_state, SMASH_STATE smash_state2) {
        synchronized (this.mStateLock) {
            if (this.mState != smash_state) {
                return false;
            }
            setState(smash_state2);
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public SMASH_STATE compareAndSetState(SMASH_STATE[] smash_stateArr, SMASH_STATE smash_state) {
        SMASH_STATE smash_state2;
        synchronized (this.mStateLock) {
            smash_state2 = this.mState;
            if (Arrays.asList(smash_stateArr).contains(this.mState)) {
                setState(smash_state);
            }
        }
        return smash_state2;
    }

    /* access modifiers changed from: 0000 */
    public void setState(SMASH_STATE smash_state) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlySmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(": current state=");
        sb.append(this.mState);
        sb.append(", new state=");
        sb.append(smash_state);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
        synchronized (this.mStateLock) {
            this.mState = smash_state;
        }
    }

    /* access modifiers changed from: 0000 */
    public String getStateString() {
        return this.mState == null ? "null" : this.mState.toString();
    }

    /* access modifiers changed from: 0000 */
    public void startTimer(TimerTask timerTask) {
        synchronized (this.mTimerLock) {
            stopTimer();
            this.mLoadTimer = new Timer();
            this.mLoadTimer.schedule(timerTask, (long) (this.mLoadTimeoutSecs * 1000));
        }
    }

    /* access modifiers changed from: 0000 */
    public void stopTimer() {
        synchronized (this.mTimerLock) {
            if (this.mLoadTimer != null) {
                this.mLoadTimer.cancel();
                this.mLoadTimer = null;
            }
        }
    }

    public boolean isBidder() {
        return this.mAdapterConfig.isBidder();
    }

    public AdapterConfig getAdapterConfig() {
        return this.mAdapterConfig;
    }

    public void setDynamicDemandSourceIdByServerData(String str) {
        this.mDynamicDemandSourceId = AuctionDataUtils.getInstance().getDynamicDemandSourceIdFromServerData(str);
    }
}
