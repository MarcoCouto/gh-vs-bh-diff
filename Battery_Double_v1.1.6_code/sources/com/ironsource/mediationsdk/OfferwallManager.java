package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.OfferwallPlacement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.InternalOfferwallApi;
import com.ironsource.mediationsdk.sdk.InternalOfferwallListener;
import com.ironsource.mediationsdk.sdk.OfferwallAdapterApi;
import com.ironsource.mediationsdk.sdk.OfferwallListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

class OfferwallManager implements InternalOfferwallApi, InternalOfferwallListener {
    private final String TAG = getClass().getName();
    private Activity mActivity;
    private OfferwallAdapterApi mAdapter;
    private AtomicBoolean mAtomicShouldPerformInit = new AtomicBoolean(true);
    private String mCurrentPlacementName;
    private AtomicBoolean mIsOfferwallAvailable = new AtomicBoolean(false);
    private InternalOfferwallListener mListenersWrapper;
    private IronSourceLoggerManager mLoggerManager = IronSourceLoggerManager.getLogger();
    private ProviderSettings mProviderSettings;
    private ServerResponseWrapper mServerResponseWrapper;

    public void setOfferwallListener(OfferwallListener offerwallListener) {
    }

    public void showOfferwall() {
    }

    OfferwallManager() {
    }

    public synchronized void initOfferwall(Activity activity, String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
        StringBuilder sb = new StringBuilder();
        sb.append(this.TAG);
        sb.append(":initOfferwall(appKey: ");
        sb.append(str);
        sb.append(", userId: ");
        sb.append(str2);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        this.mActivity = activity;
        this.mServerResponseWrapper = IronSourceObject.getInstance().getCurrentServerResponse();
        if (this.mServerResponseWrapper == null) {
            reportInitFail(ErrorBuilder.buildInitFailedError("Please check configurations for Offerwall adapters", IronSourceConstants.OFFERWALL_AD_UNIT));
            return;
        }
        this.mProviderSettings = this.mServerResponseWrapper.getProviderSettingsHolder().getProviderSettings(IronSourceConstants.SUPERSONIC_CONFIG_NAME);
        if (this.mProviderSettings == null) {
            reportInitFail(ErrorBuilder.buildInitFailedError("Please check configurations for Offerwall adapters", IronSourceConstants.OFFERWALL_AD_UNIT));
            return;
        }
        AbstractAdapter startOfferwallAdapter = startOfferwallAdapter();
        if (startOfferwallAdapter == null) {
            reportInitFail(ErrorBuilder.buildInitFailedError("Please check configurations for Offerwall adapters", IronSourceConstants.OFFERWALL_AD_UNIT));
            return;
        }
        setCustomParams(startOfferwallAdapter);
        startOfferwallAdapter.setLogListener(this.mLoggerManager);
        this.mAdapter = (OfferwallAdapterApi) startOfferwallAdapter;
        this.mAdapter.setInternalOfferwallListener(this);
        this.mAdapter.initOfferwall(activity, str, str2, this.mProviderSettings.getRewardedVideoSettings());
    }

    public void showOfferwall(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("OWManager:showOfferwall(");
        sb.append(str);
        sb.append(")");
        String sb2 = sb.toString();
        try {
            if (!IronSourceUtils.isNetworkConnected(this.mActivity)) {
                this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildNoInternetConnectionShowFailError(IronSourceConstants.OFFERWALL_AD_UNIT));
                return;
            }
            this.mCurrentPlacementName = str;
            OfferwallPlacement offerwallPlacement = this.mServerResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferwallPlacement(str);
            if (offerwallPlacement == null) {
                this.mLoggerManager.log(IronSourceTag.INTERNAL, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
                offerwallPlacement = this.mServerResponseWrapper.getConfigurations().getOfferwallConfigurations().getDefaultOfferwallPlacement();
                if (offerwallPlacement == null) {
                    this.mLoggerManager.log(IronSourceTag.INTERNAL, "Default placement was not found, please make sure you are using the right placements.", 3);
                    return;
                }
            }
            this.mLoggerManager.log(IronSourceTag.INTERNAL, sb2, 1);
            if (!(this.mIsOfferwallAvailable == null || !this.mIsOfferwallAvailable.get() || this.mAdapter == null)) {
                this.mAdapter.showOfferwall(String.valueOf(offerwallPlacement.getPlacementId()), this.mProviderSettings.getRewardedVideoSettings());
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceTag.INTERNAL, sb2, e);
        }
    }

    public synchronized boolean isOfferwallAvailable() {
        boolean z;
        z = false;
        if (this.mIsOfferwallAvailable != null) {
            z = this.mIsOfferwallAvailable.get();
        }
        return z;
    }

    public void getOfferwallCredits() {
        if (this.mAdapter != null) {
            this.mAdapter.getOfferwallCredits();
        }
    }

    public void setInternalOfferwallListener(InternalOfferwallListener internalOfferwallListener) {
        this.mListenersWrapper = internalOfferwallListener;
    }

    public void onOfferwallAvailable(boolean z) {
        onOfferwallAvailable(z, null);
    }

    public void onOfferwallAvailable(boolean z, IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onOfferwallAvailable(isAvailable: ");
        sb.append(z);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (z) {
            this.mIsOfferwallAvailable.set(true);
            if (this.mListenersWrapper != null) {
                this.mListenersWrapper.onOfferwallAvailable(true);
                return;
            }
            return;
        }
        reportInitFail(ironSourceError);
    }

    public void onOfferwallOpened() {
        this.mLoggerManager.log(IronSourceTag.ADAPTER_CALLBACK, "onOfferwallOpened()", 1);
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        try {
            if (!TextUtils.isEmpty(this.mCurrentPlacementName)) {
                mediationAdditionalData.put(IronSourceConstants.EVENTS_PLACEMENT_NAME, this.mCurrentPlacementName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(IronSourceConstants.OFFERWALL_OPENED, mediationAdditionalData));
        if (this.mListenersWrapper != null) {
            this.mListenersWrapper.onOfferwallOpened();
        }
    }

    public void onOfferwallShowFailed(IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onOfferwallShowFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (this.mListenersWrapper != null) {
            this.mListenersWrapper.onOfferwallShowFailed(ironSourceError);
        }
    }

    public boolean onOfferwallAdCredited(int i, int i2, boolean z) {
        this.mLoggerManager.log(IronSourceTag.ADAPTER_CALLBACK, "onOfferwallAdCredited()", 1);
        if (this.mListenersWrapper != null) {
            return this.mListenersWrapper.onOfferwallAdCredited(i, i2, z);
        }
        return false;
    }

    public void onGetOfferwallCreditsFailed(IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onGetOfferwallCreditsFailed(");
        sb.append(ironSourceError);
        sb.append(")");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
        if (this.mListenersWrapper != null) {
            this.mListenersWrapper.onGetOfferwallCreditsFailed(ironSourceError);
        }
    }

    public void onOfferwallClosed() {
        this.mLoggerManager.log(IronSourceTag.ADAPTER_CALLBACK, "onOfferwallClosed()", 1);
        if (this.mListenersWrapper != null) {
            this.mListenersWrapper.onOfferwallClosed();
        }
    }

    private synchronized void reportInitFail(IronSourceError ironSourceError) {
        if (this.mIsOfferwallAvailable != null) {
            this.mIsOfferwallAvailable.set(false);
        }
        if (this.mAtomicShouldPerformInit != null) {
            this.mAtomicShouldPerformInit.set(true);
        }
        if (this.mListenersWrapper != null) {
            this.mListenersWrapper.onOfferwallAvailable(false, ironSourceError);
        }
    }

    private AbstractAdapter startOfferwallAdapter() {
        try {
            IronSourceObject instance = IronSourceObject.getInstance();
            AbstractAdapter offerwallAdapter = instance.getOfferwallAdapter(IronSourceConstants.SUPERSONIC_CONFIG_NAME);
            if (offerwallAdapter == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("com.ironsource.adapters.");
                sb.append(IronSourceConstants.SUPERSONIC_CONFIG_NAME.toLowerCase());
                sb.append(".");
                sb.append(IronSourceConstants.SUPERSONIC_CONFIG_NAME);
                sb.append("Adapter");
                Class cls = Class.forName(sb.toString());
                offerwallAdapter = (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, new Class[]{String.class}).invoke(cls, new Object[]{IronSourceConstants.SUPERSONIC_CONFIG_NAME});
                if (offerwallAdapter == null) {
                    return null;
                }
            }
            instance.addOWAdapter(offerwallAdapter);
            return offerwallAdapter;
        } catch (Throwable th) {
            this.mLoggerManager.log(IronSourceTag.API, "SupersonicAds initialization failed - please verify that required dependencies are in you build path.", 2);
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":startOfferwallAdapter");
            ironSourceLoggerManager.logException(ironSourceTag, sb2.toString(), th);
            return null;
        }
    }

    private void setCustomParams(AbstractAdapter abstractAdapter) {
        try {
            Integer age = IronSourceObject.getInstance().getAge();
            if (age != null) {
                abstractAdapter.setAge(age.intValue());
            }
            String gender = IronSourceObject.getInstance().getGender();
            if (gender != null) {
                abstractAdapter.setGender(gender);
            }
            String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
            if (mediationSegment != null) {
                abstractAdapter.setMediationSegment(mediationSegment);
            }
            Boolean consent = IronSourceObject.getInstance().getConsent();
            if (consent != null) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
                StringBuilder sb = new StringBuilder();
                sb.append("Offerwall | setConsent(consent:");
                sb.append(consent);
                sb.append(")");
                ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 1);
                abstractAdapter.setConsent(consent.booleanValue());
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceTag ironSourceTag2 = IronSourceTag.INTERNAL;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(":setCustomParams():");
            sb2.append(e.toString());
            ironSourceLoggerManager2.log(ironSourceTag2, sb2.toString(), 3);
        }
    }
}
