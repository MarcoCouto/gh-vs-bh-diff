package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;

public class ISDemandOnlyListenerWrapper {
    private static final ISDemandOnlyListenerWrapper sInstance = new ISDemandOnlyListenerWrapper();
    /* access modifiers changed from: private */
    public ISDemandOnlyInterstitialListener mListener = null;

    public static ISDemandOnlyListenerWrapper getInstance() {
        return sInstance;
    }

    private ISDemandOnlyListenerWrapper() {
    }

    public void setListener(ISDemandOnlyInterstitialListener iSDemandOnlyInterstitialListener) {
        this.mListener = iSDemandOnlyInterstitialListener;
    }

    public ISDemandOnlyInterstitialListener getListener() {
        return this.mListener;
    }

    public void onInterstitialAdReady(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdReady(str);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onInterstitialAdReady() instanceId=");
                    sb.append(str);
                    iSDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onInterstitialAdLoadFailed(final String str, final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdLoadFailed(str, ironSourceError);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onInterstitialAdLoadFailed() instanceId=");
                    sb.append(str);
                    sb.append(" error=");
                    sb.append(ironSourceError.getErrorMessage());
                    iSDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onInterstitialAdOpened(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdOpened(str);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onInterstitialAdOpened() instanceId=");
                    sb.append(str);
                    iSDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onInterstitialAdClosed(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdClosed(str);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onInterstitialAdClosed() instanceId=");
                    sb.append(str);
                    iSDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onInterstitialAdShowFailed(final String str, final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdShowFailed(str, ironSourceError);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onInterstitialAdShowFailed() instanceId=");
                    sb.append(str);
                    sb.append(" error=");
                    sb.append(ironSourceError.getErrorMessage());
                    iSDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    public void onInterstitialAdClicked(final String str) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.mListener.onInterstitialAdClicked(str);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onInterstitialAdClicked() instanceId=");
                    sb.append(str);
                    iSDemandOnlyListenerWrapper.log(sb.toString());
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, str, 1);
    }
}
