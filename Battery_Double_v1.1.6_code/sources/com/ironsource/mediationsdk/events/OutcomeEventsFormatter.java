package com.ironsource.mediationsdk.events;

import com.ironsource.eventsmodule.EventData;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class OutcomeEventsFormatter extends AbstractEventsFormatter {
    private final String DEFAULT_OC_EVENTS_URL = "https://outcome-ssp.supersonicads.com/mediation?adUnit=3";

    public String getDefaultEventsUrl() {
        return "https://outcome-ssp.supersonicads.com/mediation?adUnit=3";
    }

    public String getFormatterType() {
        return "outcome";
    }

    OutcomeEventsFormatter(int i) {
        this.mAdUnit = i;
    }

    public String format(ArrayList<EventData> arrayList, JSONObject jSONObject) {
        if (jSONObject == null) {
            this.mGeneralProperties = new JSONObject();
        } else {
            this.mGeneralProperties = jSONObject;
        }
        JSONArray jSONArray = new JSONArray();
        if (arrayList != null && !arrayList.isEmpty()) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                JSONObject createJSONForEvent = createJSONForEvent((EventData) it.next());
                if (createJSONForEvent != null) {
                    jSONArray.put(createJSONForEvent);
                }
            }
        }
        return createDataToSend(jSONArray);
    }
}
