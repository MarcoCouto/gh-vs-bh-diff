package com.ironsource.mediationsdk.events;

import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.SessionDepthManager;

public class InterstitialEventsManager extends BaseEventsManager {
    private static InterstitialEventsManager sInstance;
    private String mCurrentISPlacement;

    /* access modifiers changed from: protected */
    public boolean shouldExtractCurrentPlacement(EventData eventData) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean shouldIncludeCurrentPlacement(EventData eventData) {
        return false;
    }

    private InterstitialEventsManager() {
        this.mFormatterType = "ironbeast";
        this.mAdUnitType = 2;
        this.mEventType = IronSourceConstants.INTERSTITIAL_EVENT_TYPE;
        this.mCurrentISPlacement = "";
    }

    public static synchronized InterstitialEventsManager getInstance() {
        InterstitialEventsManager interstitialEventsManager;
        synchronized (InterstitialEventsManager.class) {
            if (sInstance == null) {
                sInstance = new InterstitialEventsManager();
                sInstance.initState();
            }
            interstitialEventsManager = sInstance;
        }
        return interstitialEventsManager;
    }

    /* access modifiers changed from: protected */
    public boolean isTriggerEvent(EventData eventData) {
        int eventId = eventData.getEventId();
        return eventId == 2204 || eventId == 2004 || eventId == 2005 || eventId == 2301 || eventId == 2300 || eventId == 3005 || eventId == 3015;
    }

    /* access modifiers changed from: protected */
    public int getSessionDepth(EventData eventData) {
        return SessionDepthManager.getInstance().getSessionDepth(eventData.getEventId() >= 3000 && eventData.getEventId() < 4000 ? 3 : 2);
    }

    /* access modifiers changed from: protected */
    public void increaseSessionDepthIfNeeded(EventData eventData) {
        if (eventData.getEventId() == 2204) {
            SessionDepthManager.getInstance().increaseSessionDepth(2);
        } else if (eventData.getEventId() == 3305) {
            SessionDepthManager.getInstance().increaseSessionDepth(3);
        }
    }

    /* access modifiers changed from: protected */
    public void setCurrentPlacement(EventData eventData) {
        this.mCurrentISPlacement = eventData.getAdditionalDataJSON().optString(IronSourceConstants.EVENTS_PLACEMENT_NAME);
    }

    /* access modifiers changed from: protected */
    public String getCurrentPlacement(int i) {
        return this.mCurrentISPlacement;
    }

    /* access modifiers changed from: protected */
    public void initConnectivitySensitiveEventsSet() {
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(2001));
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(2002));
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(2004));
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(IronSourceConstants.IS_INSTANCE_READY_TRUE));
        this.mConnectivitySensitiveEventsSet.add(Integer.valueOf(IronSourceConstants.IS_INSTANCE_READY_FALSE));
    }
}
