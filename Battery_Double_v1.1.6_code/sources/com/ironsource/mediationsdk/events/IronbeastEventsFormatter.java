package com.ironsource.mediationsdk.events;

import com.ironsource.eventsmodule.EventData;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class IronbeastEventsFormatter extends AbstractEventsFormatter {
    private final String DEFAULT_IB_EVENTS_URL = "https://outcome-ssp.supersonicads.com/mediation?adUnit=2";
    private final String IB_KEY_DATA = "data";
    private final String IB_KEY_TABLE = "table";
    private final String IB_TABLE_NAME = "super.dwh.mediation_events";

    public String getDefaultEventsUrl() {
        return "https://outcome-ssp.supersonicads.com/mediation?adUnit=2";
    }

    public String getFormatterType() {
        return "ironbeast";
    }

    IronbeastEventsFormatter(int i) {
        this.mAdUnit = i;
    }

    public String format(ArrayList<EventData> arrayList, JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        if (jSONObject == null) {
            this.mGeneralProperties = new JSONObject();
        } else {
            this.mGeneralProperties = jSONObject;
        }
        try {
            JSONArray jSONArray = new JSONArray();
            if (arrayList != null && !arrayList.isEmpty()) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    JSONObject createJSONForEvent = createJSONForEvent((EventData) it.next());
                    if (createJSONForEvent != null) {
                        jSONArray.put(createJSONForEvent);
                    }
                }
            }
            jSONObject2.put("table", "super.dwh.mediation_events");
            jSONObject2.put("data", createDataToSend(jSONArray));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject2.toString();
    }
}
