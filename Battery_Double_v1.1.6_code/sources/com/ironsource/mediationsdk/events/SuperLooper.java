package com.ironsource.mediationsdk.events;

import android.os.Handler;
import android.os.HandlerThread;
import com.ironsource.mediationsdk.logger.ThreadExceptionHandler;

public class SuperLooper extends Thread {
    private static SuperLooper mInstance;
    private SupersonicSdkThread mSdkThread = new SupersonicSdkThread(getClass().getSimpleName());

    private class SupersonicSdkThread extends HandlerThread {
        private Handler mHandler;

        SupersonicSdkThread(String str) {
            super(str);
            setUncaughtExceptionHandler(new ThreadExceptionHandler());
        }

        /* access modifiers changed from: 0000 */
        public void prepareHandler() {
            this.mHandler = new Handler(getLooper());
        }

        /* access modifiers changed from: 0000 */
        public Handler getCallbackHandler() {
            return this.mHandler;
        }
    }

    private SuperLooper() {
        this.mSdkThread.start();
        this.mSdkThread.prepareHandler();
    }

    public static synchronized SuperLooper getLooper() {
        SuperLooper superLooper;
        synchronized (SuperLooper.class) {
            if (mInstance == null) {
                mInstance = new SuperLooper();
            }
            superLooper = mInstance;
        }
        return superLooper;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0013, code lost:
        return;
     */
    public synchronized void post(Runnable runnable) {
        if (this.mSdkThread != null) {
            Handler callbackHandler = this.mSdkThread.getCallbackHandler();
            if (callbackHandler != null) {
                callbackHandler.post(runnable);
            }
        }
    }
}
