package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AuctionDataUtils.AuctionData;
import com.ironsource.mediationsdk.IronSource.AD_UNIT;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.sdk.DemandOnlyRvManagerListener;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;

class DemandOnlyRvManager implements DemandOnlyRvManagerListener {
    private String mAppKey;
    private AuctionSettings mAuctionSettings;
    private Context mContext;
    private ConcurrentHashMap<String, DemandOnlyRvSmash> mSmashes = new ConcurrentHashMap<>();

    DemandOnlyRvManager(Activity activity, List<ProviderSettings> list, RewardedVideoConfigurations rewardedVideoConfigurations, String str, String str2) {
        this.mAppKey = str;
        this.mContext = activity.getApplicationContext();
        this.mAuctionSettings = rewardedVideoConfigurations.getRewardedVideoAuctionSettings();
        for (ProviderSettings providerSettings : list) {
            if (providerSettings.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME) || providerSettings.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.IRONSOURCE_CONFIG_NAME)) {
                AbstractAdapter adapter = AdapterRepository.getInstance().getAdapter(providerSettings, providerSettings.getRewardedVideoSettings(), activity, true);
                if (adapter != null) {
                    DemandOnlyRvSmash demandOnlyRvSmash = new DemandOnlyRvSmash(activity, str, str2, providerSettings, this, rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadTimeout(), adapter);
                    this.mSmashes.put(providerSettings.getSubProviderId(), demandOnlyRvSmash);
                }
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("cannot load ");
                sb.append(providerSettings.getProviderTypeForReflection());
                logInternal(sb.toString());
            }
        }
    }

    public void loadRewardedVideoWithAdm(String str, String str2, boolean z) {
        try {
            if (!this.mSmashes.containsKey(str)) {
                sendMediationEvent(1500, str);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, ErrorBuilder.buildNonExistentInstanceError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                return;
            }
            DemandOnlyRvSmash demandOnlyRvSmash = (DemandOnlyRvSmash) this.mSmashes.get(str);
            if (z) {
                if (!demandOnlyRvSmash.isBidder()) {
                    IronSourceError buildLoadFailedError = ErrorBuilder.buildLoadFailedError("loadRewardedVideoWithAdm in IAB flow must be called by bidder instances");
                    logInternal(buildLoadFailedError.getErrorMessage());
                    RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, buildLoadFailedError);
                    sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, demandOnlyRvSmash);
                } else {
                    AuctionData auctionDataFromResponse = AuctionDataUtils.getInstance().getAuctionDataFromResponse(AuctionDataUtils.getInstance().decodeAdmResponse(str2));
                    AuctionResponseItem auctionResponseItem = AuctionDataUtils.getInstance().getAuctionResponseItem(demandOnlyRvSmash.getInstanceName(), auctionDataFromResponse.getWaterfall());
                    if (auctionResponseItem != null) {
                        demandOnlyRvSmash.setDynamicDemandSourceIdByServerData(auctionResponseItem.getServerData());
                        demandOnlyRvSmash.loadRewardedVideo(auctionResponseItem.getServerData(), auctionDataFromResponse.getAuctionId(), auctionResponseItem.getBurls());
                        sendProviderEvent(1001, demandOnlyRvSmash);
                    } else {
                        IronSourceError buildLoadFailedError2 = ErrorBuilder.buildLoadFailedError("loadRewardedVideoWithAdm invalid enriched adm");
                        logInternal(buildLoadFailedError2.getErrorMessage());
                        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, buildLoadFailedError2);
                        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, demandOnlyRvSmash);
                    }
                }
            } else if (!demandOnlyRvSmash.isBidder()) {
                sendProviderEvent(1001, demandOnlyRvSmash);
                demandOnlyRvSmash.loadRewardedVideo("", "", null);
            } else {
                IronSourceError buildLoadFailedError3 = ErrorBuilder.buildLoadFailedError("loadRewardedVideoWithAdm in non IAB flow must be called by non bidder instances");
                logInternal(buildLoadFailedError3.getErrorMessage());
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, buildLoadFailedError3);
                sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, demandOnlyRvSmash);
            }
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("loadRewardedVideoWithAdm exception ");
            sb.append(e.getMessage());
            logInternal(sb.toString());
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, ErrorBuilder.buildLoadFailedError("loadRewardedVideoWithAdm exception"));
        }
    }

    public String getBiddingData() {
        sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_GET_BIDDING_DATA_CALLED, null);
        if (this.mSmashes != null) {
            String biddingData = AdapterRepository.getInstance().getBiddingData(AD_UNIT.REWARDED_VIDEO, ((DemandOnlyRvSmash) ((Entry) this.mSmashes.entrySet().iterator().next()).getValue()).getAdapterConfig().getProviderSettings());
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(1);
            String sessionId = IronSourceObject.getInstance().getSessionId();
            if (biddingData != null) {
                HashMap hashMap = new HashMap();
                hashMap.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, biddingData);
                HashMap hashMap2 = new HashMap();
                hashMap2.put(IronSourceConstants.IRONSOURCE_CONFIG_NAME, hashMap);
                try {
                    new JSONObject();
                    return AuctionDataUtils.getInstance().encryptToken(AuctionDataUtils.getInstance().enrichToken(this.mContext, hashMap2, null, null, sessionDepth, sessionId, this.mAuctionSettings));
                } catch (JSONException unused) {
                    logInternal("getBiddingData() error during enrich token");
                    sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_GET_BIDDING_DATA_ENRICH_TOKEN_ERROR, null);
                    return null;
                }
            }
        }
        logInternal("called getBiddingData() with no smashes");
        sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_GET_BIDDING_DATA_RETURNED_NULL, null);
        return null;
    }

    public void showRewardedVideo(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(1500, str);
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, ErrorBuilder.buildNonExistentInstanceError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            return;
        }
        DemandOnlyRvSmash demandOnlyRvSmash = (DemandOnlyRvSmash) this.mSmashes.get(str);
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW, demandOnlyRvSmash);
        demandOnlyRvSmash.showRewardedVideo();
    }

    public boolean isRewardedVideoAvailable(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(1500, str);
            return false;
        }
        DemandOnlyRvSmash demandOnlyRvSmash = (DemandOnlyRvSmash) this.mSmashes.get(str);
        if (demandOnlyRvSmash.isRewardedVideoAvailable()) {
            sendProviderEvent(IronSourceConstants.RV_INSTANCE_READY_TRUE, demandOnlyRvSmash);
            return true;
        }
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_READY_FALSE, demandOnlyRvSmash);
        return false;
    }

    public void onRewardedVideoLoadSuccess(DemandOnlyRvSmash demandOnlyRvSmash, long j) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoLoadSuccess");
        sendProviderEvent(1002, demandOnlyRvSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoLoadSuccess(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdLoadFailed(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdLoadFailed error=");
        sb.append(ironSourceError);
        logSmashCallback(demandOnlyRvSmash, sb.toString());
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, demandOnlyRvSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{"duration", Long.valueOf(j)}});
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED_REASON, demandOnlyRvSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{"duration", Long.valueOf(j)}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(demandOnlyRvSmash.getSubProviderId(), ironSourceError);
    }

    public void onRewardedVideoAdOpened(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdOpened");
        sendProviderEvent(1005, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdOpened(demandOnlyRvSmash.getSubProviderId());
        if (demandOnlyRvSmash.isBidder()) {
            for (String str : demandOnlyRvSmash.mBUrl) {
                if (str != null) {
                    AuctionDataUtils.getInstance().sendResponse(str);
                }
            }
        }
    }

    public void onRewardedVideoAdClosed(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdClosed");
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_CLOSED, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdClosed(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash) {
        StringBuilder sb = new StringBuilder();
        sb.append("onRewardedVideoAdShowFailed error=");
        sb.append(ironSourceError);
        logSmashCallback(demandOnlyRvSmash, sb.toString());
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_FAILED, demandOnlyRvSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(demandOnlyRvSmash.getSubProviderId(), ironSourceError);
    }

    public void onRewardedVideoAdClicked(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdClicked");
        sendProviderEvent(1006, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdClicked(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdVisible(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdVisible");
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_VISIBLE, demandOnlyRvSmash);
    }

    public void onRewardedVideoAdRewarded(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdRewarded");
        Map providerEventData = demandOnlyRvSmash.getProviderEventData();
        if (!TextUtils.isEmpty(IronSourceObject.getInstance().getDynamicUserId())) {
            providerEventData.put(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceObject.getInstance().getDynamicUserId());
        }
        if (IronSourceObject.getInstance().getRvServerParams() != null) {
            for (String str : IronSourceObject.getInstance().getRvServerParams().keySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append("custom_");
                sb.append(str);
                providerEventData.put(sb.toString(), IronSourceObject.getInstance().getRvServerParams().get(str));
            }
        }
        Placement defaultRewardedVideoPlacement = IronSourceObject.getInstance().getCurrentServerResponse().getConfigurations().getRewardedVideoConfigurations().getDefaultRewardedVideoPlacement();
        if (defaultRewardedVideoPlacement != null) {
            providerEventData.put(IronSourceConstants.EVENTS_PLACEMENT_NAME, defaultRewardedVideoPlacement.getPlacementName());
            providerEventData.put(IronSourceConstants.EVENTS_REWARD_NAME, defaultRewardedVideoPlacement.getRewardName());
            providerEventData.put(IronSourceConstants.EVENTS_REWARD_AMOUNT, Integer.valueOf(defaultRewardedVideoPlacement.getRewardAmount()));
        } else {
            IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, "defaultPlacement is null", 3);
        }
        EventData eventData = new EventData(1010, new JSONObject(providerEventData));
        StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(Long.toString(eventData.getTimeStamp()));
        sb2.append(this.mAppKey);
        sb2.append(demandOnlyRvSmash.getInstanceName());
        eventData.addToAdditionalData(IronSourceConstants.EVENTS_TRANS_ID, IronSourceUtils.getTransId(sb2.toString()));
        RewardedVideoEventsManager.getInstance().log(eventData);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdRewarded(demandOnlyRvSmash.getSubProviderId());
    }

    private void sendProviderEvent(int i, DemandOnlyRvSmash demandOnlyRvSmash) {
        sendProviderEvent(i, demandOnlyRvSmash, null);
    }

    private void sendProviderEvent(int i, DemandOnlyRvSmash demandOnlyRvSmash, Object[][] objArr) {
        Map providerEventData = demandOnlyRvSmash.getProviderEventData();
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("RV sendProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }

    private void sendMediationEvent(int i, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_DEMAND_ONLY, Integer.valueOf(1));
        String str2 = "spId";
        if (str == null) {
            str = "";
        }
        hashMap.put(str2, str);
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyRvManager ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void logSmashCallback(DemandOnlyRvSmash demandOnlyRvSmash, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyRvManager ");
        sb.append(demandOnlyRvSmash.getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }
}
