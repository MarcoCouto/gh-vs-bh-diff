package com.ironsource.mediationsdk.metadata;

import com.ironsource.mediationsdk.metadata.MetaData.MetaDataValueTypes;

public class MetaDataUtils {
    public static MetaDataValueTypes getValueTypeForKey(String str) {
        if (str.equalsIgnoreCase(MetaDataConstants.META_DATA_CCPA_KEY)) {
            return MetaDataValueTypes.META_DATA_VALUE_BOOLEAN;
        }
        return MetaDataValueTypes.META_DATA_VALUE_STRING;
    }

    public static boolean isKnownKey(String str) {
        return str.equalsIgnoreCase(MetaDataConstants.META_DATA_CCPA_KEY);
    }

    public static String formatValueForType(String str, MetaDataValueTypes metaDataValueTypes) {
        if (metaDataValueTypes != MetaDataValueTypes.META_DATA_VALUE_BOOLEAN) {
            return str;
        }
        if (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("yes")) {
            return "true";
        }
        return (str.equalsIgnoreCase("false") || str.equalsIgnoreCase("no")) ? "false" : "";
    }

    public static MetaData formatMetaData(String str, String str2) {
        if (!isKnownKey(str)) {
            return new MetaData(str, str2);
        }
        String lowerCase = str.toLowerCase();
        MetaDataValueTypes valueTypeForKey = getValueTypeForKey(lowerCase);
        return new MetaData(lowerCase, formatValueForType(str2, valueTypeForKey), valueTypeForKey);
    }

    public static String checkMetaDataKeyValidity(String str) {
        return (str == null || str.length() > 64 || !str.matches("[A-Za-z0-9_\\-.]+")) ? "The MetaData key you entered is invalid. Please enter a key of maximum 64 characters that consists of only letters, digits and the following characters: . - _" : "";
    }

    public static String checkMetaDataValueValidity(String str) {
        return (str == null || str.length() > 64 || !str.matches("[A-Za-z0-9_\\-.]+")) ? "The MetaData value you entered is invalid. Please enter a value of maximum 64 characters that consists of only letters, digits and the following characters: . - _" : "";
    }

    public static boolean isValidCCPAMetaData(String str, String str2) {
        return str.equals(MetaDataConstants.META_DATA_CCPA_KEY) && str2.length() > 0;
    }

    public static boolean getCCPABooleanValue(String str) {
        return !str.equals("false");
    }
}
