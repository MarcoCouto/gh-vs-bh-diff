package com.ironsource.mediationsdk.metadata;

public class MetaData {
    private String mKey;
    private String mValue;
    private MetaDataValueTypes mValueType;

    public enum MetaDataValueTypes {
        META_DATA_VALUE_STRING,
        META_DATA_VALUE_BOOLEAN,
        META_DATA_VALUE_INT,
        META_DATA_VALUE_LONG,
        META_DATA_VALUE_DOUBLE,
        META_DATA_VALUE_FLOAT
    }

    public MetaData(String str, String str2, MetaDataValueTypes metaDataValueTypes) {
        this.mKey = str;
        this.mValue = str2;
        this.mValueType = metaDataValueTypes;
    }

    public MetaData(String str, String str2) {
        this.mKey = str;
        this.mValue = str2;
        this.mValueType = MetaDataValueTypes.META_DATA_VALUE_STRING;
    }

    public String getMetaDataKey() {
        return this.mKey;
    }

    public String getMetaDataValue() {
        return this.mValue;
    }

    public MetaDataValueTypes getMetaDataValueType() {
        return this.mValueType;
    }
}
