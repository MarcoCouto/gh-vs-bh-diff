package com.ironsource.mediationsdk.metadata;

public class MetaDataConstants {
    public static final String META_DATA_CCPA_KEY = "do_not_sell";
    protected static final String META_DATA_FALSE_VALUE = "false";
    protected static final String META_DATA_TRUE_VALUE = "true";
}
