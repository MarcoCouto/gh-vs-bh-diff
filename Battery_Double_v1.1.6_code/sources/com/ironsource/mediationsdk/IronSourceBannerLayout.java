package com.ironsource.mediationsdk;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.BannerListener;

public class IronSourceBannerLayout extends FrameLayout {
    private boolean isDestroyed = false;
    private Activity mActivity;
    /* access modifiers changed from: private */
    public BannerListener mBannerListener;
    /* access modifiers changed from: private */
    public View mBannerView;
    /* access modifiers changed from: private */
    public boolean mIsBannerDisplayed = false;
    private String mPlacementName;
    private ISBannerSize mSize;

    public IronSourceBannerLayout(Activity activity, ISBannerSize iSBannerSize) {
        super(activity);
        this.mActivity = activity;
        if (iSBannerSize == null) {
            iSBannerSize = ISBannerSize.BANNER;
        }
        this.mSize = iSBannerSize;
    }

    /* access modifiers changed from: protected */
    public void destroyBanner() {
        this.isDestroyed = true;
        this.mBannerListener = null;
        this.mActivity = null;
        this.mSize = null;
        this.mPlacementName = null;
        this.mBannerView = null;
    }

    public boolean isDestroyed() {
        return this.isDestroyed;
    }

    public View getBannerView() {
        return this.mBannerView;
    }

    public Activity getActivity() {
        return this.mActivity;
    }

    public ISBannerSize getSize() {
        return this.mSize;
    }

    public String getPlacementName() {
        return this.mPlacementName;
    }

    public void setPlacementName(String str) {
        this.mPlacementName = str;
    }

    public void setBannerListener(BannerListener bannerListener) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.API, "setBannerListener()", 1);
        this.mBannerListener = bannerListener;
    }

    public void removeBannerListener() {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.API, "removeBannerListener()", 1);
        this.mBannerListener = null;
    }

    public BannerListener getBannerListener() {
        return this.mBannerListener;
    }

    /* access modifiers changed from: 0000 */
    public void sendBannerAdLoaded(BannerSmash bannerSmash) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("onBannerAdLoaded() | internal | adapter: ");
        sb.append(bannerSmash.getName());
        logger.log(ironSourceTag, sb.toString(), 0);
        if (this.mBannerListener != null && !this.mIsBannerDisplayed) {
            IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onBannerAdLoaded()", 1);
            this.mBannerListener.onBannerAdLoaded();
        }
        this.mIsBannerDisplayed = true;
    }

    /* access modifiers changed from: 0000 */
    public void sendBannerAdLoadFailed(final IronSourceError ironSourceError) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append("onBannerAdLoadFailed()  error=");
        sb.append(ironSourceError);
        logger.log(ironSourceTag, sb.toString(), 1);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (IronSourceBannerLayout.this.mIsBannerDisplayed) {
                    IronSourceBannerLayout.this.mBannerListener.onBannerAdLoadFailed(ironSourceError);
                    return;
                }
                try {
                    if (IronSourceBannerLayout.this.mBannerView != null) {
                        IronSourceBannerLayout.this.removeView(IronSourceBannerLayout.this.mBannerView);
                        IronSourceBannerLayout.this.mBannerView = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (IronSourceBannerLayout.this.mBannerListener != null) {
                    IronSourceBannerLayout.this.mBannerListener.onBannerAdLoadFailed(ironSourceError);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void sendBannerAdClicked() {
        if (this.mBannerListener != null) {
            IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onBannerAdClicked()", 1);
            this.mBannerListener.onBannerAdClicked();
        }
    }

    /* access modifiers changed from: 0000 */
    public void sendBannerAdScreenPresented() {
        if (this.mBannerListener != null) {
            IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onBannerAdScreenPresented()", 1);
            this.mBannerListener.onBannerAdScreenPresented();
        }
    }

    /* access modifiers changed from: 0000 */
    public void sendBannerAdScreenDismissed() {
        if (this.mBannerListener != null) {
            IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onBannerAdScreenDismissed()", 1);
            this.mBannerListener.onBannerAdScreenDismissed();
        }
    }

    /* access modifiers changed from: 0000 */
    public void sendBannerAdLeftApplication() {
        if (this.mBannerListener != null) {
            IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, "onBannerAdLeftApplication()", 1);
            this.mBannerListener.onBannerAdLeftApplication();
        }
    }

    /* access modifiers changed from: 0000 */
    public void addViewWithFrameLayoutParams(final View view, final LayoutParams layoutParams) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                IronSourceBannerLayout.this.removeAllViews();
                ViewParent parent = view.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(view);
                }
                IronSourceBannerLayout.this.mBannerView = view;
                IronSourceBannerLayout.this.addView(view, 0, layoutParams);
            }
        });
    }
}
