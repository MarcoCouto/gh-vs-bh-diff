package com.ironsource.mediationsdk;

import com.appodeal.ads.AppodealNetworks;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.smaato.sdk.core.api.VideoType;
import io.realm.BuildConfig;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AdaptersCompatibilityHandler {
    private static final AdaptersCompatibilityHandler instance = new AdaptersCompatibilityHandler();
    private final ConcurrentHashMap<String, String> mAdapterNameToMinIsVersion = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, String> mAdapterNameToMinRvVersion;

    public static AdaptersCompatibilityHandler getInstance() {
        return instance;
    }

    private AdaptersCompatibilityHandler() {
        this.mAdapterNameToMinIsVersion.put(AppodealNetworks.ADCOLONY, "4.1.6");
        this.mAdapterNameToMinIsVersion.put(AppodealNetworks.VUNGLE, "4.1.5");
        this.mAdapterNameToMinIsVersion.put(AppodealNetworks.APPLOVIN, "4.3.3");
        this.mAdapterNameToMinIsVersion.put("admob", "4.3.2");
        this.mAdapterNameToMinRvVersion = new ConcurrentHashMap<>();
        this.mAdapterNameToMinRvVersion.put(AppodealNetworks.ADCOLONY, "4.1.6");
        this.mAdapterNameToMinRvVersion.put("admob", "4.3.2");
        this.mAdapterNameToMinRvVersion.put(AppodealNetworks.APPLOVIN, "4.3.3");
        this.mAdapterNameToMinRvVersion.put(AppodealNetworks.CHARTBOOST, "4.1.9");
        this.mAdapterNameToMinRvVersion.put("fyber", BuildConfig.VERSION_NAME);
        this.mAdapterNameToMinRvVersion.put("hyprmx", "4.1.2");
        this.mAdapterNameToMinRvVersion.put("inmobi", "4.3.1");
        this.mAdapterNameToMinRvVersion.put("maio", "4.1.3");
        this.mAdapterNameToMinRvVersion.put("mediabrix", "4.1.1");
        this.mAdapterNameToMinRvVersion.put("mopub", "3.2.0");
        this.mAdapterNameToMinRvVersion.put(AppodealNetworks.TAPJOY, "4.0.0");
        this.mAdapterNameToMinRvVersion.put("unityads", "4.1.4");
        this.mAdapterNameToMinRvVersion.put(AppodealNetworks.VUNGLE, "4.1.5");
    }

    public boolean isAdapterVersionRVCompatible(AbstractAdapter abstractAdapter) {
        return isAdapterVersionCompatible(abstractAdapter, this.mAdapterNameToMinRvVersion, "rewarded video");
    }

    public boolean isAdapterVersionISCompatible(AbstractAdapter abstractAdapter) {
        return isAdapterVersionCompatible(abstractAdapter, this.mAdapterNameToMinIsVersion, VideoType.INTERSTITIAL);
    }

    private boolean isAdapterVersionCompatible(AbstractAdapter abstractAdapter, Map<String, String> map, String str) {
        if (abstractAdapter == null) {
            return false;
        }
        String lowerCase = abstractAdapter.getProviderName().toLowerCase();
        if (!map.containsKey(lowerCase)) {
            return true;
        }
        String str2 = (String) map.get(lowerCase);
        String version = abstractAdapter.getVersion();
        boolean isVersionGreaterOrEqual = isVersionGreaterOrEqual(str2, version);
        if (!isVersionGreaterOrEqual) {
            StringBuilder sb = new StringBuilder();
            sb.append(abstractAdapter.getProviderName());
            sb.append(" adapter ");
            sb.append(version);
            sb.append(" is incompatible with SDK version ");
            sb.append(IronSourceUtils.getSDKVersion());
            sb.append(" for ");
            sb.append(str);
            sb.append(" ad unit, please update your adapter to the latest version");
            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, sb.toString(), 3);
        }
        return isVersionGreaterOrEqual;
    }

    public boolean isBannerAdapterCompatible(AbstractAdapter abstractAdapter) {
        if (abstractAdapter == null) {
            return false;
        }
        String version = abstractAdapter.getVersion();
        boolean isVersionGreaterOrEqual = isVersionGreaterOrEqual("4.3.0", version);
        if (!isVersionGreaterOrEqual) {
            StringBuilder sb = new StringBuilder();
            sb.append(abstractAdapter.getProviderName());
            sb.append(" adapter ");
            sb.append(version);
            sb.append(" is incompatible with SDK version ");
            sb.append(IronSourceUtils.getSDKVersion());
            sb.append(", please update your adapter to the latest version");
            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, sb.toString(), 3);
        }
        return isVersionGreaterOrEqual;
    }

    private boolean isVersionGreaterOrEqual(String str, String str2) {
        if (str.equalsIgnoreCase(str2)) {
            return true;
        }
        String[] split = str.split("\\.");
        String[] split2 = str2.split("\\.");
        for (int i = 0; i < 3; i++) {
            int parseInt = Integer.parseInt(split[i]);
            int parseInt2 = Integer.parseInt(split2[i]);
            if (parseInt2 < parseInt) {
                return false;
            }
            if (parseInt2 > parseInt) {
                return true;
            }
        }
        return true;
    }
}
