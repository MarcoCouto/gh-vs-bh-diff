package com.ironsource.mediationsdk;

import java.util.List;

/* compiled from: AuctionHandler */
interface AuctionEventListener {
    void onAuctionFailed(int i, String str, int i2, String str2, long j);

    void onAuctionSuccess(List<AuctionResponseItem> list, String str, int i, long j);
}
