package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyIsManagerListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.sdk.constants.Constants.JSMethods;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class DemandOnlyIsSmash extends DemandOnlySmash implements InterstitialSmashListener {
    /* access modifiers changed from: private */
    public DemandOnlyIsManagerListener mListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;

    public void onInterstitialAdShowSucceeded() {
    }

    public void onInterstitialInitFailed(IronSourceError ironSourceError) {
    }

    public void onInterstitialInitSuccess() {
    }

    public DemandOnlyIsSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, DemandOnlyIsManagerListener demandOnlyIsManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getInterstitialSettings()), abstractAdapter);
        this.mListener = demandOnlyIsManagerListener;
        this.mLoadTimeoutSecs = i;
        this.mAdapter.initInterstitial(activity, str, str2, this.mAdUnitSettings, this);
    }

    public void loadInterstitial(String str, String str2, List<String> list) {
        StringBuilder sb = new StringBuilder();
        sb.append("loadInterstitial state=");
        sb.append(getStateString());
        logInternal(sb.toString());
        SMASH_STATE compareAndSetState = compareAndSetState(new SMASH_STATE[]{SMASH_STATE.NOT_LOADED, SMASH_STATE.LOADED}, SMASH_STATE.LOAD_IN_PROGRESS);
        if (compareAndSetState == SMASH_STATE.NOT_LOADED || compareAndSetState == SMASH_STATE.LOADED) {
            this.mLoadStartTime = new Date().getTime();
            startLoadTimer();
            if (isBidder()) {
                this.mAuctionId = str2;
                this.mBUrl = list;
                this.mAdapter.loadInterstitial(this.mAdUnitSettings, this, str);
                return;
            }
            this.mAdapter.loadInterstitial(this.mAdUnitSettings, this);
        } else if (compareAndSetState == SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_ALREADY_IN_PROGRESS, "load already in progress"), this, 0);
        } else {
            this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_ALREADY_IN_PROGRESS, "cannot load because show is in progress"), this, 0);
        }
    }

    public void showInterstitial() {
        StringBuilder sb = new StringBuilder();
        sb.append("showInterstitial state=");
        sb.append(getStateString());
        logInternal(sb.toString());
        if (compareAndSetState(SMASH_STATE.LOADED, SMASH_STATE.SHOW_IN_PROGRESS)) {
            this.mAdapter.showInterstitial(this.mAdUnitSettings, this);
            return;
        }
        this.mListener.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_CALL_LOAD_BEFORE_SHOW, "load must be called before show"), this);
    }

    public boolean isInterstitialReady() {
        return this.mAdapter.isInterstitialReady(this.mAdUnitSettings);
    }

    private void startLoadTimer() {
        logInternal("start timer");
        startTimer(new TimerTask() {
            public void run() {
                DemandOnlyIsSmash demandOnlyIsSmash = DemandOnlyIsSmash.this;
                StringBuilder sb = new StringBuilder();
                sb.append("load timed out state=");
                sb.append(DemandOnlyIsSmash.this.getStateString());
                demandOnlyIsSmash.logInternal(sb.toString());
                if (DemandOnlyIsSmash.this.compareAndSetState(SMASH_STATE.LOAD_IN_PROGRESS, SMASH_STATE.NOT_LOADED)) {
                    DemandOnlyIsSmash.this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_TIMED_OUT, "load timed out"), DemandOnlyIsSmash.this, new Date().getTime() - DemandOnlyIsSmash.this.mLoadStartTime);
                }
            }
        });
    }

    public void onInterstitialAdReady() {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdReady state=");
        sb.append(getStateString());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (compareAndSetState(SMASH_STATE.LOAD_IN_PROGRESS, SMASH_STATE.LOADED)) {
            this.mListener.onInterstitialAdReady(this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onInterstitialAdLoadFailed(IronSourceError ironSourceError) {
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdLoadFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        sb.append(" state=");
        sb.append(getStateString());
        logAdapterCallback(sb.toString());
        stopTimer();
        if (compareAndSetState(SMASH_STATE.LOAD_IN_PROGRESS, SMASH_STATE.NOT_LOADED)) {
            this.mListener.onInterstitialAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onInterstitialAdOpened() {
        logAdapterCallback("onInterstitialAdOpened");
        this.mListener.onInterstitialAdOpened(this);
    }

    public void onInterstitialAdClosed() {
        setState(SMASH_STATE.NOT_LOADED);
        logAdapterCallback("onInterstitialAdClosed");
        this.mListener.onInterstitialAdClosed(this);
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError) {
        setState(SMASH_STATE.NOT_LOADED);
        StringBuilder sb = new StringBuilder();
        sb.append("onInterstitialAdShowFailed error=");
        sb.append(ironSourceError.getErrorMessage());
        logAdapterCallback(sb.toString());
        this.mListener.onInterstitialAdShowFailed(ironSourceError, this);
    }

    public void onInterstitialAdClicked() {
        logAdapterCallback(JSMethods.ON_INTERSTITIAL_AD_CLICKED);
        this.mListener.onInterstitialAdClicked(this);
    }

    public void onInterstitialAdVisible() {
        logAdapterCallback("onInterstitialAdVisible");
        this.mListener.onInterstitialAdVisible(this);
    }

    private void logAdapterCallback(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyInterstitialSmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.ADAPTER_CALLBACK, sb.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("DemandOnlyInterstitialSmash ");
        sb.append(this.mAdapterConfig.getProviderName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }
}
