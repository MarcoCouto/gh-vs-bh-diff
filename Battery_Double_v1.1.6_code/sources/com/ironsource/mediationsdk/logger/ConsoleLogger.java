package com.ironsource.mediationsdk.logger;

import android.util.Log;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.sdk.constants.Constants.RequestParameters;

public class ConsoleLogger extends IronSourceLogger {
    public static final String CONSOLE_PREFIX = "ironSourceSDK: ";
    public static final String NAME = "console";

    private ConsoleLogger() {
        super(NAME);
    }

    public ConsoleLogger(int i) {
        super(NAME, i);
    }

    public void log(IronSourceTag ironSourceTag, String str, int i) {
        switch (i) {
            case 0:
                StringBuilder sb = new StringBuilder();
                sb.append(CONSOLE_PREFIX);
                sb.append(ironSourceTag);
                Log.v(sb.toString(), str);
                return;
            case 1:
                StringBuilder sb2 = new StringBuilder();
                sb2.append(CONSOLE_PREFIX);
                sb2.append(ironSourceTag);
                Log.i(sb2.toString(), str);
                return;
            case 2:
                StringBuilder sb3 = new StringBuilder();
                sb3.append(CONSOLE_PREFIX);
                sb3.append(ironSourceTag);
                Log.w(sb3.toString(), str);
                return;
            case 3:
                StringBuilder sb4 = new StringBuilder();
                sb4.append(CONSOLE_PREFIX);
                sb4.append(ironSourceTag);
                Log.e(sb4.toString(), str);
                return;
            default:
                return;
        }
    }

    public void logException(IronSourceTag ironSourceTag, String str, Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(":stacktrace[");
        sb.append(Log.getStackTraceString(th));
        sb.append(RequestParameters.RIGHT_BRACKETS);
        log(ironSourceTag, sb.toString(), 3);
    }
}
