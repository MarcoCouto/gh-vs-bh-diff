package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AuctionHistory.ISAuctionPerformance;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionCappingManager;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.constants.Constants.JSMethods;
import com.smaato.sdk.core.api.VideoType;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

class ProgIsManager implements ProgIsManagerListener, AuctionEventListener {
    /* access modifiers changed from: private */
    public Context mAppContext;
    private String mAuctionFallback = "";
    /* access modifiers changed from: private */
    public AuctionHandler mAuctionHandler;
    /* access modifiers changed from: private */
    public AuctionHistory mAuctionHistory;
    private int mAuctionTrial;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId;
    private String mCurrentPlacement;
    /* access modifiers changed from: private */
    public long mInitMangerTime;
    private boolean mIsAuctionEnabled;
    private long mLoadStartTime;
    private int mMaxSmashesToLoad;
    /* access modifiers changed from: private */
    public SessionCappingManager mSessionCappingManager;
    boolean mShouldTrackNetworkState = false;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, ProgIsSmash> mSmashes;
    private MEDIATION_STATE mState;
    /* access modifiers changed from: private */
    public long mTimeToWaitBeforeFirstAction;
    private CopyOnWriteArrayList<ProgIsSmash> mWaterfall;
    private ConcurrentHashMap<String, ISAuctionPerformance> mWaterfallPerformance;
    private ConcurrentHashMap<String, AuctionResponseItem> mWaterfallServerData;

    enum MEDIATION_STATE {
        STATE_NOT_INITIALIZED,
        STATE_READY_TO_LOAD,
        STATE_AUCTION,
        STATE_LOADING_SMASHES,
        STATE_READY_TO_SHOW,
        STATE_SHOWING
    }

    private boolean shouldAddAuctionParams(int i) {
        return i == 2002 || i == 2003 || i == 2200 || i == 2005 || i == 2204 || i == 2201 || i == 2203 || i == 2006 || i == 2004 || i == 2110 || i == 2301 || i == 2300;
    }

    public ProgIsManager(Activity activity, List<ProviderSettings> list, InterstitialConfigurations interstitialConfigurations, String str, String str2, int i) {
        long time = new Date().getTime();
        sendMediationEvent(IronSourceConstants.IS_MANAGER_INIT_STARTED);
        setState(MEDIATION_STATE.STATE_NOT_INITIALIZED);
        this.mSmashes = new ConcurrentHashMap<>();
        this.mWaterfall = new CopyOnWriteArrayList<>();
        this.mWaterfallServerData = new ConcurrentHashMap<>();
        this.mWaterfallPerformance = new ConcurrentHashMap<>();
        this.mCurrentPlacement = "";
        this.mCurrentAuctionId = "";
        this.mAppContext = activity.getApplicationContext();
        this.mMaxSmashesToLoad = interstitialConfigurations.getInterstitialAdaptersSmartLoadAmount();
        CallbackThrottler.getInstance().setDelayLoadFailureNotificationInSeconds(i);
        AuctionSettings interstitialAuctionSettings = interstitialConfigurations.getInterstitialAuctionSettings();
        this.mTimeToWaitBeforeFirstAction = interstitialAuctionSettings.getTimeToWaitBeforeFirstAuctionMs();
        this.mIsAuctionEnabled = interstitialAuctionSettings.getNumOfMaxTrials() > 0;
        if (this.mIsAuctionEnabled) {
            this.mAuctionHandler = new AuctionHandler(VideoType.INTERSTITIAL, interstitialAuctionSettings, this);
        }
        ArrayList arrayList = new ArrayList();
        for (ProviderSettings providerSettings : list) {
            AbstractAdapter adapter = AdapterRepository.getInstance().getAdapter(providerSettings, providerSettings.getInterstitialSettings(), activity);
            if (adapter != null && AdaptersCompatibilityHandler.getInstance().isAdapterVersionISCompatible(adapter)) {
                ProgIsSmash progIsSmash = r0;
                ProgIsSmash progIsSmash2 = new ProgIsSmash(activity, str, str2, providerSettings, this, interstitialConfigurations.getInterstitialAdaptersSmartLoadTimeout(), adapter);
                String instanceName = progIsSmash.getInstanceName();
                this.mSmashes.put(instanceName, progIsSmash);
                arrayList.add(instanceName);
            }
        }
        this.mAuctionHistory = new AuctionHistory(arrayList, interstitialAuctionSettings.getAuctionSavedHistoryLimit());
        this.mSessionCappingManager = new SessionCappingManager(new ArrayList(this.mSmashes.values()));
        for (ProgIsSmash progIsSmash3 : this.mSmashes.values()) {
            if (progIsSmash3.isBidder()) {
                progIsSmash3.initForBidding();
            }
        }
        this.mInitMangerTime = new Date().getTime();
        setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
        sendMediationEvent(IronSourceConstants.IS_MANAGER_INIT_ENDED, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - time)}});
    }

    /* access modifiers changed from: private */
    public void makeAuction() {
        setState(MEDIATION_STATE.STATE_AUCTION);
        AsyncTask.execute(new Runnable() {
            public void run() {
                ProgIsManager.this.mCurrentAuctionId = "";
                StringBuilder sb = new StringBuilder();
                long access$200 = ProgIsManager.this.mTimeToWaitBeforeFirstAction - (new Date().getTime() - ProgIsManager.this.mInitMangerTime);
                if (access$200 > 0) {
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        public void run() {
                            ProgIsManager.this.makeAuction();
                        }
                    }, access$200);
                    return;
                }
                ProgIsManager.this.sendMediationEvent(2000, null);
                HashMap hashMap = new HashMap();
                ArrayList arrayList = new ArrayList();
                for (ProgIsSmash progIsSmash : ProgIsManager.this.mSmashes.values()) {
                    if (!ProgIsManager.this.mSessionCappingManager.isCapped(progIsSmash)) {
                        if (progIsSmash.isBidder()) {
                            Map biddingData = progIsSmash.getBiddingData();
                            if (biddingData != null) {
                                hashMap.put(progIsSmash.getInstanceName(), biddingData);
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("2");
                                sb2.append(progIsSmash.getInstanceName());
                                sb2.append(",");
                                sb.append(sb2.toString());
                            }
                        } else if (!progIsSmash.isBidder()) {
                            arrayList.add(progIsSmash.getInstanceName());
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("1");
                            sb3.append(progIsSmash.getInstanceName());
                            sb3.append(",");
                            sb.append(sb3.toString());
                        }
                    }
                }
                if (hashMap.size() == 0 && arrayList.size() == 0) {
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1005)}, new Object[]{"duration", Integer.valueOf(0)}});
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(1005, "No candidates available for auctioning"));
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1005)}});
                    ProgIsManager.this.setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
                    return;
                }
                if (sb.length() > 256) {
                    sb.setLength(256);
                } else if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_AUCTION_REQUEST_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
                int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(2);
                if (ProgIsManager.this.mAuctionHandler != null) {
                    ProgIsManager.this.mAuctionHandler.executeAuction(ProgIsManager.this.mAppContext, hashMap, arrayList, ProgIsManager.this.mAuctionHistory, sessionDepth);
                }
            }
        });
    }

    public void onAuctionSuccess(List<AuctionResponseItem> list, String str, int i, long j) {
        this.mCurrentAuctionId = str;
        this.mAuctionTrial = i;
        this.mAuctionFallback = "";
        sendMediationEvent(IronSourceConstants.IS_AUCTION_SUCCESS, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        updateWaterfall(list);
        loadSmashes();
    }

    public void onAuctionFailed(int i, String str, int i2, String str2, long j) {
        logInternal("Auction failed | moving to fallback waterfall");
        this.mAuctionTrial = i2;
        this.mAuctionFallback = str2;
        if (TextUtils.isEmpty(str)) {
            sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{"duration", Long.valueOf(j)}});
        } else {
            sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str}, new Object[]{"duration", Long.valueOf(j)}});
        }
        updateWaterfallToNonBidding();
        loadSmashes();
    }

    private void updateWaterfallToNonBidding() {
        updateWaterfall(extractNonBidderProvidersFromWaterfall());
    }

    private List<AuctionResponseItem> extractNonBidderProvidersFromWaterfall() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (ProgIsSmash progIsSmash : this.mSmashes.values()) {
            if (!progIsSmash.isBidder() && !this.mSessionCappingManager.isCapped(progIsSmash)) {
                copyOnWriteArrayList.add(new AuctionResponseItem(progIsSmash.getInstanceName()));
            }
        }
        return copyOnWriteArrayList;
    }

    private String getAsString(AuctionResponseItem auctionResponseItem) {
        ProgIsSmash progIsSmash = (ProgIsSmash) this.mSmashes.get(auctionResponseItem.getInstanceName());
        String str = progIsSmash != null ? progIsSmash.isBidder() ? "2" : "1" : TextUtils.isEmpty(auctionResponseItem.getServerData()) ? "1" : "2";
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(auctionResponseItem.getInstanceName());
        return sb.toString();
    }

    private void updateWaterfall(List<AuctionResponseItem> list) {
        this.mWaterfall.clear();
        this.mWaterfallServerData.clear();
        this.mWaterfallPerformance.clear();
        StringBuilder sb = new StringBuilder();
        for (AuctionResponseItem auctionResponseItem : list) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(getAsString(auctionResponseItem));
            sb2.append(",");
            sb.append(sb2.toString());
            ProgIsSmash progIsSmash = (ProgIsSmash) this.mSmashes.get(auctionResponseItem.getInstanceName());
            if (progIsSmash != null) {
                progIsSmash.setIsLoadCandidate(true);
                this.mWaterfall.add(progIsSmash);
                this.mWaterfallServerData.put(progIsSmash.getInstanceName(), auctionResponseItem);
                this.mWaterfallPerformance.put(auctionResponseItem.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceDidntAttemptToLoad);
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("updateWaterfall() - could not find matching smash for auction response item ");
                sb3.append(auctionResponseItem.getInstanceName());
                logInternal(sb3.toString());
            }
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append("updateWaterfall() - response waterfall is ");
        sb4.append(sb.toString());
        logInternal(sb4.toString());
        if (sb.length() > 256) {
            sb.setLength(256);
        } else if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        } else {
            logInternal("Updated waterfall is empty");
        }
        sendMediationEvent(IronSourceConstants.IS_RESULT_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
    }

    private void loadSmashes() {
        if (this.mWaterfall.isEmpty()) {
            setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_IS_LOAD_FAILED_NO_CANDIDATES)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "Empty waterfall"}});
            CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_IS_LOAD_FAILED_NO_CANDIDATES, "Empty waterfall"));
            return;
        }
        setState(MEDIATION_STATE.STATE_LOADING_SMASHES);
        for (int i = 0; i < Math.min(this.mMaxSmashesToLoad, this.mWaterfall.size()); i++) {
            ProgIsSmash progIsSmash = (ProgIsSmash) this.mWaterfall.get(i);
            String serverData = ((AuctionResponseItem) this.mWaterfallServerData.get(progIsSmash.getInstanceName())).getServerData();
            progIsSmash.setDynamicDemandSourceIdByServerData(serverData);
            sendProviderEvent(2002, progIsSmash);
            progIsSmash.loadInterstitial(serverData);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x007b, code lost:
        return;
     */
    public synchronized void loadInterstitial() {
        if (this.mState == MEDIATION_STATE.STATE_SHOWING) {
            String str = "loadInterstitial: load cannot be invoked while showing an ad";
            IronSourceLoggerManager.getLogger().log(IronSourceTag.API, str, 3);
            ISListenerWrapper.getInstance().onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_IS_LOAD_DURING_SHOW, str));
        } else if ((this.mState == MEDIATION_STATE.STATE_READY_TO_LOAD || this.mState == MEDIATION_STATE.STATE_READY_TO_SHOW) && !CallbackThrottler.getInstance().hasPendingInvocation()) {
            this.mCurrentAuctionId = "";
            this.mCurrentPlacement = "";
            sendMediationEvent(2001);
            this.mLoadStartTime = new Date().getTime();
            if (this.mIsAuctionEnabled) {
                if (!this.mWaterfallPerformance.isEmpty()) {
                    this.mAuctionHistory.storeWaterfallPerformance(this.mWaterfallPerformance);
                    this.mWaterfallPerformance.clear();
                }
                makeAuction();
            } else {
                updateWaterfallToNonBidding();
                loadSmashes();
            }
        } else {
            logInternal("loadInterstitial: load is already in progress");
        }
    }

    public synchronized void showInterstitial(String str) {
        if (this.mState == MEDIATION_STATE.STATE_SHOWING) {
            String str2 = "showInterstitial error: can't show ad while an ad is already showing";
            logAPIError(str2);
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_IS_SHOW_CALLED_DURING_SHOW, str2));
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_IS_SHOW_CALLED_DURING_SHOW)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str2}});
        } else if (this.mState != MEDIATION_STATE.STATE_READY_TO_SHOW) {
            StringBuilder sb = new StringBuilder();
            sb.append("showInterstitial() error state=");
            sb.append(this.mState.toString());
            logInternal(sb.toString());
            String str3 = "showInterstitial error: show called while no ads are available";
            logAPIError(str3);
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, str3));
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str3}});
        } else if (str == null) {
            String str4 = "showInterstitial error: empty default placement";
            logAPIError(str4);
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(1020, str4));
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1020)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str4}});
        } else {
            this.mCurrentPlacement = str;
            sendMediationEventWithPlacement(2100);
            if (CappingManager.isInterstitialPlacementCapped(this.mAppContext, this.mCurrentPlacement)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("placement ");
                sb2.append(this.mCurrentPlacement);
                sb2.append(" is capped");
                String sb3 = sb2.toString();
                logAPIError(sb3);
                ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT, sb3));
                sendMediationEventWithPlacement(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb3}});
                return;
            }
            Iterator it = this.mWaterfall.iterator();
            while (it.hasNext()) {
                ProgIsSmash progIsSmash = (ProgIsSmash) it.next();
                if (progIsSmash.isReadyToShow()) {
                    showInterstitial(progIsSmash, this.mCurrentPlacement);
                    return;
                }
                StringBuilder sb4 = new StringBuilder();
                sb4.append("showInterstitial ");
                sb4.append(progIsSmash.getInstanceName());
                sb4.append(" isReadyToShow() == false");
                logInternal(sb4.toString());
            }
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
            sendMediationEventWithPlacement(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "Show Fail - No ads to show"}});
        }
    }

    private void showInterstitial(ProgIsSmash progIsSmash, String str) {
        setState(MEDIATION_STATE.STATE_SHOWING);
        progIsSmash.showInterstitial();
        sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW, progIsSmash);
        this.mSessionCappingManager.increaseShowCounter(progIsSmash);
        if (this.mSessionCappingManager.isCapped(progIsSmash)) {
            progIsSmash.setCappedPerSession();
            sendProviderEvent(IronSourceConstants.IS_CAP_SESSION, progIsSmash);
            StringBuilder sb = new StringBuilder();
            sb.append(progIsSmash.getInstanceName());
            sb.append(" was session capped");
            IronSourceUtils.sendAutomationLog(sb.toString());
        }
        CappingManager.incrementIsShowCounter(this.mAppContext, str);
        if (CappingManager.isInterstitialPlacementCapped(this.mAppContext, str)) {
            sendMediationEventWithPlacement(IronSourceConstants.IS_CAP_PLACEMENT);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return false;
     */
    public synchronized boolean isInterstitialReady() {
        if ((!this.mShouldTrackNetworkState || IronSourceUtils.isNetworkConnected(this.mAppContext)) && this.mState == MEDIATION_STATE.STATE_READY_TO_SHOW) {
            Iterator it = this.mWaterfall.iterator();
            while (it.hasNext()) {
                if (((ProgIsSmash) it.next()).isReadyToShow()) {
                    return true;
                }
            }
            return false;
        }
    }

    public void onInterstitialAdReady(ProgIsSmash progIsSmash, long j) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdReady");
            sendProviderEvent(2003, progIsSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
            if (progIsSmash != null && this.mWaterfallPerformance.containsKey(progIsSmash.getInstanceName())) {
                this.mWaterfallPerformance.put(progIsSmash.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceLoadedSuccessfully);
            }
            if (this.mState == MEDIATION_STATE.STATE_LOADING_SMASHES) {
                setState(MEDIATION_STATE.STATE_READY_TO_SHOW);
                ISListenerWrapper.getInstance().onInterstitialAdReady();
                sendMediationEvent(2004, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - this.mLoadStartTime)}});
                if (this.mIsAuctionEnabled) {
                    AuctionResponseItem auctionResponseItem = (AuctionResponseItem) this.mWaterfallServerData.get(progIsSmash.getInstanceName());
                    if (auctionResponseItem != null) {
                        this.mAuctionHandler.reportLoadSuccess(auctionResponseItem);
                        this.mAuctionHandler.reportAuctionLose(this.mWaterfall, this.mWaterfallServerData, auctionResponseItem);
                    } else {
                        String instanceName = progIsSmash != null ? progIsSmash.getInstanceName() : "Smash is null";
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialAdReady winner instance ");
                        sb.append(instanceName);
                        sb.append(" missing from waterfall");
                        logInternal(sb.toString());
                        sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_IS_NOTIFICATIONS_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1010)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "Loaded missing"}, new Object[]{IronSourceConstants.EVENTS_EXT1, instanceName}});
                    }
                }
            }
        }
    }

    public void onInterstitialAdLoadFailed(IronSourceError ironSourceError, ProgIsSmash progIsSmash, long j) {
        ProgIsSmash progIsSmash2;
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("onInterstitialAdLoadFailed error=");
            sb.append(ironSourceError.getErrorMessage());
            sb.append(" state=");
            sb.append(this.mState.name());
            logSmashCallback(progIsSmash, sb.toString());
            sendProviderEvent(IronSourceConstants.IS_INSTANCE_LOAD_FAILED, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{"duration", Long.valueOf(j)}});
            if (progIsSmash != null && this.mWaterfallPerformance.containsKey(progIsSmash.getInstanceName())) {
                this.mWaterfallPerformance.put(progIsSmash.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceFailedToLoad);
            }
            Iterator it = this.mWaterfall.iterator();
            boolean z = false;
            while (true) {
                if (!it.hasNext()) {
                    progIsSmash2 = null;
                    break;
                }
                progIsSmash2 = (ProgIsSmash) it.next();
                if (progIsSmash2.getIsLoadCandidate()) {
                    break;
                } else if (progIsSmash2.isLoadingInProgress()) {
                    z = true;
                }
            }
            if (progIsSmash2 == null && this.mState == MEDIATION_STATE.STATE_LOADING_SMASHES && !z) {
                CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, "No ads to show"));
                sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
                setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
            }
        }
        if (progIsSmash2 != null) {
            sendProviderEvent(2002, progIsSmash2);
            progIsSmash2.loadInterstitial(((AuctionResponseItem) this.mWaterfallServerData.get(progIsSmash2.getInstanceName())).getServerData());
        }
    }

    public void onInterstitialAdOpened(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdOpened");
            ISListenerWrapper.getInstance().onInterstitialAdOpened();
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_OPENED, progIsSmash);
            if (this.mIsAuctionEnabled) {
                AuctionResponseItem auctionResponseItem = (AuctionResponseItem) this.mWaterfallServerData.get(progIsSmash.getInstanceName());
                if (auctionResponseItem != null) {
                    this.mAuctionHandler.reportImpression(auctionResponseItem, this.mCurrentPlacement);
                    this.mWaterfallPerformance.put(progIsSmash.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceShowedSuccessfully);
                } else {
                    String instanceName = progIsSmash != null ? progIsSmash.getInstanceName() : "Smash is null";
                    StringBuilder sb = new StringBuilder();
                    sb.append("onInterstitialAdOpened showing instance ");
                    sb.append(instanceName);
                    sb.append(" missing from waterfall");
                    logInternal(sb.toString());
                    Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1011)};
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Showing missing ");
                    sb2.append(this.mState);
                    sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_IS_NOTIFICATIONS_ERROR, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb2.toString()}, new Object[]{IronSourceConstants.EVENTS_EXT1, instanceName}});
                }
            }
        }
    }

    public void onInterstitialAdClosed(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdClosed");
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_CLOSED, progIsSmash);
            ISListenerWrapper.getInstance().onInterstitialAdClosed();
            setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
        }
    }

    public void onInterstitialAdShowSucceeded(ProgIsSmash progIsSmash) {
        logSmashCallback(progIsSmash, "onInterstitialAdShowSucceeded");
        ISListenerWrapper.getInstance().onInterstitialAdShowSucceeded();
        sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_SUCCESS, progIsSmash);
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError, ProgIsSmash progIsSmash) {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("onInterstitialAdShowFailed error=");
            sb.append(ironSourceError.getErrorMessage());
            logSmashCallback(progIsSmash, sb.toString());
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(ironSourceError);
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_FAILED, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
            this.mWaterfallPerformance.put(progIsSmash.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceFailedToShow);
            setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
        }
    }

    public void onInterstitialAdClicked(ProgIsSmash progIsSmash) {
        logSmashCallback(progIsSmash, JSMethods.ON_INTERSTITIAL_AD_CLICKED);
        ISListenerWrapper.getInstance().onInterstitialAdClicked();
        sendProviderEventWithPlacement(2006, progIsSmash);
    }

    public void onInterstitialAdVisible(ProgIsSmash progIsSmash) {
        logSmashCallback(progIsSmash, "onInterstitialAdVisible");
    }

    public void onInterstitialInitFailed(IronSourceError ironSourceError, ProgIsSmash progIsSmash) {
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_FAILED, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
    }

    public void onInterstitialInitSuccess(ProgIsSmash progIsSmash) {
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_SUCCESS, progIsSmash);
    }

    /* access modifiers changed from: private */
    public void setState(MEDIATION_STATE mediation_state) {
        this.mState = mediation_state;
        StringBuilder sb = new StringBuilder();
        sb.append("state=");
        sb.append(mediation_state);
        logInternal(sb.toString());
    }

    private void logSmashCallback(ProgIsSmash progIsSmash, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("ProgIsManager ");
        sb.append(progIsSmash.getInstanceName());
        sb.append(" : ");
        sb.append(str);
        IronSourceLoggerManager.getLogger().log(IronSourceTag.INTERNAL, sb.toString(), 0);
    }

    private void logAPIError(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.API, str, 3);
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("ProgIsManager ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void sendMediationEvent(int i) {
        sendMediationEvent(i, null, false);
    }

    /* access modifiers changed from: private */
    public void sendMediationEvent(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false);
    }

    private void sendMediationEventWithPlacement(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, true);
    }

    private void sendMediationEventWithPlacement(int i) {
        sendMediationEvent(i, null, true);
    }

    private void sendMediationEvent(int i, Object[][] objArr, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_PROGRAMMATIC, Integer.valueOf(1));
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            hashMap.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            hashMap.put(IronSourceConstants.EVENTS_PLACEMENT_NAME, this.mCurrentPlacement);
        }
        if (shouldAddAuctionParams(i)) {
            InterstitialEventsManager.getInstance().setEventAuctionParams(hashMap, this.mAuctionTrial, this.mAuctionFallback);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    hashMap.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("sendMediationEvent ");
                sb.append(e.getMessage());
                logInternal(sb.toString());
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash) {
        sendProviderEvent(i, progIsSmash, null, false);
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash, Object[][] objArr) {
        sendProviderEvent(i, progIsSmash, objArr, false);
    }

    private void sendProviderEventWithPlacement(int i, ProgIsSmash progIsSmash, Object[][] objArr) {
        sendProviderEvent(i, progIsSmash, objArr, true);
    }

    private void sendProviderEventWithPlacement(int i, ProgIsSmash progIsSmash) {
        sendProviderEvent(i, progIsSmash, null, true);
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash, Object[][] objArr, boolean z) {
        Map providerEventData = progIsSmash.getProviderEventData();
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            providerEventData.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            providerEventData.put(IronSourceConstants.EVENTS_PLACEMENT_NAME, this.mCurrentPlacement);
        }
        if (shouldAddAuctionParams(i)) {
            InterstitialEventsManager.getInstance().setEventAuctionParams(providerEventData, this.mAuctionTrial, this.mAuctionFallback);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("IS sendProviderEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }

    /* access modifiers changed from: 0000 */
    public void shouldTrackNetworkState(Context context, boolean z) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("Should Track Network State: ");
        sb.append(z);
        logger.log(ironSourceTag, sb.toString(), 0);
        this.mShouldTrackNetworkState = z;
    }
}
