package com.ironsource.mediationsdk;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AuctionDataUtils {
    private static final String AUCTION_RESPONSE_SERVER_DATA_ADM_KEY = "adMarkup";
    private static final String AUCTION_RESPONSE_SERVER_DATA_MARKET_PLACE_DEMAND_TYPE_KEY = "dynamicDemandSource";
    private static final String AUCTION_RESPONSE_SERVER_DATA_PARAMS_KEY = "params";
    private static final String TAG = "AuctionDataUtils";
    private static AuctionDataUtils sInstance = new AuctionDataUtils();

    public static class AuctionData {
        /* access modifiers changed from: private */
        public String mAuctionId;
        /* access modifiers changed from: private */
        public int mErrorCode;
        /* access modifiers changed from: private */
        public String mErrorMessage;
        /* access modifiers changed from: private */
        public List<AuctionResponseItem> mWaterfall;

        public String getAuctionId() {
            return this.mAuctionId;
        }

        public List<AuctionResponseItem> getWaterfall() {
            return this.mWaterfall;
        }

        public int getErrorCode() {
            return this.mErrorCode;
        }

        public String getErrorMessage() {
            return this.mErrorMessage;
        }
    }

    static class ImpressionHttpTask extends AsyncTask<String, Void, Boolean> {
        private static final int SERVER_REQUEST_TIMEOUT = 15000;

        ImpressionHttpTask() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(strArr[0]).openConnection();
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.setReadTimeout(15000);
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                httpURLConnection.disconnect();
                return Boolean.valueOf(responseCode == 200);
            } catch (Exception unused) {
                return Boolean.valueOf(false);
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
        }
    }

    public static AuctionDataUtils getInstance() {
        return sInstance;
    }

    public AuctionData getAuctionDataFromResponse(JSONObject jSONObject) throws JSONException {
        AuctionData auctionData = new AuctionData();
        auctionData.mAuctionId = jSONObject.getString(IronSourceConstants.EVENTS_AUCTION_ID);
        auctionData.mWaterfall = new ArrayList();
        JSONArray jSONArray = jSONObject.getJSONArray("waterfall");
        int i = 0;
        while (i < jSONArray.length()) {
            AuctionResponseItem auctionResponseItem = new AuctionResponseItem(jSONArray.getJSONObject(i));
            if (auctionResponseItem.isValid()) {
                auctionData.mWaterfall.add(auctionResponseItem);
                i++;
            } else {
                auctionData.mErrorCode = 1002;
                StringBuilder sb = new StringBuilder();
                sb.append("waterfall ");
                sb.append(i);
                auctionData.mErrorMessage = sb.toString();
                throw new JSONException("invalid response");
            }
        }
        return auctionData;
    }

    public AuctionResponseItem getAuctionResponseItem(String str, List<AuctionResponseItem> list) {
        for (int i = 0; i < list.size(); i++) {
            if (((AuctionResponseItem) list.get(i)).getInstanceName().equals(str)) {
                return (AuctionResponseItem) list.get(i);
            }
        }
        return null;
    }

    public JSONObject enrichToken(Context context, Map<String, Object> map, List<String> list, AuctionHistory auctionHistory, int i, String str, AuctionSettings auctionSettings) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        for (String str2 : map.keySet()) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, 2);
            jSONObject2.put("biddingAdditionalData", new JSONObject((Map) map.get(str2)));
            jSONObject2.put("performance", auctionHistory != null ? auctionHistory.getStoredPerformanceForInstance(str2) : "");
            jSONObject.put(str2, jSONObject2);
        }
        if (list != null) {
            for (String str3 : list) {
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, 1);
                jSONObject3.put("performance", auctionHistory != null ? auctionHistory.getStoredPerformanceForInstance(str3) : "");
                jSONObject.put(str3, jSONObject3);
            }
        }
        ConcurrentHashMap metaData = AdapterRepository.getInstance().getMetaData();
        JSONObject jSONObject4 = new JSONObject();
        for (Entry entry : metaData.entrySet()) {
            jSONObject4.put((String) entry.getKey(), entry.getValue());
        }
        JSONObject jSONObject5 = new JSONObject();
        jSONObject5.put(RequestParameters.APPLICATION_USER_ID, IronSourceObject.getInstance().getIronSourceUserId());
        String gender = IronSourceObject.getInstance().getGender();
        if (TextUtils.isEmpty(gender)) {
            gender = "unknown";
        }
        jSONObject5.put("applicationUserGender", gender);
        Integer age = IronSourceObject.getInstance().getAge();
        if (age == null) {
            age = Integer.valueOf(-1);
        }
        jSONObject5.put("applicationUserAge", age);
        Boolean consent = IronSourceObject.getInstance().getConsent();
        if (consent != null) {
            jSONObject5.put(RequestParameters.CONSENT, consent.booleanValue() ? 1 : 0);
        }
        jSONObject5.put(RequestParameters.MOBILE_CARRIER, DeviceStatus.getMobileCarrier(context));
        jSONObject5.put(RequestParameters.CONNECTION_TYPE, IronSourceUtils.getConnectionType(context));
        jSONObject5.put("deviceOS", "android");
        jSONObject5.put("deviceWidth", context.getResources().getConfiguration().screenWidthDp);
        jSONObject5.put("deviceHeight", context.getResources().getConfiguration().screenHeightDp);
        String str4 = RequestParameters.DEVICE_OS_VERSION;
        StringBuilder sb = new StringBuilder();
        sb.append(VERSION.SDK_INT);
        sb.append("(");
        sb.append(VERSION.RELEASE);
        sb.append(")");
        jSONObject5.put(str4, sb.toString());
        jSONObject5.put(RequestParameters.DEVICE_MODEL, Build.MODEL);
        jSONObject5.put("deviceMake", Build.MANUFACTURER);
        jSONObject5.put(RequestParameters.PACKAGE_NAME, context.getPackageName());
        jSONObject5.put(RequestParameters.APPLICATION_VERSION_NAME, ApplicationContext.getPublisherApplicationVersion(context, context.getPackageName()));
        jSONObject5.put("clientTimestamp", new Date().getTime());
        String str5 = "";
        String str6 = "";
        boolean z = false;
        try {
            String[] advertisingIdInfo = DeviceStatus.getAdvertisingIdInfo(context);
            if (advertisingIdInfo != null && advertisingIdInfo.length == 2) {
                if (!TextUtils.isEmpty(advertisingIdInfo[0])) {
                    str5 = advertisingIdInfo[0];
                }
                z = Boolean.valueOf(advertisingIdInfo[1]).booleanValue();
            }
        } catch (Exception unused) {
        }
        if (!TextUtils.isEmpty(str5)) {
            str6 = IronSourceConstants.TYPE_GAID;
        } else {
            str5 = DeviceStatus.getOrGenerateOnceUniqueIdentifier(context);
            if (!TextUtils.isEmpty(str5)) {
                str6 = IronSourceConstants.TYPE_UUID;
            }
        }
        if (!TextUtils.isEmpty(str5)) {
            jSONObject5.put("advId", str5);
            jSONObject5.put("advIdType", str6);
            jSONObject5.put(RequestParameters.isLAT, z ? "true" : "false");
        }
        JSONObject jSONObject6 = new JSONObject();
        jSONObject6.put(RequestParameters.APPLICATION_KEY, IronSourceObject.getInstance().getIronSourceAppKey());
        jSONObject6.put(RequestParameters.SDK_VERSION, IronSourceUtils.getSDKVersion());
        jSONObject6.put("clientParams", jSONObject5);
        jSONObject6.put("sessionDepth", i);
        jSONObject6.put("sessionId", str);
        jSONObject6.put("instances", jSONObject);
        jSONObject6.put("auctionData", auctionSettings.getAuctionData());
        jSONObject6.put("metaData", jSONObject4);
        return jSONObject6;
    }

    /* access modifiers changed from: 0000 */
    public String encryptToken(JSONObject jSONObject) {
        return IronSourceAES.encode(IronSourceUtils.KEY, jSONObject.toString());
    }

    /* access modifiers changed from: 0000 */
    public JSONObject decodeAdmResponse(String str) {
        try {
            return new JSONObject(IronSourceAES.decode(IronSourceUtils.KEY, str));
        } catch (Exception unused) {
            return null;
        }
    }

    public void sendResponse(String str) {
        new ImpressionHttpTask().execute(new String[]{str});
    }

    public Map<String, String> getAuctionResponseServerDataParams(String str) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("params")) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("params");
                Iterator keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String str2 = (String) keys.next();
                    Object obj = jSONObject2.get(str2);
                    if (obj instanceof String) {
                        hashMap.put(str2, (String) obj);
                    }
                }
            }
        } catch (JSONException unused) {
        }
        return hashMap;
    }

    public String getAdmFromServerData(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            return jSONObject.has(AUCTION_RESPONSE_SERVER_DATA_ADM_KEY) ? jSONObject.getString(AUCTION_RESPONSE_SERVER_DATA_ADM_KEY) : str;
        } catch (JSONException unused) {
            return str;
        }
    }

    public String getDynamicDemandSourceIdFromServerData(String str) {
        String str2 = "";
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has("params")) {
                return str2;
            }
            JSONObject jSONObject2 = jSONObject.getJSONObject("params");
            return jSONObject2.has("dynamicDemandSource") ? jSONObject2.getString("dynamicDemandSource") : str2;
        } catch (JSONException unused) {
            return str2;
        }
    }
}
