package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.environment.NetworkStateReceiver;
import com.ironsource.environment.NetworkStateReceiver.NetworkStateReceiverListener;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AuctionHistory.ISAuctionPerformance;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionCappingManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

class ProgRvManager implements ProgRvManagerListener, RvLoadTriggerCallback, AuctionEventListener, NetworkStateReceiverListener {
    /* access modifiers changed from: private */
    public Context mAppContext;
    private String mAuctionFallback = "";
    /* access modifiers changed from: private */
    public AuctionHandler mAuctionHandler;
    /* access modifiers changed from: private */
    public AuctionHistory mAuctionHistory;
    /* access modifiers changed from: private */
    public long mAuctionStartTime;
    private int mAuctionTrial;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId;
    private String mCurrentPlacement;
    private boolean mIsAuctionEnabled;
    private boolean mIsAuctionOnShowStart;
    private boolean mIsShowingVideo;
    private long mLastChangedAvailabilityTime;
    Boolean mLastMediationAvailabilityState;
    private Boolean mLastReportedAvailabilityState;
    private int mMaxSmashesToLoad;
    private NetworkStateReceiver mNetworkStateReceiver;
    private List<AuctionResponseItem> mNextWaterfallToLoad;
    private RvLoadTrigger mRvLoadTrigger;
    /* access modifiers changed from: private */
    public SessionCappingManager mSessionCappingManager;
    /* access modifiers changed from: private */
    public int mSessionDepth = 1;
    private boolean mShouldLoadAfterClose;
    boolean mShouldTrackNetworkState = false;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, ProgRvSmash> mSmashes;
    private RV_MEDIATION_STATE mState;
    private long mTimeToWaitBeforeLoadMS;
    private CopyOnWriteArrayList<ProgRvSmash> mWaterfall;
    private ConcurrentHashMap<String, ISAuctionPerformance> mWaterfallPerformance;
    private ConcurrentHashMap<String, AuctionResponseItem> mWaterfallServerData;

    private enum RV_MEDIATION_STATE {
        RV_STATE_INITIATING,
        RV_STATE_AUCTION_IN_PROGRESS,
        RV_STATE_NOT_LOADED,
        RV_STATE_LOADING_SMASHES,
        RV_STATE_READY_TO_SHOW
    }

    private boolean shouldAddAuctionParams(int i) {
        return i == 1003 || i == 1302 || i == 1301;
    }

    public ProgRvManager(Activity activity, List<ProviderSettings> list, RewardedVideoConfigurations rewardedVideoConfigurations, String str, String str2) {
        long time = new Date().getTime();
        sendMediationEventWithoutAuctionId(IronSourceConstants.RV_MANAGER_INIT_STARTED);
        setState(RV_MEDIATION_STATE.RV_STATE_INITIATING);
        this.mAppContext = activity.getApplicationContext();
        this.mLastReportedAvailabilityState = null;
        this.mMaxSmashesToLoad = rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadAmount();
        this.mCurrentPlacement = "";
        AuctionSettings rewardedVideoAuctionSettings = rewardedVideoConfigurations.getRewardedVideoAuctionSettings();
        this.mIsShowingVideo = false;
        this.mWaterfall = new CopyOnWriteArrayList<>();
        this.mNextWaterfallToLoad = new ArrayList();
        this.mWaterfallServerData = new ConcurrentHashMap<>();
        this.mWaterfallPerformance = new ConcurrentHashMap<>();
        this.mLastChangedAvailabilityTime = new Date().getTime();
        this.mIsAuctionEnabled = rewardedVideoAuctionSettings.getNumOfMaxTrials() > 0;
        this.mIsAuctionOnShowStart = rewardedVideoAuctionSettings.getIsAuctionOnShowStart();
        this.mShouldLoadAfterClose = !rewardedVideoAuctionSettings.getIsLoadWhileShow();
        this.mTimeToWaitBeforeLoadMS = rewardedVideoAuctionSettings.getTimeToWaitBeforeLoadMs();
        if (this.mIsAuctionEnabled) {
            this.mAuctionHandler = new AuctionHandler("rewardedVideo", rewardedVideoAuctionSettings, this);
        }
        this.mRvLoadTrigger = new RvLoadTrigger(rewardedVideoAuctionSettings, this);
        this.mSmashes = new ConcurrentHashMap<>();
        ArrayList arrayList = new ArrayList();
        for (ProviderSettings providerSettings : list) {
            AbstractAdapter adapter = AdapterRepository.getInstance().getAdapter(providerSettings, providerSettings.getRewardedVideoSettings(), activity);
            if (adapter != null && AdaptersCompatibilityHandler.getInstance().isAdapterVersionRVCompatible(adapter)) {
                ProgRvSmash progRvSmash = r0;
                ProgRvSmash progRvSmash2 = new ProgRvSmash(activity, str, str2, providerSettings, this, rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadTimeout(), adapter);
                String instanceName = progRvSmash.getInstanceName();
                this.mSmashes.put(instanceName, progRvSmash);
                arrayList.add(instanceName);
            }
        }
        this.mAuctionHistory = new AuctionHistory(arrayList, rewardedVideoAuctionSettings.getAuctionSavedHistoryLimit());
        this.mSessionCappingManager = new SessionCappingManager(new ArrayList(this.mSmashes.values()));
        for (ProgRvSmash progRvSmash3 : this.mSmashes.values()) {
            if (progRvSmash3.isBidder()) {
                progRvSmash3.initForBidding();
            }
        }
        sendMediationEventWithoutAuctionId(IronSourceConstants.RV_MANAGER_INIT_ENDED, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - time)}});
        loadRewardedVideo(rewardedVideoAuctionSettings.getTimeToWaitBeforeFirstAuctionMs());
    }

    private void loadRewardedVideo(long j) {
        if (this.mSessionCappingManager.areAllSmashesCapped()) {
            sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(80001)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "all smashes are capped"}});
            handleLoadFailure();
            return;
        }
        if (this.mIsAuctionEnabled) {
            if (!this.mWaterfallPerformance.isEmpty()) {
                this.mAuctionHistory.storeWaterfallPerformance(this.mWaterfallPerformance);
                this.mWaterfallPerformance.clear();
            }
            new Timer().schedule(new TimerTask() {
                public void run() {
                    ProgRvManager.this.makeAuction();
                }
            }, j);
        } else {
            updateWaterfallToNonBidding();
            if (this.mNextWaterfallToLoad.isEmpty()) {
                sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(80002)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "waterfall is empty"}});
                handleLoadFailure();
                return;
            }
            sendMediationEventWithoutAuctionId(1000);
            if (!this.mShouldLoadAfterClose || !this.mIsShowingVideo) {
                loadSmashesForNextWaterfall();
            }
        }
    }

    public synchronized void showRewardedVideo(Placement placement) {
        if (placement == null) {
            String str = "showRewardedVideo error: empty default placement";
            logAPIError(str);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(1021, str));
            sendMediationEvent(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1021)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str}}, false, true);
            return;
        }
        this.mCurrentPlacement = placement.getPlacementName();
        StringBuilder sb = new StringBuilder();
        sb.append("showRewardedVideo() placement=");
        sb.append(this.mCurrentPlacement);
        logApi(sb.toString());
        sendMediationEventWithPlacement(IronSourceConstants.RV_API_SHOW_CALLED);
        if (this.mIsShowingVideo) {
            String str2 = "showRewardedVideo error: can't show ad while an ad is already showing";
            logAPIError(str2);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_RV_SHOW_CALLED_DURING_SHOW, str2));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_RV_SHOW_CALLED_DURING_SHOW)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str2}});
        } else if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
            String str3 = "showRewardedVideo error: show called while no ads are available";
            logAPIError(str3);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_RV_SHOW_CALLED_WRONG_STATE, str3));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_RV_SHOW_CALLED_WRONG_STATE)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str3}});
        } else if (CappingManager.isRvPlacementCapped(this.mAppContext, this.mCurrentPlacement)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("showRewardedVideo error: placement ");
            sb2.append(this.mCurrentPlacement);
            sb2.append(" is capped");
            String sb3 = sb2.toString();
            logAPIError(sb3);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT, sb3));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb3}});
        } else {
            Iterator it = this.mWaterfall.iterator();
            while (it.hasNext()) {
                ProgRvSmash progRvSmash = (ProgRvSmash) it.next();
                if (progRvSmash.isReadyToShow()) {
                    this.mIsShowingVideo = true;
                    progRvSmash.reportShowChance(true, this.mSessionDepth);
                    showVideo(progRvSmash, placement);
                    setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
                    return;
                }
                progRvSmash.reportShowChance(false, this.mSessionDepth);
            }
            String str4 = "showRewardedVideo(): No ads to show";
            logApi(str4);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str4}});
            this.mRvLoadTrigger.showError();
        }
    }

    private void showVideo(ProgRvSmash progRvSmash, Placement placement) {
        logInternal("showVideo()");
        this.mSessionCappingManager.increaseShowCounter(progRvSmash);
        if (this.mSessionCappingManager.isCapped(progRvSmash)) {
            progRvSmash.setCappedPerSession();
            StringBuilder sb = new StringBuilder();
            sb.append(progRvSmash.getInstanceName());
            sb.append(" rewarded video is now session capped");
            IronSourceUtils.sendAutomationLog(sb.toString());
        }
        CappingManager.incrementRvShowCounter(this.mAppContext, placement.getPlacementName());
        if (CappingManager.isRvPlacementCapped(this.mAppContext, placement.getPlacementName())) {
            sendMediationEventWithPlacement(IronSourceConstants.RV_CAP_PLACEMENT);
        }
        progRvSmash.showVideo(placement, this.mSessionDepth);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0039, code lost:
        return false;
     */
    public synchronized boolean isRewardedVideoAvailable() {
        if (this.mShouldTrackNetworkState && !IronSourceUtils.isNetworkConnected(this.mAppContext)) {
            return false;
        }
        if (this.mState == RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
            if (!this.mIsShowingVideo) {
                Iterator it = this.mWaterfall.iterator();
                while (it.hasNext()) {
                    if (((ProgRvSmash) it.next()).isReadyToShow()) {
                        return true;
                    }
                }
                return false;
            }
        }
    }

    /* access modifiers changed from: private */
    public void makeAuction() {
        setState(RV_MEDIATION_STATE.RV_STATE_AUCTION_IN_PROGRESS);
        AsyncTask.execute(new Runnable() {
            public void run() {
                ProgRvManager.this.logInternal("makeAuction()");
                ProgRvManager.this.mCurrentAuctionId = "";
                ProgRvManager.this.mAuctionStartTime = new Date().getTime();
                HashMap hashMap = new HashMap();
                ArrayList arrayList = new ArrayList();
                StringBuilder sb = new StringBuilder();
                for (ProgRvSmash progRvSmash : ProgRvManager.this.mSmashes.values()) {
                    progRvSmash.unloadVideo();
                    if (!ProgRvManager.this.mSessionCappingManager.isCapped(progRvSmash)) {
                        if (progRvSmash.isBidder()) {
                            Map biddingData = progRvSmash.getBiddingData();
                            if (biddingData != null) {
                                hashMap.put(progRvSmash.getInstanceName(), biddingData);
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("2");
                                sb2.append(progRvSmash.getInstanceName());
                                sb2.append(",");
                                sb.append(sb2.toString());
                            }
                        } else if (!progRvSmash.isBidder()) {
                            arrayList.add(progRvSmash.getInstanceName());
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("1");
                            sb3.append(progRvSmash.getInstanceName());
                            sb3.append(",");
                            sb.append(sb3.toString());
                        }
                    }
                }
                if (hashMap.keySet().size() == 0 && arrayList.size() == 0) {
                    ProgRvManager.this.logInternal("makeAuction() failed - request waterfall is empty");
                    ProgRvManager.this.sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(80003)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "waterfall request is empty"}});
                    ProgRvManager.this.handleLoadFailure();
                    return;
                }
                ProgRvManager progRvManager = ProgRvManager.this;
                StringBuilder sb4 = new StringBuilder();
                sb4.append("makeAuction() - request waterfall is: ");
                sb4.append(sb);
                progRvManager.logInternal(sb4.toString());
                if (sb.length() > 256) {
                    sb.setLength(256);
                } else if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                ProgRvManager.this.sendMediationEventWithoutAuctionId(1000);
                ProgRvManager.this.sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_REQUEST);
                ProgRvManager.this.sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_REQUEST_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
                ProgRvManager.this.mAuctionHandler.executeAuction(ProgRvManager.this.mAppContext, hashMap, arrayList, ProgRvManager.this.mAuctionHistory, ProgRvManager.this.mSessionDepth);
            }
        });
    }

    /* access modifiers changed from: private */
    public void loadSmashesForNextWaterfall() {
        updateWaterfall(this.mNextWaterfallToLoad);
        loadSmashes();
    }

    private void updateWaterfallToNonBidding() {
        updateNextWaterfallToLoad(extractNonBidderProvidersFromWaterfall());
        StringBuilder sb = new StringBuilder();
        sb.append("fallback_");
        sb.append(System.currentTimeMillis());
        this.mCurrentAuctionId = sb.toString();
    }

    private void updateNextWaterfallToLoad(List<AuctionResponseItem> list) {
        this.mNextWaterfallToLoad = list;
        StringBuilder sb = new StringBuilder();
        for (AuctionResponseItem auctionResponseItem : list) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(getAsString(auctionResponseItem));
            sb2.append(",");
            sb.append(sb2.toString());
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("updateNextWaterfallToLoad() - next waterfall is ");
        sb3.append(sb.toString());
        logInternal(sb3.toString());
        if (sb.length() > 256) {
            sb.setLength(256);
        } else if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        } else {
            logInternal("Updated waterfall is empty");
        }
        sendMediationEvent(IronSourceConstants.RV_AUCTION_RESPONSE_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
    }

    private List<AuctionResponseItem> extractNonBidderProvidersFromWaterfall() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (ProgRvSmash progRvSmash : this.mSmashes.values()) {
            if (!progRvSmash.isBidder() && !this.mSessionCappingManager.isCapped(progRvSmash)) {
                copyOnWriteArrayList.add(new AuctionResponseItem(progRvSmash.getInstanceName()));
            }
        }
        return copyOnWriteArrayList;
    }

    private String getAsString(AuctionResponseItem auctionResponseItem) {
        ProgRvSmash progRvSmash = (ProgRvSmash) this.mSmashes.get(auctionResponseItem.getInstanceName());
        String str = progRvSmash != null ? progRvSmash.isBidder() ? "2" : "1" : TextUtils.isEmpty(auctionResponseItem.getServerData()) ? "1" : "2";
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(auctionResponseItem.getInstanceName());
        return sb.toString();
    }

    private void updateWaterfall(List<AuctionResponseItem> list) {
        this.mWaterfall.clear();
        this.mWaterfallServerData.clear();
        this.mWaterfallPerformance.clear();
        for (AuctionResponseItem auctionResponseItem : list) {
            ProgRvSmash progRvSmash = (ProgRvSmash) this.mSmashes.get(auctionResponseItem.getInstanceName());
            if (progRvSmash != null) {
                progRvSmash.setIsLoadCandidate(true);
                this.mWaterfall.add(progRvSmash);
                this.mWaterfallServerData.put(progRvSmash.getInstanceName(), auctionResponseItem);
                this.mWaterfallPerformance.put(auctionResponseItem.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceDidntAttemptToLoad);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("updateWaterfall() - could not find matching smash for auction response item ");
                sb.append(auctionResponseItem.getInstanceName());
                logInternal(sb.toString());
            }
        }
        this.mNextWaterfallToLoad.clear();
    }

    private void loadSmashes() {
        if (this.mWaterfall.isEmpty()) {
            sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(80004)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "waterfall is empty"}});
            handleLoadFailure();
            return;
        }
        setState(RV_MEDIATION_STATE.RV_STATE_LOADING_SMASHES);
        int i = 0;
        for (int i2 = 0; i2 < this.mWaterfall.size() && i < this.mMaxSmashesToLoad; i2++) {
            ProgRvSmash progRvSmash = (ProgRvSmash) this.mWaterfall.get(i2);
            if (progRvSmash.getIsLoadCandidate()) {
                String serverData = ((AuctionResponseItem) this.mWaterfallServerData.get(progRvSmash.getInstanceName())).getServerData();
                progRvSmash.setDynamicDemandSourceIdByServerData(serverData);
                progRvSmash.loadVideo(serverData, this.mCurrentAuctionId, this.mAuctionTrial, this.mAuctionFallback, this.mSessionDepth);
                i++;
            }
        }
    }

    /* access modifiers changed from: private */
    public void handleLoadFailure() {
        setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
        reportAvailabilityIfNeeded(false);
        this.mRvLoadTrigger.loadError();
    }

    private void setState(RV_MEDIATION_STATE rv_mediation_state) {
        StringBuilder sb = new StringBuilder();
        sb.append("current state=");
        sb.append(this.mState);
        sb.append(", new state=");
        sb.append(rv_mediation_state);
        logInternal(sb.toString());
        this.mState = rv_mediation_state;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0136, code lost:
        return;
     */
    public synchronized void onLoadSuccess(ProgRvSmash progRvSmash, String str) {
        logSmashCallback(progRvSmash, "onLoadSuccess ");
        if (this.mCurrentAuctionId == null || str.equalsIgnoreCase(this.mCurrentAuctionId)) {
            RV_MEDIATION_STATE rv_mediation_state = this.mState;
            this.mWaterfallPerformance.put(progRvSmash.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceLoadedSuccessfully);
            reportAvailabilityIfNeeded(true);
            if (this.mState == RV_MEDIATION_STATE.RV_STATE_LOADING_SMASHES) {
                setState(RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW);
                sendMediationEvent(1003, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - this.mAuctionStartTime)}});
                if (this.mIsAuctionEnabled) {
                    AuctionResponseItem auctionResponseItem = (AuctionResponseItem) this.mWaterfallServerData.get(progRvSmash.getInstanceName());
                    if (auctionResponseItem != null) {
                        this.mAuctionHandler.reportLoadSuccess(auctionResponseItem);
                        this.mAuctionHandler.reportAuctionLose(this.mWaterfall, this.mWaterfallServerData, auctionResponseItem);
                    } else {
                        String instanceName = progRvSmash != null ? progRvSmash.getInstanceName() : "Smash is null";
                        StringBuilder sb = new StringBuilder();
                        sb.append("onLoadSuccess winner instance ");
                        sb.append(instanceName);
                        sb.append(" missing from waterfall. auctionId: ");
                        sb.append(str);
                        sb.append(" and the current id is ");
                        sb.append(this.mCurrentAuctionId);
                        logErrorInternal(sb.toString());
                        Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1010)};
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Loaded missing ");
                        sb2.append(rv_mediation_state);
                        sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_NOTIFICATIONS_ERROR, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb2.toString()}, new Object[]{IronSourceConstants.EVENTS_EXT1, instanceName}});
                    }
                }
            }
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("onLoadSuccess was invoked with auctionId: ");
            sb3.append(str);
            sb3.append(" and the current id is ");
            sb3.append(this.mCurrentAuctionId);
            logInternal(sb3.toString());
            Object[] objArr2 = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(2)};
            StringBuilder sb4 = new StringBuilder();
            sb4.append("onLoadSuccess wrong auction ID ");
            sb4.append(this.mState);
            progRvSmash.sendProviderEvent(IronSourceConstants.RV_MANAGER_UNEXPECTED_STATE, new Object[][]{objArr2, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb4.toString()}});
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c2, code lost:
        if (r4 == null) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00c4, code lost:
        r4.loadVideo(((com.ironsource.mediationsdk.AuctionResponseItem) r10.mWaterfallServerData.get(r4.getInstanceName())).getServerData(), r10.mCurrentAuctionId, r10.mAuctionTrial, r10.mAuctionFallback, r10.mSessionDepth);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00df, code lost:
        return;
     */
    public void onLoadError(ProgRvSmash progRvSmash, String str) {
        ProgRvSmash progRvSmash2;
        synchronized (this) {
            logSmashCallback(progRvSmash, "onLoadError ");
            if (!str.equalsIgnoreCase(this.mCurrentAuctionId)) {
                StringBuilder sb = new StringBuilder();
                sb.append("onLoadError was invoked with auctionId:");
                sb.append(str);
                sb.append(" and the current id is ");
                sb.append(this.mCurrentAuctionId);
                logInternal(sb.toString());
                Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(4)};
                StringBuilder sb2 = new StringBuilder();
                sb2.append("loadError wrong auction ID ");
                sb2.append(this.mState);
                progRvSmash.sendProviderEvent(IronSourceConstants.RV_MANAGER_UNEXPECTED_STATE, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb2.toString()}});
                return;
            }
            this.mWaterfallPerformance.put(progRvSmash.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceFailedToLoad);
            Iterator it = this.mWaterfall.iterator();
            boolean z = false;
            boolean z2 = false;
            while (true) {
                if (!it.hasNext()) {
                    progRvSmash2 = null;
                    break;
                }
                progRvSmash2 = (ProgRvSmash) it.next();
                if (progRvSmash2.getIsLoadCandidate()) {
                    if (this.mWaterfallServerData.get(progRvSmash2.getInstanceName()) != null) {
                        break;
                    }
                } else if (progRvSmash2.isLoadingInProgress()) {
                    z2 = true;
                } else if (progRvSmash2.isReadyToShow()) {
                    z = true;
                }
            }
            ProgRvSmash progRvSmash3 = progRvSmash2;
            if (progRvSmash3 == null && !z && !z2) {
                logInternal("onLoadError(): No other available smashes");
                reportAvailabilityIfNeeded(false);
                setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
                this.mRvLoadTrigger.loadError();
            }
        }
    }

    public void onRewardedVideoAdOpened(ProgRvSmash progRvSmash) {
        synchronized (this) {
            this.mSessionDepth++;
            logSmashCallback(progRvSmash, "onRewardedVideoAdOpened");
            RVListenerWrapper.getInstance().onRewardedVideoAdOpened();
            if (this.mIsAuctionEnabled) {
                AuctionResponseItem auctionResponseItem = (AuctionResponseItem) this.mWaterfallServerData.get(progRvSmash.getInstanceName());
                if (auctionResponseItem != null) {
                    this.mAuctionHandler.reportImpression(auctionResponseItem, this.mCurrentPlacement);
                    this.mWaterfallPerformance.put(progRvSmash.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceShowedSuccessfully);
                } else {
                    String instanceName = progRvSmash != null ? progRvSmash.getInstanceName() : "Smash is null";
                    StringBuilder sb = new StringBuilder();
                    sb.append("onRewardedVideoAdOpened showing instance ");
                    sb.append(instanceName);
                    sb.append(" missing from waterfall");
                    logErrorInternal(sb.toString());
                    Object[] objArr = {IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(1011)};
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Showing missing ");
                    sb2.append(this.mState);
                    sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_NOTIFICATIONS_ERROR, new Object[][]{objArr, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, sb2.toString()}, new Object[]{IronSourceConstants.EVENTS_EXT1, instanceName}});
                }
            }
            this.mRvLoadTrigger.showStart();
        }
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError, ProgRvSmash progRvSmash) {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("onRewardedVideoAdShowFailed error=");
            sb.append(ironSourceError.getErrorMessage());
            logSmashCallback(progRvSmash, sb.toString());
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(ironSourceError);
            this.mIsShowingVideo = false;
            this.mWaterfallPerformance.put(progRvSmash.getInstanceName(), ISAuctionPerformance.ISAuctionPerformanceFailedToShow);
            if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                reportAvailabilityIfNeeded(false);
            }
            this.mRvLoadTrigger.showError();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0059 A[Catch:{ Throwable -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005c A[Catch:{ Throwable -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0095 A[Catch:{ Throwable -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c A[Catch:{ Throwable -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b8 A[Catch:{ Throwable -> 0x003c }] */
    public void onRewardedVideoAdClosed(ProgRvSmash progRvSmash) {
        boolean z;
        synchronized (this) {
            try {
                Iterator it = this.mSmashes.values().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ProgRvSmash progRvSmash2 = (ProgRvSmash) it.next();
                    if (progRvSmash2.isRewardedVideoAvailable()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(progRvSmash2.getInstanceName());
                        sb.append(" has available RV");
                        logInternal(sb.toString());
                        z = true;
                        break;
                    }
                }
            } catch (Throwable unused) {
                logInternal("Failed to check RV availability");
            }
            Object[][] objArr = new Object[1][];
            Object[] objArr2 = new Object[2];
            objArr2[0] = IronSourceConstants.EVENTS_EXT1;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("otherRVAvailable = ");
            sb2.append(!z ? "true" : "false");
            objArr2[1] = sb2.toString();
            objArr[0] = objArr2;
            progRvSmash.sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_CLOSED, objArr);
            StringBuilder sb3 = new StringBuilder();
            sb3.append("onRewardedVideoAdClosed, mediation state: ");
            sb3.append(this.mState.name());
            logSmashCallback(progRvSmash, sb3.toString());
            RVListenerWrapper.getInstance().onRewardedVideoAdClosed();
            this.mIsShowingVideo = false;
            if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                reportAvailabilityIfNeeded(false);
            }
            if (this.mIsAuctionOnShowStart) {
                this.mRvLoadTrigger.showEnd();
            } else if (this.mNextWaterfallToLoad != null && this.mNextWaterfallToLoad.size() > 0) {
                new Timer().schedule(new TimerTask() {
                    public void run() {
                        ProgRvManager.this.loadSmashesForNextWaterfall();
                    }
                }, this.mTimeToWaitBeforeLoadMS);
            }
        }
        z = false;
        Object[][] objArr3 = new Object[1][];
        Object[] objArr22 = new Object[2];
        objArr22[0] = IronSourceConstants.EVENTS_EXT1;
        StringBuilder sb22 = new StringBuilder();
        sb22.append("otherRVAvailable = ");
        sb22.append(!z ? "true" : "false");
        objArr22[1] = sb22.toString();
        objArr3[0] = objArr22;
        progRvSmash.sendProviderEventWithPlacement(IronSourceConstants.RV_INSTANCE_CLOSED, objArr3);
        StringBuilder sb32 = new StringBuilder();
        sb32.append("onRewardedVideoAdClosed, mediation state: ");
        sb32.append(this.mState.name());
        logSmashCallback(progRvSmash, sb32.toString());
        RVListenerWrapper.getInstance().onRewardedVideoAdClosed();
        this.mIsShowingVideo = false;
        if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
        }
        if (this.mIsAuctionOnShowStart) {
        }
    }

    public void onRewardedVideoAdStarted(ProgRvSmash progRvSmash) {
        logSmashCallback(progRvSmash, "onRewardedVideoAdStarted");
        RVListenerWrapper.getInstance().onRewardedVideoAdStarted();
    }

    public void onRewardedVideoAdEnded(ProgRvSmash progRvSmash) {
        logSmashCallback(progRvSmash, "onRewardedVideoAdEnded");
        RVListenerWrapper.getInstance().onRewardedVideoAdEnded();
    }

    public void onRewardedVideoAdRewarded(ProgRvSmash progRvSmash, Placement placement) {
        logSmashCallback(progRvSmash, "onRewardedVideoAdRewarded");
        RVListenerWrapper.getInstance().onRewardedVideoAdRewarded(placement);
    }

    public void onRewardedVideoAdClicked(ProgRvSmash progRvSmash, Placement placement) {
        logSmashCallback(progRvSmash, "onRewardedVideoAdClicked");
        RVListenerWrapper.getInstance().onRewardedVideoAdClicked(placement);
    }

    private void reportAvailabilityIfNeeded(boolean z) {
        if (this.mLastReportedAvailabilityState == null || this.mLastReportedAvailabilityState.booleanValue() != z) {
            this.mLastReportedAvailabilityState = Boolean.valueOf(z);
            long time = new Date().getTime() - this.mLastChangedAvailabilityTime;
            this.mLastChangedAvailabilityTime = new Date().getTime();
            if (z) {
                sendMediationEvent(IronSourceConstants.RV_CALLBACK_AVAILABILITY_TRUE, new Object[][]{new Object[]{"duration", Long.valueOf(time)}});
            } else {
                sendMediationEvent(IronSourceConstants.RV_CALLBACK_AVAILABILITY_FALSE, new Object[][]{new Object[]{"duration", Long.valueOf(time)}});
            }
            RVListenerWrapper.getInstance().onRewardedVideoAvailabilityChanged(z);
        }
    }

    private void logSmashCallback(ProgRvSmash progRvSmash, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(progRvSmash.getInstanceName());
        sb.append(" : ");
        sb.append(str);
        String sb2 = sb.toString();
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("ProgRvManager: ");
        sb3.append(sb2);
        logger.log(ironSourceTag, sb3.toString(), 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("ProgRvManager: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void logErrorInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("ProgRvManager: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 3);
    }

    private void logAPIError(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.API, str, 3);
    }

    private void logApi(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.API, str, 1);
    }

    /* access modifiers changed from: private */
    public void sendMediationEventWithoutAuctionId(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false, false);
    }

    /* access modifiers changed from: private */
    public void sendMediationEventWithoutAuctionId(int i) {
        sendMediationEvent(i, null, false, false);
    }

    /* access modifiers changed from: private */
    public void sendMediationEvent(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false, true);
    }

    private void sendMediationEventWithPlacement(int i) {
        sendMediationEvent(i, null, true, true);
    }

    private void sendMediationEventWithPlacement(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, true, true);
    }

    private void sendMediationEvent(int i, Object[][] objArr, boolean z, boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_PROGRAMMATIC, Integer.valueOf(1));
        if (z2 && !TextUtils.isEmpty(this.mCurrentAuctionId)) {
            hashMap.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            hashMap.put(IronSourceConstants.EVENTS_PLACEMENT_NAME, this.mCurrentPlacement);
        }
        if (shouldAddAuctionParams(i)) {
            RewardedVideoEventsManager.getInstance().setEventAuctionParams(hashMap, this.mAuctionTrial, this.mAuctionFallback);
        }
        hashMap.put("sessionDepth", Integer.valueOf(this.mSessionDepth));
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    hashMap.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
                StringBuilder sb = new StringBuilder();
                sb.append("ProgRvManager: RV sendMediationEvent ");
                sb.append(Log.getStackTraceString(e));
                logger.log(ironSourceTag, sb.toString(), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    public synchronized void onLoadTriggered() {
        StringBuilder sb = new StringBuilder();
        sb.append("onLoadTriggered: RV load was triggered in ");
        sb.append(this.mState);
        sb.append(" state");
        logInternal(sb.toString());
        loadRewardedVideo(0);
    }

    public void onAuctionSuccess(List<AuctionResponseItem> list, String str, int i, long j) {
        logInternal("makeAuction(): success");
        this.mCurrentAuctionId = str;
        this.mAuctionTrial = i;
        this.mAuctionFallback = "";
        sendMediationEvent(IronSourceConstants.RV_AUCTION_SUCCESS, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        updateNextWaterfallToLoad(list);
        if (!this.mShouldLoadAfterClose || !this.mIsShowingVideo) {
            loadSmashesForNextWaterfall();
        }
    }

    public void onAuctionFailed(int i, String str, int i2, String str2, long j) {
        logInternal("Auction failed | moving to fallback waterfall");
        this.mAuctionTrial = i2;
        this.mAuctionFallback = str2;
        if (TextUtils.isEmpty(str)) {
            sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{"duration", Long.valueOf(j)}});
        } else {
            sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str}, new Object[]{"duration", Long.valueOf(j)}});
        }
        updateWaterfallToNonBidding();
        if (!this.mShouldLoadAfterClose || !this.mIsShowingVideo) {
            loadSmashesForNextWaterfall();
        }
    }

    public void onNetworkAvailabilityChanged(boolean z) {
        if (this.mShouldTrackNetworkState) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append("Network Availability Changed To: ");
            sb.append(z);
            logger.log(ironSourceTag, sb.toString(), 1);
            if (shouldNotifyNetworkAvailabilityChanged(z)) {
                RVListenerWrapper.getInstance().onRewardedVideoAvailabilityChanged(z);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void shouldTrackNetworkState(Context context, boolean z) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("Should Track Network State: ");
        sb.append(z);
        logger.log(ironSourceTag, sb.toString(), 0);
        this.mShouldTrackNetworkState = z;
        if (this.mShouldTrackNetworkState) {
            if (this.mNetworkStateReceiver == null) {
                this.mNetworkStateReceiver = new NetworkStateReceiver(context, this);
            }
            context.getApplicationContext().registerReceiver(this.mNetworkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } else if (this.mNetworkStateReceiver != null) {
            context.getApplicationContext().unregisterReceiver(this.mNetworkStateReceiver);
        }
    }

    private boolean shouldNotifyNetworkAvailabilityChanged(boolean z) {
        if (this.mLastMediationAvailabilityState == null) {
            return false;
        }
        boolean z2 = true;
        if (z && !this.mLastMediationAvailabilityState.booleanValue() && isRewardedVideoAvailable()) {
            this.mLastMediationAvailabilityState = Boolean.valueOf(true);
        } else if (z || !this.mLastMediationAvailabilityState.booleanValue()) {
            z2 = false;
        } else {
            this.mLastMediationAvailabilityState = Boolean.valueOf(false);
        }
        return z2;
    }
}
