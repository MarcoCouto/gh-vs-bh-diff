package com.ironsource.mediationsdk.server;

import android.util.Log;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.logger.ThreadExceptionHandler;
import org.json.JSONObject;

public class Server {
    /* access modifiers changed from: private */
    public static void callRequestURL(String str, boolean z, int i) {
        try {
            new JSONObject(HttpFunctions.getStringFromURL(ServerURL.getRequestURL(str, z, i)));
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.NETWORK;
            StringBuilder sb = new StringBuilder();
            sb.append("callRequestURL(reqUrl:");
            sb.append(str);
            sb.append(", hit:");
            sb.append(z);
            sb.append(")");
            logger.log(ironSourceTag, sb.toString(), 1);
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder("callRequestURL(reqUrl:");
            if (str == null) {
                sb2.append("null");
            } else {
                sb2.append(str);
            }
            sb2.append(", hit:");
            sb2.append(z);
            sb2.append(")");
            IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag2 = IronSourceTag.NETWORK;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb2.toString());
            sb3.append(", e:");
            sb3.append(Log.getStackTraceString(th));
            logger2.log(ironSourceTag2, sb3.toString(), 0);
        }
    }

    public static void callAsyncRequestURL(final String str, final boolean z, final int i) {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                Server.callRequestURL(str, z, i);
            }
        }, "callAsyncRequestURL");
        thread.setUncaughtExceptionHandler(new ThreadExceptionHandler());
        thread.start();
    }
}
