package com.ironsource.mediationsdk.server;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Pair;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Vector;

public class ServerURL {
    private static final String AMPERSAND = "&";
    private static final String ANDROID = "android";
    private static final String APPLICATION_KEY = "applicationKey";
    private static final String APPLICATION_USER_ID = "applicationUserId";
    private static final String APPLICATION_VERSION = "appVer";
    private static String BASE_URL_PREFIX = "https://init.supersonicads.com/sdk/v";
    private static String BASE_URL_SUFFIX = "?request=";
    private static final String CONNECTION_TYPE = "connType";
    private static final String DEVICE_MAKE = "devMake";
    private static final String DEVICE_MODEL = "devModel";
    private static final String EQUAL = "=";
    private static final String GAID = "advId";
    private static final String IMPRESSION = "impression";
    private static final String MEDIATION_TYPE = "mt";
    private static final String OS_VERSION = "osVer";
    private static final String PLACEMENT = "placementId";
    private static final String PLATFORM_KEY = "platform";
    private static final String PLUGIN_FW_VERSION = "plugin_fw_v";
    private static final String PLUGIN_TYPE = "pluginType";
    private static final String PLUGIN_VERSION = "pluginVersion";
    private static final String SDK_VERSION = "sdkVersion";
    private static final String SERR = "serr";

    public static String getCPVProvidersURL(Context context, String str, String str2, String str3, String str4, Vector<Pair<String, String>> vector) throws UnsupportedEncodingException {
        Vector vector2 = new Vector();
        vector2.add(new Pair("platform", "android"));
        vector2.add(new Pair("applicationKey", str));
        vector2.add(new Pair("applicationUserId", str2));
        vector2.add(new Pair("sdkVersion", IronSourceUtils.getSDKVersion()));
        if (IronSourceUtils.getSerr() == 0) {
            vector2.add(new Pair(SERR, String.valueOf(IronSourceUtils.getSerr())));
        }
        if (!TextUtils.isEmpty(ConfigFile.getConfigFile().getPluginType())) {
            vector2.add(new Pair(PLUGIN_TYPE, ConfigFile.getConfigFile().getPluginType()));
        }
        if (!TextUtils.isEmpty(ConfigFile.getConfigFile().getPluginVersion())) {
            vector2.add(new Pair(PLUGIN_VERSION, ConfigFile.getConfigFile().getPluginVersion()));
        }
        if (!TextUtils.isEmpty(ConfigFile.getConfigFile().getPluginFrameworkVersion())) {
            vector2.add(new Pair(PLUGIN_FW_VERSION, ConfigFile.getConfigFile().getPluginFrameworkVersion()));
        }
        if (!TextUtils.isEmpty(str3)) {
            vector2.add(new Pair(GAID, str3));
        }
        if (!TextUtils.isEmpty(str4)) {
            vector2.add(new Pair(MEDIATION_TYPE, str4));
        }
        String publisherApplicationVersion = ApplicationContext.getPublisherApplicationVersion(context, context.getPackageName());
        if (!TextUtils.isEmpty(publisherApplicationVersion)) {
            vector2.add(new Pair(APPLICATION_VERSION, publisherApplicationVersion));
        }
        int i = VERSION.SDK_INT;
        String str5 = OS_VERSION;
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append("");
        vector2.add(new Pair(str5, sb.toString()));
        vector2.add(new Pair(DEVICE_MAKE, Build.MANUFACTURER));
        vector2.add(new Pair(DEVICE_MODEL, Build.MODEL));
        String connectionType = IronSourceUtils.getConnectionType(context);
        if (!TextUtils.isEmpty(connectionType)) {
            vector2.add(new Pair(CONNECTION_TYPE, connectionType));
        }
        if (vector != null) {
            vector2.addAll(vector);
        }
        String encode = URLEncoder.encode(IronSourceAES.encode(IronSourceUtils.KEY, createURLParams(vector2)), "UTF-8");
        StringBuilder sb2 = new StringBuilder();
        sb2.append(getBaseUrl(IronSourceUtils.getSDKVersion()));
        sb2.append(encode);
        return sb2.toString();
    }

    public static String getRequestURL(String str, boolean z, int i) throws UnsupportedEncodingException {
        Vector vector = new Vector();
        vector.add(new Pair("impression", Boolean.toString(z)));
        vector.add(new Pair("placementId", Integer.toString(i)));
        String createURLParams = createURLParams(vector);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("&");
        sb.append(createURLParams);
        return sb.toString();
    }

    private static String createURLParams(Vector<Pair<String, String>> vector) throws UnsupportedEncodingException {
        String str = "";
        Iterator it = vector.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            if (str.length() > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("&");
                str = sb.toString();
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append((String) pair.first);
            sb2.append("=");
            sb2.append(URLEncoder.encode((String) pair.second, "UTF-8"));
            str = sb2.toString();
        }
        return str;
    }

    private static String getBaseUrl(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(BASE_URL_PREFIX);
        sb.append(str);
        sb.append(BASE_URL_SUFFIX);
        return sb.toString();
    }

    private static void setBaseUrlPrefix(String str) {
        BASE_URL_PREFIX = str;
    }
}
