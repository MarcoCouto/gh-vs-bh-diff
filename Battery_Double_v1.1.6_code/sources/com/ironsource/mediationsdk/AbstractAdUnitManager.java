package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.BaseApi;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.concurrent.CopyOnWriteArrayList;

abstract class AbstractAdUnitManager implements BaseApi {
    final String KEY_PLACEMENT = IronSourceConstants.EVENTS_PLACEMENT_NAME;
    final String KEY_PROVIDER_PRIORITY = "providerPriority";
    final String KEY_REASON = IronSourceConstants.EVENTS_ERROR_REASON;
    final String KEY_REWARD_AMOUNT = IronSourceConstants.EVENTS_REWARD_AMOUNT;
    final String KEY_REWARD_NAME = IronSourceConstants.EVENTS_REWARD_NAME;
    final String KEY_STATUS = "status";
    Activity mActivity;
    String mAppKey;
    boolean mBackFillInitStarted;
    private AbstractSmash mBackfillSmash;
    boolean mCanShowPremium = true;
    DailyCappingManager mDailyCappingManager = null;
    Boolean mLastMediationAvailabilityState;
    IronSourceLoggerManager mLoggerManager = IronSourceLoggerManager.getLogger();
    private AbstractSmash mPremiumSmash;
    boolean mShouldTrackNetworkState = false;
    int mSmartLoadAmount;
    final CopyOnWriteArrayList<AbstractSmash> mSmashArray = new CopyOnWriteArrayList<>();
    String mUserId;

    public void setMediationSegment(String str) {
    }

    /* access modifiers changed from: 0000 */
    public abstract void shouldTrackNetworkState(Context context, boolean z);

    AbstractAdUnitManager() {
    }

    public void onResume(Activity activity) {
        if (activity != null) {
            this.mActivity = activity;
        }
    }

    /* access modifiers changed from: 0000 */
    public void setSmartLoadAmount(int i) {
        this.mSmartLoadAmount = i;
    }

    /* access modifiers changed from: 0000 */
    public void addSmashToArray(AbstractSmash abstractSmash) {
        this.mSmashArray.add(abstractSmash);
        if (this.mDailyCappingManager != null) {
            this.mDailyCappingManager.addSmash(abstractSmash);
        }
    }

    /* access modifiers changed from: 0000 */
    public void setBackfillSmash(AbstractSmash abstractSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(abstractSmash.getInstanceName());
        sb.append(" is set as backfill");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
        this.mBackfillSmash = abstractSmash;
    }

    /* access modifiers changed from: 0000 */
    public void setPremiumSmash(AbstractSmash abstractSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(abstractSmash.getInstanceName());
        sb.append(" is set as premium");
        ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 0);
        this.mPremiumSmash = abstractSmash;
    }

    /* access modifiers changed from: 0000 */
    public AbstractSmash getBackfillSmash() {
        return this.mBackfillSmash;
    }

    /* access modifiers changed from: 0000 */
    public AbstractSmash getPremiumSmash() {
        return this.mPremiumSmash;
    }

    /* access modifiers changed from: 0000 */
    public void setCustomParams(AbstractSmash abstractSmash) {
        try {
            String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
            if (!TextUtils.isEmpty(mediationSegment)) {
                abstractSmash.setMediationSegment(mediationSegment);
            }
            String pluginType = ConfigFile.getConfigFile().getPluginType();
            if (!TextUtils.isEmpty(pluginType)) {
                abstractSmash.setPluginData(pluginType, ConfigFile.getConfigFile().getPluginFrameworkVersion());
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb = new StringBuilder();
            sb.append(":setCustomParams():");
            sb.append(e.toString());
            ironSourceLoggerManager.log(ironSourceTag, sb.toString(), 3);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean canShowPremium() {
        return this.mCanShowPremium;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void disablePremiumForCurrentSession() {
        this.mCanShowPremium = false;
    }
}
