package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;

public class RVListenerWrapper {
    private static final RVListenerWrapper sInstance = new RVListenerWrapper();
    /* access modifiers changed from: private */
    public RewardedVideoListener mListener = null;

    public static synchronized RVListenerWrapper getInstance() {
        RVListenerWrapper rVListenerWrapper;
        synchronized (RVListenerWrapper.class) {
            rVListenerWrapper = sInstance;
        }
        return rVListenerWrapper;
    }

    private RVListenerWrapper() {
    }

    /* access modifiers changed from: private */
    public String getPlacementName(Placement placement) {
        return placement == null ? "" : placement.getPlacementName();
    }

    public synchronized void setListener(RewardedVideoListener rewardedVideoListener) {
        this.mListener = rewardedVideoListener;
    }

    public synchronized void onRewardedVideoAdOpened() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdOpened();
                        RVListenerWrapper.this.log("onRewardedVideoAdOpened()");
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdClosed() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdClosed();
                        RVListenerWrapper.this.log("onRewardedVideoAdClosed()");
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAvailabilityChanged(final boolean z) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAvailabilityChanged(z);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onRewardedVideoAvailabilityChanged() available=");
                        sb.append(z);
                        rVListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdStarted() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdStarted();
                        RVListenerWrapper.this.log("onRewardedVideoAdStarted()");
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdEnded() {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdEnded();
                        RVListenerWrapper.this.log("onRewardedVideoAdEnded()");
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdRewarded(final Placement placement) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdRewarded(placement);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onRewardedVideoAdRewarded() placement=");
                        sb.append(RVListenerWrapper.this.getPlacementName(placement));
                        rVListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdShowFailed(final IronSourceError ironSourceError) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdShowFailed(ironSourceError);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onRewardedVideoAdShowFailed() error=");
                        sb.append(ironSourceError.getErrorMessage());
                        rVListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    public synchronized void onRewardedVideoAdClicked(final Placement placement) {
        if (this.mListener != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.mListener.onRewardedVideoAdClicked(placement);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onRewardedVideoAdClicked() placement=");
                        sb.append(RVListenerWrapper.this.getPlacementName(placement));
                        rVListenerWrapper.log(sb.toString());
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceTag.CALLBACK, str, 1);
    }
}
