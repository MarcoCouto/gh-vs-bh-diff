package com.ironsource.mediationsdk.utils;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.mediationsdk.IronSource.AD_UNIT;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.ConsoleLogger;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ApplicationConfigurations;
import com.ironsource.mediationsdk.model.ApplicationEvents;
import com.ironsource.mediationsdk.model.ApplicationLogger;
import com.ironsource.mediationsdk.model.BannerConfigurations;
import com.ironsource.mediationsdk.model.BannerPlacement;
import com.ironsource.mediationsdk.model.Configurations;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.OfferwallConfigurations;
import com.ironsource.mediationsdk.model.OfferwallPlacement;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.PlacementAvailabilitySettings;
import com.ironsource.mediationsdk.model.PlacementAvailabilitySettings.PlacementAvailabilitySettingsBuilder;
import com.ironsource.mediationsdk.model.PlacementCappingType;
import com.ironsource.mediationsdk.model.ProviderOrder;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.ProviderSettingsHolder;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.model.ServerSegmetData;
import com.ironsource.sdk.constants.Constants;
import com.smaato.sdk.core.api.VideoType;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerResponseWrapper {
    public static final String APP_KEY_FIELD = "appKey";
    public static final String RESPONSE_FIELD = "response";
    public static final String USER_ID_FIELD = "userId";
    private final String AB_TESTING = "abt";
    private final String ADAPTER_TIMEOUT_IN_MILLIS_FIELD = "atim";
    private final String ADAPTER_TIMEOUT_IN_SECS_FIELD = "adapterTimeOutInSeconds";
    private final String AD_SOURCE_NAME_FIELD = "adSourceName";
    private final String AD_UNITS_FIELD = "adUnits";
    private final String APPLICATION_FIELD = "application";
    private final String AUCTION_DATA_FIELD = "auctionData";
    private final String AUCTION_FIELD = "auction";
    private final String AUCTION_PROGRAMMATIC_FIELD = IronSourceConstants.EVENTS_PROGRAMMATIC;
    private final String AUCTION_RETRY_INTERVAL_FIELD = "auctionRetryInterval";
    private final String AUCTION_SAVED_HISTORY_LIMIT_FIELD = "auctionSavedHistory";
    private final String AUCTION_TIMEOUT_FIELD = "auctionTimeout";
    private final String AUCTION_TRIALS_FIELD = IronSourceConstants.AUCTION_TRIALS;
    private final String AUCTION_URL_FIELD = "auctioneerURL";
    private final String BACKFILL_FIELD = "backFill";
    private final String BACKUP_THRESHOLD_FIELD = "backupThreshold";
    private final String BN_FIELD = "banner";
    private final String CONFIGURATIONS_FIELD = TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS;
    private final String CONSOLE_FIELD = ConsoleLogger.NAME;
    private final int DEFAULT_ADAPTERS_SMARTLOAD_AMOUNT = 2;
    private final int DEFAULT_ADAPTERS_SMARTLOAD_TIMEOUT = 60;
    private final int DEFAULT_AUCTION_SAVED_HISTORY_LIMIT = 15;
    private final String DEFAULT_BANNER_LOAD_REFRESH_INTERVAL = "bannerInterval";
    private final int DEFAULT_BANNER_SMARTLOAD_TIMEOUT = 10000;
    private final int DEFAULT_BN_DELAY_LOAD_FAILURE_TIMEOUT = 3;
    private final int DEFAULT_IS_DELAY_LOAD_FAILURE_TIMEOUT = 3;
    private final int DEFAULT_LOG_LEVEL = 3;
    private final int DEFAULT_MANUAL_LOAD_INTERVAL_FIELD = 300;
    private final int DEFAULT_MAX_EVENTS_PER_BATCH = 5000;
    private final long DEFAULT_TIMEOUT = TapjoyConstants.TIMER_INCREMENT;
    private final int DEFAULT_TRIALS = 2;
    private final String DELAY_LOAD_FAILURE = "delayLoadFailure";
    private final String ERROR_KEY = "error";
    private final String EVENTS_FIELD = EventEntry.TABLE_NAME;
    private final String GENERIC_PARAMS_FIELD = "genericParams";
    private final String INTEGRATION_FIELD = "integration";
    private final String IS_AUCTION_ON_SHOW_START_FIELD = "isAuctionOnShowStart";
    private final String IS_FIELD = VideoType.INTERSTITIAL;
    private final String IS_LOAD_WHILE_SHOW_FIELD = "isLoadWhileShow";
    private final String IS_MULTIPLE_INSTANCES_FIELD = "mpis";
    private final String LOGGERS_FIELD = "loggers";
    private final String MANUAL_LOAD_INTERVAL_FIELD = "loadRVInterval";
    private final String MAX_EVENTS_PER_BATCH = "maxEventsPerBatch";
    private final String MAX_NUM_OF_ADAPTERS_TO_LOAD_ON_START_FIELD = "maxNumOfAdaptersToLoadOnStart";
    private final String MAX_NUM_OF_EVENTS_FIELD = "maxNumberOfEvents";
    private final String MIN_TIME_BEFORE_FIRST_AUCTION_FIELD = "minTimeBeforeFirstAuction";
    private final String NON_CONNECTIVITY_EVENTS_FIELD = "nonConnectivityEvents";
    private final String OPT_IN_EVENTS_FIELD = "optIn";
    private final String OPT_OUT_EVENTS_FIELD = "optOut";
    private final String OW_FIELD = "offerwall";
    private final String PLACEMENTS_FIELD = "placements";
    private final String PLACEMENT_ID_FIELD = Constants.PLACEMENT_ID;
    private final String PLACEMENT_NAME_FIELD = "placementName";
    private final String PLACEMENT_SETTINGS_CAPPING_FIELD = "capping";
    private final String PLACEMENT_SETTINGS_CAPPING_UNIT_FIELD = "unit";
    private final String PLACEMENT_SETTINGS_CAPPING_VALUE_FIELD = "maxImpressions";
    private final String PLACEMENT_SETTINGS_DELIVERY_FIELD = "delivery";
    private final String PLACEMENT_SETTINGS_ENABLED_FIELD = String.ENABLED;
    private final String PLACEMENT_SETTINGS_IS_DEFAULT_FIELD = "isDefault";
    private final String PLACEMENT_SETTINGS_PACING_FIELD = "pacing";
    private final String PLACEMENT_SETTINGS_PACING_VALUE_FIELD = "numOfSeconds";
    private final String PREMIUM_FIELD = "premium";
    private final String PROVIDER_LOAD_NAME_FIELD = "providerLoadName";
    private final String PROVIDER_ORDER_FIELD = "providerOrder";
    private final String PROVIDER_SETTINGS_FIELD = "providerSettings";
    private final String PUBLISHER_FIELD = "publisher";
    private final String RV_FIELD = "rewardedVideo";
    private final String SEGMENT_FIELD = "segment";
    private final String SEND_EVENTS_TOGGLE_FIELD = "sendEventsToggle";
    private final String SEND_ULTRA_EVENTS_FIELD = "sendUltraEvents";
    private final String SERVER_EVENTS_TYPE = "serverEventsType";
    private final String SERVER_EVENTS_URL_FIELD = "serverEventsURL";
    private final String SERVER_FIELD = "server";
    private final String SUB_PROVIDER_ID_FIELD = "spId";
    private final String TIME_TO_WAIT_BEFORE_AUCTION_FIELD = "timeToWaitBeforeAuction";
    private final String TIME_TO_WAIT_BEFORE_LOAD_FIELD = "timeToWaitBeforeLoad";
    private final String TRIGGER_EVENTS_FIELD = "triggerEvents";
    private final String UUID_ENABLED_FIELD = DeviceStatus.UUID_ENABLED;
    private final String VIRTUAL_ITEM_COUNT_FIELD = "virtualItemCount";
    private final String VIRTUAL_ITEM_NAME_FIELD = "virtualItemName";
    private String mAppKey;
    private Configurations mConfigurations;
    private Context mContext;
    private ProviderOrder mProviderOrder;
    private ProviderSettingsHolder mProviderSettingsHolder;
    private JSONObject mResponse;
    private String mUserId;

    public ServerResponseWrapper(Context context, String str, String str2, String str3) {
        this.mContext = context;
        try {
            if (TextUtils.isEmpty(str3)) {
                this.mResponse = new JSONObject();
            } else {
                this.mResponse = new JSONObject(str3);
            }
            parseProviderSettings();
            parseConfigurations();
            parseProviderOrder();
            if (TextUtils.isEmpty(str)) {
                str = "";
            }
            this.mAppKey = str;
            if (TextUtils.isEmpty(str2)) {
                str2 = "";
            }
            this.mUserId = str2;
        } catch (JSONException e) {
            e.printStackTrace();
            defaultInit();
        }
    }

    public ServerResponseWrapper(ServerResponseWrapper serverResponseWrapper) {
        try {
            this.mContext = serverResponseWrapper.getContext();
            this.mResponse = new JSONObject(serverResponseWrapper.mResponse.toString());
            this.mAppKey = serverResponseWrapper.mAppKey;
            this.mUserId = serverResponseWrapper.mUserId;
            this.mProviderOrder = serverResponseWrapper.getProviderOrder();
            this.mProviderSettingsHolder = serverResponseWrapper.getProviderSettingsHolder();
            this.mConfigurations = serverResponseWrapper.getConfigurations();
        } catch (Exception unused) {
            defaultInit();
        }
    }

    private void defaultInit() {
        this.mResponse = new JSONObject();
        this.mAppKey = "";
        this.mUserId = "";
        this.mProviderOrder = new ProviderOrder();
        this.mProviderSettingsHolder = ProviderSettingsHolder.getProviderSettingsHolder();
        this.mConfigurations = new Configurations();
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(APP_KEY_FIELD, this.mAppKey);
            jSONObject.put(USER_ID_FIELD, this.mUserId);
            jSONObject.put(RESPONSE_FIELD, this.mResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public boolean isValidResponse() {
        if (!((((this.mResponse != null) && !this.mResponse.has("error")) && this.mProviderOrder != null) && this.mProviderSettingsHolder != null) || this.mConfigurations == null) {
            return false;
        }
        return true;
    }

    public List<AD_UNIT> getInitiatedAdUnits() {
        if (this.mResponse == null || this.mConfigurations == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!(this.mConfigurations.getRewardedVideoConfigurations() == null || this.mProviderOrder == null || this.mProviderOrder.getRewardedVideoProviderOrder().size() <= 0)) {
            arrayList.add(AD_UNIT.REWARDED_VIDEO);
        }
        if (!(this.mConfigurations.getInterstitialConfigurations() == null || this.mProviderOrder == null || this.mProviderOrder.getInterstitialProviderOrder().size() <= 0)) {
            arrayList.add(AD_UNIT.INTERSTITIAL);
        }
        if (this.mConfigurations.getOfferwallConfigurations() != null) {
            arrayList.add(AD_UNIT.OFFERWALL);
        }
        if (this.mConfigurations.getBannerConfigurations() != null) {
            arrayList.add(AD_UNIT.BANNER);
        }
        return arrayList;
    }

    private void parseProviderOrder() {
        try {
            JSONObject section = getSection(this.mResponse, "providerOrder");
            JSONArray optJSONArray = section.optJSONArray("rewardedVideo");
            JSONArray optJSONArray2 = section.optJSONArray(VideoType.INTERSTITIAL);
            JSONArray optJSONArray3 = section.optJSONArray("banner");
            this.mProviderOrder = new ProviderOrder();
            if (!(optJSONArray == null || getConfigurations() == null || getConfigurations().getRewardedVideoConfigurations() == null)) {
                String backFillProviderName = getConfigurations().getRewardedVideoConfigurations().getBackFillProviderName();
                String premiumProviderName = getConfigurations().getRewardedVideoConfigurations().getPremiumProviderName();
                for (int i = 0; i < optJSONArray.length(); i++) {
                    String optString = optJSONArray.optString(i);
                    if (optString.equals(backFillProviderName)) {
                        this.mProviderOrder.setRVBackFillProvider(backFillProviderName);
                    } else {
                        if (optString.equals(premiumProviderName)) {
                            this.mProviderOrder.setRVPremiumProvider(premiumProviderName);
                        }
                        this.mProviderOrder.addRewardedVideoProvider(optString);
                        ProviderSettings providerSettings = ProviderSettingsHolder.getProviderSettingsHolder().getProviderSettings(optString);
                        if (providerSettings != null) {
                            providerSettings.setRewardedVideoPriority(i);
                        }
                    }
                }
            }
            if (!(optJSONArray2 == null || getConfigurations() == null || getConfigurations().getInterstitialConfigurations() == null)) {
                String backFillProviderName2 = getConfigurations().getInterstitialConfigurations().getBackFillProviderName();
                String premiumProviderName2 = getConfigurations().getInterstitialConfigurations().getPremiumProviderName();
                for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                    String optString2 = optJSONArray2.optString(i2);
                    if (optString2.equals(backFillProviderName2)) {
                        this.mProviderOrder.setISBackFillProvider(backFillProviderName2);
                    } else {
                        if (optString2.equals(premiumProviderName2)) {
                            this.mProviderOrder.setISPremiumProvider(premiumProviderName2);
                        }
                        this.mProviderOrder.addInterstitialProvider(optString2);
                        ProviderSettings providerSettings2 = ProviderSettingsHolder.getProviderSettingsHolder().getProviderSettings(optString2);
                        if (providerSettings2 != null) {
                            providerSettings2.setInterstitialPriority(i2);
                        }
                    }
                }
            }
            if (optJSONArray3 != null) {
                for (int i3 = 0; i3 < optJSONArray3.length(); i3++) {
                    String optString3 = optJSONArray3.optString(i3);
                    this.mProviderOrder.addBannerProvider(optString3);
                    ProviderSettings providerSettings3 = ProviderSettingsHolder.getProviderSettingsHolder().getProviderSettings(optString3);
                    if (providerSettings3 != null) {
                        providerSettings3.setBannerPriority(i3);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseProviderSettings() {
        try {
            this.mProviderSettingsHolder = ProviderSettingsHolder.getProviderSettingsHolder();
            JSONObject section = getSection(this.mResponse, "providerSettings");
            Iterator keys = section.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                JSONObject optJSONObject = section.optJSONObject(str);
                if (optJSONObject != null) {
                    boolean optBoolean = optJSONObject.optBoolean("mpis", false);
                    String optString = optJSONObject.optString("spId", "0");
                    String optString2 = optJSONObject.optString("adSourceName", null);
                    String optString3 = optJSONObject.optString("providerLoadName", str);
                    JSONObject section2 = getSection(optJSONObject, "adUnits");
                    JSONObject section3 = getSection(optJSONObject, "application");
                    JSONObject section4 = getSection(section2, "rewardedVideo");
                    JSONObject section5 = getSection(section2, VideoType.INTERSTITIAL);
                    JSONObject section6 = getSection(section2, "banner");
                    JSONObject mergeJsons = IronSourceUtils.mergeJsons(section4, section3);
                    JSONObject mergeJsons2 = IronSourceUtils.mergeJsons(section5, section3);
                    JSONObject mergeJsons3 = IronSourceUtils.mergeJsons(section6, section3);
                    if (this.mProviderSettingsHolder.containsProviderSettings(str)) {
                        ProviderSettings providerSettings = this.mProviderSettingsHolder.getProviderSettings(str);
                        JSONObject rewardedVideoSettings = providerSettings.getRewardedVideoSettings();
                        JSONObject interstitialSettings = providerSettings.getInterstitialSettings();
                        JSONObject bannerSettings = providerSettings.getBannerSettings();
                        providerSettings.setRewardedVideoSettings(IronSourceUtils.mergeJsons(rewardedVideoSettings, mergeJsons));
                        providerSettings.setInterstitialSettings(IronSourceUtils.mergeJsons(interstitialSettings, mergeJsons2));
                        providerSettings.setBannerSettings(IronSourceUtils.mergeJsons(bannerSettings, mergeJsons3));
                        providerSettings.setIsMultipleInstances(optBoolean);
                        providerSettings.setSubProviderId(optString);
                        providerSettings.setAdSourceNameForEvents(optString2);
                    } else if (shouldMergeWithDebugSettings(optString3)) {
                        ProviderSettings providerSettings2 = this.mProviderSettingsHolder.getProviderSettings("Mediation");
                        JSONObject rewardedVideoSettings2 = providerSettings2.getRewardedVideoSettings();
                        JSONObject interstitialSettings2 = providerSettings2.getInterstitialSettings();
                        JSONObject bannerSettings2 = providerSettings2.getBannerSettings();
                        JSONObject jSONObject = new JSONObject(rewardedVideoSettings2.toString());
                        JSONObject jSONObject2 = new JSONObject(interstitialSettings2.toString());
                        JSONObject jSONObject3 = new JSONObject(bannerSettings2.toString());
                        ProviderSettings providerSettings3 = new ProviderSettings(str, optString3, section3, IronSourceUtils.mergeJsons(jSONObject, mergeJsons), IronSourceUtils.mergeJsons(jSONObject2, mergeJsons2), IronSourceUtils.mergeJsons(jSONObject3, mergeJsons3));
                        providerSettings3.setIsMultipleInstances(optBoolean);
                        providerSettings3.setSubProviderId(optString);
                        providerSettings3.setAdSourceNameForEvents(optString2);
                        this.mProviderSettingsHolder.addProviderSettings(providerSettings3);
                    } else {
                        ProviderSettings providerSettings4 = new ProviderSettings(str, optString3, section3, mergeJsons, mergeJsons2, mergeJsons3);
                        providerSettings4.setIsMultipleInstances(optBoolean);
                        providerSettings4.setSubProviderId(optString);
                        providerSettings4.setAdSourceNameForEvents(optString2);
                        this.mProviderSettingsHolder.addProviderSettings(providerSettings4);
                    }
                }
            }
            this.mProviderSettingsHolder.fillSubProvidersDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean shouldMergeWithDebugSettings(String str) {
        String lowerCase = str.toLowerCase();
        return this.mProviderSettingsHolder.containsProviderSettings("Mediation") && (IronSourceConstants.SUPERSONIC_CONFIG_NAME.toLowerCase().equals(lowerCase) || IronSourceConstants.IRONSOURCE_CONFIG_NAME.toLowerCase().equals(lowerCase) || IronSourceConstants.RIS_CONFIG_NAME.toLowerCase().equals(lowerCase));
    }

    private void parseConfigurations() {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        JSONObject jSONObject3;
        JSONObject jSONObject4;
        RewardedVideoConfigurations rewardedVideoConfigurations;
        InterstitialConfigurations interstitialConfigurations;
        BannerConfigurations bannerConfigurations;
        int i;
        int i2;
        OfferwallConfigurations offerwallConfigurations;
        ServerSegmetData serverSegmetData;
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        int[] iArr5;
        int[] iArr6;
        int[] iArr7;
        int[] iArr8;
        int[] iArr9;
        int[] iArr10;
        int[] iArr11;
        int[] iArr12;
        AuctionSettings auctionSettings;
        int[] iArr13;
        int[] iArr14;
        int[] iArr15;
        int[] iArr16;
        AuctionSettings auctionSettings2;
        try {
            JSONObject section = getSection(this.mResponse, TapjoyConstants.PREF_SERVER_PROVIDED_CONFIGURATIONS);
            JSONObject section2 = getSection(section, "adUnits");
            JSONObject section3 = getSection(section, "application");
            JSONObject section4 = getSection(section2, "rewardedVideo");
            JSONObject section5 = getSection(section2, VideoType.INTERSTITIAL);
            JSONObject section6 = getSection(section2, "offerwall");
            JSONObject section7 = getSection(section2, "banner");
            JSONObject section8 = getSection(section3, EventEntry.TABLE_NAME);
            JSONObject section9 = getSection(section3, "loggers");
            JSONObject section10 = getSection(section3, "segment");
            JSONObject section11 = getSection(section3, "auction");
            if (section3 != null) {
                IronSourceUtils.saveBooleanToSharedPrefs(this.mContext, DeviceStatus.UUID_ENABLED, section3.optBoolean(DeviceStatus.UUID_ENABLED, true));
            }
            if (section8 != null) {
                String optString = section8.optString("abt");
                if (!TextUtils.isEmpty(optString)) {
                    InterstitialEventsManager.getInstance().setABT(optString);
                    RewardedVideoEventsManager.getInstance().setABT(optString);
                }
            }
            if (section4 != null) {
                JSONArray optJSONArray = section4.optJSONArray("placements");
                JSONObject section12 = getSection(section4, EventEntry.TABLE_NAME);
                int intConfigValue = getIntConfigValue(section4, section3, "maxNumOfAdaptersToLoadOnStart", 2);
                int intConfigValue2 = getIntConfigValue(section4, section3, "adapterTimeOutInSeconds", 60);
                int intConfigValue3 = getIntConfigValue(section4, section3, "loadRVInterval", 300);
                JSONObject mergeJsons = IronSourceUtils.mergeJsons(section12, section8);
                boolean optBoolean = mergeJsons.optBoolean("sendUltraEvents", false);
                boolean optBoolean2 = mergeJsons.optBoolean("sendEventsToggle", false);
                String optString2 = mergeJsons.optString("serverEventsURL", "");
                String optString3 = mergeJsons.optString("serverEventsType", "");
                int optInt = mergeJsons.optInt("backupThreshold", -1);
                int optInt2 = mergeJsons.optInt("maxNumberOfEvents", -1);
                int optInt3 = mergeJsons.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray2 = mergeJsons.optJSONArray("optOut");
                if (optJSONArray2 != null) {
                    int[] iArr17 = new int[optJSONArray2.length()];
                    for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                        iArr17[i3] = optJSONArray2.optInt(i3);
                    }
                    iArr13 = iArr17;
                } else {
                    iArr13 = null;
                }
                JSONArray optJSONArray3 = mergeJsons.optJSONArray("optIn");
                if (optJSONArray3 != null) {
                    int[] iArr18 = new int[optJSONArray3.length()];
                    for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                        iArr18[i4] = optJSONArray3.optInt(i4);
                    }
                    iArr14 = iArr18;
                } else {
                    iArr14 = null;
                }
                JSONArray optJSONArray4 = mergeJsons.optJSONArray("triggerEvents");
                if (optJSONArray4 != null) {
                    int[] iArr19 = new int[optJSONArray4.length()];
                    for (int i5 = 0; i5 < optJSONArray4.length(); i5++) {
                        iArr19[i5] = optJSONArray4.optInt(i5);
                    }
                    iArr15 = iArr19;
                } else {
                    iArr15 = null;
                }
                JSONArray optJSONArray5 = mergeJsons.optJSONArray("nonConnectivityEvents");
                if (optJSONArray5 != null) {
                    int[] iArr20 = new int[optJSONArray5.length()];
                    for (int i6 = 0; i6 < optJSONArray5.length(); i6++) {
                        iArr20[i6] = optJSONArray5.optInt(i6);
                    }
                    iArr16 = iArr20;
                } else {
                    iArr16 = null;
                }
                ApplicationEvents applicationEvents = new ApplicationEvents(optBoolean, optBoolean2, optString2, optString3, optInt, optInt2, optInt3, iArr13, iArr14, iArr15, iArr16);
                if (section11 != null) {
                    JSONObject section13 = getSection(section11, "rewardedVideo");
                    String optString4 = section11.optString("auctionData", "");
                    String optString5 = section11.optString("auctioneerURL", "");
                    int optInt4 = section11.optInt(IronSourceConstants.AUCTION_TRIALS, 2);
                    jSONObject4 = section10;
                    long optLong = section11.optLong("auctionTimeout", TapjoyConstants.TIMER_INCREMENT);
                    int optInt5 = section11.optInt("auctionSavedHistory", 15);
                    boolean optBoolean3 = section13.optBoolean(IronSourceConstants.EVENTS_PROGRAMMATIC, false);
                    int optInt6 = section13.optInt("minTimeBeforeFirstAuction", 2000);
                    int optInt7 = section13.optInt("auctionRetryInterval", 30000);
                    int optInt8 = section13.optInt("timeToWaitBeforeAuction", 5000);
                    jSONObject3 = section9;
                    jSONObject2 = section6;
                    jSONObject = section7;
                    auctionSettings2 = new AuctionSettings(optString4, optString5, optInt4, optInt5, optLong, optBoolean3, (long) optInt6, (long) optInt7, (long) optInt8, (long) section13.optInt("timeToWaitBeforeLoad", 50), section13.optBoolean("isAuctionOnShowStart", false), section13.optBoolean("isLoadWhileShow", false));
                } else {
                    jSONObject2 = section6;
                    jSONObject = section7;
                    jSONObject3 = section9;
                    jSONObject4 = section10;
                    auctionSettings2 = new AuctionSettings();
                }
                RewardedVideoConfigurations rewardedVideoConfigurations2 = new RewardedVideoConfigurations(intConfigValue, intConfigValue2, intConfigValue3, applicationEvents, auctionSettings2);
                if (optJSONArray != null) {
                    for (int i7 = 0; i7 < optJSONArray.length(); i7++) {
                        Placement parseSingleRVPlacement = parseSingleRVPlacement(optJSONArray.optJSONObject(i7));
                        if (parseSingleRVPlacement != null) {
                            rewardedVideoConfigurations2.addRewardedVideoPlacement(parseSingleRVPlacement);
                        }
                    }
                }
                String optString6 = section4.optString("backFill");
                if (!TextUtils.isEmpty(optString6)) {
                    rewardedVideoConfigurations2.setBackFillProviderName(optString6);
                }
                String optString7 = section4.optString("premium");
                if (!TextUtils.isEmpty(optString7)) {
                    rewardedVideoConfigurations2.setPremiumProviderName(optString7);
                }
                rewardedVideoConfigurations = rewardedVideoConfigurations2;
            } else {
                jSONObject2 = section6;
                jSONObject = section7;
                jSONObject3 = section9;
                jSONObject4 = section10;
                rewardedVideoConfigurations = null;
            }
            if (section5 != null) {
                JSONArray optJSONArray6 = section5.optJSONArray("placements");
                JSONObject section14 = getSection(section5, EventEntry.TABLE_NAME);
                int intConfigValue4 = getIntConfigValue(section5, section3, "maxNumOfAdaptersToLoadOnStart", 2);
                int intConfigValue5 = getIntConfigValue(section5, section3, "adapterTimeOutInSeconds", 60);
                int intConfigValue6 = getIntConfigValue(section5, section3, "delayLoadFailure", 3);
                JSONObject mergeJsons2 = IronSourceUtils.mergeJsons(section14, section8);
                boolean optBoolean4 = mergeJsons2.optBoolean("sendEventsToggle", false);
                String optString8 = mergeJsons2.optString("serverEventsURL", "");
                String optString9 = mergeJsons2.optString("serverEventsType", "");
                int optInt9 = mergeJsons2.optInt("backupThreshold", -1);
                int optInt10 = mergeJsons2.optInt("maxNumberOfEvents", -1);
                int optInt11 = mergeJsons2.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray7 = mergeJsons2.optJSONArray("optOut");
                if (optJSONArray7 != null) {
                    int[] iArr21 = new int[optJSONArray7.length()];
                    for (int i8 = 0; i8 < optJSONArray7.length(); i8++) {
                        iArr21[i8] = optJSONArray7.optInt(i8);
                    }
                    iArr9 = iArr21;
                } else {
                    iArr9 = null;
                }
                JSONArray optJSONArray8 = mergeJsons2.optJSONArray("optIn");
                if (optJSONArray8 != null) {
                    int[] iArr22 = new int[optJSONArray8.length()];
                    for (int i9 = 0; i9 < optJSONArray8.length(); i9++) {
                        iArr22[i9] = optJSONArray8.optInt(i9);
                    }
                    iArr10 = iArr22;
                } else {
                    iArr10 = null;
                }
                JSONArray optJSONArray9 = mergeJsons2.optJSONArray("triggerEvents");
                if (optJSONArray9 != null) {
                    int[] iArr23 = new int[optJSONArray9.length()];
                    for (int i10 = 0; i10 < optJSONArray9.length(); i10++) {
                        iArr23[i10] = optJSONArray9.optInt(i10);
                    }
                    iArr11 = iArr23;
                } else {
                    iArr11 = null;
                }
                JSONArray optJSONArray10 = mergeJsons2.optJSONArray("nonConnectivityEvents");
                if (optJSONArray10 != null) {
                    int[] iArr24 = new int[optJSONArray10.length()];
                    for (int i11 = 0; i11 < optJSONArray10.length(); i11++) {
                        iArr24[i11] = optJSONArray10.optInt(i11);
                    }
                    iArr12 = iArr24;
                } else {
                    iArr12 = null;
                }
                ApplicationEvents applicationEvents2 = new ApplicationEvents(false, optBoolean4, optString8, optString9, optInt9, optInt10, optInt11, iArr9, iArr10, iArr11, iArr12);
                if (section11 != null) {
                    JSONObject section15 = getSection(section11, VideoType.INTERSTITIAL);
                    AuctionSettings auctionSettings3 = new AuctionSettings(section11.optString("auctionData", ""), section11.optString("auctioneerURL", ""), section11.optInt(IronSourceConstants.AUCTION_TRIALS, 2), section11.optInt("auctionSavedHistory", 15), section11.optLong("auctionTimeout", TapjoyConstants.TIMER_INCREMENT), section15.optBoolean(IronSourceConstants.EVENTS_PROGRAMMATIC, false), (long) section15.optInt("minTimeBeforeFirstAuction", 2000), 0, 0, 0, true, true);
                    auctionSettings = auctionSettings3;
                } else {
                    auctionSettings = new AuctionSettings();
                }
                InterstitialConfigurations interstitialConfigurations2 = new InterstitialConfigurations(intConfigValue4, intConfigValue5, applicationEvents2, auctionSettings, intConfigValue6);
                if (optJSONArray6 != null) {
                    for (int i12 = 0; i12 < optJSONArray6.length(); i12++) {
                        InterstitialPlacement parseSingleISPlacement = parseSingleISPlacement(optJSONArray6.optJSONObject(i12));
                        if (parseSingleISPlacement != null) {
                            interstitialConfigurations2.addInterstitialPlacement(parseSingleISPlacement);
                        }
                    }
                }
                String optString10 = section5.optString("backFill");
                if (!TextUtils.isEmpty(optString10)) {
                    interstitialConfigurations2.setBackFillProviderName(optString10);
                }
                String optString11 = section5.optString("premium");
                if (!TextUtils.isEmpty(optString11)) {
                    interstitialConfigurations2.setPremiumProviderName(optString11);
                }
                interstitialConfigurations = interstitialConfigurations2;
            } else {
                interstitialConfigurations = null;
            }
            if (jSONObject != null) {
                JSONObject jSONObject5 = jSONObject;
                JSONArray optJSONArray11 = jSONObject5.optJSONArray("placements");
                JSONObject section16 = getSection(jSONObject5, EventEntry.TABLE_NAME);
                int intConfigValue7 = getIntConfigValue(jSONObject5, section3, "maxNumOfAdaptersToLoadOnStart", 1);
                i = -1;
                long longConfigValue = getLongConfigValue(jSONObject5, section3, "atim", TapjoyConstants.TIMER_INCREMENT);
                int intConfigValue8 = getIntConfigValue(jSONObject5, section3, "delayLoadFailure", 3);
                int intConfigValue9 = getIntConfigValue(jSONObject5, section3, "bannerInterval", 60);
                JSONObject mergeJsons3 = IronSourceUtils.mergeJsons(section16, section8);
                boolean optBoolean5 = mergeJsons3.optBoolean("sendEventsToggle", false);
                String optString12 = mergeJsons3.optString("serverEventsURL", "");
                String optString13 = mergeJsons3.optString("serverEventsType", "");
                int optInt12 = mergeJsons3.optInt("backupThreshold", -1);
                int optInt13 = mergeJsons3.optInt("maxNumberOfEvents", -1);
                i2 = 5000;
                int optInt14 = mergeJsons3.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray12 = mergeJsons3.optJSONArray("optOut");
                if (optJSONArray12 != null) {
                    int[] iArr25 = new int[optJSONArray12.length()];
                    for (int i13 = 0; i13 < optJSONArray12.length(); i13++) {
                        iArr25[i13] = optJSONArray12.optInt(i13);
                    }
                    iArr5 = iArr25;
                } else {
                    iArr5 = null;
                }
                JSONArray optJSONArray13 = mergeJsons3.optJSONArray("optIn");
                if (optJSONArray13 != null) {
                    int[] iArr26 = new int[optJSONArray13.length()];
                    for (int i14 = 0; i14 < optJSONArray13.length(); i14++) {
                        iArr26[i14] = optJSONArray13.optInt(i14);
                    }
                    iArr6 = iArr26;
                } else {
                    iArr6 = null;
                }
                JSONArray optJSONArray14 = mergeJsons3.optJSONArray("triggerEvents");
                if (optJSONArray14 != null) {
                    int[] iArr27 = new int[optJSONArray14.length()];
                    for (int i15 = 0; i15 < optJSONArray14.length(); i15++) {
                        iArr27[i15] = optJSONArray14.optInt(i15);
                    }
                    iArr7 = iArr27;
                } else {
                    iArr7 = null;
                }
                JSONArray optJSONArray15 = mergeJsons3.optJSONArray("nonConnectivityEvents");
                if (optJSONArray15 != null) {
                    int[] iArr28 = new int[optJSONArray15.length()];
                    for (int i16 = 0; i16 < optJSONArray15.length(); i16++) {
                        iArr28[i16] = optJSONArray15.optInt(i16);
                    }
                    iArr8 = iArr28;
                } else {
                    iArr8 = null;
                }
                ApplicationEvents applicationEvents3 = new ApplicationEvents(false, optBoolean5, optString12, optString13, optInt12, optInt13, optInt14, iArr5, iArr6, iArr7, iArr8);
                BannerConfigurations bannerConfigurations2 = new BannerConfigurations(intConfigValue7, longConfigValue, applicationEvents3, intConfigValue9, intConfigValue8);
                if (optJSONArray11 != null) {
                    for (int i17 = 0; i17 < optJSONArray11.length(); i17++) {
                        BannerPlacement parseSingleBNPlacement = parseSingleBNPlacement(optJSONArray11.optJSONObject(i17));
                        if (parseSingleBNPlacement != null) {
                            bannerConfigurations2.addBannerPlacement(parseSingleBNPlacement);
                        }
                    }
                }
                bannerConfigurations = bannerConfigurations2;
            } else {
                i2 = 5000;
                i = -1;
                bannerConfigurations = null;
            }
            if (jSONObject2 != null) {
                JSONObject jSONObject6 = jSONObject2;
                JSONObject mergeJsons4 = IronSourceUtils.mergeJsons(getSection(jSONObject6, EventEntry.TABLE_NAME), section8);
                boolean optBoolean6 = mergeJsons4.optBoolean("sendEventsToggle", false);
                String optString14 = mergeJsons4.optString("serverEventsURL", "");
                String optString15 = mergeJsons4.optString("serverEventsType", "");
                int optInt15 = mergeJsons4.optInt("backupThreshold", i);
                int optInt16 = mergeJsons4.optInt("maxNumberOfEvents", i);
                int optInt17 = mergeJsons4.optInt("maxEventsPerBatch", i2);
                JSONArray optJSONArray16 = mergeJsons4.optJSONArray("optOut");
                if (optJSONArray16 != null) {
                    int[] iArr29 = new int[optJSONArray16.length()];
                    for (int i18 = 0; i18 < optJSONArray16.length(); i18++) {
                        iArr29[i18] = optJSONArray16.optInt(i18);
                    }
                    iArr = iArr29;
                } else {
                    iArr = null;
                }
                JSONArray optJSONArray17 = mergeJsons4.optJSONArray("optIn");
                if (optJSONArray17 != null) {
                    int[] iArr30 = new int[optJSONArray17.length()];
                    for (int i19 = 0; i19 < optJSONArray17.length(); i19++) {
                        iArr30[i19] = optJSONArray17.optInt(i19);
                    }
                    iArr2 = iArr30;
                } else {
                    iArr2 = null;
                }
                JSONArray optJSONArray18 = mergeJsons4.optJSONArray("triggerEvents");
                if (optJSONArray18 != null) {
                    int[] iArr31 = new int[optJSONArray18.length()];
                    for (int i20 = 0; i20 < optJSONArray18.length(); i20++) {
                        iArr31[i20] = optJSONArray18.optInt(i20);
                    }
                    iArr3 = iArr31;
                } else {
                    iArr3 = null;
                }
                JSONArray optJSONArray19 = mergeJsons4.optJSONArray("nonConnectivityEvents");
                if (optJSONArray19 != null) {
                    int[] iArr32 = new int[optJSONArray19.length()];
                    for (int i21 = 0; i21 < optJSONArray19.length(); i21++) {
                        iArr32[i21] = optJSONArray19.optInt(i21);
                    }
                    iArr4 = iArr32;
                } else {
                    iArr4 = null;
                }
                ApplicationEvents applicationEvents4 = new ApplicationEvents(false, optBoolean6, optString14, optString15, optInt15, optInt16, optInt17, iArr, iArr2, iArr3, iArr4);
                OfferwallConfigurations offerwallConfigurations2 = new OfferwallConfigurations(applicationEvents4);
                JSONArray optJSONArray20 = jSONObject6.optJSONArray("placements");
                if (optJSONArray20 != null) {
                    for (int i22 = 0; i22 < optJSONArray20.length(); i22++) {
                        OfferwallPlacement parseSingleOWPlacement = parseSingleOWPlacement(optJSONArray20.optJSONObject(i22));
                        if (parseSingleOWPlacement != null) {
                            offerwallConfigurations2.addOfferwallPlacement(parseSingleOWPlacement);
                        }
                    }
                }
                offerwallConfigurations = offerwallConfigurations2;
            } else {
                offerwallConfigurations = null;
            }
            JSONObject jSONObject7 = jSONObject3;
            ApplicationLogger applicationLogger = new ApplicationLogger(jSONObject7.optInt("server", 3), jSONObject7.optInt("publisher", 3), jSONObject7.optInt(ConsoleLogger.NAME, 3));
            if (jSONObject4 != null) {
                JSONObject jSONObject8 = jSONObject4;
                serverSegmetData = new ServerSegmetData(jSONObject8.optString("name", ""), jSONObject8.optString("id", "-1"), jSONObject8.optJSONObject("custom"));
            } else {
                serverSegmetData = null;
            }
            Configurations configurations = new Configurations(rewardedVideoConfigurations, interstitialConfigurations, offerwallConfigurations, bannerConfigurations, new ApplicationConfigurations(applicationLogger, serverSegmetData, section3.optBoolean("integration", false)));
            this.mConfigurations = configurations;
            JSONObject section17 = getSection(section8, "genericParams");
            if (section17 != null) {
                JSONObject section18 = getSection(section17, EventEntry.TABLE_NAME);
                if (section18 != null) {
                    section17.remove(EventEntry.TABLE_NAME);
                    Map parseJsonToStringMap = IronSourceUtils.parseJsonToStringMap(section18);
                    RewardedVideoEventsManager.getInstance().setEventGenericParams(parseJsonToStringMap);
                    InterstitialEventsManager.getInstance().setEventGenericParams(parseJsonToStringMap);
                }
            }
            if (section17 != null) {
                Map parseJsonToStringMap2 = IronSourceUtils.parseJsonToStringMap(section17);
                RewardedVideoEventsManager.getInstance().setBatchParams(parseJsonToStringMap2);
                InterstitialEventsManager.getInstance().setBatchParams(parseJsonToStringMap2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getIntConfigValue(JSONObject jSONObject, JSONObject jSONObject2, String str, int i) {
        int i2 = jSONObject.has(str) ? jSONObject.optInt(str, 0) : jSONObject2.has(str) ? jSONObject2.optInt(str, 0) : 0;
        return i2 == 0 ? i : i2;
    }

    private long getLongConfigValue(JSONObject jSONObject, JSONObject jSONObject2, String str, long j) {
        long j2 = jSONObject.has(str) ? jSONObject.optLong(str, 0) : jSONObject2.has(str) ? jSONObject2.optLong(str, 0) : 0;
        return j2 == 0 ? j : j2;
    }

    private Placement parseSingleRVPlacement(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt(Constants.PLACEMENT_ID, -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            String optString2 = jSONObject.optString("virtualItemName", "");
            int optInt2 = jSONObject.optInt("virtualItemCount", -1);
            PlacementAvailabilitySettings placementAvailabilitySettings = getPlacementAvailabilitySettings(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && optInt2 > 0) {
                Placement placement = new Placement(optInt, optString, optBoolean, optString2, optInt2, placementAvailabilitySettings);
                if (placementAvailabilitySettings == null) {
                    return placement;
                }
                CappingManager.addCappingInfo(this.mContext, placement);
                return placement;
            }
        }
        return null;
    }

    private InterstitialPlacement parseSingleISPlacement(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt(Constants.PLACEMENT_ID, -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            PlacementAvailabilitySettings placementAvailabilitySettings = getPlacementAvailabilitySettings(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                InterstitialPlacement interstitialPlacement = new InterstitialPlacement(optInt, optString, optBoolean, placementAvailabilitySettings);
                if (placementAvailabilitySettings == null) {
                    return interstitialPlacement;
                }
                CappingManager.addCappingInfo(this.mContext, interstitialPlacement);
                return interstitialPlacement;
            }
        }
        return null;
    }

    private OfferwallPlacement parseSingleOWPlacement(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt(Constants.PLACEMENT_ID, -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                return new OfferwallPlacement(optInt, optString, optBoolean);
            }
        }
        return null;
    }

    private BannerPlacement parseSingleBNPlacement(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt(Constants.PLACEMENT_ID, -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            PlacementAvailabilitySettings placementAvailabilitySettings = getPlacementAvailabilitySettings(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                BannerPlacement bannerPlacement = new BannerPlacement(optInt, optString, optBoolean, placementAvailabilitySettings);
                if (placementAvailabilitySettings == null) {
                    return bannerPlacement;
                }
                CappingManager.addCappingInfo(this.mContext, bannerPlacement);
                return bannerPlacement;
            }
        }
        return null;
    }

    private PlacementAvailabilitySettings getPlacementAvailabilitySettings(JSONObject jSONObject) {
        PlacementCappingType placementCappingType = null;
        if (jSONObject == null) {
            return null;
        }
        PlacementAvailabilitySettingsBuilder placementAvailabilitySettingsBuilder = new PlacementAvailabilitySettingsBuilder();
        boolean z = true;
        placementAvailabilitySettingsBuilder.delivery(jSONObject.optBoolean("delivery", true));
        JSONObject optJSONObject = jSONObject.optJSONObject("capping");
        if (optJSONObject != null) {
            String optString = optJSONObject.optString("unit");
            if (!TextUtils.isEmpty(optString)) {
                if (PlacementCappingType.PER_DAY.toString().equals(optString)) {
                    placementCappingType = PlacementCappingType.PER_DAY;
                } else if (PlacementCappingType.PER_HOUR.toString().equals(optString)) {
                    placementCappingType = PlacementCappingType.PER_HOUR;
                }
            }
            int optInt = optJSONObject.optInt("maxImpressions", 0);
            placementAvailabilitySettingsBuilder.capping(optJSONObject.optBoolean(String.ENABLED, false) && optInt > 0, placementCappingType, optInt);
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("pacing");
        if (optJSONObject2 != null) {
            int optInt2 = optJSONObject2.optInt("numOfSeconds", 0);
            if (!optJSONObject2.optBoolean(String.ENABLED, false) || optInt2 <= 0) {
                z = false;
            }
            placementAvailabilitySettingsBuilder.pacing(z, optInt2);
        }
        return placementAvailabilitySettingsBuilder.build();
    }

    private JSONObject getSection(JSONObject jSONObject, String str) {
        if (jSONObject != null) {
            return jSONObject.optJSONObject(str);
        }
        return null;
    }

    public String getRVBackFillProvider() {
        try {
            return this.mProviderOrder.getRVBackFillProvider();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceTag.INTERNAL, "getRVBackFillProvider", e);
            return null;
        }
    }

    public String getRVPremiumProvider() {
        try {
            return this.mProviderOrder.getRVPremiumProvider();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceTag.INTERNAL, "getRVPremiumProvider", e);
            return null;
        }
    }

    public ProviderSettingsHolder getProviderSettingsHolder() {
        return this.mProviderSettingsHolder;
    }

    public ProviderOrder getProviderOrder() {
        return this.mProviderOrder;
    }

    public Configurations getConfigurations() {
        return this.mConfigurations;
    }

    private Context getContext() {
        return this.mContext;
    }
}
