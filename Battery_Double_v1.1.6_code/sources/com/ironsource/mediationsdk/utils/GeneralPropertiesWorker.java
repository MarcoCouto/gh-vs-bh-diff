package com.ironsource.mediationsdk.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.facebook.places.model.PlaceFields;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class GeneralPropertiesWorker implements Runnable {
    private static final int MAX_MINUTES_OFFSET = 840;
    private static final int MINUTES_OFFSET_STEP = 15;
    private static final int MIN_MINUTES_OFFSET = -720;
    public static final String SDK_VERSION = "sdkVersion";
    private final String ADVERTISING_ID = "advertisingId";
    private final String ADVERTISING_ID_IS_LIMIT_TRACKING = RequestParameters.isLAT;
    private final String ADVERTISING_ID_TYPE = "advertisingIdType";
    private final String ANDROID_OS_VERSION = "osVersion";
    private final String APPLICATION_KEY = ServerResponseWrapper.APP_KEY_FIELD;
    private final String BATTERY_LEVEL = "battery";
    private final String BUNDLE_ID = RequestParameters.PACKAGE_NAME;
    private final String CONNECTION_TYPE = RequestParameters.CONNECTION_TYPE;
    private final String DEVICE_MODEL = RequestParameters.DEVICE_MODEL;
    private final String DEVICE_OEM = RequestParameters.DEVICE_OEM;
    private final String DEVICE_OS = "deviceOS";
    private final String EXTERNAL_FREE_MEMORY = "externalFreeMemory";
    private final String GMT_MINUTES_OFFSET = "gmtMinutesOffset";
    private final String INTERNAL_FREE_MEMORY = "internalFreeMemory";
    private final String KEY_IS_ROOT = "jb";
    private final String KEY_PLUGIN_FW_VERSION = "plugin_fw_v";
    private final String KEY_PLUGIN_TYPE = "pluginType";
    private final String KEY_PLUGIN_VERSION = "pluginVersion";
    private final String KEY_SESSION_ID = "sessionId";
    private final String LANGUAGE = "language";
    private final String MEDIATION_TYPE = "mt";
    private final String MOBILE_CARRIER = RequestParameters.MOBILE_CARRIER;
    private final String PUBLISHER_APP_VERSION = RequestParameters.APPLICATION_VERSION_NAME;
    private final String TAG = getClass().getSimpleName();
    private Context mContext;

    private String getDeviceOS() {
        return Constants.JAVASCRIPT_INTERFACE_NAME;
    }

    private GeneralPropertiesWorker() {
    }

    public GeneralPropertiesWorker(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public void run() {
        try {
            GeneralProperties.getProperties().putKeys(collectInformation());
            IronSourceUtils.saveGeneralProperties(this.mContext, GeneralProperties.getProperties().toJSON());
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
            StringBuilder sb = new StringBuilder();
            sb.append("Thread name = ");
            sb.append(getClass().getSimpleName());
            logger.logException(ironSourceTag, sb.toString(), e);
        }
    }

    private Map<String, Object> collectInformation() {
        HashMap hashMap = new HashMap();
        hashMap.put("sessionId", IronSourceObject.getInstance().getSessionId());
        String bundleId = getBundleId();
        if (!TextUtils.isEmpty(bundleId)) {
            hashMap.put(RequestParameters.PACKAGE_NAME, bundleId);
            String publisherApplicationVersion = ApplicationContext.getPublisherApplicationVersion(this.mContext, bundleId);
            if (!TextUtils.isEmpty(publisherApplicationVersion)) {
                hashMap.put(RequestParameters.APPLICATION_VERSION_NAME, publisherApplicationVersion);
            }
        }
        hashMap.put(ServerResponseWrapper.APP_KEY_FIELD, getApplicationKey());
        String str = "";
        String str2 = "";
        boolean z = false;
        try {
            String[] advertisingIdInfo = DeviceStatus.getAdvertisingIdInfo(this.mContext);
            if (advertisingIdInfo != null && advertisingIdInfo.length == 2) {
                if (!TextUtils.isEmpty(advertisingIdInfo[0])) {
                    str = advertisingIdInfo[0];
                }
                z = Boolean.valueOf(advertisingIdInfo[1]).booleanValue();
            }
        } catch (Exception unused) {
        }
        if (!TextUtils.isEmpty(str)) {
            str2 = IronSourceConstants.TYPE_GAID;
        } else {
            str = DeviceStatus.getOrGenerateOnceUniqueIdentifier(this.mContext);
            if (!TextUtils.isEmpty(str)) {
                str2 = IronSourceConstants.TYPE_UUID;
            }
        }
        if (!TextUtils.isEmpty(str)) {
            hashMap.put("advertisingId", str);
            hashMap.put("advertisingIdType", str2);
            hashMap.put(RequestParameters.isLAT, Boolean.valueOf(z));
        }
        hashMap.put("deviceOS", getDeviceOS());
        if (!TextUtils.isEmpty(getAndroidVersion())) {
            hashMap.put("osVersion", getAndroidVersion());
        }
        String connectionType = IronSourceUtils.getConnectionType(this.mContext);
        if (!TextUtils.isEmpty(connectionType)) {
            hashMap.put(RequestParameters.CONNECTION_TYPE, connectionType);
        }
        hashMap.put(SDK_VERSION, getSDKVersion());
        String language = getLanguage();
        if (!TextUtils.isEmpty(language)) {
            hashMap.put("language", language);
        }
        String deviceOEM = getDeviceOEM();
        if (!TextUtils.isEmpty(deviceOEM)) {
            hashMap.put(RequestParameters.DEVICE_OEM, deviceOEM);
        }
        String deviceModel = getDeviceModel();
        if (!TextUtils.isEmpty(deviceModel)) {
            hashMap.put(RequestParameters.DEVICE_MODEL, deviceModel);
        }
        String mobileCarrier = getMobileCarrier();
        if (!TextUtils.isEmpty(mobileCarrier)) {
            hashMap.put(RequestParameters.MOBILE_CARRIER, mobileCarrier);
        }
        hashMap.put("internalFreeMemory", Long.valueOf(getInternalStorageFreeSize()));
        hashMap.put("externalFreeMemory", Long.valueOf(getExternalStorageFreeSize()));
        hashMap.put("battery", Integer.valueOf(getBatteryLevel()));
        int gmtMinutesOffset = getGmtMinutesOffset();
        if (validateGmtMinutesOffset(gmtMinutesOffset)) {
            hashMap.put("gmtMinutesOffset", Integer.valueOf(gmtMinutesOffset));
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("pluginType", pluginType);
        }
        String pluginVersion = getPluginVersion();
        if (!TextUtils.isEmpty(pluginVersion)) {
            hashMap.put("pluginVersion", pluginVersion);
        }
        String pluginFrameworkVersion = getPluginFrameworkVersion();
        if (!TextUtils.isEmpty(pluginFrameworkVersion)) {
            hashMap.put("plugin_fw_v", pluginFrameworkVersion);
        }
        String valueOf = String.valueOf(DeviceStatus.isRootedDevice());
        if (!TextUtils.isEmpty(valueOf)) {
            hashMap.put("jb", valueOf);
        }
        String mediationType = getMediationType();
        if (!TextUtils.isEmpty(mediationType)) {
            hashMap.put("mt", mediationType);
        }
        return hashMap;
    }

    private String getPluginType() {
        String str = "";
        try {
            return ConfigFile.getConfigFile().getPluginType();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceTag.NATIVE, "getPluginType()", e);
            return str;
        }
    }

    private String getPluginVersion() {
        String str = "";
        try {
            return ConfigFile.getConfigFile().getPluginVersion();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceTag.NATIVE, "getPluginVersion()", e);
            return str;
        }
    }

    private String getPluginFrameworkVersion() {
        String str = "";
        try {
            return ConfigFile.getConfigFile().getPluginFrameworkVersion();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceTag.NATIVE, "getPluginFrameworkVersion()", e);
            return str;
        }
    }

    private String getBundleId() {
        try {
            return this.mContext.getPackageName();
        } catch (Exception unused) {
            return "";
        }
    }

    private String getApplicationKey() {
        return IronSourceObject.getInstance().getIronSourceAppKey();
    }

    private String getAndroidVersion() {
        try {
            String str = VERSION.RELEASE;
            int i = VERSION.SDK_INT;
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(i);
            sb.append("(");
            sb.append(str);
            sb.append(")");
            return sb.toString();
        } catch (Exception unused) {
            return "";
        }
    }

    private String getSDKVersion() {
        return IronSourceUtils.getSDKVersion();
    }

    private String getLanguage() {
        try {
            return Locale.getDefault().getLanguage();
        } catch (Exception unused) {
            return "";
        }
    }

    private String getDeviceOEM() {
        try {
            return Build.MANUFACTURER;
        } catch (Exception unused) {
            return "";
        }
    }

    private String getDeviceModel() {
        try {
            return Build.MODEL;
        } catch (Exception unused) {
            return "";
        }
    }

    private String getMobileCarrier() {
        String str = "";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.mContext.getSystemService(PlaceFields.PHONE);
            if (telephonyManager == null) {
                return str;
            }
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            if (!networkOperatorName.equals("")) {
                return networkOperatorName;
            }
            return str;
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
            StringBuilder sb = new StringBuilder();
            sb.append(this.TAG);
            sb.append(":getMobileCarrier()");
            logger.logException(ironSourceTag, sb.toString(), e);
            return str;
        }
    }

    private boolean isExternalStorageAbvailable() {
        try {
            return Environment.getExternalStorageState().equals("mounted");
        } catch (Exception unused) {
            return false;
        }
    }

    private long getInternalStorageFreeSize() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
        } catch (Exception unused) {
            return -1;
        }
    }

    private long getExternalStorageFreeSize() {
        if (!isExternalStorageAbvailable()) {
            return -1;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    private int getBatteryLevel() {
        try {
            Intent registerReceiver = this.mContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = 0;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : 0;
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("scale", -1);
            }
            if (intExtra == -1 || i == -1) {
                return -1;
            }
            return (int) ((((float) intExtra) / ((float) i)) * 100.0f);
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
            StringBuilder sb = new StringBuilder();
            sb.append(this.TAG);
            sb.append(":getBatteryLevel()");
            logger.logException(ironSourceTag, sb.toString(), e);
            return -1;
        }
    }

    private int getGmtMinutesOffset() {
        int i = 0;
        try {
            TimeZone timeZone = TimeZone.getDefault();
            int offset = (timeZone.getOffset(GregorianCalendar.getInstance(timeZone).getTimeInMillis()) / 1000) / 60;
            try {
                return Math.round((float) (offset / 15)) * 15;
            } catch (Exception e) {
                int i2 = offset;
                e = e;
                i = i2;
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
                StringBuilder sb = new StringBuilder();
                sb.append(this.TAG);
                sb.append(":getGmtMinutesOffset()");
                logger.logException(ironSourceTag, sb.toString(), e);
                return i;
            }
        } catch (Exception e2) {
            e = e2;
            IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag2 = IronSourceTag.NATIVE;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.TAG);
            sb2.append(":getGmtMinutesOffset()");
            logger2.logException(ironSourceTag2, sb2.toString(), e);
            return i;
        }
    }

    private boolean validateGmtMinutesOffset(int i) {
        return i <= MAX_MINUTES_OFFSET && i >= MIN_MINUTES_OFFSET && i % 15 == 0;
    }

    private String getMediationType() {
        return IronSourceObject.getInstance().getMediationType();
    }
}
