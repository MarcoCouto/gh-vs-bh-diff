package com.ironsource.mediationsdk.utils;

public class SessionDepthManager {
    public static final int BANNER = 3;
    public static final int INTERSTITIAL = 2;
    public static final int NONE = -1;
    public static final int OFFERWALL = 0;
    public static final int REWARDEDVIDEO = 1;
    private static SessionDepthManager mInstance;
    private int mBannerDepth = 1;
    private int mInterstitialDepth = 1;
    private int mOfferwallDepth = 1;
    private int mRewardedVideoDepth = 1;

    public static synchronized SessionDepthManager getInstance() {
        SessionDepthManager sessionDepthManager;
        synchronized (SessionDepthManager.class) {
            if (mInstance == null) {
                mInstance = new SessionDepthManager();
            }
            sessionDepthManager = mInstance;
        }
        return sessionDepthManager;
    }

    public synchronized void increaseSessionDepth(int i) {
        switch (i) {
            case 0:
                this.mOfferwallDepth++;
                break;
            case 1:
                this.mRewardedVideoDepth++;
                break;
            case 2:
                this.mInterstitialDepth++;
                break;
            case 3:
                this.mBannerDepth++;
                break;
        }
    }

    public synchronized int getSessionDepth(int i) {
        switch (i) {
            case 0:
                return this.mOfferwallDepth;
            case 1:
                return this.mRewardedVideoDepth;
            case 2:
                return this.mInterstitialDepth;
            case 3:
                return this.mBannerDepth;
            default:
                return -1;
        }
    }
}
