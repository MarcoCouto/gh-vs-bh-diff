package com.ironsource.sdk.service;

import android.content.Context;
import android.os.Build.VERSION;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.service.Connectivity.BroadcastReceiverStrategy;
import com.ironsource.sdk.service.Connectivity.IConnectivity;
import com.ironsource.sdk.service.Connectivity.IConnectivityStatus;
import com.ironsource.sdk.service.Connectivity.NetworkCallbackStrategy;
import com.ironsource.sdk.utils.Logger;
import org.json.JSONObject;

public abstract class ConnectivityAdapter implements IConnectivityStatus {
    private IConnectivity currentStrategy;

    public void onConnected(String str, JSONObject jSONObject) {
    }

    public void onDisconnected() {
    }

    public void onStatusChanged(String str, JSONObject jSONObject) {
    }

    protected ConnectivityAdapter(JSONObject jSONObject, Context context) {
        this.currentStrategy = decideStrategy(jSONObject, context);
        String simpleName = ConnectivityAdapter.class.getSimpleName();
        StringBuilder sb = new StringBuilder();
        sb.append("created ConnectivityAdapter with strategy ");
        sb.append(this.currentStrategy.getClass().getSimpleName());
        Logger.i(simpleName, sb.toString());
    }

    public void startListenToNetworkChanges(Context context) {
        this.currentStrategy.startListenToNetworkChanges(context);
    }

    public void stopListenToNetworkChanges(Context context) {
        this.currentStrategy.stopListenToNetworkChanges(context);
    }

    public JSONObject getConnectivityData(Context context) {
        return this.currentStrategy.getConnectivityInfo(context);
    }

    public void release() {
        this.currentStrategy.release();
    }

    private IConnectivity decideStrategy(JSONObject jSONObject, Context context) {
        if (jSONObject.optInt(RequestParameters.CONNECTIVITY_STRATEGY) == 1) {
            return new BroadcastReceiverStrategy(this);
        }
        boolean isPermissionGranted = ApplicationContext.isPermissionGranted(context, "android.permission.ACCESS_NETWORK_STATE");
        if (VERSION.SDK_INT < 23 || !isPermissionGranted) {
            return new BroadcastReceiverStrategy(this);
        }
        return new NetworkCallbackStrategy(this);
    }
}
