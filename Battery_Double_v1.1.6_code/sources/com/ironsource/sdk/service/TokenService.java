package com.ironsource.sdk.service;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Log;
import com.iab.omid.library.ironsrc.Omid;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.analytics.omid.OMIDManager;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.utils.IronSourceQaProperties;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class TokenService {
    static final String METADATA_KEY_PREFIX = "metadata_";
    private static TokenService mInstance;
    private JSONObject tokeData = new JSONObject();

    public static synchronized TokenService getInstance() {
        TokenService tokenService;
        synchronized (TokenService.class) {
            if (mInstance == null) {
                mInstance = new TokenService();
            }
            tokenService = mInstance;
        }
        return tokenService;
    }

    private TokenService() {
    }

    public void collectOmidParameters() {
        HashMap hashMap = new HashMap();
        hashMap.put(OMIDManager.OMID_VERSION_PROPERTY_NAME, Omid.getVersion());
        hashMap.put(OMIDManager.OMID_PARTNER_VERSION_PROPERTY_NAME, OMIDManager.OMID_PARTNER_VERSION);
        mInstance.collectDataFromExternalParams(hashMap);
    }

    public void collectQaParameters() {
        if (IronSourceQaProperties.isInitialized()) {
            mInstance.collectDataFromExternalParams(IronSourceQaProperties.getInstance().getParameters());
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void put(String str, Object obj) {
        try {
            this.tokeData.put(str, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return;
    }

    public void collectApplicationUserId(String str) {
        if (str != null) {
            put(RequestParameters.APPLICATION_USER_ID, SDKUtils.encodeString(str));
        }
    }

    public void collectApplicationKey(String str) {
        if (str != null) {
            put(RequestParameters.APPLICATION_KEY, SDKUtils.encodeString(str));
        }
    }

    public void collectDataFromActivity(Activity activity) {
        if (activity != null) {
            if (VERSION.SDK_INT >= 19) {
                put(SDKUtils.encodeString(RequestParameters.IMMERSIVE), Boolean.valueOf(DeviceStatus.isImmersiveSupported(activity)));
            }
            put(RequestParameters.APP_ORIENTATION, SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(activity)));
        }
    }

    public void collectAdvertisingID(final Activity activity) {
        if (activity != null) {
            try {
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            TokenService.this.updateData(DeviceData.fetchAdvertiserIdData(activity));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void collectDataFromDevice(Context context) {
        if (context != null) {
            updateData(DeviceData.fetchPermanentData(context));
            updateData(DeviceData.fetchMutableData(context));
        }
    }

    public void collectDataFromExternalParams(Map<String, String> map) {
        if (map == null) {
            Log.d("TokenService", "collectDataFromExternalParams params=null");
            return;
        }
        for (String str : map.keySet()) {
            put(str, SDKUtils.encodeString((String) map.get(str)));
        }
    }

    public void collectDataFromControllerConfig(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                put("chinaCDN", new JSONObject(str).opt("chinaCDN"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateData(JSONObject jSONObject) {
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            put(str, jSONObject.opt(str));
        }
    }

    public void updateMetaData(JSONObject jSONObject) {
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            StringBuilder sb = new StringBuilder();
            sb.append(METADATA_KEY_PREFIX);
            sb.append(str);
            put(sb.toString(), jSONObject.opt(str));
        }
    }

    public String getToken(Context context) {
        try {
            return Gibberish.encode(getRawToken(context).toString());
        } catch (Exception unused) {
            return Gibberish.encode(new JSONObject().toString());
        }
    }

    public JSONObject getRawToken(Context context) {
        fetchIndependentData();
        collectDataFromDevice(context);
        return this.tokeData;
    }

    public void fetchDependentData(Activity activity, String str, String str2) {
        collectAdvertisingID(activity);
        collectDataFromActivity(activity);
        collectDataFromDevice(activity);
        collectApplicationUserId(str2);
        collectApplicationKey(str);
    }

    public void fetchIndependentData() {
        collectDataFromControllerConfig(SDKUtils.getControllerConfig());
        collectDataFromExternalParams(SDKUtils.getInitSDKParams());
        collectQaParameters();
        collectOmidParameters();
    }
}
