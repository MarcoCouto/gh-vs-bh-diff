package com.ironsource.sdk.service.Connectivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import org.json.JSONException;
import org.json.JSONObject;

public class ConnectivityUtils {
    public static String getConnectionType(Context context) {
        if (VERSION.SDK_INT >= 23) {
            return getConnectionType(getActiveNetwork(context), context);
        }
        return getConnectionTypePreMOSVersion(context);
    }

    @SuppressLint({"MissingPermission"})
    static String getConnectionType(Network network, Context context) {
        String connectionTypePreMOSVersion;
        if (context == null) {
            return "none";
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        String str = "none";
        if (!(network == null || connectivityManager == null)) {
            try {
                if (VERSION.SDK_INT >= 21) {
                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                    if (networkCapabilities == null) {
                        return getConnectionTypePreMOSVersion(context);
                    }
                    if (networkCapabilities.hasTransport(1)) {
                        connectionTypePreMOSVersion = "wifi";
                    } else if (networkCapabilities.hasTransport(0)) {
                        connectionTypePreMOSVersion = ConnectivityService.NETWORK_TYPE_3G;
                    } else {
                        connectionTypePreMOSVersion = getConnectionTypePreMOSVersion(context);
                    }
                } else {
                    connectionTypePreMOSVersion = getConnectionTypePreMOSVersion(context);
                }
                str = connectionTypePreMOSVersion;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }

    @SuppressLint({"MissingPermission"})
    static Network getActiveNetwork(Context context) {
        if (context == null) {
            return null;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (VERSION.SDK_INT < 23 || connectivityManager == null) {
            return null;
        }
        return connectivityManager.getActiveNetwork();
    }

    @SuppressLint({"MissingPermission"})
    static JSONObject getNetworkData(Context context, Network network) {
        if (context == null) {
            return new JSONObject();
        }
        JSONObject jSONObject = new JSONObject();
        if (VERSION.SDK_INT >= 23 && network != null) {
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                if (connectivityManager != null) {
                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                    if (networkCapabilities != null) {
                        jSONObject.put("networkCapabilities", networkCapabilities.toString());
                        jSONObject.put("downloadSpeed", networkCapabilities.getLinkDownstreamBandwidthKbps());
                        jSONObject.put("uploadSpeed", networkCapabilities.getLinkUpstreamBandwidthKbps());
                        jSONObject.put(RequestParameters.HAS_VPN, hasVPN(context));
                    }
                }
                if (getConnectionType(network, context).equals(ConnectivityService.NETWORK_TYPE_3G)) {
                    jSONObject.put(RequestParameters.CELLULAR_NETWORK_TYPE, ConnectivityService.getCellularNetworkType(context));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jSONObject;
    }

    private static String getConnectionTypePreMOSVersion(Context context) {
        String connectionType = ConnectivityService.getConnectionType(context);
        return TextUtils.isEmpty(connectionType) ? "none" : connectionType;
    }

    public static boolean hasVPN(Context context) {
        return getNetworkTransport(context, getActiveNetwork(context)).equals(ConnectivityService.NETWORK_TYPE_VPN);
    }

    @SuppressLint({"MissingPermission"})
    private static String getNetworkTransport(Context context, Network network) {
        String str = "";
        if (VERSION.SDK_INT < 23 || network == null || context == null) {
            return str;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                if (networkCapabilities == null) {
                    return str;
                }
                if (networkCapabilities.hasTransport(1)) {
                    return "wifi";
                }
                if (networkCapabilities.hasTransport(0)) {
                    return ConnectivityService.NETWORK_TYPE_CELLULAR;
                }
                if (networkCapabilities.hasTransport(4)) {
                    return ConnectivityService.NETWORK_TYPE_VPN;
                }
                if (networkCapabilities.hasTransport(3)) {
                    return ConnectivityService.NETWORK_TYPE_ETHERNET;
                }
                if (networkCapabilities.hasTransport(5)) {
                    return ConnectivityService.NETWORK_TYPE_WIFI_AWARE;
                }
                if (networkCapabilities.hasTransport(6)) {
                    return ConnectivityService.NETWORK_TYPE_LOWPAN;
                }
                if (networkCapabilities.hasTransport(2)) {
                    return ConnectivityService.NETWORK_TYPE_BLUETOOTH;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }
}
