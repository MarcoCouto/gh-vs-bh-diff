package com.ironsource.sdk.service.Connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import org.json.JSONObject;

public class BroadcastReceiverStrategy implements IConnectivity {
    private BroadcastReceiver mConnectionReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String connectionType = ConnectivityUtils.getConnectionType(context);
            if (connectionType.equals("none")) {
                BroadcastReceiverStrategy.this.mNetworkState.onDisconnected();
            } else {
                BroadcastReceiverStrategy.this.mNetworkState.onConnected(connectionType, new JSONObject());
            }
        }
    };
    /* access modifiers changed from: private */
    public final IConnectivityStatus mNetworkState;

    public BroadcastReceiverStrategy(IConnectivityStatus iConnectivityStatus) {
        this.mNetworkState = iConnectivityStatus;
    }

    public void startListenToNetworkChanges(Context context) {
        try {
            context.registerReceiver(this.mConnectionReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopListenToNetworkChanges(Context context) {
        try {
            context.unregisterReceiver(this.mConnectionReceiver);
        } catch (IllegalArgumentException unused) {
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("unregisterConnectionReceiver - ");
            sb.append(e);
            Log.e("ContentValues", sb.toString());
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(Constants.NATIVE_EXCEPTION_BASE_URL);
            sb2.append(e.getStackTrace()[0].getMethodName());
            ironSourceAsyncHttpRequestTask.execute(new String[]{sb2.toString()});
        }
    }

    public JSONObject getConnectivityInfo(Context context) {
        return new JSONObject();
    }

    public void release() {
        this.mConnectionReceiver = null;
    }
}
