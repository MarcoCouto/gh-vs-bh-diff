package com.ironsource.sdk.service.Connectivity;

class ConnectivityConstants {
    static final String CELLULAR_NETWORK_TYPE = "cellularNetworkType";
    static final String DOWNLOAD_SPEED = "downloadSpeed";
    static final String HAS_VPN = "hasVPN";
    static final String NETWORK_CAPABILITIES = "networkCapabilities";
    static final String UPLOAD_SPEED = "uploadSpeed";

    ConnectivityConstants() {
    }
}
