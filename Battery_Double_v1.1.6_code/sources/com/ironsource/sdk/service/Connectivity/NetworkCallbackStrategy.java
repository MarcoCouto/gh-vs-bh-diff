package com.ironsource.sdk.service.Connectivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.NetworkRequest.Builder;
import android.os.Build.VERSION;
import android.util.Log;
import org.json.JSONObject;

public class NetworkCallbackStrategy implements IConnectivity {
    private String TAG = NetworkCallbackStrategy.class.getSimpleName();
    private int kSupportingVersionForNetworkCallback = 23;
    private NetworkCallback mNetworkCallback;
    /* access modifiers changed from: private */
    public final IConnectivityStatus mNetworkState;

    public NetworkCallbackStrategy(IConnectivityStatus iConnectivityStatus) {
        this.mNetworkState = iConnectivityStatus;
    }

    @SuppressLint({"NewApi", "MissingPermission"})
    public void startListenToNetworkChanges(final Context context) {
        if (VERSION.SDK_INT >= this.kSupportingVersionForNetworkCallback) {
            stopListenToNetworkChanges(context);
            if (ConnectivityUtils.getConnectionType(context).equals("none")) {
                this.mNetworkState.onDisconnected();
            }
            if (this.mNetworkCallback == null) {
                this.mNetworkCallback = new NetworkCallback() {
                    public void onAvailable(Network network) {
                        if (network != null) {
                            NetworkCallbackStrategy.this.mNetworkState.onConnected(ConnectivityUtils.getConnectionType(network, context), ConnectivityUtils.getNetworkData(context, network));
                        } else {
                            NetworkCallbackStrategy.this.mNetworkState.onConnected(ConnectivityUtils.getConnectionType(context), ConnectivityUtils.getNetworkData(context, ConnectivityUtils.getActiveNetwork(context)));
                        }
                    }

                    public void onLost(Network network) {
                        if (ConnectivityUtils.getConnectionType(context).equals("none")) {
                            NetworkCallbackStrategy.this.mNetworkState.onDisconnected();
                        }
                    }

                    public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
                        if (network != null) {
                            NetworkCallbackStrategy.this.mNetworkState.onStatusChanged(ConnectivityUtils.getConnectionType(network, context), ConnectivityUtils.getNetworkData(context, network));
                        }
                    }

                    public void onLinkPropertiesChanged(Network network, LinkProperties linkProperties) {
                        if (network != null) {
                            NetworkCallbackStrategy.this.mNetworkState.onStatusChanged(ConnectivityUtils.getConnectionType(network, context), ConnectivityUtils.getNetworkData(context, network));
                        }
                    }
                };
            }
            NetworkRequest build = new Builder().addCapability(12).build();
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                if (connectivityManager != null) {
                    connectivityManager.registerNetworkCallback(build, this.mNetworkCallback);
                }
            } catch (Exception unused) {
                Log.e(this.TAG, "NetworkCallback was not able to register");
            }
        }
    }

    @SuppressLint({"NewApi"})
    public void stopListenToNetworkChanges(Context context) {
        if (VERSION.SDK_INT >= this.kSupportingVersionForNetworkCallback && this.mNetworkCallback != null && context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                try {
                    connectivityManager.unregisterNetworkCallback(this.mNetworkCallback);
                } catch (Exception unused) {
                    Log.e(this.TAG, "NetworkCallback for was not registered or already unregistered");
                }
            }
        }
    }

    public JSONObject getConnectivityInfo(Context context) {
        return ConnectivityUtils.getNetworkData(context, ConnectivityUtils.getActiveNetwork(context));
    }

    public void release() {
        this.mNetworkCallback = null;
    }
}
