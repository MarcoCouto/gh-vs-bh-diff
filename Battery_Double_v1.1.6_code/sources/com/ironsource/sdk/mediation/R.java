package com.ironsource.sdk.mediation;

public final class R {

    public static final class drawable {
        public static final int ic_launcher = 2131230931;

        private drawable() {
        }
    }

    public static final class string {
        public static final int app_name = 2131623977;

        private string() {
        }
    }

    private R() {
    }
}
