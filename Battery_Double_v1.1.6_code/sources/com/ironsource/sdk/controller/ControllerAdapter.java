package com.ironsource.sdk.controller;

import android.os.Build.VERSION;
import android.webkit.JavascriptInterface;
import com.ironsource.sdk.controller.WebController.NativeAPI;
import com.ironsource.sdk.utils.Logger;
import java.lang.reflect.Method;
import java.security.AccessControlException;

class ControllerAdapter {
    private static final String TAG = "ControllerAdapter";
    private final NativeAPI mNativeAPI;

    ControllerAdapter(NativeAPI nativeAPI) {
        this.mNativeAPI = nativeAPI;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void call(String str, String str2) throws Exception {
        if (this.mNativeAPI == null) {
            Logger.e(TAG, "!!! nativeAPI == null !!!");
            return;
        }
        Method declaredMethod = NativeAPI.class.getDeclaredMethod(str, new Class[]{String.class});
        if (VERSION.SDK_INT >= 17) {
            if (!declaredMethod.isAnnotationPresent(JavascriptInterface.class)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Trying to access a private function: ");
                sb.append(str);
                throw new AccessControlException(sb.toString());
            }
        }
        declaredMethod.invoke(this.mNativeAPI, new Object[]{str2});
    }

    /* access modifiers changed from: 0000 */
    public void sendUnauthorizedError(String str) {
        if (this.mNativeAPI != null) {
            this.mNativeAPI.sendUnauthorizedError(str);
        }
    }
}
