package com.ironsource.sdk.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums.ProductType;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import java.util.Map;
import org.json.JSONObject;

public class NativeController implements IronSourceController {
    private static final Handler mUiHandler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public String mFailedControllerReason = "";
    /* access modifiers changed from: private */
    public OnOfferWallListener mOfferwallListener;

    public void destroy() {
    }

    public void enterBackground() {
    }

    public void enterForeground() {
    }

    public boolean isInterstitialAdAvailable(String str) {
        return false;
    }

    public void registerConnectionReceiver(Context context) {
    }

    public void restoreSavedState() {
    }

    public void setCommunicationWithAdView(ISNAdView iSNAdView) {
    }

    public void unregisterConnectionReceiver(Context context) {
    }

    public void updateConsentInfo(JSONObject jSONObject) {
    }

    NativeController(final ControllerEventListener controllerEventListener) {
        mUiHandler.post(new Runnable() {
            public void run() {
                controllerEventListener.handleControllerStageReady();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void setFailedControllerReason(String str) {
        this.mFailedControllerReason = str;
    }

    public void initOfferWall(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        if (onOfferWallListener != null) {
            this.mOfferwallListener = onOfferWallListener;
            mUiHandler.post(new Runnable() {
                public void run() {
                    NativeController.this.mOfferwallListener.onOfferwallInitFail(NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void showOfferWall(Map<String, String> map) {
        if (this.mOfferwallListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    NativeController.this.mOfferwallListener.onOWShowFail(NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void getOfferWallCredits(String str, String str2, final OnOfferWallListener onOfferWallListener) {
        if (onOfferWallListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    onOfferWallListener.onGetOWCreditsFailed(NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void initRewardedVideo(String str, String str2, final DemandSource demandSource, final DSRewardedVideoListener dSRewardedVideoListener) {
        if (dSRewardedVideoListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    dSRewardedVideoListener.onAdProductInitFailed(ProductType.RewardedVideo, demandSource.getDemandSourceName(), NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void showRewardedVideo(final JSONObject jSONObject, final DSRewardedVideoListener dSRewardedVideoListener) {
        if (dSRewardedVideoListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    dSRewardedVideoListener.onRVShowFail(jSONObject.optString("demandSourceName"), NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void initInterstitial(String str, String str2, final DemandSource demandSource, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.onAdProductInitFailed(ProductType.Interstitial, demandSource.getDemandSourceName(), NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void loadInterstitial(final String str, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.onInterstitialLoadFailed(str, NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void loadInterstitial(final DemandSource demandSource, Map<String, String> map, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.onInterstitialLoadFailed(demandSource.getDemandSourceName(), NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void showInterstitial(final JSONObject jSONObject, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.onInterstitialShowFailed(jSONObject.optString("demandSourceName"), NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void showInterstitial(final DemandSource demandSource, Map<String, String> map, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.onInterstitialShowFailed(demandSource.getDemandSourceName(), NativeController.this.mFailedControllerReason);
                }
            });
        }
    }

    public void initBanner(String str, String str2, DemandSource demandSource, DSBannerListener dSBannerListener) {
        if (dSBannerListener != null) {
            dSBannerListener.onAdProductInitFailed(ProductType.Banner, demandSource.getDemandSourceName(), this.mFailedControllerReason);
        }
    }

    public void loadBanner(final JSONObject jSONObject, final DSBannerListener dSBannerListener) {
        if (dSBannerListener != null) {
            mUiHandler.post(new Runnable() {
                public void run() {
                    dSBannerListener.onBannerLoadFail(jSONObject.optString("demandSourceName"), NativeController.this.mFailedControllerReason);
                }
            });
        }
    }
}
