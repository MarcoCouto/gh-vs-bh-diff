package com.ironsource.sdk.controller;

import android.app.Activity;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.constants.Constants.ErrorCodes;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums.ControllerState;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import com.ironsource.sdk.service.TokenService;
import com.ironsource.sdk.utils.Logger;
import java.util.Map;
import org.json.JSONObject;

public class ControllerManager implements ControllerEventListener {
    /* access modifiers changed from: private */
    public static final Handler mUiHandler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public String TAG = ControllerManager.class.getSimpleName();
    private CommandExecutor mCommandExecutor = new CommandExecutor();
    /* access modifiers changed from: private */
    public IronSourceController mController;
    private CommandExecutor mControllerCommandsExecuter = new CommandExecutor();
    private ControllerState mControllerState = ControllerState.None;
    private CountDownTimer mGlobalControllerTimer;

    public ControllerManager(Activity activity, TokenService tokenService, DemandSourceManager demandSourceManager) {
        createController(activity, tokenService, demandSourceManager);
    }

    private void createController(final Activity activity, final TokenService tokenService, final DemandSourceManager demandSourceManager) {
        mUiHandler.post(new Runnable() {
            public void run() {
                try {
                    ControllerManager.this.createWebController(activity, tokenService, demandSourceManager);
                } catch (Exception e) {
                    e.printStackTrace();
                    ControllerManager.this.createNativeController(e.getMessage());
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void createWebController(Activity activity, TokenService tokenService, DemandSourceManager demandSourceManager) throws Exception {
        this.mController = new WebController(activity, demandSourceManager, this);
        WebController webController = (WebController) this.mController;
        webController.addTokenJSInterface(new TokenJSAdapter(activity.getApplicationContext(), tokenService));
        webController.addOmidJSInterface(new OMIDJSAdapter(activity.getApplicationContext()));
        webController.addPermissionsJSInterface(new PermissionsJSAdapter(activity.getApplicationContext()));
        webController.addBannerJSInterface(new BannerJSAdapter());
        AnonymousClass2 r0 = new CountDownTimer(200000, 1000) {
            public void onTick(long j) {
                String access$200 = ControllerManager.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Global Controller Timer Tick ");
                sb.append(j);
                Logger.i(access$200, sb.toString());
            }

            public void onFinish() {
                Logger.i(ControllerManager.this.TAG, "Global Controller Timer Finish");
                ControllerManager.this.destroyWebControllerIfExist();
                ControllerManager.mUiHandler.post(new Runnable() {
                    public void run() {
                        ControllerManager.this.createNativeController(ErrorCodes.CONTROLLER_DOWNLOAD_TIMEOUT);
                    }
                });
            }
        };
        this.mGlobalControllerTimer = r0.start();
        webController.downloadController();
        this.mCommandExecutor.setReady();
        this.mCommandExecutor.purgeDelayedCommands();
    }

    /* access modifiers changed from: private */
    public void destroyWebControllerIfExist() {
        if (this.mController != null) {
            this.mController.destroy();
        }
    }

    /* access modifiers changed from: private */
    public void createNativeController(String str) {
        this.mController = new NativeController(this);
        ((NativeController) this.mController).setFailedControllerReason(str);
        this.mCommandExecutor.setReady();
        this.mCommandExecutor.purgeDelayedCommands();
    }

    private boolean isControllerStateReady() {
        return ControllerState.Ready.equals(this.mControllerState);
    }

    public void executeCommand(Runnable runnable) {
        this.mCommandExecutor.executeCommand(runnable);
    }

    public void handleControllerStageLoaded() {
        this.mControllerState = ControllerState.Loaded;
    }

    public void handleControllerStageReady() {
        this.mControllerState = ControllerState.Ready;
        if (this.mGlobalControllerTimer != null) {
            this.mGlobalControllerTimer.cancel();
        }
        this.mControllerCommandsExecuter.setReady();
        this.mControllerCommandsExecuter.purgeDelayedCommands();
        this.mController.restoreSavedState();
    }

    public void handleControllerStageFailed(final String str) {
        if (this.mGlobalControllerTimer != null) {
            this.mGlobalControllerTimer.cancel();
        }
        destroyWebControllerIfExist();
        mUiHandler.post(new Runnable() {
            public void run() {
                ControllerManager.this.createNativeController(str);
            }
        });
    }

    public void initOfferWall(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        CommandExecutor commandExecutor = this.mControllerCommandsExecuter;
        final String str3 = str;
        final String str4 = str2;
        final Map<String, String> map2 = map;
        final OnOfferWallListener onOfferWallListener2 = onOfferWallListener;
        AnonymousClass4 r1 = new Runnable() {
            public void run() {
                ControllerManager.this.mController.initOfferWall(str3, str4, map2, onOfferWallListener2);
            }
        };
        commandExecutor.executeCommand(r1);
    }

    public void showOfferWall(final Map<String, String> map) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.showOfferWall(map);
            }
        });
    }

    public void getOfferWallCredits(final String str, final String str2, final OnOfferWallListener onOfferWallListener) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.getOfferWallCredits(str, str2, onOfferWallListener);
            }
        });
    }

    public void initRewardedVideo(String str, String str2, DemandSource demandSource, DSRewardedVideoListener dSRewardedVideoListener) {
        CommandExecutor commandExecutor = this.mControllerCommandsExecuter;
        final String str3 = str;
        final String str4 = str2;
        final DemandSource demandSource2 = demandSource;
        final DSRewardedVideoListener dSRewardedVideoListener2 = dSRewardedVideoListener;
        AnonymousClass7 r1 = new Runnable() {
            public void run() {
                ControllerManager.this.mController.initRewardedVideo(str3, str4, demandSource2, dSRewardedVideoListener2);
            }
        };
        commandExecutor.executeCommand(r1);
    }

    public void showRewardedVideo(final JSONObject jSONObject, final DSRewardedVideoListener dSRewardedVideoListener) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.showRewardedVideo(jSONObject, dSRewardedVideoListener);
            }
        });
    }

    public void initInterstitial(String str, String str2, DemandSource demandSource, DSInterstitialListener dSInterstitialListener) {
        CommandExecutor commandExecutor = this.mControllerCommandsExecuter;
        final String str3 = str;
        final String str4 = str2;
        final DemandSource demandSource2 = demandSource;
        final DSInterstitialListener dSInterstitialListener2 = dSInterstitialListener;
        AnonymousClass9 r1 = new Runnable() {
            public void run() {
                ControllerManager.this.mController.initInterstitial(str3, str4, demandSource2, dSInterstitialListener2);
            }
        };
        commandExecutor.executeCommand(r1);
    }

    public void loadInterstitial(final String str, final DSInterstitialListener dSInterstitialListener) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.loadInterstitial(str, dSInterstitialListener);
            }
        });
    }

    public void loadInterstitial(final DemandSource demandSource, final Map<String, String> map, final DSInterstitialListener dSInterstitialListener) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.loadInterstitial(demandSource, map, dSInterstitialListener);
            }
        });
    }

    public void showInterstitial(final JSONObject jSONObject, final DSInterstitialListener dSInterstitialListener) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.showInterstitial(jSONObject, dSInterstitialListener);
            }
        });
    }

    public void showInterstitial(final DemandSource demandSource, final Map<String, String> map, final DSInterstitialListener dSInterstitialListener) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.showInterstitial(demandSource, map, dSInterstitialListener);
            }
        });
    }

    public boolean isInterstitialAdAvailable(String str) {
        if (!isControllerStateReady()) {
            return false;
        }
        return this.mController.isInterstitialAdAvailable(str);
    }

    public void initBanner(String str, String str2, DemandSource demandSource, DSBannerListener dSBannerListener) {
        CommandExecutor commandExecutor = this.mControllerCommandsExecuter;
        final String str3 = str;
        final String str4 = str2;
        final DemandSource demandSource2 = demandSource;
        final DSBannerListener dSBannerListener2 = dSBannerListener;
        AnonymousClass14 r1 = new Runnable() {
            public void run() {
                ControllerManager.this.mController.initBanner(str3, str4, demandSource2, dSBannerListener2);
            }
        };
        commandExecutor.executeCommand(r1);
    }

    public void loadBanner(final JSONObject jSONObject, final DSBannerListener dSBannerListener) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.loadBanner(jSONObject, dSBannerListener);
            }
        });
    }

    public void updateConsentInfo(final JSONObject jSONObject) {
        this.mControllerCommandsExecuter.executeCommand(new Runnable() {
            public void run() {
                ControllerManager.this.mController.updateConsentInfo(jSONObject);
            }
        });
    }

    public void enterForeground() {
        if (isControllerStateReady()) {
            this.mController.enterForeground();
        }
    }

    public void enterBackground() {
        if (isControllerStateReady()) {
            this.mController.enterBackground();
        }
    }

    public void registerConnectionReceiver(Activity activity) {
        if (isControllerStateReady()) {
            this.mController.registerConnectionReceiver(activity);
        }
    }

    public void unregisterConnectionReceiver(Activity activity) {
        if (isControllerStateReady()) {
            this.mController.unregisterConnectionReceiver(activity);
        }
    }

    public void destroy() {
        if (this.mGlobalControllerTimer != null) {
            this.mGlobalControllerTimer.cancel();
        }
        this.mGlobalControllerTimer = null;
        mUiHandler.post(new Runnable() {
            public void run() {
                ControllerManager.this.mController.destroy();
                ControllerManager.this.mController = null;
            }
        });
    }

    public IronSourceController getController() {
        return this.mController;
    }

    public void setCommunicationWithAdView(ISNAdView iSNAdView) {
        if (this.mController != null) {
            this.mController.setCommunicationWithAdView(iSNAdView);
        }
    }
}
