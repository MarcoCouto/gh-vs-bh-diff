package com.ironsource.sdk.controller;

interface ControllerEventListener {
    void handleControllerStageFailed(String str);

    void handleControllerStageLoaded();

    void handleControllerStageReady();
}
