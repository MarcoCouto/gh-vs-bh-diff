package com.ironsource.sdk.controller;

import java.util.HashMap;
import java.util.Map;

public class FeaturesManager {
    private static final String DEBUG_MODE = "debugMode";
    private static final String TAG = "FeaturesManager";
    private static volatile FeaturesManager sSoleInstance;
    private Map<String, ?> mDebugConfigurations;

    private FeaturesManager() {
        if (sSoleInstance == null) {
            this.mDebugConfigurations = new HashMap();
            return;
        }
        throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
    }

    public static FeaturesManager getInstance() {
        if (sSoleInstance == null) {
            synchronized (FeaturesManager.class) {
                if (sSoleInstance == null) {
                    sSoleInstance = new FeaturesManager();
                }
            }
        }
        return sSoleInstance;
    }

    public void updateDebugConfigurations(Map<String, Object> map) {
        if (map != null) {
            this.mDebugConfigurations = map;
        }
    }

    public int getDebugMode() {
        Integer valueOf = Integer.valueOf(0);
        try {
            if (this.mDebugConfigurations.containsKey("debugMode")) {
                valueOf = (Integer) this.mDebugConfigurations.get("debugMode");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return 0;
    }
}
