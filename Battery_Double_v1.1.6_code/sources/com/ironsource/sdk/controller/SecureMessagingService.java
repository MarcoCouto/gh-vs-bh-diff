package com.ironsource.sdk.controller;

import com.ironsource.sdk.utils.MD5Hashing;
import java.security.MessageDigest;
import java.util.UUID;

final class SecureMessagingService {
    private static final String SECURE_HASHING_ALGORITHM = "MD5";
    private String mTokenForMessaging;

    SecureMessagingService(String str) {
        this.mTokenForMessaging = str;
    }

    static String generateToken() {
        return UUID.randomUUID().toString();
    }

    private String hashMessage(String str) {
        try {
            return MD5Hashing.getHashString(str);
        } catch (Exception e) {
            e.printStackTrace();
            return hashWithDigest(str);
        }
    }

    private String hashWithDigest(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(SECURE_HASHING_ALGORITHM);
            instance.update(str.getBytes());
            return formatDigestToString(instance.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String formatDigestToString(byte[] bArr) throws Exception {
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() < 2) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("0");
                sb2.append(hexString);
                hexString = sb2.toString();
            }
            sb.append(hexString);
        }
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public boolean isValidMessage(String str, String str2, String str3) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            sb.append(this.mTokenForMessaging);
            return str3.equalsIgnoreCase(hashMessage(sb.toString()));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public String getTokenForMessaging() {
        return this.mTokenForMessaging;
    }
}
