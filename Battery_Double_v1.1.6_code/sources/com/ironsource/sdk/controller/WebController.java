package com.ironsource.sdk.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.ConsoleMessage;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebView.WebViewTransport;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Toast;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.environment.UrlHandler;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.ErrorCodes;
import com.ironsource.sdk.constants.Constants.ForceClosePosition;
import com.ironsource.sdk.constants.Constants.JSMethods;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.AdUnitsState;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSABCParameters;
import com.ironsource.sdk.data.SSAEnums.DebugMode;
import com.ironsource.sdk.data.SSAEnums.ProductType;
import com.ironsource.sdk.data.SSAFile;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.listeners.OnGenericFunctionListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnWebViewChangeListener;
import com.ironsource.sdk.listeners.internals.DSAdProductListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import com.ironsource.sdk.precache.DownloadManager;
import com.ironsource.sdk.precache.DownloadManager.OnPreCacheCompletion;
import com.ironsource.sdk.service.Connectivity.ConnectivityUtils;
import com.ironsource.sdk.service.ConnectivityAdapter;
import com.ironsource.sdk.utils.DeviceProperties;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import com.unity3d.ads.metadata.InAppPurchaseMetaData;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WebController extends WebView implements IronSourceController, OnPreCacheCompletion, DownloadListener {
    public static String APP_IDS = "appIds";
    public static String EXTERNAL_URL = "external_url";
    public static String IS_INSTALLED = "isInstalled";
    public static String IS_STORE = "is_store";
    /* access modifiers changed from: private */
    public static String JSON_KEY_FAIL = "fail";
    /* access modifiers changed from: private */
    public static String JSON_KEY_SUCCESS = "success";
    public static String REQUEST_ID = "requestId";
    public static String RESULT = "result";
    public static String SECONDARY_WEB_VIEW = "secondary_web_view";
    public static int mDebugMode;
    private final String GENERIC_MESSAGE = "We're sorry, some error occurred. we will investigate it";
    /* access modifiers changed from: private */
    public String PUB_TAG = IronSourceConstants.IRONSOURCE_CONFIG_NAME;
    /* access modifiers changed from: private */
    public String TAG = WebController.class.getSimpleName();
    /* access modifiers changed from: private */
    public DownloadManager downloadManager;
    /* access modifiers changed from: private */
    public Boolean isKitkatAndAbove = null;
    /* access modifiers changed from: private */
    public boolean isRemoveCloseEventHandler;
    /* access modifiers changed from: private */
    public String mApplicationKey;
    /* access modifiers changed from: private */
    public BannerJSAdapter mBannerJsAdapter;
    /* access modifiers changed from: private */
    public String mCacheDirectory;
    /* access modifiers changed from: private */
    public OnWebViewChangeListener mChangeListener;
    /* access modifiers changed from: private */
    public CountDownTimer mCloseEventTimer;
    /* access modifiers changed from: private */
    public ConnectivityAdapter mConnectivityAdapter;
    private ControllerAdapter mControllerAdapter;
    private String mControllerKeyPressed = "interrupt";
    /* access modifiers changed from: private */
    public FrameLayout mControllerLayout;
    /* access modifiers changed from: private */
    public ControllerEventListener mControllerListener;
    Context mCurrentActivityContext;
    /* access modifiers changed from: private */
    public View mCustomView;
    /* access modifiers changed from: private */
    public CustomViewCallback mCustomViewCallback;
    /* access modifiers changed from: private */
    public FrameLayout mCustomViewContainer;
    /* access modifiers changed from: private */
    public DSBannerListener mDSBannerListener;
    /* access modifiers changed from: private */
    public DSInterstitialListener mDSInterstitialListener;
    /* access modifiers changed from: private */
    public DSRewardedVideoListener mDSRewardedVideoListener;
    /* access modifiers changed from: private */
    public DemandSourceManager mDemandSourceManager;
    /* access modifiers changed from: private */
    public int mHiddenForceCloseHeight = 50;
    /* access modifiers changed from: private */
    public String mHiddenForceCloseLocation = ForceClosePosition.TOP_RIGHT;
    /* access modifiers changed from: private */
    public int mHiddenForceCloseWidth = 50;
    /* access modifiers changed from: private */
    public boolean mIsImmersive = false;
    /* access modifiers changed from: private */
    public boolean mIsWebControllerReady;
    public CountDownTimer mLoadControllerTimer;
    private boolean mOWCreditsMiss;
    private Map<String, String> mOWExtraParameters;
    private boolean mOWmiss;
    /* access modifiers changed from: private */
    public OMIDJSAdapter mOmidJsAdapter;
    /* access modifiers changed from: private */
    public OnGenericFunctionListener mOnGenericFunctionListener;
    /* access modifiers changed from: private */
    public OnOfferWallListener mOnOfferWallListener;
    private String mOrientationState;
    /* access modifiers changed from: private */
    public PermissionsJSAdapter mPermissionsJsAdapter;
    /* access modifiers changed from: private */
    public AdUnitsState mSavedState;
    private Object mSavedStateLocker = new Object();
    /* access modifiers changed from: private */
    public State mState;
    /* access modifiers changed from: private */
    public TokenJSAdapter mTokenJSAdapter;
    Handler mUiHandler;
    /* access modifiers changed from: private */
    public String mUserId;
    /* access modifiers changed from: private */
    public VideoEventsListener mVideoEventsListener;
    private ChromeClient mWebChromeClient;
    private WebViewMessagingMediator mWebViewMessagingMediator;

    private class ChromeClient extends WebChromeClient {
        private ChromeClient() {
        }

        public boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
            WebView webView2 = new WebView(webView.getContext());
            webView2.setWebChromeClient(this);
            webView2.setWebViewClient(new FrameBustWebViewClient());
            ((WebViewTransport) message.obj).setWebView(webView2);
            message.sendToTarget();
            Logger.i("onCreateWindow", "onCreateWindow");
            return true;
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            StringBuilder sb = new StringBuilder();
            sb.append(consoleMessage.message());
            sb.append(" -- From line ");
            sb.append(consoleMessage.lineNumber());
            sb.append(" of ");
            sb.append(consoleMessage.sourceId());
            Logger.i("MyApplication", sb.toString());
            return true;
        }

        public void onShowCustomView(View view, CustomViewCallback customViewCallback) {
            Logger.i("Test", "onShowCustomView");
            WebController.this.setVisibility(8);
            if (WebController.this.mCustomView != null) {
                Logger.i("Test", "mCustomView != null");
                customViewCallback.onCustomViewHidden();
                return;
            }
            Logger.i("Test", "mCustomView == null");
            WebController.this.mCustomViewContainer.addView(view);
            WebController.this.mCustomView = view;
            WebController.this.mCustomViewCallback = customViewCallback;
            WebController.this.mCustomViewContainer.setVisibility(0);
        }

        public View getVideoLoadingProgressView() {
            FrameLayout frameLayout = new FrameLayout(WebController.this.getCurrentActivityContext());
            frameLayout.setLayoutParams(new LayoutParams(-1, -1));
            return frameLayout;
        }

        public void onHideCustomView() {
            Logger.i("Test", "onHideCustomView");
            if (WebController.this.mCustomView != null) {
                WebController.this.mCustomView.setVisibility(8);
                WebController.this.mCustomViewContainer.removeView(WebController.this.mCustomView);
                WebController.this.mCustomView = null;
                WebController.this.mCustomViewContainer.setVisibility(8);
                WebController.this.mCustomViewCallback.onCustomViewHidden();
                WebController.this.setVisibility(0);
            }
        }
    }

    private class FrameBustWebViewClient extends WebViewClient {
        private FrameBustWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Context currentActivityContext = WebController.this.getCurrentActivityContext();
            Intent intent = new Intent(currentActivityContext, OpenUrlActivity.class);
            intent.putExtra(WebController.EXTERNAL_URL, str);
            intent.putExtra(WebController.SECONDARY_WEB_VIEW, false);
            currentActivityContext.startActivity(intent);
            return true;
        }
    }

    public class NativeAPI {
        volatile int udiaResults = 0;

        class JSCallbackTask {
            JSCallbackTask() {
            }

            /* access modifiers changed from: 0000 */
            public void sendMessage(boolean z, String str, String str2) {
                SSAObj sSAObj = new SSAObj();
                sSAObj.put(z ? WebController.JSON_KEY_SUCCESS : WebController.JSON_KEY_FAIL, str);
                sSAObj.put("data", str2);
                WebController.this.responseBack(sSAObj.toString(), z, null, null);
            }

            /* access modifiers changed from: 0000 */
            public void sendMessage(boolean z, String str, SSAObj sSAObj) {
                sSAObj.put(z ? WebController.JSON_KEY_SUCCESS : WebController.JSON_KEY_FAIL, str);
                WebController.this.responseBack(sSAObj.toString(), z, null, null);
            }

            /* access modifiers changed from: 0000 */
            public void sendMessage(boolean z, String str, JSONObject jSONObject) {
                String str2;
                if (z) {
                    try {
                        str2 = WebController.JSON_KEY_SUCCESS;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        e.getMessage();
                        return;
                    }
                } else {
                    str2 = WebController.JSON_KEY_FAIL;
                }
                jSONObject.put(str2, str);
                WebController.this.responseBack(jSONObject.toString(), z, null, null);
            }
        }

        public NativeAPI() {
        }

        @JavascriptInterface
        public void removeMessagingInterface(String str) {
            WebController.this.runOnUiThread(new Runnable() {
                public void run() {
                    WebController.this.removeJavascriptInterface(Constants.JAVASCRIPT_INERFACE_NAME_GENERATE_TOKEN);
                }
            });
        }

        @JavascriptInterface
        public void initController(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("initController(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            if (WebController.this.mLoadControllerTimer != null) {
                WebController.this.mLoadControllerTimer.cancel();
                WebController.this.mLoadControllerTimer = null;
            }
            if (sSAObj.containsKey(ParametersKeys.STAGE)) {
                String string = sSAObj.getString(ParametersKeys.STAGE);
                if (ParametersKeys.READY.equalsIgnoreCase(string)) {
                    WebController.this.mIsWebControllerReady = true;
                    WebController.this.mControllerListener.handleControllerStageReady();
                } else if (ParametersKeys.LOADED.equalsIgnoreCase(string)) {
                    WebController.this.mControllerListener.handleControllerStageLoaded();
                } else if (ParametersKeys.FAILED.equalsIgnoreCase(string)) {
                    WebController.this.mControllerListener.handleControllerStageFailed(ErrorCodes.CONTROLLER_FAILED_TO_INITIALIZE);
                } else {
                    Logger.i(WebController.this.TAG, "No STAGE mentioned! Should not get here!");
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0056, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x005a;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0060  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @JavascriptInterface
        public void getDeviceStatus(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getDeviceStatus(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            String access$1800 = WebController.this.extractSuccessFunctionToCall(str);
            String access$1900 = WebController.this.extractFailFunctionToCall(str);
            Object[] objArr = new Object[2];
            Object[] access$2000 = WebController.this.getDeviceParams(WebController.this.getContext());
            String str2 = (String) access$2000[0];
            if (((Boolean) access$2000[1]).booleanValue()) {
                if (!TextUtils.isEmpty(access$1900)) {
                    access$1800 = access$1900;
                    if (TextUtils.isEmpty(access$1800)) {
                        WebController.this.injectJavascript(WebController.this.generateJSToInject(access$1800, str2, JSMethods.ON_GET_DEVICE_STATUS_SUCCESS, JSMethods.ON_GET_DEVICE_STATUS_FAIL));
                        return;
                    }
                    return;
                }
            }
            access$1800 = null;
            if (TextUtils.isEmpty(access$1800)) {
            }
        }

        @JavascriptInterface
        public void getConnectivityInfo(String str) {
            String str2;
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getConnectivityInfo(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(WebController.JSON_KEY_SUCCESS);
            String string2 = sSAObj.getString(WebController.JSON_KEY_FAIL);
            JSONObject jSONObject = new JSONObject();
            if (WebController.this.mConnectivityAdapter != null) {
                jSONObject = WebController.this.mConnectivityAdapter.getConnectivityData(WebController.this.getContext());
            }
            if (jSONObject.length() > 0) {
                str2 = WebController.this.generateJSToInject(string, jSONObject.toString());
            } else {
                str2 = WebController.this.generateJSToInject(string2, WebController.this.parseToJson("errMsg", ErrorCodes.FAILED_TO_RETRIEVE_CONNECTION_INFO, null, null, null, null, null, null, null, false));
            }
            WebController.this.injectJavascript(str2);
        }

        @JavascriptInterface
        public void setMixedContentAlwaysAllow(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setMixedContentAlwaysAllow(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (VERSION.SDK_INT >= 21) {
                        WebController.this.getSettings().setMixedContentMode(0);
                    }
                }
            });
        }

        @JavascriptInterface
        public void getControllerConfig(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getControllerConfig(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            String string = new SSAObj(str).getString(WebController.JSON_KEY_SUCCESS);
            if (!TextUtils.isEmpty(string)) {
                String controllerConfig = SDKUtils.getControllerConfig();
                String testerParameters = SDKUtils.getTesterParameters();
                if (areTesterParametersValid(testerParameters)) {
                    try {
                        controllerConfig = addTesterParametersToConfig(controllerConfig, testerParameters);
                    } catch (JSONException unused) {
                        Logger.d(WebController.this.TAG, "getControllerConfig Error while parsing Tester AB Group parameters");
                    }
                }
                WebController.this.injectJavascript(WebController.this.generateJSToInject(string, controllerConfig));
            }
        }

        /* access modifiers changed from: 0000 */
        public String addTesterParametersToConfig(String str, String str2) throws JSONException {
            JSONObject jSONObject = new JSONObject(str);
            JSONObject jSONObject2 = new JSONObject(str2);
            jSONObject.putOpt("testerABGroup", jSONObject2.get("testerABGroup"));
            jSONObject.putOpt("testFriendlyName", jSONObject2.get("testFriendlyName"));
            return jSONObject.toString();
        }

        /* access modifiers changed from: 0000 */
        public boolean areTesterParametersValid(String str) {
            if (!TextUtils.isEmpty(str) && !str.contains("-1")) {
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    if (!jSONObject.getString("testerABGroup").isEmpty() && !jSONObject.getString("testFriendlyName").isEmpty()) {
                        return true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x005f, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0063;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0069  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @JavascriptInterface
        public void getApplicationInfo(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getApplicationInfo(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            String access$1800 = WebController.this.extractSuccessFunctionToCall(str);
            String access$1900 = WebController.this.extractFailFunctionToCall(str);
            SSAObj sSAObj = new SSAObj(str);
            Object[] objArr = new Object[2];
            Object[] access$2600 = WebController.this.getApplicationParams(sSAObj.getString(ParametersKeys.PRODUCT_TYPE), SDKUtils.fetchDemandSourceId(sSAObj));
            String str2 = (String) access$2600[0];
            if (((Boolean) access$2600[1]).booleanValue()) {
                if (!TextUtils.isEmpty(access$1900)) {
                    access$1800 = access$1900;
                    if (TextUtils.isEmpty(access$1800)) {
                        WebController.this.injectJavascript(WebController.this.generateJSToInject(access$1800, str2, JSMethods.ON_GET_APPLICATION_INFO_SUCCESS, JSMethods.ON_GET_APPLICATION_INFO_FAIL));
                        return;
                    }
                    return;
                }
            }
            access$1800 = null;
            if (TextUtils.isEmpty(access$1800)) {
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x005e, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0062;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @JavascriptInterface
        public void checkInstalledApps(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("checkInstalledApps(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            String access$1800 = WebController.this.extractSuccessFunctionToCall(str);
            String access$1900 = WebController.this.extractFailFunctionToCall(str);
            SSAObj sSAObj = new SSAObj(str);
            Object[] access$2700 = WebController.this.getAppsStatus(sSAObj.getString(WebController.APP_IDS), sSAObj.getString(WebController.REQUEST_ID));
            String str2 = (String) access$2700[0];
            if (((Boolean) access$2700[1]).booleanValue()) {
                if (!TextUtils.isEmpty(access$1900)) {
                    access$1800 = access$1900;
                    if (TextUtils.isEmpty(access$1800)) {
                        WebController.this.injectJavascript(WebController.this.generateJSToInject(access$1800, str2, JSMethods.ON_CHECK_INSTALLED_APPS_SUCCESS, JSMethods.ON_CHECK_INSTALLED_APPS_FAIL));
                        return;
                    }
                    return;
                }
            }
            access$1800 = null;
            if (TextUtils.isEmpty(access$1800)) {
            }
        }

        @JavascriptInterface
        public void saveFile(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("saveFile(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAFile sSAFile = new SSAFile(str);
            if (DeviceStatus.getAvailableMemorySizeInMegaBytes(WebController.this.mCacheDirectory) <= 0) {
                WebController.this.responseBack(str, false, DownloadManager.NO_DISK_SPACE, null);
            } else if (!SDKUtils.isExternalStorageAvailable()) {
                WebController.this.responseBack(str, false, DownloadManager.STORAGE_UNAVAILABLE, null);
            } else if (IronSourceStorageUtils.isFileCached(WebController.this.mCacheDirectory, sSAFile)) {
                WebController.this.responseBack(str, false, DownloadManager.FILE_ALREADY_EXIST, null);
            } else if (!ConnectivityService.isConnected(WebController.this.getContext())) {
                WebController.this.responseBack(str, false, DownloadManager.NO_NETWORK_CONNECTION, null);
            } else {
                WebController.this.responseBack(str, true, null, null);
                String lastUpdateTime = sSAFile.getLastUpdateTime();
                if (lastUpdateTime != null) {
                    String valueOf = String.valueOf(lastUpdateTime);
                    if (!TextUtils.isEmpty(valueOf)) {
                        String path = sSAFile.getPath();
                        if (path.contains("/")) {
                            String[] split = sSAFile.getPath().split("/");
                            path = split[split.length - 1];
                        }
                        IronSourceSharedPrefHelper.getSupersonicPrefHelper().setCampaignLastUpdate(path, valueOf);
                    }
                }
                WebController.this.downloadManager.downloadFile(sSAFile);
            }
        }

        @JavascriptInterface
        public void adUnitsReady(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("adUnitsReady(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            final AdUnitsReady adUnitsReady = new AdUnitsReady(str);
            if (!adUnitsReady.isNumOfAdUnitsExist()) {
                WebController.this.responseBack(str, false, ErrorCodes.NUM_OF_AD_UNITS_DO_NOT_EXIST, null);
                return;
            }
            WebController.this.responseBack(str, true, null, null);
            String productType = adUnitsReady.getProductType();
            if (ProductType.RewardedVideo.toString().equalsIgnoreCase(productType) && WebController.this.shouldNotifyDeveloper(productType)) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (Integer.parseInt(adUnitsReady.getNumOfAdUnits()) > 0) {
                            Log.d(WebController.this.TAG, "onRVInitSuccess()");
                            WebController.this.mDSRewardedVideoListener.onAdProductInitSuccess(ProductType.RewardedVideo, fetchDemandSourceId, adUnitsReady);
                            return;
                        }
                        WebController.this.mDSRewardedVideoListener.onRVNoMoreOffers(fetchDemandSourceId);
                    }
                });
            }
        }

        @JavascriptInterface
        public void iabTokenAPI(String str) {
            try {
                String access$600 = WebController.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("iabTokenAPI(");
                sb.append(str);
                sb.append(")");
                Logger.i(access$600, sb.toString());
                WebController.this.mTokenJSAdapter.call(new SSAObj(str).toString(), new JSCallbackTask());
            } catch (Exception e) {
                e.printStackTrace();
                String access$6002 = WebController.this.TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("iabTokenAPI failed with exception ");
                sb2.append(e.getMessage());
                Logger.i(access$6002, sb2.toString());
            }
        }

        @JavascriptInterface
        public void deleteFolder(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("deleteFolder(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAFile sSAFile = new SSAFile(str);
            if (!IronSourceStorageUtils.isPathExist(WebController.this.mCacheDirectory, sSAFile.getPath())) {
                WebController.this.responseBack(str, false, ErrorCodes.FOLDER_NOT_EXIST_MSG, "1");
                return;
            }
            WebController.this.responseBack(str, IronSourceStorageUtils.deleteFolder(WebController.this.mCacheDirectory, sSAFile.getPath()), null, null);
        }

        @JavascriptInterface
        public void deleteFile(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("deleteFile(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAFile sSAFile = new SSAFile(str);
            if (!IronSourceStorageUtils.isPathExist(WebController.this.mCacheDirectory, sSAFile.getPath())) {
                WebController.this.responseBack(str, false, ErrorCodes.FILE_NOT_EXIST_MSG, "1");
                return;
            }
            WebController.this.responseBack(str, IronSourceStorageUtils.deleteFile(WebController.this.mCacheDirectory, sSAFile.getPath(), sSAFile.getFile()), null, null);
        }

        @JavascriptInterface
        public void displayWebView(String str) {
            Intent intent;
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("displayWebView(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            SSAObj sSAObj = new SSAObj(str);
            boolean booleanValue = ((Boolean) sSAObj.get("display")).booleanValue();
            String string = sSAObj.getString(ParametersKeys.PRODUCT_TYPE);
            boolean z = sSAObj.getBoolean(ParametersKeys.IS_STANDALONE_VIEW);
            String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (booleanValue) {
                WebController.this.mIsImmersive = sSAObj.getBoolean(ParametersKeys.IMMERSIVE);
                boolean z2 = sSAObj.getBoolean(ParametersKeys.ACTIVITY_THEME_TRANSLUCENT);
                if (WebController.this.getState() != State.Display) {
                    WebController.this.setState(State.Display);
                    String access$6002 = WebController.this.TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("State: ");
                    sb2.append(WebController.this.mState);
                    Logger.i(access$6002, sb2.toString());
                    Context currentActivityContext = WebController.this.getCurrentActivityContext();
                    String orientationState = WebController.this.getOrientationState();
                    int applicationRotation = DeviceStatus.getApplicationRotation(currentActivityContext);
                    if (z) {
                        ControllerView controllerView = new ControllerView(currentActivityContext);
                        controllerView.addView(WebController.this.mControllerLayout);
                        controllerView.showInterstitial(WebController.this);
                        return;
                    }
                    if (z2) {
                        intent = new Intent(currentActivityContext, InterstitialActivity.class);
                    } else {
                        intent = new Intent(currentActivityContext, ControllerActivity.class);
                    }
                    if (ProductType.RewardedVideo.toString().equalsIgnoreCase(string)) {
                        if ("application".equals(orientationState)) {
                            orientationState = SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(WebController.this.getCurrentActivityContext()));
                        }
                        intent.putExtra(ParametersKeys.PRODUCT_TYPE, ProductType.RewardedVideo.toString());
                        WebController.this.mSavedState.adOpened(ProductType.RewardedVideo.ordinal());
                        WebController.this.mSavedState.setDisplayedDemandSourceId(fetchDemandSourceId);
                        if (WebController.this.shouldNotifyDeveloper(ProductType.RewardedVideo.toString())) {
                            WebController.this.mDSRewardedVideoListener.onAdProductOpen(ProductType.RewardedVideo, fetchDemandSourceId);
                        }
                    } else if (ProductType.OfferWall.toString().equalsIgnoreCase(string)) {
                        intent.putExtra(ParametersKeys.PRODUCT_TYPE, ProductType.OfferWall.toString());
                        WebController.this.mSavedState.adOpened(ProductType.OfferWall.ordinal());
                    } else if (ProductType.Interstitial.toString().equalsIgnoreCase(string)) {
                        if ("application".equals(orientationState)) {
                            orientationState = SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(WebController.this.getCurrentActivityContext()));
                        }
                        intent.putExtra(ParametersKeys.PRODUCT_TYPE, ProductType.Interstitial.toString());
                    }
                    intent.setFlags(536870912);
                    intent.putExtra(ParametersKeys.IMMERSIVE, WebController.this.mIsImmersive);
                    intent.putExtra(ParametersKeys.ORIENTATION_SET_FLAG, orientationState);
                    intent.putExtra(ParametersKeys.ROTATION_SET_FLAG, applicationRotation);
                    currentActivityContext.startActivity(intent);
                    return;
                }
                String access$6003 = WebController.this.TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("State: ");
                sb3.append(WebController.this.mState);
                Logger.i(access$6003, sb3.toString());
                return;
            }
            WebController.this.setState(State.Gone);
            WebController.this.closeWebView();
        }

        @JavascriptInterface
        public void getOrientation(String str) {
            String access$1800 = WebController.this.extractSuccessFunctionToCall(str);
            String jSONObject = SDKUtils.getOrientation(WebController.this.getCurrentActivityContext()).toString();
            if (!TextUtils.isEmpty(access$1800)) {
                WebController.this.injectJavascript(WebController.this.generateJSToInject(access$1800, jSONObject, JSMethods.ON_GET_ORIENTATION_SUCCESS, JSMethods.ON_GET_ORIENTATION_FAIL));
            }
        }

        @JavascriptInterface
        public void setOrientation(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setOrientation(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            String string = new SSAObj(str).getString("orientation");
            WebController.this.setOrientationState(string);
            int applicationRotation = DeviceStatus.getApplicationRotation(WebController.this.getCurrentActivityContext());
            if (WebController.this.mChangeListener != null) {
                WebController.this.mChangeListener.onOrientationChanged(string, applicationRotation);
            }
        }

        @JavascriptInterface
        public void getCachedFilesMap(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getCachedFilesMap(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            String access$1800 = WebController.this.extractSuccessFunctionToCall(str);
            if (!TextUtils.isEmpty(access$1800)) {
                SSAObj sSAObj = new SSAObj(str);
                if (!sSAObj.containsKey("path")) {
                    WebController.this.responseBack(str, false, ErrorCodes.PATH_KEY_DOES_NOT_EXIST, null);
                    return;
                }
                String str2 = (String) sSAObj.get("path");
                if (!IronSourceStorageUtils.isPathExist(WebController.this.mCacheDirectory, str2)) {
                    WebController.this.responseBack(str, false, ErrorCodes.PATH_FILE_DOES_NOT_EXIST_ON_DISK, null);
                    return;
                }
                WebController.this.injectJavascript(WebController.this.generateJSToInject(access$1800, IronSourceStorageUtils.getCachedFilesMap(WebController.this.mCacheDirectory, str2), JSMethods.ON_GET_CACHED_FILES_MAP_SUCCESS, JSMethods.ON_GET_CACHED_FILES_MAP_FAIL));
            }
        }

        private void callJavaScriptFunction(String str, String str2) {
            if (!TextUtils.isEmpty(str)) {
                WebController.this.injectJavascript(WebController.this.generateJSToInject(str, str2));
            }
        }

        @JavascriptInterface
        public void getDemandSourceState(String str) {
            String str2;
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getMediationState(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString("demandSourceName");
            String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            String string2 = sSAObj.getString(ParametersKeys.PRODUCT_TYPE);
            if (string2 != null && string != null) {
                try {
                    ProductType productType = SDKUtils.getProductType(string2);
                    if (productType != null) {
                        DemandSource demandSourceById = WebController.this.mDemandSourceManager.getDemandSourceById(productType, fetchDemandSourceId);
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put(ParametersKeys.PRODUCT_TYPE, string2);
                        jSONObject.put("demandSourceName", string);
                        jSONObject.put("demandSourceId", fetchDemandSourceId);
                        if (demandSourceById == null || demandSourceById.isMediationState(-1)) {
                            str2 = WebController.this.extractFailFunctionToCall(str);
                        } else {
                            str2 = WebController.this.extractSuccessFunctionToCall(str);
                            jSONObject.put("state", demandSourceById.getMediationState());
                        }
                        callJavaScriptFunction(str2, jSONObject.toString());
                    }
                } catch (Exception e) {
                    WebController.this.responseBack(str, false, e.getMessage(), null);
                    e.printStackTrace();
                }
            }
        }

        @JavascriptInterface
        public void adCredited(String str) {
            final String str2;
            final boolean z;
            final boolean z2;
            String access$4000 = WebController.this.PUB_TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("adCredited(");
            sb.append(str);
            sb.append(")");
            Log.d(access$4000, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(ParametersKeys.CREDITS);
            boolean z3 = false;
            final int parseInt = string != null ? Integer.parseInt(string) : 0;
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            final String string2 = sSAObj.getString(ParametersKeys.PRODUCT_TYPE);
            if (TextUtils.isEmpty(string2)) {
                Log.d(WebController.this.PUB_TAG, "adCredited | not product NAME !!!!");
            }
            if (ProductType.Interstitial.toString().equalsIgnoreCase(string2)) {
                handleAdCreditedOnInterstitial(fetchDemandSourceId, parseInt);
                return;
            }
            String string3 = sSAObj.getString(ParametersKeys.TOTAL);
            final int parseInt2 = string3 != null ? Integer.parseInt(string3) : 0;
            sSAObj.getBoolean("externalPoll");
            if (!ProductType.OfferWall.toString().equalsIgnoreCase(string2)) {
                str2 = null;
                z2 = false;
                z = false;
            } else if (sSAObj.isNull(InAppPurchaseMetaData.KEY_SIGNATURE) || sSAObj.isNull("timestamp") || sSAObj.isNull("totalCreditsFlag")) {
                WebController.this.responseBack(str, false, "One of the keys are missing: signature/timestamp/totalCreditsFlag", null);
                return;
            } else {
                String string4 = sSAObj.getString(InAppPurchaseMetaData.KEY_SIGNATURE);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(string3);
                sb2.append(WebController.this.mApplicationKey);
                sb2.append(WebController.this.mUserId);
                if (string4.equalsIgnoreCase(SDKUtils.getMD5(sb2.toString()))) {
                    z3 = true;
                } else {
                    WebController.this.responseBack(str, false, "Controller signature is not equal to SDK signature", null);
                }
                boolean z4 = sSAObj.getBoolean("totalCreditsFlag");
                str2 = sSAObj.getString("timestamp");
                z = z4;
                z2 = z3;
            }
            if (WebController.this.shouldNotifyDeveloper(string2)) {
                WebController webController = WebController.this;
                final String str3 = str;
                AnonymousClass4 r3 = new Runnable() {
                    public void run() {
                        if (string2.equalsIgnoreCase(ProductType.RewardedVideo.toString())) {
                            WebController.this.mDSRewardedVideoListener.onRVAdCredited(fetchDemandSourceId, parseInt);
                        } else if (string2.equalsIgnoreCase(ProductType.OfferWall.toString()) && z2 && WebController.this.mOnOfferWallListener.onOWAdCredited(parseInt, parseInt2, z) && !TextUtils.isEmpty(str2)) {
                            if (IronSourceSharedPrefHelper.getSupersonicPrefHelper().setLatestCompeltionsTime(str2, WebController.this.mApplicationKey, WebController.this.mUserId)) {
                                WebController.this.responseBack(str3, true, null, null);
                            } else {
                                WebController.this.responseBack(str3, false, "Time Stamp could not be stored", null);
                            }
                        }
                    }
                };
                webController.runOnUiThread(r3);
            }
        }

        private void handleAdCreditedOnInterstitial(final String str, final int i) {
            if (WebController.this.shouldNotifyDeveloper(ProductType.Interstitial.toString())) {
                DemandSource demandSourceById = WebController.this.mDemandSourceManager.getDemandSourceById(ProductType.Interstitial, str);
                if (demandSourceById != null && demandSourceById.isRewarded()) {
                    WebController.this.runOnUiThread(new Runnable() {
                        public void run() {
                            WebController.this.mDSInterstitialListener.onInterstitialAdRewarded(str, i);
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void removeCloseEventHandler(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("removeCloseEventHandler(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            if (WebController.this.mCloseEventTimer != null) {
                WebController.this.mCloseEventTimer.cancel();
            }
            WebController.this.isRemoveCloseEventHandler = true;
        }

        @JavascriptInterface
        public void onGetDeviceStatusSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetDeviceStatusSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_GET_DEVICE_STATUS_SUCCESS, str);
        }

        @JavascriptInterface
        public void onGetDeviceStatusFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetDeviceStatusFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_GET_DEVICE_STATUS_FAIL, str);
        }

        @JavascriptInterface
        public void onInitRewardedVideoSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onInitRewardedVideoSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().setSSABCParameters(new SSABCParameters(str));
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_INIT_REWARDED_VIDEO_SUCCESS, str);
        }

        @JavascriptInterface
        public void onInitRewardedVideoFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onInitRewardedVideoFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            DemandSource demandSourceById = WebController.this.mDemandSourceManager.getDemandSourceById(ProductType.RewardedVideo, fetchDemandSourceId);
            if (demandSourceById != null) {
                demandSourceById.setDemandSourceInitState(3);
            }
            if (WebController.this.shouldNotifyDeveloper(ProductType.RewardedVideo.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String access$600 = WebController.this.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onRVInitFail(message:");
                        sb.append(str);
                        sb.append(")");
                        Log.d(access$600, sb.toString());
                        WebController.this.mDSRewardedVideoListener.onAdProductInitFailed(ProductType.RewardedVideo, fetchDemandSourceId, str);
                    }
                });
            }
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_INIT_REWARDED_VIDEO_FAIL, str);
        }

        @JavascriptInterface
        public void onGetApplicationInfoSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetApplicationInfoSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_GET_APPLICATION_INFO_SUCCESS, str);
        }

        @JavascriptInterface
        public void onGetApplicationInfoFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetApplicationInfoFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_GET_APPLICATION_INFO_FAIL, str);
        }

        @JavascriptInterface
        public void onShowRewardedVideoSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onShowRewardedVideoSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_SHOW_REWARDED_VIDEO_SUCCESS, str);
        }

        @JavascriptInterface
        public void onShowRewardedVideoFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onShowRewardedVideoFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (WebController.this.shouldNotifyDeveloper(ProductType.RewardedVideo.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String access$600 = WebController.this.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onRVShowFail(message:");
                        sb.append(string);
                        sb.append(")");
                        Log.d(access$600, sb.toString());
                        WebController.this.mDSRewardedVideoListener.onRVShowFail(fetchDemandSourceId, str);
                    }
                });
            }
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_SHOW_REWARDED_VIDEO_FAIL, str);
        }

        @JavascriptInterface
        public void onGetCachedFilesMapSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetCachedFilesMapSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_GET_CACHED_FILES_MAP_SUCCESS, str);
        }

        @JavascriptInterface
        public void onGetCachedFilesMapFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetCachedFilesMapFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_GET_CACHED_FILES_MAP_FAIL, str);
        }

        @JavascriptInterface
        public void onShowOfferWallSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onShowOfferWallSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.mSavedState.adOpened(ProductType.OfferWall.ordinal());
            final String valueFromJsonObject = SDKUtils.getValueFromJsonObject(str, Constants.PLACEMENT_ID);
            if (WebController.this.shouldNotifyDeveloper(ProductType.OfferWall.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        WebController.this.mOnOfferWallListener.onOWShowSuccess(valueFromJsonObject);
                    }
                });
            }
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_SHOW_OFFER_WALL_SUCCESS, str);
        }

        @JavascriptInterface
        public void onShowOfferWallFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onShowOfferWallFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            final String string = new SSAObj(str).getString("errMsg");
            if (WebController.this.shouldNotifyDeveloper(ProductType.OfferWall.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        WebController.this.mOnOfferWallListener.onOWShowFail(str);
                    }
                });
            }
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_SHOW_OFFER_WALL_FAIL, str);
        }

        @JavascriptInterface
        public void onInitInterstitialSuccess(String str) {
            Logger.i(WebController.this.TAG, "onInitInterstitialSuccess()");
            WebController.this.toastingErrMsg(JSMethods.ON_INIT_INTERSTITIAL_SUCCESS, "true");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(WebController.this.TAG, "onInitInterstitialSuccess failed with no demand source");
                return;
            }
            if (WebController.this.shouldNotifyDeveloper(ProductType.Interstitial.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(WebController.this.TAG, "onInterstitialInitSuccess()");
                        WebController.this.mDSInterstitialListener.onAdProductInitSuccess(ProductType.Interstitial, fetchDemandSourceId, null);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onInitInterstitialFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onInitInterstitialFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(WebController.this.TAG, "onInitInterstitialSuccess failed with no demand source");
                return;
            }
            DemandSource demandSourceById = WebController.this.mDemandSourceManager.getDemandSourceById(ProductType.Interstitial, fetchDemandSourceId);
            if (demandSourceById != null) {
                demandSourceById.setDemandSourceInitState(3);
            }
            if (WebController.this.shouldNotifyDeveloper(ProductType.Interstitial.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String access$600 = WebController.this.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onInterstitialInitFail(message:");
                        sb.append(str);
                        sb.append(")");
                        Log.d(access$600, sb.toString());
                        WebController.this.mDSInterstitialListener.onAdProductInitFailed(ProductType.Interstitial, fetchDemandSourceId, str);
                    }
                });
            }
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_INIT_INTERSTITIAL_FAIL, str);
        }

        @JavascriptInterface
        public void adClicked(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("adClicked(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(ParametersKeys.PRODUCT_TYPE);
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (!TextUtils.isEmpty(fetchDemandSourceId)) {
                final ProductType access$4600 = WebController.this.getStringProductTypeAsEnum(string);
                final DSAdProductListener access$4700 = WebController.this.getAdProductListenerByProductType(access$4600);
                if (!(access$4600 == null || access$4700 == null)) {
                    WebController.this.runOnUiThread(new Runnable() {
                        public void run() {
                            access$4700.onAdProductClick(access$4600, fetchDemandSourceId);
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void onShowInterstitialSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onShowInterstitialSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.responseBack(str, true, null, null);
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(WebController.this.TAG, "onShowInterstitialSuccess called with no demand");
                return;
            }
            WebController.this.mSavedState.adOpened(ProductType.Interstitial.ordinal());
            WebController.this.mSavedState.setDisplayedDemandSourceId(fetchDemandSourceId);
            if (WebController.this.shouldNotifyDeveloper(ProductType.Interstitial.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        WebController.this.mDSInterstitialListener.onAdProductOpen(ProductType.Interstitial, fetchDemandSourceId);
                        WebController.this.mDSInterstitialListener.onInterstitialShowSuccess(fetchDemandSourceId);
                    }
                });
                WebController.this.toastingErrMsg(JSMethods.ON_SHOW_INTERSTITIAL_SUCCESS, str);
            }
            setInterstitialAvailability(fetchDemandSourceId, false);
        }

        private void setInterstitialAvailability(String str, boolean z) {
            DemandSource demandSourceById = WebController.this.mDemandSourceManager.getDemandSourceById(ProductType.Interstitial, str);
            if (demandSourceById != null) {
                demandSourceById.setAvailabilityState(z);
            }
        }

        @JavascriptInterface
        public void onInitOfferWallSuccess(String str) {
            WebController.this.toastingErrMsg(JSMethods.ON_INIT_OFFERWALL_SUCCESS, "true");
            WebController.this.mSavedState.setOfferwallInitSuccess(true);
            if (WebController.this.mSavedState.reportInitOfferwall()) {
                WebController.this.mSavedState.setOfferwallReportInit(false);
                if (WebController.this.shouldNotifyDeveloper(ProductType.OfferWall.toString())) {
                    WebController.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Log.d(WebController.this.TAG, "onOfferWallInitSuccess()");
                            WebController.this.mOnOfferWallListener.onOfferwallInitSuccess();
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void onInitOfferWallFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onInitOfferWallFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.mSavedState.setOfferwallInitSuccess(false);
            final String string = new SSAObj(str).getString("errMsg");
            if (WebController.this.mSavedState.reportInitOfferwall()) {
                WebController.this.mSavedState.setOfferwallReportInit(false);
                if (WebController.this.shouldNotifyDeveloper(ProductType.OfferWall.toString())) {
                    WebController.this.runOnUiThread(new Runnable() {
                        public void run() {
                            String str = string;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            String access$600 = WebController.this.TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("onOfferWallInitFail(message:");
                            sb.append(str);
                            sb.append(")");
                            Log.d(access$600, sb.toString());
                            WebController.this.mOnOfferWallListener.onOfferwallInitFail(str);
                        }
                    });
                }
            }
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_INIT_OFFERWALL_FAIL, str);
        }

        @JavascriptInterface
        public void onLoadInterstitialSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onLoadInterstitialSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            setInterstitialAvailability(fetchDemandSourceId, true);
            WebController.this.responseBack(str, true, null, null);
            if (WebController.this.shouldNotifyDeveloper(ProductType.Interstitial.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        WebController.this.mDSInterstitialListener.onInterstitialLoadSuccess(fetchDemandSourceId);
                    }
                });
            }
            WebController.this.toastingErrMsg(JSMethods.ON_LOAD_INTERSTITIAL_SUCCESS, "true");
        }

        @JavascriptInterface
        public void onLoadInterstitialFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onLoadInterstitialFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            WebController.this.responseBack(str, true, null, null);
            if (!TextUtils.isEmpty(fetchDemandSourceId)) {
                setInterstitialAvailability(fetchDemandSourceId, false);
                if (WebController.this.shouldNotifyDeveloper(ProductType.Interstitial.toString())) {
                    WebController.this.runOnUiThread(new Runnable() {
                        public void run() {
                            String str = string;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            WebController.this.mDSInterstitialListener.onInterstitialLoadFailed(fetchDemandSourceId, str);
                        }
                    });
                }
                WebController.this.toastingErrMsg(JSMethods.ON_LOAD_INTERSTITIAL_FAIL, "true");
            }
        }

        @JavascriptInterface
        public void onShowInterstitialFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onShowInterstitialFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            WebController.this.responseBack(str, true, null, null);
            if (!TextUtils.isEmpty(fetchDemandSourceId)) {
                setInterstitialAvailability(fetchDemandSourceId, false);
                if (WebController.this.shouldNotifyDeveloper(ProductType.Interstitial.toString())) {
                    WebController.this.runOnUiThread(new Runnable() {
                        public void run() {
                            String str = string;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            WebController.this.mDSInterstitialListener.onInterstitialShowFailed(fetchDemandSourceId, str);
                        }
                    });
                }
                WebController.this.toastingErrMsg(JSMethods.ON_SHOW_INTERSTITIAL_FAIL, str);
            }
        }

        @JavascriptInterface
        public void onInitBannerSuccess(String str) {
            Logger.i(WebController.this.TAG, "onInitBannerSuccess()");
            WebController.this.toastingErrMsg(JSMethods.ON_INIT_BANNER_SUCCESS, "true");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(WebController.this.TAG, "onInitBannerSuccess failed with no demand source");
                return;
            }
            if (WebController.this.shouldNotifyDeveloper(ProductType.Banner.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(WebController.this.TAG, "onBannerInitSuccess()");
                        WebController.this.mDSBannerListener.onAdProductInitSuccess(ProductType.Banner, fetchDemandSourceId, null);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onInitBannerFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onInitBannerFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(WebController.this.TAG, "onInitBannerFail failed with no demand source");
                return;
            }
            DemandSource demandSourceById = WebController.this.mDemandSourceManager.getDemandSourceById(ProductType.Banner, fetchDemandSourceId);
            if (demandSourceById != null) {
                demandSourceById.setDemandSourceInitState(3);
            }
            if (WebController.this.shouldNotifyDeveloper(ProductType.Banner.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String access$600 = WebController.this.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("onBannerInitFail(message:");
                        sb.append(str);
                        sb.append(")");
                        Log.d(access$600, sb.toString());
                        WebController.this.mDSBannerListener.onAdProductInitFailed(ProductType.Banner, fetchDemandSourceId, str);
                    }
                });
            }
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_INIT_BANNER_FAIL, str);
        }

        @JavascriptInterface
        public void onLoadBannerSuccess(String str) {
            Logger.i(WebController.this.TAG, "onLoadBannerSuccess()");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            WebController.this.responseBack(str, true, null, null);
            if (WebController.this.shouldNotifyDeveloper(ProductType.Banner.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(WebController.this.TAG, "onBannerLoadSuccess()");
                        WebController.this.mDSBannerListener.onBannerLoadSuccess(fetchDemandSourceId);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onLoadBannerFail(String str) {
            Logger.i(WebController.this.TAG, "onLoadBannerFail()");
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            WebController.this.responseBack(str, true, null, null);
            if (!TextUtils.isEmpty(fetchDemandSourceId) && WebController.this.shouldNotifyDeveloper(ProductType.Banner.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(WebController.this.TAG, "onLoadBannerFail()");
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        WebController.this.mDSBannerListener.onBannerLoadFail(fetchDemandSourceId, str);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onGenericFunctionSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGenericFunctionSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            if (WebController.this.mOnGenericFunctionListener == null) {
                Logger.d(WebController.this.TAG, "genericFunctionListener was not found");
                return;
            }
            WebController.this.runOnUiThread(new Runnable() {
                public void run() {
                    WebController.this.mOnGenericFunctionListener.onGFSuccess();
                }
            });
            WebController.this.responseBack(str, true, null, null);
        }

        @JavascriptInterface
        public void onGenericFunctionFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGenericFunctionFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            if (WebController.this.mOnGenericFunctionListener == null) {
                Logger.d(WebController.this.TAG, "genericFunctionListener was not found");
                return;
            }
            final String string = new SSAObj(str).getString("errMsg");
            WebController.this.runOnUiThread(new Runnable() {
                public void run() {
                    WebController.this.mOnGenericFunctionListener.onGFFail(string);
                }
            });
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_GENERIC_FUNCTION_FAIL, str);
        }

        @JavascriptInterface
        public void openUrl(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("openUrl(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString("url");
            String string2 = sSAObj.getString("method");
            Context currentActivityContext = WebController.this.getCurrentActivityContext();
            try {
                if (string2.equalsIgnoreCase(ParametersKeys.EXTERNAL_BROWSER)) {
                    UrlHandler.openUrl(currentActivityContext, string);
                } else if (string2.equalsIgnoreCase(ParametersKeys.WEB_VIEW)) {
                    Intent intent = new Intent(currentActivityContext, OpenUrlActivity.class);
                    intent.putExtra(WebController.EXTERNAL_URL, string);
                    intent.putExtra(WebController.SECONDARY_WEB_VIEW, true);
                    intent.putExtra(ParametersKeys.IMMERSIVE, WebController.this.mIsImmersive);
                    currentActivityContext.startActivity(intent);
                } else if (string2.equalsIgnoreCase("store")) {
                    Intent intent2 = new Intent(currentActivityContext, OpenUrlActivity.class);
                    intent2.putExtra(WebController.EXTERNAL_URL, string);
                    intent2.putExtra(WebController.IS_STORE, true);
                    intent2.putExtra(WebController.SECONDARY_WEB_VIEW, true);
                    currentActivityContext.startActivity(intent2);
                }
            } catch (Exception e) {
                WebController.this.responseBack(str, false, e.getMessage(), null);
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void setForceClose(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setForceClose(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString("width");
            String string2 = sSAObj.getString("height");
            WebController.this.mHiddenForceCloseWidth = Integer.parseInt(string);
            WebController.this.mHiddenForceCloseHeight = Integer.parseInt(string2);
            WebController.this.mHiddenForceCloseLocation = sSAObj.getString(ParametersKeys.POSITION);
        }

        @JavascriptInterface
        public void setBackButtonState(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setBackButtonState(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().setBackButtonState(new SSAObj(str).getString("state"));
        }

        @JavascriptInterface
        public void setStoreSearchKeys(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setStoreSearchKeys(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().setSearchKeys(str);
        }

        @JavascriptInterface
        public void setWebviewBackgroundColor(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setWebviewBackgroundColor(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.setWebviewBackground(str);
        }

        @JavascriptInterface
        public void toggleUDIA(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("toggleUDIA(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(ParametersKeys.TOGGLE)) {
                WebController.this.responseBack(str, false, ErrorCodes.TOGGLE_KEY_DOES_NOT_EXIST, null);
                return;
            }
            int parseInt = Integer.parseInt(sSAObj.getString(ParametersKeys.TOGGLE));
            if (parseInt != 0) {
                String binaryString = Integer.toBinaryString(parseInt);
                if (TextUtils.isEmpty(binaryString)) {
                    WebController.this.responseBack(str, false, ErrorCodes.FIALED_TO_CONVERT_TOGGLE, null);
                    return;
                }
                if (binaryString.toCharArray()[3] == '0') {
                    IronSourceSharedPrefHelper.getSupersonicPrefHelper().setShouldRegisterSessions(true);
                } else {
                    IronSourceSharedPrefHelper.getSupersonicPrefHelper().setShouldRegisterSessions(false);
                }
            }
        }

        @JavascriptInterface
        public void getUDIA(String str) {
            this.udiaResults = 0;
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getUDIA(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.extractSuccessFunctionToCall(str);
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(ParametersKeys.GET_BY_FLAG)) {
                WebController.this.responseBack(str, false, ErrorCodes.GET_BY_FLAG_KEY_DOES_NOT_EXIST, null);
                return;
            }
            int parseInt = Integer.parseInt(sSAObj.getString(ParametersKeys.GET_BY_FLAG));
            if (parseInt != 0) {
                String binaryString = Integer.toBinaryString(parseInt);
                if (TextUtils.isEmpty(binaryString)) {
                    WebController.this.responseBack(str, false, ErrorCodes.FIALED_TO_CONVERT_GET_BY_FLAG, null);
                    return;
                }
                char[] charArray = new StringBuilder(binaryString).reverse().toString().toCharArray();
                JSONArray jSONArray = new JSONArray();
                if (charArray[3] == '0') {
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put("sessions", IronSourceSharedPrefHelper.getSupersonicPrefHelper().getSessions());
                        IronSourceSharedPrefHelper.getSupersonicPrefHelper().deleteSessions();
                        jSONArray.put(jSONObject);
                    } catch (JSONException unused) {
                    }
                }
            }
        }

        @JavascriptInterface
        public void setUserUniqueId(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setUserUniqueId(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(ParametersKeys.USER_UNIQUE_ID) || !sSAObj.containsKey(ParametersKeys.PRODUCT_TYPE)) {
                WebController.this.responseBack(str, false, ErrorCodes.UNIQUE_ID_OR_PRODUCT_TYPE_DOES_NOT_EXIST, null);
                return;
            }
            if (IronSourceSharedPrefHelper.getSupersonicPrefHelper().setUniqueId(sSAObj.getString(ParametersKeys.USER_UNIQUE_ID))) {
                WebController.this.responseBack(str, true, null, null);
            } else {
                WebController.this.responseBack(str, false, ErrorCodes.SET_USER_UNIQUE_ID_FAILED, null);
            }
        }

        @JavascriptInterface
        public void getUserUniqueId(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getUserUniqueId(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(ParametersKeys.PRODUCT_TYPE)) {
                WebController.this.responseBack(str, false, ErrorCodes.PRODUCT_TYPE_DOES_NOT_EXIST, null);
                return;
            }
            String access$1800 = WebController.this.extractSuccessFunctionToCall(str);
            if (!TextUtils.isEmpty(access$1800)) {
                String string = sSAObj.getString(ParametersKeys.PRODUCT_TYPE);
                WebController.this.injectJavascript(WebController.this.generateJSToInject(access$1800, WebController.this.parseToJson(ParametersKeys.USER_UNIQUE_ID, IronSourceSharedPrefHelper.getSupersonicPrefHelper().getUniqueId(string), ParametersKeys.PRODUCT_TYPE, string, null, null, null, null, null, false), JSMethods.ON_GET_USER_UNIQUE_ID_SUCCESS, JSMethods.ON_GET_USER_UNIQUE_ID_FAIL));
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0062, code lost:
            if (android.text.TextUtils.isEmpty(r5) == false) goto L_0x0064;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0055, code lost:
            if (android.text.TextUtils.isEmpty(r5) == false) goto L_0x0064;
         */
        @JavascriptInterface
        public void getAppsInstallTime(String str) {
            boolean z;
            String str2;
            String str3;
            String str4;
            try {
                str2 = DeviceStatus.getAppsInstallTime(WebController.this.getContext(), Boolean.parseBoolean(new SSAObj(str).getString(ParametersKeys.INCLUDE_SYSTEM_APPS))).toString();
                z = false;
            } catch (Exception e) {
                String access$600 = WebController.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("getAppsInstallTime failed(");
                sb.append(e.getLocalizedMessage());
                sb.append(")");
                Logger.i(access$600, sb.toString());
                str2 = e.getLocalizedMessage();
                z = true;
            }
            String str5 = null;
            if (z) {
                str4 = WebController.this.extractFailFunctionToCall(str);
            } else {
                str4 = WebController.this.extractSuccessFunctionToCall(str);
            }
            str5 = str4;
            if (!TextUtils.isEmpty(str5)) {
                try {
                    str3 = URLDecoder.decode(str2, Charset.defaultCharset().name());
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                    str3 = str2;
                }
                WebController.this.injectJavascript(WebController.this.generateJSToInject(str5, str3));
            }
        }

        @JavascriptInterface
        public void onGetUserUniqueIdSuccess(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetUserUniqueIdSuccess(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
        }

        @JavascriptInterface
        public void onGetUserUniqueIdFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetUserUniqueIdFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
        }

        @JavascriptInterface
        public void onOfferWallGeneric(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onOfferWallGeneric(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            if (WebController.this.shouldNotifyDeveloper(ProductType.OfferWall.toString())) {
                WebController.this.mOnOfferWallListener.onOWGeneric("", "");
            }
        }

        @JavascriptInterface
        public void setUserData(String str) {
            String str2 = str;
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setUserData(");
            sb.append(str2);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str2);
            if (!sSAObj.containsKey(ParametersKeys.KEY)) {
                WebController.this.responseBack(str2, false, ErrorCodes.KEY_DOES_NOT_EXIST, null);
            } else if (!sSAObj.containsKey("value")) {
                WebController.this.responseBack(str2, false, ErrorCodes.VALUE_DOES_NOT_EXIST, null);
            } else {
                String string = sSAObj.getString(ParametersKeys.KEY);
                String string2 = sSAObj.getString("value");
                if (IronSourceSharedPrefHelper.getSupersonicPrefHelper().setUserData(string, string2)) {
                    WebController.this.injectJavascript(WebController.this.generateJSToInject(WebController.this.extractSuccessFunctionToCall(str2), WebController.this.parseToJson(string, string2, null, null, null, null, null, null, null, false)));
                } else {
                    WebController.this.responseBack(str2, false, "SetUserData failed writing to shared preferences", null);
                }
            }
        }

        @JavascriptInterface
        public void getUserData(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getUserData(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(ParametersKeys.KEY)) {
                WebController.this.responseBack(str, false, ErrorCodes.KEY_DOES_NOT_EXIST, null);
                return;
            }
            String access$1800 = WebController.this.extractSuccessFunctionToCall(str);
            String string = sSAObj.getString(ParametersKeys.KEY);
            WebController.this.injectJavascript(WebController.this.generateJSToInject(access$1800, WebController.this.parseToJson(string, IronSourceSharedPrefHelper.getSupersonicPrefHelper().getUserData(string), null, null, null, null, null, null, null, false)));
        }

        @JavascriptInterface
        public void onGetUserCreditsFail(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onGetUserCreditsFail(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            final String string = new SSAObj(str).getString("errMsg");
            if (WebController.this.shouldNotifyDeveloper(ProductType.OfferWall.toString())) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        WebController.this.mOnOfferWallListener.onGetOWCreditsFailed(str);
                    }
                });
            }
            WebController.this.responseBack(str, true, null, null);
            WebController.this.toastingErrMsg(JSMethods.ON_GET_USER_CREDITS_FAILED, str);
        }

        @JavascriptInterface
        public void onAdWindowsClosed(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onAdWindowsClosed(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            WebController.this.mSavedState.adClosed();
            WebController.this.mSavedState.setDisplayedDemandSourceId(null);
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(ParametersKeys.PRODUCT_TYPE);
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            final ProductType access$4600 = WebController.this.getStringProductTypeAsEnum(string);
            String access$4000 = WebController.this.PUB_TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("onAdClosed() with type ");
            sb2.append(access$4600);
            Log.d(access$4000, sb2.toString());
            if (WebController.this.shouldNotifyDeveloper(string)) {
                WebController.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (access$4600 == ProductType.RewardedVideo || access$4600 == ProductType.Interstitial) {
                            DSAdProductListener access$4700 = WebController.this.getAdProductListenerByProductType(access$4600);
                            if (access$4700 != null) {
                                access$4700.onAdProductClose(access$4600, fetchDemandSourceId);
                            }
                        } else if (access$4600 == ProductType.OfferWall) {
                            WebController.this.mOnOfferWallListener.onOWAdClosed();
                        }
                    }
                });
            }
        }

        @JavascriptInterface
        public void onVideoStatusChanged(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onVideoStatusChanged(");
            sb.append(str);
            sb.append(")");
            Log.d(access$600, sb.toString());
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(ParametersKeys.PRODUCT_TYPE);
            if (WebController.this.mVideoEventsListener != null && !TextUtils.isEmpty(string)) {
                String string2 = sSAObj.getString("status");
                if (ParametersKeys.VIDEO_STATUS_STARTED.equalsIgnoreCase(string2)) {
                    WebController.this.mVideoEventsListener.onVideoStarted();
                } else if ("paused".equalsIgnoreCase(string2)) {
                    WebController.this.mVideoEventsListener.onVideoPaused();
                } else if ("playing".equalsIgnoreCase(string2)) {
                    WebController.this.mVideoEventsListener.onVideoResumed();
                } else if (ParametersKeys.VIDEO_STATUS_ENDED.equalsIgnoreCase(string2)) {
                    WebController.this.mVideoEventsListener.onVideoEnded();
                } else if ("stopped".equalsIgnoreCase(string2)) {
                    WebController.this.mVideoEventsListener.onVideoStopped();
                } else {
                    String access$6002 = WebController.this.TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("onVideoStatusChanged: unknown status: ");
                    sb2.append(string2);
                    Logger.i(access$6002, sb2.toString());
                }
            }
        }

        @JavascriptInterface
        public void postAdEventNotification(String str) {
            String str2 = str;
            try {
                String access$600 = WebController.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("postAdEventNotification(");
                sb.append(str2);
                sb.append(")");
                Logger.i(access$600, sb.toString());
                SSAObj sSAObj = new SSAObj(str2);
                final String string = sSAObj.getString("eventName");
                if (TextUtils.isEmpty(string)) {
                    WebController.this.responseBack(str2, false, ErrorCodes.EVENT_NAME_DOES_NOT_EXIST, null);
                    return;
                }
                String string2 = sSAObj.getString(ParametersKeys.NOTIFICATION_DEMAND_SOURCE_NAME);
                String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
                String str3 = !TextUtils.isEmpty(fetchDemandSourceId) ? fetchDemandSourceId : string2;
                JSONObject jSONObject = (JSONObject) sSAObj.get(ParametersKeys.EXTRA_DATA);
                String string3 = sSAObj.getString(ParametersKeys.PRODUCT_TYPE);
                ProductType access$4600 = WebController.this.getStringProductTypeAsEnum(string3);
                if (WebController.this.shouldNotifyDeveloper(string3)) {
                    String access$1800 = WebController.this.extractSuccessFunctionToCall(str2);
                    if (!TextUtils.isEmpty(access$1800)) {
                        WebController.this.injectJavascript(WebController.this.generateJSToInject(access$1800, WebController.this.parseToJson(ParametersKeys.PRODUCT_TYPE, string3, "eventName", string, "demandSourceName", string2, "demandSourceId", str3, null, false), JSMethods.POST_AD_EVENT_NOTIFICATION_SUCCESS, JSMethods.POST_AD_EVENT_NOTIFICATION_FAIL));
                    }
                    WebController webController = WebController.this;
                    final ProductType productType = access$4600;
                    final String str4 = str3;
                    final JSONObject jSONObject2 = jSONObject;
                    AnonymousClass27 r1 = new Runnable() {
                        public void run() {
                            if (productType == ProductType.Interstitial || productType == ProductType.RewardedVideo) {
                                DSAdProductListener access$4700 = WebController.this.getAdProductListenerByProductType(productType);
                                if (access$4700 != null) {
                                    access$4700.onAdProductEventNotificationReceived(productType, str4, string, jSONObject2);
                                }
                            } else if (productType == ProductType.OfferWall) {
                                WebController.this.mOnOfferWallListener.onOfferwallEventNotificationReceived(string, jSONObject2);
                            }
                        }
                    };
                    webController.runOnUiThread(r1);
                } else {
                    WebController.this.responseBack(str2, false, ErrorCodes.PRODUCT_TYPE_DOES_NOT_EXIST, null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: 0000 */
        public void sendUnauthorizedError(String str) {
            WebController.this.injectJavascript(WebController.this.generateJSToInject(JSMethods.ON_UNAUTHORIZED_MESSAGE, str, null, null));
        }

        @JavascriptInterface
        public void bannerViewAPI(String str) {
            try {
                WebController.this.mBannerJsAdapter.sendMessageToISNAdView(str);
            } catch (Exception e) {
                e.printStackTrace();
                String access$600 = WebController.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("bannerViewAPI failed with exception ");
                sb.append(e.getMessage());
                Logger.e(access$600, sb.toString());
            }
        }

        @JavascriptInterface
        public void omidAPI(final String str) {
            WebController.this.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        String access$600 = WebController.this.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("omidAPI(");
                        sb.append(str);
                        sb.append(")");
                        Logger.i(access$600, sb.toString());
                        WebController.this.mOmidJsAdapter.call(new SSAObj(str).toString(), new JSCallbackTask(), WebController.this.getWebview());
                    } catch (Exception e) {
                        e.printStackTrace();
                        String access$6002 = WebController.this.TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("omidAPI failed with exception ");
                        sb2.append(e.getMessage());
                        Logger.i(access$6002, sb2.toString());
                    }
                }
            });
        }

        @JavascriptInterface
        public void permissionsAPI(String str) {
            try {
                String access$600 = WebController.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("permissionsAPI(");
                sb.append(str);
                sb.append(")");
                Logger.i(access$600, sb.toString());
                WebController.this.mPermissionsJsAdapter.call(new SSAObj(str).toString(), new JSCallbackTask());
            } catch (Exception e) {
                e.printStackTrace();
                String access$6002 = WebController.this.TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("permissionsAPI failed with exception ");
                sb2.append(e.getMessage());
                Logger.i(access$6002, sb2.toString());
            }
        }

        @JavascriptInterface
        public void getDeviceVolume(String str) {
            String access$600 = WebController.this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("getDeviceVolume(");
            sb.append(str);
            sb.append(")");
            Logger.i(access$600, sb.toString());
            try {
                float deviceVolume = DeviceProperties.getInstance(WebController.this.getCurrentActivityContext()).getDeviceVolume(WebController.this.getCurrentActivityContext());
                SSAObj sSAObj = new SSAObj(str);
                sSAObj.put(RequestParameters.DEVICE_VOLUME, String.valueOf(deviceVolume));
                WebController.this.responseBack(sSAObj.toString(), true, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private interface OnInitProductHandler {
        void handleInitProductFailed(String str, ProductType productType, DemandSource demandSource);
    }

    static class Result {
        String methodName;
        String script;

        Result() {
        }
    }

    public enum State {
        Display,
        Gone
    }

    private class SupersonicWebViewTouchListener implements OnTouchListener {
        private SupersonicWebViewTouchListener() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                String access$600 = WebController.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("X:");
                int i = (int) x;
                sb.append(i);
                sb.append(" Y:");
                int i2 = (int) y;
                sb.append(i2);
                Logger.i(access$600, sb.toString());
                int deviceWidth = DeviceStatus.getDeviceWidth();
                int deviceHeight = DeviceStatus.getDeviceHeight();
                String access$6002 = WebController.this.TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Width:");
                sb2.append(deviceWidth);
                sb2.append(" Height:");
                sb2.append(deviceHeight);
                Logger.i(access$6002, sb2.toString());
                int dpToPx = SDKUtils.dpToPx((long) WebController.this.mHiddenForceCloseWidth);
                int dpToPx2 = SDKUtils.dpToPx((long) WebController.this.mHiddenForceCloseHeight);
                if (ForceClosePosition.TOP_RIGHT.equalsIgnoreCase(WebController.this.mHiddenForceCloseLocation)) {
                    i = deviceWidth - i;
                } else if (!ForceClosePosition.TOP_LEFT.equalsIgnoreCase(WebController.this.mHiddenForceCloseLocation)) {
                    if (ForceClosePosition.BOTTOM_RIGHT.equalsIgnoreCase(WebController.this.mHiddenForceCloseLocation)) {
                        i = deviceWidth - i;
                        i2 = deviceHeight - i2;
                    } else if (ForceClosePosition.BOTTOM_LEFT.equalsIgnoreCase(WebController.this.mHiddenForceCloseLocation)) {
                        i2 = deviceHeight - i2;
                    } else {
                        i = 0;
                        i2 = 0;
                    }
                }
                if (i <= dpToPx && i2 <= dpToPx2) {
                    WebController.this.isRemoveCloseEventHandler = false;
                    if (WebController.this.mCloseEventTimer != null) {
                        WebController.this.mCloseEventTimer.cancel();
                    }
                    WebController webController = WebController.this;
                    AnonymousClass1 r1 = new CountDownTimer(AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS, 500) {
                        public void onTick(long j) {
                            String access$600 = WebController.this.TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Close Event Timer Tick ");
                            sb.append(j);
                            Logger.i(access$600, sb.toString());
                        }

                        public void onFinish() {
                            Logger.i(WebController.this.TAG, "Close Event Timer Finish");
                            if (WebController.this.isRemoveCloseEventHandler) {
                                WebController.this.isRemoveCloseEventHandler = false;
                            } else {
                                WebController.this.engageEnd("forceClose");
                            }
                        }
                    };
                    webController.mCloseEventTimer = r1.start();
                }
            }
            return false;
        }
    }

    private class ViewClient extends WebViewClient {
        private ViewClient() {
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            Logger.i("onPageStarted", str);
            super.onPageStarted(webView, str, bitmap);
        }

        public void onPageFinished(WebView webView, String str) {
            Logger.i("onPageFinished", str);
            if (str.contains("adUnit") || str.contains("index.html")) {
                WebController.this.pageFinished();
            }
            super.onPageFinished(webView, str);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(" ");
            sb.append(str);
            Logger.i("onReceivedError", sb.toString());
            super.onReceivedError(webView, i, str, str2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Logger.i("shouldOverrideUrlLoading", str);
            try {
                if (WebController.this.handleSearchKeysURLs(str)) {
                    WebController.this.interceptedUrlToStore();
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return super.shouldOverrideUrlLoading(webView, str);
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            boolean z;
            Logger.i("shouldInterceptRequest", str);
            try {
                z = new URL(str).getFile().contains("mraid.js");
            } catch (MalformedURLException unused) {
                z = false;
            }
            if (z) {
                StringBuilder sb = new StringBuilder();
                sb.append("file://");
                sb.append(WebController.this.mCacheDirectory);
                sb.append(File.separator);
                sb.append("mraid.js");
                String sb2 = sb.toString();
                try {
                    new FileInputStream(new File(sb2));
                    return new WebResourceResponse("text/javascript", "UTF-8", getClass().getResourceAsStream(sb2));
                } catch (FileNotFoundException unused2) {
                }
            }
            return super.shouldInterceptRequest(webView, str);
        }
    }

    /* access modifiers changed from: private */
    public WebView getWebview() {
        return this;
    }

    private Map<String, String> getExtraParamsByProduct(ProductType productType) {
        if (productType == ProductType.OfferWall) {
            return this.mOWExtraParameters;
        }
        return null;
    }

    public WebController(Activity activity, DemandSourceManager demandSourceManager, ControllerEventListener controllerEventListener) {
        super(activity.getApplicationContext());
        this.mCurrentActivityContext = new MutableContextWrapper(activity);
        Logger.i(this.TAG, "C'tor");
        this.mControllerListener = controllerEventListener;
        this.mCacheDirectory = initializeCacheDirectory(this.mCurrentActivityContext.getApplicationContext());
        this.mDemandSourceManager = demandSourceManager;
        initLayout(this.mCurrentActivityContext);
        this.mSavedState = new AdUnitsState();
        this.downloadManager = getDownloadManager();
        this.downloadManager.setOnPreCacheCompletion(this);
        this.mWebChromeClient = new ChromeClient();
        setWebViewClient(new ViewClient());
        setWebChromeClient(this.mWebChromeClient);
        setWebViewSettings();
        createSecuredCommunication();
        setDownloadListener(this);
        setOnTouchListener(new SupersonicWebViewTouchListener());
        this.mUiHandler = createMainThreadHandler();
        this.mConnectivityAdapter = createConnectivityAdapter(activity);
        registerConnectionReceiver(activity);
        setDebugMode(FeaturesManager.getInstance().getDebugMode());
    }

    private ConnectivityAdapter createConnectivityAdapter(Context context) {
        return new ConnectivityAdapter(SDKUtils.getControllerConfigAsJSONObject(), context) {
            public void onConnected(String str, JSONObject jSONObject) {
                if (WebController.this.mIsWebControllerReady) {
                    WebController.this.sendConnectionTypeChanged(str);
                }
            }

            public void onDisconnected() {
                if (WebController.this.mIsWebControllerReady) {
                    WebController.this.sendConnectionTypeChanged("none");
                }
            }

            public void onStatusChanged(String str, JSONObject jSONObject) {
                if (jSONObject != null && WebController.this.mIsWebControllerReady) {
                    try {
                        jSONObject.put(RequestParameters.CONNECTION_TYPE, str);
                        WebController.this.sendConnectionInfoChanged(jSONObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    private void createSecuredCommunication() {
        SecureMessagingService secureMessagingService = new SecureMessagingService(SecureMessagingService.generateToken());
        addJavascriptInterface(createControllerMessageHandler(secureMessagingService), Constants.JAVASCRIPT_INTERFACE_NAME);
        addJavascriptInterface(createSecureMessagingInterface(secureMessagingService), Constants.JAVASCRIPT_INERFACE_NAME_GENERATE_TOKEN);
    }

    /* access modifiers changed from: 0000 */
    public ControllerMessageHandler createControllerMessageHandler(SecureMessagingService secureMessagingService) {
        return new ControllerMessageHandler(new ControllerAdapter(new NativeAPI()), secureMessagingService);
    }

    /* access modifiers changed from: 0000 */
    public SecureMessagingInterface createSecureMessagingInterface(SecureMessagingService secureMessagingService) {
        return new SecureMessagingInterface(secureMessagingService);
    }

    /* access modifiers changed from: 0000 */
    public Handler createMainThreadHandler() {
        return new Handler(Looper.getMainLooper());
    }

    /* access modifiers changed from: 0000 */
    public DownloadManager getDownloadManager() {
        return DownloadManager.getInstance(this.mCacheDirectory);
    }

    /* access modifiers changed from: 0000 */
    public String initializeCacheDirectory(Context context) {
        return IronSourceStorageUtils.initializeCacheDirectory(context.getApplicationContext());
    }

    public void addOmidJSInterface(OMIDJSAdapter oMIDJSAdapter) {
        this.mOmidJsAdapter = oMIDJSAdapter;
    }

    public void addPermissionsJSInterface(PermissionsJSAdapter permissionsJSAdapter) {
        this.mPermissionsJsAdapter = permissionsJSAdapter;
    }

    public void addBannerJSInterface(BannerJSAdapter bannerJSAdapter) {
        this.mBannerJsAdapter = bannerJSAdapter;
        this.mBannerJsAdapter.setCommunicationWithController(getControllerDelegate());
    }

    public void addTokenJSInterface(TokenJSAdapter tokenJSAdapter) {
        this.mTokenJSAdapter = tokenJSAdapter;
    }

    public void notifyLifeCycle(String str, String str2) {
        injectJavascript(generateJSToInject(JSMethods.ON_NATIVE_LIFE_CYCLE_EVENT, parseToJson(ParametersKeys.LIFE_CYCLE_EVENT, str2, ParametersKeys.PRODUCT_TYPE, str, null, null, null, null, null, false)));
    }

    public WebViewMessagingMediator getControllerDelegate() {
        if (this.mWebViewMessagingMediator == null) {
            this.mWebViewMessagingMediator = new WebViewMessagingMediator() {
                public void sendMessageToController(String str, JSONObject jSONObject) {
                    WebController.this.injectJavascript(WebController.this.generateJSToInject(str, jSONObject.toString()));
                }
            };
        }
        return this.mWebViewMessagingMediator;
    }

    private void initLayout(Context context) {
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        this.mControllerLayout = new FrameLayout(context);
        this.mCustomViewContainer = new FrameLayout(context);
        this.mCustomViewContainer.setLayoutParams(new LayoutParams(-1, -1));
        this.mCustomViewContainer.setVisibility(8);
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new LayoutParams(-1, -1));
        frameLayout.addView(this);
        this.mControllerLayout.addView(this.mCustomViewContainer, layoutParams);
        this.mControllerLayout.addView(frameLayout);
    }

    private void setWebViewSettings() {
        WebSettings settings = getSettings();
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        settings.setBuiltInZoomControls(false);
        settings.setJavaScriptEnabled(true);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setGeolocationEnabled(true);
        settings.setGeolocationDatabasePath("/data/data/org.itri.html5webview/databases/");
        settings.setDomStorageEnabled(true);
        try {
            setDisplayZoomControls(settings);
            setMediaPlaybackJellyBean(settings);
        } catch (Throwable th) {
            String str = this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("setWebSettings - ");
            sb.append(th.toString());
            Logger.e(str, sb.toString());
        }
    }

    private void setDisplayZoomControls(WebSettings webSettings) {
        if (VERSION.SDK_INT > 11) {
            webSettings.setDisplayZoomControls(false);
        }
    }

    public WebBackForwardList saveState(Bundle bundle) {
        return super.saveState(bundle);
    }

    @SuppressLint({"NewApi"})
    private void setMediaPlaybackJellyBean(WebSettings webSettings) {
        if (VERSION.SDK_INT >= 17) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }
    }

    @SuppressLint({"NewApi"})
    private void setWebDebuggingEnabled() {
        if (VERSION.SDK_INT >= 19) {
            setWebContentsDebuggingEnabled(true);
        }
    }

    public void downloadController() {
        IronSourceStorageUtils.deleteFile(this.mCacheDirectory, "", Constants.MOBILE_CONTROLLER_HTML);
        String controllerUrl = SDKUtils.getControllerUrl();
        SSAFile sSAFile = new SSAFile(controllerUrl, "");
        if (!this.downloadManager.isMobileControllerThreadLive()) {
            String str = this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Download Mobile Controller: ");
            sb.append(controllerUrl);
            Logger.i(str, sb.toString());
            this.downloadManager.downloadMobileControllerFile(sSAFile);
            return;
        }
        Logger.i(this.TAG, "Download Mobile Controller: already alive");
    }

    public void setDebugMode(int i) {
        mDebugMode = i;
    }

    public int getDebugMode() {
        return mDebugMode;
    }

    /* access modifiers changed from: private */
    public boolean shouldNotifyDeveloper(String str) {
        boolean z = false;
        if (TextUtils.isEmpty(str)) {
            Logger.d(this.TAG, "Trying to trigger a listener - no product was found");
            return false;
        }
        if (!str.equalsIgnoreCase(ProductType.Interstitial.toString()) ? !str.equalsIgnoreCase(ProductType.RewardedVideo.toString()) ? !str.equalsIgnoreCase(ProductType.Banner.toString()) ? (str.equalsIgnoreCase(ProductType.OfferWall.toString()) || str.equalsIgnoreCase(ProductType.OfferWallCredits.toString())) && this.mOnOfferWallListener != null : this.mDSBannerListener != null : this.mDSRewardedVideoListener != null : this.mDSInterstitialListener != null) {
            z = true;
        }
        if (!z) {
            String str2 = this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Trying to trigger a listener - no listener was found for product ");
            sb.append(str);
            Logger.d(str2, sb.toString());
        }
        return z;
    }

    public void setOrientationState(String str) {
        this.mOrientationState = str;
    }

    public String getOrientationState() {
        return this.mOrientationState;
    }

    /* access modifiers changed from: private */
    public DSAdProductListener getAdProductListenerByProductType(ProductType productType) {
        if (productType == ProductType.Interstitial) {
            return this.mDSInterstitialListener;
        }
        if (productType == ProductType.RewardedVideo) {
            return this.mDSRewardedVideoListener;
        }
        if (productType == ProductType.Banner) {
            return this.mDSBannerListener;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public ProductType getStringProductTypeAsEnum(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.equalsIgnoreCase(ProductType.Interstitial.toString())) {
            return ProductType.Interstitial;
        }
        if (str.equalsIgnoreCase(ProductType.RewardedVideo.toString())) {
            return ProductType.RewardedVideo;
        }
        if (str.equalsIgnoreCase(ProductType.OfferWall.toString())) {
            return ProductType.OfferWall;
        }
        if (str.equalsIgnoreCase(ProductType.Banner.toString())) {
            return ProductType.Banner;
        }
        return null;
    }

    public void setVideoEventsListener(VideoEventsListener videoEventsListener) {
        this.mVideoEventsListener = videoEventsListener;
    }

    public void removeVideoEventsListener() {
        this.mVideoEventsListener = null;
    }

    /* access modifiers changed from: private */
    public void setWebviewBackground(String str) {
        String string = new SSAObj(str).getString("color");
        setBackgroundColor(!"transparent".equalsIgnoreCase(string) ? Color.parseColor(string) : 0);
    }

    public void load(int i) {
        try {
            loadUrl("about:blank");
        } catch (Throwable th) {
            String str = this.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("WebViewController:: load: ");
            sb.append(th.toString());
            Logger.e(str, sb.toString());
            new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=webviewLoadBlank"});
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("file://");
        sb2.append(this.mCacheDirectory);
        sb2.append(File.separator);
        sb2.append(Constants.MOBILE_CONTROLLER_HTML);
        String sb3 = sb2.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append(this.mCacheDirectory);
        sb4.append(File.separator);
        sb4.append(Constants.MOBILE_CONTROLLER_HTML);
        if (new File(sb4.toString()).exists()) {
            JSONObject controllerConfigAsJSONObject = SDKUtils.getControllerConfigAsJSONObject();
            setWebDebuggingEnabled(controllerConfigAsJSONObject);
            String requestParameters = getRequestParameters(controllerConfigAsJSONObject);
            StringBuilder sb5 = new StringBuilder();
            sb5.append(sb3);
            sb5.append("?");
            sb5.append(requestParameters);
            String sb6 = sb5.toString();
            final int i2 = i;
            AnonymousClass3 r3 = new CountDownTimer(50000, 1000) {
                public void onTick(long j) {
                    String access$600 = WebController.this.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Loading Controller Timer Tick ");
                    sb.append(j);
                    Logger.i(access$600, sb.toString());
                }

                public void onFinish() {
                    Logger.i(WebController.this.TAG, "Loading Controller Timer Finish");
                    if (i2 == 3) {
                        WebController.this.mControllerListener.handleControllerStageFailed(ErrorCodes.CONTROLLER_FAILED_TO_LOAD);
                    } else {
                        WebController.this.load(i2 + 1);
                    }
                }
            };
            this.mLoadControllerTimer = r3.start();
            try {
                loadUrl(sb6);
            } catch (Throwable th2) {
                String str2 = this.TAG;
                StringBuilder sb7 = new StringBuilder();
                sb7.append("WebViewController:: load: ");
                sb7.append(th2.toString());
                Logger.e(str2, sb7.toString());
                new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=webviewLoadWithPath"});
            }
            String str3 = this.TAG;
            StringBuilder sb8 = new StringBuilder();
            sb8.append("load(): ");
            sb8.append(sb6);
            Logger.i(str3, sb8.toString());
            return;
        }
        Logger.i(this.TAG, "load(): Mobile Controller HTML Does not exist");
        new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=htmlControllerDoesNotExistOnFileSystem"});
    }

    private void setWebDebuggingEnabled(JSONObject jSONObject) {
        if (jSONObject.optBoolean("inspectWebview")) {
            setWebDebuggingEnabled();
        }
    }

    private void initProduct(String str, String str2, ProductType productType, DemandSource demandSource, OnInitProductHandler onInitProductHandler) {
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
            onInitProductHandler.handleInitProductFailed("User id or Application key are missing", productType, demandSource);
            return;
        }
        IronSourceSharedPrefHelper.getSupersonicPrefHelper().setApplicationKey(str);
        injectJavascript(createInitProductJSMethod(productType, demandSource).script);
    }

    public void initRewardedVideo(String str, String str2, DemandSource demandSource, DSRewardedVideoListener dSRewardedVideoListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mDSRewardedVideoListener = dSRewardedVideoListener;
        this.mSavedState.setRVAppKey(str);
        this.mSavedState.setRVUserId(str2);
        initProduct(str, str2, ProductType.RewardedVideo, demandSource, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, ProductType productType, DemandSource demandSource) {
                WebController.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void initInterstitial(String str, String str2, DemandSource demandSource, DSInterstitialListener dSInterstitialListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mDSInterstitialListener = dSInterstitialListener;
        this.mSavedState.setInterstitialAppKey(this.mApplicationKey);
        this.mSavedState.setInterstitialUserId(this.mUserId);
        initProduct(this.mApplicationKey, this.mUserId, ProductType.Interstitial, demandSource, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, ProductType productType, DemandSource demandSource) {
                WebController.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void loadInterstitial(String str, DSInterstitialListener dSInterstitialListener) {
        HashMap hashMap = new HashMap();
        hashMap.put("demandSourceName", str);
        String flatMapToJsonAsString = SDKUtils.flatMapToJsonAsString(hashMap);
        this.mSavedState.setReportLoadInterstitial(str, true);
        injectJavascript(generateJSToInject(JSMethods.LOAD_INTERSTITIAL, flatMapToJsonAsString, JSMethods.ON_LOAD_INTERSTITIAL_SUCCESS, JSMethods.ON_LOAD_INTERSTITIAL_FAIL));
    }

    public void loadInterstitial(DemandSource demandSource, Map<String, String> map, DSInterstitialListener dSInterstitialListener) {
        handleLoadAd(demandSource, map);
    }

    private void handleLoadAd(DemandSource demandSource, Map<String, String> map) {
        Map mergeHashMaps = SDKUtils.mergeHashMaps(new Map[]{map, demandSource.convertToMap()});
        this.mSavedState.setReportLoadInterstitial(demandSource.getId(), true);
        injectJavascript(generateJSToInject(JSMethods.LOAD_INTERSTITIAL, SDKUtils.flatMapToJsonAsString(mergeHashMaps), JSMethods.ON_LOAD_INTERSTITIAL_SUCCESS, JSMethods.ON_LOAD_INTERSTITIAL_FAIL));
    }

    public void showInterstitial(JSONObject jSONObject, DSInterstitialListener dSInterstitialListener) {
        injectJavascript(createShowProductJSMethod(ProductType.Interstitial, jSONObject));
    }

    public void showInterstitial(DemandSource demandSource, Map<String, String> map, DSInterstitialListener dSInterstitialListener) {
        injectJavascript(createShowProductJSMethod(ProductType.Interstitial, new JSONObject(SDKUtils.mergeHashMaps(new Map[]{map, demandSource.convertToMap()}))));
    }

    public boolean isInterstitialAdAvailable(String str) {
        DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(ProductType.Interstitial, str);
        return demandSourceById != null && demandSourceById.getAvailabilityState();
    }

    public void initOfferWall(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mOWExtraParameters = map;
        this.mOnOfferWallListener = onOfferWallListener;
        this.mSavedState.setOfferWallExtraParams(this.mOWExtraParameters);
        this.mSavedState.setOfferwallReportInit(true);
        initProduct(this.mApplicationKey, this.mUserId, ProductType.OfferWall, null, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, ProductType productType, DemandSource demandSource) {
                WebController.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void showOfferWall(Map<String, String> map) {
        this.mOWExtraParameters = map;
        injectJavascript(generateJSToInject(JSMethods.SHOW_OFFER_WALL, JSMethods.ON_SHOW_OFFER_WALL_SUCCESS, JSMethods.ON_SHOW_OFFER_WALL_FAIL));
    }

    public void getOfferWallCredits(String str, String str2, OnOfferWallListener onOfferWallListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mOnOfferWallListener = onOfferWallListener;
        initProduct(this.mApplicationKey, this.mUserId, ProductType.OfferWallCredits, null, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, ProductType productType, DemandSource demandSource) {
                WebController.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void initBanner(String str, String str2, DemandSource demandSource, DSBannerListener dSBannerListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mDSBannerListener = dSBannerListener;
        initProduct(str, str2, ProductType.Banner, demandSource, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, ProductType productType, DemandSource demandSource) {
                WebController.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void loadBanner(JSONObject jSONObject, DSBannerListener dSBannerListener) {
        if (jSONObject != null) {
            injectJavascript(generateJSToInject(JSMethods.LOAD_BANNER, jSONObject.toString(), JSMethods.ON_LOAD_BANNER_SUCCESS, JSMethods.ON_LOAD_BANNER_FAIL));
        }
    }

    public void updateConsentInfo(JSONObject jSONObject) {
        injectJavascript(generateJSToInject(JSMethods.UPDATE_CONSENT_INFO, jSONObject != null ? jSONObject.toString() : null));
    }

    private Result createInitProductJSMethod(ProductType productType, DemandSource demandSource) {
        Result result = new Result();
        if (productType == ProductType.RewardedVideo || productType == ProductType.Interstitial || productType == ProductType.OfferWall || productType == ProductType.Banner) {
            HashMap hashMap = new HashMap();
            hashMap.put(RequestParameters.APPLICATION_KEY, this.mApplicationKey);
            hashMap.put(RequestParameters.APPLICATION_USER_ID, this.mUserId);
            if (demandSource != null) {
                if (demandSource.getExtraParams() != null) {
                    hashMap.putAll(demandSource.getExtraParams());
                }
                hashMap.put("demandSourceName", demandSource.getDemandSourceName());
                hashMap.put("demandSourceId", demandSource.getId());
            }
            Map extraParamsByProduct = getExtraParamsByProduct(productType);
            if (extraParamsByProduct != null) {
                hashMap.putAll(extraParamsByProduct);
            }
            String flatMapToJsonAsString = SDKUtils.flatMapToJsonAsString(hashMap);
            JSMethods initMethodByProduct = JSMethods.getInitMethodByProduct(productType);
            String generateJSToInject = generateJSToInject(initMethodByProduct.methodName, flatMapToJsonAsString, initMethodByProduct.successCallbackName, initMethodByProduct.failureCallbackName);
            result.methodName = initMethodByProduct.methodName;
            result.script = generateJSToInject;
        } else if (productType == ProductType.OfferWallCredits) {
            String generateJSToInject2 = generateJSToInject(JSMethods.GET_USER_CREDITS, parseToJson(ParametersKeys.PRODUCT_TYPE, ParametersKeys.OFFER_WALL, RequestParameters.APPLICATION_KEY, this.mApplicationKey, RequestParameters.APPLICATION_USER_ID, this.mUserId, null, null, null, false), "null", JSMethods.ON_GET_USER_CREDITS_FAILED);
            result.methodName = JSMethods.GET_USER_CREDITS;
            result.script = generateJSToInject2;
        }
        return result;
    }

    private String createShowProductJSMethod(ProductType productType, JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        hashMap.put("sessionDepth", Integer.toString(jSONObject.optInt("sessionDepth")));
        String optString = jSONObject.optString("demandSourceName");
        String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(jSONObject);
        DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(productType, fetchDemandSourceId);
        if (demandSourceById != null) {
            if (demandSourceById.getExtraParams() != null) {
                hashMap.putAll(demandSourceById.getExtraParams());
            }
            if (!TextUtils.isEmpty(optString)) {
                hashMap.put("demandSourceName", optString);
            }
            if (!TextUtils.isEmpty(fetchDemandSourceId)) {
                hashMap.put("demandSourceId", fetchDemandSourceId);
            }
        }
        Map extraParamsByProduct = getExtraParamsByProduct(productType);
        if (extraParamsByProduct != null) {
            hashMap.putAll(extraParamsByProduct);
        }
        String flatMapToJsonAsString = SDKUtils.flatMapToJsonAsString(hashMap);
        JSMethods showMethodByProduct = JSMethods.getShowMethodByProduct(productType);
        return generateJSToInject(showMethodByProduct.methodName, flatMapToJsonAsString, showMethodByProduct.successCallbackName, showMethodByProduct.failureCallbackName);
    }

    /* access modifiers changed from: 0000 */
    public void setMissProduct(ProductType productType, DemandSource demandSource) {
        if (productType == ProductType.RewardedVideo || productType == ProductType.Interstitial || productType == ProductType.Banner) {
            if (demandSource != null) {
                demandSource.setDemandSourceInitState(1);
            }
        } else if (productType == ProductType.OfferWall) {
            this.mOWmiss = true;
        } else if (productType == ProductType.OfferWallCredits) {
            this.mOWCreditsMiss = true;
        }
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("setMissProduct(");
        sb.append(productType);
        sb.append(")");
        Logger.i(str, sb.toString());
    }

    /* access modifiers changed from: private */
    public void triggerOnControllerInitProductFail(final String str, final ProductType productType, final DemandSource demandSource) {
        if (shouldNotifyDeveloper(productType.toString())) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (ProductType.RewardedVideo == productType || ProductType.Interstitial == productType || ProductType.Banner == productType) {
                        if (demandSource != null && !TextUtils.isEmpty(demandSource.getId())) {
                            DSAdProductListener access$4700 = WebController.this.getAdProductListenerByProductType(productType);
                            String access$600 = WebController.this.TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("onAdProductInitFailed (message:");
                            sb.append(str);
                            sb.append(")(");
                            sb.append(productType);
                            sb.append(")");
                            Log.d(access$600, sb.toString());
                            if (access$4700 != null) {
                                access$4700.onAdProductInitFailed(productType, demandSource.getId(), str);
                            }
                        }
                    } else if (ProductType.OfferWall == productType) {
                        WebController.this.mOnOfferWallListener.onOfferwallInitFail(str);
                    } else if (ProductType.OfferWallCredits == productType) {
                        WebController.this.mOnOfferWallListener.onGetOWCreditsFailed(str);
                    }
                }
            });
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, DSRewardedVideoListener dSRewardedVideoListener) {
        injectJavascript(createShowProductJSMethod(ProductType.RewardedVideo, jSONObject));
    }

    public void assetCached(String str, String str2) {
        injectJavascript(generateJSToInject(JSMethods.ASSET_CACHED, parseToJson(ParametersKeys.FILE, str, "path", str2, null, null, null, null, null, false)));
    }

    public void assetCachedFailed(String str, String str2, String str3) {
        injectJavascript(generateJSToInject(JSMethods.ASSET_CACHED_FAILED, parseToJson(ParametersKeys.FILE, str, "path", str2, "errMsg", str3, null, null, null, false)));
    }

    public void enterBackground() {
        injectJavascript(generateJSToInject(JSMethods.ENTER_BACKGROUND));
    }

    public void enterForeground() {
        injectJavascript(generateJSToInject(JSMethods.ENTER_FOREGROUND));
    }

    public void viewableChange(boolean z, String str) {
        injectJavascript(generateJSToInject(JSMethods.VIEWABLE_CHANGE, parseToJson(ParametersKeys.WEB_VIEW, str, null, null, null, null, null, null, ParametersKeys.IS_VIEWABLE, z)));
    }

    public void nativeNavigationPressed(String str) {
        injectJavascript(generateJSToInject(JSMethods.NATIVE_NAVIGATION_PRESSED, parseToJson("action", str, null, null, null, null, null, null, null, false)));
    }

    public void pageFinished() {
        injectJavascript(generateJSToInject(JSMethods.PAGE_FINISHED));
    }

    public void interceptedUrlToStore() {
        injectJavascript(generateJSToInject(JSMethods.INTERCEPTED_URL_TO_STORE));
    }

    public void failedToStartStoreActivity(String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            str2 = ErrorCodes.STORE_ACTIVITY_FAILED_UNKNOWN_URL;
        }
        String str3 = str2;
        if (TextUtils.isEmpty(str)) {
            str = ErrorCodes.STORE_ACTIVITY_FAILED_REASON_UNSPECIFIED;
        }
        injectJavascript(generateJSToInject(JSMethods.FAILED_TO_START_STORE_ACTIVITY, parseToJson("errMsg", str, "url", str3, null, null, null, null, null, false)));
    }

    /* access modifiers changed from: private */
    public void injectJavascript(String str) {
        if (!TextUtils.isEmpty(str)) {
            String str2 = "empty";
            if (getDebugMode() == DebugMode.MODE_0.getValue()) {
                str2 = "console.log(\"JS exeption: \" + JSON.stringify(e));";
            } else if (getDebugMode() >= DebugMode.MODE_1.getValue() && getDebugMode() <= DebugMode.MODE_3.getValue()) {
                str2 = "console.log(\"JS exeption: \" + JSON.stringify(e));";
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("try{");
            sb.append(str);
            sb.append("}catch(e){");
            sb.append(str2);
            sb.append("}");
            StringBuilder sb2 = new StringBuilder();
            sb2.append("javascript:");
            sb2.append(sb.toString());
            final String sb3 = sb2.toString();
            runOnUiThread(new Runnable() {
                public void run() {
                    Logger.i(WebController.this.TAG, sb3);
                    try {
                        if (WebController.this.isKitkatAndAbove != null) {
                            if (WebController.this.isKitkatAndAbove.booleanValue()) {
                                WebController.this.evaluateJavascriptKitKat(sb.toString());
                            } else {
                                WebController.this.loadUrl(sb3);
                            }
                        } else if (VERSION.SDK_INT >= 19) {
                            WebController.this.evaluateJavascriptKitKat(sb.toString());
                            WebController.this.isKitkatAndAbove = Boolean.valueOf(true);
                        } else {
                            WebController.this.loadUrl(sb3);
                            WebController.this.isKitkatAndAbove = Boolean.valueOf(false);
                        }
                    } catch (NoSuchMethodError e) {
                        String access$600 = WebController.this.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("evaluateJavascrip NoSuchMethodError: SDK version=");
                        sb.append(VERSION.SDK_INT);
                        sb.append(" ");
                        sb.append(e);
                        Logger.e(access$600, sb.toString());
                        WebController.this.loadUrl(sb3);
                        WebController.this.isKitkatAndAbove = Boolean.valueOf(false);
                    } catch (Throwable th) {
                        String access$6002 = WebController.this.TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("injectJavascript: ");
                        sb2.append(th.toString());
                        Logger.e(access$6002, sb2.toString());
                        new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=injectJavaScript"});
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public void evaluateJavascriptKitKat(String str) {
        evaluateJavascript(str, null);
    }

    public Context getCurrentActivityContext() {
        return ((MutableContextWrapper) this.mCurrentActivityContext).getBaseContext();
    }

    private String getRequestParameters(JSONObject jSONObject) {
        DeviceProperties instance = DeviceProperties.getInstance(getContext());
        StringBuilder sb = new StringBuilder();
        String supersonicSdkVersion = DeviceProperties.getSupersonicSdkVersion();
        if (!TextUtils.isEmpty(supersonicSdkVersion)) {
            sb.append(RequestParameters.SDK_VERSION);
            sb.append(RequestParameters.EQUAL);
            sb.append(supersonicSdkVersion);
            sb.append(RequestParameters.AMPERSAND);
        }
        String deviceOsType = instance.getDeviceOsType();
        if (!TextUtils.isEmpty(deviceOsType)) {
            sb.append(RequestParameters.DEVICE_OS);
            sb.append(RequestParameters.EQUAL);
            sb.append(deviceOsType);
        }
        Uri parse = Uri.parse(SDKUtils.getControllerUrl());
        if (parse != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(parse.getScheme());
            sb2.append(":");
            String sb3 = sb2.toString();
            String host = parse.getHost();
            int port = parse.getPort();
            if (port != -1) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append(host);
                sb4.append(":");
                sb4.append(port);
                host = sb4.toString();
            }
            sb.append(RequestParameters.AMPERSAND);
            sb.append(RequestParameters.PROTOCOL);
            sb.append(RequestParameters.EQUAL);
            sb.append(sb3);
            sb.append(RequestParameters.AMPERSAND);
            sb.append(RequestParameters.DOMAIN);
            sb.append(RequestParameters.EQUAL);
            sb.append(host);
            if (jSONObject.keys().hasNext()) {
                try {
                    String jSONObject2 = new JSONObject(jSONObject, new String[]{RequestParameters.IS_SECURED, RequestParameters.APPLICATION_KEY}).toString();
                    if (!TextUtils.isEmpty(jSONObject2)) {
                        sb.append(RequestParameters.AMPERSAND);
                        sb.append(RequestParameters.CONTROLLER_CONFIG);
                        sb.append(RequestParameters.EQUAL);
                        sb.append(jSONObject2);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sb.append(RequestParameters.AMPERSAND);
            sb.append("debug");
            sb.append(RequestParameters.EQUAL);
            sb.append(getDebugMode());
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public void closeWebView() {
        if (this.mChangeListener != null) {
            this.mChangeListener.onCloseRequested();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0017, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0023;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    public void responseBack(String str, boolean z, String str2, String str3) {
        SSAObj sSAObj = new SSAObj(str);
        String string = sSAObj.getString(JSON_KEY_SUCCESS);
        String string2 = sSAObj.getString(JSON_KEY_FAIL);
        if (!z) {
            if (!TextUtils.isEmpty(string2)) {
                string = string2;
                if (TextUtils.isEmpty(string)) {
                    if (!TextUtils.isEmpty(str2)) {
                        try {
                            str = new JSONObject(str).put("errMsg", str2).toString();
                        } catch (JSONException unused) {
                        }
                    }
                    if (!TextUtils.isEmpty(str3)) {
                        try {
                            str = new JSONObject(str).put(ParametersKeys.ERR_CODE, str3).toString();
                        } catch (JSONException unused2) {
                        }
                    }
                    injectJavascript(generateJSToInject(string, str));
                    return;
                }
                return;
            }
        }
        string = null;
        if (TextUtils.isEmpty(string)) {
        }
    }

    /* access modifiers changed from: private */
    public String extractSuccessFunctionToCall(String str) {
        return new SSAObj(str).getString(JSON_KEY_SUCCESS);
    }

    /* access modifiers changed from: private */
    public String extractFailFunctionToCall(String str) {
        return new SSAObj(str).getString(JSON_KEY_FAIL);
    }

    /* access modifiers changed from: private */
    public String parseToJson(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, boolean z) {
        JSONObject jSONObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                jSONObject.put(str, SDKUtils.encodeString(str2));
            }
            if (!TextUtils.isEmpty(str3) && !TextUtils.isEmpty(str4)) {
                jSONObject.put(str3, SDKUtils.encodeString(str4));
            }
            if (!TextUtils.isEmpty(str5) && !TextUtils.isEmpty(str6)) {
                jSONObject.put(str5, SDKUtils.encodeString(str6));
            }
            if (!TextUtils.isEmpty(str7) && !TextUtils.isEmpty(str8)) {
                jSONObject.put(str7, SDKUtils.encodeString(str8));
            }
            if (!TextUtils.isEmpty(str9)) {
                jSONObject.put(str9, z);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            StringBuilder sb = new StringBuilder();
            sb.append(Constants.NATIVE_EXCEPTION_BASE_URL);
            sb.append(e.getStackTrace()[0].getMethodName());
            ironSourceAsyncHttpRequestTask.execute(new String[]{sb.toString()});
        }
        return jSONObject.toString();
    }

    private String mapToJson(Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        if (map != null && !map.isEmpty()) {
            for (String str : map.keySet()) {
                try {
                    jSONObject.put(str, SDKUtils.encodeString((String) map.get(str)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return jSONObject.toString();
    }

    /* access modifiers changed from: private */
    public Object[] getDeviceParams(Context context) {
        boolean z;
        DeviceProperties instance = DeviceProperties.getInstance(context);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(RequestParameters.APP_ORIENTATION, SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(getCurrentActivityContext())));
            String deviceOem = instance.getDeviceOem();
            if (deviceOem != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_OEM), SDKUtils.encodeString(deviceOem));
            }
            String deviceModel = instance.getDeviceModel();
            if (deviceModel != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_MODEL), SDKUtils.encodeString(deviceModel));
                z = false;
            } else {
                z = true;
            }
            try {
                SDKUtils.loadGoogleAdvertiserInfo(context);
                String advertiserId = SDKUtils.getAdvertiserId();
                Boolean valueOf = Boolean.valueOf(SDKUtils.isLimitAdTrackingEnabled());
                if (!TextUtils.isEmpty(advertiserId)) {
                    Logger.i(this.TAG, "add AID and LAT");
                    jSONObject.put(RequestParameters.isLAT, valueOf);
                    StringBuilder sb = new StringBuilder();
                    sb.append(RequestParameters.DEVICE_IDS);
                    sb.append(RequestParameters.LEFT_BRACKETS);
                    sb.append(RequestParameters.AID);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    jSONObject.put(sb.toString(), SDKUtils.encodeString(advertiserId));
                }
                String deviceOsType = instance.getDeviceOsType();
                if (deviceOsType != null) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_OS), SDKUtils.encodeString(deviceOsType));
                } else {
                    z = true;
                }
                String deviceOsVersion = instance.getDeviceOsVersion();
                if (deviceOsVersion != null) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_OS_VERSION), deviceOsVersion.replaceAll("[^0-9/.]", ""));
                } else {
                    z = true;
                }
                String deviceOsVersion2 = instance.getDeviceOsVersion();
                if (deviceOsVersion2 != null) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_OS_VERSION_FULL), SDKUtils.encodeString(deviceOsVersion2));
                }
                String valueOf2 = String.valueOf(instance.getDeviceApiLevel());
                if (valueOf2 != null) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_API_LEVEL), valueOf2);
                } else {
                    z = true;
                }
                String supersonicSdkVersion = DeviceProperties.getSupersonicSdkVersion();
                if (supersonicSdkVersion != null) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.SDK_VERSION), SDKUtils.encodeString(supersonicSdkVersion));
                }
                if (instance.getDeviceCarrier() != null && instance.getDeviceCarrier().length() > 0) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.MOBILE_CARRIER), SDKUtils.encodeString(instance.getDeviceCarrier()));
                }
                String connectionType = ConnectivityUtils.getConnectionType(context);
                if (!connectionType.equals("none")) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.CONNECTION_TYPE), SDKUtils.encodeString(connectionType));
                } else {
                    z = true;
                }
                if (VERSION.SDK_INT >= 23) {
                    int cellularNetworkType = ConnectivityService.getCellularNetworkType(context);
                    if (cellularNetworkType != 0) {
                        jSONObject.put(SDKUtils.encodeString(RequestParameters.CELLULAR_NETWORK_TYPE), cellularNetworkType);
                    }
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.HAS_VPN), ConnectivityUtils.hasVPN(context));
                }
                String language = context.getResources().getConfiguration().locale.getLanguage();
                if (!TextUtils.isEmpty(language)) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_LANGUAGE), SDKUtils.encodeString(language.toUpperCase()));
                }
                if (SDKUtils.isExternalStorageAvailable()) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.DISK_FREE_SIZE), SDKUtils.encodeString(String.valueOf(DeviceStatus.getAvailableMemorySizeInMegaBytes(this.mCacheDirectory))));
                } else {
                    z = true;
                }
                String valueOf3 = String.valueOf(DeviceStatus.getDeviceWidth());
                if (!TextUtils.isEmpty(valueOf3)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(SDKUtils.encodeString(RequestParameters.DEVICE_SCREEN_SIZE));
                    sb2.append(RequestParameters.LEFT_BRACKETS);
                    sb2.append(SDKUtils.encodeString("width"));
                    sb2.append(RequestParameters.RIGHT_BRACKETS);
                    jSONObject.put(sb2.toString(), SDKUtils.encodeString(valueOf3));
                } else {
                    z = true;
                }
                String valueOf4 = String.valueOf(DeviceStatus.getDeviceHeight());
                StringBuilder sb3 = new StringBuilder();
                sb3.append(SDKUtils.encodeString(RequestParameters.DEVICE_SCREEN_SIZE));
                sb3.append(RequestParameters.LEFT_BRACKETS);
                sb3.append(SDKUtils.encodeString("height"));
                sb3.append(RequestParameters.RIGHT_BRACKETS);
                jSONObject.put(sb3.toString(), SDKUtils.encodeString(valueOf4));
                String packageName = ApplicationContext.getPackageName(getContext());
                if (!TextUtils.isEmpty(packageName)) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.PACKAGE_NAME), SDKUtils.encodeString(packageName));
                }
                String valueOf5 = String.valueOf(DeviceStatus.getDeviceDensity());
                if (!TextUtils.isEmpty(valueOf5)) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_SCREEN_SCALE), SDKUtils.encodeString(valueOf5));
                }
                String valueOf6 = String.valueOf(DeviceStatus.isRootedDevice());
                if (!TextUtils.isEmpty(valueOf6)) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.IS_ROOT_DEVICE), SDKUtils.encodeString(valueOf6));
                }
                jSONObject.put(SDKUtils.encodeString(RequestParameters.DEVICE_VOLUME), (double) DeviceProperties.getInstance(context).getDeviceVolume(context));
                Context currentActivityContext = getCurrentActivityContext();
                if (VERSION.SDK_INT >= 19 && (currentActivityContext instanceof Activity)) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.IMMERSIVE), DeviceStatus.isImmersiveSupported((Activity) currentActivityContext));
                }
                jSONObject.put(SDKUtils.encodeString(RequestParameters.BATTERY_LEVEL), DeviceStatus.getBatteryLevel(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(RequestParameters.NETWORK_MCC), ConnectivityService.getNetworkMCC(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(RequestParameters.NETWORK_MNC), ConnectivityService.getNetworkMNC(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(RequestParameters.PHONE_TYPE), ConnectivityService.getPhoneType(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(RequestParameters.SIM_OPERATOR), SDKUtils.encodeString(ConnectivityService.getSimOperator(currentActivityContext)));
                jSONObject.put(SDKUtils.encodeString("lastUpdateTime"), ApplicationContext.getLastUpdateTime(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(RequestParameters.FIRST_INSTALL_TIME), ApplicationContext.getFirstInstallTime(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(RequestParameters.APPLICATION_VERSION_NAME), SDKUtils.encodeString(ApplicationContext.getApplicationVersionName(currentActivityContext)));
                String installerPackageName = ApplicationContext.getInstallerPackageName(currentActivityContext);
                if (!TextUtils.isEmpty(installerPackageName)) {
                    jSONObject.put(SDKUtils.encodeString(RequestParameters.INSTALLER_PACKAGE_NAME), SDKUtils.encodeString(installerPackageName));
                }
            } catch (JSONException e) {
                e = e;
                e.printStackTrace();
                IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
                StringBuilder sb4 = new StringBuilder();
                sb4.append(Constants.NATIVE_EXCEPTION_BASE_URL);
                sb4.append(e.getStackTrace()[0].getMethodName());
                ironSourceAsyncHttpRequestTask.execute(new String[]{sb4.toString()});
                return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
            }
        } catch (JSONException e2) {
            e = e2;
            z = false;
            e.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask2 = new IronSourceAsyncHttpRequestTask();
            StringBuilder sb42 = new StringBuilder();
            sb42.append(Constants.NATIVE_EXCEPTION_BASE_URL);
            sb42.append(e.getStackTrace()[0].getMethodName());
            ironSourceAsyncHttpRequestTask2.execute(new String[]{sb42.toString()});
            return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
        }
        return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
    }

    /* access modifiers changed from: private */
    public Object[] getApplicationParams(String str, String str2) {
        boolean z;
        JSONObject jSONObject = new JSONObject();
        Map map = null;
        if (!TextUtils.isEmpty(str)) {
            ProductType stringProductTypeAsEnum = getStringProductTypeAsEnum(str);
            if (stringProductTypeAsEnum == ProductType.OfferWall) {
                map = this.mOWExtraParameters;
            } else {
                DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(stringProductTypeAsEnum, str2);
                if (demandSourceById != null) {
                    Map extraParams = demandSourceById.getExtraParams();
                    extraParams.put("demandSourceName", demandSourceById.getDemandSourceName());
                    extraParams.put("demandSourceId", demandSourceById.getId());
                    map = extraParams;
                }
            }
            try {
                jSONObject.put(ParametersKeys.PRODUCT_TYPE, str);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                Map initSDKParams = SDKUtils.getInitSDKParams();
                jSONObject = initSDKParams != null ? SDKUtils.mergeJSONObjects(jSONObject, new JSONObject(initSDKParams)) : jSONObject;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            z = false;
        } else {
            z = true;
        }
        if (!TextUtils.isEmpty(this.mUserId)) {
            try {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.APPLICATION_USER_ID), SDKUtils.encodeString(this.mUserId));
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        } else {
            z = true;
        }
        if (!TextUtils.isEmpty(this.mApplicationKey)) {
            try {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.APPLICATION_KEY), SDKUtils.encodeString(this.mApplicationKey));
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
        } else {
            z = true;
        }
        if (map != null && !map.isEmpty()) {
            for (Entry entry : map.entrySet()) {
                if (((String) entry.getKey()).equalsIgnoreCase("sdkWebViewCache")) {
                    setWebviewCache((String) entry.getValue());
                }
                try {
                    jSONObject.put(SDKUtils.encodeString((String) entry.getKey()), SDKUtils.encodeString((String) entry.getValue()));
                } catch (JSONException e5) {
                    e5.printStackTrace();
                }
            }
        }
        return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
    }

    /* access modifiers changed from: private */
    public Object[] getAppsStatus(String str, String str2) {
        boolean z;
        boolean z2;
        JSONObject jSONObject = new JSONObject();
        try {
            if (TextUtils.isEmpty(str) || str.equalsIgnoreCase("null")) {
                jSONObject.put("error", "appIds is null or empty");
                z = true;
                return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
            } else if (TextUtils.isEmpty(str2) || str2.equalsIgnoreCase("null")) {
                jSONObject.put("error", "requestId is null or empty");
                z = true;
                return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
            } else {
                List installedApplications = DeviceStatus.getInstalledApplications(getContext());
                JSONArray jSONArray = new JSONArray(str);
                JSONObject jSONObject2 = new JSONObject();
                for (int i = 0; i < jSONArray.length(); i++) {
                    String trim = jSONArray.getString(i).trim();
                    if (!TextUtils.isEmpty(trim)) {
                        JSONObject jSONObject3 = new JSONObject();
                        Iterator it = installedApplications.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (trim.equalsIgnoreCase(((ApplicationInfo) it.next()).packageName)) {
                                    jSONObject3.put(IS_INSTALLED, true);
                                    jSONObject2.put(trim, jSONObject3);
                                    z2 = true;
                                    break;
                                }
                            } else {
                                z2 = false;
                                break;
                            }
                        }
                        if (!z2) {
                            jSONObject3.put(IS_INSTALLED, false);
                            jSONObject2.put(trim, jSONObject3);
                        }
                    }
                }
                jSONObject.put(RESULT, jSONObject2);
                jSONObject.put(REQUEST_ID, str2);
                z = false;
                return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
            }
        } catch (Exception unused) {
        }
    }

    public void onFileDownloadSuccess(SSAFile sSAFile) {
        if (sSAFile.getFile().contains(Constants.MOBILE_CONTROLLER_HTML)) {
            load(1);
        } else {
            assetCached(sSAFile.getFile(), sSAFile.getPath());
        }
    }

    public void onFileDownloadFail(SSAFile sSAFile) {
        if (sSAFile.getFile().contains(Constants.MOBILE_CONTROLLER_HTML)) {
            this.mControllerListener.handleControllerStageFailed(ErrorCodes.CONTROLLER_FAILED_TO_DOWNLOAD);
        } else {
            assetCachedFailed(sSAFile.getFile(), sSAFile.getPath(), sSAFile.getErrMsg());
        }
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        String str5 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" ");
        sb.append(str4);
        Logger.i(str5, sb.toString());
    }

    /* access modifiers changed from: private */
    public void toastingErrMsg(final String str, String str2) {
        final String string = new SSAObj(str2).getString("errMsg");
        if (!TextUtils.isEmpty(string)) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (WebController.this.getDebugMode() == DebugMode.MODE_3.getValue()) {
                        Context currentActivityContext = WebController.this.getCurrentActivityContext();
                        StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append(" : ");
                        sb.append(string);
                        Toast.makeText(currentActivityContext, sb.toString(), 1).show();
                    }
                }
            });
        }
    }

    public void setControllerKeyPressed(String str) {
        this.mControllerKeyPressed = str;
    }

    public String getControllerKeyPressed() {
        String str = this.mControllerKeyPressed;
        setControllerKeyPressed("interrupt");
        return str;
    }

    public void sendConnectionTypeChanged(String str) {
        String str2 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("device status changed, connection type ");
        sb.append(str);
        Logger.i(str2, sb.toString());
        injectJavascript(generateJSToInject(JSMethods.DEVICE_STATUS_CHANGED, parseToJson(RequestParameters.CONNECTION_TYPE, str, null, null, null, null, null, null, null, false)));
    }

    public void sendConnectionInfoChanged(JSONObject jSONObject) {
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("device connection info changed: ");
        sb.append(jSONObject.toString());
        Logger.i(str, sb.toString());
        injectJavascript(generateJSToInject(JSMethods.CONNECTION_INFO_CHANGED, parseToJson(RequestParameters.CONNECTION_INFO, jSONObject.toString(), null, null, null, null, null, null, null, false)));
    }

    public void engageEnd(String str) {
        if (str.equals("forceClose")) {
            closeWebView();
        }
        injectJavascript(generateJSToInject(JSMethods.ENGAGE_END, parseToJson("action", str, null, null, null, null, null, null, null, false)));
    }

    public void registerConnectionReceiver(Context context) {
        if (this.mConnectivityAdapter != null) {
            this.mConnectivityAdapter.startListenToNetworkChanges(context);
        }
    }

    public void unregisterConnectionReceiver(Context context) {
        if (this.mConnectivityAdapter != null) {
            this.mConnectivityAdapter.stopListenToNetworkChanges(context);
        }
    }

    public void pause() {
        if (VERSION.SDK_INT > 10) {
            try {
                onPause();
            } catch (Throwable th) {
                String str = this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("WebViewController: pause() - ");
                sb.append(th);
                Logger.i(str, sb.toString());
                new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=webviewPause"});
            }
        }
    }

    public void resume() {
        if (VERSION.SDK_INT > 10) {
            try {
                onResume();
            } catch (Throwable th) {
                String str = this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("WebViewController: onResume() - ");
                sb.append(th);
                Logger.i(str, sb.toString());
                new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=webviewResume"});
            }
        }
    }

    public void setOnWebViewControllerChangeListener(OnWebViewChangeListener onWebViewChangeListener) {
        this.mChangeListener = onWebViewChangeListener;
    }

    public FrameLayout getLayout() {
        return this.mControllerLayout;
    }

    public boolean inCustomView() {
        return this.mCustomView != null;
    }

    public void hideCustomView() {
        this.mWebChromeClient.onHideCustomView();
    }

    private void setWebviewCache(String str) {
        if (str.equalsIgnoreCase("0")) {
            getSettings().setCacheMode(2);
        } else {
            getSettings().setCacheMode(-1);
        }
    }

    public boolean handleSearchKeysURLs(String str) {
        List<String> searchKeys = IronSourceSharedPrefHelper.getSupersonicPrefHelper().getSearchKeys();
        if (searchKeys != null) {
            try {
                if (!searchKeys.isEmpty()) {
                    for (String contains : searchKeys) {
                        if (str.contains(contains)) {
                            UrlHandler.openUrl(getCurrentActivityContext(), str);
                            return true;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public void setState(State state) {
        this.mState = state;
    }

    public State getState() {
        return this.mState;
    }

    private void sendProductErrorMessage(ProductType productType, DemandSource demandSource) {
        triggerOnControllerInitProductFail(SDKUtils.createErrorMessage(getErrorCodeByProductType(productType), ErrorCodes.InitiatingController), productType, demandSource);
    }

    private String getErrorCodeByProductType(ProductType productType) {
        String str = "";
        switch (productType) {
            case RewardedVideo:
                return ErrorCodes.InitRV;
            case Interstitial:
                return ErrorCodes.InitIS;
            case OfferWall:
                return ErrorCodes.InitOW;
            case OfferWallCredits:
                return ErrorCodes.ShowOWCredits;
            case Banner:
                return ErrorCodes.InitBN;
            default:
                return str;
        }
    }

    public void destroy() {
        super.destroy();
        if (this.downloadManager != null) {
            this.downloadManager.release();
        }
        if (this.mConnectivityAdapter != null) {
            this.mConnectivityAdapter.release();
        }
        this.mUiHandler = null;
        this.mCurrentActivityContext = null;
    }

    private String generateJSToInject(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("SSA_CORE.SDKController.runFunction('");
        sb.append(str);
        sb.append("');");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public String generateJSToInject(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("SSA_CORE.SDKController.runFunction('");
        sb.append(str);
        sb.append("?parameters=");
        sb.append(str2);
        sb.append("');");
        return sb.toString();
    }

    private String generateJSToInject(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        sb.append("SSA_CORE.SDKController.runFunction('");
        sb.append(str);
        sb.append("','");
        sb.append(str2);
        sb.append("','");
        sb.append(str3);
        sb.append("');");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public String generateJSToInject(String str, String str2, String str3, String str4) {
        StringBuilder sb = new StringBuilder();
        sb.append("SSA_CORE.SDKController.runFunction('");
        sb.append(str);
        sb.append("?parameters=");
        sb.append(str2);
        sb.append("','");
        sb.append(str3);
        sb.append("','");
        sb.append(str4);
        sb.append("');");
        return sb.toString();
    }

    public AdUnitsState getSavedState() {
        return this.mSavedState;
    }

    public void restoreSavedState() {
        restoreState(this.mSavedState);
    }

    public void restoreState(AdUnitsState adUnitsState) {
        synchronized (this.mSavedStateLocker) {
            if (adUnitsState.shouldRestore() && this.mIsWebControllerReady) {
                String str = this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("restoreState(state:");
                sb.append(adUnitsState);
                sb.append(")");
                Log.d(str, sb.toString());
                int displayedProduct = adUnitsState.getDisplayedProduct();
                if (displayedProduct != -1) {
                    if (displayedProduct == ProductType.RewardedVideo.ordinal()) {
                        Log.d(this.TAG, "onRVAdClosed()");
                        ProductType productType = ProductType.RewardedVideo;
                        String displayedDemandSourceId = adUnitsState.getDisplayedDemandSourceId();
                        DSAdProductListener adProductListenerByProductType = getAdProductListenerByProductType(productType);
                        if (adProductListenerByProductType != null && !TextUtils.isEmpty(displayedDemandSourceId)) {
                            adProductListenerByProductType.onAdProductClose(productType, displayedDemandSourceId);
                        }
                    } else if (displayedProduct == ProductType.Interstitial.ordinal()) {
                        Log.d(this.TAG, "onInterstitialAdClosed()");
                        ProductType productType2 = ProductType.Interstitial;
                        String displayedDemandSourceId2 = adUnitsState.getDisplayedDemandSourceId();
                        DSAdProductListener adProductListenerByProductType2 = getAdProductListenerByProductType(productType2);
                        if (adProductListenerByProductType2 != null && !TextUtils.isEmpty(displayedDemandSourceId2)) {
                            adProductListenerByProductType2.onAdProductClose(productType2, displayedDemandSourceId2);
                        }
                    } else if (displayedProduct == ProductType.OfferWall.ordinal()) {
                        Log.d(this.TAG, "onOWAdClosed()");
                        if (this.mOnOfferWallListener != null) {
                            this.mOnOfferWallListener.onOWAdClosed();
                        }
                    }
                    adUnitsState.adOpened(-1);
                    adUnitsState.setDisplayedDemandSourceId(null);
                } else {
                    Log.d(this.TAG, "No ad was opened");
                }
                String interstitialAppKey = adUnitsState.getInterstitialAppKey();
                String interstitialUserId = adUnitsState.getInterstitialUserId();
                for (DemandSource demandSource : this.mDemandSourceManager.getDemandSources(ProductType.Interstitial)) {
                    if (demandSource.getDemandSourceInitState() == 2) {
                        String str2 = this.TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("initInterstitial(appKey:");
                        sb2.append(interstitialAppKey);
                        sb2.append(", userId:");
                        sb2.append(interstitialUserId);
                        sb2.append(", demandSource:");
                        sb2.append(demandSource.getDemandSourceName());
                        sb2.append(")");
                        Log.d(str2, sb2.toString());
                        initInterstitial(interstitialAppKey, interstitialUserId, demandSource, this.mDSInterstitialListener);
                    }
                }
                String rVAppKey = adUnitsState.getRVAppKey();
                String rVUserId = adUnitsState.getRVUserId();
                for (DemandSource demandSource2 : this.mDemandSourceManager.getDemandSources(ProductType.RewardedVideo)) {
                    if (demandSource2.getDemandSourceInitState() == 2) {
                        String demandSourceName = demandSource2.getDemandSourceName();
                        Log.d(this.TAG, "onRVNoMoreOffers()");
                        this.mDSRewardedVideoListener.onRVNoMoreOffers(demandSourceName);
                        String str3 = this.TAG;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("initRewardedVideo(appKey:");
                        sb3.append(rVAppKey);
                        sb3.append(", userId:");
                        sb3.append(rVUserId);
                        sb3.append(", demandSource:");
                        sb3.append(demandSourceName);
                        sb3.append(")");
                        Log.d(str3, sb3.toString());
                        initRewardedVideo(rVAppKey, rVUserId, demandSource2, this.mDSRewardedVideoListener);
                    }
                }
                adUnitsState.setShouldRestore(false);
            }
            this.mSavedState = adUnitsState;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (!this.mChangeListener.onBackButtonPressed()) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void runOnUiThread(Runnable runnable) {
        this.mUiHandler.post(runnable);
    }

    public void setCommunicationWithAdView(ISNAdView iSNAdView) {
        if (this.mBannerJsAdapter != null) {
            this.mBannerJsAdapter.setCommunicationWithAdView(iSNAdView);
        }
    }
}
