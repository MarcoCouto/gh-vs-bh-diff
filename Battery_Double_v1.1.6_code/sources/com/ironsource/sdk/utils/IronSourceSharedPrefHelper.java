package com.ironsource.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.data.SSABCParameters;
import com.ironsource.sdk.data.SSAEnums.BackButtonState;
import com.ironsource.sdk.data.SSAEnums.ProductType;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.data.SSASession;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceSharedPrefHelper {
    private static final String APPLICATION_KEY = "application_key";
    private static final String APPLICATION_KEY_IS = "application_key_is";
    private static final String APPLICATION_KEY_OW = "application_key_ow";
    private static final String APPLICATION_KEY_RV = "application_key_rv";
    private static final String BACK_BUTTON_STATE = "back_button_state";
    private static final String IS_REPORTED = "is_reported";
    private static final String REGISTER_SESSIONS = "register_sessions";
    private static final String SEARCH_KEYS = "search_keys";
    private static final String SESSIONS = "sessions";
    private static final String SSA_RV_PARAMETER_CONNECTION_RETRIES = "ssa_rv_parameter_connection_retries";
    private static final String SSA_SDK_DOWNLOAD_URL = "ssa_sdk_download_url";
    private static final String SSA_SDK_LOAD_URL = "ssa_sdk_load_url";
    private static final String SUPERSONIC_SHARED_PREF = "supersonic_shared_preferen";
    private static final String UNIQUE_ID = "unique_id";
    private static final String UNIQUE_ID_IS = "unique_id_is";
    private static final String UNIQUE_ID_OW = "unique_id_ow";
    private static final String UNIQUE_ID_RV = "unique_id_rv";
    private static final String VERSION = "version";
    private static IronSourceSharedPrefHelper mInstance;
    private SharedPreferences mSharedPreferences;

    private IronSourceSharedPrefHelper(Context context) {
        this.mSharedPreferences = context.getSharedPreferences(SUPERSONIC_SHARED_PREF, 0);
    }

    public static synchronized IronSourceSharedPrefHelper getSupersonicPrefHelper(Context context) {
        IronSourceSharedPrefHelper ironSourceSharedPrefHelper;
        synchronized (IronSourceSharedPrefHelper.class) {
            if (mInstance == null) {
                mInstance = new IronSourceSharedPrefHelper(context);
            }
            ironSourceSharedPrefHelper = mInstance;
        }
        return ironSourceSharedPrefHelper;
    }

    public static synchronized IronSourceSharedPrefHelper getSupersonicPrefHelper() {
        IronSourceSharedPrefHelper ironSourceSharedPrefHelper;
        synchronized (IronSourceSharedPrefHelper.class) {
            ironSourceSharedPrefHelper = mInstance;
        }
        return ironSourceSharedPrefHelper;
    }

    public String getConnectionRetries() {
        return this.mSharedPreferences.getString(SSA_RV_PARAMETER_CONNECTION_RETRIES, "3");
    }

    public void setSSABCParameters(SSABCParameters sSABCParameters) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString(SSA_RV_PARAMETER_CONNECTION_RETRIES, sSABCParameters.getConnectionRetries());
        edit.commit();
    }

    public void setBackButtonState(String str) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString(BACK_BUTTON_STATE, str);
        edit.commit();
    }

    public BackButtonState getBackButtonState() {
        int parseInt = Integer.parseInt(this.mSharedPreferences.getString(BACK_BUTTON_STATE, "2"));
        if (parseInt == 0) {
            return BackButtonState.None;
        }
        if (parseInt == 1) {
            return BackButtonState.Device;
        }
        if (parseInt == 2) {
            return BackButtonState.Controller;
        }
        return BackButtonState.Controller;
    }

    public void setSearchKeys(String str) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString(SEARCH_KEYS, str);
        edit.commit();
    }

    public List<String> getSearchKeys() {
        String string = this.mSharedPreferences.getString(SEARCH_KEYS, null);
        ArrayList arrayList = new ArrayList();
        if (string != null) {
            SSAObj sSAObj = new SSAObj(string);
            if (sSAObj.containsKey(ParametersKeys.SEARCH_KEYS)) {
                try {
                    arrayList.addAll(sSAObj.toList((JSONArray) sSAObj.get(ParametersKeys.SEARCH_KEYS)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    public JSONArray getSessions() {
        JSONArray jSONArray;
        String string = this.mSharedPreferences.getString(SESSIONS, null);
        if (string == null) {
            return new JSONArray();
        }
        try {
            jSONArray = new JSONArray(string);
        } catch (JSONException unused) {
            jSONArray = new JSONArray();
        }
        return jSONArray;
    }

    public void deleteSessions() {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString(SESSIONS, null);
        edit.commit();
    }

    public void addSession(SSASession sSASession) {
        if (getShouldRegisterSessions()) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("sessionStartTime", sSASession.getSessionStartTime());
                jSONObject.put("sessionEndTime", sSASession.getSessionEndTime());
                jSONObject.put("sessionType", sSASession.getSessionType());
                jSONObject.put("connectivity", sSASession.getConnectivity());
            } catch (JSONException unused) {
            }
            JSONArray sessions = getSessions();
            if (sessions == null) {
                sessions = new JSONArray();
            }
            sessions.put(jSONObject);
            Editor edit = this.mSharedPreferences.edit();
            edit.putString(SESSIONS, sessions.toString());
            edit.commit();
        }
    }

    private boolean getShouldRegisterSessions() {
        return this.mSharedPreferences.getBoolean(REGISTER_SESSIONS, true);
    }

    public void setShouldRegisterSessions(boolean z) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putBoolean(REGISTER_SESSIONS, z);
        edit.commit();
    }

    public boolean setUserData(String str, String str2) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString(str, str2);
        return edit.commit();
    }

    public String getUserData(String str) {
        String string = this.mSharedPreferences.getString(str, null);
        return string != null ? string : "{}";
    }

    public String getApplicationKey(ProductType productType) {
        String str = null;
        switch (productType) {
            case RewardedVideo:
                str = this.mSharedPreferences.getString(APPLICATION_KEY_RV, null);
                break;
            case OfferWall:
                str = this.mSharedPreferences.getString(APPLICATION_KEY_OW, null);
                break;
            case Interstitial:
                str = this.mSharedPreferences.getString(APPLICATION_KEY_IS, null);
                break;
        }
        return str == null ? this.mSharedPreferences.getString(APPLICATION_KEY, "EMPTY_APPLICATION_KEY") : str;
    }

    public void setApplicationKey(String str) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString(APPLICATION_KEY, str);
        edit.commit();
    }

    public boolean setUniqueId(String str) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString(UNIQUE_ID, str);
        return edit.commit();
    }

    public String getCurrentSDKVersion() {
        return this.mSharedPreferences.getString("version", "UN_VERSIONED");
    }

    public void setCurrentSDKVersion(String str) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString("version", str);
        edit.commit();
    }

    public String getSDKDownloadUrl() {
        return this.mSharedPreferences.getString(SSA_SDK_DOWNLOAD_URL, null);
    }

    public String getCampaignLastUpdate(String str) {
        return this.mSharedPreferences.getString(str, null);
    }

    public void setCampaignLastUpdate(String str, String str2) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public String getUniqueId(String str) {
        String str2 = null;
        if (str.equalsIgnoreCase(ProductType.RewardedVideo.toString())) {
            str2 = this.mSharedPreferences.getString(UNIQUE_ID_RV, null);
        } else if (str.equalsIgnoreCase(ProductType.OfferWall.toString())) {
            str2 = this.mSharedPreferences.getString(UNIQUE_ID_OW, null);
        } else if (str.equalsIgnoreCase(ProductType.Interstitial.toString())) {
            str2 = this.mSharedPreferences.getString(UNIQUE_ID_IS, null);
        }
        return str2 == null ? this.mSharedPreferences.getString(UNIQUE_ID, "EMPTY_UNIQUE_ID") : str2;
    }

    public String getUniqueId(ProductType productType) {
        return getUniqueId(productType.toString());
    }

    public boolean setLatestCompeltionsTime(String str, String str2, String str3) {
        String string = this.mSharedPreferences.getString("ssaUserData", null);
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                if (!jSONObject.isNull(str2)) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(str2);
                    if (!jSONObject2.isNull(str3)) {
                        jSONObject2.getJSONObject(str3).put("timestamp", str);
                        Editor edit = this.mSharedPreferences.edit();
                        edit.putString("ssaUserData", jSONObject.toString());
                        return edit.commit();
                    }
                }
            } catch (JSONException e) {
                IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
                StringBuilder sb = new StringBuilder();
                sb.append(Constants.NATIVE_EXCEPTION_BASE_URL);
                sb.append(e.getStackTrace()[0].getMethodName());
                ironSourceAsyncHttpRequestTask.execute(new String[]{sb.toString()});
            }
        }
        return false;
    }

    public void setReportAppStarted(boolean z) {
        Editor edit = this.mSharedPreferences.edit();
        edit.putBoolean(IS_REPORTED, z);
        edit.apply();
    }

    public boolean getReportAppStarted() {
        return this.mSharedPreferences.getBoolean(IS_REPORTED, false);
    }
}
