package com.ironsource.sdk.utils;

import android.content.Context;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.data.SSAEnums.ProductType;
import com.ironsource.sdk.data.SSAFile;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceStorageUtils {
    private static final String SSA_DIRECTORY_NAME = "supersonicads";

    public static String initializeCacheDirectory(Context context) {
        createRootDirectory(context);
        return refreshRootDirectory(context);
    }

    private static String refreshRootDirectory(Context context) {
        String currentSDKVersion = IronSourceSharedPrefHelper.getSupersonicPrefHelper(context).getCurrentSDKVersion();
        String supersonicSdkVersion = DeviceProperties.getSupersonicSdkVersion();
        if (currentSDKVersion.equalsIgnoreCase(supersonicSdkVersion)) {
            return getDiskCacheDir(context, SSA_DIRECTORY_NAME).getPath();
        }
        IronSourceSharedPrefHelper.getSupersonicPrefHelper().setCurrentSDKVersion(supersonicSdkVersion);
        File externalCacheDir = DeviceStatus.getExternalCacheDir(context);
        if (externalCacheDir != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(externalCacheDir.getAbsolutePath());
            sb.append(File.separator);
            sb.append(SSA_DIRECTORY_NAME);
            sb.append(File.separator);
            deleteAllFiles(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(DeviceStatus.getInternalCacheDirPath(context));
        sb2.append(File.separator);
        sb2.append(SSA_DIRECTORY_NAME);
        sb2.append(File.separator);
        deleteAllFiles(sb2.toString());
        return createRootDirectory(context);
    }

    private static void deleteAllFiles(String str) {
        File[] listFiles = new File(str).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    deleteAllFiles(file.getAbsolutePath());
                    file.delete();
                } else {
                    file.delete();
                }
            }
        }
    }

    private static File getDiskCacheDir(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(getDiskCacheDirPath(context));
        sb.append(File.separator);
        sb.append(str);
        return new File(sb.toString());
    }

    public static String getDiskCacheDirPath(Context context) {
        if (!SDKUtils.isExternalStorageAvailable()) {
            return DeviceStatus.getInternalCacheDirPath(context);
        }
        File externalCacheDir = DeviceStatus.getExternalCacheDir(context);
        if (externalCacheDir == null || !externalCacheDir.canWrite()) {
            return DeviceStatus.getInternalCacheDirPath(context);
        }
        return externalCacheDir.getPath();
    }

    private static String createRootDirectory(Context context) {
        File diskCacheDir = getDiskCacheDir(context, SSA_DIRECTORY_NAME);
        if (!diskCacheDir.exists()) {
            diskCacheDir.mkdir();
        }
        return diskCacheDir.getPath();
    }

    public static String makeDir(String str, String str2) {
        File file = new File(str, str2);
        if (file.exists() || file.mkdirs()) {
            return file.getPath();
        }
        return null;
    }

    public static synchronized boolean deleteFile(String str, String str2, String str3) {
        synchronized (IronSourceStorageUtils.class) {
            File file = new File(str, str2);
            if (!file.exists()) {
                return false;
            }
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                return false;
            }
            int length = listFiles.length;
            int i = 0;
            while (i < length) {
                File file2 = listFiles[i];
                if (!file2.isFile() || !file2.getName().equalsIgnoreCase(str3)) {
                    i++;
                } else {
                    boolean delete = file2.delete();
                    return delete;
                }
            }
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        return false;
     */
    public static synchronized boolean isFileCached(String str, SSAFile sSAFile) {
        File[] listFiles;
        synchronized (IronSourceStorageUtils.class) {
            File file = new File(str, sSAFile.getPath());
            if (file.listFiles() != null) {
                for (File file2 : file.listFiles()) {
                    if (file2.isFile() && file2.getName().equalsIgnoreCase(SDKUtils.getFileName(sSAFile.getFile()))) {
                        return true;
                    }
                }
            }
        }
    }

    public static boolean isPathExist(String str, String str2) {
        return new File(str, str2).exists();
    }

    public static synchronized boolean deleteFolder(String str, String str2) {
        boolean z;
        synchronized (IronSourceStorageUtils.class) {
            File file = new File(str, str2);
            z = deleteFolderContentRecursive(file) && file.delete();
        }
        return z;
    }

    private static boolean deleteFolderContentRecursive(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return true;
        }
        boolean z = true;
        for (File file2 : listFiles) {
            if (file2.isDirectory()) {
                z &= deleteFolderContentRecursive(file2);
            }
            if (!file2.delete()) {
                z = false;
            }
        }
        return z;
    }

    public static String getCachedFilesMap(String str, String str2) {
        JSONObject buildFilesMap = buildFilesMap(str, str2);
        try {
            buildFilesMap.put("path", str2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return buildFilesMap.toString();
    }

    private static JSONObject buildFilesMap(String str, String str2) {
        File file = new File(str, str2);
        JSONObject jSONObject = new JSONObject();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                try {
                    Object looping = looping(file2);
                    if (looping instanceof JSONArray) {
                        jSONObject.put("files", looping(file2));
                    } else if (looping instanceof JSONObject) {
                        jSONObject.put(file2.getName(), looping(file2));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
                    StringBuilder sb = new StringBuilder();
                    sb.append(Constants.NATIVE_EXCEPTION_BASE_URL);
                    sb.append(e.getStackTrace()[0].getMethodName());
                    ironSourceAsyncHttpRequestTask.execute(new String[]{sb.toString()});
                }
            }
        }
        return jSONObject;
    }

    private static Object looping(File file) {
        File[] listFiles;
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        try {
            if (file.isFile()) {
                jSONArray.put(file.getName());
                return jSONArray;
            }
            for (File file2 : file.listFiles()) {
                if (file2.isDirectory()) {
                    jSONObject.put(file2.getName(), looping(file2));
                } else {
                    jSONArray.put(file2.getName());
                    jSONObject.put("files", jSONArray);
                }
            }
            if (file.isDirectory()) {
                String campaignLastUpdate = IronSourceSharedPrefHelper.getSupersonicPrefHelper().getCampaignLastUpdate(file.getName());
                if (campaignLastUpdate != null) {
                    jSONObject.put("lastUpdateTime", campaignLastUpdate);
                }
            }
            String lowerCase = file.getName().toLowerCase();
            ProductType productType = null;
            if (lowerCase.startsWith(ProductType.RewardedVideo.toString().toLowerCase())) {
                productType = ProductType.RewardedVideo;
            } else if (lowerCase.startsWith(ProductType.OfferWall.toString().toLowerCase())) {
                productType = ProductType.OfferWall;
            } else if (lowerCase.startsWith(ProductType.Interstitial.toString().toLowerCase())) {
                productType = ProductType.Interstitial;
            }
            if (productType != null) {
                jSONObject.put(SDKUtils.encodeString(RequestParameters.APPLICATION_USER_ID), SDKUtils.encodeString(IronSourceSharedPrefHelper.getSupersonicPrefHelper().getUniqueId(productType)));
                jSONObject.put(SDKUtils.encodeString(RequestParameters.APPLICATION_KEY), SDKUtils.encodeString(IronSourceSharedPrefHelper.getSupersonicPrefHelper().getApplicationKey(productType)));
            }
            return jSONObject;
        } catch (JSONException e) {
            e.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            StringBuilder sb = new StringBuilder();
            sb.append(Constants.NATIVE_EXCEPTION_BASE_URL);
            sb.append(e.getStackTrace()[0].getMethodName());
            ironSourceAsyncHttpRequestTask.execute(new String[]{sb.toString()});
        }
    }

    public static boolean renameFile(String str, String str2) throws Exception {
        return new File(str).renameTo(new File(str2));
    }

    public static int saveFile(byte[] bArr, String str) throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            byte[] bArr2 = new byte[102400];
            int i = 0;
            while (true) {
                int read = byteArrayInputStream.read(bArr2);
                if (read == -1) {
                    return i;
                }
                fileOutputStream.write(bArr2, 0, read);
                i += read;
            }
        } finally {
            fileOutputStream.close();
            byteArrayInputStream.close();
        }
    }
}
