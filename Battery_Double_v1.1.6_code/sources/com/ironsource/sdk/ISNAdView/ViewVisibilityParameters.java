package com.ironsource.sdk.ISNAdView;

import java.util.HashMap;
import org.json.JSONObject;

class ViewVisibilityParameters {
    /* access modifiers changed from: private */
    public int mVisibility = 4;
    private HashMap<String, Boolean> mVisibilityParametersMap = new HashMap<String, Boolean>() {
        {
            boolean z = true;
            put(ISNAdViewConstants.IS_VISIBLE_KEY, Boolean.valueOf(ViewVisibilityParameters.this.mVisibility == 0));
            String str = ISNAdViewConstants.IS_WINDOW_VISIBLE_KEY;
            if (ViewVisibilityParameters.this.mWindowVisibility != 0) {
                z = false;
            }
            put(str, Boolean.valueOf(z));
            put(ISNAdViewConstants.IS_SHOWN_KEY, Boolean.valueOf(false));
            put(ISNAdViewConstants.IS_VIEW_VISIBLE, Boolean.valueOf(false));
        }
    };
    /* access modifiers changed from: private */
    public int mWindowVisibility = 4;

    ViewVisibilityParameters() {
    }

    /* access modifiers changed from: 0000 */
    public void updateViewVisibilityParameters(String str, int i, boolean z) {
        boolean z2 = false;
        if (this.mVisibilityParametersMap.containsKey(str)) {
            this.mVisibilityParametersMap.put(str, Boolean.valueOf(i == 0));
        }
        this.mVisibilityParametersMap.put(ISNAdViewConstants.IS_SHOWN_KEY, Boolean.valueOf(z));
        if ((((Boolean) this.mVisibilityParametersMap.get(ISNAdViewConstants.IS_WINDOW_VISIBLE_KEY)).booleanValue() || ((Boolean) this.mVisibilityParametersMap.get(ISNAdViewConstants.IS_VISIBLE_KEY)).booleanValue()) && ((Boolean) this.mVisibilityParametersMap.get(ISNAdViewConstants.IS_SHOWN_KEY)).booleanValue()) {
            z2 = true;
        }
        this.mVisibilityParametersMap.put(ISNAdViewConstants.IS_VIEW_VISIBLE, Boolean.valueOf(z2));
    }

    /* access modifiers changed from: 0000 */
    public JSONObject collectVisibilityParameters() {
        return new JSONObject(this.mVisibilityParametersMap);
    }
}
