package com.ironsource.sdk.ISNAdView;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ISNAdViewWebClient extends WebViewClient {
    private IErrorReportDelegate mDelegate;

    private String getMessage(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("ISNAdViewWebClient | External Adunit failed to load.");
        sb.append(str);
        sb.append(" ");
        sb.append("Status code: ");
        sb.append(str2);
        return sb.toString();
    }

    public ISNAdViewWebClient(IErrorReportDelegate iErrorReportDelegate) {
        this.mDelegate = iErrorReportDelegate;
    }

    public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
        String str = "";
        try {
            if (VERSION.SDK_INT >= 21) {
                str = String.valueOf(webResourceResponse.getStatusCode());
            }
            this.mDelegate.reportOnError(getMessage("onReceivedHttpError", str));
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onReceivedHttpError(webView, webResourceRequest, webResourceResponse);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        try {
            this.mDelegate.reportOnError(getMessage("onReceivedError", String.valueOf(i)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onReceivedError(webView, i, str, str2);
    }

    @TargetApi(23)
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        try {
            this.mDelegate.reportOnError(getMessage("onReceivedErrorM", String.valueOf(webResourceError.getErrorCode())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onReceivedError(webView, webResourceRequest, webResourceError);
    }
}
