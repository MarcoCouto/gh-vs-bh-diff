package com.ironsource.sdk;

import org.json.JSONObject;

public class IronSourceNetworkAPIUtils {
    static String manual_rewarded_instance_prefix = "ManRewInst_";

    public static String generateInstanceId(JSONObject jSONObject) {
        if (!jSONObject.optBoolean("rewarded")) {
            return jSONObject.optString("name");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(manual_rewarded_instance_prefix);
        sb.append(jSONObject.optString("name"));
        return sb.toString();
    }
}
