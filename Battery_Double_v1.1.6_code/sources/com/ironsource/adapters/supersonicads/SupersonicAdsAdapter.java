package com.ironsource.adapters.supersonicads;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE;
import com.ironsource.mediationsdk.AdapterUtils;
import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.metadata.MetaDataConstants;
import com.ironsource.mediationsdk.metadata.MetaDataUtils;
import com.ironsource.mediationsdk.sdk.BannerSmashListener;
import com.ironsource.mediationsdk.sdk.InternalOfferwallListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.OfferwallAdapterApi;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.ISAdSize;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.IronSourceNetwork;
import com.ironsource.sdk.SSAFactory;
import com.ironsource.sdk.SSAPublisher;
import com.ironsource.sdk.analytics.omid.OMIDManager;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.FeaturesManager;
import com.ironsource.sdk.constants.Constants.JSMethods;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.SSAEnums.ProductType;
import com.ironsource.sdk.listeners.OnBannerListener;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnRewardedVideoListener;
import com.ironsource.sdk.utils.SDKUtils;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

class SupersonicAdsAdapter extends AbstractAdapter implements OfferwallAdapterApi, OnOfferWallListener, OnInterstitialListener, OnRewardedVideoListener, OnBannerListener {
    private static final String VERSION = "6.16.0";
    /* access modifiers changed from: private */
    public static AtomicBoolean mDidInitSdk = new AtomicBoolean(false);
    private static AtomicBoolean mDidSetInitParams = new AtomicBoolean(false);
    private static Handler mUIThreadHandler;
    private final String AD_VISIBLE_EVENT_NAME = "impressions";
    private final String APPLICATION_PRIVATE_KEY = "privateKey";
    private final String APPLICATION_USER_AGE_GROUP = "applicationUserAgeGroup";
    private final String APPLICATION_USER_GENDER = "applicationUserGender";
    private final String CAMPAIGN_ID = RequestParameters.CAMPAIGN_ID;
    private final String CLIENT_SIDE_CALLBACKS = ParametersKeys.USE_CLIENT_SIDE_CALLBACKS;
    private final String CUSTOM_PARAM_PREFIX = "custom_";
    private final String CUSTOM_SEGMENT = "custom_Segment";
    private final String DYNAMIC_CONTROLLER_CONFIG = RequestParameters.CONTROLLER_CONFIG;
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = FeaturesManager.DEBUG_MODE;
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private final String ITEM_COUNT = "itemCount";
    private final String ITEM_NAME = "itemName";
    private final String ITEM_SIGNATURE = "itemSignature";
    private final String LANGUAGE = "language";
    private final String MAX_VIDEO_LENGTH = "maxVideoLength";
    private final String OW_PLACEMENT_ID = Constants.PLACEMENT_ID;
    private final String SDK_PLUGIN_TYPE = "SDKPluginType";
    private final String SUPERSONIC_ADS = IronSourceConstants.SUPERSONIC_CONFIG_NAME;
    private final String TIMESTAMP = "timestamp";
    /* access modifiers changed from: private */
    public boolean mConsent;
    /* access modifiers changed from: private */
    public boolean mDidSetConsent;
    private boolean mIsRVAvailable = false;
    /* access modifiers changed from: private */
    public ISNAdView mIsnAdView;
    private String mMediationSegment;
    /* access modifiers changed from: private */
    public InternalOfferwallListener mOfferwallListener;
    /* access modifiers changed from: private */
    public SSAPublisher mSSAPublisher;
    private String mUserAgeGroup;
    private String mUserGender;

    public String getVersion() {
        return "6.16.0";
    }

    public boolean isOfferwallAvailable() {
        return true;
    }

    public void onInterstitialAdRewarded(String str, int i) {
    }

    public void onOWGeneric(String str, String str2) {
    }

    public static SupersonicAdsAdapter startAdapter(String str) {
        return new SupersonicAdsAdapter(str);
    }

    private SupersonicAdsAdapter(String str) {
        super(str);
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData(IronSourceConstants.SUPERSONIC_CONFIG_NAME, "6.16.0");
        integrationData.activities = new String[]{"com.ironsource.sdk.controller.ControllerActivity", "com.ironsource.sdk.controller.InterstitialActivity", "com.ironsource.sdk.controller.OpenUrlActivity"};
        return integrationData;
    }

    public String getCoreSDKVersion() {
        return SDKUtils.getSDKVersion();
    }

    public static String getAdapterSDKVersion() {
        return SDKUtils.getSDKVersion();
    }

    public void onPause(Activity activity) {
        if (this.mSSAPublisher != null) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" mSSAPublisher.onPause");
            log(ironSourceTag, sb.toString(), 0);
            this.mSSAPublisher.onPause(activity);
        }
    }

    public void onResume(Activity activity) {
        if (this.mSSAPublisher != null) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" mSSAPublisher.onResume");
            log(ironSourceTag, sb.toString(), 0);
            this.mSSAPublisher.onResume(activity);
        }
    }

    public void setAge(int i) {
        if (i >= 13 && i <= 17) {
            this.mUserAgeGroup = "1";
        } else if (i >= 18 && i <= 20) {
            this.mUserAgeGroup = "2";
        } else if (i >= 21 && i <= 24) {
            this.mUserAgeGroup = "3";
        } else if (i >= 25 && i <= 34) {
            this.mUserAgeGroup = "4";
        } else if (i >= 35 && i <= 44) {
            this.mUserAgeGroup = "5";
        } else if (i >= 45 && i <= 54) {
            this.mUserAgeGroup = OMIDManager.OMID_PARTNER_VERSION;
        } else if (i >= 55 && i <= 64) {
            this.mUserAgeGroup = "7";
        } else if (i <= 65 || i > 120) {
            this.mUserAgeGroup = "0";
        } else {
            this.mUserAgeGroup = "8";
        }
    }

    public void setGender(String str) {
        this.mUserGender = str;
    }

    public void setMediationSegment(String str) {
        this.mMediationSegment = str;
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        setParamsBeforeInit(jSONObject);
        final Activity activity2 = activity;
        final JSONObject jSONObject2 = jSONObject;
        final String str3 = str;
        final String str4 = str2;
        AnonymousClass1 r0 = new Runnable() {
            public void run() {
                try {
                    SupersonicAdsAdapter.this.mSSAPublisher = SSAFactory.getPublisherInstance(activity2);
                    HashMap access$100 = SupersonicAdsAdapter.this.getRewardedVideoExtraParams(jSONObject2);
                    if (SupersonicAdsAdapter.this.mDidSetConsent) {
                        SupersonicAdsAdapter.this.applyConsent(SupersonicAdsAdapter.this.mConsent);
                    }
                    SupersonicAdsAdapter supersonicAdsAdapter = SupersonicAdsAdapter.this;
                    IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
                    StringBuilder sb = new StringBuilder();
                    sb.append(SupersonicAdsAdapter.this.getProviderName());
                    sb.append(" mSSAPublisher.initRewardedVideo");
                    supersonicAdsAdapter.log(ironSourceTag, sb.toString(), 0);
                    SupersonicAdsAdapter.this.mSSAPublisher.initRewardedVideo(str3, str4, SupersonicAdsAdapter.this.getProviderName(), access$100, SupersonicAdsAdapter.this);
                    SupersonicAdsAdapter.mDidInitSdk.set(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    SupersonicAdsAdapter.this.onRVInitFail(JSMethods.INIT_REWARDED_VIDEO);
                }
            }
        };
        activity.runOnUiThread(r0);
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": fetchRewardedVideo");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllRewardedVideoSmashes.iterator();
        while (it.hasNext()) {
            RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) it.next();
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(this.mIsRVAvailable);
            }
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        this.mActiveRewardedVideoSmash = rewardedVideoSmashListener;
        if (this.mSSAPublisher != null) {
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(1);
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("demandSourceName", getProviderName());
                jSONObject2.put("sessionDepth", sessionDepth);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" mSSAPublisher.showRewardedVideo");
            log(ironSourceTag, sb.toString(), 0);
            this.mSSAPublisher.showRewardedVideo(jSONObject2);
            return;
        }
        this.mIsRVAvailable = false;
        if (this.mActiveRewardedVideoSmash != null) {
            this.mActiveRewardedVideoSmash.onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
        }
        Iterator it = this.mAllRewardedVideoSmashes.iterator();
        while (it.hasNext()) {
            RewardedVideoSmashListener rewardedVideoSmashListener2 = (RewardedVideoSmashListener) it.next();
            if (rewardedVideoSmashListener2 != null) {
                rewardedVideoSmashListener2.onRewardedVideoAvailabilityChanged(false);
            }
        }
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        return this.mIsRVAvailable;
    }

    public void initInterstitial(final Activity activity, final String str, final String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        setParamsBeforeInit(jSONObject);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    SupersonicAdsAdapter.this.mSSAPublisher = SSAFactory.getPublisherInstance(activity);
                    HashMap access$700 = SupersonicAdsAdapter.this.getInterstitialExtraParams();
                    if (SupersonicAdsAdapter.this.mDidSetConsent) {
                        SupersonicAdsAdapter.this.applyConsent(SupersonicAdsAdapter.this.mConsent);
                    }
                    SupersonicAdsAdapter supersonicAdsAdapter = SupersonicAdsAdapter.this;
                    IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
                    StringBuilder sb = new StringBuilder();
                    sb.append(SupersonicAdsAdapter.this.getProviderName());
                    sb.append(" mSSAPublisher.initInterstitial");
                    supersonicAdsAdapter.log(ironSourceTag, sb.toString(), 0);
                    SupersonicAdsAdapter.this.mSSAPublisher.initInterstitial(str, str2, SupersonicAdsAdapter.this.getProviderName(), access$700, SupersonicAdsAdapter.this);
                    SupersonicAdsAdapter.mDidInitSdk.set(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    SupersonicAdsAdapter.this.onInterstitialInitFailed(e.getMessage());
                }
            }
        });
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        if (this.mSSAPublisher != null) {
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("demandSourceName", getProviderName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" mSSAPublisher.loadInterstitial");
            log(ironSourceTag, sb.toString(), 0);
            this.mSSAPublisher.loadInterstitial(jSONObject2);
            return;
        }
        log(IronSourceTag.NATIVE, "Please call initInterstitial before calling loadInterstitial", 2);
        Iterator it = this.mAllInterstitialSmashes.iterator();
        while (it.hasNext()) {
            InterstitialSmashListener interstitialSmashListener2 = (InterstitialSmashListener) it.next();
            if (interstitialSmashListener2 != null) {
                interstitialSmashListener2.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("Load was called before Init"));
            }
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        this.mActiveInterstitialSmash = interstitialSmashListener;
        if (this.mSSAPublisher != null) {
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(2);
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("demandSourceName", getProviderName());
                jSONObject2.put("sessionDepth", sessionDepth);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" mSSAPublisher.showInterstitial");
            log(ironSourceTag, sb.toString(), 0);
            this.mSSAPublisher.showInterstitial(jSONObject2);
            return;
        }
        log(IronSourceTag.NATIVE, "Please call loadInterstitial before calling showInterstitial", 2);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        return this.mSSAPublisher != null && this.mSSAPublisher.isInterstitialAdAvailable(getProviderName());
    }

    public void setInternalOfferwallListener(InternalOfferwallListener internalOfferwallListener) {
        this.mOfferwallListener = internalOfferwallListener;
    }

    public void initOfferwall(Activity activity, String str, String str2, JSONObject jSONObject) {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": initOfferwall");
        log(ironSourceTag, sb.toString(), 0);
        setParamsBeforeInit(jSONObject);
        final JSONObject jSONObject2 = jSONObject;
        final Activity activity2 = activity;
        final String str3 = str;
        final String str4 = str2;
        AnonymousClass3 r3 = new Runnable() {
            public void run() {
                try {
                    HashMap access$900 = SupersonicAdsAdapter.this.getOfferwallExtraParams(jSONObject2);
                    SupersonicAdsAdapter.this.mSSAPublisher = SSAFactory.getPublisherInstance(activity2);
                    if (SupersonicAdsAdapter.this.mDidSetConsent) {
                        SupersonicAdsAdapter.this.applyConsent(SupersonicAdsAdapter.this.mConsent);
                    }
                    SupersonicAdsAdapter supersonicAdsAdapter = SupersonicAdsAdapter.this;
                    IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
                    StringBuilder sb = new StringBuilder();
                    sb.append(SupersonicAdsAdapter.this.getProviderName());
                    sb.append(" mSSAPublisher.initOfferWall");
                    supersonicAdsAdapter.log(ironSourceTag, sb.toString(), 0);
                    SupersonicAdsAdapter.this.mSSAPublisher.initOfferWall(str3, str4, access$900, SupersonicAdsAdapter.this);
                    SupersonicAdsAdapter.mDidInitSdk.set(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                    IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(SupersonicAdsAdapter.this.getProviderName());
                    sb2.append(":initOfferwall(userId:");
                    sb2.append(str4);
                    sb2.append(")");
                    logger.logException(ironSourceTag2, sb2.toString(), e);
                    InternalOfferwallListener access$1100 = SupersonicAdsAdapter.this.mOfferwallListener;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Adapter initialization failure - ");
                    sb3.append(SupersonicAdsAdapter.this.getProviderName());
                    sb3.append(" - ");
                    sb3.append(e.getMessage());
                    access$1100.onOfferwallAvailable(false, ErrorBuilder.buildInitFailedError(sb3.toString(), IronSourceConstants.OFFERWALL_AD_UNIT));
                }
            }
        };
        activity.runOnUiThread(r3);
    }

    public void getOfferwallCredits() {
        if (this.mSSAPublisher != null) {
            String ironSourceAppKey = IronSourceObject.getInstance().getIronSourceAppKey();
            String ironSourceUserId = IronSourceObject.getInstance().getIronSourceUserId();
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" mSSAPublisher.getOfferWallCredits");
            log(ironSourceTag, sb.toString(), 0);
            this.mSSAPublisher.getOfferWallCredits(ironSourceAppKey, ironSourceUserId, this);
            return;
        }
        log(IronSourceTag.NATIVE, "Please call init before calling getOfferwallCredits", 2);
    }

    public void showOfferwall(String str, JSONObject jSONObject) {
        HashMap offerwallExtraParams = getOfferwallExtraParams(jSONObject);
        if (offerwallExtraParams != null) {
            offerwallExtraParams.put(Constants.PLACEMENT_ID, str);
        }
        if (this.mSSAPublisher != null) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" mSSAPublisher.showOfferWall");
            log(ironSourceTag, sb.toString(), 0);
            this.mSSAPublisher.showOfferWall(offerwallExtraParams);
            return;
        }
        log(IronSourceTag.NATIVE, "Please call init before calling showOfferwall", 2);
    }

    public void initBanners(Activity activity, String str, String str2, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": initBanners");
        log(ironSourceTag, sb.toString(), 0);
        setParamsBeforeInit(jSONObject);
        final JSONObject jSONObject2 = jSONObject;
        final Activity activity2 = activity;
        final String str3 = str;
        final String str4 = str2;
        AnonymousClass4 r2 = new Runnable() {
            public void run() {
                try {
                    HashMap access$1200 = SupersonicAdsAdapter.this.getBannerExtraParams(jSONObject2);
                    SupersonicAdsAdapter.this.mSSAPublisher = SSAFactory.getPublisherInstance(activity2);
                    if (SupersonicAdsAdapter.this.mDidSetConsent) {
                        SupersonicAdsAdapter.this.applyConsent(SupersonicAdsAdapter.this.mConsent);
                    }
                    SupersonicAdsAdapter supersonicAdsAdapter = SupersonicAdsAdapter.this;
                    IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
                    StringBuilder sb = new StringBuilder();
                    sb.append(SupersonicAdsAdapter.this.getProviderName());
                    sb.append(" mSSAPublisher.initBanner");
                    supersonicAdsAdapter.log(ironSourceTag, sb.toString(), 0);
                    SupersonicAdsAdapter.this.mSSAPublisher.initBanner(str3, str4, SupersonicAdsAdapter.this.getProviderName(), access$1200, SupersonicAdsAdapter.this);
                    SupersonicAdsAdapter.mDidInitSdk.set(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    SupersonicAdsAdapter.this.onBannerInitFailed(e.getMessage());
                }
            }
        };
        activity.runOnUiThread(r2);
    }

    public void loadBanner(final IronSourceBannerLayout ironSourceBannerLayout, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        try {
            if (this.mSSAPublisher == null) {
                log(IronSourceTag.NATIVE, "Please call initBanner before calling loadBanner", 2);
                Iterator it = this.mAllBannerSmashes.iterator();
                while (it.hasNext()) {
                    BannerSmashListener bannerSmashListener2 = (BannerSmashListener) it.next();
                    if (bannerSmashListener2 != null) {
                        bannerSmashListener2.onBannerAdLoadFailed(ErrorBuilder.buildLoadFailedError("Load was called before Init"));
                    }
                }
            }
            this.mActiveBannerSmash = bannerSmashListener;
            if (this.mIsnAdView != null) {
                this.mIsnAdView.performCleanup();
                this.mIsnAdView = null;
            }
            final JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("demandSourceName", getProviderName());
            jSONObject2.put(ParametersKeys.PRODUCT_TYPE, ProductType.Banner);
            if (mUIThreadHandler == null) {
                mUIThreadHandler = new Handler(Looper.getMainLooper());
            }
            mUIThreadHandler.post(new Runnable() {
                public void run() {
                    try {
                        SupersonicAdsAdapter.this.mIsnAdView = SupersonicAdsAdapter.this.createBanner(ironSourceBannerLayout.getActivity(), ironSourceBannerLayout.getSize(), SupersonicAdsAdapter.this.mActiveBannerSmash);
                        if (SupersonicAdsAdapter.this.mIsnAdView != null) {
                            SupersonicAdsAdapter.this.log(IronSourceTag.ADAPTER_API, "mIsnAdView.loadAd", 0);
                            SupersonicAdsAdapter.this.mIsnAdView.loadAd(jSONObject2);
                        }
                    } catch (Exception e) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Banner Load Fail, ");
                        sb.append(SupersonicAdsAdapter.this.getProviderName());
                        sb.append(" - ");
                        sb.append(e.getMessage());
                        IronSourceError buildLoadFailedError = ErrorBuilder.buildLoadFailedError(sb.toString());
                        if (SupersonicAdsAdapter.this.mActiveBannerSmash != null) {
                            SupersonicAdsAdapter.this.mActiveBannerSmash.onBannerAdLoadFailed(buildLoadFailedError);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reloadBanner(JSONObject jSONObject) {
        try {
            if (this.mIsnAdView != null) {
                log(IronSourceTag.ADAPTER_API, "mIsnAdView.loadAd", 0);
                this.mIsnAdView.loadAd(jSONObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            IronSourceTag ironSourceTag = IronSourceTag.NATIVE;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" reloadBanner Failed to reload banner ad");
            log(ironSourceTag, sb.toString(), 2);
        }
    }

    public void destroyBanner(JSONObject jSONObject) {
        if (this.mIsnAdView != null) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" mIsnAdView.performCleanup");
            log(ironSourceTag, sb.toString(), 0);
            this.mIsnAdView.performCleanup();
            this.mIsnAdView = null;
        }
    }

    private void setParamsBeforeInit(JSONObject jSONObject) {
        if (mDidSetInitParams.compareAndSet(false, true)) {
            SDKUtils.setControllerUrl(jSONObject.optString("controllerUrl"));
            int optInt = jSONObject.optInt(FeaturesManager.DEBUG_MODE, 0);
            if (isAdaptersDebugEnabled()) {
                optInt = 3;
            }
            SDKUtils.setDebugMode(optInt);
            SDKUtils.setControllerConfig(jSONObject.optString(RequestParameters.CONTROLLER_CONFIG, ""));
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" setParamsBeforeInit setting controller url to  ");
            sb.append(jSONObject.optString("controllerUrl"));
            logger.log(ironSourceTag, sb.toString(), 1);
            IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(getProviderName());
            sb2.append(" setParamsBeforeInit setting controller config to  ");
            sb2.append(jSONObject.optString(RequestParameters.CONTROLLER_CONFIG));
            logger2.log(ironSourceTag2, sb2.toString(), 1);
            IronSourceLoggerManager logger3 = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag3 = IronSourceTag.ADAPTER_API;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(getProviderName());
            sb3.append(" setParamsBeforeInit setting debug mode to ");
            sb3.append(optInt);
            logger3.log(ironSourceTag3, sb3.toString(), 1);
        }
    }

    private HashMap<String, String> getGenenralExtraParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(this.mUserAgeGroup)) {
            hashMap.put("applicationUserAgeGroup", this.mUserAgeGroup);
        }
        if (!TextUtils.isEmpty(this.mUserGender)) {
            hashMap.put("applicationUserGender", this.mUserGender);
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("SDKPluginType", pluginType);
        }
        return hashMap;
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> getRewardedVideoExtraParams(JSONObject jSONObject) {
        HashMap<String, String> genenralExtraParams = getGenenralExtraParams();
        String optString = jSONObject.optString("language");
        if (!TextUtils.isEmpty(optString)) {
            genenralExtraParams.put("language", optString);
        }
        String optString2 = jSONObject.optString("maxVideoLength");
        if (!TextUtils.isEmpty(optString2)) {
            genenralExtraParams.put("maxVideoLength", optString2);
        }
        String optString3 = jSONObject.optString(RequestParameters.CAMPAIGN_ID);
        if (!TextUtils.isEmpty(optString3)) {
            genenralExtraParams.put(RequestParameters.CAMPAIGN_ID, optString3);
        }
        if (!TextUtils.isEmpty(this.mMediationSegment)) {
            genenralExtraParams.put("custom_Segment", this.mMediationSegment);
        }
        addItemNameCountSignature(genenralExtraParams, jSONObject);
        Map rewardedVideoCustomParams = SupersonicConfig.getConfigObj().getRewardedVideoCustomParams();
        if (rewardedVideoCustomParams != null && !rewardedVideoCustomParams.isEmpty()) {
            genenralExtraParams.putAll(rewardedVideoCustomParams);
        }
        return genenralExtraParams;
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> getInterstitialExtraParams() {
        return getGenenralExtraParams();
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> getOfferwallExtraParams(JSONObject jSONObject) {
        HashMap<String, String> genenralExtraParams = getGenenralExtraParams();
        String optString = jSONObject.optString("language");
        if (!TextUtils.isEmpty(optString)) {
            genenralExtraParams.put("language", optString);
        }
        genenralExtraParams.put(ParametersKeys.USE_CLIENT_SIDE_CALLBACKS, String.valueOf(SupersonicConfig.getConfigObj().getClientSideCallbacks()));
        Map offerwallCustomParams = SupersonicConfig.getConfigObj().getOfferwallCustomParams();
        if (offerwallCustomParams != null && !offerwallCustomParams.isEmpty()) {
            genenralExtraParams.putAll(offerwallCustomParams);
        }
        addItemNameCountSignature(genenralExtraParams, jSONObject);
        return genenralExtraParams;
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> getBannerExtraParams(JSONObject jSONObject) {
        return getGenenralExtraParams();
    }

    private void addItemNameCountSignature(HashMap<String, String> hashMap, JSONObject jSONObject) {
        try {
            String optString = jSONObject.optString("itemName");
            int optInt = jSONObject.optInt("itemCount", -1);
            String optString2 = jSONObject.optString("privateKey");
            boolean z = true;
            if (TextUtils.isEmpty(optString)) {
                z = false;
            } else {
                hashMap.put("itemName", optString);
            }
            if (TextUtils.isEmpty(optString2)) {
                z = false;
            }
            if (optInt == -1) {
                z = false;
            } else {
                hashMap.put("itemCount", String.valueOf(optInt));
            }
            if (z) {
                int currentTimestamp = IronSourceUtils.getCurrentTimestamp();
                hashMap.put("timestamp", String.valueOf(currentTimestamp));
                hashMap.put("itemSignature", createItemSig(currentTimestamp, optString, optInt, optString2));
            }
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceTag.ADAPTER_API, " addItemNameCountSignature", e);
        }
    }

    private String createItemSig(int i, String str, int i2, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(str);
        sb.append(i2);
        sb.append(str2);
        return IronSourceUtils.getMD5(sb.toString());
    }

    private String createMinimumOfferCommissionSig(double d, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(d);
        sb.append(str);
        return IronSourceUtils.getMD5(sb.toString());
    }

    private String createUserCreationDateSig(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        sb.append(str3);
        return IronSourceUtils.getMD5(sb.toString());
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007d, code lost:
        if (r8 != false) goto L_0x0082;
     */
    public ISNAdView createBanner(Activity activity, ISBannerSize iSBannerSize, BannerSmashListener bannerSmashListener) {
        char c;
        String description = iSBannerSize.getDescription();
        switch (description.hashCode()) {
            case -387072689:
                if (description.equals("RECTANGLE")) {
                    c = 4;
                    break;
                }
            case 72205083:
                if (description.equals("LARGE")) {
                    c = 1;
                    break;
                }
            case 79011241:
                if (description.equals("SMART")) {
                    c = 2;
                    break;
                }
            case 1951953708:
                if (description.equals(AdPreferences.TYPE_BANNER)) {
                    c = 0;
                    break;
                }
            case 1999208305:
                if (description.equals("CUSTOM")) {
                    c = 3;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        int i = 90;
        int i2 = ModuleDescriptor.MODULE_VERSION;
        switch (c) {
            case 0:
                i = 50;
                break;
            case 1:
                break;
            case 2:
                boolean isLargeScreen = AdapterUtils.isLargeScreen(activity);
                if (isLargeScreen) {
                    i2 = 728;
                    break;
                }
                break;
            case 3:
                int width = iSBannerSize.getWidth();
                int height = iSBannerSize.getHeight();
                if (width >= 320 && (height == 50 || height == 90)) {
                    i = height;
                    i2 = width;
                    break;
                } else {
                    if (bannerSmashListener != null) {
                        bannerSmashListener.onBannerAdLoadFailed(ErrorBuilder.unsupportedBannerSize(IronSourceConstants.SUPERSONIC_CONFIG_NAME));
                    }
                    return null;
                }
            default:
                if (bannerSmashListener != null) {
                    bannerSmashListener.onBannerAdLoadFailed(ErrorBuilder.unsupportedBannerSize(IronSourceConstants.SUPERSONIC_CONFIG_NAME));
                }
                return null;
        }
        return this.mSSAPublisher.createBanner(activity, new ISAdSize(AdapterUtils.dpToPixels(activity, i2), AdapterUtils.dpToPixels(activity, i), description));
    }

    public void onRVNoMoreOffers() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onRVNoMoreOffers ");
        logger.log(ironSourceTag, sb.toString(), 1);
        this.mIsRVAvailable = false;
        Iterator it = this.mAllRewardedVideoSmashes.iterator();
        while (it.hasNext()) {
            RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) it.next();
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
            }
        }
    }

    public void onRVInitSuccess(AdUnitsReady adUnitsReady) {
        int i;
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onRVInitSuccess ");
        logger.log(ironSourceTag, sb.toString(), 1);
        boolean z = false;
        try {
            i = Integer.parseInt(adUnitsReady.getNumOfAdUnits());
        } catch (NumberFormatException e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceTag.NATIVE, ": onRVInitSuccess: parseInt()", e);
            i = 0;
        }
        if (i > 0) {
            z = true;
        }
        this.mIsRVAvailable = z;
        Iterator it = this.mAllRewardedVideoSmashes.iterator();
        while (it.hasNext()) {
            RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) it.next();
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(z);
            }
        }
    }

    public void onRVInitFail(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onRVInitFail ");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllRewardedVideoSmashes.iterator();
        while (it.hasNext()) {
            RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) it.next();
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
            }
        }
    }

    public void onRVAdClicked() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onRVAdClicked ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveRewardedVideoSmash != null) {
            this.mActiveRewardedVideoSmash.onRewardedVideoAdClicked();
        }
    }

    public void onRVEventNotificationReceived(String str, JSONObject jSONObject) {
        if (jSONObject != null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(": onRVEventNotificationReceived: ");
            sb.append(str);
            sb.append(" extData: ");
            sb.append(jSONObject.toString());
            logger.log(ironSourceTag, sb.toString(), 1);
        }
        if (!TextUtils.isEmpty(str) && "impressions".equals(str) && this.mActiveRewardedVideoSmash != null) {
            this.mActiveRewardedVideoSmash.onRewardedVideoAdVisible();
        }
    }

    public void onRVShowFail(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onRVShowFail ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveRewardedVideoSmash != null) {
            this.mActiveRewardedVideoSmash.onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, str));
        }
    }

    public void onRVAdCredited(int i) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onRVAdCredited ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveRewardedVideoSmash != null) {
            this.mActiveRewardedVideoSmash.onRewardedVideoAdRewarded();
        }
    }

    public void onRVAdClosed() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onRVAdClosed ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveRewardedVideoSmash != null) {
            this.mActiveRewardedVideoSmash.onRewardedVideoAdClosed();
        }
    }

    public void onRVAdOpened() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onRVAdOpened ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveRewardedVideoSmash != null) {
            this.mActiveRewardedVideoSmash.onRewardedVideoAdOpened();
        }
    }

    public void onInterstitialInitSuccess() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialInitSuccess ");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllInterstitialSmashes.iterator();
        while (it.hasNext()) {
            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) it.next();
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialInitSuccess();
            }
        }
    }

    public void onInterstitialInitFailed(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialInitFailed ");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllInterstitialSmashes.iterator();
        while (it.hasNext()) {
            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) it.next();
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError(str, "Interstitial"));
            }
        }
    }

    public void onInterstitialLoadSuccess() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialLoadSuccess ");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllInterstitialSmashes.iterator();
        while (it.hasNext()) {
            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) it.next();
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialAdReady();
            }
        }
    }

    public void onInterstitialLoadFailed(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialAdLoadFailed ");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllInterstitialSmashes.iterator();
        while (it.hasNext()) {
            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) it.next();
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(str));
            }
        }
    }

    public void onInterstitialOpen() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialAdOpened ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdOpened();
        }
    }

    public void onInterstitialClose() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialAdClosed ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdClosed();
        }
    }

    public void onInterstitialShowSuccess() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialAdShowSucceeded ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdShowSucceeded();
        }
    }

    public void onInterstitialShowFailed(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialAdShowFailed ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", str));
        }
    }

    public void onInterstitialClick() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onInterstitialAdClicked ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveInterstitialSmash != null) {
            this.mActiveInterstitialSmash.onInterstitialAdClicked();
        }
    }

    public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
        if (jSONObject != null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(": onInterstitialEventNotificationReceived: ");
            sb.append(str);
            sb.append(" extData: ");
            sb.append(jSONObject.toString());
            logger.log(ironSourceTag, sb.toString(), 1);
            if (!TextUtils.isEmpty(str) && "impressions".equals(str) && this.mActiveInterstitialSmash != null) {
                this.mActiveInterstitialSmash.onInterstitialAdVisible();
            }
        }
    }

    public void onOfferwallInitSuccess() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onOfferwallInitSuccess ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.onOfferwallAvailable(true);
        }
    }

    public void onOfferwallInitFail(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onOfferwallInitFail ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.onOfferwallAvailable(false, ErrorBuilder.buildGenericError(str));
        }
    }

    public void onOfferwallEventNotificationReceived(String str, JSONObject jSONObject) {
        if (jSONObject != null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" :onOfferwallEventNotificationReceived ");
            logger.log(ironSourceTag, sb.toString(), 1);
        }
    }

    public void onOWShowSuccess(String str) {
        if (TextUtils.isEmpty(str)) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(":onOWShowSuccess()");
            log(ironSourceTag, sb.toString(), 1);
        } else {
            IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(getProviderName());
            sb2.append(":onOWShowSuccess(placementId:");
            sb2.append(str);
            sb2.append(")");
            log(ironSourceTag2, sb2.toString(), 1);
        }
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.onOfferwallOpened();
        }
    }

    public void onOWShowFail(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onOWShowFail ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.onOfferwallShowFailed(ErrorBuilder.buildGenericError(str));
        }
    }

    public boolean onOWAdCredited(int i, int i2, boolean z) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onOWAdCredited ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mOfferwallListener == null || !this.mOfferwallListener.onOfferwallAdCredited(i, i2, z)) {
            return false;
        }
        return true;
    }

    public void onOWAdClosed() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onOWAdClosed ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.onOfferwallClosed();
        }
    }

    public void onGetOWCreditsFailed(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onGetOWCreditsFailed ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.onGetOfferwallCreditsFailed(ErrorBuilder.buildGenericError(str));
        }
    }

    public void onBannerInitSuccess() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onBannerInitSuccess ");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllBannerSmashes.iterator();
        while (it.hasNext()) {
            BannerSmashListener bannerSmashListener = (BannerSmashListener) it.next();
            if (bannerSmashListener != null) {
                bannerSmashListener.onBannerInitSuccess();
            }
        }
    }

    public void onBannerInitFailed(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onBannerInitFailed ");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllBannerSmashes.iterator();
        while (it.hasNext()) {
            BannerSmashListener bannerSmashListener = (BannerSmashListener) it.next();
            if (bannerSmashListener != null) {
                bannerSmashListener.onBannerInitFailed(ErrorBuilder.buildInitFailedError(str, "Banner"));
            }
        }
    }

    public void onBannerClick() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onBannerAdClicked ");
        logger.log(ironSourceTag, sb.toString(), 1);
        if (this.mActiveBannerSmash != null) {
            this.mActiveBannerSmash.onBannerAdClicked();
        }
    }

    public void onBannerLoadSuccess() {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onBannerLoadSuccess ");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllBannerSmashes.iterator();
        while (it.hasNext()) {
            BannerSmashListener bannerSmashListener = (BannerSmashListener) it.next();
            if (!(bannerSmashListener == null || this.mIsnAdView == null || this.mIsnAdView.getAdViewSize() == null)) {
                LayoutParams layoutParams = new LayoutParams(this.mIsnAdView.getAdViewSize().getWidth(), this.mIsnAdView.getAdViewSize().getHeight());
                layoutParams.gravity = 17;
                bannerSmashListener.onBannerAdLoaded(this.mIsnAdView, layoutParams);
            }
        }
    }

    public void onBannerLoadFail(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": onBannerLoadFail");
        logger.log(ironSourceTag, sb.toString(), 1);
        Iterator it = this.mAllBannerSmashes.iterator();
        while (it.hasNext()) {
            BannerSmashListener bannerSmashListener = (BannerSmashListener) it.next();
            if (bannerSmashListener != null) {
                bannerSmashListener.onBannerAdLoadFailed(ErrorBuilder.buildInitFailedError(str, "Banner"));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void addBannerListener(BannerSmashListener bannerSmashListener) {
        this.mAllBannerSmashes.add(bannerSmashListener);
    }

    /* access modifiers changed from: protected */
    public void removeBannerListener(BannerSmashListener bannerSmashListener) {
        this.mAllBannerSmashes.remove(bannerSmashListener);
    }

    /* access modifiers changed from: protected */
    public void setMediationState(MEDIATION_STATE mediation_state, String str) {
        if (this.mSSAPublisher != null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(": setMediationState(");
            sb.append(str);
            sb.append(" , ");
            sb.append(getProviderName());
            sb.append(" , ");
            sb.append(mediation_state.getValue());
            sb.append(")");
            logger.log(ironSourceTag, sb.toString(), 1);
            this.mSSAPublisher.setMediationState(str, getProviderName(), mediation_state.getValue());
        }
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": setConsent (");
        sb.append(z ? "true" : "false");
        sb.append(")");
        logger.log(ironSourceTag, sb.toString(), 1);
        this.mDidSetConsent = true;
        this.mConsent = z;
        applyConsent(z);
    }

    /* access modifiers changed from: private */
    public void applyConsent(boolean z) {
        if (this.mSSAPublisher != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(RequestParameters.GDPR_CONSENT_STATUS, String.valueOf(z));
                jSONObject.put("demandSourceName", getProviderName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.updateConsentInfo(jSONObject);
        }
    }

    public void earlyInit(Activity activity, String str, String str2, JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": earlyInit");
        IronSourceUtils.sendAutomationLog(sb.toString());
        if (activity == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(getProviderName());
            sb2.append(": null activity");
            logger.log(ironSourceTag, sb2.toString(), 2);
            return;
        }
        if (mDidInitSdk.compareAndSet(false, true)) {
            if (isAdaptersDebugEnabled()) {
                SDKUtils.setDebugMode(3);
            } else {
                SDKUtils.setDebugMode(jSONObject.optInt(FeaturesManager.DEBUG_MODE, 0));
            }
            SDKUtils.setControllerUrl(jSONObject.optString("controllerUrl"));
            IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(getProviderName());
            sb3.append(" IronSourceNetwork setting controller url to  ");
            sb3.append(jSONObject.optString("controllerUrl"));
            logger2.log(ironSourceTag2, sb3.toString(), 1);
            SDKUtils.setControllerConfig(jSONObject.optString(RequestParameters.CONTROLLER_CONFIG, ""));
            IronSourceLoggerManager logger3 = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag3 = IronSourceTag.ADAPTER_API;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(getProviderName());
            sb4.append(" IronSourceNetwork setting controller config to  ");
            sb4.append(jSONObject.optString(RequestParameters.CONTROLLER_CONFIG));
            logger3.log(ironSourceTag3, sb4.toString(), 1);
            HashMap initParams = getInitParams();
            IronSourceNetwork.initSDK(activity, str, str2, initParams);
            IronSourceLoggerManager logger4 = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag4 = IronSourceTag.ADAPTER_API;
            StringBuilder sb5 = new StringBuilder();
            sb5.append("initSDK with appKey=");
            sb5.append(str);
            sb5.append(" userId=");
            sb5.append(str2);
            sb5.append(" parameters ");
            sb5.append(initParams);
            logger4.log(ironSourceTag4, sb5.toString(), 1);
        }
    }

    private HashMap<String, String> getInitParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(this.mUserAgeGroup)) {
            hashMap.put("applicationUserAgeGroup", this.mUserAgeGroup);
        }
        if (!TextUtils.isEmpty(this.mUserGender)) {
            hashMap.put("applicationUserGender", this.mUserGender);
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("SDKPluginType", pluginType);
        }
        if (!TextUtils.isEmpty(this.mMediationSegment)) {
            hashMap.put("custom_Segment", this.mMediationSegment);
        }
        return hashMap;
    }

    private boolean isValidMetaData(String str, String str2) {
        if (str.equals(MetaDataConstants.META_DATA_CCPA_KEY)) {
            return MetaDataUtils.isValidCCPAMetaData(str, str2);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void setMetaData(String str, String str2) {
        if (!mDidInitSdk.get()) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append(getProviderName());
            sb.append(" setMetaData: key=");
            sb.append(str);
            sb.append(", value=");
            sb.append(str2);
            logger.log(ironSourceTag, sb.toString(), 1);
            if (!isValidMetaData(str, str2)) {
                IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(getProviderName());
                sb2.append(" MetaData not valid");
                logger2.log(ironSourceTag2, sb2.toString(), 1);
                return;
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(str, str2);
                IronSourceNetwork.updateMetadata(jSONObject);
            } catch (JSONException e) {
                IronSourceLoggerManager logger3 = IronSourceLoggerManager.getLogger();
                IronSourceTag ironSourceTag3 = IronSourceTag.ADAPTER_API;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(getProviderName());
                sb3.append(" setMetaData error - ");
                sb3.append(e);
                logger3.log(ironSourceTag3, sb3.toString(), 3);
                e.printStackTrace();
            }
        }
    }
}
