package com.ironsource.adapters.ironsource;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.AuctionDataUtils;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.metadata.MetaDataConstants;
import com.ironsource.mediationsdk.metadata.MetaDataUtils;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.IronSourceAdInstanceBuilder;
import com.ironsource.sdk.IronSourceNetwork;
import com.ironsource.sdk.analytics.omid.OMIDManager;
import com.ironsource.sdk.constants.Constants.FeaturesManager;
import com.ironsource.sdk.constants.Constants.JSMethods;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceAdapter extends AbstractAdapter {
    private static final int IS_LOAD_EXCEPTION = 1000;
    private static final int IS_SHOW_EXCEPTION = 1001;
    private static final int RV_LOAD_EXCEPTION = 1002;
    private static final int RV_SHOW_EXCEPTION = 1003;
    private static final String VERSION = "6.16.0";
    private static AtomicBoolean mDidInitSdk = new AtomicBoolean(false);
    private final String ADM_KEY = ParametersKeys.ADM;
    private final String APPLICATION_USER_AGE_GROUP = "applicationUserAgeGroup";
    private final String APPLICATION_USER_GENDER = "applicationUserGender";
    private final String CUSTOM_SEGMENT = "custom_Segment";
    private final String DEMAND_SOURCE_NAME = "demandSourceName";
    private final String DYNAMIC_CONTROLLER_CONFIG = RequestParameters.CONTROLLER_CONFIG;
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = FeaturesManager.DEBUG_MODE;
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private final String SDK_PLUGIN_TYPE = "SDKPluginType";
    private Context mContext;
    private ConcurrentHashMap<String, IronSourceAdInstance> mDemandSourceToISAd;
    private ConcurrentHashMap<String, InterstitialSmashListener> mDemandSourceToISSmash;
    private ConcurrentHashMap<String, IronSourceAdInstance> mDemandSourceToRvAd;
    private ConcurrentHashMap<String, RewardedVideoSmashListener> mDemandSourceToRvSmash;
    private String mMediationSegment;
    private String mUserAgeGroup;
    private String mUserGender;

    private class IronSourceInterstitialListener implements OnInterstitialListener {
        private String mDemandSourceName;
        private InterstitialSmashListener mListener;

        IronSourceInterstitialListener(InterstitialSmashListener interstitialSmashListener, String str) {
            this.mDemandSourceName = str;
            this.mListener = interstitialSmashListener;
        }

        public void onInterstitialInitSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialInitSuccess");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
        }

        public void onInterstitialInitFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialInitFailed");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
        }

        public void onInterstitialLoadSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialLoadSuccess");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onInterstitialAdReady();
        }

        public void onInterstitialLoadFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialLoadFailed ");
            sb.append(str);
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(str));
        }

        public void onInterstitialOpen() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialOpen");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onInterstitialAdOpened();
        }

        public void onInterstitialAdRewarded(String str, int i) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialAdRewarded demandSourceId=");
            sb.append(str);
            sb.append(" amount=");
            sb.append(i);
            ironSourceAdapter.log(ironSourceTag, sb.toString());
        }

        public void onInterstitialClose() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialClose");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onInterstitialAdClosed();
        }

        public void onInterstitialShowSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialShowSuccess");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onInterstitialAdShowSucceeded();
        }

        public void onInterstitialShowFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialShowFailed ");
            sb.append(str);
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", str));
        }

        public void onInterstitialClick() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialClick");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onInterstitialAdClicked();
        }

        public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" interstitialListener onInterstitialEventNotificationReceived eventName=");
            sb.append(str);
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onInterstitialAdVisible();
        }
    }

    private class IronSourceRewardedVideoListener implements OnInterstitialListener {
        private String mDemandSourceName;
        boolean mIsRvDemandOnly;
        RewardedVideoSmashListener mListener;

        IronSourceRewardedVideoListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
            this.mDemandSourceName = str;
            this.mListener = rewardedVideoSmashListener;
            this.mIsRvDemandOnly = false;
        }

        IronSourceRewardedVideoListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str, boolean z) {
            this.mDemandSourceName = str;
            this.mListener = rewardedVideoSmashListener;
            this.mIsRvDemandOnly = z;
        }

        public void onInterstitialInitSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialInitSuccess");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
        }

        public void onInterstitialInitFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialInitFailed");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
        }

        public void onInterstitialLoadSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialLoadSuccess");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            if (this.mIsRvDemandOnly) {
                this.mListener.onRewardedVideoLoadSuccess();
            } else {
                this.mListener.onRewardedVideoAvailabilityChanged(true);
            }
        }

        public void onInterstitialLoadFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialLoadFailed ");
            sb.append(str);
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            if (this.mIsRvDemandOnly) {
                this.mListener.onRewardedVideoLoadFailed(ErrorBuilder.buildLoadFailedError(str));
            } else {
                this.mListener.onRewardedVideoAvailabilityChanged(false);
            }
        }

        public void onInterstitialOpen() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialOpen");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onRewardedVideoAdOpened();
        }

        public void onInterstitialAdRewarded(String str, int i) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialAdRewarded demandSourceId=");
            sb.append(str);
            sb.append(" amount=");
            sb.append(i);
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onRewardedVideoAdRewarded();
        }

        public void onInterstitialClose() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialClose");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onRewardedVideoAdClosed();
        }

        public void onInterstitialShowSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialShowSuccess");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            if (!this.mIsRvDemandOnly) {
                this.mListener.onRewardedVideoAvailabilityChanged(false);
            }
        }

        public void onInterstitialShowFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append("rewardedVideoListener onInterstitialShowSuccess ");
            sb.append(str);
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildShowFailedError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT, str));
        }

        public void onInterstitialClick() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialClick");
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onRewardedVideoAdClicked();
        }

        public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_CALLBACK;
            StringBuilder sb = new StringBuilder();
            sb.append(this.mDemandSourceName);
            sb.append(" rewardedVideoListener onInterstitialEventNotificationReceived eventName=");
            sb.append(str);
            ironSourceAdapter.log(ironSourceTag, sb.toString());
            this.mListener.onRewardedVideoAdVisible();
        }
    }

    public String getVersion() {
        return "6.16.0";
    }

    public static IronSourceAdapter startAdapter(String str) {
        return new IronSourceAdapter(str);
    }

    private IronSourceAdapter(String str) {
        super(str);
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(": new instance");
        log(ironSourceTag, sb.toString());
        this.mDemandSourceToISAd = new ConcurrentHashMap<>();
        this.mDemandSourceToRvAd = new ConcurrentHashMap<>();
        this.mDemandSourceToRvSmash = new ConcurrentHashMap<>();
        this.mDemandSourceToISSmash = new ConcurrentHashMap<>();
        this.mUserAgeGroup = null;
        this.mUserGender = null;
        this.mMediationSegment = null;
    }

    public String getCoreSDKVersion() {
        return SDKUtils.getSDKVersion();
    }

    public void onPause(Activity activity) {
        log(IronSourceTag.ADAPTER_API, "IronSourceNetwork.onPause");
        IronSourceNetwork.onPause(activity);
    }

    public void onResume(Activity activity) {
        log(IronSourceTag.ADAPTER_API, "IronSourceNetwork.onResume");
        IronSourceNetwork.onResume(activity);
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
        StringBuilder sb = new StringBuilder();
        sb.append("setConsent (");
        sb.append(z ? "true" : "false");
        sb.append(")");
        log(ironSourceTag, sb.toString());
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(RequestParameters.GDPR_CONSENT_STATUS, String.valueOf(z));
            IronSourceNetwork.updateConsentInfo(jSONObject);
        } catch (JSONException e) {
            IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("setConsent exception ");
            sb2.append(e.getMessage());
            logError(ironSourceTag2, sb2.toString());
        }
    }

    public void setAge(int i) {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("setAge: ");
        sb.append(i);
        log(ironSourceTag, sb.toString());
        if (i >= 13 && i <= 17) {
            this.mUserAgeGroup = "1";
        } else if (i >= 18 && i <= 20) {
            this.mUserAgeGroup = "2";
        } else if (i >= 21 && i <= 24) {
            this.mUserAgeGroup = "3";
        } else if (i >= 25 && i <= 34) {
            this.mUserAgeGroup = "4";
        } else if (i >= 35 && i <= 44) {
            this.mUserAgeGroup = "5";
        } else if (i >= 45 && i <= 54) {
            this.mUserAgeGroup = OMIDManager.OMID_PARTNER_VERSION;
        } else if (i >= 55 && i <= 64) {
            this.mUserAgeGroup = "7";
        } else if (i <= 65 || i > 120) {
            this.mUserAgeGroup = "0";
        } else {
            this.mUserAgeGroup = "8";
        }
    }

    public void setGender(String str) {
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("setGender: ");
        sb.append(str);
        log(ironSourceTag, sb.toString());
        this.mUserGender = str;
    }

    public void setMediationSegment(String str) {
        this.mMediationSegment = str;
    }

    private HashMap<String, String> getInitParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(this.mUserAgeGroup)) {
            hashMap.put("applicationUserAgeGroup", this.mUserAgeGroup);
        }
        if (!TextUtils.isEmpty(this.mUserGender)) {
            hashMap.put("applicationUserGender", this.mUserGender);
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("SDKPluginType", pluginType);
        }
        if (!TextUtils.isEmpty(this.mMediationSegment)) {
            hashMap.put("custom_Segment", this.mMediationSegment);
        }
        return hashMap;
    }

    public void earlyInit(Activity activity, String str, String str2, JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append(getDemandSourceName(jSONObject));
        sb.append(": earlyInit");
        IronSourceUtils.sendAutomationLog(sb.toString());
        initSDK(activity, str, str2, jSONObject);
    }

    public Map<String, Object> getIsBiddingData(JSONObject jSONObject) {
        log(IronSourceTag.ADAPTER_API, "getIsBiddingData");
        HashMap hashMap = new HashMap();
        String token = IronSourceNetwork.getToken(this.mContext);
        if (token != null) {
            hashMap.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, token);
        } else {
            logError(IronSourceTag.ADAPTER_API, "IS bidding token is null");
            hashMap.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, "");
        }
        return hashMap;
    }

    public void initInterstitialForBidding(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("initInterstitialForBidding demandSourceName=");
        sb.append(demandSourceName);
        log(ironSourceTag, jSONObject, sb.toString());
        initInterstitialInternal(activity, str, str2, jSONObject, interstitialSmashListener, demandSourceName);
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("initInterstitial demandSourceName=");
        sb.append(demandSourceName);
        log(ironSourceTag, jSONObject, sb.toString());
        initInterstitialInternal(activity, str, str2, jSONObject, interstitialSmashListener, demandSourceName);
    }

    private void initInterstitialInternal(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener, String str3) {
        initSDK(activity, str, str2, jSONObject);
        this.mDemandSourceToISSmash.put(str3, interstitialSmashListener);
        interstitialSmashListener.onInterstitialInitSuccess();
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener, String str) {
        log(IronSourceTag.ADAPTER_API, jSONObject, JSMethods.LOAD_INTERSTITIAL);
        try {
            loadAdInternal(getDemandSourceName(jSONObject), str, false, true, false);
        } catch (Exception e) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("loadInterstitial for bidding exception ");
            sb.append(e.getMessage());
            logError(ironSourceTag, sb.toString());
            interstitialSmashListener.onInterstitialAdLoadFailed(new IronSourceError(1000, e.getMessage()));
        }
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(IronSourceTag.ADAPTER_API, jSONObject, JSMethods.LOAD_INTERSTITIAL);
        try {
            loadAdInternal(getDemandSourceName(jSONObject), null, false, false, false);
        } catch (Exception e) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("loadInterstitial exception ");
            sb.append(e.getMessage());
            logError(ironSourceTag, sb.toString());
            interstitialSmashListener.onInterstitialAdLoadFailed(new IronSourceError(1000, e.getMessage()));
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(IronSourceTag.ADAPTER_API, jSONObject, JSMethods.SHOW_INTERSTITIAL);
        try {
            showAdInternal((IronSourceAdInstance) this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)), 2);
        } catch (Exception e) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("showInterstitial exception ");
            sb.append(e.getMessage());
            logError(ironSourceTag, sb.toString());
            interstitialSmashListener.onInterstitialAdShowFailed(new IronSourceError(1001, e.getMessage()));
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        IronSourceAdInstance ironSourceAdInstance = (IronSourceAdInstance) this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject));
        return ironSourceAdInstance != null && IronSourceNetwork.isAdAvailableForInstance(ironSourceAdInstance);
    }

    public Map<String, Object> getRvBiddingData(JSONObject jSONObject) {
        log(IronSourceTag.ADAPTER_API, "getRvBiddingData");
        HashMap hashMap = new HashMap();
        String token = IronSourceNetwork.getToken(this.mContext);
        if (token != null) {
            hashMap.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, token);
        } else {
            logError(IronSourceTag.ADAPTER_API, "RV bidding token is null");
            hashMap.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, "");
        }
        return hashMap;
    }

    public void initRvForBidding(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("initRvForBidding demandSourceName=");
        sb.append(demandSourceName);
        log(ironSourceTag, jSONObject, sb.toString());
        initRewardedVideoInternal(activity, str, str2, jSONObject, rewardedVideoSmashListener, demandSourceName);
        rewardedVideoSmashListener.onRewardedVideoInitSuccess();
    }

    public void initRvForDemandOnly(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("initRvForDemandOnly demandSourceName=");
        sb.append(demandSourceName);
        log(ironSourceTag, jSONObject, sb.toString());
        initRewardedVideoInternal(activity, str, str2, jSONObject, rewardedVideoSmashListener, demandSourceName);
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceTag ironSourceTag = IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append("initRewardedVideo with demandSourceName=");
        sb.append(demandSourceName);
        log(ironSourceTag, jSONObject, sb.toString());
        initRewardedVideoInternal(activity, str, str2, jSONObject, rewardedVideoSmashListener, demandSourceName);
        fetchRewardedVideo(jSONObject);
    }

    private void initRewardedVideoInternal(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener, String str3) {
        initSDK(activity, str, str2, jSONObject);
        this.mDemandSourceToRvSmash.put(str3, rewardedVideoSmashListener);
    }

    private IronSourceAdInstance getAdInstance(String str, boolean z, boolean z2, boolean z3) {
        IronSourceAdInstance ironSourceAdInstance;
        IronSourceAdInstanceBuilder ironSourceAdInstanceBuilder;
        if (z3) {
            ironSourceAdInstance = (IronSourceAdInstance) this.mDemandSourceToRvAd.get(str);
        } else {
            ironSourceAdInstance = (IronSourceAdInstance) this.mDemandSourceToISAd.get(str);
        }
        if (ironSourceAdInstance == null) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("getAdInstance creating ad instance for ");
            sb.append(str);
            sb.append(" isDemandOnlyForRv=");
            sb.append(z);
            sb.append(" isBidder=");
            sb.append(z2);
            sb.append(" isRewarded=");
            sb.append(z3);
            log(ironSourceTag, sb.toString());
            if (z3) {
                ironSourceAdInstanceBuilder = new IronSourceAdInstanceBuilder(str, new IronSourceRewardedVideoListener((RewardedVideoSmashListener) this.mDemandSourceToRvSmash.get(str), str, z)).setExtraParams(getInitParams());
                ironSourceAdInstanceBuilder.setRewarded();
            } else {
                ironSourceAdInstanceBuilder = new IronSourceAdInstanceBuilder(str, new IronSourceInterstitialListener((InterstitialSmashListener) this.mDemandSourceToISSmash.get(str), str)).setExtraParams(getInitParams());
            }
            if (z2) {
                ironSourceAdInstanceBuilder.setInAppBidding();
            }
            ironSourceAdInstance = ironSourceAdInstanceBuilder.build();
            if (z3) {
                this.mDemandSourceToRvAd.put(str, ironSourceAdInstance);
            } else {
                this.mDemandSourceToISAd.put(str, ironSourceAdInstance);
            }
        }
        return ironSourceAdInstance;
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        log(IronSourceTag.ADAPTER_API, jSONObject, "fetchRewardedVideo");
        String demandSourceName = getDemandSourceName(jSONObject);
        try {
            loadAdInternal(demandSourceName, null, false, false, true);
        } catch (Exception e) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("fetchRewardedVideo exception ");
            sb.append(e.getMessage());
            logError(ironSourceTag, sb.toString());
            RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) this.mDemandSourceToRvSmash.get(demandSourceName);
            if (rewardedVideoSmashListener != null) {
                IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("fetchRewardedVideo exception ");
                sb2.append(e.getMessage());
                logError(ironSourceTag2, sb2.toString());
                rewardedVideoSmashListener.onRewardedVideoLoadFailed(new IronSourceError(1002, e.getMessage()));
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
            }
        }
    }

    public void loadVideoForDemandOnly(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(IronSourceTag.ADAPTER_API, jSONObject, "loadVideoForDemandOnly");
        try {
            loadAdInternal(getDemandSourceName(jSONObject), null, true, false, true);
        } catch (Exception e) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("loadVideoForDemandOnly exception ");
            sb.append(e.getMessage());
            logError(ironSourceTag, sb.toString());
            rewardedVideoSmashListener.onRewardedVideoLoadFailed(new IronSourceError(1002, e.getMessage()));
        }
    }

    public void loadVideoForDemandOnly(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
        log(IronSourceTag.ADAPTER_API, jSONObject, "loadVideoForDemandOnly in bidding mode");
        try {
            loadAdInternal(getDemandSourceName(jSONObject), str, true, true, true);
        } catch (Exception e) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("loadVideoForDemandOnly exception ");
            sb.append(e.getMessage());
            logError(ironSourceTag, sb.toString());
            rewardedVideoSmashListener.onRewardedVideoLoadFailed(new IronSourceError(1002, e.getMessage()));
        }
    }

    public void loadVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
        log(IronSourceTag.ADAPTER_API, jSONObject, "loadVideo (RV in bidding mode)");
        try {
            loadAdInternal(getDemandSourceName(jSONObject), str, false, true, true);
        } catch (Exception e) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("loadVideo exception ");
            sb.append(e.getMessage());
            logError(ironSourceTag, sb.toString());
            rewardedVideoSmashListener.onRewardedVideoLoadFailed(new IronSourceError(1002, e.getMessage()));
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
        }
    }

    private void loadAdInternal(String str, String str2, boolean z, boolean z2, boolean z3) throws Exception {
        HashMap hashMap = new HashMap();
        if (str2 != null) {
            hashMap.put(ParametersKeys.ADM, AuctionDataUtils.getInstance().getAdmFromServerData(str2));
            hashMap.putAll(AuctionDataUtils.getInstance().getAuctionResponseServerDataParams(str2));
        }
        IronSourceAdInstance adInstance = getAdInstance(str, z, z2, z3);
        printInstanceExtraParams(hashMap);
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
        StringBuilder sb = new StringBuilder();
        sb.append("loadAd demandSourceName=");
        sb.append(adInstance.getName());
        log(ironSourceTag, sb.toString());
        IronSourceNetwork.loadAd(adInstance, hashMap);
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        try {
            showAdInternal((IronSourceAdInstance) this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)), 1);
        } catch (Exception e) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("showRewardedVideo exception ");
            sb.append(e.getMessage());
            logError(ironSourceTag, sb.toString());
            rewardedVideoSmashListener.onRewardedVideoAdShowFailed(new IronSourceError(1003, e.getMessage()));
        }
    }

    private void showAdInternal(IronSourceAdInstance ironSourceAdInstance, int i) throws Exception {
        int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(i);
        HashMap hashMap = new HashMap();
        hashMap.put("sessionDepth", String.valueOf(sessionDepth));
        IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
        StringBuilder sb = new StringBuilder();
        sb.append("showAd demandSourceName=");
        sb.append(ironSourceAdInstance.getName());
        sb.append(" showParams=");
        sb.append(hashMap);
        log(ironSourceTag, sb.toString());
        IronSourceNetwork.showAd(ironSourceAdInstance, hashMap);
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        IronSourceAdInstance ironSourceAdInstance = (IronSourceAdInstance) this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject));
        return ironSourceAdInstance != null && IronSourceNetwork.isAdAvailableForInstance(ironSourceAdInstance);
    }

    private void initSDK(Activity activity, String str, String str2, JSONObject jSONObject) {
        if (activity == null) {
            logError(IronSourceTag.INTERNAL, "initSDK: null activity");
            return;
        }
        this.mContext = activity.getApplicationContext();
        if (mDidInitSdk.compareAndSet(false, true)) {
            int optInt = jSONObject.optInt(FeaturesManager.DEBUG_MODE, 0);
            if (isAdaptersDebugEnabled()) {
                optInt = 3;
            }
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("initSDK setting debug mode to ");
            sb.append(optInt);
            log(ironSourceTag, sb.toString());
            SDKUtils.setDebugMode(optInt);
            SDKUtils.setControllerUrl(jSONObject.optString("controllerUrl"));
            IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("IronSourceNetwork setting controller url to  ");
            sb2.append(jSONObject.optString("controllerUrl"));
            log(ironSourceTag2, sb2.toString());
            SDKUtils.setControllerConfig(jSONObject.optString(RequestParameters.CONTROLLER_CONFIG));
            IronSourceTag ironSourceTag3 = IronSourceTag.ADAPTER_API;
            StringBuilder sb3 = new StringBuilder();
            sb3.append("IronSourceNetwork setting controller config to  ");
            sb3.append(jSONObject.optString(RequestParameters.CONTROLLER_CONFIG));
            log(ironSourceTag3, sb3.toString());
            HashMap initParams = getInitParams();
            IronSourceTag ironSourceTag4 = IronSourceTag.ADAPTER_API;
            StringBuilder sb4 = new StringBuilder();
            sb4.append("initSDK with appKey=");
            sb4.append(str);
            sb4.append(" userId=");
            sb4.append(str2);
            sb4.append(" parameters ");
            sb4.append(initParams);
            log(ironSourceTag4, sb4.toString());
            IronSourceNetwork.initSDK(activity, str, str2, initParams);
        }
    }

    private String getDemandSourceName(JSONObject jSONObject) {
        if (!TextUtils.isEmpty(jSONObject.optString("demandSourceName"))) {
            return jSONObject.optString("demandSourceName");
        }
        return getProviderName();
    }

    private void logError(IronSourceTag ironSourceTag, String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        StringBuilder sb = new StringBuilder();
        sb.append("IronSourceAdapter: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 3);
    }

    /* access modifiers changed from: private */
    public void log(IronSourceTag ironSourceTag, String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        StringBuilder sb = new StringBuilder();
        sb.append("IronSourceAdapter: ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private void log(IronSourceTag ironSourceTag, JSONObject jSONObject, String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        StringBuilder sb = new StringBuilder();
        sb.append("IronSourceAdapter ");
        sb.append(getDemandSourceName(jSONObject));
        sb.append(": ");
        sb.append(str);
        logger.log(ironSourceTag, sb.toString(), 0);
    }

    private boolean isValidMetaData(String str, String str2) {
        if (str.equals(MetaDataConstants.META_DATA_CCPA_KEY)) {
            return MetaDataUtils.isValidCCPAMetaData(str, str2);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void setMetaData(String str, String str2) {
        if (!mDidInitSdk.get()) {
            IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
            StringBuilder sb = new StringBuilder();
            sb.append("setMetaData: key=");
            sb.append(str);
            sb.append(", value=");
            sb.append(str2);
            log(ironSourceTag, sb.toString());
            if (!isValidMetaData(str, str2)) {
                log(IronSourceTag.ADAPTER_API, "MetaData not valid");
                return;
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(str, str2);
                IronSourceNetwork.updateMetadata(jSONObject);
            } catch (JSONException e) {
                IronSourceTag ironSourceTag2 = IronSourceTag.ADAPTER_API;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("setMetaData error - ");
                sb2.append(e);
                logError(ironSourceTag2, sb2.toString());
                e.printStackTrace();
            }
        }
    }

    private void printInstanceExtraParams(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            log(IronSourceTag.ADAPTER_API, "instance extra params:");
            for (String str : map.keySet()) {
                IronSourceTag ironSourceTag = IronSourceTag.ADAPTER_API;
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(RequestParameters.EQUAL);
                sb.append((String) map.get(str));
                log(ironSourceTag, sb.toString());
            }
        }
    }
}
