package com.ironsource.eventsmodule;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import android.provider.BaseColumns;
import android.util.Log;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.smaato.sdk.video.vast.model.ErrorCode;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class DataBaseEventsStorage extends SQLiteOpenHelper implements IEventsStorageHelper {
    private static final String COMMA_SEP = ",";
    private static final String TYPE_INTEGER = " INTEGER";
    private static final String TYPE_TEXT = " TEXT";
    private static DataBaseEventsStorage mInstance;
    private final int DB_OPEN_BACKOFF_TIME = ErrorCode.GENERAL_LINEAR_ERROR;
    private final int DB_RETRY_NUM = 4;
    private final String SQL_CREATE_ENTRIES = "CREATE TABLE events (_id INTEGER PRIMARY KEY,eventid INTEGER,timestamp INTEGER,type TEXT,data TEXT )";
    private final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS events";

    static abstract class EventEntry implements BaseColumns {
        public static final String COLUMN_NAME_DATA = "data";
        public static final String COLUMN_NAME_EVENT_ID = "eventid";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final int NUMBER_OF_COLUMNS = 4;
        public static final String TABLE_NAME = "events";

        EventEntry() {
        }
    }

    public DataBaseEventsStorage(Context context, String str, int i) {
        super(context, str, null, i);
    }

    public static synchronized DataBaseEventsStorage getInstance(Context context, String str, int i) {
        DataBaseEventsStorage dataBaseEventsStorage;
        synchronized (DataBaseEventsStorage.class) {
            if (mInstance == null) {
                mInstance = new DataBaseEventsStorage(context, str, i);
            }
            dataBaseEventsStorage = mInstance;
        }
        return dataBaseEventsStorage;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0067, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x004c A[SYNTHETIC, Splitter:B:31:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0059 A[SYNTHETIC, Splitter:B:38:0x0059] */
    public synchronized void saveEvents(List<EventData> list, String str) {
        if (list != null) {
            if (!list.isEmpty()) {
                SQLiteDatabase sQLiteDatabase = null;
                try {
                    SQLiteDatabase sQLiteDatabase2 = getDataBaseWithRetries(true);
                    try {
                        for (EventData contentValuesForEvent : list) {
                            ContentValues contentValuesForEvent2 = getContentValuesForEvent(contentValuesForEvent, str);
                            if (!(sQLiteDatabase2 == null || contentValuesForEvent2 == null)) {
                                sQLiteDatabase2.insert(EventEntry.TABLE_NAME, null, contentValuesForEvent2);
                            }
                        }
                        if (sQLiteDatabase2 != null) {
                            if (sQLiteDatabase2.isOpen()) {
                                sQLiteDatabase2.close();
                            }
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (sQLiteDatabase2 != null) {
                            if (sQLiteDatabase2.isOpen()) {
                                sQLiteDatabase2.close();
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    Log.e(IronSourceConstants.IRONSOURCE_CONFIG_NAME, "Exception while saving events: ", th);
                    if (sQLiteDatabase != null) {
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0071, code lost:
        if (r11.isOpen() != false) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0073, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009f, code lost:
        if (r11.isOpen() != false) goto L_0x0073;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0090 A[SYNTHETIC, Splitter:B:40:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00a6 A[SYNTHETIC, Splitter:B:50:0x00a6] */
    public synchronized ArrayList<EventData> loadEvents(String str) {
        ArrayList<EventData> arrayList;
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        arrayList = new ArrayList<>();
        Cursor cursor2 = null;
        try {
            sQLiteDatabase = getDataBaseWithRetries(false);
            String str2 = "type = ?";
            try {
                String[] strArr = {str};
                SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
                cursor = sQLiteDatabase2.query(EventEntry.TABLE_NAME, null, str2, strArr, null, null, "timestamp ASC");
            } catch (Throwable th) {
                th = th;
                try {
                    Log.e(IronSourceConstants.IRONSOURCE_CONFIG_NAME, "Exception while loading events: ", th);
                    if (cursor2 != null) {
                    }
                    if (sQLiteDatabase != null) {
                    }
                    return arrayList;
                } catch (Throwable th2) {
                    th = th2;
                    cursor = cursor2;
                    if (cursor != null) {
                    }
                    sQLiteDatabase.close();
                    throw th;
                }
            }
            try {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        arrayList.add(new EventData(cursor.getInt(cursor.getColumnIndex(EventEntry.COLUMN_NAME_EVENT_ID)), cursor.getLong(cursor.getColumnIndex("timestamp")), new JSONObject(cursor.getString(cursor.getColumnIndex("data")))));
                        cursor.moveToNext();
                    }
                    cursor.close();
                }
                if (cursor != null) {
                    if (!cursor.isClosed()) {
                        cursor.close();
                    }
                }
                if (sQLiteDatabase != null) {
                }
            } catch (Throwable th3) {
                th = th3;
                if (cursor != null) {
                    if (!cursor.isClosed()) {
                        cursor.close();
                    }
                }
                if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                    sQLiteDatabase.close();
                }
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            sQLiteDatabase = null;
            if (cursor != null) {
            }
            sQLiteDatabase.close();
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0019, code lost:
        if (r7.isOpen() != false) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001b, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0039, code lost:
        if (r7.isOpen() != false) goto L_0x001b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0035 A[SYNTHETIC, Splitter:B:24:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0040 A[SYNTHETIC, Splitter:B:30:0x0040] */
    public synchronized void clearEvents(String str) {
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        String str2 = "type = ?";
        String[] strArr = {str};
        try {
            sQLiteDatabase = getDataBaseWithRetries(true);
            try {
                sQLiteDatabase.delete(EventEntry.TABLE_NAME, str2, strArr);
                if (sQLiteDatabase != null) {
                }
            } catch (Throwable th2) {
                th = th2;
                try {
                    Log.e(IronSourceConstants.IRONSOURCE_CONFIG_NAME, "Exception while clearing events: ", th);
                    if (sQLiteDatabase != null) {
                    }
                } catch (Throwable th3) {
                    th = th3;
                    if (sQLiteDatabase != null) {
                    }
                    throw th;
                }
            }
        } catch (Throwable th4) {
            th = th4;
            sQLiteDatabase = null;
            if (sQLiteDatabase != null) {
                if (sQLiteDatabase.isOpen()) {
                    sQLiteDatabase.close();
                }
            }
            throw th;
        }
    }

    private ContentValues getContentValuesForEvent(EventData eventData, String str) {
        if (eventData == null) {
            return null;
        }
        ContentValues contentValues = new ContentValues(4);
        contentValues.put(EventEntry.COLUMN_NAME_EVENT_ID, Integer.valueOf(eventData.getEventId()));
        contentValues.put("timestamp", Long.valueOf(eventData.getTimeStamp()));
        contentValues.put("type", str);
        contentValues.put("data", eventData.getAdditionalData());
        return contentValues;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE events (_id INTEGER PRIMARY KEY,eventid INTEGER,timestamp INTEGER,type TEXT,data TEXT )");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events");
        onCreate(sQLiteDatabase);
    }

    private synchronized SQLiteDatabase getDataBaseWithRetries(boolean z) throws Throwable {
        int i = 0;
        while (true) {
            if (z) {
                try {
                    return getWritableDatabase();
                } catch (Throwable th) {
                    i++;
                    if (i < 4) {
                        SystemClock.sleep((long) (i * ErrorCode.GENERAL_LINEAR_ERROR));
                    } else {
                        throw th;
                    }
                }
            } else {
                return getReadableDatabase();
            }
        }
    }
}
