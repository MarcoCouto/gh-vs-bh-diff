package com.adcolony.sdk;

public abstract class AdColonyAdViewListener {
    String a = "";
    AdColonyAdSize b;
    c0 c;

    /* access modifiers changed from: 0000 */
    public AdColonyAdSize a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public c0 b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        return this.a;
    }

    public void onClicked(AdColonyAdView adColonyAdView) {
    }

    public void onClosed(AdColonyAdView adColonyAdView) {
    }

    public void onLeftApplication(AdColonyAdView adColonyAdView) {
    }

    public void onOpened(AdColonyAdView adColonyAdView) {
    }

    public abstract void onRequestFilled(AdColonyAdView adColonyAdView);

    public void onRequestNotFilled(AdColonyZone adColonyZone) {
    }

    /* access modifiers changed from: 0000 */
    public void a(c0 c0Var) {
        this.c = c0Var;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyAdSize adColonyAdSize) {
        this.b = adColonyAdSize;
    }
}
