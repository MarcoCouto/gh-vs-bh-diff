package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.security.NetworkSecurityPolicy;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.webkit.WebView;
import com.facebook.AccessToken;
import com.facebook.appevents.UserDataStore;
import com.facebook.internal.NativeProtocol;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TapjoyConstants;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

class j {
    private static int i = 2;
    static final String j = "Production";
    private String a = "";
    /* access modifiers changed from: private */
    public String b;
    private boolean c;
    private boolean d;
    private JSONObject e = s.b();
    private String f = "android";
    private String g = "android_native";
    private String h = "";

    class a implements z {

        /* renamed from: com.adcolony.sdk.j$a$a reason: collision with other inner class name */
        class C0005a implements Runnable {
            final /* synthetic */ x a;

            C0005a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                try {
                    if (j.this.f() < 14) {
                        new d(this.a, false).execute(new Void[0]);
                    } else {
                        new d(this.a, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                    }
                } catch (RuntimeException unused) {
                    new a().a("Error retrieving device info, disabling AdColony.").a(u.j);
                    AdColony.disable();
                }
            }
        }

        a() {
        }

        public void a(x xVar) {
            k0.a((Runnable) new C0005a(xVar));
        }
    }

    class b implements z {
        b() {
        }

        public void a(x xVar) {
            JSONObject b = s.b();
            s.b(b, IronSourceConstants.EVENTS_RESULT, k0.f(s.h(xVar.b(), "name")));
            s.b(b, "success", true);
            xVar.a(b).d();
        }
    }

    class c implements Runnable {
        final /* synthetic */ Context a;

        c(Context context) {
            this.a = context;
        }

        public void run() {
            try {
                j.this.b = new WebView(this.a).getSettings().getUserAgentString();
            } catch (RuntimeException e) {
                a aVar = new a();
                StringBuilder sb = new StringBuilder();
                sb.append(e.toString());
                sb.append(": during WebView initialization.");
                aVar.a(sb.toString()).a(" Disabling AdColony.").a(u.i);
                j.this.b = "";
                AdColony.disable();
            }
            a.c().j().a(j.this.b);
        }
    }

    private static class d extends AsyncTask<Void, Void, JSONObject> {
        private x a;
        private boolean b;

        d(x xVar, boolean z) {
            this.a = xVar;
            this.b = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public JSONObject doInBackground(Void... voidArr) {
            if (VERSION.SDK_INT < 14) {
                return null;
            }
            return a.c().h().o();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(JSONObject jSONObject) {
            if (this.b) {
                new x("Device.update_info", 1, jSONObject).d();
            } else {
                this.a.a(jSONObject).d();
            }
        }
    }

    j() {
    }

    /* access modifiers changed from: 0000 */
    public String A() {
        return Build.MODEL;
    }

    /* access modifiers changed from: 0000 */
    public int B() {
        Context b2 = a.b();
        if (b2 == null) {
            return 2;
        }
        int i2 = b2.getResources().getConfiguration().orientation;
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return 2;
        }
        return 1;
    }

    /* access modifiers changed from: 0000 */
    public String C() {
        return VERSION.RELEASE;
    }

    /* access modifiers changed from: 0000 */
    public String D() {
        Context b2 = a.b();
        return b2 == null ? "unknown" : b2.getPackageName();
    }

    /* access modifiers changed from: 0000 */
    public String E() {
        return "4.1.4";
    }

    /* access modifiers changed from: 0000 */
    public String F() {
        String str;
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        TelephonyManager telephonyManager = (TelephonyManager) b2.getSystemService(PlaceFields.PHONE);
        if (telephonyManager == null) {
            str = "";
        } else {
            str = telephonyManager.getSimCountryIso();
        }
        return str;
    }

    /* access modifiers changed from: 0000 */
    public int G() {
        return TimeZone.getDefault().getOffset(15) / 60000;
    }

    /* access modifiers changed from: 0000 */
    public String H() {
        return TimeZone.getDefault().getID();
    }

    /* access modifiers changed from: 0000 */
    public String I() {
        if (this.b == null) {
            Context b2 = a.b();
            if (b2 != null) {
                k0.a((Runnable) new c(b2));
            }
        }
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public boolean J() {
        if (!a.d()) {
            return false;
        }
        int B = B();
        if (B != 0) {
            if (B == 1 && i == 0) {
                new a().a("Sending device info update").a(u.f);
                i = B;
                if (f() < 14) {
                    new d(null, true).execute(new Void[0]);
                } else {
                    new d(null, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                }
                return true;
            }
        } else if (i == 1) {
            new a().a("Sending device info update").a(u.f);
            i = B;
            if (f() < 14) {
                new d(null, true).execute(new Void[0]);
            } else {
                new d(null, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            }
            return true;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void K() {
        this.c = false;
        a.a("Device.get_info", (z) new a());
        a.a("Device.application_exists", (z) new b());
    }

    /* access modifiers changed from: 0000 */
    public boolean L() {
        Context b2 = a.b();
        boolean z = false;
        if (b2 == null) {
            return false;
        }
        DisplayMetrics displayMetrics = b2.getResources().getDisplayMetrics();
        float f2 = ((float) displayMetrics.widthPixels) / displayMetrics.xdpi;
        float f3 = ((float) displayMetrics.heightPixels) / displayMetrics.ydpi;
        if (Math.sqrt((double) ((f2 * f2) + (f3 * f3))) >= 6.0d) {
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        return Secure.getString(b2.getContentResolver(), TapjoyConstants.TJC_ADVERTISING_ID);
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        Context b2 = a.b();
        boolean z = false;
        if (b2 == null) {
            return false;
        }
        try {
            if (Secure.getInt(b2.getContentResolver(), "limit_ad_tracking") != 0) {
                z = true;
            }
        } catch (SettingNotFoundException unused) {
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"HardwareIds"})
    public String e() {
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        return Secure.getString(b2.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
    }

    /* access modifiers changed from: 0000 */
    public int f() {
        return VERSION.SDK_INT;
    }

    /* access modifiers changed from: 0000 */
    public double g() {
        Context b2 = a.b();
        double d2 = Utils.DOUBLE_EPSILON;
        if (b2 == null) {
            return Utils.DOUBLE_EPSILON;
        }
        try {
            Intent registerReceiver = b2.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver == null) {
                return Utils.DOUBLE_EPSILON;
            }
            int intExtra = registerReceiver.getIntExtra("level", -1);
            int intExtra2 = registerReceiver.getIntExtra("scale", -1);
            if (intExtra >= 0 && intExtra2 >= 0) {
                double d3 = (double) intExtra;
                double d4 = (double) intExtra2;
                Double.isNaN(d3);
                Double.isNaN(d4);
                d2 = d3 / d4;
            }
            return d2;
        } catch (IllegalArgumentException unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    public String h() {
        String str;
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        TelephonyManager telephonyManager = (TelephonyManager) b2.getSystemService(PlaceFields.PHONE);
        if (telephonyManager == null) {
            str = "";
        } else {
            str = telephonyManager.getNetworkOperatorName();
        }
        if (str.length() == 0) {
            str = "unknown";
        }
        return str;
    }

    /* access modifiers changed from: 0000 */
    public boolean i() {
        return VERSION.SDK_INT < 23 || NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
    }

    /* access modifiers changed from: 0000 */
    public String j() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public String k() {
        return Locale.getDefault().getCountry();
    }

    /* access modifiers changed from: 0000 */
    public int l() {
        TimeZone timeZone = TimeZone.getDefault();
        if (!timeZone.inDaylightTime(new Date())) {
            return 0;
        }
        return timeZone.getDSTSavings() / 60000;
    }

    /* access modifiers changed from: 0000 */
    public boolean m() {
        Context b2 = a.b();
        if (b2 == null || VERSION.SDK_INT < 29) {
            return false;
        }
        int i2 = b2.getResources().getConfiguration().uiMode & 48;
        if (i2 == 16 || i2 != 32) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public float n() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0.0f;
        }
        return b2.getResources().getDisplayMetrics().density;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject o() {
        JSONObject b2 = s.b();
        h c2 = a.c();
        s.a(b2, TapjoyConstants.TJC_CARRIER_NAME, h());
        s.a(b2, "data_path", a.c().t().b());
        s.b(b2, "device_api", f());
        s.b(b2, "display_width", s());
        s.b(b2, "display_height", r());
        s.b(b2, "screen_width", s());
        s.b(b2, "screen_height", r());
        s.b(b2, "display_dpi", q());
        s.a(b2, TapjoyConstants.TJC_DEVICE_TYPE_NAME, p());
        s.a(b2, "locale_language_code", t());
        s.a(b2, UserDataStore.LAST_NAME, t());
        s.a(b2, "locale_country_code", k());
        s.a(b2, "locale", k());
        s.a(b2, TapjoyConstants.TJC_DEVICE_MAC_ADDRESS, w());
        s.a(b2, "manufacturer", x());
        s.a(b2, "device_brand", x());
        s.a(b2, "media_path", a.c().t().c());
        s.a(b2, "temp_storage_path", a.c().t().d());
        s.b(b2, "memory_class", y());
        s.b(b2, "network_speed", 20);
        s.a(b2, "memory_used_mb", z());
        s.a(b2, "model", A());
        s.a(b2, "device_model", A());
        s.a(b2, TapjoyConstants.TJC_SDK_TYPE, this.g);
        s.a(b2, "sdk_version", E());
        s.a(b2, "network_type", c2.n().a());
        s.a(b2, TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, C());
        s.a(b2, "os_name", this.f);
        s.a(b2, TapjoyConstants.TJC_PLATFORM, this.f);
        s.a(b2, "arch", a());
        s.a(b2, AccessToken.USER_ID_KEY, s.h(c2.q().d, AccessToken.USER_ID_KEY));
        s.a(b2, "app_id", c2.q().a);
        s.a(b2, "app_bundle_name", k0.d());
        s.a(b2, "app_bundle_version", k0.e());
        s.a(b2, "battery_level", g());
        s.a(b2, "cell_service_country_code", F());
        s.a(b2, "timezone_ietf", H());
        s.b(b2, "timezone_gmt_m", G());
        s.b(b2, "timezone_dst_m", l());
        s.a(b2, "launch_metadata", u());
        s.a(b2, "controller_version", c2.c());
        int B = B();
        i = B;
        s.b(b2, "current_orientation", B);
        s.b(b2, "cleartext_permitted", i());
        s.a(b2, "density", (double) n());
        s.b(b2, "dark_mode", m());
        JSONArray a2 = s.a();
        if (k0.f("com.android.vending")) {
            a2.put("google");
        }
        if (k0.f("com.amazon.venezia")) {
            a2.put("amazon");
        }
        s.a(b2, "available_stores", a2);
        s.a(b2, NativeProtocol.RESULT_ARGS_PERMISSIONS, k0.c(a.b()));
        int i2 = 40;
        while (!this.c && i2 > 0) {
            try {
                Thread.sleep(50);
                i2--;
            } catch (Exception unused) {
            }
        }
        s.a(b2, "advertiser_id", b());
        s.b(b2, "limit_tracking", v());
        if (b() == null || b().equals("")) {
            s.a(b2, "android_id_sha1", k0.b(e()));
        }
        return b2;
    }

    /* access modifiers changed from: 0000 */
    public String p() {
        return L() ? "tablet" : PlaceFields.PHONE;
    }

    /* access modifiers changed from: 0000 */
    public int q() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) b2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.densityDpi;
    }

    /* access modifiers changed from: 0000 */
    public int r() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) b2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.heightPixels;
    }

    /* access modifiers changed from: 0000 */
    public int s() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) b2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.widthPixels;
    }

    /* access modifiers changed from: 0000 */
    public String t() {
        return Locale.getDefault().getLanguage();
    }

    /* access modifiers changed from: 0000 */
    public JSONObject u() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public boolean v() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public String w() {
        return "";
    }

    /* access modifiers changed from: 0000 */
    public String x() {
        return Build.MANUFACTURER;
    }

    /* access modifiers changed from: 0000 */
    public int y() {
        Context b2 = a.b();
        int i2 = 0;
        if (b2 == null) {
            return 0;
        }
        ActivityManager activityManager = (ActivityManager) b2.getSystemService("activity");
        if (activityManager != null) {
            i2 = activityManager.getMemoryClass();
        }
        return i2;
    }

    /* access modifiers changed from: 0000 */
    public long z() {
        Runtime runtime = Runtime.getRuntime();
        return (runtime.totalMemory() - runtime.freeMemory()) / ((long) 1048576);
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        this.e = jSONObject;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.h = str;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.c = z;
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return System.getProperty("os.arch").toLowerCase(Locale.ENGLISH);
    }
}
