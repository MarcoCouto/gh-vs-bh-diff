package com.adcolony.sdk;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class d {
    private HashMap<String, c> a;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AdColonyInterstitial> b;
    private HashMap<String, AdColonyAdViewListener> c;
    /* access modifiers changed from: private */
    public HashMap<String, AdColonyAdView> d;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            d.this.c(xVar);
        }
    }

    class b implements z {
        b() {
        }

        public void a(x xVar) {
            d.this.a(xVar);
        }
    }

    class c implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) d.this.b.get(s.h(this.a.b(), "id"));
                if (adColonyInterstitial != null && adColonyInterstitial.getListener() != null) {
                    adColonyInterstitial.getListener().onAudioStopped(adColonyInterstitial);
                }
            }
        }

        c() {
        }

        public void a(x xVar) {
            k0.a((Runnable) new a(xVar));
        }
    }

    /* renamed from: com.adcolony.sdk.d$d reason: collision with other inner class name */
    class C0002d implements z {

        /* renamed from: com.adcolony.sdk.d$d$a */
        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) d.this.b.get(s.h(this.a.b(), "id"));
                if (adColonyInterstitial != null && adColonyInterstitial.getListener() != null) {
                    adColonyInterstitial.getListener().onAudioStarted(adColonyInterstitial);
                }
            }
        }

        C0002d() {
        }

        public void a(x xVar) {
            k0.a((Runnable) new a(xVar));
        }
    }

    class e implements z {
        e() {
        }

        public void a(x xVar) {
            d.this.i(xVar);
        }
    }

    class f implements z {
        f() {
        }

        public void a(x xVar) {
            d.this.b(xVar);
        }
    }

    class g implements z {
        g() {
        }

        public void a(x xVar) {
            d.this.h(xVar);
        }
    }

    class h implements z {
        h() {
        }

        public void a(x xVar) {
            JSONObject b = s.b();
            s.b(b, "success", true);
            xVar.a(b).d();
        }
    }

    class i implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                x xVar = this.a;
                xVar.a(xVar.b()).d();
            }
        }

        i() {
        }

        public void a(x xVar) {
            k0.a((Runnable) new a(xVar));
        }
    }

    class j implements Runnable {
        final /* synthetic */ AdColonyInterstitial a;
        final /* synthetic */ AdColonyInterstitialListener b;

        j(AdColonyInterstitial adColonyInterstitial, AdColonyInterstitialListener adColonyInterstitialListener) {
            this.a = adColonyInterstitial;
            this.b = adColonyInterstitialListener;
        }

        public void run() {
            this.a.a(true);
            this.b.onExpiring(this.a);
            k i = a.c().i();
            if (i.a() != null) {
                i.a().dismiss();
                i.a((AlertDialog) null);
            }
        }
    }

    class k implements Runnable {
        final /* synthetic */ Context a;
        final /* synthetic */ x b;
        final /* synthetic */ AdColonyAdViewListener c;
        final /* synthetic */ String d;

        k(Context context, x xVar, AdColonyAdViewListener adColonyAdViewListener, String str) {
            this.a = context;
            this.b = xVar;
            this.c = adColonyAdViewListener;
            this.d = str;
        }

        public void run() {
            AdColonyAdView adColonyAdView = new AdColonyAdView(this.a, this.b, this.c);
            d.this.d.put(this.d, adColonyAdView);
            adColonyAdView.setOmidManager(this.c.b());
            adColonyAdView.d();
            this.c.a((c0) null);
            this.c.onRequestFilled(adColonyAdView);
        }
    }

    class l implements Runnable {
        final /* synthetic */ AdColonyInterstitial a;
        final /* synthetic */ x b;
        final /* synthetic */ AdColonyInterstitialListener c;

        l(AdColonyInterstitial adColonyInterstitial, x xVar, AdColonyInterstitialListener adColonyInterstitialListener) {
            this.a = adColonyInterstitial;
            this.b = xVar;
            this.c = adColonyInterstitialListener;
        }

        public void run() {
            if (this.a.e() == null) {
                this.a.a(s.g(this.b.b(), "iab"));
            }
            this.a.a(s.h(this.b.b(), "ad_id"));
            this.a.b(s.h(this.b.b(), "creative_id"));
            c0 e = this.a.e();
            if (!(e == null || e.d() == 2)) {
                try {
                    e.a();
                } catch (IllegalArgumentException unused) {
                    new a().a("IllegalArgumentException when creating omid session").a(u.j);
                }
            }
            this.c.onRequestFilled(this.a);
        }
    }

    class m implements Runnable {
        final /* synthetic */ AdColonyInterstitial a;
        final /* synthetic */ AdColonyInterstitialListener b;

        m(AdColonyInterstitial adColonyInterstitial, AdColonyInterstitialListener adColonyInterstitialListener) {
            this.a = adColonyInterstitial;
            this.b = adColonyInterstitialListener;
        }

        public void run() {
            AdColonyZone adColonyZone = (AdColonyZone) a.c().w().get(this.a.getZoneID());
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(this.a.getZoneID());
                adColonyZone.b(6);
            }
            this.b.onRequestNotFilled(adColonyZone);
        }
    }

    class n implements Runnable {
        final /* synthetic */ AdColonyInterstitialListener a;
        final /* synthetic */ AdColonyInterstitial b;

        n(AdColonyInterstitialListener adColonyInterstitialListener, AdColonyInterstitial adColonyInterstitial) {
            this.a = adColonyInterstitialListener;
            this.b = adColonyInterstitial;
        }

        public void run() {
            a.c().d(false);
            this.a.onClosed(this.b);
        }
    }

    class o implements Runnable {
        final /* synthetic */ c a;

        o(c cVar) {
            this.a = cVar;
        }

        public void run() {
            for (int i = 0; i < this.a.i().size(); i++) {
                a.b((String) this.a.j().get(i), (z) this.a.i().get(i));
            }
            this.a.j().clear();
            this.a.i().clear();
            this.a.removeAllViews();
            c cVar = this.a;
            cVar.A = null;
            cVar.z = null;
            new a().a("Destroying container tied to ad_session_id = ").a(this.a.a()).a(u.f);
            for (m0 m0Var : this.a.n().values()) {
                if (!m0Var.s()) {
                    int c = m0Var.c();
                    if (c <= 0) {
                        c = m0Var.d();
                    }
                    a.c().a(c);
                    m0Var.loadUrl("about:blank");
                    m0Var.clearCache(true);
                    m0Var.removeAllViews();
                    m0Var.a(true);
                }
            }
            new a().a("Stopping and releasing all media players associated with ").a("VideoViews tied to ad_session_id = ").a(this.a.a()).a(u.f);
            for (l0 l0Var : this.a.m().values()) {
                l0Var.i();
                l0Var.j();
            }
            this.a.m().clear();
            this.a.l().clear();
            this.a.n().clear();
            this.a.h().clear();
            this.a.e().clear();
            this.a.f().clear();
            this.a.g().clear();
            this.a.m = true;
        }
    }

    class p implements Runnable {
        final /* synthetic */ AdColonyAdViewListener a;

        p(AdColonyAdViewListener adColonyAdViewListener) {
            this.a = adColonyAdViewListener;
        }

        public void run() {
            String c = this.a.c();
            AdColonyZone adColonyZone = (AdColonyZone) a.c().w().get(c);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(c);
                adColonyZone.b(6);
            }
            this.a.onRequestNotFilled(adColonyZone);
        }
    }

    class q implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                d.this.e(this.a);
            }
        }

        q() {
        }

        public void a(x xVar) {
            k0.a((Runnable) new a(xVar));
        }
    }

    class r implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                d.this.f(this.a);
            }
        }

        r() {
        }

        public void a(x xVar) {
            k0.a((Runnable) new a(xVar));
        }
    }

    class s implements z {
        s() {
        }

        public void a(x xVar) {
            d.this.k(xVar);
        }
    }

    class t implements z {
        t() {
        }

        public void a(x xVar) {
            d.this.j(xVar);
        }
    }

    class u implements z {
        u() {
        }

        public void a(x xVar) {
            d.this.g(xVar);
        }
    }

    class v implements z {
        v() {
        }

        public void a(x xVar) {
            d.this.l(xVar);
        }
    }

    class w implements z {
        w() {
        }

        public void a(x xVar) {
            d.this.d(xVar);
        }
    }

    d() {
    }

    /* access modifiers changed from: private */
    public boolean k(x xVar) {
        JSONObject b2 = xVar.b();
        String c2 = xVar.c();
        String h2 = s.h(b2, "ad_session_id");
        int f2 = s.f(b2, "view_id");
        c cVar = (c) this.a.get(h2);
        View view = (View) cVar.e().get(Integer.valueOf(f2));
        if (cVar == null) {
            a(c2, h2);
            return false;
        } else if (view == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(f2);
            a(c2, sb.toString());
            return false;
        } else {
            view.bringToFront();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean l(x xVar) {
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "id");
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.get(h2);
        AdColonyAdView adColonyAdView = (AdColonyAdView) this.d.get(h2);
        int a2 = s.a(b2, "orientation", -1);
        boolean z = adColonyAdView != null;
        if (adColonyInterstitial != null || z) {
            JSONObject b3 = s.b();
            s.a(b3, "id", h2);
            if (adColonyInterstitial != null) {
                adColonyInterstitial.a(s.f(b3, "module_id"));
                adColonyInterstitial.b(a2);
                adColonyInterstitial.i();
            }
            return true;
        }
        a(xVar.c(), h2);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean c(x xVar) {
        String h2 = s.h(xVar.b(), "id");
        AdColonyAdViewListener adColonyAdViewListener = (AdColonyAdViewListener) this.c.remove(h2);
        if (adColonyAdViewListener == null) {
            a(xVar.c(), h2);
            return false;
        }
        k0.a((Runnable) new p(adColonyAdViewListener));
        return true;
    }

    /* access modifiers changed from: private */
    public boolean d(x xVar) {
        String h2 = s.h(xVar.b(), "id");
        AdColonyAdViewListener adColonyAdViewListener = (AdColonyAdViewListener) this.c.remove(h2);
        if (adColonyAdViewListener == null) {
            a(xVar.c(), h2);
            return false;
        }
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        k kVar = new k(b2, xVar, adColonyAdViewListener, h2);
        k0.a((Runnable) kVar);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean f(x xVar) {
        String h2 = s.h(xVar.b(), "ad_session_id");
        c cVar = (c) this.a.get(h2);
        if (cVar == null) {
            a(xVar.c(), h2);
            return false;
        }
        a(cVar);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean g(x xVar) {
        AdColonyInterstitialListener adColonyInterstitialListener;
        JSONObject b2 = xVar.b();
        int f2 = s.f(b2, "status");
        if (f2 == 5 || f2 == 1 || f2 == 0 || f2 == 6) {
            return false;
        }
        String h2 = s.h(b2, "id");
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.remove(h2);
        if (adColonyInterstitial == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = adColonyInterstitial.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(xVar.c(), h2);
            return false;
        }
        k0.a((Runnable) new n(adColonyInterstitialListener, adColonyInterstitial));
        adColonyInterstitial.a((c) null);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean h(x xVar) {
        String h2 = s.h(xVar.b(), "id");
        JSONObject b2 = s.b();
        s.a(b2, "id", h2);
        Context b3 = a.b();
        if (b3 == null) {
            s.b(b2, "has_audio", false);
            xVar.a(b2).d();
            return false;
        }
        boolean b4 = k0.b(k0.a(b3));
        double a2 = k0.a(k0.a(b3));
        s.b(b2, "has_audio", b4);
        s.a(b2, AvidVideoPlaybackListenerImpl.VOLUME, a2);
        xVar.a(b2).d();
        return b4;
    }

    /* access modifiers changed from: private */
    public boolean i(x xVar) {
        AdColonyInterstitialListener adColonyInterstitialListener;
        String h2 = s.h(xVar.b(), "id");
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.get(h2);
        if (adColonyInterstitial == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = adColonyInterstitial.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(xVar.c(), h2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            k0.a((Runnable) new l(adColonyInterstitial, xVar, adColonyInterstitialListener));
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean j(x xVar) {
        JSONObject b2 = xVar.b();
        String c2 = xVar.c();
        String h2 = s.h(b2, "ad_session_id");
        int f2 = s.f(b2, "view_id");
        c cVar = (c) this.a.get(h2);
        if (cVar == null) {
            a(c2, h2);
            return false;
        }
        View view = (View) cVar.e().get(Integer.valueOf(f2));
        if (view == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(f2);
            a(c2, sb.toString());
            return false;
        }
        cVar.removeView(view);
        cVar.addView(view, view.getLayoutParams());
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        this.a = new HashMap<>();
        this.b = new ConcurrentHashMap<>();
        this.c = new HashMap<>();
        this.d = new HashMap<>();
        a.a("AdContainer.create", (z) new q());
        a.a("AdContainer.destroy", (z) new r());
        a.a("AdContainer.move_view_to_index", (z) new s());
        a.a("AdContainer.move_view_to_front", (z) new t());
        a.a("AdSession.finish_fullscreen_ad", (z) new u());
        a.a("AdSession.start_fullscreen_ad", (z) new v());
        a.a("AdSession.ad_view_available", (z) new w());
        a.a("AdSession.ad_view_unavailable", (z) new a());
        a.a("AdSession.expiring", (z) new b());
        a.a("AdSession.audio_stopped", (z) new c());
        a.a("AdSession.audio_started", (z) new C0002d());
        a.a("AdSession.interstitial_available", (z) new e());
        a.a("AdSession.interstitial_unavailable", (z) new f());
        a.a("AdSession.has_audio", (z) new g());
        a.a("WebView.prepare", (z) new h());
        a.a("AdSession.expanded", (z) new i());
    }

    /* access modifiers changed from: 0000 */
    public boolean a(x xVar) {
        AdColonyInterstitialListener adColonyInterstitialListener;
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "id");
        if (s.f(b2, "type") == 0) {
            AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.remove(h2);
            if (adColonyInterstitial == null) {
                adColonyInterstitialListener = null;
            } else {
                adColonyInterstitialListener = adColonyInterstitial.getListener();
            }
            if (adColonyInterstitialListener == null) {
                a(xVar.c(), h2);
                return false;
            } else if (!a.d()) {
                return false;
            } else {
                k0.a((Runnable) new j(adColonyInterstitial, adColonyInterstitialListener));
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean b(x xVar) {
        AdColonyInterstitialListener adColonyInterstitialListener;
        String h2 = s.h(xVar.b(), "id");
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.remove(h2);
        if (adColonyInterstitial == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = adColonyInterstitial.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(xVar.c(), h2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            k0.a((Runnable) new m(adColonyInterstitial, adColonyInterstitialListener));
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, AdColonyAdViewListener> c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, AdColonyAdView> b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, c> d() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, AdColonyAdViewListener adColonyAdViewListener, AdColonyAdSize adColonyAdSize, AdColonyAdOptions adColonyAdOptions) {
        String a2 = k0.a();
        JSONObject b2 = s.b();
        float n2 = a.c().h().n();
        s.a(b2, "zone_id", str);
        s.b(b2, "type", 1);
        s.b(b2, "width_pixels", (int) (((float) adColonyAdSize.getWidth()) * n2));
        s.b(b2, "height_pixels", (int) (((float) adColonyAdSize.getHeight()) * n2));
        s.b(b2, "width", adColonyAdSize.getWidth());
        s.b(b2, "height", adColonyAdSize.getHeight());
        s.a(b2, "id", a2);
        adColonyAdViewListener.a(str);
        adColonyAdViewListener.a(adColonyAdSize);
        if (adColonyAdOptions != null) {
            JSONObject jSONObject = adColonyAdOptions.d;
            if (jSONObject != null) {
                s.a(b2, "options", jSONObject);
            }
        }
        this.c.put(a2, adColonyAdViewListener);
        new x("AdSession.on_request", 1, b2).d();
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, AdColonyInterstitialListener adColonyInterstitialListener, AdColonyAdOptions adColonyAdOptions) {
        String a2 = k0.a();
        h c2 = a.c();
        JSONObject b2 = s.b();
        s.a(b2, "zone_id", str);
        s.b(b2, Events.CREATIVE_FULLSCREEN, true);
        s.b(b2, "width", c2.h().s());
        s.b(b2, "height", c2.h().r());
        s.b(b2, "type", 0);
        s.a(b2, "id", a2);
        new a().a("AdSession request with id = ").a(a2).a(u.d);
        AdColonyInterstitial adColonyInterstitial = new AdColonyInterstitial(a2, adColonyInterstitialListener, str);
        this.b.put(a2, adColonyInterstitial);
        if (!(adColonyAdOptions == null || adColonyAdOptions.d == null)) {
            adColonyInterstitial.a(adColonyAdOptions);
            s.a(b2, "options", adColonyAdOptions.d);
        }
        new a().a("Requesting AdColony interstitial advertisement.").a(u.c);
        new x("AdSession.on_request", 1, b2).d();
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        k0.a((Runnable) new o(cVar));
        AdColonyAdView adColonyAdView = (AdColonyAdView) this.d.get(cVar.a());
        if (adColonyAdView == null || adColonyAdView.c()) {
            new a().a("Removing ad 4").a(u.d);
            this.a.remove(cVar.a());
            cVar.z = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, String str2) {
        new a().a("Message '").a(str).a("' sent with invalid id: ").a(str2).a(u.i);
    }

    /* access modifiers changed from: 0000 */
    public ConcurrentHashMap<String, AdColonyInterstitial> a() {
        return this.b;
    }

    /* access modifiers changed from: private */
    public boolean e(x xVar) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        JSONObject b3 = xVar.b();
        String h2 = s.h(b3, "ad_session_id");
        c cVar = new c(b2.getApplicationContext(), h2);
        cVar.j(xVar);
        this.a.put(h2, cVar);
        if (s.f(b3, "width") == 0) {
            AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) this.b.get(h2);
            if (adColonyInterstitial == null) {
                a(xVar.c(), h2);
                return false;
            }
            adColonyInterstitial.a(cVar);
        } else {
            cVar.a(false);
        }
        JSONObject b4 = s.b();
        s.b(b4, "success", true);
        xVar.a(b4).d();
        return true;
    }
}
