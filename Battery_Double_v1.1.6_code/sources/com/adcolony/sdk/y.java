package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"UseSparseArrays"})
class y {
    private ArrayList<a0> a = new ArrayList<>();
    private HashMap<Integer, a0> b = new HashMap<>();
    private int c = 2;
    private HashMap<String, ArrayList<z>> d = new HashMap<>();
    private JSONArray e = s.a();
    /* access modifiers changed from: private */
    public int f = 1;

    class a implements Runnable {
        final /* synthetic */ Context a;

        a(Context context) {
            this.a = context;
        }

        public void run() {
            AdColonyAppOptions q = a.c().q();
            q.e();
            JSONObject b2 = q.b();
            JSONObject b3 = s.b();
            s.a(b2, "os_name", "android");
            StringBuilder sb = new StringBuilder();
            sb.append(a.c().t().a());
            sb.append("7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5");
            s.a(b3, "filepath", sb.toString());
            s.a(b3, String.VIDEO_INFO, b2);
            s.b(b3, "m_origin", 0);
            s.b(b3, "m_id", y.this.f = y.this.f + 1);
            s.a(b3, "m_type", "Controller.create");
            try {
                new m0(this.a, 1, false).a(true, new x(b3));
            } catch (RuntimeException e) {
                a aVar = new a();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(e.toString());
                sb2.append(": during WebView initialization.");
                aVar.a(sb2.toString()).a(" Disabling AdColony.").a(u.i);
                AdColony.disable();
            }
        }
    }

    class b implements Runnable {
        final /* synthetic */ String a;
        final /* synthetic */ JSONObject b;

        b(String str, JSONObject jSONObject) {
            this.a = str;
            this.b = jSONObject;
        }

        public void run() {
            y.this.a(this.a, this.b);
        }
    }

    y() {
    }

    /* access modifiers changed from: 0000 */
    public void b(String str, z zVar) {
        synchronized (this.d) {
            ArrayList arrayList = (ArrayList) this.d.get(str);
            if (arrayList != null) {
                arrayList.remove(zVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, a0> c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public int d() {
        int i = this.c;
        this.c = i + 1;
        return i;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void e() {
        synchronized (this.a) {
            for (int size = this.a.size() - 1; size >= 0; size--) {
                ((a0) this.a.get(size)).a();
            }
        }
        JSONArray jSONArray = null;
        if (this.e.length() > 0) {
            jSONArray = this.e;
            this.e = s.a();
        }
        if (jSONArray != null) {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string = jSONObject.getString("m_type");
                    if (jSONObject.getInt("m_origin") >= 2) {
                        k0.a((Runnable) new b(string, jSONObject));
                    } else {
                        a(string, jSONObject);
                    }
                } catch (JSONException e2) {
                    new a().a("JSON error from message dispatcher's updateModules(): ").a(e2.toString()).a(u.j);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, z zVar) {
        ArrayList arrayList = (ArrayList) this.d.get(str);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.d.put(str, arrayList);
        }
        arrayList.add(zVar);
    }

    /* access modifiers changed from: 0000 */
    public ArrayList<a0> b() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        h c2 = a.c();
        if (!c2.y() && !c2.z()) {
            Context b2 = a.b();
            if (b2 != null) {
                k0.a((Runnable) new a(b2));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public a0 a(a0 a0Var) {
        synchronized (this.a) {
            int c2 = a0Var.c();
            if (c2 <= 0) {
                c2 = a0Var.d();
            }
            this.a.add(a0Var);
            this.b.put(Integer.valueOf(c2), a0Var);
        }
        return a0Var;
    }

    /* access modifiers changed from: 0000 */
    public a0 a(int i) {
        synchronized (this.a) {
            a0 a0Var = (a0) this.b.get(Integer.valueOf(i));
            if (a0Var == null) {
                return null;
            }
            this.a.remove(a0Var);
            this.b.remove(Integer.valueOf(i));
            a0Var.b();
            return a0Var;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, JSONObject jSONObject) {
        synchronized (this.d) {
            ArrayList arrayList = (ArrayList) this.d.get(str);
            if (arrayList != null) {
                x xVar = new x(jSONObject);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    try {
                        ((z) it.next()).a(xVar);
                    } catch (RuntimeException e2) {
                        new a().a((Object) e2).a(u.j);
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        try {
            if (!jSONObject.has("m_id")) {
                String str = "m_id";
                int i = this.f;
                this.f = i + 1;
                jSONObject.put(str, i);
            }
            if (!jSONObject.has("m_origin")) {
                jSONObject.put("m_origin", 0);
            }
            int i2 = jSONObject.getInt("m_target");
            if (i2 == 0) {
                synchronized (this) {
                    this.e.put(jSONObject);
                }
                return;
            }
            a0 a0Var = (a0) this.b.get(Integer.valueOf(i2));
            if (a0Var != null) {
                a0Var.a(jSONObject);
            }
        } catch (JSONException e2) {
            new a().a("JSON error in ADCMessageDispatcher's sendMessage(): ").a(e2.toString()).a(u.j);
        }
    }
}
