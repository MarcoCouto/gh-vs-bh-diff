package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.RequiresApi;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.ConsoleMessage;
import android.webkit.ConsoleMessage.MessageLevel;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsoluteLayout.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.iab.omid.library.adcolony.ScriptInjector;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;

@SuppressLint({"SetJavaScriptEnabled"})
class m0 extends WebView implements a0 {
    static boolean N = false;
    /* access modifiers changed from: private */
    public boolean A;
    private boolean B;
    /* access modifiers changed from: private */
    public boolean C;
    private boolean D;
    private boolean E;
    private boolean F;
    /* access modifiers changed from: private */
    public JSONArray G = s.a();
    /* access modifiers changed from: private */
    public JSONObject H = s.b();
    private JSONObject I = s.b();
    /* access modifiers changed from: private */
    public c J;
    /* access modifiers changed from: private */
    public x K;
    private ImageView L;
    /* access modifiers changed from: private */
    public final Object M = new Object();
    /* access modifiers changed from: private */
    public String a;
    private String b;
    private String c = "";
    private String d = "";
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f = "";
    private String g = "";
    private String h = "";
    /* access modifiers changed from: private */
    public String i = "";
    private String j = "";
    /* access modifiers changed from: private */
    public int k;
    private int l;
    private int m;
    private int n;
    private int o;
    /* access modifiers changed from: private */
    public int p;
    private int q;
    /* access modifiers changed from: private */
    public int r;
    private int s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public int u;
    private int v;
    private int w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public boolean y;
    private boolean z;

    class a implements Runnable {
        a() {
        }

        public void run() {
            String str = "";
            synchronized (m0.this.M) {
                if (m0.this.G.length() > 0) {
                    if (m0.this.x) {
                        str = m0.this.G.toString();
                    }
                    m0.this.G = s.a();
                }
            }
            if (m0.this.x) {
                m0 m0Var = m0.this;
                StringBuilder sb = new StringBuilder();
                sb.append("NativeLayer.dispatch_messages(ADC3_update(");
                sb.append(str);
                sb.append("));");
                m0Var.a(sb.toString());
            }
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            c0 c0Var;
            int i;
            try {
                d b = a.c().b();
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) b.a().get(m0.this.e);
                AdColonyAdView adColonyAdView = (AdColonyAdView) b.b().get(m0.this.e);
                if (adColonyInterstitial == null) {
                    c0Var = null;
                } else {
                    c0Var = adColonyInterstitial.e();
                }
                if (c0Var == null && adColonyAdView != null) {
                    c0Var = adColonyAdView.getOmidManager();
                }
                if (c0Var == null) {
                    i = -1;
                } else {
                    i = c0Var.d();
                }
                if (c0Var != null && i == 2) {
                    c0Var.a((WebView) m0.this);
                    c0Var.a(m0.this.J);
                }
            } catch (IllegalArgumentException unused) {
                new a().a("IllegalArgumentException when creating omid session").a(u.j);
            }
        }
    }

    class c extends WebChromeClient {
        final /* synthetic */ JSONObject a;

        c(JSONObject jSONObject) {
            this.a = jSONObject;
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            String str;
            MessageLevel messageLevel = consoleMessage.messageLevel();
            String message = consoleMessage.message();
            boolean z = false;
            boolean z2 = messageLevel == MessageLevel.ERROR;
            String str2 = "Viewport target-densitydpi is not supported.";
            if (consoleMessage.message().contains("Viewport argument key \"shrink-to-fit\" not recognized and ignored") || consoleMessage.message().contains(str2)) {
                z = true;
            }
            if (message.contains("ADC3_update is not defined") || message.contains("NativeLayer.dispatch_messages is not a function")) {
                m0.this.a(this.a, "Unable to communicate with AdColony. Please ensure that you have added an exception for our Javascript interface in your ProGuard configuration and that you do not have a faulty proxy enabled on your device.");
            }
            if (!z && (messageLevel == MessageLevel.WARNING || z2)) {
                AdColonyInterstitial adColonyInterstitial = null;
                if (m0.this.e != null) {
                    adColonyInterstitial = (AdColonyInterstitial) a.c().b().a().get(m0.this.e);
                }
                if (adColonyInterstitial == null) {
                    str = "unknown";
                } else {
                    str = adColonyInterstitial.a();
                }
                new a().a("onConsoleMessage: ").a(consoleMessage.message()).a(" with ad id: ").a(str).a(z2 ? u.j : u.h);
            }
            return true;
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            new a().a("JS Alert: ").a(str2).a(u.f);
            jsResult.confirm();
            return true;
        }
    }

    class d extends l {
        d() {
            super(m0.this, null);
        }

        @RequiresApi(api = 23)
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            m0.this.a(webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
        }

        @RequiresApi(api = 23)
        public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
            if (webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(m0.this.f.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new a().a("UTF-8 not supported.").a(u.j);
                }
            }
            return null;
        }

        @RequiresApi(api = 24)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            if (!m0.this.A || !webResourceRequest.isForMainFrame()) {
                return false;
            }
            Uri url = webResourceRequest.getUrl();
            k0.a(new Intent("android.intent.action.VIEW", url));
            JSONObject b2 = s.b();
            s.a(b2, "url", url.toString());
            s.a(b2, "ad_session_id", m0.this.e);
            new x("WebView.redirect_detected", m0.this.J.k(), b2).d();
            i0 u = a.c().u();
            u.a(m0.this.e);
            u.b(m0.this.e);
            return true;
        }
    }

    class e extends l {
        e() {
            super(m0.this, null);
        }

        @RequiresApi(api = 21)
        public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
            if (webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(m0.this.f.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new a().a("UTF-8 not supported.").a(u.j);
                }
            }
            return null;
        }
    }

    class f {
        f() {
        }

        @JavascriptInterface
        public void dispatch_messages(String str) {
            m0.this.b(str);
        }

        @JavascriptInterface
        public void enable_reverse_messaging() {
            m0.this.C = true;
        }

        @JavascriptInterface
        public String pull_messages() {
            String str = "[]";
            synchronized (m0.this.M) {
                if (m0.this.G.length() > 0) {
                    if (m0.this.x) {
                        str = m0.this.G.toString();
                    }
                    m0.this.G = s.a();
                }
            }
            return str;
        }

        @JavascriptInterface
        public void push_messages(String str) {
            m0.this.b(str);
        }
    }

    class g implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                m0.this.b(this.a);
            }
        }

        g() {
        }

        public void a(x xVar) {
            if (m0.this.c(xVar)) {
                k0.a((Runnable) new a(xVar));
            }
        }
    }

    class h implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                m0.this.a(this.a);
            }
        }

        h() {
        }

        public void a(x xVar) {
            if (m0.this.c(xVar)) {
                k0.a((Runnable) new a(xVar));
            }
        }
    }

    class i implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                m0.this.a(s.h(this.a.b(), "custom_js"));
            }
        }

        i() {
        }

        public void a(x xVar) {
            if (m0.this.c(xVar)) {
                k0.a((Runnable) new a(xVar));
            }
        }
    }

    class j implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                m0.this.b(s.d(this.a.b(), "transparent"));
            }
        }

        j() {
        }

        public void a(x xVar) {
            if (m0.this.c(xVar)) {
                k0.a((Runnable) new a(xVar));
            }
        }
    }

    class k implements OnClickListener {
        k() {
        }

        public void onClick(View view) {
            k0.a(new Intent("android.intent.action.VIEW", Uri.parse(m0.this.i)));
            a.c().u().b(m0.this.e);
        }
    }

    private class l extends WebViewClient {
        private l() {
        }

        public void onLoadResource(WebView webView, String str) {
            if (str.equals(m0.this.a)) {
                m0.this.a("if (typeof(CN) != 'undefined' && CN.div) {\n  if (typeof(cn_dispatch_on_touch_begin) != 'undefined') CN.div.removeEventListener('mousedown',  cn_dispatch_on_touch_begin, true);\n  if (typeof(cn_dispatch_on_touch_end) != 'undefined')   CN.div.removeEventListener('mouseup',  cn_dispatch_on_touch_end, true);\n  if (typeof(cn_dispatch_on_touch_move) != 'undefined')  CN.div.removeEventListener('mousemove',  cn_dispatch_on_touch_move, true);\n}\n");
            }
        }

        public void onPageFinished(WebView webView, String str) {
            JSONObject b = s.b();
            s.b(b, "id", m0.this.k);
            s.a(b, "url", str);
            new a().a("onPageFinished called with URL = ").a(str).a(u.d);
            if (m0.this.J == null) {
                new x("WebView.on_load", m0.this.t, b).d();
            } else {
                s.a(b, "ad_session_id", m0.this.e);
                s.b(b, "container_id", m0.this.J.c());
                new x("WebView.on_load", m0.this.J.k(), b).d();
            }
            if ((m0.this.x || m0.this.y) && !m0.this.A) {
                int h = m0.this.u > 0 ? m0.this.u : m0.this.t;
                if (m0.this.u > 0) {
                    float n = a.c().h().n();
                    s.b(m0.this.H, "app_orientation", k0.g(k0.g()));
                    s.b(m0.this.H, AvidJSONUtil.KEY_X, k0.a((View) m0.this));
                    s.b(m0.this.H, AvidJSONUtil.KEY_Y, k0.b((View) m0.this));
                    s.b(m0.this.H, "width", (int) (((float) m0.this.p) / n));
                    s.b(m0.this.H, "height", (int) (((float) m0.this.r) / n));
                    s.a(m0.this.H, "ad_session_id", m0.this.e);
                }
                m0 m0Var = m0.this;
                StringBuilder sb = new StringBuilder();
                sb.append("ADC3_init(");
                sb.append(h);
                sb.append(",");
                sb.append(m0.this.H.toString());
                sb.append(");");
                m0Var.a(sb.toString());
                m0.this.A = true;
            }
            if (!m0.this.y) {
                return;
            }
            if (m0.this.t != 1 || m0.this.u > 0) {
                JSONObject b2 = s.b();
                s.b(b2, "success", true);
                s.b(b2, "id", m0.this.t);
                m0.this.K.a(b2).d();
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            m0.this.A = false;
            new a().a("onPageStarted with URL = ").a(str).a(u.f);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            if (VERSION.SDK_INT < 23) {
                m0.this.a(i, str, str2);
            }
        }

        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            if (VERSION.SDK_INT < 26) {
                return super.onRenderProcessGone(webView, renderProcessGoneDetail);
            }
            if (renderProcessGoneDetail.didCrash()) {
                m0.this.a(s.b(), "An error occurred while rendering the ad. Ad closing.");
            }
            return true;
        }

        @TargetApi(11)
        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            if (VERSION.SDK_INT < 21 && str.endsWith("mraid.js")) {
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(m0.this.f.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new a().a("UTF-8 not supported.").a(u.j);
                }
            }
            return null;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!m0.this.A) {
                return false;
            }
            k0.a(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            i0 u = a.c().u();
            u.a(m0.this.e);
            u.b(m0.this.e);
            JSONObject b = s.b();
            s.a(b, "url", str);
            s.a(b, "ad_session_id", m0.this.e);
            new x("WebView.redirect_detected", m0.this.J.k(), b).d();
            return true;
        }

        /* synthetic */ l(m0 m0Var, c cVar) {
            this();
        }
    }

    private m0(Context context) {
        super(context);
    }

    private void w() {
        Context b2 = a.b();
        if (b2 != null && this.J != null && !this.E) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(-1);
            gradientDrawable.setShape(1);
            ImageView imageView = new ImageView(b2);
            this.L = imageView;
            imageView.setImageURI(Uri.fromFile(new File(this.h)));
            this.L.setBackground(gradientDrawable);
            this.L.setOnClickListener(new k());
            u();
            addView(this.L);
        }
    }

    public void b() {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        AdColonyAdView adColonyAdView;
        if (motionEvent.getAction() == 1) {
            if (this.e == null) {
                adColonyAdView = null;
            } else {
                adColonyAdView = (AdColonyAdView) a.c().b().b().get(this.e);
            }
            if (adColonyAdView != null && !adColonyAdView.getUserInteraction()) {
                JSONObject b2 = s.b();
                s.a(b2, "ad_session_id", this.e);
                new x("WebView.on_first_click", 1, b2).d();
                adColonyAdView.setUserInteraction(true);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: 0000 */
    public boolean r() {
        return this.B;
    }

    /* access modifiers changed from: 0000 */
    public boolean s() {
        return this.z;
    }

    /* access modifiers changed from: 0000 */
    public boolean t() {
        return this.D;
    }

    /* access modifiers changed from: 0000 */
    public void u() {
        if (this.L != null) {
            int s2 = a.c().h().s();
            int r2 = a.c().h().r();
            if (this.F) {
                s2 = this.l + this.p;
            }
            if (this.F) {
                r2 = this.n + this.r;
            }
            float n2 = a.c().h().n();
            int i2 = (int) (((float) this.v) * n2);
            int i3 = (int) (((float) this.w) * n2);
            this.L.setLayoutParams(new LayoutParams(i2, i3, s2 - i2, r2 - i3));
        }
    }

    /* access modifiers changed from: 0000 */
    public void v() {
        k0.a((Runnable) new a());
    }

    public int d() {
        return this.t;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        ImageView imageView = this.L;
        if (imageView != null) {
            this.J.a((View) imageView);
        }
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        this.J.i().add(a.a("WebView.set_visible", (z) new g(), true));
        this.J.i().add(a.a("WebView.set_bounds", (z) new h(), true));
        this.J.i().add(a.a("WebView.execute_js", (z) new i(), true));
        this.J.i().add(a.a("WebView.set_transparent", (z) new j(), true));
        this.J.j().add("WebView.set_visible");
        this.J.j().add("WebView.set_bounds");
        this.J.j().add("WebView.execute_js");
        this.J.j().add("WebView.set_transparent");
    }

    /* access modifiers changed from: 0000 */
    public void g() {
        k0.a((Runnable) new b());
    }

    /* access modifiers changed from: 0000 */
    public int h() {
        return this.r;
    }

    /* access modifiers changed from: 0000 */
    public int i() {
        return this.p;
    }

    /* access modifiers changed from: 0000 */
    public int j() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public int k() {
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    public int l() {
        return this.s;
    }

    /* access modifiers changed from: 0000 */
    public int m() {
        return this.q;
    }

    /* access modifiers changed from: 0000 */
    public int n() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public int o() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    public void p() {
        a(false, (x) null);
    }

    /* access modifiers changed from: 0000 */
    public void q() {
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.p, this.r);
        layoutParams.setMargins(this.l, this.n, 0, 0);
        layoutParams.gravity = 0;
        this.J.addView(this, layoutParams);
        if (!this.h.equals("") && !this.i.equals("")) {
            w();
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        JSONArray a2 = s.a(str);
        for (int i2 = 0; i2 < a2.length(); i2++) {
            a.c().m().a(s.c(a2, i2));
        }
    }

    public int c() {
        return this.u;
    }

    /* access modifiers changed from: 0000 */
    public boolean c(x xVar) {
        JSONObject b2 = xVar.b();
        return s.f(b2, "id") == this.k && s.f(b2, "container_id") == this.J.c() && s.h(b2, "ad_session_id").equals(this.J.a());
    }

    private void b(Exception exc) {
        new a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(s.h(this.H, TtmlNode.TAG_METADATA)).a(u.j);
        JSONObject b2 = s.b();
        s.a(b2, "id", this.e);
        new x("AdSession.on_error", this.J.k(), b2).d();
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        if (this.B) {
            new a().a("Ignoring call to execute_js as WebView has been destroyed.").a(u.d);
            return;
        }
        if (VERSION.SDK_INT >= 19) {
            evaluateJavascript(str, null);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("javascript:");
            sb.append(str);
            loadUrl(sb.toString());
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        setBackgroundColor(z2 ? 0 : -1);
    }

    /* access modifiers changed from: 0000 */
    public void b(x xVar) {
        if (s.d(xVar.b(), String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
        if (this.y) {
            JSONObject b2 = s.b();
            s.b(b2, "success", true);
            s.b(b2, "id", this.t);
            xVar.a(b2).d();
        }
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"AddJavascriptInterface"})
    public void a(boolean z2, x xVar) {
        String str;
        String str2;
        if (this.K == null) {
            this.K = xVar;
        }
        JSONObject b2 = this.K.b();
        this.y = z2;
        this.z = s.d(b2, "is_display_module");
        if (z2) {
            String h2 = s.h(b2, "filepath");
            this.j = s.h(b2, "interstitial_html");
            this.g = s.h(b2, "mraid_filepath");
            this.d = s.h(b2, "base_url");
            this.b = h2;
            this.I = s.g(b2, "iab");
            if (N && this.t == 1) {
                this.b = "android_asset/ADCController.js";
            }
            if (this.j.equals("")) {
                StringBuilder sb = new StringBuilder();
                sb.append("file:///");
                sb.append(this.b);
                str2 = sb.toString();
            } else {
                str2 = "";
            }
            this.a = str2;
            this.H = s.g(b2, String.VIDEO_INFO);
            this.e = s.h(b2, "ad_session_id");
            this.x = true;
        }
        setFocusable(true);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        if (VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        setWebChromeClient(new c(b2));
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setGeolocationEnabled(true);
        settings.setUseWideViewPort(true);
        if (VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        if (VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
            settings.setAllowFileAccess(true);
        }
        int i2 = VERSION.SDK_INT;
        WebViewClient webViewClient = i2 >= 23 ? new d() : i2 >= 21 ? new e() : new l(this, null);
        addJavascriptInterface(new f(), "NativeLayer");
        setWebViewClient(webViewClient);
        if (this.z) {
            try {
                if (this.j.equals("")) {
                    FileInputStream fileInputStream = new FileInputStream(this.b);
                    StringBuilder sb2 = new StringBuilder(fileInputStream.available());
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr, 0, 1024);
                        if (read < 0) {
                            break;
                        }
                        sb2.append(new String(bArr, 0, read));
                    }
                    if (this.b.contains(".html")) {
                        str = sb2.toString();
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("<html><script>");
                        sb3.append(sb2.toString());
                        sb3.append("</script></html>");
                        str = sb3.toString();
                    }
                } else {
                    String str3 = "script\\s*src\\s*=\\s*\"mraid.js\"";
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("script src=\"file://");
                    sb4.append(this.g);
                    sb4.append("\"");
                    str = this.j.replaceFirst(str3, sb4.toString());
                }
                String h3 = s.h(s.g(b2, String.VIDEO_INFO), TtmlNode.TAG_METADATA);
                String a2 = a(str, s.h(s.b(h3), "iab_filepath"));
                String str4 = "var\\s*ADC_DEVICE_INFO\\s*=\\s*null\\s*;";
                StringBuilder sb5 = new StringBuilder();
                sb5.append("var ADC_DEVICE_INFO = ");
                sb5.append(h3);
                sb5.append(";");
                loadDataWithBaseURL(this.a.equals("") ? this.d : this.a, a2.replaceFirst(str4, Matcher.quoteReplacement(sb5.toString())), WebRequest.CONTENT_TYPE_HTML, null, null);
            } catch (IOException e2) {
                a((Exception) e2);
                return;
            } catch (IllegalArgumentException e3) {
                a((Exception) e3);
                return;
            } catch (IndexOutOfBoundsException e4) {
                a((Exception) e4);
                return;
            }
        } else if (!this.a.startsWith("http") && !this.a.startsWith(ParametersKeys.FILE)) {
            loadDataWithBaseURL(this.d, this.a, WebRequest.CONTENT_TYPE_HTML, null, null);
        } else if (this.a.contains(".html") || !this.a.startsWith(ParametersKeys.FILE)) {
            loadUrl(this.a);
        } else {
            String str5 = this.a;
            StringBuilder sb6 = new StringBuilder();
            sb6.append("<html><script src=\"");
            sb6.append(this.a);
            sb6.append("\"></script></html>");
            loadDataWithBaseURL(str5, sb6.toString(), WebRequest.CONTENT_TYPE_HTML, null, null);
        }
        if (!z2) {
            f();
            q();
        }
        if (z2 || this.x) {
            a.c().m().a((a0) this);
        }
        if (!this.c.equals("")) {
            a(this.c);
        }
    }

    m0(Context context, x xVar, int i2, int i3, c cVar) {
        super(context);
        this.K = xVar;
        a(xVar, i2, i3, cVar);
        p();
    }

    m0(Context context, int i2, boolean z2) {
        super(context);
        this.t = i2;
        this.z = z2;
    }

    private String a(String str, String str2) {
        c0 c0Var;
        d b2 = a.c().b();
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) b2.a().get(this.e);
        AdColonyAdViewListener adColonyAdViewListener = (AdColonyAdViewListener) b2.c().get(this.e);
        if (adColonyInterstitial != null && this.I.length() > 0 && !s.h(this.I, AppEventsConstants.EVENT_PARAM_AD_TYPE).equals("video")) {
            adColonyInterstitial.a(this.I);
        } else if (adColonyAdViewListener != null && this.I.length() > 0) {
            adColonyAdViewListener.a(new c0(this.I, this.e));
        }
        if (adColonyInterstitial == null) {
            c0Var = null;
        } else {
            c0Var = adColonyInterstitial.e();
        }
        if (c0Var == null && adColonyAdViewListener != null) {
            c0Var = adColonyAdViewListener.b();
        }
        if (c0Var != null && c0Var.d() == 2) {
            this.D = true;
            if (!str2.equals("")) {
                try {
                    return ScriptInjector.injectScriptContentIntoHtml(a.c().k().a(str2, false).toString(), str);
                } catch (IOException e2) {
                    a((Exception) e2);
                }
            }
        }
        return str;
    }

    private boolean a(Exception exc) {
        new a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(s.h(this.H, TtmlNode.TAG_METADATA)).a(u.j);
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) a.c().b().a().remove(s.h(this.H, "ad_session_id"));
        if (adColonyInterstitial == null) {
            return false;
        }
        AdColonyInterstitialListener listener = adColonyInterstitial.getListener();
        if (listener == null) {
            return false;
        }
        listener.onExpiring(adColonyInterstitial);
        adColonyInterstitial.a(true);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a(x xVar, int i2, c cVar) {
        a(xVar, i2, -1, cVar);
        q();
    }

    /* access modifiers changed from: 0000 */
    public void a(x xVar, int i2, int i3, c cVar) {
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "url");
        this.a = h2;
        if (h2.equals("")) {
            this.a = s.h(b2, "data");
        }
        this.d = s.h(b2, "base_url");
        this.c = s.h(b2, "custom_js");
        this.e = s.h(b2, "ad_session_id");
        this.H = s.g(b2, String.VIDEO_INFO);
        this.g = s.h(b2, "mraid_filepath");
        this.u = s.d(b2, "use_mraid_module") ? a.c().m().d() : this.u;
        this.h = s.h(b2, "ad_choices_filepath");
        this.i = s.h(b2, "ad_choices_url");
        this.E = s.d(b2, "disable_ad_choices");
        this.F = s.d(b2, "ad_choices_snap_to_webview");
        this.v = s.f(b2, "ad_choices_width");
        this.w = s.f(b2, "ad_choices_height");
        if (this.I.length() == 0) {
            this.I = s.g(b2, "iab");
        }
        boolean z2 = false;
        if (!this.z && !this.g.equals("")) {
            if (this.u > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("script src=\"file://");
                sb.append(this.g);
                sb.append("\"");
                this.a = a(this.a.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", sb.toString()), s.h(s.g(this.H, DeviceRequestsHelper.DEVICE_INFO_PARAM), "iab_filepath"));
            } else {
                try {
                    this.f = a.c().k().a(this.g, false).toString();
                    String str = "bridge.os_name\\s*=\\s*\"\"\\s*;";
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("bridge.os_name = \"\";\nvar ADC_DEVICE_INFO = ");
                    sb2.append(this.H.toString());
                    sb2.append(";\n");
                    this.f = this.f.replaceFirst(str, sb2.toString());
                } catch (IOException e2) {
                    b((Exception) e2);
                } catch (IllegalArgumentException e3) {
                    b((Exception) e3);
                } catch (IndexOutOfBoundsException e4) {
                    b((Exception) e4);
                }
            }
        }
        this.k = i2;
        this.J = cVar;
        if (i3 >= 0) {
            this.t = i3;
        } else {
            f();
        }
        this.p = s.f(b2, "width");
        this.r = s.f(b2, "height");
        this.l = s.f(b2, AvidJSONUtil.KEY_X);
        int f2 = s.f(b2, AvidJSONUtil.KEY_Y);
        this.n = f2;
        this.q = this.p;
        this.s = this.r;
        this.o = f2;
        this.m = this.l;
        if (s.d(b2, "enable_messages") || this.y) {
            z2 = true;
        }
        this.x = z2;
        g();
    }

    /* access modifiers changed from: 0000 */
    public void a(x xVar) {
        JSONObject b2 = xVar.b();
        this.l = s.f(b2, AvidJSONUtil.KEY_X);
        this.n = s.f(b2, AvidJSONUtil.KEY_Y);
        this.p = s.f(b2, "width");
        this.r = s.f(b2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.l, this.n, 0, 0);
        layoutParams.width = this.p;
        layoutParams.height = this.r;
        setLayoutParams(layoutParams);
        if (this.y) {
            JSONObject b3 = s.b();
            s.b(b3, "success", true);
            s.b(b3, "id", this.t);
            xVar.a(b3).d();
        }
        u();
    }

    public void a(JSONObject jSONObject) {
        synchronized (this.M) {
            this.G.put(jSONObject);
        }
    }

    public void a() {
        if (a.d() && this.A && !this.C) {
            v();
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject, String str) {
        Context b2 = a.b();
        if (b2 != null && (b2 instanceof b)) {
            x xVar = new x("AdSession.finish_fullscreen_ad", 0);
            s.b(jSONObject, "status", 1);
            new a().a(str).a(u.i);
            ((b) b2).a(xVar);
        } else if (this.t == 1) {
            new a().a("Unable to communicate with controller, disabling AdColony.").a(u.i);
            AdColony.disable();
        } else if (this.u > 0) {
            this.x = false;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str, String str2) {
        if (this.J != null) {
            JSONObject b2 = s.b();
            s.b(b2, "id", this.k);
            s.a(b2, "ad_session_id", this.e);
            s.b(b2, "container_id", this.J.c());
            s.b(b2, "code", i2);
            s.a(b2, "error", str);
            s.a(b2, "url", str2);
            new x("WebView.on_error", this.J.k(), b2).d();
        }
        new a().a("onReceivedError: ").a(str).a(u.j);
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        this.B = z2;
    }
}
