package com.adcolony.sdk;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import org.json.JSONObject;

public class AdColonyInterstitial {
    public static final int ADCOLONY_IAP_ENGAGEMENT_END_CARD = 0;
    public static final int ADCOLONY_IAP_ENGAGEMENT_OVERLAY = 1;
    private AdColonyInterstitialListener a;
    private c b;
    private AdColonyAdOptions c;
    private c0 d;
    private int e;
    private String f;
    private String g;
    private String h;
    private int i;
    private String j;
    private boolean k;
    private boolean l;
    private boolean m;

    AdColonyInterstitial(String str, AdColonyInterstitialListener adColonyInterstitialListener, String str2) {
        this.a = adColonyInterstitialListener;
        this.j = str2;
        this.f = str;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(AdColonyZone adColonyZone) {
        if (adColonyZone != null) {
            if (adColonyZone.getPlayFrequency() <= 1) {
                return false;
            }
            if (adColonyZone.a() == 0) {
                adColonyZone.a(adColonyZone.getPlayFrequency() - 1);
                return false;
            }
            adColonyZone.a(adColonyZone.a() - 1);
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        this.e = i2;
    }

    /* access modifiers changed from: 0000 */
    public c c() {
        return this.b;
    }

    public boolean cancel() {
        if (this.b != null) {
            Context b2 = a.b();
            if (b2 == null || (b2 instanceof AdColonyInterstitialActivity)) {
                JSONObject b3 = s.b();
                s.a(b3, "id", this.b.a());
                new x("AdSession.on_request_close", this.b.k(), b3).d();
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        String str = this.h;
        return str == null ? "" : str;
    }

    public boolean destroy() {
        a.c().b().a().remove(this.f);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public c0 e() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public int f() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public boolean g() {
        return this.d != null;
    }

    public AdColonyInterstitialListener getListener() {
        return this.a;
    }

    public String getZoneID() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public boolean h() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public boolean i() {
        Context b2 = a.b();
        if (b2 == null || !a.e()) {
            return false;
        }
        a.c().d(true);
        a.c().a(this.b);
        a.c().a(this);
        new a().a("Launching fullscreen Activity via AdColonyInterstitial's launch ").a("method.").a(u.d);
        Intent intent = new Intent(b2, AdColonyInterstitialActivity.class);
        if (b2 instanceof Application) {
            intent.addFlags(268435456);
        }
        b2.startActivity(intent);
        this.l = true;
        return true;
    }

    public boolean isExpired() {
        return this.k || this.l;
    }

    public void setListener(@NonNull AdColonyInterstitialListener adColonyInterstitialListener) {
        this.a = adColonyInterstitialListener;
    }

    public boolean show() {
        if (!a.e()) {
            return false;
        }
        h c2 = a.c();
        if (this.l) {
            new a().a("This ad object has already been shown. Please request a new ad ").a("via AdColony.requestInterstitial.").a(u.g);
            return false;
        } else if (this.k) {
            new a().a("This ad object has expired. Please request a new ad via AdColony").a(".requestInterstitial.").a(u.g);
            return false;
        } else if (c2.B()) {
            new a().a("Can not show ad while an interstitial is already active.").a(u.g);
            return false;
        } else if (a((AdColonyZone) c2.w().get(this.j))) {
            new a().a("Skipping show()").a(u.f);
            return false;
        } else {
            JSONObject b2 = s.b();
            s.a(b2, "zone_id", this.j);
            s.b(b2, "type", 0);
            s.a(b2, "id", this.f);
            AdColonyAdOptions adColonyAdOptions = this.c;
            if (adColonyAdOptions != null) {
                s.b(b2, "pre_popup", adColonyAdOptions.a);
                s.b(b2, "post_popup", this.c.b);
            }
            AdColonyZone adColonyZone = (AdColonyZone) c2.w().get(this.j);
            if (adColonyZone != null && adColonyZone.isRewarded() && c2.r() == null) {
                new a().a("Rewarded ad: show() called with no reward listener set.").a(u.g);
            }
            new x("AdSession.launch_ad_unit", 1, b2).d();
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.h = str;
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        if (jSONObject.length() > 0) {
            this.d = new c0(jSONObject, this.f);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        this.b = cVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.k = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyAdOptions adColonyAdOptions) {
        this.c = adColonyAdOptions;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        this.i = i2;
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        String str = this.g;
        return str == null ? "" : str;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.g = str;
    }
}
