package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

class b0 {
    b0() {
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        if (c()) {
            return "wifi";
        }
        return b() ? "cell" : "none";
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"MissingPermission"})
    public boolean b() {
        NetworkInfo networkInfo;
        Context b = a.b();
        boolean z = false;
        if (b == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) b.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null) {
                networkInfo = null;
            } else {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo == null) {
                return false;
            }
            int type = networkInfo.getType();
            if (type == 0 || type >= 2) {
                z = true;
            }
            return z;
        } catch (SecurityException e) {
            new a().a("SecurityException - please ensure you added the ").a("ACCESS_NETWORK_STATE permission: ").a(e.toString()).a(u.i);
            return false;
        } catch (Exception e2) {
            new a().a("Exception occurred when retrieving activeNetworkInfo in ").a("ADCNetwork.using_mobile(): ").a(e2.toString()).a(u.j);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"MissingPermission"})
    public boolean c() {
        NetworkInfo networkInfo;
        Context b = a.b();
        boolean z = false;
        if (b == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) b.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null) {
                networkInfo = null;
            } else {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo == null) {
                return false;
            }
            if (networkInfo.getType() == 1) {
                z = true;
            }
            return z;
        } catch (SecurityException e) {
            new a().a("SecurityException - please ensure you added the ").a("ACCESS_NETWORK_STATE permission: ").a(e.toString()).a(u.i);
            return false;
        } catch (Exception e2) {
            new a().a("Exception occurred when retrieving activeNetworkInfo in ").a("ADCNetwork.using_wifi(): ").a(e2.toString()).a(u.j);
            return false;
        }
    }
}
