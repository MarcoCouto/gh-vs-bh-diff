package com.adcolony.sdk;

import android.support.annotation.NonNull;
import org.json.JSONObject;

public class AdColonyAdOptions {
    boolean a;
    boolean b;
    AdColonyUserMetadata c;
    JSONObject d = s.b();

    public AdColonyAdOptions enableConfirmationDialog(boolean z) {
        this.a = z;
        s.b(this.d, "confirmation_enabled", true);
        return this;
    }

    public AdColonyAdOptions enableResultsDialog(boolean z) {
        this.b = z;
        s.b(this.d, "results_enabled", true);
        return this;
    }

    public Object getOption(@NonNull String str) {
        return s.b(this.d, str);
    }

    public AdColonyUserMetadata getUserMetadata() {
        return this.c;
    }

    public AdColonyAdOptions setOption(@NonNull String str, boolean z) {
        if (k0.h(str)) {
            s.b(this.d, str, z);
        }
        return this;
    }

    public AdColonyAdOptions setUserMetadata(@NonNull AdColonyUserMetadata adColonyUserMetadata) {
        this.c = adColonyUserMetadata;
        s.a(this.d, "user_metadata", adColonyUserMetadata.b);
        return this;
    }

    public AdColonyAdOptions setOption(@NonNull String str, double d2) {
        if (k0.h(str)) {
            s.a(this.d, str, d2);
        }
        return this;
    }

    public AdColonyAdOptions setOption(@NonNull String str, @NonNull String str2) {
        if (str != null && k0.h(str) && k0.h(str2)) {
            s.a(this.d, str, str2);
        }
        return this;
    }
}
