package com.adcolony.sdk;

import android.os.Bundle;
import java.util.HashMap;

class d0 {
    private static int a = 0;
    private static HashMap<String, Integer> b = new HashMap<>();
    private static HashMap<String, Integer> c = new HashMap<>();
    static final int d = 5;
    static final int e = 1;
    static final int f = 3;
    static final int g = 0;
    static final int h = 1;

    d0() {
    }

    static boolean a(int i, Bundle bundle) {
        int currentTimeMillis = (int) (System.currentTimeMillis() / 1000);
        if (i != 0) {
            if (i != 1 || bundle == null) {
                return false;
            }
            String string = bundle.getString("zone_id");
            if (b.get(string) == null) {
                b.put(string, Integer.valueOf(currentTimeMillis));
            }
            if (c.get(string) == null) {
                c.put(string, Integer.valueOf(0));
            }
            if (currentTimeMillis - ((Integer) b.get(string)).intValue() > 1) {
                c.put(string, Integer.valueOf(1));
                b.put(string, Integer.valueOf(currentTimeMillis));
                return false;
            }
            int intValue = ((Integer) c.get(string)).intValue() + 1;
            c.put(string, Integer.valueOf(intValue));
            return intValue > 3;
        } else if (currentTimeMillis - a < 5) {
            return true;
        } else {
            a = currentTimeMillis;
            return false;
        }
    }
}
