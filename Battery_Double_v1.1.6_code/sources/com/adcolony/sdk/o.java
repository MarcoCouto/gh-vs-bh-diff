package com.adcolony.sdk;

import java.util.Date;
import org.json.JSONObject;

class o extends v {
    static final r i = new r("adcolony_fatal_reports", "4.1.4", "Production");
    static final String j = "sourceFile";
    static final String k = "lineNumber";
    static final String l = "methodName";
    static final String m = "stackTrace";
    static final String n = "isAdActive";
    static final String o = "activeAdId";
    static final String p = "active_creative_ad_id";
    static final String q = "listOfCachedAds";
    static final String r = "listOfCreativeAdIds";
    static final String s = "adCacheSize";
    /* access modifiers changed from: private */
    public JSONObject h;

    private class a extends a {
        a() {
        }

        /* access modifiers changed from: 0000 */
        public a a(JSONObject jSONObject) {
            ((o) this.a).h = jSONObject;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(Date date) {
            s.a(((o) this.a).h, "timestamp", v.e.format(date));
            return super.a(date);
        }
    }

    o() {
    }

    /* access modifiers changed from: 0000 */
    public JSONObject f() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public o a(JSONObject jSONObject) {
        a aVar = new a();
        aVar.a(jSONObject);
        aVar.a(s.h(jSONObject, "message"));
        try {
            aVar.a(new Date(Long.parseLong(s.h(jSONObject, "timestamp"))));
        } catch (NumberFormatException unused) {
        }
        aVar.a(i);
        aVar.a(-1);
        return (o) aVar.a();
    }
}
