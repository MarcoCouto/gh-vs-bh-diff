package com.adcolony.sdk;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import java.io.File;
import org.json.JSONObject;

public class AdColonyAdView extends FrameLayout {
    /* access modifiers changed from: private */
    public c a = ((c) a.c().b().d().get(this.d));
    private AdColonyAdViewListener b;
    private AdColonyAdSize c;
    /* access modifiers changed from: private */
    public String d;
    private String e;
    private String f;
    private ImageView g;
    private c0 h;
    private x i;
    private boolean j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private JSONObject t;

    class a implements Runnable {
        a() {
        }

        public void run() {
            Context b = a.b();
            if (b instanceof AdColonyAdViewActivity) {
                ((AdColonyAdViewActivity) b).b();
            }
            d b2 = a.c().b();
            b2.b().remove(AdColonyAdView.this.d);
            b2.a(AdColonyAdView.this.a);
            JSONObject b3 = s.b();
            s.a(b3, "id", AdColonyAdView.this.d);
            new x("AdSession.on_ad_view_destroyed", 1, b3).d();
        }
    }

    class b implements OnClickListener {
        final /* synthetic */ Context a;

        b(Context context) {
            this.a = context;
        }

        public void onClick(View view) {
            Context context = this.a;
            if (context instanceof AdColonyAdViewActivity) {
                ((AdColonyAdViewActivity) context).b();
            }
        }
    }

    AdColonyAdView(Context context, x xVar, AdColonyAdViewListener adColonyAdViewListener) {
        super(context);
        this.b = adColonyAdViewListener;
        this.e = adColonyAdViewListener.c();
        JSONObject b2 = xVar.b();
        this.t = b2;
        this.d = s.h(b2, "id");
        this.f = s.h(b2, "close_button_filepath");
        this.j = s.d(b2, "trusted_demand_source");
        this.n = s.d(b2, "close_button_snap_to_webview");
        this.r = s.f(b2, "close_button_width");
        this.s = s.f(b2, "close_button_height");
        this.c = adColonyAdViewListener.a();
        setLayoutParams(new LayoutParams(this.a.d(), this.a.b()));
        setBackgroundColor(0);
        addView(this.a);
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        if (this.h != null) {
            getWebView().g();
        }
    }

    public boolean destroy() {
        if (this.k) {
            new a().a("Ignoring duplicate call to destroy().").a(u.g);
            return false;
        }
        this.k = true;
        c0 c0Var = this.h;
        if (!(c0Var == null || c0Var.c() == null)) {
            this.h.b();
        }
        k0.a((Runnable) new a());
        return true;
    }

    /* access modifiers changed from: 0000 */
    public String getAdSessionId() {
        return this.d;
    }

    public AdColonyAdSize getAdSize() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public c getContainer() {
        return this.a;
    }

    public AdColonyAdViewListener getListener() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public c0 getOmidManager() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public int getOrientation() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    public boolean getTrustedDemandSource() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public boolean getUserInteraction() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public m0 getWebView() {
        c cVar = this.a;
        if (cVar == null) {
            return null;
        }
        return (m0) cVar.n().get(Integer.valueOf(2));
    }

    public String getZoneId() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public void setExpandMessage(x xVar) {
        this.i = xVar;
    }

    /* access modifiers changed from: 0000 */
    public void setExpandedHeight(int i2) {
        this.q = (int) (((float) i2) * a.c().h().n());
    }

    /* access modifiers changed from: 0000 */
    public void setExpandedWidth(int i2) {
        this.p = (int) (((float) i2) * a.c().h().n());
    }

    public void setListener(AdColonyAdViewListener adColonyAdViewListener) {
        this.b = adColonyAdViewListener;
    }

    /* access modifiers changed from: 0000 */
    public void setNoCloseButton(boolean z) {
        this.l = this.j && z;
    }

    /* access modifiers changed from: 0000 */
    public void setOmidManager(c0 c0Var) {
        this.h = c0Var;
    }

    /* access modifiers changed from: 0000 */
    public void setOrientation(int i2) {
        this.o = i2;
    }

    /* access modifiers changed from: 0000 */
    public void setUserInteraction(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.j || this.m) {
            float n2 = a.c().h().n();
            this.a.setLayoutParams(new LayoutParams((int) (((float) this.c.getWidth()) * n2), (int) (((float) this.c.getHeight()) * n2)));
            m0 webView = getWebView();
            if (webView != null) {
                x xVar = new x("WebView.set_bounds", 0);
                JSONObject b2 = s.b();
                s.b(b2, AvidJSONUtil.KEY_X, webView.n());
                s.b(b2, AvidJSONUtil.KEY_Y, webView.o());
                s.b(b2, "width", webView.m());
                s.b(b2, "height", webView.l());
                xVar.b(b2);
                webView.a(xVar);
                JSONObject b3 = s.b();
                s.a(b3, "ad_session_id", this.d);
                new x("MRAID.on_close", this.a.k(), b3).d();
            }
            ImageView imageView = this.g;
            if (imageView != null) {
                this.a.removeView(imageView);
            }
            addView(this.a);
            AdColonyAdViewListener adColonyAdViewListener = this.b;
            if (adColonyAdViewListener != null) {
                adColonyAdViewListener.onClosed(this);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        if (this.j || this.m) {
            j h2 = a.c().h();
            int s2 = h2.s();
            int r2 = h2.r();
            int i2 = this.p;
            if (i2 <= 0) {
                i2 = s2;
            }
            int i3 = this.q;
            if (i3 <= 0) {
                i3 = r2;
            }
            int i4 = (s2 - i2) / 2;
            int i5 = (r2 - i3) / 2;
            this.a.setLayoutParams(new LayoutParams(s2, r2));
            m0 webView = getWebView();
            if (webView != null) {
                x xVar = new x("WebView.set_bounds", 0);
                JSONObject b2 = s.b();
                s.b(b2, AvidJSONUtil.KEY_X, i4);
                s.b(b2, AvidJSONUtil.KEY_Y, i5);
                s.b(b2, "width", i2);
                s.b(b2, "height", i3);
                xVar.b(b2);
                webView.a(xVar);
                float n2 = h2.n();
                JSONObject b3 = s.b();
                s.b(b3, "app_orientation", k0.g(k0.g()));
                s.b(b3, "width", (int) (((float) i2) / n2));
                s.b(b3, "height", (int) (((float) i3) / n2));
                s.b(b3, AvidJSONUtil.KEY_X, k0.a((View) webView));
                s.b(b3, AvidJSONUtil.KEY_Y, k0.b((View) webView));
                s.a(b3, "ad_session_id", this.d);
                new x("MRAID.on_size_change", this.a.k(), b3).d();
            }
            ImageView imageView = this.g;
            if (imageView != null) {
                this.a.removeView(imageView);
            }
            Context b4 = a.b();
            if (!(b4 == null || this.l || webView == null)) {
                float n3 = a.c().h().n();
                int i6 = (int) (((float) this.r) * n3);
                int i7 = (int) (((float) this.s) * n3);
                if (this.n) {
                    s2 = webView.j() + webView.i();
                }
                int k2 = this.n ? webView.k() : 0;
                ImageView imageView2 = new ImageView(b4.getApplicationContext());
                this.g = imageView2;
                imageView2.setImageURI(Uri.fromFile(new File(this.f)));
                LayoutParams layoutParams = new LayoutParams(i6, i7);
                layoutParams.setMargins(s2 - i6, k2, 0, 0);
                this.g.setOnClickListener(new b(b4));
                this.a.addView(this.g, layoutParams);
            }
            if (this.i != null) {
                JSONObject b5 = s.b();
                s.b(b5, "success", true);
                this.i.a(b5).d();
                this.i = null;
            }
            return true;
        }
        if (this.i != null) {
            JSONObject b6 = s.b();
            s.b(b6, "success", false);
            this.i.a(b6).d();
            this.i = null;
        }
        return false;
    }
}
