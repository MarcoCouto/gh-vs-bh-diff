package com.adcolony.sdk;

class r {
    private String a;
    private String b;
    private String c;
    private String d = "%s_%s_%s";

    public r(String str, String str2, String str3) {
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    public String a() {
        return this.c;
    }

    public String b() {
        return String.format(this.d, new Object[]{c(), d(), a()});
    }

    public String c() {
        return this.a;
    }

    public String d() {
        return this.b;
    }
}
