package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Environment;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class i0 {

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            i0.this.a(xVar);
        }
    }

    class b implements z {
        b() {
        }

        public void a(x xVar) {
            i0.this.b(xVar);
        }
    }

    class c implements z {
        c() {
        }

        public void a(x xVar) {
            i0.this.k(xVar);
        }
    }

    class d implements z {
        d() {
        }

        public void a(x xVar) {
            i0.this.o(xVar);
        }
    }

    class e implements z {
        e() {
        }

        public void a(x xVar) {
            i0.this.n(xVar);
        }
    }

    class f implements z {
        f() {
        }

        public void a(x xVar) {
            i0.this.d(xVar);
        }
    }

    class g implements z {
        g() {
        }

        public void a(x xVar) {
            i0.this.q(xVar);
        }
    }

    class h implements z {
        h() {
        }

        public void a(x xVar) {
            i0.this.p(xVar);
        }
    }

    class i implements OnScanCompletedListener {
        final /* synthetic */ JSONObject a;
        final /* synthetic */ x b;

        i(JSONObject jSONObject, x xVar) {
            this.a = jSONObject;
            this.b = xVar;
        }

        public void onScanCompleted(String str, Uri uri) {
            k0.a("Screenshot saved to Gallery!", 0);
            s.b(this.a, "success", true);
            this.b.a(this.a).d();
        }
    }

    class j implements Runnable {
        final /* synthetic */ String a;

        j(String str) {
            this.a = str;
        }

        public void run() {
            JSONObject b2 = s.b();
            s.a(b2, "type", "open_hook");
            s.a(b2, "message", this.a);
            new x("CustomMessage.controller_send", 0, b2).d();
        }
    }

    class k implements z {
        k() {
        }

        public void a(x xVar) {
            i0.this.h(xVar);
        }
    }

    class l implements z {
        l() {
        }

        public void a(x xVar) {
            i0.this.i(xVar);
        }
    }

    class m implements z {
        m() {
        }

        public void a(x xVar) {
            i0.this.l(xVar);
        }
    }

    class n implements z {
        n() {
        }

        public void a(x xVar) {
            i0.this.j(xVar);
        }
    }

    class o implements z {
        o() {
        }

        public void a(x xVar) {
            i0.this.m(xVar);
        }
    }

    class p implements z {
        p() {
        }

        public void a(x xVar) {
            i0.this.g(xVar);
        }
    }

    class q implements z {
        q() {
        }

        public void a(x xVar) {
            i0.this.f(xVar);
        }
    }

    class r implements z {
        r() {
        }

        public void a(x xVar) {
            i0.this.e(xVar);
        }
    }

    class s implements z {
        s() {
        }

        public void a(x xVar) {
            i0.this.c(xVar);
        }
    }

    i0() {
    }

    /* access modifiers changed from: private */
    public boolean n(x xVar) {
        String h2 = s.h(xVar.b(), "ad_session_id");
        Activity activity = a.b() instanceof Activity ? (Activity) a.b() : null;
        boolean z = activity instanceof AdColonyAdViewActivity;
        if (!(activity instanceof b)) {
            return false;
        }
        if (z) {
            ((AdColonyAdViewActivity) activity).b();
        } else {
            JSONObject b2 = s.b();
            s.a(b2, "id", h2);
            new x("AdSession.on_request_close", ((b) activity).d, b2).d();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean o(x xVar) {
        JSONObject b2 = xVar.b();
        d b3 = a.c().b();
        String h2 = s.h(b2, "ad_session_id");
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) b3.a().get(h2);
        AdColonyAdView adColonyAdView = (AdColonyAdView) b3.b().get(h2);
        if ((adColonyInterstitial == null || adColonyInterstitial.getListener() == null || adColonyInterstitial.c() == null) && (adColonyAdView == null || adColonyAdView.getListener() == null)) {
            return false;
        }
        if (adColonyAdView == null) {
            new x("AdUnit.make_in_app_purchase", adColonyInterstitial.c().k()).d();
        }
        a(h2);
        c(h2);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean p(x xVar) {
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "ad_session_id");
        int f2 = s.f(b2, "orientation");
        d b3 = a.c().b();
        AdColonyAdView adColonyAdView = (AdColonyAdView) b3.b().get(h2);
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) b3.a().get(h2);
        Context b4 = a.b();
        if (adColonyAdView != null) {
            adColonyAdView.setOrientation(f2);
        } else if (adColonyInterstitial != null) {
            adColonyInterstitial.b(f2);
        }
        if (adColonyInterstitial == null && adColonyAdView == null) {
            new a().a("Invalid ad session id sent with set orientation properties message: ").a(h2).a(u.j);
            return false;
        }
        if (b4 instanceof b) {
            ((b) b4).a(adColonyAdView == null ? adColonyInterstitial.f() : adColonyAdView.getOrientation());
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean q(x xVar) {
        AdColonyAdView adColonyAdView = (AdColonyAdView) a.c().b().b().get(s.h(xVar.b(), "ad_session_id"));
        if (adColonyAdView == null) {
            return false;
        }
        adColonyAdView.setNoCloseButton(s.d(xVar.b(), "use_custom_close"));
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean e(x xVar) {
        JSONObject b2 = s.b();
        JSONObject b3 = xVar.b();
        String h2 = s.h(b3, "ad_session_id");
        if (s.d(b3, "deep_link")) {
            return h(xVar);
        }
        Context b4 = a.b();
        if (b4 == null) {
            return false;
        }
        if (k0.a(b4.getPackageManager().getLaunchIntentForPackage(s.h(b3, "handle")))) {
            s.b(b2, "success", true);
            xVar.a(b2).d();
            b(h2);
            a(h2);
            c(h2);
            return true;
        }
        k0.a("Failed to launch external application.", 0);
        s.b(b2, "success", false);
        xVar.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean f(x xVar) {
        JSONObject b2 = s.b();
        JSONObject b3 = xVar.b();
        JSONArray c2 = s.c(b3, "recipients");
        boolean d2 = s.d(b3, String.HTML);
        String h2 = s.h(b3, "subject");
        String h3 = s.h(b3, TtmlNode.TAG_BODY);
        String h4 = s.h(b3, "ad_session_id");
        String[] strArr = new String[c2.length()];
        for (int i2 = 0; i2 < c2.length(); i2++) {
            strArr[i2] = s.d(c2, i2);
        }
        Intent intent = new Intent("android.intent.action.SEND");
        if (!d2) {
            intent.setType("plain/text");
        }
        intent.putExtra("android.intent.extra.SUBJECT", h2).putExtra("android.intent.extra.TEXT", h3).putExtra("android.intent.extra.EMAIL", strArr);
        if (k0.a(intent)) {
            s.b(b2, "success", true);
            xVar.a(b2).d();
            b(h4);
            a(h4);
            c(h4);
            return true;
        }
        k0.a("Failed to send email.", 0);
        s.b(b2, "success", false);
        xVar.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean g(x xVar) {
        JSONObject b2 = s.b();
        JSONObject b3 = xVar.b();
        String h2 = s.h(b3, "url");
        String h3 = s.h(b3, "ad_session_id");
        AdColonyAdView adColonyAdView = (AdColonyAdView) a.c().b().b().get(h3);
        if (adColonyAdView != null && !adColonyAdView.getTrustedDemandSource() && !adColonyAdView.getUserInteraction()) {
            return false;
        }
        if (h2.startsWith("browser")) {
            h2 = h2.replaceFirst("browser", "http");
        }
        if (h2.startsWith("safari")) {
            h2 = h2.replaceFirst("safari", "http");
        }
        d(h2);
        if (k0.a(new Intent("android.intent.action.VIEW", Uri.parse(h2)))) {
            s.b(b2, "success", true);
            xVar.a(b2).d();
            b(h3);
            a(h3);
            c(h3);
            return true;
        }
        k0.a("Failed to launch browser.", 0);
        s.b(b2, "success", false);
        xVar.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean h(x xVar) {
        JSONObject b2 = s.b();
        JSONObject b3 = xVar.b();
        String h2 = s.h(b3, "product_id");
        String h3 = s.h(b3, "ad_session_id");
        if (h2.equals("")) {
            h2 = s.h(b3, "handle");
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(h2));
        d(h2);
        if (k0.a(intent)) {
            s.b(b2, "success", true);
            xVar.a(b2).d();
            b(h3);
            a(h3);
            c(h3);
            return true;
        }
        k0.a("Unable to open.", 0);
        s.b(b2, "success", false);
        xVar.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|9|10|11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        com.adcolony.sdk.k0.a("Error saving screenshot.", 0);
        com.adcolony.sdk.s.b(r2, "success", false);
        r11.a(r2).d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00e1, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e2, code lost:
        com.adcolony.sdk.k0.a("Error saving screenshot.", 0);
        com.adcolony.sdk.s.b(r2, "success", false);
        r11.a(r2).d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00f3, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x00ab */
    public boolean i(x xVar) {
        Context b2 = a.b();
        if (b2 != null && (b2 instanceof Activity)) {
            try {
                if (ActivityCompat.checkSelfPermission(b2, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
                    a(s.h(xVar.b(), "ad_session_id"));
                    JSONObject b3 = s.b();
                    StringBuilder sb = new StringBuilder();
                    sb.append(Environment.getExternalStorageDirectory().toString());
                    sb.append("/Pictures/AdColony_Screenshots/AdColony_Screenshot_");
                    sb.append(System.currentTimeMillis());
                    sb.append(".jpg");
                    String sb2 = sb.toString();
                    View rootView = ((Activity) b2).getWindow().getDecorView().getRootView();
                    rootView.setDrawingCacheEnabled(true);
                    Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                    rootView.setDrawingCacheEnabled(false);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(Environment.getExternalStorageDirectory().getPath());
                    sb3.append("/Pictures");
                    File file = new File(sb3.toString());
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(Environment.getExternalStorageDirectory().getPath());
                    sb4.append("/Pictures/AdColony_Screenshots");
                    File file2 = new File(sb4.toString());
                    file.mkdirs();
                    file2.mkdirs();
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(sb2));
                    createBitmap.compress(CompressFormat.JPEG, 90, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    MediaScannerConnection.scanFile(b2, new String[]{sb2}, null, new i(b3, xVar));
                    return true;
                }
                k0.a("Error saving screenshot.", 0);
                JSONObject b4 = xVar.b();
                s.b(b4, "success", false);
                xVar.a(b4).d();
                return false;
            } catch (NoClassDefFoundError unused) {
                k0.a("Error saving screenshot.", 0);
                JSONObject b5 = xVar.b();
                s.b(b5, "success", false);
                xVar.a(b5).d();
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean j(x xVar) {
        JSONObject b2 = xVar.b();
        JSONObject b3 = s.b();
        String h2 = s.h(b2, "ad_session_id");
        JSONArray c2 = s.c(b2, "recipients");
        String str = "";
        for (int i2 = 0; i2 < c2.length(); i2++) {
            if (i2 != 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(";");
                str = sb.toString();
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(s.d(c2, i2));
            str = sb2.toString();
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("smsto:");
        sb3.append(str);
        if (k0.a(new Intent("android.intent.action.VIEW", Uri.parse(sb3.toString())).putExtra("sms_body", s.h(b2, TtmlNode.TAG_BODY)))) {
            s.b(b3, "success", true);
            xVar.a(b3).d();
            b(h2);
            a(h2);
            c(h2);
            return true;
        }
        k0.a("Failed to create sms.", 0);
        s.b(b3, "success", false);
        xVar.a(b3).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean k(x xVar) {
        JSONObject b2 = s.b();
        JSONObject b3 = xVar.b();
        Intent type = new Intent("android.intent.action.SEND").setType(WebRequest.CONTENT_TYPE_PLAIN_TEXT);
        StringBuilder sb = new StringBuilder();
        sb.append(s.h(b3, MimeTypes.BASE_TYPE_TEXT));
        sb.append(" ");
        sb.append(s.h(b3, "url"));
        Intent putExtra = type.putExtra("android.intent.extra.TEXT", sb.toString());
        String h2 = s.h(b3, "ad_session_id");
        if (k0.a(putExtra, true)) {
            s.b(b2, "success", true);
            xVar.a(b2).d();
            b(h2);
            a(h2);
            c(h2);
            return true;
        }
        k0.a("Unable to create social post.", 0);
        s.b(b2, "success", false);
        xVar.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean l(x xVar) {
        JSONObject b2 = s.b();
        JSONObject b3 = xVar.b();
        Intent intent = new Intent("android.intent.action.DIAL");
        StringBuilder sb = new StringBuilder();
        sb.append("tel:");
        sb.append(s.h(b3, "phone_number"));
        Intent data = intent.setData(Uri.parse(sb.toString()));
        String h2 = s.h(b3, "ad_session_id");
        if (k0.a(data)) {
            s.b(b2, "success", true);
            xVar.a(b2).d();
            b(h2);
            a(h2);
            c(h2);
            return true;
        }
        k0.a("Failed to dial number.", 0);
        s.b(b2, "success", false);
        xVar.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean m(x xVar) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        int a2 = s.a(xVar.b(), "length_ms", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
        JSONObject b3 = s.b();
        JSONArray c2 = k0.c(b2);
        boolean z = false;
        for (int i2 = 0; i2 < c2.length(); i2++) {
            if (s.d(c2, i2).equals("android.permission.VIBRATE")) {
                z = true;
            }
        }
        if (!z) {
            new a().a("No vibrate permission detected.").a(u.g);
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return false;
        }
        try {
            ((Vibrator) b2.getSystemService("vibrator")).vibrate((long) a2);
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return true;
        } catch (Exception unused) {
            new a().a("Vibrate command failed.").a(u.g);
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        a.a("System.open_store", (z) new k());
        a.a("System.save_screenshot", (z) new l());
        a.a("System.telephone", (z) new m());
        a.a("System.sms", (z) new n());
        a.a("System.vibrate", (z) new o());
        a.a("System.open_browser", (z) new p());
        a.a("System.mail", (z) new q());
        a.a("System.launch_app", (z) new r());
        a.a("System.create_calendar_event", (z) new s());
        a.a("System.check_app_presence", (z) new a());
        a.a("System.check_social_presence", (z) new b());
        a.a("System.social_post", (z) new c());
        a.a("System.make_in_app_purchase", (z) new d());
        a.a("System.close", (z) new e());
        a.a("System.expand", (z) new f());
        a.a("System.use_custom_close", (z) new g());
        a.a("System.set_orientation_properties", (z) new h());
    }

    /* access modifiers changed from: 0000 */
    public boolean b(x xVar) {
        return a(xVar);
    }

    /* access modifiers changed from: 0000 */
    public boolean c(x xVar) {
        Intent intent;
        x xVar2 = xVar;
        JSONObject b2 = s.b();
        JSONObject b3 = xVar.b();
        String str = "";
        String str2 = "";
        String h2 = s.h(b3, "ad_session_id");
        JSONObject g2 = s.g(b3, "params");
        JSONObject g3 = s.g(g2, "recurrence");
        JSONArray a2 = s.a();
        JSONArray a3 = s.a();
        JSONArray a4 = s.a();
        String h3 = s.h(g2, "description");
        s.h(g2, "location");
        String h4 = s.h(g2, "start");
        String h5 = s.h(g2, TtmlNode.END);
        String h6 = s.h(g2, "summary");
        if (g3 != null && g3.length() > 0) {
            str2 = s.h(g3, "expires");
            str = s.h(g3, "frequency").toUpperCase(Locale.getDefault());
            a2 = s.c(g3, "daysInWeek");
            a3 = s.c(g3, "daysInMonth");
            a4 = s.c(g3, "daysInYear");
        }
        if (h6.equals("")) {
            h6 = h3;
        }
        Date k2 = k0.k(h4);
        Date k3 = k0.k(h5);
        Date k4 = k0.k(str2);
        if (k2 == null || k3 == null) {
            x xVar3 = xVar2;
            k0.a("Unable to create Calendar Event", 0);
            s.b(b2, "success", false);
            xVar3.a(b2).d();
            return false;
        }
        long time = k2.getTime();
        long time2 = k3.getTime();
        long j2 = 0;
        long time3 = k4 != null ? (k4.getTime() - k2.getTime()) / 1000 : 0;
        if (str.equals("DAILY")) {
            j2 = (time3 / 86400) + 1;
        } else if (str.equals("WEEKLY")) {
            j2 = (time3 / 604800) + 1;
        } else if (str.equals("MONTHLY")) {
            j2 = (time3 / 2629800) + 1;
        } else if (str.equals("YEARLY")) {
            j2 = (time3 / 31557600) + 1;
        }
        long j3 = j2;
        if (g3 == null || g3.length() <= 0) {
            intent = new Intent("android.intent.action.EDIT").setType("vnd.android.cursor.item/event").putExtra("title", h6).putExtra("description", h3).putExtra("beginTime", time).putExtra("endTime", time2);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("FREQ=");
            sb.append(str);
            sb.append(";COUNT=");
            sb.append(j3);
            String sb2 = sb.toString();
            try {
                if (a2.length() != 0) {
                    String a5 = k0.a(a2);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(sb2);
                    sb3.append(";BYDAY=");
                    sb3.append(a5);
                    sb2 = sb3.toString();
                }
                if (a3.length() != 0) {
                    String b4 = k0.b(a3);
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(sb2);
                    sb4.append(";BYMONTHDAY=");
                    sb4.append(b4);
                    sb2 = sb4.toString();
                }
                if (a4.length() != 0) {
                    String b5 = k0.b(a4);
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(sb2);
                    sb5.append(";BYYEARDAY=");
                    sb5.append(b5);
                    sb2 = sb5.toString();
                }
            } catch (JSONException unused) {
            }
            intent = new Intent("android.intent.action.EDIT").setType("vnd.android.cursor.item/event").putExtra("title", h6).putExtra("description", h3).putExtra("beginTime", time).putExtra("endTime", time2).putExtra("rrule", sb2);
        }
        if (k0.a(intent)) {
            s.b(b2, "success", true);
            xVar.a(b2).d();
            b(h2);
            a(h2);
            c(h2);
            return true;
        }
        x xVar4 = xVar;
        k0.a("Unable to create Calendar Event.", 0);
        s.b(b2, "success", false);
        xVar4.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean d(x xVar) {
        JSONObject b2 = xVar.b();
        Context b3 = a.b();
        if (b3 != null && a.e()) {
            String h2 = s.h(b2, "ad_session_id");
            h c2 = a.c();
            AdColonyAdView adColonyAdView = (AdColonyAdView) c2.b().b().get(h2);
            if (adColonyAdView != null && ((adColonyAdView.getTrustedDemandSource() || adColonyAdView.getUserInteraction()) && c2.e() != adColonyAdView)) {
                adColonyAdView.setExpandMessage(xVar);
                adColonyAdView.setExpandedWidth(s.f(b2, "width"));
                adColonyAdView.setExpandedHeight(s.f(b2, "height"));
                adColonyAdView.setOrientation(s.a(b2, "orientation", -1));
                adColonyAdView.setNoCloseButton(s.d(b2, "use_custom_close"));
                c2.a(adColonyAdView);
                c2.a(adColonyAdView.getContainer());
                Intent intent = new Intent(b3, AdColonyAdViewActivity.class);
                if (b3 instanceof Application) {
                    intent.addFlags(268435456);
                }
                c(h2);
                a(h2);
                b3.startActivity(intent);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        d b2 = a.c().b();
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) b2.a().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.getListener() == null) {
            AdColonyAdView adColonyAdView = (AdColonyAdView) b2.b().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (!(adColonyAdView == null || listener == null)) {
                listener.onLeftApplication(adColonyAdView);
            }
            return;
        }
        adColonyInterstitial.getListener().onLeftApplication(adColonyInterstitial);
    }

    private void d(String str) {
        k0.b.execute(new j(str));
    }

    /* access modifiers changed from: 0000 */
    public boolean a(x xVar) {
        JSONObject b2 = s.b();
        String h2 = s.h(xVar.b(), "name");
        boolean f2 = k0.f(h2);
        s.b(b2, "success", true);
        s.b(b2, IronSourceConstants.EVENTS_RESULT, f2);
        s.a(b2, "name", h2);
        s.a(b2, NotificationCompat.CATEGORY_SERVICE, h2);
        xVar.a(b2).d();
        return true;
    }

    private boolean c(@NonNull String str) {
        if (((AdColonyAdView) a.c().b().b().get(str)) == null) {
            return false;
        }
        JSONObject b2 = s.b();
        s.a(b2, "ad_session_id", str);
        new x("MRAID.on_event", 1, b2).d();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        d b2 = a.c().b();
        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) b2.a().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.getListener() == null) {
            AdColonyAdView adColonyAdView = (AdColonyAdView) b2.b().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (!(adColonyAdView == null || listener == null)) {
                listener.onClicked(adColonyAdView);
            }
            return;
        }
        adColonyInterstitial.getListener().onClicked(adColonyInterstitial);
    }
}
