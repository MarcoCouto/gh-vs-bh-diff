package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.iab.omid.library.adcolony.Omid;
import com.iab.omid.library.adcolony.adsession.Partner;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledExecutorService;
import org.json.JSONArray;
import org.json.JSONObject;

class h implements a {
    static final String U = "026ae9c9824b3e483fa6c71fa88f57ae27816141";
    static final String V = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5";
    static String W = "https://adc3-launch.adcolony.com/v4/launch";
    private static volatile String X = "";
    private String A;
    private String B;
    private String C = "";
    private boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    private boolean F;
    /* access modifiers changed from: private */
    public boolean G;
    boolean H;
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean L;
    /* access modifiers changed from: private */
    public boolean M;
    /* access modifiers changed from: private */
    public boolean N;
    /* access modifiers changed from: private */
    public boolean O;
    private int P;
    /* access modifiers changed from: private */
    public int Q = 1;
    private final int R = 120;
    private ActivityLifecycleCallbacks S;
    /* access modifiers changed from: private */
    public Partner T = null;
    private i a;
    /* access modifiers changed from: private */
    public y b;
    /* access modifiers changed from: private */
    public m c;
    /* access modifiers changed from: private */
    public f0 d;
    private d e;
    /* access modifiers changed from: private */
    public k f;
    private p g;
    private i0 h;
    /* access modifiers changed from: private */
    public g0 i;
    private w j;
    j k;
    b0 l;
    private c m;
    private AdColonyAdView n;
    private AdColonyInterstitial o;
    /* access modifiers changed from: private */
    public AdColonyRewardListener p;
    private HashMap<String, AdColonyCustomMessageListener> q = new HashMap<>();
    /* access modifiers changed from: private */
    public AdColonyAppOptions r;
    /* access modifiers changed from: private */
    public x s;
    /* access modifiers changed from: private */
    public boolean t;
    /* access modifiers changed from: private */
    public x u;
    private JSONObject v;
    private HashMap<String, AdColonyZone> w = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, m0> x = new HashMap<>();
    private String y;
    private String z;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            int f = s.f(xVar.b(), "number");
            JSONObject b = s.b();
            s.a(b, "uuids", k0.a(f));
            xVar.a(b).d();
        }
    }

    class b implements z {

        class a implements Runnable {
            final /* synthetic */ Context a;
            final /* synthetic */ x b;

            a(Context context, x xVar) {
                this.a = context;
                this.b = xVar;
            }

            public void run() {
                h.this.a(this.a, this.b);
            }
        }

        b() {
        }

        public void a(x xVar) {
            Context b = a.b();
            if (b != null) {
                k0.b.execute(new a(b, xVar));
            }
        }
    }

    class c implements z {
        c() {
        }

        public void a(x xVar) {
            h.this.h().b(s.h(xVar.b(), "version"));
            e0 e0Var = w.n;
            if (e0Var != null) {
                e0Var.e(h.this.h().j());
            }
            new a().a("Controller version: ").a(h.this.h().j()).a(u.f);
        }
    }

    class d implements Runnable {
        d() {
        }

        public void run() {
            Context b = a.b();
            if (!h.this.O && b != null) {
                try {
                    h.this.O = Omid.activateWithOmidApiVersion(Omid.getVersion(), b.getApplicationContext());
                } catch (IllegalArgumentException unused) {
                    new a().a("IllegalArgumentException when activating Omid").a(u.j);
                    h.this.O = false;
                }
            }
            if (h.this.O && h.this.T == null) {
                try {
                    h.this.T = Partner.createPartner("AdColony", "4.1.4");
                } catch (IllegalArgumentException unused2) {
                    new a().a("IllegalArgumentException when creating Omid Partner").a(u.j);
                    h.this.O = false;
                }
            }
        }
    }

    class e implements Runnable {
        e() {
        }

        public void run() {
            JSONObject b = s.b();
            s.a(b, "url", h.W);
            s.a(b, Param.CONTENT_TYPE, "application/json");
            s.a(b, "content", h.this.h().o().toString());
            new a().a("Launch: ").a(h.this.h().o().toString()).a(u.d);
            new a().a("Saving Launch to ").a(h.this.i.a()).a(h.U).a(u.f);
            h.this.c.a(new l(new x("WebServices.post", 0, b), h.this));
        }
    }

    class f implements Runnable {
        final /* synthetic */ Context a;
        final /* synthetic */ boolean b;
        final /* synthetic */ x c;

        f(Context context, boolean z, x xVar) {
            this.a = context;
            this.b = z;
            this.c = xVar;
        }

        public void run() {
            m0 m0Var = new m0(this.a.getApplicationContext(), h.this.b.d(), this.b);
            m0Var.a(true, this.c);
            h.this.x.put(Integer.valueOf(m0Var.d()), m0Var);
        }
    }

    class g implements Runnable {

        class a implements Runnable {
            a() {
            }

            public void run() {
                if (a.c().s().d()) {
                    h.this.E();
                }
            }
        }

        g() {
        }

        public void run() {
            new Handler().postDelayed(new a(), (long) (h.this.Q * 1000));
        }
    }

    /* renamed from: com.adcolony.sdk.h$h reason: collision with other inner class name */
    class C0004h implements Runnable {
        C0004h() {
        }

        public void run() {
            boolean f = h.this.F();
            a aVar = new a();
            StringBuilder sb = new StringBuilder();
            sb.append("Loaded library. Success=");
            sb.append(f);
            aVar.a(sb.toString()).a(u.d);
        }
    }

    class i implements Runnable {
        final /* synthetic */ m0 a;

        i(m0 m0Var) {
            this.a = m0Var;
        }

        public void run() {
            m0 m0Var = this.a;
            if (m0Var != null && m0Var.s()) {
                this.a.loadUrl("about:blank");
                this.a.clearCache(true);
                this.a.removeAllViews();
                this.a.a(true);
                this.a.destroy();
            }
            if (h.this.u != null) {
                h.this.u.d();
                h.this.u = null;
                h.this.t = false;
            }
        }
    }

    class j implements Runnable {
        final /* synthetic */ x a;

        j(x xVar) {
            this.a = xVar;
        }

        public void run() {
            h.this.p.onReward(new AdColonyReward(this.a));
        }
    }

    class k implements z {
        k() {
        }

        public void a(x xVar) {
            h.this.a(xVar);
        }
    }

    class l implements ActivityLifecycleCallbacks {
        l() {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            if (!h.this.d.d()) {
                h.this.d.c(true);
            }
            a.a((Context) activity);
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
            a.d = false;
            h.this.d.d(false);
            h.this.d.e(true);
            a.c().h().J();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c4, code lost:
            if (com.adcolony.sdk.w.n.b.isTerminated() == false) goto L_0x00d1;
         */
        public void onActivityResumed(Activity activity) {
            a.d = true;
            a.a((Context) activity);
            Context b = a.b();
            if (b == null || !h.this.d.b() || !(b instanceof b) || ((b) b).e) {
                new a().a("onActivityResumed() Activity Lifecycle Callback").a(u.f);
                a.a((Context) activity);
                if (h.this.s != null) {
                    h.this.s.a(h.this.s.b()).d();
                    h.this.s = null;
                }
                h.this.E = false;
                h.this.d.d(true);
                h.this.d.e(true);
                h.this.d.f(false);
                h hVar = h.this;
                if (hVar.H && !hVar.d.d()) {
                    h.this.d.c(true);
                }
                h.this.f.c();
                e0 e0Var = w.n;
                if (e0Var != null) {
                    ScheduledExecutorService scheduledExecutorService = e0Var.b;
                    if (scheduledExecutorService != null) {
                        if (!scheduledExecutorService.isShutdown()) {
                        }
                    }
                }
                AdColony.a((Context) activity, a.c().r);
                return;
            }
            new a().a("Ignoring onActivityResumed").a(u.f);
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    class m implements z {
        m() {
        }

        public void a(x xVar) {
            h.this.f(xVar);
        }
    }

    class n implements z {
        n() {
        }

        public void a(x xVar) {
            h.this.G = true;
            if (h.this.M) {
                JSONObject b = s.b();
                JSONObject b2 = s.b();
                s.a(b2, TapjoyConstants.TJC_APP_VERSION_NAME, k0.e());
                s.a(b, "app_bundle_info", b2);
                new x("AdColony.on_update", 1, b).d();
                h.this.M = false;
            }
            if (h.this.N) {
                new x("AdColony.on_install", 1).d();
            }
            if (w.n != null) {
                w.n.f(s.h(xVar.b(), "app_session_id"));
            }
            if (AdColonyEventTracker.a()) {
                AdColonyEventTracker.b();
            }
            int a2 = s.a(xVar.b(), "concurrent_requests", 4);
            if (a2 != h.this.c.a()) {
                h.this.c.a(a2);
            }
            h.this.G();
        }
    }

    class o implements z {
        o() {
        }

        public void a(x xVar) {
            h.this.g(xVar);
        }
    }

    class p implements z {
        p() {
        }

        public void a(x xVar) {
            h.this.d(xVar);
        }
    }

    class q implements z {
        q() {
        }

        public void a(x xVar) {
            h.this.e(xVar);
        }
    }

    class r implements z {
        r() {
        }

        public void a(x xVar) {
            h.this.a(true, true);
        }
    }

    class s implements z {
        s() {
        }

        public void a(x xVar) {
            JSONObject b = s.b();
            s.a(b, "sha1", k0.b(s.h(xVar.b(), "data")));
            xVar.a(b).d();
        }
    }

    class t implements z {
        t() {
        }

        public void a(x xVar) {
            JSONObject b = s.b();
            s.b(b, "crc32", k0.a(s.h(xVar.b(), "data")));
            xVar.a(b).d();
        }
    }

    h() {
    }

    static String D() {
        return X;
    }

    /* access modifiers changed from: private */
    public void E() {
        new Thread(new e()).start();
    }

    /* access modifiers changed from: private */
    public boolean F() {
        this.b.a();
        return true;
    }

    /* access modifiers changed from: private */
    public void G() {
        JSONObject b2 = s.b();
        s.a(b2, "type", "AdColony.on_configuration_completed");
        JSONArray jSONArray = new JSONArray();
        for (String put : w().keySet()) {
            jSONArray.put(put);
        }
        JSONObject b3 = s.b();
        s.a(b3, "zone_ids", jSONArray);
        s.a(b2, "message", b3);
        new x("CustomMessage.controller_send", 0, b2).d();
    }

    private void H() {
        Application application;
        Context b2 = a.b();
        if (b2 != null && this.S == null && VERSION.SDK_INT > 14) {
            this.S = new l();
            if (b2 instanceof Application) {
                application = (Application) b2;
            } else {
                application = ((Activity) b2).getApplication();
            }
            application.registerActivityLifecycleCallbacks(this.S);
        }
    }

    private void I() {
        if (a.c().s().d()) {
            int i2 = this.P + 1;
            this.P = i2;
            int i3 = this.Q * i2;
            int i4 = 120;
            if (i3 <= 120) {
                i4 = i3;
            }
            this.Q = i4;
            k0.a((Runnable) new g());
            return;
        }
        new a().a("Max launch server download attempts hit, or AdColony is no longer").a(" active.").a(u.h);
    }

    /* access modifiers changed from: 0000 */
    public boolean A() {
        return this.G;
    }

    /* access modifiers changed from: 0000 */
    public boolean B() {
        return this.D;
    }

    /* access modifiers changed from: 0000 */
    public boolean C() {
        return this.t;
    }

    /* access modifiers changed from: 0000 */
    public f0 s() {
        if (this.d == null) {
            f0 f0Var = new f0();
            this.d = f0Var;
            f0Var.a();
        }
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public g0 t() {
        if (this.i == null) {
            g0 g0Var = new g0();
            this.i = g0Var;
            g0Var.e();
        }
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public i0 u() {
        if (this.h == null) {
            i0 i0Var = new i0();
            this.h = i0Var;
            i0Var.a();
        }
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, m0> v() {
        return this.x;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, AdColonyZone> w() {
        return this.w;
    }

    /* access modifiers changed from: 0000 */
    public boolean x() {
        return this.r != null;
    }

    /* access modifiers changed from: 0000 */
    public boolean y() {
        return this.E;
    }

    /* access modifiers changed from: 0000 */
    public boolean z() {
        return this.F;
    }

    /* access modifiers changed from: private */
    public void f(x xVar) {
        a(s.f(xVar.b(), "id"));
    }

    /* access modifiers changed from: private */
    public void g(x xVar) {
        AdColonyAppOptions adColonyAppOptions = this.r;
        JSONObject jSONObject = adColonyAppOptions.d;
        s.a(jSONObject, "app_id", adColonyAppOptions.a);
        s.a(jSONObject, "zone_ids", this.r.c);
        JSONObject b2 = s.b();
        s.a(b2, "options", jSONObject);
        xVar.a(b2).d();
    }

    /* access modifiers changed from: 0000 */
    public j h() {
        if (this.k == null) {
            j jVar = new j();
            this.k = jVar;
            jVar.K();
        }
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public k i() {
        if (this.f == null) {
            this.f = new k();
        }
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public m j() {
        if (this.c == null) {
            this.c = new m();
        }
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public p k() {
        if (this.g == null) {
            p pVar = new p();
            this.g = pVar;
            pVar.b();
        }
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject l() {
        return this.v;
    }

    /* access modifiers changed from: 0000 */
    public y m() {
        if (this.b == null) {
            y yVar = new y();
            this.b = yVar;
            yVar.a();
        }
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public b0 n() {
        if (this.l == null) {
            this.l = new b0();
        }
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public String o() {
        return this.y;
    }

    /* access modifiers changed from: 0000 */
    public Partner p() {
        return this.T;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAppOptions q() {
        if (this.r == null) {
            this.r = new AdColonyAppOptions();
        }
        return this.r;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyRewardListener r() {
        return this.p;
    }

    private boolean e(boolean z2) {
        return a(z2, false);
    }

    /* access modifiers changed from: 0000 */
    public c f() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public void b(x xVar) {
        this.u = xVar;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        return this.C;
    }

    /* access modifiers changed from: 0000 */
    public boolean d(x xVar) {
        if (this.p == null) {
            return false;
        }
        k0.a((Runnable) new j(xVar));
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void e(x xVar) {
        AdColonyZone adColonyZone;
        if (this.F) {
            new a().a("AdColony is disabled. Ignoring zone_info message.").a(u.h);
            return;
        }
        String h2 = s.h(xVar.b(), "zone_id");
        if (this.w.containsKey(h2)) {
            adColonyZone = (AdColonyZone) this.w.get(h2);
        } else {
            AdColonyZone adColonyZone2 = new AdColonyZone(h2);
            this.w.put(h2, adColonyZone2);
            adColonyZone = adColonyZone2;
        }
        adColonyZone.a(xVar);
    }

    private boolean b(String str) {
        Context b2 = a.b();
        if (b2 != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(b2.getFilesDir().getAbsolutePath());
            sb.append("/adc3/");
            sb.append(V);
            File file = new File(sb.toString());
            if (file.exists()) {
                return k0.a(str, file);
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void c(x xVar) {
        this.s = xVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append(r3.i.a());
        r1.append(U);
        new java.io.File(r1.toString()).delete();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0047 */
    private boolean c(JSONObject jSONObject) {
        if (jSONObject == null) {
            new a().a("Launch response verification failed - response is null or unknown").a(u.f);
            return false;
        }
        JSONObject g2 = s.g(jSONObject, "controller");
        this.z = s.h(g2, "url");
        this.A = s.h(g2, "sha1");
        this.B = s.h(jSONObject, "status");
        X = s.h(jSONObject, "pie");
        if (AdColonyEventTracker.a()) {
            AdColonyEventTracker.b();
        }
        b(jSONObject);
        if (this.B.equals("disable") && !m0.N) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(this.i.a());
                sb.append(V);
                new File(sb.toString()).delete();
            } catch (Exception unused) {
            }
            new a().a("Launch server response with disabled status. Disabling AdColony ").a("until next launch.").a(u.h);
            AdColony.disable();
            return false;
        } else if ((!this.z.equals("") && !this.B.equals("")) || m0.N) {
            return true;
        } else {
            new a().a("Missing controller status or URL. Disabling AdColony until next ").a("launch.").a(u.i);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public Context a() {
        return a.b();
    }

    /* access modifiers changed from: 0000 */
    public AdColonyInterstitial d() {
        return this.o;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0101  */
    public void a(AdColonyAppOptions adColonyAppOptions, boolean z2) {
        boolean z3;
        this.F = z2;
        this.r = adColonyAppOptions;
        this.b = new y();
        this.a = new i();
        m mVar = new m();
        this.c = mVar;
        mVar.b();
        f0 f0Var = new f0();
        this.d = f0Var;
        f0Var.a();
        d dVar = new d();
        this.e = dVar;
        dVar.e();
        this.f = new k();
        p pVar = new p();
        this.g = pVar;
        pVar.b();
        this.j = new w();
        w.a();
        g0 g0Var = new g0();
        this.i = g0Var;
        g0Var.e();
        i0 i0Var = new i0();
        this.h = i0Var;
        i0Var.a();
        j jVar = new j();
        this.k = jVar;
        jVar.K();
        b0 b0Var = new b0();
        this.l = b0Var;
        this.y = b0Var.a();
        AdColony.a(a.b(), adColonyAppOptions);
        boolean z4 = false;
        if (!z2) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.i.a());
            sb.append(U);
            this.J = new File(sb.toString()).exists();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.i.a());
            sb2.append(V);
            boolean exists = new File(sb2.toString()).exists();
            this.K = exists;
            if (this.J && exists) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.i.a());
                sb3.append(U);
                if (s.h(s.c(sb3.toString()), GeneralPropertiesWorker.SDK_VERSION).equals(this.k.E())) {
                    z3 = true;
                    this.I = z3;
                    if (this.J) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append(this.i.a());
                        sb4.append(U);
                        JSONObject c2 = s.c(sb4.toString());
                        this.v = c2;
                        b(c2);
                    }
                    e(this.I);
                    H();
                }
            }
            z3 = false;
            this.I = z3;
            if (this.J) {
            }
            e(this.I);
            H();
        }
        a.a("Module.load", (z) new k());
        a.a("Module.unload", (z) new m());
        a.a("AdColony.on_configured", (z) new n());
        a.a("AdColony.get_app_info", (z) new o());
        a.a("AdColony.v4vc_reward", (z) new p());
        a.a("AdColony.zone_info", (z) new q());
        a.a("AdColony.probe_launch_server", (z) new r());
        a.a("Crypto.sha1", (z) new s());
        a.a("Crypto.crc32", (z) new t());
        a.a("Crypto.uuid", (z) new a());
        a.a("Device.query_advertiser_info", (z) new b());
        a.a("AdColony.controller_version", (z) new c());
        int a2 = k0.a(this.i);
        this.M = a2 == 1;
        if (a2 == 2) {
            z4 = true;
        }
        this.N = z4;
        k0.a((Runnable) new d());
    }

    /* access modifiers changed from: 0000 */
    public void d(boolean z2) {
        this.D = z2;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<String, AdColonyCustomMessageListener> g() {
        return this.q;
    }

    private void b(JSONObject jSONObject) {
        if (!m0.N) {
            JSONObject g2 = s.g(jSONObject, "logging");
            w.k = s.a(g2, "send_level", 1);
            w.a = s.d(g2, "log_private");
            w.i = s.a(g2, "print_level", 3);
            this.j.b(s.c(g2, "modules"));
        }
        h().a(s.g(jSONObject, TtmlNode.TAG_METADATA));
        this.C = s.h(s.g(jSONObject, "controller"), "version");
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull AdColonyAppOptions adColonyAppOptions) {
        this.r = adColonyAppOptions;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAdView e() {
        return this.n;
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z2) {
        this.E = z2;
    }

    /* access modifiers changed from: 0000 */
    public d b() {
        if (this.e == null) {
            d dVar = new d();
            this.e = dVar;
            dVar.e();
        }
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public void c(boolean z2) {
        this.t = z2;
    }

    /* access modifiers changed from: private */
    public boolean a(boolean z2, boolean z3) {
        if (!a.d()) {
            return false;
        }
        this.L = z3;
        this.I = z2;
        if (z2 && !z3 && !F()) {
            return false;
        }
        E();
        return true;
    }

    private boolean a(JSONObject jSONObject) {
        if (!this.I) {
            new a().a("Non-standard launch. Downloading new controller.").a(u.h);
            return true;
        }
        JSONObject jSONObject2 = this.v;
        if (jSONObject2 != null && s.h(s.g(jSONObject2, "controller"), "sha1").equals(s.h(s.g(jSONObject, "controller"), "sha1"))) {
            return false;
        }
        new a().a("Controller sha1 does not match, downloading new controller.").a(u.h);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        this.F = z2;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(x xVar) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        try {
            int f2 = xVar.b().has("id") ? s.f(xVar.b(), "id") : 0;
            if (f2 <= 0) {
                f2 = this.b.d();
            }
            a(f2);
            k0.a((Runnable) new f(b2, s.d(xVar.b(), "is_display_module"), xVar));
            return true;
        } catch (RuntimeException e2) {
            a aVar = new a();
            StringBuilder sb = new StringBuilder();
            sb.append(e2.toString());
            sb.append(": during WebView initialization.");
            aVar.a(sb.toString()).a(" Disabling AdColony.").a(u.i);
            AdColony.disable();
            return false;
        }
    }

    public void a(l lVar, x xVar, Map<String, List<String>> map) {
        if (lVar.m.equals(W)) {
            if (lVar.o) {
                new a().a("Launch: ").a(lVar.n).a(u.d);
                JSONObject a2 = s.a(lVar.n, "Parsing launch response");
                s.a(a2, GeneralPropertiesWorker.SDK_VERSION, h().E());
                StringBuilder sb = new StringBuilder();
                sb.append(this.i.a());
                sb.append(U);
                s.i(a2, sb.toString());
                if (!c(a2)) {
                    if (!this.I) {
                        new a().a("Incomplete or disabled launch server response. ").a("Disabling AdColony until next launch.").a(u.i);
                        a(true);
                    }
                    return;
                }
                if (a(a2)) {
                    new a().a("Controller missing or out of date. Downloading controller").a(u.f);
                    JSONObject b2 = s.b();
                    s.a(b2, "url", this.z);
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(this.i.a());
                    sb2.append(V);
                    s.a(b2, "filepath", sb2.toString());
                    this.c.a(new l(new x("WebServices.download", 0, b2), this));
                }
                this.v = a2;
            } else {
                I();
            }
        } else if (lVar.m.equals(this.z)) {
            if (!b(this.A) && !m0.N) {
                new a().a("Downloaded controller sha1 does not match, retrying.").a(u.g);
                I();
            } else if (!this.I && !this.L) {
                k0.a((Runnable) new C0004h());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(Context context, x xVar) {
        boolean z2;
        if (context == null) {
            return false;
        }
        String str = "";
        Info info = null;
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(context);
        } catch (NoClassDefFoundError unused) {
            new a().a("Google Play Services ads dependencies are missing. Collecting ").a("Android ID instead of Advertising ID.").a(u.g);
            return false;
        } catch (NoSuchMethodError unused2) {
            new a().a("Google Play Services is out of date, please update to GPS 4.0+. ").a("Collecting Android ID instead of Advertising ID.").a(u.g);
        } catch (Exception e2) {
            e2.printStackTrace();
            if (!Build.MANUFACTURER.equals("Amazon")) {
                new a().a("Advertising ID is not available. Collecting Android ID instead of").a(" Advertising ID.").a(u.g);
                return false;
            }
            str = h().c();
            z2 = h().d();
        }
        z2 = false;
        if (!Build.MANUFACTURER.equals("Amazon") && info == null) {
            return false;
        }
        if (!Build.MANUFACTURER.equals("Amazon")) {
            str = info.getId();
            z2 = info.isLimitAdTrackingEnabled();
        }
        h().a(str);
        w.n.e.put("advertisingId", h().b());
        h().b(z2);
        h().a(true);
        if (xVar != null) {
            JSONObject b2 = s.b();
            s.a(b2, "advertiser_id", h().b());
            s.b(b2, "limit_ad_tracking", h().v());
            xVar.a(b2).d();
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyAppOptions adColonyAppOptions) {
        synchronized (this.e.a()) {
            for (Entry value : this.e.a().entrySet()) {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) value.getValue();
                AdColonyInterstitialListener listener = adColonyInterstitial.getListener();
                adColonyInterstitial.a(true);
                if (listener != null) {
                    listener.onExpiring(adColonyInterstitial);
                }
            }
            this.e.a().clear();
        }
        this.G = false;
        a(1);
        this.w.clear();
        this.r = adColonyAppOptions;
        this.b.a();
        a(true, true);
    }

    /* access modifiers changed from: 0000 */
    public boolean a(int i2) {
        a0 a2 = this.b.a(i2);
        m0 m0Var = (m0) this.x.remove(Integer.valueOf(i2));
        boolean z2 = false;
        if (a2 == null) {
            return false;
        }
        if (m0Var != null && m0Var.t()) {
            z2 = true;
        }
        i iVar = new i(m0Var);
        if (z2) {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            new Handler().postDelayed(iVar, 1000);
        } else {
            iVar.run();
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyRewardListener adColonyRewardListener) {
        this.p = adColonyRewardListener;
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        this.m = cVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyAdView adColonyAdView) {
        this.n = adColonyAdView;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyInterstitial adColonyInterstitial) {
        this.o = adColonyInterstitial;
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.y = str;
    }
}
