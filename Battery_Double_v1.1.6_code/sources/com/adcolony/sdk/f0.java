package com.adcolony.sdk;

import android.content.Context;
import com.facebook.login.widget.ToolTipPopup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executors;
import org.json.JSONObject;

class f0 implements Runnable {
    private final long a = 30000;
    private final int b = 17;
    private final int c = 15000;
    private final int d = 1000;
    private long e;
    private long f;
    private long g;
    private long h;
    private long i;
    private long j;
    private long k;
    private long l;
    private boolean m = true;
    private boolean n = true;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    /* access modifiers changed from: private */
    public boolean s;
    private boolean t;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            f0.this.s = true;
        }
    }

    f0() {
    }

    private void f() {
        a(false);
    }

    private void g() {
        b(false);
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z) {
        ArrayList b2 = a.c().m().b();
        synchronized (b2) {
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                a0 a0Var = (a0) it.next();
                JSONObject b3 = s.b();
                s.b(b3, "from_window_focus", z);
                new x("SessionInfo.on_resume", a0Var.d(), b3).d();
            }
        }
        w.b();
        this.n = false;
    }

    /* access modifiers changed from: 0000 */
    public void c(boolean z) {
        if (!this.p) {
            if (this.q) {
                a.c().b(false);
                this.q = false;
            }
            this.e = 0;
            this.f = 0;
            this.p = true;
            this.m = true;
            this.s = false;
            new Thread(this).start();
            if (z) {
                JSONObject b2 = s.b();
                s.a(b2, "id", k0.a());
                new x("SessionInfo.on_start", 1, b2).d();
                m0 m0Var = (m0) a.c().m().c().get(Integer.valueOf(1));
                if (m0Var != null) {
                    m0Var.v();
                }
            }
            if (AdColony.a.isShutdown()) {
                AdColony.a = Executors.newSingleThreadExecutor();
            }
            w.b();
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        this.p = false;
        this.m = false;
        e0 e0Var = w.n;
        if (e0Var != null) {
            e0Var.b();
        }
        JSONObject b2 = s.b();
        double d2 = (double) this.e;
        Double.isNaN(d2);
        s.a(b2, "session_length", d2 / 1000.0d);
        new x("SessionInfo.on_stop", 1, b2).d();
        a.g();
        AdColony.a.shutdown();
    }

    public void run() {
        long j2;
        while (!this.r) {
            this.h = System.currentTimeMillis();
            a.g();
            if (this.f >= 30000) {
                break;
            }
            if (!this.m) {
                if (this.o && !this.n) {
                    this.o = false;
                    f();
                }
                long j3 = this.f;
                if (this.l == 0) {
                    j2 = 0;
                } else {
                    j2 = System.currentTimeMillis() - this.l;
                }
                this.f = j3 + j2;
                this.l = System.currentTimeMillis();
            } else {
                if (this.o && this.n) {
                    this.o = false;
                    g();
                }
                this.f = 0;
                this.l = 0;
            }
            this.g = 17;
            a(17);
            long currentTimeMillis = System.currentTimeMillis() - this.h;
            this.i = currentTimeMillis;
            if (currentTimeMillis > 0 && currentTimeMillis < ToolTipPopup.DEFAULT_POPUP_DISPLAY_TIME) {
                this.e += currentTimeMillis;
            }
            h c2 = a.c();
            long currentTimeMillis2 = System.currentTimeMillis();
            if (currentTimeMillis2 - this.k > 15000) {
                this.k = currentTimeMillis2;
            }
            if (a.d() && currentTimeMillis2 - this.j > 1000) {
                this.j = currentTimeMillis2;
                String a2 = c2.n().a();
                if (!a2.equals(c2.o())) {
                    c2.a(a2);
                    JSONObject b2 = s.b();
                    s.a(b2, "network_type", c2.o());
                    new x("Network.on_status_change", 1, b2).d();
                }
            }
        }
        new a().a("AdColony session ending, releasing Context.").a(u.e);
        a.c().b(true);
        a.a((Context) null);
        this.q = true;
        this.t = true;
        e();
        b bVar = new b(10.0d);
        while (!this.s && !bVar.a() && this.t) {
            a.g();
            a(100);
        }
    }

    public void a() {
        a.a("SessionInfo.stopped", (z) new a());
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        return this.p;
    }

    /* access modifiers changed from: 0000 */
    public void f(boolean z) {
        this.t = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        ArrayList b2 = a.c().m().b();
        synchronized (b2) {
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                a0 a0Var = (a0) it.next();
                JSONObject b3 = s.b();
                s.b(b3, "from_window_focus", z);
                new x("SessionInfo.on_pause", a0Var.d(), b3).d();
            }
        }
        this.n = true;
        a.g();
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.m;
    }

    private void a(long j2) {
        try {
            Thread.sleep(j2);
        } catch (InterruptedException unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(boolean z) {
        this.o = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return this.o;
    }
}
