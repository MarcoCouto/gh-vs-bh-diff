package com.adcolony.sdk;

import android.content.Context;
import java.lang.ref.WeakReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a {
    private static WeakReference<Context> a;
    /* access modifiers changed from: private */
    public static h b;
    static boolean c;
    static boolean d;

    /* renamed from: com.adcolony.sdk.a$a reason: collision with other inner class name */
    static class C0000a implements Runnable {
        final /* synthetic */ Context a;

        C0000a(Context context) {
            this.a = context;
        }

        public void run() {
            a.b.a(this.a, (x) null);
        }
    }

    a() {
    }

    static Context b() {
        WeakReference<Context> weakReference = a;
        if (weakReference == null) {
            return null;
        }
        return (Context) weakReference.get();
    }

    static h c() {
        if (!e()) {
            Context b2 = b();
            if (b2 == null) {
                return new h();
            }
            b = new h();
            StringBuilder sb = new StringBuilder();
            sb.append(b2.getFilesDir().getAbsolutePath());
            sb.append("/adc3/AppInfo");
            JSONObject c2 = s.c(sb.toString());
            JSONArray c3 = s.c(c2, "zoneIds");
            b.a(new AdColonyAppOptions().a(s.h(c2, "appId")).a(s.a(c3)), false);
        }
        return b;
    }

    static boolean d() {
        WeakReference<Context> weakReference = a;
        return (weakReference == null || weakReference.get() == null) ? false : true;
    }

    static boolean e() {
        return b != null;
    }

    static boolean f() {
        return c;
    }

    static void g() {
        c().m().e();
    }

    static void a(Context context, AdColonyAppOptions adColonyAppOptions, boolean z) {
        a(context);
        d = true;
        h hVar = b;
        if (hVar == null) {
            h hVar2 = new h();
            b = hVar2;
            hVar2.a(adColonyAppOptions, z);
        } else {
            hVar.a(adColonyAppOptions);
        }
        k0.b.execute(new C0000a(context));
        new a().a("Configuring AdColony").a(u.e);
        b.b(false);
        b.s().d(true);
        b.s().e(true);
        b.s().f(false);
        h hVar3 = b;
        hVar3.H = true;
        hVar3.s().c(false);
    }

    static void b(String str, z zVar) {
        c().m().b(str, zVar);
    }

    static void a(Context context) {
        if (context == null) {
            a.clear();
        } else {
            a = new WeakReference<>(context);
        }
    }

    static void a(String str, z zVar) {
        c().m().a(str, zVar);
    }

    static z a(String str, z zVar, boolean z) {
        c().m().a(str, zVar);
        return zVar;
    }

    static void a(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = s.b();
        }
        s.a(jSONObject, "m_type", str);
        c().m().a(jSONObject);
    }

    static void a(String str) {
        try {
            x xVar = new x("CustomMessage.send", 0);
            xVar.b().put("message", str);
            xVar.d();
        } catch (JSONException e) {
            new a().a("JSON error from ADC.java's send_custom_message(): ").a(e.toString()).a(u.j);
        }
    }
}
