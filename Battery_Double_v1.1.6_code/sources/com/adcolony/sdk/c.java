package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.FloatRange;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
import com.github.mikephil.charting.utils.Utils;
import com.iab.omid.library.adcolony.adsession.AdSession;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

class c extends FrameLayout {
    VideoView A;
    private HashMap<Integer, l0> a;
    private HashMap<Integer, j0> b;
    private HashMap<Integer, m0> c;
    private HashMap<Integer, n> d;
    private HashMap<Integer, q> e;
    private HashMap<Integer, Boolean> f;
    private HashMap<Integer, View> g;
    private int h;
    private int i;
    private int j;
    private int k;
    /* access modifiers changed from: private */
    public String l;
    boolean m;
    boolean n;
    /* access modifiers changed from: private */
    public float o = 0.0f;
    /* access modifiers changed from: private */
    public double p = Utils.DOUBLE_EPSILON;
    /* access modifiers changed from: private */
    public long q = 0;
    /* access modifiers changed from: private */
    public int r = 0;
    /* access modifiers changed from: private */
    public int s = 0;
    private ArrayList<z> t;
    private ArrayList<String> u;
    private boolean v;
    private boolean w;
    private boolean x;
    private AdSession y;
    Context z;

    class a implements Runnable {
        final /* synthetic */ Runnable a;

        a(Runnable runnable) {
            this.a = runnable;
        }

        public void run() {
            while (!c.this.m) {
                k0.a(this.a);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException unused) {
                }
            }
        }
    }

    class b implements z {
        b() {
        }

        public void a(x xVar) {
            if (c.this.i(xVar)) {
                c cVar = c.this;
                cVar.a((View) cVar.c(xVar));
            }
        }
    }

    /* renamed from: com.adcolony.sdk.c$c reason: collision with other inner class name */
    class C0001c implements z {
        C0001c() {
        }

        public void a(x xVar) {
            if (c.this.i(xVar)) {
                c.this.g(xVar);
            }
        }
    }

    class d implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                c cVar = c.this;
                cVar.a((View) cVar.d(this.a));
            }
        }

        d() {
        }

        public void a(x xVar) {
            if (c.this.i(xVar)) {
                k0.a((Runnable) new a(xVar));
            }
        }
    }

    class e implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                c.this.h(this.a);
            }
        }

        e() {
        }

        public void a(x xVar) {
            if (c.this.i(xVar)) {
                k0.a((Runnable) new a(xVar));
            }
        }
    }

    class f implements z {
        f() {
        }

        public void a(x xVar) {
            if (c.this.i(xVar)) {
                c cVar = c.this;
                cVar.a(cVar.b(xVar));
            }
        }
    }

    class g implements z {
        g() {
        }

        public void a(x xVar) {
            if (c.this.i(xVar)) {
                c.this.f(xVar);
            }
        }
    }

    class h implements z {
        h() {
        }

        public void a(x xVar) {
            if (c.this.i(xVar)) {
                c cVar = c.this;
                cVar.a((View) cVar.a(xVar));
            }
        }
    }

    class i implements z {
        i() {
        }

        public void a(x xVar) {
            if (c.this.i(xVar)) {
                c.this.e(xVar);
            }
        }
    }

    class j implements Runnable {
        final /* synthetic */ boolean a;

        j(boolean z) {
            this.a = z;
        }

        public void run() {
            m0 webView;
            double d;
            if (c.this.q == 0) {
                c.this.q = System.currentTimeMillis();
            }
            View view = (View) c.this.getParent();
            AdColonyAdView adColonyAdView = (AdColonyAdView) a.c().b().b().get(c.this.l);
            if (adColonyAdView == null) {
                webView = null;
            } else {
                webView = adColonyAdView.getWebView();
            }
            m0 m0Var = webView;
            Context b2 = a.b();
            boolean z = false;
            float a2 = o0.a(view, b2, true, this.a, true, adColonyAdView != null);
            if (b2 == null) {
                d = Utils.DOUBLE_EPSILON;
            } else {
                d = k0.a(k0.a(b2));
            }
            int a3 = k0.a((View) m0Var);
            int b3 = k0.b((View) m0Var);
            if (!(a3 == c.this.r && b3 == c.this.s)) {
                z = true;
            }
            if (z) {
                c.this.r = a3;
                c.this.s = b3;
                c.this.a(a3, b3, m0Var);
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (c.this.q + 200 < currentTimeMillis) {
                c.this.q = currentTimeMillis;
                if (!(c.this.o == a2 && c.this.p == d && !z)) {
                    c.this.a(a2, d);
                }
                c.this.o = a2;
                c.this.p = d;
            }
        }
    }

    private c(Context context) {
        super(context);
    }

    /* access modifiers changed from: 0000 */
    public boolean g(x xVar) {
        int f2 = s.f(xVar.b(), "id");
        l0 l0Var = (l0) this.a.remove(Integer.valueOf(f2));
        if (((View) this.g.remove(Integer.valueOf(f2))) == null || l0Var == null) {
            d b2 = a.c().b();
            String c2 = xVar.c();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(f2);
            b2.a(c2, sb.toString());
            return false;
        }
        if (l0Var.c()) {
            l0Var.i();
        }
        l0Var.a();
        removeView(l0Var);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean h(x xVar) {
        int f2 = s.f(xVar.b(), "id");
        h c2 = a.c();
        View view = (View) this.g.remove(Integer.valueOf(f2));
        m0 m0Var = (m0) this.c.remove(Integer.valueOf(f2));
        if (m0Var == null || view == null) {
            d b2 = c2.b();
            String c3 = xVar.c();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(f2);
            b2.a(c3, sb.toString());
            return false;
        }
        c2.m().a(m0Var.d());
        removeView(m0Var);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean i(x xVar) {
        JSONObject b2 = xVar.b();
        return s.f(b2, "container_id") == this.j && s.h(b2, "ad_session_id").equals(this.l);
    }

    /* access modifiers changed from: 0000 */
    public void j(x xVar) {
        int i2;
        this.a = new HashMap<>();
        this.b = new HashMap<>();
        this.c = new HashMap<>();
        this.d = new HashMap<>();
        this.e = new HashMap<>();
        this.f = new HashMap<>();
        this.g = new HashMap<>();
        this.t = new ArrayList<>();
        this.u = new ArrayList<>();
        JSONObject b2 = xVar.b();
        if (s.d(b2, "transparent")) {
            setBackgroundColor(0);
        }
        this.j = s.f(b2, "id");
        this.h = s.f(b2, "width");
        this.i = s.f(b2, "height");
        this.k = s.f(b2, "module_id");
        this.n = s.d(b2, "viewability_enabled");
        this.v = this.j == 1;
        h c2 = a.c();
        if (this.h == 0 && this.i == 0) {
            this.h = c2.h().s();
            if (c2.q().getMultiWindowEnabled()) {
                i2 = c2.h().r() - k0.d(a.b());
            } else {
                i2 = c2.h().r();
            }
            this.i = i2;
        } else {
            setLayoutParams(new LayoutParams(this.h, this.i));
        }
        this.t.add(a.a("VideoView.create", (z) new b(), true));
        this.t.add(a.a("VideoView.destroy", (z) new C0001c(), true));
        this.t.add(a.a("WebView.create", (z) new d(), true));
        this.t.add(a.a("WebView.destroy", (z) new e(), true));
        this.t.add(a.a("TextView.create", (z) new f(), true));
        this.t.add(a.a("TextView.destroy", (z) new g(), true));
        this.t.add(a.a("ImageView.create", (z) new h(), true));
        this.t.add(a.a("ImageView.destroy", (z) new i(), true));
        this.u.add("VideoView.create");
        this.u.add("VideoView.destroy");
        this.u.add("WebView.create");
        this.u.add("WebView.destroy");
        this.u.add("TextView.create");
        this.u.add("TextView.destroy");
        this.u.add("ImageView.create");
        this.u.add("ImageView.destroy");
        VideoView videoView = new VideoView(this.z);
        this.A = videoView;
        videoView.setVisibility(8);
        addView(this.A);
        setClipToPadding(false);
        if (this.n) {
            d(s.d(xVar.b(), "advanced_viewability"));
        }
    }

    /* access modifiers changed from: 0000 */
    public int k() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, j0> l() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, l0> m() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, m0> n() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public boolean o() {
        return this.w;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        h c2 = a.c();
        d b2 = c2.b();
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject b3 = s.b();
        s.b(b3, "view_id", -1);
        s.a(b3, "ad_session_id", this.l);
        s.b(b3, "container_x", x2);
        s.b(b3, "container_y", y2);
        s.b(b3, "view_x", x2);
        s.b(b3, "view_y", y2);
        s.b(b3, "id", this.j);
        switch (action) {
            case 0:
                new x("AdContainer.on_touch_began", this.k, b3).d();
                break;
            case 1:
                if (!this.v) {
                    c2.a((AdColonyAdView) b2.b().get(this.l));
                }
                new x("AdContainer.on_touch_ended", this.k, b3).d();
                break;
            case 2:
                new x("AdContainer.on_touch_moved", this.k, b3).d();
                break;
            case 3:
                new x("AdContainer.on_touch_cancelled", this.k, b3).d();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(b3, "container_x", (int) motionEvent.getX(action2));
                s.b(b3, "container_y", (int) motionEvent.getY(action2));
                s.b(b3, "view_x", (int) motionEvent.getX(action2));
                s.b(b3, "view_y", (int) motionEvent.getY(action2));
                new x("AdContainer.on_touch_began", this.k, b3).d();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(b3, "container_x", (int) motionEvent.getX(action3));
                s.b(b3, "container_y", (int) motionEvent.getY(action3));
                s.b(b3, "view_x", (int) motionEvent.getX(action3));
                s.b(b3, "view_y", (int) motionEvent.getY(action3));
                s.b(b3, AvidJSONUtil.KEY_X, (int) motionEvent.getX(action3));
                s.b(b3, AvidJSONUtil.KEY_Y, (int) motionEvent.getY(action3));
                if (!this.v) {
                    c2.a((AdColonyAdView) b2.b().get(this.l));
                }
                new x("AdContainer.on_touch_ended", this.k, b3).d();
                break;
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean p() {
        return this.v;
    }

    /* access modifiers changed from: 0000 */
    public boolean q() {
        return this.x;
    }

    /* access modifiers changed from: 0000 */
    public void r() {
        JSONObject b2 = s.b();
        s.a(b2, "id", this.l);
        new x("AdSession.on_error", this.k, b2).d();
    }

    /* access modifiers changed from: 0000 */
    public l0 c(x xVar) {
        int f2 = s.f(xVar.b(), "id");
        l0 l0Var = new l0(this.z, xVar, f2, this);
        l0Var.d();
        this.a.put(Integer.valueOf(f2), l0Var);
        this.g.put(Integer.valueOf(f2), l0Var);
        return l0Var;
    }

    /* access modifiers changed from: 0000 */
    public m0 d(x xVar) {
        m0 m0Var;
        JSONObject b2 = xVar.b();
        int f2 = s.f(b2, "id");
        boolean d2 = s.d(b2, "is_module");
        h c2 = a.c();
        if (d2) {
            m0Var = (m0) c2.v().get(Integer.valueOf(s.f(b2, "module_id")));
            if (m0Var == null) {
                new a().a("Module WebView created with invalid id").a(u.i);
                return null;
            }
            m0Var.a(xVar, f2, this);
        } else {
            try {
                m0Var = new m0(this.z, xVar, f2, c2.m().d(), this);
            } catch (RuntimeException e2) {
                a aVar = new a();
                StringBuilder sb = new StringBuilder();
                sb.append(e2.toString());
                sb.append(": during WebView initialization.");
                aVar.a(sb.toString()).a(" Disabling AdColony.").a(u.i);
                AdColony.disable();
                return null;
            }
        }
        this.c.put(Integer.valueOf(f2), m0Var);
        this.g.put(Integer.valueOf(f2), m0Var);
        JSONObject b3 = s.b();
        s.b(b3, "module_id", m0Var.d());
        s.b(b3, "mraid_module_id", m0Var.c());
        xVar.a(b3).d();
        return m0Var;
    }

    /* access modifiers changed from: 0000 */
    public boolean e(x xVar) {
        int f2 = s.f(xVar.b(), "id");
        q qVar = (q) this.e.remove(Integer.valueOf(f2));
        if (((View) this.g.remove(Integer.valueOf(f2))) == null || qVar == null) {
            d b2 = a.c().b();
            String c2 = xVar.c();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(f2);
            b2.a(c2, sb.toString());
            return false;
        }
        removeView(qVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean f(x xVar) {
        TextView textView;
        int f2 = s.f(xVar.b(), "id");
        View view = (View) this.g.remove(Integer.valueOf(f2));
        if (((Boolean) this.f.remove(Integer.valueOf(this.j))).booleanValue()) {
            textView = (TextView) this.d.remove(Integer.valueOf(f2));
        } else {
            textView = (TextView) this.b.remove(Integer.valueOf(f2));
        }
        if (view == null || textView == null) {
            d b2 = a.c().b();
            String c2 = xVar.c();
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(f2);
            b2.a(c2, sb.toString());
            return false;
        }
        removeView(textView);
        return true;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"InlinedApi"})
    public View b(x xVar) {
        JSONObject b2 = xVar.b();
        int f2 = s.f(b2, "id");
        if (s.d(b2, "editable")) {
            n nVar = new n(this.z, xVar, f2, this);
            nVar.a();
            this.d.put(Integer.valueOf(f2), nVar);
            this.g.put(Integer.valueOf(f2), nVar);
            this.f.put(Integer.valueOf(f2), Boolean.valueOf(true));
            return nVar;
        } else if (!s.d(b2, "button")) {
            j0 j0Var = new j0(this.z, xVar, f2, this);
            j0Var.a();
            this.b.put(Integer.valueOf(f2), j0Var);
            this.g.put(Integer.valueOf(f2), j0Var);
            this.f.put(Integer.valueOf(f2), Boolean.valueOf(false));
            return j0Var;
        } else {
            j0 j0Var2 = new j0(this.z, 16974145, xVar, f2, this);
            j0Var2.a();
            this.b.put(Integer.valueOf(f2), j0Var2);
            this.g.put(Integer.valueOf(f2), j0Var2);
            this.f.put(Integer.valueOf(f2), Boolean.valueOf(false));
            return j0Var2;
        }
    }

    /* access modifiers changed from: 0000 */
    public ArrayList<z> i() {
        return this.t;
    }

    c(Context context, String str) {
        super(context);
        this.z = context;
        this.l = str;
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
    }

    /* access modifiers changed from: 0000 */
    public int c() {
        return this.j;
    }

    /* access modifiers changed from: 0000 */
    public q a(x xVar) {
        int f2 = s.f(xVar.b(), "id");
        q qVar = new q(this.z, xVar, f2, this);
        qVar.b();
        this.e.put(Integer.valueOf(f2), qVar);
        this.g.put(Integer.valueOf(f2), qVar);
        return qVar;
    }

    /* access modifiers changed from: 0000 */
    public void c(boolean z2) {
        this.w = z2;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, q> h() {
        return this.e;
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, m0 m0Var) {
        float n2 = a.c().h().n();
        if (m0Var != null) {
            JSONObject b2 = s.b();
            s.b(b2, "app_orientation", k0.g(k0.g()));
            s.b(b2, "width", (int) (((float) m0Var.i()) / n2));
            s.b(b2, "height", (int) (((float) m0Var.h()) / n2));
            s.b(b2, AvidJSONUtil.KEY_X, i2);
            s.b(b2, AvidJSONUtil.KEY_Y, i3);
            s.a(b2, "ad_session_id", this.l);
            new x("MRAID.on_size_change", this.k, b2).d();
        }
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, View> e() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, Boolean> g() {
        return this.f;
    }

    /* access modifiers changed from: 0000 */
    public HashMap<Integer, n> f() {
        return this.d;
    }

    /* access modifiers changed from: private */
    public void a(@FloatRange(from = 0.0d, to = 100.0d) float f2, @FloatRange(from = 0.0d, to = 1.0d) double d2) {
        JSONObject b2 = s.b();
        s.b(b2, "id", this.j);
        s.a(b2, "ad_session_id", this.l);
        s.a(b2, "exposure", (double) f2);
        s.a(b2, AvidVideoPlaybackListenerImpl.VOLUME, d2);
        new x("AdContainer.on_exposure_change", this.k, b2).d();
    }

    /* access modifiers changed from: 0000 */
    public int b() {
        return this.i;
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        this.h = i2;
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.l;
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z2) {
        this.x = z2;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        this.i = i2;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z2) {
        this.v = z2;
    }

    /* access modifiers changed from: 0000 */
    public void a(AdSession adSession) {
        this.y = adSession;
        a((Map) this.g);
    }

    /* access modifiers changed from: 0000 */
    public void a(Map map) {
        if (this.y != null && map != null) {
            for (Entry value : map.entrySet()) {
                this.y.addFriendlyObstruction((View) value.getValue());
            }
        }
    }

    private void d(boolean z2) {
        new Thread(new a(new j(z2))).start();
    }

    /* access modifiers changed from: 0000 */
    public void a(View view) {
        AdSession adSession = this.y;
        if (adSession != null && view != null) {
            adSession.addFriendlyObstruction(view);
        }
    }

    /* access modifiers changed from: 0000 */
    public int d() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public ArrayList<String> j() {
        return this.u;
    }
}
