package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

public class AdColony {
    static ExecutorService a = Executors.newSingleThreadExecutor();

    static class a implements Runnable {
        final /* synthetic */ String a;
        final /* synthetic */ AdColonyInterstitialListener b;

        a(String str, AdColonyInterstitialListener adColonyInterstitialListener) {
            this.a = str;
            this.b = adColonyInterstitialListener;
        }

        public void run() {
            AdColonyZone adColonyZone = (AdColonyZone) a.c().w().get(this.a);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(this.a);
            }
            this.b.onRequestNotFilled(adColonyZone);
        }
    }

    static class b implements Runnable {
        final /* synthetic */ String a;
        final /* synthetic */ AdColonyAdViewListener b;

        b(String str, AdColonyAdViewListener adColonyAdViewListener) {
            this.a = str;
            this.b = adColonyAdViewListener;
        }

        public void run() {
            AdColonyZone adColonyZone;
            if (!a.e()) {
                adColonyZone = null;
            } else {
                adColonyZone = (AdColonyZone) a.c().w().get(this.a);
            }
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(this.a);
            }
            this.b.onRequestNotFilled(adColonyZone);
        }
    }

    static class c implements Runnable {
        final /* synthetic */ AdColonyInterstitial a;

        c(AdColonyInterstitial adColonyInterstitial) {
            this.a = adColonyInterstitial;
        }

        public void run() {
            AdColonyInterstitialListener listener = this.a.getListener();
            this.a.a(true);
            if (listener != null) {
                listener.onExpiring(this.a);
            }
        }
    }

    static class d implements Runnable {
        final /* synthetic */ h a;

        d(h hVar) {
            this.a = hVar;
        }

        public void run() {
            ArrayList arrayList = new ArrayList();
            Iterator it = this.a.m().b().iterator();
            while (it.hasNext()) {
                arrayList.add((a0) it.next());
            }
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                a0 a0Var = (a0) it2.next();
                this.a.a(a0Var.d());
                if (a0Var instanceof m0) {
                    m0 m0Var = (m0) a0Var;
                    if (!m0Var.s()) {
                        m0Var.loadUrl("about:blank");
                        m0Var.clearCache(true);
                        m0Var.removeAllViews();
                        m0Var.a(true);
                    }
                }
            }
        }
    }

    static class e implements Runnable {
        final /* synthetic */ double a;
        final /* synthetic */ String b;
        final /* synthetic */ String c;
        final /* synthetic */ String d;

        e(double d2, String str, String str2, String str3) {
            this.a = d2;
            this.b = str;
            this.c = str2;
            this.d = str3;
        }

        public void run() {
            AdColony.a();
            JSONObject b2 = s.b();
            double d2 = this.a;
            if (d2 >= Utils.DOUBLE_EPSILON) {
                s.a(b2, "price", d2);
            }
            String str = this.b;
            if (str != null && str.length() <= 3) {
                s.a(b2, "currency_code", this.b);
            }
            s.a(b2, "product_id", this.c);
            s.a(b2, "transaction_id", this.d);
            new x("AdColony.on_iap_report", 1, b2).d();
        }
    }

    static class f implements Runnable {
        final /* synthetic */ AdColonyAdViewListener a;
        final /* synthetic */ String b;
        final /* synthetic */ AdColonyAdSize c;
        final /* synthetic */ AdColonyAdOptions d;

        f(AdColonyAdViewListener adColonyAdViewListener, String str, AdColonyAdSize adColonyAdSize, AdColonyAdOptions adColonyAdOptions) {
            this.a = adColonyAdViewListener;
            this.b = str;
            this.c = adColonyAdSize;
            this.d = adColonyAdOptions;
        }

        public void run() {
            h c2 = a.c();
            if (c2.y() || c2.z()) {
                AdColony.b();
                AdColony.a(this.a, this.b);
            }
            if (!AdColony.a() && a.d()) {
                AdColony.a(this.a, this.b);
            }
            if (((AdColonyZone) c2.w().get(this.b)) == null) {
                new AdColonyZone(this.b);
                new a().a("Zone info for ").a(this.b).a(" doesn't exist in hashmap").a(u.d);
            }
            c2.b().a(this.b, this.a, this.c, this.d);
        }
    }

    static class g implements Runnable {
        final /* synthetic */ AdColonyAppOptions a;

        g(AdColonyAppOptions adColonyAppOptions) {
            this.a = adColonyAppOptions;
        }

        public void run() {
            AdColony.a();
            JSONObject b = s.b();
            s.a(b, "options", this.a.b());
            new x("Options.set_options", 1, b).d();
        }
    }

    static class h implements Runnable {
        final /* synthetic */ String a;

        h(String str) {
            this.a = str;
        }

        public void run() {
            AdColony.a();
            JSONObject b = s.b();
            s.a(b, "type", this.a);
            new x("CustomMessage.register", 1, b).d();
        }
    }

    static class i implements Runnable {
        final /* synthetic */ String a;

        i(String str) {
            this.a = str;
        }

        public void run() {
            AdColony.a();
            JSONObject b = s.b();
            s.a(b, "type", this.a);
            new x("CustomMessage.unregister", 1, b).d();
        }
    }

    static class j implements Runnable {
        j() {
        }

        public void run() {
            AdColony.a();
            for (String str : a.c().g().keySet()) {
                JSONObject b = s.b();
                s.a(b, "type", str);
                new x("CustomMessage.unregister", 1, b).d();
            }
        }
    }

    static class k implements Runnable {
        final /* synthetic */ AdColonyInterstitialListener a;
        final /* synthetic */ String b;
        final /* synthetic */ AdColonyAdOptions c;

        class a implements Runnable {
            final /* synthetic */ AdColonyZone a;

            a(AdColonyZone adColonyZone) {
                this.a = adColonyZone;
            }

            public void run() {
                k.this.a.onRequestNotFilled(this.a);
            }
        }

        k(AdColonyInterstitialListener adColonyInterstitialListener, String str, AdColonyAdOptions adColonyAdOptions) {
            this.a = adColonyInterstitialListener;
            this.b = str;
            this.c = adColonyAdOptions;
        }

        public void run() {
            h c2 = a.c();
            if (c2.y() || c2.z()) {
                AdColony.b();
                AdColony.a(this.a, this.b);
            } else if (AdColony.a() || !a.d()) {
                AdColonyZone adColonyZone = (AdColonyZone) c2.w().get(this.b);
                if (adColonyZone == null) {
                    adColonyZone = new AdColonyZone(this.b);
                    a a2 = new a().a("Zone info for ");
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.b);
                    sb.append(" doesn't exist in hashmap");
                    a2.a(sb.toString()).a(u.d);
                }
                if (adColonyZone.getZoneType() == 2 || adColonyZone.getZoneType() == 1) {
                    k0.a((Runnable) new a(adColonyZone));
                } else {
                    c2.b().a(this.b, this.a, this.c);
                }
            } else {
                AdColony.a(this.a, this.b);
            }
        }
    }

    private static boolean a(Context context, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        if (d0.a(0, null)) {
            new a().a("Cannot configure AdColony; configuration mechanism requires 5 ").a("seconds between attempts.").a(u.g);
            return false;
        }
        if (context == null) {
            context = a.b();
        }
        if (context == null) {
            new a().a("Ignoring call to AdColony.configure() as the provided Activity or ").a("Application context is null and we do not currently hold a ").a("reference to either for our use.").a(u.g);
            return false;
        }
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        if (adColonyAppOptions == null) {
            adColonyAppOptions = new AdColonyAppOptions();
        }
        if (a.e() && !s.d(a.c().q().b(), "reconfigurable")) {
            h c2 = a.c();
            if (!c2.q().a().equals(str)) {
                new a().a("Ignoring call to AdColony.configure() as the app id does not ").a("match what was used during the initial configuration.").a(u.g);
                return false;
            } else if (k0.a(strArr, c2.q().c())) {
                new a().a("Ignoring call to AdColony.configure() as the same zone ids ").a("were used during the previous configuration.").a(u.g);
                return true;
            }
        }
        adColonyAppOptions.a(str);
        adColonyAppOptions.a(strArr);
        adColonyAppOptions.f();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss:SSS", Locale.US);
        long currentTimeMillis = System.currentTimeMillis();
        String format = simpleDateFormat.format(new Date(currentTimeMillis));
        boolean z = true;
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (strArr[i2] != null && !strArr[i2].equals("")) {
                z = false;
            }
        }
        if (str.equals("") || z) {
            new a().a("AdColony.configure() called with an empty app or zone id String.").a(u.i);
            return false;
        }
        a.c = true;
        if (VERSION.SDK_INT < 14) {
            new a().a("The minimum API level for the AdColony SDK is 14.").a(u.g);
            a.a(context, adColonyAppOptions, true);
        } else {
            a.a(context, adColonyAppOptions, false);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(a.c().t().f());
        sb.append("/adc3/AppInfo");
        String sb2 = sb.toString();
        JSONObject b2 = s.b();
        if (new File(sb2).exists()) {
            b2 = s.c(sb2);
        }
        JSONObject b3 = s.b();
        if (s.h(b2, "appId").equals(str)) {
            s.a(b3, "zoneIds", s.a(s.c(b2, "zoneIds"), strArr, true));
            s.a(b3, "appId", str);
        } else {
            s.a(b3, "zoneIds", s.a(strArr));
            s.a(b3, "appId", str);
        }
        s.i(b3, sb2);
        a a2 = new a().a("Configure: Total Time (ms): ");
        StringBuilder sb3 = new StringBuilder();
        sb3.append("");
        sb3.append(System.currentTimeMillis() - currentTimeMillis);
        a a3 = a2.a(sb3.toString());
        StringBuilder sb4 = new StringBuilder();
        sb4.append(" and started at ");
        sb4.append(format);
        a3.a(sb4.toString()).a(u.h);
        return true;
    }

    public static boolean addCustomMessageListener(@NonNull AdColonyCustomMessageListener adColonyCustomMessageListener, String str) {
        if (!a.f()) {
            new a().a("Ignoring call to AdColony.addCustomMessageListener as AdColony ").a("has not yet been configured.").a(u.g);
            return false;
        } else if (!k0.h(str)) {
            new a().a("Ignoring call to AdColony.addCustomMessageListener.").a(u.g);
            return false;
        } else {
            try {
                a.c().g().put(str, adColonyCustomMessageListener);
                a.execute(new h(str));
                return true;
            } catch (RejectedExecutionException unused) {
                return false;
            }
        }
    }

    static void b() {
        new a().a("The AdColony API is not available while AdColony is disabled.").a(u.i);
    }

    public static boolean clearCustomMessageListeners() {
        if (!a.f()) {
            new a().a("Ignoring call to AdColony.clearCustomMessageListeners as AdColony").a(" has not yet been configured.").a(u.g);
            return false;
        }
        a.c().g().clear();
        a.execute(new j());
        return true;
    }

    public static boolean configure(Activity activity, @NonNull String str, @NonNull String... strArr) {
        return a(activity, null, str, strArr);
    }

    public static boolean disable() {
        if (!a.f()) {
            return false;
        }
        Context b2 = a.b();
        if (b2 != null && (b2 instanceof b)) {
            ((Activity) b2).finish();
        }
        h c2 = a.c();
        for (AdColonyInterstitial cVar : c2.b().a().values()) {
            k0.a((Runnable) new c(cVar));
        }
        k0.a((Runnable) new d(c2));
        a.c().a(true);
        return true;
    }

    public static AdColonyAppOptions getAppOptions() {
        if (!a.f()) {
            return null;
        }
        return a.c().q();
    }

    public static AdColonyCustomMessageListener getCustomMessageListener(@NonNull String str) {
        if (!a.f()) {
            return null;
        }
        return (AdColonyCustomMessageListener) a.c().g().get(str);
    }

    public static AdColonyRewardListener getRewardListener() {
        if (!a.f()) {
            return null;
        }
        return a.c().r();
    }

    public static String getSDKVersion() {
        if (!a.f()) {
            return "";
        }
        return a.c().h().E();
    }

    public static AdColonyZone getZone(@NonNull String str) {
        if (!a.f()) {
            new a().a("Ignoring call to AdColony.getZone() as AdColony has not yet been ").a("configured.").a(u.g);
            return null;
        }
        HashMap w = a.c().w();
        if (w.containsKey(str)) {
            return (AdColonyZone) w.get(str);
        }
        AdColonyZone adColonyZone = new AdColonyZone(str);
        a.c().w().put(str, adColonyZone);
        return adColonyZone;
    }

    public static boolean notifyIAPComplete(@NonNull String str, @NonNull String str2) {
        return notifyIAPComplete(str, str2, null, Utils.DOUBLE_EPSILON);
    }

    public static boolean removeCustomMessageListener(@NonNull String str) {
        if (!a.f()) {
            new a().a("Ignoring call to AdColony.removeCustomMessageListener as AdColony").a(" has not yet been configured.").a(u.g);
            return false;
        }
        a.c().g().remove(str);
        a.execute(new i(str));
        return true;
    }

    public static boolean removeRewardListener() {
        if (!a.f()) {
            new a().a("Ignoring call to AdColony.removeRewardListener() as AdColony has ").a("not yet been configured.").a(u.g);
            return false;
        }
        a.c().a((AdColonyRewardListener) null);
        return true;
    }

    public static boolean requestAdView(@NonNull String str, @NonNull AdColonyAdViewListener adColonyAdViewListener, @NonNull AdColonyAdSize adColonyAdSize) {
        return requestAdView(str, adColonyAdViewListener, adColonyAdSize, null);
    }

    public static boolean requestInterstitial(@NonNull String str, @NonNull AdColonyInterstitialListener adColonyInterstitialListener) {
        return requestInterstitial(str, adColonyInterstitialListener, null);
    }

    public static boolean setAppOptions(@NonNull AdColonyAppOptions adColonyAppOptions) {
        if (!a.f()) {
            new a().a("Ignoring call to AdColony.setAppOptions() as AdColony has not yet").a(" been configured.").a(u.g);
            return false;
        }
        a.c().b(adColonyAppOptions);
        adColonyAppOptions.f();
        try {
            a.execute(new g(adColonyAppOptions));
            return true;
        } catch (RejectedExecutionException unused) {
            return false;
        }
    }

    public static boolean setRewardListener(@NonNull AdColonyRewardListener adColonyRewardListener) {
        if (!a.f()) {
            new a().a("Ignoring call to AdColony.setRewardListener() as AdColony has not").a(" yet been configured.").a(u.g);
            return false;
        }
        a.c().a(adColonyRewardListener);
        return true;
    }

    public static boolean configure(Activity activity, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        return a(activity, adColonyAppOptions, str, strArr);
    }

    public static boolean notifyIAPComplete(@NonNull String str, @NonNull String str2, String str3, @FloatRange(from = 0.0d) double d2) {
        if (!a.f()) {
            new a().a("Ignoring call to notifyIAPComplete as AdColony has not yet been ").a("configured.").a(u.g);
            return false;
        } else if (!k0.h(str) || !k0.h(str2)) {
            new a().a("Ignoring call to notifyIAPComplete as one of the passed Strings ").a("is greater than ").a(128).a(" characters.").a(u.g);
            return false;
        } else {
            if (str3 != null && str3.length() > 3) {
                new a().a("You are trying to report an IAP event with a currency String ").a("containing more than 3 characters.").a(u.g);
            }
            e eVar = new e(d2, str3, str, str2);
            a.execute(eVar);
            return true;
        }
    }

    public static boolean requestAdView(@NonNull String str, @NonNull AdColonyAdViewListener adColonyAdViewListener, @NonNull AdColonyAdSize adColonyAdSize, AdColonyAdOptions adColonyAdOptions) {
        if (!a.f()) {
            new a().a("Ignoring call to requestAdView as AdColony has not yet been").a(" configured.").a(u.g);
            a(adColonyAdViewListener, str);
            return false;
        } else if (adColonyAdSize.getHeight() <= 0 || adColonyAdSize.getWidth() <= 0) {
            new a().a("Ignoring call to requestAdView as you've provided an AdColonyAdSize").a(" object with an invalid width or height.").a(u.g);
            return false;
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("zone_id", str);
            if (d0.a(1, bundle)) {
                a(adColonyAdViewListener, str);
                return false;
            }
            try {
                a.execute(new f(adColonyAdViewListener, str, adColonyAdSize, adColonyAdOptions));
                return true;
            } catch (RejectedExecutionException unused) {
                a(adColonyAdViewListener, str);
                return false;
            }
        }
    }

    public static boolean requestInterstitial(@NonNull String str, @NonNull AdColonyInterstitialListener adColonyInterstitialListener, AdColonyAdOptions adColonyAdOptions) {
        if (!a.f()) {
            new a().a("Ignoring call to AdColony.requestInterstitial as AdColony has not").a(" yet been configured.").a(u.g);
            adColonyInterstitialListener.onRequestNotFilled(new AdColonyZone(str));
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("zone_id", str);
        if (d0.a(1, bundle)) {
            AdColonyZone adColonyZone = (AdColonyZone) a.c().w().get(str);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(str);
                a a2 = new a().a("Zone info for ");
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(" doesn't exist in hashmap");
                a2.a(sb.toString()).a(u.d);
            }
            adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
            return false;
        }
        try {
            a.execute(new k(adColonyInterstitialListener, str, adColonyAdOptions));
            return true;
        } catch (RejectedExecutionException unused) {
            a(adColonyInterstitialListener, str);
            return false;
        }
    }

    public static boolean configure(Application application, @NonNull String str, @NonNull String... strArr) {
        return configure(application, (AdColonyAppOptions) null, str, strArr);
    }

    public static boolean configure(Application application, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        return a(application, adColonyAppOptions, str, strArr);
    }

    static boolean a() {
        b bVar = new b(15.0d);
        h c2 = a.c();
        while (!c2.A() && !bVar.a()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException unused) {
            }
        }
        return c2.A();
    }

    static boolean a(AdColonyInterstitialListener adColonyInterstitialListener, String str) {
        if (adColonyInterstitialListener != null && a.d()) {
            k0.a((Runnable) new a(str, adColonyInterstitialListener));
        }
        return false;
    }

    static boolean a(AdColonyAdViewListener adColonyAdViewListener, String str) {
        if (adColonyAdViewListener != null && a.d()) {
            k0.a((Runnable) new b(str, adColonyAdViewListener));
        }
        return false;
    }

    static void a(Context context, AdColonyAppOptions adColonyAppOptions) {
        if (adColonyAppOptions != null && context != null) {
            String b2 = k0.b(context);
            String e2 = k0.e();
            int f2 = k0.f();
            String h2 = a.c().h().h();
            String str = "none";
            if (a.c().n().c()) {
                str = "wifi";
            } else if (a.c().n().b()) {
                str = TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE;
            }
            HashMap hashMap = new HashMap();
            hashMap.put("sessionId", "unknown");
            hashMap.put("advertiserId", "unknown");
            StringBuilder sb = new StringBuilder();
            sb.append(Locale.getDefault().getDisplayLanguage());
            sb.append(" (");
            sb.append(Locale.getDefault().getDisplayCountry());
            sb.append(")");
            hashMap.put("countryLocale", sb.toString());
            hashMap.put("countryLocalShort", a.c().h().k());
            hashMap.put("manufacturer", a.c().h().x());
            hashMap.put("model", a.c().h().A());
            hashMap.put("osVersion", a.c().h().C());
            hashMap.put("carrierName", h2);
            hashMap.put("networkType", str);
            hashMap.put(TapjoyConstants.TJC_PLATFORM, "android");
            hashMap.put("appName", b2);
            hashMap.put(RequestParameters.APPLICATION_VERSION_NAME, e2);
            hashMap.put("appBuildNumber", Integer.valueOf(f2));
            StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(adColonyAppOptions.a());
            hashMap.put("appId", sb2.toString());
            hashMap.put("apiLevel", Integer.valueOf(VERSION.SDK_INT));
            hashMap.put(GeneralPropertiesWorker.SDK_VERSION, a.c().h().E());
            hashMap.put("controllerVersion", "unknown");
            hashMap.put("zoneIds", adColonyAppOptions.d());
            JSONObject mediationInfo = adColonyAppOptions.getMediationInfo();
            JSONObject pluginInfo = adColonyAppOptions.getPluginInfo();
            if (!s.h(mediationInfo, "mediation_network").equals("")) {
                hashMap.put("mediationNetwork", s.h(mediationInfo, "mediation_network"));
                hashMap.put("mediationNetworkVersion", s.h(mediationInfo, "mediation_network_version"));
            }
            if (!s.h(pluginInfo, TapjoyConstants.TJC_PLUGIN).equals("")) {
                hashMap.put(TapjoyConstants.TJC_PLUGIN, s.h(pluginInfo, TapjoyConstants.TJC_PLUGIN));
                hashMap.put("pluginVersion", s.h(pluginInfo, "plugin_version"));
            }
            w.a(hashMap);
        }
    }
}
