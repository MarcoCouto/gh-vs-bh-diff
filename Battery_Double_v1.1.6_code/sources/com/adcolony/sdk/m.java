package com.adcolony.sdk;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

class m implements a {
    private BlockingQueue<Runnable> a = new LinkedBlockingQueue();
    private ThreadPoolExecutor b;
    private LinkedList<l> c;
    private String d;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            m mVar = m.this;
            mVar.a(new l(xVar, mVar));
        }
    }

    class b implements z {
        b() {
        }

        public void a(x xVar) {
            m mVar = m.this;
            mVar.a(new l(xVar, mVar));
        }
    }

    class c implements z {
        c() {
        }

        public void a(x xVar) {
            m mVar = m.this;
            mVar.a(new l(xVar, mVar));
        }
    }

    m() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4, 16, 60, TimeUnit.SECONDS, this.a);
        this.b = threadPoolExecutor;
        this.c = new LinkedList<>();
        this.d = a.c().h().I();
    }

    /* access modifiers changed from: 0000 */
    public void a(l lVar) {
        String str = this.d;
        if (str == null || str.equals("")) {
            this.c.push(lVar);
            return;
        }
        try {
            this.b.execute(lVar);
        } catch (RejectedExecutionException unused) {
            a a2 = new a().a("RejectedExecutionException: ThreadPoolExecutor unable to ");
            StringBuilder sb = new StringBuilder();
            sb.append("execute download for url ");
            sb.append(lVar.m);
            a2.a(sb.toString()).a(u.j);
            a(lVar, lVar.a(), null);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a.a("WebServices.download", (z) new a());
        a.a("WebServices.get", (z) new b());
        a.a("WebServices.post", (z) new c());
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.d = str;
        while (!this.c.isEmpty()) {
            a((l) this.c.removeLast());
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        this.b.setCorePoolSize(i);
    }

    /* access modifiers changed from: 0000 */
    public int a() {
        return this.b.getCorePoolSize();
    }

    public void a(l lVar, x xVar, Map<String, List<String>> map) {
        JSONObject b2 = s.b();
        s.a(b2, "url", lVar.m);
        s.b(b2, "success", lVar.o);
        s.b(b2, "status", lVar.q);
        s.a(b2, TtmlNode.TAG_BODY, lVar.n);
        s.b(b2, "size", lVar.p);
        if (map != null) {
            JSONObject b3 = s.b();
            for (Entry entry : map.entrySet()) {
                String obj = ((List) entry.getValue()).toString();
                String substring = obj.substring(1, obj.length() - 1);
                if (entry.getKey() != null) {
                    s.a(b3, (String) entry.getKey(), substring);
                }
            }
            s.a(b2, "headers", b3);
        }
        xVar.a(b2).d();
    }
}
