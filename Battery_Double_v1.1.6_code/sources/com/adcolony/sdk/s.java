package com.adcolony.sdk;

import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class s {
    s() {
    }

    static JSONObject a(String str, String str2) {
        String str3;
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            if (str2 == null) {
                str3 = "";
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                sb.append(": ");
                sb.append(e.toString());
                str3 = sb.toString();
            }
            new a().a(str3).a(u.j);
            return new JSONObject();
        }
    }

    static JSONObject b() {
        return new JSONObject();
    }

    static JSONObject c(JSONArray jSONArray, int i) {
        JSONObject optJSONObject = jSONArray.optJSONObject(i);
        return optJSONObject == null ? new JSONObject() : optJSONObject;
    }

    static String d(JSONArray jSONArray, int i) {
        return jSONArray.optString(i);
    }

    static double e(JSONObject jSONObject, String str) {
        return jSONObject.optDouble(str, Utils.DOUBLE_EPSILON);
    }

    static int f(JSONObject jSONObject, String str) {
        return jSONObject.optInt(str);
    }

    static JSONObject g(JSONObject jSONObject, String str) {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        return optJSONObject == null ? new JSONObject() : optJSONObject;
    }

    static String h(JSONObject jSONObject, String str) {
        return jSONObject.optString(str);
    }

    static boolean i(JSONObject jSONObject, String str) {
        try {
            a.c().k().a(str, jSONObject.toString(), false);
            return true;
        } catch (IOException e) {
            new a().a("IOException in ADCJSON's saveObject: ").a(e.toString()).a(u.j);
            return false;
        }
    }

    static JSONObject b(String str) {
        return a(str, (String) null);
    }

    static boolean d(JSONObject jSONObject, String str) {
        return jSONObject.optBoolean(str);
    }

    static void e(JSONArray jSONArray, int i) {
        jSONArray.put(i);
    }

    static Object b(JSONObject jSONObject, String str) {
        Object opt = jSONObject.opt(str);
        return opt == null ? Boolean.valueOf(false) : opt;
    }

    static JSONArray c(JSONObject jSONObject, String str) {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        return optJSONArray == null ? new JSONArray() : optJSONArray;
    }

    static Object b(JSONArray jSONArray, int i) {
        Object opt = jSONArray.opt(i);
        return opt == null ? Boolean.valueOf(false) : opt;
    }

    static JSONObject c(String str) {
        try {
            String sb = a.c().k().a(str, false).toString();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("loadObject from filepath ");
            sb2.append(str);
            return a(sb, sb2.toString());
        } catch (IOException e) {
            new a().a("IOException in ADCJSON's loadObject: ").a(e.toString()).a(u.j);
            return b();
        }
    }

    static boolean b(JSONObject jSONObject, String str, int i) {
        try {
            jSONObject.put(str, i);
            return true;
        } catch (JSONException e) {
            new a().a("JSON error in ADCJSON putInteger(): ").a(e.toString()).a(u.j);
            return false;
        }
    }

    static JSONArray a() {
        return new JSONArray();
    }

    static JSONArray a(String str) {
        try {
            return new JSONArray(str);
        } catch (JSONException e) {
            new a().a(e.toString()).a(u.j);
            return new JSONArray();
        }
    }

    static boolean b(JSONObject jSONObject, String str, boolean z) {
        try {
            jSONObject.put(str, z);
            return true;
        } catch (JSONException e) {
            new a().a("JSON error in ADCJSON putBoolean(): ").a(e.toString()).a(u.j);
            return false;
        }
    }

    static JSONObject a(JSONArray jSONArray, int i) {
        try {
            return jSONArray.getJSONObject(i);
        } catch (JSONException e) {
            new a().a(e.toString()).a(u.j);
            return new JSONObject();
        }
    }

    static void b(JSONArray jSONArray, String str) {
        jSONArray.put(str);
    }

    static int a(JSONObject jSONObject, String str, int i) {
        return jSONObject.optInt(str, i);
    }

    static boolean a(JSONObject jSONObject, String str, boolean z) {
        return jSONObject.optBoolean(str, z);
    }

    static boolean a(JSONObject jSONObject, String str, String str2) {
        try {
            jSONObject.put(str, str2);
            return true;
        } catch (JSONException e) {
            new a().a("JSON error in ADCJSON putString(): ").a(e.toString()).a(u.j);
            return false;
        }
    }

    static boolean a(JSONObject jSONObject, String str, long j) {
        try {
            jSONObject.put(str, j);
            return true;
        } catch (JSONException e) {
            new a().a("JSON error in ADCJSON putLong(): ").a(e.toString()).a(u.j);
            return false;
        }
    }

    static boolean a(JSONObject jSONObject, String str, JSONArray jSONArray) {
        try {
            jSONObject.put(str, jSONArray);
            return true;
        } catch (JSONException e) {
            new a().a("JSON error in ADCJSON putArray(): ").a(e.toString()).a(u.j);
            return false;
        }
    }

    static boolean a(JSONObject jSONObject, String str, JSONObject jSONObject2) {
        try {
            jSONObject.put(str, jSONObject2);
            return true;
        } catch (JSONException e) {
            new a().a("JSON error in ADCJSON putObject(): ").a(e.toString()).a(u.j);
            return false;
        }
    }

    static boolean a(JSONObject jSONObject, String str, double d) {
        try {
            jSONObject.put(str, d);
            return true;
        } catch (JSONException e) {
            new a().a("JSON error in ADCJSON putDouble(): ").a(e.toString()).a(u.j);
            return false;
        }
    }

    static void a(JSONArray jSONArray, boolean z) {
        jSONArray.put(z);
    }

    static void a(JSONArray jSONArray, Object obj) {
        jSONArray.put(obj);
    }

    static String[] a(JSONArray jSONArray) {
        String[] strArr = new String[jSONArray.length()];
        for (int i = 0; i < jSONArray.length(); i++) {
            strArr[i] = d(jSONArray, i);
        }
        return strArr;
    }

    static JSONArray a(String[] strArr) {
        JSONArray a = a();
        for (String b : strArr) {
            b(a, b);
        }
        return a;
    }

    static boolean a(JSONArray jSONArray, String str) {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (d(jSONArray, i).equals(str)) {
                return true;
            }
        }
        return false;
    }

    static boolean a(JSONObject jSONObject, String str) {
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            if (str.equals(keys.next())) {
                return true;
            }
        }
        return false;
    }

    static JSONArray a(JSONArray jSONArray, String[] strArr, boolean z) {
        for (String str : strArr) {
            if (!z || !a(jSONArray, str)) {
                b(jSONArray, str);
            }
        }
        return jSONArray;
    }

    static JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) {
        try {
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                jSONObject.put(str, jSONObject2.get(str));
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
