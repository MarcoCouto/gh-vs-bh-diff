package com.adcolony.sdk;

class u {
    static u c = new u(3, false);
    static u d = new u(3, true);
    static u e = new u(2, false);
    static u f = new u(2, true);
    static u g = new u(1, false);
    static u h = new u(1, true);
    static u i = new u(0, false);
    static u j = new u(0, true);
    private int a;
    private boolean b;

    static class a {
        StringBuilder a = new StringBuilder();

        a() {
        }

        /* access modifiers changed from: 0000 */
        public a a(char c) {
            if (c != 10) {
                this.a.append(c);
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(String str) {
            this.a.append(str);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(Object obj) {
            if (obj != null) {
                this.a.append(obj.toString());
            } else {
                this.a.append("null");
            }
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(double d) {
            k0.a(d, 2, this.a);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(int i) {
            this.a.append(i);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(boolean z) {
            this.a.append(z);
            return this;
        }

        /* access modifiers changed from: 0000 */
        public void a(u uVar) {
            uVar.a(this.a.toString());
        }
    }

    u(int i2, boolean z) {
        this.a = i2;
        this.b = z;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        w.a(this.a, str, this.b);
    }
}
