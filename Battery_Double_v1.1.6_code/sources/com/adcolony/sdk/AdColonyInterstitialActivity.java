package com.adcolony.sdk;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyInterstitialActivity extends b {
    AdColonyInterstitial m;
    private g n;

    public AdColonyInterstitialActivity() {
        AdColonyInterstitial adColonyInterstitial;
        if (!a.e()) {
            adColonyInterstitial = null;
        } else {
            adColonyInterstitial = a.c().d();
        }
        this.m = adColonyInterstitial;
    }

    /* access modifiers changed from: 0000 */
    public void a(x xVar) {
        super.a(xVar);
        d b = a.c().b();
        JSONObject g = s.g(xVar.b(), "v4iap");
        JSONArray c = s.c(g, "product_ids");
        if (g != null) {
            AdColonyInterstitial adColonyInterstitial = this.m;
            if (!(adColonyInterstitial == null || adColonyInterstitial.getListener() == null || c.length() <= 0)) {
                this.m.getListener().onIAPEvent(this.m, s.d(c, 0), s.f(g, "engagement_type"));
            }
        }
        b.a(this.a);
        if (this.m != null) {
            b.a().remove(this.m.b());
        }
        AdColonyInterstitial adColonyInterstitial2 = this.m;
        if (!(adColonyInterstitial2 == null || adColonyInterstitial2.getListener() == null)) {
            this.m.getListener().onClosed(this.m);
            this.m.a((c) null);
            this.m.setListener(null);
            this.m = null;
        }
        g gVar = this.n;
        if (gVar != null) {
            gVar.a();
            this.n = null;
        }
        new a().a("finish_ad call finished").a(u.f);
    }

    public /* bridge */ /* synthetic */ void onBackPressed() {
        super.onBackPressed();
    }

    public /* bridge */ /* synthetic */ void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        AdColonyInterstitial adColonyInterstitial = this.m;
        this.b = adColonyInterstitial == null ? -1 : adColonyInterstitial.f();
        super.onCreate(bundle);
        if (a.e()) {
            AdColonyInterstitial adColonyInterstitial2 = this.m;
            if (adColonyInterstitial2 != null) {
                c0 e = adColonyInterstitial2.e();
                if (e != null) {
                    e.a(this.a);
                }
                this.n = new g(new Handler(Looper.getMainLooper()), this.m);
                if (this.m.getListener() != null) {
                    this.m.getListener().onOpened(this.m);
                }
            }
        }
    }

    public /* bridge */ /* synthetic */ void onDestroy() {
        super.onDestroy();
    }

    public /* bridge */ /* synthetic */ void onPause() {
        super.onPause();
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    public /* bridge */ /* synthetic */ void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    /* access modifiers changed from: 0000 */
    public void a(AdColonyInterstitial adColonyInterstitial) {
        this.m = adColonyInterstitial;
    }
}
