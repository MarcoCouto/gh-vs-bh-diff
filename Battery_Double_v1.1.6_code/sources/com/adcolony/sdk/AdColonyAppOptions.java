package com.adcolony.sdk;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.util.Log;
import com.facebook.AccessToken;
import com.tapjoy.TapjoyConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyAppOptions {
    public static final String ADMARVEL = "AdMarvel";
    public static final String ADMOB = "AdMob";
    public static final String ADOBEAIR = "Adobe AIR";
    public static final String AERSERVE = "AerServe";
    public static final int ALL = 2;
    public static final String APPODEAL = "Appodeal";
    public static final String COCOS2DX = "Cocos2d-x";
    public static final String CORONA = "Corona";
    public static final String FUSEPOWERED = "Fuse Powered";
    public static final String FYBER = "Fyber";
    public static final String IRONSOURCE = "ironSource";
    public static final int LANDSCAPE = 1;
    public static final String MOPUB = "MoPub";
    public static final int PORTRAIT = 0;
    @Deprecated
    public static final int SENSOR = 2;
    public static final String UNITY = "Unity";
    String a = "";
    String[] b;
    JSONArray c = s.a();
    JSONObject d = s.b();
    AdColonyUserMetadata e;

    public AdColonyAppOptions() {
        setOriginStore("google");
        if (a.e()) {
            h c2 = a.c();
            if (c2.x()) {
                a(c2.q().a);
                a(c2.q().b);
            }
        }
    }

    public static AdColonyAppOptions getMoPubAppOptions(@NonNull String str) {
        String str2 = "AdColonyMoPub";
        AdColonyAppOptions mediationNetwork = new AdColonyAppOptions().setMediationNetwork(MOPUB, "1.0");
        if (str != null && !str.isEmpty()) {
            String[] split = str.split(",");
            int length = split.length;
            int i = 0;
            while (i < length) {
                String[] split2 = split[i].split(":");
                if (split2.length == 2) {
                    String str3 = split2[0];
                    char c2 = 65535;
                    int hashCode = str3.hashCode();
                    if (hashCode != 109770977) {
                        if (hashCode == 351608024 && str3.equals("version")) {
                            c2 = 1;
                        }
                    } else if (str3.equals("store")) {
                        c2 = 0;
                    }
                    if (c2 == 0) {
                        mediationNetwork.setOriginStore(split2[1]);
                    } else if (c2 != 1) {
                        Log.e(str2, "AdColony client options in wrong format - please check your MoPub dashboard");
                        return mediationNetwork;
                    } else {
                        mediationNetwork.setAppVersion(split2[1]);
                    }
                    i++;
                } else {
                    Log.e(str2, "AdColony client options not recognized - please check your MoPub dashboard");
                    return null;
                }
            }
        }
        return mediationNetwork;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAppOptions a(String str) {
        if (str == null) {
            return this;
        }
        this.a = str;
        s.a(this.d, "app_id", str);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public JSONObject b() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public String[] c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public JSONArray d() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        setOption("bundle_id", a.c().h().D());
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        if (s.a(this.d, "use_forced_controller")) {
            m0.N = s.d(this.d, "use_forced_controller");
        }
        if (s.a(this.d, "use_staging_launch_server") && s.d(this.d, "use_staging_launch_server")) {
            h.W = "https://adc3-launch-staging.adcolony.com/v4/launch";
        }
    }

    public int getAppOrientation() {
        return s.a(this.d, "app_orientation", -1);
    }

    public String getAppVersion() {
        return s.h(this.d, TapjoyConstants.TJC_APP_VERSION_NAME);
    }

    public String getGDPRConsentString() {
        return s.h(this.d, "consent_string");
    }

    public boolean getGDPRRequired() {
        return s.d(this.d, "gdpr_required");
    }

    public boolean getKeepScreenOn() {
        return s.d(this.d, "keep_screen_on");
    }

    public JSONObject getMediationInfo() {
        JSONObject b2 = s.b();
        s.a(b2, "name", s.h(this.d, "mediation_network"));
        s.a(b2, "version", s.h(this.d, "mediation_network_version"));
        return b2;
    }

    public boolean getMultiWindowEnabled() {
        return s.d(this.d, "multi_window_enabled");
    }

    public Object getOption(@NonNull String str) {
        return s.b(this.d, str);
    }

    public String getOriginStore() {
        return s.h(this.d, "origin_store");
    }

    public JSONObject getPluginInfo() {
        JSONObject b2 = s.b();
        s.a(b2, "name", s.h(this.d, TapjoyConstants.TJC_PLUGIN));
        s.a(b2, "version", s.h(this.d, "plugin_version"));
        return b2;
    }

    public int getRequestedAdOrientation() {
        return s.a(this.d, "orientation", -1);
    }

    public boolean getTestModeEnabled() {
        return s.d(this.d, "test_mode");
    }

    public String getUserID() {
        return s.h(this.d, AccessToken.USER_ID_KEY);
    }

    public AdColonyUserMetadata getUserMetadata() {
        return this.e;
    }

    public AdColonyAppOptions setAppOrientation(@IntRange(from = 0, to = 2) int i) {
        setOption("app_orientation", (double) i);
        return this;
    }

    public AdColonyAppOptions setAppVersion(@NonNull String str) {
        if (k0.h(str)) {
            setOption(TapjoyConstants.TJC_APP_VERSION_NAME, str);
        }
        return this;
    }

    public AdColonyAppOptions setGDPRConsentString(@NonNull String str) {
        s.a(this.d, "consent_string", str);
        return this;
    }

    public AdColonyAppOptions setGDPRRequired(boolean z) {
        setOption("gdpr_required", z);
        return this;
    }

    public AdColonyAppOptions setKeepScreenOn(boolean z) {
        s.b(this.d, "keep_screen_on", z);
        return this;
    }

    public AdColonyAppOptions setMediationNetwork(@NonNull String str, @NonNull String str2) {
        if (k0.h(str) && k0.h(str2)) {
            s.a(this.d, "mediation_network", str);
            s.a(this.d, "mediation_network_version", str2);
        }
        return this;
    }

    public AdColonyAppOptions setMultiWindowEnabled(boolean z) {
        s.b(this.d, "multi_window_enabled", z);
        return this;
    }

    public AdColonyAppOptions setOption(@NonNull String str, boolean z) {
        if (k0.h(str)) {
            s.b(this.d, str, z);
        }
        return this;
    }

    public AdColonyAppOptions setOriginStore(@NonNull String str) {
        if (k0.h(str)) {
            setOption("origin_store", str);
        }
        return this;
    }

    public AdColonyAppOptions setPlugin(@NonNull String str, @NonNull String str2) {
        if (k0.h(str) && k0.h(str2)) {
            s.a(this.d, TapjoyConstants.TJC_PLUGIN, str);
            s.a(this.d, "plugin_version", str2);
        }
        return this;
    }

    public AdColonyAppOptions setRequestedAdOrientation(@IntRange(from = 0, to = 2) int i) {
        setOption("orientation", (double) i);
        return this;
    }

    public AdColonyAppOptions setTestModeEnabled(boolean z) {
        s.b(this.d, "test_mode", z);
        return this;
    }

    public AdColonyAppOptions setUserID(@NonNull String str) {
        if (k0.h(str)) {
            setOption(AccessToken.USER_ID_KEY, str);
        }
        return this;
    }

    public AdColonyAppOptions setUserMetadata(@NonNull AdColonyUserMetadata adColonyUserMetadata) {
        this.e = adColonyUserMetadata;
        s.a(this.d, "user_metadata", adColonyUserMetadata.b);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public AdColonyAppOptions a(String... strArr) {
        if (strArr == null) {
            return this;
        }
        this.b = strArr;
        this.c = s.a();
        for (String b2 : strArr) {
            s.b(this.c, b2);
        }
        return this;
    }

    public AdColonyAppOptions setOption(@NonNull String str, double d2) {
        if (k0.h(str)) {
            s.a(this.d, str, d2);
        }
        return this;
    }

    public AdColonyAppOptions setOption(@NonNull String str, @NonNull String str2) {
        if (str != null && k0.h(str) && k0.h(str2)) {
            s.a(this.d, str, str2);
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.a;
    }
}
