package com.adcolony.sdk;

import android.content.Context;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

class l implements Runnable {
    private HttpURLConnection a;
    private InputStream b;
    private x c;
    private a d;
    private final int e = 4096;
    private final int f = 299;
    private String g;
    private int h = 0;
    private boolean i = false;
    private Map<String, List<String>> j;
    private String k = "";
    private String l = "";
    String m = "";
    String n = "";
    boolean o;
    int p;
    int q;

    interface a {
        void a(l lVar, x xVar, Map<String, List<String>> map);
    }

    l(x xVar, a aVar) {
        this.c = xVar;
        this.d = aVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0059, code lost:
        r0 = "UTF-8";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005d, code lost:
        if (r8.g == null) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0065, code lost:
        if (r8.g.isEmpty() != false) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0067, code lost:
        r0 = r8.g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
        if ((r10 instanceof java.io.ByteArrayOutputStream) == false) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006d, code lost:
        r8.n = ((java.io.ByteArrayOutputStream) r10).toString(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0077, code lost:
        if (r10 == null) goto L_0x007c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0079, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007c, code lost:
        if (r9 == null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007e, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0081, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0084, code lost:
        return true;
     */
    private boolean a(InputStream inputStream, OutputStream outputStream) throws Exception {
        Throwable th;
        BufferedInputStream bufferedInputStream;
        Throwable e2;
        BufferedInputStream bufferedInputStream2 = null;
        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = bufferedInputStream.read(bArr, 0, 4096);
                    if (read == -1) {
                        break;
                    }
                    int i2 = this.p + read;
                    this.p = i2;
                    if (this.i) {
                        if (i2 > this.h) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Data exceeds expected maximum (");
                            sb.append(this.p);
                            sb.append("/");
                            sb.append(this.h);
                            sb.append("): ");
                            sb.append(this.a.getURL().toString());
                            throw new Exception(sb.toString());
                        }
                    }
                    outputStream.write(bArr, 0, read);
                }
            } catch (Exception e3) {
                e2 = e3;
                try {
                    throw e2;
                } catch (Throwable th2) {
                    BufferedInputStream bufferedInputStream3 = bufferedInputStream;
                    th = th2;
                    bufferedInputStream2 = bufferedInputStream3;
                }
            }
        } catch (Exception e4) {
            Throwable th3 = e4;
            bufferedInputStream = null;
            e2 = th3;
            throw e2;
        } catch (Throwable th4) {
            th = th4;
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (bufferedInputStream2 != null) {
                bufferedInputStream2.close();
            }
            throw th;
        }
    }

    private boolean b() throws IOException {
        JSONObject b2 = this.c.b();
        String h2 = s.h(b2, Param.CONTENT_TYPE);
        String h3 = s.h(b2, "content");
        boolean d2 = s.d(b2, "no_redirect");
        this.m = s.h(b2, "url");
        this.k = s.h(b2, "filepath");
        StringBuilder sb = new StringBuilder();
        sb.append(a.c().t().d());
        String str = this.k;
        sb.append(str.substring(str.lastIndexOf("/") + 1));
        this.l = sb.toString();
        this.g = s.h(b2, "encoding");
        int a2 = s.a(b2, "max_size", 0);
        this.h = a2;
        this.i = a2 != 0;
        this.p = 0;
        this.b = null;
        this.a = null;
        this.j = null;
        if (!this.m.startsWith("file://")) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.m).openConnection();
            this.a = httpURLConnection;
            httpURLConnection.setInstanceFollowRedirects(!d2);
            this.a.setRequestProperty(HttpRequest.HEADER_ACCEPT_CHARSET, "UTF-8");
            String I = a.c().h().I();
            if (I != null && !I.equals("")) {
                this.a.setRequestProperty("User-Agent", I);
            }
            if (!h2.equals("")) {
                this.a.setRequestProperty("Content-Type", h2);
            }
            if (this.c.c().equals("WebServices.post")) {
                this.a.setDoOutput(true);
                this.a.setFixedLengthStreamingMode(h3.getBytes("UTF-8").length);
                new PrintStream(this.a.getOutputStream()).print(h3);
            }
        } else if (this.m.startsWith("file:///android_asset/")) {
            Context b3 = a.b();
            if (b3 != null) {
                this.b = b3.getAssets().open(this.m.substring(22));
            }
        } else {
            this.b = new FileInputStream(this.m.substring(7));
        }
        if (this.a == null && this.b == null) {
            return false;
        }
        return true;
    }

    private boolean c() throws Exception {
        OutputStream outputStream;
        String c2 = this.c.c();
        if (this.b != null) {
            outputStream = this.k.length() == 0 ? new ByteArrayOutputStream(4096) : new FileOutputStream(new File(this.k).getAbsolutePath());
        } else if (c2.equals("WebServices.download")) {
            this.b = this.a.getInputStream();
            outputStream = new FileOutputStream(this.l);
        } else if (c2.equals("WebServices.get")) {
            this.b = this.a.getInputStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else if (c2.equals("WebServices.post")) {
            this.a.connect();
            this.b = (this.a.getResponseCode() < 200 || this.a.getResponseCode() > 299) ? this.a.getErrorStream() : this.a.getInputStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else {
            outputStream = null;
        }
        HttpURLConnection httpURLConnection = this.a;
        if (httpURLConnection != null) {
            this.q = httpURLConnection.getResponseCode();
            this.j = this.a.getHeaderFields();
        }
        return a(this.b, outputStream);
    }

    public void run() {
        boolean z = false;
        this.o = false;
        try {
            if (b()) {
                this.o = c();
                if (this.c.c().equals("WebServices.post") && this.q != 200) {
                    this.o = false;
                }
            }
        } catch (MalformedURLException e2) {
            new a().a("MalformedURLException: ").a(e2.toString()).a(u.j);
            this.o = true;
        } catch (OutOfMemoryError unused) {
            a a2 = new a().a("Out of memory error - disabling AdColony. (").a(this.p).a("/").a(this.h);
            StringBuilder sb = new StringBuilder();
            sb.append("): ");
            sb.append(this.m);
            a2.a(sb.toString()).a(u.i);
            a.c().a(true);
        } catch (IOException e3) {
            new a().a("Download of ").a(this.m).a(" failed: ").a(e3.toString()).a(u.h);
            int i2 = this.q;
            if (i2 == 0) {
                i2 = 504;
            }
            this.q = i2;
        } catch (IllegalStateException e4) {
            new a().a("okhttp error: ").a(e4.toString()).a(u.i);
            e4.printStackTrace();
        } catch (Exception e5) {
            new a().a("Exception: ").a(e5.toString()).a(u.i);
            e5.printStackTrace();
        }
        z = true;
        if (this.o) {
            new a().a("Downloaded ").a(this.m).a(u.f);
        }
        if (z) {
            if (this.c.c().equals("WebServices.download")) {
                a(this.l, this.k);
            }
            this.d.a(this, this.c, this.j);
        }
    }

    private void a(String str, String str2) {
        try {
            String substring = str2.substring(0, str2.lastIndexOf("/") + 1);
            if (str2 != null && !"".equals(str2) && !substring.equals(a.c().t().d()) && !new File(str).renameTo(new File(str2))) {
                a a2 = new a().a("Moving of ");
                if (str == null) {
                    str = "temp folder's asset file";
                }
                a2.a(str).a(" failed.").a(u.h);
            }
        } catch (Exception e2) {
            new a().a("Exception: ").a(e2.toString()).a(u.i);
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: 0000 */
    public x a() {
        return this.c;
    }
}
