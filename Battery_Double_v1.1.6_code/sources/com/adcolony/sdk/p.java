package com.adcolony.sdk;

import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class p {
    private LinkedList<Runnable> a = new LinkedList<>();
    private boolean b;

    class a implements z {

        /* renamed from: com.adcolony.sdk.p$a$a reason: collision with other inner class name */
        class C0006a implements Runnable {
            final /* synthetic */ x a;

            C0006a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.e(this.a);
                p.this.a();
            }
        }

        a() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new C0006a(xVar));
        }
    }

    class b implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.a(this.a, new File(s.h(this.a.b(), "filepath")));
                p.this.a();
            }
        }

        b() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new a(xVar));
        }
    }

    class c implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.b(this.a);
                p.this.a();
            }
        }

        c() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new a(xVar));
        }
    }

    class d implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.c(this.a);
                p.this.a();
            }
        }

        d() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new a(xVar));
        }
    }

    class e implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.d(this.a);
                p.this.a();
            }
        }

        e() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new a(xVar));
        }
    }

    class f implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.a(this.a);
                p.this.a();
            }
        }

        f() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new a(xVar));
        }
    }

    class g implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.g(this.a);
                p.this.a();
            }
        }

        g() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new a(xVar));
        }
    }

    class h implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.h(this.a);
                p.this.a();
            }
        }

        h() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new a(xVar));
        }
    }

    class i implements z {

        class a implements Runnable {
            final /* synthetic */ x a;

            a(x xVar) {
                this.a = xVar;
            }

            public void run() {
                p.this.f(this.a);
                p.this.a();
            }
        }

        i() {
        }

        public void a(x xVar) {
            p.this.a((Runnable) new a(xVar));
        }
    }

    p() {
    }

    /* access modifiers changed from: private */
    public boolean f(x xVar) {
        String h2 = s.h(xVar.b(), "filepath");
        a.c().t().g();
        JSONObject b2 = s.b();
        try {
            if (new File(h2).mkdir()) {
                s.b(b2, "success", true);
                xVar.a(b2).d();
                return true;
            }
            s.b(b2, "success", false);
            return false;
        } catch (Exception unused) {
            s.b(b2, "success", false);
            xVar.a(b2).d();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean g(x xVar) {
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "filepath");
        a.c().t().g();
        JSONObject b3 = s.b();
        try {
            int f2 = s.f(b2, "offset");
            int f3 = s.f(b2, "size");
            boolean d2 = s.d(b2, "gunzip");
            String h3 = s.h(b2, "output_filepath");
            InputStream h0Var = new h0(new FileInputStream(h2), f2, f3);
            InputStream gZIPInputStream = d2 ? new GZIPInputStream(h0Var, 1024) : h0Var;
            if (h3.equals("")) {
                StringBuilder sb = new StringBuilder(gZIPInputStream.available());
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = gZIPInputStream.read(bArr, 0, 1024);
                    if (read < 0) {
                        break;
                    }
                    sb.append(new String(bArr, 0, read, "ISO-8859-1"));
                }
                s.b(b3, "size", sb.length());
                s.a(b3, "data", sb.toString());
            } else {
                FileOutputStream fileOutputStream = new FileOutputStream(h3);
                byte[] bArr2 = new byte[1024];
                int i2 = 0;
                while (true) {
                    int read2 = gZIPInputStream.read(bArr2, 0, 1024);
                    if (read2 < 0) {
                        break;
                    }
                    fileOutputStream.write(bArr2, 0, read2);
                    i2 += read2;
                }
                fileOutputStream.close();
                s.b(b3, "size", i2);
            }
            gZIPInputStream.close();
            s.b(b3, "success", true);
            xVar.a(b3).d();
            return true;
        } catch (IOException unused) {
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return false;
        } catch (OutOfMemoryError unused2) {
            new a().a("Out of memory error - disabling AdColony.").a(u.i);
            a.c().a(true);
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00f4, code lost:
        new com.adcolony.sdk.u.a().a("Out of memory error - disabling AdColony.").a(com.adcolony.sdk.u.i);
        com.adcolony.sdk.a.c().a(true);
        com.adcolony.sdk.s.b(r4, "success", false);
        r0.a(r4).d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0119, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[ExcHandler: OutOfMemoryError (unused java.lang.OutOfMemoryError), SYNTHETIC, Splitter:B:1:0x0027] */
    public boolean h(x xVar) {
        boolean z;
        x xVar2 = xVar;
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "filepath");
        String h3 = s.h(b2, "bundle_path");
        JSONArray c2 = s.c(b2, "bundle_filenames");
        a.c().t().g();
        JSONObject b3 = s.b();
        try {
            File file = new File(h3);
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            byte[] bArr = new byte[32];
            randomAccessFile.readInt();
            int readInt = randomAccessFile.readInt();
            JSONArray jSONArray = new JSONArray();
            byte[] bArr2 = new byte[1024];
            int i2 = 0;
            while (i2 < readInt) {
                randomAccessFile.seek((long) ((i2 * 44) + 8));
                randomAccessFile.read(bArr);
                new String(bArr);
                randomAccessFile.readInt();
                int readInt2 = randomAccessFile.readInt();
                int readInt3 = randomAccessFile.readInt();
                jSONArray.put(readInt3);
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(h2);
                    sb.append(c2.get(i2));
                    String sb2 = sb.toString();
                    JSONArray jSONArray2 = c2;
                    String str = h2;
                    randomAccessFile.seek((long) readInt2);
                    FileOutputStream fileOutputStream = new FileOutputStream(sb2);
                    int i3 = readInt3 / 1024;
                    int i4 = readInt3 % 1024;
                    for (int i5 = 0; i5 < i3; i5++) {
                        randomAccessFile.read(bArr2, 0, 1024);
                        fileOutputStream.write(bArr2, 0, 1024);
                    }
                    randomAccessFile.read(bArr2, 0, i4);
                    fileOutputStream.write(bArr2, 0, i4);
                    fileOutputStream.close();
                    i2++;
                    h2 = str;
                    c2 = jSONArray2;
                } catch (JSONException unused) {
                    new a().a("Could extract file name at index ").a(i2).a(" unpacking ad unit bundle at ").a(h3).a(u.i);
                    z = false;
                    s.b(b3, "success", false);
                    xVar2.a(b3).d();
                    return false;
                }
            }
            randomAccessFile.close();
            file.delete();
            s.b(b3, "success", true);
            s.a(b3, "file_sizes", jSONArray);
            xVar2.a(b3).d();
            return true;
        } catch (IOException unused2) {
            z = false;
            new a().a("Failed to find or open ad unit bundle at path: ").a(h3).a(u.j);
            s.b(b3, "success", z);
            xVar2.a(b3).d();
            return z;
        } catch (OutOfMemoryError unused3) {
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean d(x xVar) {
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "filepath");
        String h3 = s.h(b2, "new_filepath");
        a.c().t().g();
        JSONObject b3 = s.b();
        try {
            if (new File(h2).renameTo(new File(h3))) {
                s.b(b3, "success", true);
                xVar.a(b3).d();
                return true;
            }
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return false;
        } catch (Exception unused) {
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean e(x xVar) {
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "filepath");
        String h3 = s.h(b2, "data");
        String h4 = s.h(b2, "encoding");
        boolean z = h4 != null && h4.equals("utf8");
        a.c().t().g();
        JSONObject b3 = s.b();
        try {
            a(h2, h3, z);
            s.b(b3, "success", true);
            xVar.a(b3).d();
            return true;
        } catch (IOException unused) {
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, String str2, boolean z) throws IOException {
        BufferedWriter bufferedWriter = z ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(str), "UTF-8")) : new BufferedWriter(new OutputStreamWriter(new FileOutputStream(str)));
        bufferedWriter.write(str2);
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a.a("FileSystem.save", (z) new a());
        a.a("FileSystem.delete", (z) new b());
        a.a("FileSystem.listing", (z) new c());
        a.a("FileSystem.load", (z) new d());
        a.a("FileSystem.rename", (z) new e());
        a.a("FileSystem.exists", (z) new f());
        a.a("FileSystem.extract", (z) new g());
        a.a("FileSystem.unpack_bundle", (z) new h());
        a.a("FileSystem.create_directory", (z) new i());
    }

    /* access modifiers changed from: 0000 */
    public String c(x xVar) {
        JSONObject b2 = xVar.b();
        String h2 = s.h(b2, "filepath");
        String h3 = s.h(b2, "encoding");
        boolean z = h3 != null && h3.equals("utf8");
        a.c().t().g();
        JSONObject b3 = s.b();
        try {
            StringBuilder a2 = a(h2, z);
            s.b(b3, "success", true);
            s.a(b3, "data", a2.toString());
            xVar.a(b3).d();
            return a2.toString();
        } catch (IOException unused) {
            s.b(b3, "success", false);
            xVar.a(b3).d();
            return "";
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(x xVar, File file) {
        a.c().t().g();
        JSONObject b2 = s.b();
        if (a(file)) {
            s.b(b2, "success", true);
            xVar.a(b2).d();
            return true;
        }
        s.b(b2, "success", false);
        xVar.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(File file) {
        try {
            if (!file.isDirectory()) {
                return file.delete();
            }
            if (file.list().length == 0) {
                return file.delete();
            }
            String[] list = file.list();
            if (list.length > 0) {
                return a(new File(file, list[0]));
            }
            if (file.list().length == 0) {
                return file.delete();
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public StringBuilder a(String str, boolean z) throws IOException {
        BufferedReader bufferedReader;
        File file = new File(str);
        StringBuilder sb = new StringBuilder((int) file.length());
        if (z) {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath())));
        }
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
                sb.append("\n");
            } else {
                bufferedReader.close();
                return sb;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(x xVar) {
        String h2 = s.h(xVar.b(), "filepath");
        a.c().t().g();
        JSONObject b2 = s.b();
        try {
            boolean a2 = a(h2);
            s.b(b2, IronSourceConstants.EVENTS_RESULT, a2);
            s.b(b2, "success", true);
            xVar.a(b2).d();
            return a2;
        } catch (Exception e2) {
            s.b(b2, IronSourceConstants.EVENTS_RESULT, false);
            s.b(b2, "success", false);
            xVar.a(b2).d();
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(String str) throws Exception {
        return new File(str).exists();
    }

    /* access modifiers changed from: 0000 */
    public void a(Runnable runnable) {
        if (!this.a.isEmpty() || this.b) {
            this.a.push(runnable);
            return;
        }
        this.b = true;
        runnable.run();
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.b = false;
        if (!this.a.isEmpty()) {
            this.b = true;
            ((Runnable) this.a.removeLast()).run();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b(x xVar) {
        String h2 = s.h(xVar.b(), "filepath");
        a.c().t().g();
        JSONObject b2 = s.b();
        String[] list = new File(h2).list();
        if (list != null) {
            JSONArray a2 = s.a();
            for (String str : list) {
                JSONObject b3 = s.b();
                s.a(b3, "filename", str);
                StringBuilder sb = new StringBuilder();
                sb.append(h2);
                sb.append(str);
                if (new File(sb.toString()).isDirectory()) {
                    s.b(b3, "is_folder", true);
                } else {
                    s.b(b3, "is_folder", false);
                }
                s.a(a2, (Object) b3);
            }
            s.b(b2, "success", true);
            s.a(b2, "entries", a2);
            xVar.a(b2).d();
            return true;
        }
        s.b(b2, "success", false);
        xVar.a(b2).d();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public List<String> b(String str, boolean z) throws IOException {
        BufferedReader bufferedReader;
        File file = new File(str);
        file.length();
        ArrayList arrayList = new ArrayList();
        if (z) {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath())));
        }
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                arrayList.add(readLine);
            } else {
                bufferedReader.close();
                return arrayList;
            }
        }
    }
}
