package com.adcolony.sdk;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings.System;
import com.google.android.exoplayer2.util.MimeTypes;
import org.json.JSONObject;

class g extends ContentObserver {
    private AudioManager a;
    private AdColonyInterstitial b;

    public g(Handler handler, AdColonyInterstitial adColonyInterstitial) {
        super(handler);
        Context b2 = a.b();
        if (b2 != null) {
            this.a = (AudioManager) b2.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            this.b = adColonyInterstitial;
            b2.getApplicationContext().getContentResolver().registerContentObserver(System.CONTENT_URI, true, this);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        Context b2 = a.b();
        if (b2 != null) {
            b2.getApplicationContext().getContentResolver().unregisterContentObserver(this);
        }
        this.b = null;
        this.a = null;
    }

    public boolean deliverSelfNotifications() {
        return false;
    }

    public void onChange(boolean z) {
        if (this.a != null) {
            AdColonyInterstitial adColonyInterstitial = this.b;
            if (adColonyInterstitial != null && adColonyInterstitial.c() != null) {
                double streamVolume = (double) ((((float) this.a.getStreamVolume(3)) / 15.0f) * 100.0f);
                JSONObject b2 = s.b();
                s.a(b2, "audio_percentage", streamVolume);
                s.a(b2, "ad_session_id", this.b.c().a());
                s.b(b2, "id", this.b.c().c());
                new x("AdContainer.on_audio_change", this.b.c().k(), b2).d();
                new a().a("Volume changed to ").a(streamVolume).a(u.f);
            }
        }
    }
}
