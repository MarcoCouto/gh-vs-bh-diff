package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import com.google.android.exoplayer2.util.MimeTypes;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

@SuppressLint({"AppCompatCustomView"})
class j0 extends Button {
    private c A;
    private x B;
    private final int a = 0;
    private final int b = 1;
    private final int c = 2;
    private final int d = 3;
    private final int e = 1;
    private final int f = 2;
    private final int g = 3;
    private final int h = 0;
    private final int i = 1;
    private final int j = 2;
    private final int k = 1;
    private final int l = 2;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    private String z;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.a(xVar);
            }
        }
    }

    class b implements z {
        b() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.k(xVar);
            }
        }
    }

    class c implements z {
        c() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.e(xVar);
            }
        }
    }

    class d implements z {
        d() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.f(xVar);
            }
        }
    }

    class e implements z {
        e() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.d(xVar);
            }
        }
    }

    class f implements z {
        f() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.j(xVar);
            }
        }
    }

    class g implements z {
        g() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.g(xVar);
            }
        }
    }

    class h implements z {
        h() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.h(xVar);
            }
        }
    }

    class i implements z {
        i() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.b(xVar);
            }
        }
    }

    class j implements z {
        j() {
        }

        public void a(x xVar) {
            if (j0.this.c(xVar)) {
                j0.this.i(xVar);
            }
        }
    }

    private j0(Context context) {
        super(context);
    }

    /* access modifiers changed from: 0000 */
    public int a(boolean z2, int i2) {
        if (i2 == 0) {
            return z2 ? 1 : 16;
        }
        if (i2 != 1) {
            if (i2 != 2) {
                return 17;
            }
            if (z2) {
                return GravityCompat.END;
            }
            return 80;
        } else if (z2) {
            return GravityCompat.START;
        } else {
            return 48;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(x xVar) {
        JSONObject b2 = xVar.b();
        this.u = s.f(b2, AvidJSONUtil.KEY_X);
        this.v = s.f(b2, AvidJSONUtil.KEY_Y);
        setGravity(a(true, this.u) | a(false, this.v));
    }

    /* access modifiers changed from: 0000 */
    public void b(x xVar) {
        JSONObject b2 = s.b();
        s.a(b2, MimeTypes.BASE_TYPE_TEXT, getText().toString());
        xVar.a(b2).d();
    }

    /* access modifiers changed from: 0000 */
    public boolean c(x xVar) {
        JSONObject b2 = xVar.b();
        return s.f(b2, "id") == this.m && s.f(b2, "container_id") == this.A.c() && s.h(b2, "ad_session_id").equals(this.A.a());
    }

    /* access modifiers changed from: 0000 */
    public void d(x xVar) {
        String h2 = s.h(xVar.b(), "background_color");
        this.w = h2;
        setBackgroundColor(k0.j(h2));
    }

    /* access modifiers changed from: 0000 */
    public void e(x xVar) {
        JSONObject b2 = xVar.b();
        this.n = s.f(b2, AvidJSONUtil.KEY_X);
        this.o = s.f(b2, AvidJSONUtil.KEY_Y);
        this.p = s.f(b2, "width");
        this.q = s.f(b2, "height");
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.n, this.o, 0, 0);
        layoutParams.width = this.p;
        layoutParams.height = this.q;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public void f(x xVar) {
        String h2 = s.h(xVar.b(), "font_color");
        this.x = h2;
        setTextColor(k0.j(h2));
    }

    /* access modifiers changed from: 0000 */
    public void g(x xVar) {
        int f2 = s.f(xVar.b(), "font_size");
        this.t = f2;
        setTextSize((float) f2);
    }

    /* access modifiers changed from: 0000 */
    public void h(x xVar) {
        int f2 = s.f(xVar.b(), "font_style");
        this.r = f2;
        switch (f2) {
            case 0:
                setTypeface(getTypeface(), 0);
                return;
            case 1:
                setTypeface(getTypeface(), 1);
                return;
            case 2:
                setTypeface(getTypeface(), 2);
                return;
            case 3:
                setTypeface(getTypeface(), 3);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: 0000 */
    public void i(x xVar) {
        String h2 = s.h(xVar.b(), MimeTypes.BASE_TYPE_TEXT);
        this.y = h2;
        setText(h2);
    }

    /* access modifiers changed from: 0000 */
    public void j(x xVar) {
        int f2 = s.f(xVar.b(), "font_family");
        this.s = f2;
        switch (f2) {
            case 0:
                setTypeface(Typeface.DEFAULT);
                return;
            case 1:
                setTypeface(Typeface.SERIF);
                return;
            case 2:
                setTypeface(Typeface.SANS_SERIF);
                return;
            case 3:
                setTypeface(Typeface.MONOSPACE);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: 0000 */
    public void k(x xVar) {
        if (s.d(xVar.b(), String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        h c2 = a.c();
        d b2 = c2.b();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject b3 = s.b();
        s.b(b3, "view_id", this.m);
        s.a(b3, "ad_session_id", this.z);
        s.b(b3, "container_x", this.n + x2);
        s.b(b3, "container_y", this.o + y2);
        s.b(b3, "view_x", x2);
        s.b(b3, "view_y", y2);
        s.b(b3, "id", this.A.getId());
        switch (action) {
            case 0:
                new x("AdContainer.on_touch_began", this.A.k(), b3).d();
                break;
            case 1:
                if (!this.A.p()) {
                    c2.a((AdColonyAdView) b2.b().get(this.z));
                }
                if (x2 > 0 && x2 < getWidth() && y2 > 0 && y2 < getHeight()) {
                    new x("AdContainer.on_touch_ended", this.A.k(), b3).d();
                    break;
                } else {
                    new x("AdContainer.on_touch_cancelled", this.A.k(), b3).d();
                    break;
                }
                break;
            case 2:
                new x("AdContainer.on_touch_moved", this.A.k(), b3).d();
                break;
            case 3:
                new x("AdContainer.on_touch_cancelled", this.A.k(), b3).d();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(b3, "container_x", ((int) motionEvent.getX(action2)) + this.n);
                s.b(b3, "container_y", ((int) motionEvent.getY(action2)) + this.o);
                s.b(b3, "view_x", (int) motionEvent.getX(action2));
                s.b(b3, "view_y", (int) motionEvent.getY(action2));
                new x("AdContainer.on_touch_began", this.A.k(), b3).d();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                int x3 = (int) motionEvent.getX(action3);
                int y3 = (int) motionEvent.getY(action3);
                s.b(b3, "container_x", ((int) motionEvent.getX(action3)) + this.n);
                s.b(b3, "container_y", ((int) motionEvent.getY(action3)) + this.o);
                s.b(b3, "view_x", (int) motionEvent.getX(action3));
                s.b(b3, "view_y", (int) motionEvent.getY(action3));
                if (!this.A.p()) {
                    c2.a((AdColonyAdView) b2.b().get(this.z));
                }
                if (x3 > 0 && x3 < getWidth() && y3 > 0 && y3 < getHeight()) {
                    new x("AdContainer.on_touch_ended", this.A.k(), b3).d();
                    break;
                } else {
                    new x("AdContainer.on_touch_cancelled", this.A.k(), b3).d();
                    break;
                }
                break;
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        int i2;
        int i3;
        JSONObject b2 = this.B.b();
        this.z = s.h(b2, "ad_session_id");
        this.n = s.f(b2, AvidJSONUtil.KEY_X);
        this.o = s.f(b2, AvidJSONUtil.KEY_Y);
        this.p = s.f(b2, "width");
        this.q = s.f(b2, "height");
        this.s = s.f(b2, "font_family");
        this.r = s.f(b2, "font_style");
        this.t = s.f(b2, "font_size");
        this.w = s.h(b2, "background_color");
        this.x = s.h(b2, "font_color");
        this.y = s.h(b2, MimeTypes.BASE_TYPE_TEXT);
        this.u = s.f(b2, "align_x");
        this.v = s.f(b2, "align_y");
        h c2 = a.c();
        if (this.y.equals("")) {
            this.y = "Learn More";
        }
        setVisibility(4);
        LayoutParams layoutParams = s.d(b2, "wrap_content") ? new LayoutParams(-2, -2) : new LayoutParams(this.p, this.q);
        layoutParams.gravity = 0;
        setText(this.y);
        setTextSize((float) this.t);
        if (s.d(b2, "overlay")) {
            this.n = 0;
            this.o = 0;
            i3 = (int) (c2.h().n() * 6.0f);
            i2 = (int) (c2.h().n() * 6.0f);
            int n2 = (int) (c2.h().n() * 4.0f);
            setPadding(n2, n2, n2, n2);
            layoutParams.gravity = 8388693;
        } else {
            i3 = 0;
            i2 = 0;
        }
        layoutParams.setMargins(this.n, this.o, i3, i2);
        this.A.addView(this, layoutParams);
        switch (this.s) {
            case 0:
                setTypeface(Typeface.DEFAULT);
                break;
            case 1:
                setTypeface(Typeface.SERIF);
                break;
            case 2:
                setTypeface(Typeface.SANS_SERIF);
                break;
            case 3:
                setTypeface(Typeface.MONOSPACE);
                break;
        }
        switch (this.r) {
            case 0:
                setTypeface(getTypeface(), 0);
                break;
            case 1:
                setTypeface(getTypeface(), 1);
                break;
            case 2:
                setTypeface(getTypeface(), 2);
                break;
            case 3:
                setTypeface(getTypeface(), 3);
                break;
        }
        setGravity(a(true, this.u) | a(false, this.v));
        if (!this.w.equals("")) {
            setBackgroundColor(k0.j(this.w));
        }
        if (!this.x.equals("")) {
            setTextColor(k0.j(this.x));
        }
        this.A.i().add(a.a("TextView.set_visible", (z) new b(), true));
        this.A.i().add(a.a("TextView.set_bounds", (z) new c(), true));
        this.A.i().add(a.a("TextView.set_font_color", (z) new d(), true));
        this.A.i().add(a.a("TextView.set_background_color", (z) new e(), true));
        this.A.i().add(a.a("TextView.set_typeface", (z) new f(), true));
        this.A.i().add(a.a("TextView.set_font_size", (z) new g(), true));
        this.A.i().add(a.a("TextView.set_font_style", (z) new h(), true));
        this.A.i().add(a.a("TextView.get_text", (z) new i(), true));
        this.A.i().add(a.a("TextView.set_text", (z) new j(), true));
        this.A.i().add(a.a("TextView.align", (z) new a(), true));
        this.A.j().add("TextView.set_visible");
        this.A.j().add("TextView.set_bounds");
        this.A.j().add("TextView.set_font_color");
        this.A.j().add("TextView.set_background_color");
        this.A.j().add("TextView.set_typeface");
        this.A.j().add("TextView.set_font_size");
        this.A.j().add("TextView.set_font_style");
        this.A.j().add("TextView.get_text");
        this.A.j().add("TextView.set_text");
        this.A.j().add("TextView.align");
        new a().a("TextView added to layout").a(u.f);
    }

    j0(Context context, x xVar, int i2, c cVar) {
        super(context);
        this.m = i2;
        this.B = xVar;
        this.A = cVar;
    }

    j0(Context context, int i2, x xVar, int i3, c cVar) {
        super(context, null, i2);
        this.m = i3;
        this.B = xVar;
        this.A = cVar;
    }
}
