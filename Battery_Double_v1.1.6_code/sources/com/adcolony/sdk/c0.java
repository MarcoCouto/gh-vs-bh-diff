package com.adcolony.sdk;

import android.webkit.WebView;
import com.facebook.appevents.AppEventsConstants;
import com.iab.omid.library.adcolony.adsession.AdEvents;
import com.iab.omid.library.adcolony.adsession.AdSession;
import com.iab.omid.library.adcolony.adsession.AdSessionConfiguration;
import com.iab.omid.library.adcolony.adsession.AdSessionContext;
import com.iab.omid.library.adcolony.adsession.Owner;
import com.iab.omid.library.adcolony.adsession.VerificationScriptResource;
import com.iab.omid.library.adcolony.adsession.video.InteractionType;
import com.iab.omid.library.adcolony.adsession.video.Position;
import com.iab.omid.library.adcolony.adsession.video.VastProperties;
import com.iab.omid.library.adcolony.adsession.video.VideoEvents;
import com.tapjoy.TapjoyConstants;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

class c0 {
    private AdSessionContext a;
    private AdSessionConfiguration b;
    private AdSession c;
    private AdEvents d;
    private VideoEvents e;
    private AdColonyCustomMessageListener f;
    private List<VerificationScriptResource> g = new ArrayList();
    /* access modifiers changed from: private */
    public int h = -1;
    /* access modifiers changed from: private */
    public String i = "";
    private int j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    private int p;
    private int q;
    private String r = "";
    /* access modifiers changed from: private */
    public String s = "";

    class a implements Runnable {
        final /* synthetic */ String a;

        a(String str) {
            this.a = str;
        }

        public void run() {
            JSONObject b2 = s.b();
            JSONObject b3 = s.b();
            s.b(b3, "session_type", c0.this.h);
            s.a(b3, TapjoyConstants.TJC_SESSION_ID, c0.this.i);
            s.a(b3, "event", this.a);
            s.a(b2, "type", "iab_hook");
            s.a(b2, "message", b3.toString());
            new x("CustomMessage.controller_send", 0, b2).d();
        }
    }

    class b implements AdColonyCustomMessageListener {

        class a implements Runnable {
            final /* synthetic */ String a;
            final /* synthetic */ String b;
            final /* synthetic */ float c;

            a(String str, String str2, float f) {
                this.a = str;
                this.b = str2;
                this.c = f;
            }

            public void run() {
                if (this.a.equals(c0.this.s)) {
                    c0.this.a(this.b, this.c);
                    return;
                }
                AdColonyAdView adColonyAdView = (AdColonyAdView) a.c().b().b().get(this.a);
                c0 omidManager = adColonyAdView != null ? adColonyAdView.getOmidManager() : null;
                if (omidManager != null) {
                    omidManager.a(this.b, this.c);
                }
            }
        }

        b() {
        }

        public void onAdColonyCustomMessage(AdColonyCustomMessage adColonyCustomMessage) {
            JSONObject b = s.b(adColonyCustomMessage.getMessage());
            String h = s.h(b, "event_type");
            float floatValue = BigDecimal.valueOf(s.e(b, "duration")).floatValue();
            boolean d = s.d(b, "replay");
            boolean equals = s.h(b, "skip_type").equals("dec");
            String h2 = s.h(b, "asi");
            if (!h.equals("skip") || !equals) {
                if (!d || (!h.equals("start") && !h.equals("first_quartile") && !h.equals("midpoint") && !h.equals("third_quartile") && !h.equals("complete"))) {
                    k0.a((Runnable) new a(h2, h, floatValue));
                }
                return;
            }
            c0.this.o = true;
        }
    }

    c0(JSONObject jSONObject, String str) {
        VerificationScriptResource verificationScriptResource;
        this.h = a(jSONObject);
        this.n = s.d(jSONObject, "skippable");
        this.p = s.f(jSONObject, "skip_offset");
        this.q = s.f(jSONObject, "video_duration");
        JSONArray c2 = s.c(jSONObject, "js_resources");
        JSONArray c3 = s.c(jSONObject, "verification_params");
        JSONArray c4 = s.c(jSONObject, "vendor_keys");
        this.s = str;
        for (int i2 = 0; i2 < c2.length(); i2++) {
            try {
                String d2 = s.d(c3, i2);
                String d3 = s.d(c4, i2);
                URL url = new URL(s.d(c2, i2));
                if (!d2.equals("") && !d3.equals("")) {
                    verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithParameters(d3, url, d2);
                } else if (!d3.equals("")) {
                    verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithoutParameters(d3, url);
                } else {
                    verificationScriptResource = VerificationScriptResource.createVerificationScriptResourceWithoutParameters(url);
                }
                this.g.add(verificationScriptResource);
            } catch (MalformedURLException unused) {
                new a().a("Invalid js resource url passed to Omid").a(u.j);
            }
        }
        try {
            this.r = a.c().k().a(s.h(jSONObject, "filepath"), true).toString();
        } catch (IOException unused2) {
            new a().a("Error loading IAB JS Client").a(u.j);
        }
    }

    private void f() {
        b bVar = new b();
        this.f = bVar;
        AdColony.addCustomMessageListener(bVar, "viewability_ad_event");
    }

    /* access modifiers changed from: 0000 */
    public int d() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        this.l = true;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        AdColony.removeCustomMessageListener("viewability_ad_event");
        this.c.finish();
        b("end_session");
        this.c = null;
    }

    /* access modifiers changed from: 0000 */
    public AdSession c() {
        return this.c;
    }

    private int a(JSONObject jSONObject) {
        if (this.h == -1) {
            this.j = s.f(jSONObject, "ad_unit_type");
            String h2 = s.h(jSONObject, AppEventsConstants.EVENT_PARAM_AD_TYPE);
            int i2 = this.j;
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 1) {
                if (h2.equals("video")) {
                    return 0;
                }
                if (h2.equals("display")) {
                    return 1;
                }
                if (h2.equals("banner_display") || h2.equals("interstitial_display")) {
                    return 2;
                }
            }
        }
        return this.h;
    }

    private void b(String str) {
        k0.b.execute(new a(str));
    }

    private void b(c cVar) {
        b("register_ad_view");
        m0 m0Var = (m0) a.c().v().get(Integer.valueOf(cVar.k()));
        if (m0Var == null && !cVar.n().isEmpty()) {
            m0Var = (m0) ((Entry) cVar.n().entrySet().iterator().next()).getValue();
        }
        AdSession adSession = this.c;
        if (adSession == null || m0Var == null) {
            AdSession adSession2 = this.c;
            if (adSession2 != null) {
                adSession2.registerAdView(cVar);
                cVar.a(this.c);
                b("register_obstructions");
                return;
            }
            return;
        }
        adSession.registerAdView(m0Var);
        m0Var.e();
    }

    /* access modifiers changed from: 0000 */
    public void a(c cVar) {
        VideoEvents videoEvents;
        VastProperties vastProperties;
        if (!this.m && this.h >= 0 && this.c != null) {
            b(cVar);
            f();
            if (this.h != 0) {
                videoEvents = null;
            } else {
                videoEvents = VideoEvents.createVideoEvents(this.c);
            }
            this.e = videoEvents;
            this.c.start();
            this.d = AdEvents.createAdEvents(this.c);
            b("start_session");
            if (this.e != null) {
                Position position = Position.PREROLL;
                if (this.n) {
                    vastProperties = VastProperties.createVastPropertiesForSkippableVideo((float) this.p, true, position);
                } else {
                    vastProperties = VastProperties.createVastPropertiesForNonSkippableVideo(true, position);
                }
                this.e.loaded(vastProperties);
            }
            this.m = true;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() throws IllegalArgumentException {
        a((WebView) null);
    }

    /* access modifiers changed from: 0000 */
    public void a(WebView webView) throws IllegalArgumentException {
        if (this.h >= 0) {
            String str = this.r;
            if (str != null && !str.equals("")) {
                List<VerificationScriptResource> list = this.g;
                if (list == null) {
                    return;
                }
                if (!list.isEmpty() || d() == 2) {
                    h c2 = a.c();
                    Owner owner = Owner.NATIVE;
                    int d2 = d();
                    if (d2 == 0) {
                        this.a = AdSessionContext.createNativeAdSessionContext(c2.p(), this.r, this.g, null);
                        AdSessionConfiguration createAdSessionConfiguration = AdSessionConfiguration.createAdSessionConfiguration(owner, owner, false);
                        this.b = createAdSessionConfiguration;
                        AdSession createAdSession = AdSession.createAdSession(createAdSessionConfiguration, this.a);
                        this.c = createAdSession;
                        this.i = createAdSession.getAdSessionId();
                        b("inject_javascript");
                    } else if (d2 == 1) {
                        this.a = AdSessionContext.createNativeAdSessionContext(c2.p(), this.r, this.g, null);
                        AdSessionConfiguration createAdSessionConfiguration2 = AdSessionConfiguration.createAdSessionConfiguration(owner, null, false);
                        this.b = createAdSessionConfiguration2;
                        AdSession createAdSession2 = AdSession.createAdSession(createAdSessionConfiguration2, this.a);
                        this.c = createAdSession2;
                        this.i = createAdSession2.getAdSessionId();
                        b("inject_javascript");
                    } else if (d2 == 2) {
                        this.a = AdSessionContext.createHtmlAdSessionContext(c2.p(), webView, "");
                        AdSessionConfiguration createAdSessionConfiguration3 = AdSessionConfiguration.createAdSessionConfiguration(owner, null, false);
                        this.b = createAdSessionConfiguration3;
                        AdSession createAdSession3 = AdSession.createAdSession(createAdSessionConfiguration3, this.a);
                        this.c = createAdSession3;
                        this.i = createAdSession3.getAdSessionId();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        a(str, 0.0f);
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, float f2) {
        if (a.d() && this.c != null && (this.e != null || str.equals("start") || str.equals("skip") || str.equals("continue") || str.equals("cancel"))) {
            char c2 = 65535;
            try {
                switch (str.hashCode()) {
                    case -1941887438:
                        if (str.equals("first_quartile")) {
                            c2 = 1;
                            break;
                        }
                        break;
                    case -1710060637:
                        if (str.equals("buffer_start")) {
                            c2 = 12;
                            break;
                        }
                        break;
                    case -1638835128:
                        if (str.equals("midpoint")) {
                            c2 = 2;
                            break;
                        }
                        break;
                    case -1367724422:
                        if (str.equals("cancel")) {
                            c2 = 7;
                            break;
                        }
                        break;
                    case -934426579:
                        if (str.equals("resume")) {
                            c2 = 11;
                            break;
                        }
                        break;
                    case -651914917:
                        if (str.equals("third_quartile")) {
                            c2 = 3;
                            break;
                        }
                        break;
                    case -599445191:
                        if (str.equals("complete")) {
                            c2 = 4;
                            break;
                        }
                        break;
                    case -567202649:
                        if (str.equals("continue")) {
                            c2 = 5;
                            break;
                        }
                        break;
                    case -342650039:
                        if (str.equals("sound_mute")) {
                            c2 = 8;
                            break;
                        }
                        break;
                    case 3532159:
                        if (str.equals("skip")) {
                            c2 = 6;
                            break;
                        }
                        break;
                    case 106440182:
                        if (str.equals("pause")) {
                            c2 = 10;
                            break;
                        }
                        break;
                    case 109757538:
                        if (str.equals("start")) {
                            c2 = 0;
                            break;
                        }
                        break;
                    case 583742045:
                        if (str.equals("in_video_engagement")) {
                            c2 = 14;
                            break;
                        }
                        break;
                    case 823102269:
                        if (str.equals("html5_interaction")) {
                            c2 = 15;
                            break;
                        }
                        break;
                    case 1648173410:
                        if (str.equals("sound_unmute")) {
                            c2 = 9;
                            break;
                        }
                        break;
                    case 1906584668:
                        if (str.equals("buffer_end")) {
                            c2 = 13;
                            break;
                        }
                        break;
                }
                switch (c2) {
                    case 0:
                        this.d.impressionOccurred();
                        if (this.e != null) {
                            VideoEvents videoEvents = this.e;
                            if (f2 <= 0.0f) {
                                f2 = (float) this.q;
                            }
                            videoEvents.start(f2, 1.0f);
                        }
                        b(str);
                        break;
                    case 1:
                        this.e.firstQuartile();
                        b(str);
                        break;
                    case 2:
                        this.e.midpoint();
                        b(str);
                        break;
                    case 3:
                        this.e.thirdQuartile();
                        b(str);
                        break;
                    case 4:
                        this.o = true;
                        this.e.complete();
                        b(str);
                        break;
                    case 5:
                        b(str);
                        b();
                        break;
                    case 6:
                    case 7:
                        if (this.e != null) {
                            this.e.skipped();
                        }
                        b(str);
                        b();
                        break;
                    case 8:
                        this.e.volumeChange(0.0f);
                        b(str);
                        break;
                    case 9:
                        this.e.volumeChange(1.0f);
                        b(str);
                        break;
                    case 10:
                        if (!this.k && !this.l && !this.o) {
                            this.e.pause();
                            b(str);
                            this.k = true;
                            this.l = false;
                            break;
                        }
                    case 11:
                        if (this.k && !this.o) {
                            this.e.resume();
                            b(str);
                            this.k = false;
                            break;
                        }
                    case 12:
                        this.e.bufferStart();
                        b(str);
                        break;
                    case 13:
                        this.e.bufferFinish();
                        b(str);
                        break;
                    case 14:
                    case 15:
                        this.e.adUserInteraction(InteractionType.CLICK);
                        b(str);
                        if (this.l && !this.k && !this.o) {
                            this.e.pause();
                            b("pause");
                            this.k = true;
                            this.l = false;
                            break;
                        }
                }
            } catch (IllegalArgumentException | IllegalStateException e2) {
                a a2 = new a().a("Recording IAB event for ").a(str);
                StringBuilder sb = new StringBuilder();
                sb.append(" caused ");
                sb.append(e2.getClass());
                a2.a(sb.toString()).a(u.h);
            }
        }
    }
}
