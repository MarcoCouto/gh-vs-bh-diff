package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.File;
import org.json.JSONObject;

@SuppressLint({"AppCompatCustomView"})
class q extends ImageView {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private boolean f;
    private boolean g;
    private boolean h;
    private String i;
    private String j;
    private x k;
    private c l;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            if (q.this.a(xVar)) {
                q.this.d(xVar);
            }
        }
    }

    class b implements z {
        b() {
        }

        public void a(x xVar) {
            if (q.this.a(xVar)) {
                q.this.b(xVar);
            }
        }
    }

    class c implements z {
        c() {
        }

        public void a(x xVar) {
            if (q.this.a(xVar)) {
                q.this.c(xVar);
            }
        }
    }

    private q(Context context) {
        super(context);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        h c2 = a.c();
        d b2 = c2.b();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        JSONObject b3 = s.b();
        s.b(b3, "view_id", this.a);
        s.a(b3, "ad_session_id", this.j);
        s.b(b3, "container_x", this.b + x);
        s.b(b3, "container_y", this.c + y);
        s.b(b3, "view_x", x);
        s.b(b3, "view_y", y);
        s.b(b3, "id", this.l.getId());
        switch (action) {
            case 0:
                new x("AdContainer.on_touch_began", this.l.k(), b3).d();
                break;
            case 1:
                if (!this.l.p()) {
                    c2.a((AdColonyAdView) b2.b().get(this.j));
                }
                if (x > 0 && x < this.d && y > 0 && y < this.e) {
                    new x("AdContainer.on_touch_ended", this.l.k(), b3).d();
                    break;
                } else {
                    new x("AdContainer.on_touch_cancelled", this.l.k(), b3).d();
                    break;
                }
                break;
            case 2:
                new x("AdContainer.on_touch_moved", this.l.k(), b3).d();
                break;
            case 3:
                new x("AdContainer.on_touch_cancelled", this.l.k(), b3).d();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(b3, "container_x", ((int) motionEvent.getX(action2)) + this.b);
                s.b(b3, "container_y", ((int) motionEvent.getY(action2)) + this.c);
                s.b(b3, "view_x", (int) motionEvent.getX(action2));
                s.b(b3, "view_y", (int) motionEvent.getY(action2));
                new x("AdContainer.on_touch_began", this.l.k(), b3).d();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                int x2 = (int) motionEvent.getX(action3);
                int y2 = (int) motionEvent.getY(action3);
                s.b(b3, "container_x", ((int) motionEvent.getX(action3)) + this.b);
                s.b(b3, "container_y", ((int) motionEvent.getY(action3)) + this.c);
                s.b(b3, "view_x", (int) motionEvent.getX(action3));
                s.b(b3, "view_y", (int) motionEvent.getY(action3));
                if (!this.l.p()) {
                    c2.a((AdColonyAdView) b2.b().get(this.j));
                }
                if (x2 > 0 && x2 < this.d && y2 > 0 && y2 < this.e) {
                    new x("AdContainer.on_touch_ended", this.l.k(), b3).d();
                    break;
                } else {
                    new x("AdContainer.on_touch_cancelled", this.l.k(), b3).d();
                    break;
                }
                break;
        }
        return true;
    }

    q(Context context, x xVar, int i2, c cVar) {
        super(context);
        this.a = i2;
        this.k = xVar;
        this.l = cVar;
    }

    /* access modifiers changed from: private */
    public boolean a(x xVar) {
        JSONObject b2 = xVar.b();
        return s.f(b2, "id") == this.a && s.f(b2, "container_id") == this.l.c() && s.h(b2, "ad_session_id").equals(this.l.a());
    }

    /* access modifiers changed from: private */
    public void c(x xVar) {
        this.i = s.h(xVar.b(), "filepath");
        setImageURI(Uri.fromFile(new File(this.i)));
    }

    /* access modifiers changed from: private */
    public void d(x xVar) {
        if (s.d(xVar.b(), String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        JSONObject b2 = this.k.b();
        this.j = s.h(b2, "ad_session_id");
        this.b = s.f(b2, AvidJSONUtil.KEY_X);
        this.c = s.f(b2, AvidJSONUtil.KEY_Y);
        this.d = s.f(b2, "width");
        this.e = s.f(b2, "height");
        this.i = s.h(b2, "filepath");
        this.f = s.d(b2, "dpi");
        this.g = s.d(b2, "invert_y");
        this.h = s.d(b2, "wrap_content");
        setImageURI(Uri.fromFile(new File(this.i)));
        if (this.f) {
            float n = (((float) this.e) * a.c().h().n()) / ((float) getDrawable().getIntrinsicHeight());
            this.e = (int) (((float) getDrawable().getIntrinsicHeight()) * n);
            int intrinsicWidth = (int) (((float) getDrawable().getIntrinsicWidth()) * n);
            this.d = intrinsicWidth;
            this.b -= intrinsicWidth;
            this.c = this.g ? this.c + this.e : this.c - this.e;
        }
        setVisibility(4);
        LayoutParams layoutParams = this.h ? new LayoutParams(-2, -2) : new LayoutParams(this.d, this.e);
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.gravity = 0;
        this.l.addView(this, layoutParams);
        this.l.i().add(a.a("ImageView.set_visible", (z) new a(), true));
        this.l.i().add(a.a("ImageView.set_bounds", (z) new b(), true));
        this.l.i().add(a.a("ImageView.set_image", (z) new c(), true));
        this.l.j().add("ImageView.set_visible");
        this.l.j().add("ImageView.set_bounds");
        this.l.j().add("ImageView.set_image");
    }

    /* access modifiers changed from: 0000 */
    public int[] a() {
        return new int[]{this.b, this.c, this.d, this.e};
    }

    /* access modifiers changed from: private */
    public void b(x xVar) {
        JSONObject b2 = xVar.b();
        this.b = s.f(b2, AvidJSONUtil.KEY_X);
        this.c = s.f(b2, AvidJSONUtil.KEY_Y);
        this.d = s.f(b2, "width");
        this.e = s.f(b2, "height");
        if (this.f) {
            float n = (((float) this.e) * a.c().h().n()) / ((float) getDrawable().getIntrinsicHeight());
            this.e = (int) (((float) getDrawable().getIntrinsicHeight()) * n);
            int intrinsicWidth = (int) (((float) getDrawable().getIntrinsicWidth()) * n);
            this.d = intrinsicWidth;
            this.b -= intrinsicWidth;
            this.c -= this.e;
        }
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.width = this.d;
        layoutParams.height = this.e;
        setLayoutParams(layoutParams);
    }
}
