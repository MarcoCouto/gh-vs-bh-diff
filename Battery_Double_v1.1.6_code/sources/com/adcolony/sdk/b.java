package com.adcolony.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout.LayoutParams;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import java.util.Iterator;
import java.util.Map.Entry;
import org.json.JSONObject;

class b extends Activity {
    static final int k = 0;
    static final int l = 1;
    c a;
    int b = -1;
    String c;
    int d;
    boolean e;
    boolean f;
    boolean g;
    boolean h;
    boolean i;
    boolean j;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            b.this.a(xVar);
        }
    }

    b() {
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        Iterator it = this.a.m().entrySet().iterator();
        while (it.hasNext() && !isFinishing()) {
            l0 l0Var = (l0) ((Entry) it.next()).getValue();
            if (!l0Var.f() && l0Var.b().isPlaying()) {
                l0Var.g();
            }
        }
        AdColonyInterstitial d2 = a.c().d();
        if (d2 != null && d2.g() && d2.e().c() != null && z && this.i) {
            d2.e().a("pause");
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(boolean z) {
        for (Entry value : this.a.m().entrySet()) {
            l0 l0Var = (l0) value.getValue();
            if (!l0Var.f() && !l0Var.b().isPlaying() && !a.c().i().b()) {
                l0Var.h();
            }
        }
        AdColonyInterstitial d2 = a.c().d();
        if (d2 != null && d2.g() && d2.e().c() != null) {
            if ((!z || (z && !this.i)) && this.j) {
                d2.e().a("resume");
            }
        }
    }

    public void onBackPressed() {
        JSONObject b2 = s.b();
        s.a(b2, "id", this.a.a());
        new x("AdSession.on_back_button", this.a.k(), b2).d();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this instanceof AdColonyInterstitialActivity) {
            a();
        } else {
            ((AdColonyAdViewActivity) this).c();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!a.e() || a.c().f() == null) {
            finish();
            return;
        }
        h c2 = a.c();
        this.g = false;
        c f2 = c2.f();
        this.a = f2;
        f2.b(false);
        if (k0.h()) {
            this.a.b(true);
        }
        this.c = this.a.a();
        this.d = this.a.k();
        boolean multiWindowEnabled = c2.q().getMultiWindowEnabled();
        this.h = multiWindowEnabled;
        if (multiWindowEnabled) {
            getWindow().addFlags(2048);
            getWindow().clearFlags(1024);
        } else {
            getWindow().addFlags(1024);
            getWindow().clearFlags(2048);
        }
        requestWindowFeature(1);
        getWindow().getDecorView().setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        if (c2.q().getKeepScreenOn()) {
            getWindow().addFlags(128);
        }
        ViewParent parent = this.a.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.a);
        }
        setContentView(this.a);
        this.a.i().add(a.a("AdSession.finish_fullscreen_ad", (z) new a(), true));
        this.a.j().add("AdSession.finish_fullscreen_ad");
        a(this.b);
        if (!this.a.o()) {
            JSONObject b2 = s.b();
            s.a(b2, "id", this.a.a());
            s.b(b2, "screen_width", this.a.d());
            s.b(b2, "screen_height", this.a.b());
            new a().a("AdSession.on_fullscreen_ad_started").a(u.d);
            new x("AdSession.on_fullscreen_ad_started", this.a.k(), b2).d();
            this.a.c(true);
        } else {
            a();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (a.e() && this.a != null && !this.e) {
            if ((VERSION.SDK_INT < 24 || !k0.h()) && !this.a.q()) {
                JSONObject b2 = s.b();
                s.a(b2, "id", this.a.a());
                new x("AdSession.on_error", this.a.k(), b2).d();
                this.g = true;
            }
        }
    }

    public void onPause() {
        super.onPause();
        a(this.f);
        this.f = false;
    }

    public void onResume() {
        super.onResume();
        a();
        b(this.f);
        this.f = true;
        this.j = true;
    }

    public void onWindowFocusChanged(boolean z) {
        if (z && this.f) {
            a.c().s().b(true);
            b(this.f);
            this.i = true;
        } else if (!z && this.f) {
            new a().a("Activity is active but window does not have focus, pausing.").a(u.f);
            a.c().s().a(true);
            a(this.f);
            this.i = false;
        }
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        h c2 = a.c();
        if (this.a == null) {
            this.a = c2.f();
        }
        c cVar = this.a;
        if (cVar != null) {
            cVar.b(false);
            if (k0.h()) {
                this.a.b(true);
            }
            int s = c2.h().s();
            int r = this.h ? c2.h().r() - k0.d(a.b()) : c2.h().r();
            if (s > 0 && r > 0) {
                JSONObject b2 = s.b();
                JSONObject b3 = s.b();
                float n = c2.h().n();
                s.b(b3, "width", (int) (((float) s) / n));
                s.b(b3, "height", (int) (((float) r) / n));
                s.b(b3, "app_orientation", k0.g(k0.g()));
                s.b(b3, AvidJSONUtil.KEY_X, 0);
                s.b(b3, AvidJSONUtil.KEY_Y, 0);
                s.a(b3, "ad_session_id", this.a.a());
                s.b(b2, "screen_width", s);
                s.b(b2, "screen_height", r);
                s.a(b2, "ad_session_id", this.a.a());
                s.b(b2, "id", this.a.c());
                this.a.setLayoutParams(new LayoutParams(s, r));
                this.a.b(s);
                this.a.a(r);
                new x("MRAID.on_size_change", this.a.k(), b3).d();
                new x("AdContainer.on_orientation_change", this.a.k(), b2).d();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(x xVar) {
        int f2 = s.f(xVar.b(), "status");
        if ((f2 == 5 || f2 == 0 || f2 == 6 || f2 == 1) && !this.e) {
            h c2 = a.c();
            k i2 = c2.i();
            c2.c(xVar);
            if (i2.a() != null) {
                i2.a().dismiss();
                i2.a((AlertDialog) null);
            }
            if (!this.g) {
                finish();
            }
            this.e = true;
            ((ViewGroup) getWindow().getDecorView()).removeAllViews();
            c2.d(false);
            JSONObject b2 = s.b();
            s.a(b2, "id", this.a.a());
            new x("AdSession.on_close", this.a.k(), b2).d();
            c2.a((c) null);
            c2.a((AdColonyInterstitial) null);
            c2.a((AdColonyAdView) null);
            a.c().b().a().remove(this.a.a());
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        if (i2 == 0) {
            setRequestedOrientation(7);
        } else if (i2 != 1) {
            setRequestedOrientation(4);
        } else {
            setRequestedOrientation(6);
        }
        this.b = i2;
    }
}
