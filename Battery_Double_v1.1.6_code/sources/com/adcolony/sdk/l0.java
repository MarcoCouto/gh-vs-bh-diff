package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import com.github.mikephil.charting.utils.Utils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

@TargetApi(14)
class l0 extends TextureView implements OnErrorListener, OnPreparedListener, OnCompletionListener, OnSeekCompleteListener, SurfaceTextureListener {
    /* access modifiers changed from: private */
    public boolean A;
    private boolean B;
    /* access modifiers changed from: private */
    public boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private String F;
    /* access modifiers changed from: private */
    public String G;
    private FileInputStream H;
    private x I;
    /* access modifiers changed from: private */
    public c J;
    private Surface K;
    private SurfaceTexture L;
    /* access modifiers changed from: private */
    public RectF M = new RectF();
    /* access modifiers changed from: private */
    public j N;
    private ProgressBar O;
    /* access modifiers changed from: private */
    public MediaPlayer P;
    /* access modifiers changed from: private */
    public JSONObject Q = s.b();
    private ExecutorService R = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public x S;
    private float a;
    private float b;
    /* access modifiers changed from: private */
    public float c;
    /* access modifiers changed from: private */
    public float d;
    private float e;
    private float f;
    /* access modifiers changed from: private */
    public int g;
    private boolean h = true;
    /* access modifiers changed from: private */
    public Paint i = new Paint();
    /* access modifiers changed from: private */
    public Paint j = new Paint(1);
    private int k;
    private int l;
    private int m;
    private int n;
    /* access modifiers changed from: private */
    public int o;
    private int p;
    private int q;
    private int r;
    /* access modifiers changed from: private */
    public double s;
    /* access modifiers changed from: private */
    public double t;
    /* access modifiers changed from: private */
    public long u;
    /* access modifiers changed from: private */
    public boolean v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public boolean y;
    private boolean z;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            if (l0.this.a(xVar)) {
                l0.this.h();
            }
        }
    }

    class b implements z {
        b() {
        }

        public void a(x xVar) {
            if (l0.this.a(xVar)) {
                l0.this.c(xVar);
            }
        }
    }

    class c implements z {
        c() {
        }

        public void a(x xVar) {
            if (l0.this.a(xVar)) {
                l0.this.d(xVar);
            }
        }
    }

    class d implements z {
        d() {
        }

        public void a(x xVar) {
            if (l0.this.a(xVar)) {
                l0.this.g();
            }
        }
    }

    class e implements z {
        e() {
        }

        public void a(x xVar) {
            if (l0.this.a(xVar)) {
                l0.this.b(xVar);
            }
        }
    }

    class f implements z {
        f() {
        }

        public void a(x xVar) {
            if (l0.this.a(xVar)) {
                l0.this.e(xVar);
            }
        }
    }

    class g implements Runnable {
        g() {
        }

        public void run() {
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (l0.this.S != null) {
                JSONObject b = s.b();
                s.b(b, "id", l0.this.o);
                s.a(b, "ad_session_id", l0.this.G);
                s.b(b, "success", true);
                l0.this.S.a(b).d();
                l0.this.S = null;
            }
        }
    }

    class h implements Runnable {
        h() {
        }

        public void run() {
            l0.this.u = 0;
            while (!l0.this.v && !l0.this.y && a.d()) {
                Context b = a.b();
                if (!l0.this.v && !l0.this.A && b != null && (b instanceof Activity)) {
                    if (l0.this.P.isPlaying()) {
                        if (l0.this.u == 0 && a.d) {
                            l0.this.u = System.currentTimeMillis();
                        }
                        l0.this.x = true;
                        l0 l0Var = l0.this;
                        double currentPosition = (double) l0Var.P.getCurrentPosition();
                        Double.isNaN(currentPosition);
                        l0Var.s = currentPosition / 1000.0d;
                        l0 l0Var2 = l0.this;
                        double duration = (double) l0Var2.P.getDuration();
                        Double.isNaN(duration);
                        l0Var2.t = duration / 1000.0d;
                        if (System.currentTimeMillis() - l0.this.u > 1000 && !l0.this.D && a.d) {
                            if (l0.this.s == Utils.DOUBLE_EPSILON) {
                                new a().a("getCurrentPosition() not working, firing ").a("AdSession.on_error").a(u.j);
                                l0.this.k();
                            } else {
                                l0.this.D = true;
                            }
                        }
                        if (l0.this.C) {
                            l0.this.e();
                        }
                    }
                    if (l0.this.x && !l0.this.v && !l0.this.y) {
                        s.b(l0.this.Q, "id", l0.this.o);
                        s.b(l0.this.Q, "container_id", l0.this.J.c());
                        s.a(l0.this.Q, "ad_session_id", l0.this.G);
                        s.a(l0.this.Q, "elapsed", l0.this.s);
                        s.a(l0.this.Q, "duration", l0.this.t);
                        new x("VideoView.on_progress", l0.this.J.k(), l0.this.Q).d();
                    }
                    if (l0.this.w || ((Activity) b).isFinishing()) {
                        l0.this.w = false;
                        l0.this.i();
                    } else {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException unused) {
                            l0.this.k();
                            new a().a("InterruptedException in ADCVideoView's update thread.").a(u.i);
                        }
                    }
                }
                return;
            }
            if (l0.this.w) {
                l0.this.i();
            }
        }
    }

    class i implements Runnable {
        final /* synthetic */ Context a;

        i(Context context) {
            this.a = context;
        }

        public void run() {
            l0.this.N = new j(this.a);
            LayoutParams layoutParams = new LayoutParams((int) (l0.this.c * 4.0f), (int) (l0.this.c * 4.0f));
            layoutParams.setMargins(0, l0.this.J.b() - ((int) (l0.this.c * 4.0f)), 0, 0);
            layoutParams.gravity = 0;
            l0.this.J.addView(l0.this.N, layoutParams);
        }
    }

    private class j extends View {
        j(Context context) {
            super(context);
            setWillNotDraw(false);
            try {
                getClass().getMethod("setLayerType", new Class[]{Integer.TYPE, Paint.class}).invoke(this, new Object[]{Integer.valueOf(1), null});
            } catch (Exception unused) {
            }
        }

        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawArc(l0.this.M, 270.0f, l0.this.d, false, l0.this.i);
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(l0.this.g);
            String sb2 = sb.toString();
            float centerX = l0.this.M.centerX();
            double centerY = (double) l0.this.M.centerY();
            double d = (double) l0.this.j.getFontMetrics().bottom;
            Double.isNaN(d);
            double d2 = d * 1.35d;
            Double.isNaN(centerY);
            canvas.drawText(sb2, centerX, (float) (centerY + d2), l0.this.j);
            invalidate();
        }
    }

    private l0(Context context) {
        super(context);
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.v = true;
        this.s = this.t;
        s.b(this.Q, "id", this.o);
        s.b(this.Q, "container_id", this.J.c());
        s.a(this.Q, "ad_session_id", this.G);
        s.a(this.Q, "elapsed", this.s);
        s.a(this.Q, "duration", this.t);
        new x("VideoView.on_progress", this.J.k(), this.Q).d();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        k();
        a aVar = new a();
        StringBuilder sb = new StringBuilder();
        sb.append("MediaPlayer error: ");
        sb.append(i2);
        sb.append(",");
        sb.append(i3);
        aVar.a(sb.toString()).a(u.i);
        return true;
    }

    public void onMeasure(int i2, int i3) {
        l();
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.z = true;
        if (this.E) {
            this.J.removeView(this.O);
        }
        if (this.B) {
            this.p = mediaPlayer.getVideoWidth();
            this.q = mediaPlayer.getVideoHeight();
            l();
            new a().a("MediaPlayer getVideoWidth = ").a(mediaPlayer.getVideoWidth()).a(u.f);
            new a().a("MediaPlayer getVideoHeight = ").a(mediaPlayer.getVideoHeight()).a(u.f);
        }
        JSONObject b2 = s.b();
        s.b(b2, "id", this.o);
        s.b(b2, "container_id", this.J.c());
        s.a(b2, "ad_session_id", this.G);
        new a().a("ADCVideoView is prepared").a(u.d);
        new x("VideoView.on_ready", this.J.k(), b2).d();
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        ExecutorService executorService = this.R;
        if (executorService != null && !executorService.isShutdown()) {
            try {
                this.R.submit(new g());
            } catch (RejectedExecutionException unused) {
                k();
            }
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        if (surfaceTexture == null || this.A) {
            new a().a("Null texture provided by system's onSurfaceTextureAvailable or ").a("MediaPlayer has been destroyed.").a(u.j);
            return;
        }
        Surface surface = new Surface(surfaceTexture);
        this.K = surface;
        try {
            this.P.setSurface(surface);
        } catch (IllegalStateException unused) {
            new a().a("IllegalStateException thrown when calling MediaPlayer.setSurface()").a(u.i);
            k();
        }
        this.L = surfaceTexture;
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.L = surfaceTexture;
        if (!this.A) {
            return false;
        }
        surfaceTexture.release();
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        this.L = surfaceTexture;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.L = surfaceTexture;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        h c2 = a.c();
        d b2 = c2.b();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject b3 = s.b();
        s.b(b3, "view_id", this.o);
        s.a(b3, "ad_session_id", this.G);
        s.b(b3, "container_x", this.k + x2);
        s.b(b3, "container_y", this.l + y2);
        s.b(b3, "view_x", x2);
        s.b(b3, "view_y", y2);
        s.b(b3, "id", this.J.c());
        switch (action) {
            case 0:
                new x("AdContainer.on_touch_began", this.J.k(), b3).d();
                break;
            case 1:
                if (!this.J.p()) {
                    c2.a((AdColonyAdView) b2.b().get(this.G));
                }
                new x("AdContainer.on_touch_ended", this.J.k(), b3).d();
                break;
            case 2:
                new x("AdContainer.on_touch_moved", this.J.k(), b3).d();
                break;
            case 3:
                new x("AdContainer.on_touch_cancelled", this.J.k(), b3).d();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(b3, "container_x", ((int) motionEvent.getX(action2)) + this.k);
                s.b(b3, "container_y", ((int) motionEvent.getY(action2)) + this.l);
                s.b(b3, "view_x", (int) motionEvent.getX(action2));
                s.b(b3, "view_y", (int) motionEvent.getY(action2));
                new x("AdContainer.on_touch_began", this.J.k(), b3).d();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(b3, "container_x", ((int) motionEvent.getX(action3)) + this.k);
                s.b(b3, "container_y", ((int) motionEvent.getY(action3)) + this.l);
                s.b(b3, "view_x", (int) motionEvent.getX(action3));
                s.b(b3, "view_y", (int) motionEvent.getY(action3));
                if (!this.J.p()) {
                    c2.a((AdColonyAdView) b2.b().get(this.G));
                }
                new x("AdContainer.on_touch_ended", this.J.k(), b3).d();
                break;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void k() {
        JSONObject b2 = s.b();
        s.a(b2, "id", this.G);
        new x("AdSession.on_error", this.J.k(), b2).d();
        this.v = true;
    }

    private void l() {
        double d2 = (double) this.m;
        double d3 = (double) this.p;
        Double.isNaN(d2);
        Double.isNaN(d3);
        double d4 = d2 / d3;
        double d5 = (double) this.n;
        double d6 = (double) this.q;
        Double.isNaN(d5);
        Double.isNaN(d6);
        double d7 = d5 / d6;
        if (d4 > d7) {
            d4 = d7;
        }
        double d8 = (double) this.p;
        Double.isNaN(d8);
        int i2 = (int) (d8 * d4);
        double d9 = (double) this.q;
        Double.isNaN(d9);
        int i3 = (int) (d9 * d4);
        new a().a("setMeasuredDimension to ").a(i2).a(" by ").a(i3).a(u.f);
        setMeasuredDimension(i2, i3);
        if (this.B) {
            LayoutParams layoutParams = (LayoutParams) getLayoutParams();
            layoutParams.width = i2;
            layoutParams.height = i3;
            layoutParams.gravity = 17;
            layoutParams.setMargins(0, 0, 0, 0);
            setLayoutParams(layoutParams);
        }
    }

    private void m() {
        try {
            this.R.submit(new h());
        } catch (RejectedExecutionException unused) {
            k();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean g() {
        if (!this.z) {
            new a().a("ADCVideoView pause() called while MediaPlayer is not prepared.").a(u.h);
            return false;
        } else if (!this.x) {
            new a().a("Ignoring ADCVideoView pause due to invalid MediaPlayer state.").a(u.f);
            return false;
        } else {
            this.r = this.P.getCurrentPosition();
            this.t = (double) this.P.getDuration();
            this.P.pause();
            this.y = true;
            new a().a("Video view paused").a(u.d);
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean h() {
        if (!this.z) {
            return false;
        }
        if (!this.y && a.d) {
            this.P.start();
            m();
            new a().a("MediaPlayer is prepared - ADCVideoView play() called.").a(u.d);
        } else if (!this.v && a.d) {
            this.P.start();
            this.y = false;
            if (!this.R.isShutdown()) {
                m();
            }
            j jVar = this.N;
            if (jVar != null) {
                jVar.invalidate();
            }
        }
        setWillNotDraw(false);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void i() {
        new a().a("MediaPlayer stopped and released.").a(u.f);
        try {
            if (!this.v && this.z && this.P.isPlaying()) {
                this.P.stop();
            }
        } catch (IllegalStateException unused) {
            new a().a("Caught IllegalStateException when calling stop on MediaPlayer").a(u.h);
        }
        ProgressBar progressBar = this.O;
        if (progressBar != null) {
            this.J.removeView(progressBar);
        }
        this.v = true;
        this.z = false;
        this.P.release();
    }

    /* access modifiers changed from: 0000 */
    public void j() {
        this.w = true;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        JSONObject b2 = this.I.b();
        this.G = s.h(b2, "ad_session_id");
        this.k = s.f(b2, AvidJSONUtil.KEY_X);
        this.l = s.f(b2, AvidJSONUtil.KEY_Y);
        this.m = s.f(b2, "width");
        this.n = s.f(b2, "height");
        this.C = s.d(b2, "enable_timer");
        this.E = s.d(b2, "enable_progress");
        this.F = s.h(b2, "filepath");
        this.p = s.f(b2, "video_width");
        this.q = s.f(b2, "video_height");
        this.f = a.c().h().n();
        new a().a("Original video dimensions = ").a(this.p).a(AvidJSONUtil.KEY_X).a(this.q).a(u.d);
        setVisibility(4);
        LayoutParams layoutParams = new LayoutParams(this.m, this.n);
        layoutParams.setMargins(this.k, this.l, 0, 0);
        layoutParams.gravity = 0;
        this.J.addView(this, layoutParams);
        if (this.E) {
            Context b3 = a.b();
            if (b3 != null) {
                ProgressBar progressBar = new ProgressBar(b3);
                this.O = progressBar;
                int i2 = (int) (this.f * 100.0f);
                this.J.addView(progressBar, new LayoutParams(i2, i2, 17));
            }
        }
        this.P = new MediaPlayer();
        this.z = false;
        try {
            if (!this.F.startsWith("http")) {
                FileInputStream fileInputStream = new FileInputStream(this.F);
                this.H = fileInputStream;
                this.P.setDataSource(fileInputStream.getFD());
            } else {
                this.B = true;
                this.P.setDataSource(this.F);
            }
            this.P.setOnErrorListener(this);
            this.P.setOnPreparedListener(this);
            this.P.setOnCompletionListener(this);
            this.P.prepareAsync();
        } catch (IOException e2) {
            new a().a("Failed to create/prepare MediaPlayer: ").a(e2.toString()).a(u.i);
            k();
        }
        this.J.i().add(a.a("VideoView.play", (z) new a(), true));
        this.J.i().add(a.a("VideoView.set_bounds", (z) new b(), true));
        this.J.i().add(a.a("VideoView.set_visible", (z) new c(), true));
        this.J.i().add(a.a("VideoView.pause", (z) new d(), true));
        this.J.i().add(a.a("VideoView.seek_to_time", (z) new e(), true));
        this.J.i().add(a.a("VideoView.set_volume", (z) new f(), true));
        this.J.j().add("VideoView.play");
        this.J.j().add("VideoView.set_bounds");
        this.J.j().add("VideoView.set_visible");
        this.J.j().add("VideoView.pause");
        this.J.j().add("VideoView.seek_to_time");
        this.J.j().add("VideoView.set_volume");
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        if (this.h) {
            this.e = (float) (360.0d / this.t);
            this.j.setColor(-3355444);
            this.j.setShadowLayer((float) ((int) (this.f * 2.0f)), 0.0f, 0.0f, ViewCompat.MEASURED_STATE_MASK);
            this.j.setTextAlign(Align.CENTER);
            this.j.setLinearText(true);
            this.j.setTextSize(this.f * 12.0f);
            this.i.setStyle(Style.STROKE);
            float f2 = this.f * 2.0f;
            if (f2 > 6.0f) {
                f2 = 6.0f;
            }
            if (f2 < 4.0f) {
                f2 = 4.0f;
            }
            this.i.setStrokeWidth(f2);
            this.i.setShadowLayer((float) ((int) (this.f * 3.0f)), 0.0f, 0.0f, ViewCompat.MEASURED_STATE_MASK);
            this.i.setColor(-3355444);
            Rect rect = new Rect();
            this.j.getTextBounds("0123456789", 0, 9, rect);
            this.c = (float) rect.height();
            Context b2 = a.b();
            if (b2 != null) {
                k0.a((Runnable) new i(b2));
            }
            this.h = false;
        }
        this.g = (int) (this.t - this.s);
        float f3 = this.c;
        float f4 = (float) ((int) f3);
        this.a = f4;
        float f5 = (float) ((int) (3.0f * f3));
        this.b = f5;
        float f6 = f3 / 2.0f;
        float f7 = f3 * 2.0f;
        this.M.set(f4 - f6, f5 - f7, f4 + f7, f5 + f6);
        double d2 = (double) this.e;
        double d3 = this.t - this.s;
        Double.isNaN(d2);
        this.d = (float) (d2 * d3);
    }

    /* access modifiers changed from: 0000 */
    public boolean f() {
        return this.v;
    }

    /* access modifiers changed from: private */
    public void c(x xVar) {
        JSONObject b2 = xVar.b();
        this.k = s.f(b2, AvidJSONUtil.KEY_X);
        this.l = s.f(b2, AvidJSONUtil.KEY_Y);
        this.m = s.f(b2, "width");
        this.n = s.f(b2, "height");
        LayoutParams layoutParams = (LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.k, this.l, 0, 0);
        layoutParams.width = this.m;
        layoutParams.height = this.n;
        setLayoutParams(layoutParams);
        if (this.C && this.N != null) {
            int i2 = (int) (this.c * 4.0f);
            LayoutParams layoutParams2 = new LayoutParams(i2, i2);
            layoutParams2.setMargins(0, this.J.b() - ((int) (this.c * 4.0f)), 0, 0);
            layoutParams2.gravity = 0;
            this.N.setLayoutParams(layoutParams2);
        }
    }

    /* access modifiers changed from: private */
    public boolean b(x xVar) {
        if (!this.z) {
            return false;
        }
        if (this.v) {
            this.v = false;
        }
        this.S = xVar;
        int f2 = s.f(xVar.b(), LocationConst.TIME);
        int i2 = f2 * 1000;
        int duration = this.P.getDuration() / 1000;
        this.P.setOnSeekCompleteListener(this);
        this.P.seekTo(i2);
        if (duration == f2) {
            this.v = true;
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.L != null) {
            this.A = true;
        }
        this.R.shutdown();
    }

    /* access modifiers changed from: private */
    public boolean a(x xVar) {
        JSONObject b2 = xVar.b();
        return s.f(b2, "id") == this.o && s.f(b2, "container_id") == this.J.c() && s.h(b2, "ad_session_id").equals(this.J.a());
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return this.P != null;
    }

    /* access modifiers changed from: 0000 */
    public MediaPlayer b() {
        return this.P;
    }

    l0(Context context, x xVar, int i2, c cVar) {
        super(context);
        this.J = cVar;
        this.I = xVar;
        this.o = i2;
        setSurfaceTextureListener(this);
    }

    /* access modifiers changed from: private */
    public boolean e(x xVar) {
        boolean z2 = false;
        if (!this.z) {
            return false;
        }
        float e2 = (float) s.e(xVar.b(), AvidVideoPlaybackListenerImpl.VOLUME);
        AdColonyInterstitial d2 = a.c().d();
        if (d2 != null) {
            if (((double) e2) <= Utils.DOUBLE_EPSILON) {
                z2 = true;
            }
            d2.b(z2);
        }
        this.P.setVolume(e2, e2);
        JSONObject b2 = s.b();
        s.b(b2, "success", true);
        xVar.a(b2).d();
        return true;
    }

    /* access modifiers changed from: private */
    public void d(x xVar) {
        if (s.d(xVar.b(), String.VISIBLE)) {
            setVisibility(0);
            if (this.C) {
                j jVar = this.N;
                if (jVar != null) {
                    jVar.setVisibility(0);
                    return;
                }
                return;
            }
            return;
        }
        setVisibility(4);
        if (this.C) {
            j jVar2 = this.N;
            if (jVar2 != null) {
                jVar2.setVisibility(4);
            }
        }
    }
}
