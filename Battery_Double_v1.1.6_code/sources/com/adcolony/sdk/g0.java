package com.adcolony.sdk;

import android.content.Context;
import android.os.StatFs;
import com.github.mikephil.charting.utils.Utils;
import java.io.File;

class g0 {
    private String a;
    private String b;
    private String c;
    private String d;
    private File e;
    private File f;
    private File g;

    g0() {
    }

    /* access modifiers changed from: 0000 */
    public double a(String str) {
        try {
            StatFs statFs = new StatFs(str);
            return (double) (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()));
        } catch (RuntimeException unused) {
            return Utils.DOUBLE_EPSILON;
        }
    }

    /* access modifiers changed from: 0000 */
    public String b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public boolean e() {
        new a().a("Configuring storage").a(u.f);
        h c2 = a.c();
        StringBuilder sb = new StringBuilder();
        sb.append(f());
        sb.append("/adc3/");
        this.a = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(this.a);
        sb2.append("media/");
        this.b = sb2.toString();
        File file = new File(this.b);
        this.e = file;
        if (!file.isDirectory()) {
            this.e.delete();
            this.e.mkdirs();
        }
        if (!this.e.isDirectory()) {
            c2.a(true);
            return false;
        } else if (a(this.b) < 2.097152E7d) {
            new a().a("Not enough memory available at media path, disabling AdColony.").a(u.g);
            c2.a(true);
            return false;
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(f());
            sb3.append("/adc3/data/");
            this.c = sb3.toString();
            File file2 = new File(this.c);
            this.f = file2;
            if (!file2.isDirectory()) {
                this.f.delete();
            }
            this.f.mkdirs();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(this.a);
            sb4.append("tmp/");
            this.d = sb4.toString();
            File file3 = new File(this.d);
            this.g = file3;
            if (!file3.isDirectory()) {
                this.g.delete();
                this.g.mkdirs();
            }
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    public String f() {
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        return b2.getFilesDir().getAbsolutePath();
    }

    /* access modifiers changed from: 0000 */
    public boolean g() {
        File file = this.e;
        if (file == null || this.f == null || this.g == null) {
            return false;
        }
        if (!file.isDirectory()) {
            this.e.delete();
        }
        if (!this.f.isDirectory()) {
            this.f.delete();
        }
        if (!this.g.isDirectory()) {
            this.g.delete();
        }
        this.e.mkdirs();
        this.f.mkdirs();
        this.g.mkdirs();
        return true;
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return this.a;
    }
}
