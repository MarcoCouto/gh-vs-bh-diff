package com.adcolony.sdk;

import org.json.JSONObject;

public class AdColonyReward {
    private int a;
    private String b;
    private String c;
    private boolean d;

    AdColonyReward(x xVar) {
        JSONObject b2 = xVar.b();
        this.a = s.f(b2, "reward_amount");
        this.b = s.h(b2, "reward_name");
        this.d = s.d(b2, "success");
        this.c = s.h(b2, "zone_id");
    }

    public int getRewardAmount() {
        return this.a;
    }

    public String getRewardName() {
        return this.b;
    }

    public String getZoneID() {
        return this.c;
    }

    public boolean success() {
        return this.d;
    }
}
