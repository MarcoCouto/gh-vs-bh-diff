package com.adcolony.sdk;

import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.tapjoy.TapjoyConstants;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class e0 {
    static final String h = "adcolony_android";
    static final String i = "adcolony_fatal_reports";
    t a;
    ScheduledExecutorService b;
    List<v> c = new ArrayList();
    List<v> d = new ArrayList();
    HashMap<String, Object> e;
    private r f = new r(h, "4.1.4", "Production");
    private r g = new r(i, "4.1.4", "Production");

    class a implements Runnable {
        a() {
        }

        public void run() {
            e0.this.a();
        }
    }

    class b implements Runnable {
        final /* synthetic */ v a;

        b(v vVar) {
            this.a = vVar;
        }

        public void run() {
            e0.this.c.add(this.a);
        }
    }

    e0(t tVar, ScheduledExecutorService scheduledExecutorService, HashMap<String, Object> hashMap) {
        this.a = tVar;
        this.b = scheduledExecutorService;
        this.e = hashMap;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(long j, TimeUnit timeUnit) {
        try {
            if (!this.b.isShutdown() && !this.b.isTerminated()) {
                this.b.scheduleAtFixedRate(new a(), j, j, timeUnit);
            }
        } catch (RuntimeException unused) {
            Log.e("ADCLogError", "Internal error when submitting remote log to executor service");
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:9|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r4.b.shutdownNow();
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0045 */
    public synchronized void b() {
        this.b.shutdown();
        if (!this.b.awaitTermination(1, TimeUnit.SECONDS)) {
            this.b.shutdownNow();
            if (!this.b.awaitTermination(1, TimeUnit.SECONDS)) {
                PrintStream printStream = System.err;
                StringBuilder sb = new StringBuilder();
                sb.append(getClass().getSimpleName());
                sb.append(": ScheduledExecutorService ");
                sb.append("did not terminate");
                printStream.println(sb.toString());
            }
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void c(String str) {
        b(new a().a(2).a(this.f).a(str).a());
    }

    /* access modifiers changed from: 0000 */
    public synchronized void d(String str) {
        b(new a().a(1).a(this.f).a(str).a());
    }

    /* access modifiers changed from: 0000 */
    public synchronized void e(String str) {
        this.e.put("controllerVersion", str);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void f(String str) {
        this.e.put("sessionId", str);
    }

    private synchronized JSONObject c(v vVar) throws JSONException {
        JSONObject jSONObject;
        jSONObject = new JSONObject(this.e);
        jSONObject.put("environment", vVar.a().a());
        jSONObject.put("level", vVar.c());
        jSONObject.put("message", vVar.d());
        jSONObject.put("clientTimestamp", vVar.e());
        JSONObject mediationInfo = a.c().q().getMediationInfo();
        JSONObject pluginInfo = a.c().q().getPluginInfo();
        double g2 = a.c().h().g();
        jSONObject.put("mediation_network", s.h(mediationInfo, "name"));
        jSONObject.put("mediation_network_version", s.h(mediationInfo, "version"));
        jSONObject.put(TapjoyConstants.TJC_PLUGIN, s.h(pluginInfo, "name"));
        jSONObject.put("plugin_version", s.h(pluginInfo, "version"));
        jSONObject.put("batteryInfo", g2);
        if (vVar instanceof o) {
            jSONObject = s.a(jSONObject, ((o) vVar).f());
            jSONObject.put(TapjoyConstants.TJC_PLATFORM, "android");
        }
        return jSONObject;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0039 */
    public synchronized void a() {
        synchronized (this) {
            try {
                if (this.c.size() > 0) {
                    this.a.a(a(this.f, this.c));
                    this.c.clear();
                }
                if (this.d.size() > 0) {
                    this.a.a(a(this.g, this.d));
                    this.d.clear();
                }
            } catch (IOException unused) {
                this.c.clear();
            } catch (JSONException ) {
                this.c.clear();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void b(String str) {
        b(new a().a(0).a(this.f).a(str).a());
    }

    /* access modifiers changed from: 0000 */
    public synchronized void b(v vVar) {
        try {
            if (!this.b.isShutdown() && !this.b.isTerminated()) {
                this.b.submit(new b(vVar));
            }
        } catch (RejectedExecutionException unused) {
            Log.e("ADCLogError", "Internal error when submitting remote log to executor service");
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(String str) {
        b(new a().a(3).a(this.f).a(str).a());
    }

    /* access modifiers changed from: 0000 */
    public void a(o oVar) {
        oVar.a(this.g);
        oVar.a(-1);
        a((v) oVar);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(v vVar) {
        this.d.add(vVar);
    }

    /* access modifiers changed from: 0000 */
    public String a(r rVar, List<v> list) throws IOException, JSONException {
        String b2 = a.c().h().b();
        String str = this.e.get("advertiserId") != null ? (String) this.e.get("advertiserId") : "unknown";
        if (b2 != null && b2.length() > 0 && !b2.equals(str)) {
            this.e.put("advertiserId", b2);
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(Param.INDEX, rVar.c());
        jSONObject.put("environment", rVar.a());
        jSONObject.put("version", rVar.d());
        JSONArray jSONArray = new JSONArray();
        for (v c2 : list) {
            jSONArray.put(c(c2));
        }
        jSONObject.put("logs", jSONArray);
        return jSONObject.toString();
    }
}
