package com.adcolony.sdk;

import com.appodeal.ads.utils.LogConstants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class v {
    static final SimpleDateFormat e = new SimpleDateFormat("yyyyMMdd'T'HHmmss.SSSZ", Locale.US);
    static final String f = "message";
    static final String g = "timestamp";
    /* access modifiers changed from: private */
    public Date a;
    /* access modifiers changed from: private */
    public int b;
    protected String c;
    /* access modifiers changed from: private */
    public r d;

    static class a {
        protected v a = new v();

        a() {
        }

        /* access modifiers changed from: 0000 */
        public a a(int i) {
            this.a.b = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(r rVar) {
            this.a.d = rVar;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(String str) {
            this.a.c = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public a a(Date date) {
            this.a.a = date;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public v a() {
            if (this.a.a == null) {
                this.a.a = new Date(System.currentTimeMillis());
            }
            return this.a;
        }
    }

    v() {
    }

    /* access modifiers changed from: 0000 */
    public int b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        switch (this.b) {
            case -1:
                return "Fatal";
            case 0:
                return "Error";
            case 1:
                return "Warn";
            case 2:
                return LogConstants.EVENT_INFO;
            case 3:
                return "Debug";
            default:
                return "UNKNOWN LOG LEVEL";
        }
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return e.format(this.a);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(e());
        sb.append(" ");
        sb.append(c());
        sb.append("/");
        sb.append(a().a());
        sb.append(": ");
        sb.append(d());
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public void a(r rVar) {
        this.d = rVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        this.b = i;
    }

    /* access modifiers changed from: 0000 */
    public r a() {
        return this.d;
    }
}
