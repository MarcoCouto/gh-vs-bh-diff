package com.adcolony.sdk;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.zip.GZIPOutputStream;

class t {
    URL a;

    public t(URL url) {
        this.a = url;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0088  */
    public int a(String str) throws IOException {
        GZIPOutputStream gZIPOutputStream;
        HttpURLConnection httpURLConnection;
        boolean z;
        DataOutputStream dataOutputStream;
        DataOutputStream dataOutputStream2 = null;
        boolean z2 = false;
        try {
            httpURLConnection = (HttpURLConnection) this.a.openConnection();
            try {
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                httpURLConnection.setRequestProperty(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setDoInput(true);
                gZIPOutputStream = new GZIPOutputStream(httpURLConnection.getOutputStream());
            } catch (IOException e) {
                e = e;
                gZIPOutputStream = null;
                z = false;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    z2 = z;
                }
            } catch (Throwable th2) {
                th = th2;
                gZIPOutputStream = null;
                if (dataOutputStream2 != null && !z2) {
                    dataOutputStream2.close();
                }
                if (gZIPOutputStream != null) {
                    gZIPOutputStream.close();
                }
                if (httpURLConnection != null) {
                    if (httpURLConnection.getInputStream() != null) {
                        httpURLConnection.getInputStream().close();
                    }
                    httpURLConnection.disconnect();
                }
                throw th;
            }
            try {
                dataOutputStream = new DataOutputStream(gZIPOutputStream);
            } catch (IOException e2) {
                e = e2;
                z = false;
                throw e;
            } catch (Throwable th3) {
                th = th3;
                dataOutputStream2.close();
                if (gZIPOutputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                throw th;
            }
            try {
                dataOutputStream.write(str.getBytes(Charset.forName("UTF-8")));
                dataOutputStream.close();
                try {
                    int responseCode = httpURLConnection.getResponseCode();
                    gZIPOutputStream.close();
                    if (httpURLConnection != null) {
                        if (httpURLConnection.getInputStream() != null) {
                            httpURLConnection.getInputStream().close();
                        }
                        httpURLConnection.disconnect();
                    }
                    return responseCode;
                } catch (IOException e3) {
                    e = e3;
                    z2 = true;
                    z = z2;
                    dataOutputStream2 = dataOutputStream;
                    throw e;
                } catch (Throwable th4) {
                    th = th4;
                    dataOutputStream2 = dataOutputStream;
                    z2 = true;
                    dataOutputStream2.close();
                    if (gZIPOutputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    throw th;
                }
            } catch (IOException e4) {
                e = e4;
                z = z2;
                dataOutputStream2 = dataOutputStream;
                throw e;
            } catch (Throwable th5) {
                th = th5;
                dataOutputStream2 = dataOutputStream;
                dataOutputStream2.close();
                if (gZIPOutputStream != null) {
                }
                if (httpURLConnection != null) {
                }
                throw th;
            }
        } catch (IOException e5) {
            e = e5;
            httpURLConnection = null;
            gZIPOutputStream = null;
            z = false;
            throw e;
        } catch (Throwable th6) {
            th = th6;
            httpURLConnection = null;
            gZIPOutputStream = null;
            dataOutputStream2.close();
            if (gZIPOutputStream != null) {
            }
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }
}
