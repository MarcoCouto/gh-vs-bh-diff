package com.adcolony.sdk;

import android.util.Log;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

class w {
    static boolean a = false;
    static final int b = 4000;
    static final int c = 4;
    static final int d = 3;
    static final int e = 2;
    static final int f = 1;
    static final int g = 0;
    static final int h = -1;
    static int i = 3;
    static JSONObject j = s.b();
    static int k = 1;
    private static ExecutorService l = null;
    private static final Queue<Runnable> m = new ConcurrentLinkedQueue();
    static e0 n;

    static class a implements z {
        a() {
        }

        public void a(x xVar) {
            w.b(s.f(xVar.b(), "module"), 0, s.h(xVar.b(), "message"), true);
        }
    }

    static class b implements Runnable {
        final /* synthetic */ int a;
        final /* synthetic */ String b;
        final /* synthetic */ int c;
        final /* synthetic */ boolean d;

        b(int i, String str, int i2, boolean z) {
            this.a = i;
            this.b = str;
            this.c = i2;
            this.d = z;
        }

        public void run() {
            w.a(this.a, this.b, this.c);
            int i = 0;
            while (i <= this.b.length() / w.b) {
                int i2 = i * w.b;
                i++;
                int i3 = i * w.b;
                if (i3 > this.b.length()) {
                    i3 = this.b.length();
                }
                if (this.c == 3 && w.a(s.g(w.j, Integer.toString(this.a)), 3, this.d)) {
                    Log.d("AdColony [TRACE]", this.b.substring(i2, i3));
                } else if (this.c == 2 && w.a(s.g(w.j, Integer.toString(this.a)), 2, this.d)) {
                    Log.i("AdColony [INFO]", this.b.substring(i2, i3));
                } else if (this.c == 1 && w.a(s.g(w.j, Integer.toString(this.a)), 1, this.d)) {
                    Log.w("AdColony [WARNING]", this.b.substring(i2, i3));
                } else if (this.c == 0 && w.a(s.g(w.j, Integer.toString(this.a)), 0, this.d)) {
                    Log.e("AdColony [ERROR]", this.b.substring(i2, i3));
                } else if (this.c == -1 && w.i >= -1) {
                    Log.e("AdColony [FATAL]", this.b.substring(i2, i3));
                }
            }
        }
    }

    static class c implements z {
        c() {
        }

        public void a(x xVar) {
            w.i = s.f(xVar.b(), "level");
        }
    }

    static class d implements z {
        d() {
        }

        public void a(x xVar) {
            w.b(s.f(xVar.b(), "module"), 3, s.h(xVar.b(), "message"), false);
        }
    }

    static class e implements z {
        e() {
        }

        public void a(x xVar) {
            w.b(s.f(xVar.b(), "module"), 3, s.h(xVar.b(), "message"), true);
        }
    }

    static class f implements z {
        f() {
        }

        public void a(x xVar) {
            w.b(s.f(xVar.b(), "module"), 2, s.h(xVar.b(), "message"), false);
        }
    }

    static class g implements z {
        g() {
        }

        public void a(x xVar) {
            w.b(s.f(xVar.b(), "module"), 2, s.h(xVar.b(), "message"), true);
        }
    }

    static class h implements z {
        h() {
        }

        public void a(x xVar) {
            w.b(s.f(xVar.b(), "module"), 1, s.h(xVar.b(), "message"), false);
        }
    }

    static class i implements z {
        i() {
        }

        public void a(x xVar) {
            w.b(s.f(xVar.b(), "module"), 1, s.h(xVar.b(), "message"), true);
        }
    }

    static class j implements z {
        j() {
        }

        public void a(x xVar) {
            w.b(s.f(xVar.b(), "module"), 0, s.h(xVar.b(), "message"), false);
        }
    }

    w() {
    }

    static void a(int i2, String str, boolean z) {
        b(0, i2, str, z);
    }

    static void b(int i2, int i3, String str, boolean z) {
        if (!a(a(i2, i3, str, z))) {
            synchronized (m) {
                m.add(a(i2, i3, str, z));
            }
        }
    }

    static void c() {
        ExecutorService executorService = l;
        if (executorService != null) {
            executorService.shutdown();
            try {
                if (!l.awaitTermination(1, TimeUnit.SECONDS)) {
                    l.shutdownNow();
                    if (!l.awaitTermination(1, TimeUnit.SECONDS)) {
                        System.err.println("ADCLogManager: ScheduledExecutorService did not terminate");
                    }
                }
            } catch (InterruptedException unused) {
                l.shutdownNow();
                Thread.currentThread().interrupt();
            }
        }
    }

    private static Runnable a(int i2, int i3, String str, boolean z) {
        return new b(i2, str, i3, z);
    }

    static boolean a(JSONObject jSONObject, int i2, boolean z) {
        int f2 = s.f(jSONObject, "print_level");
        boolean d2 = s.d(jSONObject, "log_private");
        if (jSONObject.length() == 0) {
            f2 = i;
            d2 = a;
        }
        boolean z2 = false;
        if ((z && !d2) || f2 == 4) {
            return false;
        }
        if (f2 >= i2) {
            z2 = true;
        }
        return z2;
    }

    static void b() {
        ExecutorService executorService = l;
        if (executorService == null || executorService.isShutdown() || l.isTerminated()) {
            l = Executors.newSingleThreadExecutor();
        }
        synchronized (m) {
            while (!m.isEmpty()) {
                a((Runnable) m.poll());
            }
        }
    }

    static boolean a(JSONObject jSONObject, int i2) {
        int f2 = s.f(jSONObject, "send_level");
        if (jSONObject.length() == 0) {
            f2 = k;
        }
        return f2 >= i2 && f2 != 4;
    }

    static void a(HashMap<String, Object> hashMap) {
        try {
            e0 e0Var = new e0(new t(new URL("https://wd.adcolony.com/logs")), Executors.newSingleThreadScheduledExecutor(), hashMap);
            n = e0Var;
            e0Var.a(5, TimeUnit.SECONDS);
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(JSONArray jSONArray) {
        j = a(jSONArray);
    }

    static void a(int i2, String str, int i3) {
        if (n != null) {
            if (i3 == 3 && a(s.g(j, Integer.toString(i2)), 3)) {
                n.a(str);
            } else if (i3 == 2 && a(s.g(j, Integer.toString(i2)), 2)) {
                n.c(str);
            } else if (i3 == 1 && a(s.g(j, Integer.toString(i2)), 1)) {
                n.d(str);
            } else if (i3 == 0 && a(s.g(j, Integer.toString(i2)), 0)) {
                n.b(str);
            }
        }
    }

    static void a(o oVar) {
        e0 e0Var = n;
        if (e0Var != null && k != 4) {
            e0Var.a(oVar);
        }
    }

    static void a() {
        a.a("Log.set_log_level", (z) new c());
        a.a("Log.public.trace", (z) new d());
        a.a("Log.private.trace", (z) new e());
        a.a("Log.public.info", (z) new f());
        a.a("Log.private.info", (z) new g());
        a.a("Log.public.warning", (z) new h());
        a.a("Log.private.warning", (z) new i());
        a.a("Log.public.error", (z) new j());
        a.a("Log.private.error", (z) new a());
    }

    /* access modifiers changed from: 0000 */
    public JSONObject a(JSONArray jSONArray) {
        JSONObject b2 = s.b();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject c2 = s.c(jSONArray, i2);
            s.a(b2, Integer.toString(s.f(c2, "id")), c2);
        }
        return b2;
    }

    private static boolean a(Runnable runnable) {
        try {
            if (l != null && !l.isShutdown() && !l.isTerminated()) {
                l.submit(runnable);
                return true;
            }
        } catch (RejectedExecutionException unused) {
            Log.e("ADCLogError", "Internal error when submitting log to executor service.");
        }
        return false;
    }
}
