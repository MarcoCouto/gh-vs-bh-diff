package com.adcolony.sdk;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewParent;

public class AdColonyAdViewActivity extends b {
    AdColonyAdView m;

    public AdColonyAdViewActivity() {
        AdColonyAdView adColonyAdView;
        if (!a.e()) {
            adColonyAdView = null;
        } else {
            adColonyAdView = a.c().e();
        }
        this.m = adColonyAdView;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        ViewParent parent = this.a.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.a);
        }
        this.m.a();
        a.c().a((AdColonyAdView) null);
        finish();
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        this.m.b();
    }

    public void onBackPressed() {
        b();
    }

    public /* bridge */ /* synthetic */ void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        if (a.e()) {
            AdColonyAdView adColonyAdView = this.m;
            if (adColonyAdView != null) {
                this.b = adColonyAdView.getOrientation();
                super.onCreate(bundle);
                this.m.b();
                AdColonyAdViewListener listener = this.m.getListener();
                if (listener != null) {
                    listener.onOpened(this.m);
                }
                return;
            }
        }
        a.c().a((AdColonyAdView) null);
        finish();
    }

    public /* bridge */ /* synthetic */ void onDestroy() {
        super.onDestroy();
    }

    public /* bridge */ /* synthetic */ void onPause() {
        super.onPause();
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    public /* bridge */ /* synthetic */ void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }
}
