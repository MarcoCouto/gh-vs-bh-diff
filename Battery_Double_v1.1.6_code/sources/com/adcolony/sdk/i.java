package com.adcolony.sdk;

import org.json.JSONObject;

class i implements z {

    class a implements Runnable {
        final /* synthetic */ String a;
        final /* synthetic */ String b;

        a(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        public void run() {
            new a().a("Received custom message ").a(this.a).a(" of type ").a(this.b).a(u.f);
            try {
                AdColonyCustomMessageListener adColonyCustomMessageListener = (AdColonyCustomMessageListener) a.c().g().get(this.b);
                if (adColonyCustomMessageListener != null) {
                    adColonyCustomMessageListener.onAdColonyCustomMessage(new AdColonyCustomMessage(this.b, this.a));
                }
            } catch (RuntimeException unused) {
            }
        }
    }

    i() {
        a.a("CustomMessage.controller_send", (z) this);
    }

    public void a(x xVar) {
        JSONObject b = xVar.b();
        k0.a((Runnable) new a(s.h(b, "message"), s.h(b, "type")));
    }
}
