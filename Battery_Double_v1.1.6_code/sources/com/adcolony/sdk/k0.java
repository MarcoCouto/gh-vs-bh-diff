package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.Toast;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.CRC32;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class k0 {
    static final int a = 128;
    static ExecutorService b = Executors.newSingleThreadExecutor();
    static Handler c;

    static class a implements Runnable {
        final /* synthetic */ Context a;
        final /* synthetic */ String b;
        final /* synthetic */ int c;

        a(Context context, String str, int i) {
            this.a = context;
            this.b = str;
            this.c = i;
        }

        public void run() {
            Toast.makeText(this.a, this.b, this.c).show();
        }
    }

    static class b {
        double a;
        double b = ((double) System.currentTimeMillis());

        b(double d) {
            a(d);
        }

        /* access modifiers changed from: 0000 */
        public void a(double d) {
            this.a = d;
            double currentTimeMillis = (double) System.currentTimeMillis();
            Double.isNaN(currentTimeMillis);
            this.b = (currentTimeMillis / 1000.0d) + this.a;
        }

        /* access modifiers changed from: 0000 */
        public double b() {
            double currentTimeMillis = (double) System.currentTimeMillis();
            Double.isNaN(currentTimeMillis);
            double d = this.b - (currentTimeMillis / 1000.0d);
            return d <= Utils.DOUBLE_EPSILON ? Utils.DOUBLE_EPSILON : d;
        }

        /* access modifiers changed from: 0000 */
        public void c() {
            a(this.a);
        }

        public String toString() {
            return k0.a(b(), 2);
        }

        /* access modifiers changed from: 0000 */
        public boolean a() {
            return b() == Utils.DOUBLE_EPSILON;
        }
    }

    k0() {
    }

    static int a(String str) {
        CRC32 crc32 = new CRC32();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            crc32.update(str.charAt(i));
        }
        return (int) crc32.getValue();
    }

    static String b(String str) {
        try {
            return n0.a(str);
        } catch (Exception unused) {
            return null;
        }
    }

    static boolean c() {
        boolean z = false;
        try {
            h c2 = a.c();
            StringBuilder sb = new StringBuilder();
            sb.append(c2.t().a());
            sb.append("026ae9c9824b3e483fa6c71fa88f57ae27816141");
            File file = new File(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(c2.t().a());
            sb2.append("7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5");
            File file2 = new File(sb2.toString());
            boolean a2 = c2.k().a(file);
            boolean a3 = c2.k().a(file2);
            if (a2 && a3) {
                z = true;
            }
            return z;
        } catch (Exception unused) {
            new a().a("Unable to delete controller or launch response.").a(u.j);
            return false;
        }
    }

    static String d() {
        Application application;
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        if (b2 instanceof Application) {
            application = (Application) b2;
        } else {
            application = ((Activity) b2).getApplication();
        }
        PackageManager packageManager = application.getPackageManager();
        try {
            CharSequence applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(b2.getPackageName(), 0));
            return applicationLabel == null ? "" : applicationLabel.toString();
        } catch (Exception unused) {
            new a().a("Failed to retrieve application label.").a(u.j);
            return "";
        }
    }

    static String e() {
        Context b2 = a.b();
        if (b2 == null) {
            return "1.0";
        }
        try {
            return b2.getPackageManager().getPackageInfo(b2.getPackageName(), 0).versionName;
        } catch (Exception unused) {
            new a().a("Failed to retrieve package info.").a(u.j);
            return "1.0";
        }
    }

    static boolean f(String str) {
        Application application;
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        try {
            if (b2 instanceof Application) {
                application = (Application) b2;
            } else {
                application = ((Activity) b2).getApplication();
            }
            application.getPackageManager().getApplicationInfo(str, 0);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    static String g() {
        String str = "landscape";
        Context b2 = a.b();
        return (!(b2 instanceof Activity) || b2.getResources().getConfiguration().orientation != 1) ? str : "portrait";
    }

    static boolean h(String str) {
        if (str != null && str.length() <= 128) {
            return true;
        }
        new a().a("String must be non-null and the max length is 128 characters.").a(u.g);
        return false;
    }

    static void i(String str) {
        File[] listFiles = new File(str).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    new a().a(">").a(file.getAbsolutePath()).a(u.d);
                    i(file.getAbsolutePath());
                } else {
                    new a().a(file.getAbsolutePath()).a(u.d);
                }
            }
        }
    }

    static int j(String str) {
        try {
            return (int) Long.parseLong(str, 16);
        } catch (NumberFormatException unused) {
            new a().a("Unable to parse '").a(str).a("' as a color.").a(u.h);
            return ViewCompat.MEASURED_STATE_MASK;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:4|5|6) */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0024, code lost:
        return new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", java.util.Locale.US).parse(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        return new java.text.SimpleDateFormat("yyyy-MM-dd", java.util.Locale.US).parse(r5);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0020 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0025 */
    static Date k(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ", Locale.US);
        return simpleDateFormat.parse(str);
    }

    static double b() {
        double currentTimeMillis = (double) System.currentTimeMillis();
        Double.isNaN(currentTimeMillis);
        return currentTimeMillis / 1000.0d;
    }

    static boolean b(AudioManager audioManager) {
        boolean z = false;
        if (audioManager == null) {
            new a().a("isAudioEnabled() called with a null AudioManager").a(u.j);
            return false;
        }
        try {
            if (audioManager.getStreamVolume(3) > 0) {
                z = true;
            }
            return z;
        } catch (Exception e) {
            new a().a("Exception occurred when accessing AudioManager.getStreamVolume: ").a(e.toString()).a(u.j);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    static int g(String str) {
        char c2;
        int hashCode = str.hashCode();
        if (hashCode != 729267099) {
            if (hashCode == 1430647483 && str.equals("landscape")) {
                c2 = 1;
                if (c2 != 0) {
                    return c2 != 1 ? -1 : 1;
                }
                return 0;
            }
        } else if (str.equals("portrait")) {
            c2 = 0;
            if (c2 != 0) {
            }
        }
        c2 = 65535;
        if (c2 != 0) {
        }
    }

    static String a() {
        return UUID.randomUUID().toString();
    }

    static boolean h() {
        Context b2 = a.b();
        return b2 != null && VERSION.SDK_INT >= 24 && (b2 instanceof Activity) && ((Activity) b2).isInMultiWindowMode();
    }

    static JSONArray a(int i) {
        JSONArray a2 = s.a();
        for (int i2 = 0; i2 < i; i2++) {
            s.b(a2, a());
        }
        return a2;
    }

    static int f() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0;
        }
        try {
            return b2.getPackageManager().getPackageInfo(b2.getPackageName(), 0).versionCode;
        } catch (Exception unused) {
            new a().a("Failed to retrieve package info.").a(u.j);
            return 0;
        }
    }

    static boolean a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length != strArr2.length) {
            return false;
        }
        Arrays.sort(strArr);
        Arrays.sort(strArr2);
        return Arrays.equals(strArr, strArr2);
    }

    private static void c(String str) {
        Context b2 = a.b();
        if (b2 != null) {
            try {
                InputStream open = b2.getAssets().open(str);
                StringBuilder sb = new StringBuilder();
                sb.append(a.c().t().c());
                sb.append(str);
                FileOutputStream fileOutputStream = new FileOutputStream(sb.toString());
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = open.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                open.close();
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (Exception e) {
                new a().a("Failed copy hardcoded ad unit file named: ").a(str).a(" with error: ").a(e.getMessage()).a(u.j);
            }
        }
    }

    static String e(String str) {
        return str == null ? "" : URLDecoder.decode(str);
    }

    static String b(@NonNull Context context) {
        try {
            return context.getPackageName();
        } catch (Exception unused) {
            return "unknown";
        }
    }

    static boolean a(Runnable runnable) {
        Looper mainLooper = Looper.getMainLooper();
        if (mainLooper == null) {
            return false;
        }
        if (c == null) {
            c = new Handler(mainLooper);
        }
        if (mainLooper == Looper.myLooper()) {
            runnable.run();
        } else {
            c.post(runnable);
        }
        return true;
    }

    static String b(JSONArray jSONArray) throws JSONException {
        String str = "";
        for (int i = 0; i < jSONArray.length(); i++) {
            if (i > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(",");
                str = sb.toString();
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(jSONArray.getInt(i));
            str = sb2.toString();
        }
        return str;
    }

    static int b(View view) {
        if (view == null) {
            return 0;
        }
        int[] iArr = {0, 0};
        view.getLocationOnScreen(iArr);
        return (int) (((float) iArr[1]) / a.c().h().n());
    }

    static int d(Context context) {
        if (context == null) {
            return 0;
        }
        int identifier = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return context.getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    static double a(AudioManager audioManager) {
        if (audioManager == null) {
            new a().a("getAudioVolume() called with a null AudioManager").a(u.j);
            return Utils.DOUBLE_EPSILON;
        }
        try {
            double streamVolume = (double) audioManager.getStreamVolume(3);
            double streamMaxVolume = (double) audioManager.getStreamMaxVolume(3);
            if (streamMaxVolume == Utils.DOUBLE_EPSILON) {
                return Utils.DOUBLE_EPSILON;
            }
            Double.isNaN(streamVolume);
            Double.isNaN(streamMaxVolume);
            return streamVolume / streamMaxVolume;
        } catch (Exception e) {
            new a().a("Exception occurred when accessing AudioManager: ").a(e.toString()).a(u.j);
            return Utils.DOUBLE_EPSILON;
        }
    }

    public static void d(String str) {
        Context b2 = a.b();
        if (b2 != null) {
            try {
                String[] list = b2.getAssets().list(str);
                if (list.length == 0) {
                    c(str);
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(a.c().t().c());
                    sb.append(str);
                    File file = new File(sb.toString());
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    for (String append : list) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append("/");
                        sb2.append(append);
                        d(sb2.toString());
                    }
                }
            } catch (IOException e) {
                new a().a("Failed copy hardcoded ad unit with error: ").a(e.getMessage()).a(u.j);
            }
        }
    }

    static JSONArray c(Context context) {
        JSONArray a2 = s.a();
        if (context == null) {
            return a2;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
            if (packageInfo.requestedPermissions == null) {
                return a2;
            }
            JSONArray a3 = s.a();
            int i = 0;
            while (i < packageInfo.requestedPermissions.length) {
                try {
                    a3.put(packageInfo.requestedPermissions[i]);
                    i++;
                } catch (Exception unused) {
                }
            }
            return a3;
        } catch (Exception unused2) {
            return a2;
        }
    }

    static AudioManager a(Context context) {
        if (context != null) {
            return (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        }
        new a().a("getAudioManager called with a null Context").a(u.j);
        return null;
    }

    static String a(double d, int i) {
        StringBuilder sb = new StringBuilder();
        a(d, i, sb);
        return sb.toString();
    }

    static void a(double d, int i, StringBuilder sb) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            sb.append(d);
            return;
        }
        if (d < Utils.DOUBLE_EPSILON) {
            d = -d;
            sb.append('-');
        }
        if (i == 0) {
            sb.append(Math.round(d));
            return;
        }
        long pow = (long) Math.pow(10.0d, (double) i);
        double d2 = (double) pow;
        Double.isNaN(d2);
        long round = Math.round(d * d2);
        sb.append(round / pow);
        sb.append('.');
        long j = round % pow;
        if (j == 0) {
            for (int i2 = 0; i2 < i; i2++) {
                sb.append('0');
            }
            return;
        }
        for (long j2 = j * 10; j2 < pow; j2 *= 10) {
            sb.append('0');
        }
        sb.append(j);
    }

    static String a(Exception exc) {
        StringWriter stringWriter = new StringWriter();
        exc.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    static boolean a(String str, File file) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[8192];
                while (true) {
                    try {
                        int read = fileInputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        instance.update(bArr, 0, read);
                    } catch (IOException e) {
                        throw new RuntimeException("Unable to process file for MD5", e);
                    } catch (Throwable th) {
                        try {
                            fileInputStream.close();
                        } catch (IOException unused) {
                            new a().a("Exception on closing MD5 input stream").a(u.j);
                        }
                        throw th;
                    }
                }
                String bigInteger = new BigInteger(1, instance.digest()).toString(16);
                boolean equals = str.equals(String.format("%40s", new Object[]{bigInteger}).replace(' ', '0'));
                try {
                    fileInputStream.close();
                } catch (IOException unused2) {
                    new a().a("Exception on closing MD5 input stream").a(u.j);
                }
                return equals;
            } catch (FileNotFoundException unused3) {
                new a().a("Exception while getting FileInputStream").a(u.j);
                return false;
            }
        } catch (NoSuchAlgorithmException unused4) {
            new a().a("Exception while getting Digest").a(u.j);
            return false;
        }
    }

    static String a(JSONArray jSONArray) throws JSONException {
        String str = "";
        for (int i = 0; i < jSONArray.length(); i++) {
            if (i > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(",");
                str = sb.toString();
            }
            switch (jSONArray.getInt(i)) {
                case 1:
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append("MO");
                    str = sb2.toString();
                    break;
                case 2:
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append("TU");
                    str = sb3.toString();
                    break;
                case 3:
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str);
                    sb4.append("WE");
                    str = sb4.toString();
                    break;
                case 4:
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(str);
                    sb5.append("TH");
                    str = sb5.toString();
                    break;
                case 5:
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append(str);
                    sb6.append("FR");
                    str = sb6.toString();
                    break;
                case 6:
                    StringBuilder sb7 = new StringBuilder();
                    sb7.append(str);
                    sb7.append("SA");
                    str = sb7.toString();
                    break;
                case 7:
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(str);
                    sb8.append("SU");
                    str = sb8.toString();
                    break;
            }
        }
        return str;
    }

    static boolean a(Intent intent, boolean z) {
        try {
            Context b2 = a.b();
            if (b2 == null) {
                return false;
            }
            AdColonyInterstitial d = a.c().d();
            if (d != null && d.g()) {
                d.e().e();
            }
            if (z) {
                b2.startActivity(Intent.createChooser(intent, "Handle this via..."));
            } else {
                b2.startActivity(intent);
            }
            return true;
        } catch (Exception e) {
            new a().a(e.toString()).a(u.h);
            return false;
        }
    }

    static boolean a(Intent intent) {
        return a(intent, false);
    }

    static boolean a(String str, int i) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        a((Runnable) new a(b2, str, i));
        return true;
    }

    static int a(g0 g0Var) {
        int i = 0;
        try {
            Context b2 = a.b();
            if (b2 != null) {
                int i2 = (int) (b2.getPackageManager().getPackageInfo(b2.getPackageName(), 0).lastUpdateTime / 1000);
                StringBuilder sb = new StringBuilder();
                sb.append(g0Var.a());
                sb.append("AppVersion");
                boolean exists = new File(sb.toString()).exists();
                boolean z = true;
                if (exists) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(g0Var.a());
                    sb2.append("AppVersion");
                    if (s.f(s.c(sb2.toString()), "last_update") != i2) {
                        i = 1;
                    } else {
                        z = false;
                    }
                } else {
                    i = 2;
                }
                if (z) {
                    JSONObject b3 = s.b();
                    s.b(b3, "last_update", i2);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(g0Var.a());
                    sb3.append("AppVersion");
                    s.i(b3, sb3.toString());
                }
            }
        } catch (Exception unused) {
        }
        return i;
    }

    static int a(View view) {
        if (view == null) {
            return 0;
        }
        int[] iArr = {0, 0};
        view.getLocationOnScreen(iArr);
        return (int) (((float) iArr[0]) / a.c().h().n());
    }
}
