package com.adcolony.sdk;

import android.support.annotation.NonNull;
import org.json.JSONObject;

public class AdColonyZone {
    public static final int BANNER = 1;
    public static final int INTERSTITIAL = 0;
    @Deprecated
    public static final int NATIVE = 2;
    static final int n = 0;
    static final int o = 1;
    static final int p = 2;
    static final int q = 3;
    static final int r = 4;
    static final int s = 5;
    static final int t = 6;
    private String a;
    private String b;
    private String c;
    private String d;
    private int e = 5;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private boolean l;
    private boolean m;

    AdColonyZone(@NonNull String str) {
        this.a = str;
    }

    private boolean a(boolean z) {
        if (a.e() && !a.c().y() && !a.c().z()) {
            return z;
        }
        c();
        return false;
    }

    private int c(int i2) {
        if (a.e() && !a.c().y() && !a.c().z()) {
            return i2;
        }
        c();
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.e == 0;
    }

    public int getPlayFrequency() {
        return c(this.i);
    }

    public int getRemainingViewsUntilReward() {
        return c(this.g);
    }

    public int getRewardAmount() {
        return c(this.j);
    }

    public String getRewardName() {
        return a(this.b);
    }

    public int getViewsPerReward() {
        return c(this.h);
    }

    public String getZoneID() {
        return a(this.a);
    }

    public int getZoneType() {
        return this.f;
    }

    public boolean isRewarded() {
        return this.m;
    }

    public boolean isValid() {
        return a(this.l);
    }

    /* access modifiers changed from: 0000 */
    public void b(int i2) {
        this.e = i2;
    }

    private String a(String str) {
        return a(str, "");
    }

    private void c() {
        new a().a("The AdColonyZone API is not available while AdColony is disabled.").a(u.i);
    }

    private String a(String str, String str2) {
        if (a.e() && !a.c().y() && !a.c().z()) {
            return str;
        }
        c();
        return str2;
    }

    /* access modifiers changed from: 0000 */
    public void a(x xVar) {
        JSONObject b2 = xVar.b();
        JSONObject g2 = s.g(b2, "reward");
        this.b = s.h(g2, "reward_name");
        this.j = s.f(g2, "reward_amount");
        this.h = s.f(g2, "views_per_reward");
        this.g = s.f(g2, "views_until_reward");
        this.c = s.h(g2, "reward_name_plural");
        this.d = s.h(g2, "reward_prompt");
        this.m = s.d(b2, "rewarded");
        this.e = s.f(b2, "status");
        this.f = s.f(b2, "type");
        this.i = s.f(b2, "play_interval");
        this.a = s.h(b2, "zone_id");
        boolean z = true;
        if (this.e == 1) {
            z = false;
        }
        this.l = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(int i2) {
        this.k = i2;
    }

    /* access modifiers changed from: 0000 */
    public int a() {
        return this.k;
    }
}
