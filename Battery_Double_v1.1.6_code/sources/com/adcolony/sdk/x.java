package com.adcolony.sdk;

import org.json.JSONException;
import org.json.JSONObject;

class x {
    private String a;
    private JSONObject b;

    x(JSONObject jSONObject) {
        try {
            this.b = jSONObject;
            this.a = jSONObject.getString("m_type");
        } catch (JSONException e) {
            new a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(u.j);
        }
    }

    /* access modifiers changed from: 0000 */
    public x a() {
        return a((JSONObject) null);
    }

    /* access modifiers changed from: 0000 */
    public JSONObject b() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public String c() {
        return this.a;
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        a.a(this.a, this.b);
    }

    /* access modifiers changed from: 0000 */
    public x a(String str) {
        return a(s.b(str));
    }

    /* access modifiers changed from: 0000 */
    public void b(JSONObject jSONObject) {
        this.b = jSONObject;
    }

    /* access modifiers changed from: 0000 */
    public x a(JSONObject jSONObject) {
        try {
            x xVar = new x("reply", this.b.getInt("m_origin"), jSONObject);
            xVar.b.put("m_id", this.b.getInt("m_id"));
            return xVar;
        } catch (JSONException e) {
            new a().a("JSON error in ADCMessage's createReply(): ").a(e.toString()).a(u.j);
            return new x("JSONException", 0);
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        this.a = str;
    }

    x(String str, int i) {
        try {
            this.a = str;
            JSONObject jSONObject = new JSONObject();
            this.b = jSONObject;
            jSONObject.put("m_target", i);
        } catch (JSONException e) {
            new a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(u.j);
        }
    }

    x(String str, int i, String str2) {
        try {
            this.a = str;
            JSONObject b2 = s.b(str2);
            this.b = b2;
            b2.put("m_target", i);
        } catch (JSONException e) {
            new a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(u.j);
        }
    }

    x(String str, int i, JSONObject jSONObject) {
        try {
            this.a = str;
            if (jSONObject == null) {
                jSONObject = new JSONObject();
            }
            this.b = jSONObject;
            jSONObject.put("m_target", i);
        } catch (JSONException e) {
            new a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(u.j);
        }
    }
}
