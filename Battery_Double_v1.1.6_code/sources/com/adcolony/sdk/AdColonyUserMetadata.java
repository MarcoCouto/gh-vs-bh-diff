package com.adcolony.sdk;

import android.location.Location;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyUserMetadata {
    public static final String USER_EDUCATION_ASSOCIATES_DEGREE = "associates_degree";
    public static final String USER_EDUCATION_BACHELORS_DEGREE = "bachelors_degree";
    public static final String USER_EDUCATION_GRADE_SCHOOL = "grade_school";
    public static final String USER_EDUCATION_GRADUATE_DEGREE = "graduate_degree";
    public static final String USER_EDUCATION_HIGH_SCHOOL_DIPLOMA = "high_school_diploma";
    public static final String USER_EDUCATION_SOME_COLLEGE = "some_college";
    public static final String USER_EDUCATION_SOME_HIGH_SCHOOL = "some_high_school";
    public static final String USER_FEMALE = "female";
    public static final String USER_MALE = "male";
    public static final String USER_MARRIED = "married";
    public static final String USER_SINGLE = "single";
    static final int d = 128;
    JSONArray a = s.a();
    JSONObject b = s.b();
    Location c;

    public AdColonyUserMetadata addUserInterest(@NonNull String str) {
        if (k0.h(str)) {
            s.b(this.a, str);
            s.a(this.b, "adc_interests", this.a);
        }
        return this;
    }

    public AdColonyUserMetadata clearUserInterests() {
        JSONArray a2 = s.a();
        this.a = a2;
        s.a(this.b, "adc_interests", a2);
        return this;
    }

    public Object getMetadata(@NonNull String str) {
        return s.b(this.b, str);
    }

    public int getUserAge() {
        return s.f(this.b, "adc_age");
    }

    public int getUserAnnualHouseholdIncome() {
        return s.f(this.b, "adc_household_income");
    }

    public String getUserEducation() {
        return s.h(this.b, "adc_education");
    }

    public String getUserGender() {
        return s.h(this.b, "adc_gender");
    }

    public String[] getUserInterests() {
        String[] strArr = new String[this.a.length()];
        for (int i = 0; i < this.a.length(); i++) {
            strArr[i] = s.d(this.a, i);
        }
        return strArr;
    }

    public Location getUserLocation() {
        return this.c;
    }

    public String getUserMaritalStatus() {
        return s.h(this.b, "adc_marital_status");
    }

    public String getUserZipCode() {
        return s.h(this.b, "adc_zip");
    }

    public AdColonyUserMetadata setMetadata(@NonNull String str, boolean z) {
        if (k0.h(str)) {
            s.b(this.b, str, z);
        }
        return this;
    }

    public AdColonyUserMetadata setUserAge(@IntRange(from = 0, to = 130) int i) {
        setMetadata("adc_age", (double) i);
        return this;
    }

    public AdColonyUserMetadata setUserAnnualHouseholdIncome(@IntRange(from = 0) int i) {
        setMetadata("adc_household_income", (double) i);
        return this;
    }

    public AdColonyUserMetadata setUserEducation(@NonNull String str) {
        if (k0.h(str)) {
            setMetadata("adc_education", str);
        }
        return this;
    }

    public AdColonyUserMetadata setUserGender(@NonNull String str) {
        if (k0.h(str)) {
            setMetadata("adc_gender", str);
        }
        return this;
    }

    public AdColonyUserMetadata setUserLocation(@NonNull Location location) {
        this.c = location;
        setMetadata("adc_longitude", location.getLongitude());
        setMetadata("adc_latitude", location.getLatitude());
        setMetadata("adc_speed", (double) location.getSpeed());
        setMetadata("adc_altitude", location.getAltitude());
        setMetadata("adc_time", (double) location.getTime());
        setMetadata("adc_accuracy", (double) location.getAccuracy());
        return this;
    }

    public AdColonyUserMetadata setUserMaritalStatus(@NonNull String str) {
        if (k0.h(str)) {
            setMetadata("adc_marital_status", str);
        }
        return this;
    }

    public AdColonyUserMetadata setUserZipCode(@NonNull String str) {
        if (k0.h(str)) {
            setMetadata("adc_zip", str);
        }
        return this;
    }

    public AdColonyUserMetadata setMetadata(@NonNull String str, double d2) {
        if (k0.h(str)) {
            s.a(this.b, str, d2);
        }
        return this;
    }

    public AdColonyUserMetadata setMetadata(@NonNull String str, @NonNull String str2) {
        if (k0.h(str2) && k0.h(str)) {
            s.a(this.b, str, str2);
        }
        return this;
    }
}
