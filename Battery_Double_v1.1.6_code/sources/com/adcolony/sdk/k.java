package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Build.VERSION;
import org.json.JSONObject;

class k {
    static AlertDialog d;
    /* access modifiers changed from: private */
    public x a;
    /* access modifiers changed from: private */
    public AlertDialog b;
    /* access modifiers changed from: private */
    public boolean c;

    class a implements z {
        a() {
        }

        public void a(x xVar) {
            if (!a.d() || !(a.b() instanceof Activity)) {
                new a().a("Missing Activity reference, can't build AlertDialog.").a(u.i);
                return;
            }
            if (s.d(xVar.b(), "on_resume")) {
                k.this.a = xVar;
            } else {
                k.this.a(xVar);
            }
        }
    }

    class b implements OnClickListener {
        final /* synthetic */ x a;

        b(x xVar) {
            this.a = xVar;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            k.this.b = null;
            dialogInterface.dismiss();
            JSONObject b2 = s.b();
            s.b(b2, "positive", true);
            k.this.c = false;
            this.a.a(b2).d();
        }
    }

    class c implements OnClickListener {
        final /* synthetic */ x a;

        c(x xVar) {
            this.a = xVar;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            k.this.b = null;
            dialogInterface.dismiss();
            JSONObject b2 = s.b();
            s.b(b2, "positive", false);
            k.this.c = false;
            this.a.a(b2).d();
        }
    }

    class d implements OnCancelListener {
        final /* synthetic */ x a;

        d(x xVar) {
            this.a = xVar;
        }

        public void onCancel(DialogInterface dialogInterface) {
            k.this.b = null;
            k.this.c = false;
            JSONObject b2 = s.b();
            s.b(b2, "positive", false);
            this.a.a(b2).d();
        }
    }

    class e implements Runnable {
        final /* synthetic */ Builder a;

        e(Builder builder) {
            this.a = builder;
        }

        public void run() {
            k.this.c = true;
            k.this.b = this.a.show();
        }
    }

    k() {
        a.a("Alert.show", (z) new a());
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        x xVar = this.a;
        if (xVar != null) {
            a(xVar);
            this.a = null;
        }
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"InlinedApi"})
    public void a(x xVar) {
        Context b2 = a.b();
        if (b2 != null) {
            Builder builder = VERSION.SDK_INT >= 21 ? new Builder(b2, 16974374) : new Builder(b2, 16974126);
            JSONObject b3 = xVar.b();
            String h = s.h(b3, "message");
            String h2 = s.h(b3, "title");
            String h3 = s.h(b3, "positive");
            String h4 = s.h(b3, "negative");
            builder.setMessage(h);
            builder.setTitle(h2);
            builder.setPositiveButton(h3, new b(xVar));
            if (!h4.equals("")) {
                builder.setNegativeButton(h4, new c(xVar));
            }
            builder.setOnCancelListener(new d(xVar));
            k0.a((Runnable) new e(builder));
        }
    }

    /* access modifiers changed from: 0000 */
    public AlertDialog a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void a(AlertDialog alertDialog) {
        this.b = alertDialog;
    }
}
