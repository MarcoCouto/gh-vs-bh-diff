package com.adcolony.sdk;

import android.support.annotation.NonNull;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

public class AdColonyCustomMessage {
    String a;
    String b;

    class a implements Runnable {
        a() {
        }

        public void run() {
            AdColony.a();
            JSONObject b = s.b();
            s.a(b, "type", AdColonyCustomMessage.this.a);
            s.a(b, "message", AdColonyCustomMessage.this.b);
            new x("CustomMessage.native_send", 1, b).d();
        }
    }

    public AdColonyCustomMessage(@NonNull String str, @NonNull String str2) {
        if (k0.h(str) || k0.h(str2)) {
            this.a = str;
            this.b = str2;
        }
    }

    public String getMessage() {
        return this.b;
    }

    public String getType() {
        return this.a;
    }

    public void send() {
        try {
            AdColony.a.execute(new a());
        } catch (RejectedExecutionException unused) {
        }
    }

    public AdColonyCustomMessage set(String str, String str2) {
        this.a = str;
        this.b = str2;
        return this;
    }
}
