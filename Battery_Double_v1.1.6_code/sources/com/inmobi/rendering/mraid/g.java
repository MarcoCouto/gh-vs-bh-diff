package com.inmobi.rendering.mraid;

import com.ironsource.sdk.constants.Constants.ForceClosePosition;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ResizeProperties */
public class g {
    private static final String g = "g";
    String a = ForceClosePosition.TOP_RIGHT;
    int b;
    int c;
    int d = 0;
    int e = 0;
    boolean f = true;

    public static g a(String str, g gVar) {
        g gVar2 = new g();
        try {
            JSONObject jSONObject = new JSONObject(str);
            gVar2.b = jSONObject.getInt("width");
            gVar2.c = jSONObject.getInt("height");
            gVar2.d = jSONObject.getInt("offsetX");
            gVar2.e = jSONObject.getInt("offsetY");
            if (gVar == null) {
                return gVar2;
            }
            gVar2.a = jSONObject.optString("customClosePosition", gVar.a);
            gVar2.f = jSONObject.optBoolean("allowOffscreen", gVar.f);
            return gVar2;
        } catch (JSONException unused) {
            return null;
        }
    }

    public final String a() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("width", this.b);
            jSONObject.put("height", this.c);
            jSONObject.put("customClosePosition", this.a);
            jSONObject.put("offsetX", this.d);
            jSONObject.put("offsetY", this.e);
            jSONObject.put("allowOffscreen", this.f);
            return jSONObject.toString();
        } catch (JSONException unused) {
            return "";
        }
    }
}
