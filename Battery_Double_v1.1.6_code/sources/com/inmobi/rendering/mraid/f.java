package com.inmobi.rendering.mraid;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OrientationProperties */
public class f {
    private static String e = "f";
    public boolean a = true;
    public String b = "none";
    public String c = "right";
    public String d = null;

    public static f a(String str, f fVar) {
        f fVar2 = new f();
        fVar2.d = str;
        try {
            JSONObject jSONObject = new JSONObject(str);
            fVar2.b = jSONObject.optString("forceOrientation", fVar.b);
            fVar2.a = jSONObject.optBoolean("allowOrientationChange", fVar.a);
            fVar2.c = jSONObject.optString("direction", fVar.c);
            if (!fVar2.b.equals("portrait") && !fVar2.b.equals("landscape")) {
                fVar2.b = "none";
            }
            if (fVar2.c.equals("left") || fVar2.c.equals("right")) {
                return fVar2;
            }
            fVar2.c = "right";
            return fVar2;
        } catch (JSONException unused) {
            return null;
        }
    }
}
