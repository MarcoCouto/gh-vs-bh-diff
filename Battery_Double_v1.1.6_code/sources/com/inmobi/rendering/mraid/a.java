package com.inmobi.rendering.mraid;

import com.inmobi.commons.core.utilities.b.c;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ExpandProperties */
public class a {
    private static final String d = "a";
    public boolean a = false;
    public boolean b;
    public String c;
    private int e = c.a().a;
    private int f = c.a().b;
    private boolean g = true;

    public a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("width", this.e);
            jSONObject.put("height", this.f);
            jSONObject.put("useCustomClose", this.a);
            jSONObject.put("isModal", this.g);
        } catch (JSONException e2) {
            new StringBuilder("Exception in composing ExpandProperties: ").append(e2.getMessage());
        }
        this.c = jSONObject.toString();
    }

    public static a a(String str) {
        a aVar = new a();
        aVar.c = str;
        try {
            JSONObject jSONObject = new JSONObject(str);
            aVar.g = true;
            if (jSONObject.has("useCustomClose")) {
                aVar.b = true;
            }
            aVar.a = jSONObject.optBoolean("useCustomClose", false);
        } catch (JSONException unused) {
        }
        return aVar;
    }
}
