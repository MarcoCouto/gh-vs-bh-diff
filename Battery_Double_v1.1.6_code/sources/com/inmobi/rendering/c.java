package com.inmobi.rendering;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings.System;
import android.support.annotation.Nullable;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.chartboost.sdk.CBLocation;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.google.android.exoplayer2.util.MimeTypes;
import com.inmobi.a.n;
import com.inmobi.ads.AdContainer;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.ah;
import com.inmobi.ads.ak;
import com.inmobi.ads.bn;
import com.inmobi.ads.bs;
import com.inmobi.commons.core.configs.h;
import com.inmobi.commons.core.network.a.C0051a;
import com.inmobi.commons.core.network.d;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.rendering.mraid.MediaRenderView;
import com.inmobi.rendering.mraid.MraidMediaProcessor;
import com.inmobi.rendering.mraid.MraidMediaProcessor.HeadphonesPluggedChangeReceiver;
import com.inmobi.rendering.mraid.MraidMediaProcessor.RingerModeChangeReceiver;
import com.inmobi.rendering.mraid.b;
import com.inmobi.rendering.mraid.f;
import com.inmobi.rendering.mraid.g;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.smaato.sdk.core.api.VideoType;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.tapjoy.TJAdUnitConstants.String;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JavaScriptBridge */
public class c {
    static final String[] a = {"tel", "sms", MRAIDNativeFeature.CALENDAR, MRAIDNativeFeature.INLINE_VIDEO};
    /* access modifiers changed from: private */
    public static final String b = "c";
    /* access modifiers changed from: private */
    public RenderView c;
    private RenderingProperties d;
    private f e;

    @TargetApi(16)
    /* compiled from: JavaScriptBridge */
    private static class a implements OnGlobalLayoutListener {
        /* access modifiers changed from: private */
        public int a;
        /* access modifiers changed from: private */
        public int b;
        private View c;
        /* access modifiers changed from: private */
        public final Boolean d = Boolean.valueOf(false);

        a(View view) {
            this.c = view;
        }

        public final void onGlobalLayout() {
            try {
                this.a = com.inmobi.commons.core.utilities.b.c.b(this.c.getWidth());
                this.b = com.inmobi.commons.core.utilities.b.c.b(this.c.getHeight());
                if (VERSION.SDK_INT >= 16) {
                    this.c.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    this.c.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                synchronized (this.d) {
                    this.d.notify();
                }
            } catch (Exception e) {
                c.b;
                new StringBuilder("SDK encountered unexpected error in JavaScriptBridge$1.onGlobalLayout(); ").append(e.getMessage());
            }
        }
    }

    @JavascriptInterface
    public String getPlatform(String str) {
        return "android";
    }

    @JavascriptInterface
    public String getSdkVersion(String str) {
        return "7.3.0";
    }

    @JavascriptInterface
    public String getVersion(String str) {
        return "2.0";
    }

    @JavascriptInterface
    public void log(String str, String str2) {
    }

    @JavascriptInterface
    public void onOrientationChange(String str) {
    }

    @JavascriptInterface
    public void showAlert(String str, String str2) {
    }

    @JavascriptInterface
    public void storePicture(String str, String str2) {
    }

    c(RenderView renderView, RenderingProperties renderingProperties) {
        this.c = renderView;
        this.d = renderingProperties;
    }

    @JavascriptInterface
    public void open(final String str, final String str2) {
        if (this.c != null) {
            if (!this.c.e()) {
                this.c.c(MraidJsMethods.OPEN);
            } else {
                new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            c.this.c.c(MraidJsMethods.OPEN, str, str2);
                        } catch (Exception e) {
                            c.this.c.b(str, "Unexpected error", MraidJsMethods.OPEN);
                            Logger.a(InternalLogLevel.ERROR, "InMobi", "Failed to open URL; SDK encountered unexpected error");
                            c.b;
                            new StringBuilder("SDK encountered unexpected error in handling open() request from creative; ").append(e.getMessage());
                        }
                    }
                });
            }
        }
    }

    @JavascriptInterface
    public void openEmbedded(final String str, final String str2) {
        if (this.c != null) {
            if (!this.c.e()) {
                this.c.c("openEmbedded");
            } else {
                new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            c.this.c.c("openEmbedded", str, str2);
                        } catch (Exception e) {
                            c.this.c.b(str, "Unexpected error", "openEmbedded");
                            Logger.a(InternalLogLevel.ERROR, "InMobi", "Failed to open URL; SDK encountered unexpected error");
                            c.b;
                            new StringBuilder("SDK encountered unexpected error in handling openEmbedded() request from creative; ").append(e.getMessage());
                        }
                    }
                });
            }
        }
    }

    @JavascriptInterface
    public void ping(String str, String str2, boolean z) {
        if (this.c != null) {
            if (str2 == null || str2.trim().length() == 0 || !URLUtil.isValidUrl(str2)) {
                RenderView renderView = this.c;
                StringBuilder sb = new StringBuilder("Invalid URL:");
                sb.append(str2);
                renderView.b(str, sb.toString(), "ping");
                return;
            }
            StringBuilder sb2 = new StringBuilder("JavaScript called ping() URL: >>> ");
            sb2.append(str2);
            sb2.append(" <<<");
            try {
                new Thread(str2, z) {
                    final /* synthetic */ String a;
                    final /* synthetic */ boolean b;

                    {
                        this.a = r2;
                        this.b = r3;
                    }

                    public final void run() {
                        try {
                            h hVar = new h();
                            com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) hVar, (com.inmobi.commons.core.configs.b.c) null);
                            if (!hVar.g) {
                                a aVar = new a(this.a, this.b, false, c.j.a + 1);
                                c.a;
                                StringBuilder sb = new StringBuilder("Received click (");
                                sb.append(aVar.b);
                                sb.append(") for pinging over HTTP");
                                c.a(c.this, aVar);
                            }
                        } catch (Exception e) {
                            c.a;
                            new StringBuilder("SDK encountered unexpected error in pinging click; ").append(e.getMessage());
                        }
                    }
                }.start();
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "ping");
                Logger.a(InternalLogLevel.ERROR, "InMobi", "Failed to fire ping; SDK encountered unexpected error");
                new StringBuilder("SDK encountered unexpected error in handling ping() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void pingInWebView(String str, String str2, boolean z) {
        if (this.c != null) {
            if (str2 == null || str2.trim().length() == 0 || !URLUtil.isValidUrl(str2)) {
                RenderView renderView = this.c;
                StringBuilder sb = new StringBuilder("Invalid URL:");
                sb.append(str2);
                renderView.b(str, sb.toString(), "pingInWebView");
                return;
            }
            StringBuilder sb2 = new StringBuilder("JavaScript called pingInWebView() URL: >>> ");
            sb2.append(str2);
            sb2.append(" <<<");
            try {
                new Thread(str2, z) {
                    final /* synthetic */ String a;
                    final /* synthetic */ boolean b;

                    {
                        this.a = r2;
                        this.b = r3;
                    }

                    public final void run() {
                        try {
                            h hVar = new h();
                            com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) hVar, (com.inmobi.commons.core.configs.b.c) null);
                            if (!hVar.g) {
                                a aVar = new a(this.a, this.b, true, c.j.a + 1);
                                c.a;
                                StringBuilder sb = new StringBuilder("Received click (");
                                sb.append(aVar.b);
                                sb.append(") for pinging in WebView");
                                c.a(c.this, aVar);
                            }
                        } catch (Exception e) {
                            c.a;
                            new StringBuilder("SDK encountered unexpected error in pinging click over WebView; ").append(e.getMessage());
                        }
                    }
                }.start();
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "pingInWebView");
                Logger.a(InternalLogLevel.ERROR, "InMobi", "Failed to fire ping; SDK encountered unexpected error");
                new StringBuilder("SDK encountered unexpected error in handling pingInWebView() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public String getPlatformVersion(String str) {
        return Integer.toString(VERSION.SDK_INT);
    }

    @JavascriptInterface
    public void fireAdReady(String str) {
        try {
            this.c.getListener().u();
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "fireAdReady");
            new StringBuilder("SDK encountered unexpected error in handling fireAdReady() signal from creative; ").append(e2.getMessage());
        }
    }

    @JavascriptInterface
    public void fireAdFailed(String str) {
        try {
            this.c.getListener().w();
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "fireAdFailed");
            new StringBuilder("SDK encountered unexpected error in handling fireAdFailed() signal from creative; ").append(e2.getMessage());
        }
    }

    @JavascriptInterface
    public String getDefaultPosition(String str) {
        if (this.c == null) {
            return new JSONObject().toString();
        }
        synchronized (this.c.getDefaultPositionMonitor()) {
            this.c.setDefaultPositionLock();
            new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        c.this.c.setDefaultPosition();
                    } catch (Exception e) {
                        c.b;
                        new StringBuilder("SDK encountered unexpected error in getting/setting default position; ").append(e.getMessage());
                    }
                }
            });
            while (this.c.j) {
                try {
                    this.c.getDefaultPositionMonitor().wait();
                } catch (InterruptedException unused) {
                }
            }
        }
        return this.c.getDefaultPosition();
    }

    @JavascriptInterface
    public String getCurrentPosition(String str) {
        if (this.c == null) {
            return "";
        }
        synchronized (this.c.getCurrentPositionMonitor()) {
            this.c.setCurrentPositionLock();
            new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        c.this.c.setCurrentPosition();
                    } catch (Exception e) {
                        c.b;
                        new StringBuilder("SDK encountered unexpected error in getting/setting current position; ").append(e.getMessage());
                    }
                }
            });
            while (this.c.k) {
                try {
                    this.c.getCurrentPositionMonitor().wait();
                } catch (InterruptedException unused) {
                }
            }
        }
        return this.c.getCurrentPosition();
    }

    @JavascriptInterface
    public void setExpandProperties(String str, String str2) {
        if (this.c != null && !"Expanded".equals(this.c.getState())) {
            try {
                this.c.setExpandProperties(com.inmobi.rendering.mraid.a.a(str2));
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "setExpandProperties");
                new StringBuilder("SDK encountered unexpected error in setExpandProperties(); ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public String getExpandProperties(String str) {
        if (this.c == null) {
            return "";
        }
        return this.c.getExpandProperties().c;
    }

    @JavascriptInterface
    public void expand(final String str, final String str2) {
        if (this.d.a != PlacementType.PLACEMENT_TYPE_FULLSCREEN && this.c != null) {
            if (!this.c.e()) {
                this.c.c("expand");
            } else if (!this.c.n) {
                this.c.b(str, "Creative is not visible. Ignoring request.", "expand");
            } else if (str2 == null || str2.length() == 0 || str2.startsWith("http")) {
                new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
                    public final void run() {
                        int i;
                        try {
                            RenderView a2 = c.this.c;
                            String str = str;
                            String str2 = str2;
                            if (CBLocation.LOCATION_DEFAULT.equals(a2.d) || "Resized".equals(a2.d)) {
                                a2.s = true;
                                b bVar = a2.f;
                                if (bVar.c == null) {
                                    bVar.c = (ViewGroup) bVar.a.getParent();
                                    bVar.d = bVar.c.indexOfChild(bVar.a);
                                }
                                if (bVar.a != null) {
                                    com.inmobi.rendering.mraid.a expandProperties = bVar.a.getExpandProperties();
                                    bVar.b = URLUtil.isValidUrl(str2);
                                    if (bVar.b) {
                                        RenderView renderView = new RenderView(bVar.a.getContainerContext(), new RenderingProperties(PlacementType.PLACEMENT_TYPE_INLINE), null, bVar.a.getImpressionId());
                                        renderView.a(bVar.a.getListener(), bVar.a.getAdConfig());
                                        renderView.setOriginalRenderView(bVar.a);
                                        renderView.loadUrl(str2);
                                        renderView.setPlacementId(bVar.a.getPlacementId());
                                        renderView.setAllowAutoRedirection(bVar.a.getAllowAutoRedirection());
                                        renderView.setCreativeId(bVar.a.getCreativeId());
                                        i = InMobiAdActivity.a((AdContainer) renderView);
                                        if (expandProperties != null) {
                                            renderView.setUseCustomClose(bVar.a.l);
                                        }
                                    } else {
                                        FrameLayout frameLayout = new FrameLayout(bVar.a.getContainerContext());
                                        LayoutParams layoutParams = new LayoutParams(bVar.a.getWidth(), bVar.a.getHeight());
                                        frameLayout.setId(SupportMenu.USER_MASK);
                                        bVar.c.addView(frameLayout, bVar.d, layoutParams);
                                        bVar.c.removeView(bVar.a);
                                        i = InMobiAdActivity.a((AdContainer) bVar.a);
                                    }
                                    bVar.a.getListener().E();
                                    Intent intent = new Intent(bVar.a.getContainerContext(), InMobiAdActivity.class);
                                    intent.putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE", 102);
                                    intent.putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_INDEX", i);
                                    intent.putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_TYPE", 200);
                                    com.inmobi.commons.a.a.a(bVar.a.getContainerContext(), intent);
                                }
                                a2.requestLayout();
                                a2.invalidate();
                                a2.m = true;
                                a2.setFocusable(true);
                                a2.setFocusableInTouchMode(true);
                                a2.requestFocus();
                                HashMap hashMap = new HashMap();
                                hashMap.put(String.COMMAND, "expand");
                                hashMap.put("scheme", bs.a(str));
                                a2.c.b("CreativeInvokedAction", hashMap);
                                return;
                            }
                            new StringBuilder("Render view state must be either DEFAULT or RESIZED to admit the expand request. Current state:").append(a2.d);
                        } catch (Exception e) {
                            c.this.c.b(str, "Unexpected error", "expand");
                            Logger.a(InternalLogLevel.ERROR, "InMobi", "Failed to expand ad; SDK encountered an unexpected error");
                            c.b;
                            new StringBuilder("SDK encountered unexpected error in handling expand() request; ").append(e.getMessage());
                        }
                    }
                });
            } else {
                this.c.b(str, "Invalid URL", "expand");
            }
        }
    }

    @JavascriptInterface
    public void setResizeProperties(String str, String str2) {
        if (this.c != null) {
            g a2 = g.a(str2, this.c.getResizeProperties());
            if (a2 == null) {
                this.c.b(str, "setResizeProperties", "All mandatory fields are not present");
            }
            this.c.setResizeProperties(a2);
        }
    }

    @JavascriptInterface
    public String getResizeProperties(String str) {
        if (this.c == null) {
            return "";
        }
        g resizeProperties = this.c.getResizeProperties();
        if (resizeProperties == null) {
            return "";
        }
        return resizeProperties.a();
    }

    @JavascriptInterface
    public void resize(final String str) {
        if (this.d.a != PlacementType.PLACEMENT_TYPE_FULLSCREEN && this.c != null) {
            if (this.c.n) {
                new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            RenderView a2 = c.this.c;
                            String str = str;
                            if ((CBLocation.LOCATION_DEFAULT.equals(a2.d) || "Resized".equals(a2.d)) && a2.getResizeProperties() != null) {
                                a2.s = true;
                                a2.g.a();
                                a2.requestLayout();
                                a2.invalidate();
                                a2.m = true;
                                a2.setFocusable(true);
                                a2.setFocusableInTouchMode(true);
                                a2.requestFocus();
                                a2.setAndUpdateViewState("Resized");
                                a2.c.c(a2);
                                a2.s = false;
                                HashMap hashMap = new HashMap();
                                hashMap.put(String.COMMAND, MraidJsMethods.RESIZE);
                                hashMap.put("scheme", bs.a(str));
                                a2.c.b("CreativeInvokedAction", hashMap);
                            }
                        } catch (Exception e) {
                            c.this.c.b(str, "Unexpected error", MraidJsMethods.RESIZE);
                            Logger.a(InternalLogLevel.ERROR, c.b, "Could not resize ad; SDK encountered an unexpected error");
                            c.b;
                            new StringBuilder("SDK encountered an unexpected error in handling resize() request; ").append(e.getMessage());
                        }
                    }
                });
            } else {
                this.c.b(str, "Creative is not visible. Ignoring request.", MraidJsMethods.RESIZE);
            }
        }
    }

    @JavascriptInterface
    public void setOrientationProperties(String str, String str2) {
        this.e = f.a(str2, this.c.getOrientationProperties());
        this.c.setOrientationProperties(this.e);
    }

    @JavascriptInterface
    public String getOrientationProperties(String str) {
        return this.e.d;
    }

    @JavascriptInterface
    public boolean isViewable(String str) {
        if (this.c == null) {
            return false;
        }
        return this.c.n;
    }

    @JavascriptInterface
    public void useCustomClose(final String str, final boolean z) {
        new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
            public final void run() {
                try {
                    c.this.c.b(z);
                } catch (Exception e) {
                    c.this.c.b(str, "Unexpected error", "useCustomClose");
                    c.b;
                    new StringBuilder("SDK encountered internal error in handling useCustomClose() request from creative; ").append(e.getMessage());
                }
            }
        });
    }

    @JavascriptInterface
    public void playVideo(final String str, final String str2) {
        if (this.c != null) {
            if (str2 == null || str2.trim().length() == 0 || !str2.startsWith("http") || (!str2.endsWith("mp4") && !str2.endsWith("avi") && !str2.endsWith("m4v"))) {
                this.c.b(str, "Null or empty or invalid media playback URL supplied", MraidJsMethods.PLAY_VIDEO);
                return;
            }
            StringBuilder sb = new StringBuilder("JavaScript called: playVideo (");
            sb.append(str2);
            sb.append(")");
            new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        RenderView a2 = c.this.c;
                        String str = str;
                        String trim = str2.trim();
                        if (PlacementType.PLACEMENT_TYPE_FULLSCREEN == a2.e.a || "Expanded".equals(a2.getViewState())) {
                            if (a2.b != null) {
                                if (a2.b.get() != null) {
                                    a2.setAdActiveFlag(true);
                                    MraidMediaProcessor mraidMediaProcessor = a2.h;
                                    Activity activity = (Activity) a2.b.get();
                                    mraidMediaProcessor.b = new MediaRenderView(activity);
                                    MediaRenderView mediaRenderView = mraidMediaProcessor.b;
                                    mediaRenderView.h = MediaRenderView.a(trim);
                                    mediaRenderView.g = "anonymous";
                                    if (mediaRenderView.b == null) {
                                        mediaRenderView.b = Bitmap.createBitmap(24, 24, Config.ARGB_8888);
                                        mediaRenderView.b = MediaRenderView.b(mediaRenderView.h);
                                    }
                                    ViewGroup viewGroup = (ViewGroup) activity.findViewById(16908290);
                                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                                    layoutParams.addRule(13);
                                    mraidMediaProcessor.b.setLayoutParams(layoutParams);
                                    RelativeLayout relativeLayout = new RelativeLayout(activity);
                                    relativeLayout.setOnTouchListener(new OnTouchListener() {
                                        public final boolean onTouch(View view, MotionEvent motionEvent) {
                                            return true;
                                        }
                                    });
                                    relativeLayout.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                                    relativeLayout.addView(mraidMediaProcessor.b);
                                    viewGroup.addView(relativeLayout, new LayoutParams(-1, -1));
                                    mraidMediaProcessor.b.c = relativeLayout;
                                    mraidMediaProcessor.b.requestFocus();
                                    mraidMediaProcessor.b.setOnKeyListener(new OnKeyListener() {
                                        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                                            if (4 != i || keyEvent.getAction() != 0) {
                                                return false;
                                            }
                                            MraidMediaProcessor.this.b.a();
                                            return true;
                                        }
                                    });
                                    mraidMediaProcessor.b.d = new a() {
                                        public final void a(MediaRenderView mediaRenderView) {
                                            MraidMediaProcessor.f;
                                            MraidMediaProcessor.this.a.setAdActiveFlag(false);
                                            ViewGroup viewGroup = mediaRenderView.c;
                                            if (viewGroup != null) {
                                                ((ViewGroup) viewGroup.getParent()).removeView(viewGroup);
                                            }
                                            mediaRenderView.c = null;
                                        }

                                        public final void a() {
                                            MraidMediaProcessor.f;
                                        }
                                    };
                                    MediaRenderView mediaRenderView2 = mraidMediaProcessor.b;
                                    mediaRenderView2.setVideoPath(mediaRenderView2.h);
                                    mediaRenderView2.setOnCompletionListener(mediaRenderView2);
                                    mediaRenderView2.setOnPreparedListener(mediaRenderView2);
                                    mediaRenderView2.setOnErrorListener(mediaRenderView2);
                                    if (mediaRenderView2.a == null && VERSION.SDK_INT >= 19) {
                                        mediaRenderView2.a = new CustomMediaController(mediaRenderView2.getContext());
                                        mediaRenderView2.a.setAnchorView(mediaRenderView2);
                                        mediaRenderView2.setMediaController(mediaRenderView2.a);
                                    }
                                    HashMap hashMap = new HashMap();
                                    hashMap.put(String.COMMAND, MraidJsMethods.PLAY_VIDEO);
                                    hashMap.put("scheme", bs.a(str));
                                    a2.c.b("CreativeInvokedAction", hashMap);
                                }
                            }
                            a2.b(str, "Media playback is  not allowed before it is visible! Ignoring request ...", MraidJsMethods.PLAY_VIDEO);
                        }
                    } catch (Exception e) {
                        c.this.c.b(str, "Unexpected error", MraidJsMethods.PLAY_VIDEO);
                        Logger.a(InternalLogLevel.ERROR, "InMobi", "Error playing video; SDK encountered an unexpected error");
                        c.b;
                        new StringBuilder("SDK encountered unexpected error in handling playVideo() request from creative; ").append(e.getMessage());
                    }
                }
            });
        }
    }

    @JavascriptInterface
    public String getState(String str) {
        return this.c.getState().toLowerCase(Locale.ENGLISH);
    }

    @JavascriptInterface
    public String getScreenSize(String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("width", com.inmobi.commons.core.utilities.b.c.a().a);
            jSONObject.put("height", com.inmobi.commons.core.utilities.b.c.a().b);
        } catch (JSONException unused) {
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "getScreenSize");
            new StringBuilder("SDK encountered unexpected error while getting screen dimensions; ").append(e2.getMessage());
        }
        return jSONObject.toString();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:1|2|(2:4|(2:6|7)(1:8))|9|(3:13|5b|21)|25|26|27|28|29) */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0066 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x007f */
    @JavascriptInterface
    public String getMaxSize(String str) {
        int b2;
        int c2;
        JSONObject jSONObject = new JSONObject();
        try {
            Activity fullScreenActivity = this.c.getFullScreenActivity();
            if (fullScreenActivity == null) {
                if (!(this.c.getContainerContext() instanceof Activity)) {
                    return getScreenSize(str);
                }
                fullScreenActivity = (Activity) this.c.getContainerContext();
            }
            FrameLayout frameLayout = (FrameLayout) fullScreenActivity.findViewById(16908290);
            int b3 = com.inmobi.commons.core.utilities.b.c.b(frameLayout.getWidth());
            int b4 = com.inmobi.commons.core.utilities.b.c.b(frameLayout.getHeight());
            if (this.c.getFullScreenActivity() != null && (b3 == 0 || b4 == 0)) {
                a aVar = new a(frameLayout);
                frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(aVar);
                synchronized (aVar.d) {
                    aVar.d.wait();
                    b2 = aVar.a;
                    c2 = aVar.b;
                }
                int i = b2;
                b4 = c2;
                b3 = i;
            }
            jSONObject.put("width", b3);
            jSONObject.put("height", b4);
            new StringBuilder("getMaxSize called:").append(jSONObject.toString());
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "getMaxSize");
            new StringBuilder("SDK encountered unexpected error in handling getMaxSize() request from creative; ").append(e2.getMessage());
        }
        return jSONObject.toString();
    }

    @JavascriptInterface
    public void close(final String str) {
        new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
            public final void run() {
                try {
                    c.this.c.getReferenceContainer().b();
                } catch (Exception e) {
                    c.this.c.b(str, "Unexpected error", "close");
                    Logger.a(InternalLogLevel.ERROR, "InMobi", "Failed to close ad; SDK encountered an unexpected error");
                    c.b;
                    new StringBuilder("SDK encountered an expected error in handling the close() request from creative; ").append(e.getMessage());
                }
            }
        });
    }

    @JavascriptInterface
    public String getPlacementType(String str) {
        return PlacementType.PLACEMENT_TYPE_FULLSCREEN == this.d.a ? VideoType.INTERSTITIAL : String.INLINE;
    }

    @JavascriptInterface
    public String supports(String str, String str2) {
        if (Arrays.asList(a).contains(str2) || this.c.e(str2)) {
            return String.valueOf(this.c.e(str2));
        }
        return "false";
    }

    @JavascriptInterface
    public void openExternal(String str, String str2, @Nullable String str3) {
        if (this.c != null) {
            if (!this.c.e()) {
                this.c.c("openExternal");
            } else {
                this.c.a("openExternal", str, str2, str3);
            }
        }
    }

    @JavascriptInterface
    public void asyncPing(String str, String str2) {
        if (!URLUtil.isValidUrl(str2)) {
            this.c.b(str, "Invalid url", "asyncPing");
            return;
        }
        try {
            HashMap hashMap = new HashMap();
            hashMap.put(String.COMMAND, "ping");
            hashMap.put("scheme", bs.a(str));
            this.c.a("CreativeInvokedAction", (Map<String, Object>) hashMap);
            final com.inmobi.commons.core.network.c cVar = new com.inmobi.commons.core.network.c(HttpRequest.METHOD_GET, str2);
            cVar.t = false;
            cVar.z = false;
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            new com.inmobi.commons.core.network.a(cVar, new C0051a() {
                public final void a(d dVar) {
                    c.b;
                    try {
                        n.a().a(cVar.g());
                        n.a().b(dVar.c());
                        n.a().c(SystemClock.elapsedRealtime() - elapsedRealtime);
                    } catch (Exception e) {
                        c.b;
                        new StringBuilder("Error in setting request-response data size. ").append(e.getMessage());
                    }
                }

                public final void b(d dVar) {
                    c.b;
                }
            }).a();
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "asyncPing");
            new StringBuilder("SDK encountered internal error in handling asyncPing() request from creative; ").append(e2.getMessage());
        }
    }

    @JavascriptInterface
    public void disableCloseRegion(final String str, final boolean z) {
        if (this.c != null) {
            new Handler(this.c.getContainerContext().getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        c.this.c.a(z);
                    } catch (Exception e) {
                        c.this.c.b(str, "Unexpected error", "disableCloseRegion");
                        c.b;
                        new StringBuilder("SDK encountered unexpected error in handling disableCloseRegion() request from creative; ").append(e.getMessage());
                    }
                }
            });
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r4.c.getListener().b(new java.util.HashMap<>());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0091, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0092, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0093, code lost:
        r4.c.b(r5, "Unexpected error", "onUserInteraction");
        new java.lang.StringBuilder("SDK encountered unexpected error in handling onUserInteraction() signal from creative; ").append(r6.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00aa, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0083 */
    @JavascriptInterface
    public void onUserInteraction(String str, String str2) {
        if (this.c != null && !this.c.e()) {
            this.c.c("onUserInteraction");
        } else if (str2 == null) {
            try {
                this.c.getListener().b(new HashMap<>());
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "onUserInteraction");
                new StringBuilder("SDK encountered unexpected error in handling onUserInteraction() signal from creative; ").append(e2.getMessage());
            }
        } else {
            JSONObject jSONObject = new JSONObject(str2);
            HashMap hashMap = new HashMap();
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                hashMap.put(str3, jSONObject.get(str3));
            }
            try {
                this.c.getListener().b(hashMap);
            } catch (Exception e3) {
                this.c.b(str, "Unexpected error", "onUserInteraction");
                new StringBuilder("SDK encountered unexpected error in handling onUserInteraction() signal from creative; ").append(e3.getMessage());
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:19|20|21) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.c.getListener().a(new java.util.HashMap<>());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007d, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007e, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007f, code lost:
        r4.c.b(r5, "Unexpected error", "incentCompleted");
        new java.lang.StringBuilder("SDK encountered unexpected error in handling onUserInteraction() signal from creative; ").append(r6.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0096, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006f */
    @JavascriptInterface
    public void incentCompleted(String str, String str2) {
        if (str2 == null) {
            try {
                this.c.getListener().a(new HashMap<>());
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "incentCompleted");
                new StringBuilder("SDK encountered unexpected error in handling onUserInteraction() signal from creative; ").append(e2.getMessage());
            }
        } else {
            JSONObject jSONObject = new JSONObject(str2);
            HashMap hashMap = new HashMap();
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                hashMap.put(str3, jSONObject.get(str3));
            }
            try {
                this.c.getListener().a(hashMap);
            } catch (Exception e3) {
                this.c.b(str, "Unexpected error", "incentCompleted");
                new StringBuilder("SDK encountered unexpected error in handling onUserInteraction() signal from creative; ").append(e3.getMessage());
            }
        }
    }

    @JavascriptInterface
    public String getOrientation(String str) {
        int b2 = com.inmobi.commons.core.utilities.b.c.b();
        if (b2 == 1) {
            return "0";
        }
        if (b2 == 3) {
            return "90";
        }
        if (b2 == 2) {
            return "180";
        }
        return b2 == 4 ? "270" : "-1";
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:9|10|11|12|13|14|15) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003e */
    @JavascriptInterface
    public void saveContent(String str, String str2, String str3) {
        if (str2 == null || str2.length() == 0 || str3 == null || str3.length() == 0) {
            JSONObject jSONObject = new JSONObject();
            String str4 = "url";
            if (str3 == null) {
                str3 = "";
            }
            try {
                jSONObject.put(str4, str3);
                jSONObject.put(IronSourceConstants.EVENTS_ERROR_REASON, 1);
            } catch (JSONException unused) {
            }
            String replace = jSONObject.toString().replace("\"", "\\\"");
            StringBuilder sb = new StringBuilder("sendSaveContentResult(\"saveContent_");
            if (str2 == null) {
                str2 = "";
            }
            sb.append(str2);
            sb.append("\", 'failed', \"");
            sb.append(replace);
            sb.append("\");");
            this.c.a(str, sb.toString());
            return;
        }
        try {
            RenderView renderView = this.c;
            StringBuilder sb2 = new StringBuilder("saveContent called: content ID: ");
            sb2.append(str2);
            sb2.append("; URL: ");
            sb2.append(str3);
            if (!renderView.e("saveContent")) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("url", str3);
                jSONObject2.put(IronSourceConstants.EVENTS_ERROR_REASON, 5);
                String replace2 = jSONObject2.toString().replace("\"", "\\\"");
                StringBuilder sb3 = new StringBuilder("sendSaveContentResult(\"saveContent_");
                sb3.append(str2);
                sb3.append("\", 'failed', \"");
                sb3.append(replace2);
                sb3.append("\");");
                renderView.a(str, sb3.toString());
                return;
            }
            HashSet hashSet = new HashSet();
            hashSet.add(new bn(-1, str3));
            com.inmobi.ads.a.b bVar = new com.inmobi.ads.a.b(UUID.randomUUID().toString(), (Set<bn>) hashSet, renderView.v, str2);
            bVar.g = str;
            com.inmobi.ads.a.f a2 = com.inmobi.ads.a.f.a();
            a2.c.execute(new Runnable(bVar) {
                final /* synthetic */ b a;

                {
                    this.a = r2;
                }

                public final void run() {
                    f.this.b(this.a);
                    f.f;
                    StringBuilder sb = new StringBuilder("Attempting to cache ");
                    sb.append(this.a.b.size());
                    sb.append("remote URLs ");
                    for (bn bnVar : this.a.b) {
                        f.b(f.this, bnVar.b);
                    }
                }
            });
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "saveContent");
            new StringBuilder("SDK encountered unexpected error in handling saveContent() request from creative; ").append(e2.getMessage());
        }
    }

    @JavascriptInterface
    public void cancelSaveContent(String str, String str2) {
        try {
            RenderView.d();
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "cancelSaveContent");
            new StringBuilder("SDK encountered unexpected error in handling cancelSaveContent() request from creative; ").append(e2.getMessage());
        }
    }

    @JavascriptInterface
    public String isDeviceMuted(String str) {
        if (this.c == null) {
            return "false";
        }
        boolean z = false;
        try {
            this.c.getMediaProcessor();
            z = MraidMediaProcessor.a();
        } catch (Exception e2) {
            new StringBuilder("SDK encountered unexpected error in checking if device is muted; ").append(e2.getMessage());
        }
        return String.valueOf(z);
    }

    @JavascriptInterface
    public String isHeadphonePlugged(String str) {
        if (this.c == null) {
            return "false";
        }
        boolean z = false;
        try {
            this.c.getMediaProcessor();
            z = MraidMediaProcessor.d();
        } catch (Exception e2) {
            new StringBuilder("SDK encountered unexpected error in checking if headphones are plugged-in; ").append(e2.getMessage());
        }
        return String.valueOf(z);
    }

    @JavascriptInterface
    public void registerDeviceMuteEventListener(String str) {
        if (this.c != null) {
            try {
                MraidMediaProcessor mediaProcessor = this.c.getMediaProcessor();
                Context b2 = com.inmobi.commons.a.a.b();
                if (b2 != null && mediaProcessor.c == null) {
                    mediaProcessor.c = new RingerModeChangeReceiver(str);
                    b2.registerReceiver(mediaProcessor.c, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
                }
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "registerDeviceMuteEventListener");
                new StringBuilder("SDK encountered unexpected error in handling registerDeviceMuteEventListener() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void unregisterDeviceMuteEventListener(String str) {
        if (this.c != null) {
            try {
                this.c.getMediaProcessor().b();
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "unRegisterDeviceMuteEventListener");
                new StringBuilder("SDK encountered unexpected error in handling unregisterDeviceMuteEventListener() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void registerDeviceVolumeChangeEventListener(String str) {
        if (this.c != null) {
            try {
                MraidMediaProcessor mediaProcessor = this.c.getMediaProcessor();
                Context b2 = com.inmobi.commons.a.a.b();
                if (b2 != null && mediaProcessor.d == null) {
                    mediaProcessor.d = new com.inmobi.rendering.mraid.MraidMediaProcessor.a(str, b2, new Handler());
                    b2.getContentResolver().registerContentObserver(System.CONTENT_URI, true, mediaProcessor.d);
                }
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "registerDeviceVolumeChangeEventListener");
                new StringBuilder("SDK encountered unexpected error in handling registerDeviceVolumeChangeEventListener() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void unregisterDeviceVolumeChangeEventListener(String str) {
        if (this.c != null) {
            try {
                this.c.getMediaProcessor().c();
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "unregisterDeviceVolumeChangeEventListener");
                new StringBuilder("SDK encountered unexpected error in handling unregisterDeviceVolumeChangeEventListener() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public int getDeviceVolume(String str) {
        if (this.c == null) {
            return -1;
        }
        try {
            MraidMediaProcessor mediaProcessor = this.c.getMediaProcessor();
            Context b2 = com.inmobi.commons.a.a.b();
            if (b2 == null) {
                return -1;
            }
            if (!mediaProcessor.a.getRenderingConfig().m || !com.inmobi.commons.a.a.d()) {
                return ((AudioManager) b2.getSystemService(MimeTypes.BASE_TYPE_AUDIO)).getStreamVolume(3);
            }
            return 0;
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "getDeviceVolume");
            new StringBuilder("SDK encountered unexpected error in handling getDeviceVolume() request from creative; ").append(e2.getMessage());
            return -1;
        }
    }

    @JavascriptInterface
    public void registerHeadphonePluggedEventListener(String str) {
        if (this.c != null) {
            try {
                MraidMediaProcessor mediaProcessor = this.c.getMediaProcessor();
                Context b2 = com.inmobi.commons.a.a.b();
                if (b2 != null && mediaProcessor.e == null) {
                    mediaProcessor.e = new HeadphonesPluggedChangeReceiver(str);
                    b2.registerReceiver(mediaProcessor.e, new IntentFilter("android.intent.action.HEADSET_PLUG"));
                }
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "registerHeadphonePluggedEventListener");
                new StringBuilder("SDK encountered unexpected error in handling registerHeadphonePluggedEventListener() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void unregisterHeadphonePluggedEventListener(String str) {
        if (this.c != null) {
            try {
                this.c.getMediaProcessor().e();
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "unregisterHeadphonePluggedEventListener");
                new StringBuilder("SDK encountered unexpected error in handling unregisterHeadphonePluggedEventListener() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void disableBackButton(String str, boolean z) {
        if (this.c != null) {
            this.c.setDisableBackButton(z);
        }
    }

    @JavascriptInterface
    public boolean isBackButtonDisabled(String str) {
        if (this.c == null) {
            return false;
        }
        return this.c.p;
    }

    @JavascriptInterface
    public void registerBackButtonPressedEventListener(String str) {
        if (this.c != null) {
            try {
                this.c.q = str;
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "registerBackButtonPressedEventListener");
                new StringBuilder("SDK encountered unexpected error in handling registerBackButtonPressedEventListener() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void unregisterBackButtonPressedEventListener(String str) {
        if (this.c != null) {
            try {
                this.c.q = null;
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "unregisterBackButtonPressedEventListener");
                new StringBuilder("SDK encountered unexpected error in handling unregisterBackButtonPressedEventListener() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void startDownloader(String str, String str2, String str3, String str4) {
        if (this.c != null) {
            if (!this.c.e()) {
                this.c.c("startDownloader");
                return;
            }
            try {
                if (TextUtils.isEmpty(str2)) {
                    this.c.b(str, "Invalid URL", "startDownloader");
                    return;
                }
                RenderView renderView = this.c;
                AdContainer referenceContainer = renderView.getReferenceContainer();
                if (referenceContainer instanceof ah) {
                    ah ahVar = (ah) referenceContainer;
                    ak.a(str2, str3, str4);
                    ahVar.a(renderView);
                    return;
                }
                if (referenceContainer instanceof RenderView) {
                    ak.a(str2, str3, str4);
                }
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "startDownloader");
                new StringBuilder("SDK encountered unexpected error in handling startDownloader() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void registerDownloaderCallbacks(String str) {
        if (this.c != null) {
            try {
                RenderView renderView = this.c;
                AdContainer referenceContainer = renderView.getReferenceContainer();
                if (referenceContainer instanceof ah) {
                    ((ah) referenceContainer).a(renderView);
                }
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "registerDownloaderCallbacks");
                new StringBuilder("SDK encountered unexpected error in handling registerDownloaderCallbacks() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void unregisterDownloaderCallbacks(String str) {
        if (this.c != null) {
            try {
                RenderView renderView = this.c;
                AdContainer referenceContainer = renderView.getReferenceContainer();
                if (referenceContainer instanceof ah) {
                    ah ahVar = (ah) referenceContainer;
                    if (ahVar.z != null) {
                        ahVar.z.remove(renderView);
                    }
                }
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "unregisterDownloaderCallbacks");
                new StringBuilder("SDK encountered unexpected error in handling unregisterDownloaderCallbacks() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public int getDownloadProgress(String str) {
        if (this.c == null) {
            return -1;
        }
        try {
            return this.c.getDownloadProgress();
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "getDownloadProgress");
            new StringBuilder("SDK encountered unexpected error in handling getDownloadProgress() request from creative; ").append(e2.getMessage());
            return -1;
        }
    }

    @JavascriptInterface
    public int getDownloadStatus(String str) {
        if (this.c == null) {
            return -1;
        }
        try {
            return this.c.getDownloadStatus();
        } catch (Exception e2) {
            this.c.b(str, "Unexpected error", "getDownloadStatus");
            new StringBuilder("SDK encountered unexpected error in handling getDownloadStatus() request from creative; ").append(e2.getMessage());
            return -1;
        }
    }

    @JavascriptInterface
    public void setCloseEndCardTracker(String str, String str2) {
        if (this.c != null) {
            try {
                this.c.setCloseEndCardTracker(str2);
            } catch (Exception e2) {
                this.c.b(str, "Unexpected error", "getDownloadStatus");
                new StringBuilder("SDK encountered unexpected error in handling getDownloadStatus() request from creative; ").append(e2.getMessage());
            }
        }
    }

    @JavascriptInterface
    public void fireSkip(String str) {
        RenderView.f();
    }

    @JavascriptInterface
    public void fireComplete(String str) {
        if (this.c != null) {
            RenderView.g();
        }
    }

    @JavascriptInterface
    public void showEndCard(String str) {
        if (this.c != null) {
            AdContainer referenceContainer = this.c.getReferenceContainer();
            if (referenceContainer instanceof ah) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        ah.this.r = true;
                        ah.this.c((ak) null);
                    }
                });
            }
        }
    }

    @JavascriptInterface
    public void saveBlob(String str, String str2) {
        if (this.c != null) {
            RenderView renderView = this.c;
            if (renderView.t != null) {
                renderView.t.e(str2);
            }
        }
    }

    @JavascriptInterface
    public void getBlob(String str, String str2) {
        if (this.c != null) {
            RenderView renderView = this.c;
            if (renderView.t != null) {
                renderView.t.a(str, str2, renderView);
            }
        }
    }
}
