package com.inmobi.rendering;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions.Callback;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsoluteLayout.LayoutParams;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.chartboost.sdk.CBLocation;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.inmobi.a.n;
import com.inmobi.ads.AdContainer;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.NativeTracker;
import com.inmobi.ads.NativeVideoWrapper;
import com.inmobi.ads.ad;
import com.inmobi.ads.bd;
import com.inmobi.ads.be;
import com.inmobi.ads.br;
import com.inmobi.ads.bs;
import com.inmobi.ads.c;
import com.inmobi.ads.c.i;
import com.inmobi.ads.cb;
import com.inmobi.ads.cc;
import com.inmobi.ads.o;
import com.inmobi.ads.v;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b;
import com.inmobi.rendering.mraid.MraidMediaProcessor;
import com.inmobi.rendering.mraid.d;
import com.inmobi.rendering.mraid.e;
import com.inmobi.rendering.mraid.f;
import com.inmobi.rendering.mraid.g;
import com.inmobi.rendering.mraid.h;
import com.integralads.avid.library.inmobi.session.AbstractAvidAdSession;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.smaato.sdk.richmedia.mraid.bridge.MraidJsMethods;
import com.tapjoy.TJAdUnitConstants.String;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"SetJavaScriptEnabled", "ViewConstructor", "ClickableViewAccessibility"})
public final class RenderView extends WebView implements AdContainer, b {
    public static final a a = new a() {
        public final void E() {
        }

        public final void a(RenderView renderView) {
        }

        public final void a(HashMap<Object, Object> hashMap) {
        }

        public final void b(RenderView renderView) {
        }

        public final void b(String str, Map<String, Object> map) {
        }

        public final void b(HashMap<Object, Object> hashMap) {
        }

        public final void c(RenderView renderView) {
        }

        public final void d(RenderView renderView) {
        }

        public final void u() {
        }

        public final void w() {
        }

        public final void y() {
        }

        public final void z() {
        }
    };
    /* access modifiers changed from: private */
    public static final String w = RenderView.class.getSimpleName();
    private WeakReference<ViewGroup> A;
    private h B;
    private c C;
    private c D;
    /* access modifiers changed from: private */
    public List<String> E = new ArrayList();
    private boolean F;
    private com.inmobi.rendering.mraid.a G;
    private g H;
    private f I;
    private JSONObject J;
    private JSONObject K;
    private boolean L = true;
    /* access modifiers changed from: private */
    public boolean M = false;
    private final Object N = new Object();
    private final Object O = new Object();
    private boolean P = true;
    /* access modifiers changed from: private */
    public View Q;
    /* access modifiers changed from: private */
    public CustomViewCallback R;
    private int S = -1;
    private boolean T = false;
    private long U = Long.MIN_VALUE;
    private String V;
    private String W;
    private AdContainer aa;
    private o ab;
    private boolean ac = false;
    private boolean ad;
    @Nullable
    private Set<br> ae;
    private cb af;
    private final com.inmobi.ads.AdContainer.a ag = new com.inmobi.ads.AdContainer.a() {
        public final void a() {
            RenderView.w;
            if (RenderView.this.c != null) {
                RenderView.this.c.y();
            }
        }

        public final void a(Object obj) {
            RenderView.w;
            if (PlacementType.PLACEMENT_TYPE_INLINE == RenderView.this.e.a) {
                if (RenderView.this.x != null) {
                    RenderView.this.x.setAndUpdateViewState("Expanded");
                } else {
                    RenderView.this.setAndUpdateViewState("Expanded");
                }
                RenderView.this.s = false;
            }
            if (RenderView.this.c != null) {
                RenderView.this.c.c(RenderView.this);
            }
        }

        public final void b(Object obj) {
            RenderView.w;
            if (PlacementType.PLACEMENT_TYPE_INLINE == RenderView.this.e.a) {
                RenderView.this.setAndUpdateViewState(CBLocation.LOCATION_DEFAULT);
                if (RenderView.this.x != null) {
                    RenderView.this.x.setAndUpdateViewState(CBLocation.LOCATION_DEFAULT);
                }
            } else if (CBLocation.LOCATION_DEFAULT.equals(RenderView.this.d)) {
                RenderView.this.setAndUpdateViewState("Hidden");
            }
            if (RenderView.this.c != null) {
                RenderView.this.c.d(RenderView.this);
            }
        }
    };
    private final WebViewClient ah = new WebViewClient() {
        public final boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            RenderView.w;
            if (VERSION.SDK_INT < 21) {
                return false;
            }
            String uri = webResourceRequest.getUrl().toString();
            if (RenderView.this.i) {
                webView.loadUrl(uri);
                return true;
            } else if (RenderView.this.e() || RenderView.this.z || "about:blank".equals(uri)) {
                RenderView.w;
                StringBuilder sb = new StringBuilder("Placement type: ");
                sb.append(RenderView.this.e.a);
                sb.append(" url:");
                sb.append(uri);
                if (PlacementType.PLACEMENT_TYPE_FULLSCREEN != RenderView.this.e.a) {
                    RenderView.w;
                    if (b.a(RenderView.this.getContainerContext(), uri, null) != null) {
                        RenderView.this.getListener().z();
                    }
                    return true;
                } else if (!RenderView.this.z || !b.a(uri)) {
                    RenderView.w;
                    if (b.a(RenderView.this.getContainerContext(), uri, null) != null) {
                        RenderView.this.getListener().z();
                    }
                    return true;
                } else {
                    RenderView.w;
                    return false;
                }
            } else {
                RenderView.this.c("redirect");
                return true;
            }
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            RenderView.w;
            if (RenderView.this.i) {
                webView.loadUrl(str);
                return true;
            } else if (RenderView.this.e() || RenderView.this.z || "about:blank".equals(str)) {
                RenderView.w;
                StringBuilder sb = new StringBuilder("Placement type: ");
                sb.append(RenderView.this.e.a);
                sb.append(" url:");
                sb.append(str);
                if (PlacementType.PLACEMENT_TYPE_FULLSCREEN != RenderView.this.e.a) {
                    RenderView.w;
                    if (b.a(RenderView.this.getContainerContext(), str, null) != null) {
                        RenderView.this.getListener().z();
                    }
                    return true;
                } else if (!RenderView.this.z || !b.a(str)) {
                    RenderView.w;
                    if (b.a(RenderView.this.getContainerContext(), str, null) != null) {
                        RenderView.this.getListener().z();
                    }
                    return true;
                } else {
                    RenderView.w;
                    return false;
                }
            } else {
                RenderView.this.c("redirect");
                return true;
            }
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            RenderView.w;
            RenderView.this.M = false;
            RenderView.this.setAndUpdateViewState("Loading");
        }

        public final void onPageFinished(WebView webView, String str) {
            RenderView.w;
            if (RenderView.this.E.contains(str) && !RenderView.this.M) {
                RenderView.this.M = true;
                RenderView.w;
                RenderView.this.d(RenderView.this.getMraidJsString());
            }
            if ("Loading".equals(RenderView.this.d)) {
                RenderView.this.c.a(RenderView.this);
                RenderView.k(RenderView.this);
                if (RenderView.this.x != null) {
                    RenderView.this.setAndUpdateViewState("Expanded");
                    return;
                }
                RenderView.this.setAndUpdateViewState(CBLocation.LOCATION_DEFAULT);
            }
        }

        public final void onLoadResource(WebView webView, String str) {
            RenderView.w;
            String url = RenderView.this.getUrl();
            if (str != null && url != null && str.contains("/mraid.js") && !url.equals("about:blank") && !url.startsWith("file:")) {
                if (!RenderView.this.E.contains(url)) {
                    RenderView.this.E.add(url);
                }
                if (!RenderView.this.M) {
                    RenderView.this.M = true;
                    RenderView.w;
                    RenderView.this.d(RenderView.this.getMraidJsString());
                }
            }
        }

        @TargetApi(22)
        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            RenderView.w;
            StringBuilder sb = new StringBuilder("Loading error. Error code:");
            sb.append(i);
            sb.append(" Error msg:");
            sb.append(str);
            sb.append(" Failing url:");
            sb.append(str2);
        }

        @TargetApi(23)
        public final void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            RenderView.w;
            StringBuilder sb = new StringBuilder("Loading error. Error code:");
            sb.append(webResourceError.getErrorCode());
            sb.append(" Error msg:");
            sb.append(webResourceError.getDescription());
            sb.append(" Failing url:");
            sb.append(webResourceRequest.getUrl());
        }
    };
    private final WebChromeClient ai = new WebChromeClient() {
        public final boolean onJsAlert(WebView webView, String str, String str2, final JsResult jsResult) {
            RenderView.w;
            StringBuilder sb = new StringBuilder("jsAlert called with: ");
            sb.append(str2);
            sb.append(str);
            if (RenderView.a(RenderView.this, jsResult)) {
                Activity fullScreenActivity = RenderView.this.getFullScreenActivity();
                if (fullScreenActivity != null) {
                    new Builder(fullScreenActivity).setMessage(str2).setTitle(str).setPositiveButton(17039370, new OnClickListener() {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            jsResult.confirm();
                        }
                    }).setCancelable(false).create().show();
                } else {
                    jsResult.cancel();
                }
            }
            return true;
        }

        public final boolean onJsConfirm(WebView webView, String str, String str2, final JsResult jsResult) {
            RenderView.w;
            StringBuilder sb = new StringBuilder("jsConfirm called with: ");
            sb.append(str2);
            sb.append(str);
            if (RenderView.a(RenderView.this, jsResult)) {
                Activity fullScreenActivity = RenderView.this.getFullScreenActivity();
                if (fullScreenActivity != null) {
                    new Builder(fullScreenActivity).setMessage(str2).setPositiveButton(17039370, new OnClickListener() {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            jsResult.confirm();
                        }
                    }).setNegativeButton(17039360, new OnClickListener() {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            jsResult.cancel();
                        }
                    }).create().show();
                } else {
                    jsResult.cancel();
                }
            }
            return true;
        }

        public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
            RenderView.w;
            StringBuilder sb = new StringBuilder("jsPrompt called with: ");
            sb.append(str2);
            sb.append(str);
            if (!RenderView.a(RenderView.this, (JsResult) jsPromptResult)) {
                return true;
            }
            if (RenderView.this.getFullScreenActivity() != null) {
                return false;
            }
            jsPromptResult.cancel();
            return true;
        }

        public final void onShowCustomView(View view, CustomViewCallback customViewCallback) {
            if (RenderView.this.b != null && RenderView.this.b.get() != null) {
                RenderView.this.Q = view;
                RenderView.this.R = customViewCallback;
                RenderView.this.Q.setOnTouchListener(new OnTouchListener() {
                    public final boolean onTouch(View view, MotionEvent motionEvent) {
                        return true;
                    }
                });
                FrameLayout frameLayout = (FrameLayout) ((Activity) RenderView.this.b.get()).findViewById(16908290);
                RenderView.this.Q.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                frameLayout.addView(RenderView.this.Q, new LayoutParams(-1, -1, 0, 0));
                RenderView.this.Q.requestFocus();
                View m = RenderView.this.Q;
                m.setOnKeyListener(new OnKeyListener() {
                    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                        if (4 != keyEvent.getKeyCode() || keyEvent.getAction() != 0) {
                            return false;
                        }
                        RenderView.w;
                        AnonymousClass5.this.a();
                        return true;
                    }
                });
                m.setFocusable(true);
                m.setFocusableInTouchMode(true);
                m.requestFocus();
            }
        }

        public final void onHideCustomView() {
            a();
            super.onHideCustomView();
        }

        /* access modifiers changed from: private */
        public void a() {
            if (RenderView.this.Q != null) {
                if (RenderView.this.R != null) {
                    RenderView.this.R.onCustomViewHidden();
                    RenderView.this.R = null;
                }
                if (!(RenderView.this.Q == null || RenderView.this.Q.getParent() == null)) {
                    ((ViewGroup) RenderView.this.Q.getParent()).removeView(RenderView.this.Q);
                    RenderView.this.Q = null;
                }
            }
        }

        public final void onGeolocationPermissionsShowPrompt(final String str, final Callback callback) {
            if (!(RenderView.this.b == null || RenderView.this.b.get() == null)) {
                new Builder((Context) RenderView.this.b.get()).setTitle("Location Permission").setMessage("Allow location access").setPositiveButton(17039370, new OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        callback.invoke(str, true, false);
                    }
                }).setNegativeButton(17039360, new OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        callback.invoke(str, false, false);
                    }
                }).create().show();
            }
            super.onGeolocationPermissionsShowPrompt(str, callback);
        }

        public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            StringBuilder sb = new StringBuilder();
            sb.append(consoleMessage.message());
            sb.append(" -- From line ");
            sb.append(consoleMessage.lineNumber());
            sb.append(" of ");
            sb.append(consoleMessage.sourceId());
            RenderView.w;
            return true;
        }
    };
    /* access modifiers changed from: 0000 */
    public WeakReference<Activity> b;
    /* access modifiers changed from: 0000 */
    public a c;
    /* access modifiers changed from: 0000 */
    public String d = CBLocation.LOCATION_DEFAULT;
    /* access modifiers changed from: 0000 */
    public RenderingProperties e;
    com.inmobi.rendering.mraid.b f;
    e g;
    MraidMediaProcessor h;
    public boolean i;
    boolean j = true;
    boolean k = true;
    public boolean l = false;
    boolean m = false;
    boolean n = false;
    boolean o = false;
    boolean p = false;
    String q = null;
    public AtomicBoolean r = new AtomicBoolean(false);
    /* access modifiers changed from: 0000 */
    public boolean s;
    a t;
    public boolean u;
    final com.inmobi.ads.a.g v = new com.inmobi.ads.a.g() {
        public final void a(com.inmobi.ads.a.b bVar) {
            if (bVar.g != null && bVar.a.size() > 0) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("url", ((com.inmobi.ads.a.a) bVar.a.get(0)).d);
                    jSONObject.put(IronSourceConstants.EVENTS_ERROR_REASON, ((com.inmobi.ads.a.a) bVar.a.get(0)).l);
                } catch (JSONException unused) {
                }
                String replace = jSONObject.toString().replace("\"", "\\\"");
                StringBuilder sb = new StringBuilder("sendSaveContentResult(\"saveContent_");
                sb.append(bVar.h);
                sb.append("\", 'failed', \"");
                sb.append(replace);
                sb.append("\");");
                String sb2 = sb.toString();
                RenderView.w;
                RenderView.this.a(bVar.g, sb2);
            }
        }

        public final void b(com.inmobi.ads.a.b bVar) {
            if (bVar.g != null && bVar.a.size() > 0) {
                StringBuilder sb = new StringBuilder("sendSaveContentResult(\"saveContent_");
                sb.append(bVar.h);
                sb.append("\", 'success', \"");
                sb.append(((com.inmobi.ads.a.a) bVar.a.get(0)).k);
                sb.append("\");");
                String sb2 = sb.toString();
                RenderView.w;
                RenderView.this.a(bVar.g, sb2);
            }
        }
    };
    /* access modifiers changed from: private */
    public RenderView x;
    @Nullable
    private WeakReference<Activity> y;
    /* access modifiers changed from: private */
    public boolean z = false;

    public interface a {
        void E();

        void a(RenderView renderView);

        void a(HashMap<Object, Object> hashMap);

        void b(RenderView renderView);

        void b(String str, Map<String, Object> map);

        void b(HashMap<Object, Object> hashMap);

        void c(RenderView renderView);

        void d(RenderView renderView);

        void u();

        void w();

        void y();

        void z();
    }

    public static void d() {
    }

    static void f() {
    }

    static void g() {
    }

    public final Object getDataModel() {
        return null;
    }

    public final String getMarkupType() {
        return String.HTML;
    }

    @Nullable
    public final View getVideoContainerView() {
        return null;
    }

    public RenderView(Context context, RenderingProperties renderingProperties, @Nullable Set<br> set, @Nullable String str) {
        super(context.getApplicationContext());
        if (context instanceof Activity) {
            this.y = new WeakReference<>((Activity) context);
        }
        this.x = null;
        this.e = renderingProperties;
        this.s = false;
        this.ae = set;
        this.W = str;
        setReferenceContainer(this);
        this.aa = this;
        this.ab = new o();
        this.ad = false;
    }

    public final void setIsPreload(boolean z2) {
        this.u = z2;
    }

    public final void setPlacementId(long j2) {
        this.U = j2;
    }

    public final void setImpressionId(String str) {
        this.W = str;
    }

    public final void setCreativeId(String str) {
        this.V = str;
    }

    public final void setAllowAutoRedirection(boolean z2) {
        this.ac = z2;
    }

    public final void setBlobProvider(a aVar) {
        this.t = aVar;
    }

    public final String getImpressionId() {
        return this.W;
    }

    public final String getCreativeId() {
        return this.V;
    }

    public final long getPlacementId() {
        return this.U;
    }

    public final boolean getAllowAutoRedirection() {
        return this.ac;
    }

    public final void setOriginalRenderView(RenderView renderView) {
        this.x = renderView;
    }

    public final RenderView getOriginalRenderView() {
        return this.x;
    }

    public final com.inmobi.ads.AdContainer.a getFullScreenEventsListener() {
        return this.ag;
    }

    public final RenderingProperties getRenderingProperties() {
        return this.e;
    }

    public final String getState() {
        return this.d;
    }

    public final Object getDefaultPositionMonitor() {
        return this.N;
    }

    public final Object getCurrentPositionMonitor() {
        return this.O;
    }

    public final void setDefaultPositionLock() {
        this.j = true;
    }

    public final void setCurrentPositionLock() {
        this.k = true;
    }

    @NonNull
    public final Context getContainerContext() {
        if (this.b == null || this.b.get() == null) {
            return getContext();
        }
        return (Context) this.b.get();
    }

    public final void setDefaultPosition() {
        int[] iArr = new int[2];
        this.J = new JSONObject();
        if (this.A == null) {
            this.A = new WeakReference<>((ViewGroup) getParent());
        }
        if (this.A.get() != null) {
            ((ViewGroup) this.A.get()).getLocationOnScreen(iArr);
            try {
                this.J.put(AvidJSONUtil.KEY_X, com.inmobi.commons.core.utilities.b.c.b(iArr[0]));
                this.J.put(AvidJSONUtil.KEY_Y, com.inmobi.commons.core.utilities.b.c.b(iArr[1]));
                int b2 = com.inmobi.commons.core.utilities.b.c.b(((ViewGroup) this.A.get()).getWidth());
                int b3 = com.inmobi.commons.core.utilities.b.c.b(((ViewGroup) this.A.get()).getHeight());
                this.J.put("width", b2);
                this.J.put("height", b3);
            } catch (JSONException unused) {
            }
        } else {
            this.J.put(AvidJSONUtil.KEY_X, 0);
            this.J.put(AvidJSONUtil.KEY_Y, 0);
            this.J.put("width", 0);
            this.J.put("height", 0);
        }
        synchronized (this.N) {
            this.j = false;
            this.N.notifyAll();
        }
    }

    public final String getDefaultPosition() {
        return this.J == null ? "" : this.J.toString();
    }

    public final void setCurrentPosition() {
        this.K = new JSONObject();
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        try {
            this.K.put(AvidJSONUtil.KEY_X, com.inmobi.commons.core.utilities.b.c.b(iArr[0]));
            this.K.put(AvidJSONUtil.KEY_Y, com.inmobi.commons.core.utilities.b.c.b(iArr[1]));
            int b2 = com.inmobi.commons.core.utilities.b.c.b(getWidth());
            int b3 = com.inmobi.commons.core.utilities.b.c.b(getHeight());
            this.K.put("width", b2);
            this.K.put("height", b3);
        } catch (JSONException unused) {
        }
        synchronized (this.O) {
            this.k = false;
            this.O.notifyAll();
        }
    }

    public final String getCurrentPosition() {
        return this.K == null ? "" : this.K.toString();
    }

    public final void setFullScreenActivityContext(Activity activity) {
        this.b = new WeakReference<>(activity);
        if (this.I != null) {
            setOrientationProperties(this.I);
        }
    }

    public final Activity getFullScreenActivity() {
        if (this.b == null) {
            return null;
        }
        return (Activity) this.b.get();
    }

    public final Activity getPubActivity() {
        if (this.y == null) {
            return null;
        }
        return (Activity) this.y.get();
    }

    public final i getRenderingConfig() {
        return this.D.i;
    }

    public final c.g getMraidConfig() {
        return this.D.j;
    }

    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        StringBuilder sb = new StringBuilder("onSizeChanged (");
        sb.append(i2);
        sb.append(", ");
        sb.append(i3);
        sb.append(")");
        if (i2 != 0 && i3 != 0) {
            int b2 = com.inmobi.commons.core.utilities.b.c.b(i2);
            int b3 = com.inmobi.commons.core.utilities.b.c.b(i3);
            StringBuilder sb2 = new StringBuilder("window.mraidview.broadcastEvent('sizeChange',");
            sb2.append(b2);
            sb2.append(",");
            sb2.append(b3);
            sb2.append(");");
            d(sb2.toString());
        }
    }

    public final void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        boolean z2 = i2 == 0;
        if (this.n != z2) {
            d(z2);
        }
    }

    public final void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        this.T = !z2;
        c(z2);
    }

    public final void onScreenStateChanged(int i2) {
        super.onScreenStateChanged(i2);
        if (i2 == 0) {
            c(false);
            return;
        }
        if (!this.T) {
            c(true);
        }
    }

    private void c(boolean z2) {
        if (this.n != z2) {
            if (VERSION.SDK_INT <= 23 || getFullScreenActivity() == null || !getFullScreenActivity().isInMultiWindowMode()) {
                d(z2);
            }
        }
    }

    private void d(boolean z2) {
        if (!this.s) {
            this.n = z2;
            if (!z2) {
                this.B.a(getContainerContext());
            } else {
                this.c.b(this);
            }
            boolean z3 = this.n;
            StringBuilder sb = new StringBuilder("window.mraidview.broadcastEvent('viewableChange',");
            sb.append(z3);
            sb.append(");");
            d(sb.toString());
        }
    }

    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.F = isHardwareAccelerated();
        if (this.A == null) {
            this.A = new WeakReference<>((ViewGroup) getParent());
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        new StringBuilder("Touch event received, action:").append(motionEvent.getAction());
        this.ad = true;
        if (e()) {
            d("window.mraidview.onUserInteraction();");
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public final void onDetachedFromWindow() {
        this.E.clear();
        getMediaProcessor().b();
        getMediaProcessor().c();
        getMediaProcessor().e();
        this.B.a(getContainerContext());
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e2) {
            StringBuilder sb = new StringBuilder("Detaching WebView from window encountered an error (");
            sb.append(e2.getMessage());
            sb.append(")");
            try {
                HashMap hashMap = new HashMap();
                hashMap.put("type", "IllegalArgumentException");
                StringBuilder sb2 = new StringBuilder();
                sb2.append(e2.getMessage());
                hashMap.put("message", sb2.toString());
                com.inmobi.commons.core.e.b.a();
                com.inmobi.commons.core.e.b.a("ads", "ExceptionCaught", hashMap);
            } catch (Exception e3) {
                StringBuilder sb3 = new StringBuilder("Error in submitting telemetey event : (");
                sb3.append(e3.getMessage());
                sb3.append(")");
            }
        }
    }

    @SuppressLint({"AddJavascriptInterface"})
    @TargetApi(19)
    public final void a(a aVar, @NonNull c cVar) {
        this.D = cVar;
        this.c = aVar;
        this.A = new WeakReference<>((ViewGroup) getParent());
        if ("row".contains("staging") && VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        if (getRenderingConfig() != null) {
            setBackgroundColor(getRenderingConfig().f);
        }
        if (getMraidConfig() != null) {
            if ((System.currentTimeMillis() / 1000) - new com.inmobi.rendering.mraid.c().a.b("last_updated_ts", 0) > getMraidConfig().a) {
                d dVar = new d(getMraidConfig().d, getMraidConfig().b, getMraidConfig().c);
                if (dVar.a != null) {
                    dVar.b = new com.inmobi.commons.core.network.c(HttpRequest.METHOD_GET, dVar.a);
                    dVar.b.z = false;
                    HashMap hashMap = new HashMap();
                    hashMap.put(HttpRequest.HEADER_ACCEPT_ENCODING, HttpRequest.ENCODING_GZIP);
                    dVar.b.a((Map<String, String>) hashMap);
                    new Thread(new Runnable() {
                        public final void run() {
                            byte[] bArr;
                            int i = 0;
                            while (i <= d.this.d) {
                                d.c;
                                long elapsedRealtime = SystemClock.elapsedRealtime();
                                com.inmobi.commons.core.network.d a2 = new com.inmobi.commons.core.network.e(d.this.b).a();
                                try {
                                    n.a().a(d.this.b.g());
                                    n.a().b(a2.c());
                                    n.a().c(SystemClock.elapsedRealtime() - elapsedRealtime);
                                } catch (Exception e) {
                                    d.c;
                                    new StringBuilder("Error in setting request-response data size. ").append(e.getMessage());
                                }
                                if (a2.a()) {
                                    d.c;
                                    i++;
                                    if (i > d.this.d) {
                                        break;
                                    }
                                    try {
                                        Thread.sleep((long) (d.this.e * 1000));
                                    } catch (InterruptedException unused) {
                                        d.c;
                                    }
                                } else {
                                    c cVar = new c();
                                    List list = (List) a2.d.get(HttpRequest.HEADER_CONTENT_ENCODING);
                                    if (list == null || !((String) list.get(0)).equals(HttpRequest.ENCODING_GZIP)) {
                                        cVar.a(a2.b());
                                        d.c;
                                        try {
                                            HashMap hashMap = new HashMap();
                                            hashMap.put("url", d.this.a);
                                            hashMap.put("latency", Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
                                            hashMap.put("payloadSize", Long.valueOf(d.this.b.g() + a2.c()));
                                            com.inmobi.commons.core.e.b.a();
                                            com.inmobi.commons.core.e.b.a("ads", "MraidFetchLatency", hashMap);
                                            return;
                                        } catch (Exception e2) {
                                            d.c;
                                            StringBuilder sb = new StringBuilder("Error in submitting telemetry event : (");
                                            sb.append(e2.getMessage());
                                            sb.append(")");
                                            return;
                                        }
                                    } else {
                                        d.c;
                                        if (a2.a == null || a2.a.length == 0) {
                                            bArr = new byte[0];
                                        } else {
                                            bArr = new byte[a2.a.length];
                                            System.arraycopy(a2.a, 0, bArr, 0, a2.a.length);
                                        }
                                        byte[] a3 = com.inmobi.commons.core.utilities.d.a(bArr);
                                        if (a3 != null) {
                                            try {
                                                cVar.a(new String(a3, "UTF-8"));
                                                d.c;
                                                try {
                                                    HashMap hashMap2 = new HashMap();
                                                    hashMap2.put("url", d.this.a);
                                                    hashMap2.put("latency", Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
                                                    hashMap2.put("payloadSize", Long.valueOf(d.this.b.g() + a2.c()));
                                                    com.inmobi.commons.core.e.b.a();
                                                    com.inmobi.commons.core.e.b.a("ads", "MraidFetchLatency", hashMap2);
                                                    return;
                                                } catch (Exception e3) {
                                                    d.c;
                                                    StringBuilder sb2 = new StringBuilder("Error in submitting telemetry event : (");
                                                    sb2.append(e3.getMessage());
                                                    sb2.append(")");
                                                    return;
                                                }
                                            } catch (UnsupportedEncodingException e4) {
                                                d.c;
                                                d.c;
                                                e4.getMessage();
                                            }
                                        }
                                        return;
                                    }
                                }
                            }
                        }
                    }).start();
                }
            }
        }
        if (VERSION.SDK_INT >= 16) {
            setImportantForAccessibility(2);
        }
        setScrollable(false);
        if (VERSION.SDK_INT >= 17) {
            getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        getSettings().setJavaScriptEnabled(true);
        getSettings().setGeolocationEnabled(true);
        setWebViewClient(this.ah);
        setWebChromeClient(this.ai);
        this.C = new c(this, this.e);
        addJavascriptInterface(this.C, "sdkController");
        this.f = new com.inmobi.rendering.mraid.b(this);
        this.g = new e(this);
        this.h = new MraidMediaProcessor(this);
        this.B = new h(this);
        this.G = new com.inmobi.rendering.mraid.a();
        this.H = new g();
        this.I = new f();
    }

    public final void setScrollable(boolean z2) {
        setScrollContainer(z2);
        setVerticalScrollBarEnabled(z2);
        setHorizontalScrollBarEnabled(z2);
    }

    /* access modifiers changed from: 0000 */
    public final void setIsInAppBrowser(boolean z2) {
        this.z = z2;
    }

    public final boolean c() {
        return this.r.get();
    }

    @TargetApi(11)
    public final void destroy() {
        if (!this.r.get()) {
            if (!this.L) {
                this.L = true;
                return;
            }
            this.r.set(true);
            this.s = true;
            this.S = -1;
            removeJavascriptInterface("sdkController");
            if (this.b != null) {
                this.b.clear();
            }
            if (this.A != null) {
                this.A.clear();
            }
            if (this.af != null) {
                this.af.d();
                this.af.e();
            }
            this.aa = null;
            ViewParent parent = getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(this);
                removeAllViews();
            }
            super.destroy();
        }
    }

    public final void a(int i2, Map<String, String> map) {
        switch (i2) {
            case 1:
                return;
            case 2:
                d("inmobi.recordEvent(120,null);");
                return;
            case 3:
                return;
            default:
                return;
        }
    }

    public final void setRequestedScreenOrientation() {
        if (getFullScreenActivity() != null && this.I != null) {
            setOrientationProperties(this.I);
        }
    }

    public final void setReferenceContainer(AdContainer adContainer) {
        this.aa = adContainer;
    }

    public final AdContainer getReferenceContainer() {
        return this.aa;
    }

    @NonNull
    public final c getAdConfig() {
        return this.D;
    }

    public final o getApkDownloader() {
        return this.ab;
    }

    @SuppressLint({"SwitchIntDef"})
    @NonNull
    public final cb getViewableAd() {
        Activity activity;
        if (this.af == null) {
            this.af = new cc(this);
            if (getFullScreenActivity() == null) {
                activity = getPubActivity();
            } else {
                activity = getFullScreenActivity();
            }
            if (this.ae != null) {
                if (activity != null) {
                    try {
                        for (br brVar : this.ae) {
                            int i2 = brVar.a;
                            if (i2 == 1) {
                                this.af = new ad(this, activity, this.af, brVar.b);
                            } else if (i2 == 3) {
                                AbstractAvidAdSession abstractAvidAdSession = (AbstractAvidAdSession) brVar.b.get("avidAdSession");
                                boolean z2 = brVar.b.containsKey("deferred") && ((Boolean) brVar.b.get("deferred")).booleanValue();
                                if (abstractAvidAdSession != null) {
                                    v vVar = new v(this, activity, this.af, abstractAvidAdSession, z2);
                                    this.af = vVar;
                                }
                            }
                        }
                    } catch (Exception e2) {
                        new StringBuilder("Exception occurred while creating the HTML viewable ad : ").append(e2.getMessage());
                    }
                } else {
                    HashMap hashMap = new HashMap();
                    hashMap.put("type", getMarkupType());
                    if (this.W != null) {
                        hashMap.put("impId", this.W);
                    }
                    com.inmobi.commons.core.e.b.a();
                    com.inmobi.commons.core.e.b.a("ads", "TrackersForService", hashMap);
                }
            }
        }
        return this.af;
    }

    public final void a(String str) {
        this.s = false;
        if (!this.r.get()) {
            loadDataWithBaseURL("", str, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        }
    }

    public final void b(String str) {
        this.s = false;
        if (!this.r.get()) {
            loadUrl(str);
        }
    }

    public final void stopLoading() {
        if (!this.r.get()) {
            super.stopLoading();
        }
    }

    public final void b(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder("broadcastEvent('error',\"");
        sb.append(str2);
        sb.append("\", \"");
        sb.append(str3);
        sb.append("\")");
        a(str, sb.toString());
    }

    public final void a(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".");
        sb.append(str2);
        d(sb.toString());
    }

    public final void a(String str, Map<String, Object> map) {
        this.c.b(str, map);
    }

    public final void d(final String str) {
        if (getContainerContext() != null) {
            new Handler(getContainerContext().getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        if (!RenderView.this.r.get()) {
                            StringBuilder sb = new StringBuilder("javascript:try{");
                            sb.append(str);
                            sb.append("}catch(e){}");
                            String sb2 = sb.toString();
                            RenderView.w;
                            if (VERSION.SDK_INT < 19) {
                                RenderView.this.loadUrl(sb2);
                                return;
                            }
                            RenderView.this.evaluateJavascript(sb2, null);
                        }
                    } catch (Exception e) {
                        RenderView.w;
                        new StringBuilder("SDK encountered an unexpected error injecting JavaScript in the Ad container; ").append(e.getMessage());
                    }
                }
            });
        }
    }

    public final void setUseCustomClose(boolean z2) {
        this.l = z2;
    }

    public final void setCloseRegionDisabled(boolean z2) {
        this.o = z2;
    }

    public final void setDisableBackButton(boolean z2) {
        this.p = z2;
    }

    public final void a(boolean z2) {
        setCloseRegionDisabled(z2);
        View rootView = getRootView();
        if (rootView != null) {
            CustomView customView = (CustomView) rootView.findViewById(65531);
            if (customView != null) {
                customView.setVisibility(this.o ? 8 : 0);
            }
        }
    }

    public final void b(boolean z2) {
        setUseCustomClose(z2);
        if (getRootView() != null) {
            CustomView customView = (CustomView) getRootView().findViewById(65532);
            if (customView != null) {
                customView.setVisibility(this.l ? 8 : 0);
            }
        }
    }

    public final void b() {
        MraidMediaProcessor mraidMediaProcessor = this.h;
        if (mraidMediaProcessor.b != null) {
            mraidMediaProcessor.b.a();
            mraidMediaProcessor.b = null;
        }
        if ("Expanded".equals(this.d)) {
            if (!CBLocation.LOCATION_DEFAULT.equals(this.d)) {
                this.s = true;
                com.inmobi.rendering.mraid.b bVar = this.f;
                if (bVar.a.getOriginalRenderView() == null) {
                    View findViewById = bVar.c.getRootView().findViewById(SupportMenu.USER_MASK);
                    ((ViewGroup) bVar.a.getParent()).removeView(bVar.a);
                    ((ViewGroup) findViewById.getParent()).removeView(findViewById);
                    bVar.c.addView(bVar.a, bVar.d, new RelativeLayout.LayoutParams(bVar.c.getWidth(), bVar.c.getHeight()));
                    bVar.a.j();
                }
                i();
                this.s = false;
            }
            this.L = false;
        } else if ("Resized".equals(this.d)) {
            if (!CBLocation.LOCATION_DEFAULT.equals(this.d)) {
                this.s = true;
                e eVar = this.g;
                ViewGroup viewGroup = (ViewGroup) eVar.a.getParent();
                View findViewById2 = viewGroup.getRootView().findViewById(65534);
                View findViewById3 = eVar.b.getRootView().findViewById(SupportMenu.USER_MASK);
                ((ViewGroup) findViewById2.getParent()).removeView(findViewById2);
                if (!(findViewById3 == null || findViewById3.getParent() == null)) {
                    ((ViewGroup) findViewById3.getParent()).removeView(findViewById3);
                }
                viewGroup.removeView(eVar.a);
                eVar.b.addView(eVar.a, eVar.c, new RelativeLayout.LayoutParams(eVar.b.getWidth(), eVar.b.getHeight()));
                eVar.a.j();
                setAndUpdateViewState(CBLocation.LOCATION_DEFAULT);
                this.c.d(this);
                this.s = false;
            }
        } else if (CBLocation.LOCATION_DEFAULT.equals(this.d)) {
            setAndUpdateViewState("Hidden");
            if (PlacementType.PLACEMENT_TYPE_FULLSCREEN == this.e.a) {
                i();
            } else {
                ((ViewGroup) getParent()).removeAllViews();
            }
        }
        this.E.clear();
        this.m = false;
    }

    private void i() {
        InMobiAdActivity.a((Object) this);
        Activity fullScreenActivity = getFullScreenActivity();
        if (fullScreenActivity != null) {
            ((InMobiAdActivity) fullScreenActivity).a = true;
            fullScreenActivity.finish();
            if (this.S != -1) {
                fullScreenActivity.overridePendingTransition(0, this.S);
            }
        } else {
            if (PlacementType.PLACEMENT_TYPE_INLINE == this.e.a) {
                setAndUpdateViewState(CBLocation.LOCATION_DEFAULT);
                if (this.x != null) {
                    this.x.setAndUpdateViewState(CBLocation.LOCATION_DEFAULT);
                }
            } else if (CBLocation.LOCATION_DEFAULT.equals(this.d)) {
                setAndUpdateViewState("Hidden");
            }
            if (this.c != null) {
                this.c.d(this);
            }
        }
    }

    public final void setExitAnimation(int i2) {
        this.S = i2;
    }

    private static String f(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException unused) {
            return str;
        }
    }

    public final void a(String str, String str2, String str3, @Nullable String str4) {
        if (str3 != null) {
            a(str, str2, str3, str4, false);
        } else if (str4 != null) {
            a(str, str2, str4, null, true);
        } else {
            String str5 = str2;
            b(str2, "Empty url and fallback url", "openExternal");
        }
    }

    private void a(String str, String str2, String str3, @Nullable String str4, boolean z2) {
        while (true) {
            try {
                b.b(getContainerContext(), str3);
                HashMap hashMap = new HashMap();
                hashMap.put(String.COMMAND, "openExternal");
                hashMap.put("scheme", bs.a(str2));
                this.c.b("CreativeInvokedAction", hashMap);
                getListener().z();
                StringBuilder sb = new StringBuilder("broadcastEvent('");
                sb.append(str);
                sb.append("Successful','");
                sb.append(str3);
                sb.append("');");
                a(str2, sb.toString());
                return;
            } catch (URISyntaxException e2) {
                if (z2) {
                    b("DeeplinkFallbackFailed", str3);
                } else {
                    b("DeeplinkFailed", str3);
                }
                new StringBuilder("Error message in processing openExternal: ").append(e2.getMessage());
                StringBuilder sb2 = new StringBuilder("Cannot resolve URI (");
                sb2.append(f(str3));
                sb2.append(")");
                b(str2, sb2.toString(), str);
                if (str4 == null) {
                    return;
                }
            } catch (ActivityNotFoundException e3) {
                if (z2) {
                    b("DeeplinkFallbackFailed", str3);
                } else {
                    b("DeeplinkFailed", str3);
                }
                new StringBuilder("Error message in processing openExternal: ").append(e3.getMessage());
                StringBuilder sb3 = new StringBuilder("Cannot resolve URI (");
                sb3.append(f(str3));
                sb3.append(")");
                b(str2, sb3.toString(), str);
                if (str4 == null) {
                    return;
                }
            } catch (Exception e4) {
                b(str2, "Unexpected error", "openExternal");
                Logger.a(InternalLogLevel.ERROR, "InMobi", "Could not open URL; SDK encountered an unexpected error");
                new StringBuilder("SDK encountered unexpected error in handling openExternal() request from creative; ").append(e4.getMessage());
                return;
            }
            str3 = str4;
            str4 = null;
            z2 = true;
        }
    }

    private void b(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("url", str2);
        a(str, (Map<String, Object>) hashMap);
    }

    public final void c(String str, String str2, String str3) {
        if (str3 == null || (str3.startsWith("http") && !URLUtil.isValidUrl(str3))) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" called with invalid url (");
            sb.append(str3);
            sb.append(")");
            b(str2, "Invalid URL", str);
        } else if (!str3.startsWith("http") || str3.contains("play.google.com") || str3.contains("market.android.com") || str3.contains("market%3A%2F%2F")) {
            a(str, str2, str3, null);
        } else {
            InMobiAdActivity.a(this);
            Intent intent = new Intent(getContainerContext(), InMobiAdActivity.class);
            intent.putExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE", 100);
            intent.putExtra("com.inmobi.rendering.InMobiAdActivity.IN_APP_BROWSER_URL", str3);
            com.inmobi.commons.a.a.a(getContainerContext(), intent);
            StringBuilder sb2 = new StringBuilder("broadcastEvent('");
            sb2.append(str);
            sb2.append("Successful','");
            sb2.append(str3);
            sb2.append("');");
            a(str2, sb2.toString());
            HashMap hashMap = new HashMap();
            hashMap.put(String.COMMAND, "openEmbedded");
            hashMap.put("scheme", bs.a(str2));
            this.c.b("CreativeInvokedAction", hashMap);
        }
    }

    public final void setRenderViewEventListener(a aVar) {
        this.c = aVar;
    }

    public final a getListener() {
        if (this.c != null) {
            return this.c;
        }
        a aVar = a;
        this.c = aVar;
        return aVar;
    }

    public final String getViewState() {
        return this.d;
    }

    public final MraidMediaProcessor getMediaProcessor() {
        return this.h;
    }

    public final com.inmobi.rendering.mraid.a getExpandProperties() {
        return this.G;
    }

    public final g getResizeProperties() {
        return this.H;
    }

    public final void setResizeProperties(g gVar) {
        this.H = gVar;
    }

    public final void setAndUpdateViewState(String str) {
        this.d = str;
        new StringBuilder("set state:").append(this.d);
        String lowerCase = this.d.toLowerCase(Locale.ENGLISH);
        StringBuilder sb = new StringBuilder("window.mraidview.broadcastEvent('stateChange','");
        sb.append(lowerCase);
        sb.append("');");
        d(sb.toString());
    }

    private void j() {
        setVisibility(0);
        requestLayout();
    }

    public final void setAdActiveFlag(boolean z2) {
        this.m = z2;
    }

    public final f getOrientationProperties() {
        return this.I;
    }

    public final void setOrientationProperties(f fVar) {
        this.I = fVar;
        if (!(this.b == null || this.b.get() == null || fVar.a)) {
            String str = fVar.b;
            char c2 = 65535;
            int hashCode = str.hashCode();
            boolean z2 = true;
            if (hashCode != 729267099) {
                if (hashCode == 1430647483 && str.equals("landscape")) {
                    c2 = 0;
                }
            } else if (str.equals("portrait")) {
                c2 = 1;
            }
            switch (c2) {
                case 0:
                    if (!(com.inmobi.commons.core.utilities.b.c.b() == 3 || com.inmobi.commons.core.utilities.b.c.b() == 4)) {
                        z2 = false;
                    }
                    if (z2) {
                        if (3 == com.inmobi.commons.core.utilities.b.c.b()) {
                            ((Activity) this.b.get()).setRequestedOrientation(0);
                            return;
                        } else {
                            ((Activity) this.b.get()).setRequestedOrientation(8);
                            return;
                        }
                    } else if (fVar.c.equals("left")) {
                        ((Activity) this.b.get()).setRequestedOrientation(8);
                        return;
                    } else if (fVar.c.equals("right")) {
                        ((Activity) this.b.get()).setRequestedOrientation(0);
                        return;
                    }
                    break;
                case 1:
                    if (com.inmobi.commons.core.utilities.b.c.b() == 2) {
                        ((Activity) this.b.get()).setRequestedOrientation(9);
                        return;
                    } else {
                        ((Activity) this.b.get()).setRequestedOrientation(1);
                        return;
                    }
                default:
                    if (com.inmobi.commons.core.utilities.b.c.b() != 2) {
                        if (com.inmobi.commons.core.utilities.b.c.b() != 4) {
                            if (com.inmobi.commons.core.utilities.b.c.b() != 3) {
                                ((Activity) this.b.get()).setRequestedOrientation(1);
                                break;
                            } else {
                                ((Activity) this.b.get()).setRequestedOrientation(0);
                                return;
                            }
                        } else {
                            ((Activity) this.b.get()).setRequestedOrientation(8);
                            return;
                        }
                    } else {
                        ((Activity) this.b.get()).setRequestedOrientation(9);
                        return;
                    }
            }
        }
    }

    public final String getMraidJsString() {
        String c2 = new com.inmobi.rendering.mraid.c().a.c("mraid_js_string");
        return c2 == null ? "var imIsObjValid=function(a){return\"undefined\"!=typeof a&&null!=a?!0:!1},EventListeners=function(a){this.event=a;this.count=0;var b=[];this.add=function(a){b.push(a);++this.count};this.remove=function(a){var e=!1,d=this;b=b.filter(function(b){if(b=b===a)--d.count,e=!0;return!b});return e};this.removeAll=function(){b=[];this.count=0};this.broadcast=function(a){b.forEach(function(e){try{e.apply({},a)}catch(d){}})};this.toString=function(){var c=[a,\":\"];b.forEach(function(a){c.push(\"|\",String(a),\"|\")});\nreturn c.join(\"\")}},InmobiObj=function(){this.listeners=[];this.addEventListener=function(a,b){try{if(imIsObjValid(b)&&imIsObjValid(a)){var c=this.listeners;c[a]||(c[a]=new EventListeners);c[a].add(b);\"micIntensityChange\"==a&&window.imraidview.startListeningMicIntensity();\"deviceMuted\"==a&&window.imraidview.startListeningDeviceMuteEvents();\"deviceVolumeChange\"==a&&window.imraidview.startListeningDeviceVolumeChange();\"volumeChange\"==a&&window.imraidview.startListeningVolumeChange();\"headphones\"==a&&\nwindow.imraidview.startListeningHeadphonePluggedEvents();\"backButtonPressed\"==a&&window.imraidview.startListeningForBackButtonPressedEvent();\"downloadStatusChanged\"==a&&window.imraidview.registerDownloaderCallbacks()}}catch(e){this.log(e)}};this.removeEventListener=function(a,b){if(imIsObjValid(a)){var c=this.listeners;imIsObjValid(c[a])&&(imIsObjValid(b)?c[a].remove(b):c[a].removeAll());\"micIntensityChange\"==a&&0==c[a].count&&window.imraidview.stopListeningMicIntensity();\"deviceMuted\"==a&&0==c[a].count&&\nwindow.imraidview.stopListeningDeviceMuteEvents();\"deviceVolumeChange\"==a&&0==c[a].count&&window.imraidview.stopListeningDeviceVolumeChange();\"volumeChange\"==a&&0==c[a].count&&window.imraidview.stopListeningVolumeChange();\"headphones\"==a&&0==c[a].count&&window.imraidview.stopListeningHeadphonePluggedEvents();\"backButtonPressed\"==a&&0==c[a].count&&window.imraidview.stopListeningForBackButtonPressedEvent();\"downloadStatusChanged\"==a&&0==c[a].count&&window.imraidview.unregisterDownloaderCallbacks()}};\nthis.broadcastEvent=function(a){if(imIsObjValid(a)){for(var b=Array(arguments.length),c=0;c<arguments.length;c++)b[c]=arguments[c];c=b.shift();try{this.listeners[c]&&this.listeners[c].broadcast(b)}catch(e){}}};this.sendSaveContentResult=function(a){if(imIsObjValid(a)){for(var b=Array(arguments.length),c=0;c<arguments.length;c++)if(2==c){var e=arguments[c],e=JSON.parse(e);b[c]=e}else b[c]=arguments[c];e=b[1];\"success\"!=e&&(c=b[0].substring(b[0].indexOf(\"_\")+1),imraid.saveContentIDMap[c]&&delete imraid.saveContentIDMap[c]);\nwindow.imraid.broadcastEvent(b[0],b[1],b[2])}}},__im__iosNativeMessageHandler=void 0;window.webkit&&(window.webkit.messageHandlers&&window.webkit.messageHandlers.nativeMessageHandler)&&(__im__iosNativeMessageHandler=window.webkit.messageHandlers.nativeMessageHandler);\nvar __im__iosNativeCall={nativeCallInFlight:!1,nativeCallQueue:[],executeNativeCall:function(a){this.nativeCallInFlight?this.nativeCallQueue.push(a):(this.nativeCallInFlight=!0,imIsObjValid(__im__iosNativeMessageHandler)?__im__iosNativeMessageHandler.postMessage(a):window.location=a)},nativeCallComplete:function(a){0==this.nativeCallQueue.length?this.nativeCallInFlight=!1:(a=this.nativeCallQueue.shift(),imIsObjValid(__im__iosNativeMessageHandler)?__im__iosNativeMessageHandler.postMessage(a):window.location=\na)}},IOSNativeCall=function(){this.urlScheme=\"\";this.executeNativeCall=function(a){if(imIsObjValid(__im__iosNativeMessageHandler)){e={};e.command=a;e.scheme=this.urlScheme;for(var b={},c=1;c<arguments.length;c+=2)d=arguments[c+1],null!=d&&(b[arguments[c]]=\"\"+d);e.params=b}else for(var e=this.urlScheme+\"://\"+a,d,b=!0,c=1;c<arguments.length;c+=2)d=arguments[c+1],null!=d&&(b?(e+=\"?\",b=!1):e+=\"&\",e+=arguments[c]+\"=\"+escape(d));__im__iosNativeCall.executeNativeCall(e);return\"OK\"};this.nativeCallComplete=\nfunction(a){__im__iosNativeCall.nativeCallComplete(a);return\"OK\"};this.updateKV=function(a,b){this[a]=b;var c=this.broadcastMap[a];c&&this.broadcastEvent(c,b)}};\n(function(){var a=window.mraidview={};a.orientationProperties={allowOrientationChange:!0,forceOrientation:\"none\",direction:\"right\"};var b=[],c=!1;a.detectAndBlockFraud=function(e){a.isPossibleFraud()&&a.fireRedirectFraudBeacon(e);return!1};a.popupBlocked=function(e){a.firePopupBlockedBeacon(e)};a.zeroPad=function(a){var d=\"\";10>a&&(d+=\"0\");return d+a};a.supports=function(a){console.log(\"bridge: supports (MRAID)\");if(\"string\"!=typeof a)window.mraid.broadcastEvent(\"error\",\"Supports method expects string parameter\",\n\"supports\");else return\"false\"!=sdkController.supports(\"window.mraidview\",a)};a.useCustomClose=function(a){try{sdkController.useCustomClose(\"window.mraidview\",a)}catch(d){imraidview.showAlert(\"use CustomClose: \"+d)}};a.close=function(){try{sdkController.close(\"window.mraidview\")}catch(a){imraidview.showAlert(\"close: \"+a)}};a.stackCommands=function(a,d){c?b.push(a):(eval(a),d&&(c=!0))};a.expand=function(a){try{\"undefined\"==typeof a&&(a=null),sdkController.expand(\"window.mraidview\",a)}catch(d){imraidview.showAlert(\"executeNativeExpand: \"+\nd+\", URL = \"+a)}};a.setExpandProperties=function(b){try{b?this.props=b:b=null;if(\"undefined\"!=typeof b.lockOrientation&&null!=b.lockOrientation&&\"undefined\"!=typeof b.orientation&&null!=b.orientation){var d={};d.allowOrientationChange=!b.lockOrientation;d.forceOrientation=b.orientation;a.setOrientationProperties(d)}sdkController.setExpandProperties(\"window.mraidview\",a.stringify(b))}catch(c){imraidview.showAlert(\"executeNativesetExpandProperties: \"+c+\", props = \"+b)}};a.getExpandProperties=function(){try{return eval(\"(\"+\nsdkController.getExpandProperties(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getExpandProperties: \"+a)}};a.setOrientationProperties=function(b){try{b?(\"undefined\"!=typeof b.allowOrientationChange&&(a.orientationProperties.allowOrientationChange=b.allowOrientationChange),\"undefined\"!=typeof b.forceOrientation&&(a.orientationProperties.forceOrientation=b.forceOrientation)):b=null,sdkController.setOrientationProperties(\"window.mraidview\",a.stringify(a.orientationProperties))}catch(d){imraidview.showAlert(\"setOrientationProperties: \"+\nd+\", props = \"+b)}};a.getOrientationProperties=function(){return{forceOrientation:a.orientationProperties.forceOrientation,allowOrientationChange:a.orientationProperties.allowOrientationChange}};a.resizeProps=null;a.setResizeProperties=function(b){var d,c;try{d=parseInt(b.width);c=parseInt(b.height);if(isNaN(d)||isNaN(c)||1>d||1>c)throw\"Invalid\";b.width=d;b.height=c;a.resizeProps=b;sdkController.setResizeProperties(\"window.mraidview\",a.stringify(b))}catch(g){window.mraid.broadcastEvent(\"error\",\"Invalid properties.\",\n\"setResizeProperties\")}};a.getResizeProperties=function(){try{return eval(\"(\"+sdkController.getResizeProperties(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getResizeProperties: \"+a)}};a.open=function(a){\"undefined\"==typeof a&&(a=null);try{sdkController.open(\"window.mraidview\",a)}catch(d){imraidview.showAlert(\"open: \"+d)}};a.getScreenSize=function(){try{return eval(\"(\"+sdkController.getScreenSize(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getScreenSize: \"+a)}};a.getMaxSize=\nfunction(){try{return eval(\"(\"+sdkController.getMaxSize(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getMaxSize: \"+a)}};a.getCurrentPosition=function(){try{return eval(\"(\"+sdkController.getCurrentPosition(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getCurrentPosition: \"+a)}};a.getDefaultPosition=function(){try{return eval(\"(\"+sdkController.getDefaultPosition(\"window.mraidview\")+\")\")}catch(a){imraidview.showAlert(\"getDefaultPosition: \"+a)}};a.getState=function(){try{return String(sdkController.getState(\"window.mraidview\"))}catch(a){imraidview.showAlert(\"getState: \"+\na)}};a.isViewable=function(){try{return sdkController.isViewable(\"window.mraidview\")}catch(a){imraidview.showAlert(\"isViewable: \"+a)}};a.getPlacementType=function(){return sdkController.getPlacementType(\"window.mraidview\")};a.close=function(){try{sdkController.close(\"window.mraidview\")}catch(a){imraidview.showAlert(\"close: \"+a)}};\"function\"!=typeof String.prototype.startsWith&&(String.prototype.startsWith=function(a){return 0==this.indexOf(a)});a.playVideo=function(a){var d=\"\";null!=a&&(d=a);try{sdkController.playVideo(\"window.mraidview\",\nd)}catch(b){imraidview.showAlert(\"playVideo: \"+b)}};a.stringify=function(b){if(\"undefined\"===typeof JSON){var d=\"\",c;if(\"undefined\"==typeof b.length)return a.stringifyArg(b);for(c=0;c<b.length;c++)0<c&&(d+=\",\"),d+=a.stringifyArg(b[c]);return d+\"]\"}return JSON.stringify(b)};a.stringifyArg=function(a){var b,c,g;c=typeof a;b=\"\";if(\"number\"===c||\"boolean\"===c)b+=args;else if(a instanceof Array)b=b+\"[\"+a+\"]\";else if(a instanceof Object){c=!0;b+=\"{\";for(g in a)null!==a[g]&&(c||(b+=\",\"),b=b+'\"'+g+'\":',c=\ntypeof a[g],b=\"number\"===c||\"boolean\"===c?b+a[g]:\"function\"===typeof a[g]?b+'\"\"':a[g]instanceof Object?b+this.stringify(args[i][g]):b+'\"'+a[g]+'\"',c=!1);b+=\"}\"}else a=a.replace(/\\\\/g,\"\\\\\\\\\"),a=a.replace(/\"/g,'\\\\\"'),b=b+'\"'+a+'\"';imraidview.showAlert(\"json:\"+b);return b};getPID=function(a){var b=\"\";null!=a&&(\"undefined\"!=typeof a.id&&null!=a.id)&&(b=a.id);return b};a.resize=function(){if(null==a.resizeProps)window.mraid.broadcastEvent(\"error\",\"Valid resize dimensions must be provided before calling resize\",\n\"resize\");else try{sdkController.resize(\"window.mraidview\")}catch(b){imraidview.showAlert(\"resize called in bridge\")}};a.storePicture=function(b){console.log(\"bridge: storePicture\");if(\"string\"!=typeof b)window.mraid.broadcastEvent(\"error\",\"storePicture method expects url as string parameter\",\"storePicture\");else{if(a.supports(\"storePicture\"))return!window.confirm(\"Do you want to download the file?\")?(window.mraid.broadcastEvent(\"error\",\"Store picture on \"+b+\" was cancelled by user.\",\"storePicture\"),\n!1):sdkController.storePicture(\"window.mraidview\",b);window.mraid.broadcastEvent(\"error\",\"Store picture on \"+b+\" was cancelled because it is unsupported in this device/app.\",\"storePicture\")}};a.fireMediaTrackingEvent=function(a,b){};a.fireMediaErrorEvent=function(a,b){};a.fireMediaTimeUpdateEvent=function(a,b,c){};a.fireMediaCloseEvent=function(a,b,c){};a.fireMediaVolumeChangeEvent=function(a,b,c){};a.broadcastEvent=function(){window.mraid.broadcastEvent.apply(window.mraid,arguments)}})();\n(function(){var a=window.mraid=new InmobiObj,b=window.mraidview,c=!1;b.isAdShownToUser=!1;b.onUserInteraction=function(){c=!0};b.isPossibleFraud=function(){return a.supports(\"redirectFraudDetection\")&&(!b.isAdShownToUser||!c)};b.fireRedirectFraudBeacon=function(a){if(\"undefined\"!=typeof inmobi&&inmobi.recordEvent){var d={};d.trigger=a;d.isAdShown=b.isAdShownToUser.toString();inmobi.recordEvent(135,d)}};b.firePopupBlockedBeacon=function(a){if(\"undefined\"!=typeof inmobi&&inmobi.recordEvent){var b={};\nb.trigger=a;inmobi.recordEvent(136,b)}};window.onbeforeunload=function(){b.detectAndBlockFraud(\"redirect\")};a.addEventListener(\"viewableChange\",function(a){a&&!b.isAdShownToUser&&(b.isAdShownToUser=!0)});a.useCustomClose=b.useCustomClose;a.close=b.close;a.getExpandProperties=b.getExpandProperties;a.setExpandProperties=function(c){\"undefined\"!=typeof c&&(\"useCustomClose\"in c&&\"undefined\"!=typeof a.getState()&&\"expanded\"!=a.getState())&&a.useCustomClose(c.useCustomClose);b.setExpandProperties(c)};a.getResizeProperties=\nb.getResizeProperties;a.setResizeProperties=b.setResizeProperties;a.getOrientationProperties=b.getOrientationProperties;a.setOrientationProperties=b.setOrientationProperties;a.expand=b.expand;a.getMaxSize=b.getMaxSize;a.getState=b.getState;a.isViewable=b.isViewable;a.createCalendarEvent=function(a){window.mraid.broadcastEvent(\"error\",\"Method not supported\",\"createCalendarEvent\")};a.open=function(c){b.detectAndBlockFraud(\"mraid.open\")||(\"string\"!=typeof c?a.broadcastEvent(\"error\",\"URL is required.\",\n\"open\"):b.open(c))};a.resize=b.resize;a.getVersion=function(){return\"2.0\"};a.getPlacementType=b.getPlacementType;a.playVideo=function(a){b.playVideo(a)};a.getScreenSize=b.getScreenSize;a.getCurrentPosition=b.getCurrentPosition;a.getDefaultPosition=b.getDefaultPosition;a.supports=function(a){return b.supports(a)};a.storePicture=function(c){\"string\"!=typeof c?a.broadcastEvent(\"error\",\"Request must specify a valid URL\",\"storePicture\"):b.storePicture(c)}})();\n(function(){var a=window.imraidview={},b,c=!0;a.setOrientationProperties=function(b){try{b?(\"undefined\"!=typeof b.allowOrientationChange&&(mraidview.orientationProperties.allowOrientationChange=b.allowOrientationChange),\"undefined\"!=typeof b.forceOrientation&&(mraidview.orientationProperties.forceOrientation=b.forceOrientation),\"undefined\"!=typeof b.direction&&(mraidview.orientationProperties.direction=b.direction)):b=null,sdkController.setOrientationProperties(\"window.imraidview\",mraidview.stringify(mraidview.orientationProperties))}catch(c){a.showAlert(\"setOrientationProperties: \"+\nc+\", props = \"+b)}};a.getOrientationProperties=function(){return mraidview.orientationProperties};a.getWindowOrientation=function(){var a=window.orientation;0>a&&(a+=360);window.innerWidth!==this.previousWidth&&0==a&&window.innerWidth>window.innerHeight&&(a=90);return a};var e=function(){window.setTimeout(function(){if(c||a.getWindowOrientation()!==b)c=!1,b=a.getWindowOrientation(),sdkController.onOrientationChange(\"window.imraidview\"),imraid.broadcastEvent(\"orientationChange\",b)},200)};a.registerOrientationListener=\nfunction(){b=a.getWindowOrientation();window.addEventListener(\"resize\",e,!1);window.addEventListener(\"orientationchange\",e,!1)};a.unRegisterOrientationListener=function(){window.removeEventListener(\"resize\",e,!1);window.removeEventListener(\"orientationchange\",e,!1)};window.imraidview.registerOrientationListener();a.firePostStatusEvent=function(a){window.imraid.broadcastEvent(\"postStatus\",a)};a.fireMediaTrackingEvent=function(a,b){var c={};c.name=a;var f=\"inmobi_media_\"+a;\"undefined\"!=typeof b&&(null!=\nb&&\"\"!=b)&&(f=f+\"_\"+b);window.imraid.broadcastEvent(f,c)};a.fireMediaErrorEvent=function(a,b){var c={name:\"error\"};c.code=b;var f=\"inmobi_media_\"+c.name;\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(f=f+\"_\"+a);window.imraid.broadcastEvent(f,c)};a.fireMediaTimeUpdateEvent=function(a,b,c){var f={name:\"timeupdate\",target:{}};f.target.currentTime=b;f.target.duration=c;b=\"inmobi_media_\"+f.name;\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(b=b+\"_\"+a);window.imraid.broadcastEvent(b,f)};a.saveContent=function(a,\nb,c){window.imraid.addEventListener(\"saveContent_\"+a,c);sdkController.saveContent(\"window.imraidview\",a,b)};a.cancelSaveContent=function(a){sdkController.cancelSaveContent(\"window.imraidview\",a)};a.disableCloseRegion=function(a){sdkController.disableCloseRegion(\"window.imraidview\",a)};a.fireGalleryImageSelectedEvent=function(a,b,c){var f=new Image;f.src=\"data:image/jpeg;base64,\"+a;f.width=b;f.height=c;window.imraid.broadcastEvent(\"galleryImageSelected\",f)};a.fireCameraPictureCatpturedEvent=function(a,\nb,c){var f=new Image;f.src=\"data:image/jpeg;base64,\"+a;f.width=b;f.height=c;window.imraid.broadcastEvent(\"cameraPictureCaptured\",f)};a.fireMediaCloseEvent=function(a,b,c){var f={name:\"close\"};f.viaUserInteraction=b;f.target={};f.target.currentTime=c;b=\"inmobi_media_\"+f.name;\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(b=b+\"_\"+a);window.imraid.broadcastEvent(b,f)};a.fireMediaVolumeChangeEvent=function(a,b,c){var f={name:\"volumechange\",target:{}};f.target.volume=b;f.target.muted=c;b=\"inmobi_media_\"+f.name;\n\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(b=b+\"_\"+a);window.imraid.broadcastEvent(b,f)};a.fireDeviceMuteChangeEvent=function(a){window.imraid.broadcastEvent(\"deviceMuted\",a)};a.fireDeviceVolumeChangeEvent=function(a){window.imraid.broadcastEvent(\"deviceVolumeChange\",a)};a.fireHeadphonePluggedEvent=function(a){window.imraid.broadcastEvent(\"headphones\",a)};a.showAlert=function(a){sdkController.showAlert(\"window.imraidview\",a)};a.openExternal=function(b,c){try{600<=getSdkVersionInt()?sdkController.openExternal(\"window.imraidview\",\nb,c):sdkController.openExternal(\"window.imraidview\",b)}catch(e){a.showAlert(\"openExternal: \"+e)}};a.log=function(b){try{sdkController.log(\"window.imraidview\",b)}catch(c){a.showAlert(\"log: \"+c)}};a.getPlatform=function(){return\"android\"};a.asyncPing=function(b){try{sdkController.asyncPing(\"window.imraidview\",b)}catch(c){a.showAlert(\"asyncPing: \"+c)}};a.startListeningDeviceMuteEvents=function(){sdkController.registerDeviceMuteEventListener(\"window.imraidview\")};a.stopListeningDeviceMuteEvents=function(){sdkController.unregisterDeviceMuteEventListener(\"window.imraidview\")};\na.startListeningDeviceVolumeChange=function(){sdkController.registerDeviceVolumeChangeEventListener(\"window.imraidview\")};a.stopListeningDeviceVolumeChange=function(){sdkController.unregisterDeviceVolumeChangeEventListener(\"window.imraidview\")};a.startListeningHeadphonePluggedEvents=function(){sdkController.registerHeadphonePluggedEventListener(\"window.imraidview\")};a.stopListeningHeadphonePluggedEvents=function(){sdkController.unregisterHeadphonePluggedEventListener(\"window.imraidview\")};getSdkVersionInt=\nfunction(){for(var b=a.getSdkVersion().split(\".\"),c=b.length,e=\"\",f=0;f<c;f++)e+=b[f];return parseInt(e)};a.getSdkVersion=function(){return window._im_imaiview.getSdkVersion()};a.supports=function(a){console.log(\"bridge: supports (IMRAID)\");if(\"string\"!=typeof a)window.imraid.broadcastEvent(\"error\",\"Supports method expects string parameter\",\"supports\");else return\"false\"!=sdkController.supports(\"window.imraidview\",a)};a.postToSocial=function(b,c,e,f){window.imraid.broadcastEvent(\"error\",\"Method not supported\",\n\"postToSocial\");a.log(\"Method postToSocial not supported\")};a.incentCompleted=function(a){if(\"object\"!=typeof a||null==a)sdkController.incentCompleted(\"window.imraidview\",null);else try{sdkController.incentCompleted(\"window.imraidview\",JSON.stringify(a))}catch(b){sdkController.incentCompleted(\"window.imraidview\",null)}};a.getOrientation=function(){try{return String(sdkController.getOrientation(\"window.imraidview\"))}catch(b){a.showAlert(\"getOrientation: \"+b)}};a.acceptAction=function(b){try{sdkController.acceptAction(\"window.imraidview\",\nmraidview.stringify(b))}catch(c){a.showAlert(\"acceptAction: \"+c+\", params = \"+b)}};a.rejectAction=function(b){try{sdkController.rejectAction(\"window.imraidview\",mraidview.stringify(b))}catch(c){a.showAlert(\"rejectAction: \"+c+\", params = \"+b)}};a.updateToPassbook=function(b){window.imraid.broadcastEvent(\"error\",\"Method not supported\",\"updateToPassbook\");a.log(\"Method not supported\")};a.isDeviceMuted=function(){return\"false\"!=sdkController.isDeviceMuted(\"window.imraidview\")};a.getDeviceVolume=function(){return 603>=\ngetSdkVersionInt()?-1:sdkController.getDeviceVolume(\"window.imraidview\")};a.isHeadPhonesPlugged=function(){return\"false\"!=sdkController.isHeadphonePlugged(\"window.imraidview\")};a.sendSaveContentResult=function(){window.imraid.sendSaveContentResult.apply(window.imraid,arguments)};a.broadcastEvent=function(){window.imraid.broadcastEvent.apply(window.imraid,arguments)};a.disableBackButton=function(a){void 0==a||\"boolean\"!=typeof a?console.log(\"disableBackButton called with invalid params\"):sdkController.disableBackButton(\"window.imraidview\",\na)};a.isBackButtonDisabled=function(){return sdkController.isBackButtonDisabled(\"window.imraidview\")};a.startListeningForBackButtonPressedEvent=function(){sdkController.registerBackButtonPressedEventListener(\"window.imraidview\")};a.stopListeningForBackButtonPressedEvent=function(){sdkController.unregisterBackButtonPressedEventListener(\"window.imraidview\")};a.hideStatusBar=function(){};a.setOpaqueBackground=function(){};a.startDownloader=function(a,b,c){682<=getSdkVersionInt()&&sdkController.startDownloader(\"window.imraidview\",\na,b,c)};a.registerDownloaderCallbacks=function(){682<=getSdkVersionInt()&&sdkController.registerDownloaderCallbacks(\"window.imraidview\")};a.unregisterDownloaderCallbacks=function(){682<=getSdkVersionInt()&&sdkController.unregisterDownloaderCallbacks(\"window.imraidview\")};a.getDownloadProgress=function(){return 682<=getSdkVersionInt()?sdkController.getDownloadProgress(\"window.imraidview\"):-1};a.getDownloadStatus=function(){return 682<=getSdkVersionInt()?sdkController.getDownloadStatus(\"window.imraidview\"):\n-1};a.fireEvent=function(a){700<=getSdkVersionInt()&&(\"fireSkip\"===a?sdkController.fireSkip(\"window.imraidview\"):\"fireComplete\"===a?sdkController.fireComplete(\"window.imraidview\"):\"showEndCard\"===a&&sdkController.showEndCard(\"window.imraidview\"))};a.saveBlob=function(a){700<=getSdkVersionInt()&&sdkController.saveBlob(\"window.imraidview\",a)};a.getBlob=function(a,b){700<=getSdkVersionInt()&&sdkController.getBlob(a,b)};a.setCloseEndCardTracker=function(a){700<=getSdkVersionInt()&&sdkController.setCloseEndCardTracker(\"window.imraidview\",\na)}})();\n(function(){var a=window.imraid=new InmobiObj,b=window.imraidview;a.getOrientation=b.getOrientation;a.setOrientationProperties=b.setOrientationProperties;a.getOrientationProperties=b.getOrientationProperties;a.saveContentIDMap={};a.saveContent=function(c,e,d){var k=arguments.length,g,f=null;if(3>k){if(\"function\"===typeof arguments[k-1])g=arguments[k-1];else return;f={reason:1}}else a.saveContentIDMap[c]&&(g=arguments[2],f={reason:11,url:arguments[1]});\"function\"!==!g&&(f?(window.imraid.addEventListener(\"saveContent_failed_\"+c,\ng),window.imraid.sendSaveContentResult(\"saveContent_failed_\"+c,\"failed\",JSON.stringify(f))):(a.removeEventListener(\"saveContent_\"+c),a.saveContentIDMap[c]=!0,b.saveContent(c,e,d)))};a.cancelSaveContent=function(a){b.cancelSaveContent(a)};a.asyncPing=function(c){\"string\"!=typeof c?a.broadcastEvent(\"error\",\"URL is required.\",\"asyncPing\"):b.asyncPing(c)};a.disableCloseRegion=b.disableCloseRegion;a.getSdkVersion=b.getSdkVersion;a.log=function(c){\"undefined\"==typeof c?a.broadcastEvent(\"error\",\"message is required.\",\n\"log\"):\"string\"==typeof c?b.log(c):b.log(JSON.stringify(c))};a.getInMobiAIVersion=function(){return\"2.0\"};a.getVendorName=function(){return\"inmobi\"};a.openExternal=function(a,e){mraidview.detectAndBlockFraud(\"imraid.openExternal\")||b.openExternal(a,e)};a.updateToPassbook=function(c){mraidview.detectAndBlockFraud(\"imraid.updateToPassbook\")||(\"string\"!=typeof c?a.broadcastEvent(\"error\",\"Request must specify a valid URL\",\"updateToPassbook\"):b.updateToPassbook(c))};a.postToSocial=function(a,e,d,k){mraidview.detectAndBlockFraud(\"imraid.postToSocial\")||\nb.postToSocial(a,e,d,k)};a.getPlatform=b.getPlatform;a.incentCompleted=b.incentCompleted;a.loadSKStore=b.loadSKStore;a.showSKStore=function(a){mraidview.detectAndBlockFraud(\"imraid.showSKStore\")||b.showSKStore(a)};a.supports=function(a){return b.supports(a)};a.isDeviceMuted=function(){return!imIsObjValid(a.listeners.deviceMuted)?-1:b.isDeviceMuted()};a.isHeadPhonesPlugged=function(){return!imIsObjValid(a.listeners.headphones)?!1:b.isHeadPhonesPlugged()};a.getDeviceVolume=function(){return b.getDeviceVolume()};\na.setDeviceVolume=function(a){b.setDeviceVolume(a)};a.hideStatusBar=function(){b.hideStatusBar()};a.setOpaqueBackground=function(){b.setOpaqueBackground()};a.disableBackButton=b.disableBackButton;a.isBackButtonDisabled=b.isBackButtonDisabled;a.startDownloader=b.startDownloader;a.getDownloadProgress=b.getDownloadProgress;a.getDownloadStatus=b.getDownloadStatus;a.fireEvent=b.fireEvent;a.saveBlob=b.saveBlob;a.getBlob=b.getBlob;a.setCloseEndCardTracker=b.setCloseEndCardTracker})();\n(function(){var a=window._im_imaiview={ios:{}};window.imaiview=a;a.broadcastEvent=function(){for(var a=Array(arguments.length),c=0;c<arguments.length;c++)a[c]=arguments[c];c=a.shift();try{window.mraid.broadcastEvent(c,a)}catch(e){}};a.getPlatform=function(){return\"android\"};a.getPlatformVersion=function(){return sdkController.getPlatformVersion(\"window.imaiview\")};a.log=function(a){sdkController.log(\"window.imaiview\",a)};a.openEmbedded=function(a){sdkController.openEmbedded(\"window.imaiview\",a)};\na.openExternal=function(a,c){600<=getSdkVersionInt()?sdkController.openExternal(\"window.imaiview\",a,c):sdkController.openExternal(\"window.imaiview\",a)};a.ping=function(a,c){sdkController.ping(\"window.imaiview\",a,c)};a.pingInWebView=function(a,c){sdkController.pingInWebView(\"window.imaiview\",a,c)};a.getSdkVersion=function(){try{var a=sdkController.getSdkVersion(\"window.imaiview\");if(\"string\"==typeof a&&null!=a)return a}catch(c){return\"3.7.0\"}};a.onUserInteraction=function(a){if(\"object\"!=typeof a||\nnull==a)sdkController.onUserInteraction(\"window.imaiview\",null);else try{sdkController.onUserInteraction(\"window.imaiview\",JSON.stringify(a))}catch(c){sdkController.onUserInteraction(\"window.imaiview\",null)}};a.fireAdReady=function(){sdkController.fireAdReady(\"window.imaiview\")};a.fireAdFailed=function(){sdkController.fireAdFailed(\"window.imaiview\")};a.broadcastEvent=function(){window.imai.broadcastEvent.apply(window.imai,arguments)}})();\n(function(){var a=window._im_imaiview;window._im_imai=new InmobiObj;window._im_imai.ios=new InmobiObj;var b=window._im_imai;window.imai=window._im_imai;b.matchString=function(a,b){if(\"string\"!=typeof a||null==a||null==b)return-1;var d=-1;try{d=a.indexOf(b)}catch(k){}return d};b.isHttpUrl=function(a){return\"string\"!=typeof a||null==a?!1:0==b.matchString(a,\"http://\")?!0:0==b.matchString(a,\"https://\")?!0:!1};b.appendTapParams=function(a,e,d){if(!imIsObjValid(e)||!imIsObjValid(d))return a;b.isHttpUrl(a)&&\n(a=-1==b.matchString(a,\"?\")?a+(\"?u-tap-o=\"+e+\",\"+d):a+(\"&u-tap-o=\"+e+\",\"+d));return a};b.performAdClick=function(a,e){e=e||event;if(imIsObjValid(a)){var d=a.clickConfig,k=a.landingConfig;if(!imIsObjValid(d)&&!imIsObjValid(k))b.log(\"click/landing config are invalid, Nothing to process .\"),this.broadcastEvent(\"error\",\"click/landing config are invalid, Nothing to process .\");else{var g=null,f=null,h=null,m=null,n=null,l=null,q=null,p=null;if(imIsObjValid(e))try{m=e.changedTouches[0].pageX,n=e.changedTouches[0].pageY}catch(r){n=\nm=0}imIsObjValid(k)?imIsObjValid(d)?(l=k.url,q=k.fallbackUrl,p=k.urlType,g=d.url,f=d.pingWV,h=d.fr):(l=k.url,p=k.urlType):(l=d.url,p=d.urlType);d=b.getPlatform();try{if(\"boolean\"!=typeof h&&\"number\"!=typeof h||null==h)h=!0;if(0>h||1<h)h=!0;if(\"boolean\"!=typeof f&&\"number\"!=typeof f||null==f)f=!0;if(0>f||1<f)f=!0;if(\"number\"!=typeof p||null==p)p=0;g=b.appendTapParams(g,m,n);imIsObjValid(g)?!0==f?b.pingInWebView(g,h):b.ping(g,h):b.log(\"clickurl provided is null.\");if(imIsObjValid(l))switch(imIsObjValid(g)||\n(l=b.appendTapParams(l,m,n)),p){case 1:b.openEmbedded(l);break;case 2:\"ios\"==d?b.ios.openItunesProductView(l):this.broadcastEvent(\"error\",\"Cannot process openItunesProductView for os\"+d);break;default:b.openExternal(l,q)}else b.log(\"Landing url provided is null.\")}catch(s){}}}else b.log(\" invalid config, nothing to process .\"),this.broadcastEvent(\"error\",\"invalid config, nothing to process .\")};b.performActionClick=function(a,e){e=e||event;if(imIsObjValid(a)){var d=a.clickConfig,k=a.landingConfig;\nif(!imIsObjValid(d)&&!imIsObjValid(k))b.log(\"click/landing config are invalid, Nothing to process .\"),this.broadcastEvent(\"error\",\"click/landing config are invalid, Nothing to process .\");else{var g=null,f=null,h=null,m=null,n=null;if(imIsObjValid(e))try{m=e.changedTouches[0].pageX,n=e.changedTouches[0].pageY}catch(l){n=m=0}imIsObjValid(d)&&(g=d.url,f=d.pingWV,h=d.fr);try{if(\"boolean\"!=typeof h&&\"number\"!=typeof h||null==h)h=!0;if(0>h||1<h)h=!0;if(\"boolean\"!=typeof f&&\"number\"!=typeof f||null==f)f=\n!0;if(0>f||1<f)f=!0;g=b.appendTapParams(g,m,n);imIsObjValid(g)?!0==f?b.pingInWebView(g,h):b.ping(g,h):b.log(\"clickurl provided is null.\");b.onUserInteraction(k)}catch(q){}}}else b.log(\" invalid config, nothing to process .\"),this.broadcastEvent(\"error\",\"invalid config, nothing to process .\")};b.getVersion=function(){return\"1.0\"};b.getPlatform=a.getPlatform;b.getPlatformVersion=a.getPlatformVersion;b.log=a.log;b.openEmbedded=function(b){mraidview.detectAndBlockFraud(\"imai.openEmbedded\")||a.openEmbedded(b)};\nb.openExternal=function(b,e){mraidview.detectAndBlockFraud(\"imai.openExternal\")||a.openExternal(b,e)};b.ping=a.ping;b.pingInWebView=a.pingInWebView;b.onUserInteraction=a.onUserInteraction;b.getSdkVersion=a.getSdkVersion;b.loadSKStore=a.loadSKStore;b.showSKStore=function(b){mraidview.detectAndBlockFraud(\"imai.showSKStore\")||a.showSKStore(b)};b.ios.openItunesProductView=function(b){mraidview.detectAndBlockFraud(\"imai.ios.openItunesProductView\")||a.ios.openItunesProductView(b)};b.fireAdReady=a.fireAdReady;\nb.fireAdFailed=a.fireAdFailed})();" : c2;
    }

    public final void a() {
        this.P = false;
        try {
            getClass().getMethod("setLayerType", new Class[]{Integer.TYPE, Paint.class}).invoke(this, new Object[]{Integer.valueOf(1), null});
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException unused) {
        } catch (InvocationTargetException unused2) {
        }
    }

    @TargetApi(16)
    public final boolean e(String str) {
        char c2;
        switch (str.hashCode()) {
            case -1886160473:
                if (str.equals(MraidJsMethods.PLAY_VIDEO)) {
                    c2 = 1;
                    break;
                }
            case -1647691422:
                if (str.equals(MRAIDNativeFeature.INLINE_VIDEO)) {
                    c2 = 3;
                    break;
                }
            case 1509574865:
                if (str.equals("html5video")) {
                    c2 = 4;
                    break;
                }
            case 1642189884:
                if (str.equals("saveContent")) {
                    c2 = 2;
                    break;
                }
            case 1772979069:
                if (str.equals("redirectFraudDetection")) {
                    c2 = 0;
                    break;
                }
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                return false;
            case 1:
            case 2:
                return true;
            case 3:
            case 4:
                return this.F && this.P;
            default:
                return false;
        }
    }

    public final boolean e() {
        i renderingConfig = getRenderingConfig();
        if (renderingConfig == null) {
            return false;
        }
        if (!renderingConfig.g || this.ac || this.ad) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final int getDownloadProgress() {
        getReferenceContainer().getApkDownloader();
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public final int getDownloadStatus() {
        getReferenceContainer().getApkDownloader();
        return -2;
    }

    public final void a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append("(");
        sb.append(str3);
        sb.append(");");
        a(str, sb.toString());
    }

    /* access modifiers changed from: 0000 */
    public final void setCloseEndCardTracker(String str) {
        AdContainer referenceContainer = getReferenceContainer();
        if (referenceContainer instanceof bd) {
            NativeVideoWrapper nativeVideoWrapper = (NativeVideoWrapper) ((bd) referenceContainer).getVideoContainerView();
            if (nativeVideoWrapper != null) {
                be beVar = (be) nativeVideoWrapper.getVideoView().getTag();
                if (beVar != null && beVar.b() != null && beVar.b().f() != null) {
                    beVar.b().f().a(new NativeTracker(str, 0, TrackerEventType.TRACKER_EVENT_TYPE_END_CARD_CLOSE, null));
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void c(String str) {
        StringBuilder sb = new StringBuilder("window.mraidview.fireRedirectFraudBeacon('");
        sb.append(str);
        sb.append("')");
        d(sb.toString());
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("plId", Long.valueOf(this.U));
            hashMap.put(RequestParameters.CREATIVE_ID, this.V);
            hashMap.put("impId", this.W);
            hashMap.put("trigger", str);
            com.inmobi.commons.core.e.b.a();
            com.inmobi.commons.core.e.b.a("ads", "BlockAutoRedirection", hashMap);
        } catch (Exception e2) {
            StringBuilder sb2 = new StringBuilder("Error in submitting telemetey event : (");
            sb2.append(e2.getMessage());
            sb2.append(")");
        }
    }

    public final void setExpandProperties(com.inmobi.rendering.mraid.a aVar) {
        if (aVar.b) {
            setUseCustomClose(aVar.a);
        }
        this.G = aVar;
    }

    static /* synthetic */ void k(RenderView renderView) {
        renderView.d("window.imaiview.broadcastEvent('ready');");
        renderView.d("window.mraidview.broadcastEvent('ready');");
    }

    static /* synthetic */ boolean a(RenderView renderView, JsResult jsResult) {
        i renderingConfig = renderView.getRenderingConfig();
        if (renderingConfig != null && renderingConfig.l) {
            return true;
        }
        jsResult.cancel();
        renderView.d("window.mraidview.popupBlocked('popupBlocked')");
        return false;
    }
}
