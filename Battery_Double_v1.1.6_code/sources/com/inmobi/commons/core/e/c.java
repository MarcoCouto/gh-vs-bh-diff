package com.inmobi.commons.core.e;

import com.tapjoy.TJAdUnitConstants.String;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: TelemetryComponentConfig */
public final class c {
    int a = 0;
    boolean b = false;
    private String c = "telemetry";
    private Map<String, a> d = new HashMap();

    /* compiled from: TelemetryComponentConfig */
    static final class a {
        String a;
        int b;
    }

    public c() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0034 A[Catch:{ JSONException -> 0x009f }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004a A[Catch:{ JSONException -> 0x009f }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0051 A[Catch:{ JSONException -> 0x009f }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0064 A[Catch:{ JSONException -> 0x009f }] */
    public c(String str, JSONObject jSONObject, c cVar) {
        String str2;
        boolean z;
        if (jSONObject == null) {
            a(str);
            return;
        }
        if (str != null) {
            try {
                if (str.trim().length() != 0) {
                    str2 = str;
                    this.c = str2;
                    if (jSONObject.has(String.ENABLED)) {
                        if (!jSONObject.getBoolean(String.ENABLED)) {
                            z = false;
                            this.b = z;
                            this.a = jSONObject.has("samplingFactor") ? jSONObject.getInt("samplingFactor") : cVar.a;
                            this.d = new HashMap();
                            if (jSONObject.has(EventEntry.TABLE_NAME)) {
                                JSONArray jSONArray = jSONObject.getJSONArray(EventEntry.TABLE_NAME);
                                for (int i = 0; i < jSONArray.length(); i++) {
                                    a aVar = new a();
                                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                                    aVar.a = jSONObject2.getString("type");
                                    aVar.b = jSONObject2.has("samplingFactor") ? jSONObject2.getInt("samplingFactor") : this.a;
                                    this.d.put(aVar.a, aVar);
                                }
                            }
                        }
                    }
                    z = true;
                    this.b = z;
                    this.a = jSONObject.has("samplingFactor") ? jSONObject.getInt("samplingFactor") : cVar.a;
                    this.d = new HashMap();
                    if (jSONObject.has(EventEntry.TABLE_NAME)) {
                    }
                }
            } catch (JSONException unused) {
                a(str);
                return;
            }
        }
        str2 = cVar.c;
        this.c = str2;
        if (jSONObject.has(String.ENABLED)) {
        }
        z = true;
        this.b = z;
        this.a = jSONObject.has("samplingFactor") ? jSONObject.getInt("samplingFactor") : cVar.a;
        this.d = new HashMap();
        if (jSONObject.has(EventEntry.TABLE_NAME)) {
        }
    }

    private void a(String str) {
        this.b = true;
        this.c = str;
    }
}
