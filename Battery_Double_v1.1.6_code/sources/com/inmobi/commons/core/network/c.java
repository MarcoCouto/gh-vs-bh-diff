package com.inmobi.commons.core.network;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.configs.b;
import com.inmobi.commons.core.configs.g;
import com.inmobi.commons.core.utilities.b.e;
import com.inmobi.commons.core.utilities.b.f;
import com.inmobi.commons.core.utilities.uid.d;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.json.JSONObject;

/* compiled from: NetworkRequest */
public class c {
    private static final String a = "c";
    private static byte[] f;
    private static byte[] g;
    public boolean A;
    private d b;
    private boolean c;
    private byte[] d;
    private byte[] e;
    private boolean h;
    protected Map<String, String> l;
    protected Map<String, String> m;
    public Map<String, String> n;
    String o;
    public String p;
    public int q;
    public int r;
    public boolean s;
    public boolean t;
    public long u;
    public boolean v;
    public int w;
    public boolean x;
    public g y;
    public boolean z;

    public c(String str, String str2, boolean z2, d dVar) {
        this(str, str2, z2, dVar, false, 0);
    }

    public c(String str, String str2) {
        this(str, str2, false, null, false, 0);
        this.h = false;
    }

    public c(String str, String str2, d dVar, int i) {
        this(str, str2, true, dVar, false, i);
    }

    public c(String str, String str2, boolean z2, d dVar, boolean z3, int i) {
        this.l = new HashMap();
        this.q = 60000;
        this.r = 60000;
        this.s = true;
        this.t = true;
        this.u = -1;
        this.w = 0;
        this.h = true;
        this.x = false;
        this.z = true;
        this.A = false;
        this.o = str;
        this.p = str2;
        this.c = z2;
        this.b = dVar;
        this.l.put("User-Agent", a.f());
        this.v = z3;
        this.w = i;
        if (HttpRequest.METHOD_GET.equals(str)) {
            this.m = new HashMap();
        } else if (HttpRequest.METHOD_POST.equals(str)) {
            this.n = new HashMap();
        }
        this.y = new g();
        b.a().a((com.inmobi.commons.core.configs.a) this.y, (com.inmobi.commons.core.configs.b.c) null);
    }

    public final void a(Map<String, String> map) {
        if (map != null) {
            this.l.putAll(map);
        }
    }

    public final void b(Map<String, String> map) {
        if (map != null) {
            this.m.putAll(map);
        }
    }

    public final void c(Map<String, String> map) {
        this.n.putAll(map);
    }

    public final Map<String, String> d() {
        com.inmobi.commons.core.utilities.d.a(this.l);
        return this.l;
    }

    public final String e() {
        String str = this.p;
        if (this.m == null) {
            return str;
        }
        String c2 = c();
        if (c2 == null || c2.trim().length() == 0) {
            return str;
        }
        if (!str.contains("?")) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("?");
            str = sb.toString();
        }
        if (!str.endsWith(RequestParameters.AMPERSAND) && !str.endsWith("?")) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(RequestParameters.AMPERSAND);
            str = sb2.toString();
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append(c2);
        return sb3.toString();
    }

    @CallSuper
    public void a() {
        e.c();
        boolean z2 = true;
        if (this.w != 1) {
            z2 = false;
        }
        this.w = e.a(z2);
        if (this.t) {
            if (HttpRequest.METHOD_GET.equals(this.o)) {
                d(this.m);
            } else if (HttpRequest.METHOD_POST.equals(this.o)) {
                d(this.n);
            }
        }
        if (this.h) {
            JSONObject a2 = e.a();
            if (a2 != null) {
                if (HttpRequest.METHOD_GET.equals(this.o)) {
                    this.m.put("consentObject", a2.toString());
                } else if (HttpRequest.METHOD_POST.equals(this.o)) {
                    this.n.put("consentObject", a2.toString());
                }
            }
        }
        if (this.z) {
            if (HttpRequest.METHOD_GET.equals(this.o)) {
                this.m.put("u-appsecure", Integer.toString(com.inmobi.commons.core.utilities.b.a.a().c));
            } else if (HttpRequest.METHOD_POST.equals(this.o)) {
                this.n.put("u-appsecure", Integer.toString(com.inmobi.commons.core.utilities.b.a.a().c));
            }
        }
    }

    private String c() {
        com.inmobi.commons.core.utilities.d.a(this.m);
        return com.inmobi.commons.core.utilities.d.a(this.m, RequestParameters.AMPERSAND);
    }

    public final String f() {
        com.inmobi.commons.core.utilities.d.a(this.n);
        String a2 = com.inmobi.commons.core.utilities.d.a(this.n, RequestParameters.AMPERSAND);
        new StringBuilder("Post body url: ").append(this.p);
        if (!b()) {
            return a2;
        }
        if (this.A) {
            if (f == null) {
                f = com.inmobi.commons.core.utilities.a.b.a(16);
            }
            if (g == null) {
                g = com.inmobi.commons.core.utilities.a.b.a();
            }
            this.d = f;
            this.e = g;
        } else {
            this.d = com.inmobi.commons.core.utilities.a.b.a(16);
            this.e = com.inmobi.commons.core.utilities.a.b.a();
        }
        byte[] bArr = this.d;
        byte[] bArr2 = this.e;
        g gVar = this.y;
        byte[] a3 = com.inmobi.commons.core.utilities.a.b.a(8);
        HashMap hashMap = new HashMap();
        hashMap.put("sm", com.inmobi.commons.core.utilities.a.b.a(a2, bArr2, bArr, a3, gVar.b, gVar.a));
        hashMap.put("sn", gVar.c);
        return com.inmobi.commons.core.utilities.d.a((Map<String, String>) hashMap, RequestParameters.AMPERSAND);
    }

    public boolean b() {
        return this.c;
    }

    private void d(Map<String, String> map) {
        map.putAll(com.inmobi.commons.core.utilities.b.a.a().b);
        map.putAll(com.inmobi.commons.core.utilities.b.b.a(this.x));
        map.putAll(f.a());
        if (this.b != null) {
            if (b()) {
                d dVar = this.b;
                HashMap hashMap = new HashMap();
                hashMap.put("u-id-map", new JSONObject(dVar.a((String) null, false)).toString());
                map.putAll(hashMap);
                return;
            }
            d dVar2 = this.b;
            String num = Integer.toString(new Random().nextInt());
            String a2 = com.inmobi.commons.core.utilities.a.c.a(new JSONObject(dVar2.a(num, true)).toString());
            HashMap hashMap2 = new HashMap();
            hashMap2.put("u-id-map", a2);
            hashMap2.put("u-id-key", num);
            com.inmobi.commons.core.utilities.uid.c.a();
            hashMap2.put("u-key-ver", com.inmobi.commons.core.utilities.uid.c.d());
            map.putAll(hashMap2);
        }
    }

    public final long g() {
        try {
            if (HttpRequest.METHOD_GET.equals(this.o)) {
                return 0 + ((long) c().length());
            }
            if (HttpRequest.METHOD_POST.equals(this.o)) {
                return ((long) f().length()) + 0;
            }
            return 0;
        } catch (Exception unused) {
            return 0;
        }
    }

    @Nullable
    public final byte[] a(byte[] bArr) {
        try {
            if (this.A) {
                return com.inmobi.commons.core.utilities.a.b.a(Base64.decode(bArr, 0), g, f);
            }
            return com.inmobi.commons.core.utilities.a.b.a(Base64.decode(bArr, 0), this.e, this.d);
        } catch (IllegalArgumentException e2) {
            new StringBuilder("Msg : ").append(e2.getMessage());
            return null;
        }
    }
}
