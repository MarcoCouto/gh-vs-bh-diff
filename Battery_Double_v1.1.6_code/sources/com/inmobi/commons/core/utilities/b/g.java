package com.inmobi.commons.core.utilities.b;

import android.location.Location;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.d.c;
import java.util.HashMap;
import java.util.Locale;

/* compiled from: PublisherProvidedUserInfoDao */
public final class g {
    private static int a = Integer.MIN_VALUE;
    private static String b = null;
    private static String c = null;
    private static String d = null;
    private static String e = null;
    private static String f = null;
    private static String g = null;
    private static int h = Integer.MIN_VALUE;
    private static String i;
    private static String j;
    private static String k;
    private static String l;
    private static Location m;

    public static String a() {
        return c.a("user_info_store");
    }

    public static void b() {
        a(a);
        a(b);
        b(c);
        c(d);
        d(e);
        e(f);
        f(g);
        b(h);
        g(i);
        h(j);
        i(k);
        j(l);
        a(m);
    }

    public static void a(int i2) {
        if (!a.a() || i2 == Integer.MIN_VALUE) {
            a = i2;
        } else {
            c.b("user_info_store").a("user_age", i2);
        }
    }

    public static void a(String str) {
        if (!a.a() || str == null) {
            b = str;
        } else {
            c.b("user_info_store").a("user_age_group", str);
        }
    }

    public static void b(String str) {
        if (!a.a() || str == null) {
            c = str;
        } else {
            c.b("user_info_store").a("user_area_code", str);
        }
    }

    public static void c(String str) {
        if (!a.a() || str == null) {
            d = str;
        } else {
            c.b("user_info_store").a("user_post_code", str);
        }
    }

    public static void d(String str) {
        if (!a.a() || str == null) {
            e = str;
        } else {
            c.b("user_info_store").a("user_city_code", str);
        }
    }

    public static void e(String str) {
        if (!a.a() || str == null) {
            f = str;
        } else {
            c.b("user_info_store").a("user_state_code", str);
        }
    }

    public static void f(String str) {
        if (!a.a() || str == null) {
            g = str;
        } else {
            c.b("user_info_store").a("user_country_code", str);
        }
    }

    public static void b(int i2) {
        if (!a.a() || i2 == Integer.MIN_VALUE) {
            h = i2;
        } else {
            c.b("user_info_store").a("user_yob", i2);
        }
    }

    public static void g(String str) {
        if (!a.a() || str == null) {
            i = str;
        } else {
            c.b("user_info_store").a("user_gender", str);
        }
    }

    public static void h(String str) {
        if (!a.a() || str == null) {
            j = str;
        } else {
            c.b("user_info_store").a("user_education", str);
        }
    }

    public static void i(String str) {
        if (!a.a() || str == null) {
            k = str;
        } else {
            c.b("user_info_store").a("user_language", str);
        }
    }

    public static void j(String str) {
        if (!a.a() || str == null) {
            l = str;
        } else {
            c.b("user_info_store").a("user_interest", str);
        }
    }

    public static Location c() {
        if (m != null) {
            return m;
        }
        String c2 = c.b("user_info_store").c("user_location");
        Location location = null;
        if (c2 == null) {
            return null;
        }
        Location location2 = new Location("");
        try {
            String[] split = c2.split(",");
            location2.setLatitude(Double.parseDouble(split[0]));
            location2.setLongitude(Double.parseDouble(split[1]));
            location2.setAccuracy(Float.parseFloat(split[2]));
            location2.setTime(Long.parseLong(split[3]));
            location = location2;
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException unused) {
        }
        return location;
    }

    public static void a(Location location) {
        if (!a.a() || location == null) {
            m = location;
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(location.getLatitude());
        sb.append(",");
        sb.append(location.getLongitude());
        sb.append(",");
        sb.append((int) location.getAccuracy());
        sb.append(",");
        sb.append(location.getTime());
        c.b("user_info_store").a("user_location", sb.toString());
    }

    public static HashMap<String, String> d() {
        int i2;
        int i3;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        HashMap<String, String> hashMap = new HashMap<>();
        if (a != Integer.MIN_VALUE) {
            i2 = a;
        } else {
            i2 = c.b("user_info_store").d("user_age");
        }
        if (i2 != Integer.MIN_VALUE && i2 > 0) {
            hashMap.put("u-age", String.valueOf(i2));
        }
        if (h != Integer.MIN_VALUE) {
            i3 = h;
        } else {
            i3 = c.b("user_info_store").d("user_yob");
        }
        if (i3 != Integer.MIN_VALUE && i3 > 0) {
            hashMap.put("u-yearofbirth", String.valueOf(i3));
        }
        if (e != null) {
            str = e;
        } else {
            str = c.b("user_info_store").c("user_city_code");
        }
        if (f != null) {
            str2 = f;
        } else {
            str2 = c.b("user_info_store").c("user_state_code");
        }
        if (g != null) {
            str3 = g;
        } else {
            str3 = c.b("user_info_store").c("user_country_code");
        }
        String str11 = "";
        if (!(str == null || str.trim().length() == 0)) {
            str11 = str.trim();
        }
        if (!(str2 == null || str2.trim().length() == 0)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str11);
            sb.append("-");
            sb.append(str2.trim());
            str11 = sb.toString();
        }
        if (!(str3 == null || str3.trim().length() == 0)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str11);
            sb2.append("-");
            sb2.append(str3.trim());
            str11 = sb2.toString();
        }
        if (!(str11 == null || str11.trim().length() == 0)) {
            hashMap.put("u-location", str11);
        }
        if (b != null) {
            str4 = b;
        } else {
            str4 = c.b("user_info_store").c("user_age_group");
        }
        if (str4 != null) {
            hashMap.put("u-agegroup", str4.toLowerCase(Locale.ENGLISH));
        }
        if (c != null) {
            str5 = c;
        } else {
            str5 = c.b("user_info_store").c("user_area_code");
        }
        if (str5 != null) {
            hashMap.put("u-areacode", str5);
        }
        if (d != null) {
            str6 = d;
        } else {
            str6 = c.b("user_info_store").c("user_post_code");
        }
        if (str6 != null) {
            hashMap.put("u-postalcode", str6);
        }
        if (i != null) {
            str7 = i;
        } else {
            str7 = c.b("user_info_store").c("user_gender");
        }
        if (str7 != null) {
            hashMap.put("u-gender", str7);
        }
        if (j != null) {
            str8 = j;
        } else {
            str8 = c.b("user_info_store").c("user_education");
        }
        if (str8 != null) {
            hashMap.put("u-education", str8);
        }
        if (k != null) {
            str9 = k;
        } else {
            str9 = c.b("user_info_store").c("user_language");
        }
        if (str9 != null) {
            hashMap.put("u-language", str9);
        }
        if (l != null) {
            str10 = l;
        } else {
            str10 = c.b("user_info_store").c("user_interest");
        }
        if (str10 != null) {
            hashMap.put("u-interests", str10);
        }
        return hashMap;
    }
}
