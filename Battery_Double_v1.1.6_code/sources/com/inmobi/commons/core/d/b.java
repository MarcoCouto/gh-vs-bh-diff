package com.inmobi.commons.core.d;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import com.inmobi.commons.a.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: DbStore */
public final class b {
    private static final String a = "b";
    private static volatile b b;
    private static final Object c = new Object();
    private static final Object d = new Object();
    private static int e = 0;
    private SQLiteDatabase f;

    private b() {
        try {
            this.f = new a(a.b()).getWritableDatabase();
            b = this;
        } catch (Exception unused) {
        }
    }

    public static synchronized b a() {
        b bVar;
        b bVar2;
        synchronized (b.class) {
            synchronized (d) {
                e++;
            }
            bVar = b;
            if (bVar == null) {
                synchronized (c) {
                    bVar2 = b;
                    if (bVar2 == null) {
                        bVar2 = new b();
                        b = bVar2;
                    }
                }
                bVar = bVar2;
            }
        }
        return bVar;
    }

    public final synchronized void a(String str, ContentValues contentValues, String str2, String[] strArr) {
        try {
            if (!a(str, contentValues)) {
                b(str, contentValues, str2, strArr);
            }
        } catch (Exception e2) {
            new StringBuilder("SDK encountered unexpected error in DbStore.insertOrUpdate() method; ").append(e2.getMessage());
        }
    }

    public final synchronized boolean a(String str, ContentValues contentValues) {
        try {
            return this.f.insertWithOnConflict(str, null, contentValues, 4) != -1;
        } catch (Exception e2) {
            new StringBuilder("SDK encountered unexpected error in DbStore.insert() method; ").append(e2.getMessage());
            return false;
        }
    }

    public final synchronized int a(String str, String str2, String[] strArr) {
        try {
        } catch (Exception e2) {
            new StringBuilder("SDK encountered an unexpected error in DbStore.delete() method; ").append(e2.getMessage());
            return -1;
        }
        return this.f.delete(str, str2, strArr);
    }

    public final synchronized int b(String str, ContentValues contentValues, String str2, String[] strArr) {
        try {
        } catch (Exception e2) {
            new StringBuilder("SDK encountered an unexpected error in DbStore.delete() method; ").append(e2.getMessage());
            return -1;
        }
        return this.f.updateWithOnConflict(str, contentValues, str2, strArr, 4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055 A[SYNTHETIC, Splitter:B:25:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005c A[SYNTHETIC, Splitter:B:30:0x005c] */
    public final synchronized List<ContentValues> a(String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            Cursor cursor = null;
            try {
                Cursor query = this.f.query(str, strArr, str2, strArr2, str3, str4, str5, str6);
                try {
                    if (query.moveToFirst()) {
                        do {
                            ContentValues contentValues = new ContentValues();
                            DatabaseUtils.cursorRowToContentValues(query, contentValues);
                            arrayList.add(contentValues);
                        } while (query.moveToNext());
                    }
                    query.close();
                    if (query != null) {
                        query.close();
                    }
                } catch (Exception e2) {
                    e = e2;
                    cursor = query;
                    try {
                        new StringBuilder("SDK encountered unexpected error in DbStore.getRows() method; ").append(e.getMessage());
                        if (cursor != null) {
                            cursor.close();
                        }
                        return arrayList;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                new StringBuilder("SDK encountered unexpected error in DbStore.getRows() method; ").append(e.getMessage());
                if (cursor != null) {
                }
                return arrayList;
            }
        }
        return arrayList;
    }

    public final synchronized int a(String str) {
        int i;
        try {
            StringBuilder sb = new StringBuilder("SELECT COUNT(*) FROM ");
            sb.append(str);
            sb.append(" ; ");
            Cursor rawQuery = this.f.rawQuery(sb.toString(), null);
            rawQuery.moveToFirst();
            i = rawQuery.getInt(0);
            rawQuery.close();
        } catch (Exception e2) {
            new StringBuilder("SDK encountered unexpected error in DbStore.getCount() method; ").append(e2.getMessage());
            return -1;
        }
        return i;
    }

    public final synchronized int b(String str, String str2, String[] strArr) {
        int i;
        try {
            StringBuilder sb = new StringBuilder("SELECT COUNT(*) FROM ");
            sb.append(str);
            sb.append(" WHERE ");
            sb.append(str2);
            sb.append(" ; ");
            Cursor rawQuery = this.f.rawQuery(sb.toString(), strArr);
            rawQuery.moveToFirst();
            i = rawQuery.getInt(0);
            rawQuery.close();
        } catch (Exception e2) {
            new StringBuilder("SDK encountered unexpected error in DbStore.getCount() method; ").append(e2.getMessage());
            return -1;
        }
        return i;
    }

    public final synchronized void a(String str, String str2) {
        try {
            StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
            sb.append(str);
            sb.append(str2);
            sb.append(";");
            this.f.execSQL(sb.toString());
        } catch (Exception e2) {
            new StringBuilder("SDK encountered unexpected error in DbStore.createTableIfNotExists() method; ").append(e2.getMessage());
        }
    }

    public final synchronized void b() {
        try {
            synchronized (d) {
                int i = e - 1;
                e = i;
                if (i == 0) {
                    this.f.close();
                    b = null;
                }
            }
        } catch (Exception e2) {
            new StringBuilder("SDK encountered unexpected error in DbStore.close() method; ").append(e2.getMessage());
        }
    }
}
