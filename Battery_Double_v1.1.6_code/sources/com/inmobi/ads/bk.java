package com.inmobi.ads;

import android.content.ContentValues;
import com.facebook.appevents.AppEventsConstants;
import com.inmobi.commons.core.d.b;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: PlacementDao */
public class bk {
    public static final String[] a = {"id", "placement_id", "tp_key", "last_accessed_ts", AppEventsConstants.EVENT_PARAM_AD_TYPE, "m10_context"};
    private static final String b = "bk";
    private static bk c;
    private static final Object d = new Object();

    public static bk a() {
        bk bkVar = c;
        if (bkVar == null) {
            synchronized (d) {
                bkVar = c;
                if (bkVar == null) {
                    bkVar = new bk();
                    c = bkVar;
                }
            }
        }
        return bkVar;
    }

    private bk() {
        b a2 = b.a();
        a2.a(IronSourceConstants.EVENTS_PLACEMENT_NAME, "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, placement_id INTEGER NOT NULL,tp_key TEXT,last_accessed_ts INTEGER NOT NULL,ad_type TEXT NOT NULL,m10_context TEXT NOT NULL,UNIQUE(placement_id,m10_context,tp_key))");
        a2.b();
    }

    public static int a(long j, String str) {
        b a2 = b.a();
        int a3 = a2.a(IronSourceConstants.EVENTS_PLACEMENT_NAME, "ad_type=? AND last_accessed_ts<?", new String[]{str, String.valueOf(System.currentTimeMillis() - (j * 1000))});
        StringBuilder sb = new StringBuilder("Deleted ");
        sb.append(a3);
        sb.append(" expired pids from cache");
        a2.b();
        return a3;
    }

    public final synchronized int a(List<bj> list, int i) {
        if (list.size() == 0) {
            return 0;
        }
        b a2 = b.a();
        for (int i2 = 0; i2 < list.size(); i2++) {
            bj bjVar = (bj) list.get(i2);
            String[] strArr = {String.valueOf(bjVar.a), bjVar.f.toString(), bjVar.b};
            String str = IronSourceConstants.EVENTS_PLACEMENT_NAME;
            ContentValues contentValues = new ContentValues();
            contentValues.put("placement_id", Long.valueOf(bjVar.a));
            contentValues.put("last_accessed_ts", Long.valueOf(System.currentTimeMillis()));
            contentValues.put("tp_key", bjVar.b);
            contentValues.put(AppEventsConstants.EVENT_PARAM_AD_TYPE, bjVar.e);
            contentValues.put("m10_context", bjVar.f.toString());
            a2.a(str, contentValues, "placement_id = ? AND m10_context = ? AND tp_key=?", strArr);
        }
        int a3 = a2.a(IronSourceConstants.EVENTS_PLACEMENT_NAME) - i;
        if (a3 > 0) {
            b bVar = a2;
            List a4 = bVar.a(IronSourceConstants.EVENTS_PLACEMENT_NAME, new String[]{"id"}, null, null, null, null, "last_accessed_ts ASC", String.valueOf(a3));
            String[] strArr2 = new String[a4.size()];
            for (int i3 = 0; i3 < a4.size(); i3++) {
                strArr2[i3] = String.valueOf(((ContentValues) a4.get(i3)).getAsInteger("id"));
            }
            String replace = Arrays.toString(strArr2).replace(RequestParameters.LEFT_BRACKETS, "(").replace(RequestParameters.RIGHT_BRACKETS, ")");
            String str2 = IronSourceConstants.EVENTS_PLACEMENT_NAME;
            StringBuilder sb = new StringBuilder("id IN ");
            sb.append(replace);
            a2.a(str2, sb.toString(), null);
        }
        a2.b();
        return a3;
    }

    public static List<bj> a(String str) {
        ArrayList arrayList = new ArrayList();
        b a2 = b.a();
        b bVar = a2;
        List<ContentValues> a3 = bVar.a(IronSourceConstants.EVENTS_PLACEMENT_NAME, a, "ad_type=? ", new String[]{str}, null, null, null, null);
        a2.b();
        for (ContentValues bjVar : a3) {
            arrayList.add(new bj(bjVar));
        }
        return arrayList;
    }
}
