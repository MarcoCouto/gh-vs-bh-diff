package com.inmobi.ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.inmobi.ads.t.a;

public class GifView extends ImageView implements a {
    private t a;
    private float b;
    private boolean c;
    private String d;

    public GifView(Context context) {
        this(context, null);
    }

    public GifView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = -1.0f;
        this.c = true;
        this.d = "unspecified";
        setLayerType(1, null);
    }

    public GifView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = -1.0f;
        this.c = true;
        this.d = "unspecified";
        setLayerType(1, null);
    }

    public void setPaused(boolean z) {
        this.a.a(z);
    }

    public void setGif(t tVar) {
        this.a = tVar;
        if (this.a != null) {
            this.a.a((a) this);
            this.a.a();
        }
        requestLayout();
    }

    public void setContentMode(String str) {
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.b = getScale();
        Drawable drawable = getDrawable();
        int i3 = 0;
        int i4 = 1;
        if (drawable != null) {
            i3 = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (i3 <= 0) {
                i3 = 1;
            }
            if (intrinsicHeight > 0) {
                i4 = intrinsicHeight;
            }
        } else if (this.a != null) {
            int b2 = this.a.b();
            int c2 = this.a.c();
            if (b2 <= 0) {
                b2 = 1;
            }
            if (c2 > 0) {
                i4 = c2;
            }
            i3 = b2;
        } else {
            i4 = 0;
        }
        int paddingTop = i4 + getPaddingTop() + getPaddingBottom();
        setMeasuredDimension(resolveSize(Math.max(i3 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i), resolveSize(Math.max(paddingTop, getSuggestedMinimumHeight()), i2));
    }

    private int getDensity() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (!(getContext() instanceof Activity)) {
            return PsExtractor.VIDEO_STREAM_MASK;
        }
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.densityDpi;
    }

    private float getScale() {
        this.b = ((float) getContext().getResources().getDisplayMetrics().densityDpi) / ((float) getDensity());
        if (this.b < 0.1f) {
            this.b = 0.1f;
        }
        if (this.b > 5.0f) {
            this.b = 5.0f;
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.c = getVisibility() == 0;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.a != null) {
            if (this.a.d()) {
                this.a.e();
                a(canvas);
                b();
                return;
            }
            a(canvas);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0080  */
    private void a(Canvas canvas) {
        char c2;
        float f;
        float f2;
        canvas.save();
        canvas.scale(this.b, this.b);
        float width = (float) getWidth();
        float height = (float) getHeight();
        float b2 = ((float) this.a.b()) * this.b;
        float c3 = ((float) this.a.c()) * this.b;
        String str = this.d;
        int hashCode = str.hashCode();
        if (hashCode != -1362001767) {
            if (hashCode == 727618043 && str.equals("aspectFill")) {
                c2 = 0;
                float f3 = 0.0f;
                switch (c2) {
                    case 0:
                        f = Math.max(width / b2, height / c3);
                        f3 = ((width - (b2 * f)) / 2.0f) / (this.b * f);
                        f2 = ((height - (c3 * f)) / 2.0f) / (this.b * f);
                        canvas.scale(f, f);
                        break;
                    case 1:
                        f = Math.min(width / b2, height / c3);
                        f3 = ((width - (b2 * f)) / 2.0f) / (this.b * f);
                        f2 = ((height - (c3 * f)) / 2.0f) / (this.b * f);
                        canvas.scale(f, f);
                        break;
                    default:
                        f = height / c3;
                        canvas.scale(width / b2, f);
                        f2 = 0.0f;
                        break;
                }
                float[] fArr = {f3, f2, f};
                this.a.a(canvas, fArr[0], fArr[1]);
                canvas.restore();
            }
        } else if (str.equals("aspectFit")) {
            c2 = 1;
            float f32 = 0.0f;
            switch (c2) {
                case 0:
                    break;
                case 1:
                    break;
            }
            float[] fArr2 = {f32, f2, f};
            this.a.a(canvas, fArr2[0], fArr2[1]);
            canvas.restore();
        }
        c2 = 65535;
        float f322 = 0.0f;
        switch (c2) {
            case 0:
                break;
            case 1:
                break;
        }
        float[] fArr22 = {f322, f2, f};
        this.a.a(canvas, fArr22[0], fArr22[1]);
        canvas.restore();
    }

    private void b() {
        if (this.c) {
            if (VERSION.SDK_INT >= 16) {
                postInvalidateOnAnimation();
                return;
            }
            invalidate();
        }
    }

    @SuppressLint({"NewApi"})
    public void onScreenStateChanged(int i) {
        super.onScreenStateChanged(i);
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        this.c = z;
        b();
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(view, i);
        this.c = i == 0;
        b();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        this.c = i == 0;
        b();
    }

    public final void a() {
        invalidate();
    }
}
