package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.inmobi.ads.c.l;
import com.inmobi.rendering.RenderView;
import com.integralads.avid.library.inmobi.session.AbstractAvidAdSession;
import com.integralads.avid.library.inmobi.session.AvidAdSessionManager;
import com.integralads.avid.library.inmobi.session.ExternalAvidAdSessionContext;
import java.lang.ref.WeakReference;

/* compiled from: IasTrackedHtmlAd */
public class v extends ca {
    private static final String d = "v";
    @NonNull
    private final WeakReference<Activity> e;
    @NonNull
    private final cb f;
    @NonNull
    private final AbstractAvidAdSession<WebView> g;
    private final boolean h;

    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0057  */
    @Nullable
    static AbstractAvidAdSession<WebView> a(@Nullable Context context, boolean z, @NonNull String str, @Nullable RenderView renderView) {
        char c;
        AbstractAvidAdSession<WebView> abstractAvidAdSession;
        ExternalAvidAdSessionContext externalAvidAdSessionContext = new ExternalAvidAdSessionContext("7.3.0", z);
        int hashCode = str.hashCode();
        if (hashCode != -284840886) {
            if (hashCode != 112202875) {
                if (hashCode == 1425678798 && str.equals("nonvideo")) {
                    c = 2;
                    switch (c) {
                        case 2:
                            abstractAvidAdSession = AvidAdSessionManager.startAvidDisplayAdSession(context, externalAvidAdSessionContext);
                            break;
                        case 3:
                            abstractAvidAdSession = AvidAdSessionManager.startAvidVideoAdSession(context, externalAvidAdSessionContext);
                            break;
                        default:
                            abstractAvidAdSession = null;
                            break;
                    }
                    if (!(abstractAvidAdSession == null || renderView == null)) {
                        if (!(context instanceof Activity)) {
                            abstractAvidAdSession.registerAdView(renderView, (Activity) context);
                        } else {
                            abstractAvidAdSession.registerAdView(renderView, null);
                        }
                    }
                    return abstractAvidAdSession;
                }
            } else if (str.equals("video")) {
                c = 3;
                switch (c) {
                    case 2:
                        break;
                    case 3:
                        break;
                }
                if (!(context instanceof Activity)) {
                }
                return abstractAvidAdSession;
            }
        } else if (str.equals("unknown")) {
            c = 1;
            switch (c) {
                case 2:
                    break;
                case 3:
                    break;
            }
            if (!(context instanceof Activity)) {
            }
            return abstractAvidAdSession;
        }
        c = 65535;
        switch (c) {
            case 2:
                break;
            case 3:
                break;
        }
        if (!(context instanceof Activity)) {
        }
        return abstractAvidAdSession;
    }

    public v(@NonNull AdContainer adContainer, @NonNull Activity activity, @NonNull cb cbVar, @NonNull AbstractAvidAdSession<WebView> abstractAvidAdSession, boolean z) {
        super(adContainer);
        this.e = new WeakReference<>(activity);
        this.f = cbVar;
        this.g = abstractAvidAdSession;
        this.h = z;
    }

    @Nullable
    public final View a() {
        return this.f.a();
    }

    @Nullable
    public final View a(View view, ViewGroup viewGroup, boolean z) {
        return this.f.a(view, viewGroup, z);
    }

    @Nullable
    public final View b() {
        return this.f.b();
    }

    @NonNull
    public final c c() {
        return this.f.c();
    }

    public final void a(@Nullable View... viewArr) {
        try {
            Activity activity = (Activity) this.e.get();
            l lVar = this.f.c().k;
            if (activity != null && lVar.j) {
                if (this.a instanceof ah) {
                    ah ahVar = (ah) this.a;
                    if (ahVar.s() != null) {
                        a(activity, (WebView) ahVar.s(), viewArr);
                    }
                } else {
                    View b = this.f.b();
                    if (b != null) {
                        a(activity, (WebView) b, viewArr);
                    }
                }
            }
        } catch (Exception e2) {
            new StringBuilder("Exception in startTrackingForImpression with message : ").append(e2.getMessage());
        } catch (Throwable th) {
            this.f.a(viewArr);
            throw th;
        }
        this.f.a(viewArr);
    }

    private void a(Activity activity, WebView webView, @Nullable View[] viewArr) {
        if (viewArr != null) {
            for (View registerFriendlyObstruction : viewArr) {
                this.g.registerFriendlyObstruction(registerFriendlyObstruction);
            }
        }
        this.g.registerAdView(webView, activity);
        if (this.h && this.g.getAvidDeferredAdSessionListener() != null) {
            this.g.getAvidDeferredAdSessionListener().recordReadyEvent();
        }
    }

    public final void a(int i) {
        this.f.a(i);
    }

    public final void a(Context context, int i) {
        this.f.a(context, i);
    }

    public final void e() {
        super.e();
        try {
            this.e.clear();
        } catch (Exception e2) {
            new StringBuilder("Exception in destroy with message : ").append(e2.getMessage());
        } catch (Throwable th) {
            this.f.e();
            throw th;
        }
        this.f.e();
    }

    public final void d() {
        View view;
        try {
            if (this.a instanceof ah) {
                view = ((ah) this.a).s();
            } else {
                view = (WebView) this.f.b();
            }
            this.g.unregisterAdView(view);
            this.g.endSession();
        } catch (Exception e2) {
            new StringBuilder("Exception in stopTrackingForImpression with message : ").append(e2.getMessage());
        } catch (Throwable th) {
            this.f.d();
            throw th;
        }
        this.f.d();
    }
}
