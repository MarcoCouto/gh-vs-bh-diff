package com.inmobi.ads;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.InMobiAdRequestStatus.StatusCode;
import com.inmobi.ads.a.d;
import com.inmobi.ads.d.a;
import com.inmobi.ads.i.b;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.c;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;
import java.util.Map;

/* compiled from: NativeAdUnit */
public final class aj extends i {
    /* access modifiers changed from: 0000 */
    public static final String z = "aj";
    WeakReference<View> A;
    boolean B = false;
    private boolean C;
    private a D;
    private int E = 0;

    public final String b() {
        return "native";
    }

    public final String c() {
        return null;
    }

    @NonNull
    public static aj a(@NonNull Context context, bj bjVar, b bVar, int i) {
        i iVar = (i) a.a.get(bjVar);
        aj ajVar = iVar instanceof aj ? (aj) iVar : null;
        if (ajVar == null || 1 != i) {
            if (ajVar == null) {
                new StringBuilder("Creating new adUnit for placement-ID : ").append(bjVar.a);
                ajVar = new aj(context, bjVar.a, bVar);
                if (i != 0) {
                    a.a.put(bjVar, ajVar);
                }
            } else {
                new StringBuilder("Found pre-fetching adUnit for placement-ID : ").append(bjVar.a);
                ajVar.a(context);
                a.a.remove(bjVar);
                ajVar.C = true;
            }
            ajVar.a(bVar);
            ajVar.a(bjVar.f);
            return ajVar;
        }
        throw new IllegalStateException("There's already a pre-loading going on for the same placementID");
    }

    public static aj a(Context context, bj bjVar, b bVar) {
        return new aj(context, bjVar.a, bVar);
    }

    private aj(@NonNull Context context, long j, b bVar) {
        super(context, j, bVar);
    }

    public final void a(Context context) {
        super.a(context);
        b(context);
    }

    @VisibleForTesting
    private void b(Context context) {
        AdContainer j = j();
        if (j instanceof ah) {
            ((ah) j).a(context);
        }
    }

    /* access modifiers changed from: 0000 */
    @UiThread
    public final void q() {
        try {
            if (p()) {
                c("IllegalState");
            } else {
                super.a(false);
            }
        } catch (Exception e) {
            Logger.a(InternalLogLevel.ERROR, "InMobi", "Unable to Prefetch ad; SDK encountered an unexpected error");
            new StringBuilder("Prefetch failed with unexpected error: ").append(e.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
        }
    }

    @UiThread
    public final void a(boolean z2) {
        try {
            if (p()) {
                c("IllegalState");
            } else {
                super.a(z2);
            }
        } catch (Exception e) {
            Logger.a(InternalLogLevel.ERROR, "InMobi", "Unable to Prefetch ad; SDK encountered an unexpected error");
            new StringBuilder("Prefetch failed with unexpected error: ").append(e.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
        }
    }

    /* access modifiers changed from: protected */
    public final void b(a aVar) {
        i().a(aVar);
    }

    /* access modifiers changed from: protected */
    public final void b(long j, boolean z2) {
        super.b(j, z2);
        boolean z3 = false;
        if (!z2) {
            if (j == this.d && (2 == this.a || 5 == this.a)) {
                this.a = 0;
                if (f() != null) {
                    f().a(new InMobiAdRequestStatus(StatusCode.AD_NO_LONGER_AVAILABLE));
                }
            }
        } else if (j == this.d && 2 == this.a) {
            b f = f();
            if (f != null) {
                if (this.D != null) {
                    if (this.D instanceof bc) {
                        bc bcVar = (bc) this.D;
                        d.a();
                        com.inmobi.ads.a.a b = d.b(bcVar.l);
                        if (b != null && b.a()) {
                            by byVar = new by(b.e, bcVar.m, bcVar.n, bcVar.h(), bcVar.i(), this.g.m);
                            this.j = byVar;
                        }
                    }
                    z3 = true;
                }
                if (!z3) {
                    f.a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
                } else if (a() != null) {
                    if (this.t) {
                        this.v = true;
                        H();
                        return;
                    }
                    I();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void c(long j, @NonNull a aVar) {
        super.c(j, aVar);
        this.D = aVar;
        b f = f();
        if (a(aVar)) {
            if (f != null) {
                f.a(true);
            } else {
                g();
            }
            if (this.r != 0 || aVar.j) {
                c(aVar);
            } else {
                a(true, (RenderView) null);
            }
            if (aVar.j) {
                this.t = true;
                G();
            }
        } else if (f != null) {
            f.a(false);
        } else {
            g();
        }
    }

    public final boolean a(a aVar) {
        if (super.a(aVar)) {
            return true;
        }
        b(aVar);
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final void I() {
        a(f(), this.h, new Runnable() {
            public final void run() {
                if (2 == aj.this.a) {
                    aj.this.a = 5;
                    AdContainer j = aj.this.j();
                    RenderView renderView = aj.this.u;
                    b f = aj.this.f();
                    if (j instanceof ah) {
                        ah ahVar = (ah) j;
                        ahVar.w = renderView;
                        ahVar.y = aj.this.r;
                        aj.this.D();
                        if (f != null) {
                            aj.z;
                            f.a();
                        }
                    } else {
                        if (f != null) {
                            aj.z;
                            f.a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
                        }
                    }
                }
            }
        }, Looper.getMainLooper());
    }

    public final void M() {
        try {
            super.t();
            this.h = null;
        } catch (Exception e) {
            Logger.a(InternalLogLevel.ERROR, "InMobi", "Could not destroy native ad; SDK encountered unexpected error");
            new StringBuilder("SDK encountered unexpected error in destroying native ad unit; ").append(e.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
        }
    }

    /* access modifiers changed from: protected */
    public final PlacementType d() {
        return PlacementType.PLACEMENT_TYPE_INLINE;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final Map<String, String> e() {
        Map<String, String> e = super.e();
        e.put("a-parentViewWidth", String.valueOf(c.a().a));
        e.put("a-productVersion", "NS-1.0.0-20160411");
        e.put("trackerType", "url_ping");
        return e;
    }

    public final void n() {
        if (!this.w) {
            b f = f();
            if (m()) {
                a("MissingDependency");
                if (f != null) {
                    f.a(new InMobiAdRequestStatus(StatusCode.MISSING_REQUIRED_DEPENDENCIES));
                }
            } else if (1 == this.a || 2 == this.a) {
                Logger.a(InternalLogLevel.ERROR, z, "An ad load is already in progress. Please wait for the load to complete before requesting for another ad");
                if (!this.C) {
                    a(new InMobiAdRequestStatus(StatusCode.REQUEST_PENDING), false);
                }
            } else {
                InternalLogLevel internalLogLevel = InternalLogLevel.DEBUG;
                String str = z;
                StringBuilder sb = new StringBuilder("Fetching a Native ad for placement id: ");
                sb.append(this.d);
                Logger.a(internalLogLevel, str, sb.toString());
                if (5 != this.a || h()) {
                    super.n();
                    return;
                }
                a(f, "VAR", "");
                a(f, "ARF", "");
                if (f != null) {
                    b(a());
                    f.a(true);
                    f.a();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final int r() {
        if (1 != this.a && 2 != this.a) {
            return super.r();
        }
        StringBuilder sb = new StringBuilder("An ad load is already in progress. Please wait for the load to complete before requesting for another ad for placement id: ");
        sb.append(this.d);
        Logger.a(InternalLogLevel.ERROR, "InMobi", sb.toString());
        this.s.post(new Runnable() {
            public final void run() {
                aj.this.a(new InMobiAdRequestStatus(StatusCode.REQUEST_PENDING), false);
            }
        });
        return 2;
    }

    public final boolean N() {
        return this.a == 5;
    }

    public final void F() {
        if (1 == this.a) {
            this.a = 9;
            if (!this.n) {
                this.C = false;
                n();
            } else if (this.q != null) {
                this.q.a(this);
            }
        }
    }

    public final void b(InMobiAdRequestStatus inMobiAdRequestStatus) {
        if (1 == this.a) {
            this.a = 3;
            b f = f();
            if (!this.n && f != null) {
                this.C = false;
                a(f, "VAR", "");
                a(f, "ARN", "");
                f.a(inMobiAdRequestStatus);
            } else if (this.q != null) {
                this.q.a(this, inMobiAdRequestStatus);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void b(b bVar) {
        if (this.a == 5) {
            this.a = 7;
        } else if (this.a == 7) {
            this.E++;
        }
        StringBuilder sb = new StringBuilder("Successfully displayed fullscreen for placement id: ");
        sb.append(this.d);
        Logger.a(InternalLogLevel.DEBUG, "InMobi", sb.toString());
        if (this.E == 0) {
            if (bVar != null) {
                bVar.d();
                return;
            }
            g();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void c(b bVar) {
        if (this.a == 7) {
            if (this.E > 0) {
                this.E--;
            } else {
                this.a = 5;
            }
        }
        StringBuilder sb = new StringBuilder("Successfully dismissed fullscreen for placement id: ");
        sb.append(this.d);
        Logger.a(InternalLogLevel.DEBUG, "InMobi", sb.toString());
        if (this.E == 0 && this.a == 5) {
            if (bVar != null) {
                bVar.e();
                return;
            }
            g();
        }
    }
}
