package com.inmobi.ads;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.inmobi.ads.InMobiAdRequest.MonetizationContext;
import com.inmobi.commons.core.d.b;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* compiled from: AdDao */
public class d {
    private static final String a = "d";
    private static d b;
    private static final Object c = new Object();
    private static final String[] d = {"id", "ad_content", TapjoyConstants.TJC_VIDEO_URL, "video_track_duration", TapjoyConstants.TJC_CLICK_URL, "video_trackers", "companion_ads", "web_vast", "preload_webView", "asset_urls", AppEventsConstants.EVENT_PARAM_AD_TYPE, "ad_size", "placement_id", "tp_key", "insertion_ts", "expiry_duration", "imp_id", "m10_context", "client_request_id", "bid", "bidInfo", "marked"};

    public static d a() {
        d dVar = b;
        if (dVar == null) {
            synchronized (c) {
                dVar = b;
                if (dVar == null) {
                    dVar = new d();
                    b = dVar;
                }
            }
        }
        return dVar;
    }

    private static String[] a(long j, MonetizationContext monetizationContext, String str) {
        return new String[]{String.valueOf(j), monetizationContext.a, str, "0"};
    }

    private static String[] e(long j, String str, MonetizationContext monetizationContext, String str2) {
        return new String[]{String.valueOf(j), str, monetizationContext.a, str2, "0"};
    }

    private d() {
        b a2 = b.a();
        a2.a("ad", "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, placement_id INTEGER NOT NULL, ad_content TEXT NOT NULL, ad_type TEXT NOT NULL, ad_size TEXT, asset_urls TEXT, video_url TEXT, video_track_duration TEXT, click_url TEXT, video_trackers TEXT, companion_ads TEXT, web_vast TEXT, preload_webView INTEGER DEFAULT 0, insertion_ts INTEGER NOT NULL, imp_id TEXT NOT NULL UNIQUE, m10_context TEXT NOT NULL, tp_key TEXT, expiry_duration INTEGER NOT NULL, client_request_id TEXT NOT NULL,bid INTEGER NOT NULL,bidInfo TEXT,marked INTEGER DEFAULT 0)");
        d();
        a2.b();
    }

    static List<a> a(String str, long j) {
        long j2;
        ArrayList arrayList = new ArrayList();
        b a2 = b.a();
        int i = 0;
        b bVar = a2;
        List<ContentValues> a3 = bVar.a("ad", d, "ad_type=?", new String[]{str}, null, null, null, null);
        if (a3.size() == 0) {
            a2.b();
            return arrayList;
        }
        for (ContentValues a4 : a3) {
            a a5 = C0044a.a(a4);
            if (a5.c() == -1) {
                j2 = (a5.e + TimeUnit.SECONDS.toMillis(j)) - System.currentTimeMillis();
            } else {
                j2 = a5.c() - System.currentTimeMillis();
            }
            if (j2 < 0) {
                i += a(a5.g);
                arrayList.add(a5);
            }
        }
        StringBuilder sb = new StringBuilder("Deleted ");
        sb.append(i);
        sb.append(" expired ads from cache of type: ");
        sb.append(str);
        a2.b();
        return arrayList;
    }

    static List<a> b() {
        ArrayList arrayList = new ArrayList();
        for (ContentValues a2 : b.a().a("ad", d, null, null, null, null, null, null)) {
            arrayList.add(C0044a.a(a2));
        }
        return arrayList;
    }

    private static void d() {
        b a2 = b.a();
        b bVar = a2;
        for (ContentValues contentValues : bVar.a("ad", d, "marked=?", new String[]{"1"}, null, null, null, null)) {
            contentValues.put("marked", "0");
            a2.a("ad", contentValues, "imp_id=?", new String[]{contentValues.getAsString("imp_id")});
        }
        a2.b();
    }

    static int a(long j, String str, MonetizationContext monetizationContext, String str2) {
        int i;
        b a2 = b.a();
        if (str == null || str.trim().length() == 0) {
            i = a2.b("ad", "placement_id=? AND m10_context=? AND tp_key=? AND marked=?", a(j, monetizationContext, str2));
        } else {
            i = a2.b("ad", "placement_id=? AND ad_size=? AND m10_context=? AND tp_key=? AND marked=?", e(j, str, monetizationContext, str2));
        }
        a2.b();
        return i;
    }

    /* access modifiers changed from: 0000 */
    public final synchronized a b(long j, String str, MonetizationContext monetizationContext, String str2) {
        a f;
        f = f(j, str, monetizationContext, str2);
        if (f != null) {
            b.a().a("ad", "id=?", new String[]{String.valueOf(f.a)});
        }
        return f;
    }

    private synchronized a f(long j, String str, MonetizationContext monetizationContext, String str2) {
        List list;
        b a2 = b.a();
        if (str == null || str.trim().length() == 0) {
            list = a2.a("ad", d, "placement_id=? AND m10_context=? AND tp_key=? AND marked=?", a(j, monetizationContext, str2), null, null, "insertion_ts", "1");
        } else {
            list = a2.a("ad", d, "placement_id=? AND ad_size=? AND m10_context=? AND tp_key=? AND marked=?", e(j, str, monetizationContext, str2), null, null, "insertion_ts", "1");
        }
        if (list.size() == 0) {
            return null;
        }
        return C0044a.a((ContentValues) list.get(0));
    }

    @NonNull
    public final synchronized List<a> c(long j, String str, MonetizationContext monetizationContext, String str2) {
        return a(j, str, monetizationContext, str2, false);
    }

    /* access modifiers changed from: 0000 */
    public final synchronized List<a> a(String str, String str2) {
        ArrayList arrayList;
        List<ContentValues> list;
        arrayList = new ArrayList();
        b a2 = b.a();
        if (str2 == null || str2.trim().length() == 0) {
            list = a2.a("ad", d, "video_url=?", new String[]{str}, null, null, "insertion_ts", null);
        } else {
            list = a2.a("ad", d, "video_url=? AND ad_size=?", new String[]{str, str2}, null, null, "insertion_ts", null);
        }
        for (ContentValues a3 : list) {
            arrayList.add(C0044a.a(a3));
        }
        return arrayList;
    }

    public final synchronized List<a> d(long j, String str, MonetizationContext monetizationContext, String str2) {
        return a(j, str, monetizationContext, str2, true);
    }

    private static List<a> a(long j, String str, MonetizationContext monetizationContext, String str2, boolean z) {
        List<ContentValues> list;
        ArrayList arrayList = new ArrayList();
        b a2 = b.a();
        if (str == null || str.trim().length() == 0) {
            list = a2.a("ad", d, "placement_id=? AND m10_context=? AND tp_key=? AND marked=?", a(j, monetizationContext, str2), null, null, z ? "bid" : "insertion_ts", null);
        } else {
            list = a2.a("ad", d, "placement_id=? AND ad_size=? AND m10_context=? AND tp_key=? AND marked=?", e(j, str, monetizationContext, str2), null, null, z ? "bid" : "insertion_ts", null);
        }
        for (ContentValues a3 : list) {
            arrayList.add(C0044a.a(a3));
        }
        return arrayList;
    }

    /* access modifiers changed from: 0000 */
    public final synchronized List<a> b(String str, String str2) {
        ArrayList arrayList;
        List<ContentValues> list;
        arrayList = new ArrayList();
        b a2 = b.a();
        if (str2 == null || str2.trim().length() == 0) {
            b bVar = a2;
            list = bVar.a("ad", d, "video_url=?", new String[]{str}, null, null, "insertion_ts", null);
        } else {
            b bVar2 = a2;
            list = bVar2.a("ad", d, "video_url=? AND ad_size=?", new String[]{str, str2}, null, null, "insertion_ts", null);
        }
        for (ContentValues contentValues : list) {
            a2.a("ad", "id=?", new String[]{String.valueOf(contentValues.getAsInteger("id").intValue())});
            arrayList.add(C0044a.a(contentValues));
        }
        return arrayList;
    }

    public static int a(String str) {
        b a2 = b.a();
        int a3 = a2.a("ad", "imp_id = ?", new String[]{String.valueOf(str)});
        a2.b();
        return a3;
    }

    public static void b(String str) {
        b a2 = b.a();
        a c2 = c(str);
        if (c2 != null) {
            ContentValues a3 = c2.a();
            a3.put("marked", "1");
            a2.b("ad", a3, "imp_id=?", new String[]{str});
        }
    }

    @Nullable
    public static a c(String str) {
        List a2 = b.a().a("ad", d, "imp_id=?", new String[]{str}, null, null, null, "1");
        if (a2.size() == 0) {
            return null;
        }
        return C0044a.a((ContentValues) a2.get(0));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0016, code lost:
        return;
     */
    public final synchronized void a(List<a> list, long j, int i, String str, MonetizationContext monetizationContext, String str2, @Nullable String str3) {
        long j2 = j;
        MonetizationContext monetizationContext2 = monetizationContext;
        String str4 = str2;
        synchronized (this) {
            boolean isEmpty = TextUtils.isEmpty(str3);
            if ((!isEmpty || i != 0) && list.size() != 0) {
                b a2 = b.a();
                for (a aVar : list) {
                    aVar.e = System.currentTimeMillis();
                    ContentValues a3 = aVar.a();
                    a3.put("tp_key", str4);
                    a2.a("ad", a3);
                }
                if (!isEmpty) {
                    b(str3);
                }
                int a4 = a(j2, null, monetizationContext2, str4) - i;
                if (a4 > 0) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("type", str);
                    hashMap.put("count", Integer.valueOf(a4));
                    com.inmobi.commons.core.e.b.a();
                    com.inmobi.commons.core.e.b.a("ads", "DbSpaceOverflow", hashMap);
                    String[] strArr = {"id"};
                    String valueOf = String.valueOf(a4);
                    List a5 = a2.a("ad", strArr, "placement_id=? AND m10_context=? AND tp_key=? AND marked=?", a(j2, monetizationContext2, str4), null, null, "insertion_ts ASC", valueOf);
                    String[] strArr2 = new String[a5.size()];
                    for (int i2 = 0; i2 < a5.size(); i2++) {
                        strArr2[i2] = String.valueOf(((ContentValues) a5.get(i2)).getAsInteger("id"));
                    }
                    String replace = Arrays.toString(strArr2).replace(RequestParameters.LEFT_BRACKETS, "(").replace(RequestParameters.RIGHT_BRACKETS, ")");
                    StringBuilder sb = new StringBuilder("id IN ");
                    sb.append(replace);
                    a2.a("ad", sb.toString(), null);
                }
                a2.b();
            }
        }
    }

    public static void c() {
        b a2 = b.a();
        a2.a("ad", null, null);
        a2.b();
    }

    public static int a(a aVar) {
        return a(aVar.g);
    }
}
