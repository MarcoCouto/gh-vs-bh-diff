package com.inmobi.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.RecyclerView;
import com.facebook.share.internal.ShareConstants;
import com.inmobi.a.o;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.InMobiAdRequest.MonetizationContext;
import com.inmobi.ads.InMobiAdRequestStatus.StatusCode;
import com.inmobi.commons.core.configs.g;
import com.inmobi.commons.core.configs.h;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.rendering.RenderView;
import com.integralads.avid.library.inmobi.session.AbstractAvidAdSession;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.squareup.picasso.Picasso;
import com.tapjoy.TJAdUnitConstants.String;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@UiThread
/* compiled from: AdUnit */
public abstract class i implements com.inmobi.ads.bm.a, com.inmobi.ads.h.a, com.inmobi.commons.core.configs.b.c, com.inmobi.rendering.RenderView.a, com.inmobi.rendering.a {
    /* access modifiers changed from: private */
    public static final String z = "i";
    private WeakReference<Context> A;
    /* access modifiers changed from: private */
    public boolean B;
    /* access modifiers changed from: private */
    @Nullable
    public h C;
    private long D;
    private long E;
    private WeakReference<b> F;
    /* access modifiers changed from: private */
    @Nullable
    public RenderView G;
    private bo H;
    /* access modifiers changed from: private */
    public long I;
    private long J = 0;
    @NonNull
    private a K;
    private Runnable L;
    /* access modifiers changed from: private */
    public Set<br> M;
    private MonetizationContext N;
    /* access modifiers changed from: private */
    public bm O;
    /* access modifiers changed from: private */
    public boolean P;
    private com.inmobi.ads.e.a Q;
    /* access modifiers changed from: private */
    @Nullable
    public com.inmobi.ads.c.a R;
    /* access modifiers changed from: private */
    public com.inmobi.rendering.RenderView.a S = new com.inmobi.rendering.RenderView.a() {
        public final void E() {
        }

        public final void a(HashMap<Object, Object> hashMap) {
        }

        public final void b(RenderView renderView) {
        }

        public final void b(String str, Map<String, Object> map) {
        }

        public final void b(HashMap<Object, Object> hashMap) {
        }

        public final void c(RenderView renderView) {
        }

        public final void d(RenderView renderView) {
        }

        public final void y() {
        }

        public final void z() {
        }

        public final void u() {
            i.this.s.post(new Runnable() {
                public final void run() {
                    if (2 == i.this.a) {
                        i.this.P = true;
                        i.this.H();
                    }
                }
            });
        }

        public final void w() {
            i.this.s.post(new Runnable() {
                public final void run() {
                    if (i.this.k != null) {
                        i.this.i().a(i.this.k);
                    }
                    i.this.a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR), false);
                }
            });
        }

        public final void a(RenderView renderView) {
            i.this.s.post(new Runnable() {
                public final void run() {
                    if (2 == i.this.a) {
                        i.this.J();
                    }
                }
            });
        }
    };
    int a;
    final JSONObject b = new JSONObject();
    final boolean c = false;
    public long d;
    public String e;
    public Map<String, String> f;
    /* access modifiers changed from: 0000 */
    public c g;
    String h;
    JSONObject i;
    by j;
    /* access modifiers changed from: 0000 */
    public String k;
    public String l;
    String m;
    public boolean n = false;
    ah o;
    ExecutorService p;
    public e q;
    int r;
    Handler s;
    boolean t;
    /* access modifiers changed from: 0000 */
    public RenderView u;
    boolean v;
    boolean w = false;
    /* access modifiers changed from: 0000 */
    public String x;
    boolean y;

    /* compiled from: AdUnit */
    static final class a extends Handler {
        private WeakReference<i> a;

        a(i iVar) {
            super(Looper.getMainLooper());
            this.a = new WeakReference<>(iVar);
        }

        public final void handleMessage(Message message) {
            i iVar = this.a == null ? null : (i) this.a.get();
            if (iVar != null) {
                Bundle data = message.getData();
                long j = data.getLong(Constants.PLACEMENT_ID);
                int i = message.what;
                switch (i) {
                    case 1:
                        iVar.a(j, data.getBoolean("adAvailable"), (a) message.obj);
                        return;
                    case 2:
                        iVar.c(j, (a) message.obj);
                        return;
                    case 3:
                        return;
                    case 4:
                        iVar.b(j, data.getBoolean("assetAvailable"));
                        return;
                    default:
                        switch (i) {
                            case 11:
                                iVar.v();
                                return;
                            case 12:
                                iVar.x();
                                return;
                            case 13:
                                iVar.b((InMobiAdRequestStatus) message.obj);
                                return;
                            case 14:
                                iVar.F();
                                return;
                            default:
                                return;
                        }
                }
            }
        }
    }

    /* compiled from: AdUnit */
    public static abstract class b {
        public void a() {
        }

        public void a(InMobiAdRequestStatus inMobiAdRequestStatus) {
        }

        /* access modifiers changed from: 0000 */
        public void a(i iVar) {
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull Map<Object, Object> map) {
        }

        public void a(boolean z) {
        }

        /* access modifiers changed from: 0000 */
        public void a(byte[] bArr) {
        }

        /* access modifiers changed from: 0000 */
        public void b() {
        }

        /* access modifiers changed from: 0000 */
        public void b(InMobiAdRequestStatus inMobiAdRequestStatus) {
        }

        /* access modifiers changed from: 0000 */
        public void b(@NonNull Map<Object, Object> map) {
        }

        /* access modifiers changed from: 0000 */
        public void b(boolean z) {
        }

        /* access modifiers changed from: 0000 */
        public void c() {
        }

        /* access modifiers changed from: 0000 */
        public void d() {
        }

        /* access modifiers changed from: 0000 */
        public void e() {
        }

        /* access modifiers changed from: 0000 */
        public void f() {
        }

        /* access modifiers changed from: 0000 */
        public void g() {
        }

        /* access modifiers changed from: 0000 */
        public void h() {
        }

        public boolean i() {
            return true;
        }

        /* access modifiers changed from: 0000 */
        public void j() {
        }
    }

    /* compiled from: AdUnit */
    public static class c {
        public static Map<String, Object> a(JSONArray jSONArray) {
            HashMap hashMap = new HashMap();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    arrayList.add(jSONArray.getString(i));
                } catch (JSONException e) {
                    i.z;
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                }
            }
            hashMap.put("trackerUrls", arrayList);
            return hashMap;
        }
    }

    /* compiled from: AdUnit */
    static class d {
        @NonNull
        static HashMap<String, String> a(@NonNull String str, @NonNull String str2, JSONArray jSONArray, JSONArray jSONArray2, JSONObject jSONObject) {
            HashMap<String, String> hashMap = new HashMap<>();
            int i = 0;
            if (jSONArray != null) {
                try {
                    int length = jSONArray.length();
                    int i2 = 0;
                    while (i2 < length) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        int i3 = i2 + 1;
                        sb.append(i3);
                        hashMap.put(sb.toString(), jSONArray.getString(i2));
                        i2 = i3;
                    }
                } catch (Exception e) {
                    i.z;
                    new StringBuilder("Exception while parsing map details for Moat : ").append(e.getMessage());
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                }
            }
            if (jSONArray2 != null) {
                int length2 = jSONArray2.length();
                while (i < length2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    int i4 = i + 1;
                    sb2.append(i4);
                    hashMap.put(sb2.toString(), jSONArray2.getString(i));
                    i = i4;
                }
            }
            if (jSONObject != null) {
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str3 = (String) keys.next();
                    hashMap.put(str3, jSONObject.optString(str3));
                }
            }
            return hashMap;
        }

        @Nullable
        static Map<String, Object> a(@NonNull JSONArray jSONArray) {
            JSONObject jSONObject;
            try {
                int length = jSONArray.length();
                int i = 0;
                while (true) {
                    if (i >= length) {
                        jSONObject = null;
                        break;
                    }
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    if (jSONObject2.has("moat")) {
                        jSONObject = jSONObject2.getJSONObject("moat");
                        break;
                    }
                    i++;
                }
                if (jSONObject == null) {
                    return null;
                }
                HashMap hashMap = new HashMap();
                hashMap.put(String.ENABLED, Boolean.valueOf(jSONObject.getBoolean(String.ENABLED)));
                hashMap.put("instrumentVideo", Boolean.valueOf(jSONObject.optBoolean("instrumentVideo", false)));
                hashMap.put("partnerCode", jSONObject.optString("partnerCode", null));
                hashMap.put("clientLevels", jSONObject.optJSONArray("clientLevels"));
                hashMap.put("clientSlicers", jSONObject.optJSONArray("clientSlicers"));
                hashMap.put("zMoatExtras", jSONObject.optJSONObject("zMoatExtras"));
                return hashMap;
            } catch (JSONException e) {
                i.z;
                new StringBuilder("Exception while parsing MoatParams from response : ").append(e.getMessage());
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                return null;
            }
        }
    }

    /* compiled from: AdUnit */
    public interface e {
        void a(@NonNull i iVar);

        void a(@NonNull i iVar, @NonNull InMobiAdRequestStatus inMobiAdRequestStatus);
    }

    public final void E() {
    }

    /* access modifiers changed from: 0000 */
    public void I() {
    }

    /* access modifiers changed from: 0000 */
    public void J() {
    }

    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract void b(a aVar);

    /* access modifiers changed from: 0000 */
    public void b(b bVar) {
    }

    public abstract String c();

    /* access modifiers changed from: 0000 */
    public void c(b bVar) {
    }

    /* access modifiers changed from: protected */
    public abstract PlacementType d();

    /* access modifiers changed from: protected */
    public void v() {
    }

    public final void a(com.inmobi.commons.core.configs.a aVar) {
        this.g = (c) aVar;
        i().d = this.g.a(b());
        if (this.O != null) {
            this.O.b = this.g.a(b());
        }
        com.inmobi.commons.core.e.b.a().a("ads", this.g.l);
    }

    public i(Context context, long j2, b bVar) {
        this.A = new WeakReference<>(context);
        this.d = j2;
        this.F = new WeakReference<>(bVar);
        this.Q = new com.inmobi.ads.e.b(com.inmobi.b.a.a());
        this.m = "unknown";
        this.g = new c();
        com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) new g(), (com.inmobi.commons.core.configs.b.c) null);
        com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) this.g, (com.inmobi.commons.core.configs.b.c) this);
        this.p = Executors.newSingleThreadExecutor();
        this.p.submit(new Runnable() {
            public final void run() {
                i.this.C = new h(i.this, i.this.g.a(i.this.b()), i.this.b(false));
            }
        });
        this.K = new a(this);
        this.H = new bo(this);
        this.M = new HashSet();
        this.r = -1;
        this.L = new Runnable() {
            public final void run() {
                int r = i.this.r();
                switch (r) {
                    case -2:
                    case -1:
                    case 0:
                    case 1:
                    case 2:
                        break;
                    default:
                        StringBuilder sb = new StringBuilder("Unknown return value (");
                        sb.append(r);
                        sb.append(") from #doAdLoadWork()");
                        break;
                }
                i.z;
            }
        };
        com.inmobi.commons.core.e.b.a().a("ads", this.g.l);
        this.s = new Handler(Looper.getMainLooper());
        this.t = false;
        this.x = "";
        this.i = this.b;
        this.B = false;
        this.a = 0;
    }

    @Nullable
    public final Context a() {
        if (this.A == null) {
            return null;
        }
        return (Context) this.A.get();
    }

    public void a(Context context) {
        this.A = new WeakReference<>(context);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Map<String, String> e() {
        HashMap hashMap = new HashMap();
        hashMap.put("preload-request", this.n ? "1" : "0");
        return hashMap;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final b f() {
        b bVar = (b) this.F.get();
        if (bVar == null) {
            g();
        }
        return bVar;
    }

    /* access modifiers changed from: 0000 */
    public final void g() {
        Logger.a(InternalLogLevel.DEBUG, "InMobi", "Listener was garbage collected. Unable to give callback");
        d("ListenerNotFound");
    }

    /* access modifiers changed from: 0000 */
    public final void a(b bVar) {
        this.F = new WeakReference<>(bVar);
    }

    public final boolean h() {
        if (1 == this.a) {
            return false;
        }
        return this.E == -1 ? this.D != 0 && System.currentTimeMillis() - this.D > TimeUnit.SECONDS.toMillis(this.g.a(b()).d) : this.D != 0 && System.currentTimeMillis() > this.E;
    }

    @NonNull
    public final h i() {
        if (this.C == null) {
            this.C = new h(this, this.g.a(b()), b(false));
        }
        return this.C;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public RenderView k() {
        return this.G;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0053 A[Catch:{ JSONException -> 0x021e, IllegalArgumentException -> 0x020e }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0056 A[Catch:{ JSONException -> 0x021e, IllegalArgumentException -> 0x020e }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0059 A[Catch:{ JSONException -> 0x021e, IllegalArgumentException -> 0x020e }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0078 A[Catch:{ JSONException -> 0x021e, IllegalArgumentException -> 0x020e }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0079 A[Catch:{ JSONException -> 0x021e, IllegalArgumentException -> 0x020e }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0181 A[Catch:{ JSONException -> 0x020c, IllegalArgumentException -> 0x020a }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0184 A[Catch:{ JSONException -> 0x020c, IllegalArgumentException -> 0x020a }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0187 A[Catch:{ JSONException -> 0x020c, IllegalArgumentException -> 0x020a }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0194 A[Catch:{ JSONException -> 0x020c, IllegalArgumentException -> 0x020a }] */
    public boolean a(a aVar) {
        boolean z2;
        JSONException e2;
        IllegalArgumentException e3;
        String str;
        String str2;
        char c2;
        try {
            JSONObject jSONObject = new JSONObject(aVar.c);
            this.D = aVar.e;
            this.E = aVar.c();
            this.k = aVar.g;
            this.l = aVar.h;
            String optString = jSONObject.optString("markupType");
            char c3 = 2;
            if (optString != null) {
                if (optString.length() != 0) {
                    int hashCode = optString.hashCode();
                    if (hashCode != -1084172778) {
                        if (hashCode == 3213227) {
                            if (optString.equals(String.HTML)) {
                                c2 = 1;
                                switch (c2) {
                                    case 1:
                                        str = String.HTML;
                                        break;
                                    case 2:
                                        str = "inmobiJson";
                                        break;
                                    default:
                                        str = "unknown";
                                        break;
                                }
                                this.m = str;
                                this.B = jSONObject.optBoolean("allowAutoRedirection", false);
                                this.i = aVar.b();
                                if (!"unknown".equals(this.m)) {
                                    return false;
                                }
                                if ("inmobiJson".equals(this.m)) {
                                    this.h = jSONObject.getJSONObject("pubContent").toString();
                                } else {
                                    this.h = jSONObject.getString("pubContent").trim();
                                }
                                if (this.h == null || this.h.length() == 0 || "unknown".equals(this.m)) {
                                    z2 = false;
                                } else {
                                    this.h = this.h.replace("@__imm_aft@", String.valueOf(System.currentTimeMillis() - this.I));
                                    z2 = true;
                                }
                                try {
                                    this.x = jSONObject.optString(RequestParameters.CREATIVE_ID, "");
                                    if (this.M.isEmpty()) {
                                        if (jSONObject.has("viewability")) {
                                            br brVar = new br(1);
                                            brVar.b = d.a(jSONObject.getJSONArray("viewability"));
                                            this.M.add(brVar);
                                            Context a2 = a();
                                            if (a2 != null && (a2 instanceof Activity)) {
                                                z.a(((Activity) a2).getApplication());
                                            }
                                            if (brVar.b != null) {
                                                new StringBuilder("Read out Moat params: ").append(brVar.b.toString());
                                            }
                                        }
                                        if (jSONObject.has("metaInfo")) {
                                            JSONObject jSONObject2 = jSONObject.getJSONObject("metaInfo");
                                            String str3 = "unknown";
                                            if (jSONObject2.has("creativeType")) {
                                                str3 = jSONObject2.getString("creativeType");
                                            }
                                            if (jSONObject2.has("iasEnabled") && jSONObject2.getBoolean("iasEnabled")) {
                                                br brVar2 = new br(3);
                                                HashMap hashMap = new HashMap();
                                                int hashCode2 = str3.hashCode();
                                                if (hashCode2 != 112202875) {
                                                    if (hashCode2 == 1425678798) {
                                                        if (str3.equals("nonvideo")) {
                                                            c3 = 1;
                                                            switch (c3) {
                                                                case 1:
                                                                    str2 = "nonvideo";
                                                                    break;
                                                                case 2:
                                                                    str2 = "video";
                                                                    break;
                                                                default:
                                                                    str2 = "unknown";
                                                                    break;
                                                            }
                                                            hashMap.put("creativeType", str2);
                                                            brVar2.b = hashMap;
                                                            if (brVar2.b != null) {
                                                                new StringBuilder("Read out IAS params: ").append(brVar2.b.toString());
                                                            }
                                                            this.M.add(brVar2);
                                                        }
                                                    }
                                                } else if (str3.equals("video")) {
                                                    switch (c3) {
                                                        case 1:
                                                            break;
                                                        case 2:
                                                            break;
                                                    }
                                                    hashMap.put("creativeType", str2);
                                                    brVar2.b = hashMap;
                                                    if (brVar2.b != null) {
                                                    }
                                                    this.M.add(brVar2);
                                                }
                                                c3 = 65535;
                                                switch (c3) {
                                                    case 1:
                                                        break;
                                                    case 2:
                                                        break;
                                                }
                                                hashMap.put("creativeType", str2);
                                                brVar2.b = hashMap;
                                                if (brVar2.b != null) {
                                                }
                                                this.M.add(brVar2);
                                            }
                                            if (jSONObject2.has("adMasterSDKInfo") && this.g.k.k.a) {
                                                JSONObject jSONObject3 = jSONObject2.getJSONObject("adMasterSDKInfo");
                                                if (jSONObject3.has(String.ENABLED) && jSONObject3.getBoolean(String.ENABLED)) {
                                                    br brVar3 = new br(6);
                                                    brVar3.b = c.a(jSONObject3.getJSONArray("trackerUrls"));
                                                    if (a() != null) {
                                                        a.a;
                                                    }
                                                    this.M.add(brVar3);
                                                }
                                            }
                                        }
                                        if (jSONObject.has("tracking") && "web".equals(jSONObject.getString("tracking"))) {
                                            this.r = 0;
                                        }
                                    }
                                } catch (JSONException e4) {
                                    e2 = e4;
                                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                                    return z2;
                                } catch (IllegalArgumentException e5) {
                                    e3 = e5;
                                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
                                    return z2;
                                }
                                return z2;
                            }
                        }
                    } else if (optString.equals("inmobiJson")) {
                        c2 = 2;
                        switch (c2) {
                            case 1:
                                break;
                            case 2:
                                break;
                        }
                        this.m = str;
                        this.B = jSONObject.optBoolean("allowAutoRedirection", false);
                        this.i = aVar.b();
                        if (!"unknown".equals(this.m)) {
                        }
                    }
                    c2 = 65535;
                    switch (c2) {
                        case 1:
                            break;
                        case 2:
                            break;
                    }
                    this.m = str;
                    this.B = jSONObject.optBoolean("allowAutoRedirection", false);
                    this.i = aVar.b();
                    if (!"unknown".equals(this.m)) {
                    }
                }
            }
            str = String.HTML;
            this.m = str;
            this.B = jSONObject.optBoolean("allowAutoRedirection", false);
            this.i = aVar.b();
            if (!"unknown".equals(this.m)) {
            }
        } catch (JSONException e6) {
            e2 = e6;
            z2 = false;
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return z2;
        } catch (IllegalArgumentException e7) {
            e3 = e7;
            z2 = false;
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
            return z2;
        }
    }

    /* access modifiers changed from: protected */
    public void b(long j2, boolean z2) {
        StringBuilder sb = new StringBuilder("Asset availability changed (");
        sb.append(z2);
        sb.append(") for placement ID (");
        sb.append(j2);
        sb.append(")");
    }

    /* access modifiers changed from: protected */
    @UiThread
    public void a(long j2, boolean z2, @NonNull a aVar) {
        if (j2 == this.d && 1 == this.a && z2) {
            this.D = aVar.e;
            this.E = aVar.c();
        }
    }

    /* access modifiers changed from: protected */
    @UiThread
    public void c(long j2, @NonNull a aVar) {
        if (j2 == this.d && this.a == 1) {
            if (a(aVar)) {
                a(f(), "ARF", "");
                this.J = SystemClock.elapsedRealtime();
                this.a = 2;
                return;
            }
            b("ParsingFailed");
            a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR), true);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(InMobiAdRequestStatus inMobiAdRequestStatus) {
        if (StatusCode.NO_FILL.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("NoFill");
        } else if (StatusCode.SERVER_ERROR.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("ServerError");
        } else if (StatusCode.NETWORK_UNREACHABLE.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("NetworkUnreachable");
        } else if (StatusCode.AD_ACTIVE.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("AdActive");
        } else if (StatusCode.REQUEST_PENDING.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("RequestPending");
        } else if (StatusCode.REQUEST_INVALID.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("RequestInvalid");
        } else if (StatusCode.REQUEST_TIMED_OUT.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("RequestTimedOut");
        } else if (StatusCode.EARLY_REFRESH_REQUEST.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("EarlyRefreshRequest");
        } else if (StatusCode.INTERNAL_ERROR.equals(inMobiAdRequestStatus.getStatusCode())) {
            b("InternalError");
        } else {
            if (StatusCode.MONETIZATION_DISABLED.equals(inMobiAdRequestStatus.getStatusCode())) {
                b("MonetizationDisabled");
            }
        }
    }

    public void a(MonetizationContext monetizationContext) {
        this.N = monetizationContext;
    }

    public MonetizationContext l() {
        return this.N;
    }

    static boolean m() {
        try {
            RecyclerView.class.getName();
            Picasso.class.getName();
            return false;
        } catch (NoClassDefFoundError unused) {
            return true;
        }
    }

    @UiThread
    public void n() {
        d("AdLoadRequested");
        if (!com.inmobi.commons.core.utilities.d.a()) {
            a(new InMobiAdRequestStatus(StatusCode.NETWORK_UNREACHABLE), true);
        } else {
            this.p.execute(this.L);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void o() {
        final b f2 = f();
        final long currentTimeMillis = System.currentTimeMillis();
        boolean z2 = true;
        if (com.inmobi.commons.core.utilities.d.a()) {
            switch (this.a) {
                case 1:
                case 2:
                case 4:
                    if (f2 != null) {
                        f2.b(new InMobiAdRequestStatus(StatusCode.GET_SIGNALS_CALLED_WHILE_LOADING));
                    }
                    a(f2, "ART", "LoadInProgress");
                    a("AdGetSignalsFailed", currentTimeMillis);
                    break;
                case 6:
                case 7:
                case 8:
                    if (f2 != null) {
                        f2.b(new InMobiAdRequestStatus(StatusCode.AD_ACTIVE));
                    }
                    a(f2, "ART", "ReloadNotPermitted");
                    a("AdGetSignalsFailed", currentTimeMillis);
                    break;
                case 10:
                    if (f2 != null) {
                        f2.b(new InMobiAdRequestStatus(StatusCode.FETCHING_SIGNALS_STATE_ERROR));
                    }
                    a(f2, "ART", "SignalsFetchInProgress");
                    a("AdGetSignalsFailed", currentTimeMillis);
                    break;
                default:
                    z2 = false;
                    break;
            }
        } else {
            if (f2 != null) {
                f2.b(new InMobiAdRequestStatus(StatusCode.NETWORK_UNREACHABLE));
            }
            a(f2, "ART", "NetworkNotAvailable");
            a("AdGetSignalsFailed", currentTimeMillis);
        }
        if (!z2) {
            d("AdGetSignalsRequested");
            this.p.execute(new Runnable() {
                public final void run() {
                    i.this.a = 10;
                    String a2 = com.inmobi.ads.d.a.a(i.this.f);
                    if (i.this.R == null) {
                        i.this.R = new com.inmobi.ads.c.a(i.this, a2);
                    } else {
                        i.this.R.b = a2;
                    }
                    if (f2 != null) {
                        try {
                            byte[] a3 = i.this.R.a();
                            if (a3 == null) {
                                i.this.a = 3;
                                f2.b(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
                                i.this.a(i.this.f(), "ART", "RequestCreationFailed");
                                i.this.a("AdGetSignalsFailed", currentTimeMillis);
                                return;
                            }
                            f2.a(a3);
                            i.this.a = 11;
                            i.this.a(i.this.f(), "VAR", "");
                            i.this.a("AdGetSignalsSucceeded", currentTimeMillis);
                        } catch (com.inmobi.ads.b.b unused) {
                            i.this.a = 3;
                            f2.b(new InMobiAdRequestStatus(StatusCode.GDPR_COMPLIANCE_ENFORCED));
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(@NonNull String str, long j2) {
        HashMap hashMap = new HashMap();
        hashMap.put("latency", Long.valueOf(System.currentTimeMillis() - j2));
        a(str, (Map<String, Object>) hashMap);
    }

    @UiThread
    public void a(final boolean z2) {
        d("AdPrefetchRequested");
        this.a = 1;
        this.p.execute(new Runnable() {
            public final void run() {
                try {
                    if (!com.inmobi.commons.core.utilities.d.a()) {
                        i.this.b(i.this.d, new InMobiAdRequestStatus(StatusCode.NETWORK_UNREACHABLE));
                        return;
                    }
                    o.a().e();
                    i.L();
                    h hVar = new h();
                    com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) hVar, (com.inmobi.commons.core.configs.b.c) null);
                    if (!hVar.g) {
                        i.this.I = System.currentTimeMillis();
                        try {
                            if (i.this.O == null) {
                                i.this.O = new bm(i.this, i.this.g.a(i.this.b()));
                            }
                            i.this.l = i.this.O.a(i.this.b(false), z2, i.this.g.c);
                        } catch (com.inmobi.ads.b.a e) {
                            i.z;
                            e.getMessage();
                            if (!i.this.O.a) {
                                i.this.b(i.this.d, new InMobiAdRequestStatus(StatusCode.EARLY_REFRESH_REQUEST));
                            }
                        }
                    }
                } catch (Exception e2) {
                    Logger.a(InternalLogLevel.ERROR, "InMobi", "Unable to Prefetch ad; SDK encountered an unexpected error");
                    i.z;
                    new StringBuilder("Prefetch failed with unexpected error: ").append(e2.getMessage());
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @UiThread
    public void q() {
        a(false);
    }

    private void a(@NonNull final String str, final WeakReference<b> weakReference) {
        this.s.post(new Runnable() {
            public final void run() {
                i.this.a = 3;
                i.this.b(str);
                if (i.this.w) {
                    i.z;
                    return;
                }
                b bVar = (b) weakReference.get();
                if (bVar == null) {
                    i.this.g();
                } else if ("int".equals(i.this.b())) {
                    i.this.a(bVar, "AVFB", "");
                    bVar.b();
                } else {
                    bVar.a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void s() {
        AdContainer j2 = j();
        if (j2 != null) {
            j2.a(2, null);
        }
    }

    @NonNull
    public final f b(boolean z2) {
        String str = this.g.a;
        long j2 = this.d;
        com.inmobi.commons.core.utilities.uid.d dVar = new com.inmobi.commons.core.utilities.uid.d(this.g.p.a);
        com.inmobi.ads.a.d.a();
        f fVar = new f(str, j2, dVar, com.inmobi.ads.a.d.c(), z2);
        fVar.f = this.e;
        fVar.g = this.f;
        fVar.e = b();
        fVar.b = "sdkJson";
        fVar.d = this.g.a(b()).b;
        fVar.h = e();
        fVar.c = c();
        fVar.q = this.g.e * 1000;
        fVar.r = this.g.e * 1000;
        fVar.j = this.N;
        fVar.x = M();
        return fVar;
    }

    @UiThread
    public void t() {
        if (!this.w) {
            this.w = true;
            this.k = null;
            this.D = 0;
            this.E = -1;
            this.M.clear();
            AdContainer j2 = j();
            if (j2 != null) {
                j2.destroy();
            }
            this.a = 0;
            this.m = "unknown";
            this.P = false;
            this.u = null;
            this.t = false;
            this.v = false;
            this.x = "";
            this.i = this.b;
            this.B = false;
        }
    }

    /* access modifiers changed from: protected */
    public void x() {
        b("RenderFailed");
    }

    /* access modifiers changed from: 0000 */
    public final void A() {
        this.H.removeMessages(0);
    }

    /* access modifiers changed from: 0000 */
    public final void B() {
        this.s.post(new Runnable() {
            public final void run() {
                i.this.C();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void C() {
        b("RenderTimeOut");
        if (this.a == 2) {
            this.a = 3;
            if (f() != null) {
                f().a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, str);
        c("AdLoadRejected", (Map<String, Object>) hashMap);
    }

    /* access modifiers changed from: 0000 */
    public final void D() {
        HashMap hashMap = new HashMap();
        hashMap.put("latency", Long.valueOf(SystemClock.elapsedRealtime() - this.J));
        c("AdLoadSuccessful", (Map<String, Object>) hashMap);
    }

    /* access modifiers changed from: 0000 */
    public final void b(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, str);
        hashMap.put("latency", Long.valueOf(SystemClock.elapsedRealtime() - this.J));
        c("AdLoadFailed", (Map<String, Object>) hashMap);
    }

    /* access modifiers changed from: 0000 */
    public final void c(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, str);
        c("AdPrefetchRejected", (Map<String, Object>) hashMap);
    }

    /* access modifiers changed from: 0000 */
    public final void a(b bVar, String str, String str2) {
        if (bVar != null && bVar.i()) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" ");
            sb.append(str2);
            c cVar = this.g;
            String b2 = b();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(b2);
            sb2.append("Dict");
            com.inmobi.ads.c.a aVar = (com.inmobi.ads.c.a) cVar.g.get(sb2.toString());
            if (aVar == null) {
                aVar = cVar.f;
            }
            if (aVar.h) {
                String str3 = "";
                if (this.k != null) {
                    str3 = this.k;
                }
                String str4 = str;
                com.inmobi.commons.core.f.b bVar2 = new com.inmobi.commons.core.f.b(UUID.randomUUID().toString(), this.m, str4, this.d, str3, str2, (String) com.inmobi.commons.core.utilities.b.b.a(M()).get("d-nettype-raw"), b(), System.currentTimeMillis());
                this.Q.a(bVar2);
            }
        }
    }

    @VisibleForTesting
    private boolean M() {
        return this.g.i.m && com.inmobi.commons.a.a.d();
    }

    public final void b(String str, Map<String, Object> map) {
        c(str, map);
    }

    public final void a(String str, Map<String, Object> map) {
        c(str, map);
    }

    public final void d(String str) {
        c(str, (Map<String, Object>) new HashMap<String,Object>());
    }

    public final void c(String str, Map<String, Object> map) {
        String str2;
        HashMap hashMap = new HashMap();
        hashMap.put("type", b());
        hashMap.put("plId", Long.valueOf(this.d));
        hashMap.put("impId", this.k);
        hashMap.put("isPreloaded", this.n ? "1" : "0");
        String str3 = "networkType";
        switch (com.inmobi.commons.core.utilities.b.b.a()) {
            case 0:
                str2 = "carrier";
                break;
            case 1:
                str2 = "wifi";
                break;
            default:
                str2 = "NIL";
                break;
        }
        hashMap.put(str3, str2);
        hashMap.put("ts", Long.valueOf(System.currentTimeMillis()));
        if (map.get("clientRequestId") == null) {
            hashMap.put("clientRequestId", this.l);
        }
        for (Entry entry : map.entrySet()) {
            hashMap.put(entry.getKey(), entry.getValue());
        }
        try {
            com.inmobi.commons.core.e.b.a();
            com.inmobi.commons.core.e.b.a("ads", str, hashMap);
        } catch (Exception e2) {
            StringBuilder sb = new StringBuilder("Error in submitting telemetry event : (");
            sb.append(e2.getMessage());
            sb.append(")");
        }
    }

    /* access modifiers changed from: 0000 */
    public final void c(@NonNull a aVar) {
        if (aVar instanceof bc) {
            bc bcVar = (bc) aVar;
            Context a2 = a();
            boolean z2 = this.g.k.j;
            for (br brVar : this.M) {
                if (z2 && 3 == brVar.a && "video" == brVar.b.get("creativeType")) {
                    try {
                        by byVar = new by(bcVar.l, bcVar.m, bcVar.n, bcVar.h(), bcVar.i(), this.g.m);
                        be beVar = (be) new ao(d(), new JSONObject(this.h), this.g, byVar).c(ShareConstants.VIDEO_URL).get(0);
                        if (a2 != null) {
                            HashSet hashSet = new HashSet();
                            for (NativeTracker nativeTracker : beVar.u) {
                                if (TrackerEventType.TRACKER_EVENT_TYPE_IAS == nativeTracker.b) {
                                    hashSet.add(d(nativeTracker.a, nativeTracker.c));
                                }
                            }
                            if (hashSet.size() != 0) {
                                brVar.b.put("avidAdSession", w.a(a2, (Set<String>) hashSet));
                                brVar.b.put("deferred", Boolean.valueOf(true));
                            }
                        }
                    } catch (Exception e2) {
                        new StringBuilder("Setting up impression tracking for AVID encountered an unexpected error: ").append(e2.getMessage());
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                    }
                }
            }
        }
    }

    @Nullable
    private static String d(String str, Map<String, String> map) {
        if (map == null || str == null) {
            return str;
        }
        for (String str2 : map.keySet()) {
            str = str.replace(str2, (CharSequence) map.get(str2));
        }
        return str;
    }

    public final void a(long j2) {
        d("AdPrefetchSuccessful");
        if (!this.w && a() != null) {
            Message obtain = Message.obtain();
            obtain.what = 14;
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.PLACEMENT_ID, j2);
            obtain.setData(bundle);
            this.K.sendMessage(obtain);
        }
    }

    public final void b(long j2, InMobiAdRequestStatus inMobiAdRequestStatus) {
        if (StatusCode.EARLY_REFRESH_REQUEST.equals(inMobiAdRequestStatus.getStatusCode())) {
            c("EarlyRefreshRequest");
        } else if (StatusCode.NETWORK_UNREACHABLE.equals(inMobiAdRequestStatus.getStatusCode())) {
            c("NetworkUnreachable");
        } else {
            d("AdPrefetchFailed");
        }
        if (!this.w && a() != null) {
            Message obtain = Message.obtain();
            obtain.what = 13;
            obtain.obj = inMobiAdRequestStatus;
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.PLACEMENT_ID, j2);
            obtain.setData(bundle);
            this.K.sendMessage(obtain);
        }
    }

    public final void e(final String str) {
        this.p.execute(new Runnable() {
            public final void run() {
                if (i.this.k == null || str == null) {
                    i.z;
                    return;
                }
                d.a();
                String h = i.this.k;
                String str = str;
                com.inmobi.commons.core.d.b a2 = com.inmobi.commons.core.d.b.a();
                a c = d.c(h);
                int i = 0;
                if (c != null) {
                    c.i = str;
                    i = a2.b("ad", c.a(), "imp_id=?", new String[]{h});
                }
                i.z;
                StringBuilder sb = new StringBuilder("Updated ");
                sb.append(i);
                sb.append("for blob ");
                sb.append(str);
            }
        });
    }

    public final void a(final String str, final String str2, @NonNull final com.inmobi.rendering.b bVar) {
        this.p.execute(new Runnable() {
            public final void run() {
                try {
                    if (i.this.k != null) {
                        d.a();
                        a c2 = d.c(i.this.k);
                        if (c2 != null) {
                            bVar.a(str, str2, c2.i);
                            i.z;
                            return;
                        }
                        i.z;
                        bVar.a(str, str2, "");
                        return;
                    }
                    i.z;
                    bVar.a(str, str2, "");
                } catch (Exception e) {
                    i.z;
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public final void G() {
        this.p.execute(new Runnable() {
            public final void run() {
                try {
                    final ao aoVar = new ao(i.this.d(), new JSONObject(i.this.h), i.this.g, null);
                    i.this.s.post(new Runnable() {
                        public final void run() {
                            try {
                                bg bgVar = aoVar.k;
                                if (bgVar != null) {
                                    i.this.u = new RenderView(i.this.a(), new RenderingProperties(i.this.d()), i.this.M, i.this.k);
                                    i.this.u.a(i.this.S, i.this.g);
                                    i.this.u.i = true;
                                    i.this.u.setBlobProvider(i.this);
                                    i.this.u.setIsPreload(true);
                                    i.this.u.setPlacementId(i.this.d);
                                    i.this.u.setCreativeId(i.this.x);
                                    i.this.u.setAllowAutoRedirection(i.this.B);
                                    if (i.this.r == 0) {
                                        i.this.a(true, i.this.u);
                                    }
                                    if ("URL".equals(bgVar.z)) {
                                        i.this.u.b((String) bgVar.e);
                                    } else {
                                        i.this.u.a((String) bgVar.e);
                                    }
                                }
                                i.g(i.this);
                            } catch (Exception e) {
                                i.z;
                                i.this.a = 3;
                                i.this.a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR), false);
                                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                            }
                        }
                    });
                } catch (Exception e) {
                    i.z;
                    i.this.s.post(new Runnable() {
                        public final void run() {
                            i.this.a = 3;
                            i.this.a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR), false);
                        }
                    });
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @UiThread
    public final void H() {
        if (this.t && this.v && this.P) {
            A();
            I();
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003d  */
    @Nullable
    public final AdContainer j() {
        char c2;
        int i2 = this.a;
        String str = this.m;
        int hashCode = str.hashCode();
        if (hashCode == -1084172778) {
            if (str.equals("inmobiJson")) {
                c2 = 2;
                switch (c2) {
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        } else if (hashCode == 3213227 && str.equals(String.HTML)) {
            c2 = 1;
            switch (c2) {
                case 1:
                    if (i2 == 0 || 1 == i2 || 3 == i2) {
                        return null;
                    }
                    return k();
                case 2:
                    if (i2 == 0 || 1 == i2 || 3 == i2 || 2 == i2) {
                        return null;
                    }
                    return this.o;
                default:
                    return null;
            }
        }
        c2 = 65535;
        switch (c2) {
            case 1:
                break;
            case 2:
                break;
        }
    }

    public final void a(long j2, boolean z2) {
        if (!this.w && a() != null) {
            Message obtain = Message.obtain();
            obtain.what = 4;
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.PLACEMENT_ID, j2);
            bundle.putBoolean("assetAvailable", z2);
            obtain.setData(bundle);
            this.K.sendMessage(obtain);
        }
    }

    public final void b(long j2, a aVar) {
        if (!this.w && a() != null) {
            this.J = SystemClock.elapsedRealtime();
            Message obtain = Message.obtain();
            obtain.what = 1;
            obtain.obj = aVar;
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.PLACEMENT_ID, j2);
            bundle.putBoolean("adAvailable", true);
            obtain.setData(bundle);
            this.K.sendMessage(obtain);
        }
    }

    public final void a(long j2, @NonNull a aVar) {
        if (!this.w && a() != null) {
            Message obtain = Message.obtain();
            obtain.what = 2;
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.PLACEMENT_ID, j2);
            obtain.setData(bundle);
            obtain.obj = aVar;
            this.K.sendMessage(obtain);
        }
    }

    public void a(final long j2, final InMobiAdRequestStatus inMobiAdRequestStatus) {
        if (!this.w && a() != null) {
            this.s.post(new Runnable() {
                public final void run() {
                    try {
                        if (j2 == i.this.d) {
                            i.this.a(i.this.f(), "ARN", "");
                            StringBuilder sb = new StringBuilder("Failed to fetch ad for placement id: ");
                            sb.append(i.this.d);
                            sb.append(", reason phrase available in onAdLoadFailed callback.");
                            Logger.a(InternalLogLevel.DEBUG, "InMobi", sb.toString());
                            i.this.a(inMobiAdRequestStatus, true);
                        }
                    } catch (Exception e) {
                        Logger.a(InternalLogLevel.ERROR, "[InMobi]", "Unable to load Ad; SDK encountered an unexpected error");
                        i.z;
                        new StringBuilder("onAdFetchFailed with error: ").append(e.getMessage());
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    @UiThread
    public void a(InMobiAdRequestStatus inMobiAdRequestStatus, boolean z2) {
        if (this.a == 1 && z2) {
            this.a = 3;
        }
        b f2 = f();
        if (f2 != null) {
            f2.a(inMobiAdRequestStatus);
        }
        a(inMobiAdRequestStatus);
    }

    /* access modifiers changed from: 0000 */
    @UiThread
    public final void a(@Nullable final byte[] bArr) {
        boolean z2;
        if (com.inmobi.commons.core.utilities.b.e.e()) {
            switch (this.a) {
                case 0:
                case 3:
                case 11:
                    z2 = false;
                    break;
                case 1:
                    this.s.post(new Runnable() {
                        public final void run() {
                            i.this.a(new InMobiAdRequestStatus(StatusCode.LOAD_WITH_RESPONSE_CALLED_WHILE_LOADING), false);
                        }
                    });
                    break;
                case 6:
                case 7:
                case 8:
                    this.s.post(new Runnable() {
                        public final void run() {
                            i.this.a(new InMobiAdRequestStatus(StatusCode.AD_ACTIVE), false);
                        }
                    });
                    break;
                case 10:
                    this.s.post(new Runnable() {
                        public final void run() {
                            i.this.a(new InMobiAdRequestStatus(StatusCode.FETCHING_SIGNALS_STATE_ERROR), false);
                        }
                    });
                    break;
                default:
                    this.s.post(new Runnable() {
                        public final void run() {
                            i.this.a(new InMobiAdRequestStatus(StatusCode.GET_SIGNALS_NOT_CALLED_FOR_LOAD_WITH_RESPONSE), false);
                        }
                    });
                    break;
            }
        } else {
            t();
            this.s.post(new Runnable() {
                public final void run() {
                    i.this.a(new InMobiAdRequestStatus(StatusCode.GDPR_COMPLIANCE_ENFORCED), false);
                }
            });
        }
        z2 = true;
        if (!z2) {
            if (bArr == null || bArr.length == 0) {
                a(new InMobiAdRequestStatus(StatusCode.INVALID_RESPONSE_IN_LOAD), true);
                return;
            }
            if (this.R == null) {
                this.R = new com.inmobi.ads.c.a(this, com.inmobi.ads.d.a.a(this.f));
                com.inmobi.ads.c.a aVar = this.R;
                aVar.d = new com.inmobi.ads.c.b(aVar.a.b(true), null);
            }
            this.p.execute(new Runnable() {
                public final void run() {
                    i.a(i.this, bArr, i.this.R);
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean p() {
        if (1 == this.a) {
            b(this.d, new InMobiAdRequestStatus(StatusCode.REQUEST_PENDING));
            StringBuilder sb = new StringBuilder("An ad prefetch is already in progress. Please wait for the prefetch to complete before requesting for another ad for placement id: ");
            sb.append(this.d);
            Logger.a(InternalLogLevel.ERROR, "InMobi", sb.toString());
            return true;
        } else if (8 == this.a || 7 == this.a) {
            b(this.d, new InMobiAdRequestStatus(StatusCode.AD_ACTIVE));
            StringBuilder sb2 = new StringBuilder("An ad is currently being viewed by the user. Please wait for the user to close the ad before requesting for another ad for placement id: ");
            sb2.append(this.d);
            Logger.a(InternalLogLevel.ERROR, "InMobi", sb2.toString());
            return true;
        } else {
            if (2 == this.a) {
                if (String.HTML.equals(this.m)) {
                    b(this.d, new InMobiAdRequestStatus(StatusCode.REQUEST_PENDING));
                    StringBuilder sb3 = new StringBuilder("An ad load is already in progress. Please wait for the load to complete before requesting prefetch for another ad for placement id: ");
                    sb3.append(this.d);
                    Logger.a(InternalLogLevel.ERROR, "InMobi", sb3.toString());
                    return true;
                } else if ("inmobiJson".equals(this.m)) {
                    a(this.d);
                    return true;
                }
            }
            if (5 != this.a && 9 != this.a) {
                return false;
            }
            a(this.d);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public int r() {
        String str;
        boolean z2 = true;
        try {
            this.a = 1;
            o.a().e();
            com.inmobi.commons.core.utilities.uid.c.a();
            com.inmobi.commons.core.utilities.uid.c.c();
            h hVar = new h();
            com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) hVar, (com.inmobi.commons.core.configs.b.c) null);
            if (!hVar.g) {
                f b2 = b(false);
                this.I = System.currentTimeMillis();
                h i2 = i();
                try {
                    int i3 = this.g.c;
                    h.a();
                    i2.c = b2;
                    if ("int".equals(i2.c.e)) {
                        h.c();
                        List c2 = i2.b.c(i2.c.a, i2.c.c, i2.c.j, com.inmobi.ads.d.a.a(i2.c.g));
                        if (c2.size() == 0) {
                            if (SystemClock.elapsedRealtime() - i2.e >= ((long) (i3 * 1000))) {
                                z2 = false;
                            }
                            if (!z2) {
                                str = i2.a(i2.c, i2.c.c().equals("1"));
                            } else {
                                throw new com.inmobi.ads.b.a("Ignoring request to fetch an ad from the network sooner than the minimum request interval");
                            }
                        } else {
                            str = ((a) c2.get(0)).h;
                            if ("INMOBIJSON".equalsIgnoreCase(((a) c2.get(0)).e())) {
                                i2.a.b(i2.c.a, (a) c2.get(0));
                                i2.a(c2);
                            } else {
                                str = i2.b();
                            }
                        }
                    } else {
                        str = i2.b();
                    }
                    HashMap hashMap = new HashMap();
                    hashMap.put("im-accid", com.inmobi.commons.a.a.e());
                    hashMap.put("isPreloaded", i2.c.c());
                    i2.a.a("AdCacheAdRequested", (Map<String, Object>) hashMap);
                    this.l = str;
                    a(f(), "VAR", "");
                    if (this.n) {
                        d("AdPreLoadRequested");
                    }
                } catch (com.inmobi.ads.b.a e2) {
                    e2.getMessage();
                    this.s.post(new Runnable() {
                        public final void run() {
                            i.this.a(new InMobiAdRequestStatus(StatusCode.EARLY_REFRESH_REQUEST), true);
                        }
                    });
                }
                return 0;
            }
            d("LoadAfterMonetizationDisabled");
            Logger.a(InternalLogLevel.ERROR, z, "SDK will not perform this load operation as monetization has been disabled. Please contact InMobi for further info.");
            a(new InMobiAdRequestStatus(StatusCode.MONETIZATION_DISABLED), true);
            return -1;
        } catch (Exception e3) {
            Logger.a(InternalLogLevel.ERROR, "InMobi", "Unable to load ad; SDK encountered an unexpected error");
            new StringBuilder("Load failed with unexpected error: ").append(e3.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
            return -2;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(b bVar, @NonNull String str, @Nullable Runnable runnable, @Nullable Looper looper) {
        Runnable runnable2 = runnable;
        Looper looper2 = looper;
        if (String.HTML.equals(this.m)) {
            final String str2 = str;
            this.s.post(new Runnable() {
                public final void run() {
                    i.e(i.this);
                    if (i.this.G != null) {
                        i.this.G.a(str2);
                    }
                    i.g(i.this);
                }
            });
            return;
        }
        if ("inmobiJson".equals(this.m)) {
            final WeakReference weakReference = new WeakReference(bVar);
            try {
                this.J = SystemClock.elapsedRealtime();
                ao aoVar = new ao(d(), new JSONObject(this.h), this.g, this.j);
                if (!aoVar.c() || a() == null) {
                    a("DataModelValidationFailed", weakReference);
                    return;
                }
                ah a2 = b.a(a(), new RenderingProperties(d()), aoVar, this.k, this.l, this.M, this.g, this.d, this.B, this.x);
                a2.a((com.inmobi.ads.ah.c) new com.inmobi.ads.ah.c() {
                    public final void a() {
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                i.this.a(bVar, "AVFB", "");
                                bVar.b();
                                return;
                            }
                            i.this.g();
                        }
                    }

                    public final void b() {
                        i.this.d("AdRendered");
                        if (!i.this.w) {
                            i.this.s.post(new Runnable() {
                                public final void run() {
                                    i.this.b((b) weakReference.get());
                                }
                            });
                        }
                    }

                    public final void c() {
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                bVar.c();
                            } else {
                                i.this.g();
                            }
                        }
                    }

                    public final void d() {
                        StringBuilder sb = new StringBuilder("Successfully impressed ad for placement id: ");
                        sb.append(i.this.d);
                        Logger.a(InternalLogLevel.DEBUG, "InMobi", sb.toString());
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                bVar.g();
                            } else {
                                i.this.g();
                            }
                        }
                    }

                    public final void e() {
                        StringBuilder sb = new StringBuilder("Ad interaction for placement id: ");
                        sb.append(i.this.d);
                        Logger.a(InternalLogLevel.DEBUG, "InMobi", sb.toString());
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                bVar.a((Map<Object, Object>) new HashMap<Object,Object>());
                            } else {
                                i.this.g();
                            }
                        }
                    }

                    public final void f() {
                        if (!i.this.w) {
                            StringBuilder sb = new StringBuilder("Ad dismissed for placement id: ");
                            sb.append(i.this.d);
                            Logger.a(InternalLogLevel.DEBUG, "InMobi", sb.toString());
                            i.this.s.post(new Runnable() {
                                public final void run() {
                                    i.this.c((b) weakReference.get());
                                }
                            });
                        }
                    }

                    public final void a(Map<String, String> map) {
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                bVar.b((Map<Object, Object>) new HashMap<Object,Object>(map));
                            } else {
                                i.this.g();
                            }
                        }
                    }

                    public final void g() {
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                bVar.f();
                            } else {
                                i.this.g();
                            }
                        }
                    }

                    public final void h() {
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                bVar.h();
                            } else {
                                i.this.g();
                            }
                        }
                    }

                    public final void a(String str, Map<String, Object> map) {
                        i.this.c(str, map);
                    }

                    public final void i() {
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                bVar.j();
                            } else {
                                i.this.g();
                            }
                        }
                    }

                    public final void a(boolean z) {
                        if (!i.this.w) {
                            b bVar = (b) weakReference.get();
                            if (bVar != null) {
                                bVar.b(z);
                            } else {
                                i.this.g();
                            }
                        }
                    }
                });
                this.o = a2;
                if (!(runnable2 == null || looper2 == null)) {
                    new Handler(looper2).post(runnable2);
                }
            } catch (JSONException e2) {
                a("InternalError", weakReference);
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            } catch (Exception e3) {
                new StringBuilder("Encountered unexpected error in loading ad markup into container: ").append(e3.getMessage());
                a("InternalError", weakReference);
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
            }
        }
    }

    public final void u() {
        if (!this.w && a() != null) {
            this.K.sendEmptyMessage(11);
        }
    }

    public final void w() {
        if (!this.w && a() != null) {
            this.K.sendEmptyMessage(12);
        }
    }

    public void a(RenderView renderView) {
        if (!this.w && a() != null) {
        }
    }

    public void b(RenderView renderView) {
        if (!this.w && a() != null) {
        }
    }

    public final void y() {
        if (!this.w && a() != null && 7 == this.a) {
            this.a = 3;
            a(f(), "AVFB", "");
            if (f() != null) {
                f().b();
            }
        }
    }

    public void c(RenderView renderView) {
        if (!this.w && a() != null) {
        }
    }

    public void d(RenderView renderView) {
        if (!this.w && a() != null) {
        }
    }

    public final void a(HashMap<Object, Object> hashMap) {
        if (!this.w && a() != null) {
            new StringBuilder("Ad reward action completed. Params:").append(hashMap.toString());
            if (f() != null) {
                f().b((Map<Object, Object>) hashMap);
            }
        }
    }

    public final void b(HashMap<Object, Object> hashMap) {
        if (!this.w && a() != null) {
            new StringBuilder("Ad interaction. Params:").append(hashMap.toString());
            d("AdInteracted");
            if (f() != null) {
                f().a((Map<Object, Object>) hashMap);
            }
        }
    }

    public final void z() {
        if (!(this.w || a() == null || f() == null)) {
            f().f();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(boolean z2, RenderView renderView) {
        boolean z3 = this.g.k.j;
        for (br brVar : this.M) {
            if (z3 && 3 == brVar.a) {
                try {
                    AbstractAvidAdSession a2 = v.a(a(), z2, (String) brVar.b.get("creativeType"), renderView);
                    if (a2 != null) {
                        brVar.b.put("avidAdSession", a2);
                        brVar.b.put("deferred", Boolean.valueOf(z2));
                    }
                } catch (Exception e2) {
                    new StringBuilder("Setting up impression tracking for IAS encountered an unexpected error: ").append(e2.getMessage());
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                }
            }
        }
    }

    public void F() {
        if (1 == this.a && this.q != null) {
            this.q.a(this);
        }
    }

    public void b(InMobiAdRequestStatus inMobiAdRequestStatus) {
        if (1 == this.a && this.q != null) {
            this.q.a(this, inMobiAdRequestStatus);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003b A[Catch:{ Exception -> 0x0055 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045 A[Catch:{ Exception -> 0x0055 }] */
    static /* synthetic */ void a(i iVar, byte[] bArr, com.inmobi.ads.c.a aVar) {
        g gVar;
        try {
            iVar.a("AdLoadWithResponseRequested", aVar.c);
            iVar.a = 1;
            if (aVar.d != null) {
                com.inmobi.ads.c.b bVar = aVar.d;
                byte[] a2 = bVar.a.a(bArr);
                if (a2 != null && bVar.a.v) {
                    a2 = com.inmobi.commons.core.utilities.d.a(a2);
                }
                if (a2 != null) {
                    if (a2.length != 0) {
                        com.inmobi.commons.core.network.d dVar = new com.inmobi.commons.core.network.d();
                        dVar.b(a2);
                        gVar = new g(bVar.a, dVar);
                        if (gVar == null) {
                            aVar.a.i().a(gVar);
                            return;
                        }
                        throw new IllegalStateException("Unable to decrypt response.");
                    }
                }
                gVar = null;
                if (gVar == null) {
                }
            } else {
                throw new IllegalStateException("GMARequest is null.");
            }
        } catch (Exception unused) {
            iVar.s.post(new Runnable() {
                public final void run() {
                    i.this.a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR), true);
                }
            });
        }
    }

    static /* synthetic */ void L() {
        com.inmobi.commons.core.utilities.uid.c.a();
        com.inmobi.commons.core.utilities.uid.c.c();
    }

    static /* synthetic */ void e(i iVar) {
        if ((iVar.G == null || iVar.G.r.get()) && iVar.a() != null) {
            iVar.G = new RenderView(iVar.a(), new RenderingProperties(iVar.d()), iVar.M, iVar.k);
            iVar.G.a((com.inmobi.rendering.RenderView.a) iVar, iVar.g);
            iVar.G.setPlacementId(iVar.d);
            iVar.G.setCreativeId(iVar.x);
            iVar.G.setAllowAutoRedirection(iVar.B);
        }
    }

    static /* synthetic */ void g(i iVar) {
        iVar.A();
        iVar.H.sendEmptyMessageDelayed(0, (long) (iVar.g.i.a * 1000));
    }
}
