package com.inmobi.ads;

import android.media.MediaMetadataRetriever;

/* compiled from: NativeTimer */
public final class ba {
    a a;
    a b;

    /* compiled from: NativeTimer */
    public static class a {
        private long a;
        private long b;
        private String c;
        private ao d;

        public a(long j, long j2, String str, ao aoVar) {
            this.a = j;
            this.b = j2;
            this.c = str;
            this.d = aoVar;
        }

        public final long a() {
            long j = this.a;
            ak b2 = this.d.b(this.c);
            if (b2 != null && (b2 instanceof be)) {
                be beVar = (be) b2;
                if (beVar != null) {
                    String b3 = beVar.b().b();
                    if (b3 != null) {
                        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                        mediaMetadataRetriever.setDataSource(b3);
                        long intValue = (long) Integer.valueOf(mediaMetadataRetriever.extractMetadata(9)).intValue();
                        double d2 = (double) j;
                        double d3 = (double) this.b;
                        Double.isNaN(d3);
                        double d4 = (d3 * 1.0d) / 100.0d;
                        double d5 = (double) (intValue / 1000);
                        Double.isNaN(d5);
                        double d6 = d4 * d5;
                        Double.isNaN(d2);
                        j = (long) (d2 + d6);
                        mediaMetadataRetriever.release();
                    }
                }
            }
            if (j >= 0) {
                return j;
            }
            return 0;
        }
    }

    public ba(a aVar, a aVar2) {
        this.a = aVar;
        this.b = aVar2;
    }
}
