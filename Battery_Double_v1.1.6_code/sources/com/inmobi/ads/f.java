package com.inmobi.ads;

import android.net.Uri;
import com.inmobi.a.b.b;
import com.inmobi.a.m;
import com.inmobi.ads.InMobiAdRequest.MonetizationContext;
import com.inmobi.commons.core.network.c;
import com.inmobi.commons.core.utilities.b.a;
import com.inmobi.commons.core.utilities.b.g;
import com.inmobi.commons.core.utilities.uid.d;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/* compiled from: AdNetworkRequest */
public final class f extends c {
    private static final String B = "f";
    public long a;
    String b = "json";
    public String c;
    int d = 1;
    public String e;
    public String f;
    public Map<String, String> g;
    Map<String, String> h;
    public final String i;
    public MonetizationContext j;
    public final r k;

    public f(String str, long j2, d dVar, String str2, boolean z) {
        super(HttpRequest.METHOD_POST, str, a(str), dVar, a(str), 0);
        this.A = z;
        this.a = j2;
        this.n.put("im-plid", String.valueOf(this.a));
        this.n.putAll(g.d());
        this.n.putAll(com.inmobi.commons.core.utilities.b.c.c());
        this.n.put("u-appIS", a.a().a);
        this.n.putAll(m.a().f());
        this.n.putAll(m.a().e());
        Map map = this.n;
        com.inmobi.a.b.a a2 = b.a();
        HashMap hashMap = new HashMap();
        if (a2 != null) {
            hashMap.put("c-ap-bssid", String.valueOf(a2.a));
        }
        map.putAll(hashMap);
        this.n.putAll(com.inmobi.a.a.c.b());
        this.n.putAll(com.inmobi.a.a.c.c());
        this.n.putAll(com.inmobi.a.a.c.a());
        this.i = UUID.randomUUID().toString();
        this.n.put("client-request-id", this.i);
        if (str2 != null) {
            this.n.put("u-appcache", str2);
        }
        this.n.put("sdk-flavor", "row");
        this.k = new r(z);
        this.n.put("skdv", this.y.c);
        this.n.put("skdm", this.k.a(this.y.b, this.y.a));
    }

    private static boolean a(String str) {
        if (str == null) {
            return true;
        }
        Uri parse = Uri.parse(str);
        if ("http".equals(parse.getScheme()) || !"https".equals(parse.getScheme())) {
            return true;
        }
        return false;
    }

    public final void a() {
        super.a();
        this.n.put("format", this.b);
        this.n.put("mk-ads", String.valueOf(this.d));
        this.n.put("adtype", this.e);
        if (this.f != null) {
            this.n.put("p-keywords", this.f);
        }
        String str = this.j != null ? this.j == MonetizationContext.MONETIZATION_CONTEXT_OTHER ? "M10N_CONTEXT_OTHER" : "M10N_CONTEXT_ACTIVITY" : "M10N_CONTEXT_ACTIVITY";
        this.n.put("m10n_context", str);
        if (this.g != null) {
            for (Entry entry : this.g.entrySet()) {
                if (!this.n.containsKey(entry.getKey())) {
                    this.n.put(entry.getKey(), entry.getValue());
                }
            }
        }
        if (this.h != null) {
            this.n.putAll(this.h);
        }
    }

    public final String c() {
        return this.h.containsKey("preload-request") ? (String) this.h.get("preload-request") : "0";
    }

    public final boolean b() {
        return this.A || super.b();
    }
}
