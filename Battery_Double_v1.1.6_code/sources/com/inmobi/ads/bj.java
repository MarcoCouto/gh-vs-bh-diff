package com.inmobi.ads;

import android.content.ContentValues;
import com.facebook.appevents.AppEventsConstants;
import com.inmobi.ads.InMobiAdRequest.MonetizationContext;
import com.inmobi.ads.d.a;
import java.util.Map;

/* compiled from: Placement */
public final class bj {
    public long a;
    public String b;
    public Map<String, String> c;
    public String d;
    String e;
    public MonetizationContext f = MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;

    public static bj a(long j, Map<String, String> map, String str, String str2) {
        bj bjVar = new bj(j, a.a(map), str);
        bjVar.d = str2;
        bjVar.c = map;
        return bjVar;
    }

    private bj(long j, String str, String str2) {
        this.a = j;
        this.b = str;
        this.e = str2;
        if (this.b == null) {
            this.b = "";
        }
    }

    public bj(ContentValues contentValues) {
        this.a = contentValues.getAsLong("placement_id").longValue();
        this.b = contentValues.getAsString("tp_key");
        this.e = contentValues.getAsString(AppEventsConstants.EVENT_PARAM_AD_TYPE);
        this.f = MonetizationContext.a(contentValues.getAsString("m10_context"));
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bj bjVar = (bj) obj;
        return this.a == bjVar.a && this.f == bjVar.f && this.b.equals(bjVar.b) && this.e.equals(bjVar.e);
    }

    public final int hashCode() {
        return (((((int) (this.a ^ (this.a >>> 32))) * 31) + this.e.hashCode()) * 30) + this.f.hashCode();
    }
}
