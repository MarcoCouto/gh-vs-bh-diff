package com.inmobi.ads;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import java.lang.ref.WeakReference;

public class NativeRecyclerViewAdapter extends Adapter<a> implements ax {
    private static final String a = "NativeRecyclerViewAdapter";
    private final ao b;
    private au c;
    private SparseArray<WeakReference<View>> d;
    private boolean e = false;

    class a extends ViewHolder {
        /* access modifiers changed from: private */
        public ViewGroup b;

        a(View view) {
            super(view);
            this.b = (ViewGroup) view;
        }
    }

    NativeRecyclerViewAdapter(@NonNull ao aoVar, @NonNull au auVar) {
        this.b = aoVar;
        this.c = auVar;
        this.d = new SparseArray<>();
    }

    @NonNull
    public a onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new a(new FrameLayout(viewGroup.getContext()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0016, code lost:
        if (r1 == null) goto L_0x0018;
     */
    public void onBindViewHolder(@NonNull a aVar, int i) {
        View view;
        am a2 = this.b.a(i);
        WeakReference weakReference = (WeakReference) this.d.get(i);
        if (weakReference != null) {
            view = (View) weakReference.get();
        }
        view = buildScrollableView(i, aVar.b, a2);
        if (view != null) {
            if (i != getItemCount() - 1) {
                aVar.b.setPadding(0, 0, 16, 0);
            }
            aVar.b.addView(view);
            this.d.put(i, new WeakReference(view));
        }
    }

    public void onViewRecycled(@NonNull a aVar) {
        aVar.b.removeAllViews();
        super.onViewRecycled(aVar);
    }

    public ViewGroup buildScrollableView(int i, @NonNull ViewGroup viewGroup, @NonNull am amVar) {
        ViewGroup a2 = this.c.a(viewGroup, amVar);
        this.c.b(a2, amVar);
        a2.setLayoutParams(bf.a((ak) amVar, viewGroup));
        return a2;
    }

    public int getItemCount() {
        return this.b.b();
    }

    public void destroy() {
        this.e = true;
    }
}
