package com.inmobi.ads;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.inmobi.ads.InMobiAdRequest.MonetizationContext;
import com.inmobi.ads.c.k;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.network.d;
import com.inmobi.commons.core.utilities.a.b;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Ad */
public class a {
    /* access modifiers changed from: private */
    public static final String l = "a";
    final int a;
    final String b;
    final String c;
    final long d;
    long e;
    long f;
    public final String g;
    String h;
    String i;
    boolean j;
    public final float k;
    private final String m;
    private String n;
    private MonetizationContext o;
    @Nullable
    private final String p;

    /* renamed from: com.inmobi.ads.a$a reason: collision with other inner class name */
    /* compiled from: Ad */
    static final class C0044a {
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0060 A[Catch:{ Exception -> 0x02c4 }] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0070 A[Catch:{ Exception -> 0x02c4 }] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x007c A[Catch:{ Exception -> 0x02c4 }] */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x007f A[Catch:{ Exception -> 0x02c4 }] */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x02a5 A[Catch:{ Exception -> 0x02c2 }] */
        @Nullable
        static a a(JSONObject jSONObject, long j, String str, String str2, String str3, MonetizationContext monetizationContext, r rVar) {
            a aVar;
            long j2;
            char c;
            int hashCode;
            JSONObject jSONObject2 = jSONObject;
            r rVar2 = rVar;
            a aVar2 = null;
            try {
                String string = jSONObject2.getString("markupType");
                long optLong = jSONObject2.optLong("expiry", -1);
                if (-1 != optLong) {
                    if (optLong > 0) {
                        j2 = TimeUnit.SECONDS.toMillis(optLong);
                        String string2 = jSONObject2.getString("impressionId");
                        float parseFloat = Float.parseFloat(d.a(b.a(Base64.decode(jSONObject2.getString("bid"), 0), rVar2.b, rVar2.a)));
                        String a = rVar2.a(jSONObject2.optString("bidInfoEncrypted", null));
                        c = 65535;
                        hashCode = string.hashCode();
                        if (hashCode == -1084172778) {
                            if (hashCode == 3213227) {
                                if (string.equals(String.HTML)) {
                                    c = 1;
                                }
                            }
                        } else if (string.equals("inmobiJson")) {
                            c = 2;
                        }
                        switch (c) {
                            case 1:
                                a aVar3 = new a(jSONObject, j, str, str2, string2, str3, monetizationContext, j2, parseFloat, a, 0);
                                return aVar3;
                            case 2:
                                JSONObject jSONObject3 = new JSONObject(jSONObject2.getString("pubContent"));
                                a.l;
                                jSONObject3.toString();
                                if (jSONObject3.isNull("rootContainer")) {
                                    a.l;
                                    try {
                                        HashMap hashMap = new HashMap();
                                        hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, "MISSING rootContainer");
                                        hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, "Missing rootContainer ad markup");
                                        com.inmobi.commons.core.e.b.a();
                                        com.inmobi.commons.core.e.b.a("ads", "ServerError", hashMap);
                                    } catch (Exception e) {
                                        a.l;
                                        StringBuilder sb = new StringBuilder("Error in submitting telemetry event : (");
                                        sb.append(e.getMessage());
                                        sb.append(")");
                                    }
                                    return null;
                                }
                                JSONObject jSONObject4 = jSONObject3.getJSONObject("rootContainer");
                                JSONArray jSONArray = new JSONArray();
                                for (String a2 : c(jSONObject4)) {
                                    a.a(jSONArray, a2, 2);
                                }
                                for (String a3 : d(jSONObject4)) {
                                    a.a(jSONArray, a3, 1);
                                }
                                String a4 = a(jSONObject4);
                                boolean b = b(jSONObject4);
                                if (a4.trim().length() == 0) {
                                    a.l;
                                    r1 = r1;
                                    try {
                                        a aVar4 = new a(jSONObject, jSONArray.toString(), j, str, str2, string2, str3, monetizationContext, b, j2, parseFloat, a);
                                        return aVar4;
                                    } catch (Exception e2) {
                                        e = e2;
                                        aVar = null;
                                        break;
                                    }
                                } else {
                                    c cVar = new c();
                                    aVar2 = null;
                                    com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) cVar, (c) null);
                                    by a5 = new bv(cVar.m).a(a4);
                                    if (a5.f != 0) {
                                        a.l;
                                        HashMap hashMap2 = new HashMap();
                                        hashMap2.put(IronSourceConstants.EVENTS_ERROR_CODE, String.valueOf(a5.f));
                                        hashMap2.put(IronSourceConstants.EVENTS_ERROR_REASON, "Processing VAST XML to build a video descriptor failed");
                                        hashMap2.put("latency", "0");
                                        com.inmobi.commons.core.e.b.a();
                                        com.inmobi.commons.core.e.b.a("ads", "VastProcessingError", hashMap2);
                                        return null;
                                    }
                                    HashMap hashMap3 = new HashMap();
                                    hashMap3.put("message", "VAST PROCESSING SUCCESS");
                                    com.inmobi.commons.core.e.b.a();
                                    com.inmobi.commons.core.e.b.a("ads", "VastProcessingSuccess", hashMap3);
                                    List<NativeTracker> list = a5.d;
                                    JSONArray jSONArray2 = new JSONArray();
                                    for (NativeTracker nativeTracker : list) {
                                        jSONArray2.put(nativeTracker.toString());
                                    }
                                    List<bu> list2 = a5.e;
                                    JSONArray jSONArray3 = new JSONArray();
                                    for (bu buVar : list2) {
                                        jSONArray3.put(buVar.toString());
                                    }
                                    String b2 = a5.b();
                                    if (b2 != null) {
                                        if (!b2.isEmpty()) {
                                            a.a(jSONArray, b2, 0);
                                            List<String> a6 = a(jSONObject3, cVar.m);
                                            a.l;
                                            new StringBuilder("Media size for pages").append(a6.size());
                                            for (String a7 : a6) {
                                                a.a(jSONArray, a7, 0);
                                            }
                                            for (String a8 : a(jSONObject3, "pages")) {
                                                a.a(jSONArray, a8, 2);
                                            }
                                            for (String a9 : b(jSONObject3, "pages")) {
                                                a.a(jSONArray, a9, 1);
                                            }
                                            String jSONArray4 = jSONArray.toString();
                                            String b3 = a5.b();
                                            String str4 = a5.b;
                                            String str5 = a5.c;
                                            String jSONArray5 = jSONArray2.toString();
                                            String jSONArray6 = jSONArray3.toString();
                                            r1 = r1;
                                            String str6 = str4;
                                            String str7 = jSONArray5;
                                            aVar = null;
                                            try {
                                                bc bcVar = new bc(jSONObject, jSONArray4, j, str, str2, string2, str3, b3, str6, str5, str7, jSONArray6, monetizationContext, b, j2, parseFloat, a);
                                                return bcVar;
                                            } catch (Exception e3) {
                                                e = e3;
                                                break;
                                            }
                                        }
                                    }
                                    HashMap hashMap4 = new HashMap();
                                    hashMap4.put(IronSourceConstants.EVENTS_ERROR_CODE, "ZERO LENGTH ASSET");
                                    hashMap4.put(IronSourceConstants.EVENTS_ERROR_REASON, "Asset length is 0");
                                    com.inmobi.commons.core.e.b.a();
                                    com.inmobi.commons.core.e.b.a("ads", "ServerError", hashMap4);
                                    a.l;
                                    return null;
                                }
                            default:
                                return null;
                        }
                        a.l;
                        new StringBuilder("Error parsing ad markup; ").append(e.getMessage());
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                        return aVar;
                    }
                }
                j2 = -1;
                String string22 = jSONObject2.getString("impressionId");
                float parseFloat2 = Float.parseFloat(d.a(b.a(Base64.decode(jSONObject2.getString("bid"), 0), rVar2.b, rVar2.a)));
                String a10 = rVar2.a(jSONObject2.optString("bidInfoEncrypted", null));
                c = 65535;
                hashCode = string.hashCode();
                if (hashCode == -1084172778) {
                }
                switch (c) {
                    case 1:
                        break;
                    case 2:
                        break;
                }
            } catch (Exception e4) {
                e = e4;
                aVar = aVar2;
            }
            a.l;
            new StringBuilder("Error parsing ad markup; ").append(e.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
            return aVar;
        }

        private static List<String> a(JSONObject jSONObject, k kVar) {
            ArrayList arrayList = new ArrayList();
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("pages");
                for (int i = 0; i < jSONArray.length(); i++) {
                    if (!jSONArray.getJSONObject(i).isNull("rootContainer")) {
                        String a = a(jSONArray.getJSONObject(i).getJSONObject("rootContainer"));
                        if (a.trim().length() == 0) {
                            a.l;
                        } else {
                            by a2 = new bv(kVar).a(a);
                            if (a2 != null) {
                                if (a2.f == 0) {
                                    String b = a2.b();
                                    if (b != null && !b.isEmpty()) {
                                        arrayList.add(b);
                                    }
                                }
                            }
                            a.l;
                        }
                    }
                }
            } catch (JSONException unused) {
                a.l;
            }
            return arrayList;
        }

        private static List<String> a(JSONObject jSONObject, String str) {
            ArrayList arrayList = new ArrayList();
            try {
                JSONArray jSONArray = jSONObject.getJSONArray(str);
                for (int i = 0; i < jSONArray.length(); i++) {
                    if (!jSONArray.getJSONObject(i).isNull("rootContainer")) {
                        arrayList.addAll(c(jSONArray.getJSONObject(i).getJSONObject("rootContainer")));
                    }
                }
            } catch (JSONException unused) {
                a.l;
            }
            return arrayList;
        }

        private static List<String> b(JSONObject jSONObject, String str) {
            ArrayList arrayList = new ArrayList();
            try {
                JSONArray jSONArray = jSONObject.getJSONArray(str);
                for (int i = 0; i < jSONArray.length(); i++) {
                    if (!jSONArray.getJSONObject(i).isNull("rootContainer")) {
                        arrayList.addAll(d(jSONArray.getJSONObject(i).getJSONObject("rootContainer")));
                    }
                }
            } catch (JSONException unused) {
                a.l;
            }
            return arrayList;
        }

        static a a(ContentValues contentValues) {
            if (!contentValues.containsKey(TapjoyConstants.TJC_VIDEO_URL) || contentValues.getAsString(TapjoyConstants.TJC_VIDEO_URL) == null || contentValues.getAsString(TapjoyConstants.TJC_VIDEO_URL).isEmpty()) {
                return new a(contentValues);
            }
            return new bc(contentValues);
        }

        @NonNull
        private static String a(@NonNull JSONObject jSONObject) {
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("assetValue");
                if (jSONArray.length() == 0) {
                    return "";
                }
                String string = jSONObject.getString("assetType");
                if (string.equalsIgnoreCase("video")) {
                    return jSONArray.getString(0);
                }
                if (!string.equalsIgnoreCase("container")) {
                    return "";
                }
                String str = "";
                for (int i = 0; i < jSONArray.length(); i++) {
                    str = a(jSONArray.getJSONObject(i));
                    if (str.trim().length() != 0) {
                        break;
                    }
                }
                return str;
            } catch (JSONException e) {
                a.l;
                StringBuilder sb = new StringBuilder("Error getting VAST video XML (");
                sb.append(e.getMessage());
                sb.append(")");
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                return "";
            }
        }

        private static boolean b(@NonNull JSONObject jSONObject) {
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("assetValue");
                if (jSONArray.length() == 0) {
                    return false;
                }
                String string = jSONObject.getString("assetType");
                if (string.equalsIgnoreCase(ParametersKeys.WEB_VIEW)) {
                    if (jSONObject.isNull("preload") || !jSONObject.getBoolean("preload")) {
                        return false;
                    }
                    return true;
                } else if (!string.equalsIgnoreCase("container")) {
                    return false;
                } else {
                    boolean z = false;
                    for (int i = 0; i < jSONArray.length(); i++) {
                        z = b(jSONArray.getJSONObject(i));
                        if (z) {
                            break;
                        }
                    }
                    return z;
                }
            } catch (JSONException e) {
                a.l;
                StringBuilder sb = new StringBuilder("Error getting preload webview flag (");
                sb.append(e.getMessage());
                sb.append(")");
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                return false;
            }
        }

        @NonNull
        private static List<String> c(@NonNull JSONObject jSONObject) {
            ArrayList arrayList = new ArrayList();
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("assetValue");
                if (jSONArray.length() == 0) {
                    return arrayList;
                }
                String string = jSONObject.getString("assetType");
                if (string.equalsIgnoreCase(MessengerShareContentUtility.MEDIA_IMAGE)) {
                    if (!jSONObject.isNull("preload") && jSONObject.getBoolean("preload")) {
                        arrayList.add(jSONArray.getString(0));
                    }
                    return arrayList;
                } else if (!string.equalsIgnoreCase("container")) {
                    return arrayList;
                } else {
                    for (int i = 0; i < jSONArray.length(); i++) {
                        arrayList.addAll(c(jSONArray.getJSONObject(i)));
                    }
                    return arrayList;
                }
            } catch (JSONException e) {
                a.l;
                StringBuilder sb = new StringBuilder("Error getting getImageAssetUrls (");
                sb.append(e.getMessage());
                sb.append(")");
                return arrayList;
            }
        }

        @NonNull
        private static List<String> d(@NonNull JSONObject jSONObject) {
            ArrayList arrayList = new ArrayList();
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("assetValue");
                if (jSONArray.length() == 0) {
                    return arrayList;
                }
                String string = jSONObject.getString("assetType");
                if (string.equalsIgnoreCase("gif")) {
                    arrayList.add(jSONArray.getString(0));
                    return arrayList;
                } else if (!string.equalsIgnoreCase("container")) {
                    return arrayList;
                } else {
                    for (int i = 0; i < jSONArray.length(); i++) {
                        arrayList.addAll(d(jSONArray.getJSONObject(i)));
                    }
                    return arrayList;
                }
            } catch (JSONException e) {
                a.l;
                StringBuilder sb = new StringBuilder("Error getting getGifAssetUrls (");
                sb.append(e.getMessage());
                sb.append(")");
                return arrayList;
            }
        }
    }

    /* synthetic */ a(JSONObject jSONObject, long j2, String str, String str2, String str3, String str4, MonetizationContext monetizationContext, long j3, float f2, String str5, byte b2) {
        this(jSONObject, j2, str, str2, str3, str4, monetizationContext, j3, f2, str5);
    }

    private a(JSONObject jSONObject, long j2, String str, String str2, String str3, String str4, MonetizationContext monetizationContext, long j3, float f2, @Nullable String str5) {
        this(jSONObject, null, j2, str, str2, str3, str4, monetizationContext, false, j3, f2, str5);
    }

    a(JSONObject jSONObject, String str, long j2, String str2, String str3, String str4, String str5, MonetizationContext monetizationContext, boolean z, long j3, float f2, @Nullable String str6) {
        this.a = -1;
        this.c = jSONObject.toString();
        this.n = str;
        this.d = j2;
        this.b = str2;
        this.m = str3;
        this.e = System.currentTimeMillis();
        this.g = str4;
        this.h = str5;
        this.o = monetizationContext;
        this.i = "";
        this.j = z;
        this.f = j3;
        this.k = f2;
        this.p = str6;
    }

    a(ContentValues contentValues) {
        this.a = contentValues.getAsInteger("id").intValue();
        this.b = contentValues.getAsString(AppEventsConstants.EVENT_PARAM_AD_TYPE);
        this.m = contentValues.getAsString("ad_size");
        this.n = contentValues.getAsString("asset_urls");
        this.c = contentValues.getAsString("ad_content");
        this.d = contentValues.getAsLong("placement_id").longValue();
        this.e = contentValues.getAsLong("insertion_ts").longValue();
        this.f = contentValues.getAsLong("expiry_duration").longValue();
        this.g = contentValues.getAsString("imp_id");
        this.h = contentValues.getAsString("client_request_id");
        this.o = MonetizationContext.a(contentValues.getAsString("m10_context"));
        if (this.o == null) {
            this.o = MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;
        }
        this.i = contentValues.getAsString("web_vast");
        this.j = contentValues.getAsInteger("preload_webView").intValue() != 0;
        this.k = contentValues.getAsFloat("bid").floatValue();
        this.p = contentValues.getAsString("bidInfo");
    }

    public ContentValues a() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppEventsConstants.EVENT_PARAM_AD_TYPE, this.b);
        contentValues.put("ad_size", this.m);
        contentValues.put("asset_urls", this.n);
        contentValues.put("ad_content", this.c);
        contentValues.put("placement_id", Long.valueOf(this.d));
        contentValues.put("insertion_ts", Long.valueOf(this.e));
        contentValues.put("expiry_duration", Long.valueOf(this.f));
        contentValues.put("imp_id", this.g);
        contentValues.put("client_request_id", this.h);
        contentValues.put("m10_context", this.o.a);
        if (this.i != null) {
            contentValues.put("web_vast", this.i);
        }
        contentValues.put("preload_webView", Integer.valueOf(this.j ? 1 : 0));
        contentValues.put("bid", Float.valueOf(this.k));
        contentValues.put("bidInfo", this.p);
        return contentValues;
    }

    @NonNull
    public final JSONObject b() {
        try {
            return this.p == null ? new JSONObject() : new JSONObject(this.p);
        } catch (JSONException unused) {
            return new JSONObject();
        }
    }

    /* access modifiers changed from: 0000 */
    public final long c() {
        if (this.f == -1) {
            return -1;
        }
        return this.e + this.f;
    }

    @NonNull
    public final Set<bn> d() {
        HashSet hashSet = new HashSet();
        if (this.n == null || this.n.length() == 0) {
            return hashSet;
        }
        try {
            JSONArray jSONArray = new JSONArray(this.n);
            if (jSONArray.length() == 0) {
                return hashSet;
            }
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject = new JSONObject(jSONArray.getString(i2));
                int i3 = jSONObject.getInt("type");
                String string = jSONObject.getString("url");
                if (string != null) {
                    hashSet.add(new bn(i3, string));
                }
            }
            return hashSet;
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return hashSet;
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String e() {
        try {
            JSONObject jSONObject = new JSONObject(this.c);
            if (jSONObject.isNull("markupType")) {
                return "";
            }
            return jSONObject.getString("markupType");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return "";
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@NonNull a aVar) {
        return d().equals(aVar.d());
    }

    @Nullable
    public final JSONObject f() {
        try {
            JSONObject jSONObject = new JSONObject(this.c);
            if (jSONObject.has("cachedAdData")) {
                return jSONObject.getJSONObject("cachedAdData");
            }
            return null;
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return null;
        }
    }

    static /* synthetic */ void a(JSONArray jSONArray, String str, int i2) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("type", i2);
        jSONObject.put("url", str);
        jSONArray.put(jSONObject);
    }
}
