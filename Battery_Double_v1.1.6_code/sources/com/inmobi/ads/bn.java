package com.inmobi.ads;

import android.support.annotation.NonNull;

/* compiled from: RawAsset */
public final class bn {
    public final int a;
    @NonNull
    public final String b;

    public bn(int i, @NonNull String str) {
        this.a = i;
        this.b = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bn)) {
            return false;
        }
        bn bnVar = (bn) obj;
        return this.a == bnVar.a && this.b.equals(bnVar.b);
    }

    public final int hashCode() {
        return (this.a * 31) + this.b.hashCode();
    }
}
