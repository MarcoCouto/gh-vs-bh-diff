package com.inmobi.ads.d;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiAdRequestStatus.StatusCode;
import com.inmobi.ads.aj;
import com.inmobi.ads.bj;
import com.inmobi.ads.bk;
import com.inmobi.ads.f;
import com.inmobi.ads.i;
import com.inmobi.ads.i.e;
import com.inmobi.ads.p;
import com.inmobi.commons.core.configs.b;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: AdPreFetcher */
public class a implements c {
    public static ConcurrentHashMap<bj, i> a = new ConcurrentHashMap<>(8, 0.9f, 3);
    public static com.inmobi.ads.c b = null;
    /* access modifiers changed from: private */
    public static final String d = "a";
    private static volatile a e;
    private static volatile a f;
    private static volatile a g;
    private static final Object h = new Object();
    private static final Object i = new Object();
    private static final Object j = new Object();
    public String c;

    /* renamed from: com.inmobi.ads.d.a$a reason: collision with other inner class name */
    /* compiled from: AdPreFetcher */
    static class C0048a implements e {
        private bj a;

        C0048a(bj bjVar) {
            this.a = bjVar;
        }

        public final void a(@NonNull i iVar) {
            a.d;
            a.a.remove(this.a);
        }

        public final void a(@NonNull i iVar, @NonNull InMobiAdRequestStatus inMobiAdRequestStatus) {
            a.d;
            new StringBuilder("onAdLoadFailed called. Status:").append(inMobiAdRequestStatus.getMessage());
            a.a.remove(this.a);
            if (StatusCode.NO_FILL.equals(inMobiAdRequestStatus.getStatusCode())) {
                iVar.d("PreLoadServerNoFill");
            }
        }
    }

    private static a d() {
        a aVar = e;
        if (aVar == null) {
            synchronized (h) {
                aVar = e;
                if (aVar == null) {
                    aVar = new a("banner");
                    e = aVar;
                }
            }
        }
        return aVar;
    }

    private static a e() {
        a aVar = f;
        if (aVar == null) {
            synchronized (i) {
                aVar = f;
                if (aVar == null) {
                    aVar = new a("int");
                    f = aVar;
                }
            }
        }
        return aVar;
    }

    private static a f() {
        a aVar = g;
        if (aVar == null) {
            synchronized (j) {
                aVar = g;
                if (aVar == null) {
                    aVar = new a("native");
                    g = aVar;
                }
            }
        }
        return aVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0048  */
    @NonNull
    public static a a(String str) {
        char c2;
        int hashCode = str.hashCode();
        if (hashCode == -1396342996) {
            if (str.equals("banner")) {
                c2 = 0;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        } else if (hashCode == -1052618729) {
            if (str.equals("native")) {
                c2 = 1;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        } else if (hashCode == 104431 && str.equals("int")) {
            c2 = 2;
            switch (c2) {
                case 0:
                    return d();
                case 1:
                    return f();
                case 2:
                    return e();
                default:
                    throw new IllegalArgumentException("Unknown adType passed");
            }
        }
        c2 = 65535;
        switch (c2) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    public static i b(String str, @NonNull Context context, @NonNull bj bjVar) {
        char c2 = 65535;
        try {
            int hashCode = str.hashCode();
            if (hashCode != -1396342996) {
                if (hashCode != -1052618729) {
                    if (hashCode == 104431) {
                        if (str.equals("int")) {
                            c2 = 1;
                        }
                    }
                } else if (str.equals("native")) {
                    c2 = 2;
                }
            } else if (str.equals("banner")) {
                c2 = 0;
            }
            switch (c2) {
                case 0:
                    return p.a(context, bjVar, null, 1);
                case 1:
                    return com.inmobi.ads.ac.a.a(com.inmobi.commons.a.a.b(), bjVar, null);
                case 2:
                    return aj.a(context, bjVar, null, 1);
            }
        } catch (IllegalStateException e2) {
            e2.getMessage();
        }
        return null;
    }

    a(String str) {
        this.c = str;
        b = new com.inmobi.ads.c();
        b.a().a((com.inmobi.commons.core.configs.a) b, (c) this);
        com.inmobi.commons.core.e.b.a().a("ads", b.l);
    }

    public final void a(com.inmobi.commons.core.configs.a aVar) {
        b = (com.inmobi.ads.c) aVar;
        com.inmobi.commons.core.e.b.a().a("ads", b.l);
    }

    private void g() {
        Iterator it = a.entrySet().iterator();
        while (it.hasNext()) {
            try {
                Entry entry = (Entry) it.next();
                final i iVar = (i) entry.getValue();
                if (iVar.h()) {
                    StringBuilder sb = new StringBuilder("cleanUpExpiredCachedAdUnits. pid:");
                    sb.append(((bj) entry.getKey()).a);
                    sb.append(" tp:");
                    sb.append(((bj) entry.getKey()).b);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            try {
                                iVar.t();
                            } catch (Exception e) {
                                a.d;
                                new StringBuilder("Encountered an unexpected error clearing the ad unit: ").append(e.getMessage());
                                Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered an unexpected error clearing an old ad");
                                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                            }
                        }
                    });
                    it.remove();
                }
            } catch (Exception e2) {
                new StringBuilder("SDK encountered an unexpected error in expiring ad units; ").append(e2.getMessage());
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                return;
            }
        }
    }

    public final void b() {
        h();
        g();
    }

    public final void a(final bj bjVar) {
        if (b.c(this.c).a) {
            new Thread() {
                public final void run() {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(bjVar);
                    int a2 = bk.a().a((List<bj>) arrayList, a.b.c(a.this.c).c);
                    if (a2 > 0) {
                        HashMap hashMap = new HashMap();
                        hashMap.put("count", Integer.valueOf(a2));
                        hashMap.put("type", a.this.c);
                        hashMap.put("plId", Long.valueOf(bjVar.a));
                        com.inmobi.commons.core.e.b.a();
                        com.inmobi.commons.core.e.b.a("ads", "PreLoadPidOverflow", hashMap);
                    }
                }
            }.start();
        }
    }

    private void h() {
        if (b.c(this.c).a) {
            bk.a();
            int a2 = bk.a(b.c(this.c).b, this.c);
            if (a2 > 0) {
                try {
                    HashMap hashMap = new HashMap();
                    hashMap.put("type", this.c);
                    hashMap.put("count", Integer.valueOf(a2));
                    com.inmobi.commons.core.e.b.a();
                    com.inmobi.commons.core.e.b.a("ads", "PreLoadPidExpiry", hashMap);
                } catch (Exception e2) {
                    StringBuilder sb = new StringBuilder("Error in submitting telemetry event : (");
                    sb.append(e2.getMessage());
                    sb.append(")");
                }
            }
        }
    }

    @NonNull
    public static String a(Map<String, String> map) {
        if (map == null) {
            return "";
        }
        if (((String) map.get("tp")) == null) {
            return "";
        }
        return (String) map.get("tp");
    }

    public static void a(String str, Map<String, Object> map, f fVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", fVar.e);
        hashMap.put("plId", Long.valueOf(fVar.a));
        hashMap.put("isPreloaded", Integer.valueOf(1));
        hashMap.put("networkType", Integer.valueOf(com.inmobi.commons.core.utilities.b.b.a()));
        hashMap.put("ts", Long.valueOf(System.currentTimeMillis()));
        if (map.get("clientRequestId") == null) {
            hashMap.put("clientRequestId", fVar.i);
        }
        for (Entry entry : map.entrySet()) {
            hashMap.put(entry.getKey(), entry.getValue());
        }
        try {
            com.inmobi.commons.core.e.b.a();
            com.inmobi.commons.core.e.b.a("ads", str, hashMap);
        } catch (Exception e2) {
            StringBuilder sb = new StringBuilder("Error in submitting telemetry event : (");
            sb.append(e2.getMessage());
            sb.append(")");
        }
    }

    public final void a() {
        Application application = (Application) com.inmobi.commons.a.a.b();
        if (application != null) {
            application.registerComponentCallbacks(new ComponentCallbacks2() {
                public final void onConfigurationChanged(Configuration configuration) {
                }

                public final void onLowMemory() {
                }

                public final void onTrimMemory(int i) {
                    if (i == 15) {
                        a.a(a.this);
                    }
                }
            });
        }
        h();
        g();
        if (b.c(this.c).a) {
            bk.a();
            ArrayList arrayList = (ArrayList) bk.a(this.c);
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                final bj bjVar = (bj) arrayList.get(i2);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    private e c;

                    public final void run() {
                        try {
                            Context b2 = com.inmobi.commons.a.a.b();
                            if (b2 != null) {
                                a.d;
                                StringBuilder sb = new StringBuilder("preFetchAdUnit. pid:");
                                sb.append(bjVar.a);
                                sb.append(" tp:");
                                sb.append(bjVar.b);
                                if (bjVar.c == null && bjVar.b != null) {
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("tp", bjVar.b);
                                    bjVar.c = hashMap;
                                }
                                this.c = new C0048a(bjVar);
                                i a2 = a.b(a.this.c, b2, bjVar);
                                if (a2 != null) {
                                    a2.e = bjVar.d;
                                    a2.f = bjVar.c;
                                    a2.n = true;
                                    a2.q = this.c;
                                    a2.a(true);
                                }
                            }
                        } catch (Exception e) {
                            a.d;
                            new StringBuilder("SDK encountered an unexpected error preloading ad units; ").append(e.getMessage());
                            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                        }
                    }
                });
            }
        }
    }

    static /* synthetic */ void a(a aVar) {
        Context b2 = com.inmobi.commons.a.a.b();
        if (b2 != null) {
            new Handler(b2.getMainLooper()).post(new Runnable() {
                public final void run() {
                    try {
                        a.d;
                        Iterator it = a.a.entrySet().iterator();
                        while (it.hasNext()) {
                            ((i) ((Entry) it.next()).getValue()).t();
                            it.remove();
                        }
                    } catch (Exception e) {
                        a.d;
                        new StringBuilder("SDK encountered unexpected error in flushing ad unit cache; ").append(e.getMessage());
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                    }
                }
            });
        }
    }
}
