package com.inmobi.ads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiAdRequestStatus.StatusCode;
import com.inmobi.ads.a.b;
import com.inmobi.ads.a.f;
import com.inmobi.ads.a.g;
import com.inmobi.ads.bj;
import com.inmobi.ads.c.d;
import com.inmobi.ads.i;
import com.inmobi.ads.p;
import com.inmobi.commons.core.utilities.b.e;
import com.inmobi.commons.core.utilities.c;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AdStore */
public class h implements com.inmobi.ads.e.a {
    /* access modifiers changed from: private */
    public static final String f = "h";
    /* access modifiers changed from: 0000 */
    @NonNull
    public final a a;
    @NonNull
    public final d b;
    /* access modifiers changed from: 0000 */
    @NonNull
    public f c;
    @NonNull
    public d d;
    long e = 0;
    private final g g = new g() {
        public final void a(b bVar) {
            String str;
            h.f;
            StringBuilder sb = new StringBuilder("onAssetsFetchFailure of batch ");
            if (bVar == null) {
                str = null;
            } else {
                str = bVar.toString();
            }
            sb.append(str);
            ArrayList<Long> arrayList = new ArrayList<>();
            if (bVar != null) {
                for (com.inmobi.ads.a.a aVar : bVar.a) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("url", aVar.d);
                    hashMap.put("latency", Long.valueOf(aVar.a));
                    hashMap.put("size", Long.valueOf(c.a(aVar.e)));
                    h.this.a.a("VideoAssetDownloadFailed", (Map<String, Object>) hashMap);
                    for (a aVar2 : h.this.b.b(aVar.d, h.this.c.c)) {
                        if (!arrayList.contains(Long.valueOf(aVar2.d))) {
                            arrayList.add(Long.valueOf(aVar2.d));
                        }
                    }
                }
            }
            if (!arrayList.contains(Long.valueOf(h.this.c.a))) {
                arrayList.add(Long.valueOf(h.this.c.a));
            }
            for (Long longValue : arrayList) {
                h.this.a.a(longValue.longValue(), false);
            }
        }

        public final void b(b bVar) {
            String str;
            h.f;
            StringBuilder sb = new StringBuilder("onAssetsFetchSuccess of batch ");
            if (bVar == null) {
                str = null;
            } else {
                str = bVar.toString();
            }
            sb.append(str);
            ArrayList<Long> arrayList = new ArrayList<>();
            if (bVar != null) {
                for (com.inmobi.ads.a.a aVar : bVar.a) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("url", aVar.d);
                    hashMap.put("latency", Long.valueOf(aVar.a));
                    hashMap.put("size", Long.valueOf(c.a(aVar.e)));
                    hashMap.put("clientRequestId", bVar.f);
                    if (aVar.j) {
                        h.this.a.a("GotCachedVideoAsset", (Map<String, Object>) hashMap);
                    } else {
                        h.this.a.a("VideoAssetDownloaded", (Map<String, Object>) hashMap);
                    }
                    List<a> a2 = h.this.b.a(aVar.d, h.this.c.c);
                    h.f;
                    StringBuilder sb2 = new StringBuilder("Found ");
                    sb2.append(a2.size());
                    sb2.append(" ads mapping to this asset");
                    for (a aVar2 : a2) {
                        if (!arrayList.contains(Long.valueOf(aVar2.d))) {
                            arrayList.add(Long.valueOf(aVar2.d));
                        }
                    }
                }
            }
            if (!arrayList.contains(Long.valueOf(h.this.c.a))) {
                arrayList.add(Long.valueOf(h.this.c.a));
            }
            for (Long longValue : arrayList) {
                long longValue2 = longValue.longValue();
                h.f;
                StringBuilder sb3 = new StringBuilder("Notifying ad unit with placement ID (");
                sb3.append(longValue2);
                sb3.append(")");
                h.this.a.a(longValue2, true);
            }
        }
    };

    /* compiled from: AdStore */
    public interface a {
        void a(long j, InMobiAdRequestStatus inMobiAdRequestStatus);

        void a(long j, @NonNull a aVar);

        void a(long j, boolean z);

        void a(String str, Map<String, Object> map);

        void b(long j, a aVar);
    }

    public h(@NonNull a aVar, @NonNull d dVar, @NonNull f fVar) {
        this.a = aVar;
        this.b = d.a();
        this.d = dVar;
        this.c = fVar;
    }

    public static void a() {
        if (e.b()) {
            d.c();
        }
    }

    public static void c() {
        b.b();
    }

    /* access modifiers changed from: 0000 */
    public final void a(final a aVar) {
        new Thread() {
            public final void run() {
                h.this.b;
                d.a(aVar);
            }
        }.start();
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull final String str) {
        new Thread() {
            public final void run() {
                h.this.b;
                d.a(str);
            }
        }.start();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final String a(f fVar, boolean z) {
        b(fVar, z);
        this.e = SystemClock.elapsedRealtime();
        new e(fVar, this).a();
        HashMap hashMap = new HashMap();
        hashMap.put("isPreloaded", fVar.c());
        hashMap.put("clientRequestId", fVar.i);
        hashMap.put("im-accid", com.inmobi.commons.a.a.e());
        this.a.a("ServerCallInitiated", (Map<String, Object>) hashMap);
        return fVar.i;
    }

    private void a(List<a> list, @NonNull String str, @Nullable String str2) {
        this.b.a(list, this.c.a, this.d.a, this.c.e, this.c.j, str, str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x003a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0092  */
    private void a(List<a> list, String str) {
        char c2;
        a aVar = (a) list.get(0);
        String upperCase = aVar.e().toUpperCase(Locale.ENGLISH);
        int hashCode = upperCase.hashCode();
        if (hashCode == -598127114) {
            if (upperCase.equals("INMOBIJSON")) {
                c2 = 1;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }
        } else if (hashCode == 2228139 && upperCase.equals("HTML")) {
            c2 = 0;
            switch (c2) {
                case 0:
                    if ("native".equals(this.c.e)) {
                        this.a.a(this.c.a, new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
                        return;
                    }
                    a(list.subList(1, list.size()), str, null);
                    this.a.a(this.c.a, aVar);
                    a(this.c);
                    return;
                case 1:
                    a(list, str, null);
                    if ("int".equals(this.c.e)) {
                        this.a.b(this.c.a, aVar);
                    } else if ("native".equals(this.c.e)) {
                        a b2 = this.b.b(this.c.a, this.c.c, this.c.j, str);
                        if (b2 == null) {
                            b2 = aVar;
                        } else if (!aVar.a(b2)) {
                            list.add(0, b2);
                        }
                        this.a.a(this.c.a, b2);
                        a(this.c);
                    }
                    a(list);
                    return;
                default:
                    return;
            }
        }
        c2 = 65535;
        switch (c2) {
            case 0:
                break;
            case 1:
                break;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0055 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00bb  */
    private void b(List<a> list, String str, @NonNull String str2) {
        char c2;
        a(list, str, str2);
        String str3 = this.c.e;
        b.b();
        a c3 = d.c(str2);
        if (c3 != null) {
            String upperCase = c3.e().toUpperCase(Locale.ENGLISH);
            int hashCode = upperCase.hashCode();
            char c4 = 65535;
            boolean z = false;
            if (hashCode == -598127114) {
                if (upperCase.equals("INMOBIJSON")) {
                    c2 = 1;
                    switch (c2) {
                        case 0:
                            break;
                        case 1:
                            break;
                    }
                }
            } else if (hashCode == 2228139 && upperCase.equals("HTML")) {
                c2 = 0;
                switch (c2) {
                    case 0:
                        d.a(str2);
                        this.a.a(this.c.a, c3);
                        a(this.c);
                        return;
                    case 1:
                        String str4 = this.c.e;
                        int hashCode2 = str4.hashCode();
                        if (hashCode2 != -1052618729) {
                            if (hashCode2 == 104431 && str4.equals("int")) {
                                c4 = 0;
                            }
                        } else if (str4.equals("native")) {
                            c4 = 1;
                        }
                        switch (c4) {
                            case 0:
                                this.a.b(this.c.a, c3);
                                break;
                            case 1:
                                d.a(str2);
                                this.a.a(this.c.a, c3);
                                a(this.c);
                                break;
                        }
                        Iterator it = list.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (c3.a((a) it.next())) {
                                    z = true;
                                }
                            }
                        }
                        if (!z) {
                            list.add(c3);
                        }
                        a(list);
                        return;
                    default:
                        return;
                }
            }
            c2 = 65535;
            switch (c2) {
                case 0:
                    break;
                case 1:
                    break;
            }
        } else {
            this.a.a(this.c.a, new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
        }
    }

    public final void a(g gVar) {
        StringBuilder sb = new StringBuilder();
        List<a> a2 = a(gVar, sb);
        String sb2 = sb.toString();
        boolean isEmpty = TextUtils.isEmpty(sb2);
        if (a2 == null) {
            new StringBuilder("Could not parse ad response:").append(gVar.a.b());
            this.a.a(this.c.a, new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
        } else if (a2.size() != 0 || !isEmpty) {
            HashMap hashMap = new HashMap();
            hashMap.put("numberOfAdsReturned", Integer.valueOf(a2.size()));
            hashMap.put("latency", Long.valueOf(SystemClock.elapsedRealtime() - this.e));
            hashMap.put("isPreloaded", this.c.c());
            hashMap.put("im-accid", com.inmobi.commons.a.a.e());
            this.a.a("ServerFill", (Map<String, Object>) hashMap);
            for (a aVar : a2) {
                HashMap hashMap2 = new HashMap();
                hashMap2.put("ts", Long.valueOf(System.currentTimeMillis()));
                hashMap2.put("impId", aVar.g);
                hashMap2.put("plId", Long.valueOf(aVar.d));
                this.a.a("AdCacheImpressionInserted", (Map<String, Object>) hashMap2);
            }
            String a3 = com.inmobi.ads.d.a.a(this.c.g);
            if (isEmpty) {
                a(a2, a3);
            } else {
                b(a2, a3, sb2);
            }
        } else {
            new StringBuilder("Ad response received but no ad available:").append(gVar.a.b());
            HashMap hashMap3 = new HashMap();
            hashMap3.put("latency", Long.valueOf(SystemClock.elapsedRealtime() - this.e));
            hashMap3.put("isPreloaded", this.c.c());
            hashMap3.put("im-accid", com.inmobi.commons.a.a.e());
            this.a.a("ServerNoFill", (Map<String, Object>) hashMap3);
            this.a.a(this.c.a, new InMobiAdRequestStatus(StatusCode.NO_FILL));
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(List<a> list) {
        if (list != null && list.size() > 0) {
            a aVar = (a) list.get(0);
            if (aVar != null) {
                Set d2 = aVar.d();
                if (d2.size() == 0) {
                    this.a.a(this.c.a, true);
                    return;
                } else {
                    f.a().a(new b(UUID.randomUUID().toString(), aVar.h, d2, this.g));
                }
            }
            for (a aVar2 : list.subList(1, list.size())) {
                if (aVar2 != null) {
                    Set d3 = aVar2.d();
                    if (d3.size() != 0) {
                        f.a().a(new b(UUID.randomUUID().toString(), aVar2.h, d3, (g) null));
                    }
                }
            }
        }
    }

    @Nullable
    private List<a> a(g gVar, @Nullable StringBuilder sb) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONObject jSONObject = new JSONObject(gVar.a.b());
            sb.append(jSONObject.optString("winnerImpressionId").trim());
            JSONArray jSONArray = jSONObject.getJSONArray("ads");
            if (jSONArray != null) {
                int min = Math.min(gVar.c.d, jSONArray.length());
                for (int i = 0; i < min; i++) {
                    a a2 = C0044a.a(jSONArray.getJSONObject(i), gVar.c.a, gVar.c.e, gVar.c.c, gVar.c.i, gVar.c.j, gVar.c.k);
                    if (a2 != null) {
                        arrayList.add(a2);
                    }
                }
                if (min <= 0 || !arrayList.isEmpty()) {
                    return arrayList;
                }
                return null;
            }
        } catch (JSONException e2) {
            HashMap hashMap = new HashMap();
            hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, "ParsingError");
            hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, e2.getLocalizedMessage());
            hashMap.put("latency", Long.valueOf(SystemClock.elapsedRealtime() - this.e));
            hashMap.put("im-accid", com.inmobi.commons.a.a.e());
            this.a.a("ServerError", (Map<String, Object>) hashMap);
            arrayList = null;
        }
        return arrayList;
    }

    public final void b(g gVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, String.valueOf(gVar.a.b.a.getValue()));
        hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, gVar.a.b.b);
        hashMap.put("latency", Long.valueOf(SystemClock.elapsedRealtime() - this.e));
        hashMap.put("im-accid", com.inmobi.commons.a.a.e());
        this.a.a("ServerError", (Map<String, Object>) hashMap);
        this.a.a(this.c.a, gVar.b);
    }

    /* access modifiers changed from: 0000 */
    public final String b() {
        String a2 = com.inmobi.ads.d.a.a(this.c.g);
        b.b();
        a aVar = null;
        if (d.a(this.c.a, this.c.c, this.c.j, a2) != 0) {
            a b2 = this.b.b(this.c.a, this.c.c, this.c.j, a2);
            if (b2 != null) {
                HashMap hashMap = new HashMap();
                hashMap.put("clientRequestId", b2.h);
                hashMap.put("im-accid", com.inmobi.commons.a.a.e());
                hashMap.put("isPreloaded", this.c.c());
                this.a.a("AdCacheHit", (Map<String, Object>) hashMap);
                a(this.c);
                aVar = b2;
            }
        }
        if (aVar != null) {
            String str = aVar.h;
            this.a.a(this.c.a, aVar);
            if (!"INMOBIJSON".equalsIgnoreCase(aVar.e())) {
                return str;
            }
            a((List<a>) new ArrayList<a>(Collections.singletonList(aVar)));
            return str;
        } else if (this.c.c().equals("1")) {
            return a(this.c, true);
        } else {
            return a(this.c, false);
        }
    }

    private static void b(f fVar, boolean z) {
        if (fVar != null) {
            Map<String, String> map = fVar.h;
            if (map == null) {
                map = new HashMap<>();
            }
            map.put("preload-request", String.valueOf(z ? 1 : 0));
            fVar.h = map;
        }
    }

    public final void a(@NonNull f fVar) {
        b.b();
        int a2 = d.a(fVar.a, fVar.c, fVar.j, com.inmobi.ads.d.a.a(fVar.g));
        boolean equals = "int".equals(fVar.e);
        if (a2 < this.d.c) {
            new StringBuilder("Cached ad count below threshold, firing ad request for Placement : ").append(fVar.a);
            com.inmobi.ads.d.a a3 = com.inmobi.ads.d.a.a(fVar.e);
            if (equals) {
                b(fVar, true);
                try {
                    new bm(new com.inmobi.ads.bm.a(fVar) {
                        final /* synthetic */ com.inmobi.ads.f a;

                        {
                            this.a = r2;
                        }

                        public final void a(long j) {
                            a.d;
                        }

                        public final void b(long j, InMobiAdRequestStatus inMobiAdRequestStatus) {
                            a.d;
                            new StringBuilder("Interstitial Prefetch failed with the message - ").append(inMobiAdRequestStatus.getMessage());
                        }

                        public final void a(String str, Map<String, Object> map) {
                            a.a(str, map, this.a);
                        }
                    }, this.d).a(fVar, true, com.inmobi.ads.d.a.b.c);
                } catch (com.inmobi.ads.b.a e2) {
                    e2.getMessage();
                }
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable(fVar) {
                    final /* synthetic */ com.inmobi.ads.f a;
                    private i.e c;

                    {
                        this.a = r2;
                    }

                    public final void run() {
                        try {
                            Context b2 = com.inmobi.commons.a.a.b();
                            if (b2 != null) {
                                bj a2 = bj.a(this.a.a, this.a.g, this.a.e, this.a.f);
                                a2.f = this.a.j;
                                a.d;
                                StringBuilder sb = new StringBuilder("preFetchAdUnit. pid:");
                                sb.append(a2.a);
                                sb.append(" tp:");
                                sb.append(a2.b);
                                if (a2.c == null && a2.b != null) {
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("tp", a2.b);
                                    a2.c = hashMap;
                                }
                                this.c = new C0048a(a2);
                                i a3 = a.b(a.this.c, b2, a2);
                                if (a3 != null) {
                                    a3.e = a2.d;
                                    a3.f = a2.c;
                                    a3.n = true;
                                    a3.q = this.c;
                                    if (a.this.c.equalsIgnoreCase("banner")) {
                                        ((p) a3).B = this.a.c;
                                        ((p) a3).z = true;
                                    }
                                    a3.a(true);
                                }
                            }
                        } catch (Exception e) {
                            a.d;
                            new StringBuilder("SDK encountered an unexpected error preloading ad units; ").append(e.getMessage());
                            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                        }
                    }
                });
            }
        }
    }
}
