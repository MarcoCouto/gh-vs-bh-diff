package com.inmobi.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.inmobi.commons.core.network.c;
import com.inmobi.commons.core.utilities.uid.d;
import io.fabric.sdk.android.services.network.HttpRequest;

/* compiled from: IceNetworkRequest */
public class j extends c {
    private static final String c = "j";
    int a;
    int b;

    j(String str, int i, int i2, d dVar, k kVar) {
        super(HttpRequest.METHOD_POST, str, true, dVar);
        this.a = i;
        this.b = i2;
        this.n.put(MessengerShareContentUtility.ATTACHMENT_PAYLOAD, kVar.a().toString());
    }
}
