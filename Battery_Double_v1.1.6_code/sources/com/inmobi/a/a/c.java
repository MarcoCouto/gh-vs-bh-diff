package com.inmobi.a.a;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.location.LocationManager;
import android.os.Build.VERSION;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.facebook.places.model.PlaceFields;
import com.inmobi.a.o;
import com.inmobi.a.p.b;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.utilities.e;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;

@TargetApi(17)
/* compiled from: CellularInfoUtil */
public class c {
    private static final String a = "c";

    private static boolean a(int i, int i2) {
        return (i & i2) == i2;
    }

    public static Map<String, String> a() {
        Object obj;
        HashMap hashMap = new HashMap();
        Context b = a.b();
        if (b == null) {
            return hashMap;
        }
        b bVar = o.a().a.a;
        if (!(bVar.n && bVar.a)) {
            return hashMap;
        }
        int i = o.a().a.a.m;
        boolean a2 = a(i, 2);
        boolean a3 = a(i, 1);
        a aVar = new a();
        TelephonyManager telephonyManager = (TelephonyManager) b.getSystemService(PlaceFields.PHONE);
        if (!a2) {
            int[] a4 = a(telephonyManager.getNetworkOperator());
            aVar.a = a4[0];
            aVar.b = a4[1];
            String networkCountryIso = telephonyManager.getNetworkCountryIso();
            if (networkCountryIso != null) {
                aVar.e = networkCountryIso.toLowerCase(Locale.ENGLISH);
            }
        }
        if (!a3) {
            int[] a5 = a(telephonyManager.getSimOperator());
            aVar.c = a5[0];
            aVar.d = a5[1];
        }
        String str = "s-ho";
        String str2 = null;
        if (aVar.c == -1 && aVar.d == -1) {
            obj = null;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(aVar.c);
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(aVar.d);
            obj = sb.toString();
        }
        hashMap.put(str, obj);
        String str3 = "s-co";
        if (!(aVar.a == -1 && aVar.b == -1)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(aVar.a);
            sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb2.append(aVar.b);
            str2 = sb2.toString();
        }
        hashMap.put(str3, str2);
        hashMap.put("s-iso", aVar.e);
        return hashMap;
    }

    private static int[] a(String str) {
        int[] iArr = {-1, -1};
        if (str == null || str.equals("")) {
            return iArr;
        }
        try {
            int parseInt = Integer.parseInt(str.substring(0, 3));
            int parseInt2 = Integer.parseInt(str.substring(3));
            iArr[0] = parseInt;
            iArr[1] = parseInt2;
        } catch (IndexOutOfBoundsException | NumberFormatException unused) {
        }
        return iArr;
    }

    @SuppressLint({"NewApi"})
    private static boolean d() {
        if (VERSION.SDK_INT < 28) {
            return true;
        }
        LocationManager locationManager = (LocationManager) a.b().getSystemService("location");
        if (locationManager != null) {
            return locationManager.isLocationEnabled();
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0047  */
    public static Map<String, String> b() {
        boolean z;
        Context b;
        b bVar = o.a().a.a;
        b bVar2 = null;
        if (bVar.p && bVar.a) {
            Context b2 = a.b();
            if (b2 != null) {
                boolean a2 = e.a(b2, "signals", "android.permission.ACCESS_COARSE_LOCATION");
                boolean a3 = e.a(b2, "signals", "android.permission.ACCESS_FINE_LOCATION");
                if (a2 || a3) {
                    z = true;
                    if (z && d()) {
                        b = a.b();
                        if (b != null) {
                            TelephonyManager telephonyManager = (TelephonyManager) b.getSystemService(PlaceFields.PHONE);
                            int[] a4 = a(telephonyManager.getNetworkOperator());
                            String valueOf = String.valueOf(a4[0]);
                            String valueOf2 = String.valueOf(a4[1]);
                            if (VERSION.SDK_INT >= 17) {
                                List allCellInfo = telephonyManager.getAllCellInfo();
                                if (allCellInfo != null) {
                                    CellInfo cellInfo = null;
                                    for (int i = 0; i < allCellInfo.size(); i++) {
                                        cellInfo = (CellInfo) allCellInfo.get(i);
                                        if (cellInfo.isRegistered()) {
                                            break;
                                        }
                                    }
                                    if (cellInfo != null) {
                                        bVar2 = new b(cellInfo, valueOf, valueOf2, telephonyManager.getNetworkType());
                                    }
                                }
                            }
                            CellLocation cellLocation = telephonyManager.getCellLocation();
                            if (!(cellLocation == null || a4[0] == -1)) {
                                bVar2 = new b();
                                if (cellLocation instanceof CdmaCellLocation) {
                                    CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                                    bVar2.b = Integer.MAX_VALUE;
                                    bVar2.c = telephonyManager.getNetworkType();
                                    bVar2.a = b.a(valueOf, cdmaCellLocation.getSystemId(), cdmaCellLocation.getNetworkId(), cdmaCellLocation.getBaseStationId());
                                } else {
                                    GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                                    bVar2.b = Integer.MAX_VALUE;
                                    bVar2.c = telephonyManager.getNetworkType();
                                    bVar2.a = b.a(valueOf, valueOf2, gsmCellLocation.getLac(), gsmCellLocation.getCid(), gsmCellLocation.getPsc(), Integer.MAX_VALUE);
                                }
                            }
                        }
                    }
                }
            }
            z = false;
            b = a.b();
            if (b != null) {
            }
        }
        HashMap hashMap = new HashMap();
        if (bVar2 != null) {
            hashMap.put("c-sc", bVar2.a().toString());
        }
        return hashMap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x0125  */
    public static Map<String, String> c() {
        ArrayList arrayList;
        if (a.a()) {
            boolean z = false;
            if ((a.a() && e.a(a.b(), "signals", "android.permission.ACCESS_COARSE_LOCATION")) && d()) {
                b bVar = o.a().a.a;
                if (bVar.o && bVar.a) {
                    Context b = a.b();
                    if (b == null) {
                        arrayList = new ArrayList();
                    } else {
                        TelephonyManager telephonyManager = (TelephonyManager) b.getSystemService(PlaceFields.PHONE);
                        ArrayList arrayList2 = new ArrayList();
                        int[] a2 = a(telephonyManager.getNetworkOperator());
                        String valueOf = String.valueOf(a2[0]);
                        String valueOf2 = String.valueOf(a2[1]);
                        if (VERSION.SDK_INT >= 17) {
                            List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
                            if (allCellInfo != null) {
                                for (CellInfo cellInfo : allCellInfo) {
                                    if (!cellInfo.isRegistered()) {
                                        arrayList2.add(new b(cellInfo, valueOf, valueOf2, telephonyManager.getNetworkType()));
                                    }
                                }
                                arrayList = arrayList2;
                            }
                        }
                        List neighboringCellInfo = telephonyManager.getNeighboringCellInfo();
                        if (neighboringCellInfo == null || neighboringCellInfo.isEmpty()) {
                            arrayList = new ArrayList();
                        } else {
                            Iterator it = neighboringCellInfo.iterator();
                            if (it.hasNext()) {
                                NeighboringCellInfo neighboringCellInfo2 = (NeighboringCellInfo) it.next();
                                b bVar2 = new b();
                                int networkType = neighboringCellInfo2.getNetworkType();
                                bVar2.c = networkType;
                                if (neighboringCellInfo2.getRssi() == 99) {
                                    bVar2.b = Integer.MAX_VALUE;
                                } else {
                                    if (!(networkType == 3 || networkType == 15)) {
                                        switch (networkType) {
                                            case 8:
                                            case 9:
                                            case 10:
                                                break;
                                        }
                                    }
                                    z = true;
                                    if (z) {
                                        bVar2.b = neighboringCellInfo2.getRssi() - 116;
                                    } else {
                                        bVar2.b = (neighboringCellInfo2.getRssi() * 2) - 113;
                                    }
                                }
                                bVar2.a = b.a(valueOf, valueOf2, neighboringCellInfo2.getLac(), neighboringCellInfo2.getCid(), -1, Integer.MAX_VALUE);
                                arrayList2.add(bVar2);
                                arrayList = arrayList2;
                            } else {
                                arrayList = new ArrayList();
                            }
                        }
                    }
                    HashMap hashMap = new HashMap();
                    if (!arrayList.isEmpty()) {
                        JSONArray jSONArray = new JSONArray();
                        jSONArray.put(((b) arrayList.get(arrayList.size() - 1)).a());
                        hashMap.put("v-sc", jSONArray.toString());
                    }
                    return hashMap;
                }
            }
        }
        arrayList = new ArrayList();
        HashMap hashMap2 = new HashMap();
        if (!arrayList.isEmpty()) {
        }
        return hashMap2;
    }
}
