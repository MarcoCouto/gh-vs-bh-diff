package com.unity3d.services.core.request;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.unity3d.services.core.log.DeviceLog;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

public class WebRequest {
    private String _body;
    private boolean _canceled;
    private int _connectTimeout;
    private long _contentLength;
    private Map<String, List<String>> _headers;
    private IWebRequestProgressListener _progressListener;
    private int _readTimeout;
    private String _requestType;
    private int _responseCode;
    private Map<String, List<String>> _responseHeaders;
    private URL _url;

    public enum RequestType {
        POST,
        GET,
        HEAD
    }

    public WebRequest(String str, String str2, Map<String, List<String>> map) throws MalformedURLException {
        this(str, str2, map, 30000, 30000);
    }

    public WebRequest(String str, String str2, Map<String, List<String>> map, int i, int i2) throws MalformedURLException {
        this._requestType = RequestType.GET.name();
        this._responseCode = -1;
        this._contentLength = -1;
        this._canceled = false;
        this._url = new URL(str);
        this._requestType = str2;
        this._headers = map;
        this._connectTimeout = i;
        this._readTimeout = i2;
    }

    public void cancel() {
        this._canceled = true;
    }

    public boolean isCanceled() {
        return this._canceled;
    }

    public URL getUrl() {
        return this._url;
    }

    public String getRequestType() {
        return this._requestType;
    }

    public String getBody() {
        return this._body;
    }

    public void setBody(String str) {
        this._body = str;
    }

    public String getQuery() {
        if (this._url != null) {
            return this._url.getQuery();
        }
        return null;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return this._responseHeaders;
    }

    public Map<String, List<String>> getHeaders() {
        return this._headers;
    }

    public int getResponseCode() {
        return this._responseCode;
    }

    public long getContentLength() {
        return this._contentLength;
    }

    public int getConnectTimeout() {
        return this._connectTimeout;
    }

    public void setConnectTimeout(int i) {
        this._connectTimeout = i;
    }

    public int getReadTimeout() {
        return this._readTimeout;
    }

    public void setReadTimeout(int i) {
        this._readTimeout = i;
    }

    public void setProgressListener(IWebRequestProgressListener iWebRequestProgressListener) {
        this._progressListener = iWebRequestProgressListener;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x007d A[SYNTHETIC, Splitter:B:27:0x007d] */
    public long makeStreamRequest(OutputStream outputStream) throws Exception {
        InputStream inputStream;
        HttpURLConnection httpUrlConnectionWithHeaders = getHttpUrlConnectionWithHeaders();
        httpUrlConnectionWithHeaders.setDoInput(true);
        if (getRequestType().equals(RequestType.POST.name())) {
            httpUrlConnectionWithHeaders.setDoOutput(true);
            PrintWriter printWriter = null;
            try {
                PrintWriter printWriter2 = new PrintWriter(new OutputStreamWriter(httpUrlConnectionWithHeaders.getOutputStream(), "UTF-8"), true);
                try {
                    if (getBody() == null) {
                        printWriter2.print(getQuery());
                    } else {
                        printWriter2.print(getBody());
                    }
                    printWriter2.flush();
                    try {
                        printWriter2.close();
                    } catch (Exception e) {
                        Exception exc = e;
                        DeviceLog.exception("Error closing writer", exc);
                        throw exc;
                    }
                } catch (IOException e2) {
                    e = e2;
                    printWriter = printWriter2;
                    try {
                        DeviceLog.exception("Error while writing POST params", e);
                        StringBuilder sb = new StringBuilder();
                        sb.append("Error writing POST params: ");
                        sb.append(e.getMessage());
                        throw new NetworkIOException(sb.toString());
                    } catch (Throwable th) {
                        th = th;
                        if (printWriter != null) {
                            try {
                                printWriter.close();
                            } catch (Exception e3) {
                                Exception exc2 = e3;
                                DeviceLog.exception("Error closing writer", exc2);
                                throw exc2;
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    printWriter = printWriter2;
                    if (printWriter != null) {
                    }
                    throw th;
                }
            } catch (IOException e4) {
                e = e4;
                DeviceLog.exception("Error while writing POST params", e);
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Error writing POST params: ");
                sb2.append(e.getMessage());
                throw new NetworkIOException(sb2.toString());
            }
        }
        try {
            this._responseCode = httpUrlConnectionWithHeaders.getResponseCode();
            this._contentLength = (long) httpUrlConnectionWithHeaders.getContentLength();
            if (httpUrlConnectionWithHeaders.getHeaderFields() != null) {
                this._responseHeaders = httpUrlConnectionWithHeaders.getHeaderFields();
            }
            try {
                inputStream = httpUrlConnectionWithHeaders.getInputStream();
            } catch (IOException e5) {
                IOException iOException = e5;
                inputStream = httpUrlConnectionWithHeaders.getErrorStream();
                if (inputStream == null) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Can't open error stream: ");
                    sb3.append(iOException.getMessage());
                    throw new NetworkIOException(sb3.toString());
                }
            }
            if (this._progressListener != null) {
                this._progressListener.onRequestStart(getUrl().toString(), this._contentLength, this._responseCode, this._responseHeaders);
            }
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            byte[] bArr = new byte[4096];
            long j = 0;
            int i = 0;
            while (!isCanceled() && i != -1) {
                try {
                    i = bufferedInputStream.read(bArr);
                    if (i > 0) {
                        outputStream.write(bArr, 0, i);
                        j += (long) i;
                        if (this._progressListener != null) {
                            this._progressListener.onRequestProgress(getUrl().toString(), j, this._contentLength);
                        }
                    } else {
                        OutputStream outputStream2 = outputStream;
                    }
                } catch (IOException e6) {
                    IOException iOException2 = e6;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("Network exception: ");
                    sb4.append(iOException2.getMessage());
                    throw new NetworkIOException(sb4.toString());
                } catch (Exception e7) {
                    Exception exc3 = e7;
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("Unknown Exception: ");
                    sb5.append(exc3.getMessage());
                    throw new Exception(sb5.toString());
                }
            }
            OutputStream outputStream3 = outputStream;
            httpUrlConnectionWithHeaders.disconnect();
            outputStream.flush();
            return j;
        } catch (IOException | RuntimeException e8) {
            StringBuilder sb6 = new StringBuilder();
            sb6.append("Response code: ");
            sb6.append(e8.getMessage());
            throw new NetworkIOException(sb6.toString());
        }
    }

    public String makeRequest() throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        makeStreamRequest(byteArrayOutputStream);
        return byteArrayOutputStream.toString("UTF-8");
    }

    private HttpURLConnection getHttpUrlConnectionWithHeaders() throws NetworkIOException, IllegalArgumentException {
        HttpURLConnection httpURLConnection;
        if (getUrl().toString().startsWith("https://")) {
            try {
                httpURLConnection = (HttpsURLConnection) getUrl().openConnection();
            } catch (IOException e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Open HTTPS connection: ");
                sb.append(e.getMessage());
                throw new NetworkIOException(sb.toString());
            }
        } else if (getUrl().toString().startsWith("http://")) {
            try {
                httpURLConnection = (HttpURLConnection) getUrl().openConnection();
            } catch (IOException e2) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Open HTTP connection: ");
                sb2.append(e2.getMessage());
                throw new NetworkIOException(sb2.toString());
            }
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Invalid url-protocol in url: ");
            sb3.append(getUrl().toString());
            throw new IllegalArgumentException(sb3.toString());
        }
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setConnectTimeout(getConnectTimeout());
        httpURLConnection.setReadTimeout(getReadTimeout());
        try {
            httpURLConnection.setRequestMethod(getRequestType());
            if (getHeaders() != null && getHeaders().size() > 0) {
                for (String str : getHeaders().keySet()) {
                    for (String str2 : (List) getHeaders().get(str)) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("Setting header: ");
                        sb4.append(str);
                        sb4.append(RequestParameters.EQUAL);
                        sb4.append(str2);
                        DeviceLog.debug(sb4.toString());
                        httpURLConnection.setRequestProperty(str, str2);
                    }
                }
            }
            return httpURLConnection;
        } catch (ProtocolException e3) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("Set Request Method: ");
            sb5.append(getRequestType());
            sb5.append(", ");
            sb5.append(e3.getMessage());
            throw new NetworkIOException(sb5.toString());
        }
    }
}
