package com.unity3d.services.core.misc;

import android.text.TextUtils;
import com.unity3d.services.core.log.DeviceLog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonStorage {
    private JSONObject _data;

    public synchronized boolean initData() {
        if (this._data != null) {
            return false;
        }
        this._data = new JSONObject();
        return true;
    }

    public synchronized void setData(JSONObject jSONObject) {
        this._data = jSONObject;
    }

    public synchronized JSONObject getData() {
        return this._data;
    }

    public synchronized boolean hasData() {
        if (this._data == null || this._data.length() <= 0) {
            return false;
        }
        return true;
    }

    public synchronized void clearData() {
        this._data = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        return true;
     */
    public synchronized boolean set(String str, Object obj) {
        if (!(this._data == null || str == null || str.length() == 0)) {
            if (obj != null) {
                createObjectTree(getParentObjectTreeFor(str));
                if (findObject(getParentObjectTreeFor(str)) instanceof JSONObject) {
                    JSONObject jSONObject = (JSONObject) findObject(getParentObjectTreeFor(str));
                    String[] split = str.split("\\.");
                    if (jSONObject != null) {
                        try {
                            jSONObject.put(split[split.length - 1], obj);
                        } catch (JSONException e) {
                            DeviceLog.exception("Couldn't set value", e);
                            return false;
                        }
                    }
                } else {
                    DeviceLog.debug("Cannot set subvalue to an object that is not JSONObject");
                    return false;
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Storage not properly initialized or incorrect parameters:");
        sb.append(this._data);
        sb.append(", ");
        sb.append(str);
        sb.append(", ");
        sb.append(obj);
        DeviceLog.error(sb.toString());
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004a, code lost:
        return null;
     */
    public synchronized Object get(String str) {
        Object obj;
        obj = null;
        if (this._data == null) {
            DeviceLog.error("Data is NULL, readStorage probably not called");
            return null;
        }
        String[] split = str.split("\\.");
        if (findObject(getParentObjectTreeFor(str)) instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) findObject(getParentObjectTreeFor(str));
            if (jSONObject != null) {
                try {
                    if (jSONObject.has(split[split.length - 1])) {
                        obj = jSONObject.get(split[split.length - 1]);
                    }
                } catch (Exception e) {
                    DeviceLog.exception("Error getting data", e);
                }
            }
        }
        return obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0071, code lost:
        return r2;
     */
    public synchronized List<String> getKeys(String str, boolean z) {
        List<String> list;
        if (!(get(str) instanceof JSONObject)) {
            return null;
        }
        JSONObject jSONObject = (JSONObject) get(str);
        ArrayList arrayList = new ArrayList();
        if (jSONObject != null) {
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str2 = (String) keys.next();
                if (z) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(".");
                    sb.append(str2);
                    list = getKeys(sb.toString(), z);
                } else {
                    list = null;
                }
                arrayList.add(str2);
                if (list != null) {
                    for (String str3 : list) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str2);
                        sb2.append(".");
                        sb2.append(str3);
                        arrayList.add(sb2.toString());
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
        return false;
     */
    public synchronized boolean delete(String str) {
        if (this._data == null) {
            DeviceLog.error("Data is NULL, readStorage probably not called");
            return false;
        }
        String[] split = str.split("\\.");
        if (findObject(getParentObjectTreeFor(str)) instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) findObject(getParentObjectTreeFor(str));
            if (!(jSONObject == null || jSONObject.remove(split[split.length - 1]) == null)) {
                return true;
            }
        }
    }

    private synchronized Object findObject(String str) {
        String[] split = str.split("\\.");
        JSONObject jSONObject = this._data;
        if (str.length() == 0) {
            return jSONObject;
        }
        int i = 0;
        while (i < split.length) {
            if (!jSONObject.has(split[i])) {
                return null;
            }
            try {
                jSONObject = jSONObject.getJSONObject(split[i]);
                i++;
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Couldn't read JSONObject: ");
                sb.append(split[i]);
                DeviceLog.exception(sb.toString(), e);
                return null;
            }
        }
        return jSONObject;
    }

    private synchronized void createObjectTree(String str) {
        String[] split = str.split("\\.");
        JSONObject jSONObject = this._data;
        if (str.length() != 0) {
            for (int i = 0; i < split.length; i++) {
                if (!jSONObject.has(split[i])) {
                    try {
                        JSONObject put = jSONObject.put(split[i], new JSONObject());
                        try {
                            jSONObject = put.getJSONObject(split[i]);
                        } catch (Exception e) {
                            JSONObject jSONObject2 = put;
                            e = e;
                            jSONObject = jSONObject2;
                            DeviceLog.exception("Couldn't create new JSONObject", e);
                        }
                    } catch (Exception e2) {
                        e = e2;
                        DeviceLog.exception("Couldn't create new JSONObject", e);
                    }
                } else {
                    try {
                        jSONObject = jSONObject.getJSONObject(split[i]);
                    } catch (Exception e3) {
                        DeviceLog.exception("Couldn't get existing JSONObject", e3);
                    }
                }
            }
        }
    }

    private synchronized String getParentObjectTreeFor(String str) {
        ArrayList arrayList;
        arrayList = new ArrayList(Arrays.asList(str.split("\\.")));
        arrayList.remove(arrayList.size() - 1);
        return TextUtils.join(".", arrayList.toArray());
    }
}
