package com.ogury.core;

import com.ogury.cm.accbb;

public final class OguryError extends Exception {
    private final int errorCode;

    public OguryError(int i, String str) {
        accbb.b(str, "message");
        super(str);
        this.errorCode = i;
    }

    public final int getErrorCode() {
        return this.errorCode;
    }
}
