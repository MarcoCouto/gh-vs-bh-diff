package com.ogury.crashreport;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.Arrays;

/* compiled from: Closeable.kt */
public class tx4320 {
    public static final void a(Closeable closeable, Throwable th) {
        if (th == null) {
            closeable.close();
            return;
        }
        try {
            closeable.close();
        } catch (Throwable th2) {
            tx7920.b(th, "receiver$0");
            tx7920.b(th2, "exception");
            tx2880.a.a(th, th2);
        }
    }

    public static byte[] a(File file) {
        Throwable th;
        tx7920.b(file, "receiver$0");
        Closeable fileInputStream = new FileInputStream(file);
        try {
            FileInputStream fileInputStream2 = (FileInputStream) fileInputStream;
            int i = 0;
            long length = file.length();
            if (length <= 2147483647L) {
                int i2 = (int) length;
                byte[] bArr = new byte[i2];
                while (i2 > 0) {
                    int read = fileInputStream2.read(bArr, i, i2);
                    if (read < 0) {
                        break;
                    }
                    i2 -= read;
                    i += read;
                }
                if (i2 != 0) {
                    bArr = Arrays.copyOf(bArr, i);
                    tx7920.a((Object) bArr, "java.util.Arrays.copyOf(this, newSize)");
                }
                a(fileInputStream, null);
                return bArr;
            }
            StringBuilder sb = new StringBuilder("File ");
            sb.append(file);
            sb.append(" is too big (");
            sb.append(length);
            sb.append(" bytes) to fit in memory.");
            throw new OutOfMemoryError(sb.toString());
        } catch (Throwable th2) {
            a(fileInputStream, th);
            throw th2;
        }
    }

    public static /* synthetic */ String a(File file, Charset charset, int i) {
        Charset charset2 = dd.a;
        tx7920.b(file, "receiver$0");
        tx7920.b(charset2, HttpRequest.PARAM_CHARSET);
        return new String(tx5040.a(file), charset2);
    }
}
