package com.ogury.crashreport;

import android.content.Context;
import android.content.pm.PackageInfo;

/* compiled from: AppInfo.kt */
public final class rtf1 {
    public static final C0060rtf1 a = new C0060rtf1(0);
    private final String b;
    private final String c;

    /* renamed from: com.ogury.crashreport.rtf1$rtf1 reason: collision with other inner class name */
    /* compiled from: AppInfo.kt */
    public static final class C0060rtf1 {
        private C0060rtf1() {
        }

        public /* synthetic */ C0060rtf1(byte b) {
            this();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
            if (r0 == null) goto L_0x0022;
         */
        public static rtf1 a(Context context) {
            PackageInfo packageInfo;
            String str;
            tx7920.b(context, "context");
            try {
                packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            } catch (Exception e) {
                f0 f0Var = f0.a;
                f0.a(e);
                packageInfo = null;
            }
            if (packageInfo != null) {
                str = packageInfo.versionName;
            }
            str = "";
            String packageName = context.getPackageName();
            tx7920.a((Object) packageName, "context.packageName");
            return new rtf1(str, packageName);
        }

        private static int a(int i, int i2) {
            int i3 = i % i2;
            return i3 >= 0 ? i3 : i3 + i2;
        }

        private static int b(int i, int i2, int i3) {
            return a(a(i, i3) - a(i2, i3), i3);
        }

        public static int a(int i, int i2, int i3) {
            if (i3 > 0) {
                return i >= i2 ? i2 : i2 - b(i2, i, i3);
            }
            if (i3 >= 0) {
                throw new IllegalArgumentException("Step is zero.");
            } else if (i <= i2) {
                return i2;
            } else {
                return i2 + b(i, i2, -i3);
            }
        }
    }

    public rtf1(String str, String str2) {
        tx7920.b(str, "version");
        tx7920.b(str2, "packageName");
        this.b = str;
        this.c = str2;
    }

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }
}
