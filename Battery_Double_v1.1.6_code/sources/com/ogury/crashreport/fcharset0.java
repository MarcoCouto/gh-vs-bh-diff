package com.ogury.crashreport;

import java.lang.Thread.UncaughtExceptionHandler;

/* compiled from: ExceptionHandler.kt */
public final class fcharset0 implements UncaughtExceptionHandler {
    public static final rtf1 a = new rtf1(0);
    private final cocoartf1671 b;
    private final UncaughtExceptionHandler c;

    /* compiled from: ExceptionHandler.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    public fcharset0(cocoartf1671 cocoartf1671, UncaughtExceptionHandler uncaughtExceptionHandler) {
        tx7920.b(cocoartf1671, "crashFileStore");
        this.b = cocoartf1671;
        this.c = uncaughtExceptionHandler;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        tx7920.b(thread, "thread");
        tx7920.b(th, "exception");
        try {
            this.b.a(th);
            UncaughtExceptionHandler uncaughtExceptionHandler = this.c;
            if (uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, th);
            }
        } catch (Exception e) {
            f0 f0Var = f0.a;
            f0.a(e);
        }
    }
}
