package com.ogury.crashreport;

import java.util.Iterator;

/* compiled from: Iterators.kt */
public abstract class tx720 implements Iterator<Integer> {
    public abstract int a();

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* synthetic */ Object next() {
        return Integer.valueOf(a());
    }
}
