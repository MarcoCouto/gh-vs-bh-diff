package com.ogury.crashreport;

/* compiled from: Thread.kt */
public final class tx1440 {

    /* compiled from: Thread.kt */
    public static final class rtf1 extends Thread {
        private /* synthetic */ tx6480 a;

        rtf1(tx6480 tx6480) {
            this.a = tx6480;
        }

        public final void run() {
            this.a.a();
        }
    }

    public static final Thread a(boolean z, boolean z2, ClassLoader classLoader, String str, int i, tx6480<vieww10800> tx6480) {
        tx7920.b(tx6480, "block");
        rtf1 rtf12 = new rtf1(tx6480);
        if (z2) {
            rtf12.setDaemon(true);
        }
        if (i > 0) {
            rtf12.setPriority(i);
        }
        if (str != null) {
            rtf12.setName(str);
        }
        if (classLoader != null) {
            rtf12.setContextClassLoader(classLoader);
        }
        if (z) {
            rtf12.start();
        }
        return rtf12;
    }
}
