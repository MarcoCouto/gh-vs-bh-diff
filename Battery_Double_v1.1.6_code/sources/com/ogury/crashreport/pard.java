package com.ogury.crashreport;

/* compiled from: _ArraysJvm.kt */
class pard extends viewkind0 {
    public static <T, A extends Appendable> A a(T[] tArr, A a, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tx7200<? super T, ? extends CharSequence> tx7200) {
        tx7920.b(tArr, "receiver$0");
        tx7920.b(a, "buffer");
        tx7920.b(charSequence, "separator");
        tx7920.b(charSequence2, "prefix");
        tx7920.b(charSequence3, "postfix");
        tx7920.b(charSequence4, "truncated");
        a.append(charSequence2);
        int i2 = 0;
        for (T t : tArr) {
            i2++;
            boolean z = true;
            if (i2 > 1) {
                a.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            tx7920.b(a, "receiver$0");
            if (tx7200 != null) {
                a.append((CharSequence) tx7200.a());
            } else {
                if (t != null) {
                    z = t instanceof CharSequence;
                }
                if (z) {
                    a.append((CharSequence) t);
                } else if (t instanceof Character) {
                    a.append(((Character) t).charValue());
                } else {
                    a.append(String.valueOf(t));
                }
            }
        }
        if (i >= 0 && i2 > i) {
            a.append(charSequence4);
        }
        a.append(charSequence3);
        return a;
    }

    public static /* synthetic */ String a(Object[] objArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, tx7200 tx7200, int i2) {
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = "";
        CharSequence charSequence7 = "...";
        tx7920.b(objArr, "receiver$0");
        tx7920.b(charSequence, "separator");
        tx7920.b(charSequence5, "prefix");
        tx7920.b(charSequence6, "postfix");
        tx7920.b(charSequence7, "truncated");
        String sb = ((StringBuilder) viewh8400.a((T[]) objArr, (A) new StringBuilder(), charSequence, charSequence5, charSequence6, -1, charSequence7, null)).toString();
        tx7920.a((Object) sb, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb;
    }
}
