package com.ogury.crashreport;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.NoSuchElementException;

/* compiled from: Strings.kt */
class hh extends gg {
    public static final int a(CharSequence charSequence) {
        tx7920.b(charSequence, "receiver$0");
        return charSequence.length() - 1;
    }

    public static /* synthetic */ int a(CharSequence charSequence, char c, int i, boolean z, int i2) {
        int i3;
        boolean z2;
        if ((i2 & 2) != 0) {
            i = 0;
        }
        tx7920.b(charSequence, "receiver$0");
        if (!(charSequence instanceof String)) {
            char[] cArr = {'.'};
            tx7920.b(charSequence, "receiver$0");
            tx7920.b(cArr, "chars");
            if (charSequence instanceof String) {
                tx7920.b(cArr, "receiver$0");
                switch (cArr.length) {
                    case 0:
                        throw new NoSuchElementException("Array is empty.");
                    case 1:
                        i3 = ((String) charSequence).indexOf(cArr[0], i);
                        break;
                    default:
                        throw new IllegalArgumentException("Array has more than one element.");
                }
            } else {
                int a = aa.a(i, 0);
                int a2 = ii.a(charSequence);
                if (a <= a2) {
                    while (true) {
                        char charAt = charSequence.charAt(a);
                        int i4 = 0;
                        while (true) {
                            if (i4 > 0) {
                                z2 = false;
                                break;
                            } else if (bb.a(cArr[i4], charAt, false)) {
                                z2 = true;
                                break;
                            } else {
                                i4++;
                            }
                        }
                        if (!z2) {
                            if (a == a2) {
                                break;
                            }
                            a++;
                        } else {
                            i3 = a;
                            break;
                        }
                    }
                }
                return -1;
            }
        } else {
            i3 = ((String) charSequence).indexOf(46, i);
        }
        return i3;
    }

    public static /* synthetic */ boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z, int i) {
        int i2;
        tx7920.b(charSequence, "receiver$0");
        tx7920.b(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (charSequence2 instanceof String) {
            String str = (String) charSequence2;
            tx7920.b(charSequence, "receiver$0");
            tx7920.b(str, "string");
            if (!(charSequence instanceof String)) {
                i2 = a(charSequence, str, 0, charSequence.length(), false, false, 16);
            } else {
                i2 = ((String) charSequence).indexOf(str, 0);
            }
            return i2 >= 0;
        }
        return a(charSequence, charSequence2, 0, charSequence.length(), false, false, 16) >= 0;
    }

    private static /* synthetic */ int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3) {
        boolean z3;
        boolean z4;
        partightenfactor0 cf0 = new cf0(aa.a(i, 0), aa.b(i2, charSequence.length()));
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int a = cf0.a();
            int b = cf0.b();
            int c = cf0.c();
            if (c <= 0 ? a >= b : a <= b) {
                while (true) {
                    int length = charSequence2.length();
                    tx7920.b(charSequence2, "receiver$0");
                    tx7920.b(charSequence, FacebookRequestErrorClassification.KEY_OTHER);
                    if (a >= 0 && charSequence2.length() - length >= 0 && a <= charSequence.length() - length) {
                        int i4 = 0;
                        while (true) {
                            if (i4 >= length) {
                                z3 = true;
                                break;
                            } else if (!bb.a(charSequence2.charAt(i4 + 0), charSequence.charAt(a + i4), z)) {
                                break;
                            } else {
                                i4++;
                            }
                        }
                    }
                    z3 = false;
                    if (!z3) {
                        if (a == b) {
                            break;
                        }
                        a += c;
                    } else {
                        return a;
                    }
                }
            }
        } else {
            int a2 = cf0.a();
            int b2 = cf0.b();
            int c2 = cf0.c();
            if (c2 <= 0 ? a2 >= b2 : a2 <= b2) {
                while (true) {
                    String str = (String) charSequence2;
                    String str2 = (String) charSequence;
                    int length2 = charSequence2.length();
                    tx7920.b(str, "receiver$0");
                    tx7920.b(str2, FacebookRequestErrorClassification.KEY_OTHER);
                    if (!z) {
                        z4 = str.regionMatches(0, str2, a2, length2);
                    } else {
                        z4 = str.regionMatches(z, 0, str2, a2, length2);
                    }
                    if (!z4) {
                        if (a2 == b2) {
                            break;
                        }
                        a2 += c2;
                    } else {
                        return a2;
                    }
                }
            }
        }
        return -1;
    }
}
