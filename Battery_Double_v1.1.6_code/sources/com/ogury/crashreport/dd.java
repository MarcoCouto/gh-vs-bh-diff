package com.ogury.crashreport;

import com.google.android.exoplayer2.C;
import java.nio.charset.Charset;

/* compiled from: Charsets.kt */
public final class dd {
    public static final Charset a;

    static {
        new dd();
        Charset forName = Charset.forName("UTF-8");
        tx7920.a((Object) forName, "Charset.forName(\"UTF-8\")");
        a = forName;
        tx7920.a((Object) Charset.forName("UTF-16"), "Charset.forName(\"UTF-16\")");
        tx7920.a((Object) Charset.forName("UTF-16BE"), "Charset.forName(\"UTF-16BE\")");
        tx7920.a((Object) Charset.forName("UTF-16LE"), "Charset.forName(\"UTF-16LE\")");
        tx7920.a((Object) Charset.forName(C.ASCII_NAME), "Charset.forName(\"US-ASCII\")");
        tx7920.a((Object) Charset.forName("ISO-8859-1"), "Charset.forName(\"ISO-8859-1\")");
    }

    private dd() {
    }
}
