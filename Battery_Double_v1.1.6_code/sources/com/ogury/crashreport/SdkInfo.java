package com.ogury.crashreport;

import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;

/* compiled from: SdkInfo.kt */
public final class SdkInfo {
    private final String aaid;
    private final String apiKey;
    private final String sdkVersion;

    public SdkInfo(String str, String str2, String str3) {
        tx7920.b(str, GeneralPropertiesWorker.SDK_VERSION);
        tx7920.b(str2, "apiKey");
        tx7920.b(str3, "aaid");
        this.sdkVersion = str;
        this.apiKey = str2;
        this.aaid = str3;
    }

    public final String getAaid() {
        return this.aaid;
    }

    public final String getApiKey() {
        return this.apiKey;
    }

    public final String getSdkVersion() {
        return this.sdkVersion;
    }
}
