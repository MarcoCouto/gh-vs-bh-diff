package com.ogury.crashreport;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.crashreport.red255.rtf1;
import com.ogury.crashreport.rtf1.C0060rtf1;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/* compiled from: CrashFileStore.kt */
public final class cocoartf1671 {
    private final rtf1 a;
    private final cocoasubrtf100 b;
    private final Helvetica c;

    public cocoartf1671(Context context, cocoasubrtf100 cocoasubrtf100, Helvetica helvetica) {
        tx7920.b(context, "context");
        tx7920.b(cocoasubrtf100, "crashFormatter");
        tx7920.b(helvetica, "fileStore");
        this.b = cocoasubrtf100;
        this.c = helvetica;
        C0060rtf1 rtf1 = rtf1.a;
        this.a = C0060rtf1.a(context);
    }

    public final void a(Throwable th) throws IOException {
        green255 green255;
        File file;
        Throwable th2 = th;
        tx7920.b(th2, "throwable");
        rtf1 rtf1 = red255.a;
        String str = Build.MODEL;
        tx7920.a((Object) str, "Build.MODEL");
        String str2 = VERSION.RELEASE;
        tx7920.a((Object) str2, "Build.VERSION.RELEASE");
        green255.rtf1 rtf12 = green255.a;
        Runtime runtime = Runtime.getRuntime();
        if (runtime != null) {
            green255 = new green255(runtime.freeMemory(), runtime.totalMemory(), runtime.maxMemory(), true);
        } else {
            green255 green2552 = new green255(0, 0, 0, false, 15);
            green255 = green2552;
        }
        ansi a2 = this.b.a(th2, new red255(str, str2, green255), this.a, this.c);
        if (!(a2 instanceof colortbl) && (a2 instanceof blue255)) {
            blue255 blue255 = (blue255) a2;
            String b2 = blue255.b();
            File[] a3 = this.c.a();
            int length = a3.length;
            boolean z = false;
            int i = 0;
            while (true) {
                if (i >= length) {
                    file = null;
                    break;
                }
                file = a3[i];
                String name = file.getName();
                tx7920.a((Object) name, "file.name");
                if (ii.a(name, b2, false, 2)) {
                    break;
                }
                i++;
            }
            if (file != null) {
                String a4 = blue255.a();
                String a5 = a(file);
                PrintWriter printWriter = new PrintWriter(file);
                if (a5.length() == 0) {
                    z = true;
                }
                if (z) {
                    printWriter.print(a4);
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(a5);
                    sb.append(',');
                    sb.append(a4);
                    printWriter.print(sb.toString());
                }
                printWriter.close();
            }
        }
    }

    public static String a(File file) throws Exception {
        tx7920.b(file, ParametersKeys.FILE);
        StringBuilder sb = new StringBuilder();
        sb.append(tx5040.a(file, null, 1));
        String sb2 = sb.toString();
        tx7920.a((Object) sb2, "sb.toString()");
        return sb2;
    }

    public final File[] a() {
        return this.c.a();
    }
}
