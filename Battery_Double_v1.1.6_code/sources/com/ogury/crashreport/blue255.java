package com.ogury.crashreport;

import com.google.android.gms.measurement.AppMeasurement;

/* compiled from: Crash.kt */
public final class blue255 extends ansi {
    private final String a;
    private final String b;

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (com.ogury.crashreport.tx7920.a((java.lang.Object) r2.b, (java.lang.Object) r3.b) != false) goto L_0x001f;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof blue255) {
                blue255 blue255 = (blue255) obj;
                if (tx7920.a((Object) this.a, (Object) blue255.a)) {
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("PresageCrash(crash=");
        sb.append(this.a);
        sb.append(", packageName=");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }

    public blue255(String str, String str2) {
        tx7920.b(str, AppMeasurement.CRASH_ORIGIN);
        tx7920.b(str2, "packageName");
        super(0);
        this.a = str;
        this.b = str2;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }
}
