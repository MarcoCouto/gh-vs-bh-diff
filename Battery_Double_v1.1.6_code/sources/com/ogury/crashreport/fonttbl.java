package com.ogury.crashreport;

import android.content.Context;
import android.content.SharedPreferences;
import com.ironsource.sdk.constants.Constants.ParametersKeys;

/* compiled from: CrashReportDao.kt */
public final class fonttbl {
    private final SharedPreferences a;

    /* compiled from: CrashReportDao.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    static {
        new rtf1(0);
    }

    public fonttbl(Context context) {
        tx7920.b(context, "context");
        this.a = context.getSharedPreferences("crashreport", 0);
    }

    public final void a(String str, String str2) {
        tx7920.b(str, ParametersKeys.KEY);
        tx7920.b(str2, "uploadUrl");
        this.a.edit().putString(str, str2).apply();
    }

    public final String a(String str) {
        tx7920.b(str, ParametersKeys.KEY);
        String string = this.a.getString(str, "");
        return string == null ? "" : string;
    }
}
