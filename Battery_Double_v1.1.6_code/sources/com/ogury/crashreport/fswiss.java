package com.ogury.crashreport;

import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.io.File;

/* compiled from: CrashUploader.kt */
public final class fswiss {
    private final cocoartf1671 a;
    private final fonttbl b;

    /* compiled from: CrashUploader.kt */
    static final class ansi extends tx8640 implements tx6480<vieww10800> {
        private /* synthetic */ fswiss a;

        ansi(fswiss fswiss) {
            this.a = fswiss;
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a() {
            fswiss.a(this.a);
            return vieww10800.a;
        }
    }

    /* compiled from: CrashUploader.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    static {
        new rtf1(0);
    }

    public fswiss(cocoartf1671 cocoartf1671, fonttbl fonttbl) {
        tx7920.b(cocoartf1671, "crashFileStore");
        tx7920.b(fonttbl, "crashReportDao");
        this.a = cocoartf1671;
        this.b = fonttbl;
    }

    public static final /* synthetic */ void a(fswiss fswiss) {
        File[] a2;
        try {
            for (File file : fswiss.a.a()) {
                String a3 = cocoartf1671.a(file);
                if (!(a3.length() == 0)) {
                    ansicpg1252 ansicpg1252 = ansicpg1252.a;
                    StringBuilder sb = new StringBuilder(RequestParameters.LEFT_BRACKETS);
                    sb.append(a3);
                    sb.append(']');
                    String sb2 = sb.toString();
                    fonttbl fonttbl = fswiss.b;
                    String name = file.getName();
                    tx7920.a((Object) name, "file.name");
                    if (ansicpg1252.a(sb2, fonttbl.a(name)) < 500) {
                        cocoartf1671 cocoartf1671 = fswiss.a;
                        tx7920.b(file, ParametersKeys.FILE);
                        Helvetica.a(file);
                    }
                }
            }
        } catch (Exception e) {
            f0 f0Var = f0.a;
            f0.a(e);
        }
    }
}
