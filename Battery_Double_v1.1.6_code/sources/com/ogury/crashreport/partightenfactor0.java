package com.ogury.crashreport;

import com.ogury.crashreport.rtf1.C0060rtf1;
import java.util.Iterator;

/* compiled from: Progressions.kt */
public class partightenfactor0 implements Iterable<Integer> {
    public static final rtf1 a = new rtf1(0);
    private final int b;
    private final int c;
    private final int d;

    /* compiled from: Progressions.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    public partightenfactor0(int i, int i2, int i3) {
        if (i3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (i3 != Integer.MIN_VALUE) {
            this.b = i;
            this.c = C0060rtf1.a(i, i2, i3);
            this.d = i3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
        }
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    public boolean d() {
        return this.d > 0 ? this.b > this.c : this.b < this.c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r2.d == r3.d) goto L_0x0027;
     */
    public boolean equals(Object obj) {
        if (obj instanceof partightenfactor0) {
            if (!d() || !((partightenfactor0) obj).d()) {
                partightenfactor0 partightenfactor0 = (partightenfactor0) obj;
                if (this.b == partightenfactor0.b) {
                    if (this.c == partightenfactor0.c) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (d()) {
            return -1;
        }
        return (((this.b * 31) + this.c) * 31) + this.d;
    }

    public String toString() {
        StringBuilder sb;
        int i;
        if (this.d > 0) {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append("..");
            sb.append(this.c);
            sb.append(" step ");
            i = this.d;
        } else {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append(" downTo ");
            sb.append(this.c);
            sb.append(" step ");
            i = -this.d;
        }
        sb.append(i);
        return sb.toString();
    }

    public /* synthetic */ Iterator iterator() {
        return new fs24(this.b, this.c, this.d);
    }
}
