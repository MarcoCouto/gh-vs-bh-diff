package com.ogury.crashreport;

/* compiled from: PhoneInfo.kt */
public final class red255 {
    public static final rtf1 a = new rtf1(0);
    private final String b;
    private final String c;
    private final green255 d;

    /* compiled from: PhoneInfo.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    public red255(String str, String str2, green255 green255) {
        tx7920.b(str, "phoneModel");
        tx7920.b(str2, "androidVersion");
        tx7920.b(green255, "memory");
        this.b = str;
        this.c = str2;
        this.d = green255;
    }

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final green255 c() {
        return this.d;
    }
}
