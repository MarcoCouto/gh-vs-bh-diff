package com.ogury.cm;

import com.ogury.cm.OguryChoiceManager.Answer;

public final /* synthetic */ class aaaca {
    public static final /* synthetic */ int[] a;

    static {
        int[] iArr = new int[Answer.values().length];
        a = iArr;
        iArr[Answer.FULL_APPROVAL.ordinal()] = 1;
        a[Answer.REFUSAL.ordinal()] = 2;
        a[Answer.NO_ANSWER.ordinal()] = 3;
    }
}
