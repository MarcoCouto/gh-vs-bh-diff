package com.ogury.cm;

public final class aabac {
    public static final aabac a = new aabac();

    private aabac() {
    }

    public static boolean a(int i, int i2) {
        return (i & i2) == i2;
    }

    public static boolean a(Integer[] numArr, int i) {
        accbb.b(numArr, "container");
        if (!(numArr.length == 0)) {
            int i2 = i / 32;
            if (numArr.length > i2) {
                return a(numArr[i2].intValue(), (int) Math.pow(2.0d, (double) (i - (i2 * 32))));
            }
        }
        return false;
    }
}
