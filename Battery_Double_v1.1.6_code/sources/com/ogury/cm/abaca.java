package com.ogury.cm;

import android.content.Context;
import io.fabric.sdk.android.services.network.HttpRequest;

public final class abaca {
    private final abacc a = new abacc();

    public static void a(Context context, String str, String str2, aacbc aacbc) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        accbb.b(str2, "requestType");
        accbb.b(aacbc, "callback");
        abbac b = b(str2, aacbc);
        new abbab();
        abacc.a(b.b(abbab.a(context, str)).f());
    }

    public static void a(String str, aacbc aacbc) {
        accbb.b(str, "errorMessage");
        accbb.b(aacbc, "callback");
        abbac b = b("event", aacbc);
        new abbab();
        aacca aacca = aacca.a;
        abacc.a(b.b(abbab.a(str)).a("X-CM-SECURE-TOKEN", aacca.g()).f());
    }

    public static void a(String str, aacbc aacbc, String str2, Integer[] numArr) {
        accbb.b(str, "assetKey");
        accbb.b(aacbc, "callback");
        accbb.b(str2, "iabString");
        abbac b = b("external-consent", aacbc);
        new abbab();
        abacc.a(b.b(abbab.a(str, str2, numArr)).f());
    }

    public static void a(String str, aacbc aacbc, String str2, String[] strArr) {
        accbb.b(str, "assetKey");
        accbb.b(aacbc, "callback");
        accbb.b(str2, "iabString");
        abbac b = b("external-consent", aacbc);
        new abbab();
        abacc.a(b.b(abbab.a(str, str2, strArr)).f());
    }

    private static abbac b(String str, aacbc aacbc) {
        return new abbac().a(HttpRequest.METHOD_POST).c(str).a(aacbc);
    }
}
