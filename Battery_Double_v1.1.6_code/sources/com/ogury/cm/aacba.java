package com.ogury.cm;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public final class aacba {
    public static final aacba a = new aacba();

    static final class aaaaa implements Runnable {
        final /* synthetic */ Context a;
        final /* synthetic */ aacac b;

        aaaaa(Context context, aacac aacac) {
            this.a = context;
            this.b = aacac;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
            if (r0 != null) goto L_0x002e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0038, code lost:
            throw new java.lang.IllegalStateException("aaid is null");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0039, code lost:
            r0 = com.ogury.cm.aacba.a(com.ogury.cm.aacba.a, r2.a);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
            r0 = android.provider.Settings.Secure.getString(r2.a.getContentResolver(), com.tapjoy.TapjoyConstants.TJC_ADVERTISING_ID);
            com.ogury.cm.accbb.a((java.lang.Object) r0, "Settings.Secure.getStrin…solver, \"advertising_id\")");
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x001b */
        public final void run() {
            aacbb aacbb = aacbb.a;
            acabb a2 = aacbb.a(this.a);
            if (a2 != null) {
                String str = a2.a();
                if (str != null) {
                    this.b.a(str);
                    return;
                }
            }
            throw new IllegalStateException("androidAdvertisingId is null");
        }
    }

    static final class aaaab<T> implements Comparator<ApplicationInfo> {
        public static final aaaab a = new aaaab();

        aaaab() {
        }

        public final /* synthetic */ int compare(Object obj, Object obj2) {
            ApplicationInfo applicationInfo = (ApplicationInfo) obj2;
            String str = ((ApplicationInfo) obj).packageName;
            String str2 = applicationInfo.packageName;
            accbb.a((Object) str2, "rhs.packageName");
            return str.compareTo(str2);
        }
    }

    private aacba() {
    }

    /* access modifiers changed from: private */
    public static String a(Context context) {
        ApplicationInfo applicationInfo = null;
        if (context != null) {
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager != null) {
                    List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(128);
                    ArrayList arrayList = new ArrayList();
                    if (!installedApplications.isEmpty()) {
                        for (ApplicationInfo applicationInfo2 : installedApplications) {
                            if (!((1 & applicationInfo2.flags) == 0 || applicationInfo2.packageName == null)) {
                                arrayList.add(applicationInfo2);
                            }
                        }
                        List list = arrayList;
                        Comparator comparator = aaaab.a;
                        accbb.b(list, "receiver$0");
                        accbb.b(comparator, "comparator");
                        if (list.size() > 1) {
                            Collections.sort(list, comparator);
                        }
                        applicationInfo = (ApplicationInfo) arrayList.get(0);
                    }
                }
            } catch (Exception unused) {
                return "00000000-0000-0000-0000-000000000000";
            }
        }
        if (applicationInfo == null) {
            return "00000000-0000-0000-0000-000000000000";
        }
        try {
            long j = context.getPackageManager().getPackageInfo(applicationInfo.packageName, 128).firstInstallTime;
            StringBuilder sb = new StringBuilder();
            sb.append(String.valueOf(j));
            String sb2 = sb.toString();
            Charset charset = babaa.a;
            if (sb2 != null) {
                byte[] bytes = sb2.getBytes(charset);
                accbb.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                String uuid = UUID.nameUUIDFromBytes(bytes).toString();
                accbb.a((Object) uuid, "UUID.nameUUIDFromBytes((…toByteArray()).toString()");
                return uuid;
            }
            throw new acbab("null cannot be cast to non-null type java.lang.String");
        } catch (Exception unused2) {
            return "00000000-0000-0000-0000-000000000000";
        }
    }

    public static void a(Context context, aacac aacac) {
        accbb.b(context, "context");
        accbb.b(aacac, "aaidCallback");
        new Thread(new aaaaa(context, aacac)).start();
    }
}
