package com.ogury.cm;

import android.util.Log;
import com.applovin.sdk.AppLovinEventParameters;

public final class acaaa {
    private String a = "";
    private final String b;

    public acaaa(String str, String str2) {
        accbb.b(str, AppLovinEventParameters.PRODUCT_IDENTIFIER);
        accbb.b(str2, "skuType");
        this.b = str;
        int hashCode = str2.hashCode();
        if (hashCode != 505523517) {
            if (hashCode == 1741942868 && str2.equals("OneTimePurchase")) {
                this.a = "inapp";
                return;
            }
        } else if (str2.equals("Subscription")) {
            this.a = "subs";
            return;
        }
        Log.e("FairChoice", "Illegal SKU type");
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }
}
