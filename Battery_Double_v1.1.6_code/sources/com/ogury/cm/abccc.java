package com.ogury.cm;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import com.applovin.sdk.AppLovinEventTypes;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs.CastExtraArgs;

public final class abccc implements abcba {
    private acaab a;

    public final void activateProduct(acaaa acaaa) {
        accbb.b(acaaa, AppLovinEventTypes.USER_VIEWED_PRODUCT);
    }

    public final void endDataSourceConnections() {
        Log.d("FairChoice", "endDataSourceConnections");
    }

    public final boolean isBillingDisabled() {
        return true;
    }

    public final boolean isProductActivated() {
        return false;
    }

    public final void launchBillingFlow(Activity activity) {
        accbb.b(activity, "activity");
    }

    public final void queryProductDetails() {
        acaab acaab = this.a;
        if (acaab != null) {
            acaab.a("ok", "");
        }
    }

    public final void queryPurchase(acaac acaac) {
        accbb.b(acaac, CastExtraArgs.LISTENER);
        acaac.a();
    }

    public final void setBillingFinishedListener(abcac abcac) {
    }

    public final void setQueryProductDetailsListener(acaab acaab) {
        this.a = acaab;
        acaab acaab2 = this.a;
        if (acaab2 != null) {
            acaab2.a("ok", "");
        }
    }

    public final void startDataSourceConnections(Context context) {
        accbb.b(context, "context");
        abcab abcab = abcab.a;
        abcab.a(context);
    }

    public final boolean tokenExistsForActiveProduct() {
        return false;
    }
}
