package com.ogury.cm;

import java.util.Iterator;

public class baaac implements baaaa, Iterable<Integer> {
    public static final aaaaa a = new aaaaa(null);
    private final int b;
    private final int c;
    private final int d = 1;

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    public baaac(int i, int i2, int i3) {
        this.b = i;
        this.c = com.ogury.cm.ConsentActivity.aaaaa.a(i, i2, 1);
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    public boolean d() {
        return this.d > 0 ? this.b > this.c : this.b < this.c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r2.d == r3.d) goto L_0x0027;
     */
    public boolean equals(Object obj) {
        if (obj instanceof baaac) {
            if (!d() || !((baaac) obj).d()) {
                baaac baaac = (baaac) obj;
                if (this.b == baaac.b) {
                    if (this.c == baaac.c) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (d()) {
            return -1;
        }
        return (((this.b * 31) + this.c) * 31) + this.d;
    }

    public /* synthetic */ Iterator iterator() {
        return new baaba(this.b, this.c, this.d);
    }

    public String toString() {
        StringBuilder sb;
        int i;
        if (this.d > 0) {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append("..");
            sb.append(this.c);
            sb.append(" step ");
            i = this.d;
        } else {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append(" downTo ");
            sb.append(this.c);
            sb.append(" step ");
            i = -this.d;
        }
        sb.append(i);
        return sb.toString();
    }
}
