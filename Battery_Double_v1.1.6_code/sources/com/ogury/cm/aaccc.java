package com.ogury.cm;

import android.util.Log;

public final class aaccc {
    public static final aaccc a = new aaccc();

    private aaccc() {
    }

    public static void a(String str) {
        accbb.b(str, "message");
        Log.e("consent_sdk", str);
    }

    public static void a(String str, Throwable th) {
        accbb.b(str, "message");
        accbb.b(th, "error");
        Log.e("consent_sdk", str, th);
    }

    public static void a(Throwable th) {
        accbb.b(th, "error");
        Log.e("consent_sdk", "caught_error", th);
    }

    public static void b(String str) {
        accbb.b(str, "message");
        Log.d("consent_sdk", str);
    }
}
