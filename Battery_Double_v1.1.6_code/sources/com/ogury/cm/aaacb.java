package com.ogury.cm;

public final class aaacb extends aaaba {

    static final class aaaaa extends accbc implements accac<Integer, Boolean> {
        final /* synthetic */ aaacb a;
        final /* synthetic */ int b;

        aaaaa(aaacb aaacb, int i) {
            this.a = aaacb;
            this.b = i;
            super(1);
        }

        public final /* synthetic */ Object a(Object obj) {
            ((Number) obj).intValue();
            aabac aabac = aabac.a;
            return Boolean.valueOf(aabac.a(aaacb.f().g(), this.b));
        }
    }

    static final class aaaab extends accbc implements accac<Integer, Boolean> {
        final /* synthetic */ aaacb a;
        final /* synthetic */ int b;

        aaaab(aaacb aaacb, int i) {
            this.a = aaacb;
            this.b = i;
            super(1);
        }

        public final /* synthetic */ Object a(Object obj) {
            ((Number) obj).intValue();
            return Boolean.valueOf(this.a.g(this.b) && this.a.f(this.b));
        }
    }

    static final class aaaac extends accbc implements accac<Integer, Boolean> {
        final /* synthetic */ aaacb a;
        final /* synthetic */ int b;

        aaaac(aaacb aaacb, int i) {
            this.a = aaacb;
            this.b = i;
            super(1);
        }

        public final /* synthetic */ Object a(Object obj) {
            ((Number) obj).intValue();
            aabac aabac = aabac.a;
            return Boolean.valueOf(aabac.a(aaacb.f().j(), this.b));
        }
    }

    static final class aaaba extends accbc implements accac<Integer, Boolean> {
        final /* synthetic */ aaacb a;
        final /* synthetic */ int b;

        aaaba(aaacb aaacb, int i) {
            this.a = aaacb;
            this.b = i;
            super(1);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0028, code lost:
            if ((r3.b <= com.ogury.cm.aaacb.f().n()) != false) goto L_0x002c;
         */
        public final /* synthetic */ Object a(Object obj) {
            ((Number) obj).intValue();
            aabac aabac = aabac.a;
            boolean z = true;
            if (aabac.a(aaacb.f().h(), this.b)) {
            }
            z = false;
            return Boolean.valueOf(z);
        }
    }

    static final class aaabb extends accbc implements accac<Integer, Boolean> {
        final /* synthetic */ aaacb a;
        final /* synthetic */ int b;

        aaabb(aaacb aaacb, int i) {
            this.a = aaacb;
            this.b = i;
            super(1);
        }

        public final /* synthetic */ Object a(Object obj) {
            ((Number) obj).intValue();
            aabac aabac = aabac.a;
            return Boolean.valueOf(aabac.a(aaacb.f().i(), this.b));
        }
    }

    static final class aaabc extends accbc implements accac<Integer, Boolean> {
        final /* synthetic */ aaacb a;
        final /* synthetic */ int b;

        aaabc(aaacb aaacb, int i) {
            this.a = aaacb;
            this.b = i;
            super(1);
        }

        public final /* synthetic */ Object a(Object obj) {
            ((Number) obj).intValue();
            boolean z = false;
            if (this.a.c(this.b)) {
                aaaab aaaab = aaaab.a;
                final int a2 = aaaab.a("purposes", this.b, aaacb.f().m());
                if (aaacb.a(this.a, a2, new accac<Integer, Boolean>(this) {
                    final /* synthetic */ aaabc a;

                    {
                        this.a = r1;
                    }

                    public final /* synthetic */ Object a(Object obj) {
                        ((Number) obj).intValue();
                        return Boolean.valueOf(this.a.a.b(a2));
                    }
                })) {
                    aaaab aaaab2 = aaaab.a;
                    final int a3 = aaaab.a("specialFeatures", this.b, aaacb.f().m());
                    if (aaacb.a(this.a, a3, new accac<Integer, Boolean>(this) {
                        final /* synthetic */ aaabc a;

                        {
                            this.a = r1;
                        }

                        public final /* synthetic */ Object a(Object obj) {
                            ((Number) obj).intValue();
                            return Boolean.valueOf(this.a.a.a(a3));
                        }
                    })) {
                        z = true;
                    }
                }
            }
            return Boolean.valueOf(z);
        }
    }

    static final class aaaca extends accbc implements accac<Integer, Boolean> {
        final /* synthetic */ aaacb a;
        final /* synthetic */ int b;

        aaaca(aaacb aaacb, int i) {
            this.a = aaacb;
            this.b = i;
            super(1);
        }

        public final /* synthetic */ Object a(Object obj) {
            ((Number) obj).intValue();
            boolean z = false;
            if (this.a.d(this.b)) {
                aaaab aaaab = aaaab.a;
                final int a2 = aaaab.a("legIntPurposes", this.b, aaacb.f().m());
                if (aaacb.a(this.a, a2, new accac<Integer, Boolean>(this) {
                    final /* synthetic */ aaaca a;

                    {
                        this.a = r1;
                    }

                    public final /* synthetic */ Object a(Object obj) {
                        ((Number) obj).intValue();
                        return Boolean.valueOf(this.a.a.e(a2));
                    }
                })) {
                    z = true;
                }
            }
            return Boolean.valueOf(z);
        }
    }

    public aaacb() {
        aacca aacca = aacca.a;
        aacca.a(2);
        aacca aacca2 = aacca.a;
        aacca.a((ababa) new ababc());
        com.ogury.cm.abbcb.aaaaa aaaaa2 = abbcb.a;
        a(com.ogury.cm.abbcb.aaaaa.a());
    }

    private static boolean a(int i, accac<? super Integer, Boolean> accac) {
        int n = f().n();
        if (i >= 0 && n >= i) {
            return ((Boolean) accac.a(Integer.valueOf(i))).booleanValue();
        }
        return false;
    }

    public static final /* synthetic */ boolean a(aaacb aaacb, int i, accac accac) {
        if (i == 0) {
            return true;
        }
        return ((Boolean) accac.a(Integer.valueOf(i))).booleanValue();
    }

    public static ababc f() {
        aacca aacca = aacca.a;
        return (ababc) ababa.f();
    }

    private static Integer[] g() {
        return new Integer[]{Integer.valueOf(2), Integer.valueOf(4), Integer.valueOf(8), Integer.valueOf(16), Integer.valueOf(32), Integer.valueOf(64), Integer.valueOf(128), Integer.valueOf(256), Integer.valueOf(512), Integer.valueOf(1024)};
    }

    public final void a() {
        aacca aacca = aacca.a;
        aacca.a((ababa) new ababc());
    }

    public final /* synthetic */ ababa b() {
        return f();
    }

    public final boolean b(int i) {
        return a(i, g(), (accac<? super Integer, Boolean>) new aaabb<Object,Boolean>(this, i));
    }

    public final boolean c(int i) {
        return a(i, new aaaaa(this, i));
    }

    public final Integer[] c() {
        return new Integer[]{Integer.valueOf(2), Integer.valueOf(4)};
    }

    public final boolean d(int i) {
        return a(i, new aaaba(this, i));
    }

    public final boolean e(int i) {
        return a(i, g(), (accac<? super Integer, Boolean>) new aaaac<Object,Boolean>(this, i));
    }

    public final boolean f(int i) {
        return a(i, new aaabc(this, i));
    }

    public final boolean g(int i) {
        return a(i, new aaaca(this, i));
    }

    public final boolean h(int i) {
        return a(i, new aaaab(this, i));
    }
}
