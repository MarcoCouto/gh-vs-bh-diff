package com.ogury.cm;

import android.content.Context;

public final class OguryChoiceManagerExternal {
    public static final OguryChoiceManagerExternal a = new OguryChoiceManagerExternal();
    private static Integer b;

    public static final class TcfV1 {
        public static final TcfV1 a = new TcfV1();
        private static aabaa b = new aabaa();

        private TcfV1() {
        }

        public static /* synthetic */ void clientConsentExternalV1$annotations() {
        }

        public static final void setConsent(Context context, String str, String str2, String[] strArr) {
            accbb.b(context, "context");
            accbb.b(str, "assetKey");
            accbb.b(str2, "iabString");
            OguryChoiceManagerExternal.access$checkTcfVersion(OguryChoiceManagerExternal.a, 1);
            OguryChoiceManagerExternal.a.setConsumedTcfVersion$consent_manager_prodRelease(Integer.valueOf(1));
            if (!(!babac.a((CharSequence) str)) || !(!babac.a((CharSequence) str2))) {
                aaccc aaccc = aaccc.a;
                aaccc.a("assetKey and iabString must not be empty");
                return;
            }
            b.a(context, str, str2, strArr);
        }

        public final aabaa getClientConsentExternalV1$consent_manager_prodRelease() {
            return b;
        }

        public final void setClientConsentExternalV1$consent_manager_prodRelease(aabaa aabaa) {
            accbb.b(aabaa, "<set-?>");
            b = aabaa;
        }
    }

    public static final class TcfV2 {
        public static final TcfV2 a = new TcfV2();
        private static aabab b = new aabab();

        private TcfV2() {
        }

        public static /* synthetic */ void clientConsentExternalV2$annotations() {
        }

        public static final void setConsent(Context context, String str, String str2, Integer[] numArr) {
            accbb.b(context, "context");
            accbb.b(str, "assetKey");
            accbb.b(str2, "iabString");
            OguryChoiceManagerExternal.access$checkTcfVersion(OguryChoiceManagerExternal.a, 2);
            OguryChoiceManagerExternal.a.setConsumedTcfVersion$consent_manager_prodRelease(Integer.valueOf(2));
            if (!(!babac.a((CharSequence) str)) || !(!babac.a((CharSequence) str2))) {
                aaccc aaccc = aaccc.a;
                aaccc.a("assetKey and iabString must not be empty");
                return;
            }
            b.a(context, str, str2, numArr);
        }

        public final aabab getClientConsentExternalV2$consent_manager_prodRelease() {
            return b;
        }

        public final void setClientConsentExternalV2$consent_manager_prodRelease(aabab aabab) {
            accbb.b(aabab, "<set-?>");
            b = aabab;
        }
    }

    private OguryChoiceManagerExternal() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0016  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0015 A[RETURN] */
    public static final /* synthetic */ void access$checkTcfVersion(OguryChoiceManagerExternal oguryChoiceManagerExternal, int i) {
        boolean z;
        if (b != null) {
            Integer num = b;
            if (num == null || num.intValue() != i) {
                z = false;
                if (z) {
                    throw new IllegalStateException("You cannot use method from another TCF version.".toString());
                }
                return;
            }
        }
        z = true;
        if (z) {
        }
    }

    public static /* synthetic */ void consumedTcfVersion$annotations() {
    }

    public final Integer getConsumedTcfVersion$consent_manager_prodRelease() {
        return b;
    }

    public final void setConsumedTcfVersion$consent_manager_prodRelease(Integer num) {
        b = num;
    }
}
