package com.ogury.cm;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import com.applovin.sdk.AppLovinEventTypes;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs.CastExtraArgs;

public final class abcbc implements abcba {
    public static final abcbc a = new abcbc();
    private static abcba b = abcab.a.a();

    private abcbc() {
    }

    public static void a() {
        Log.w("FairChoice", "FairChoice not available, switching to default implementation");
        abcab abcab = abcab.a;
        abcab.b();
        b = abcab.a.a();
    }

    public final void activateProduct(acaaa acaaa) {
        accbb.b(acaaa, AppLovinEventTypes.USER_VIEWED_PRODUCT);
        b.activateProduct(acaaa);
    }

    public final void endDataSourceConnections() {
        b.endDataSourceConnections();
    }

    public final boolean isBillingDisabled() {
        return b.isBillingDisabled();
    }

    public final boolean isProductActivated() {
        return b.isProductActivated();
    }

    public final void launchBillingFlow(Activity activity) {
        accbb.b(activity, "activity");
        b.launchBillingFlow(activity);
    }

    public final void queryProductDetails() {
        b.queryProductDetails();
    }

    public final void queryPurchase(acaac acaac) {
        accbb.b(acaac, CastExtraArgs.LISTENER);
        b.queryPurchase(acaac);
    }

    public final void setBillingFinishedListener(abcac abcac) {
        b.setBillingFinishedListener(abcac);
    }

    public final void setQueryProductDetailsListener(acaab acaab) {
        b.setQueryProductDetailsListener(acaab);
    }

    public final void startDataSourceConnections(Context context) {
        accbb.b(context, "context");
        b.startDataSourceConnections(context);
    }

    public final boolean tokenExistsForActiveProduct() {
        return b.tokenExistsForActiveProduct();
    }
}
