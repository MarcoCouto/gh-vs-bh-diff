package com.ogury.cm;

import android.content.Context;
import android.content.res.Resources;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import com.github.mikephil.charting.BuildConfig;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public final class abbab {
    public static final aaaaa a = new aaaaa(null);

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    public static String a(Context context, String str) {
        accbb.b(context, "context");
        accbb.b(str, ServerResponseWrapper.APP_KEY_FIELD);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("assetType", "android");
        jSONObject.put("assetKey", str);
        aacca aacca = aacca.a;
        jSONObject.put("deviceId", aacca.c().b());
        jSONObject.put(GeneralPropertiesWorker.SDK_VERSION, BuildConfig.VERSION_NAME);
        String str2 = "connectivity";
        NetworkInfo a2 = com.ogury.cm.ConsentActivity.aaaaa.a(context);
        jSONObject.put(str2, a2 != null ? a2.getTypeName() : null);
        String str3 = RequestParameters.DEVICE_MODEL;
        StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER);
        sb.append(" ");
        sb.append(Build.MODEL);
        String sb2 = sb.toString();
        if (sb2.length() > 32) {
            if (sb2 != null) {
                sb2 = sb2.substring(0, 31);
                accbb.a((Object) sb2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            } else {
                throw new acbab("null cannot be cast to non-null type java.lang.String");
            }
        }
        jSONObject.put(str3, sb2);
        jSONObject.put("deviceOsVersion", String.valueOf(VERSION.SDK_INT));
        Context applicationContext = context.getApplicationContext();
        accbb.a((Object) applicationContext, "context.applicationContext");
        Resources resources = applicationContext.getResources();
        accbb.a((Object) resources, "context.applicationContext.resources");
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        accbb.a((Object) displayMetrics, "context.applicationConte….resources.displayMetrics");
        jSONObject.put("deviceScreenWidth", displayMetrics.widthPixels);
        jSONObject.put("deviceScreenHeight", displayMetrics.heightPixels);
        String str4 = String.BUNDLE;
        aacca aacca2 = aacca.a;
        jSONObject.put(str4, aacca.c().a());
        StringBuilder sb3 = new StringBuilder();
        Locale locale = Locale.getDefault();
        accbb.a((Object) locale, "Locale.getDefault()");
        sb3.append(locale.getLanguage());
        sb3.append("-");
        Locale locale2 = Locale.getDefault();
        accbb.a((Object) locale2, "Locale.getDefault()");
        sb3.append(locale2.getCountry());
        jSONObject.put("locale", sb3.toString());
        jSONObject.put("deviceScreenDensity", displayMetrics.densityDpi);
        if (abcbc.a.isBillingDisabled()) {
            jSONObject.put("billingEnabled", false);
        }
        abccb abccb = abccb.a;
        String a3 = abccb.a();
        if (a3 != null) {
            jSONObject.put("purchaseTokens", new JSONArray(a3));
        }
        String jSONObject2 = jSONObject.toString();
        accbb.a((Object) jSONObject2, "json.toString()");
        return jSONObject2;
    }

    public static String a(String str) {
        accbb.b(str, "errorMessage");
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("event", "CONSENT_ERROR");
        jSONObject.put("errorMessage", str);
        String jSONObject2 = jSONObject.toString();
        accbb.a((Object) jSONObject2, "json.toString()");
        return jSONObject2;
    }

    public static String a(String str, String str2, Integer[] numArr) {
        accbb.b(str, ServerResponseWrapper.APP_KEY_FIELD);
        accbb.b(str2, "iabString");
        JSONObject jSONObject = new JSONObject();
        a(jSONObject, str, str2);
        a(jSONObject, numArr);
        String jSONObject2 = jSONObject.toString();
        accbb.a((Object) jSONObject2, "json.toString()");
        return jSONObject2;
    }

    public static String a(String str, String str2, String[] strArr) {
        accbb.b(str, ServerResponseWrapper.APP_KEY_FIELD);
        accbb.b(str2, "iabString");
        JSONObject jSONObject = new JSONObject();
        a(jSONObject, str, str2);
        a(jSONObject, strArr);
        String jSONObject2 = jSONObject.toString();
        accbb.a((Object) jSONObject2, "json.toString()");
        return jSONObject2;
    }

    private static void a(JSONObject jSONObject, String str, String str2) {
        jSONObject.put("assetType", "android");
        jSONObject.put("assetKey", str);
        aacca aacca = aacca.a;
        jSONObject.put("deviceId", aacca.c().b());
        jSONObject.put("iabString", str2);
    }

    private static void a(JSONObject jSONObject, Integer[] numArr) {
        JSONArray jSONArray = new JSONArray();
        if (numArr != null) {
            if (!(numArr.length == 0)) {
                for (Integer intValue : numArr) {
                    jSONArray.put(intValue.intValue());
                }
                jSONObject.put("vendorIds", jSONArray);
            }
        }
    }

    private static void a(JSONObject jSONObject, String[] strArr) {
        JSONArray jSONArray = new JSONArray();
        if (strArr != null) {
            if (!(strArr.length == 0)) {
                for (String put : strArr) {
                    jSONArray.put(put);
                }
                jSONObject.put(String.VENDORS, jSONArray);
            }
        }
    }
}
