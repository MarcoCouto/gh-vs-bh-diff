package com.ogury.cm;

import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public final class abbbc extends abbba {
    public static final aaaaa b = new aaaaa(null);
    private String c = "userConsent";

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    public final String a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final boolean a(JSONObject jSONObject) {
        boolean a = super.a(jSONObject);
        if (a) {
            aacca aacca = aacca.a;
            ababc ababc = (ababc) ababa.f();
            if (jSONObject == null) {
                accbb.a();
            }
            ababc.a(jSONObject.optJSONObject("iabResponse"));
            JSONObject optJSONObject = jSONObject.optJSONObject(ServerResponseWrapper.RESPONSE_FIELD);
            JSONObject optJSONObject2 = optJSONObject.optJSONObject(this.c);
            JSONObject optJSONObject3 = jSONObject.optJSONObject("sdk");
            accbb.a((Object) optJSONObject3, "jsonResponse.optJSONObject(SDK)");
            JSONObject optJSONObject4 = optJSONObject3.optJSONObject("unifiedVendors");
            accbb.a((Object) optJSONObject4, "sdkObject.optJSONObject(UNIFIED_VENDORS)");
            ababc.a(optJSONObject.optBoolean("hasPaid"));
            ababc.a(optJSONObject2.optInt("purposes"));
            ababc.c(optJSONObject2.optInt("specialFeatures"));
            String optString = optJSONObject2.optString(String.VENDORS, "");
            accbb.a((Object) optString, "userConsent.optString(VENDORS, \"\")");
            Object[] array = com.ogury.cm.ConsentActivity.aaaaa.a(com.ogury.cm.ConsentActivity.aaaaa.b(optString)).toArray(new Integer[0]);
            if (array != null) {
                ababc.a((Integer[]) array);
                String optString2 = optJSONObject2.optString("vendorsLI", "");
                accbb.a((Object) optString2, "userConsent.optString(VENDORS_LI, \"\")");
                Object[] array2 = com.ogury.cm.ConsentActivity.aaaaa.a(com.ogury.cm.ConsentActivity.aaaaa.b(optString2)).toArray(new Integer[0]);
                if (array2 != null) {
                    ababc.b((Integer[]) array2);
                    ababc.b(optJSONObject2.optInt("purposesLI"));
                    ababc.b(optJSONObject4.optJSONObject(String.VENDORS));
                    ababc.d(optJSONObject2.optInt("maxVendorId"));
                    String optString3 = optJSONObject.optString("iabString", "");
                    accbb.a((Object) optString3, "responseObject.optString(IAB_STRING, \"\")");
                    ababc.a(optString3);
                } else {
                    throw new acbab("null cannot be cast to non-null type kotlin.Array<T>");
                }
            } else {
                throw new acbab("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return a;
    }
}
