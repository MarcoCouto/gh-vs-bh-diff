package com.ogury.cm;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.core.OguryError;
import java.util.Iterator;
import java.util.List;

public final class aaaac {
    public static final aaaac a = new aaaac();
    private static aacbc b;
    private static aaaaa c;
    private static WebView d;
    private static final Handler e = new Handler();

    public static final class aaaaa implements aacbc {
        final /* synthetic */ com.ogury.cm.accca.aaaaa a;
        final /* synthetic */ com.ogury.cm.accca.aaaaa b;
        final /* synthetic */ Context c;

        aaaaa(com.ogury.cm.accca.aaaaa aaaaa, com.ogury.cm.accca.aaaaa aaaaa2, Context context) {
            this.a = aaaaa;
            this.b = aaaaa2;
            this.c = context;
        }

        public final void a(OguryError oguryError) {
            accbb.b(oguryError, "error");
            aaaac aaaac = aaaac.a;
            aaaac.d().removeCallbacksAndMessages(null);
            aaaac aaaac2 = aaaac.a;
            aacbc a2 = aaaac.a();
            if (a2 != null) {
                a2.a(oguryError);
            }
        }

        public final void a(String str) {
            accbb.b(str, ServerResponseWrapper.RESPONSE_FIELD);
            if (accbb.a((Object) str, (Object) ParametersKeys.READY)) {
                this.a.a = true;
                aaaac aaaac = aaaac.a;
                boolean z = this.b.a;
                aaaac aaaac2 = aaaac.a;
                aaaac.a(z, aaaac.a(), 15000);
            } else if (accbb.a((Object) str, (Object) "success")) {
                this.b.a = true;
                aaaac.a(aaaac.a, this.c);
                aaaac aaaac3 = aaaac.a;
                aaaac.d().removeCallbacksAndMessages(null);
            } else {
                if (babac.a(str, "parsedConfig=", false, 2, null)) {
                    aaaac.a(aaaac.a, this.c, str);
                }
            }
        }
    }

    public static final class aaaab implements aacac {
        final /* synthetic */ Context a;
        final /* synthetic */ String b;

        static final class aaaaa implements Runnable {
            final /* synthetic */ aaaab a;
            final /* synthetic */ String b;

            aaaaa(aaaab aaaab, String str) {
                this.a = aaaab;
                this.b = str;
            }

            public final void run() {
                aacca aacca = aacca.a;
                aacca.c().b(this.b);
                com.ogury.cm.abaaa.aaaaa aaaaa = abaaa.a;
                com.ogury.cm.abaaa.aaaaa.a().a(this.a.a);
                com.ogury.cm.abbcb.aaaaa aaaaa2 = abbcb.a;
                abbcb.a(com.ogury.cm.abbcb.aaaaa.a(), this.a.a, null, 2, null);
                aaaac aaaac = aaaac.a;
                aacbc a2 = aaaac.a();
                if (a2 != null) {
                    a2.a(this.a.b);
                }
            }
        }

        aaaab(Context context, String str) {
            this.a = context;
            this.b = str;
        }

        public final void a(String str) {
            accbb.b(str, "aaid");
            new Handler(Looper.getMainLooper()).post(new aaaaa(this, str));
        }
    }

    /* renamed from: com.ogury.cm.aaaac$aaaac reason: collision with other inner class name */
    static final class C0057aaaac implements Runnable {
        final /* synthetic */ boolean a;
        final /* synthetic */ aacbc b;

        C0057aaaac(boolean z, aacbc aacbc) {
            this.a = z;
            this.b = aacbc;
        }

        public final void run() {
            if (!this.a) {
                aaaac.a(this.b, new OguryError(1002, "Timeout error"));
            }
        }
    }

    private aaaac() {
    }

    public static aacbc a() {
        return b;
    }

    public static void a(Context context, aacbc aacbc) {
        accbb.b(context, "context");
        accbb.b(aacbc, "callback");
        abcbc.a.queryProductDetails();
        b = aacbc;
        com.ogury.cm.accca.aaaaa aaaaa2 = new com.ogury.cm.accca.aaaaa();
        aaaaa2.a = false;
        com.ogury.cm.accca.aaaaa aaaaa3 = new com.ogury.cm.accca.aaaaa();
        aaaaa3.a = false;
        try {
            WebView webView = new WebView(context.getApplicationContext(), null);
            d = webView;
            WebSettings settings = webView.getSettings();
            if (settings != null) {
                settings.setJavaScriptEnabled(true);
            }
            WebView webView2 = d;
            if (webView2 != null) {
                webView2.setBackgroundColor(0);
            }
            WebView webView3 = d;
            if (webView3 != null) {
                webView3.setLayerType(1, null);
            }
            c = new aaaaa(context, new aaaaa(aaaaa2, aaaaa3, context));
            WebView webView4 = d;
            if (webView4 != null) {
                webView4.setWebViewClient(c);
            }
            a(aaaaa2.a, aacbc, 20000);
            WebView webView5 = d;
            if (webView5 != null) {
                StringBuilder sb = new StringBuilder("https://consent-form.ogury.co");
                aacca aacca = aacca.a;
                sb.append(aacca.c().e());
                sb.append("?assetType=android");
                webView5.loadUrl(sb.toString());
            }
        } catch (Exception unused) {
            aaccc aaccc = aaccc.a;
            aaccc.b("Cannot create WebView");
            a(b, new OguryError(4, "Cannot create WebView"));
        }
    }

    public static void a(WebView webView) {
        d = null;
    }

    public static void a(aaaaa aaaaa2) {
        c = null;
    }

    public static final /* synthetic */ void a(aaaac aaaac, Context context) {
        Object systemService = context.getSystemService("activity");
        if (systemService != null) {
            List runningAppProcesses = ((ActivityManager) systemService).getRunningAppProcesses();
            boolean z = false;
            if (runningAppProcesses != null) {
                String packageName = context.getPackageName();
                Iterator it = runningAppProcesses.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z = true;
                        break;
                    }
                    RunningAppProcessInfo runningAppProcessInfo = (RunningAppProcessInfo) it.next();
                    if (runningAppProcessInfo.importance == 100 && accbb.a((Object) runningAppProcessInfo.processName, (Object) packageName)) {
                        break;
                    }
                }
            }
            if (!z) {
                com.ogury.cm.ConsentActivity.aaaaa aaaaa2 = ConsentActivity.a;
                accbb.b(context, "context");
                context.startActivity(new Intent(context, ConsentActivity.class));
                return;
            }
            a(b, new OguryError(4, "App in background"));
            return;
        }
        throw new acbab("null cannot be cast to non-null type android.app.ActivityManager");
    }

    public static final /* synthetic */ void a(aaaac aaaac, Context context, String str) {
        if (str != null) {
            String substring = str.substring(13);
            accbb.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
            aacba aacba = aacba.a;
            aacba.a(context, (aacac) new aaaab(context, substring));
            return;
        }
        throw new acbab("null cannot be cast to non-null type java.lang.String");
    }

    /* access modifiers changed from: private */
    public static void a(aacbc aacbc, OguryError oguryError) {
        d = null;
        c = null;
        if (aacbc != null) {
            aacbc.a(oguryError);
        }
    }

    /* access modifiers changed from: private */
    public static void a(boolean z, aacbc aacbc, long j) {
        e.postDelayed(new C0057aaaac(z, aacbc), j);
    }

    public static aaaaa b() {
        return c;
    }

    public static WebView c() {
        return d;
    }

    public static Handler d() {
        return e;
    }
}
