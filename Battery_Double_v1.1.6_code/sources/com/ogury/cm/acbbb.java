package com.ogury.cm;

import android.content.Context;
import com.ogury.cm.ConsentActivity.aaaaa;
import java.io.Closeable;

public final class acbbb {
    public static int a(Integer[] numArr) {
        accbb.b(numArr, "receiver$0");
        int i = 0;
        for (Integer intValue : numArr) {
            i += intValue.intValue();
        }
        return i;
    }

    private static <T, A extends Appendable> A a(T[] tArr, A a, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, accac<? super T, ? extends CharSequence> accac) {
        accbb.b(tArr, "receiver$0");
        accbb.b(a, "buffer");
        accbb.b(charSequence, "separator");
        accbb.b(charSequence2, "prefix");
        accbb.b(charSequence3, "postfix");
        accbb.b(charSequence4, "truncated");
        a.append(charSequence2);
        int i2 = 0;
        for (T t : tArr) {
            i2++;
            if (i2 > 1) {
                a.append(charSequence);
            }
            babac.a((Appendable) a, t, null);
        }
        a.append(charSequence3);
        return a;
    }

    public static /* synthetic */ String a(Object[] objArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, accac accac, int i2, Object obj) {
        CharSequence charSequence5 = ", ";
        CharSequence charSequence6 = "...";
        Object[] objArr2 = objArr;
        accbb.b(objArr, "receiver$0");
        accbb.b(charSequence5, "separator");
        CharSequence charSequence7 = charSequence2;
        accbb.b(charSequence2, "prefix");
        CharSequence charSequence8 = charSequence3;
        accbb.b(charSequence3, "postfix");
        accbb.b(charSequence6, "truncated");
        String sb = ((StringBuilder) a(objArr2, new StringBuilder(), charSequence5, charSequence7, charSequence8, -1, charSequence6, null)).toString();
        accbb.a((Object) sb, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb;
    }

    public static void a(Closeable closeable, Throwable th) {
        if (th == null) {
            closeable.close();
            return;
        }
        try {
            closeable.close();
        } catch (Throwable th2) {
            accbb.b(th, "receiver$0");
            accbb.b(th2, "exception");
            acbcc.a.a(th, th2);
        }
    }

    public static boolean a(Context context) {
        accbb.b(context, "context");
        return aaaaa.b(context);
    }
}
