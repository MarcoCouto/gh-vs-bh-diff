package com.ogury.cm;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class abbcc extends abbcb {
    private String b = "cacheConsent";

    private static ababb b() {
        aacca aacca = aacca.a;
        return (ababb) ababa.f();
    }

    public final String a() {
        return this.b;
    }

    public final boolean a(String str, Context context) {
        accbb.b(str, "assetKey");
        accbb.b(context, "context");
        boolean a = super.a(str, context);
        if (a) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(this.b, 0);
            ababb b2 = b();
            String string = sharedPreferences.getString("purposes", "");
            accbb.a((Object) string, "prefs.getString(PURPOSES, \"\")");
            b2.d(string);
            String string2 = sharedPreferences.getString("acceptedVendors", "");
            accbb.a((Object) string2, "prefs.getString(ACCEPTED_VENDORS, \"\")");
            b2.b(string2);
            String string3 = sharedPreferences.getString("refusedVendors", "");
            accbb.a((Object) string3, "prefs.getString(REFUSED_VENDORS, \"\")");
            b2.c(string3);
            String string4 = sharedPreferences.getString("specialFeatures", "");
            accbb.a((Object) string4, "prefs.getString(SPECIAL_FEATURES, \"\")");
            b2.e(string4);
        }
        return a;
    }

    /* access modifiers changed from: protected */
    public final void d(Context context) {
        accbb.b(context, "context");
        super.d(context);
        Editor edit = context.getSharedPreferences(this.b, 0).edit();
        ababb b2 = b();
        edit.putString("purposes", b2.i());
        edit.putString("acceptedVendors", b2.g());
        edit.putString("refusedVendors", b2.h());
        edit.putString("specialFeatures", b2.j());
        edit.apply();
    }
}
