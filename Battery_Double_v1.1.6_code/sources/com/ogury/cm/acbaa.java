package com.ogury.cm;

import java.io.Serializable;

public final class acbaa<A, B> implements Serializable {
    private final A a;
    private final B b;

    public acbaa(A a2, B b2) {
        this.a = a2;
        this.b = b2;
    }

    public final A a() {
        return this.a;
    }

    public final B b() {
        return this.b;
    }

    public final A c() {
        return this.a;
    }

    public final B d() {
        return this.b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (com.ogury.cm.accbb.a((java.lang.Object) r2.b, (java.lang.Object) r3.b) != false) goto L_0x001f;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof acbaa) {
                acbaa acbaa = (acbaa) obj;
                if (accbb.a((Object) this.a, (Object) acbaa.a)) {
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        A a2 = this.a;
        int i = 0;
        int hashCode = (a2 != null ? a2.hashCode() : 0) * 31;
        B b2 = this.b;
        if (b2 != null) {
            i = b2.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("(");
        sb.append(this.a);
        sb.append(", ");
        sb.append(this.b);
        sb.append(')');
        return sb.toString();
    }
}
