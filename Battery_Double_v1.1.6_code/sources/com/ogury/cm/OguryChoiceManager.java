package com.ogury.cm;

import android.app.Activity;
import android.content.Context;
import com.ogury.cm.aabba.aaaaa;

public final class OguryChoiceManager {
    public static final OguryChoiceManager INSTANCE = new OguryChoiceManager();
    public static final int TCF_VERSION_1 = 1;
    public static final int TCF_VERSION_2 = 2;
    private static aaaba a;
    private static baacc b;
    private static Context c;
    private static String d;
    private static boolean e;
    private static boolean f;

    public enum Answer {
        FULL_APPROVAL,
        PARTIAL_APPROVAL,
        REFUSAL,
        NO_ANSWER
    }

    public static final class TcfV1 {
        public static final TcfV1 INSTANCE = new TcfV1();

        public enum Purpose {
            INFORMATION,
            PERSONALISATION,
            AD,
            CONTENT,
            MEASUREMENT
        }

        public static final class SpecialFeature {
            public static final SpecialFeature INSTANCE = new SpecialFeature();
            public static final int PRECISE_GEOLOCATION = 1;

            private SpecialFeature() {
            }
        }

        private TcfV1() {
        }

        public static final String getIabString() {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 1);
            if (OguryChoiceManager.INSTANCE.getClientConsentImpl() == null) {
                accbb.a();
            }
            return aaaba.d();
        }

        public static final boolean isAccepted(String str) {
            accbb.b(str, "vendorSlug");
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 1);
            if (OguryChoiceManager.INSTANCE.getClientConsentImpl() != null) {
                accbb.b(str, "receiver$0");
                StringBuilder sb = new StringBuilder("\"");
                sb.append(str);
                sb.append("\"");
                return aaabc.b(sb.toString());
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV1");
        }

        public static final boolean isPurposeAccepted(int i) {
            Purpose purpose;
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 1);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                aaabc aaabc = (aaabc) clientConsentImpl;
                if (i < 0 || 4 < i) {
                    return false;
                }
                if (i == Purpose.INFORMATION.ordinal()) {
                    purpose = Purpose.INFORMATION;
                } else if (i == Purpose.PERSONALISATION.ordinal()) {
                    purpose = Purpose.PERSONALISATION;
                } else if (i == Purpose.AD.ordinal()) {
                    purpose = Purpose.AD;
                } else if (i == Purpose.CONTENT.ordinal()) {
                    purpose = Purpose.CONTENT;
                } else if (i == Purpose.MEASUREMENT.ordinal()) {
                    purpose = Purpose.MEASUREMENT;
                } else {
                    throw new IllegalArgumentException("Bad purpose number used! Please use one of the declared ones in OguryChoiceManager TcfV1 class.");
                }
                return aaabc.a(purpose);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV1");
        }

        public static final boolean isPurposeAccepted(Purpose purpose) {
            accbb.b(purpose, "purpose");
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 1);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaabc) clientConsentImpl).a(purpose);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV1");
        }

        public static final boolean isSpecialFeatureAccepted(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 1);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaabc) clientConsentImpl).a(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV1");
        }
    }

    public static final class TcfV2 {
        public static final TcfV2 INSTANCE = new TcfV2();

        public static final class Purpose {
            public static final int CREATE_PERSONALISED_ADS = 8;
            public static final int CREATE_PERSONALISED_CONTENT = 32;
            public static final int DEVELOP_AND_IMPROVE_PRODUCTS = 1024;
            public static final Purpose INSTANCE = new Purpose();
            public static final int MARKET_RESEARCH = 512;
            public static final int MEASURE_AD_PERFORMANCE = 128;
            public static final int MEASURE_CONTENT_PERFORMANCE = 256;
            public static final int SELECT_BASIC_ADS = 4;
            public static final int SELECT_PERSONALISED_ADS = 16;
            public static final int SELECT_PERSONALISED_CONTENT = 64;
            public static final int STORE_INFORMATION = 2;

            private Purpose() {
            }
        }

        public static final class SpecialFeature {
            public static final SpecialFeature INSTANCE = new SpecialFeature();
            public static final int PRECISE_GEOLOCATION = 2;
            public static final int SCAN_DEVICE_CHARACTERISTICS = 4;

            private SpecialFeature() {
            }
        }

        private TcfV2() {
        }

        public static final String getIabString() {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            if (OguryChoiceManager.INSTANCE.getClientConsentImpl() == null) {
                accbb.a();
            }
            return aaaba.d();
        }

        public static final boolean isAccepted(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaacb) clientConsentImpl).c(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV2");
        }

        public static final boolean isAllVendorConditionsMet(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaacb) clientConsentImpl).h(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV2");
        }

        public static final boolean isLiPurposeMet(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaacb) clientConsentImpl).e(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV2");
        }

        public static final boolean isLiVendorMet(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaacb) clientConsentImpl).d(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV2");
        }

        public static final boolean isPurposeAccepted(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaacb) clientConsentImpl).b(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV2");
        }

        public static final boolean isSpecialFeatureAccepted(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaacb) clientConsentImpl).a(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV2");
        }

        public static final boolean isVendorAndItsPurposesAccepted(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaacb) clientConsentImpl).f(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV2");
        }

        public static final boolean isVendorsLiAndLiPurposeMet(int i) {
            OguryChoiceManager.access$checkInstance(OguryChoiceManager.INSTANCE, 2);
            aaaba clientConsentImpl = OguryChoiceManager.INSTANCE.getClientConsentImpl();
            if (clientConsentImpl != null) {
                return ((aaacb) clientConsentImpl).g(i);
            }
            throw new acbab("null cannot be cast to non-null type com.ogury.cm.choiceManager.ClientConsentImplV2");
        }
    }

    private OguryChoiceManager() {
    }

    private static void a() {
        if (f) {
            d();
        } else {
            b();
        }
    }

    public static final /* synthetic */ void access$checkInstance(OguryChoiceManager oguryChoiceManager, int i) {
        a();
        switch (i) {
            case 1:
                if (!(a instanceof aaabc)) {
                    throw new IllegalStateException("You cannot use method from another TCF version.".toString());
                }
                return;
            case 2:
                if (!(a instanceof aaacb)) {
                    throw new IllegalStateException("You cannot use method from another TCF version.".toString());
                }
                return;
            default:
                throw new IllegalArgumentException("Bad TCF version is passed, you can use one of versions declared in class OguryChoiceManager.");
        }
    }

    public static final void ask(Activity activity, OguryConsentListener oguryConsentListener) {
        accbb.b(activity, "activity");
        accbb.b(oguryConsentListener, "oguryConsentListener");
        e = true;
        a();
        aaaba aaaba = a;
        if (aaaba == null) {
            accbb.a();
        }
        Context context = activity;
        String str = d;
        if (str == null) {
            accbb.a();
        }
        aaaba.a(context, str, oguryConsentListener);
    }

    private static void b() {
        if (c != null) {
            CharSequence charSequence = d;
            if (charSequence == null || babac.a(charSequence)) {
                throw new IllegalStateException("AssetKey is not allowed to be empty.");
            } else if (!e) {
                c();
                Context context = c;
                if (context == null) {
                    accbb.a();
                }
                if (!baacc.a(context)) {
                    aaccc aaccc = aaccc.a;
                    aaccc.a("You must first call ask to get config.");
                    return;
                }
                throw new IllegalStateException("You must first call ask to get config.");
            }
        } else {
            throw new IllegalStateException("You must first call initialize method.");
        }
    }

    private static baacc c() {
        if (b == null) {
            b = new baacc();
        }
        baacc baacc = b;
        if (baacc == null) {
            accbb.a();
        }
        return baacc;
    }

    public static /* synthetic */ void clientConsentImpl$annotations() {
    }

    private static void d() {
        if (d == null) {
            aaccc aaccc = aaccc.a;
            aaccc.a("You must first call ask to initialize SDK.");
            return;
        }
        String str = d;
        if (str == null) {
            accbb.a();
        }
        if (babac.a((CharSequence) str)) {
            aaccc aaccc2 = aaccc.a;
            aaccc.a("AssetKey is not allowed to be empty.");
        }
    }

    public static /* synthetic */ void debugUtilVal$annotations() {
    }

    public static final void edit(Activity activity, OguryConsentListener oguryConsentListener) {
        accbb.b(activity, "activity");
        accbb.b(oguryConsentListener, "oguryConsentListener");
        e = true;
        a();
        aaaba aaaba = a;
        if (aaaba == null) {
            accbb.a();
        }
        Context context = activity;
        String str = d;
        if (str == null) {
            accbb.a();
        }
        aaaba.b(context, str, oguryConsentListener);
    }

    public static final boolean gdprApplies() {
        a();
        if (a == null) {
            accbb.a();
        }
        return aaaba.e();
    }

    public static final boolean hasPaid() {
        a();
        aaaba aaaba = a;
        if (aaaba == null) {
            accbb.a();
        }
        Context context = c;
        if (context == null) {
            accbb.a();
        }
        accbb.b(context, "context");
        abbcb abbcb = aaaba.a;
        if (abbcb == null) {
            accbb.a("sharedPrefsHandler");
        }
        return abbcb.a(context);
    }

    public static final void initialize(Context context, String str, OguryCmConfig oguryCmConfig) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        accbb.b(oguryCmConfig, "oguryCmConfig");
        aaaaa aaaaa = aabba.a;
        aaaaa.a(context, oguryCmConfig.getTcfVersion());
        if (a == null) {
            c = context.getApplicationContext();
            d = str;
            aaabb aaabb = aaabb.a;
            a = aaabb.a(oguryCmConfig.getTcfVersion());
            return;
        }
        aaccc aaccc = aaccc.a;
        aaccc.a("SDK initialize is already done.");
    }

    public static /* synthetic */ void initialize$default(Context context, String str, OguryCmConfig oguryCmConfig, int i, Object obj) {
        if ((i & 4) != 0) {
            oguryCmConfig = new OguryCmConfig();
        }
        initialize(context, str, oguryCmConfig);
    }

    public static final boolean isEditAvailable() {
        a();
        aaaba aaaba = a;
        if (aaaba == null) {
            accbb.a();
        }
        Context context = c;
        if (context == null) {
            accbb.a();
        }
        accbb.b(context, "context");
        abbcb abbcb = aaaba.a;
        if (abbcb == null) {
            accbb.a("sharedPrefsHandler");
        }
        accbb.b(context, "context");
        return context.getSharedPreferences(abbcb.a(), 0).getBoolean("isEditAvailable", true);
    }

    public static final void updateOguryCmConfig(OguryCmConfig oguryCmConfig) {
        accbb.b(oguryCmConfig, "oguryCmConfig");
        int tcfVersion = oguryCmConfig.getTcfVersion();
        if (!e && c != null) {
            aacca aacca = aacca.a;
            if (aacca.a() != tcfVersion) {
                aaaaa aaaaa = aabba.a;
                Context context = c;
                if (context == null) {
                    accbb.a();
                }
                aaaaa.a(context, tcfVersion);
                aaabb aaabb = aaabb.a;
                a = aaabb.a(tcfVersion);
                return;
            }
        }
        if (c == null) {
            aaccc aaccc = aaccc.a;
            aaccc.a("Initialization is required before changing TCF version.");
            return;
        }
        if (e) {
            aaccc aaccc2 = aaccc.a;
            aaccc.a("Ask method is already called.");
        }
    }

    public final aaaba getClientConsentImpl() {
        return a;
    }

    public final baacc getDebugUtilVal$3dacfc6e$16412e62() {
        return b;
    }

    public final void initializeTcfV1$consent_manager_prodRelease(Context context) {
        f = true;
        aacca aacca = aacca.a;
        aacca.a(1);
        if (c == null) {
            c = context != null ? context.getApplicationContext() : null;
        }
        if (a == null) {
            aaabb aaabb = aaabb.a;
            a = aaabb.a(1);
        }
    }

    public final void resetClientConsentImpl$consent_manager_prodRelease() {
        a = null;
    }

    public final void resetFieldsForTests() {
        c = null;
        d = null;
        e = false;
        f = false;
    }

    public final void setClientConsentImpl(aaaba aaaba) {
        a = aaaba;
    }

    public final void setDebugUtilVal$47bacc58$5428575c(baacc baacc) {
        b = baacc;
    }
}
