package com.ogury.cm;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.core.OguryError;
import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ConsentActivity extends Activity {
    public static final aaaaa a = new aaaaa(null);
    private final LayoutParams b = new LayoutParams(-1, -1);
    private HashMap c;

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }

        private static int a(int i, int i2) {
            int i3 = i % 1;
            return i3 >= 0 ? i3 : i3 + 1;
        }

        public static int a(int i, int i2, int i3) {
            return i >= i2 ? i2 : i2 - a(a(i2, 1) - a(i, 1), 1);
        }

        public static /* synthetic */ long a(InputStream inputStream, OutputStream outputStream, int i, int i2, Object obj) {
            accbb.b(inputStream, "receiver$0");
            accbb.b(outputStream, "out");
            byte[] bArr = new byte[8192];
            int read = inputStream.read(bArr);
            long j = 0;
            while (read >= 0) {
                outputStream.write(bArr, 0, read);
                j += (long) read;
                read = inputStream.read(bArr);
            }
            return j;
        }

        private static long a(Reader reader, Writer writer, int i) {
            accbb.b(reader, "receiver$0");
            accbb.b(writer, "out");
            char[] cArr = new char[8192];
            int read = reader.read(cArr);
            long j = 0;
            while (read >= 0) {
                writer.write(cArr, 0, read);
                j += (long) read;
                read = reader.read(cArr);
            }
            return j;
        }

        public static NetworkInfo a(Context context) {
            accbb.b(context, "receiver$0");
            String str = "android.permission.ACCESS_NETWORK_STATE";
            accbb.b(context, "receiver$0");
            accbb.b(str, ParametersKeys.PERMISSION);
            if (!(context.checkCallingOrSelfPermission(str) == 0)) {
                return null;
            }
            Object systemService = context.getSystemService("connectivity");
            if (systemService != null) {
                return ((ConnectivityManager) systemService).getActiveNetworkInfo();
            }
            throw new acbab("null cannot be cast to non-null type android.net.ConnectivityManager");
        }

        public static <A, B> acbaa<A, B> a(A a, B b) {
            return new acbaa<>(a, b);
        }

        public static String a(Reader reader) {
            accbb.b(reader, "receiver$0");
            StringWriter stringWriter = new StringWriter();
            a(reader, (Writer) stringWriter, 8192);
            String stringWriter2 = stringWriter.toString();
            accbb.a((Object) stringWriter2, "buffer.toString()");
            return stringWriter2;
        }

        public static List<Object> a(JSONArray jSONArray) {
            accbb.b(jSONArray, "receiver$0");
            int length = jSONArray.length();
            ArrayList arrayList = new ArrayList(length);
            for (int i = 0; i < length; i++) {
                arrayList.add(jSONArray.get(i));
            }
            return arrayList;
        }

        public static JSONObject a(String str) {
            accbb.b(str, "receiver$0");
            try {
                return new JSONObject(str);
            } catch (Exception unused) {
                return null;
            }
        }

        public static void a(Closeable closeable) {
            accbb.b(closeable, "receiver$0");
            try {
                closeable.close();
            } catch (Throwable unused) {
            }
        }

        public static JSONArray b(String str) {
            accbb.b(str, "receiver$0");
            try {
                return new JSONArray(str);
            } catch (Exception unused) {
                return new JSONArray();
            }
        }

        public static boolean b(Context context) {
            accbb.b(context, "receiver$0");
            NetworkInfo a = a(context);
            return a != null && a.isConnected();
        }

        public static int c(String str) {
            accbb.b(str, "receiver$0");
            try {
                List<Number> a = acccc.a((Object) a(b(str)));
                if (!a.isEmpty()) {
                    int i = 0;
                    for (Number intValue : a) {
                        i += intValue.intValue();
                    }
                    return i;
                }
            } catch (Exception e) {
                aaccc aaccc = aaccc.a;
                aaccc.a("Error in intJsonArraySum extension", e);
            }
            return 0;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this.c != null) {
            this.c.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        View view = (View) this.c.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this.c.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            finish();
            return;
        }
        aaaac aaaac = aaaac.a;
        if (aaaac.c() != null) {
            Context context = this;
            FrameLayout frameLayout = new FrameLayout(context, null);
            frameLayout.setLayoutParams(this.b);
            aaaac aaaac2 = aaaac.a;
            aaaaa b2 = aaaac.b();
            if (b2 != null) {
                b2.a(context);
            }
            aaaac aaaac3 = aaaac.a;
            WebView c2 = aaaac.c();
            if (VERSION.SDK_INT < 16 && c2 != null) {
                c2.setLayerType(1, null);
            }
            aaaac aaaac4 = aaaac.a;
            frameLayout.addView(aaaac.c(), this.b);
            aaaac aaaac5 = aaaac.a;
            aaaac.a((WebView) null);
            setContentView(frameLayout);
            return;
        }
        aaaac aaaac6 = aaaac.a;
        aacbc a2 = aaaac.a();
        if (a2 != null) {
            a2.a(new OguryError(1003, "Cached webview has been destroyed"));
        }
        aaccc aaccc = aaccc.a;
        aaccc.b("Cached webview has been destroyed");
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        aaaac aaaac = aaaac.a;
        aaaac.a((WebView) null);
        aaaac aaaac2 = aaaac.a;
        aaaac.a((aaaaa) null);
        super.onDestroy();
    }
}
