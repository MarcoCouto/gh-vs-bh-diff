package com.ogury.cm;

import com.facebook.share.internal.ShareConstants;
import com.ogury.core.OguryError;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

public final class abacc {
    public static final aaaaa a = new aaaaa(null);

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    private static String a(InputStream inputStream, String str) {
        Throwable th;
        if (str != null) {
            if (str == null ? false : str.equalsIgnoreCase(HttpRequest.ENCODING_GZIP)) {
                abacb abacb = abacb.a;
                accbb.b(inputStream, "receiver$0");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(Math.max(8192, inputStream.available()));
                com.ogury.cm.ConsentActivity.aaaaa.a(inputStream, byteArrayOutputStream, 0, 2, null);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                accbb.a((Object) byteArray, "buffer.toByteArray()");
                return abacb.a(byteArray);
            }
        }
        Closeable bufferedReader = new BufferedReader(new InputStreamReader(inputStream, babaa.a), 8192);
        try {
            String a2 = com.ogury.cm.ConsentActivity.aaaaa.a((Reader) (BufferedReader) bufferedReader);
            acbbb.a(bufferedReader, null);
            return a2;
        } catch (Throwable th2) {
            acbbb.a(bufferedReader, th);
            throw th2;
        }
    }

    public static void a(abbaa abbaa) {
        OutputStream outputStream;
        accbb.b(abbaa, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        try {
            URLConnection openConnection = new URL(abbaa.c()).openConnection();
            if (openConnection != null) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) openConnection;
                httpsURLConnection.setReadTimeout(10000);
                httpsURLConnection.setConnectTimeout(30000);
                httpsURLConnection.setRequestMethod(abbaa.a());
                httpsURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpsURLConnection.setRequestProperty("Accept", "application/json");
                httpsURLConnection.setRequestProperty(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
                httpsURLConnection.setRequestProperty(HttpRequest.HEADER_ACCEPT_ENCODING, HttpRequest.ENCODING_GZIP);
                List<acbaa> e = abbaa.e();
                if (e != null) {
                    for (acbaa acbaa : e) {
                        httpsURLConnection.setRequestProperty((String) acbaa.a(), (String) acbaa.b());
                    }
                }
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                outputStream = httpsURLConnection.getOutputStream();
                accbb.a((Object) outputStream, "conn.outputStream");
                abacb abacb = abacb.a;
                outputStream.write(abacb.a(abbaa.b()));
                outputStream.flush();
                com.ogury.cm.ConsentActivity.aaaaa.a((Closeable) outputStream);
                int responseCode = httpsURLConnection.getResponseCode();
                aacbc d = abbaa.d();
                if (200 <= responseCode) {
                    if (299 >= responseCode) {
                        if (d != null) {
                            InputStream inputStream = httpsURLConnection.getInputStream();
                            accbb.a((Object) inputStream, "conn.inputStream");
                            d.a(a(inputStream, httpsURLConnection.getContentEncoding()));
                        }
                        httpsURLConnection.disconnect();
                    }
                }
                if (d != null) {
                    com.ogury.cm.aabbb.aaaaa aaaaa2 = aabbb.a;
                    InputStream errorStream = httpsURLConnection.getErrorStream();
                    accbb.a((Object) errorStream, "conn.errorStream");
                    d.a(com.ogury.cm.aabbb.aaaaa.a(a(errorStream, httpsURLConnection.getContentEncoding())));
                }
                try {
                    httpsURLConnection.disconnect();
                } catch (Exception e2) {
                    aaccc aaccc = aaccc.a;
                    aaccc.a((Throwable) e2);
                }
            } else {
                throw new acbab("null cannot be cast to non-null type javax.net.ssl.HttpsURLConnection");
            }
        } catch (Exception e3) {
            aacbc d2 = abbaa.d();
            if (d2 != null) {
                String message = e3.getMessage();
                if (message == null) {
                    message = "";
                }
                d2.a(new OguryError(3, message));
            }
        } catch (Throwable th) {
            com.ogury.cm.ConsentActivity.aaaaa.a((Closeable) outputStream);
            throw th;
        }
    }
}
