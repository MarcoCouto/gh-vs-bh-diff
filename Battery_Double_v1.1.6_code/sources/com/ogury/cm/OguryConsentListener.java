package com.ogury.cm;

import com.ogury.cm.OguryChoiceManager.Answer;
import com.ogury.core.OguryError;

public interface OguryConsentListener {
    void onComplete(Answer answer);

    void onError(OguryError oguryError);
}
