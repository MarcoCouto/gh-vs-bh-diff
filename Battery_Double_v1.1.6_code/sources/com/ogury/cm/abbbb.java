package com.ogury.cm;

import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import org.json.JSONObject;

public final class abbbb extends abbba {
    public static final aaaaa b = new aaaaa(null);
    private String c = ServerResponseWrapper.RESPONSE_FIELD;

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    public final String a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final boolean a(JSONObject jSONObject) {
        boolean a = super.a(jSONObject);
        if (a) {
            aacca aacca = aacca.a;
            ababb ababb = (ababb) ababa.f();
            if (jSONObject == null) {
                accbb.a();
            }
            JSONObject optJSONObject = jSONObject.optJSONObject(ServerResponseWrapper.RESPONSE_FIELD).optJSONObject(this.c);
            ababb.a(optJSONObject.optBoolean("hasPaid"));
            String optString = optJSONObject.optString("purposes", "");
            accbb.a((Object) optString, "consentResponse.optString(PURPOSES, \"\")");
            ababb.d(optString);
            String optString2 = optJSONObject.optString("specialFeatures", "");
            accbb.a((Object) optString2, "consentResponse.optString(SPECIAL_FEATURES, \"\")");
            ababb.e(optString2);
            String optString3 = optJSONObject.optString("acceptedVendors", "");
            accbb.a((Object) optString3, "consentResponse.optString(ACCEPTED_VENDORS, \"\")");
            ababb.b(optString3);
            String optString4 = optJSONObject.optString("refusedVendors", "");
            accbb.a((Object) optString4, "consentResponse.optString(REFUSED_VENDORS, \"\")");
            ababb.c(optString4);
            String optString5 = optJSONObject.optString("iabString", "");
            accbb.a((Object) optString5, "consentResponse.optString(IAB_STRING, \"\")");
            ababb.a(optString5);
        }
        return a;
    }
}
