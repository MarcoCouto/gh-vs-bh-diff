package com.ogury.cm;

import com.ogury.core.OguryError;
import org.json.JSONObject;

public final class aabbb {
    public static final aaaaa a = new aaaaa(null);

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0068, code lost:
            if (r3.equals("assetKey-unknown") != false) goto L_0x0073;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0071, code lost:
            if (r3.equals("no-such-assetKey") != false) goto L_0x0073;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0073, code lost:
            r0 = 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x007b, code lost:
            if (r3.equals("assetType-not-matching") != false) goto L_0x0086;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0084, code lost:
            if (r3.equals("bundle-not-matching") != false) goto L_0x0086;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0086, code lost:
            r0 = 2;
         */
        public static OguryError a(String str) {
            if (str != null) {
                JSONObject a = com.ogury.cm.ConsentActivity.aaaaa.a(str);
                if (a != null && a.has("error")) {
                    aaaaa aaaaa = aabbb.a;
                    if (a.has("sdk")) {
                        try {
                            JSONObject optJSONObject = a.optJSONObject("sdk");
                            com.ogury.cm.abbba.aaaaa aaaaa2 = abbba.a;
                            accbb.a((Object) optJSONObject, "sdkObject");
                            com.ogury.cm.abbba.aaaaa.a(optJSONObject);
                            com.ogury.cm.abbba.aaaaa aaaaa3 = abbba.a;
                            com.ogury.cm.abbba.aaaaa.b(optJSONObject);
                        } catch (Exception e) {
                            aaccc aaccc = aaccc.a;
                            aaccc.a("Error while parsing json config: ", e);
                        }
                    }
                    String optString = a.optString("error");
                    int i = 4;
                    if (optString != null) {
                        switch (optString.hashCode()) {
                            case -1570691213:
                                if (optString.equals("edit-disabled-user-has-paid")) {
                                    i = 1006;
                                    break;
                                }
                                break;
                            case -398774240:
                                boolean equals = optString.equals("domain-not-matching");
                                break;
                            case -303694790:
                                if (optString.equals("edit-disabled-georestricted-user")) {
                                    i = 1008;
                                    break;
                                }
                                break;
                            case -92841694:
                                break;
                            case 85866586:
                                break;
                            case 199821065:
                                break;
                            case 680846796:
                                break;
                            case 1299590356:
                                if (optString.equals("region-restricted")) {
                                    i = 1000;
                                    break;
                                }
                                break;
                            case 1884026692:
                                if (optString.equals("edit-disabled-device-id-restricted")) {
                                    i = 1007;
                                    break;
                                }
                                break;
                        }
                    }
                    accbb.a((Object) optString, "error");
                    return new OguryError(i, com.ogury.cm.aabba.aaaaa.b(optString));
                }
            }
            return new OguryError(1004, com.ogury.cm.aabba.aaaaa.b("parsing-error"));
        }
    }
}
