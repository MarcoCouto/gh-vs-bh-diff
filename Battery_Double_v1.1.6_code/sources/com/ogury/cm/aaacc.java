package com.ogury.cm;

import android.content.Context;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ogury.core.OguryError;

public abstract class aaacc {
    private String a = "";
    private String b = "";
    private acbbb c = new acbbb();
    private aacba d = aacba.a;
    private abaca e = new abaca();

    public static final class aaaaa implements aacac {
        final /* synthetic */ aaacc a;

        aaaaa(aaacc aaacc) {
            this.a = aaacc;
        }

        public final void a(String str) {
            accbb.b(str, "aaid");
            aacca aacca = aacca.a;
            aacca.c().b(str);
            this.a.c();
        }
    }

    public static final class aaaab implements aacbc {
        aaaab() {
        }

        public final void a(OguryError oguryError) {
            accbb.b(oguryError, "error");
            aaccc aaccc = aaccc.a;
            StringBuilder sb = new StringBuilder("Error while setting consent! Error code: ");
            sb.append(oguryError.getErrorCode());
            sb.append(", Error message: ");
            sb.append(oguryError.getMessage());
            aaccc.a(sb.toString());
        }

        public final void a(String str) {
            accbb.b(str, ServerResponseWrapper.RESPONSE_FIELD);
        }
    }

    protected static aacbc d() {
        return new aaaab();
    }

    /* access modifiers changed from: protected */
    public final String a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public final void a(Context context) {
        accbb.b(context, "context");
        if (acbbb.a(context)) {
            aacba.a(context, (aacac) new aaaaa(this));
            return;
        }
        aaccc aaccc = aaccc.a;
        aaccc.a("Error response: No Internet connection");
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        accbb.b(str, "<set-?>");
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        accbb.b(str, "<set-?>");
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public abstract void c();
}
