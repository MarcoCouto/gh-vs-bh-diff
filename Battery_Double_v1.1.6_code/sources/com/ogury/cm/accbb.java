package com.ogury.cm;

import java.util.Arrays;
import java.util.List;

public class accbb {
    private accbb() {
    }

    private static <T extends Throwable> T a(T t) {
        return a(t, accbb.class.getName());
    }

    static <T extends Throwable> T a(T t, String str) {
        StackTraceElement[] stackTrace = t.getStackTrace();
        int length = stackTrace.length;
        int i = -1;
        for (int i2 = 0; i2 < length; i2++) {
            if (str.equals(stackTrace[i2].getClassName())) {
                i = i2;
            }
        }
        List subList = Arrays.asList(stackTrace).subList(i + 1, length);
        t.setStackTrace((StackTraceElement[]) subList.toArray(new StackTraceElement[subList.size()]));
        return t;
    }

    public static void a() {
        throw ((acacb) a((T) new acacb()));
    }

    public static void a(Object obj, String str) {
        if (obj == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" must not be null");
            throw ((IllegalStateException) a((T) new IllegalStateException(sb.toString())));
        }
    }

    public static void a(String str) {
        StringBuilder sb = new StringBuilder("lateinit property ");
        sb.append(str);
        sb.append(" has not been initialized");
        throw ((acbac) a((T) new acbac(sb.toString())));
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    public static void b(Object obj, String str) {
        if (obj == null) {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            String className = stackTraceElement.getClassName();
            String methodName = stackTraceElement.getMethodName();
            StringBuilder sb = new StringBuilder("Parameter specified as non-null is null: method ");
            sb.append(className);
            sb.append(".");
            sb.append(methodName);
            sb.append(", parameter ");
            sb.append(str);
            throw ((IllegalArgumentException) a((T) new IllegalArgumentException(sb.toString())));
        }
    }
}
