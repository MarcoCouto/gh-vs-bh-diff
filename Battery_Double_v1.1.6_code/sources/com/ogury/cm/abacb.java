package com.ogury.cm;

import com.ogury.cm.ConsentActivity.aaaaa;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public final class abacb {
    public static final abacb a = new abacb();

    private abacb() {
    }

    public static String a(byte[] bArr) throws Exception {
        accbb.b(bArr, "str");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new ByteArrayInputStream(bArr)), "UTF-8"));
        try {
            String a2 = aaaaa.a((Reader) bufferedReader);
            return a2;
        } finally {
            aaaaa.a((Closeable) bufferedReader);
        }
    }

    /* JADX INFO: finally extract failed */
    public static byte[] a(String str) throws Exception {
        accbb.b(str, "str");
        if (str.length() == 0) {
            return new byte[0];
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        try {
            Charset forName = Charset.forName("UTF-8");
            accbb.a((Object) forName, "Charset.forName(charsetName)");
            byte[] bytes = str.getBytes(forName);
            accbb.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            gZIPOutputStream.write(bytes);
            aaaaa.a((Closeable) gZIPOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            accbb.a((Object) byteArray, "obj.toByteArray()");
            return byteArray;
        } catch (Throwable th) {
            aaaaa.a((Closeable) gZIPOutputStream);
            throw th;
        }
    }
}
