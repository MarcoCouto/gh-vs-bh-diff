package com.ogury.cm;

public final class baabb extends baaac {
    public static final aaaaa b = new aaaaa(null);
    /* access modifiers changed from: private */
    public static final baabb c = new baabb(1, 0);

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    public baabb(int i, int i2) {
        super(i, i2, 1);
    }

    public final boolean d() {
        return a() > b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (b() == r3.b()) goto L_0x0029;
     */
    public final boolean equals(Object obj) {
        if (obj instanceof baabb) {
            if (!d() || !((baabb) obj).d()) {
                baabb baabb = (baabb) obj;
                if (a() == baabb.a()) {
                }
            }
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (d()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a());
        sb.append("..");
        sb.append(b());
        return sb.toString();
    }
}
