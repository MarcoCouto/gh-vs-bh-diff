package com.ogury.cm;

import android.content.Context;
import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class baacc {
    public static /* synthetic */ String a(String str, String str2, String str3, boolean z, int i, Object obj) {
        accbb.b(str, "receiver$0");
        accbb.b(str2, "oldValue");
        accbb.b(str3, "newValue");
        CharSequence charSequence = str;
        String[] strArr = {str2};
        accbb.b(charSequence, "receiver$0");
        accbb.b(strArr, "delimiters");
        accbb.b(strArr, "receiver$0");
        List asList = Arrays.asList(strArr);
        accbb.a((Object) asList, "ArraysUtilJVM.asList(this)");
        baabc babab = new babab(charSequence, 0, 0, new aaaaa(asList, false));
        accac aaaab = new aaaab(charSequence);
        accbb.b(babab, "receiver$0");
        accbb.b(aaaab, "transform");
        return baaca.a(new baacb(babab, aaaab), str3, null, null, 0, null, null, 62, null);
    }

    public static <T> void a(Appendable appendable, T t, accac<? super T, ? extends CharSequence> accac) {
        accbb.b(appendable, "receiver$0");
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append((CharSequence) t);
        } else if (t instanceof Character) {
            appendable.append(((Character) t).charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }

    public static boolean a(char c, char c2, boolean z) {
        if (c == c2) {
            return true;
        }
        if (!z) {
            return false;
        }
        return Character.toUpperCase(c) == Character.toUpperCase(c2) || Character.toLowerCase(c) == Character.toLowerCase(c2);
    }

    public static boolean a(Context context) {
        accbb.b(context, "context");
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    public static boolean a(CharSequence charSequence) {
        boolean z;
        boolean z2;
        accbb.b(charSequence, "receiver$0");
        if (charSequence.length() != 0) {
            accbb.b(charSequence, "receiver$0");
            Iterator it = new baabb(0, charSequence.length() - 1).iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = true;
                    break;
                }
                char charAt = charSequence.charAt(((acbbc) it).a());
                if (Character.isWhitespace(charAt) || Character.isSpaceChar(charAt)) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (!z2) {
                    z = false;
                    break;
                }
            }
            return z;
        }
    }

    public static boolean a(String str, int i, String str2, int i2, int i3, boolean z) {
        accbb.b(str, "receiver$0");
        accbb.b(str2, FacebookRequestErrorClassification.KEY_OTHER);
        return !z ? str.regionMatches(0, str2, i2, i3) : str.regionMatches(z, 0, str2, i2, i3);
    }

    public static /* synthetic */ boolean a(String str, String str2, boolean z, int i, Object obj) {
        accbb.b(str, "receiver$0");
        accbb.b(str2, "prefix");
        return str.startsWith(str2);
    }
}
