package com.ogury.cm;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Base64;

public final class aabba {
    public static final aaaaa a = new aaaaa(null);

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }

        public static String a(String str) {
            accbb.b(str, "receiver$0");
            byte[] bytes = str.getBytes(babaa.a);
            accbb.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            String encodeToString = Base64.encodeToString(bytes, 11);
            accbb.a((Object) encodeToString, "Base64.encodeToString(by…ADDING + Base64.URL_SAFE)");
            return encodeToString;
        }

        public static void a(Context context, int i) {
            String str;
            ababa ababa;
            accbb.b(context, "context");
            switch (i) {
                case 1:
                    str = "cacheConsentV2";
                    ababa = new ababb();
                    break;
                case 2:
                    str = "cacheConsent";
                    ababa = new ababc();
                    break;
                default:
                    throw new IllegalStateException("Bad TCF version used! Please use one of the declared ones in OguryChoiceManager class.");
            }
            ababa ababa2 = ababa;
            if (context.getSharedPreferences(str, 0).contains("assetKey")) {
                Context applicationContext = context.getApplicationContext();
                accbb.a((Object) applicationContext, "context.applicationContext");
                PreferenceManager.getDefaultSharedPreferences(applicationContext).edit().clear().apply();
                applicationContext.getSharedPreferences(str, 0).edit().clear().apply();
                aacca aacca = aacca.a;
                aacca.b(ababa2);
                OguryChoiceManager.INSTANCE.resetClientConsentImpl$consent_manager_prodRelease();
            }
        }

        public static String b(String str) {
            accbb.b(str, "receiver$0");
            return babac.a(str, "-", " ", false, 4, (Object) null);
        }
    }
}
