package com.ogury.cm;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Looper;
import java.io.IOException;

public final class aacbb {
    public static final aacbb a = new aacbb();

    private aacbb() {
    }

    public static acabb a(Context context) throws Exception {
        accbb.b(context, "context");
        Thread currentThread = Thread.currentThread();
        Looper mainLooper = Looper.getMainLooper();
        accbb.a((Object) mainLooper, "Looper.getMainLooper()");
        if (!accbb.a((Object) currentThread, (Object) mainLooper.getThread())) {
            try {
                context.getPackageManager().getPackageInfo("com.android.vending", 0);
                return b(context);
            } catch (Exception e) {
                throw e;
            }
        } else {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
    }

    private static acabb b(Context context) throws Exception {
        acabc acabc = new acabc();
        Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
        intent.setPackage("com.google.android.gms");
        ServiceConnection serviceConnection = acabc;
        if (context.bindService(intent, serviceConnection, 1)) {
            try {
                acaca acaca = new acaca(acabc.a());
                acabb acabb = new acabb(acaca.a(), acaca.a(true));
                context.unbindService(serviceConnection);
                return acabb;
            } catch (Exception e) {
                throw e;
            } catch (Throwable th) {
                context.unbindService(serviceConnection);
                throw th;
            }
        } else {
            throw new IOException("Google Play connection failed");
        }
    }
}
