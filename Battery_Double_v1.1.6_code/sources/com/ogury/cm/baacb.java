package com.ogury.cm;

import java.util.Iterator;

public final class baacb<T, R> implements baabc<R> {
    /* access modifiers changed from: private */
    public final baabc<T> a;
    /* access modifiers changed from: private */
    public final accac<T, R> b;

    public static final class aaaaa implements baaaa, Iterator<R> {
        final /* synthetic */ baacb a;
        private final Iterator<T> b;

        aaaaa(baacb baacb) {
            this.a = baacb;
            this.b = baacb.a.a();
        }

        public final boolean hasNext() {
            return this.b.hasNext();
        }

        public final R next() {
            return this.a.b.a(this.b.next());
        }

        public final void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    public baacb(baabc<? extends T> baabc, accac<? super T, ? extends R> accac) {
        accbb.b(baabc, "sequence");
        accbb.b(accac, "transformer");
        this.a = baabc;
        this.b = accac;
    }

    public final Iterator<R> a() {
        return new aaaaa<>(this);
    }
}
