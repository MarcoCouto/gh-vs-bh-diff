package com.ogury.cm;

import android.content.Context;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.share.internal.ShareConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.core.OguryError;

public final class aaaaa extends WebViewClient {
    private aacab a = new aacab();
    private boolean b;
    private Context c;
    private final aacbc d;

    public aaaaa(Context context, aacbc aacbc) {
        accbb.b(context, "context");
        accbb.b(aacbc, "consentCallback");
        this.c = context;
        this.d = aacbc;
    }

    private final void a(String str) {
        if (!this.b) {
            this.b = true;
            aacbc aacbc = this.d;
            if (str == null) {
                str = "";
            }
            aacbc.a(new OguryError(1003, str));
            if (this.c instanceof ConsentActivity) {
                Context context = this.c;
                if (context != null) {
                    ((ConsentActivity) context).finish();
                    return;
                }
                throw new acbab("null cannot be cast to non-null type com.ogury.cm.ConsentActivity");
            }
        }
    }

    private final boolean a(String str, WebView webView) {
        if (!this.b) {
            this.a.a(str, this.c, this.d, webView);
        }
        return true;
    }

    public final void a(Context context) {
        accbb.b(context, "context");
        this.c = context;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        a(str);
    }

    public final void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        accbb.b(webView, ParametersKeys.VIEW);
        accbb.b(webResourceRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        accbb.b(webResourceError, "error");
        a(webResourceError.getDescription().toString());
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        accbb.b(webView, ParametersKeys.VIEW);
        accbb.b(webResourceRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        return null;
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        accbb.b(webView, ParametersKeys.VIEW);
        accbb.b(str, "url");
        return null;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        accbb.b(webView, ParametersKeys.VIEW);
        accbb.b(webResourceRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        String uri = webResourceRequest.getUrl().toString();
        accbb.a((Object) uri, "request.url.toString()");
        return a(uri, webView);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        accbb.b(webView, ParametersKeys.VIEW);
        accbb.b(str, "url");
        return a(str, webView);
    }
}
