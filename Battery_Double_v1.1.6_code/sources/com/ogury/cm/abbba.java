package com.ogury.cm;

import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.cm.OguryChoiceManager.Answer;
import com.ogury.core.OguryError;
import io.fabric.sdk.android.services.settings.AppSettingsData;
import java.net.URLDecoder;
import java.util.Date;
import org.json.JSONObject;

public abstract class abbba {
    public static final aaaaa a = new aaaaa(null);

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }

        public static abbba a() {
            abbba abbbb;
            aacca aacca = aacca.a;
            switch (aacca.a()) {
                case 1:
                    abbbb = new abbbb();
                    break;
                case 2:
                    abbbb = new abbbc();
                    break;
                default:
                    abbbb = new abbbb();
                    break;
            }
            return abbbb;
        }

        public static void a(JSONObject jSONObject) {
            accbb.b(jSONObject, "jsonObject");
            if (jSONObject.has("cacheFor")) {
                aacca aacca = aacca.a;
                aacca.a(new Date().getTime() + (jSONObject.optLong("cacheFor") * 1000));
            }
        }

        public static void b(JSONObject jSONObject) {
            accbb.b(jSONObject, "jsonObject");
            if (jSONObject.has("crashReportUrl")) {
                aacca aacca = aacca.a;
                accca c = aacca.c();
                String optString = jSONObject.optString("crashReportUrl");
                accbb.a((Object) optString, "jsonObject.optString(CRASH_REPORT_URL)");
                c.c(optString);
            }
        }
    }

    public abstract String a();

    public final void a(String str, boolean z) {
        boolean z2;
        abbca abbca;
        accbb.b(str, ServerResponseWrapper.RESPONSE_FIELD);
        try {
            aacca aacca = aacca.a;
            aacca.i();
            JSONObject a2 = com.ogury.cm.ConsentActivity.aaaaa.a(str);
            if (a2 != null) {
                boolean z3 = true;
                if (a2.has("error")) {
                    aacca aacca2 = aacca.a;
                    String string = a2.getString("error");
                    accbb.a((Object) string, "jsonObject.getString(\"error\")");
                    aacca.a(new abbca(false, new OguryError(1004, com.ogury.cm.aabba.aaaaa.b(string))));
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (!z2) {
                    if (z) {
                        if (!a2.has("config")) {
                            aacca aacca3 = aacca.a;
                            abbca = new abbca(false, new OguryError(1004, "No config field"));
                        } else if (!a2.has("form")) {
                            aacca aacca4 = aacca.a;
                            abbca = new abbca(false, new OguryError(1004, "No form filed"));
                        } else if (!a2.optJSONObject("form").has("secureToken")) {
                            aacca aacca5 = aacca.a;
                            abbca = new abbca(false, new OguryError(1004, "No secureToken filed"));
                        }
                        aacca.a(abbca);
                        z3 = false;
                    }
                    if (z3) {
                        JSONObject optJSONObject = a2.optJSONObject("form");
                        JSONObject optJSONObject2 = a2.optJSONObject("config");
                        if (optJSONObject2 != null && optJSONObject2.has("fairChoice")) {
                            JSONObject optJSONObject3 = optJSONObject2.optJSONObject("fairChoice");
                            if (optJSONObject3 != null && optJSONObject3.optBoolean(AppSettingsData.STATUS_ACTIVATED)) {
                                String optString = optJSONObject3.optString("productId");
                                String optString2 = optJSONObject3.optString(ParametersKeys.PRODUCT_TYPE);
                                abcbc abcbc = abcbc.a;
                                accbb.a((Object) optString, "productID");
                                accbb.a((Object) optString2, "skuType");
                                abcbc.activateProduct(new acaaa(optString, optString2));
                            }
                        }
                        if (optJSONObject != null) {
                            aacca aacca6 = aacca.a;
                            String optString3 = optJSONObject.optString("secureToken");
                            accbb.a((Object) optString3, "formObject.optString(\"secureToken\")");
                            aacca.b(optString3);
                            if (a2.optJSONObject("form").has("showFormat")) {
                                JSONObject optJSONObject4 = a2.optJSONObject("form");
                                String optString4 = optJSONObject4 != null ? optJSONObject4.optString("showFormat") : null;
                                if (optString4 != null) {
                                    if (!accbb.a((Object) optString4, (Object) "null")) {
                                        aacca aacca7 = aacca.a;
                                        aacca.c(optString4);
                                    }
                                }
                                aacca aacca8 = aacca.a;
                                optString4 = "";
                                aacca.c(optString4);
                            }
                        }
                        aacca aacca9 = aacca.a;
                        String jSONObject = a2.toString();
                        accbb.a((Object) jSONObject, "jsonResponse.toString()");
                        aacca.a(jSONObject);
                    }
                }
            } else if (z) {
                aacca aacca10 = aacca.a;
                aacca.a(new abbca(false, new OguryError(1004, "Json response is null")));
                return;
            }
            aacca aacca11 = aacca.a;
            if (aacca.j()) {
                if (babac.a(str, "consent=", false, 2, null)) {
                    String decode = URLDecoder.decode(str, "UTF-8");
                    if (decode == null) {
                        aacca aacca12 = aacca.a;
                        aacca.a(new abbca(false, new OguryError(1004, "Decoded Json is null")));
                        return;
                    }
                    a(com.ogury.cm.ConsentActivity.aaaaa.a(babac.a(decode, "consent=", (String) null, 2, (Object) null)));
                    return;
                }
                a(com.ogury.cm.ConsentActivity.aaaaa.a(str));
            }
        } catch (OguryError e) {
            aacca aacca13 = aacca.a;
            aacca.a(new abbca(false, e));
            aaccc aaccc = aaccc.a;
            aaccc.a("Error while parsing json config: ", e);
        } catch (Exception e2) {
            aacca aacca14 = aacca.a;
            aacca.a(new abbca(false, new OguryError(1004, String.valueOf(e2.getMessage()))));
            aaccc aaccc2 = aaccc.a;
            aaccc.a("Error while parsing json config: ", e2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(JSONObject jSONObject) {
        boolean z = false;
        if (jSONObject != null && jSONObject.has("sdk") && jSONObject.has(ServerResponseWrapper.RESPONSE_FIELD) && jSONObject.optJSONObject(ServerResponseWrapper.RESPONSE_FIELD).has(a())) {
            if (jSONObject == null) {
                accbb.a();
            }
            JSONObject optJSONObject = jSONObject.optJSONObject(ServerResponseWrapper.RESPONSE_FIELD);
            JSONObject optJSONObject2 = jSONObject.optJSONObject("sdk");
            accbb.a((Object) optJSONObject2, "jsonResponse.optJSONObject(SDK)");
            String optString = optJSONObject2.optString("formPath");
            CharSequence charSequence = optString;
            if (charSequence == null || charSequence.length() == 0) {
                z = true;
            }
            if (!z) {
                aacca aacca = aacca.a;
                aacca.c().d(optString);
            }
            aacca aacca2 = aacca.a;
            aacca.b().b(optJSONObject.optBoolean("gdprApplies", true));
            if (optJSONObject.has("lastOpt")) {
                aacca aacca3 = aacca.a;
                ababa b = aacca.b();
                String optString2 = optJSONObject.optString("lastOpt");
                accbb.a((Object) optString2, "responseObject.optString(LAST_OPT)");
                accbb.b(optString2, "receiver$0");
                Answer answer = accbb.a((Object) optString2, (Object) Answer.FULL_APPROVAL.toString()) ? Answer.FULL_APPROVAL : accbb.a((Object) optString2, (Object) Answer.PARTIAL_APPROVAL.toString()) ? Answer.PARTIAL_APPROVAL : accbb.a((Object) optString2, (Object) Answer.REFUSAL.toString()) ? Answer.REFUSAL : Answer.NO_ANSWER;
                b.a(answer);
            }
            if (optJSONObject2.has("editAvailable")) {
                aacca aacca4 = aacca.a;
                aacca.c().a(optJSONObject2.optBoolean("editAvailable"));
            }
            aaaaa.b(optJSONObject2);
            aaaaa.a(optJSONObject2);
            return true;
        }
        aacca aacca5 = aacca.a;
        aacca.a(new abbca(false, new OguryError(1004, "Json not valid")));
        return false;
    }
}
