package com.ogury.cm;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.Purchase.PurchasesResult;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsParams.Builder;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.applovin.sdk.AppLovinEventTypes;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs.CastExtraArgs;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class abcca implements BillingClientStateListener, PurchasesUpdatedListener, SkuDetailsResponseListener, abcba {
    private acaaa a;
    private acaba b;
    /* access modifiers changed from: private */
    public acaac c;
    private acaab d;
    private abcac e;
    private int f;
    /* access modifiers changed from: private */
    public volatile boolean g;
    /* access modifiers changed from: private */
    public abccb h;
    /* access modifiers changed from: private */
    public BillingClient i;

    static final class aaaaa {
        public static final aaaaa a = new aaaaa();
        private static AtomicInteger b = new AtomicInteger(1);

        /* renamed from: com.ogury.cm.abcca$aaaaa$aaaaa reason: collision with other inner class name */
        static final class C0059aaaaa implements Runnable {
            final /* synthetic */ accab a;

            C0059aaaaa(accab accab) {
                this.a = accab;
            }

            public final void run() {
                this.a.a();
            }
        }

        static final class aaaab implements Runnable {
            final /* synthetic */ accab a;

            aaaab(accab accab) {
                this.a = accab;
            }

            public final void run() {
                this.a.a();
            }
        }

        private aaaaa() {
        }

        public static void a() {
            b.set(1);
        }

        public static void a(BillingClient billingClient, abcca abcca, accab<acbba> accab) {
            accbb.b(billingClient, "billingClient");
            accbb.b(abcca, CastExtraArgs.LISTENER);
            accbb.b(accab, "task");
            if (billingClient.isReady()) {
                accab.a();
                return;
            }
            Log.d("FairChoice", "taskExecutionRetryPolicy billing not ready");
            billingClient.startConnection((BillingClientStateListener) abcca);
            new Handler(Looper.getMainLooper()).postDelayed(new aaaab(accab), AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
        }

        public static void a(accab<acbba> accab) {
            accbb.b(accab, "block");
            int andIncrement = b.getAndIncrement();
            if (andIncrement < 5) {
                new Handler(Looper.getMainLooper()).postDelayed(new C0059aaaaa(accab), (long) (((float) Math.pow(2.0d, (double) andIncrement)) * 500.0f));
            }
        }
    }

    static final class aaaab extends accbc implements accab<acbba> {
        final /* synthetic */ abcca a;
        final /* synthetic */ Activity b;
        final /* synthetic */ BillingFlowParams c;

        aaaab(abcca abcca, Activity activity, BillingFlowParams billingFlowParams) {
            this.a = abcca;
            this.b = activity;
            this.c = billingFlowParams;
            super(0);
        }

        public final /* synthetic */ Object a() {
            abcca.access$getPlayStoreBillingClient$p(this.a).launchBillingFlow(this.b, this.c);
            return acbba.a;
        }
    }

    static final class aaaac extends accbc implements accab<acbba> {
        final /* synthetic */ abcca a;

        aaaac(abcca abcca) {
            this.a = abcca;
            super(0);
        }

        public final /* synthetic */ Object a() {
            this.a.b();
            return acbba.a;
        }
    }

    static final class aaaba extends accbc implements accab<acbba> {
        final /* synthetic */ abcca a;

        aaaba(abcca abcca) {
            this.a = abcca;
            super(0);
        }

        public final /* synthetic */ Object a() {
            b();
            return acbba.a;
        }

        public final void b() {
            Log.d("FairChoice", "queryPurchases called");
            this.a.g = false;
            HashSet hashSet = new HashSet();
            PurchasesResult queryPurchases = abcca.access$getPlayStoreBillingClient$p(this.a).queryPurchases("inapp");
            String str = "FairChoice";
            StringBuilder sb = new StringBuilder("queryPurchases INAPP results: ");
            List list = null;
            sb.append(queryPurchases != null ? queryPurchases.getPurchasesList() : null);
            Log.d(str, sb.toString());
            if (queryPurchases != null) {
                List purchasesList = queryPurchases.getPurchasesList();
                if (purchasesList != null) {
                    hashSet.addAll(purchasesList);
                }
            }
            if (abcca.access$isSubscriptionSupported(this.a)) {
                PurchasesResult queryPurchases2 = abcca.access$getPlayStoreBillingClient$p(this.a).queryPurchases("subs");
                if (queryPurchases2 != null) {
                    List purchasesList2 = queryPurchases2.getPurchasesList();
                    if (purchasesList2 != null) {
                        hashSet.addAll(purchasesList2);
                    }
                }
                String str2 = "FairChoice";
                StringBuilder sb2 = new StringBuilder("queryPurchases SUBS results: ");
                if (queryPurchases2 != null) {
                    list = queryPurchases2.getPurchasesList();
                }
                sb2.append(list);
                Log.d(str2, sb2.toString());
            }
            abcca.access$getSharedPreferences$p(this.a);
            abccb.a(hashSet);
            acaac access$getPurchaseQueryListener$p = this.a.c;
            if (access$getPurchaseQueryListener$p != null) {
                access$getPurchaseQueryListener$p.a();
            }
        }
    }

    static final class aaabb extends accbc implements accab<acbba> {
        final /* synthetic */ aaaba a;

        aaabb(aaaba aaaba) {
            this.a = aaaba;
            super(0);
        }

        public final /* synthetic */ Object a() {
            this.a.b();
            return acbba.a;
        }
    }

    static final class aaabc extends accbc implements accab<acbba> {
        final /* synthetic */ abcca a;
        final /* synthetic */ String b;
        final /* synthetic */ Builder c;

        aaabc(abcca abcca, String str, Builder builder) {
            this.a = abcca;
            this.b = str;
            this.c = builder;
            super(0);
        }

        public final /* synthetic */ Object a() {
            StringBuilder sb = new StringBuilder("querySkuDetailsAsync for ");
            sb.append(this.b);
            Log.d("FairChoice", sb.toString());
            abcca.access$getPlayStoreBillingClient$p(this.a).querySkuDetailsAsync(this.c.build(), this.a);
            return acbba.a;
        }
    }

    private static String a(SkuDetails skuDetails) {
        JSONObject jSONObject = new JSONObject();
        if (skuDetails != null) {
            try {
                jSONObject = new JSONObject(skuDetails.getOriginalJson());
            } catch (Exception e2) {
                Log.w("FairChoice", "Error while parsing skuDetailsJson: ".concat(String.valueOf(e2)));
            }
            jSONObject.remove("skuDetailsToken");
            jSONObject.remove("rewardToken");
        }
        String jSONObject2 = jSONObject.toString();
        accbb.a((Object) jSONObject2, "billingJson.toString()");
        return jSONObject2;
    }

    private final void a() {
        aaaba aaaba2 = new aaaba(this);
        aaaaa aaaaa2 = aaaaa.a;
        BillingClient billingClient = this.i;
        if (billingClient == null) {
            accbb.a("playStoreBillingClient");
        }
        aaaaa.a(billingClient, this, new aaabb(aaaba2));
    }

    public static final /* synthetic */ BillingClient access$getPlayStoreBillingClient$p(abcca abcca) {
        BillingClient billingClient = abcca.i;
        if (billingClient == null) {
            accbb.a("playStoreBillingClient");
        }
        return billingClient;
    }

    public static final /* synthetic */ abccb access$getSharedPreferences$p(abcca abcca) {
        abccb abccb = abcca.h;
        if (abccb == null) {
            accbb.a("sharedPreferences");
        }
        return abccb;
    }

    public static final /* synthetic */ boolean access$isSubscriptionSupported(abcca abcca) {
        BillingClient billingClient = abcca.i;
        if (billingClient == null) {
            accbb.a("playStoreBillingClient");
        }
        BillingResult isFeatureSupported = billingClient.isFeatureSupported("subscriptions");
        accbb.a((Object) isFeatureSupported, "playStoreBillingClient.i…eatureType.SUBSCRIPTIONS)");
        int responseCode = isFeatureSupported.getResponseCode();
        if (responseCode != 0) {
            Log.w("FairChoice", "isSubscriptionSupported() got an error response: ".concat(String.valueOf(responseCode)));
        }
        return responseCode == 0;
    }

    /* access modifiers changed from: private */
    public final boolean b() {
        Log.d("FairChoice", "connectToPlayBillingService");
        BillingClient billingClient = this.i;
        if (billingClient == null) {
            accbb.a("playStoreBillingClient");
        }
        if (billingClient.isReady()) {
            return false;
        }
        BillingClient billingClient2 = this.i;
        if (billingClient2 == null) {
            accbb.a("playStoreBillingClient");
        }
        billingClient2.startConnection((BillingClientStateListener) this);
        return true;
    }

    public final void activateProduct(acaaa acaaa) {
        accbb.b(acaaa, AppLovinEventTypes.USER_VIEWED_PRODUCT);
        this.a = acaaa;
        if (this.h == null) {
            accbb.a("sharedPreferences");
        }
        abccb.a(acaaa);
    }

    public final void endDataSourceConnections() {
        Log.d("FairChoice", "endDataSourceConnections");
        BillingClient billingClient = this.i;
        if (billingClient == null) {
            accbb.a("playStoreBillingClient");
        }
        billingClient.endConnection();
    }

    public final boolean isBillingDisabled() {
        return false;
    }

    public final boolean isProductActivated() {
        return this.a != null;
    }

    public final void launchBillingFlow(Activity activity) {
        accbb.b(activity, "activity");
        BillingFlowParams.Builder newBuilder = BillingFlowParams.newBuilder();
        acaba acaba = this.b;
        BillingFlowParams build = newBuilder.setSkuDetails(acaba != null ? acaba.b() : null).build();
        aaaaa aaaaa2 = aaaaa.a;
        BillingClient billingClient = this.i;
        if (billingClient == null) {
            accbb.a("playStoreBillingClient");
        }
        aaaaa.a(billingClient, this, new aaaab(this, activity, build));
    }

    public final void onBillingServiceDisconnected() {
        Log.d("FairChoice", "onBillingServiceDisconnected");
        aaaaa aaaaa2 = aaaaa.a;
        aaaaa.a(new aaaac(this));
    }

    public final void onBillingSetupFinished(BillingResult billingResult) {
        accbb.b(billingResult, "billingResult");
        int responseCode = billingResult.getResponseCode();
        if (responseCode == 0) {
            Log.d("FairChoice", "onBillingSetupFinished successfully");
            aaaaa aaaaa2 = aaaaa.a;
            aaaaa.a();
            if (this.g) {
                a();
            }
        } else {
            if (responseCode == 3) {
                abcbc abcbc = abcbc.a;
                abcbc.a();
            }
            abcbb abcbb = abcbb.a;
            Log.d("FairChoice", "onBillingSetupFinished with failure response code: ".concat(String.valueOf(abcbb.a(responseCode))));
            this.f = responseCode;
            acaac acaac = this.c;
            if (acaac != null) {
                acaac.a();
            }
        }
    }

    public final void onPurchasesUpdated(BillingResult billingResult, List<Purchase> list) {
        accbb.b(billingResult, "billingResult");
        int responseCode = billingResult.getResponseCode();
        Object obj = null;
        if (responseCode != 0) {
            if (responseCode == 5) {
                Log.e("FairChoice", "Your app's configuration is incorrect. Review in the Google PlayConsole. Possible causes of this error include: APK is not signed with release key; SKU productId mismatch.");
            } else if (responseCode != 7) {
                StringBuilder sb = new StringBuilder("BillingClient.BillingResponse error code:");
                abcbb abcbb = abcbb.a;
                sb.append(abcbb.a(responseCode));
                Log.i("FairChoice", sb.toString());
            } else {
                Log.d("FairChoice", "already owned items");
            }
        } else if (list != null) {
            Object obj2 = null;
            for (Purchase purchase : list) {
                if (purchase.getPurchaseState() == 1) {
                    String sku = purchase.getSku();
                    acaaa acaaa = this.a;
                    if (accbb.a((Object) sku, acaaa != null ? acaaa.b() : null)) {
                        obj2 = purchase.getPurchaseToken();
                    }
                } else if (purchase.getPurchaseState() == 2) {
                    StringBuilder sb2 = new StringBuilder("Received a pending purchase of SKU: ");
                    sb2.append(purchase.getSku());
                    Log.d("FairChoice", sb2.toString());
                }
            }
            obj = obj2;
        }
        abcac abcac = this.e;
        if (abcac != null) {
            JSONObject jSONObject = new JSONObject();
            if (responseCode == 0) {
                jSONObject.put("paid", true);
                jSONObject.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, obj);
            } else {
                jSONObject.put("paid", false);
                abcbb abcbb2 = abcbb.a;
                jSONObject.put("errorMessage", abcbb.a(responseCode));
            }
            String jSONObject2 = jSONObject.toString();
            accbb.a((Object) jSONObject2, "json.toString()");
            abcac.a(jSONObject2);
        }
    }

    public final void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
        accbb.b(billingResult, "billingResult");
        int responseCode = billingResult.getResponseCode();
        if (responseCode != 0) {
            if (this.f != 0) {
                abcbb abcbb = abcbb.a;
                responseCode = this.f;
            } else {
                abcbb abcbb2 = abcbb.a;
            }
            String a2 = abcbb.a(responseCode);
            this.f = 0;
            Log.w("FairChoice", "SkuDetails query failed with response: ".concat(String.valueOf(a2)));
            acaab acaab = this.d;
            if (acaab != null) {
                acaab.a(a2, "");
            }
            return;
        }
        Log.d("FairChoice", "SkuDetails query responded with success. List: ".concat(String.valueOf(list)));
        if (list == null || list.isEmpty()) {
            this.b = new acaba("store-product-not-available", null);
        }
        if (list != null) {
            for (SkuDetails skuDetails : list) {
                String sku = skuDetails.getSku();
                acaaa acaaa = this.a;
                if (accbb.a((Object) sku, acaaa != null ? acaaa.b() : null)) {
                    this.b = new acaba("ok", skuDetails);
                }
            }
        }
        acaba acaba = this.b;
        if (acaba != null) {
            acaab acaab2 = this.d;
            if (acaab2 != null) {
                acaab2.a(acaba.a(), a(acaba.b()));
            }
        }
    }

    public final void queryProductDetails() {
        if (this.a == null) {
            Log.i("FairChoice", "Product must be activated(saved) prior to calling queryProductDetails() -> FairChoice disabled");
            return;
        }
        acaaa acaaa = this.a;
        if (acaaa != null) {
            String a2 = acaaa.a();
            List singletonList = Collections.singletonList(acaaa.b());
            accbb.a((Object) singletonList, "java.util.Collections.singletonList(element)");
            Builder newBuilder = SkuDetailsParams.newBuilder();
            newBuilder.setSkusList(singletonList).setType(a2);
            aaaaa aaaaa2 = aaaaa.a;
            BillingClient billingClient = this.i;
            if (billingClient == null) {
                accbb.a("playStoreBillingClient");
            }
            aaaaa.a(billingClient, this, new aaabc(this, a2, newBuilder));
        }
    }

    public final void queryPurchase(acaac acaac) {
        accbb.b(acaac, CastExtraArgs.LISTENER);
        this.c = acaac;
        BillingClient billingClient = this.i;
        if (billingClient == null) {
            accbb.a("playStoreBillingClient");
        }
        if (billingClient.isReady()) {
            a();
        } else {
            this.g = true;
        }
    }

    public final void setBillingFinishedListener(abcac abcac) {
        this.e = abcac;
    }

    public final void setPurchaseQueryListener(acaac acaac) {
        accbb.b(acaac, CastExtraArgs.LISTENER);
        this.g = true;
        this.c = acaac;
    }

    public final void setQueryProductDetailsListener(acaab acaab) {
        this.d = acaab;
        acaba acaba = this.b;
        if (acaba != null) {
            acaab acaab2 = this.d;
            if (acaab2 != null) {
                acaab2.a(acaba.a(), a(acaba.b()));
            }
        }
    }

    public final void startDataSourceConnections(Context context) {
        accbb.b(context, "context");
        abcab abcab = abcab.a;
        this.h = abcab.a(context);
        this.i = abcab.a.a(context, (PurchasesUpdatedListener) this);
        b();
    }

    public final boolean tokenExistsForActiveProduct() {
        if (this.h == null) {
            accbb.a("sharedPreferences");
        }
        String b2 = abccb.b();
        if (this.h == null) {
            accbb.a("sharedPreferences");
        }
        String a2 = abccb.a();
        if (a2 != null) {
            try {
                JSONArray jSONArray = new JSONArray(a2);
                int length = jSONArray.length();
                for (int i2 = 0; i2 < length; i2++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i2);
                    if (accbb.a((Object) jSONObject.getString("productId"), (Object) b2) && jSONObject.has("purchaseToken")) {
                        return true;
                    }
                }
                acbba acbba = acbba.a;
            } catch (JSONException e2) {
                StringBuilder sb = new StringBuilder("Error while parsing purchases json: ");
                sb.append(e2);
                sb.append(')');
                Integer.valueOf(Log.e("FairChoice", sb.toString()));
            }
        }
        return false;
    }
}
