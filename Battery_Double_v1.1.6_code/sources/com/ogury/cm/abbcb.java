package com.ogury.cm;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ogury.cm.OguryChoiceManager.Answer;
import java.util.Date;

public abstract class abbcb {
    public static final aaaaa a = new aaaaa(null);
    private final String b = "tcfVersion";

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }

        public static abbcb a() {
            abbcb abbcc;
            aacca aacca = aacca.a;
            switch (aacca.a()) {
                case 1:
                    abbcc = new abbcc();
                    break;
                case 2:
                    abbcc = new abcaa();
                    break;
                default:
                    abbcc = new abbcc();
                    break;
            }
            return abbcc;
        }
    }

    public static void b(Context context) {
        accbb.b(context, "context");
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove("wrongAssetKey").apply();
    }

    public static void b(Context context, String str) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("wrongAssetKey", str).commit();
    }

    public static String c(Context context) {
        accbb.b(context, "context");
        String string = PreferenceManager.getDefaultSharedPreferences(context).getString("wrongAssetKey", "");
        accbb.a((Object) string, "PreferenceManager.getDef…ring(WRONG_ASSET_KEY, \"\")");
        return string;
    }

    public abstract String a();

    public final void a(Context context, String str) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        boolean z = false;
        Editor edit = context.getSharedPreferences(a(), 0).edit();
        aacca aacca = aacca.a;
        edit.putString("optin", aacca.b().a().toString());
        aacca aacca2 = aacca.a;
        edit.putLong("cacheFor", aacca.d().getTime());
        aacca aacca3 = aacca.a;
        edit.putString("aaid", aacca.c().b());
        aacca aacca4 = aacca.a;
        edit.putBoolean("isEditAvailable", aacca.c().d());
        if (str.length() > 0) {
            z = true;
        }
        if (z) {
            edit.putString("assetKey", str);
        }
        String str2 = RequestParameters.PACKAGE_NAME;
        aacca aacca5 = aacca.a;
        edit.putString(str2, aacca.c().a());
        edit.apply();
        d(context);
    }

    public final boolean a(Context context) {
        accbb.b(context, "context");
        return context.getSharedPreferences(a(), 0).getBoolean("hasPaid", false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x005c  */
    public boolean a(String str, Context context) {
        boolean z;
        accbb.b(str, "assetKey");
        accbb.b(context, "context");
        SharedPreferences sharedPreferences = context.getSharedPreferences(a(), 0);
        if (!(!accbb.a((Object) sharedPreferences.getString("assetKey", ""), (Object) str))) {
            String string = sharedPreferences.getString(RequestParameters.PACKAGE_NAME, "");
            aacca aacca = aacca.a;
            if (!(!accbb.a((Object) string, (Object) aacca.c().a()))) {
                String string2 = sharedPreferences.getString("aaid", "");
                aacca aacca2 = aacca.a;
                if (!(!accbb.a((Object) string2, (Object) aacca.c().b()))) {
                    z = false;
                    if (!z) {
                        context.getSharedPreferences(a(), 0).edit().clear().apply();
                        aacca aacca3 = aacca.a;
                        aacca.a(new Date());
                        return false;
                    }
                    SharedPreferences sharedPreferences2 = context.getSharedPreferences(a(), 0);
                    aacca aacca4 = aacca.a;
                    ababa b2 = aacca.b();
                    String string3 = sharedPreferences2.getString("iabString", "");
                    accbb.a((Object) string3, "prefs.getString(IAB_STRING_KEY, \"\")");
                    b2.a(string3);
                    aacca aacca5 = aacca.a;
                    ababa b3 = aacca.b();
                    String string4 = sharedPreferences2.getString("optin", "NO_ANSWER");
                    accbb.a((Object) string4, "prefs.getString(OPTIN_KEY, \"NO_ANSWER\")");
                    b3.a(Answer.valueOf(string4));
                    aacca aacca6 = aacca.a;
                    aacca.a(sharedPreferences2.getLong("cacheFor", 0));
                    aacca aacca7 = aacca.a;
                    accca c = aacca.c();
                    String string5 = sharedPreferences2.getString("formPath", "/");
                    accbb.a((Object) string5, "prefs.getString(FORM_PATH_KEY, \"/\")");
                    c.d(string5);
                    return true;
                }
            }
        }
        z = true;
        if (!z) {
        }
    }

    /* access modifiers changed from: protected */
    public void d(Context context) {
        accbb.b(context, "context");
        Editor edit = context.getSharedPreferences(a(), 0).edit();
        aacca aacca = aacca.a;
        edit.putString("iabString", aacca.b().b());
        aacca aacca2 = aacca.a;
        edit.putBoolean("hasPaid", aacca.b().c());
        aacca aacca3 = aacca.a;
        edit.putString("formPath", aacca.c().e());
        edit.apply();
    }
}
