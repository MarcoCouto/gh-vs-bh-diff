package com.ogury.cm;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.android.billingclient.api.Purchase;
import com.applovin.sdk.AppLovinEventTypes;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;

public final class abccb {
    public static final abccb a = new abccb();
    private static SharedPreferences b;

    private abccb() {
    }

    public static String a() {
        SharedPreferences sharedPreferences = b;
        if (sharedPreferences == null) {
            accbb.a("sharedPref");
        }
        return sharedPreferences.getString("activePurchases", null);
    }

    public static void a(SharedPreferences sharedPreferences) {
        accbb.b(sharedPreferences, "sharedPreferences");
        b = sharedPreferences;
    }

    public static void a(acaaa acaaa) {
        accbb.b(acaaa, AppLovinEventTypes.USER_VIEWED_PRODUCT);
        SharedPreferences sharedPreferences = b;
        if (sharedPreferences == null) {
            accbb.a("sharedPref");
        }
        Editor edit = sharedPreferences.edit();
        accbb.a((Object) edit, "sharedPref.edit()");
        edit.putString("activeProduct", acaaa.b());
        edit.apply();
    }

    public static void a(HashSet<Purchase> hashSet) {
        JSONArray jSONArray = new JSONArray();
        for (Purchase purchase : hashSet) {
            if (!(purchase.getSku() == null || purchase.getPurchaseToken() == null)) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("productId", purchase.getSku());
                jSONObject.put("purchaseToken", purchase.getPurchaseToken());
                jSONArray.put(jSONObject);
            }
        }
        SharedPreferences sharedPreferences = b;
        if (sharedPreferences == null) {
            accbb.a("sharedPref");
        }
        Editor edit = sharedPreferences.edit();
        accbb.a((Object) edit, "sharedPref.edit()");
        if (jSONArray.length() > 0) {
            edit.putString("activePurchases", jSONArray.toString());
        } else {
            edit = edit.remove("activePurchases");
        }
        edit.apply();
    }

    public static String b() {
        SharedPreferences sharedPreferences = b;
        if (sharedPreferences == null) {
            accbb.a("sharedPref");
        }
        String string = sharedPreferences.getString("activeProduct", "");
        accbb.a((Object) string, "sharedPref.getString(ACTIVE_PRODUCT, \"\")");
        return string;
    }
}
