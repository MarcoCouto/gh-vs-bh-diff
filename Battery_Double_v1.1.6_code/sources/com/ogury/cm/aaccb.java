package com.ogury.cm;

import com.ogury.core.OguryError;

public final class aaccb {
    public static final aaccb a = new aaccb();

    private aaccb() {
    }

    public static String a(OguryError oguryError) {
        accbb.b(oguryError, "oguryError");
        int errorCode = oguryError.getErrorCode();
        if (errorCode == 1003) {
            return "form-error";
        }
        switch (errorCode) {
            case 0:
                return "no-internet-connection";
            case 1:
                return "assetKey-unknown";
            case 2:
                return "bundle-not-matching";
            case 3:
                return "server-not-responding";
            case 4:
                return "system-error";
            default:
                switch (errorCode) {
                    case 1005:
                        return "fair-choice-error";
                    case 1006:
                        return "fair-choice-error";
                    default:
                        return "system-error";
                }
        }
    }
}
