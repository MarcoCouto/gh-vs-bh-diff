package com.ogury.cm;

public final class aaabb {
    public static final aaabb a = new aaabb();
    private static aaaba b;

    private aaabb() {
    }

    public static aaaba a(int i) {
        aaaba aaabc;
        if (b != null) {
            return b;
        }
        switch (i) {
            case 1:
                aaabc = new aaabc();
                break;
            case 2:
                aaabc = new aaacb();
                break;
            default:
                throw new IllegalStateException("Bad TCF version used! Please use one of the declared ones in OguryChoiceManager class.");
        }
        return aaabc;
    }
}
