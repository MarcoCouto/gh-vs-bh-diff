package com.ogury.cm;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.github.mikephil.charting.BuildConfig;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ogury.cm.OguryChoiceManager.Answer;
import com.ogury.core.OguryError;
import com.ogury.crashreport.CrashConfig;
import com.ogury.crashreport.CrashReport;
import com.ogury.crashreport.SdkInfo;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

public abstract class aaaba implements aabcb {
    public static final aaaaa b = new aaaaa(null);
    public abbcb a;
    private acbbb c = new acbbb();
    /* access modifiers changed from: private */
    public String d = "";
    private Handler e = new Handler(Looper.getMainLooper());
    private aacba f = aacba.a;
    private aacbc g;
    private abaca h = new abaca();
    private acbca i = new acbca();
    private aaaac j = aaaac.a;
    private aabcc k = new aacaa(this);
    private boolean l;
    private String m = "";

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    public static final class aaaab implements acaac {
        final /* synthetic */ CountDownLatch a;

        aaaab(CountDownLatch countDownLatch) {
            this.a = countDownLatch;
        }

        public final void a() {
            this.a.countDown();
        }
    }

    public static final class aaaac implements aacac {
        final /* synthetic */ aaaba a;
        final /* synthetic */ CountDownLatch b;

        aaaac(aaaba aaaba, CountDownLatch countDownLatch) {
            this.a = aaaba;
            this.b = countDownLatch;
        }

        public final void a(String str) {
            accbb.b(str, "aaid");
            this.a.a(str);
            this.b.countDown();
        }
    }

    /* renamed from: com.ogury.cm.aaaba$aaaba reason: collision with other inner class name */
    public static final class C0058aaaba implements aacbc {
        final /* synthetic */ aaaba a;
        final /* synthetic */ Context b;

        C0058aaaba(aaaba aaaba, Context context) {
            this.a = aaaba;
            this.b = context;
        }

        public final void a(OguryError oguryError) {
            accbb.b(oguryError, "error");
            aaaba.a(this.a, oguryError);
            abcbc.a.endDataSourceConnections();
            aaaba.a(this.a, this.b, oguryError.getMessage(), this.a.d);
            aaaba.a(this.a, this.b, this.a.d);
        }

        public final void a(String str) {
            accbb.b(str, ServerResponseWrapper.RESPONSE_FIELD);
            this.a.a();
            com.ogury.cm.abbba.aaaaa aaaaa = abbba.a;
            com.ogury.cm.abbba.aaaaa.a().a(str, true);
            aacca aacca = aacca.a;
            if (aacca.j()) {
                aaaba.a(this.a, this.b);
            } else {
                aaaba aaaba = this.a;
                aacca aacca2 = aacca.a;
                aaaba.a(aaaba, aacca.e().b());
            }
            if (this.a.a == null) {
                accbb.a("sharedPrefsHandler");
            }
            abbcb.b(this.b);
            aaaba.a(this.a, this.b, this.a.d);
        }
    }

    static final class aaabb extends accbc implements accac<Integer, Boolean> {
        final /* synthetic */ aaaba a;
        final /* synthetic */ int b;

        aaabb(aaaba aaaba, int i) {
            this.a = aaaba;
            this.b = i;
            super(1);
        }

        public final /* synthetic */ Object a(Object obj) {
            ((Number) obj).intValue();
            aabac aabac = aabac.a;
            return Boolean.valueOf(aabac.a(this.a.b().e(), this.b));
        }
    }

    static final class aaabc implements Runnable {
        final /* synthetic */ accab a;

        aaabc(accab accab) {
            this.a = accab;
        }

        public final void run() {
            this.a.a();
        }
    }

    static final class aaaca implements Runnable {
        final /* synthetic */ aaaba a;
        final /* synthetic */ Context b;

        aaaca(aaaba aaaba, Context context) {
            this.a = aaaba;
            this.b = context;
        }

        public final void run() {
            aaaba.b(this.a, this.b);
        }
    }

    public static final class aaacb implements aacbc {
        final /* synthetic */ aaaba a;
        final /* synthetic */ OguryConsentListener b;

        static final class aaaaa extends accbc implements accab<acbba> {
            final /* synthetic */ aaacb a;

            aaaaa(aaacb aaacb) {
                this.a = aaacb;
                super(0);
            }

            public final /* synthetic */ Object a() {
                OguryConsentListener oguryConsentListener = this.a.b;
                aacca aacca = aacca.a;
                oguryConsentListener.onComplete(aacca.b().a());
                return acbba.a;
            }
        }

        static final class aaaab extends accbc implements accab<acbba> {
            final /* synthetic */ aaacb a;
            final /* synthetic */ OguryError b;

            aaaab(aaacb aaacb, OguryError oguryError) {
                this.a = aaacb;
                this.b = oguryError;
                super(0);
            }

            public final /* synthetic */ Object a() {
                this.a.b.onError(this.b);
                return acbba.a;
            }
        }

        aaacb(aaaba aaaba, OguryConsentListener oguryConsentListener) {
            this.a = aaaba;
            this.b = oguryConsentListener;
        }

        public final void a(OguryError oguryError) {
            accbb.b(oguryError, "error");
            this.a.a(false);
            aaaba.a(this.a, (accab) new aaaab(this, oguryError));
        }

        public final void a(String str) {
            accbb.b(str, ServerResponseWrapper.RESPONSE_FIELD);
            this.a.a(false);
            aaaba.a(this.a, (accab) new aaaaa(this));
        }
    }

    private final aacbc a(Context context) {
        return new C0058aaaba(this, context);
    }

    private final void a(Context context, String str, String str2) {
        this.l = true;
        this.d = str;
        if (acbbb.a(context)) {
            this.k.a(context, str2, new CountDownLatch(2));
            return;
        }
        aacbc aacbc = this.g;
        if (aacbc == null) {
            accbb.a("consentCallback");
        }
        aacbc.a(new OguryError(0, "No internet connection"));
    }

    private final void a(OguryConsentListener oguryConsentListener) {
        this.g = new aaacb(this, oguryConsentListener);
    }

    public static final /* synthetic */ void a(aaaba aaaba, Context context) {
        aaaba.e.post(new aaaca(aaaba, context));
        com.ogury.cm.abaaa.aaaaa aaaaa2 = abaaa.a;
        com.ogury.cm.abaaa.aaaaa.a().a(context);
        abbcb abbcb = aaaba.a;
        if (abbcb == null) {
            accbb.a("sharedPrefsHandler");
        }
        abbcb.a(context, aaaba.d);
    }

    public static final /* synthetic */ void a(aaaba aaaba, Context context, String str) {
        aaaba.i.a(context);
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        try {
            String str2 = BuildConfig.VERSION_NAME;
            aacca aacca = aacca.a;
            SdkInfo sdkInfo = new SdkInfo(str2, str, aacca.c().b());
            aacca aacca2 = aacca.a;
            CrashReport.register(context, sdkInfo, new CrashConfig(aacca.c().c(), context.getPackageName()));
        } catch (Throwable unused) {
            aaccc aaccc = aaccc.a;
            aaccc.b("crash report init failed");
        }
    }

    public static final /* synthetic */ void a(aaaba aaaba, Context context, String str, String str2) {
        if (str != null && accbb.a((Object) str, (Object) com.ogury.cm.aabba.aaaaa.b("assetKey-unknown"))) {
            if (aaaba.a == null) {
                accbb.a("sharedPrefsHandler");
            }
            abbcb.b(context, str2);
        }
    }

    public static final /* synthetic */ void a(aaaba aaaba, accab accab) {
        Thread currentThread = Thread.currentThread();
        Looper mainLooper = Looper.getMainLooper();
        accbb.a((Object) mainLooper, "Looper.getMainLooper()");
        if (accbb.a((Object) currentThread, (Object) mainLooper.getThread())) {
            accab.a();
        } else {
            aaaba.e.post(new aaabc(accab));
        }
    }

    public static final /* synthetic */ void a(aaaba aaaba, OguryError oguryError) {
        aacbc aacbc = aaaba.g;
        if (aacbc == null) {
            accbb.a("consentCallback");
        }
        aacbc.a(oguryError);
    }

    /* access modifiers changed from: private */
    public final void a(OguryError oguryError) {
        abcbc.a.endDataSourceConnections();
        aacca aacca = aacca.a;
        if (aacca.b().a() != Answer.NO_ANSWER) {
            aacbc aacbc = this.g;
            if (aacbc == null) {
                accbb.a("consentCallback");
            }
            aacca aacca2 = aacca.a;
            aacbc.a(aacca.b().a().toString());
            return;
        }
        aacbc aacbc2 = this.g;
        if (aacbc2 == null) {
            accbb.a("consentCallback");
        }
        aacbc2.a(oguryError);
    }

    private final void a(String str, Context context) {
        abaca.a(context, this.d, str, a(context));
    }

    public static boolean a(int i2, Integer[] numArr, accac<? super Integer, Boolean> accac) {
        accbb.b(numArr, "conditionValues");
        accbb.b(accac, "unit");
        if (a(numArr, i2)) {
            return ((Boolean) accac.a(Integer.valueOf(i2))).booleanValue();
        }
        aaccc aaccc = aaccc.a;
        aaccc.a("Value that you are trying to check is not valid.");
        return false;
    }

    private static boolean a(Integer[] numArr, int i2) {
        if (numArr.length == 0) {
            return true;
        }
        if (i2 != 0) {
            aabac aabac = aabac.a;
            if (aabac.a(acbbb.a(numArr), i2)) {
                return true;
            }
        }
        return false;
    }

    private static void b(Context context) {
        aacca aacca = aacca.a;
        accca c2 = aacca.c();
        Context applicationContext = context.getApplicationContext();
        accbb.a((Object) applicationContext, "context.applicationContext");
        String packageName = applicationContext.getPackageName();
        accbb.a((Object) packageName, "context.applicationContext.packageName");
        c2.a(packageName);
    }

    public static final /* synthetic */ void b(aaaba aaaba, Context context) {
        aacca aacca = aacca.a;
        boolean z = false;
        if (aacca.h().length() > 0) {
            if (!acbbb.a(context)) {
                aacbc aacbc = aaaba.g;
                if (aacbc == null) {
                    accbb.a("consentCallback");
                }
                aacbc.a(new OguryError(0, "No internet connection"));
            } else {
                aacca aacca2 = aacca.a;
                if (aacca.f().length() == 0) {
                    aacbc aacbc2 = aaaba.g;
                    if (aacbc2 == null) {
                        accbb.a("consentCallback");
                    }
                    aacbc2.a(new OguryError(4, "Missing consent configuration"));
                    aaccc aaccc = aaccc.a;
                    aaccc.b("Missing consent configuration");
                } else {
                    z = true;
                }
            }
            if (z) {
                aacbc aacbc3 = aaaba.g;
                if (aacbc3 == null) {
                    accbb.a("consentCallback");
                }
                aaaac.a(context, aacbc3);
            }
            return;
        }
        aaaba.a(new OguryError(4, "Consent not received"));
    }

    public static String d() {
        aacca aacca = aacca.a;
        if (!(aacca.b().b().length() > 0)) {
            return "";
        }
        aacca aacca2 = aacca.a;
        return aacca.b().b();
    }

    public static boolean e() {
        aacca aacca = aacca.a;
        return aacca.b().d();
    }

    public abstract void a();

    public final void a(Context context, String str, OguryConsentListener oguryConsentListener) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        accbb.b(oguryConsentListener, "oguryConsentListener");
        if (!this.l) {
            b(context);
            a(oguryConsentListener);
            a(context, str, "ask");
        }
    }

    public final void a(Context context, String str, CountDownLatch countDownLatch) {
        boolean z;
        String str2;
        accbb.b(context, "context");
        accbb.b(str, "requestType");
        accbb.b(countDownLatch, "countDownLatch");
        aacca aacca = aacca.a;
        aacca.c().b(this.m);
        String str3 = this.d;
        abbcb abbcb = this.a;
        if (abbcb == null) {
            accbb.a("sharedPrefsHandler");
        }
        abbcb.a(str3, context);
        this.d = str3;
        if (accbb.a((Object) str, (Object) "edit")) {
            str2 = "edit";
        } else {
            if (this.a == null) {
                accbb.a("sharedPrefsHandler");
            }
            String c2 = abbcb.c(context);
            boolean z2 = false;
            if (babac.a((CharSequence) c2) || (!accbb.a((Object) c2, (Object) this.d))) {
                z = false;
            } else {
                a(new OguryError(1, com.ogury.cm.aabba.aaaaa.b("assetKey-unknown")));
                z = true;
            }
            if (!z) {
                aacca aacca2 = aacca.a;
                boolean z3 = !aacca.d().after(new Date());
                boolean z4 = abcbc.a.tokenExistsForActiveProduct();
                abbcb abbcb2 = this.a;
                if (abbcb2 == null) {
                    accbb.a("sharedPrefsHandler");
                }
                boolean a2 = abbcb2.a(context);
                boolean z5 = z4 && !a2;
                boolean z6 = !z4 && a2;
                if (z3 || z5 || z6) {
                    z2 = true;
                }
                if (z2) {
                    str2 = "ask";
                } else {
                    a(new OguryError(4, "Consent not received"));
                }
            }
            return;
        }
        a(str2, context);
    }

    public final void a(Context context, CountDownLatch countDownLatch) {
        accbb.b(context, "context");
        accbb.b(countDownLatch, "countDownLatch");
        aacba.a(context, (aacac) new aaaac(this, countDownLatch));
        abcbc.a.startDataSourceConnections(context);
        abcbc.a.queryPurchase(new aaaab(countDownLatch));
    }

    public final void a(abbcb abbcb) {
        accbb.b(abbcb, "<set-?>");
        this.a = abbcb;
    }

    public final void a(String str) {
        accbb.b(str, "<set-?>");
        this.m = str;
    }

    public final void a(boolean z) {
        this.l = false;
    }

    public final boolean a(int i2) {
        return a(i2, c(), (accac<? super Integer, Boolean>) new aaabb<Object,Boolean>(this, i2));
    }

    public abstract ababa b();

    public final void b(Context context, String str, OguryConsentListener oguryConsentListener) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        accbb.b(oguryConsentListener, "oguryConsentListener");
        if (!this.l) {
            b(context);
            a(oguryConsentListener);
            a(context, str, "edit");
        }
    }

    public abstract Integer[] c();
}
