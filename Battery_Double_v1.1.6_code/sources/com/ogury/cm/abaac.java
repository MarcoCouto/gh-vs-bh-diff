package com.ogury.cm;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

public final class abaac extends abaaa {
    public static final aaaaa b = new aaaaa(null);

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    static final class aaaab extends accbc implements accab<acbba> {
        final /* synthetic */ abaac a;
        final /* synthetic */ Context b;

        aaaab(abaac abaac, Context context) {
            this.a = abaac;
            this.b = context;
            super(0);
        }

        public final /* synthetic */ Object a() {
            JSONObject l = abaac.a().l();
            if (l != null) {
                Editor edit = PreferenceManager.getDefaultSharedPreferences(this.b.getApplicationContext()).edit();
                abaac abaac = this.a;
                accbb.a((Object) edit, "editor");
                String optString = l.optString("vendorConsents");
                accbb.a((Object) optString, "it.optString(VENDOR_CONSENTS)");
                abaac.a(abaac, edit, optString);
                abaac.a(this.a, edit, l.optJSONObject("publisherRestrictions"));
                abaac abaac2 = this.a;
                String optString2 = l.optString("vendorLegitimateInterests");
                accbb.a((Object) optString2, "it.optString(VENDOR_LEGITIMATE_INTERESTS)");
                abaac.b(abaac2, edit, optString2);
                aabbc aabbc = aabbc.a;
                edit.putString("IABTCF_PublisherConsent", aabbc.a(l.optInt("publisherConsent")));
                edit.putInt("IABTCF_CmpSdkID", l.optInt("cmpSdkId"));
                edit.putInt("IABTCF_CmpSdkVersion", l.optInt("cmpSdkVersion"));
                edit.putInt("IABTCF_PolicyVersion", l.optInt("policyVersion"));
                aacca aacca = aacca.a;
                edit.putInt("IABTCF_gdprApplies", aacca.b().d() ? 1 : 0);
                edit.putString("IABTCF_PublisherCC", l.optString("publisherCC"));
                edit.putInt("IABTCF_PurposeOneTreatment", l.optInt("purposeOneTreatment"));
                edit.putInt("IABTCF_UseNonStandardStacks", l.optInt("useNonStandardStacks"));
                aacca aacca2 = aacca.a;
                edit.putString("IABTCF_TCString", aacca.b().b());
                aabbc aabbc2 = aabbc.a;
                edit.putString("IABTCF_PurposeConsents", aabbc.a(l.optInt("purposeConsents")));
                aabbc aabbc3 = aabbc.a;
                edit.putString("IABTCF_PurposeLegitimateInterests", aabbc.a(l.optInt("purposeLegitimateInterests")));
                aabbc aabbc4 = aabbc.a;
                edit.putString("IABTCF_SpecialFeaturesOptIns", aabbc.a(l.optInt("specialFeaturesOptIns")));
                aabbc aabbc5 = aabbc.a;
                edit.putString("IABTCF_PublisherLegitimateInterests", aabbc.a(l.optInt("publisherLegitimateInterests")));
                aabbc aabbc6 = aabbc.a;
                edit.putString("IABTCF_PublisherCustomPurposesConsents", aabbc.a(l.optInt("publisherCustomPurposesConsents")));
                aabbc aabbc7 = aabbc.a;
                edit.putString("IABTCF_PublisherCustomPurposesLegitimateInterests", aabbc.a(l.optInt("publisherCustomPurposesLegitimateInterests")));
                edit.apply();
            }
            return acbba.a;
        }
    }

    public static ababc a() {
        aacca aacca = aacca.a;
        return (ababc) ababa.f();
    }

    private static String a(String str) {
        if (str != null) {
            String substring = str.substring(1);
            accbb.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        throw new acbab("null cannot be cast to non-null type java.lang.String");
    }

    public static final /* synthetic */ void a(abaac abaac, Editor editor, String str) {
        aabbc aabbc = aabbc.a;
        String a = aabbc.a(str);
        if (a.length() > 0) {
            editor.putString("IABTCF_VendorConsents", a(a));
        } else {
            editor.putString("IABTCF_VendorConsents", b());
        }
    }

    public static final /* synthetic */ void a(abaac abaac, Editor editor, JSONObject jSONObject) {
        if (jSONObject != null) {
            aabbc aabbc = aabbc.a;
            accbb.b(jSONObject, "jsonObject");
            HashMap hashMap = new HashMap();
            Iterator keys = jSONObject.keys();
            accbb.a((Object) keys, "purposeIds");
            while (keys.hasNext()) {
                String str = (String) keys.next();
                JSONObject optJSONObject = jSONObject.optJSONObject(str);
                String optString = optJSONObject.optString("0");
                accbb.a((Object) optString, "arrayOfValuesForPurpose.optString(\"0\")");
                String a = aabbc.a(optString);
                int length = a.length();
                String optString2 = optJSONObject.optString("1");
                accbb.a((Object) optString2, "arrayOfValuesForPurpose.optString(\"1\")");
                String a2 = aabbc.a(optString2);
                if (a2.length() > length) {
                    length = a2.length();
                }
                String optString3 = optJSONObject.optString("2");
                accbb.a((Object) optString3, "arrayOfValuesForPurpose.optString(\"2\")");
                String a3 = aabbc.a(optString3);
                if (a3.length() > length) {
                    length = a3.length();
                }
                String str2 = "";
                int i = 1;
                while (i < length) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str2);
                    String str3 = (a.length() <= i || a.charAt(i) != '1') ? (a2.length() <= i || a2.charAt(i) != '1') ? (a3.length() <= i || a3.charAt(i) != '1') ? "3" : "2" : "1" : "0";
                    sb.append(str3);
                    str2 = sb.toString();
                    i++;
                }
                Map map = hashMap;
                accbb.a((Object) str, "purposeId");
                map.put(str, str2);
            }
            for (String str4 : hashMap.keySet()) {
                editor.putString("IABTCF_PublisherRestrictions".concat(String.valueOf(str4)), (String) hashMap.get(str4));
            }
        }
    }

    private static String b() {
        CharSequence charSequence = "0";
        int n = a().n();
        accbb.b(charSequence, "receiver$0");
        int i = 1;
        if (n >= 0) {
            switch (n) {
                case 0:
                    return "";
                case 1:
                    return charSequence.toString();
                default:
                    switch (charSequence.length()) {
                        case 0:
                            return "";
                        case 1:
                            char charAt = charSequence.charAt(0);
                            char[] cArr = new char[n];
                            int length = cArr.length;
                            for (int i2 = 0; i2 < length; i2++) {
                                cArr[i2] = charAt;
                            }
                            return new String(cArr);
                        default:
                            StringBuilder sb = new StringBuilder(charSequence.length() * n);
                            if (n > 0) {
                                while (true) {
                                    sb.append(charSequence);
                                    if (i != n) {
                                        i++;
                                    }
                                }
                            }
                            String sb2 = sb.toString();
                            accbb.a((Object) sb2, "sb.toString()");
                            return sb2;
                    }
            }
        } else {
            StringBuilder sb3 = new StringBuilder("Count 'n' must be non-negative, but was ");
            sb3.append(n);
            sb3.append('.');
            throw new IllegalArgumentException(sb3.toString().toString());
        }
    }

    public static final /* synthetic */ void b(abaac abaac, Editor editor, String str) {
        aabbc aabbc = aabbc.a;
        String a = aabbc.a(str);
        editor.putString("IABTCF_VendorLegitimateInterests", (!(a.length() > 0) || a.length() <= 1) ? b() : a(a));
    }

    public final void a(Context context) {
        accbb.b(context, "context");
        acbca.a(false, false, null, null, 0, new aaaab(this, context), 31, null);
    }
}
