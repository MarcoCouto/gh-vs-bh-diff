package com.ogury.cm;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class babab implements baabc<baabb> {
    /* access modifiers changed from: private */
    public final CharSequence a;
    /* access modifiers changed from: private */
    public final int b = 0;
    /* access modifiers changed from: private */
    public final int c = 0;
    /* access modifiers changed from: private */
    public final accba<CharSequence, Integer, acbaa<Integer, Integer>> d;

    public static final class aaaaa implements baaaa, Iterator<baabb> {
        final /* synthetic */ babab a;
        private int b = -1;
        private int c;
        private int d;
        private baabb e;
        private int f;

        aaaaa(babab babab) {
            this.a = babab;
            int d2 = babab.b;
            int length = babab.a.length();
            if (length >= 0) {
                if (d2 < 0) {
                    d2 = 0;
                } else if (d2 > length) {
                    d2 = length;
                }
                this.c = d2;
                this.d = this.c;
                return;
            }
            StringBuilder sb = new StringBuilder("Cannot coerce value to an empty range: maximum ");
            sb.append(length);
            sb.append(" is less than minimum 0.");
            throw new IllegalArgumentException(sb.toString());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0022, code lost:
            if (r7.f < com.ogury.cm.babab.a(r7.a)) goto L_0x0024;
         */
        private final void a() {
            baabb baabb;
            baabb baabb2;
            int i = 0;
            if (this.d < 0) {
                this.b = 0;
                this.e = null;
                return;
            }
            int i2 = -1;
            if (this.a.c > 0) {
                this.f++;
            }
            if (this.d <= this.a.a.length()) {
                acbaa acbaa = (acbaa) this.a.d.a(this.a.a, Integer.valueOf(this.d));
                if (acbaa == null) {
                    baabb = new baabb(this.c, babac.b(this.a.a));
                    this.e = baabb;
                    this.d = i2;
                    this.b = 1;
                }
                int intValue = ((Number) acbaa.c()).intValue();
                int intValue2 = ((Number) acbaa.d()).intValue();
                int i3 = this.c;
                if (intValue <= Integer.MIN_VALUE) {
                    com.ogury.cm.baabb.aaaaa aaaaa = baabb.b;
                    baabb2 = baabb.c;
                } else {
                    baabb2 = new baabb(i3, intValue - 1);
                }
                this.e = baabb2;
                this.c = intValue + intValue2;
                int i4 = this.c;
                if (intValue2 == 0) {
                    i = 1;
                }
                i2 = i4 + i;
                this.d = i2;
                this.b = 1;
            }
            baabb = new baabb(this.c, babac.b(this.a.a));
            this.e = baabb;
            this.d = i2;
            this.b = 1;
        }

        public final boolean hasNext() {
            if (this.b == -1) {
                a();
            }
            return this.b == 1;
        }

        public final /* synthetic */ Object next() {
            if (this.b == -1) {
                a();
            }
            if (this.b != 0) {
                baabb baabb = this.e;
                if (baabb != null) {
                    this.e = null;
                    this.b = -1;
                    return baabb;
                }
                throw new acbab("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }

        public final void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    public babab(CharSequence charSequence, int i, int i2, accba<? super CharSequence, ? super Integer, acbaa<Integer, Integer>> accba) {
        accbb.b(charSequence, "input");
        accbb.b(accba, "getNextMatch");
        this.a = charSequence;
        this.d = accba;
    }

    public final Iterator<baabb> a() {
        return new aaaaa<>(this);
    }
}
