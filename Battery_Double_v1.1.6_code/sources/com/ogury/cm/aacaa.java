package com.ogury.cm;

import android.content.Context;
import java.util.concurrent.CountDownLatch;

public final class aacaa extends aabcc {

    static final class aaaaa extends accbc implements accab<acbba> {
        final /* synthetic */ aacaa a;
        final /* synthetic */ CountDownLatch b;
        final /* synthetic */ Context c;
        final /* synthetic */ String d;

        aaaaa(aacaa aacaa, CountDownLatch countDownLatch, Context context, String str) {
            this.a = aacaa;
            this.b = countDownLatch;
            this.c = context;
            this.d = str;
            super(0);
        }

        public final /* synthetic */ Object a() {
            this.b.await();
            this.a.a().a(this.c, this.d, this.b);
            return acbba.a;
        }
    }

    public aacaa(aabcb aabcb) {
        accbb.b(aabcb, "requestScheduler");
        super(aabcb);
    }

    public final void a(Context context, String str, CountDownLatch countDownLatch) {
        accbb.b(context, "context");
        accbb.b(str, "requestType");
        accbb.b(countDownLatch, "countDownLatch");
        a().a(context, countDownLatch);
        acbca.a(false, false, null, null, 0, new aaaaa(this, countDownLatch, context, str), 31, null);
    }
}
