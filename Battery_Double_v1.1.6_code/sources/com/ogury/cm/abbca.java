package com.ogury.cm;

import com.ogury.core.OguryError;

public final class abbca {
    private final boolean a;
    private final OguryError b;

    public abbca(boolean z, OguryError oguryError) {
        accbb.b(oguryError, "error");
        this.a = z;
        this.b = oguryError;
    }

    public /* synthetic */ abbca(boolean z, OguryError oguryError, int i, baaca baaca) {
        this(true, new OguryError(0, ""));
    }

    public final boolean a() {
        return this.a;
    }

    public final OguryError b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof abbca) {
                abbca abbca = (abbca) obj;
                if (!(this.a == abbca.a) || !accbb.a((Object) this.b, (Object) abbca.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        boolean z = this.a;
        if (z) {
            z = true;
        }
        int i = (z ? 1 : 0) * true;
        OguryError oguryError = this.b;
        return i + (oguryError != null ? oguryError.hashCode() : 0);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ResponseStatus(status=");
        sb.append(this.a);
        sb.append(", error=");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }
}
