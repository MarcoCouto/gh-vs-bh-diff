package com.ogury.cm;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs.CastExtraArgs;

public final class abcab {
    public static final abcab a = new abcab();
    private static abcba b;
    private static BillingClient c;
    private static boolean d;

    private abcab() {
    }

    public static abccb a(Context context) {
        accbb.b(context, "context");
        abccb abccb = abccb.a;
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("cacheBilling", 0);
        accbb.a((Object) sharedPreferences, "context.applicationConte…ME, Context.MODE_PRIVATE)");
        abccb.a(sharedPreferences);
        return abccb;
    }

    public static void b() {
        b = new abccc();
    }

    public final synchronized BillingClient a(Context context, PurchasesUpdatedListener purchasesUpdatedListener) {
        BillingClient billingClient;
        accbb.b(context, "context");
        accbb.b(purchasesUpdatedListener, CastExtraArgs.LISTENER);
        if (!d) {
            BillingClient build = BillingClient.newBuilder(context).enablePendingPurchases().setListener(purchasesUpdatedListener).build();
            accbb.a((Object) build, "BillingClient.newBuilder…istener(listener).build()");
            c = build;
        }
        billingClient = c;
        if (billingClient == null) {
            accbb.a("billingClient");
        }
        return billingClient;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003c A[Catch:{ Exception -> 0x0025, Throwable -> 0x000f }] */
    public final synchronized abcba a() {
        abcba abcba;
        if (b == null) {
            try {
                b = new abcca();
            } catch (Exception e) {
                String str = "FairChoice";
                String message = e.getMessage();
                if (message == null) {
                    message = e.toString();
                }
                Log.d(str, message);
                abcbc abcbc = abcbc.a;
            } catch (Throwable th) {
                String str2 = "FairChoice";
                String message2 = th.getMessage();
                if (message2 == null) {
                    message2 = th.toString();
                }
                Log.d(str2, message2);
                abcbc abcbc2 = abcbc.a;
            }
        }
        abcba = b;
        if (abcba == null) {
            accbb.a("billingLibrary");
        }
        return abcba;
        abcbc.a();
        abcba = b;
        if (abcba == null) {
        }
        return abcba;
    }
}
