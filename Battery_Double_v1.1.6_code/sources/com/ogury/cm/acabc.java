package com.ogury.cm;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import java.util.concurrent.LinkedBlockingQueue;

public final class acabc implements ServiceConnection {
    private final LinkedBlockingQueue<IBinder> a = new LinkedBlockingQueue<>(1);
    private boolean b;

    public final IBinder a() throws InterruptedException {
        if (!this.b) {
            this.b = true;
            Object take = this.a.take();
            if (take != null) {
                return (IBinder) take;
            }
            throw new acbab("null cannot be cast to non-null type android.os.IBinder");
        }
        throw new IllegalStateException();
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        accbb.b(componentName, "name");
        accbb.b(iBinder, NotificationCompat.CATEGORY_SERVICE);
        try {
            this.a.put(iBinder);
        } catch (InterruptedException unused) {
            Log.d(NotificationCompat.CATEGORY_SERVICE, "intrerrupted");
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        accbb.b(componentName, "name");
    }
}
