package com.ogury.cm;

import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.util.ArrayList;

public final class abbac {
    private String a = "";
    private String b = "";
    private String c = "";
    private aacbc d;
    private ArrayList<acbaa<String, String>> e;
    private final String f = "v1/";
    private final String g = "v2/";

    public final abbac a(aacbc aacbc) {
        this.d = aacbc;
        return this;
    }

    public final abbac a(String str) {
        accbb.b(str, "requestMethod");
        this.a = str;
        return this;
    }

    public final abbac a(String str, String str2) {
        accbb.b(str, ParametersKeys.KEY);
        accbb.b(str2, "value");
        if (this.e == null) {
            this.e = new ArrayList<>();
        }
        acbaa acbaa = new acbaa(str, str2);
        ArrayList<acbaa<String, String>> arrayList = this.e;
        if (arrayList != null) {
            arrayList.add(acbaa);
        }
        return this;
    }

    public final String a() {
        return this.a;
    }

    public final abbac b(String str) {
        accbb.b(str, "requestBody");
        this.b = str;
        return this;
    }

    public final String b() {
        return this.b;
    }

    public final abbac c(String str) {
        String str2;
        accbb.b(str, "requestType");
        StringBuilder sb = new StringBuilder("https://consent-manager-events.ogury.io/");
        aacca aacca = aacca.a;
        switch (aacca.a()) {
            case 2:
                str2 = this.g;
                break;
            default:
                str2 = this.f;
                break;
        }
        sb.append(str2);
        sb.append(str);
        this.c = sb.toString();
        return this;
    }

    public final String c() {
        return this.c;
    }

    public final aacbc d() {
        return this.d;
    }

    public final ArrayList<acbaa<String, String>> e() {
        return this.e;
    }

    public final abbaa f() {
        return new abbaa(this);
    }
}
