package com.ogury.cm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebView;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.core.OguryError;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

public final class aacab {
    public static final aaaaa a = new aaaaa(null);

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    static final class aaaab extends accbc implements accab<acbba> {
        final /* synthetic */ aacab a;
        final /* synthetic */ WebView b;
        final /* synthetic */ aacbc c;

        aaaab(aacab aacab, WebView webView, aacbc aacbc) {
            this.a = aacab;
            this.b = webView;
            this.c = aacbc;
            super(0);
        }

        public final /* synthetic */ Object a() {
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            final com.ogury.cm.accca.aaaab aaaab = new com.ogury.cm.accca.aaaab();
            aaaab.a = "";
            final com.ogury.cm.accca.aaaab aaaab2 = new com.ogury.cm.accca.aaaab();
            aaaab2.a = "";
            abcbc.a.setQueryProductDetailsListener(new acaab() {
                public final void a(String str, String str2) {
                    accbb.b(str, "status");
                    accbb.b(str2, "skuDetailsJson");
                    aaaab.a = str;
                    aaaab2.a = str2;
                    countDownLatch.countDown();
                }
            });
            countDownLatch.await();
            abcbc.a.setQueryProductDetailsListener(null);
            new Handler(Looper.getMainLooper()).post(new Runnable(this) {
                final /* synthetic */ aaaab a;

                {
                    this.a = r1;
                }

                public final void run() {
                    if (accbb.a((Object) (String) aaaab.a, (Object) "ok")) {
                        aacab.a(this.a.b, (String) aaaab2.a);
                        this.a.c.a(ParametersKeys.READY);
                        return;
                    }
                    this.a.c.a(new OguryError(1005, com.ogury.cm.aabba.aaaaa.b((String) aaaab.a)));
                }
            });
            if (!accbb.a((Object) (String) aaaab.a, (Object) "ok")) {
                new abaca();
                abaca.a((String) aaaab.a, this.c);
            }
            return acbba.a;
        }
    }

    public static final class aaaac implements abcac {
        final /* synthetic */ aacab a;
        final /* synthetic */ WebView b;

        aaaac(aacab aacab, WebView webView) {
            this.a = aacab;
            this.b = webView;
        }

        public final void a(String str) {
            accbb.b(str, "jsonResult");
            aacab.a(str, this.b);
            abcbc.a.setBillingFinishedListener(null);
        }
    }

    private static String a(String str) {
        String str2;
        if (str != null) {
            StringBuilder sb = new StringBuilder(", \"");
            sb.append(com.ogury.cm.aabba.aaaaa.a(str));
            sb.append('\"');
            str2 = sb.toString();
        } else {
            str2 = "";
        }
        StringBuilder sb2 = new StringBuilder("javascript:(function(){ogFormBridge.init(\"");
        aacca aacca = aacca.a;
        sb2.append(com.ogury.cm.aabba.aaaaa.a(aacca.f()));
        sb2.append('\"');
        sb2.append(str2);
        sb2.append(")})()");
        return sb2.toString();
    }

    private static void a(Context context) {
        if (context instanceof ConsentActivity) {
            ((ConsentActivity) context).finish();
        }
    }

    /* access modifiers changed from: private */
    public static void a(WebView webView, String str) {
        webView.setVisibility(0);
        webView.loadUrl(a(str));
    }

    public static void a(String str, WebView webView) {
        accbb.b(str, "jsonResult");
        accbb.b(webView, "webView");
        StringBuilder sb = new StringBuilder("javascript:(function(){ogFormBridge.purchase(\"");
        sb.append(com.ogury.cm.aabba.aaaaa.a(str));
        sb.append("\")})()");
        webView.loadUrl(sb.toString());
    }

    public final void a(String str, Context context, aacbc aacbc, WebView webView) {
        String str2 = str;
        Context context2 = context;
        aacbc aacbc2 = aacbc;
        WebView webView2 = webView;
        accbb.b(str2, "url");
        accbb.b(context2, "context");
        accbb.b(aacbc2, "callback");
        accbb.b(webView2, "webView");
        Locale locale = Locale.US;
        accbb.a((Object) locale, "Locale.US");
        String lowerCase = str2.toLowerCase(locale);
        accbb.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        boolean z = false;
        if (babac.a(lowerCase, "https://ogyconsent", false, 2, null)) {
            String substring = str2.substring(20);
            accbb.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
            if (babac.a(substring, "consent=", false, 2, null)) {
                com.ogury.cm.abbba.aaaaa aaaaa2 = abbba.a;
                com.ogury.cm.abbba.aaaaa.a().a(substring, false);
                aacca aacca = aacca.a;
                if (aacca.j()) {
                    StringBuilder sb = new StringBuilder("parsedConfig=");
                    aacca aacca2 = aacca.a;
                    sb.append(aacca.e());
                    aacbc2.a(sb.toString());
                } else {
                    aacca aacca3 = aacca.a;
                    aacbc2.a(aacca.e().b());
                }
                a(context);
                abcbc.a.endDataSourceConnections();
            } else if (babac.a(substring, "ogyRedirect=", false, 2, null)) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(babac.a(substring, "ogyRedirect=", (String) null, 2, (Object) null)));
                    if (context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0) {
                        z = true;
                    }
                    if (z) {
                        context2.startActivity(intent);
                    }
                } catch (Exception e) {
                    aaccc aaccc = aaccc.a;
                    aaccc.a((Throwable) e);
                }
            } else {
                if (babac.a(substring, "error=", false, 2, null)) {
                    if (substring != null) {
                        String substring2 = substring.substring(6);
                        accbb.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
                        aacbc2.a(new OguryError(1003, babac.a(babac.a(substring2, "%20", " ", false, 4, (Object) null), "%22", "\"", false, 4, (Object) null)));
                        a(context);
                    } else {
                        throw new acbab("null cannot be cast to non-null type java.lang.String");
                    }
                }
            }
        } else {
            CharSequence charSequence = str2;
            if (babac.a(charSequence, (CharSequence) "?ready", false, 2, (Object) null)) {
                if (abcbc.a.isProductActivated()) {
                    acbca.a(false, false, null, null, 0, new aaaab(this, webView2, aacbc2), 31, null);
                    return;
                }
                a(webView2, (String) null);
                aacbc2.a(ParametersKeys.READY);
            } else if (babac.a(charSequence, (CharSequence) "?success", false, 2, (Object) null)) {
                aacbc2.a("success");
            } else {
                if (babac.a(charSequence, (CharSequence) "?purchase", false, 2, (Object) null)) {
                    abcbc.a.setBillingFinishedListener(new aaaac(this, webView2));
                    abcbc abcbc = abcbc.a;
                    if (context2 != null) {
                        abcbc.launchBillingFlow((Activity) context2);
                    } else {
                        throw new acbab("null cannot be cast to non-null type android.app.Activity");
                    }
                }
            }
        }
    }
}
