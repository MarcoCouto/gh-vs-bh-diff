package com.ogury.cm;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.ironsource.sdk.constants.Constants.RequestParameters;

public final class abcaa extends abbcb {
    public static final aaaaa b = new aaaaa(null);
    private String c = "cacheConsentV2";

    public static final class aaaaa {
        private aaaaa() {
        }

        public /* synthetic */ aaaaa(baaca baaca) {
            this();
        }
    }

    private static ababc b() {
        aacca aacca = aacca.a;
        return (ababc) ababa.f();
    }

    public final String a() {
        return this.c;
    }

    public final boolean a(String str, Context context) {
        accbb.b(str, "assetKey");
        accbb.b(context, "context");
        if (super.a(str, context)) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(this.c, 0);
            ababc b2 = b();
            b2.a(sharedPreferences.getInt("purposes", 0));
            String string = sharedPreferences.getString("acceptedVendors", "");
            accbb.a((Object) string, "prefs.getString(ACCEPTED_VENDORS, \"\")");
            Object[] array = com.ogury.cm.ConsentActivity.aaaaa.a(com.ogury.cm.ConsentActivity.aaaaa.b(string)).toArray(new Integer[0]);
            if (array != null) {
                b2.a((Integer[]) array);
                b2.c(sharedPreferences.getInt("specialFeatures", 0));
                String string2 = sharedPreferences.getString("vendorsLi", "");
                accbb.a((Object) string2, "prefs.getString(VENDORS_LI, \"\")");
                Object[] array2 = com.ogury.cm.ConsentActivity.aaaaa.a(com.ogury.cm.ConsentActivity.aaaaa.b(string2)).toArray(new Integer[0]);
                if (array2 != null) {
                    b2.b((Integer[]) array2);
                    b2.b(sharedPreferences.getInt("purposesLi", 0));
                    String string3 = sharedPreferences.getString("vendorPurposesAndSF", "");
                    accbb.a((Object) string3, "prefs.getString(VENDOR_PURPOSES_AND_SF, \"\")");
                    b2.b(com.ogury.cm.ConsentActivity.aaaaa.a(string3));
                    b2.d(sharedPreferences.getInt("maxVendorId", 0));
                } else {
                    throw new acbab("null cannot be cast to non-null type kotlin.Array<T>");
                }
            } else {
                throw new acbab("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void d(Context context) {
        accbb.b(context, "context");
        super.d(context);
        Editor edit = context.getSharedPreferences(this.c, 0).edit();
        edit.putInt("purposes", b().i());
        edit.putString("acceptedVendors", acbbb.a(b().g(), null, RequestParameters.LEFT_BRACKETS, RequestParameters.RIGHT_BRACKETS, 0, null, null, 57, null));
        edit.putInt("specialFeatures", b().k());
        edit.putString("vendorsLi", acbbb.a(b().h(), null, RequestParameters.LEFT_BRACKETS, RequestParameters.RIGHT_BRACKETS, 0, null, null, 57, null));
        edit.putInt("purposesLi", b().j());
        edit.putString("vendorPurposesAndSF", String.valueOf(b().m()));
        edit.putInt("maxVendorId", b().n());
        edit.apply();
    }
}
