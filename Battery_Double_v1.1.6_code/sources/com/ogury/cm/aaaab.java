package com.ogury.cm;

import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.cm.ConsentActivity.aaaaa;
import java.util.Iterator;
import org.json.JSONObject;

public final class aaaab {
    public static final aaaab a = new aaaab();

    private aaaab() {
    }

    public static int a(String str, int i, JSONObject jSONObject) {
        accbb.b(str, ParametersKeys.KEY);
        accbb.b(str, ParametersKeys.KEY);
        JSONObject optJSONObject = jSONObject != null ? jSONObject.optJSONObject(str) : null;
        if (optJSONObject == null) {
            return 0;
        }
        accbb.b(optJSONObject, "jsonObject");
        Iterator keys = optJSONObject.keys();
        accbb.a((Object) keys, "keysInJson");
        int i2 = 0;
        while (keys.hasNext()) {
            String str2 = (String) keys.next();
            String optString = optJSONObject.optString(str2);
            accbb.a((Object) optString, "jsonObject.optString(key)");
            Object[] array = aaaaa.a(aaaaa.b(optString)).toArray(new Integer[0]);
            if (array != null) {
                Integer[] numArr = (Integer[]) array;
                aabac aabac = aabac.a;
                if (aabac.a(numArr, i)) {
                    accbb.a((Object) str2, ParametersKeys.KEY);
                    i2 += (int) Math.pow(2.0d, (double) Integer.parseInt(str2));
                }
            } else {
                throw new acbab("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return i2;
    }
}
