package com.ogury.ed;

import android.app.Activity;
import android.content.Context;
import io.presage.Cheddar;
import io.presage.EcirdelAubrac;
import io.presage.SableduBoulonnais;
import io.presage.ao;
import io.presage.common.AdConfig;
import io.presage.hi;
import io.presage.hl;

public final class OguryThumbnailAd {
    public static final CamembertauCalvados a = new CamembertauCalvados(0);
    private final Cheddar b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private OguryThumbnailAd(Cheddar cheddar) {
        this.b = cheddar;
    }

    public /* synthetic */ OguryThumbnailAd(Cheddar cheddar, hi hiVar) {
        this(cheddar);
    }

    public OguryThumbnailAd(Context context, AdConfig adConfig) {
        Context applicationContext = context.getApplicationContext();
        hl.a((Object) applicationContext, "context.applicationContext");
        this(new Cheddar(applicationContext, adConfig, SableduBoulonnais.OVERLAY_THUMBNAIL));
    }

    public final void setCallback(OguryThumbnailAdCallback oguryThumbnailAdCallback) {
        this.b.a((EcirdelAubrac) new ao(oguryThumbnailAdCallback));
    }

    public final void load(int i, int i2) {
        this.b.a(i, i2);
    }

    public final void show(Activity activity, int i, int i2) {
        this.b.a(activity, i, i2);
    }

    public final boolean isLoaded() {
        return this.b.a();
    }

    public final void setWhiteListPackages(String... strArr) {
        this.b.a(strArr);
    }

    @SafeVarargs
    public final void setBlackListActivities(Class<? extends Activity>... clsArr) {
        this.b.a(clsArr);
    }

    public final void setWhiteListFragmentPackages(String... strArr) {
        this.b.b(strArr);
    }

    @SafeVarargs
    public final void setBlackListFragments(Class<? extends Object>... clsArr) {
        this.b.b(clsArr);
    }
}
