package com.ogury.consent.manager;

import android.app.Activity;
import android.content.Context;
import com.ogury.cm.OguryChoiceManager;
import com.ogury.cm.OguryChoiceManager.TcfV1;
import com.ogury.cm.OguryCmConfig;
import com.ogury.cm.OguryConsentListener;
import com.ogury.cm.acacc;
import com.ogury.cm.accbb;
import com.ogury.cm.baaca;

public final class ConsentManager {
    public static final ConsentManager INSTANCE = new ConsentManager();
    public static ConsentListener consentListener;

    public enum Answer {
        FULL_APPROVAL,
        PARTIAL_APPROVAL,
        REFUSAL,
        NO_ANSWER
    }

    public enum Purpose {
        INFORMATION,
        PERSONALISATION,
        AD,
        CONTENT,
        MEASUREMENT
    }

    public static final class SpecialFeature {
        public static final Companion Companion = new Companion(null);
        public static final int PRECISE_GEOLOCATION = 1;

        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(baaca baaca) {
                this();
            }
        }
    }

    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[com.ogury.cm.OguryChoiceManager.Answer.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[com.ogury.cm.OguryChoiceManager.Answer.FULL_APPROVAL.ordinal()] = 1;
            $EnumSwitchMapping$0[com.ogury.cm.OguryChoiceManager.Answer.PARTIAL_APPROVAL.ordinal()] = 2;
            $EnumSwitchMapping$0[com.ogury.cm.OguryChoiceManager.Answer.REFUSAL.ordinal()] = 3;
            $EnumSwitchMapping$0[com.ogury.cm.OguryChoiceManager.Answer.NO_ANSWER.ordinal()] = 4;
            int[] iArr2 = new int[Purpose.values().length];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[Purpose.INFORMATION.ordinal()] = 1;
            $EnumSwitchMapping$1[Purpose.PERSONALISATION.ordinal()] = 2;
            $EnumSwitchMapping$1[Purpose.AD.ordinal()] = 3;
            $EnumSwitchMapping$1[Purpose.CONTENT.ordinal()] = 4;
            $EnumSwitchMapping$1[Purpose.MEASUREMENT.ordinal()] = 5;
        }
    }

    private ConsentManager() {
    }

    private static com.ogury.cm.OguryChoiceManager.TcfV1.Purpose a(Purpose purpose) {
        switch (WhenMappings.$EnumSwitchMapping$1[purpose.ordinal()]) {
            case 1:
                return com.ogury.cm.OguryChoiceManager.TcfV1.Purpose.INFORMATION;
            case 2:
                return com.ogury.cm.OguryChoiceManager.TcfV1.Purpose.PERSONALISATION;
            case 3:
                return com.ogury.cm.OguryChoiceManager.TcfV1.Purpose.AD;
            case 4:
                return com.ogury.cm.OguryChoiceManager.TcfV1.Purpose.CONTENT;
            case 5:
                return com.ogury.cm.OguryChoiceManager.TcfV1.Purpose.MEASUREMENT;
            default:
                throw new acacc();
        }
    }

    private static OguryConsentListener a() {
        return new ConsentManager$getOguryConsentListener$1();
    }

    private static void a(Context context) {
        if (!(context instanceof Activity)) {
            throw new IllegalArgumentException("Context must be instance of activity.");
        }
    }

    public static final /* synthetic */ Answer access$mapAnswer(ConsentManager consentManager, com.ogury.cm.OguryChoiceManager.Answer answer) {
        switch (WhenMappings.$EnumSwitchMapping$0[answer.ordinal()]) {
            case 1:
                return Answer.FULL_APPROVAL;
            case 2:
                return Answer.PARTIAL_APPROVAL;
            case 3:
                return Answer.REFUSAL;
            case 4:
                return Answer.NO_ANSWER;
            default:
                throw new acacc();
        }
    }

    public static final void ask(Context context, String str, ConsentListener consentListener2) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        accbb.b(consentListener2, "consentListener");
        a(context);
        consentListener = consentListener2;
        OguryChoiceManager.INSTANCE.resetClientConsentImpl$consent_manager_prodRelease();
        OguryChoiceManager.initialize(context, str, new OguryCmConfig(1));
        OguryChoiceManager.ask((Activity) context, a());
    }

    public static final void edit(Context context, String str, ConsentListener consentListener2) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        accbb.b(consentListener2, "consentListener");
        a(context);
        consentListener = consentListener2;
        OguryChoiceManager.INSTANCE.resetClientConsentImpl$consent_manager_prodRelease();
        OguryChoiceManager.initialize(context, str, new OguryCmConfig(1));
        OguryChoiceManager.edit((Activity) context, a());
    }

    public static final boolean gdprApplies() {
        OguryChoiceManager.INSTANCE.initializeTcfV1$consent_manager_prodRelease(null);
        return OguryChoiceManager.gdprApplies();
    }

    public static final String getIabString() {
        OguryChoiceManager.INSTANCE.initializeTcfV1$consent_manager_prodRelease(null);
        return TcfV1.getIabString();
    }

    public static final boolean hasPaid(Context context) {
        accbb.b(context, "context");
        OguryChoiceManager.INSTANCE.initializeTcfV1$consent_manager_prodRelease(context);
        return OguryChoiceManager.hasPaid();
    }

    public static final boolean isAccepted(String str) {
        accbb.b(str, "vendorSlug");
        OguryChoiceManager.INSTANCE.initializeTcfV1$consent_manager_prodRelease(null);
        return TcfV1.isAccepted(str);
    }

    public static final boolean isEditAvailable(Context context) {
        accbb.b(context, "context");
        OguryChoiceManager.INSTANCE.initializeTcfV1$consent_manager_prodRelease(context);
        return OguryChoiceManager.isEditAvailable();
    }

    public static final boolean isPurposeAccepted(int i) {
        if (i < 0 || 4 < i) {
            return false;
        }
        OguryChoiceManager.INSTANCE.initializeTcfV1$consent_manager_prodRelease(null);
        Purpose purpose = i == Purpose.INFORMATION.ordinal() ? Purpose.INFORMATION : i == Purpose.PERSONALISATION.ordinal() ? Purpose.PERSONALISATION : i == Purpose.AD.ordinal() ? Purpose.AD : i == Purpose.CONTENT.ordinal() ? Purpose.CONTENT : Purpose.MEASUREMENT;
        return TcfV1.isPurposeAccepted(a(purpose));
    }

    public static final boolean isPurposeAccepted(Purpose purpose) {
        accbb.b(purpose, "purpose");
        OguryChoiceManager.INSTANCE.initializeTcfV1$consent_manager_prodRelease(null);
        return TcfV1.isPurposeAccepted(a(purpose));
    }

    public static final boolean isSpecialFeatureAccepted(int i) {
        OguryChoiceManager.INSTANCE.initializeTcfV1$consent_manager_prodRelease(null);
        return TcfV1.isSpecialFeatureAccepted(i);
    }

    public final ConsentListener getConsentListener() {
        ConsentListener consentListener2 = consentListener;
        if (consentListener2 == null) {
            accbb.a("consentListener");
        }
        return consentListener2;
    }

    public final void setConsentListener(ConsentListener consentListener2) {
        accbb.b(consentListener2, "<set-?>");
        consentListener = consentListener2;
    }
}
