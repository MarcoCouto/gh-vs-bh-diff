package com.ogury.consent.manager;

import com.ogury.cm.OguryChoiceManager.Answer;
import com.ogury.cm.OguryConsentListener;
import com.ogury.cm.aaccb;
import com.ogury.cm.accbb;
import com.ogury.consent.manager.util.consent.ConsentException;
import com.ogury.core.OguryError;

public final class ConsentManager$getOguryConsentListener$1 implements OguryConsentListener {
    ConsentManager$getOguryConsentListener$1() {
    }

    public final void onComplete(Answer answer) {
        accbb.b(answer, "answer");
        ConsentManager.INSTANCE.getConsentListener().onComplete(ConsentManager.access$mapAnswer(ConsentManager.INSTANCE, answer));
    }

    public final void onError(OguryError oguryError) {
        accbb.b(oguryError, "error");
        ConsentListener consentListener = ConsentManager.INSTANCE.getConsentListener();
        aaccb aaccb = aaccb.a;
        String a = aaccb.a(oguryError);
        String message = oguryError.getMessage();
        if (message == null) {
            message = "";
        }
        consentListener.onError(new ConsentException(a, message));
    }
}
