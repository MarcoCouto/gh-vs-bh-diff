package com.ogury.consent.manager.util.consent;

import com.ogury.cm.accbb;

public final class ConsentException extends Exception {
    private String type;

    public ConsentException(String str, String str2) {
        accbb.b(str, "type");
        accbb.b(str2, "message");
        super(str2);
        this.type = str;
    }

    public final String getType() {
        return this.type;
    }

    public final void setType(String str) {
        accbb.b(str, "<set-?>");
        this.type = str;
    }
}
