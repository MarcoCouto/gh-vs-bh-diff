package com.ogury.consent.manager;

import android.content.Context;
import com.ogury.cm.OguryChoiceManagerExternal.TcfV1;
import com.ogury.cm.accbb;

public final class ConsentManagerExternal {
    public static final ConsentManagerExternal a = new ConsentManagerExternal();

    private ConsentManagerExternal() {
    }

    public static final void setConsent(Context context, String str, String str2, String[] strArr) {
        accbb.b(context, "context");
        accbb.b(str, "assetKey");
        accbb.b(str2, "iabString");
        TcfV1.setConsent(context, str, str2, strArr);
    }
}
