package com.appodeal.ads;

import android.support.annotation.Nullable;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.api.Stats.Builder;
import com.appodeal.ads.utils.Log;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

class ba extends m<az> {
    int d;
    Set<Integer> e = new HashSet();
    Set<Integer> f = new HashSet();
    Set<Integer> g = new HashSet();

    ba(@Nullable c cVar) {
        super(cVar);
    }

    public AdType T() {
        return AdType.Native;
    }

    /* access modifiers changed from: protected */
    public void a(Builder builder) {
        super.a(builder);
        builder.setCapacity(this.d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void l(az azVar) {
        String str;
        String str2;
        super.l(azVar);
        try {
            JSONObject jSONObject = new JSONObject();
            if (Native.b == NativeAdType.Auto) {
                str = "type";
                str2 = "auto";
            } else if (Native.b == NativeAdType.Video) {
                str = "type";
                str2 = "video";
            } else {
                if (Native.b == NativeAdType.NoVideo) {
                    str = "type";
                    str2 = "static";
                }
                a(jSONObject);
            }
            jSONObject.put(str, str2);
            a(jSONObject);
        } catch (JSONException e2) {
            Log.log(e2);
        }
    }
}
