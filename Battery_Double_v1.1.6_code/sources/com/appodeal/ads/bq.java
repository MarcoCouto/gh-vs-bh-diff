package com.appodeal.ads;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.security.NetworkSecurityPolicy;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;
import com.appodeal.ads.c.a;
import com.appodeal.ads.utils.EventsTracker;
import com.appodeal.ads.utils.EventsTracker.EventType;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.c;
import com.appodeal.ads.utils.k;
import com.appodeal.ads.utils.s;
import com.appodeal.ads.utils.w;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Flushable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.internal.http.StatusLine;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class bq {
    static final /* synthetic */ boolean a = (!bq.class.desiredAssertionStatus());
    @VisibleForTesting
    private static long b = 0;
    private static SimpleDateFormat c = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private static Boolean d = null;
    private static final char[] e = "0123456789abcdef".toCharArray();

    public static ComponentName a(Context context, Intent intent) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (queryIntentActivities.isEmpty()) {
            return null;
        }
        for (ResolveInfo resolveInfo : queryIntentActivities) {
            if (resolveInfo.activityInfo.packageName.equals("com.android.vending")) {
                return new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
            }
        }
        return new ComponentName(((ResolveInfo) queryIntentActivities.get(0)).activityInfo.packageName, ((ResolveInfo) queryIntentActivities.get(0)).activityInfo.name);
    }

    private static Bitmap a(Bitmap bitmap, float f) {
        Matrix matrix = new Matrix();
        matrix.postRotate(f);
        try {
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } finally {
            bitmap.recycle();
        }
    }

    public static BitmapDrawable a(Context context, String str, boolean z, int i) {
        return a(context, str, z, i, 0.0f);
    }

    public static BitmapDrawable a(Context context, String str, boolean z, int i, float f) {
        byte[] decode = Base64.decode(str, 0);
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(decode, 0, decode.length);
        if (f != 0.0f) {
            decodeByteArray = a(decodeByteArray, f);
        }
        BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), decodeByteArray);
        if (z) {
            bitmapDrawable.setTileModeXY(TileMode.MIRROR, TileMode.MIRROR);
        }
        if (i > 0) {
            bitmapDrawable.setAlpha(i);
        }
        return bitmapDrawable;
    }

    public static Object a(Class cls, String str) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        if (declaredField.isAccessible()) {
            return declaredField.get(null);
        }
        return null;
    }

    @SafeVarargs
    public static Object a(Object obj, Class<?> cls, String str, Pair<Class, Object>... pairArr) throws Exception {
        Object[] objArr;
        Class[] clsArr;
        if (pairArr != null) {
            clsArr = new Class[pairArr.length];
            objArr = new Object[pairArr.length];
            for (int i = 0; i < pairArr.length; i++) {
                clsArr[i] = (Class) pairArr[i].first;
                objArr[i] = pairArr[i].second;
            }
        } else {
            clsArr = null;
            objArr = null;
        }
        int i2 = 10;
        while (i2 > 0 && cls != null) {
            try {
                Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
                declaredMethod.setAccessible(true);
                return declaredMethod.invoke(obj, objArr);
            } catch (NoSuchMethodException unused) {
                cls = cls.getSuperclass();
                i2--;
            } catch (IllegalAccessException | InvocationTargetException unused2) {
            }
        }
        return null;
    }

    public static Object a(Object obj, String str) throws Exception {
        Class cls = obj.getClass();
        int i = 10;
        while (i > 0 && cls != null) {
            try {
                Field declaredField = cls.getDeclaredField(str);
                declaredField.setAccessible(true);
                return declaredField.get(obj);
            } catch (NoSuchFieldException unused) {
                cls = cls.getSuperclass();
                i--;
            } catch (IllegalAccessException unused2) {
            }
        }
        return null;
    }

    @SafeVarargs
    public static Object a(Object obj, String str, Pair<Class, Object>... pairArr) throws Exception {
        return a(obj, obj.getClass(), str, pairArr);
    }

    public static String a(int i) {
        StringBuilder sb = new StringBuilder();
        if ((i & 1) > 0) {
            sb.append("Interstitial");
        }
        if ((i & 2) > 0) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append("Video");
        }
        if ((i & 128) > 0) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append("Rewarded video");
        }
        if ((i & 92) > 0) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append("Banner");
        }
        if ((i & 256) > 0) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append("MREC");
        }
        if ((i & 512) > 0) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append("NativeAd");
        }
        return sb.toString();
    }

    public static String a(Context context, JSONArray jSONArray, Runnable runnable) {
        if (jSONArray != null) {
            try {
                if (jSONArray.length() != 0) {
                    String str = "";
                    boolean z = false;
                    for (int i = 0; i < jSONArray.length() && !z; i++) {
                        str = jSONArray.getString(i);
                        z = a(context, str, runnable);
                    }
                    return str;
                }
            } catch (Exception e2) {
                Log.log(e2);
                a(runnable);
                return "";
            }
        }
        a(runnable);
        return "";
    }

    public static String a(Uri uri, String str) {
        FileOutputStream fileOutputStream;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(Appodeal.f, uri);
        Bitmap frameAtTime = mediaMetadataRetriever.getFrameAtTime(Long.parseLong(mediaMetadataRetriever.extractMetadata(9)), 2);
        FileOutputStream fileOutputStream2 = null;
        if (frameAtTime != null) {
            try {
                File file = new File(d(Appodeal.e, str), g(uri.toString()));
                fileOutputStream = new FileOutputStream(file);
                try {
                    frameAtTime.compress(CompressFormat.JPEG, 100, fileOutputStream);
                    String absolutePath = file.getAbsolutePath();
                    a((Flushable) fileOutputStream);
                    a((Closeable) fileOutputStream);
                    return absolutePath;
                } catch (Exception e2) {
                    e = e2;
                    try {
                        Log.log(e);
                        a((Flushable) fileOutputStream);
                        a((Closeable) fileOutputStream);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        fileOutputStream2 = fileOutputStream;
                        a((Flushable) fileOutputStream2);
                        a((Closeable) fileOutputStream2);
                        throw th;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                fileOutputStream = null;
                Log.log(e);
                a((Flushable) fileOutputStream);
                a((Closeable) fileOutputStream);
                return null;
            } catch (Throwable th2) {
                th = th2;
                a((Flushable) fileOutputStream2);
                a((Closeable) fileOutputStream2);
                throw th;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x004b A[SYNTHETIC, Splitter:B:24:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0057 A[SYNTHETIC, Splitter:B:31:0x0057] */
    public static String a(InputStream inputStream) {
        BufferedReader bufferedReader;
        try {
            StringBuilder sb = new StringBuilder(inputStream.available());
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    sb.append(10);
                } catch (Exception e2) {
                    e = e2;
                    try {
                        Log.log(e);
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (Exception e3) {
                                Log.log(e3);
                            }
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (Exception e4) {
                                Log.log(e4);
                            }
                        }
                        throw th;
                    }
                }
            }
            if (sb.length() > 0) {
                sb.setLength(sb.length() - 1);
            }
            String sb2 = sb.toString();
            try {
                bufferedReader.close();
            } catch (Exception e5) {
                Log.log(e5);
            }
            return sb2;
        } catch (Exception e6) {
            e = e6;
            bufferedReader = null;
            Log.log(e);
            if (bufferedReader != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            if (bufferedReader != null) {
            }
            throw th;
        }
    }

    public static String a(@Nullable JSONObject jSONObject, @Nullable String str) {
        return a(jSONObject, str, (String) null);
    }

    public static String a(@Nullable JSONObject jSONObject, @Nullable String str, @Nullable String str2) {
        return (jSONObject == null || str == null || jSONObject.isNull(str)) ? str2 : jSONObject.optString(str, str2);
    }

    public static void a(int i, a aVar) {
        a(b(i), aVar.a(), aVar.b());
    }

    static void a(Activity activity, String str) {
        bl.a(activity).a().putString(ServerResponseWrapper.APP_KEY_FIELD, str).apply();
    }

    public static void a(final Context context, final String str, final String str2) {
        if (z.a(context) && x(context)) {
            a((Runnable) new Runnable() {
                public void run() {
                    new Builder(context).setTitle(str).setMessage(str2).setCancelable(true).setNegativeButton("OK", new OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    }).create().show();
                }
            });
            Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_SHOW_DIALOG, str2);
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
    }

    public static void a(File file) {
        if (file.isDirectory()) {
            for (File a2 : file.listFiles()) {
                a(a2);
            }
        }
        file.delete();
    }

    public static void a(Flushable flushable) {
        if (flushable != null) {
            try {
                flushable.flush();
            } catch (Exception e2) {
                Log.log(e2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0024 A[SYNTHETIC, Splitter:B:17:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x002f A[SYNTHETIC, Splitter:B:22:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    public static void a(OutputStream outputStream, String str) {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream);
            try {
                bufferedOutputStream2.write(str.getBytes("UTF-8"));
                bufferedOutputStream2.flush();
                try {
                    bufferedOutputStream2.close();
                } catch (Exception e2) {
                    Log.log(e2);
                }
            } catch (Exception e3) {
                e = e3;
                bufferedOutputStream = bufferedOutputStream2;
                try {
                    Log.log(e);
                    if (bufferedOutputStream == null) {
                    }
                } catch (Throwable th) {
                    th = th;
                    bufferedOutputStream2 = bufferedOutputStream;
                    if (bufferedOutputStream2 != null) {
                        try {
                            bufferedOutputStream2.close();
                        } catch (Exception e4) {
                            Log.log(e4);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (bufferedOutputStream2 != null) {
                }
                throw th;
            }
        } catch (Exception e5) {
            e = e5;
            Log.log(e);
            if (bufferedOutputStream == null) {
                bufferedOutputStream.close();
            }
        }
    }

    public static void a(Object obj, String str, Object obj2) throws Exception {
        Class cls = obj.getClass();
        for (int i = 10; i > 0 && cls != null; i--) {
            try {
                Field declaredField = cls.getDeclaredField(str);
                declaredField.setAccessible(true);
                declaredField.set(obj, obj2);
            } catch (NoSuchFieldException unused) {
                cls = cls.getSuperclass();
            } catch (IllegalAccessException unused2) {
                return;
            }
        }
    }

    public static void a(Runnable runnable) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            runnable.run();
        } else {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    public static void a(Runnable runnable, long j) {
        new Handler(Looper.getMainLooper()).postDelayed(runnable, j);
    }

    private static void a(String str, List<JSONObject> list, List<JSONObject> list2) {
        Locale locale;
        String str2;
        Object[] objArr;
        Locale locale2;
        String str3;
        Object[] objArr2;
        if (Appodeal.getLogLevel() != LogLevel.none) {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("%s waterfall:", new Object[]{c(str)}));
            if (list != null && !list.isEmpty()) {
                sb.append("\n  Precache:\n    ");
                for (JSONObject jSONObject : list) {
                    if (jSONObject.has("name")) {
                        locale2 = Locale.ENGLISH;
                        str3 = "%s (%s), eCPM: %.2f; ";
                        objArr2 = new Object[]{c(jSONObject.optString("name")), c(jSONObject.optString("status")), Double.valueOf(jSONObject.optDouble(RequestInfoKeys.APPODEAL_ECPM, Utils.DOUBLE_EPSILON))};
                    } else {
                        locale2 = Locale.ENGLISH;
                        str3 = "%s, eCPM: %.2f; ";
                        objArr2 = new Object[]{c(jSONObject.optString("status")), Double.valueOf(jSONObject.optDouble(RequestInfoKeys.APPODEAL_ECPM, Utils.DOUBLE_EPSILON))};
                    }
                    sb.append(String.format(locale2, str3, objArr2));
                }
            }
            if (list2 == null || list2.isEmpty()) {
                sb.append("\n  Ads: Empty");
            } else {
                sb.append("\n  Ads:");
                int i = 100;
                for (JSONObject jSONObject2 : list2) {
                    if (i >= 100) {
                        sb.append("\n    ");
                        i = 0;
                    }
                    if (jSONObject2.has("name")) {
                        locale = Locale.ENGLISH;
                        str2 = "%s (%s), eCPM: %.2f; ";
                        objArr = new Object[]{c(jSONObject2.optString("name")), c(jSONObject2.optString("status")), Double.valueOf(jSONObject2.optDouble(RequestInfoKeys.APPODEAL_ECPM, Utils.DOUBLE_EPSILON))};
                    } else {
                        locale = Locale.ENGLISH;
                        str2 = "%s, eCPM: %.2f; ";
                        objArr = new Object[]{c(jSONObject2.optString("status")), Double.valueOf(jSONObject2.optDouble(RequestInfoKeys.APPODEAL_ECPM, Utils.DOUBLE_EPSILON))};
                    }
                    String format = String.format(locale, str2, objArr);
                    sb.append(format);
                    i += format.length();
                }
            }
            Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_DUMP, sb.toString());
        }
    }

    public static void a(@Nullable final String str, Executor executor) {
        if (!TextUtils.isEmpty(str) && executor != null) {
            executor.execute(new Runnable() {
                /* JADX WARNING: Removed duplicated region for block: B:17:0x0043 A[SYNTHETIC, Splitter:B:17:0x0043] */
                /* JADX WARNING: Removed duplicated region for block: B:23:0x0056 A[SYNTHETIC, Splitter:B:23:0x0056] */
                /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
                public void run() {
                    HttpURLConnection httpURLConnection;
                    Throwable e;
                    try {
                        httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                        try {
                            httpURLConnection.setConnectTimeout(5000);
                            httpURLConnection.setInstanceFollowRedirects(true);
                            httpURLConnection.setRequestProperty("Connection", "close");
                            httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                            httpURLConnection.getResponseCode();
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.getInputStream().close();
                                    httpURLConnection.disconnect();
                                } catch (Exception e2) {
                                    Log.log(e2);
                                }
                            }
                        } catch (Exception e3) {
                            e = e3;
                            try {
                                Log.log(e);
                                if (httpURLConnection == null) {
                                }
                            } catch (Throwable th) {
                                th = th;
                                if (httpURLConnection != null) {
                                    try {
                                        httpURLConnection.getInputStream().close();
                                        httpURLConnection.disconnect();
                                    } catch (Exception e4) {
                                        Log.log(e4);
                                    }
                                }
                                throw th;
                            }
                        }
                    } catch (Exception e5) {
                        Throwable th2 = e5;
                        httpURLConnection = null;
                        e = th2;
                        Log.log(e);
                        if (httpURLConnection == null) {
                            httpURLConnection.getInputStream().close();
                            httpURLConnection.disconnect();
                        }
                    } catch (Throwable th3) {
                        Throwable th4 = th3;
                        httpURLConnection = null;
                        th = th4;
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                }
            });
        }
    }

    public static void a(final String str, Executor executor, final Runnable runnable) {
        if (!TextUtils.isEmpty(str) && executor != null) {
            executor.execute(new Runnable() {
                /* JADX WARNING: Removed duplicated region for block: B:23:0x0058 A[Catch:{ all -> 0x0072 }] */
                /* JADX WARNING: Removed duplicated region for block: B:26:0x0062 A[SYNTHETIC, Splitter:B:26:0x0062] */
                /* JADX WARNING: Removed duplicated region for block: B:32:0x0075 A[SYNTHETIC, Splitter:B:32:0x0075] */
                /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
                public void run() {
                    HttpURLConnection httpURLConnection;
                    Throwable e;
                    try {
                        httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                        try {
                            httpURLConnection.setConnectTimeout(5000);
                            httpURLConnection.setInstanceFollowRedirects(true);
                            httpURLConnection.setRequestProperty("Connection", "close");
                            httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                            if ((httpURLConnection.getResponseCode() >= 300 || httpURLConnection.getResponseCode() < 200) && runnable != null) {
                                runnable.run();
                            }
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.getInputStream().close();
                                    httpURLConnection.disconnect();
                                } catch (Exception e2) {
                                    Log.log(e2);
                                }
                            }
                        } catch (Exception e3) {
                            e = e3;
                            try {
                                if (runnable != null) {
                                }
                                Log.log(e);
                                if (httpURLConnection == null) {
                                }
                            } catch (Throwable th) {
                                th = th;
                                if (httpURLConnection != null) {
                                    try {
                                        httpURLConnection.getInputStream().close();
                                        httpURLConnection.disconnect();
                                    } catch (Exception e4) {
                                        Log.log(e4);
                                    }
                                }
                                throw th;
                            }
                        }
                    } catch (Exception e5) {
                        Throwable th2 = e5;
                        httpURLConnection = null;
                        e = th2;
                        if (runnable != null) {
                            runnable.run();
                        }
                        Log.log(e);
                        if (httpURLConnection == null) {
                            httpURLConnection.getInputStream().close();
                            httpURLConnection.disconnect();
                        }
                    } catch (Throwable th3) {
                        Throwable th4 = th3;
                        httpURLConnection = null;
                        th = th4;
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                }
            });
        }
    }

    public static boolean a() {
        if (a("android.support.multidex.MultiDex")) {
            return true;
        }
        return a("androidx.multidex.MultiDex");
    }

    static boolean a(Activity activity) {
        return bl.a(activity).b().contains(ServerResponseWrapper.APP_KEY_FIELD);
    }

    @SuppressLint({"MissingPermission"})
    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean a(Context context, int i, int i2) {
        try {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(i, new int[]{i2});
            boolean hasValue = obtainStyledAttributes.hasValue(0);
            obtainStyledAttributes.recycle();
            return hasValue;
        } catch (Exception e2) {
            Log.log(e2);
            return false;
        }
    }

    public static boolean a(Context context, String str) {
        try {
            context.getPackageManager().getApplicationInfo(str, 0);
            return true;
        } catch (NameNotFoundException unused) {
            return false;
        }
    }

    public static boolean a(final Context context, @Nullable String str, final Runnable runnable) {
        if (TextUtils.isEmpty(str)) {
            if (runnable != null) {
                a(runnable);
            }
            return false;
        } else if (a || str != null) {
            final String d2 = d(str);
            if (!w.i || !a(d2)) {
                if (runnable != null) {
                    a(runnable);
                }
                return f(context, d2);
            }
            s.a.execute(new Runnable() {
                public void run() {
                    bq.f(context, bq.b(d2));
                    if (runnable != null) {
                        bq.a(runnable);
                    }
                }
            });
            return true;
        } else {
            throw new AssertionError();
        }
    }

    public static boolean a(Rect rect, View view) {
        return rect.contains(c(view));
    }

    public static boolean a(View view) {
        return view.getAlpha() == 0.0f;
    }

    public static boolean a(@NonNull String str) {
        return str.startsWith("http://") || str.startsWith("https://");
    }

    public static boolean a(String... strArr) {
        try {
            for (String cls : strArr) {
                Class.forName(cls, false, Appodeal.class.getClassLoader());
            }
            return true;
        } catch (ClassNotFoundException | NoClassDefFoundError unused) {
            return false;
        }
    }

    private static byte[] a(String str, byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e2) {
            Log.log(e2);
            return null;
        }
    }

    public static byte[] a(byte[] bArr) {
        return a("MD5", bArr);
    }

    @SuppressLint({"MissingPermission"})
    public static ConnectionData b(Context context) {
        String str;
        String str2;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        String str3 = "unknown";
        boolean z = false;
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            str3 = activeNetworkInfo.getTypeName();
            str = activeNetworkInfo.getSubtypeName();
            switch (activeNetworkInfo.getType()) {
                case 0:
                    switch (activeNetworkInfo.getSubtype()) {
                        case 3:
                        case 5:
                        case 6:
                        case 8:
                        case 9:
                        case 10:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                            break;
                    }
                case 1:
                case 6:
                case 9:
                    z = true;
                    break;
            }
        } else {
            str = null;
        }
        if (str3 != null) {
            if (str3.equals("CELLULAR")) {
                str3 = "MOBILE";
            }
            str3 = str3.toLowerCase(Locale.ENGLISH);
        }
        if (str != null) {
            str2 = str.toLowerCase(Locale.ENGLISH);
            if (str2.isEmpty()) {
                str2 = null;
            }
        } else {
            str2 = str;
        }
        return new ConnectionData(str3, str2, z);
    }

    static String b(int i) {
        return (i & 1) > 0 ? "Interstitial" : (i & 2) > 0 ? "Video" : (i & 128) > 0 ? LogConstants.KEY_REWARDED_VIDEO : (i & 92) > 0 ? "Banner" : (i & 256) > 0 ? LogConstants.KEY_MREC : (i & 512) > 0 ? LogConstants.KEY_NATIVE : "Unknown";
    }

    /* JADX WARNING: Removed duplicated region for block: B:83:0x0105 A[SYNTHETIC, Splitter:B:83:0x0105] */
    static String b(String str) {
        HttpURLConnection httpURLConnection;
        Exception e2;
        try {
            URL url = new URL(str);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            try {
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.setConnectTimeout(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
                httpURLConnection.setReadTimeout(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
                switch (httpURLConnection.getResponseCode()) {
                    case 301:
                    case 302:
                    case 303:
                    case IronSourceConstants.OFFERWALL_OPENED /*305*/:
                    case StatusLine.HTTP_TEMP_REDIRECT /*307*/:
                        String headerField = httpURLConnection.getHeaderField("Location");
                        if (headerField == null) {
                            String url2 = url.toString();
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.getInputStream().close();
                                    httpURLConnection.disconnect();
                                } catch (Exception e3) {
                                    Log.log(e3);
                                }
                            }
                            return url2;
                        } else if (a(headerField)) {
                            String b2 = b(httpURLConnection.getHeaderField("Location"));
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.getInputStream().close();
                                    httpURLConnection.disconnect();
                                } catch (Exception e4) {
                                    Log.log(e4);
                                }
                            }
                            return b2;
                        } else if (new URI(headerField).getScheme() == null) {
                            try {
                                String url3 = new URL(url, headerField).toString();
                                if (url3.trim().length() > 0) {
                                    String b3 = b(url3);
                                    if (httpURLConnection != null) {
                                        try {
                                            httpURLConnection.getInputStream().close();
                                            httpURLConnection.disconnect();
                                        } catch (Exception e5) {
                                            Log.log(e5);
                                        }
                                    }
                                    return b3;
                                }
                                if (httpURLConnection != null) {
                                    try {
                                        httpURLConnection.getInputStream().close();
                                        httpURLConnection.disconnect();
                                    } catch (Exception e6) {
                                        Log.log(e6);
                                    }
                                }
                                return headerField;
                            } catch (Exception e7) {
                                Log.log(e7);
                                if (httpURLConnection != null) {
                                    try {
                                        httpURLConnection.getInputStream().close();
                                        httpURLConnection.disconnect();
                                    } catch (Exception e8) {
                                        Log.log(e8);
                                    }
                                }
                                return headerField;
                            }
                        } else {
                            if (httpURLConnection != null) {
                                try {
                                    httpURLConnection.getInputStream().close();
                                    httpURLConnection.disconnect();
                                } catch (Exception e9) {
                                    Log.log(e9);
                                }
                            }
                            return headerField;
                        }
                    default:
                        String url4 = url.toString();
                        if (httpURLConnection != null) {
                            try {
                                httpURLConnection.getInputStream().close();
                                httpURLConnection.disconnect();
                            } catch (Exception e10) {
                                Log.log(e10);
                            }
                        }
                        return url4;
                }
            } catch (Exception e11) {
                e2 = e11;
            }
            e2 = e11;
        } catch (Exception e12) {
            httpURLConnection = null;
            e2 = e12;
        } catch (Throwable th) {
            th = th;
            httpURLConnection = null;
            if (httpURLConnection != null) {
            }
            throw th;
        }
        try {
            Log.log(e2);
            if (httpURLConnection != null) {
                try {
                    httpURLConnection.getInputStream().close();
                    httpURLConnection.disconnect();
                } catch (Exception e13) {
                    Log.log(e13);
                }
            }
            return str;
        } catch (Throwable th2) {
            th = th2;
            if (httpURLConnection != null) {
                try {
                    httpURLConnection.getInputStream().close();
                    httpURLConnection.disconnect();
                } catch (Exception e14) {
                    Log.log(e14);
                }
            }
            throw th;
        }
    }

    static void b(Activity activity) {
        SharedPreferences b2 = bl.a(activity).b();
        String string = b2.getString("appodealVersion", null);
        if (string == null || !string.equals(Appodeal.getVersion())) {
            Editor edit = b2.edit();
            edit.putString("appodealVersion", Appodeal.getVersion());
            edit.apply();
            k.b(activity);
        }
    }

    public static boolean b() {
        try {
            for (String file : new String[]{"/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su"}) {
                if (new File(file).exists()) {
                    return true;
                }
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
        return false;
    }

    public static boolean b(Context context, int i, int i2) {
        try {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(i, new int[]{i2});
            boolean z = obtainStyledAttributes.getBoolean(0, false);
            obtainStyledAttributes.recycle();
            return z;
        } catch (Exception e2) {
            Log.log(e2);
            return false;
        }
    }

    static boolean b(Context context, String str) {
        try {
            if (k.b == null) {
                k.b = Arrays.asList(context.getResources().getAssets().list("dex"));
            }
            return k.b.contains(String.format("%s.dx", new Object[]{str}));
        } catch (Exception e2) {
            Log.log(e2);
            return false;
        }
    }

    public static boolean b(View view) {
        return view.getMeasuredHeight() > 0 && view.getMeasuredWidth() > 0;
    }

    static byte[] b(byte[] bArr) {
        return a("SHA-224", bArr);
    }

    public static Rect c(View view) {
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        return new Rect(iArr[0], iArr[1], view.getWidth() + iArr[0], view.getHeight() + iArr[1]);
    }

    @TargetApi(21)
    static String c() {
        return VERSION.SDK_INT < 21 ? Build.CPU_ABI : Build.SUPPORTED_ABIS[0];
    }

    public static String c(Context context) {
        String networkOperator = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getNetworkOperator();
        if (networkOperator == null || networkOperator.length() < 3) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(networkOperator.substring(0, 3));
        sb.append('-');
        sb.append(networkOperator.substring(3));
        return sb.toString();
    }

    public static String c(String str) {
        if (str == null) {
            return "";
        }
        Matcher matcher = Pattern.compile("_(.)").matcher(str);
        StringBuffer stringBuffer = new StringBuffer(str.length());
        while (matcher.find()) {
            matcher.appendReplacement(stringBuffer, matcher.group(1).toUpperCase(Locale.ENGLISH));
        }
        matcher.appendTail(stringBuffer);
        stringBuffer.setCharAt(0, Character.toUpperCase(str.charAt(0)));
        return stringBuffer.toString();
    }

    static String c(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2 = i * 2;
            cArr[i2] = e[(bArr[i] >> 4) & 15];
            cArr[i2 + 1] = e[bArr[i] & 15];
        }
        return new String(cArr);
    }

    static void c(Activity activity) {
        activity.startActivity(new Intent(activity, TestActivity.class));
    }

    public static void c(final Context context, final String str) {
        if (z.a(context) && x(context)) {
            a((Runnable) new Runnable() {
                public void run() {
                    Toast.makeText(context, str, 1).show();
                    Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_SHOW_TOAST, str);
                }
            });
        }
    }

    static int d(Activity activity) {
        int i = 0;
        if (f(activity)) {
            return 0;
        }
        int identifier = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            i = activity.getResources().getDimensionPixelSize(identifier);
        }
        return i;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(12:2|3|4|5|6|(2:9|7)|10|11|12|13|14|15) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0038 */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0051 A[SYNTHETIC, Splitter:B:24:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005d A[SYNTHETIC, Splitter:B:30:0x005d] */
    static long d() {
        RandomAccessFile randomAccessFile;
        Exception e2;
        if (b == 0) {
            try {
                randomAccessFile = new RandomAccessFile("/proc/meminfo", "r");
                try {
                    Matcher matcher = Pattern.compile("(\\d+)").matcher(randomAccessFile.readLine());
                    String str = "";
                    while (matcher.find()) {
                        str = matcher.group(1);
                    }
                    b = Long.parseLong(str) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
                    b = 0;
                    try {
                        randomAccessFile.close();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                } catch (Exception e4) {
                    e2 = e4;
                    try {
                        Log.log(e2);
                        e2.printStackTrace();
                        if (randomAccessFile != null) {
                            randomAccessFile.close();
                        }
                        return b;
                    } catch (Throwable th) {
                        th = th;
                        if (randomAccessFile != null) {
                        }
                        throw th;
                    }
                }
            } catch (Exception e5) {
                Exception exc = e5;
                randomAccessFile = null;
                e2 = exc;
                Log.log(e2);
                e2.printStackTrace();
                if (randomAccessFile != null) {
                }
                return b;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                randomAccessFile = null;
                th = th3;
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (Exception e6) {
                        e6.printStackTrace();
                    }
                }
                throw th;
            }
        }
        return b;
    }

    @SuppressLint({"MissingPermission"})
    public static Location d(Context context) {
        String str;
        String str2;
        String str3;
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        String bestProvider = locationManager.getBestProvider(new Criteria(), false);
        if (bestProvider != null) {
            try {
                return locationManager.getLastKnownLocation(bestProvider);
            } catch (SecurityException unused) {
                str = LogConstants.KEY_SDK;
                str3 = "Location";
                str2 = "failed to retrieve GPS location: permission not granted";
                Log.log(str, str3, str2);
                return null;
            } catch (IllegalArgumentException unused2) {
                str = LogConstants.KEY_SDK;
                str3 = "Location";
                str2 = "failed to retrieve GPS location: device has no GPS provider";
                Log.log(str, str3, str2);
                return null;
            }
        }
        return null;
    }

    @Nullable
    public static File d(Context context, String str) {
        File externalFilesDir = context.getExternalFilesDir(null);
        if (externalFilesDir != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(externalFilesDir.getPath());
            sb.append("/");
            sb.append(str);
            sb.append("/");
            File file = new File(sb.toString());
            if (file.exists() || file.mkdirs()) {
                return file;
            }
        }
        return null;
    }

    public static String d(@NonNull String str) {
        try {
            new URL(str);
            return str;
        } catch (MalformedURLException unused) {
            try {
                return URLDecoder.decode(str, "UTF-8");
            } catch (UnsupportedEncodingException | IllegalArgumentException unused2) {
                return str;
            }
        }
    }

    public static void d(View view) {
        if (view.getParent() != null && (view.getParent() instanceof ViewGroup)) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }

    @TargetApi(18)
    static long e() {
        long blockCountLong;
        long availableBlocksLong;
        long blockSizeLong;
        if (VERSION.SDK_INT < 18) {
            StatFs statFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());
            blockCountLong = ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
            availableBlocksLong = (long) statFs.getAvailableBlocks();
            blockSizeLong = (long) statFs.getBlockSize();
        } else {
            StatFs statFs2 = new StatFs(Environment.getRootDirectory().getAbsolutePath());
            blockCountLong = statFs2.getBlockCountLong() * statFs2.getBlockSizeLong();
            availableBlocksLong = statFs2.getAvailableBlocksLong();
            blockSizeLong = statFs2.getBlockSizeLong();
        }
        return (blockCountLong - (availableBlocksLong * blockSizeLong)) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    @SuppressLint({"NewApi"})
    public static Pair<Integer, Integer> e(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        return new Pair<>(Integer.valueOf(point.x), Integer.valueOf(point.y));
    }

    public static void e(@Nullable String str) {
        a(str, (Executor) s.a);
    }

    static boolean e(Activity activity) {
        return (activity == null || activity.getWindow() == null || !activity.getWindow().isActive() || activity.getWindow().getDecorView().getWindowToken() == null) ? false : true;
    }

    @SuppressLint({"NewApi"})
    public static float f(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Point point = new Point();
        defaultDisplay.getSize(point);
        return ((float) point.x) / displayMetrics.density;
    }

    @TargetApi(18)
    static long f() {
        long blockCountLong;
        long blockSizeLong;
        if (VERSION.SDK_INT < 18) {
            StatFs statFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());
            blockCountLong = (long) statFs.getBlockCount();
            blockSizeLong = (long) statFs.getBlockSize();
        } else {
            StatFs statFs2 = new StatFs(Environment.getRootDirectory().getAbsolutePath());
            blockCountLong = statFs2.getBlockCountLong();
            blockSizeLong = statFs2.getBlockSizeLong();
        }
        return (blockCountLong * blockSizeLong) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    static URL f(String str) throws MalformedURLException {
        return (str.equals("banner") || str.equals("debug")) ? new URL("http://herokuapp.appodeal.com/android_waterfall_interstitial") : (str.equals("banner_320") || str.equals("debug_banner_320")) ? new URL("http://herokuapp.appodeal.com/android_waterfall_banner") : (str.equals("banner_mrec") || str.equals("debug_mrec")) ? new URL("http://herokuapp.appodeal.com/android_waterfall_mrec") : (str.equals("native") || str.equals("debug_native")) ? new URL("http://herokuapp.appodeal.com/android_waterfall_native") : (str.equals("video") || str.equals("debug_video")) ? new URL("http://herokuapp.appodeal.com/android_waterfall_video") : new URL("http://herokuapp.appodeal.com/android_waterfall_rewarded_video");
    }

    private static boolean f(Activity activity) {
        return (activity.getWindow().getAttributes().flags & 1024) == 1024;
    }

    /* access modifiers changed from: private */
    public static boolean f(Context context, String str) {
        try {
            Log.log(LogConstants.KEY_SDK, "Launch", String.format("url - %s", new Object[]{str}), LogLevel.verbose);
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.setFlags(268435456);
            ComponentName a2 = a(context, intent);
            if (a2 != null) {
                intent.setComponent(a2);
                context.startActivity(intent);
                return true;
            }
            String decode = URLDecoder.decode(str, "UTF-8");
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(decode));
            intent2.setFlags(268435456);
            ComponentName a3 = a(context, intent2);
            if (a3 != null) {
                intent2.setComponent(a3);
                context.startActivity(intent2);
                return true;
            }
            Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_LAUNCH_ERROR, String.format("no activities found to handle intent: %s", new Object[]{decode}));
            return false;
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    @SuppressLint({"NewApi"})
    public static float g(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Point point = new Point();
        defaultDisplay.getSize(point);
        return ((float) point.y) / displayMetrics.density;
    }

    public static String g(String str) {
        return new BigInteger(a(str.getBytes())).abs().toString(36);
    }

    public static boolean g() {
        if (d != null) {
            return d.booleanValue();
        }
        try {
            String[] strArr = Appodeal.e.getPackageManager().getPackageInfo(Appodeal.e.getPackageName(), 1024).applicationInfo.sharedLibraryFiles;
            if (strArr != null) {
                int length = strArr.length;
                int i = 0;
                while (i < length) {
                    String str = strArr[i];
                    if (!str.equalsIgnoreCase("/system/framework/org.apache.http.legacy.boot.jar")) {
                        if (!str.equalsIgnoreCase("/system/framework/org.apache.http.legacy.jar")) {
                            i++;
                        }
                    }
                    d = Boolean.valueOf(true);
                    return true;
                }
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
        d = Boolean.valueOf(false);
        return false;
    }

    public static float h(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        return displayMetrics.density;
    }

    static boolean h() {
        return VERSION.SDK_INT < 23 || NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
    }

    public static float i(Context context) {
        float f = -1.0f;
        try {
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver != null) {
                int intExtra = registerReceiver.getIntExtra("level", -1);
                int intExtra2 = registerReceiver.getIntExtra("scale", -1);
                if (!(intExtra == -1 || intExtra2 == -1)) {
                    f = (((float) intExtra) / ((float) intExtra2)) * 100.0f;
                }
                return f;
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
        return -1.0f;
    }

    static boolean i() {
        return l() || Build.FINGERPRINT.startsWith(MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE) || Build.FINGERPRINT.startsWith("unknown") || Build.MODEL.contains(CommonUtils.GOOGLE_SDK) || Build.MODEL.contains("Emulator") || Build.MODEL.contains("Android SDK built for x86") || Build.MANUFACTURER.contains("Genymotion") || (Build.BRAND.startsWith(MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE) && Build.DEVICE.startsWith(MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE)) || CommonUtils.GOOGLE_SDK.equals(Build.PRODUCT);
    }

    public static String j(Context context) {
        SharedPreferences b2 = bl.a(context).b();
        if (b2.contains("uuid")) {
            return b2.getString("uuid", null);
        }
        String uuid = UUID.randomUUID().toString();
        Editor edit = b2.edit();
        edit.putString("uuid", uuid);
        edit.apply();
        return uuid;
    }

    public static boolean j() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static boolean k() {
        String externalStorageState = Environment.getExternalStorageState();
        return "mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState);
    }

    public static boolean k(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        double d2 = (double) (((float) displayMetrics.widthPixels) / displayMetrics.xdpi);
        double d3 = (double) (((float) displayMetrics.heightPixels) / displayMetrics.ydpi);
        Double.isNaN(d2);
        Double.isNaN(d2);
        double d4 = d2 * d2;
        Double.isNaN(d3);
        Double.isNaN(d3);
        return Double.valueOf(Math.sqrt(d4 + (d3 * d3))).doubleValue() >= 6.6d;
    }

    static int l(Context context) {
        FeatureInfo[] systemAvailableFeatures = context.getPackageManager().getSystemAvailableFeatures();
        if (systemAvailableFeatures != null && systemAvailableFeatures.length > 0) {
            int length = systemAvailableFeatures.length;
            int i = 0;
            while (i < length) {
                FeatureInfo featureInfo = systemAvailableFeatures[i];
                if (featureInfo.name != null) {
                    i++;
                } else if (featureInfo.reqGlEsVersion != 0) {
                    return (featureInfo.reqGlEsVersion & SupportMenu.CATEGORY_MASK) >> 16;
                } else {
                    return 1;
                }
            }
        }
        return 1;
    }

    private static boolean l() {
        try {
            Object a2 = a(Build.class, "IS_EMULATOR");
            if (a2 instanceof Boolean) {
                return ((Boolean) a2).booleanValue();
            }
        } catch (Throwable unused) {
        }
        return false;
    }

    static long m(Context context) {
        MemoryInfo memoryInfo = new MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        return d() - (memoryInfo.availMem / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED);
    }

    public static String n(Context context) {
        switch (context.getResources().getConfiguration().orientation) {
            case 0:
                return "Unknown";
            case 1:
                return "Portrait";
            case 2:
                return "Landscape";
            default:
                return null;
        }
    }

    static boolean o(Context context) {
        switch (((AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO)).getRingerMode()) {
            case 0:
            case 1:
                return true;
            default:
                return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0027 A[Catch:{ Throwable -> 0x0042 }] */
    public static String p(Context context) {
        int i;
        try {
            if (a("com.google.android.gms.common.GoogleApiAvailabilityLight")) {
                Object a2 = a(Class.forName("com.google.android.gms.common.GoogleApiAvailabilityLight"), "GOOGLE_PLAY_SERVICES_VERSION_CODE");
                if (a2 != null) {
                    i = ((Integer) a2).intValue();
                    if (i == -1) {
                        i = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getInt("com.google.android.gms.version");
                    }
                    return String.valueOf(i);
                }
            }
            i = -1;
            if (i == -1) {
            }
            return String.valueOf(i);
        } catch (Throwable th) {
            Log.log(th);
            return "not-found";
        }
    }

    public static String q(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.google.android.webview", 0);
            if (packageInfo != null) {
                return packageInfo.versionName;
            }
        } catch (Exception unused) {
        }
        return null;
    }

    @Deprecated
    public static UserData r(Context context) {
        return bp.a();
    }

    @SuppressLint({"MissingPermission"})
    public static int s(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return 0;
        }
        int type = activeNetworkInfo.getType();
        if (type == 9) {
            return 1;
        }
        switch (type) {
            case 0:
                return 3;
            case 1:
                return 2;
            default:
                return 0;
        }
    }

    public static JSONObject t(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("battery", (double) i(context));
            jSONObject.put("rooted", b());
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public static JSONObject u(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            w session = Appodeal.getSession();
            jSONObject.put(TapjoyConstants.TJC_SESSION_ID, session.d(context));
            jSONObject.put("session_uptime", session.c());
            jSONObject.put("app_uptime", session.e(context));
            jSONObject.put("sdk", "2.6.4");
            EventsTracker eventsTracker = EventsTracker.get();
            jSONObject.put("imp_count", eventsTracker.a(EventType.Impression));
            jSONObject.put("click_count", eventsTracker.a(EventType.Click));
            jSONObject.put("finish_count", eventsTracker.a(EventType.Finish));
            jSONObject.put("imp", eventsTracker.b(EventType.Impression));
            jSONObject.put(String.CLICK, eventsTracker.b(EventType.Click));
            jSONObject.put("finish", eventsTracker.b(EventType.Finish));
            jSONObject.put("timp", eventsTracker.a(context, EventType.Impression));
            jSONObject.put("tclick", eventsTracker.a(context, EventType.Click));
            jSONObject.put("tfinish", eventsTracker.a(context, EventType.Finish));
            if (w.g != null) {
                Iterator keys = w.g.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    jSONObject.put(str, w.g.get(str));
                }
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public static boolean v(Context context) {
        return (VERSION.SDK_INT >= 19 || c.c(context)) && j();
    }

    public static boolean w(@NonNull Context context) {
        boolean z = true;
        try {
            if (a("com.google.android.gms.common.GoogleApiAvailabilityLight")) {
                Object a2 = a((Object) null, Class.forName("com.google.android.gms.common.GoogleApiAvailabilityLight"), "getInstance", (Pair<Class, Object>[]) new Pair[0]);
                if (a2 == null) {
                    return false;
                }
                Object a3 = a(a2, "isGooglePlayServicesAvailable", (Pair<Class, Object>[]) new Pair[]{new Pair(Context.class, context)});
                if (a3 instanceof Integer) {
                    if (((Integer) a3).intValue() != 0) {
                        z = false;
                    }
                    return z;
                }
            }
        } catch (Throwable th) {
            Log.log(th);
        }
        return false;
    }

    public static boolean x(Context context) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(context.getPackageName());
            sb.append(".BuildConfig");
            Object a2 = a(Class.forName(sb.toString()), "DEBUG");
            if (a2 instanceof Boolean) {
                return ((Boolean) a2).booleanValue();
            }
        } catch (Throwable th) {
            Log.log(th);
        }
        return true;
    }
}
