package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.i;
import com.appodeal.ads.i.a;
import com.appodeal.ads.m;
import com.appodeal.ads.utils.Log;
import org.json.JSONException;

abstract class j<AdRequestType extends m, AdObjectType extends i> implements Runnable {
    /* access modifiers changed from: private */
    public AdRequestType a;
    /* access modifiers changed from: private */
    public AdObjectType b;
    /* access modifiers changed from: private */
    public int c;

    j(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        this.c = 1;
        this.a = adrequesttype;
        this.b = adobjecttype;
    }

    j(AdRequestType adrequesttype, AdObjectType adobjecttype, int i) {
        this(adrequesttype, adobjecttype);
        this.c = i;
    }

    /* access modifiers changed from: private */
    public void a(Throwable th) {
        Log.log(th);
        a(th instanceof JSONException ? LoadingError.IncorrectAdunit : LoadingError.InternalError);
    }

    /* access modifiers changed from: protected */
    public void a() {
        bq.a((Runnable) new Runnable() {
            public void run() {
                try {
                    j.this.b.a(Appodeal.e, j.this.a, j.this.c, (a<AdRequestType>) new a() {
                        public void a(@Nullable m mVar, @Nullable LoadingError loadingError) {
                            j.this.a(loadingError);
                        }

                        public void a(@Nullable m mVar, @NonNull Throwable th) {
                            j.this.a(th);
                        }
                    });
                } catch (Throwable th) {
                    j.this.a(th);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(@Nullable LoadingError loadingError);

    /* access modifiers changed from: 0000 */
    public abstract void b();

    public void run() {
        try {
            b();
            a();
        } catch (Exception e) {
            a((Throwable) e);
        }
    }
}
