package com.appodeal.ads;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.Native.MediaAssetType;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.m;
import com.appodeal.ads.utils.n;
import com.appodeal.ads.utils.s;
import com.explorestack.iab.vast.VastRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class az extends i<ba, UnifiedNative, UnifiedNativeParams, UnifiedNativeCallback> {
    @Nullable
    List<NativeAd> e;
    @VisibleForTesting
    int f = 0;
    @VisibleForTesting
    boolean g = false;

    private final class a extends UnifiedNativeCallback {
        private a() {
        }

        @Nullable
        private ax a(int i) {
            if (az.this.e == null || az.this.e.size() == 0) {
                return null;
            }
            for (NativeAd nativeAd : az.this.e) {
                if (nativeAd instanceof ax) {
                    ax axVar = (ax) nativeAd;
                    if (i == axVar.l()) {
                        return axVar;
                    }
                }
            }
            return (ax) az.this.e.get(0);
        }

        public void onAdClicked() {
            Native.b().a(az.this.a(), az.this, a(-1), (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(int i, @Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            Native.b().a(az.this.a(), az.this, a(i), unifiedAdCallbackClickTrackListener);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            onAdClicked(-1, unifiedAdCallbackClickTrackListener);
        }

        public void onAdExpired() {
            Native.b().i(az.this.a(), az.this);
        }

        public void onAdFinished(int i) {
            Native.b().a(az.this.a(), az.this, a(i));
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            az.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            Native.b().b(az.this.a(), az.this, loadingError);
        }

        public void onAdLoaded(@NonNull UnifiedNativeAd unifiedNativeAd) {
            UnifiedNativeCallback unifiedNativeCallback = (UnifiedNativeCallback) az.this.p();
            if (az.this.e == null || unifiedNativeCallback == null) {
                onAdLoadFailed(LoadingError.InternalError);
                return;
            }
            az.this.e.add(new ax(az.this, unifiedNativeAd, unifiedNativeCallback));
            az.this.u();
        }

        public void onAdShowFailed() {
            Native.b().a(az.this.a(), az.this, a(-1), LoadingError.ShowFailed);
        }

        public void onAdShown(int i) {
            Native.b().e(az.this.a(), az.this, a(i));
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((ba) az.this.a()).a((AdUnit) az.this, str, obj);
        }
    }

    private final class b implements UnifiedNativeParams {
        private int b;

        b(int i) {
            this.b = i;
        }

        public int getAdCountToLoad() {
            return this.b;
        }

        public MediaAssetType getMediaAssetType() {
            return Native.c;
        }

        public NativeAdType getNativeAdType() {
            return Native.b;
        }

        public String obtainPlacementId() {
            return Native.a().u();
        }

        public String obtainSegmentId() {
            return Native.a().s();
        }
    }

    az(@NonNull ba baVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
        super(baVar, adNetwork, bnVar, 5000);
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull Object obj, @NonNull UnifiedNativeCallback unifiedNativeCallback, @NonNull UnifiedNative unifiedNative) throws Exception {
        this.e = new ArrayList(unifiedNativeParams.getAdCountToLoad());
        super.a(activity, unifiedNativeParams, obj, unifiedNativeCallback, unifiedNative);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(ax axVar) {
        String c = axVar.c();
        String e2 = axVar.e();
        if (axVar.containsVideo() && TextUtils.isEmpty(e2) && Native.d != null) {
            axVar.b(Native.d);
            e2 = Native.d;
        }
        String g2 = axVar.g();
        String h = axVar.h();
        if (Native.c != MediaAssetType.IMAGE) {
            this.f++;
        }
        if (Native.c != MediaAssetType.ICON) {
            this.f++;
        }
        if (Native.c != MediaAssetType.IMAGE) {
            a(axVar, c);
        }
        if (Native.c != MediaAssetType.ICON) {
            b(axVar, e2);
            if (Native.b != NativeAdType.Video) {
                return;
            }
            if (g2 != null && !g2.isEmpty()) {
                this.f++;
                c(axVar, g2);
            } else if (h != null && !h.isEmpty()) {
                this.f++;
                d(axVar, h);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(final ax axVar, String str) {
        if (str == null || str.isEmpty()) {
            this.f--;
        } else {
            a((Runnable) new com.appodeal.ads.utils.l.a(Appodeal.f, str).a((com.appodeal.ads.utils.l.b) new com.appodeal.ads.utils.l.b() {
                public void a() {
                    az.this.f--;
                    az.this.v();
                }

                public void a(Bitmap bitmap) {
                    axVar.a(bitmap);
                    az.this.f--;
                    az.this.v();
                }

                public void a(String str) {
                    axVar.a(str);
                    az.this.f--;
                    az.this.v();
                }
            }).a());
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(Runnable runnable) {
        s.a.execute(runnable);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedNative a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createNativeAd();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void b(final ax axVar, String str) {
        if (str == null || str.isEmpty()) {
            this.f--;
        } else {
            a((Runnable) new com.appodeal.ads.utils.l.a(Appodeal.f, str).a(true).a((com.appodeal.ads.utils.l.b) new com.appodeal.ads.utils.l.b() {
                public void a() {
                    az.this.f--;
                    az.this.v();
                }

                public void a(Bitmap bitmap) {
                    axVar.b(bitmap);
                    az.this.f--;
                    az.this.v();
                }

                public void a(String str) {
                    axVar.b(str);
                    az.this.f--;
                    az.this.v();
                }
            }).a());
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean b(ax axVar) {
        if (axVar == null) {
            return false;
        }
        try {
            return !TextUtils.isEmpty(axVar.getTitle()) && !TextUtils.isEmpty(axVar.getDescription()) && c(axVar) && d(axVar) && e(axVar);
        } catch (Exception e2) {
            Log.log(e2);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedNativeParams b(int i) {
        return new b(i);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void c(final ax axVar, String str) {
        if (str == null || str.isEmpty()) {
            this.f--;
        } else {
            a((Runnable) new m(Appodeal.f, new com.appodeal.ads.utils.m.a() {
                public void a() {
                    az.this.f--;
                    az.this.v();
                }

                public void a(Uri uri) {
                    axVar.a(uri);
                    if (TextUtils.isEmpty(axVar.e()) && uri != null && new File(uri.getPath()).exists()) {
                        axVar.b(bq.a(uri, "native_cache_image"));
                    }
                    az.this.f--;
                    az.this.v();
                }
            }, str));
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean c(ax axVar) {
        return Native.c == MediaAssetType.IMAGE || !TextUtils.isEmpty(axVar.c()) || axVar.d() != null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void d(final ax axVar, String str) {
        a((Runnable) new n(Appodeal.f, new com.appodeal.ads.utils.n.a() {
            public void a() {
                az.this.f--;
                az.this.v();
            }

            public void a(Uri uri, VastRequest vastRequest) {
                axVar.a(vastRequest);
                axVar.a(uri);
                if (TextUtils.isEmpty(axVar.e()) && uri != null && new File(uri.getPath()).exists()) {
                    axVar.b(bq.a(uri, "native_cache_image"));
                }
                az.this.f--;
                az.this.v();
            }
        }, str));
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean d(ax axVar) {
        return Native.c == MediaAssetType.ICON || !TextUtils.isEmpty(axVar.e()) || axVar.f() != null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean e(ax axVar) {
        if (Native.c == MediaAssetType.ICON || Native.b != NativeAdType.Video) {
            return true;
        }
        return axVar.k();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedNativeCallback o() {
        return new a();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void u() {
        if (this.e == null) {
            Native.b().g(a(), this);
            return;
        }
        for (NativeAd nativeAd : this.e) {
            a((ax) nativeAd);
        }
        this.g = true;
        v();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void v() {
        if (this.f == 0) {
            w();
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0073, code lost:
        return;
     */
    public synchronized void w() {
        if (this.e == null) {
            Native.b().g(a(), this);
        } else if (this.g) {
            Iterator it = this.e.iterator();
            int size = this.e.size();
            while (it.hasNext()) {
                NativeAd nativeAd = (NativeAd) it.next();
                if (!b((ax) nativeAd)) {
                    try {
                        it.remove();
                        nativeAd.destroy();
                    } catch (Exception e2) {
                        Log.log(e2);
                    }
                }
            }
            if (this.e.size() > 0) {
                Native.b().b(a(), this);
            } else if (size > 0) {
                Native.b().b(a(), this, LoadingError.InvalidAssets);
            } else {
                Native.b().g(a(), this);
            }
        }
    }

    @Nullable
    public List<NativeAd> x() {
        return this.e;
    }

    /* access modifiers changed from: 0000 */
    public int y() {
        if (this.e != null) {
            return this.e.size();
        }
        return 0;
    }
}
