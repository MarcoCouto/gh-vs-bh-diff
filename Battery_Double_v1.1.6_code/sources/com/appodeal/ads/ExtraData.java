package com.appodeal.ads;

import android.text.TextUtils;
import com.appodeal.ads.utils.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class ExtraData {
    public static final String APPSFLYER_ID = "appsflyer_id";
    private static JSONObject a = new JSONObject();

    static JSONObject a() {
        return a;
    }

    static void a(String str, Object obj) {
        if (!TextUtils.isEmpty(str) && obj != null) {
            try {
                a.put(str, obj);
            } catch (JSONException e) {
                Log.log(e);
            }
        }
    }
}
