package com.appodeal.ads;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.utils.Log;
import java.util.concurrent.CountDownLatch;

class bg implements RestrictedData {
    static final bg a = new bg();
    @VisibleForTesting
    static String b = null;

    private bg() {
    }

    @Nullable
    private static String a(@Nullable final Context context) {
        if (context == null) {
            return null;
        }
        if (b != null) {
            return b;
        }
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        bq.a((Runnable) new Runnable() {
            public void run() {
                String userAgentString;
                try {
                    if (VERSION.SDK_INT < 17) {
                        userAgentString = new WebView(context).getSettings().getUserAgentString();
                    } else {
                        bg.b = WebSettings.getDefaultUserAgent(context);
                        countDownLatch.countDown();
                    }
                } catch (IllegalArgumentException e) {
                    Log.log(e);
                    userAgentString = new WebView(context).getSettings().getUserAgentString();
                } catch (Throwable th) {
                    countDownLatch.countDown();
                    throw th;
                }
                bg.b = userAgentString;
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            Log.log(e);
        }
        return b;
    }

    public boolean canSendLocation() {
        return !isUserAgeRestricted() && !isParameterBlocked("lat") && !isParameterBlocked("lon");
    }

    public boolean canSendLocationType() {
        return !isUserAgeRestricted() && !isParameterBlocked("lt");
    }

    public boolean canSendUserSettings() {
        return !isUserAgeRestricted() && !isParameterBlocked("user_settings");
    }

    @Nullable
    public String getAddress() {
        if (canSendUserSettings()) {
            return bp.a().getAddress();
        }
        return null;
    }

    @Nullable
    public Integer getAge() {
        if (canSendUserSettings()) {
            return bp.a().getAge();
        }
        return null;
    }

    @Nullable
    public String getCity() {
        if (canSendUserSettings()) {
            return bp.a().getCity();
        }
        return null;
    }

    @Nullable
    public ConnectionData getConnectionData(@NonNull Context context) {
        return bq.b(context);
    }

    @Nullable
    public String getCountry() {
        if (canSendUserSettings()) {
            return bp.a().getCountryId();
        }
        return null;
    }

    @Nullable
    public Gender getGender() {
        if (canSendUserSettings()) {
            return bp.a().getGender();
        }
        return null;
    }

    @Nullable
    public String getHttpAgent(@NonNull Context context) {
        if (canSendUserSettings()) {
            return a(context);
        }
        return null;
    }

    @NonNull
    public String getIfa() {
        String h = be.h();
        return h != null ? h : "00000000-0000-0000-0000-000000000000";
    }

    @Nullable
    public String getIp() {
        if (canSendUserSettings()) {
            return bp.a().getIp();
        }
        return null;
    }

    @Nullable
    public String getIpv6() {
        if (canSendUserSettings()) {
            return bp.a().getIpv6();
        }
        return null;
    }

    @NonNull
    public LocationData getLocation(@NonNull Context context) {
        return new ar(context, this);
    }

    @Nullable
    public String getUserId() {
        return bp.a().getUserId();
    }

    @Nullable
    public String getZip() {
        if (canSendUserSettings()) {
            return bp.a().getZip();
        }
        return null;
    }

    public boolean isLimitAdTrackingEnabled() {
        return be.f();
    }

    public boolean isParameterBlocked(String str) {
        return isUserGdprProtected() && be.b(str);
    }

    public boolean isUserAgeRestricted() {
        return w.b();
    }

    public boolean isUserGdprProtected() {
        return be.g();
    }

    public boolean isUserHasConsent() {
        return be.c();
    }

    public boolean isUserInGdprScope() {
        return be.d();
    }
}
