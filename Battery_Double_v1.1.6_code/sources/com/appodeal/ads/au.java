package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;

class au extends c<aw, av, Object> {
    private MrecCallbacks a;

    au() {
    }

    /* access modifiers changed from: 0000 */
    public void a(MrecCallbacks mrecCallbacks) {
        this.a = mrecCallbacks;
    }

    public void a(@NonNull aw awVar, @NonNull av avVar) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_LOADED, String.format("isPrecache: %s", new Object[]{Boolean.valueOf(avVar.isPrecache())}), LogLevel.verbose);
        Appodeal.b();
        if (this.a != null) {
            this.a.onMrecLoaded(avVar.isPrecache());
        }
    }

    public void a(@Nullable aw awVar, @Nullable av avVar, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_LOAD_FAILED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onMrecFailedToLoad();
        }
    }

    public void a(@Nullable aw awVar, @Nullable av avVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_SHOWN, LogLevel.verbose);
        if (this.a != null) {
            this.a.onMrecShown();
        }
    }

    public void a(@Nullable aw awVar, @Nullable av avVar, @Nullable Object obj, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_SHOW_FAILED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onMrecShowFailed();
        }
    }

    public void b(@NonNull aw awVar, @NonNull av avVar) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_EXPIRED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onMrecExpired();
        }
    }

    public void b(@NonNull aw awVar, @NonNull av avVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_MREC, LogConstants.EVENT_NOTIFY_CLICKED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onMrecClicked();
        }
    }
}
