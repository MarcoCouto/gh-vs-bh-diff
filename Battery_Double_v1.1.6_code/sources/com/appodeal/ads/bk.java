package com.appodeal.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;

class bk extends c<bi, bh, Object> {
    @Nullable
    private RewardedVideoCallbacks a;
    @Nullable
    private NonSkippableVideoCallbacks b;

    bk() {
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable NonSkippableVideoCallbacks nonSkippableVideoCallbacks) {
        this.b = nonSkippableVideoCallbacks;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable RewardedVideoCallbacks rewardedVideoCallbacks) {
        this.a = rewardedVideoCallbacks;
    }

    public void a(@NonNull bi biVar, @NonNull bh bhVar) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_LOADED, String.format("isPrecache: %s", new Object[]{Boolean.valueOf(bhVar.isPrecache())}), LogLevel.verbose);
        Appodeal.b();
        if (this.a != null) {
            this.a.onRewardedVideoLoaded(bhVar.isPrecache());
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoLoaded(bhVar.isPrecache());
        }
    }

    public void a(@Nullable bi biVar, @Nullable bh bhVar, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_LOAD_FAILED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onRewardedVideoFailedToLoad();
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoFailedToLoad();
        }
    }

    public void a(@Nullable bi biVar, @Nullable bh bhVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_SHOWN, LogLevel.verbose);
        if (this.a != null) {
            this.a.onRewardedVideoShown();
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoShown();
        }
    }

    public void a(@Nullable bi biVar, @Nullable bh bhVar, @Nullable Object obj, @Nullable LoadingError loadingError) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_SHOW_FAILED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onRewardedVideoShowFailed();
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoShowFailed();
        }
    }

    /* renamed from: b */
    public void c(@NonNull bi biVar, @NonNull bh bhVar) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_CLOSED, String.format("finished: %s", new Object[]{Boolean.valueOf(biVar.n())}), LogLevel.verbose);
        if (this.a != null) {
            this.a.onRewardedVideoClosed(biVar.n());
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoClosed(biVar.n());
        }
    }

    public void b(@NonNull bi biVar, @NonNull bh bhVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_CLICKED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onRewardedVideoClicked();
        }
    }

    /* renamed from: c */
    public void b(@NonNull bi biVar, @NonNull bh bhVar) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_EXPIRED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onRewardedVideoExpired();
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoExpired();
        }
    }

    public void c(@NonNull bi biVar, @NonNull bh bhVar, @Nullable Object obj) {
        Log.log(LogConstants.KEY_REWARDED_VIDEO, LogConstants.EVENT_NOTIFY_FINISHED, LogLevel.verbose);
        if (this.a != null) {
            this.a.onRewardedVideoFinished(bj.c(), bj.d());
        }
        if (this.b != null) {
            this.b.onNonSkippableVideoFinished();
        }
    }
}
