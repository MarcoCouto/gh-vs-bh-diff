package com.appodeal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import com.appodeal.ads.UserSettings.Gender;
import com.appodeal.ads.b.d;
import com.appodeal.ads.b.e;
import com.appodeal.ads.b.g;
import com.appodeal.ads.b.i;
import com.appodeal.ads.m;
import com.appodeal.ads.utils.EventsTracker;
import com.appodeal.ads.utils.EventsTracker.EventType;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.ac;
import com.appodeal.ads.utils.s;
import com.appodeal.ads.utils.y;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.facebook.AccessToken;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.smaato.sdk.core.api.VideoType;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.TimeZone;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class aq<AdRequestType extends m> {
    @VisibleForTesting
    static JSONObject q;
    @VisibleForTesting
    String A;
    @VisibleForTesting
    double B;
    private final JSONObject C;
    /* access modifiers changed from: private */
    public final Handler D;
    private final int E;
    private final int F;
    private boolean G;
    @VisibleForTesting
    final a<AdRequestType> a;
    @VisibleForTesting
    final Context b;
    @VisibleForTesting
    final AdRequestType c;
    @VisibleForTesting
    final String d;
    @VisibleForTesting
    final String e;
    @VisibleForTesting
    String f;
    @VisibleForTesting
    final d g;
    @VisibleForTesting
    long h;
    @VisibleForTesting
    final String i;
    @VisibleForTesting
    Long j;
    @VisibleForTesting
    final int k;
    @VisibleForTesting
    final double l;
    @VisibleForTesting
    final double m;
    @VisibleForTesting
    final boolean n;
    @VisibleForTesting
    JSONObject o;
    @VisibleForTesting
    String p;
    @VisibleForTesting
    boolean r;
    @VisibleForTesting
    boolean s;
    @VisibleForTesting
    boolean t;
    @VisibleForTesting
    boolean u;
    @VisibleForTesting
    boolean v;
    @VisibleForTesting
    boolean w;
    @VisibleForTesting
    boolean x;
    @VisibleForTesting
    boolean y;
    @VisibleForTesting
    boolean z;

    public interface a<AdRequestType extends m> {
        void a(@Nullable AdRequestType adrequesttype);

        void a(JSONObject jSONObject, @Nullable AdRequestType adrequesttype, String str);
    }

    private class b implements Runnable {
        private b() {
        }

        public void run() {
            aq aqVar;
            String str;
            try {
                SharedPreferences b = bl.a(aq.this.b).b();
                boolean equals = aq.this.d.equals("init");
                JSONObject a2 = equals ? aq.this.a(b) : aq.this.b(b);
                if (a2 == null) {
                    aq.this.D.sendEmptyMessage(0);
                    return;
                }
                SharedPreferences b2 = bl.a(aq.this.b, "Appodeal").b();
                if (z.a == null) {
                    aqVar = aq.this;
                    str = ag.e();
                } else {
                    aqVar = aq.this;
                    str = z.a;
                }
                String a3 = aq.this.a(aqVar.a(str), a2, b2, false);
                if (equals) {
                    if (a3 == null) {
                        a3 = aq.this.a(aq.this.a(new Date()), a2, b2);
                    }
                } else if (aq.this.z) {
                    if (a3 != null) {
                        aq.this.a(b2, aq.this.d, a3);
                    } else if (b2.contains(aq.this.d) && aq.this.a(b2, aq.this.d)) {
                        Log.log(new com.appodeal.ads.utils.b.a("/get error, using saved waterfall"));
                        a3 = b2.getString(aq.this.d, "");
                    }
                }
                if (a3 == null) {
                    aq.this.D.sendEmptyMessage(0);
                    return;
                }
                try {
                    JSONObject jSONObject = new JSONObject(a3);
                    if (aq.this.z) {
                        Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_RESPONSE, a3, LogLevel.verbose);
                    } else {
                        Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_RESPONSE, a3);
                    }
                    try {
                        if (aq.this.z) {
                            bp.a(jSONObject.optJSONObject("user_data"));
                            w.b(jSONObject);
                            w.a(aq.this.b, jSONObject);
                            w.a(jSONObject);
                            aq.this.a(b2, jSONObject.optInt("wst", 86400000), aq.this.d);
                            aq.this.a(jSONObject);
                            e.a(jSONObject.optJSONArray("placements"));
                        } else {
                            if (aq.this.d.equals("init")) {
                                bp.a(jSONObject.optJSONObject("user_data"));
                                aq.this.a(jSONObject);
                                e.a(jSONObject.optJSONArray("placements"));
                            }
                            aq.this.D.sendMessage(aq.this.D.obtainMessage(1, jSONObject));
                        }
                        e.b();
                    } catch (Throwable th) {
                        Log.log(th);
                    }
                    aq.this.D.sendMessage(aq.this.D.obtainMessage(1, jSONObject));
                } catch (JSONException e) {
                    if (!aq.this.d.equals("stats")) {
                        Log.log(e);
                    }
                    aq.this.D.sendEmptyMessage(0);
                }
            } catch (Exception e2) {
                Log.log(e2);
                aq.this.D.sendEmptyMessage(0);
            }
        }
    }

    public static class c<AdRequestType extends m> {
        /* access modifiers changed from: private */
        public final Context a;
        /* access modifiers changed from: private */
        public final AdRequestType b;
        /* access modifiers changed from: private */
        public final String c;
        /* access modifiers changed from: private */
        public a<AdRequestType> d;
        /* access modifiers changed from: private */
        public String e;
        /* access modifiers changed from: private */
        public d f;
        /* access modifiers changed from: private */
        public String g;
        /* access modifiers changed from: private */
        public int h;
        /* access modifiers changed from: private */
        public double i;
        /* access modifiers changed from: private */
        public double j;
        /* access modifiers changed from: private */
        public boolean k;
        /* access modifiers changed from: private */
        public String l;
        /* access modifiers changed from: private */
        public double m;
        /* access modifiers changed from: private */
        public JSONObject n;

        public c(Context context, AdRequestType adrequesttype, String str) {
            this.h = 1;
            this.i = -1.0d;
            this.j = -1.0d;
            this.a = context;
            this.b = adrequesttype;
            this.c = str;
        }

        public c(Context context, String str) {
            this(context, null, str);
        }

        public c a(double d2) {
            this.i = d2;
            return this;
        }

        public c a(double d2, String str) {
            this.m = d2;
            this.l = str;
            return this;
        }

        public c a(a<AdRequestType> aVar) {
            this.d = aVar;
            return this;
        }

        public c a(d dVar) {
            this.f = dVar;
            return this;
        }

        public c a(String str) {
            this.e = str;
            return this;
        }

        public c a(boolean z) {
            this.k = z;
            return this;
        }

        public aq a() {
            return new aq(this);
        }

        public c b(double d2) {
            this.j = d2;
            return this;
        }

        public c b(String str) {
            this.g = str;
            return this;
        }
    }

    private aq(c<AdRequestType> cVar) {
        boolean z2 = false;
        this.E = 0;
        this.F = 1;
        this.a = cVar.d;
        this.b = cVar.a;
        this.c = cVar.b;
        this.d = cVar.c;
        this.e = cVar.e;
        this.g = cVar.f;
        this.i = cVar.g;
        this.k = cVar.h;
        this.l = cVar.i;
        this.m = cVar.j;
        this.n = cVar.k;
        this.A = cVar.l;
        this.B = cVar.m;
        this.C = cVar.n;
        if (cVar.b != null) {
            this.o = cVar.b.g();
            this.j = cVar.b.e();
            this.h = cVar.b.O();
            this.f = cVar.b.d();
            this.p = cVar.b.E();
        }
        this.D = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message message) {
                if (aq.this.a != null) {
                    switch (message.what) {
                        case 0:
                            break;
                        case 1:
                            JSONObject jSONObject = (JSONObject) message.obj;
                            if (jSONObject != null) {
                                aq.this.a.a(jSONObject, aq.this.c, aq.this.d);
                                return;
                            }
                            break;
                        default:
                            return;
                    }
                    aq.this.a.a(aq.this.c);
                }
            }
        };
        if (this.d != null) {
            if (!z.b || ((this.a != null && !(this.a instanceof ac)) || (!this.d.equals("init") && !this.d.equals("stats") && !this.d.equals("show") && !this.d.equals(String.CLICK) && !this.d.equals("finish") && !this.d.equals("install")))) {
                this.r = this.d.equals("banner") || this.d.equals("debug");
                this.s = this.d.equals("banner_320") || this.d.equals("debug_banner_320");
                this.t = this.d.equals("banner_mrec") || this.d.equals("debug_mrec");
                this.u = this.d.equals("video") || this.d.equals("debug_video");
                this.v = this.d.equals("rewarded_video") || this.d.equals("debug_rewarded_video");
                this.w = this.d.equals("native") || this.d.equals("debug_native");
                this.x = this.d.equals("debug") || this.d.equals("debug_banner_320") || this.d.equals("debug_video") || this.d.equals("debug_rewarded_video") || this.d.equals("debug_mrec") || this.d.equals("debug_native");
                this.y = this.r || this.s || this.t || this.u || this.v || this.w;
                if (!this.d.equals("init") && !this.d.equals("stats") && !this.d.equals("show") && !this.d.equals(String.CLICK) && !this.d.equals("finish") && !this.d.equals("install") && !this.d.equals("iap")) {
                    z2 = true;
                }
                this.z = z2;
                this.G = true;
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00bd, code lost:
        if ((r5 instanceof java.net.HttpURLConnection) != false) goto L_0x00bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00bf, code lost:
        ((java.net.HttpURLConnection) r5).disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00e4, code lost:
        if ((r5 instanceof java.net.HttpURLConnection) != false) goto L_0x00bf;
     */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00ea  */
    public String a(URL url, JSONObject jSONObject, SharedPreferences sharedPreferences, boolean z2) {
        URLConnection uRLConnection;
        int i2;
        String str;
        GZIPOutputStream gZIPOutputStream;
        String str2;
        InputStream inputStream;
        String str3 = null;
        try {
            uRLConnection = url.openConnection();
            try {
                if (!this.z || !sharedPreferences.contains(this.d)) {
                    i2 = 20000;
                    uRLConnection.setConnectTimeout(20000);
                } else {
                    i2 = 10000;
                    uRLConnection.setConnectTimeout(10000);
                }
                uRLConnection.setReadTimeout(i2);
                if (z2) {
                    str = UUID.randomUUID().toString();
                    uRLConnection.setRequestProperty("X-Request-ID", str);
                } else {
                    str = null;
                }
                uRLConnection.setRequestProperty("Content-Type", "text/plain; charset=UTF-8");
                uRLConnection.setDoOutput(true);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                gZIPOutputStream.write(jSONObject.toString().getBytes("UTF-8"));
                try {
                    gZIPOutputStream.close();
                } catch (Exception e2) {
                    Log.log(e2);
                }
                bq.a(uRLConnection.getOutputStream(), Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0));
                if (z2) {
                    String headerField = uRLConnection.getHeaderField("X-Signature");
                    if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(headerField)) {
                        if (a(str.getBytes(), Base64.decode(headerField, 0))) {
                            inputStream = uRLConnection.getInputStream();
                        }
                    }
                    str2 = null;
                    if (str2 != null && !str2.isEmpty() && !str2.equals(" ")) {
                        str3 = str2;
                    }
                    if (uRLConnection != null) {
                        if (!(uRLConnection instanceof HttpsURLConnection)) {
                        }
                        ((HttpsURLConnection) uRLConnection).disconnect();
                    }
                    return str3;
                }
                inputStream = uRLConnection.getInputStream();
                str2 = bq.a(inputStream);
                str3 = str2;
                if (uRLConnection != null) {
                }
            } catch (IOException e3) {
                e = e3;
                try {
                    Log.log(e);
                    if (uRLConnection != null) {
                    }
                    return str3;
                } catch (Throwable th) {
                    th = th;
                    if (uRLConnection != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                try {
                    gZIPOutputStream.close();
                } catch (Exception e4) {
                    Log.log(e4);
                }
                throw th2;
            }
        } catch (IOException e5) {
            e = e5;
            uRLConnection = null;
            Log.log(e);
            if (uRLConnection != null) {
                if (!(uRLConnection instanceof HttpsURLConnection)) {
                }
                ((HttpsURLConnection) uRLConnection).disconnect();
            }
            return str3;
        } catch (Throwable th3) {
            th = th3;
            uRLConnection = null;
            if (uRLConnection != null) {
                if (uRLConnection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) uRLConnection).disconnect();
                } else if (uRLConnection instanceof HttpURLConnection) {
                    ((HttpURLConnection) uRLConnection).disconnect();
                }
            }
            throw th;
        }
        return str3;
    }

    /* access modifiers changed from: private */
    public String a(Queue<String> queue, JSONObject jSONObject, SharedPreferences sharedPreferences) throws Exception {
        if (queue.isEmpty()) {
            return null;
        }
        String str = (String) queue.poll();
        String a2 = a(a(str), jSONObject, sharedPreferences, true);
        if (a2 == null) {
            return a(queue, jSONObject, sharedPreferences);
        }
        z.a = str;
        return a2;
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("segments");
        if (optJSONArray != null && optJSONArray.length() != 0) {
            i iVar = new i(this.b, jSONObject.optJSONObject("user_data"));
            iVar.b(optJSONArray);
            g a2 = iVar.a(optJSONArray);
            if (a2 == null) {
                i.c();
            } else if (a2.c() != i.a().c()) {
                try {
                    a2.a();
                } catch (JSONException e2) {
                    Log.log(e2);
                }
                i.a(a2);
            }
        }
    }

    private String c(String str) {
        return String.format("%s_timestamp", new Object[]{str});
    }

    @Nullable
    private JSONObject c(SharedPreferences sharedPreferences) throws Exception {
        JSONObject jSONObject;
        String str;
        String str2;
        synchronized (aq.class) {
            if (q == null || q.length() == 0) {
                q = new JSONObject();
                PackageManager packageManager = this.b.getPackageManager();
                String string = sharedPreferences.getString(ServerResponseWrapper.APP_KEY_FIELD, null);
                if (string == null) {
                    return null;
                }
                q.put("app_key", string);
                q.put("android", VERSION.RELEASE);
                q.put("android_level", VERSION.SDK_INT);
                q.put("os", Constants.JAVASCRIPT_INTERFACE_NAME);
                q.put(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
                q.put("sdk", "2.6.4");
                String packageName = this.b.getPackageName();
                q.put("package", packageName);
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
                    q.put("package_version", packageInfo.versionName);
                    q.put("package_code", packageInfo.versionCode);
                    q.put("install_time", packageInfo.firstInstallTime / 1000);
                    q.put("target_sdk_version", packageManager.getApplicationInfo(packageName, 0).targetSdkVersion);
                } catch (NameNotFoundException e2) {
                    Log.log(e2);
                }
                if (Appodeal.frameworkName != null) {
                    q.put("framework", Appodeal.frameworkName);
                }
                if (Appodeal.i != null) {
                    q.put("framework_version", Appodeal.i);
                }
                if (Appodeal.pluginVersion != null) {
                    q.put("plugin_version", Appodeal.pluginVersion);
                }
                q.put("pxratio", (double) bq.h(this.b));
                if (bq.k(this.b)) {
                    jSONObject = q;
                    str = TapjoyConstants.TJC_DEVICE_TYPE_NAME;
                    str2 = "tablet";
                } else {
                    jSONObject = q;
                    str = TapjoyConstants.TJC_DEVICE_TYPE_NAME;
                    str2 = PlaceFields.PHONE;
                }
                jSONObject.put(str, str2);
                q.put(TapjoyConstants.TJC_PLATFORM, com.appodeal.ads.utils.d.a);
                try {
                    String installerPackageName = packageManager.getInstallerPackageName(packageName);
                    if (installerPackageName == null) {
                        installerPackageName = "unknown";
                    }
                    q.put(TapjoyConstants.TJC_INSTALLER, installerPackageName);
                } catch (Exception e3) {
                    Log.log(e3);
                }
                q.put("manufacturer", Build.MANUFACTURER);
                q.put("rooted", bq.b());
                q.put("webview_version", bq.q(this.b));
                q.put("multidex", bq.a());
            }
            JSONObject jSONObject2 = new JSONObject();
            Iterator keys = q.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                jSONObject2.put(str3, q.get(str3));
            }
            Pair e4 = bq.e(this.b);
            jSONObject2.put("width", e4.first);
            jSONObject2.put("height", e4.second);
            return jSONObject2;
        }
    }

    private String d(String str) {
        return String.format("%s_wst", new Object[]{str});
    }

    /* access modifiers changed from: 0000 */
    public URL a(@NonNull String str) throws MalformedURLException {
        if (!this.z) {
            return new URL(String.format("%s/%s", new Object[]{str, this.d}));
        } else if (this.n) {
            return bq.f(this.d);
        } else {
            return new URL(String.format("%s/%s", new Object[]{str, "get"}));
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public Queue<String> a(Date date) {
        LinkedList linkedList = new LinkedList(z.d());
        String format = new SimpleDateFormat("yyyy", Locale.ENGLISH).format(date);
        String format2 = new SimpleDateFormat("yyyyMM", Locale.ENGLISH).format(date);
        String format3 = new SimpleDateFormat("yyyyMMww", Locale.ENGLISH).format(date);
        StringBuilder sb = new StringBuilder();
        sb.append("https://a.");
        sb.append(b(format));
        sb.append(".com");
        linkedList.add(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("https://a.");
        sb2.append(b(format2));
        sb2.append(".com");
        linkedList.add(sb2.toString());
        StringBuilder sb3 = new StringBuilder();
        sb3.append("https://a.");
        sb3.append(b(format3));
        sb3.append(".com");
        linkedList.add(sb3.toString());
        return linkedList;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public JSONObject a(SharedPreferences sharedPreferences) throws Exception {
        if (sharedPreferences == null) {
            return null;
        }
        String string = sharedPreferences.getString(ServerResponseWrapper.APP_KEY_FIELD, null);
        if (string == null) {
            return null;
        }
        bg bgVar = bg.a;
        String ifa = bgVar.getIfa();
        String str = bgVar.isLimitAdTrackingEnabled() ? "0" : "1";
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("app_key", string);
        jSONObject.put("sdk", "2.6.4");
        jSONObject.put("package", this.b.getPackageName());
        jSONObject.put("framework", Appodeal.frameworkName);
        if (Appodeal.i != null) {
            jSONObject.put("framework_version", Appodeal.i);
        }
        jSONObject.put("ifa", ifa);
        jSONObject.put("os", Constants.JAVASCRIPT_INTERFACE_NAME);
        jSONObject.put(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
        jSONObject.put("advertising_tracking", str);
        jSONObject.put(TapjoyConstants.TJC_PLATFORM, com.appodeal.ads.utils.d.a);
        jSONObject.put(RequestParameters.CONSENT, be.e());
        jSONObject.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, be.b());
        jSONObject.put("adidg", be.i());
        jSONObject.put("http_allowed", z.c());
        if (ExtraData.a().length() > 0) {
            jSONObject.put(RequestInfoKeys.EXT, ExtraData.a());
        }
        return jSONObject;
    }

    public void a() {
        if (this.G) {
            s.a.execute(new b());
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(SharedPreferences sharedPreferences, int i2, String str) {
        Editor edit = sharedPreferences.edit();
        edit.putInt(d(str), i2);
        edit.apply();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(SharedPreferences sharedPreferences, String str, String str2) {
        Editor edit = sharedPreferences.edit();
        edit.putString(str, str2);
        edit.putLong(c(str), System.currentTimeMillis());
        edit.apply();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(SharedPreferences sharedPreferences, String str) {
        if (System.currentTimeMillis() - sharedPreferences.getLong(c(str), 0) <= ((long) b(sharedPreferences, str))) {
            return true;
        }
        Editor edit = sharedPreferences.edit();
        edit.remove(str);
        edit.remove(c(str));
        edit.remove(d(str));
        edit.apply();
        return false;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(@NonNull byte[] bArr, @NonNull byte[] bArr2) {
        try {
            PublicKey generatePublic = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(Base64.decode("MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAENIBD8zVGWMJWVFPJ9aQkyZS+ahKDB9xbQZeXIb7keGfUEMdOaOxWd+nTa2HbkeHi0PNfdGHAyCE4mycvIPwStw==".getBytes(), 0)));
            Signature instance = Signature.getInstance("SHA256withECDSA");
            instance.initVerify(generatePublic);
            instance.update(bArr);
            return instance.verify(bArr2);
        } catch (Exception e2) {
            Log.log(e2);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int b(SharedPreferences sharedPreferences, String str) {
        return sharedPreferences.getInt(d(str), 86400000);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public String b(String str) {
        return bq.c(bq.b(str.getBytes()));
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        int a2 = EventsTracker.get().a(EventType.Impression);
        int a3 = EventsTracker.get().a(EventType.Click);
        int a4 = EventsTracker.get().a(EventType.Finish);
        try {
            jSONObject.put("show", a2);
            jSONObject.put(String.CLICK, a3);
            if (this.u || this.v || (this.i != null && (this.i.equals("video") || this.i.equals("rewarded_video")))) {
                jSONObject.put("finish", a4);
            }
            if (this.r || (this.i != null && this.i.equals("banner"))) {
                jSONObject.put(String.format("%s_%s", new Object[]{"banner", "show"}), EventsTracker.get().a(VideoType.INTERSTITIAL, EventType.Impression));
                jSONObject.put(String.format("%s_%s", new Object[]{"banner", String.CLICK}), EventsTracker.get().a(VideoType.INTERSTITIAL, EventType.Click));
            }
            if (this.u || (this.i != null && this.i.equals("video"))) {
                jSONObject.put(String.format("%s_%s", new Object[]{"video", "show"}), EventsTracker.get().a("video", EventType.Impression));
                jSONObject.put(String.format("%s_%s", new Object[]{"video", String.CLICK}), EventsTracker.get().a("video", EventType.Click));
                jSONObject.put(String.format("%s_%s", new Object[]{"video", "finish"}), EventsTracker.get().a("video", EventType.Finish));
            }
            if (this.v || (this.i != null && this.i.equals("rewarded_video"))) {
                jSONObject.put(String.format("%s_%s", new Object[]{"rewarded_video", "show"}), EventsTracker.get().a("rewarded_video", EventType.Impression));
                jSONObject.put(String.format("%s_%s", new Object[]{"rewarded_video", String.CLICK}), EventsTracker.get().a("rewarded_video", EventType.Click));
                jSONObject.put(String.format("%s_%s", new Object[]{"rewarded_video", "finish"}), EventsTracker.get().a("rewarded_video", EventType.Finish));
            }
            if (this.s || (this.i != null && this.i.equals("banner_320"))) {
                jSONObject.put(String.format("%s_%s", new Object[]{"banner_320", "show"}), EventsTracker.get().a("banner", EventType.Impression));
                jSONObject.put(String.format("%s_%s", new Object[]{"banner_320", String.CLICK}), EventsTracker.get().a("banner", EventType.Click));
            }
            if (this.t || (this.i != null && this.i.equals("banner_mrec"))) {
                jSONObject.put(String.format("%s_%s", new Object[]{"banner_mrec", "show"}), EventsTracker.get().a("mrec", EventType.Impression));
                jSONObject.put(String.format("%s_%s", new Object[]{"banner_mrec", String.CLICK}), EventsTracker.get().a("mrec", EventType.Click));
            }
            if (this.w || (this.i != null && this.i.equals("native"))) {
                jSONObject.put(String.format("%s_%s", new Object[]{"native", "show"}), EventsTracker.get().a("native", EventType.Impression));
                jSONObject.put(String.format("%s_%s", new Object[]{"native", String.CLICK}), EventsTracker.get().a("native", EventType.Click));
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
        return jSONObject;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public JSONObject b(SharedPreferences sharedPreferences) throws Exception {
        JSONObject c2 = c(sharedPreferences);
        Integer num = null;
        if (c2 == null) {
            return null;
        }
        bg bgVar = bg.a;
        String ifa = bgVar.getIfa();
        String str = bgVar.isLimitAdTrackingEnabled() ? "0" : "1";
        c2.put("ifa", ifa);
        c2.put("advertising_tracking", str);
        c2.put("adidg", be.i());
        try {
            if (this.z) {
                c2.put("http_allowed", z.c());
            }
            if (this.r) {
                c2.put("type", "banner");
            }
            if (this.s) {
                c2.put("type", "banner_320");
                if (aa.f()) {
                    c2.put("large_banners", true);
                }
            }
            if (this.t) {
                c2.put("type", "banner_mrec");
            }
            if (this.u || this.v) {
                c2.put("type", "video");
            }
            if (this.v) {
                c2.put("rewarded_video", true);
            }
            if (this.w) {
                c2.put("type", "native");
            }
            if (this.x) {
                c2.put("debug", true);
            }
            if (z.b) {
                c2.put("test", true);
            }
            c2.put("battery", (double) bq.i(this.b));
            c2.put("crr", bq.c(this.b));
            c2.put("locale", Locale.getDefault().toString());
            c2.put(TapjoyConstants.TJC_DEVICE_TIMEZONE, new SimpleDateFormat("Z", Locale.ENGLISH).format(Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.ENGLISH).getTime()));
            c2.put("local_time", System.currentTimeMillis() / 1000);
            c2.put("user_agent", bgVar.getHttpAgent(this.b));
            Appodeal.getSession().f(this.b);
            c2.put(TapjoyConstants.TJC_SESSION_ID, Appodeal.getSession().d(this.b));
            c2.put("session_uptime", Appodeal.getSession().c());
            c2.put("app_uptime", Appodeal.getSession().e(this.b));
            c2.put(RequestParameters.CONSENT, be.e());
            c2.put(IronSourceConstants.IRONSOURCE_BIDDING_TOKEN_KEY, be.a());
            ConnectionData connectionData = bgVar.getConnectionData(this.b);
            if (connectionData != null) {
                c2.put("connection", connectionData.type);
                c2.put(TapjoyConstants.TJC_CONNECTION_SUBTYPE, connectionData.subType);
                c2.put("connection_fast", connectionData.isFast);
            }
            LocationData location = bgVar.getLocation(this.b);
            c2.put("lt", location.getDeviceLocationType());
            c2.put("lat", location.obtainLatitude());
            c2.put("lon", location.obtainLongitude());
            c2.put("model", String.format("%s %s", new Object[]{Build.MANUFACTURER, Build.MODEL}));
            c2.put("coppa", w.b());
            c2.put("session_uuid", Appodeal.getSession().b());
            if (this.d.equals("iap")) {
                c2.put("currency", this.A);
                c2.put("amount", this.B);
            }
            if (this.y) {
                p a2 = this.r ? am.a() : null;
                if (this.u) {
                    a2 = bd.a();
                }
                if (this.v) {
                    a2 = bj.a();
                }
                if (this.s) {
                    a2 = aa.b();
                }
                if (this.t) {
                    a2 = at.a();
                }
                if (this.w) {
                    a2 = Native.a();
                }
                JSONArray jSONArray = new JSONArray();
                JSONObject jSONObject = new JSONObject();
                if (a2 != null) {
                    for (AdNetwork adNetwork : a2.q().b(this.b).a()) {
                        jSONArray.put(adNetwork.getName());
                        jSONObject.put(adNetwork.getName(), new JSONObject().put("ver", adNetwork.getAdapterVersion()).put("sdk", adNetwork.getVersion()));
                    }
                } else {
                    jSONArray = new JSONArray();
                }
                c2.put("show_array", jSONArray);
                c2.put("adapters", jSONObject);
            }
            if (this.j == null || this.j.longValue() == -1) {
                c2.put("segment_id", -1);
            } else {
                c2.put("segment_id", this.j);
            }
            if (this.h != 0) {
                c2.put("show_timestamp", this.h);
            }
            if (this.d.equals(String.CLICK)) {
                c2.put("click_timestamp", System.currentTimeMillis() / 1000);
            }
            if (this.d.equals("finish")) {
                c2.put("finish_timestamp", System.currentTimeMillis() / 1000);
            }
            if (this.k > 1) {
                c2.put("capacity", this.k);
            }
            if (this.l > Utils.DOUBLE_EPSILON) {
                c2.put("price_floor", this.l);
            }
            if (this.m > Utils.DOUBLE_EPSILON) {
                c2.put(RequestInfoKeys.APPODEAL_ECPM, this.m);
            }
            if (this.o != null) {
                c2.put("ad_properties", this.o);
            }
            if (this.p != null) {
                c2.put("impid", this.p);
            }
            c2.put("id", this.e);
            c2.put("main_id", this.f);
            if (this.y || this.i != null) {
                c2.put("ad_stats", b());
            }
            if (this.y && z.i == null) {
                c2.put("check_sdk_version", true);
            }
            if (this.g != null) {
                c2.put("placement_id", this.g.b());
            }
            if (this.C != null && this.d.equals("stats")) {
                c2.put("ad_unit_stat", this.C);
            }
            if (ExtraData.a().length() > 0) {
                c2.put(RequestInfoKeys.EXT, ExtraData.a());
            }
            c2.put(AccessToken.USER_ID_KEY, bgVar.getUserId());
        } catch (JSONException e2) {
            Log.log(e2);
        }
        if (this.y) {
            try {
                c2.put("sa", y.a(this.b));
            } catch (Exception e3) {
                Log.log(e3);
            }
            if (bgVar.canSendUserSettings()) {
                JSONObject jSONObject2 = new JSONObject();
                try {
                    Gender gender = bgVar.getGender();
                    String str2 = "gender";
                    if (gender != null) {
                        num = Integer.valueOf(gender.getIntValue());
                    }
                    jSONObject2.put(str2, num);
                    jSONObject2.put(IronSourceSegment.AGE, bgVar.getAge());
                } catch (JSONException e4) {
                    Log.log(e4);
                }
                c2.put("user_settings", jSONObject2);
            }
        }
        be.a(c2, (RestrictedData) bgVar);
        return c2;
    }
}
