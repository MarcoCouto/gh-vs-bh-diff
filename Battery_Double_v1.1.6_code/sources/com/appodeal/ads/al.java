package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.ak;
import com.appodeal.ads.b.d;
import com.appodeal.ads.m;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.x;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.concurrent.atomic.AtomicBoolean;

class al<AdRequestType extends m<AdObjectType>, AdObjectType extends ak> extends k<AdRequestType, AdObjectType, l> {
    @VisibleForTesting
    static final AtomicBoolean b = new AtomicBoolean(false);

    al(@NonNull String str) {
        super(str);
    }

    static void a() {
        synchronized (b) {
            b.set(false);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002e, code lost:
        r5 = super.a(r5, r6, r7);
        r6 = b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0034, code lost:
        monitor-enter(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        b.set(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        monitor-exit(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        if (r5 == false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        com.appodeal.ads.bq.a((java.lang.Runnable) new com.appodeal.ads.al.AnonymousClass1(r4), (long) com.google.android.exoplayer2.DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0047, code lost:
        return r5;
     */
    public boolean a(@NonNull Activity activity, @NonNull l lVar, @NonNull p<AdObjectType, AdRequestType, ?> pVar) {
        synchronized (b) {
            if (b.get()) {
                Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_SHOW_ERROR, String.format("Can't show %s. Fullscreen ad is already shown", new Object[]{bq.b(pVar.m())}));
                return false;
            }
            b.set(true);
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull Activity activity, @NonNull p<AdObjectType, AdRequestType, ?> pVar) {
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (c()) {
            AudioManager audioManager = (AudioManager) Appodeal.f.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            if (audioManager != null && z.d && audioManager.getStreamVolume(3) == 0 && z.e != -1) {
                audioManager.setStreamVolume(3, z.e, 0);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean b(@NonNull Activity activity, @NonNull l lVar, @NonNull p<AdObjectType, AdRequestType, ?> pVar) {
        final m y = pVar.y();
        if (y == null) {
            return false;
        }
        final d dVar = lVar.a;
        pVar.a(LogConstants.EVENT_SHOW, String.format("isDebug: %s, isLoaded: %s, isLoading: %s, placement: '%s'", new Object[]{Boolean.valueOf(lVar.b), Boolean.valueOf(y.h()), Boolean.valueOf(y.J()), dVar.n()}));
        if (!dVar.a((Context) activity, pVar.m(), y)) {
            return false;
        }
        if (y.h() || y.i() || y.d(dVar.n())) {
            final ak akVar = (ak) y.c(dVar.n());
            if (akVar != null) {
                pVar.d(y);
                final Activity activity2 = activity;
                AnonymousClass2 r0 = new Runnable() {
                    public void run() {
                        if (al.this.c()) {
                            AudioManager audioManager = (AudioManager) activity2.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
                            if (audioManager != null && z.d && audioManager.getStreamVolume(2) == 0) {
                                z.e = audioManager.getStreamVolume(3);
                                audioManager.setStreamVolume(3, 0, 0);
                            }
                        }
                        dVar.a((Context) activity2, y.T().getCode());
                        x.a(y.T().getCode(), akVar.b().getName());
                        akVar.a(activity2);
                        akVar.b(activity2);
                    }
                };
                bq.a((Runnable) r0);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        return true;
    }
}
