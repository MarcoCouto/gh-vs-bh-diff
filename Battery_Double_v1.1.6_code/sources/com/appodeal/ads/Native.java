package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.b.d;
import java.util.List;
import org.json.JSONObject;

public final class Native {
    static int a = 1;
    static NativeAdType b = NativeAdType.Auto;
    static MediaAssetType c = MediaAssetType.ALL;
    static String d;
    static boolean e = false;
    @Nullable
    @VisibleForTesting
    static ay f;
    @VisibleForTesting
    static b g;
    @VisibleForTesting
    static a h;

    public enum MediaAssetType {
        ICON,
        IMAGE,
        ALL
    }

    public enum NativeAdType {
        Auto,
        NoVideo,
        Video
    }

    @VisibleForTesting(otherwise = 3)
    static class a extends p<az, ba, c> {
        a(q<az, ba, ?> qVar) {
            super(qVar, null, 512);
        }

        /* access modifiers changed from: protected */
        public int a(ba baVar, az azVar, boolean z) {
            if (z) {
                return 1;
            }
            return Native.a;
        }

        /* access modifiers changed from: protected */
        public az a(@NonNull ba baVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
            return new az(baVar, adNetwork, bnVar);
        }

        /* access modifiers changed from: protected */
        public ba a(c cVar) {
            return new ba(cVar);
        }

        public void a(Activity activity) {
            if (r() && l()) {
                ba baVar = (ba) y();
                if (baVar == null || baVar.M()) {
                    d((Context) activity);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("video_native_autostart")) {
                Native.e = jSONObject.optBoolean("video_native_autostart", false);
            }
            if (jSONObject.has("diu")) {
                Native.d = jSONObject.optString("diu");
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(ba baVar) {
            return super.a(baVar) && !Native.c().b();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean b(ba baVar, az azVar) {
            return true;
        }

        /* access modifiers changed from: protected */
        public void b(Context context) {
            com.appodeal.ads.utils.c.b(context);
        }

        /* access modifiers changed from: protected */
        public void d() {
            Native.c().a();
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            Native.a().b(context, new c());
        }

        /* access modifiers changed from: protected */
        public boolean e() {
            return false;
        }

        /* access modifiers changed from: protected */
        public void f() {
            for (int i = 0; i < x().size() - 3; i++) {
                ba baVar = (ba) a(i);
                if (baVar != null && !baVar.s()) {
                    baVar.P();
                }
            }
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "native_disabled";
        }

        /* access modifiers changed from: protected */
        public boolean h() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean i() {
            return false;
        }
    }

    @VisibleForTesting
    static class b extends q<az, ba, ax> {
        b() {
            super(Native.c());
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void b(ba baVar) {
            Native.a(baVar, 0, false, false);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void c(ba baVar, az azVar) {
            super.c(baVar, azVar);
            baVar.d = azVar.y();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void g(ba baVar, az azVar, ax axVar) {
            if (baVar != null && axVar != null) {
                baVar.e.add(Integer.valueOf(axVar.l()));
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(ba baVar, az azVar, boolean z) {
            return true;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void c(ba baVar) {
            Native.a(baVar, 0, false, true);
        }

        /* access modifiers changed from: 0000 */
        public boolean b() {
            return false;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public boolean e(ba baVar, az azVar) {
            return azVar.isPrecache() || this.a.a(baVar, azVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public boolean f(ba baVar, az azVar, ax axVar) {
            return baVar.e.contains(Integer.valueOf(axVar.l()));
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public void l(@Nullable ba baVar, az azVar, ax axVar) {
            if (baVar != null && axVar != null) {
                baVar.f.add(Integer.valueOf(axVar.l()));
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: c */
        public boolean h(ba baVar, az azVar) {
            return false;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: d */
        public void j(ba baVar, az azVar) {
            if (azVar != null) {
                List x = azVar.x();
                if (x != null) {
                    Native.c().b.removeAll(x);
                }
            }
            if (this.a.r()) {
                Native.c().a();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean h(ba baVar, az azVar, ax axVar) {
            return baVar.f.contains(Integer.valueOf(axVar.l()));
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public boolean k(ba baVar, az azVar) {
            return baVar.h();
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public boolean i(ba baVar, az azVar, ax axVar) {
            return !baVar.e.contains(Integer.valueOf(axVar.l()));
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public boolean j(ba baVar, az azVar, ax axVar) {
            return !baVar.g.contains(Integer.valueOf(axVar.l())) && this.a.D() > 0;
        }

        /* access modifiers changed from: protected */
        /* renamed from: g */
        public d k(ba baVar, az azVar, ax axVar) {
            return axVar.n();
        }

        /* access modifiers changed from: protected */
        /* renamed from: h */
        public void c(@Nullable ba baVar, az azVar, @Nullable ax axVar) {
            if (baVar != null && axVar != null) {
                baVar.g.add(Integer.valueOf(axVar.l()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: i */
        public boolean b(ba baVar, az azVar, ax axVar) {
            return baVar.g.contains(Integer.valueOf(axVar.l()));
        }
    }

    static class c extends n<c> {
        c() {
            super("native", "debug_native");
        }
    }

    static p<az, ba, c> a() {
        if (h == null) {
            h = new a(b());
        }
        return h;
    }

    static void a(ba baVar, int i, boolean z, boolean z2) {
        a().a(baVar, i, z2, z);
    }

    static q<az, ba, ax> b() {
        if (g == null) {
            g = new b();
        }
        return g;
    }

    @NonNull
    static ay c() {
        if (f == null) {
            f = new ay();
        }
        return f;
    }
}
