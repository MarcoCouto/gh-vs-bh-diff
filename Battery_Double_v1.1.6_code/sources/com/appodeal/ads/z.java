package com.appodeal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.Log.LogLevel;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.Version;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class z {
    public static String a = null;
    static boolean b = false;
    public static LogLevel c = LogLevel.none;
    public static boolean d = false;
    public static int e = -1;
    public static boolean f = true;
    public static boolean g = true;
    public static boolean h = false;
    public static String i = null;
    @VisibleForTesting
    static Boolean j;
    private static boolean k = false;

    static int a(Integer num) {
        if (num == null) {
            return 600000;
        }
        return num.intValue();
    }

    public static void a() {
        am.a().a(0);
        bd.a().a(0);
        bj.a().a(0);
        aa.b().a(0);
        at.a().a(0);
        Native.a().a(0);
    }

    private static void a(Context context, boolean z) {
        if (context != null) {
            bl.a(context).a().putBoolean("show_errors", z).apply();
        }
    }

    static void a(JSONObject jSONObject) {
        try {
            c(jSONObject);
            if (jSONObject.has("randomize_offers")) {
                g = jSONObject.getBoolean("randomize_offers");
            }
            b(jSONObject);
            if (jSONObject.has("show_errors")) {
                a(Appodeal.f, jSONObject.getBoolean("show_errors"));
            }
            if (jSONObject.has("last_sdk_version") && i == null) {
                i = jSONObject.getString("last_sdk_version");
                if (new Version(i).compareTo(new Version("2.6.4")) == 1) {
                    Log.log(LogConstants.KEY_SDK, LogConstants.EVENT_WARNING, String.format("your SDK version %s does not match latest SDK version %s!", new Object[]{"2.6.4", i}));
                }
            }
            if (jSONObject.has("test")) {
                Appodeal.setTesting(jSONObject.getBoolean("test"));
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    static void a(boolean z) {
        k = z;
    }

    static boolean a(long j2, Integer num) {
        return System.currentTimeMillis() - j2 > ((long) a(num));
    }

    static boolean a(Context context) {
        return bl.a(context).b().getBoolean("show_errors", true);
    }

    static void b(JSONObject jSONObject) {
        if (jSONObject.optBoolean("log")) {
            Appodeal.setLogLevel(LogLevel.verbose);
        }
    }

    static boolean b() {
        return k;
    }

    static void c(JSONObject jSONObject) {
        aj a2;
        String str;
        try {
            if (jSONObject.has("ach") && jSONObject.getString("ach") != null) {
                if (jSONObject.getString("ach").equals(aj.c)) {
                    a2 = aj.a();
                    str = aj.c;
                } else if (jSONObject.getString("ach").equals(aj.d)) {
                    a2 = aj.a();
                    str = aj.d;
                } else {
                    a2 = aj.a();
                    str = aj.b;
                }
                a2.a(str);
            }
        } catch (Exception e2) {
            Log.log(e2);
        }
    }

    static boolean c() {
        if (j == null) {
            j = Boolean.valueOf(bq.h());
        }
        return j.booleanValue();
    }

    @NonNull
    static Set<String> d() {
        try {
            Set<String> stringSet = bl.a(Appodeal.f).b().getStringSet("init_url_list", new HashSet());
            if (stringSet == null) {
                stringSet = new HashSet<>();
            }
            return stringSet;
        } catch (Exception e2) {
            Log.log(e2);
            return new HashSet();
        }
    }

    static void d(JSONObject jSONObject) {
        try {
            HashSet hashSet = new HashSet();
            if (jSONObject.has("url_list")) {
                JSONArray optJSONArray = jSONObject.optJSONArray("url_list");
                if (optJSONArray != null && optJSONArray.length() > 0) {
                    for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                        String string = optJSONArray.getString(i2);
                        if (!TextUtils.isEmpty(string)) {
                            hashSet.add(string);
                        }
                    }
                }
            }
            bl.a(Appodeal.f).a().putStringSet("init_url_list", hashSet).apply();
        } catch (Exception e2) {
            Log.log(e2);
        }
    }
}
