package com.appodeal.ads;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class ar implements LocationData {
    @NonNull
    private RestrictedData a;
    @Nullable
    private Location b;
    @Nullable
    private Integer c;

    ar(@Nullable Context context, @NonNull RestrictedData restrictedData) {
        this.a = restrictedData;
        if (context != null) {
            this.b = bq.d(context);
            this.c = Integer.valueOf(this.b == null ? 0 : 1);
        }
    }

    @Nullable
    public Location getDeviceLocation() {
        if (this.a.canSendLocation()) {
            return this.b;
        }
        return null;
    }

    @Nullable
    public Integer getDeviceLocationType() {
        if (this.a.canSendLocationType()) {
            return this.c;
        }
        return null;
    }

    @Nullable
    public Float obtainLatitude() {
        if (this.a.canSendLocation()) {
            return this.b != null ? Float.valueOf(Double.valueOf(this.b.getLatitude()).floatValue()) : bp.a().getLat();
        }
        return null;
    }

    @Nullable
    public Location obtainLocation() {
        if (this.a.canSendLocation()) {
            Float obtainLatitude = obtainLatitude();
            if (obtainLatitude != null) {
                Float obtainLongitude = obtainLongitude();
                if (obtainLongitude != null) {
                    Location location = new Location("unknown");
                    location.setLatitude((double) obtainLatitude.floatValue());
                    location.setLongitude((double) obtainLongitude.floatValue());
                    return location;
                }
            }
        }
        return null;
    }

    @Nullable
    public Float obtainLongitude() {
        if (this.a.canSendLocation()) {
            return this.b != null ? Float.valueOf(Double.valueOf(this.b.getLongitude()).floatValue()) : bp.a().getLon();
        }
        return null;
    }
}
