package com.appodeal.ads;

public enum LoadingError {
    NoFill("no fill", t.NoFill, 2),
    InternalError("internal error", t.Exception, 4),
    TimeoutError("timeout error", t.TimeOutReached, 3),
    ConnectionError("connection error", t.Exception, 4),
    RequestError("request error", t.Exception, 4),
    SdkVersionNotSupported("sdk version not supported", t.Exception, 4),
    InvalidAssets("invalid assets", t.InvalidAssets, 7),
    AdapterNotFound("adapter not found", t.UndefinedAdapter, 8),
    AdTypeNotSupportedInAdapter("ad type not supported in adapter", t.IncorrectAdunit, 9),
    Canceled("ad request canceled", t.Canceled, 2),
    IncorrectAdunit("incorrect adunit", t.IncorrectAdunit, 2),
    IncorrectCreative("incorrect creative", t.IncorrectCreative, 4),
    ShowFailed("show failed", t.Exception, 4);
    
    private final String a;
    private final t b;
    private final int c;

    private LoadingError(String str, t tVar, int i) {
        this.a = str;
        this.b = tVar;
        this.c = i;
    }

    public int getCode() {
        return this.c;
    }

    public t getRequestResult() {
        return this.b;
    }

    public String toString() {
        return this.a;
    }
}
