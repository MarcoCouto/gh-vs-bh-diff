package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.b.e;
import org.json.JSONObject;

final class am {
    @VisibleForTesting
    static b a;
    @VisibleForTesting
    static a b;
    private static al<ap, an> c;

    @VisibleForTesting(otherwise = 3)
    static class a extends p<an, ap, c> {
        a(q<an, ap, ?> qVar) {
            super(qVar, e.c(), 3, 1, 3);
            this.e = 1.1f;
            this.f = 1.4f;
        }

        /* access modifiers changed from: protected */
        public an a(@NonNull ap apVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
            return new an(apVar, adNetwork, bnVar);
        }

        /* access modifiers changed from: protected */
        public ap a(c cVar) {
            return new ap(cVar);
        }

        public void a(Activity activity) {
            if (l() && r()) {
                ap apVar = (ap) y();
                if (apVar == null || apVar.M()) {
                    d((Context) activity);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(Context context, c cVar) {
            if (Appodeal.d) {
                bq.a((Runnable) new Runnable() {
                    public void run() {
                        ao.a().a.a(null, null, (LoadingError) null);
                    }
                });
            } else {
                super.a(context, cVar);
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
        }

        /* access modifiers changed from: protected */
        public boolean a(AdNetwork adNetwork, JSONObject jSONObject, String str, boolean z) {
            if (!z && adNetwork != null && adNetwork.isInterstitialShowing() && x().size() > 1) {
                ap apVar = (ap) A();
                ap apVar2 = (ap) z();
                if (!(apVar == null || apVar2 == null || apVar2.B() == null)) {
                    if (str.equals(((an) apVar2.B()).getId())) {
                        apVar.b(jSONObject);
                    }
                    am.a(apVar, 0, false, false);
                    return true;
                }
            }
            return super.a(adNetwork, jSONObject, str, z);
        }

        /* access modifiers changed from: protected */
        public boolean a(ap apVar, int i) {
            if (apVar.y() != 1 || apVar.v() == null || apVar.v() != apVar.a(i)) {
                return super.a(apVar, i);
            }
            String optString = apVar.v().optString("status");
            boolean z = false;
            if (TextUtils.isEmpty(optString)) {
                return false;
            }
            AdNetwork c = q().c(optString);
            if (c != null && c.isInterstitialShowing()) {
                z = true;
            }
            return z;
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            ao.a().a.a(context, new c());
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "interstitials_disabled";
        }

        public boolean r() {
            return ao.a().b();
        }
    }

    @VisibleForTesting
    static class b extends bc<an, ap> {
        b() {
            super(ao.a().a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void b(ap apVar) {
            am.a(apVar, 0, false, false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void q(ap apVar, an anVar) {
            anVar.b().setInterstitialShowing(true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void c(ap apVar, an anVar, LoadingError loadingError) {
            super.c(apVar, anVar, loadingError);
            ao.a().a.b();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(ap apVar, an anVar, boolean z) {
            super.b(apVar, anVar, z);
            if (c(apVar, anVar)) {
                am.a(Appodeal.e, new l(this.a.t()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(ap apVar, an anVar, Object obj) {
            return super.j(apVar, anVar, obj) && this.a.D() > 0;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void c(ap apVar) {
            am.a(apVar, 0, false, true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(@Nullable ap apVar, @Nullable an anVar, @Nullable LoadingError loadingError) {
            super.a(apVar, anVar, loadingError);
            al.a();
            if (apVar != null && !this.a.x().isEmpty()) {
                ao.a().a.b();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public boolean e(ap apVar, an anVar) {
            return (!c(apVar, anVar) && (apVar.a(0) == apVar.v() || anVar.isPrecache() || anVar.h() || this.a.a(apVar, anVar))) && super.e(apVar, anVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void e(ap apVar) {
            if (!apVar.a() && ao.a().b()) {
                ap apVar2 = (ap) this.a.y();
                if (apVar2 == null || apVar2.M()) {
                    this.a.d(Appodeal.f);
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean c(ap apVar, an anVar) {
            return apVar.u() && !anVar.h();
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean a(ap apVar) {
            return apVar.v() == null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean l(ap apVar, an anVar) {
            return apVar.v() == null || (anVar != null && apVar.v().optString("id").equals(anVar.getId()));
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public void c(ap apVar, an anVar) {
            super.c(apVar, anVar);
            if (apVar.v() == anVar.getJsonData()) {
                apVar.b((JSONObject) null);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public void n(ap apVar, an anVar) {
            am.d().b();
            al.a();
            this.a.d(null);
            anVar.b().setInterstitialShowing(false);
            if (!apVar.n() && this.a.D() > 0 && System.currentTimeMillis() - (apVar.O() * 1000) >= ((long) this.a.D())) {
                o(apVar, anVar);
            }
            f(apVar);
        }
    }

    static class c extends n<c> {
        c() {
            super("banner", "debug");
        }
    }

    static p<an, ap, c> a() {
        if (b == null) {
            b = new a(b());
        }
        return b;
    }

    static void a(@Nullable ap apVar, int i, boolean z, boolean z2) {
        a().a(apVar, i, z2, z);
    }

    static boolean a(Activity activity, l lVar) {
        return d().a(activity, lVar, a());
    }

    static q<an, ap, Object> b() {
        if (a == null) {
            a = new b();
        }
        return a;
    }

    /* access modifiers changed from: private */
    public static al<ap, an> d() {
        if (c == null) {
            c = new al<>("debug");
        }
        return c;
    }
}
