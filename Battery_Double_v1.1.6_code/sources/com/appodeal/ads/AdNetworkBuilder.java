package com.appodeal.ads;

import android.util.Pair;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.DependencyRule;
import java.util.ArrayList;
import java.util.List;

public abstract class AdNetworkBuilder {
    public abstract AdNetwork build();

    public ActivityRule[] getAdActivityRules() {
        return new ActivityRule[0];
    }

    public abstract String getAdapterVersion();

    public abstract String getName();

    public DependencyRule[] getOptionalClasses() {
        return new DependencyRule[0];
    }

    public String[] getRequiredClasses() {
        return new String[0];
    }

    public String[] getRequiredPermissions() {
        return new String[0];
    }

    public String[] getRequiredProvidersClassName() {
        return new String[0];
    }

    public String[] getRequiredReceiverClassName() {
        return new String[0];
    }

    public List<Pair<String, Pair<String, String>>> getRequiredServiceWithData() {
        return new ArrayList();
    }

    public boolean isOptional() {
        return false;
    }
}
