package com.appodeal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;

class bh extends ak<bi, UnifiedRewarded, UnifiedRewardedParams, UnifiedRewardedCallback> {

    private final class a extends UnifiedRewardedCallback {
        private a() {
        }

        public void onAdClicked() {
            bj.b().a(bh.this.a(), bh.this, null, (UnifiedAdCallbackClickTrackListener) null);
        }

        public void onAdClicked(@Nullable UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
            bj.b().a(bh.this.a(), bh.this, null, unifiedAdCallbackClickTrackListener);
        }

        public void onAdClosed() {
            bj.b().m(bh.this.a(), bh.this);
        }

        public void onAdExpired() {
            bj.b().i(bh.this.a(), bh.this);
        }

        public void onAdFinished() {
            bj.b().o(bh.this.a(), bh.this);
        }

        public void onAdInfoRequested(@Nullable Bundle bundle) {
            bh.this.a(bundle);
        }

        public void onAdLoadFailed(@Nullable LoadingError loadingError) {
            bj.b().b(bh.this.a(), bh.this, loadingError);
        }

        public void onAdLoaded() {
            bj.b().b(bh.this.a(), bh.this);
        }

        public void onAdShowFailed() {
            bj.b().a(bh.this.a(), bh.this, null, LoadingError.ShowFailed);
        }

        public void onAdShown() {
            bj.b().p(bh.this.a(), bh.this);
        }

        public void printError(@Nullable String str, @Nullable Object obj) {
            ((bi) bh.this.a()).a((AdUnit) bh.this, str, obj);
        }
    }

    private class b implements UnifiedRewardedParams {
        private b() {
        }

        public int getAfd() {
            return bj.a().D();
        }

        public int getMaxDuration() {
            return bj.b;
        }

        public String obtainPlacementId() {
            return bj.a().u();
        }

        public String obtainSegmentId() {
            return bj.a().s();
        }
    }

    bh(@NonNull bi biVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
        super(biVar, adNetwork, bnVar, 10000);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: b */
    public UnifiedRewarded a(@NonNull Activity activity, @NonNull AdNetwork adNetwork, @NonNull Object obj, int i) {
        return adNetwork.createRewarded();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: c */
    public UnifiedRewardedParams b(int i) {
        return new b();
    }

    /* access modifiers changed from: protected */
    public LoadingError s() {
        return b().isRewardedShowing() ? LoadingError.Canceled : super.s();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: t */
    public UnifiedRewardedCallback o() {
        return new a();
    }
}
