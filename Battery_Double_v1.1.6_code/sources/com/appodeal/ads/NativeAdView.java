package com.appodeal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import com.appodeal.ads.native_ad.views.b;
import java.util.ArrayList;
import java.util.List;

public class NativeAdView extends b {
    protected View a;
    protected View b;
    protected View c;
    protected View d;
    protected View e;
    protected NativeIconView f;
    protected NativeMediaView g;
    private ax h;

    public NativeAdView(Context context) {
        super(context);
    }

    public NativeAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NativeAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @TargetApi(21)
    public NativeAdView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    private void a() {
        if (this.h != null) {
            this.h.b();
        }
    }

    public void destroy() {
        b.j.a();
        if (this.h != null) {
            this.h.destroy();
        }
    }

    public View getCallToActionView() {
        return this.b;
    }

    public List<View> getClickableViews() {
        ArrayList arrayList = new ArrayList();
        if (this.a != null) {
            arrayList.add(this.a);
        }
        if (this.d != null) {
            arrayList.add(this.d);
        }
        if (this.b != null) {
            arrayList.add(this.b);
        }
        if (this.c != null) {
            arrayList.add(this.c);
        }
        if (this.f != null) {
            arrayList.add(this.f);
        }
        if (this.g != null) {
            arrayList.add(this.g);
        }
        return arrayList;
    }

    public View getDescriptionView() {
        return this.d;
    }

    public View getNativeIconView() {
        return this.f;
    }

    public NativeMediaView getNativeMediaView() {
        return this.g;
    }

    public View getProviderView() {
        return this.e;
    }

    public View getRatingView() {
        return this.c;
    }

    public View getTitleView() {
        return this.a;
    }

    public void registerView(NativeAd nativeAd) {
        registerView(nativeAd, "default");
    }

    public void registerView(NativeAd nativeAd, @NonNull String str) {
        b.h.a();
        if (this.f != null) {
            this.f.removeAllViews();
        }
        if (this.g != null) {
            this.g.removeAllViews();
        }
        a();
        this.h = (ax) nativeAd;
        a();
        if (this.f != null) {
            this.h.a(this.f);
        }
        if (this.g != null) {
            this.h.a(this.g);
        }
        this.h.a(this, str);
    }

    public void setCallToActionView(View view) {
        b.b.a();
        this.b = view;
    }

    public void setDescriptionView(View view) {
        b.d.a();
        this.d = view;
    }

    public void setNativeIconView(NativeIconView nativeIconView) {
        b.f.a();
        this.f = nativeIconView;
    }

    public void setNativeMediaView(NativeMediaView nativeMediaView) {
        b.g.a();
        this.g = nativeMediaView;
    }

    public void setProviderView(View view) {
        b.e.a();
        this.e = view;
    }

    public void setRatingView(View view) {
        b.c.a();
        this.c = view;
    }

    public void setTitleView(View view) {
        b.a.a();
        this.a = view;
    }

    public void unregisterViewForInteraction() {
        b.i.a();
        a();
    }
}
