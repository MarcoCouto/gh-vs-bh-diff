package com.appodeal.ads.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.bq;
import com.appodeal.ads.m;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class b extends f implements a {
    @VisibleForTesting
    final List<g> a = new ArrayList();
    private f f;
    private int g;

    public b(@NonNull JSONObject jSONObject, int i) {
        a(jSONObject);
        JSONArray optJSONArray = jSONObject.optJSONArray("networks");
        this.g = i;
        this.a.add(new e(i));
        this.a.add(new c(optJSONArray));
        this.a.add(new d(i));
        this.f = d();
    }

    @NonNull
    public List<JSONObject> a() {
        return this.f.b;
    }

    public void a(@Nullable m mVar) {
        this.f = d();
        for (g a2 : this.a) {
            a2.a(this.f, this.f.e, mVar);
        }
        this.f.e();
        bq.a(this.g, (a) this);
    }

    @NonNull
    public List<JSONObject> b() {
        return this.f.c;
    }

    @NonNull
    public List<JSONObject> c() {
        return this.f.d;
    }
}
