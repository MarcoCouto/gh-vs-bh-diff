package com.appodeal.ads.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.utils.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class f {
    final List<JSONObject> b = new ArrayList();
    final List<JSONObject> c = new ArrayList();
    final List<JSONObject> d = new ArrayList();
    final List<JSONObject> e = new ArrayList();

    f() {
    }

    private void a(@NonNull List<JSONObject> list, @Nullable List<JSONObject> list2) {
        if (list2 != null) {
            list.clear();
            list.addAll(list2);
        }
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull List<JSONObject> list, @NonNull String str) {
        list.clear();
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                try {
                    list.add(optJSONArray.getJSONObject(i));
                } catch (Exception e2) {
                    Log.log(e2);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable List<JSONObject> list) {
        a(this.b, list);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull JSONObject jSONObject) {
        a(jSONObject, this.b, "precache");
        a(jSONObject, this.c, "ads");
        this.e.clear();
        for (JSONObject put : this.b) {
            try {
                put.put("is_precache", true);
            } catch (JSONException e2) {
                Log.log(e2);
            }
        }
        this.e.addAll(this.b);
        this.e.addAll(this.c);
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable List<JSONObject> list) {
        a(this.c, list);
    }

    /* access modifiers changed from: 0000 */
    public void c(@Nullable List<JSONObject> list) {
        a(this.d, list);
    }

    /* access modifiers changed from: 0000 */
    public f d() {
        f fVar = new f();
        fVar.a(this.b);
        fVar.b(this.c);
        fVar.c(this.d);
        fVar.a(fVar.e, this.e);
        return fVar;
    }

    /* access modifiers changed from: 0000 */
    public void e() {
        this.b.clear();
        this.c.clear();
        for (JSONObject jSONObject : this.e) {
            (jSONObject.optBoolean("is_precache") ? this.b : this.c).add(jSONObject);
        }
    }
}
