package com.appodeal.ads.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.b.i;
import com.appodeal.ads.m;
import java.util.List;
import org.json.JSONObject;

class e extends g {
    private final int a;

    e(int i) {
        this.a = i;
    }

    public void a(@NonNull f fVar, @NonNull List<JSONObject> list, @Nullable m mVar) {
        i.a().b().a(list, this.a);
    }
}
