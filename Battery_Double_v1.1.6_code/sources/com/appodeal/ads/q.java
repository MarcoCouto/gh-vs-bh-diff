package com.appodeal.ads;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.aq.a;
import com.appodeal.ads.aq.c;
import com.appodeal.ads.b.d;
import com.appodeal.ads.i;
import com.appodeal.ads.i.b;
import com.appodeal.ads.m;
import com.appodeal.ads.unified.UnifiedAdCallbackClickTrackListener;
import com.appodeal.ads.utils.EventsTracker;
import com.appodeal.ads.utils.EventsTracker.EventType;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.appodeal.ads.utils.ac;
import com.appodeal.ads.utils.o;
import com.appodeal.ads.utils.x;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.github.mikephil.charting.utils.Utils;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

abstract class q<AdObjectType extends i, AdRequestType extends m<AdObjectType>, ReferenceObjectType> {
    @NonNull
    protected p<AdObjectType, AdRequestType, ?> a;
    /* access modifiers changed from: private */
    @NonNull
    public c<AdRequestType, AdObjectType, ReferenceObjectType> b;

    protected q(@NonNull c<AdRequestType, AdObjectType, ReferenceObjectType> cVar) {
        this.b = cVar;
    }

    private void a(int i) {
        if (this.a.r()) {
            bq.a((Runnable) new Runnable() {
                public void run() {
                    try {
                        q.this.d(q.this.a.y());
                        q.this.a.G();
                    } catch (Exception e) {
                        Log.log(e);
                    }
                }
            }, (long) i);
        }
    }

    private void b(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype, @NonNull LoadingError loadingError) {
        if (loadingError == LoadingError.ShowFailed) {
            final AdRequestType adrequesttype2 = adrequesttype;
            final AdObjectType adobjecttype2 = adobjecttype;
            final ReferenceObjectType referenceobjecttype2 = referenceobjecttype;
            final LoadingError loadingError2 = loadingError;
            AnonymousClass5 r1 = new Runnable() {
                public void run() {
                    q.this.b.a(adrequesttype2, adobjecttype2, referenceobjecttype2, loadingError2);
                }
            };
            bq.a((Runnable) r1);
            return;
        }
        d(adrequesttype, adobjecttype, loadingError);
    }

    private void d(@Nullable final AdRequestType adrequesttype, @Nullable final AdObjectType adobjecttype, @NonNull final LoadingError loadingError) {
        bq.a((Runnable) new Runnable() {
            public void run() {
                q.this.b.a(adrequesttype, adobjecttype, loadingError);
            }
        });
    }

    private void m(final AdRequestType adrequesttype, final AdObjectType adobjecttype, final ReferenceObjectType referenceobjecttype) {
        bq.a((Runnable) new Runnable() {
            public void run() {
                q.this.b.a(adrequesttype, adobjecttype, referenceobjecttype);
            }
        });
    }

    private void n(@NonNull final AdRequestType adrequesttype, @NonNull final AdObjectType adobjecttype, @Nullable final ReferenceObjectType referenceobjecttype) {
        bq.a((Runnable) new Runnable() {
            public void run() {
                q.this.b.b(adrequesttype, adobjecttype, referenceobjecttype);
            }
        });
    }

    private boolean q(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        if (!(!adrequesttype.a() && adrequesttype.z() && e(adrequesttype, adobjecttype))) {
            return false;
        }
        b(adrequesttype);
        return true;
    }

    private void r(final AdRequestType adrequesttype, final AdObjectType adobjecttype) {
        bq.a((Runnable) new Runnable() {
            public void run() {
                q.this.b.b(adrequesttype, adobjecttype);
            }
        });
    }

    private void s(@NonNull AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype) {
        try {
            if (!adrequesttype.p() && adrequesttype.j()) {
                if (Appodeal.h != null && !adrequesttype.t()) {
                    Appodeal.h.a(this.a.o(), k(adrequesttype, adobjecttype));
                }
                adrequesttype.b(adobjecttype);
                adrequesttype.f(true);
                bf.a((m) adrequesttype, (i) adobjecttype);
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    private void t(final AdRequestType adrequesttype, final AdObjectType adobjecttype) {
        bq.a((Runnable) new Runnable() {
            public void run() {
                q.this.b.c(adrequesttype, adobjecttype);
            }
        });
    }

    public void a() {
    }

    public void a(@NonNull AdRequestType adrequesttype, @NonNull AdObjectType adobjecttype) {
        adrequesttype.m(adobjecttype);
        this.a.a(LogConstants.EVENT_LOAD_START, (AdUnit) adobjecttype, (LoadingError) null);
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable LoadingError loadingError) {
        Appodeal.b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:63:0x0109 A[Catch:{ Exception -> 0x0116 }] */
    public void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable bn bnVar, @NonNull LoadingError loadingError) {
        int C;
        if (adrequesttype != null) {
            try {
                if (!adrequesttype.t()) {
                    if (!adrequesttype.q()) {
                        boolean a2 = adrequesttype.a();
                        if (adrequesttype.j(adobjecttype)) {
                            adrequesttype.i(adobjecttype);
                        }
                        if (adobjecttype == null || adobjecttype.d == b.Wait) {
                            this.a.a(LogConstants.EVENT_LOAD_FAILED, (AdUnit) adobjecttype, loadingError);
                            EventsTracker.get().a(Appodeal.f, this.a.m(), (i) adobjecttype, EventType.FailedToLoad);
                            if (adobjecttype != null) {
                                adobjecttype.d = b.FailedToLoad;
                                if (Appodeal.h != null) {
                                    Appodeal.h.a(this.a.o(), adobjecttype.d(), false, loadingError.getCode());
                                }
                                adobjecttype.a(loadingError);
                            }
                            if (bnVar != null) {
                                adrequesttype.a(bnVar, loadingError);
                            }
                            if (!this.a.c(adrequesttype)) {
                                adrequesttype.a(this.a, false);
                                s(adrequesttype, adobjecttype);
                            } else if (adrequesttype.I()) {
                                if (adobjecttype != null && !adobjecttype.isAsync() && adrequesttype.N() > 0) {
                                    this.a.a(adrequesttype, 0, adobjecttype.isPrecache(), adrequesttype.a());
                                }
                            } else {
                                if (adobjecttype == null || !adobjecttype.isPrecache()) {
                                    if (!adrequesttype.z()) {
                                        adrequesttype.a(this.a, false);
                                        if (l(adrequesttype, adobjecttype)) {
                                            s(adrequesttype, adobjecttype);
                                        }
                                        if (adrequesttype.B() != null) {
                                            f(adrequesttype, adrequesttype.B());
                                        } else {
                                            c(adrequesttype, adobjecttype, loadingError);
                                            d(adrequesttype, adobjecttype, loadingError);
                                            if (!a2) {
                                                C = h(adrequesttype, adobjecttype) ? 30000 : this.a.C();
                                                a(C);
                                            }
                                        }
                                    } else if (a2) {
                                        c(adrequesttype, adobjecttype, loadingError);
                                        d(adrequesttype, adobjecttype, loadingError);
                                    }
                                    if (adobjecttype != null) {
                                        adobjecttype.q();
                                    }
                                } else {
                                    if (!a2) {
                                        if (adrequesttype.A()) {
                                            c(adrequesttype);
                                        } else if (!adrequesttype.z()) {
                                            c(adrequesttype, adobjecttype, loadingError);
                                            d(adrequesttype, adobjecttype, loadingError);
                                            s(adrequesttype, adobjecttype);
                                            adrequesttype.a(this.a, false);
                                            C = this.a.C();
                                            a(C);
                                        }
                                    }
                                    if (adobjecttype != null) {
                                    }
                                }
                                b(adrequesttype);
                                if (adobjecttype != null) {
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.log(e);
                a(adrequesttype, adobjecttype, (ReferenceObjectType) null, LoadingError.InternalError);
            }
        }
    }

    public void a(AdRequestType adrequesttype, AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype) {
        try {
            if (!b(adrequesttype, adobjecttype, referenceobjecttype)) {
                adrequesttype.d(true);
                adobjecttype.e();
                adobjecttype.m();
                this.a.a(LogConstants.EVENT_FINISHED, (AdUnit) adobjecttype, (LoadingError) null);
                EventsTracker.get().a(Appodeal.f, this.a.m(), (i) adobjecttype, EventType.Finish);
                new c(Appodeal.f, adrequesttype, "finish").a(k(adrequesttype, adobjecttype, referenceobjecttype)).a((a<AdRequestType>) new ac<AdRequestType>()).a(adobjecttype.getId()).b(ae.a(this.a.m())).a().a();
                c(adrequesttype, adobjecttype, referenceobjecttype);
                d(adrequesttype, adobjecttype, referenceobjecttype);
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void a(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype, @Nullable LoadingError loadingError) {
        if (loadingError == null) {
            try {
                loadingError = LoadingError.InternalError;
            } catch (Exception e) {
                Log.log(e);
                return;
            }
        }
        this.a.a(LogConstants.EVENT_LOAD_FAILED_SOFT, (AdUnit) adobjecttype, loadingError);
        EventsTracker.get().a(Appodeal.f, this.a.m(), (i) adobjecttype, EventType.InternalError);
        if (adrequesttype != null) {
            adrequesttype.a(this.a, false);
            adrequesttype.a(false);
            adrequesttype.b(false);
        }
        if (adobjecttype != null) {
            adobjecttype.a(loadingError);
        }
        a(adrequesttype, adobjecttype, loadingError);
        a(this.a.C());
        b(adrequesttype, adobjecttype, referenceobjecttype, loadingError);
    }

    public void a(AdRequestType adrequesttype, AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype, @Nullable final UnifiedAdCallbackClickTrackListener unifiedAdCallbackClickTrackListener) {
        try {
            if (i(adrequesttype, adobjecttype, referenceobjecttype)) {
                e(adrequesttype, adobjecttype, referenceobjecttype);
            }
            if (j(adrequesttype, adobjecttype, referenceobjecttype)) {
                a(adrequesttype, adobjecttype, referenceobjecttype);
            }
            if (!h(adrequesttype, adobjecttype, referenceobjecttype)) {
                adrequesttype.h(true);
                if (Appodeal.h != null) {
                    Appodeal.h.b(this.a.o(), adobjecttype.d());
                }
                this.a.a(LogConstants.EVENT_CLICKED, (AdUnit) adobjecttype, (LoadingError) null);
                adobjecttype.a(Appodeal.f);
                EventsTracker.get().a(Appodeal.f, this.a.m(), (i) adobjecttype, EventType.Click);
                new c(Appodeal.f, adrequesttype, String.CLICK).a(k(adrequesttype, adobjecttype, referenceobjecttype)).a((a<AdRequestType>) new a() {
                    public void a(@Nullable m mVar) {
                        if (unifiedAdCallbackClickTrackListener != null) {
                            unifiedAdCallbackClickTrackListener.onTrackError();
                        }
                    }

                    public void a(JSONObject jSONObject, @Nullable m mVar, String str) {
                        if (unifiedAdCallbackClickTrackListener != null) {
                            unifiedAdCallbackClickTrackListener.onTrackSuccess(jSONObject);
                        }
                    }
                }).a(adobjecttype.getId()).b(ae.a(this.a.m())).a().a();
                l(adrequesttype, adobjecttype, referenceobjecttype);
                n(adrequesttype, adobjecttype, referenceobjecttype);
            } else if (unifiedAdCallbackClickTrackListener != null) {
                unifiedAdCallbackClickTrackListener.onTrackError();
            }
        } catch (Exception e) {
            Log.log(e);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull p<AdObjectType, AdRequestType, ?> pVar) {
        this.a = pVar;
    }

    /* access modifiers changed from: protected */
    public boolean a(AdRequestType adrequesttype) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean a(AdRequestType adrequesttype, AdObjectType adobjecttype, boolean z) {
        return !adrequesttype.m() && (!z || this.a.w());
    }

    /* access modifiers changed from: 0000 */
    public abstract void b(AdRequestType adrequesttype);

    public void b(final AdRequestType adrequesttype, AdObjectType adobjecttype) {
        if (adrequesttype != null) {
            try {
                if (!adrequesttype.t() && !adrequesttype.q() && !adrequesttype.p()) {
                    if (!this.a.e(adrequesttype)) {
                        if (adobjecttype.d == b.FailedToLoad) {
                            adobjecttype.q();
                            return;
                        }
                        if (adrequesttype.j(adobjecttype)) {
                            adrequesttype.i(adobjecttype);
                        }
                        adobjecttype.d = b.Loaded;
                        this.a.a(LogConstants.EVENT_LOADED, (AdUnit) adobjecttype, (LoadingError) null);
                        if (Appodeal.h != null) {
                            Appodeal.h.a(this.a.o(), adobjecttype.d(), true);
                        }
                        adobjecttype.j();
                        adrequesttype.n(adobjecttype);
                        i a2 = adrequesttype.a(adobjecttype);
                        if (a2.h() || adrequesttype.B() == null || adrequesttype.B() == adobjecttype || adrequesttype.B().getEcpm() < a2.getEcpm()) {
                            c(adrequesttype, a2);
                            d(adrequesttype, a2);
                        }
                        boolean c = this.a.c(adrequesttype);
                        boolean q = q(adrequesttype, adobjecttype);
                        if ((!q && !adrequesttype.I() && a(adrequesttype)) || !c) {
                            s(adrequesttype, adobjecttype);
                        }
                        if (c) {
                            o.a(adobjecttype, new o.b<AdObjectType>() {
                                public void a(AdObjectType adobjecttype) {
                                    q.this.i(adrequesttype, adobjecttype);
                                }
                            });
                            if (!adobjecttype.h()) {
                                b(adrequesttype, adobjecttype, q);
                                this.a.b(5000);
                            }
                        }
                        return;
                    }
                }
            } catch (Exception e) {
                Log.log(e);
                a(adrequesttype, adobjecttype, (ReferenceObjectType) null, LoadingError.InternalError);
            }
        }
        adobjecttype.q();
    }

    public void b(@Nullable AdRequestType adrequesttype, @Nullable AdObjectType adobjecttype, @Nullable LoadingError loadingError) {
        bn c = adobjecttype != null ? adobjecttype.c() : null;
        if (loadingError == null) {
            loadingError = LoadingError.NoFill;
        }
        a(adrequesttype, adobjecttype, c, loadingError);
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void b(AdRequestType adrequesttype, AdObjectType adobjecttype, boolean z) {
        if (a(adrequesttype, adobjecttype, z)) {
            adrequesttype.c(true);
            f(adrequesttype, adobjecttype);
        }
    }

    /* access modifiers changed from: 0000 */
    @Deprecated
    public boolean b() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b(AdRequestType adrequesttype, AdObjectType adobjecttype, ReferenceObjectType referenceobjecttype) {
        return adrequesttype.n();
    }

    /* access modifiers changed from: 0000 */
    public abstract void c(AdRequestType adrequesttype);

    /* access modifiers changed from: protected */
    public void c(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        adrequesttype.a(adobjecttype.getEcpm());
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void c(AdRequestType adrequesttype, AdObjectType adobjecttype, LoadingError loadingError) {
        Appodeal.b();
    }

    /* access modifiers changed from: protected */
    public void c(AdRequestType adrequesttype, AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype) {
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void d(@Nullable AdRequestType adrequesttype) {
        if (!this.a.x().isEmpty()) {
            m y = this.a.y();
            if (y == null || !y.M()) {
                return;
            }
        }
        this.a.d(Appodeal.f);
    }

    /* access modifiers changed from: protected */
    public void d(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        if (!adobjecttype.h()) {
            if (adobjecttype.isPrecache()) {
                adrequesttype.b(true);
            } else {
                adrequesttype.a(true);
            }
            o.a(adrequesttype.B());
            AdObjectType B = adrequesttype.B();
            if (!(B == null || B == adobjecttype || B.h())) {
                B.q();
            }
            adrequesttype.e(adobjecttype);
            if (!this.a.c(adrequesttype)) {
                adrequesttype.a(this.a, false, true);
            } else if (!adobjecttype.isPrecache() || adrequesttype.u()) {
                adrequesttype.a(this.a, false, adrequesttype.u());
            }
        } else {
            adrequesttype.g(adobjecttype);
            adrequesttype.c(adobjecttype);
        }
    }

    /* access modifiers changed from: 0000 */
    public final void d(final AdRequestType adrequesttype, final AdObjectType adobjecttype, @Nullable final ReferenceObjectType referenceobjecttype) {
        bq.a((Runnable) new Runnable() {
            public void run() {
                q.this.b.c(adrequesttype, adobjecttype, referenceobjecttype);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void e(AdRequestType adrequesttype) {
    }

    public synchronized void e(AdRequestType adrequesttype, AdObjectType adobjecttype, @Nullable ReferenceObjectType referenceobjecttype) {
        if (adrequesttype != null) {
            try {
                if (!f(adrequesttype, adobjecttype, referenceobjecttype)) {
                    adrequesttype.g(true);
                    adrequesttype.a(this.a, false, true);
                    if (!adrequesttype.p()) {
                        s(adrequesttype, adobjecttype);
                    }
                    o.a((i) adobjecttype);
                    x.a(this.a.n());
                    if (Appodeal.h != null) {
                        Appodeal.h.a(this.a.o(), adobjecttype.d());
                    }
                    this.a.a(LogConstants.EVENT_SHOWN, (AdUnit) adobjecttype, (LoadingError) null);
                    adrequesttype.a(false);
                    adrequesttype.b(false);
                    adrequesttype.l(adobjecttype);
                    if (c()) {
                        adobjecttype.k();
                    }
                    adobjecttype.a(this.a.t().b());
                    EventsTracker.get().a(Appodeal.f, this.a.m(), (i) adobjecttype, EventType.Impression);
                    new c(Appodeal.f, adrequesttype, "show").a(k(adrequesttype, adobjecttype, referenceobjecttype)).a((a<AdRequestType>) new ac<AdRequestType>()).a(adobjecttype.getId()).b(adobjecttype.getEcpm()).b(ae.a(this.a.m())).a(this.a.p()).a().a();
                    g(adrequesttype, adobjecttype, referenceobjecttype);
                    m(adrequesttype, adobjecttype, referenceobjecttype);
                    e(adrequesttype);
                }
            } catch (Exception e) {
                Log.log(e);
            }
        }
        return;
    }

    /* access modifiers changed from: protected */
    public boolean e(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        return adobjecttype.isPrecache() || adobjecttype.h() || this.a.a(adrequesttype, adobjecttype);
    }

    /* access modifiers changed from: 0000 */
    public void f(AdRequestType adrequesttype) {
        m b2 = this.a.b(adrequesttype);
        if (b2 != null && b2.v() != null) {
            b2.c(b2.v());
            if (b2.C() < b2.v().optDouble(RequestInfoKeys.APPODEAL_ECPM, Utils.DOUBLE_EPSILON) && (b2.y() == 1 || b2.h())) {
                b(b2);
            } else if (b2.h() && !b2.J()) {
                s(b2, b2.B());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final void f(final AdRequestType adrequesttype, final AdObjectType adobjecttype) {
        bq.a((Runnable) new Runnable() {
            public void run() {
                q.this.b.a(adrequesttype, adobjecttype);
            }
        });
    }

    /* access modifiers changed from: protected */
    public boolean f(AdRequestType adrequesttype, AdObjectType adobjecttype, ReferenceObjectType referenceobjecttype) {
        return adrequesttype.q();
    }

    /* access modifiers changed from: 0000 */
    public void g(@NonNull AdRequestType adrequesttype) {
        adrequesttype.a(this.a, false, true);
        s(adrequesttype, null);
    }

    public void g(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        b(adrequesttype, adobjecttype, (LoadingError) null);
    }

    /* access modifiers changed from: protected */
    public void g(AdRequestType adrequesttype, AdObjectType adobjecttype, ReferenceObjectType referenceobjecttype) {
    }

    /* access modifiers changed from: 0000 */
    public boolean h(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        return adrequesttype.i();
    }

    /* access modifiers changed from: protected */
    public boolean h(AdRequestType adrequesttype, AdObjectType adobjecttype, ReferenceObjectType referenceobjecttype) {
        return adrequesttype.r();
    }

    public void i(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        if (this.a.c(adrequesttype) && this.a.x().indexOf(adrequesttype) >= 0) {
            this.a.a(LogConstants.EVENT_EXPIRED, (AdUnit) adobjecttype, (LoadingError) null);
            EventsTracker.get().a(Appodeal.f, this.a.m(), (i) adobjecttype, EventType.Expired);
            if (b()) {
                if (adobjecttype.h()) {
                    o.a((i) adobjecttype);
                    adrequesttype.e(adobjecttype.getId());
                    adobjecttype.q();
                } else if (adrequesttype.f((i) adobjecttype)) {
                    o.a((i) adobjecttype);
                    o.a(adrequesttype.F().values());
                    adrequesttype.D();
                    adrequesttype.G();
                }
            }
            if (adobjecttype != null) {
                o.a((i) adobjecttype);
                adrequesttype.e(adobjecttype.getId());
            }
            adrequesttype.D();
            adrequesttype.Q();
            j(adrequesttype, adobjecttype);
            r(adrequesttype, adobjecttype);
        }
    }

    /* access modifiers changed from: protected */
    public boolean i(AdRequestType adrequesttype, AdObjectType adobjecttype, ReferenceObjectType referenceobjecttype) {
        return !adrequesttype.q();
    }

    /* access modifiers changed from: 0000 */
    public void j(AdRequestType adrequesttype, AdObjectType adobjecttype) {
    }

    /* access modifiers changed from: protected */
    public boolean j(AdRequestType adrequesttype, AdObjectType adobjecttype, ReferenceObjectType referenceobjecttype) {
        return !adrequesttype.n();
    }

    /* access modifiers changed from: protected */
    public d k(AdRequestType adrequesttype, AdObjectType adobjecttype, ReferenceObjectType referenceobjecttype) {
        return this.a.t();
    }

    /* access modifiers changed from: protected */
    public boolean k(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        return adrequesttype.h() || adrequesttype.i();
    }

    /* access modifiers changed from: 0000 */
    public abstract void l(@Nullable AdRequestType adrequesttype, AdObjectType adobjecttype, ReferenceObjectType referenceobjecttype);

    /* access modifiers changed from: protected */
    public boolean l(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        return true;
    }

    public void m(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        if (adrequesttype != null) {
            try {
                if (!adrequesttype.o()) {
                    adrequesttype.e(true);
                    adobjecttype.l();
                    this.a.a(LogConstants.EVENT_CLOSED, (AdUnit) adobjecttype, (LoadingError) null);
                    n(adrequesttype, adobjecttype);
                    t(adrequesttype, adobjecttype);
                }
            } catch (Exception e) {
                Log.log(e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void n(AdRequestType adrequesttype, AdObjectType adobjecttype) {
    }

    public void o(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        a(adrequesttype, adobjecttype, (ReferenceObjectType) null);
    }

    public void p(AdRequestType adrequesttype, AdObjectType adobjecttype) {
        e(adrequesttype, adobjecttype, null);
    }
}
