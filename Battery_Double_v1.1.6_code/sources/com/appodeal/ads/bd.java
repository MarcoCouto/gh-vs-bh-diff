package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.appodeal.ads.b.e;
import org.json.JSONObject;

final class bd {
    @VisibleForTesting
    static c a;
    @VisibleForTesting
    static b b;
    private static bs<bt, br> c;

    static class a extends n<a> {
        a() {
            super("video", "debug_video");
        }
    }

    @VisibleForTesting(otherwise = 3)
    static class b extends p<br, bt, a> {
        b(q<br, bt, ?> qVar) {
            super(qVar, e.c(), 2, 2, 3);
            this.e = 1.1f;
            this.f = 1.4f;
        }

        /* access modifiers changed from: protected */
        public br a(@NonNull bt btVar, @NonNull AdNetwork adNetwork, @NonNull bn bnVar) {
            return new br(btVar, adNetwork, bnVar);
        }

        /* access modifiers changed from: protected */
        public bt a(a aVar) {
            return new bt(aVar);
        }

        public void a(Activity activity) {
            if (l() && r()) {
                bt btVar = (bt) y();
                if (btVar == null || btVar.M()) {
                    d((Context) activity);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(Context context, a aVar) {
            if (Appodeal.d) {
                bq.a((Runnable) new Runnable() {
                    public void run() {
                        ao.a().b.a(null, null, (LoadingError) null);
                    }
                });
            } else {
                super.a(context, aVar);
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
        }

        /* access modifiers changed from: protected */
        public boolean a(AdNetwork adNetwork, JSONObject jSONObject, String str, boolean z) {
            if (!z && adNetwork != null && adNetwork.isVideoShowing() && x().size() > 1) {
                bt btVar = (bt) A();
                bt btVar2 = (bt) z();
                if (!(btVar == null || btVar2 == null || btVar2.B() == null)) {
                    if (str.equals(((br) btVar2.B()).getId())) {
                        btVar.b(jSONObject);
                    }
                    bd.a(btVar, 0, false, false);
                    return true;
                }
            }
            return super.a(adNetwork, jSONObject, str, z);
        }

        /* access modifiers changed from: protected */
        public boolean a(bt btVar, int i) {
            if (btVar.y() != 1 || btVar.v() == null || btVar.v() != btVar.a(i)) {
                return super.a(btVar, i);
            }
            String optString = btVar.v().optString("status");
            boolean z = false;
            if (TextUtils.isEmpty(optString)) {
                return false;
            }
            AdNetwork c = q().c(optString);
            if (c != null && c.isVideoShowing()) {
                z = true;
            }
            return z;
        }

        /* access modifiers changed from: protected */
        public void e(Context context) {
            ao.a().b.a(context, new a());
        }

        /* access modifiers changed from: protected */
        public String g() {
            return "video_disabled";
        }

        public boolean r() {
            return ao.a().b();
        }
    }

    @VisibleForTesting
    static class c extends bc<br, bt> {
        c() {
            super(ao.a().b);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void c(bt btVar) {
            bd.a(btVar, 0, false, true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void d(bt btVar, br brVar) {
            super.d(btVar, brVar);
            if (btVar.v() == brVar.getJsonData()) {
                btVar.b((JSONObject) null);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void c(bt btVar, br brVar, LoadingError loadingError) {
            super.c(btVar, brVar, loadingError);
            ao.a().b.b();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(bt btVar, br brVar, boolean z) {
            super.b(btVar, brVar, z);
            if (!brVar.h() && e(btVar, brVar)) {
                bd.a(Appodeal.e, new l(this.a.t()));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean j(bt btVar, br brVar, Object obj) {
            return false;
        }

        /* access modifiers changed from: 0000 */
        public void b(bt btVar) {
            bd.a(btVar, 0, false, false);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: b */
        public void q(bt btVar, br brVar) {
            brVar.b().setVideoShowing(true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void a(@Nullable bt btVar, @Nullable br brVar, @Nullable LoadingError loadingError) {
            super.a(btVar, brVar, loadingError);
            al.a();
            if (btVar != null && !this.a.x().isEmpty()) {
                ao.a().b.b();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void e(bt btVar) {
            if (!btVar.a() && ao.a().b()) {
                bt btVar2 = (bt) this.a.y();
                if (btVar2 == null || btVar2.M()) {
                    this.a.d(Appodeal.f);
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void n(bt btVar, br brVar) {
            bd.d().b();
            al.a();
            this.a.d(null);
            brVar.b().setVideoShowing(false);
            f(btVar);
        }

        /* access modifiers changed from: protected */
        public boolean c() {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean a(bt btVar) {
            return btVar.v() == null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public boolean e(bt btVar, br brVar) {
            return !e(btVar, brVar) && ((btVar.y() > 0 && btVar.a(0) == btVar.v()) || super.e(btVar, brVar));
        }

        /* access modifiers changed from: protected */
        public boolean e(bt btVar, br brVar) {
            return btVar.u();
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public boolean l(bt btVar, br brVar) {
            return btVar.v() == null || (brVar != null && btVar.v().optString("id").equals(brVar.getId()));
        }
    }

    static p<br, bt, a> a() {
        if (b == null) {
            b = new b(b());
        }
        return b;
    }

    static void a(bt btVar, int i, boolean z, boolean z2) {
        a().a(btVar, i, z2, z);
    }

    static boolean a(Activity activity, l lVar) {
        return d().a(activity, lVar, a());
    }

    static q<br, bt, Object> b() {
        if (a == null) {
            a = new c();
        }
        return a;
    }

    /* access modifiers changed from: private */
    public static bs<bt, br> d() {
        if (c == null) {
            c = new bs<>("debug_video");
        }
        return c;
    }
}
