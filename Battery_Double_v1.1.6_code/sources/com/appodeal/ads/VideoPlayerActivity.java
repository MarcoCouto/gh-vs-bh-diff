package com.appodeal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.VideoView;
import com.appodeal.ads.utils.Log;
import com.explorestack.iab.utils.Assets;
import com.explorestack.iab.vast.view.CircleCountdownView;

public class VideoPlayerActivity extends Activity implements OnCompletionListener, OnErrorListener, OnPreparedListener {
    private int a;
    private VideoView b;
    private a c;

    interface a {
        void a(int i, boolean z);
    }

    public static Intent a(Context context, String str, int i) {
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.putExtra("com.appodeal.ads.fileUri", str);
        intent.putExtra("com.appodeal.ads.seekTo", i);
        return intent;
    }

    private void a() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    VideoPlayerActivity.this.getWindow().clearFlags(128);
                } catch (Exception e) {
                    Log.log(e);
                }
            }
        });
        finish();
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.c != null) {
            this.c.a(this.b.isPlaying() ? this.b.getCurrentPosition() : 0, false);
        }
        a();
    }

    public void onBackPressed() {
        b();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        if (this.c != null) {
            this.c.a(0, true);
        }
        a();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(128);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("com.appodeal.ads.fileUri");
        this.a = intent.getIntExtra("com.appodeal.ads.seekTo", 0);
        Log.log("VideoPlayerActivity", "Start", String.format("position: %s", new Object[]{Integer.valueOf(this.a)}));
        if (stringExtra != null) {
            this.c = as.d;
            RelativeLayout relativeLayout = new RelativeLayout(this);
            relativeLayout.setLayoutParams(new LayoutParams(-1, -1));
            this.b = new VideoView(this);
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.addRule(13);
            this.b.setLayoutParams(layoutParams);
            this.b.setOnCompletionListener(this);
            this.b.setOnPreparedListener(this);
            this.b.setVideoPath(stringExtra);
            relativeLayout.addView(this.b);
            CircleCountdownView circleCountdownView = new CircleCountdownView(this, Assets.mainAssetsColor, Assets.backgroundColor);
            int round = Math.round(bq.h(this) * 50.0f);
            LayoutParams layoutParams2 = new LayoutParams(round, round);
            layoutParams2.addRule(11, -1);
            layoutParams2.addRule(10, -1);
            circleCountdownView.setImage(Assets.getBitmapFromBase64(Assets.close));
            circleCountdownView.setLayoutParams(layoutParams2);
            circleCountdownView.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    VideoPlayerActivity.this.b();
                }
            });
            relativeLayout.addView(circleCountdownView);
            setContentView(relativeLayout);
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        a();
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        if (this.b != null && this.b.canSeekForward()) {
            this.b.seekTo(this.a);
            this.b.start();
        }
    }
}
