package com.appodeal.ads;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.appodeal.ads.m;
import com.appodeal.ads.unified.UnifiedFullscreenAd;
import com.appodeal.ads.unified.UnifiedFullscreenAdCallback;
import com.appodeal.ads.unified.UnifiedFullscreenAdParams;

abstract class ak<AdRequestType extends m, UnifiedAdType extends UnifiedFullscreenAd, UnifiedAdParamsType extends UnifiedFullscreenAdParams, UnifiedAdCallbackType extends UnifiedFullscreenAdCallback> extends i<AdRequestType, UnifiedAdType, UnifiedAdParamsType, UnifiedAdCallbackType> {
    ak(@NonNull AdRequestType adrequesttype, @NonNull AdNetwork adNetwork, @NonNull bn bnVar, int i) {
        super(adrequesttype, adNetwork, bnVar, i);
    }

    public void b(Activity activity) {
        UnifiedFullscreenAd unifiedFullscreenAd = (UnifiedFullscreenAd) n();
        UnifiedFullscreenAdCallback unifiedFullscreenAdCallback = (UnifiedFullscreenAdCallback) p();
        if (unifiedFullscreenAd != null && unifiedFullscreenAdCallback != null) {
            unifiedFullscreenAd.show(activity, unifiedFullscreenAdCallback);
        } else if (unifiedFullscreenAdCallback != null) {
            unifiedFullscreenAdCallback.onAdShowFailed();
        }
    }
}
