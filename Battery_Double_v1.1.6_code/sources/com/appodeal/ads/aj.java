package com.appodeal.ads;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.util.Pair;
import com.appodeal.ads.utils.b.b;
import com.appodeal.ads.utils.b.c;
import com.appodeal.ads.utils.d;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.places.model.PlaceFields;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TapjoyConstants;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class aj {
    public static String b = "off";
    public static String c = "all";
    public static String d = "fatal";
    @VisibleForTesting
    final c a;
    private final String e;
    private JSONObject f;
    private final long g;
    private Future<?> h;
    private ExecutorService i;
    /* access modifiers changed from: private */
    public UncaughtExceptionHandler j;

    private static class a {
        static final aj a = new aj();
    }

    private aj() {
        this.e = "ExceptionsHandler";
        this.g = System.currentTimeMillis();
        this.a = new c();
        try {
            Context context = Appodeal.f;
            if (context != null && this.a.c(context)) {
                c();
            }
        } catch (Exception e2) {
            Log.e("ExceptionsHandler", e2.toString());
        }
    }

    public static aj a() {
        return a.a;
    }

    /* access modifiers changed from: private */
    public synchronized void a(Throwable th, boolean z) {
        try {
            Context context = Appodeal.f;
            if (context != null && this.a.c(context) && (!this.a.d(context) || z)) {
                JSONObject jSONObject = new JSONObject(f().toString());
                jSONObject.put(Param.FATAL, z);
                JSONArray jSONArray = new JSONArray();
                while (th != null) {
                    jSONArray.put(b(th));
                    th = th.getCause();
                }
                jSONObject.put("errors", jSONArray);
                a(jSONObject, context);
                this.a.a(context, jSONObject.toString());
                if (!z) {
                    b();
                }
            }
        } catch (Exception e2) {
            Log.e("ExceptionsHandler", e2.toString());
        }
        return;
    }

    private void a(JSONObject jSONObject, Context context) {
        try {
            ConnectionData connectionData = bg.a.getConnectionData(context);
            if (connectionData != null) {
                jSONObject.put("connection", connectionData.type);
            }
            jSONObject.put("ram_current", bq.m(context));
            jSONObject.put("disk_current", bq.e());
            jSONObject.put("battery", (double) bq.i(context));
            jSONObject.put("running_time", g());
            jSONObject.put("orientation", bq.n(context));
            jSONObject.put("online", bq.a(context));
            jSONObject.put("muted", bq.o(context));
            jSONObject.put("background", Appodeal.d);
            jSONObject.put("timestamp", System.currentTimeMillis());
        } catch (Exception e2) {
            Log.e("ExceptionsHandler", e2.toString());
        }
    }

    private JSONObject b(Throwable th) {
        if (th == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE, th.getLocalizedMessage());
            jSONObject.put("error_name", th.getClass().getName());
            JSONArray jSONArray = new JSONArray();
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace.length;
            for (int i2 = 0; i2 < length; i2++) {
                StackTraceElement stackTraceElement = stackTrace[i2];
                JSONObject jSONObject2 = new JSONObject();
                StringBuilder sb = new StringBuilder();
                sb.append(stackTraceElement.getClassName());
                sb.append(".");
                sb.append(stackTraceElement.getMethodName());
                jSONObject2.put("method", sb.toString());
                jSONObject2.put(ParametersKeys.FILE, stackTraceElement.getFileName() == null ? "Unknown" : stackTraceElement.getFileName());
                jSONObject2.put("line_number", stackTraceElement.getLineNumber());
                jSONArray.put(jSONObject2);
            }
            jSONObject.put("error_trace", jSONArray);
        } catch (Exception e2) {
            Log.e("ExceptionsHandler", e2.toString());
        }
        return jSONObject;
    }

    private void c() {
        this.j = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable th) {
                th.printStackTrace(new PrintWriter(new StringWriter()));
                aj.this.a(th, true);
                if (aj.this.j != null && !aj.this.j.equals(this)) {
                    try {
                        aj.this.j.uncaughtException(thread, th);
                    } catch (Exception unused) {
                    }
                }
            }
        });
    }

    private void d() {
        if (this.j != null) {
            Thread.setDefaultUncaughtExceptionHandler(this.j);
        }
    }

    private void e() {
        if (this.i == null) {
            this.i = Executors.newSingleThreadExecutor();
        }
    }

    private JSONObject f() {
        JSONObject jSONObject;
        String str;
        String str2;
        if (this.f != null) {
            return this.f;
        }
        this.f = new JSONObject();
        try {
            this.f.put("sdk", "2.6.4");
            String packageName = Appodeal.f.getPackageName();
            this.f.put("package", packageName);
            try {
                PackageInfo packageInfo = Appodeal.f.getPackageManager().getPackageInfo(packageName, 0);
                this.f.put("package_version", packageInfo.versionName);
                this.f.put("package_code", packageInfo.versionCode);
                if (Appodeal.frameworkName != null) {
                    this.f.put("framework", Appodeal.frameworkName);
                }
                if (Appodeal.pluginVersion != null) {
                    this.f.put("plugin_version", Appodeal.pluginVersion);
                }
            } catch (Exception e2) {
                Log.e("ExceptionsHandler", e2.toString());
            }
            this.f.put("idfa", bg.a.getIfa());
            this.f.put("android_level", VERSION.SDK_INT);
            this.f.put("model", Build.MODEL);
            this.f.put("manufacturer", Build.MANUFACTURER);
            if (bq.k(Appodeal.f)) {
                jSONObject = this.f;
                str = TapjoyConstants.TJC_DEVICE_TYPE_NAME;
                str2 = "tablet";
            } else {
                jSONObject = this.f;
                str = TapjoyConstants.TJC_DEVICE_TYPE_NAME;
                str2 = PlaceFields.PHONE;
            }
            jSONObject.put(str, str2);
            this.f.put(TapjoyConstants.TJC_PLATFORM, d.a);
            this.f.put("os", Constants.JAVASCRIPT_INTERFACE_NAME);
            this.f.put(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
            Pair e3 = bq.e(Appodeal.f);
            this.f.put("width", e3.first);
            this.f.put("height", e3.second);
            this.f.put("cpu", bq.c());
            this.f.put("opengl", bq.l(Appodeal.f));
            this.f.put("ram_total", bq.d());
            this.f.put("disk_total", bq.f());
            this.f.put("root", bq.b());
        } catch (JSONException e4) {
            Log.e("ExceptionsHandler", e4.toString());
        }
        return this.f;
    }

    private Long g() {
        return Long.valueOf(System.currentTimeMillis() - this.g);
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        if (!this.a.g(Appodeal.f).equals(str)) {
            this.a.c(Appodeal.f, str);
            if (str.equals(b)) {
                d();
                this.a.e(Appodeal.f);
                return;
            }
            c();
        }
    }

    public synchronized void a(Throwable th) {
        try {
            Context context = Appodeal.f;
            if (context != null && this.a.c(context)) {
                th.printStackTrace(new PrintWriter(new StringWriter()));
                a(th, false);
            }
        } catch (Exception e2) {
            Log.e("ExceptionsHandler", e2.toString());
        }
        return;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        Context context = Appodeal.f;
        if (context != null && bq.a(context) && this.a.f(context) && !this.a.b(context)) {
            if (this.h == null || this.h.isDone()) {
                e();
                this.h = this.i.submit(new b(context, this.a));
            }
        }
    }
}
