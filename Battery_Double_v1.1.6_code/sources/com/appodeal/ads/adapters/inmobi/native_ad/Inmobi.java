package com.appodeal.ads.adapters.inmobi.native_ad;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.Native.NativeAdType;
import com.appodeal.ads.NativeMediaView;
import com.appodeal.ads.adapters.inmobi.InmobiNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.utils.Log;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiNative;
import com.inmobi.ads.listeners.NativeAdEventListener;
import org.json.JSONObject;

public class Inmobi extends UnifiedNative<RequestParams> {
    @VisibleForTesting
    InMobiNative inMobiNative;

    private class Listener extends NativeAdEventListener {
        private final UnifiedNativeCallback callback;
        private final UnifiedNativeParams params;

        private Listener(@NonNull UnifiedNativeParams unifiedNativeParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) {
            this.params = unifiedNativeParams;
            this.callback = unifiedNativeCallback;
        }

        public void onAdLoadSucceeded(@NonNull InMobiNative inMobiNative) {
            String str = null;
            try {
                JSONObject customAdContent = inMobiNative.getCustomAdContent();
                if (customAdContent != null) {
                    JSONObject optJSONObject = customAdContent.optJSONObject("screenshots");
                    if (optJSONObject != null && optJSONObject.has("url")) {
                        str = optJSONObject.getString("url");
                    }
                }
                this.callback.onAdLoaded(new NativeAdAdapter(this.params, inMobiNative, str));
            } catch (Exception e) {
                Log.log(e);
                this.callback.onAdLoadFailed(LoadingError.InternalError);
            }
        }

        public void onAdLoadFailed(InMobiNative inMobiNative, InMobiAdRequestStatus inMobiAdRequestStatus) {
            if (inMobiAdRequestStatus != null) {
                this.callback.printError(inMobiAdRequestStatus.getMessage(), inMobiAdRequestStatus.getStatusCode());
            }
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdClicked(@NonNull InMobiNative inMobiNative) {
            this.callback.onAdClicked();
        }
    }

    @VisibleForTesting
    static class NativeAdAdapter extends UnifiedNativeAd {
        private final InMobiNative inMobiNative;
        private final UnifiedNativeParams params;

        NativeAdAdapter(@NonNull UnifiedNativeParams unifiedNativeParams, @NonNull InMobiNative inMobiNative2, String str) {
            super(inMobiNative2.getAdTitle(), inMobiNative2.getAdDescription(), inMobiNative2.getAdCtaText(), str, inMobiNative2.getAdIconUrl(), inMobiNative2.getAdRating() != 0.0f ? Float.valueOf(inMobiNative2.getAdRating()) : null);
            this.params = unifiedNativeParams;
            this.inMobiNative = inMobiNative2;
            setClickUrl(inMobiNative2.getAdLandingPageUrl());
        }

        public void onAdClick(@Nullable View view) {
            super.onAdClick(view);
            this.inMobiNative.reportAdClickAndOpenLandingPage();
        }

        public boolean onConfigureMediaView(@NonNull NativeMediaView nativeMediaView) {
            if (this.params.getNativeAdType() == NativeAdType.NoVideo || !hasVideo()) {
                return super.onConfigureMediaView(nativeMediaView);
            }
            nativeMediaView.addView(this.inMobiNative.getPrimaryViewOfWidth(nativeMediaView.getContext(), nativeMediaView, nativeMediaView, nativeMediaView.getWidth()));
            return true;
        }

        public boolean hasVideo() {
            JSONObject customAdContent = this.inMobiNative.getCustomAdContent();
            if (customAdContent != null) {
                return customAdContent.optBoolean("isVideo");
            }
            return super.hasVideo();
        }

        public boolean containsVideo() {
            return hasVideo();
        }

        public void onDestroy() {
            super.onDestroy();
            this.inMobiNative.destroy();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        this.inMobiNative = new InMobiNative((Context) activity, requestParams.placement, createListener(unifiedNativeParams, unifiedNativeCallback));
        this.inMobiNative.setExtras(RequestParams.extras);
        this.inMobiNative.load();
    }

    public void onDestroy() {
        if (this.inMobiNative != null) {
            this.inMobiNative = null;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public NativeAdEventListener createListener(@NonNull UnifiedNativeParams unifiedNativeParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) {
        return new Listener(unifiedNativeParams, unifiedNativeCallback);
    }
}
