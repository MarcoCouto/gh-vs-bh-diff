package com.appodeal.ads.adapters.inmobi.video;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inmobi.InmobiNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedVideo;
import com.appodeal.ads.unified.UnifiedVideoCallback;
import com.appodeal.ads.unified.UnifiedVideoParams;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiInterstitial;
import com.inmobi.ads.listeners.InterstitialAdEventListener;
import java.util.Map;

public class Inmobi extends UnifiedVideo<RequestParams> {
    @VisibleForTesting
    InMobiInterstitial inMobiInterstitial;

    @VisibleForTesting
    static final class Listener extends InterstitialAdEventListener {
        private final UnifiedVideoCallback callback;

        Listener(@NonNull UnifiedVideoCallback unifiedVideoCallback) {
            this.callback = unifiedVideoCallback;
        }

        public void onAdLoadSucceeded(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdLoaded();
        }

        public void onAdLoadFailed(InMobiInterstitial inMobiInterstitial, InMobiAdRequestStatus inMobiAdRequestStatus) {
            if (inMobiAdRequestStatus != null) {
                this.callback.printError(inMobiAdRequestStatus.getMessage(), inMobiAdRequestStatus.getStatusCode());
            }
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdDisplayed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdShown();
        }

        public void onAdDisplayFailed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdShowFailed();
        }

        public void onAdClicked(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map) {
            this.callback.onAdClicked();
        }

        public void onAdDismissed(InMobiInterstitial inMobiInterstitial) {
            this.callback.onAdClosed();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedVideoParams unifiedVideoParams, @NonNull RequestParams requestParams, @NonNull UnifiedVideoCallback unifiedVideoCallback) throws Exception {
        this.inMobiInterstitial = new InMobiInterstitial((Context) activity, requestParams.placement, (InterstitialAdEventListener) new Listener(unifiedVideoCallback));
        this.inMobiInterstitial.setExtras(RequestParams.extras);
        this.inMobiInterstitial.load();
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedVideoCallback unifiedVideoCallback) {
        if (this.inMobiInterstitial == null || !this.inMobiInterstitial.isReady()) {
            unifiedVideoCallback.onAdShowFailed();
        } else {
            this.inMobiInterstitial.show();
        }
    }

    public void onDestroy() {
        if (this.inMobiInterstitial != null) {
            this.inMobiInterstitial = null;
        }
    }
}
