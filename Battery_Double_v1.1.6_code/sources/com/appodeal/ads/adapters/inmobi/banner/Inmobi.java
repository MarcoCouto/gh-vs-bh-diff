package com.appodeal.ads.adapters.inmobi.banner;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.view.ViewGroup.LayoutParams;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inmobi.InmobiNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiBanner;
import com.inmobi.ads.listeners.BannerAdEventListener;
import java.util.Map;

public class Inmobi extends UnifiedBanner<RequestParams> {
    @VisibleForTesting
    InMobiBanner inMobiBanner;

    @VisibleForTesting
    static final class Listener extends BannerAdEventListener {
        private final UnifiedBannerCallback callback;

        Listener(@NonNull UnifiedBannerCallback unifiedBannerCallback) {
            this.callback = unifiedBannerCallback;
        }

        public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
            if (inMobiBanner == null || inMobiBanner.getChildCount() == 0) {
                this.callback.onAdLoadFailed(LoadingError.InvalidAssets);
            } else {
                this.callback.onAdLoaded(inMobiBanner, ModuleDescriptor.MODULE_VERSION, 50);
            }
        }

        public void onAdLoadFailed(InMobiBanner inMobiBanner, InMobiAdRequestStatus inMobiAdRequestStatus) {
            if (inMobiAdRequestStatus != null) {
                this.callback.printError(inMobiAdRequestStatus.getMessage(), inMobiAdRequestStatus.getStatusCode());
            }
            this.callback.onAdLoadFailed(LoadingError.NoFill);
        }

        public void onAdClicked(InMobiBanner inMobiBanner, Map<Object, Object> map) {
            this.callback.onAdClicked();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        this.inMobiBanner = new InMobiBanner((Context) new ContextWrapper(activity), requestParams.placement);
        this.inMobiBanner.setLayoutParams(new LayoutParams(Math.round(UnifiedAdUtils.getScreenDensity(activity) * 320.0f), Math.round(UnifiedAdUtils.getScreenDensity(activity) * 50.0f)));
        this.inMobiBanner.setEnableAutoRefresh(false);
        this.inMobiBanner.setListener((BannerAdEventListener) new Listener(unifiedBannerCallback));
        this.inMobiBanner.setExtras(RequestParams.extras);
        this.inMobiBanner.load();
    }

    public void onDestroy() {
        if (this.inMobiBanner != null) {
            this.inMobiBanner = null;
        }
    }
}
