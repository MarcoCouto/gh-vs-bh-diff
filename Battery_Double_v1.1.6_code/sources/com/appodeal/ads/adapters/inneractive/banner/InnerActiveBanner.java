package com.appodeal.ads.adapters.inneractive.banner;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inneractive.InnerActiveAdTask;
import com.appodeal.ads.adapters.inneractive.InnerActiveNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidBanner;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams.Builder;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

public class InnerActiveBanner extends UnifiedMraidBanner<RequestParams> {
    private Integer speedLimit;

    @Nullable
    public UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) {
        this.speedLimit = Integer.valueOf(requestParams.speedLimit);
        int optInt = requestParams.jsonData.optInt("width", ModuleDescriptor.MODULE_VERSION);
        return requestParams.toMraidNetworkParamsBuilder().setWidth(optInt).setHeight(requestParams.jsonData.optInt("height", 50)).build();
    }

    public void requestMraid(@NonNull Context context, @NonNull final UnifiedBannerParams unifiedBannerParams, @NonNull final UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull final UnifiedBannerCallback unifiedBannerCallback, @NonNull String str) {
        if (unifiedMraidNetworkParams.width > unifiedBannerParams.getMaxWidth(context) || unifiedMraidNetworkParams.height > unifiedBannerParams.getMaxHeight(context)) {
            unifiedBannerCallback.onAdLoadFailed(LoadingError.IncorrectAdunit);
            return;
        }
        InnerActiveAdTask.request(context, str, unifiedMraidNetworkParams.restrictedData, new Callback<String>() {
            public void onSuccess(@NonNull Context context, String str) {
                InnerActiveBanner.this.loadMraid(context, unifiedBannerParams, new Builder(unifiedMraidNetworkParams).setAdm(str).build(), unifiedBannerCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedBannerCallback.onAdLoadFailed(loadingError);
            }
        }, this.speedLimit);
    }
}
