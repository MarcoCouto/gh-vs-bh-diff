package com.appodeal.ads.adapters.inneractive;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.RestrictedData;
import com.appodeal.ads.unified.tasks.AdParamsProcessorCallback;
import com.appodeal.ads.unified.tasks.AdParamsResolver;
import com.appodeal.ads.unified.tasks.AdParamsResolverCallback;
import com.appodeal.ads.unified.tasks.AdResponseProcessor;
import com.appodeal.ads.unified.tasks.S2SAdTask;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;
import com.appodeal.ads.utils.Log;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.net.URLEncoder;

public class InnerActiveAdTask extends S2SAdTask<String, String> {
    @Nullable
    private Integer speedLimit;

    @VisibleForTesting
    static final class InnerActiveParamsResolver implements AdParamsResolver<String, String> {
        InnerActiveParamsResolver() {
        }

        public void processResponse(@NonNull String str, @NonNull AdParamsResolverCallback<String> adParamsResolverCallback) throws Exception {
            adParamsResolverCallback.onResolveSuccess(str);
        }
    }

    @VisibleForTesting
    static final class InnerActiveResponseProcessor implements AdResponseProcessor<String> {
        static final /* synthetic */ boolean $assertionsDisabled = false;

        static {
            Class<InnerActiveAdTask> cls = InnerActiveAdTask.class;
        }

        InnerActiveResponseProcessor() {
        }

        public void processResponse(@NonNull URLConnection uRLConnection, @Nullable String str, @NonNull AdParamsProcessorCallback<String> adParamsProcessorCallback) throws Exception {
            if ("NoAd".equals(str) || TextUtils.isEmpty(str) || TextUtils.getTrimmedLength(str) == 0) {
                adParamsProcessorCallback.onProcessFail(LoadingError.NoFill);
            } else {
                adParamsProcessorCallback.onProcessSuccess(str);
            }
        }
    }

    @VisibleForTesting
    InnerActiveAdTask(@NonNull Context context, @Nullable String str, @NonNull RestrictedData restrictedData, @NonNull AdResponseProcessor<String> adResponseProcessor, @NonNull AdParamsResolver<String, String> adParamsResolver, @Nullable Callback<String> callback, @Nullable Integer num) {
        super(context, str, restrictedData, adResponseProcessor, adParamsResolver, callback);
        this.speedLimit = num;
    }

    public static void request(@NonNull Context context, @NonNull String str, @NonNull RestrictedData restrictedData, @Nullable Callback<String> callback, @Nullable Integer num) {
        AnonymousClass1 r0 = new InnerActiveAdTask(context, str, restrictedData, new InnerActiveResponseProcessor(), new InnerActiveParamsResolver(), callback, num) {
        };
        r0.start();
    }

    /* access modifiers changed from: protected */
    public void prepareUrlConnection(Context context, HttpURLConnection httpURLConnection) throws Exception {
        super.prepareUrlConnection(context, httpURLConnection);
        String httpAgent = this.restrictedData.getHttpAgent(context);
        if (httpAgent != null) {
            httpURLConnection.setRequestProperty("User-Agent", URLEncoder.encode(httpAgent, "utf-8"));
        }
        String ip = this.restrictedData.getIp();
        if (ip != null) {
            httpURLConnection.setRequestProperty("IP", ip);
        }
    }

    /* access modifiers changed from: protected */
    public void processServerResult(@NonNull URLConnection uRLConnection) throws Exception {
        if (this.speedLimit == null || this.speedLimit.intValue() <= 0) {
            super.processServerResult(uRLConnection);
        } else {
            Pair readURLConnectionResponseAndSpeed = readURLConnectionResponseAndSpeed(uRLConnection.getInputStream());
            if (readURLConnectionResponseAndSpeed == null) {
                notifyFail(LoadingError.InternalError);
                return;
            }
            Integer num = (Integer) readURLConnectionResponseAndSpeed.second;
            if ("NoAd".equals(readURLConnectionResponseAndSpeed.first)) {
                notifyFail(LoadingError.NoFill);
            } else if (num.intValue() < this.speedLimit.intValue()) {
                notifyFail(LoadingError.Canceled);
            } else {
                processServerResult(uRLConnection, (String) readURLConnectionResponseAndSpeed.first);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005a A[SYNTHETIC, Splitter:B:25:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0066 A[SYNTHETIC, Splitter:B:32:0x0066] */
    @VisibleForTesting
    public Pair<String, Integer> readURLConnectionResponseAndSpeed(InputStream inputStream) {
        BufferedReader bufferedReader;
        StringBuilder sb = new StringBuilder();
        try {
            long currentTimeMillis = System.currentTimeMillis();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            int i = 0;
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    sb.append(readLine);
                    i += readLine.getBytes().length;
                } catch (Exception e) {
                    e = e;
                    try {
                        Log.log(e);
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (Exception e2) {
                                Log.log(e2);
                            }
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (Exception e3) {
                                Log.log(e3);
                            }
                        }
                        throw th;
                    }
                }
            }
            long currentTimeMillis2 = System.currentTimeMillis();
            if (currentTimeMillis2 == currentTimeMillis) {
                currentTimeMillis2 = 1 + currentTimeMillis;
            }
            Pair<String, Integer> pair = new Pair<>(sb.toString(), Integer.valueOf(Math.round((float) (((long) i) / (currentTimeMillis2 - currentTimeMillis)))));
            try {
                bufferedReader.close();
            } catch (Exception e4) {
                Log.log(e4);
            }
            return pair;
        } catch (Exception e5) {
            e = e5;
            bufferedReader = null;
            Log.log(e);
            if (bufferedReader != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            if (bufferedReader != null) {
            }
            throw th;
        }
    }
}
