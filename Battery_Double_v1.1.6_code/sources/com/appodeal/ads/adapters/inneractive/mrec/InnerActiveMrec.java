package com.appodeal.ads.adapters.inneractive.mrec;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.adapters.inneractive.InnerActiveAdTask;
import com.appodeal.ads.adapters.inneractive.InnerActiveNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidMrec;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams;
import com.appodeal.ads.unified.mraid.UnifiedMraidNetworkParams.Builder;
import com.appodeal.ads.unified.tasks.S2SAdTask.Callback;

public class InnerActiveMrec extends UnifiedMraidMrec<RequestParams> {
    private Integer speedLimit;

    @Nullable
    public UnifiedMraidNetworkParams obtainMraidParams(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) {
        this.speedLimit = Integer.valueOf(requestParams.speedLimit);
        return requestParams.toMraidNetworkParamsBuilder().setWidth(300).setHeight(250).build();
    }

    public void requestMraid(@NonNull Context context, @NonNull final UnifiedMrecParams unifiedMrecParams, @NonNull final UnifiedMraidNetworkParams unifiedMraidNetworkParams, @NonNull final UnifiedMrecCallback unifiedMrecCallback, @NonNull String str) {
        InnerActiveAdTask.request(context, str, unifiedMraidNetworkParams.restrictedData, new Callback<String>() {
            public void onSuccess(@NonNull Context context, String str) {
                InnerActiveMrec.this.loadMraid(context, unifiedMrecParams, new Builder(unifiedMraidNetworkParams).setAdm(str).build(), unifiedMrecCallback);
            }

            public void onFail(@Nullable LoadingError loadingError) {
                unifiedMrecCallback.onAdLoadFailed(loadingError);
            }
        }, this.speedLimit);
    }
}
