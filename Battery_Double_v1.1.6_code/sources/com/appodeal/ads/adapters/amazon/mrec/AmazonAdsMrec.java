package com.appodeal.ads.adapters.amazon.mrec;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.FrameLayout.LayoutParams;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdSize;
import com.appodeal.ads.adapters.amazon.AmazonAdsNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedMrecCallback;
import com.appodeal.ads.unified.UnifiedMrecParams;

public class AmazonAdsMrec extends UnifiedMrec<RequestParams> {
    private AdLayout adView;

    public void load(@NonNull Activity activity, @NonNull UnifiedMrecParams unifiedMrecParams, @NonNull RequestParams requestParams, @NonNull UnifiedMrecCallback unifiedMrecCallback) throws Exception {
        this.adView = new AdLayout((Context) activity, AdSize.SIZE_300x250);
        this.adView.setListener(new AmazonAdsMrecListener(this.adView, unifiedMrecCallback));
        this.adView.setLayoutParams(new LayoutParams(-2, -2));
        this.adView.loadAd(requestParams.targetingOptions);
    }

    public void onDestroy() {
        if (this.adView != null) {
            this.adView.destroy();
            this.adView = null;
        }
    }
}
