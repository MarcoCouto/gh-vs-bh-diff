package com.appodeal.ads.adapters.amazon.banner;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup.LayoutParams;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdSize;
import com.appodeal.ads.adapters.amazon.AmazonAdsNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;

public class AmazonAdsBanner extends UnifiedBanner<RequestParams> {
    private AdLayout bannerView;

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        int i;
        if (unifiedBannerParams.needLeaderBoard(activity)) {
            this.bannerView = new AdLayout((Context) activity, AdSize.SIZE_728x90.disableScaling());
            i = 90;
        } else {
            this.bannerView = new AdLayout((Context) activity, AdSize.SIZE_320x50.disableScaling());
            i = 50;
        }
        this.bannerView.setListener(new AmazonAdsBannerListener(this.bannerView, unifiedBannerCallback, i));
        this.bannerView.setLayoutParams(new LayoutParams(-2, -2));
        this.bannerView.loadAd(requestParams.targetingOptions);
    }

    public void onDestroy() {
        if (this.bannerView != null) {
            this.bannerView.destroy();
            this.bannerView = null;
        }
    }
}
