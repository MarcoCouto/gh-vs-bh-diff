package com.appodeal.ads.adapters.facebook;

import android.app.Activity;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.appodeal.ads.AdNetwork;
import com.appodeal.ads.AdNetworkBuilder;
import com.appodeal.ads.AdNetworkMediationParams;
import com.appodeal.ads.AdUnit;
import com.appodeal.ads.AppodealNetworks;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.NetworkInitializationListener;
import com.appodeal.ads.adapters.facebook.FacebookInitializer.FacebookInitializationListener;
import com.appodeal.ads.adapters.facebook.banner.Facebook;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedMrec;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.utils.ActivityRule;
import com.appodeal.ads.utils.ActivityRule.Builder;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AdSettings.TestAdType;
import com.facebook.ads.BuildConfig;

public class FacebookNetwork extends AdNetwork<RequestParams> {
    private static final FacebookInitializer initializer = new FacebookInitializer();

    public static final class RequestParams {
        public final String facebookKey;

        RequestParams(String str) {
            this.facebookKey = str;
        }
    }

    public static class builder extends AdNetworkBuilder {
        public String getAdapterVersion() {
            return "3";
        }

        public String getName() {
            return AppodealNetworks.FACEBOOK;
        }

        public String[] getRequiredPermissions() {
            return new String[]{"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"};
        }

        public ActivityRule[] getAdActivityRules() {
            return new ActivityRule[]{new Builder("com.facebook.ads.AudienceNetworkActivity").build()};
        }

        public String[] getRequiredClasses() {
            return new String[]{"com.facebook.ads.AdView", "com.facebook.ads.NativeAd", "com.facebook.ads.NativeBannerAd", "com.facebook.ads.InterstitialAd", "com.facebook.ads.RewardedVideoAd"};
        }

        public String[] getRequiredProvidersClassName() {
            return new String[]{"com.facebook.ads.AudienceNetworkContentProvider"};
        }

        public FacebookNetwork build() {
            return new FacebookNetwork(this);
        }
    }

    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public boolean isSupportSmartBanners() {
        return true;
    }

    private FacebookNetwork(AdNetworkBuilder adNetworkBuilder) {
        super(adNetworkBuilder);
    }

    @Nullable
    public UnifiedBanner<RequestParams> createBanner() {
        return new Facebook();
    }

    @Nullable
    public UnifiedMrec<RequestParams> createMrec() {
        return new com.appodeal.ads.adapters.facebook.mrec.Facebook();
    }

    @Nullable
    public UnifiedInterstitial<RequestParams> createInterstitial() {
        return new com.appodeal.ads.adapters.facebook.interstitial.Facebook();
    }

    @Nullable
    public UnifiedRewarded<RequestParams> createRewarded() {
        return new com.appodeal.ads.adapters.facebook.rewarded_video.Facebook();
    }

    @Nullable
    public UnifiedNative<RequestParams> createNativeAd() {
        return new com.appodeal.ads.adapters.facebook.native_ad.Facebook();
    }

    /* access modifiers changed from: protected */
    public void initialize(@NonNull Activity activity, @NonNull AdUnit adUnit, @NonNull AdNetworkMediationParams adNetworkMediationParams, @NonNull final NetworkInitializationListener<RequestParams> networkInitializationListener) throws Exception {
        if (VERSION.SDK_INT < 15) {
            networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
            return;
        }
        final RequestParams requestParams = new RequestParams(adUnit.getJsonData().getString("facebook_key"));
        AdSettings.setIsChildDirected(adNetworkMediationParams.getRestrictedData().isUserAgeRestricted());
        AdSettings.setTestMode(adNetworkMediationParams.isTestMode());
        if (adNetworkMediationParams.isTestMode()) {
            AdSettings.setTestAdType(TestAdType.DEFAULT);
        }
        if (!initializer.isInitialized()) {
            initializer.init(activity, adUnit.getMediatorName(), new FacebookInitializationListener() {
                public void onInitialized() {
                    try {
                        networkInitializationListener.onInitializationFinished(requestParams);
                    } catch (Exception unused) {
                        networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
                    }
                }

                public void onInitializationFailed() {
                    networkInitializationListener.onInitializationFailed(LoadingError.InternalError);
                }
            });
        } else {
            networkInitializationListener.onInitializationFinished(requestParams);
        }
    }

    @Nullable
    public static LoadingError mapError(@Nullable AdError adError) {
        if (adError == null) {
            return null;
        }
        switch (adError.getErrorCode()) {
            case 1000:
                return LoadingError.ConnectionError;
            case 1001:
            case 1002:
            case 2000:
            case 2001:
            case 2002:
            case 3001:
                return LoadingError.NoFill;
            case AdError.INTERSTITIAL_AD_TIMEOUT /*2009*/:
                return LoadingError.TimeoutError;
            case 2100:
            case AdError.ICONVIEW_MISSING_ERROR_CODE /*6002*/:
            case AdError.AD_ASSETS_UNSUPPORTED_TYPE_ERROR_CODE /*6003*/:
                return LoadingError.InvalidAssets;
            case AdError.SHOW_CALLED_BEFORE_LOAD_ERROR_CODE /*7001*/:
            case AdError.LOAD_CALLED_WHILE_SHOWING_AD /*7002*/:
            case AdError.MISSING_DEPENDENCIES_ERROR /*7005*/:
            case AdError.API_NOT_SUPPORTED /*7006*/:
                return LoadingError.InternalError;
            case AdError.NATIVE_AD_IS_NOT_LOADED /*7007*/:
                return LoadingError.IncorrectAdunit;
            default:
                return null;
        }
    }
}
