package com.appodeal.ads.adapters.facebook;

public final class R {

    public static final class attr {
        public static final int buttonSize = 2130968651;
        public static final int circleCrop = 2130968665;
        public static final int colorScheme = 2130968682;
        public static final int font = 2130968751;
        public static final int fontProviderAuthority = 2130968753;
        public static final int fontProviderCerts = 2130968754;
        public static final int fontProviderFetchStrategy = 2130968755;
        public static final int fontProviderFetchTimeout = 2130968756;
        public static final int fontProviderPackage = 2130968757;
        public static final int fontProviderQuery = 2130968758;
        public static final int fontStyle = 2130968759;
        public static final int fontWeight = 2130968761;
        public static final int imageAspectRatio = 2130968777;
        public static final int imageAspectRatioAdjust = 2130968778;
        public static final int scopeUris = 2130968852;

        private attr() {
        }
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2131034112;

        private bool() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131099735;
        public static final int common_google_signin_btn_text_dark_default = 2131099736;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099737;
        public static final int common_google_signin_btn_text_dark_focused = 2131099738;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099739;
        public static final int common_google_signin_btn_text_light = 2131099740;
        public static final int common_google_signin_btn_text_light_default = 2131099741;
        public static final int common_google_signin_btn_text_light_disabled = 2131099742;
        public static final int common_google_signin_btn_text_light_focused = 2131099743;
        public static final int common_google_signin_btn_text_light_pressed = 2131099744;
        public static final int common_google_signin_btn_tint = 2131099745;
        public static final int notification_action_color_filter = 2131099782;
        public static final int notification_icon_bg_color = 2131099783;
        public static final int notification_material_background_media_default_color = 2131099784;
        public static final int primary_text_default_material_dark = 2131099789;
        public static final int ripple_material_light = 2131099794;
        public static final int secondary_text_default_material_dark = 2131099795;
        public static final int secondary_text_default_material_light = 2131099796;

        private color() {
        }
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131165289;
        public static final int compat_button_inset_vertical_material = 2131165290;
        public static final int compat_button_padding_horizontal_material = 2131165291;
        public static final int compat_button_padding_vertical_material = 2131165292;
        public static final int compat_control_corner_material = 2131165293;
        public static final int notification_action_icon_size = 2131165357;
        public static final int notification_action_text_size = 2131165358;
        public static final int notification_big_circle_margin = 2131165359;
        public static final int notification_content_margin_start = 2131165360;
        public static final int notification_large_icon_height = 2131165361;
        public static final int notification_large_icon_width = 2131165362;
        public static final int notification_main_column_padding_top = 2131165363;
        public static final int notification_media_narrow_margin = 2131165364;
        public static final int notification_right_icon_size = 2131165365;
        public static final int notification_right_side_padding_top = 2131165366;
        public static final int notification_small_icon_background_padding = 2131165367;
        public static final int notification_small_icon_size_as_large = 2131165368;
        public static final int notification_subtext_size = 2131165369;
        public static final int notification_top_pad = 2131165370;
        public static final int notification_top_pad_large_text = 2131165371;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131230872;
        public static final int common_google_signin_btn_icon_dark = 2131230873;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230874;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230875;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230876;
        public static final int common_google_signin_btn_icon_disabled = 2131230877;
        public static final int common_google_signin_btn_icon_light = 2131230878;
        public static final int common_google_signin_btn_icon_light_focused = 2131230879;
        public static final int common_google_signin_btn_icon_light_normal = 2131230880;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230881;
        public static final int common_google_signin_btn_text_dark = 2131230882;
        public static final int common_google_signin_btn_text_dark_focused = 2131230883;
        public static final int common_google_signin_btn_text_dark_normal = 2131230884;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230885;
        public static final int common_google_signin_btn_text_disabled = 2131230886;
        public static final int common_google_signin_btn_text_light = 2131230887;
        public static final int common_google_signin_btn_text_light_focused = 2131230888;
        public static final int common_google_signin_btn_text_light_normal = 2131230889;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230890;
        public static final int googleg_disabled_color_18 = 2131230900;
        public static final int googleg_standard_color_18 = 2131230901;
        public static final int notification_action_background = 2131231072;
        public static final int notification_bg = 2131231073;
        public static final int notification_bg_low = 2131231074;
        public static final int notification_bg_low_normal = 2131231075;
        public static final int notification_bg_low_pressed = 2131231076;
        public static final int notification_bg_normal = 2131231077;
        public static final int notification_bg_normal_pressed = 2131231078;
        public static final int notification_icon_background = 2131231079;
        public static final int notification_template_icon_bg = 2131231080;
        public static final int notification_template_icon_low_bg = 2131231081;
        public static final int notification_tile_bg = 2131231082;
        public static final int notify_panel_notification_icon_bg = 2131231083;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action0 = 2131296262;
        public static final int action_container = 2131296273;
        public static final int action_divider = 2131296275;
        public static final int action_image = 2131296276;
        public static final int action_text = 2131296287;
        public static final int actions = 2131296288;
        public static final int adjust_height = 2131296292;
        public static final int adjust_width = 2131296293;
        public static final int async = 2131296311;
        public static final int auto = 2131296312;
        public static final int blocking = 2131296328;
        public static final int cancel_action = 2131296354;
        public static final int chronometer = 2131296363;
        public static final int dark = 2131296386;
        public static final int end_padder = 2131296400;
        public static final int forever = 2131296413;
        public static final int icon = 2131296419;
        public static final int icon_group = 2131296420;
        public static final int icon_only = 2131296421;
        public static final int info = 2131296430;
        public static final int italic = 2131296435;
        public static final int light = 2131296444;
        public static final int line1 = 2131296445;
        public static final int line3 = 2131296446;
        public static final int media_actions = 2131296456;
        public static final int none = 2131296493;
        public static final int normal = 2131296494;
        public static final int notification_background = 2131296495;
        public static final int notification_main_column = 2131296496;
        public static final int notification_main_column_container = 2131296497;
        public static final int right_icon = 2131296514;
        public static final int right_side = 2131296515;
        public static final int standard = 2131296569;
        public static final int status_bar_latest_event_content = 2131296572;
        public static final int text = 2131296595;
        public static final int text2 = 2131296596;
        public static final int time = 2131296602;
        public static final int title = 2131296603;
        public static final int wide = 2131296629;

        private id() {
        }
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131361796;
        public static final int google_play_services_version = 2131361799;
        public static final int status_bar_notification_info_maxnum = 2131361802;

        private integer() {
        }
    }

    public static final class layout {
        public static final int notification_action = 2131427410;
        public static final int notification_action_tombstone = 2131427411;
        public static final int notification_media_action = 2131427412;
        public static final int notification_media_cancel_action = 2131427413;
        public static final int notification_template_big_media = 2131427414;
        public static final int notification_template_big_media_custom = 2131427415;
        public static final int notification_template_big_media_narrow = 2131427416;
        public static final int notification_template_big_media_narrow_custom = 2131427417;
        public static final int notification_template_custom_big = 2131427418;
        public static final int notification_template_icon_group = 2131427419;
        public static final int notification_template_lines_media = 2131427420;
        public static final int notification_template_media = 2131427421;
        public static final int notification_template_media_custom = 2131427422;
        public static final int notification_template_part_chronometer = 2131427423;
        public static final int notification_template_part_time = 2131427424;

        private layout() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131624034;
        public static final int common_google_play_services_enable_text = 2131624035;
        public static final int common_google_play_services_enable_title = 2131624036;
        public static final int common_google_play_services_install_button = 2131624037;
        public static final int common_google_play_services_install_text = 2131624038;
        public static final int common_google_play_services_install_title = 2131624039;
        public static final int common_google_play_services_notification_channel_name = 2131624040;
        public static final int common_google_play_services_notification_ticker = 2131624041;
        public static final int common_google_play_services_unknown_issue = 2131624042;
        public static final int common_google_play_services_unsupported_text = 2131624043;
        public static final int common_google_play_services_update_button = 2131624044;
        public static final int common_google_play_services_update_text = 2131624045;
        public static final int common_google_play_services_update_title = 2131624046;
        public static final int common_google_play_services_updating_text = 2131624047;
        public static final int common_google_play_services_wear_update_text = 2131624048;
        public static final int common_open_on_phone = 2131624049;
        public static final int common_signin_button_text = 2131624050;
        public static final int common_signin_button_text_long = 2131624051;
        public static final int status_bar_notification_info_overflow = 2131624227;

        private string() {
        }
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131689737;
        public static final int TextAppearance_Compat_Notification_Info = 2131689738;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131689739;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131689740;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131689741;
        public static final int TextAppearance_Compat_Notification_Media = 2131689742;
        public static final int TextAppearance_Compat_Notification_Time = 2131689743;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131689744;
        public static final int TextAppearance_Compat_Notification_Title = 2131689745;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131689746;
        public static final int Widget_Compat_NotificationActionContainer = 2131689871;
        public static final int Widget_Compat_NotificationActionText = 2131689872;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] FontFamily = {com.mansoon.BatteryDouble.R.attr.fontProviderAuthority, com.mansoon.BatteryDouble.R.attr.fontProviderCerts, com.mansoon.BatteryDouble.R.attr.fontProviderFetchStrategy, com.mansoon.BatteryDouble.R.attr.fontProviderFetchTimeout, com.mansoon.BatteryDouble.R.attr.fontProviderPackage, com.mansoon.BatteryDouble.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.mansoon.BatteryDouble.R.attr.font, com.mansoon.BatteryDouble.R.attr.fontStyle, com.mansoon.BatteryDouble.R.attr.fontVariationSettings, com.mansoon.BatteryDouble.R.attr.fontWeight, com.mansoon.BatteryDouble.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] LoadingImageView = {com.mansoon.BatteryDouble.R.attr.circleCrop, com.mansoon.BatteryDouble.R.attr.imageAspectRatio, com.mansoon.BatteryDouble.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.mansoon.BatteryDouble.R.attr.buttonSize, com.mansoon.BatteryDouble.R.attr.colorScheme, com.mansoon.BatteryDouble.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
