package com.appodeal.ads.adapters.facebook;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appodeal.ads.utils.Log;
import com.appodeal.ads.utils.LogConstants;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.AudienceNetworkAds.InitListener;
import com.facebook.ads.AudienceNetworkAds.InitResult;
import com.facebook.ads.AudienceNetworkAds.InitSettingsBuilder;
import java.util.ArrayList;
import java.util.List;

class FacebookInitializer {
    private boolean isInitializationInProgress = false;
    private boolean isInitialized = false;
    /* access modifiers changed from: private */
    public List<FacebookInitializationListener> listeners;

    public interface FacebookInitializationListener {
        void onInitializationFailed();

        void onInitialized();
    }

    FacebookInitializer() {
    }

    /* access modifiers changed from: 0000 */
    public void init(@NonNull final Activity activity, @Nullable String str, @NonNull FacebookInitializationListener facebookInitializationListener) throws Exception {
        synchronized (FacebookInitializer.class) {
            if (this.isInitialized) {
                facebookInitializationListener.onInitialized();
            } else {
                if (this.listeners == null) {
                    this.listeners = new ArrayList();
                }
                this.listeners.add(facebookInitializationListener);
            }
        }
        if (!this.isInitializationInProgress) {
            this.isInitializationInProgress = true;
            InitSettingsBuilder buildInitSettings = AudienceNetworkAds.buildInitSettings(activity);
            if (!TextUtils.isEmpty(str)) {
                buildInitSettings.withMediationService(str);
            }
            buildInitSettings.withInitListener(new InitListener() {
                public void onInitialized(InitResult initResult) {
                    FacebookInitializer.this.processFinish(activity, initResult);
                }
            }).initialize();
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isInitialized() {
        return this.isInitialized;
    }

    /* access modifiers changed from: private */
    public void processFinish(@NonNull Activity activity, final InitResult initResult) {
        final ArrayList arrayList;
        Log.log(LogConstants.KEY_NETWORK, "Log", String.format("Facebook %s", new Object[]{initResult.getMessage()}));
        this.isInitialized = initResult.isSuccess();
        this.isInitializationInProgress = false;
        if (this.listeners != null) {
            synchronized (FacebookInitializer.class) {
                arrayList = new ArrayList(this.listeners);
            }
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    for (FacebookInitializationListener facebookInitializationListener : arrayList) {
                        if (initResult.isSuccess()) {
                            facebookInitializationListener.onInitialized();
                        } else {
                            facebookInitializationListener.onInitializationFailed();
                        }
                    }
                    synchronized (FacebookInitializer.class) {
                        FacebookInitializer.this.listeners.removeAll(arrayList);
                    }
                }
            });
        }
    }
}
