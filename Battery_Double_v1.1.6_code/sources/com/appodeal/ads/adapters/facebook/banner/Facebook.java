package com.appodeal.ads.adapters.facebook.banner;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.appodeal.ads.adapters.facebook.FacebookNetwork;
import com.appodeal.ads.adapters.facebook.FacebookNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedBanner;
import com.appodeal.ads.unified.UnifiedBannerCallback;
import com.appodeal.ads.unified.UnifiedBannerParams;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

public class Facebook extends UnifiedBanner<RequestParams> {
    /* access modifiers changed from: private */
    public AdSize adSize;
    /* access modifiers changed from: private */
    public AdView adView;

    private final class Listener implements AdListener {
        private final UnifiedBannerCallback callback;

        public void onLoggingImpression(Ad ad) {
        }

        Listener(@NonNull UnifiedBannerCallback unifiedBannerCallback) {
            this.callback = unifiedBannerCallback;
        }

        public void onAdLoaded(Ad ad) {
            this.callback.onAdLoaded(Facebook.this.adView, -1, Facebook.this.adSize.getHeight());
        }

        public void onError(Ad ad, AdError adError) {
            if (ad != null) {
                ad.destroy();
            }
            if (adError != null) {
                this.callback.printError(adError.getErrorMessage(), Integer.valueOf(adError.getErrorCode()));
            }
            this.callback.onAdLoadFailed(FacebookNetwork.mapError(adError));
        }

        public void onAdClicked(Ad ad) {
            this.callback.onAdClicked();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedBannerParams unifiedBannerParams, @NonNull RequestParams requestParams, @NonNull UnifiedBannerCallback unifiedBannerCallback) throws Exception {
        if (unifiedBannerParams.needLeaderBoard(activity)) {
            this.adSize = AdSize.BANNER_HEIGHT_90;
        } else {
            this.adSize = AdSize.BANNER_HEIGHT_50;
        }
        this.adView = new AdView((Context) activity, requestParams.facebookKey, this.adSize);
        this.adView.loadAd(this.adView.buildLoadAdConfig().withAdListener(new Listener(unifiedBannerCallback)).build());
    }

    public void onDestroy() {
        if (this.adView != null) {
            this.adView.destroy();
            this.adView = null;
        }
        this.adSize = null;
    }
}
