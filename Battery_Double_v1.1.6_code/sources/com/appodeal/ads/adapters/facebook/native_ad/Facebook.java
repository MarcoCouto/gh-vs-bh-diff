package com.appodeal.ads.adapters.facebook.native_ad;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.Native.MediaAssetType;
import com.appodeal.ads.NativeAdView;
import com.appodeal.ads.NativeMediaView;
import com.appodeal.ads.adapters.facebook.FacebookNetwork;
import com.appodeal.ads.adapters.facebook.FacebookNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedAdUtils;
import com.appodeal.ads.unified.UnifiedNative;
import com.appodeal.ads.unified.UnifiedNativeAd;
import com.appodeal.ads.unified.UnifiedNativeCallback;
import com.appodeal.ads.unified.UnifiedNativeParams;
import com.appodeal.ads.utils.Log;
import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAd.AdCreativeType;
import com.facebook.ads.NativeAdBase;
import com.facebook.ads.NativeAdBase.Image;
import com.facebook.ads.NativeAdBase.MediaCacheFlag;
import com.facebook.ads.NativeAdBase.Rating;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeBannerAd;
import com.facebook.ads.internal.api.NativeAdImageApi;
import com.github.mikephil.charting.utils.Utils;

public class Facebook extends UnifiedNative<RequestParams> {
    /* access modifiers changed from: private */
    public NativeBaseAdapter nativeBaseAdapter;

    private final class Listener implements NativeAdListener {
        private final UnifiedNativeCallback callback;

        public void onLoggingImpression(Ad ad) {
        }

        public void onMediaDownloaded(Ad ad) {
        }

        Listener(@NonNull UnifiedNativeCallback unifiedNativeCallback) {
            this.callback = unifiedNativeCallback;
        }

        public void onAdLoaded(Ad ad) {
            try {
                NativeAdBase nativeAdBase = (NativeAdBase) ad;
                String access$000 = Facebook.this.getImageUrl(nativeAdBase.getAdIcon());
                String access$0002 = Facebook.this.getImageUrl(nativeAdBase.getAdCoverImage());
                if (nativeAdBase instanceof NativeBannerAd) {
                    Facebook.this.nativeBaseAdapter = new NativeBannerAdapter((NativeBannerAd) nativeAdBase, access$0002, access$000);
                } else if (nativeAdBase instanceof NativeAd) {
                    Facebook.this.nativeBaseAdapter = new NativeAdapter((NativeAd) nativeAdBase, access$0002, access$000);
                } else {
                    this.callback.onAdLoadFailed(LoadingError.NoFill);
                    return;
                }
                this.callback.onAdLoaded(Facebook.this.nativeBaseAdapter);
            } catch (Exception unused) {
                this.callback.onAdLoadFailed(LoadingError.InternalError);
            }
        }

        public void onError(Ad ad, AdError adError) {
            if (ad != null) {
                ad.destroy();
            }
            if (adError != null) {
                this.callback.printError(adError.getErrorMessage(), Integer.valueOf(adError.getErrorCode()));
            }
            this.callback.onAdLoadFailed(FacebookNetwork.mapError(adError));
        }

        public void onAdClicked(Ad ad) {
            this.callback.onAdClicked();
        }
    }

    @VisibleForTesting
    static class NativeAdapter extends NativeBaseAdapter<NativeAd> {
        private MediaView facebookMediaView;

        NativeAdapter(NativeAd nativeAd, String str, String str2) {
            super(nativeAd, str, str2);
        }

        public boolean onConfigureMediaView(@NonNull NativeMediaView nativeMediaView) {
            this.facebookMediaView = new MediaView(nativeMediaView.getContext());
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.addRule(13, -1);
            nativeMediaView.removeAllViews();
            nativeMediaView.addView(this.facebookMediaView, layoutParams);
            return true;
        }

        public boolean hasVideo() {
            return ((NativeAd) this.facebookNativeAd).getAdCreativeType() == AdCreativeType.VIDEO;
        }

        public boolean containsVideo() {
            return hasVideo();
        }

        public void onRegisterForInteraction(@NonNull NativeAdView nativeAdView) {
            super.onRegisterForInteraction(nativeAdView);
            ((NativeAd) this.facebookNativeAd).registerViewForInteraction((View) nativeAdView, this.facebookMediaView, this.facebookAdIconView, nativeAdView.getClickableViews());
        }

        public void onDestroy() {
            super.onDestroy();
            if (this.facebookMediaView != null) {
                this.facebookMediaView.destroy();
                this.facebookMediaView = null;
            }
        }
    }

    @VisibleForTesting
    static class NativeBannerAdapter extends NativeBaseAdapter<NativeBannerAd> {
        NativeBannerAdapter(NativeBannerAd nativeBannerAd, String str, String str2) {
            super(nativeBannerAd, str, str2);
        }

        public void onRegisterForInteraction(@NonNull NativeAdView nativeAdView) {
            super.onRegisterForInteraction(nativeAdView);
            ((NativeBannerAd) this.facebookNativeAd).registerViewForInteraction((View) nativeAdView, this.facebookAdIconView, nativeAdView.getClickableViews());
        }
    }

    @VisibleForTesting
    static class NativeBaseAdapter<T extends NativeAdBase> extends UnifiedNativeAd {
        MediaView facebookAdIconView;
        final T facebookNativeAd;

        NativeBaseAdapter(@NonNull T t, String str, String str2) {
            super(t.getAdvertiserName(), t.getAdBodyText(), t.getAdCallToAction(), str, str2);
            this.facebookNativeAd = t;
        }

        public Float getRating() {
            Rating adStarRating = this.facebookNativeAd.getAdStarRating();
            if (adStarRating == null || adStarRating.getValue() == Utils.DOUBLE_EPSILON) {
                return super.getRating();
            }
            return Float.valueOf((float) adStarRating.getValue());
        }

        public View obtainProviderView(@NonNull Context context) {
            AdChoicesView adChoicesView = new AdChoicesView(context, (NativeAdBase) this.facebookNativeAd, true);
            RelativeLayout relativeLayout = new RelativeLayout(context);
            relativeLayout.addView(adChoicesView, new LayoutParams(Math.round(UnifiedAdUtils.getScreenDensity(context) * 20.0f), Math.round(UnifiedAdUtils.getScreenDensity(context) * 20.0f)));
            return relativeLayout;
        }

        @Nullable
        public View obtainIconView(@NonNull Context context) {
            if (this.facebookAdIconView == null) {
                this.facebookAdIconView = new AdIconView(context);
            }
            return this.facebookAdIconView;
        }

        public void onUnregisterForInteraction() {
            super.onUnregisterForInteraction();
            this.facebookNativeAd.unregisterView();
        }

        public void onDestroy() {
            super.onDestroy();
            if (this.facebookNativeAd != null) {
                this.facebookNativeAd.setAdListener(null);
                this.facebookNativeAd.destroy();
            }
            if (this.facebookAdIconView != null) {
                this.facebookAdIconView.destroy();
                this.facebookAdIconView = null;
            }
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedNativeParams unifiedNativeParams, @NonNull RequestParams requestParams, @NonNull UnifiedNativeCallback unifiedNativeCallback) throws Exception {
        NativeAdBase nativeAdBase;
        if (unifiedNativeParams.getMediaAssetType() == MediaAssetType.ICON) {
            nativeAdBase = createNativeBannerAd(activity, requestParams.facebookKey);
        } else {
            nativeAdBase = createNativeAd(activity, requestParams.facebookKey);
        }
        nativeAdBase.loadAd(nativeAdBase.buildLoadAdConfig().withAdListener(new Listener(unifiedNativeCallback)).withMediaCacheFlag(MediaCacheFlag.ALL).build());
    }

    public void onDestroy() {
        if (this.nativeBaseAdapter != null) {
            this.nativeBaseAdapter.onDestroy();
            this.nativeBaseAdapter = null;
        }
    }

    private NativeBannerAd createNativeBannerAd(Context context, String str) {
        return new NativeBannerAd(context, str);
    }

    private NativeAd createNativeAd(Context context, String str) {
        return new NativeAd(context, str);
    }

    /* access modifiers changed from: private */
    public String getImageUrl(Image image) {
        if (image == null) {
            return null;
        }
        try {
            NativeAdImageApi nativeAdImageApi = (NativeAdImageApi) UnifiedAdUtils.getObjectByName(image, "mNativeAdImageApi");
            if (nativeAdImageApi == null) {
                return null;
            }
            return nativeAdImageApi.getUrl();
        } catch (Exception e) {
            Log.log(e);
            return null;
        }
    }
}
