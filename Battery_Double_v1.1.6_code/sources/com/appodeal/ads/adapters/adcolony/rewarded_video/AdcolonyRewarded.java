package com.appodeal.ads.adapters.adcolony.rewarded_video;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyInterstitial;
import com.appodeal.ads.adapters.adcolony.AdcolonyV3Network.RequestParams;
import com.appodeal.ads.unified.UnifiedRewarded;
import com.appodeal.ads.unified.UnifiedRewardedCallback;
import com.appodeal.ads.unified.UnifiedRewardedParams;

public class AdcolonyRewarded extends UnifiedRewarded<RequestParams> {
    private AdcolonyRewardedListener listener;
    AdColonyInterstitial rewardedVideo;

    public void load(@NonNull Activity activity, @NonNull UnifiedRewardedParams unifiedRewardedParams, @NonNull RequestParams requestParams, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) throws Exception {
        this.listener = new AdcolonyRewardedListener(unifiedRewardedCallback, this);
        AdColony.setRewardListener(this.listener);
        AdColony.requestInterstitial(requestParams.zoneId, this.listener, requestParams.adOptions);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedRewardedCallback unifiedRewardedCallback) {
        if (this.rewardedVideo == null || this.rewardedVideo.isExpired()) {
            unifiedRewardedCallback.onAdShowFailed();
        } else {
            this.rewardedVideo.show();
        }
    }

    public void onDestroy() {
        if (this.rewardedVideo != null) {
            if (this.listener == this.rewardedVideo.getListener()) {
                this.rewardedVideo.setListener(null);
            }
            this.rewardedVideo.destroy();
            this.rewardedVideo = null;
        }
    }
}
