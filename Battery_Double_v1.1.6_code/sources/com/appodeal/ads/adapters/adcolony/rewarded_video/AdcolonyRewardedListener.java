package com.appodeal.ads.adapters.adcolony.rewarded_video;

import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.adcolony.sdk.AdColonyZone;
import com.appodeal.ads.LoadingError;
import com.appodeal.ads.unified.UnifiedRewardedCallback;

class AdcolonyRewardedListener extends AdColonyInterstitialListener implements AdColonyRewardListener {
    private final AdcolonyRewarded adObject;
    private UnifiedRewardedCallback callback;

    AdcolonyRewardedListener(UnifiedRewardedCallback unifiedRewardedCallback, AdcolonyRewarded adcolonyRewarded) {
        this.callback = unifiedRewardedCallback;
        this.adObject = adcolonyRewarded;
    }

    public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
        this.adObject.rewardedVideo = adColonyInterstitial;
        this.callback.onAdLoaded();
    }

    public void onRequestNotFilled(AdColonyZone adColonyZone) {
        if (adColonyZone != null) {
            UnifiedRewardedCallback unifiedRewardedCallback = this.callback;
            StringBuilder sb = new StringBuilder();
            sb.append("request not filled for zoneId: ");
            sb.append(adColonyZone.getZoneID());
            sb.append(", isValid zone: ");
            sb.append(adColonyZone.isValid());
            unifiedRewardedCallback.printError(sb.toString(), null);
        }
        this.callback.onAdLoadFailed(LoadingError.NoFill);
    }

    public void onOpened(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onAdShown();
    }

    public void onClicked(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onAdClicked();
    }

    public void onReward(AdColonyReward adColonyReward) {
        if (adColonyReward == null || adColonyReward.success()) {
            this.callback.onAdFinished();
        }
    }

    public void onClosed(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onAdClosed();
    }

    public void onExpiring(AdColonyInterstitial adColonyInterstitial) {
        this.callback.onAdExpired();
    }
}
