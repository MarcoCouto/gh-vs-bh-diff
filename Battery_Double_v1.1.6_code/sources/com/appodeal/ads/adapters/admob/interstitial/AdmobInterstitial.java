package com.appodeal.ads.adapters.admob.interstitial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.appodeal.ads.adapters.admob.AdmobNetwork;
import com.appodeal.ads.adapters.admob.AdmobNetwork.RequestParams;
import com.appodeal.ads.unified.UnifiedInterstitial;
import com.appodeal.ads.unified.UnifiedInterstitialCallback;
import com.appodeal.ads.unified.UnifiedInterstitialParams;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;

public class AdmobInterstitial extends UnifiedInterstitial<RequestParams> {
    @Nullable
    private InterstitialAd interstitialAd;

    @VisibleForTesting
    static final class AdmobInterstitialListener extends AdListener {
        @NonNull
        private UnifiedInterstitialCallback callback;

        AdmobInterstitialListener(@NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
            this.callback = unifiedInterstitialCallback;
        }

        public void onAdLoaded() {
            super.onAdLoaded();
            this.callback.onAdLoaded();
        }

        public void onAdFailedToLoad(int i) {
            super.onAdFailedToLoad(i);
            this.callback.printError(null, Integer.valueOf(i));
            this.callback.onAdLoadFailed(AdmobNetwork.mapError(i));
        }

        public void onAdOpened() {
            super.onAdOpened();
            this.callback.onAdShown();
        }

        public void onAdLeftApplication() {
            super.onAdLeftApplication();
            this.callback.onAdClicked();
        }

        public void onAdClosed() {
            super.onAdClosed();
            this.callback.onAdClosed();
        }
    }

    public void load(@NonNull Activity activity, @NonNull UnifiedInterstitialParams unifiedInterstitialParams, @NonNull RequestParams requestParams, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) throws Exception {
        this.interstitialAd = new InterstitialAd(activity);
        this.interstitialAd.setAdUnitId(requestParams.key);
        this.interstitialAd.setAdListener(new AdmobInterstitialListener(unifiedInterstitialCallback));
        this.interstitialAd.loadAd(requestParams.request);
    }

    public void show(@NonNull Activity activity, @NonNull UnifiedInterstitialCallback unifiedInterstitialCallback) {
        if (this.interstitialAd == null || !this.interstitialAd.isLoaded()) {
            unifiedInterstitialCallback.onAdShowFailed();
        } else {
            this.interstitialAd.show();
        }
    }

    public void onDestroy() {
        if (this.interstitialAd != null) {
            this.interstitialAd.setAdListener(null);
            this.interstitialAd = null;
        }
    }
}
