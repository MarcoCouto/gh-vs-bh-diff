package com.appodeal.ads.adapters.admob;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.appodeal.ads.adapters.admob";
    public static final String BUILD_TYPE = "publish";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 2;
    public static final String VERSION_NAME = "2";
}
